# NGINX Ingress Controller
This describes the installation of the [Kubernetes NGINX Ingress Controller](https://github.com/kubernetes/ingress-nginx).

We use NGINX ingress so that we can define route rules on a per-tenant basis.

This is usually set up before the cert manager.  [This blog post helped](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes)

Origin: 
* [mandatory.yaml is from here](https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/mandatory.yaml)
* [cloud-generic.yaml is from here](https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/provider/cloud-generic.yaml)

Steps:

```bash
kubectl apply -f mandatory.yaml
kubectl apply -f cloud-generic.yaml
```

See also [cert-manager README](../cert-manager/README.md)
