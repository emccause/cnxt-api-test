# K8s Tenant Management with Kustomize
Using the picture below as an example, use `kustomize` to deploy a cnxt tenant. 

>```
>k8s/
>├── README.md
>├── base-app                           generic to cnxt-api app
>│   ├── deployment.yaml
>│   ├── kustomization.yaml
>│   └── service.yaml
>├── base-env                           goes in each env (namespace) but tenant independent
>│   ├── kustomization.yaml
>│   └── maritzmotivation-docker-pull-secret.yaml
>├── cert-manager                       tenant and env independent independent (mostly, might change for prod)
>│   ├── README.md
>│   ├── cert-manager.yaml
>│   ├── letsencrypt-prod-issuer.yaml
>│   ├── letsencrypt-staging-issuer.yaml
>│   └── namespace.yaml
>├── dev                                the "dev" env namespace
>│   ├── cnxt-ingress.yaml
>│   ├── kustomization.yaml
>│   └── namespace.yaml
>├── maritz-uat
>│   ├── application.properties
>│   ├── cnxt-db-secret.yaml
>│   ├── env-patch.yaml
>│   ├── kustomization.yaml
>│   ├── replica-patch.yaml
>│   └── workload-identity-ksa.yaml
>├── <tenant>-uat-kustomize             "root" of <tenant>-<env> kustomize build
>│   └── kustomization.yaml
>├── nginx
>│   ├── README.md
>│   ├── cloud-generic.yaml
>│   └── mandatory.yaml
>├── <tenant>-dev
>│   ├── application.properties
>│   ├── cnxt-db-secret.yaml
>│   ├── env-patch.yaml
>│   ├── kustomization.yaml
>│   ├── replica-patch.yaml
>│   └── workload-identity-ksa.yaml
>├── <tenant>-dev-kustomize             "root" of <tenant>-<env> kustomize build
>│   └── kustomization.yaml
>└── uat                                the "uat" env namespace
>    ├── cnxt-ingress.yaml
>    ├── kustomization.yaml
>    └── namespace.yaml
>```

**NOTE**: If this is the first time an application has been deployed in this cluster, deploy the [nginx ingress controller](./nginx/README.md) then the [cert-manager](./cert-manager/README.md).
This can be done initially or after the app manifests are deployed.

In all these cases below, _first_, make sure you're in the proper `gcloud` and `kubectl` context for the deployment you're checking.

TL;DR deploy everything defined in `k8s/<tenant>-<env>-kustomize/kustomization.yaml`:

```
cd k8s
kustomize build <tenant>-<env>-kustomize/ | kubectl apply -f -
```

Individual manifests can be validated by saving to a file first without deploying:

```
cd k8s
kustomize build <tenant>-<env>-kustomize/ > manifests.yaml
```

Check the difference between what you have locally and what's running in K8s:

```
kubectl diff -f manifests.yaml
    OR
kustomize build <tenant>-<env>-kustomize/ | kubectl diff -f -
```

Deploy the application for a given tenant/env:

```
kubectl apply -f manifests.yaml
    OR
kustomize build <tenant>-<env>-kustomize/ | kubectl apply -f -
```

The generated manifests can be applied and discarded.  The source YAML can always be tagged in source control and regenerated from that tag. 

