#!/usr/bin/env python3
"""
## Things to Know
* To change the docker image tag and deploy, git checkout the branch that has the image tag to
    deploy (make sure that's a branch that gets an image built and pushed - see Jenkinsfile).
    Run git log to get the latest git hash on that branch. Copy and use that for '-v' arg.
* Tools used with this script to generate and apply liquibase scripts. Expected to be in your path:
    python3         -- to run this script
    kustomize       -- to build the tenant kustomization. Note: kubectl -k doesn't work because
                        the kustomize version embedded in kubectl is too old. See this issue:
                        https://github.com/kubernetes-sigs/kustomize/issues/1647
    kubectl         -- to run the apply

## Things that Could be Addressed
* Triggered task pods cannot currently be deployed separately from their associated user request
    pods. In this script, both are deployed simultaneously.
"""
import os
import getopt
import glob
import re
import sys
import fileinput
import subprocess

TENANT_DIR = '{}*-{}'
PATCH_FILE = '/env-patch.yaml'

IMAGE_TAG_PATTERN = re.compile(r"^(\s+- image: maritzmotivation-docker.jfrog.io/cnxt-services:)(.*)$")

ENV_DEFAULT = 'uat'

CURRENT_DIR = '.'

HELP_DETAILS = """
Deploy the current cnxt-api version (git commit hash as image tag) to the given environment.
NOTE: this script will use your CURRENT KUBECTL CONTEXT. Make sure you are in the correct context for deployment.
NOTE: also that this script cannot be used to deploy triggered tasks separately. Triggered tasks will be deployed
        together with their associated tenant as defined in <tenant>-<env>-kustomize/kustomization.yaml.
  Where:
    -v is the optional git commit hash to deploy. Default is current image tag.
    -e is the optional environment to deploy in. Default is '{}'.
    -p is the optional path to search for deployment directories. Can be path to one tenant (k8s/<tenant>-<env>) but
        should at minimum point to a directory that contains <tenant>-<env> directories. Default is '{}'.
    -d is the optional deployment flag. The default is just to print what it would do and exit without deploying.
    -h is this help message.
""".format(ENV_DEFAULT, CURRENT_DIR)
HELP_MSG = __file__ + ' [-v <git_hash>] [-e <environment>] [-p <path_to_deployment_dirs>] [-d]' + HELP_DETAILS


def main(argv):
    # argument defaults
    env = ENV_DEFAULT
    path = CURRENT_DIR
    new_version = ''
    do_it = False

    # process arguments
    try:
        opts, args = getopt.getopt(argv, "v:e:p:dh")
    except getopt.GetoptError as err:
        print(str(err))
        print(HELP_MSG)
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print(HELP_MSG)
            sys.exit()
        elif opt == "-v":
            new_version = arg
        elif opt == "-d":
            do_it = True
        elif opt == "-e":
            env = arg
        elif opt == "-p":
            cwd = os.getcwd()
            if arg == CURRENT_DIR:
                path = cwd
            else:
                path = arg

    # Validate arguments
    tenants_to_deploy = []
    if path.endswith(env):
        tenants_to_deploy.append(path)
        print("path " + str(tenants_to_deploy))
    else:
        # print('>>> globing... path [{}]. env [{}]'.format(path, env) )
        tenants_to_deploy = glob.glob(TENANT_DIR.format(path, env))
        if not tenants_to_deploy:
            # if haven't matched any tenants, append a '/' on end of path. might be directory.
            tenants_to_deploy = glob.glob(TENANT_DIR.format(path + '/', env))
        # print("glob " + str(tenants_to_deploy))

    if not tenants_to_deploy:
        print("No tenants to deploy found under '" + path + "' for environment '" + env + "'")
        print(HELP_MSG)
        sys.exit()

    # find tenants, change docker image tag (version)
    find_modify_tenants(tenants_to_deploy, new_version)

    # Call deploy
    deploy(tenants_to_deploy, do_it)
###############################################################################################


###############################################################################################
# Given a list of paths to chosen tenant/envs (tenants_to_deploy) and a new docker image tag to
# deploy (new_version), modify the docker image tag in all matching k8s/<tenant>-<env>/env-patch.yaml
###############################################################################################
def find_modify_tenants(tenants_to_deploy, new_version):
    for tenantEnvDir in tenants_to_deploy:
        old_version = ''
        # modify docker image tag in env-patch.yaml in place
        with fileinput.FileInput(tenantEnvDir + PATCH_FILE, inplace=True) as env_patch:
            for line in env_patch:
                tag = re.match(IMAGE_TAG_PATTERN, line)
                if tag:
                    old_version = tag.group(2)
                    if new_version not in (None, ''):
                        print(line.replace(old_version, new_version), end='')
                    else:
                        print(line.replace(old_version, old_version), end='')
                else:
                    print(line, end='')

        if new_version in (None, ''):
            print("Keeping image tag in " + tenantEnvDir + PATCH_FILE +
                  " version: '" + old_version + "'")
        else:
            print("Modifying image tag in " + tenantEnvDir + PATCH_FILE +
                  " to version '" + new_version + "'")


###############################################################################################
# Deploy the set of tenant/envs (tenants_to_deploy). If parameter "do_it"
# is set to False, the default, the K8s deployment will NOT proceed - only the output of
# what would be deployed is printed to STDOUT.
# Assumes that docker image tag that's in env-patch.yaml is what you want to deploy.
###############################################################################################
def deploy(tenants_to_deploy, do_it=False):
    # Don't deploy triggered tasks directly. Their docker tag was modified but each will be
    # deployed along with their parent tenant as defined in <tenant>-<env>-kustomize.
    tenants_to_deploy = list(filter(lambda i: "triggered-task" not in i, tenants_to_deploy))

    # check kube ctx
    p1 = subprocess.Popen(['kubectl', 'config', 'current-context'],
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)

    cmdout, cmderr = p1.communicate()

    if cmderr:
        print("Error: " + str(cmderr))
        print(HELP_MSG)
        sys.exit(1)

    for tenantEnvDir in tenants_to_deploy:
        with open(tenantEnvDir + PATCH_FILE, 'r') as inputf:
            for line in inputf.readlines():
                tag = re.match(IMAGE_TAG_PATTERN, line)
                if tag:
                    if not do_it:
                        # only print what would happen
                        print("Tenant " + tenantEnvDir + " version [" + tag.group(2) + "]" +
                              " will be deployed to [" + str(cmdout).rstrip('\n') + "]")
                    else:
                        # run kustomize and pipe thru kubectl apply to make the change
                        kustomize_dir = tenantEnvDir + "-kustomize"
                        print("Deploying " + kustomize_dir + " version [" + tag.group(2) + "]" +
                              " to [" + str(cmdout).rstrip('\n') + "]")
                        # out = os.system("kustomize build " + kustomize_dir + " | kubectl apply -f -")

                        p1 = subprocess.Popen(["kustomize", "build", kustomize_dir],
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.STDOUT)

                        p2 = subprocess.Popen(["kubectl", "apply", "-f", "-"],
                                              stdin=p1.stdout,
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.STDOUT)
                        cmdout, cmderr = p2.communicate()
                        if cmderr:
                            print("Error: " + cmderr.decode())
                            sys.exit(1)
                        else:
                            # this will tell you what k8s is changing
                            print(cmdout.decode())

    if not do_it:
        print("Use '-d' to do the actual deployment.")
###############################################################################################


if __name__ == "__main__":
    main(sys.argv[1:])
