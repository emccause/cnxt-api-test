#!/usr/bin/env python3

"""
## Things to Know
* Database username is the "sqlserver" user (see "default_db_username"). In order to keep
    pwds out of source control, they need to be passed in.  This comes in as a mapping of
    cloud sql instance name -> database pwd.  I have a file with this mapping defined and an
    alias (pxypwds) to cat the file so that I can call this in-line, e.g.,
$ runliquibase.py -p $(pxypwds) -e uat -a
    The format of the file is
    "cnxt-preproduction-db=***,cnxt-preproduction-db-2a=***,cnxt-production-db=***,cnxt-production-db3=***"
    (one line, without quotes and *** replaced with pwd). Passwords can be resolved from terraform, e.g.,
$ terraform output cloud_sql_pwd-2

* There's a lot of tools used with this script to generate and apply liquibase scripts:
    python3         -- to run this script
    cloud_sql_proxy -- to tunnel to the cloud sql instance
    mvn             -- to generate the migration scripts
    sqlcmd          -- to apply the generated migration scripts

## Things that Could be Addressed
* Script owners should look modify the TODO area below to apply to their environment.

* Since I added ability to only write out the migrate scripts, probably should have a way to use
    them to apply to the databases instead of generating all over again.

* The script looks thru the list of database names in "databases" map definition to find tbe
    database's env. Although cnxt databases used to be named so that the env was at the end of the
    database number, they don't always do that anymore, e.g., "CCX386P_COPY" is really in the dev
    env and so does not end in the appropriate env designator. Another level of mapping is
    probably better, as in "cloud_sql_proxies" map, where the env can be explicit.

* There's a "code smell" here that there's a lot of rather small function definitions used here. It
    feels to me that some of these are needed so that they can be reused, make the code more readable
    and are fairly focused on doing one thing.  However, having a lot of small functions is not
    generally a good practice. Maybe a couple could be collapsed into one or others could be used
    in-line instead of calling functions. I would not sacrifice code readability though.

* The output is volumous when running on multiple databases. Consider writing to a file so that it
    can be given to developers for debugging.
"""
import getopt
import os
import re
import signal
import subprocess
import sys
import time
from collections import defaultdict
from datetime import datetime

###############################################################################################
# these should rarely change
###############################################################################################
envs = {'dev': 'D', 'uat': 'U', 'prod': 'P'}
database_pattern = re.compile(r"""CCX\d{3}(?P<env>.+)""")
new_database_pattern = re.compile(r"""\w+_(?P<env>.+)""")

###############################################################################################
# this is only enforced for better error msgs
###############################################################################################
build_dir = 'cnxt-api'

###############################################################################################
# TODO: these are user-env-specific. Change to match your env.
###############################################################################################
migrate_script_dest_dir = '{}/dev/tmp'.format(os.environ['HOME'])
cloud_sql_proxy_cmd = '{}/bin/cloud_sql_proxy'.format(os.environ['HOME'])
sqlcmd = '/opt/mssql-tools/bin/sqlcmd'
maven_cmd = '/usr/bin/mvn'

###############################################################################################
# cloud sql, sql server default admin user
###############################################################################################
default_db_username = 'sqlserver'

###############################################################################################
# cloud sql proxy definition maps. 'db_pwd' will be filled in by script argument.
# 'db_user' is 'default_db_username' defined above
###############################################################################################
cloud_sql_proxies = {
    'cnxt-preproduction-db': {
        'instance': 'cnxt-preproduction:us-central1:cnxt-preproduction-db=tcp:1433',
        'db_user': '{}'.format(default_db_username),
        'db_pwd': ''
    },
    'cnxt-preproduction-db-2a': {
        'instance': 'cnxt-preproduction:us-central1:cnxt-preproduction-db-2a=tcp:1433',
        'db_user': '{}'.format(default_db_username),
        'db_pwd': ''
    },
    'cnxt-production-db': {
        'instance': 'cnxt-production:us-central1:cnxt-production-db=tcp:1433',
        'db_user': '{}'.format(default_db_username),
        'db_pwd': ''
    },
    'cnxt-production-db3': {
        'instance': 'cnxt-production:us-central1:cnxt-production-db3=tcp:1433',
        'db_user': '{}'.format(default_db_username),
        'db_pwd': ''
    },
}

###############################################################################################
# database to cloud sql proxy mapping. some of the naming does not follow convention.
###############################################################################################
databases = {
    # eithicon_u still generating a 66M migrate stript that never finishes updating.
    # 'ETHICON_U': 'cnxt-preproduction-db',
    'ETHICON_P': 'cnxt-production-db',
    'ETHICON_U': 'cnxt-preproduction-db',
    'CCX302P': 'cnxt-production-db',
    'CCX302U': 'cnxt-preproduction-db',
    'CCX308U': 'cnxt-preproduction-db',
    'CCX337U': 'cnxt-preproduction-db',
    'CCX347U': 'cnxt-preproduction-db',
    'CCX357U': 'cnxt-preproduction-db-2a',
    'CCX383U': 'cnxt-preproduction-db',
    # 'CCX386D': 'cnxt-preproduction-db',
    'CCX386P_COPY': 'cnxt-preproduction-db',
    'CCX386P': 'cnxt-production-db3',
    'CCX386U': 'cnxt-preproduction-db',
    'CCX388U': 'cnxt-preproduction-db',
}

HELP_DETAILS = """
  Run CultureNext liquibase against one or more cnxt databases from a particular source branch.
  There are two steps for each; generate the liquibase SQL script first, and then apply it against the database.
  By default, we only generate the SQL scripts.
  Note this script must be run in the base directory of the cnxt-api repository and on the branch that contains
  the liquibase changes you want to deploy.
  Where:
    -s is the OPTIONAL flag to only show what would be updated and exit without running liquibase to generate the
        SQL scripts. Default is to generate. Use this to see which DBs will be updated. No passwords (-p) necessary.
    -a is the OPTIONAL flag to APPLY the generated liquibase scripts against the databases. Default is to only
        GENERATE migration scripts.
    -p is the REQUIRED comma-separated list of database server passwords to use for the '{}' user for each Cloud SQL 
        server, e.g., 'cnxt-preproduction-db=1234,cnxt-preproduction-db-2a=@*54' (single-quote list).  
        Available servers are: {}.
    -d is the OPTIONAL database name(s) to run against, e.g., 'ccx111u,ccx222u, ccx333u' (quote list). If
        database names are specified, "-e <env>" is ignored.
    -e is the OPTIONAL environment for which to update all databases. Must be one of {}. If "-d <database_name>" is
        specified, this argument is ignored.
    One of '-d' or '-e' is REQUIRED.  If both are set, '-d' overrides '-e'.
""".format(default_db_username, cloud_sql_proxies.keys(), list(envs.keys()))
HELP_MSG = __file__ + '-' + HELP_DETAILS


def main(argv):
    ###############################################################################################
    # this is only enforced for better error msgs
    ###############################################################################################
    if os.getcwd().split('/')[-1] != build_dir:
        print("Error: Must execute in {} dir.".format(build_dir))
        print(HELP_MSG)
        exit(1)

    ###############################################################################################
    # arg defaults
    ###############################################################################################
    passwords_set = False
    show_only = False
    apply = False
    dbs_by_proxy = []

    ###############################################################################################
    # parse cmd line args
    ###############################################################################################
    try:
        opts, args = getopt.getopt(argv, "p:d:e:sah")
    except getopt.GetoptError as err:
        print(str(err))
        print(HELP_MSG)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(HELP_MSG)
            sys.exit()
        elif opt == '-s':
            show_only = True
        elif opt == '-a':
            apply = True
        elif opt == '-p':
            passwords_set = set_proxy_pwds(parse_list_argument(str(arg)))
        elif opt == '-e' and not dbs_by_proxy:
            dbs_by_proxy = find_databases_by_env(str(arg))
            if not dbs_by_proxy:
                print("Found no databases in env '{}'".format(str(arg)))
                print(HELP_MSG)
                exit(2)
        elif opt == '-d':
            if ',' in str(arg):
                dbs_by_proxy = find_databases_by_name(parse_list_argument(str(arg)))
            else:
                dbs_by_proxy = find_databases_by_name([str(arg)])
            if not dbs_by_proxy:
                print("Found invalid databases in arg '{}'".format(str(arg)))
                print(HELP_MSG)
                exit(2)

    ###############################################################################################
    # enforce arg requirements
    ###############################################################################################
    if not dbs_by_proxy:
        print('One of -d or -e is required.')
        print(HELP_MSG)
        exit(2)
    elif show_only:
        show(dbs_by_proxy)
        if not passwords_set:
            print('Warning, required database passwords are missing.')
        exit(0)
    elif not passwords_set:
        print('Missing required database password.')
        print(HELP_MSG)
        exit(2)

    ###############################################################################################
    # start the machine
    ###############################################################################################
    run_cmds(dbs_by_proxy, apply)
###############################################################################################


###############################################################################################
# Start cloud_sql_proxy, run liquibase to generate scrips and, if "apply" is True, update database
# She proxy instances are sorted so that we only start a given instance once for all databases
# residing in a given instance.
###############################################################################################
def run_cmds(dbs_by_proxy, apply):
    print("-> Databases to update: {}".format(dict(dbs_by_proxy)))
    # for all cloud sql proxy instances...
    for proxy in dbs_by_proxy:
        cloud_sql_proxy_instance = cloud_sql_proxies[proxy]['instance']
        db_user = cloud_sql_proxies[proxy]['db_user']
        db_pwd = cloud_sql_proxies[proxy]['db_pwd']

        # current cloud_sql_proxy pid
        cloud_sql_pid = 0
        try:
            # start cloud_sql_proxy --
            cloud_sql_pid = start_cloud_sql_proxy(cloud_sql_proxy_cmd, cloud_sql_proxy_instance)
            # hope it starts in time
            time.sleep(3)

            # for all dbs in proxy instance...
            for db in dbs_by_proxy[proxy]:
                # (1) Run liquibase to generate migration script
                print('-> Running liquibase script generation on {} db_user: {} db-pwd: {} cloud_sql_pxy: {} '
                      # .format(db, db_user, db_pwd, cloud_sql_proxy_instance))
                      .format(db, "***", "***", cloud_sql_proxy_instance))
                run_liquibase(db, db_user, db_pwd)
                migrate_script = move_file('./target/liquibase', migrate_script_dest_dir,
                                           db + '_' + str(datetime.now().strftime("%Y-%m-%d_%H-%M-%S")))

                # (2) exec sqlcmd if apply script if (-a) arg was given
                if apply:
                    print('Applying migration script to database {}'.format(db))
                    run_sqlcmd(sqlcmd, migrate_script, db_user, db_pwd)
                    print('-> finished updating {}'.format(db))
        finally:
            # stop cloud_sql_proxy
            stop_cloud_sql_proxy(cloud_sql_pid)


###############################################################################################
# Run the given database sql script  with sqlcmd
###############################################################################################
def run_sqlcmd(cmd, script, db_user, db_pwd):
    mycmd = "{} -S 127.0.0.1,1433 -U {} -P '{}' -i {}".format(cmd, db_user, db_pwd, script)
    print('-> Running sqlcmd {}'.format(mycmd))
    p1 = subprocess.Popen(mycmd,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT,
                          shell=True)
    cmdout, cmderr = p1.communicate()
    if cmderr:
        print("Error: " + str(cmderr))
        print(HELP_MSG)
        sys.exit(1)

    print("-> Output:\n" + cmdout.decode())


###############################################################################################
# Start cloud_sql_proxy instance in background and return the pid
###############################################################################################
def start_cloud_sql_proxy(cmd, instance):
    call = [cmd, '-instances=' + instance, '&']
    print('-> Startring cloud_sql_proxy {}.'.format(call))
    p1 = subprocess.Popen(call)

    return p1.pid


###############################################################################################
# Kill the given pid with SIGINT, which is the running cloud_sql_proxy instance
###############################################################################################
def stop_cloud_sql_proxy(pid):
    if pid and pid > 0:
        print('-> Shutting down cloud_sql_proxy {}.'.format(pid))
        os.kill(pid, signal.SIGINT)


###############################################################################################
# Run liquibase on a given database to generate a migration script
###############################################################################################
def run_liquibase(database, db_user, db_pwd):
    print('-> Running liquibase on {}.'.format(database))
    lb_props = [maven_cmd, 'clean', 'liquibase:updateSQL', '-Dliquibase.logging=warning', '-Dliquibase.contexts=live',
                '-Dliquibase.changelogSchemaName=component',
                '-Dliquibase.username={}'.format(db_user),
                '-Dliquibase.password={}'.format(db_pwd),
                '-Dliquibase.url=jdbc:sqlserver://localhost:1433;databaseName={}'.format(database)]
    # run mvn liquibase
    p1 = subprocess.Popen(lb_props,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    cmdout, cmderr = p1.communicate()
    if cmderr:
        print("Error: " + str(cmderr))
        print(HELP_MSG)
        sys.exit(1)

    output = cmdout.decode('UTF8')
    if 'ERROR' in output:
        print(">>> Liquibase FAILED for database: {}".format(database), file=sys.stderr)
        for line in output.split('\n'):
            if 'ERROR' in line:
                print(">>> {}".format(line))
    else:
        print("-> Output:\n" + output, file=sys.stdout)


###############################################################################################
# Move the generated "migrate.sql" file in "src_dir" to "target_dir" and tag with "postpend"
# (in front of file extension).
###############################################################################################
def move_file(src_dir, target_dir, postpend):
    src_file = src_dir + '/migrate.sql'
    dest_file = target_dir + '/migrate_' + postpend + '.sql'
    print('-> Moving {} to {}.'.format(src_file, dest_file))
    os.rename(src_file, dest_file)
    return dest_file


###############################################################################################
# Find and return all of the databases in an "env" and return the mapping of db -> cloud sql
# instance
###############################################################################################
def find_databases_by_env(env):
    indicator = envs[env.lower()]
    csps = defaultdict(list)
    for db, csp in databases.items():
        search = re.search(database_pattern, db)
        if not search:
            search = re.search(new_database_pattern, db)
        if search['env'] == indicator:
            csps[csp].append(db)
    return csps


###############################################################################################
# Find and return all of the databases in an "db_candidates" (candidate name) and return the
# mapping of db -> cloud sql instance
###############################################################################################
def find_databases_by_name(db_candidates):
    csps = defaultdict(list)
    for candidate in db_candidates:
        if candidate.upper() in databases.keys():
            csps[databases[candidate.upper()]].append(candidate.upper())
        else:
            print("Unknown database '{}'".format(candidate))
    return csps


###############################################################################################
# print the "db_dict" showing the databases that would be updated
###############################################################################################
def show(db_dict):
    for proxy in db_dict:
        cloud_sql_proxy_instance = cloud_sql_proxies[proxy]['instance']
        db_user = cloud_sql_proxies[proxy]['db_user']
        db_pwd = cloud_sql_proxies[proxy]['db_pwd']
        print("-> Will run liquibase on:")
        for db in db_dict[proxy]:
            # jenkins masks pwd with '***'
            print('  -> {} db_user: {} db-pwd: {} cloud_sql_pxy: {} '.format(db, db_user, db_pwd,
                                                                             cloud_sql_proxy_instance))


###############################################################################################
# print the "db_dict" showing the databases that would be updated
###############################################################################################
def set_proxy_pwds(pwd_map):
    for pwd in pwd_map:
        k, v = re.split(r"\s*=\s*", pwd)
        if k.lower() in cloud_sql_proxies.keys():
            cloud_sql_proxies[k.lower()]['db_pwd'] = v
        else:
            print('Unknown database server {}.'.format(k))
            return False
    return True


###############################################################################################
# Separate the given "arg_str" (comma- and/or space-separated) and return the list
###############################################################################################
def parse_list_argument(arg_str):
    return list(map(str.strip, filter(None, re.split(r",\s*", arg_str))))


###############################################################################################
# Custom SIGINT handler traps SIGINT, replaces with original handler and gracefully exits
###############################################################################################
def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)
    sys.exit(0)


###############################################################################################
# Save off current SIGINT handler and replace with custom (see above) to handle SIGINT
###############################################################################################
if __name__ == "__main__":
    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)
    main(sys.argv[1:])
