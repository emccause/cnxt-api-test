#!/usr/bin/env python3

import signal
import sys
import time
from datetime import datetime
import getopt
import re
import subprocess

ENVS = ['uat', 'dev', 'prod']
POD_START_MATCH = 'Started Application in'

HELP_DETAILS = """
Poll cnxt pod logs in given environment (namespace) to see when they have started and how long they took to start.
NOTE: This script will use your current kubectl context. Make sure you are in the correct context for the cluster.
NOTE: Also, depending how long a pod has been started (days?), the logs message we're looking for, '{}',
may be in a previously rolled log.  If so, we'll never find it.
  Where:
    -p is the optional comma-separated list of one or more pod name patterns to search for, 
        e.g., "nov,san, marit" (quote list if with spaces).  Default is all pods in namespace.
    -e is the optional environment to look for pods. Default is '{}'. Must be one of '{}'.
    -l is the optional flag to loop forever (ctl-C to quit). Useful for polling during pod startup. Default is once.
    -o is the optional flag to include pods that have already started (READY = 1/1).
        It's important to note, however, that logs may have rolled by this time and we may have missed the '{}'
        log message.
    -h is this help message.
""".format(POD_START_MATCH, ENVS[0], ENVS, POD_START_MATCH)
HELP_MSG = __file__ + ' -p <pod_name_part> [-e <environment>] [-l]' + HELP_DETAILS


def main(argv):
    namespace = ENVS[0]
    pod_list = []
    loop_forever = False
    include_old = False

    try:
        opts, args = getopt.getopt(argv, "e:p:loh")
    except getopt.GetoptError as err:
        print(str(err))
        print(HELP_MSG)
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print(HELP_MSG)
            sys.exit()
        elif opt == "-e":
            namespace = str(arg).lower()
        elif opt == "-l":
            loop_forever = True
        elif opt == "-o":
            include_old = True
        elif opt == "-p":
            pods = str(arg)
            if ',' in pods:
                pod_list = list(map(str.strip, filter(None, re.split(r",\s*", pods))))
            else:
                pod_list.append(pods)

            # Check args
    if namespace not in ENVS:
        print("Environment '{}' not found. Must be one of '{}'".format(namespace, ENVS))
        print(HELP_MSG)
        sys.exit(2)

    print("Searching env: '{}'".format(namespace))
    # Collect pods of interest
    pods = get_pods_in_namespace(namespace, pod_list, include_old)

    if loop_forever:
        start_time = datetime.now()
        count = 0
        while True:
            diff = (datetime.now() - start_time)
            print("Running for {}".format(diff))
            grep_pod_logs(namespace, pods)
            print('===')
            time.sleep(3)
            if count % 5 == 0:
                pods = get_pods_in_namespace(namespace, pod_list, include_old)
            count += 1
    else:
        grep_pod_logs(namespace, pods)


def get_pods_in_namespace(namespace, pod_list, include_old):
    # Get list of all pods in namespace
    p1 = subprocess.Popen(['kubectl', 'get', 'pods', '--namespace=' + namespace, '--no-headers'],
                          stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT)
    cmdout, cmderr = p1.communicate()
    if cmderr:
        print("Error: " + str(cmderr))
        print(HELP_MSG)
        sys.exit(1)

    # If no pods found, find out why, report, exit
    if not cmdout:
        p1 = subprocess.Popen(['kubectl', 'get', 'namespace', namespace, '-o=name'],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)
        cmdout, cmderr = p1.communicate()
        if cmdout:
            print("Error: " + str(cmdout.decode()))
            print(HELP_MSG)
            sys.exit(1)

    pods = []
    for pod_info in cmdout.decode().split('\n'):
        if not pod_info:
            continue
        interested, pod = new_pod_of_interest(pod_info, pod_list, include_old)
        if interested:
            pods.append(pod)

    if not pods:
        if not pod_list:
            print("Didn't find any new pods in namespace '{}'. Try '-o'?".format(namespace))
        else:
            print("Didn't find any new pods in namespace '{}' matching name '{}'. Try '-o'?".format(namespace, str(pod_list)))
        exit(0)

    return pods


def new_pod_of_interest(podinfo, pod_list, include_old):
    pod_info_list = podinfo.split()
    if (not pod_list or any(word in pod_info_list[0] for word in pod_list)) and (include_old or pod_info_list[1] == '0/1'):
        # print('pod: ' + pod_info_list[0] + ' ready: ' + pod_info_list[1] + ' since: ' + pod_info_list[4] + ' ***')
        return True, pod_info_list[0]
    return False, ''


def grep_pod_logs(namespace, pods):
    # Get pod logs, grep for POD_START_MATCH
    for pod in pods:
        line = [pod + ':']

        p1 = subprocess.Popen(['kubectl', 'logs', pod, '--namespace=' + namespace],
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)

        p2 = subprocess.Popen(["grep", POD_START_MATCH],
                              stdin=p1.stdout,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT)
        cmdout, cmderr = p2.communicate()
        if cmderr:
            print("Error: " + cmderr.decode())
            sys.exit(1)
        else:
            # Do a bit of log msg cleanup. We only want the msg date/time and the startup msg
            pod_log = list(map(str.strip, filter(None, re.split(r"\s+", cmdout.decode()))))
            if not pod_log:
                line.append(" --- ")
            else:
                line.append(pod_log[0] + "T" + pod_log[1])
                line.append(' '.join(map(str, pod_log[-9:])))

            print(' '.join(map(str, line)))


def exit_gracefully(signum, frame):
    # restore the original signal handler as otherwise evil things will happen
    # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
    signal.signal(signal.SIGINT, original_sigint)
    sys.exit(0)


if __name__ == "__main__":
    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)
    main(sys.argv[1:])
