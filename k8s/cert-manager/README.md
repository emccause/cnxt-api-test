# Certificate Management
Currently using [jetstack cert manager](https://github.com/jetstack/cert-manager), ([download](https://github.com/jetstack/cert-manager/releases/download/v0.12.0/cert-manager.yaml)), to manage certs with [Let's Encrypt](https://blog.acolyer.org/2020/02/12/lets-encrypt-an-automated-certificate-authority-to-encrypt-the-entire-web/) as the issuer. We will want a more permanent method to handle certs in the future.

For now, this is a manual addition to the cluster.  After application is installed (see parent [README](../README.md)), create the cert-manager `namespace`, create the `cert-manager` then create only the `letsencrypt-prod-issuer`.  The `prod-issuer` is throttled but it's an official signed cert provided by letsencrypt, where the `staging-issuer`, though un-throttled, provides a self-signed cert and the m365 UI balks at it. 

This is usually set up after the ingress controller.

Steps:

```bash
kubectl apply -f namespace.yaml
kubectl apply -f cert-manager.yaml
```

Wait until webhook pod is ready...

```
kubectl apply -f letsencrypt-prod-issuer.yaml
```

It will take a while for the cert to be issued.
See also [nginx controller README](../nginx/README.md)

 