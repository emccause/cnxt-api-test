-------------------------------------------------------------------------------------------
-- BUDGET_USER is SUBJECT_TABLE = group that budget is currently tied to via ASSOCIATION --
-------------------------------------------------------------------------------------------    
MERGE component.ACL AS Target
USING (
    SELECT A.FK1 AS BUDGET_ID, A.FK2 AS GROUP_ID, R.ROLE_ID
    FROM component.ASSOCIATION A
        JOIN component.ASSOCIATION_TYPE AT 
            ON  A.ASSOCIATION_TYPE_ID = AT.ASSOCIATION_TYPE_ID
        JOIN component.ROLE R ON R.ROLE_CODE = 'BUDGET_USER'
    WHERE AT.ASSOCIATION_TYPE_DESC = 'BUDGET_GROUPS'
) AS Source
ON (
    Target.ROLE_ID = Source.ROLE_ID
    AND Target.TARGET_TABLE = 'BUDGET'
    AND Target.TARGET_ID = Source.BUDGET_ID
    AND Target.SUBJECT_TABLE = 'GROUPS'
    AND Target.SUBJECT_ID = Source.GROUP_ID
)
WHEN NOT MATCHED
    THEN INSERT(ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) 
    VALUES(Source.ROLE_ID, 'BUDGET', Source.BUDGET_ID, 'GROUPS', Source.GROUP_ID);
    
    
-----------------------------------------------------------------------------------------------------
-- BUDGET_MISC of IS_USABLE is FALSE for the parent distributed budget, TRUE for all child budgets --
-----------------------------------------------------------------------------------------------------            
MERGE component.BUDGET_MISC AS Target
USING (
    SELECT ID,
        CASE
            WHEN PARENT_BUDGET_ID IS NULL THEN 'FALSE'    
            ELSE 'TRUE'
        END AS IS_USABLE
    FROM component.BUDGET
    WHERE BUDGET_TYPE_CODE = 'DISTRIBUTED'
) AS Source
ON (
    Target.BUDGET_ID = Source.ID
    AND Target.VF_NAME = 'IS_USABLE'
)
WHEN NOT MATCHED
    THEN INSERT(BUDGET_ID, VF_NAME, MISC_DATA, MISC_DATE) 
    VALUES(Source.ID, 'IS_USABLE', Source.IS_USABLE, GETDATE());