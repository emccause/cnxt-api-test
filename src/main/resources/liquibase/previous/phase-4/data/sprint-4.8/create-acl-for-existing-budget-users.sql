

--Create BUDGET_USER ACL's for every existing DISTRIBUTED child budget
DECLARE @roleId BIGINT
DECLARE @budgetId BIGINT
DECLARE @groupId BIGINT

SET @roleId = (SELECT ROLE_ID FROM component.ROLE WHERE ROLE_CODE = 'BUDGET_USER')

--Loop through all associations and insert an ACL to match
DECLARE id_cursor CURSOR FOR
    SELECT DISTINCT a.FK1, a.FK2
    FROM component.BUDGET b
    JOIN component.ASSOCIATION a ON a.ASSOCIATION_TYPE_ID = 3600 AND a.FK1 = b.ID
    WHERE b.BUDGET_TYPE_CODE = 'DISTRIBUTED' AND PARENT_BUDGET_ID IS NOT NULL
            
OPEN id_cursor
    
FETCH NEXT FROM id_cursor
    INTO @budgetId, @groupId
    
WHILE @@FETCH_STATUS = 0
BEGIN 
    IF NOT EXISTS (SELECT * FROM component.ACL WHERE ROLE_ID = @roleId AND TARGET_ID = @budgetId AND TARGET_TABLE = 'BUDGET' AND SUBJECT_ID = @groupId AND SUBJECT_TABLE = 'GROUPS')
    BEGIN
        INSERT INTO component.ACL (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) VALUES
        (@roleId, 'BUDGET', @budgetId, 'GROUPS', @groupId)
    END

    FETCH NEXT FROM id_cursor
    INTO @budgetId, @groupId
END
CLOSE id_cursor;
DEALLOCATE id_cursor;


--Create BUDGET_USER ACL's for every existing DISTRIBUTED parent budget
--Loop through all associations and insert an ACL to match
DECLARE id_cursor CURSOR FOR
    SELECT DISTINCT a.FK1, a.FK2
    FROM component.BUDGET b
    JOIN component.ASSOCIATION a ON a.ASSOCIATION_TYPE_ID = 3500 AND a.FK1 = b.ID
    WHERE b.BUDGET_TYPE_CODE = 'DISTRIBUTED' AND PARENT_BUDGET_ID IS NULL
            
OPEN id_cursor
    
FETCH NEXT FROM id_cursor
    INTO @budgetId, @groupId
    
WHILE @@FETCH_STATUS = 0
BEGIN 
    IF NOT EXISTS (SELECT * FROM component.ACL WHERE ROLE_ID = @roleId AND TARGET_ID = @budgetId AND TARGET_TABLE = 'BUDGET' AND SUBJECT_ID = @groupId AND SUBJECT_TABLE = 'GROUP_CONFIG')
    BEGIN
        INSERT INTO component.ACL (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) VALUES
        (@roleId, 'BUDGET', @budgetId, 'GROUP_CONFIG', @groupId)
    END

    FETCH NEXT FROM id_cursor
    INTO @budgetId, @groupId
END
CLOSE id_cursor;
DEALLOCATE id_cursor;