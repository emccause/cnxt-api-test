/****** Object:  Table [component].[NOMINATION_MISC]    ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [component].[NOMINATION_MISC](
  [NOMINATION_MISC_ID] [bigint] IDENTITY(1,1) NOT NULL,
  [NOMINATION_ID] [bigint] NOT NULL,
  [VF_NAME] [nvarchar](128) NOT NULL,
  [MISC_DATA] [nvarchar](255) NULL,
  [MISC_DATE] [datetime2](7) NULL,
  [CREATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_NOMINATION_MISC_CREATE_DATE] DEFAULT (getdate()),
  [CREATE_ID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_NOMINATION_MISC_CREATE_ID] DEFAULT (user_name()),
  [UPDATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_NOMINATION_MISC_UPDATE_DATE] DEFAULT (getdate()),
  [UPDATE_ID] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_NOMINATION_MISC_UPDATE_ID] DEFAULT (user_name())
CONSTRAINT [PK_NOMINATION_MISC] PRIMARY KEY CLUSTERED
(
  [NOMINATION_MISC_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [component].[NOMINATION_MISC]
WITH CHECK ADD  CONSTRAINT [FK_NOMINATION_ID] FOREIGN KEY([NOMINATION_ID])
REFERENCES [component].[NOMINATION] ([ID])
GO

-- ==============================  NAME  ======================================
-- TR_NOMINATION_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR          DATE            STORY         CHANGE DESCRIPTION
-- osmanos        20161024                  Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_NOMINATION_MISC_U_1]
ON [component].[NOMINATION_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    NOMINATION_MISC T1
    ,       INSERTED T2
    WHERE   T1.NOMINATION_MISC_ID = T2.NOMINATION_MISC_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    NOMINATION_MISC T1
    ,       INSERTED T2
    WHERE   T1.NOMINATION_MISC_ID = T2.NOMINATION_MISC_ID
END

RETURN

END
GO