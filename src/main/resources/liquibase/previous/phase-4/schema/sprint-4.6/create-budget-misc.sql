CREATE TABLE [component].[BUDGET_MISC](
    [BUDGET_MISC_ID] [bigint] IDENTITY(1,1) NOT NULL,
    [BUDGET_ID] [bigint] NOT NULL,
    [VF_NAME] [nvarchar](128) NOT NULL,
    [MISC_DATA] [nvarchar](255) NULL,
    [MISC_DATE] [datetime2](7) NULL,
    [CREATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_BUDGET_MISC_CREATE_DATE]  DEFAULT (getdate()),
    [CREATE_ID] [nvarchar](50) NOT NULL CONSTRAINT [DF_BUDGET_MISC_CREATE_ID]  DEFAULT (user_name()),
    [UPDATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_BUDGET_MISC_UPDATE_DATE]  DEFAULT (getdate()),
    [UPDATE_ID] [nvarchar](50) NOT NULL CONSTRAINT [DF_BUDGET_MISC_UPDATE_ID]  DEFAULT (user_name()),
 CONSTRAINT [PK_BUDGET_MISC] PRIMARY KEY NONCLUSTERED 
(
    [BUDGET_MISC_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [component].[BUDGET_MISC]  WITH CHECK ADD  CONSTRAINT [FK_BUDGET_MISC_BUDGET] FOREIGN KEY([BUDGET_ID])
REFERENCES [component].[BUDGET] ([ID])
GO

ALTER TABLE [component].[BUDGET_MISC] CHECK CONSTRAINT [FK_BUDGET_MISC_BUDGET]
GO

ALTER TABLE [component].[BUDGET_MISC]  WITH CHECK ADD  CONSTRAINT [CK_BUDGET_MISC_VF_NAME] CHECK  ((isnumeric(left([VF_NAME],(1)))=(0)))
GO

ALTER TABLE [component].[BUDGET_MISC] CHECK CONSTRAINT [CK_BUDGET_MISC_VF_NAME]
GO

CREATE UNIQUE CLUSTERED INDEX [IX_BUDGET_MISC_1] ON [component].[BUDGET_MISC]
(
    [BUDGET_ID] ASC,
    [VF_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


CREATE NONCLUSTERED INDEX [IX_BUDGET_MISC_2] ON [component].[BUDGET_MISC]
(
    [VF_NAME] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [IX_BUDGET_MISC_VF_NAME_MISC_DATA] ON [component].[BUDGET_MISC]
(
    [VF_NAME] ASC,
    [MISC_DATA] ASC
)
INCLUDE ([BUDGET_ID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


CREATE TABLE [component].[HISTORY_BUDGET_MISC](
    [HISTORY_ID] [bigint] IDENTITY(1,1) NOT NULL,
    [BUDGET_MISC_ID] [bigint] NULL,
    [BUDGET_ID] [bigint] NULL,
    [VF_NAME] [nvarchar](128) NULL,
    [MISC_DATA] [nvarchar](255) NULL,
    [MISC_DATE] [datetime2](7) NULL,
    [CREATE_DATE] [datetime2](7) NULL,
    [CREATE_ID] [nvarchar](50) NULL,
    [UPDATE_DATE] [datetime2](7) NULL,
    [UPDATE_ID] [nvarchar](50) NULL,
    [CHANGE_DATE] [datetime2](7) NULL CONSTRAINT [DF_HISTORY_BUDGET_MISC_CHANGE_DATE]  DEFAULT (getdate()),
    [CHANGE_ID] [nvarchar](80) NULL,
    [CHANGE_OPERATION] [nchar](1) NULL,
 CONSTRAINT [PK_HISTORY_BUDGET_MISC] PRIMARY KEY CLUSTERED 
(
    [HISTORY_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- ==============================  NAME  ======================================
-- TR_BUDGET_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta       20040101                Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_BUDGET_MISC_U_1]
ON [component].[BUDGET_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    BUDGET_MISC T1
    ,       INSERTED T2
    WHERE   T1.BUDGET_MISC_ID = T2.BUDGET_MISC_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    BUDGET_MISC T1
    ,       INSERTED T2
    WHERE   T1.BUDGET_MISC_ID = T2.BUDGET_MISC_ID
END

RETURN

END
GO

-- ==============================  NAME  ======================================
-- TR_BUDGET_MISC_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta        20070207                Created
-- ericksrt        20150819                Removed UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_BUDGET_MISC_DU_1]
ON [component].[BUDGET_MISC]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

--EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

--SET @change_id = ISNULL(@change_id, USER_NAME())
SET @change_id = USER_NAME()

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_BUDGET_MISC (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    BUDGET_MISC_ID
,    BUDGET_ID
,    VF_NAME
,    MISC_DATA
,    MISC_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    BUDGET_MISC_ID
,    BUDGET_ID
,    VF_NAME
,    MISC_DATA
,    MISC_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''BUDGET_MISC''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO