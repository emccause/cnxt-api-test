/****** Object:  UserDefinedFunction [component].[UF_LIST_TO_TABLE_ID]    Script Date: 1/23/2017 10:03:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--Function to split a comma delimited string representing a list of IDs to a table
CREATE function [component].[UF_LIST_TO_TABLE_ID]
 (
    @List NVARCHAR(MAX),
    @Separator CHAR
  )
RETURNS
  @Table TABLE
  (
  items BIGINT
  )
AS
  BEGIN
    DECLARE @item NVARCHAR(MAX),
            @pos BIGINT

    IF (@List is NULL) OR (len(@List) < 1)
      RETURN

    SET @pos = 1

    WHILE (@pos != 0)
      BEGIN
        SET @pos = CHARINDEX(@Separator, @List)
        IF (@pos != 0)
          SET @item = LEFT(@List, @pos - 1)
        ELSE
          SET @item = @List

        IF (LEN(@item) > 0)
          INSERT INTO @Table(items) values (@item)

        SET @List = RIGHT(@List, LEN(@List) - @pos)

        IF (LEN(@List) = 0) BREAK
      END
  RETURN
END

GO


