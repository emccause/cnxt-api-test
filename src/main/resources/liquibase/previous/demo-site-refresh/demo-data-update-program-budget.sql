--1 update specific budgets from we.com as below (placeholder for WIP )
--these will only get renamed if they exist in source DB
--Simple budgets
UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = REPLACE(BUDGET_DISPLAY_NAME,'Standing Ovation', 'Wellness Budget'),
BUDGET_NAME = REPLACE(BUDGET_NAME,'Standing Ovation', 'Wellness Budget')
 WHERE BUDGET_TYPE_CODE='SIMPLE' AND  BUDGET_DISPLAY_NAME LIKE '%Standing Ovation%'

 UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = REPLACE(BUDGET_DISPLAY_NAME,'AGL  Darlington', 'Manager Budget'),
BUDGET_NAME = REPLACE(BUDGET_NAME,'AGL Darlington', 'Manager Budget')
 WHERE BUDGET_TYPE_CODE='SIMPLE' AND  BUDGET_DISPLAY_NAME LIKE '%AGL  Darlington%'

 UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = REPLACE(BUDGET_DISPLAY_NAME,'AGL Barbee', 'Leadership Budget'),
BUDGET_NAME = REPLACE(BUDGET_NAME,'AGL Barbee', 'Leadership Budget')
 WHERE BUDGET_TYPE_CODE='SIMPLE' AND  BUDGET_DISPLAY_NAME LIKE '%AGL Barbee%'

 UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = REPLACE(BUDGET_DISPLAY_NAME,'Leadership Achievement Award', 'Leadership Award'),
BUDGET_NAME =  REPLACE(BUDGET_NAME,'Leadership Achievement Award', 'Leadership Award')
 WHERE BUDGET_TYPE_CODE='SIMPLE' AND  BUDGET_DISPLAY_NAME LIKE '%Leadership Achievement Award%'

 
 --Distributed budgets 
UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = REPLACE(BUDGET_DISPLAY_NAME,'We.com Performance Plus', 'Excellence in Action'),
BUDGET_NAME =  REPLACE(BUDGET_NAME,'We.com Performance Plus', 'Excellence in Action')
 WHERE  BUDGET_TYPE_CODE='DISTRIBUTED' AND BUDGET_DISPLAY_NAME LIKE '%We.com Performance Plus%' 
 
 --prefix the rest with a predetermined prefix (prefix is determined to be zd_ )
UPDATE component.BUDGET set BUDGET_DISPLAY_NAME = 'zd_' + BUDGET_DISPLAY_NAME
WHERE  BUDGET_DISPLAY_NAME not like ('%Excellence in Action')
and BUDGET_DISPLAY_NAME not like ('%Wellness Budget')
and BUDGET_DISPLAY_NAME not like ('%Manager Budget')
and BUDGET_DISPLAY_NAME not like ('%Leadership Budget')
and BUDGET_DISPLAY_NAME not like ('%Leadership Award')
and BUDGET_DISPLAY_NAME not like ('%Remarkable Results!')
 
--2 Programs - update specific programs from we.com as below 
UPDATE component.PROGRAM set PROGRAM_NAME = REPLACE(PROGRAM_NAME,'Celebrating You!','Celebrating You'), 
PROGRAM_DESC='Celebrate life events by sending an ecard!',
PROGRAM_LONG_DESC='<p>Send an ecard to anyone in celebration of a birthday, marriage, new house, baby or any other exciting moment!</p><p><strong>Everyone is eligible to both send and receive recognition through this program.</strong></p><p>There are no approvals required for recognitions submitted using this program.</p><p><em><strong>Check out the calendar for upcoming birthdays!</strong></em></p>'
 WHERE  PROGRAM_NAME='Celebrating You!'

UPDATE component.PROGRAM set PROGRAM_NAME = REPLACE(PROGRAM_NAME,'Cheers to Your Years!','Cheers to Your Years'), 
PROGRAM_DESC='Send an ecard to celebrate a work anniversary.',
PROGRAM_LONG_DESC='<p>Send an ecard to congratulate a co-worker on their work anniversary!</p><p><strong>Everyone is eligible to both send and receive recognition through this program.</strong></p><p>There are no approvals required for recognitions submitted using this program.</p><p><em><strong>Check out the calendar for upcoming anniversaries!</strong></em></p>'
 WHERE  PROGRAM_NAME='Cheers to Your Years!'

UPDATE component.PROGRAM set PROGRAM_NAME = REPLACE(PROGRAM_NAME,'Compass Kudos (eCard)','Everyday Appreciation'), 
PROGRAM_DESC='Recognize everyday contributions connected to our purpose.',
PROGRAM_LONG_DESC='<p>This is our peer-to-peer recognition program, where you can send your appreciation and thanks for everyday contributions connected to our purpose.</p><p><strong>Everyone is eligible to both send and receive recognition through this program.</strong></p><p>There are no approvals required for recognitions submitted using this program.</p>'
 WHERE  PROGRAM_NAME='Compass Kudos (eCard)'

UPDATE component.PROGRAM set PROGRAM_NAME= REPLACE(PROGRAM_NAME,'We.com Performance Plus (Points)','Excellence in Action'),
PROGRAM_DESC= 'Recognize actions that go above and beyond to support our purpose.',
PROGRAM_LONG_DESC='<p>Managers can use this program to recognize employees with points for above and beyond performance that is aligned to our purpose.</p><p><strong>Eligibility: Everyone is eligible to receive recognition through this program. Only managers are eligible to give recognition from this program.</strong></p><p>Approvals: Recognitions from this program will be sent to the recipient’s manager for approval.</p>'
 WHERE  PROGRAM_NAME='We.com Performance Plus (Points)'

UPDATE component.PROGRAM set PROGRAM_NAME= REPLACE(PROGRAM_NAME,'Standing Ovation Annual Award (Nomination)','Wellness Program'), 
PROGRAM_DESC='Recognize dedication and achievement in wellness goals and initiatives.',
PROGRAM_LONG_DESC='<p>Program Overview<br />The Wellness Program is an ongoing recognition program for Engage Industries individuals based on the individual’s goals and achievements during a 30 day period. This program honors individuals who select and achieve a goal during this time period. Those who consistently set and achieve their goals are continuously rewarded and recognized.</p><p>Program Period<br />The program period for Wellness Program recognition is the previous 30 days.</p><p>Eligible Participants<br />All regular full-time and part-time employees are eligible for Wellness Program recognition.</p><p>Step Levels</p><p>Casual Stroller 0-4,9999 steps per day<br />Forest Hiker 5,000-7,499 steps per day<br />Mountain Trekker 7,500-9,999 steps per day<br />Marathon Mover 10,000-12,4999 steps per day<br />Himalayan Sherpa 12,500 + steps per day<br /> <p>Point Award Amounts</p><p>Casual Stroller 150 points for achieving Stroller level for 2 continuous weeks.<br />Forest Hiker 400 points for achieving Hiker level for 2 continuous weeks.<br />Mountain Trekker 750 points for achieving Trekker level for 4 continuous weeks.<br />Marathon Mover 1,000 points for achieving Marathon level for 4 continuous weeks.<br />Himalayan Sherpa 1,400 points for achieving Sherpa level for 4 continuous weeks.<br /><p>You are eligible to receive points for achieving one level per 30 days.</p><p>Achieving Himalayan Sherpa status will also include additional recognition during our company wide meetings and at other exciting events throughout the year!</p><p>Congratulations to those who have achieved Sherpa Status as of July 2015:<br />Warren<br />Rachel<br />Adam<br />Upton<br />Buffy<br />Hayfa <br />Maris<br />Acton <br />Catherine<br />Reese<br /> </p>'
 WHERE  PROGRAM_NAME='Standing Ovation Annual Award (Nomination)'

UPDATE component.PROGRAM set 
PROGRAM_DESC='This program is for everyone who exhibits leadership qualities to be recognized.',
PROGRAM_LONG_DESC='<p>The leadership qualities, attributes, or behaviors. In short, a leader is someone who inspires others, is forward looking, is a team builder, empowers others, recognizes people, sets a positive example, builds trust, and/or drives progress.</p><p><strong>Everyone is eligible to both send and receive recognition through this program.</strong></p><p>There are no approvals required for recognitions submitted using this program.</p>'
 WHERE  PROGRAM_NAME='Lead On!'

 --prefix the rest with a predetermined prefix , end the programs to day-1
UPDATE component.PROGRAM set PROGRAM_NAME='zd_' + PROGRAM_NAME,
PROGRAM_DESC='zd_' + PROGRAM_DESC,
THRU_DATE = getdate() -1
 WHERE  PROGRAM_NAME  not in ('Celebrating You','Cheers to Your Years','Everyday Appreciation','Excellence in Action','Wellness Program', 'Lead On!')

 
 