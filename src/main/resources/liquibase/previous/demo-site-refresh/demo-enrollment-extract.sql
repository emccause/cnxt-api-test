--Dump user data into enrollment file format

SELECT 
CASE
    WHEN su.STATUS_TYPE_CODE = 'ACTIVE' THEN 'A'
    WHEN su.STATUS_TYPE_CODE = 'NEW' THEN 'A'
    WHEN su.STATUS_TYPE_CODE = 'INACTIVE' THEN 'I'
    WHEN su.STATUS_TYPE_CODE = 'RESTRICTED' THEN 'R'
    WHEN su.STATUS_TYPE_CODE = 'SUSPENDED' THEN 'S'
    WHEN su.STATUS_TYPE_CODE = 'LOCKED' THEN 'A'
END AS 'ParticipantStatus',
p.CONTROL_NUM AS 'ParticipantId',
ISNULL(p_mgr.CONTROL_NUM, '') AS 'PrimaryReportToID', 
ISNULL(p_sec.CONTROL_NUM, '') AS 'SecondaryReportToID',
ISNULL(p.FIRST_NAME, '') AS 'FirstName',
ISNULL(p.LAST_NAME, '') AS 'LastName',
ISNULL(p.MIDDLE_NAME, '') AS 'MiddleName',
ISNULL(pgm_title.MISC_DATA, '') AS 'Title',
ISNULL(pgm_companyName.MISC_DATA, '') AS 'CompanyName',
ISNULL(su.SYS_USER_NAME, '') AS 'LoginId',
'' AS 'Password',
ISNULL(e.EMAIL_ADDRESS, '') AS 'EmailAddress',
ISNULL(ph.PHONE, '') AS 'PhoneNumber',
ISNULL(ph.PHONE_EXT, '') AS 'PhoneExtension',
ISNULL(a.ADDRESS1, '') AS 'PrimaryAddressLine1',
ISNULL(a.ADDRESS2, '') AS 'AddressLine2',
ISNULL(a.ADDRESS3, '') AS 'AddressLine3',
ISNULL(a.ADDRESS4, '') AS 'AddressLine4',
ISNULL(a.ADDRESS5, '') AS 'AddressLine5',
ISNULL(a.CITY, '') AS 'CITY',
ISNULL(a.STATE, '') AS 'StateProvinceRegion',
ISNULL(a.ZIP, '') AS 'ZipPostalCode',
ISNULL(a.COUNTRY_CODE, '') AS 'CountryCode',
ISNULL(pm_hireDate.MISC_DATA, '') AS 'HireDate',
ISNULL((CONVERT(NVARCHAR(10), pg.THRU_DATE, 120)), '') AS 'TerminationDate',
ISNULL(pm_birthday.MISC_DATA, '') AS 'BirthDate',
ISNULL(pgm_department.MISC_DATA, '') AS 'Department',
ISNULL(pgm_costCenter.MISC_DATA, '') AS 'CostCenter',
ISNULL(pgm_area.MISC_DATA, '') AS 'Area',
ISNULL(pgm_grade.MISC_DATA, '') AS 'Grade',
ISNULL(pgm_function.MISC_DATA, '') AS 'Function',
ISNULL(pgm_custom1.MISC_DATA, '') AS 'Custom1',
ISNULL(pgm_custom2.MISC_DATA, '') AS 'Custom2',
ISNULL(pgm_custom3.MISC_DATA, '') AS 'Custom3',
ISNULL(pgm_custom4.MISC_DATA, '') AS 'Custom4',
ISNULL(pgm_custom5.MISC_DATA, '') AS 'Custom5',
ISNULL(p.LANGUAGE_LOCALE, '') AS 'LanguagePreference'
--'' AS 'LanguagePreference'
FROM component.PAX p
LEFT JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
OUTER APPLY ( SELECT TOP 1 * FROM component.EMAIL e WHERE e.PAX_ID = p.PAX_ID ) e
OUTER APPLY ( SELECT TOP 1 * FROM component.PHONE ph WHERE ph.PAX_ID = p.PAX_ID ) ph
OUTER APPLY ( SELECT TOP 1 * FROM component.ADDRESS a WHERE a.PAX_ID = p.PAX_ID ) a
LEFT JOIN component.PAX_MISC pm_hireDate ON pm_hireDate.PAX_ID = p.PAX_ID AND pm_hireDate.VF_NAME = 'HIRE_DATE'
LEFT JOIN component.PAX_MISC pm_birthday ON pm_birthday.PAX_ID = p.PAX_ID AND pm_birthday.VF_NAME = 'BIRTH_DAY'
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm_title ON pgm_title.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_title.VF_NAME = 'JOB_TITLE'
LEFT JOIN component.PAX_GROUP_MISC pgm_companyName ON pgm_companyName.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_companyName.VF_NAME = 'COMPANY_NAME'
LEFT JOIN component.PAX_GROUP_MISC pgm_department ON pgm_department.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_department.VF_NAME = 'DEPARTMENT'
LEFT JOIN component.PAX_GROUP_MISC pgm_costCenter ON pgm_costCenter.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_costCenter.VF_NAME = 'COST_CENTER'
LEFT JOIN component.PAX_GROUP_MISC pgm_area ON pgm_area.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_area.VF_NAME = 'AREA'
LEFT JOIN component.PAX_GROUP_MISC pgm_grade ON pgm_grade.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_grade.VF_NAME = 'GRADE'
LEFT JOIN component.PAX_GROUP_MISC pgm_function ON pgm_function.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_function.VF_NAME = 'FUNCTION'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom1 ON pgm_custom1.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom1.VF_NAME = 'CUSTOM_1'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom2 ON pgm_custom2.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom2.VF_NAME = 'CUSTOM_2'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom3 ON pgm_custom3.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom3.VF_NAME = 'CUSTOM_3'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom4 ON pgm_custom4.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom4.VF_NAME = 'CUSTOM_4'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom5 ON pgm_custom5.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom5.VF_NAME = 'CUSTOM_5'
--Primary Manager
LEFT JOIN component.RELATIONSHIP r_mgr ON r_mgr.PAX_ID_1 = p.PAX_ID AND r_mgr.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
LEFT JOIN component.PAX p_mgr ON p_mgr.PAX_ID = r_mgr.PAX_ID_2
--Secondary Manager
LEFT JOIN component.RELATIONSHIP r_sec ON r_sec.PAX_ID_1 = p.PAX_ID AND r_sec.RELATIONSHIP_TYPE_CODE = 'SECONDARY_REPORT_TO'
LEFT JOIN component.PAX p_sec ON p_sec.PAX_ID = r_sec.PAX_ID_2

--exclude these users
WHERE su.SYS_USER_NAME NOT IN ('admin','pulsem', 'monitor', 'sys_admin')