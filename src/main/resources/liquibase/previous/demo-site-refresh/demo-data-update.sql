--1 scrub PROD emails
UPDATE component.EMAIL set EMAIL_ADDRESS = 'demo.culturenext@maritz.com'
--2 scrub PROD passwords
UPDATE component.SYS_USER set SYS_USER_PASSWORD = 'Abcd1234' where SYS_USER_NAME not in ('admin','pulsem', 'monitor', 'sys_admin')
--3 remove pax-accounts from prod
DELETE from component.PAX_ACCOUNT
--4 disable sso
UPDATE component.APPLICATION_DATA set VALUE_1='false'
 WHERE  KEY_NAME='/app/project_profile/sso_enabled'
--5 update sending email
UPDATE component.APPLICATION_DATA set VALUE_1='Maritz CultureNext Demo <maritzculturenext@maritz.com>'
 WHERE  KEY_NAME='/app/email_processor/sending_email_address'
--6.1 redirect emails
UPDATE component.APPLICATION_DATA set VALUE_1='amy.laxton@maritz.com;sunitha.kumar@maritz.com;leah.harwell@maritz.com'
 WHERE  KEY_NAME= '/app/email_processor/send_to_all_addresses'
--6.2 disable workday
UPDATE component.APPLICATION_DATA set VALUE_1='FALSE'
 WHERE  KEY_NAME= '/app/project_profile/workday_enrollment_enabled'
UPDATE component.APPLICATION_DATA set VALUE_1='FALSE'
 WHERE  KEY_NAME= '/app/workday/use_production_endpoint'
--6.3
UPDATE component.APPLICATION_DATA set VALUE_1=null, value_2=null
 WHERE  KEY_NAME= '/app/vendorprocessing/subproject/override'

--7 update MARS auth
UPDATE component.APPLICATION_DATA set VALUE_1= 'CultureNext_Stage'
 WHERE  KEY_NAME= 'mars.user'
UPDATE component.APPLICATION_DATA set VALUE_1= '373JFMD4'
 WHERE  KEY_NAME= 'mars.password' 

--8 (8,9,10 are placeholders, will need to update with Demo's subclient, Product, etc. that's still being setup in ABS)
UPDATE component.APPLICATION_DATA set VALUE_1= 'FCP=1494.9,GNC=1494.10'
 WHERE  KEY_NAME= 'mars.subclient'
UPDATE component.APPLICATION_DATA set VALUE_1= 'FCP=FCP,GNC=GNC'
 WHERE  KEY_NAME= 'mars.product'

UPDATE component.APPLICATION_DATA set VALUE_1= 'GNC'
 WHERE  KEY_NAME= '/app/demographicupdates/defaultawardtype/nonUS'
UPDATE component.APPLICATION_DATA set VALUE_1= 'S08201'
 WHERE  KEY_NAME= '/app/demographicupdates/FCP/defaultprojectnumber'
UPDATE component.APPLICATION_DATA set VALUE_1= 'QAFCP'
 WHERE  KEY_NAME= '/app/demographicupdates/FCP/defaultsubprojectnumber'

IF NOT EXISTS (SELECT KEY_NAME FROM component.APPLICATION_DATA WHERE KEY_NAME = '/app/demographicupdates/GNC/defaultprojectnumber')
    BEGIN
        INSERT into component.APPLICATION_DATA (KEY_NAME, VALUE_1) 
        VALUES ('/app/demographicupdates/GNC/defaultprojectnumber', 'S08201')
     END
IF NOT EXISTS (SELECT KEY_NAME FROM component.APPLICATION_DATA WHERE KEY_NAME = '/app/demographicupdates/GNC/defaultsubprojectnumber')
    BEGIN
        INSERT into component.APPLICATION_DATA (KEY_NAME, VALUE_1) 
        VALUES ('/app/demographicupdates/GNC/defaultsubprojectnumber', 'QAGNC')
     END


--9 PAYOUT_TYPE,PAYOUT_VENDOR etc
IF NOT EXISTS (SELECT EARNINGS_TYPE_CODE FROM component.EARNINGS_TYPE WHERE EARNINGS_TYPE_CODE = 'FCP')
    BEGIN
        INSERT into component.EARNINGS_TYPE (EARNINGS_TYPE_CODE, EARNINGS_TYPE_DESC)
        VALUES ('FCP', 'Earnings Desc for FCP') 
        INSERT into component.PAYOUT_TYPE (PAYOUT_TYPE_CODE,PAYOUT_TYPE_DESC, DISPLAY_STATUS_TYPE_CODE)
        VALUES ('FCP', 'Product Code used by Engagedemo', 'ACTIVE')
        INSERT into component.CARD_TYPE (CARD_TYPE_CODE,CARD_TYPE_DESC, DISPLAY_SEQUENCE, DISPLAY_STATUS_TYPE_CODE)
        VALUES ('FCP', 'FCP', 1, 'ACTIVE')
    END
IF NOT EXISTS (SELECT EARNINGS_TYPE_CODE FROM component.EARNINGS_TYPE WHERE EARNINGS_TYPE_CODE = 'GNC')
    BEGIN
        INSERT into component.EARNINGS_TYPE (EARNINGS_TYPE_CODE, EARNINGS_TYPE_DESC)
        VALUES ('GNC', 'Earnings Desc for GNC') 
        INSERT GNC component.PAYOUT_TYPE (PAYOUT_TYPE_CODE,PAYOUT_TYPE_DESC, DISPLAY_STATUS_TYPE_CODE)
        VALUES ('GNC', 'Product Code used by Engagedemo GC', 'ACTIVE')
        INSERT into component.CARD_TYPE (CARD_TYPE_CODE,CARD_TYPE_DESC, DISPLAY_SEQUENCE, DISPLAY_STATUS_TYPE_CODE)
        VALUES ('GNC', 'GNC', 1, 'ACTIVE')
    END
    
--10 PROJECT NUMBER
IF NOT EXISTS (SELECT PROJECT_NUMBER FROM component.PROJECT WHERE PROJECT_NUMBER = 'QA')
    BEGIN
        INSERT into component.PROJECT (PROJECT_NUMBER, PROJECT_DESC) 
        VALUES ('QA', 'Initial Project Number')
     END
IF NOT EXISTS (SELECT PROJECT_NUMBER FROM component.PROJECT WHERE PROJECT_NUMBER = 'S08201')
    BEGIN
        INSERT into component.PROJECT (PROJECT_NUMBER, PROJECT_DESC) 
        VALUES ('S08201', 'Initial Project Number')
     END
--subprojects will get updated with budget-name-update
     