/****** Object:  View [component].[VW_BUDGET_INFO]    Script Date: 30/12/2015 12:34:23 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*==============================================================*/
/* View: VW_BUDGET_INFO                                         */
/*==============================================================*/
ALTER view [component].[VW_BUDGET_INFO] as

SELECT 
    b.ID
,    b.BUDGET_TYPE_CODE
,    b.BUDGET_NAME
,    b.BUDGET_DISPLAY_NAME
,    b.BUDGET_DESC
,    b.PARENT_BUDGET_ID
,    b.INDIVIDUAL_GIVING_LIMIT
,    b.FROM_DATE
,    b.THRU_DATE
,    b.STATUS_TYPE_CODE
,    b.SUB_PROJECT_ID
,    b.BUDGET_CONSTRAINT
,    p.PROJECT_NUMBER
,    sp.SUB_PROJECT_NUMBER
,    b.CREATE_DATE
,    b.ALERT_AMOUNT
,    a_parent.FK2 as 'GROUP_CONFIG_ID'
,    g.GROUP_ID
,    g.GROUP_DESC as 'GROUP_NAME'
,    TOTAL_USED.TOTAL_USED
,    TOTAL_AMOUNT.TOTAL_AMOUNT
,    TRANSACTIONS.IS_USED
FROM component.BUDGET b
JOIN component.SUB_PROJECT sp ON sp.ID = b.SUB_PROJECT_ID
JOIN component.PROJECT p ON p.ID = sp.PROJECT_ID
LEFT JOIN component.ASSOCIATION a_parent ON a_parent.ASSOCIATION_TYPE_ID = 3500 AND a_parent.FK1 = b.ID
LEFT JOIN component.ASSOCIATION a_child ON a_child.ASSOCIATION_TYPE_ID = 3600 AND a_child.FK1 = b.ID
LEFT JOIN component.GROUPS g ON g.GROUP_ID = a_child.FK2

LEFT JOIN ( --TOTAL_USED (SIMPLE AND CHILD)
--Calculate the total budget used (for simple and distributed-children budgets)
SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
(CASE 
    WHEN DEBITS.DEBITS IS NULL THEN 0 
    ELSE DEBITS.DEBITS
END)
-
(CASE
    WHEN CREDITS.CREDITS IS NULL THEN 0 
    ELSE CREDITS.CREDITS  
END)
AS 'TOTAL_USED'
FROM component.BUDGET b
LEFT JOIN (
--DEBITS
SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID 
JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID AND th_debit.TYPE_CODE = 'BUDG' AND th_debit.SUB_TYPE_CODE <> 'FUND' AND th_debit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
GROUP BY b.ID )DEBITS
ON b.ID = DEBITS.ID
LEFT JOIN (
--CREDITS
SELECT b.ID, SUM(disc_credit.AMOUNT) 'CREDITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID AND th_credit.TYPE_CODE = 'BUDG' AND th_credit.SUB_TYPE_CODE <> 'FUND' AND th_credit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
GROUP BY b.ID ) CREDITS 
ON b.ID = CREDITS.ID) TOTAL_USED
ON b.ID = TOTAL_USED.ID

LEFT JOIN ( --TOTAL_AMOUNT (SIMPLE AND CHILD)
--Calculate the total budget amount (for simple and distributed-children budgets)
SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
(CASE
    WHEN CREDITS.CREDITS IS NULL THEN 0 
    ELSE CREDITS.CREDITS  
END)
-
(CASE 
    WHEN DEBITS.DEBITS IS NULL THEN 0 
    ELSE DEBITS.DEBITS
END)
AS 'TOTAL_AMOUNT'
FROM component.BUDGET b
LEFT JOIN (
--DEBITS
SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID AND th_debit.TYPE_CODE = 'BUDG' AND th_debit.SUB_TYPE_CODE = 'FUND' AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID )DEBITS
ON b.ID = DEBITS.ID
LEFT JOIN (
--CREDITS
SELECT b.ID, SUM(disc_credit.AMOUNT) AS 'CREDITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID AND th_credit.TYPE_CODE = 'BUDG' AND th_credit.SUB_TYPE_CODE = 'FUND' AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID ) CREDITS 
ON b.ID = CREDITS.ID ) TOTAL_AMOUNT
ON b.ID = TOTAL_AMOUNT.ID

LEFT JOIN ( --IS_USED (SIMPLE AND CHILD)
--Calculate the 'IS_USED' flag (for simple and distributed-children budgets)
SELECT DISTINCT b.ID,
CASE 
    WHEN th.ID IS NOT NULL OR pb.ID IS NOT NULL
    THEN 'true' 
END AS 'IS_USED'
FROM component.BUDGET b
JOIN component.DISCRETIONARY disc ON disc.BUDGET_ID_CREDIT = b.ID OR disc.BUDGET_ID_DEBIT = b.ID
JOIN component.TRANSACTION_HEADER th on th.ID = disc.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE <> 'FUND' AND th.STATUS_TYPE_CODE = 'APPROVED'
LEFT JOIN component.PROGRAM_BUDGET pb on pb.BUDGET_ID = b.ID) TRANSACTIONS
ON b.ID = TRANSACTIONS.ID
GO


