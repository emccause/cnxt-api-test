/*
Run this script on:

        (local).tenant_google    -  This database will be modified

to synchronize it with:

        (local).CCX300D

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.3.0 from Red Gate Software Ltd at 11/20/2015 11:27:03 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_ROLE_PERMISSION]'
GO
DROP VIEW [component].[VW_ROLE_PERMISSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_RECOGNITION_BUDGET_PROGRAM_ACTIVITY]'
GO
DROP VIEW [component].[VW_RECOGNITION_BUDGET_PROGRAM_ACTIVITY]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_GROUPS]'
GO
DROP VIEW [component].[VW_PROGRAM_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_VISIBILITY]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_VISIBILITY]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_RECOGNITION_STOCK_PHRASE]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_RECOGNITION_STOCK_PHRASE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_PERMISSION]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_PERMISSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_GROUPS]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_APPLICATION_TEXT_PROPERTIES]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_APPLICATION_TEXT_PROPERTIES]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PROGRAM_ACTIVITY_APPLICATION_FIELD]'
GO
DROP VIEW [component].[VW_PROGRAM_ACTIVITY_APPLICATION_FIELD]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PAYOUTS_TO_PROCESS]'
GO
DROP VIEW [component].[VW_PAYOUTS_TO_PROCESS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_PAX_REPORT_TO]'
GO
DROP VIEW [component].[VW_PAX_REPORT_TO]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_USER_GROUPS_SEARCH_DATA_NEW]'
GO
DROP VIEW [component].[VW_USER_GROUPS_SEARCH_DATA_NEW]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_REPLACE_SPECIAL_CHARACTERS]'
GO
DROP FUNCTION [component].[UF_REPLACE_SPECIAL_CHARACTERS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_SPECIAL_CHARACTER_REPLACEMENT]'
GO
DROP VIEW [component].[VW_SPECIAL_CHARACTER_REPLACEMENT]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_GROUP_PAX]'
GO
DROP FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_GROUP_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_GROUP]'
GO
DROP FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_GROUP]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_PAX]'
GO
DROP FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_LIST_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_PAX]'
GO
DROP FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_GROUP]'
GO
DROP FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_ACTIVITY_BY_GROUP]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX]'
GO
DROP FUNCTION [component].[UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UF_LIST_TO_TABLE]'
GO


--Function to split a comma delimited string representing a list of IDs to a table
ALTER function [component].[UF_LIST_TO_TABLE]
 (
    @List NVARCHAR(MAX),
    @Separator CHAR
  )
RETURNS
  @Table TABLE
  (
  items NVARCHAR(MAX)
  )
AS
  BEGIN
    DECLARE @item NVARCHAR(MAX),
            @pos INT

    IF (@List is NULL) OR (len(@List) < 1)
      RETURN

    SET @pos = 1

    WHILE (@pos != 0)
      BEGIN
        SET @pos = CHARINDEX(@Separator, @List)
        IF (@pos != 0)
          SET @item = LEFT(@List, @pos - 1)
        ELSE
          SET @item = @List

        IF (LEN(@item) > 0)
          INSERT INTO @Table(items) values (@item)

        SET @List = RIGHT(@List, LEN(@List) - @pos)

        IF (LEN(@List) = 0) BREAK
      END
  RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP]'
GO


-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID
-- RETURN A DISTINCT LIST OF PROGRAM_ACTIVITY_ID(S) ASSOCIATED WITH THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- ERICKSRT        20150811                CHANGE PROGRAM_ACTIVITY_ID to PROGRAM_ID
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP] (
    @group_id BIGINT)
RETURNS @result_set TABLE (
    [id] BIGINT NULL)
AS
BEGIN

DECLARE @fk TABLE ([id] BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET LIST OF ANCESTOR GROUP(S); INCLUDING THE GIVEN GROUP
INSERT @fk SELECT [id] FROM UF_ASSOCIATION_FETCH_GROUP_BY_GROUP (@group_id, -1)

-- GET LIST OF PROGRAM_ACTIVITY(S) ASSOCIATED WITH GROUP(S)
INSERT    @result_set
SELECT    DISTINCT
    ASSOCIATION.FK1 AS PROGRAM_ID
FROM    
    ASSOCIATION
,    @fk AS GROUPS
WHERE    
    ASSOCIATION.FK2 = GROUPS.[id]
AND    ASSOCIATION.ASSOCIATION_TYPE_ID = 400 -- PROGRAM_ACTIVITY_GROUPS

-- RETURN RESULT SET
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_PAX]'
GO


-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_ID
-- RETURN A DISTINCT LIST OF PROGRAM_ID(S) FOR THE GIVEN PAX_ID
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA        20150818                Updates for UDM 3
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_PAX] (
    @pax_id BIGINT)
RETURNS @result_set TABLE (
    [id] BIGINT NULL)
AS
BEGIN

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET LIST OF ACTIVE PROGRAM_ID(S) DIRECTLY ASSOCIATED WITH THE GIVEN PAX_ID
INSERT    @result_set
SELECT
    PROGRAM_ENROLLMENT.PROGRAM_ID
FROM
    PROGRAM_ENROLLMENT
WHERE    
    PROGRAM_ENROLLMENT.PAX_ID = @pax_id
AND    PROGRAM_ENROLLMENT.ENROLL_STATUS_TYPE_CODE = 'ACTIVE'

-- RETURN RESULT SET
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_PAX]'
GO


-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF PAX_ID(S) AND A CHARACTER DELIMITER
-- RETURN A DISTINCT LIST OF PAX_ID, PROGRAM_ID FOR THE GIVEN PAX_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- ERICKSRT        20150811                CHANGE PROGRAM_ACTIVITY_ID to PROGRAM_ID
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_PAX] (
    @delim_list_pax VARCHAR(4000)
,    @delimiter CHAR(1))
RETURNS @result_set TABLE (
    pax_id BIGINT NULL
,    PROGRAM_id BIGINT NULL)
AS
BEGIN

DECLARE    @pax TABLE (pax_id BIGINT NULL)
DECLARE    @wrk TABLE (pax_id BIGINT NULL, PROGRAM_id BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- PARSE DELIMITED LIST OF PAX_ID(S) TO A LIST OF INTEGER(S)
INSERT @pax SELECT token FROM UF_PARSE_STRING_TO_INTEGER (@delim_list_pax, @delimiter)

-- PROCESS LIST OF PAX
DECLARE c_pax CURSOR
FAST_FORWARD
FOR
SELECT    pax_id
FROM    @pax

DECLARE @pax_id BIGINT
OPEN c_pax

FETCH NEXT FROM c_pax INTO @pax_id
WHILE (@@fetch_status = 0)
BEGIN
    -- GET PROGRAM FROM PROGRAM_ENROLLMENT
     INSERT @wrk SELECT @pax_id, [id] FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_PAX (@pax_id)

    -- INSERT PLACE HOLDER
    IF (@@rowcount = 0)
        INSERT @wrk SELECT @pax_id, NULL

    -- FIND GROUPS FOR PAX
    -- PROCESS LIST OF GROUPS; CURSOR
    DECLARE c_group CURSOR
    FAST_FORWARD
    FOR
    SELECT    [id] AS GROUP_ID
    FROM    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, -1)
    
    DECLARE @group_id BIGINT
    OPEN c_group
    
    FETCH NEXT FROM c_group INTO @group_id
    WHILE (@@fetch_status = 0)
    BEGIN
        -- FIND PROGRAM FROM GROUP AND GROUPS' ANCESTORS
         INSERT @wrk SELECT @pax_id, [id] FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP (@group_id)

        -- FETCH NEXT ROW
        FETCH NEXT FROM c_group INTO @group_id
    END
    
    CLOSE c_group
    DEALLOCATE c_group

    -- FETCH NEXT ROW
    FETCH NEXT FROM c_pax INTO @pax_id
END -- WHILE

CLOSE c_pax
DEALLOCATE c_pax

-- GET DISTINCT LIST
INSERT @result_set SELECT DISTINCT pax_id, PROGRAM_id FROM @wrk

-- RETURN RESULT SET
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP]'
GO


-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S) AND A CHARACTER DELIMITER
-- RETURN A DISTINCT LIST OF PAX_ID, PROGRAM_ID FOR THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- ERICKSRT        20150811                CHANGE PROGRAM_ID to PROGRAM_ID
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP] (
    @delim_list_group VARCHAR(4000)
,    @delimiter CHAR(1))
RETURNS @result_set TABLE (
    pax_id BIGINT NULL
,    program_id BIGINT NULL)
AS
BEGIN

DECLARE    @pax TABLE (pax_id BIGINT NULL)
DECLARE    @wrk TABLE (pax_id BIGINT NULL, program_id BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- PARSE DELIMITED LIST OF GROUP_ID(S) TO A LIST OF INTEGER(S)
-- PROCESS LIST OF GROUP(S)
DECLARE c_group CURSOR
FAST_FORWARD
FOR
SELECT    token AS GROUP_ID
FROM    UF_PARSE_STRING_TO_INTEGER (@delim_list_group, @delimiter)

DECLARE @group_id BIGINT
OPEN c_group

FETCH NEXT FROM c_group INTO @group_id
WHILE (@@fetch_status = 0)
BEGIN
    -- FIND PAX IN GROUP AND GROUP DESCENDANTS
      INSERT @pax SELECT [id] FROM UF_ASSOCIATION_FETCH_PAX_BY_GROUP (@group_id)

    -- PROCESS LIST OF PAX; CURSOR
    DECLARE c_pax CURSOR
    FAST_FORWARD
    FOR
    SELECT    pax_id
    FROM    @pax
    
    DECLARE @pax_id BIGINT
    OPEN c_pax
    
    FETCH NEXT FROM c_pax INTO @pax_id
    WHILE (@@fetch_status = 0)
    BEGIN
        -- GET PROGRAM ACTIVITY(S) ASSOCIATED WITH PAX
        INSERT @wrk SELECT pax_id, PROGRAM_ID FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_PAX (@pax_id, @delimiter)
        
        -- INSERT PLACE HOLDER
        IF (@@rowcount = 0)
            INSERT @wrk SELECT @pax_id, NULL

        -- FETCH NEXT ROW
        FETCH NEXT FROM c_pax INTO @pax_id
    END
    
    CLOSE c_pax
    DEALLOCATE c_pax

    -- FETCH NEXT ROW
    FETCH NEXT FROM c_group INTO @group_id
END -- WHILE

CLOSE c_group
DEALLOCATE c_group

-- GET DISTINCT LIST
INSERT @result_set SELECT DISTINCT pax_id, PROGRAM_ID FROM @wrk

-- RETURN RESULT SET
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX]'
GO


-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S) AND/OR A DELIMITED LIST OF PAX_ID(S)
-- RETURN A DISTINCT LIST OF PAX, PROGRAM (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- ERICKSRT        20150811                CHANGE PROGRAM_ACTIVITY_ID to PROGRAM_ID
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX] (
    @delim_list_group VARCHAR(4000)
,    @delim_list_pax VARCHAR(4000))
RETURNS @result_set TABLE (
    pax_id BIGINT NULL
,    PROGRAM_id BIGINT NULL)
AS
BEGIN

DECLARE    @delimiter CHAR(1)
DECLARE    @wrk TABLE (pax_id BIGINT NULL, PROGRAM_id BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET    @delimiter = ','

-- GET LIST OF PROGRAM ACTIVITIES FOR GIVEN LIST OF GROUPS
INSERT @wrk SELECT pax_id, PROGRAM_id FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP (@delim_list_group, @delimiter)

-- GET LIST OF PROGRAM ACTIVITIES FOR GIVEN LIST OF PAX
INSERT @wrk SELECT pax_id, PROGRAM_id FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_PAX (@delim_list_pax, @delimiter)

-- REMOVE UNNECESSARY PLACE HOLDER RECORDS
DELETE @wrk FROM @wrk AS wrk, (SELECT pax_id FROM @wrk GROUP BY pax_id HAVING COUNT(1) > 1) AS del WHERE wrk.pax_id = del.pax_id AND wrk.PROGRAM_id IS NULL

-- GET DISTINCT LIST
INSERT @result_set SELECT DISTINCT pax_id, PROGRAM_id FROM @wrk

-- RETURN THE RESULT SET
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UF_RECOGNITION_FETCH_LIST_NEXT_APPROVER_NAME_BY_NOMINATION]'
GO


-- ==============================  NAME  ======================================
-- UF_RECOGNITION_FETCH_LIST_NEXT_APPROVER_NAME_BY_NOMINATION
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A NOMINATION_ID
-- RETURN A COMMA DELIMITED LIST OF DISTINCT NEXT APPROVER(S)
-- 
-- NOTE: (GIVE CONSIDERATION TO AUXILIARY_SUBMITTER)
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KRASCCL        20130408                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814                Updates for UDM 3
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_RECOGNITION_FETCH_LIST_NEXT_APPROVER_NAME_BY_NOMINATION] (
    @nomination_id BIGINT)
RETURNS NVARCHAR(1000)
AS
BEGIN

DECLARE    @delimited_values NVARCHAR(1000)

-- INITIALIZE
SET    @delimited_values = NULL

-- GENERATE COMMA-SEPERATED LIST
SELECT    @delimited_values = COALESCE(@delimited_values + ', ', '') + CONVERT(NVARCHAR, NEXT_APPROVER)
FROM    (
    SELECT COALESCE(P.FIRST_NAME,'') + ' ' + COALESCE(P.LAST_NAME, '') AS NEXT_APPROVER
    FROM RECOGNITION R 
    INNER JOIN APPROVAL_PENDING AP ON AP.TARGET_SUB_ID = R.ID 
    INNER JOIN PAX P ON P.PAX_ID =  AP.NEXT_APPROVER_PAX_ID 
    WHERE R.STATUS_TYPE_CODE <> 'INACTIVE'
        AND R.NOMINATION_ID = @nomination_id
) AS LIST
ORDER BY NEXT_APPROVER ASC

-- RETURN VALUE
RETURN @delimited_values

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UF_RECOGNITION_FETCH_LIST_RECEIVER_NAME_BY_NOMINATION]'
GO


-- ==============================  NAME  ======================================
-- UF_RECOGNITION_FETCH_LIST_RECEIVER_NAME_BY_NOMINATION
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A NOMINATION_ID
-- RETURN A COMMA DELIMITED LIST OF DISTINCT RECEIVER NAME(S)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KRASCCL        20130408                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814                Updates for UDM 3
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_RECOGNITION_FETCH_LIST_RECEIVER_NAME_BY_NOMINATION] (
   @nomination_id BIGINT)
RETURNS NVARCHAR(1000)
AS
BEGIN

DECLARE    @delimited_values NVARCHAR(1000)

-- INITIALIZE
SET    @delimited_values = NULL

-- GENERATE COMMA-SEPERATED LIST
SELECT    @delimited_values = COALESCE(@delimited_values + ',', '') + P.FIRST_NAME + ' ' + P.LAST_NAME FROM (
    SELECT    DISTINCT COALESCE(PAX.FIRST_NAME, '') AS FIRST_NAME, COALESCE(PAX.LAST_NAME, '') AS LAST_NAME
    FROM    RECOGNITION
    INNER JOIN PAX ON PAX.PAX_ID = RECOGNITION.RECEIVER_PAX_ID
    WHERE    RECOGNITION.NOMINATION_ID = @nomination_id
    AND RECOGNITION.STATUS_TYPE_CODE NOT IN ('X', 'CANCELLED')  --TODO what does X mean?
) P
ORDER BY
    P.LAST_NAME
,   P.FIRST_NAME

-- RETURN VALUE
RETURN @delimited_values

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION]'
GO


-- ==============================  NAME  ======================================
-- UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A NOMINATION_ID
-- RETURN A COMMA DELIMITED LIST OF DISTINCT RECEIVER_PAX_ID(S)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080618                CREATED
-- PALD00        20081113                ADDED THE RECOGNITION STATUS CHECK
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814                Updates for UDM 3
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION] (
    @nomination_id BIGINT)
RETURNS VARCHAR(1000)
AS
BEGIN

DECLARE    @delimited_values VARCHAR(1000)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZE
SET    @delimited_values = NULL

-- GENERATE COMMA-SEPERATED LIST
SELECT    @delimited_values = COALESCE(@delimited_values + ',', '') + CONVERT(VARCHAR, PAX_ID)
FROM    (
    SELECT    DISTINCT RECOGNITION.RECEIVER_PAX_ID AS PAX_ID
    FROM    RECOGNITION
    WHERE    RECOGNITION.NOMINATION_ID = @nomination_id  AND  RECOGNITION.STATUS_TYPE_CODE NOT IN ('X','CANCELLED')    
) AS PAX
ORDER BY
    PAX_ID

-- RETURN VALUE
RETURN @delimited_values

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_BUDGET_INFO]'
GO

/*==============================================================*/
/* View: VW_BUDGET_INFO                                         */
/*==============================================================*/
ALTER view [component].[VW_BUDGET_INFO] as

SELECT 
    b.ID
,    b.BUDGET_TYPE_CODE
,    b.BUDGET_NAME
,    b.BUDGET_DISPLAY_NAME
,    b.BUDGET_DESC
,    b.PARENT_BUDGET_ID
,    b.INDIVIDUAL_GIVING_LIMIT
,    b.FROM_DATE
,    b.THRU_DATE
,    b.STATUS_TYPE_CODE
,    b.SUB_PROJECT_ID
,    b.BUDGET_CONSTRAINT
,    p.PROJECT_NUMBER
,    sp.SUB_PROJECT_NUMBER
,    b.CREATE_DATE
,    b.ALERT_AMOUNT
,    a_parent.FK2 as 'GROUP_CONFIG_ID'
,    g.GROUP_ID
,    g.GROUP_DESC as 'GROUP_NAME'
,    TOTAL_USED.TOTAL_USED
,    TOTAL_AMOUNT.TOTAL_AMOUNT
,    TRANSACTIONS.IS_USED
FROM component.BUDGET b
JOIN component.SUB_PROJECT sp ON sp.ID = b.SUB_PROJECT_ID
JOIN component.PROJECT p ON p.ID = sp.PROJECT_ID
LEFT JOIN component.ASSOCIATION a_parent ON a_parent.ASSOCIATION_TYPE_ID = 3500 AND a_parent.FK1 = b.ID
LEFT JOIN component.ASSOCIATION a_child ON a_child.ASSOCIATION_TYPE_ID = 3600 AND a_child.FK1 = b.ID
LEFT JOIN component.GROUPS g ON g.GROUP_ID = a_child.FK2

LEFT JOIN ( --TOTAL_USED (SIMPLE AND CHILD)
--Calculate the total budget used (for simple and distributed-children budgets)
SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
(CASE 
    WHEN DEBITS.DEBITS IS NULL THEN 0 
    ELSE DEBITS.DEBITS
END)
-
(CASE
    WHEN CREDITS.CREDITS IS NULL THEN 0 
    ELSE CREDITS.CREDITS  
END)
AS 'TOTAL_USED'
FROM component.BUDGET b
LEFT JOIN (
--DEBITS
SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID 
JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID AND th_debit.TYPE_CODE = 'BUDG' AND th_debit.SUB_TYPE_CODE <> 'FUND' AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID )DEBITS
ON b.ID = DEBITS.ID
LEFT JOIN (
--CREDITS
SELECT b.ID, SUM(disc_credit.AMOUNT) 'CREDITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID AND th_credit.TYPE_CODE = 'BUDG' AND th_credit.SUB_TYPE_CODE <> 'FUND' AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID ) CREDITS 
ON b.ID = CREDITS.ID) TOTAL_USED
ON b.ID = TOTAL_USED.ID

LEFT JOIN ( --TOTAL_AMOUNT (SIMPLE AND CHILD)
--Calculate the total budget amount (for simple and distributed-children budgets)
SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
(CASE
    WHEN CREDITS.CREDITS IS NULL THEN 0 
    ELSE CREDITS.CREDITS  
END)
-
(CASE 
    WHEN DEBITS.DEBITS IS NULL THEN 0 
    ELSE DEBITS.DEBITS
END)
AS 'TOTAL_AMOUNT'
FROM component.BUDGET b
LEFT JOIN (
--DEBITS
SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID AND th_debit.TYPE_CODE = 'BUDG' AND th_debit.SUB_TYPE_CODE = 'FUND' AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID )DEBITS
ON b.ID = DEBITS.ID
LEFT JOIN (
--CREDITS
SELECT b.ID, SUM(disc_credit.AMOUNT) AS 'CREDITS'
FROM component.BUDGET b 
LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID AND th_credit.TYPE_CODE = 'BUDG' AND th_credit.SUB_TYPE_CODE = 'FUND' AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
GROUP BY b.ID ) CREDITS 
ON b.ID = CREDITS.ID ) TOTAL_AMOUNT
ON b.ID = TOTAL_AMOUNT.ID

LEFT JOIN ( --IS_USED (SIMPLE AND CHILD)
--Calculate the 'IS_USED' flag (for simple and distributed-children budgets)
SELECT DISTINCT b.ID,
CASE 
    WHEN th.ID IS NOT NULL OR pb.ID IS NOT NULL
    THEN 'true' 
END AS 'IS_USED'
FROM component.BUDGET b
JOIN component.DISCRETIONARY disc ON disc.BUDGET_ID_CREDIT = b.ID OR disc.BUDGET_ID_DEBIT = b.ID
JOIN component.TRANSACTION_HEADER th on th.ID = disc.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE <> 'FUND' AND th.STATUS_TYPE_CODE = 'APPROVED'
LEFT JOIN component.PROGRAM_BUDGET pb on pb.BUDGET_ID = b.ID) TRANSACTIONS
ON b.ID = TRANSACTIONS.ID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_HIERARCHY_ABOVE]'
GO

/*==============================================================*/
/* View: VW_HIERARCHY_ABOVE                                     */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_HIERARCHY_ABOVE
-- ===========================  DESCRIPTION  ==================================
-- Returns hierarchy info above the starting pax
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150910                Created
-- KNIGHTGA        20150917                Added level
-- BIASONMR        20151016                Added description and pax type code
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_HIERARCHY_ABOVE] as
/*
select * from component.VW_HIERARCHY_ABOVE hb 
 where organization_code = 'MZD' 
   and starting_pax_id = 73
 
select * from component.VW_HIERARCHY_ABOVE hb
where organization_code = 'MZD' 
   and starting_pax_id = 2


select hb.* from component.VW_HIERARCHY_ABOVE hb 
 where hb.organization_code = 'MZD' 
   and hb.starting_control_num = 'SE0104'
*/

with hierarchy(level, owner_group_id, pax_group_id, organization_code, role_code, pax_id, control_num, pax_type_code, description, starting_organization_code, starting_role_code, starting_pax_group_id, starting_pax_id, starting_control_num, starting_pax_type_code) as (
    select 0 as level
          ,pg.owner_group_id
          ,pg.pax_group_id
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_id
          ,p.control_num
          ,p.pax_type_code
          ,case when p.pax_type_code in ('HIERARCHY', 'BUSINESS') then p.company_name
                when p.pax_type_code in ('PERSON') then p.first_name + ' ' + p.last_name
                else isnull(p.company_name, p.first_name + ' ' + p.last_name)
           end
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_group_id
          ,pg.pax_id
          ,p.control_num
          ,p.pax_type_code
      from component.pax_group pg
           inner join component.pax p on p.pax_id = pg.pax_id
     where pg.thru_date is null
     union all
    select h.level + 1
          ,pg.owner_group_id
          ,pg.pax_group_id
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_id
          ,p.control_num 
          ,p.pax_type_code
          ,case when p.pax_type_code in ('HIERARCHY', 'BUSINESS') then p.company_name
                when p.pax_type_code in ('PERSON') then p.first_name + ' ' + p.last_name
                else isnull(p.company_name, p.first_name + ' ' + p.last_name)
           end
          ,h.starting_organization_code
          ,h.starting_role_code
          ,h.starting_pax_group_id
          ,h.starting_pax_id
          ,h.starting_control_num
          ,h.starting_pax_type_code
      from hierarchy h 
           inner join component.pax_group pg on pg.pax_group_id = h.owner_group_id
           inner join component.pax p on p.pax_id = pg.pax_id
     where pg.thru_date is null
)
select h.*
  from hierarchy h
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_HIERARCHY_BELOW]'
GO

/*==============================================================*/
/* View: VW_HIERARCHY_BELOW                                     */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_HIERARCHY_BELOW
-- ===========================  DESCRIPTION  ==================================
-- Returns hierarchy info below the starting pax
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150910                Created
-- KNIGHTGA        20150917                Added level
-- BIASONMR        20151016                Added description and pax type code
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_HIERARCHY_BELOW] as
/*
select * from component.VW_HIERARCHY_BELOW hb 
 where organization_code = 'MZD' 
   and starting_pax_id = 73

 
select * from component.VW_HIERARCHY_BELOW hb
where organization_code = 'MZD' 
   and starting_pax_id = 2

select * from component.VW_HIERARCHY_BELOW hb
where organization_code = 'MZD' 
   and starting_control_num in ('SE0104', 'SE0103')

select distinct pax_id from component.VW_HIERARCHY_BELOW hb
where organization_code = 'MZD' 
   and starting_control_num in ('SE0104', 'SE0103')

select distinct pax_id from component.VW_HIERARCHY_BELOW hb
where starting_organization_code + ':' + starting_control_num in ('MZD:SE0104', 'MZD:SE0103')

select * from component.VW_HIERARCHY_BELOW h 
where starting_control_num = 'COMPANY1'
*/

with hierarchy(level, owner_group_id, pax_group_id, organization_code, role_code, pax_id, control_num, pax_type_code, description, starting_organization_code, starting_role_code, starting_pax_group_id, starting_pax_id, starting_control_num, starting_pax_type_code) as (
    select 0 as level
          ,pg.owner_group_id
          ,pg.pax_group_id
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_id
          ,p.control_num
          ,p.pax_type_code
          ,case when p.pax_type_code in ('HIERARCHY', 'BUSINESS') then p.company_name
                when p.pax_type_code in ('PERSON') then p.first_name + ' ' + p.last_name
                else isnull(p.company_name, p.first_name + ' ' + p.last_name)
           end
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_group_id
          ,pg.pax_id
          ,p.control_num
          ,p.pax_type_code
      from component.pax_group pg
           inner join component.pax p on p.pax_id = pg.pax_id
     where pg.thru_date is null
     union all
    select h.level + 1
          ,pg.owner_group_id
          ,pg.pax_group_id
          ,pg.organization_code
          ,pg.role_code
          ,pg.pax_id
          ,p.control_num 
          ,p.pax_type_code
          ,case when p.pax_type_code in ('HIERARCHY', 'BUSINESS') then p.company_name
                when p.pax_type_code in ('PERSON') then p.first_name + ' ' + p.last_name
                else isnull(p.company_name, p.first_name + ' ' + p.last_name)
           end
          ,h.starting_organization_code
          ,h.starting_role_code
          ,h.starting_pax_group_id
          ,h.starting_pax_id
          ,h.starting_control_num
          ,h.starting_pax_type_code
      from hierarchy h 
           inner join component.pax_group pg on pg.owner_group_id = h.pax_group_id
           inner join component.pax p on p.pax_id = pg.pax_id
     where pg.thru_date is null
)
select h.*
  from hierarchy h
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_HIERARCHY_DEFINITION_ABOVE]'
GO

/*==============================================================*/
/* View: VW_HIERARCHY_DEFINITION_ABOVE                          */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_HIERARCHY_DEFINITION_ABOVE
-- ===========================  DESCRIPTION  ==================================
-- Returns hierarchy info above the starting role
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- BIASONMR        20151016                Created
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_HIERARCHY_DEFINITION_ABOVE] as
/*
  select * from component.VW_HIERARCHY_DEFINITION_ABOVE hda
  where hda.starting_organization_code = 'ACD'
    and hda.starting_role_code = 'DIST'
    
  select * from component.VW_HIERARCHY_DEFINITION_ABOVE hda
  where hda.starting_org_group_id = 19
*/

with hierarchy(level, owner_group_id, org_group_id, organization_code, role_code, description, starting_organization_code, starting_role_code, starting_description, starting_org_group_id) as (
    select 0 as level
          ,og.owner_group_id
          ,og.org_group_id
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,og.org_group_id
      from component.org_group og
     union all
    select h.level + 1
          ,og.owner_group_id
          ,og.org_group_id
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,h.starting_organization_code
          ,h.starting_role_code
          ,h.starting_description
          ,h.starting_org_group_id
      from hierarchy h
           inner join component.org_group og on og.org_group_id = h.owner_group_id
)
select h.*
  from hierarchy h
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_HIERARCHY_DEFINITION_BELOW]'
GO

/*==============================================================*/
/* View: VW_HIERARCHY_DEFINITION_BELOW                          */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_HIERARCHY_DEFINITION_BELOW
-- ===========================  DESCRIPTION  ==================================
-- Returns hierarchy info below the starting role
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- BIASONMR        20151016                Created
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_HIERARCHY_DEFINITION_BELOW] as
/*
  select * from component.VW_HIERARCHY_DEFINITION_BELOW hdb
  where hdb.starting_organization_code = 'ACD'
    and hdb.starting_role_code = 'NATL'
    
  select * from component.VW_HIERARCHY_DEFINITION_BELOW hdb
  where hdb.starting_org_group_id = 16
*/

with hierarchy(level, owner_group_id, org_group_id, organization_code, role_code, description, starting_organization_code, starting_role_code, starting_description, starting_org_group_id) as (
    select 0 as level
          ,og.owner_group_id
          ,og.org_group_id
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,og.org_group_id
      from component.org_group og
     union all
    select h.level + 1
          ,og.owner_group_id
          ,og.org_group_id
          ,og.organization_code
          ,og.role_code
          ,og.org_group_desc
          ,h.starting_organization_code
          ,h.starting_role_code
          ,h.starting_description
          ,h.starting_org_group_id
      from hierarchy h 
           inner join component.org_group og on og.owner_group_id = h.org_group_id
)
select h.*
  from hierarchy h
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_ACL_RESOLVE_ROLES]'
GO

/*==============================================================*/
/* View: VW_ACL_RESOLVE_ROLES                                   */
/*==============================================================*/
create view [component].[VW_ACL_RESOLVE_ROLES] as
select
   r.role_code as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   acl.SUBJECT_ID as PAX_ID
from
   component.ACL
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
where
   acl.SUBJECT_TABLE = 'PAX'

union
select
   r.ROLE_CODE as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   gp.PAX_ID as PAX_ID
from
   component.ACL
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
   inner join component.GROUPS_PAX gp on  gp.GROUP_ID = acl.SUBJECT_ID
where
   acl.SUBJECT_TABLE = 'GROUPS'

union
select
   case when p2.control_num is null then r.role_code else r.role_code + '@' + p2.control_num end as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   pg.PAX_ID as PAX_ID
from
   component.ACL acl
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
   inner join component.PAX_GROUP pg on  pg.PAX_GROUP_ID = acl.SUBJECT_ID and pg.THRU_DATE is null
   left join component.PAX_GROUP pg2 on  pg2.PAX_GROUP_ID = pg.OWNER_GROUP_ID
   left join component.PAX p2 on  p2.PAX_ID = pg2.PAX_ID
where
   acl.SUBJECT_TABLE = 'PAX_GROUP'

union
select
   case when p2.control_num is null then r.role_code else r.role_code + '@' + p2.control_num end as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   pg.PAX_ID as PAX_ID
from
   component.ACL acl
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
   inner join component.ORG_GROUP og on  og.ORG_GROUP_ID = acl.SUBJECT_ID
   inner join component.PAX_GROUP pg on  pg.ORGANIZATION_CODE = og.ORGANIZATION_CODE and pg.ROLE_CODE = og.ROLE_CODE and pg.THRU_DATE is null
   left join component.PAX_GROUP pg2 on  pg2.PAX_GROUP_ID = pg.OWNER_GROUP_ID
   left join component.PAX p2 on  p2.PAX_ID = pg2.PAX_ID
where
   acl.SUBJECT_TABLE = 'ORG_GROUP'

union
select
   r.ROLE_CODE as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   rp.PAX_ID as PAX_ID
from
   component.ACL
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
   inner join component.ROLE_PAX rp on  rp.ROLE_ID = acl.SUBJECT_ID
where
   acl.SUBJECT_TABLE = 'ROLE'

union
select
   r.ROLE_CODE as [ROLE],
   acl.TARGET_TABLE,
   acl.TARGET_ID,
   gp.PAX_ID as PAX_ID
from
   component.ACL
   inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
   inner join component.ROLE_GROUPS rg on  rg.ROLE_ID = acl.SUBJECT_ID
   inner join component.GROUPS_PAX gp on  gp.GROUP_ID = rg.GROUP_ID
where
   acl.SUBJECT_TABLE = 'ROLE'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_TEST_ITEM]'
GO

/*==============================================================*/
/* View: VW_TEST_ITEM                                           */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_ITEM
-- ===========================  DESCRIPTION  ==================================
-- Returns the taken/available tests for the given AUDIENCE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20151014                Created
-- KNIGHTGA        20151029                Updated for latest UDM 4 changes
-- KNIGHTGA        20151109                Updated th status codes to HOLD and APPROVED
--
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_TEST_ITEM] as
/*
select * from component.VW_TEST_ITEM
select * from component.VW_TEST_ITEM where pax_id = 2
*/

select t.ID as TEST_ID
      ,t.TEST_TYPE_CODE
      ,t.NAME
      ,t.DESCRIPTION
      ,t.CAN_RESUME
      ,t.FROM_DATE
      ,t.THRU_DATE
      ,dt1.NUMBER_QUESTIONS
      ,tr.id as TEST_RESULT_ID
      ,th.ACTIVITY_DATE as TAKEN_DATE
      ,tr.COMPLETION_DATE
      ,tr.CORRECT_COUNT
      ,tr.CORRECT_PERCENTAGE
      ,tr.SCORE
      ,th.STATUS_TYPE_CODE
      ,th.PAX_GROUP_ID
      ,acl.PAX_ID
  from component.test t
       inner join (
                    select t.ID as TEST_ID
                          ,count(tq.ID) as NUMBER_QUESTIONS
                      from component.test t
                           inner join component.TEST_SECTION ts
                                   on ts.TEST_ID = t.id
                                  and ts.STATUS_TYPE_CODE = 'ACTIVE'
                           inner join component.TEST_QUESTION tq
                                   on tq.TEST_SECTION_ID = ts.ID
                                  and tq.STATUS_TYPE_CODE = 'ACTIVE'
                     group by t.id
                  ) as dt1
               on dt1.test_id = t.id
       inner join component.vw_acl_resolve_roles acl
               on acl.TARGET_ID = t.id
              and acl.TARGET_TABLE = 'TEST'
              and acl.ROLE = 'AUDIENCE'
       left join (
                    select tr.TEST_ID
                          ,pg.PAX_ID
                          ,max(tr.id) as TEST_RESULT_ID
                          ,count(tr.id) as NUMBER_ATTEMPTS
                      from component.TEST_RESULT tr
                           inner join component.TRANSACTION_HEADER th
                                   on th.id = tr.transaction_header_id
                                  and th.status_type_code in ('HOLD', 'APPROVED')
                           inner join component.pax_group pg on pg.pax_id = th.pax_group_id
                     group by tr.test_id,pg.pax_id
                 ) as dt2
              on dt2.test_id = t.ID
             and dt2.pax_id = acl.PAX_ID
       left join component.TEST_RESULT tr
                 inner join component.TRANSACTION_HEADER th
                         on th.id = tr.transaction_header_id
                        and th.status_type_code in ('HOLD', 'APPROVED')
              on tr.id = dt2.test_result_id
 where t.status_type_code = 'ACTIVE'
   and ( (tr.id is null and getdate() between t.from_date and isnull(t.thru_date, getdate()))
         or
         (tr.id is not null)
       )
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_GROUP_CONFIG_VISIBILITY]'
GO

/*==============================================================*/
/* View: VW_GROUP_CONFIG_VISIBILITY                             */
/*==============================================================*/
ALTER view [component].[VW_GROUP_CONFIG_VISIBILITY] as
select
ID as GROUP_CONFIG_ID,
case when public_vis is null then admin_vis
else public_vis
end as VISIBILITY
from (
select gc.id, public_vis.VISIBILTIY as PUBLIC_VIS, admin_vis.VISIBILTIY as ADMIN_VIS from 

component.group_config gc
left join
(
select gc.id, 'PUBLIC' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_DESC = 'RECOGNITION'
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join component.group_config gc
on gc.ID = acl.target_id
and acl.TARGET_TABLE = 'GROUP_CONFIG'
) as public_vis
on gc.id = public_vis.id

left join
(
select distinct gc.id, 'ADMIN' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_DESC like '%Admin%'
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join component.group_config gc
on gc.ID = acl.target_id
and acl.TARGET_TABLE = 'GROUP_CONFIG'
) as admin_vis
on gc.id = admin_vis.id) as data
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_GROUPS_VISIBILITY]'
GO

/*==============================================================*/
/* View: VW_GROUPS_VISIBILITY                                   */
/*==============================================================*/
ALTER view [component].[VW_GROUPS_VISIBILITY] as
select
GROUP_ID,
case
when group_config_id is not null then GROUP_CONFIG_VIS
when public_vis is null then admin_vis
else public_vis
end as VISIBILITY
from (
select g.group_id, public_vis.VISIBILTIY as PUBLIC_VIS, admin_vis.VISIBILTIY as ADMIN_VIS, g.GROUP_CONFIG_ID, gcv.VISIBILITY as GROUP_CONFIG_VIS from 

component.groups g
left join
(
select g.group_id, 'PUBLIC' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_DESC = 'RECOGNITION'  --Why RECOGNITION?
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join component.groups g
on g.group_id = acl.target_id
and acl.TARGET_TABLE = 'GROUPS'
) as public_vis
on g.group_id = public_vis.group_id

left join
(
select distinct g.group_id, 'ADMIN' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_DESC like '%Admin%'
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join component.groups g
on g.group_id = acl.target_id
and acl.TARGET_TABLE = 'GROUPS'
) as admin_vis
on g.group_id = admin_vis.group_id

left join component.vw_group_config_visibility gcv
on g.group_config_id = gcv.group_config_id

) as data
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PROGRAM_APPLICATION_FIELD]'
GO

/*==============================================================*/
/* View: VW_PROGRAM_APPLICATION_FIELD                           */
/*==============================================================*/
create view [component].[VW_PROGRAM_APPLICATION_FIELD] (PROGRAM_ID, APPLICATION_FIELD_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS APPLICATION_FIELD_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1800
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PROGRAM_APPLICATION_TEXT_PROPERTIES]'
GO

/*==============================================================*/
/* View: VW_PROGRAM_APPLICATION_TEXT_PROPERTIES                 */
/*==============================================================*/
create view [component].[VW_PROGRAM_APPLICATION_TEXT_PROPERTIES] (PROGRAM_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1900
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PROGRAM_RECOGNITION_STOCK_PHRASE]'
GO

/*==============================================================*/
/* View: VW_PROGRAM_RECOGNITION_STOCK_PHRASE                    */
/*==============================================================*/
create view [component].[VW_PROGRAM_RECOGNITION_STOCK_PHRASE] (PROGRAM_ID, RECOGNITION_STOCK_PHRASE_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS RECOGNITION_STOCK_PHRASE_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2000
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_SPIN_PROGRAM_TOKENS]'
GO

/*==============================================================*/
/* View: VW_SPIN_PROGRAM_TOKENS                                 */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_SPIN_PROGRAM_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by program_activity_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111102                Created
-- KNIGHTGA        20150818                Updated for UDM 3
--
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_SPIN_PROGRAM_TOKENS] as
/*
select * from component.VW_SPIN_PROGRAM_TOKENS
select * from component.VW_SPIN_PROGRAM_TOKENS where program_id = 50
*/

select th.program_id as PROGRAM_ID
      ,cast(sum(case when th.transaction_claim_id is null then d.amount else 0 end) as int) as TOKENS_AVAILABLE
      ,cast(sum(case when th.transaction_claim_id is null then 0 else d.amount end) as int) as TOKENS_USED
      ,cast(sum(d.amount) as int) as TOKENS_TOTAL
  from component.transaction_header th
       inner join component.discretionary d on d.transaction_header_id = th.id
 where th.type_code = 'DISC'
   and (
         (th.sub_type_code = 'SPIN_TOKEN' and th.status_type_code = 'APPROVED')
          or
         (th.sub_type_code = 'SPIN_TOKEN_AWARD' and th.status_type_code in ('HOLD', 'APPROVED'))
       )
 group by th.program_id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_TEST_RESULT_ANSWER]'
GO

/*==============================================================*/
/* View: VW_TEST_RESULT_ANSWER                                  */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_ANSWER
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result answers by test question id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150925                Created
-- KNIGHTGA        20151026                Added NEXT_TEST_SECTION_ID
-- KNIGHTGA        20151028                Updated for latest UDM 4 changes
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_TEST_RESULT_ANSWER] as
/*
select * from component.VW_TEST_RESULT_ANSWER
select * from component.VW_TEST_RESULT_ANSWER where test_question_id = 1
*/

select tr.TEST_ID
      ,trq.TEST_RESULT_ID
      ,tq.TEST_SECTION_ID
      ,trq.id as TEST_RESULT_QUESTION_ID
      ,tq.ID as TEST_QUESTION_ID
      ,tra.id as TEST_RESULT_ANSWER_ID
      ,ta.ID as TEST_ANSWER_ID
      ,ta.ANSWER_TEXT
      ,ta.ANSWER_WEIGHT
      ,ta.USER_PROVIDED
      ,tra.USER_RESPONSE
      ,tra.USER_SELECTED
      ,tra.DISPLAY_SEQUENCE
      ,ta.NEXT_TEST_SECTION_ID
  from component.test_result_answer tra
       inner join component.test_answer ta on tra.test_answer_id = ta.id
       inner join component.test_result_question trq on trq.ID = tra.TEST_RESULT_QUESTION_ID
       inner join component.test_question tq on tq.id = trq.test_question_id
       inner join component.test_result tr on tr.ID = trq.TEST_RESULT_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_TEST_RESULT_QUESTION]'
GO

/*==============================================================*/
/* View: VW_TEST_RESULT_QUESTION                                */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_QUESTION
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result questions by test result id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150925                Created
-- KNIGHTGA        20151028                Updated for latest UDM 4 changes
--
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_TEST_RESULT_QUESTION] as
/*
select * from component.VW_TEST_RESULT_QUESTION
select * from component.VW_TEST_RESULT_QUESTION where test_result_id = 1
*/

select tr.TEST_ID
      ,trq.TEST_RESULT_ID
      ,tq.TEST_SECTION_ID
      ,trq.id as TEST_RESULT_QUESTION_ID
      ,tq.ID as TEST_QUESTION_ID
      ,tq.TEST_QUESTION_TYPE_CODE
      ,tq.QUESTION_TEXT
      ,tq.QUESTION_WEIGHT
      ,tq.MAX_ANSWERS
      ,tq.REQ_ANSWERS
      ,tq.REFERENCE_URL
      ,tq.REFERENCE_NAME
      ,tq.IMAGE_URL
      ,isnull(trq.DISPLAY_SEQUENCE,tq.DISPLAY_SEQUENCE) as DISPLAY_SEQUENCE
  from component.test_result_question trq 
       inner join component.test_question tq on tq.id = trq.test_question_id
       inner join component.test_result tr on tr.ID = trq.TEST_RESULT_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_SPIN_PAX_TOKENS]'
GO

/*==============================================================*/
/* View: VW_SPIN_PAX_TOKENS                                     */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_SPIN_PAX_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by pax_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111212                Created
-- KNIGHTGA        20150818                Updated for UDM 3
--
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_SPIN_PAX_TOKENS] as
/*
select * from component.VW_SPIN_PAX_TOKENS
select * from component.VW_SPIN_PAX_TOKENS where pax_id = 1 and program_id = 50
*/

select th.program_id as PROGRAM_ID
      ,pg.pax_id as PAX_ID
      ,cast(sum(case when th.transaction_claim_id is null then d.amount else 0 end) as int) as TOKENS_AVAILABLE
      ,cast(sum(case when th.transaction_claim_id is null then 0 else d.amount end) as int) as TOKENS_USED
      ,cast(sum(d.amount) as int) as TOKENS_TOTAL
  from component.transaction_header th
       inner join component.discretionary d on d.transaction_header_id = th.id
       inner join component.pax_group pg on pg.pax_group_id = th.pax_group_id
 where th.type_code = 'DISC'
   and (
         (th.sub_type_code = 'SPIN_TOKEN' and th.status_type_code = 'APPROVED')
          or
          (th.sub_type_code = 'SPIN_TOKEN_AWARD' and th.status_type_code in ('HOLD', 'APPROVED'))
       )
 group by th.program_id
         ,pg.pax_id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_SPIN_AWARD_POOL]'
GO

/*==============================================================*/
/* View: VW_SPIN_AWARD_POOL                                     */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_SPIN_AWARD_POOL
-- ===========================  DESCRIPTION  ==================================
-- Returns award pool information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111212                Created
-- KNIGHTGA        20150818                Updated for UDM 3
--
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_SPIN_AWARD_POOL] as
/*
select * from component.VW_SPIN_AWARD_POOL
select * from component.VW_SPIN_AWARD_POOL where program_id = 50
*/

select pg.PROGRAM_ID
      ,cast(pga.AWARD_AMOUNT as int) as DENOMINATION
      ,max(pga.QUANTITY) as QUANTITY
      ,sum(case when disc.id is not null and th.transaction_claim_id is null then 1 else 0 end) as AWARDS_AVAILABLE
      ,sum(case when disc.id is not null and th.transaction_claim_id is not null then 1 else 0 end) as AWARDS_USED
      --,count(disc.id) as AWARDS_TOTAL
  from component.program_game pg
       inner join component.program_game_award pga on pga.program_game_id = pg.id
       left join component.transaction_header th 
              on th.program_id = pg.program_id
             and th.type_code = 'DISC'
             and th.sub_type_code in ('SPIN_AWARD', 'SPIN_TOKEN_AWARD')
             and th.status_type_code in ('APPROVED', 'HOLD')
       left join component.discretionary disc 
              on disc.transaction_header_id = th.id
             and disc.amount = pga.AWARD_AMOUNT
 group by pg.program_id
         ,cast(pga.AWARD_AMOUNT as int)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_BUDGET_BALANCE]'
GO

/*==============================================================*/
/* View: VW_BUDGET_BALANCE                                      */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_BUDGET_BALANCE
-- ===========================  DESCRIPTION  ==================================
-- Return the budget balance
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150914                Created
--
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_BUDGET_BALANCE] as
/*
select * from component.VW_BUDGET_BALANCE
select * from component.VW_BUDGET_BALANCE where program_id = 50

select bb.* 
  from component.VW_BUDGET_BALANCE bb
       inner join component.program_budget pb on pb.budget_id = bb.budget_id
 where pb.program_id = 50

*/

SELECT b.ID as BUDGET_ID
      ,isnull(discInfo.FUNDING, 0) as FUNDING
      ,isnull(discInfo.DEBITS, 0) + isnull(trInfo.DEBITS, 0) as DEBITS
      ,isnull(discInfo.CREDITS, 0) + isnull(trInfo.CREDITS, 0) as CREDITS
      ,isnull(discInfo.BALANCE, 0) + isnull(trInfo.BALANCE, 0) as BALANCE
  FROM component.BUDGET b
       left join (
                    select b.ID as budget_id
                          ,sum(case when th.sub_type_code = 'FUND' and disc.BUDGET_ID_DEBIT = b.id then -disc.amount 
                                    when th.sub_type_code = 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount
                                    else 0 
                                end) as FUNDING
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount 
                                    else 0 
                               end ) as CREDITS
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_DEBIT = b.id then disc.amount 
                                    else 0 
                               end ) as DEBITS
                          ,sum(case when disc.BUDGET_ID_DEBIT = b.id then -disc.amount else disc.amount end) as BALANCE
                     FROM component.DISCRETIONARY disc 
                          inner join component.BUDGET b on b.id = isnull(disc.BUDGET_ID_CREDIT, disc.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = disc.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as discInfo
              ON discInfo.budget_id = b.ID
       left join (
                    select b.ID as budget_id
                          ,sum(case when tr.BUDGET_ID_CREDIT = b.id then tr.earnings_amount else 0 end) AS CREDITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then tr.earnings_amount else 0 end) AS DEBITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then -tr.earnings_amount else tr.earnings_amount end) as BALANCE
                     FROM component.transaction_result tr
                          inner join component.BUDGET b on b.id = isnull(tr.BUDGET_ID_CREDIT, tr.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = tr.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as trInfo
              ON trInfo.budget_id = b.ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PROGRAM_VISIBILITY]'
GO
ALTER view [component].[VW_PROGRAM_VISIBILITY] as

select
PROGRAM_ID,
case when public_vis is null then admin_vis
else public_vis
end as VISIBILITY
from (
select PRGM.PROGRAM_ID, public_vis.VISIBILTIY as PUBLIC_VIS, admin_vis.VISIBILTIY as ADMIN_VIS from 

component.PROGRAM PRGM
left join
(
select PRGM.PROGRAM_ID, 'PUBLIC' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.subject_table = 'ROLE'
and r.ROLE_DESC = 'RECOGNITION'
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'PVIS'
inner join component.PROGRAM PRGM
on PRGM.PROGRAM_ID = acl.target_id
and acl.target_table = 'PROGRAM'
) as public_vis
on PRGM.PROGRAM_ID = public_vis.PROGRAM_ID

left join
(
select distinct PRGM.PROGRAM_ID, 'ADMIN' as VISIBILTIY from component.role r
inner join component.acl acl
on acl.subject_id = r.role_id
and acl.subject_table = 'ROLE'
and r.ROLE_DESC like '%Admin%'
inner join component.role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'PVIS'
inner join component.PROGRAM PRGM
on PRGM.PROGRAM_ID = acl.target_id
and acl.target_table = 'PROGRAM'
) as admin_vis
on PRGM.PROGRAM_ID = admin_vis.PROGRAM_ID) as data

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES]'
GO
EXEC sp_refreshview N'[component].[VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_EARNINGS_REPORT_CATEGORY_APPLICATION_TEXT_PROPERTIES]'
GO
EXEC sp_refreshview N'[component].[VW_EARNINGS_REPORT_CATEGORY_APPLICATION_TEXT_PROPERTIES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_GROUPS_GROUPS]'
GO
EXEC sp_refreshview N'[component].[VW_GROUPS_GROUPS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_GROUP_TOTAL_MEMBERSHIP]'
GO
EXEC sp_refreshview N'[component].[VW_GROUP_TOTAL_MEMBERSHIP]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_NOMINATION_RECOGNITION_STOCK_PHRASE]'
GO
EXEC sp_refreshview N'[component].[VW_NOMINATION_RECOGNITION_STOCK_PHRASE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_PAX_GROUP_GROUPS]'
GO
EXEC sp_refreshview N'[component].[VW_PAX_GROUP_GROUPS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_PROXY_PAX_PAX]'
GO
EXEC sp_refreshview N'[component].[VW_PROXY_PAX_PAX]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES]'
GO
EXEC sp_refreshview N'[component].[VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_RECOGNITION_STOCK_PHRASE_APPLICATION_TEXT_PROPERTIES]'
GO
EXEC sp_refreshview N'[component].[VW_RECOGNITION_STOCK_PHRASE_APPLICATION_TEXT_PROPERTIES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [component].[VW_SYS_USER_PERMISSION]'
GO
EXEC sp_refreshview N'[component].[VW_SYS_USER_PERMISSION]'
GO