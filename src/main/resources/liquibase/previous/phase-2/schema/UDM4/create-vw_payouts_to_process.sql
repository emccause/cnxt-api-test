
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*==============================================================*/
/* View: VW_PAYOUTS_TO_PROCESS                                  */
/*==============================================================*/
CREATE view [component].[VW_PAYOUTS_TO_PROCESS] as
            select pay.payout_amount as POINTS, 
            pay.payout_id as INDICATIVE_DATA_1, 
            pay.payout_type_code as PAYOUT_TYPE_CODE, 
            pi.product_code as PRODUCT_CODE, 
            -- and the pax info
            p.pax_id as PAX_ID, 
            p.control_num as PARTICIPANT_ID, 
            substring(RTRIM(LTRIM(ISNULL(p.first_name,'') + ' ' + ISNULL(p.last_name,''))),1,26) as ACCOUNT_NAME, 
            p.last_name as PARTICIPANT_LAST_NAME, 
            p.first_name as PARTICIPANT_FIRST_NAME,
            substring(p.middle_name,1,1) as PARTICIPANT_MIDDLE_INITIAL, 
            upper(substring(p.name_suffix,1,5)) as PARTICIPANT_NAME_SUFFIX, 
            upper(p.name_prefix) as PARTICIPANT_NAME_PREFIX, 
            upper(p.language_code) as LANGUAGE_ID, 
            -- determine which address the user wants.
            -- if the preferred flag is not set, use the business
            -- address if it exists. if not, use the home.
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.address1 
                        else 
                            ha.address1 
                        end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.address1 
                WHEN ba.preferred = 'Y' THEN 
                    ba.address1 
                ELSE 
                    ba.address1 
            END AS STREET_ADDRESS_1, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.address2 
                        else 
                            ha.address2 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.address2 
                WHEN ba.preferred = 'Y' THEN 
                    ba.address2 
                ELSE 
                    ba.address2 
            END AS STREET_ADDRESS_2, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.city 
                        else 
                            ha.city 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.city 
                WHEN ba.preferred = 'Y' THEN 
                    ba.city 
                ELSE 
                    ba.city 
            END AS CITY, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.state 
                        else 
                            ha.state 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.state 
                WHEN ba.preferred = 'Y' THEN 
                    ba.state 
                ELSE 
                    ba.state 
            END AS STATE_PROVINCE, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.zip 
                        else 
                            ha.zip 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.zip 
                WHEN ba.preferred = 'Y' THEN 
                    ba.zip 
                ELSE 
                    ba.zip 
            END AS ZIP, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.country_code 
                        else 
                            ha.country_code 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.country_code 
                WHEN ba.preferred = 'Y' THEN 
                    ba.country_code 
                ELSE 
                    ba.country_code 
            END AS COUNTRY, 
            CASE 
                WHEN ha.preferred <> 'Y' and ba.preferred <> 'Y' then 
                    case 
                        when ba.pax_id is not null then 
                            ba.country_code 
                        else 
                            ha.country_code 
                    end 
                WHEN ha.preferred = 'Y' THEN 
                    ha.country_code 
                WHEN ba.preferred = 'Y' THEN 
                    ba.country_code 
                ELSE 
                    ba.country_code 
            END AS COUNTRY_OF_ORIGIN, 
            CASE 
                WHEN he.preferred <> 'Y' and be.preferred <> 'Y' then 
                    case 
                        when be.email_address is not null then 
                            be.email_address 
                        else 
                            he.email_address 
                    end 
                WHEN he.preferred = 'Y' THEN 
                    he.email_address 
                WHEN be.preferred = 'Y' THEN 
                    be.email_address 
                ELSE 
                    be.email_address 
            END AS EMAIL_ADDRESS, 
            p.ssn as PARTICIPANT_SSN, 
            np.phone as PARTICIPANT_NIGHT_PHONE, 
            fp.phone as PARTICIPANT_FAX_PHONE, 
            dp.phone as PARTICIPANT_DAY_PHONE, 
            '01' as TRANSACTION_CODE, 
            pay.payout_desc as TRANSACTION_DESCRIPTION_OVERRIDE, 
            pa.card_num as CARD_ABS_ACCOUNT_NUMBER, 
            pay.sub_project_number as SUB_PROJECT_NUMBER, 
            pay.project_number as PROJECT_NUMBER
            from payout pay 
            -- get the all award for ABS info
            inner join payout_type pt on pt.payout_type_code=pay.payout_type_code 
            inner join payout_item pi on pi.payout_item_name=pt.payout_type_desc 
            inner join payout_vendor pv on pv.payout_vendor_id=pi.payout_vendor_id 
            inner join card_type ct on pt.payout_type_code=ct.card_type_code 
            -- get the participant info
            LEFT JOIN pax p ON pay.pax_id=p.pax_id 
            -- get the home and business addresses
            LEFT JOIN address ha ON ha.address_type_code = 'HOM' and p.pax_id = ha.pax_id 
            LEFT JOIN address ba ON ba.address_type_code = 'BUS' and p.pax_id = ba.pax_id 
            LEFT JOIN phone np on np.phone_type_code = 'HOM' and p.pax_id = np.pax_id 
            LEFT JOIN phone fp on fp.phone_type_code = 'BFX' and p.pax_id = fp.pax_id 
            LEFT JOIN phone dp on dp.phone_type_code = 'BUS' and p.pax_id = dp.pax_id 
            LEFT JOIN email he on he.email_type_code = 'HOM' and p.pax_id = he.pax_id 
            LEFT JOIN email be on be.email_type_code = 'BUS' and p.pax_id = be.pax_id 
            -- get the account info
            LEFT JOIN pax_account pa on pa.pax_id=p.pax_id and pa.card_type_code= ct.card_type_code 
            -- only the payouts that are ready to process and for ABS vendor
            where pay.status_type_code = 'PROCESSED' and pv.payout_vendor_name='ABS'
GO