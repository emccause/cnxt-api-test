-- ==============================  NAME  ======================================
-- TR_COMMENT_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- kilinskis    20151022                Created
--
-- ===========================  DECLARATIONS  =================================
 
CREATE TRIGGER [component].[TR_COMMENT_DU_1]
ON [component].[COMMENT]
FOR DELETE,UPDATE
AS
BEGIN
 
-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT= 0
       RETURN
 
-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ((SELECT TRIGGER_NESTLEVEL()) > 1 )
       RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)
 
-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================
 
-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving
-------------------------------------------------------------------
IF EXISTS(SELECT TOP 1 * FROM INSERTED)
       SET @operation ='UPDATE'
ELSE
       SET @operation ='DELETED'
 
SET @change_date=GETDATE()
 
--EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT
 
--SET @change_id = ISNULL(@change_id, USER_NAME())
SET @change_id=USER_NAME()
 
SET @change_operation=LEFT(@operation, 1)
 
-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_COMMENT(
       CHANGE_DATE
,      CHANGE_ID
,      CHANGE_OPERATION
,      ID
,      TARGET_TABLE
,      TARGET_ID
,      PAX_ID
,      COMMENT
,      COMMENT_DATE
,      STATUS_TYPE_CODE
,      ADMIN_PAX_ID
,      ADMIN_UPDATE_TIMESTAMP
,      CREATE_DATE
,      CREATE_ID
,      UPDATE_DATE
,      UPDATE_ID
)
SELECT
       @change_date
,      @change_id
,      @change_operation
,      ID
,      TARGET_TABLE
,      TARGET_ID
,      PAX_ID
,      COMMENT
,      COMMENT_DATE
,      STATUS_TYPE_CODE
,      ADMIN_PAX_ID
,      ADMIN_UPDATE_TIMESTAMP
,      CREATE_DATE
,      CREATE_ID
,      UPDATE_DATE
,      UPDATE_ID
FROM
       DELETED
 
SELECT @err = @@ERROR
 
-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
       SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID)+'''. The failure occurred in database '''+DB_NAME()+''', table ''COMMENT''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err)+'  The statement has been terminated.'
       SET @severity = 16 -- Indicate errors that can be corrected by the user
       GOTO RAISE_ERROR
END
 
RETURN
 
-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:
 
RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN
 
END
 
GO