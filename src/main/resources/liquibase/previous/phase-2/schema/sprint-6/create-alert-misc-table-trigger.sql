/****** Object:  Trigger [component].[TR_PROGRAM_ACTIVITY_1]    Script Date: 10/29/2015 1:35:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================  NAME  ======================================
-- TR_ALERT_MISC_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dagarfin        20151029                Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_ALERT_MISC_1]
ON [component].[ALERT_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    ALERT_MISC T1
    ,       INSERTED T2
    WHERE   T1.ALERT_MISC_ID = T2.ALERT_MISC_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    ALERT_MISC T1
    ,       INSERTED T2
    WHERE   T1.ALERT_MISC_ID = T2.ALERT_MISC_ID
END

RETURN

END
