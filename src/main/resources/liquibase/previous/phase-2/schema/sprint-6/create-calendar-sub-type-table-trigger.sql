/****** Object:  Trigger [component].[TR_CALENDAR_SUB_TYPE_1]    Script Date: 11/03/2015 1:35:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================  NAME  ======================================
-- TR_CALENDAR_SUB_TYPE_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dagarfin        20151103                Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_CALENDAR_SUB_TYPE_1]
ON [component].[CALENDAR_SUB_TYPE]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    CALENDAR_SUB_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    CALENDAR_SUB_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
