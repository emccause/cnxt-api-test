/****** Object:  Table [component].[CALENDAR_ENTRY]    Script Date: 10/29/2015 8:02:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [component].[CALENDAR_ENTRY](
    [ID] [bigint] IDENTITY(1,1) NOT NULL,
    [CALENDAR_SUB_TYPE_ID] [bigint] NOT NULL,
    [CALENDAR_EVENT_NAME] [nvarchar](100) NOT NULL,
    [CALENDAR_EVENT_DESC] [nvarchar](255) NULL,
    [FROM_DATE] [datetime2](7) NOT NULL,
    [THRU_DATE] [datetime2](7) NULL,
    [ALL_DAY_EVENT_STATUS_TYPE_CODE] [nvarchar](20) NULL,
    [STATUS_TYPE_CODE] [nvarchar](20) NOT NULL,
    [FK1] [bigint] NULL,
    [LOCATION] [nvarchar](100) NULL,
    [CREATE_DATE] [datetime2](7) NOT NULL,
    [CREATE_ID] [nvarchar](50) NOT NULL,
    [UPDATE_DATE] [datetime2](7) NOT NULL,
    [UPDATE_ID] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CALENDAR_ENTRY] PRIMARY KEY CLUSTERED
(
    [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [component].[CALENDAR_ENTRY] ADD  CONSTRAINT [DF_CALENDAR_ENTRY_CREATE_DATE]  DEFAULT (getdate()) FOR [CREATE_DATE]
GO

ALTER TABLE [component].[CALENDAR_ENTRY] ADD  CONSTRAINT [DF_CALENDAR_ENTRY_CREATE_ID]  DEFAULT (user_name()) FOR [CREATE_ID]
GO

ALTER TABLE [component].[CALENDAR_ENTRY] ADD  CONSTRAINT [DF_CALENDAR_ENTRY_UPDATE_DATE]  DEFAULT (getdate()) FOR [UPDATE_DATE]
GO

ALTER TABLE [component].[CALENDAR_ENTRY] ADD  CONSTRAINT [DF_CALENDAR_ENTRY_UPDATE_ID]  DEFAULT (user_name()) FOR [UPDATE_ID]
GO

ALTER TABLE [component].[CALENDAR_ENTRY]  WITH CHECK ADD  CONSTRAINT [FK_CALENDAR_ENTRY_CALENDAR_SUB_TYPE] FOREIGN KEY([CALENDAR_SUB_TYPE_ID])
REFERENCES [component].[CALENDAR_SUB_TYPE] ([ID])
GO

ALTER TABLE [component].[CALENDAR_ENTRY] CHECK CONSTRAINT [FK_CALENDAR_ENTRY_CALENDAR_SUB_TYPE]
GO

ALTER TABLE [component].[CALENDAR_ENTRY]  WITH CHECK ADD  CONSTRAINT [FK_CALENDAR_ENTRY_STATUS_TYPE] FOREIGN KEY([STATUS_TYPE_CODE])
REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO

ALTER TABLE [component].[CALENDAR_ENTRY] CHECK CONSTRAINT [FK_CALENDAR_ENTRY_STATUS_TYPE]
GO

ALTER TABLE [component].[CALENDAR_ENTRY]  WITH CHECK ADD  CONSTRAINT [FK_CALENDAR_ENTRY_STATUS_TYPE_2] FOREIGN KEY([ALL_DAY_EVENT_STATUS_TYPE_CODE])
REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO

ALTER TABLE [component].[CALENDAR_ENTRY] CHECK CONSTRAINT [FK_CALENDAR_ENTRY_STATUS_TYPE_2]
GO