-- All special characters related to recg_participant_header 

update ph set TRANSLATIONS=N'Vous avez &#233;t&#233; reconnu(e)!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'&#12524;&#12467;&#12464;&#12491;&#12471;&#12519;&#12531;&#12373;&#12428;&#12414;&#12375;&#12383;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'&#24744;&#33719;&#24471;&#20102;&#35090;&#22870;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'&#24744;&#24050;&#32147;&#29554;&#24471;&#29518;&#21237;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to recg_manager_header 
update ph set TRANSLATIONS=N'Sie haben neue Aktivit&#228;ten!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='de_DE'

update ph set TRANSLATIONS=N'Vous avez une nouvelle activit&#233;!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'&#26032;&#12375;&#12356;&#12450;&#12463;&#12486;&#12451;&#12499;&#12486;&#12451;&#12364;&#12354;&#12426;&#12414;&#12377;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'&#24744;&#26377;&#26032;&#27963;&#21160;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'&#24744;&#26377;&#26032;&#27963;&#21205;&#65281;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to view_recognition_button
update ph set TRANSLATIONS=N'&#12499;&#12517;&#12540;&#35469;&#35672;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'&#37492;&#20110;&#35782;&#21035;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'&#37969;&#26044;&#35672;&#21029;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to view_activity_button
update ph set TRANSLATIONS=N'ANSICHT AKTIVIT&#196;T'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='de_DE'

update ph set TRANSLATIONS=N'VUE ACTIVIT&#201;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'&#12499;&#12517;&#12540;&#12539;&#12450;&#12463;&#12486;&#12451;&#12499;&#12486;&#12451;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'&#26597;&#30475;&#27963;&#21160;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'&#26597;&#30475;&#27963;&#21205;'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='zh_TW' 
