-- All special characters related to recg_participant_header 

update ph set TRANSLATIONS=N'Vous avez été reconnu(e)!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'あなたが認識されています！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'您获得了褒奖！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'您已經獲得獎勵！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_participant_header' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to recg_manager_header 
update ph set TRANSLATIONS=N'Sie haben neue Aktivitäten!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='de_DE'

update ph set TRANSLATIONS=N'Vous avez une nouvelle activité!'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'新しいアクティビティがあります！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'您有新活动！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'您有新活動！'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/recg_manager_header' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to view_recognition_button
update ph set TRANSLATIONS=N'ビュー認識'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'鉴于识别'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'鑑於識別'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_recognition_button' and ph.LOCALE_CODE='zh_TW'

-- All special characters related to view_activity_button
update ph set TRANSLATIONS=N'ANSICHT AKTIVITÄT'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='de_DE'

update ph set TRANSLATIONS=N'VUE ACTIVITÉ'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='fr_CA'

update ph set TRANSLATIONS=N'ビュー・アクティビティ'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='ja_JP'

update ph set TRANSLATIONS=N'查看活动'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE 
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='zh_CN'

update ph set TRANSLATIONS=N'查看活動'
from component.PHRASE_TRANSLATION ph
inner join component.TRANSLATABLE_PHRASE tp on tp.id = ph.TRANSLATABLE_PHRASE_ID
inner join component.TRANSLATABLE t on t.ID = tp.TRANSLATABLE_ID and t.TABLE_NAME = 'APPLICATION_DATA' AND t.COLUMN_NAME = 'VALUE_1'
inner join component.APPLICATION_DATA a on a.APPLICATION_DATA_ID = tp.TO_TRANSLATE
and a.KEY_NAME = '/app/email_processor/view_activity_button' and ph.LOCALE_CODE='zh_TW' 
