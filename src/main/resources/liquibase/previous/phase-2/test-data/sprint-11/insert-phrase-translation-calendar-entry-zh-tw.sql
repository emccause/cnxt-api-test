IF EXISTS (SELECT *
    FROM component.LOCALE_INFO LI
    WHERE LI.LOCALE_CODE = 'zh_TW' AND LI.DISPLAY_STATUS_TYPE_CODE = 'ACTIVE')
BEGIN

    DECLARE @translatablePhraseId bigint
    DECLARE @calendarEntryName nvarchar(255)

    DECLARE translatablePhrase CURSOR FOR
    SELECT tp.ID, ce.CALENDAR_ENTRY_NAME
    FROM component.TRANSLATABLE_PHRASE tp
    JOIN component.TRANSLATABLE t
    ON tp.TRANSLATABLE_ID = t.ID
    JOIN component.CALENDAR_ENTRY ce
    ON tp.TO_TRANSLATE = ce.ID
    WHERE t.TABLE_NAME = 'CALENDAR_ENTRY' AND t.COLUMN_NAME = 'CALENDAR_ENTRY_NAME'

    DECLARE @outerLoop int

    OPEN translatablePhrase

    FETCH NEXT FROM translatablePhrase into @translatablePhraseId, @calendarEntryName
    SET @outerLoop = @@FETCH_STATUS
    WHILE @outerLoop = 0
    BEGIN
        IF NOT EXISTS (SELECT * FROM component.PHRASE_TRANSLATION PT
        WHERE PT.TRANSLATABLE_PHRASE_ID = @translatablePhraseId
        AND PT.LOCALE_CODE = 'zh_TW'
        AND PT.TRANSLATIONS = 'zh_TW-' + @calendarEntryName )
        BEGIN
            INSERT INTO component.PHRASE_TRANSLATION (TRANSLATABLE_PHRASE_ID,LOCALE_CODE,TRANSLATIONS,ACTIVE)
                VALUES (
                  @translatablePhraseId,
                  'zh_TW',
                  'zh_TW-' + @calendarEntryName,
                  'Y'
                )
        END
        FETCH NEXT FROM translatablePhrase into @translatablePhraseId, @calendarEntryName
        SET @outerLoop = @@FETCH_STATUS
    END
    CLOSE translatablePhrase
    DEALLOCATE translatablePhrase

END