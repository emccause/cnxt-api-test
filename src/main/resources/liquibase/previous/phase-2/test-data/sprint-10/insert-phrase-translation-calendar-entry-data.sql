DECLARE @translatablePhraseId bigint
DECLARE @calendarEntryName nvarchar(255)
DECLARE @languageCode nvarchar(5)

DECLARE translatablePhrase CURSOR FOR
SELECT tp.ID, ce.CALENDAR_ENTRY_NAME 
FROM component.TRANSLATABLE_PHRASE tp
JOIN component.TRANSLATABLE t
ON tp.TRANSLATABLE_ID = t.ID
JOIN component.CALENDAR_ENTRY ce
ON tp.TO_TRANSLATE = ce.ID
WHERE t.TABLE_NAME = 'CALENDAR_ENTRY' AND t.COLUMN_NAME = 'CALENDAR_ENTRY_NAME'

DECLARE languageCodeCursor CURSOR FOR
SELECT LOCALE_CODE FROM component.LOCALE_INFO
WHERE DISPLAY_STATUS_TYPE_CODE = 'ACTIVE'

DECLARE @outerLoop int
DECLARE @innerLoop int

OPEN translatablePhrase

FETCH NEXT FROM translatablePhrase into @translatablePhraseId, @calendarEntryName
SET @outerLoop = @@FETCH_STATUS
WHILE @outerLoop = 0
BEGIN
    OPEN languageCodeCursor
    FETCH NEXT FROM languageCodeCursor into @languageCode
    SET @innerLoop = @@FETCH_STATUS
    WHILE @innerLoop = 0
    BEGIN
        INSERT INTO component.PHRASE_TRANSLATION (TRANSLATABLE_PHRASE_ID,LOCALE_CODE,TRANSLATIONS,ACTIVE)
        VALUES (
          @translatablePhraseId,
          @languageCode,
          @languageCode + '-' + @calendarEntryName,
          'Y'
        )
        FETCH NEXT FROM languageCodeCursor into @languageCode
        SET @innerLoop = @@FETCH_STATUS
    END
    CLOSE languageCodeCursor
    FETCH NEXT FROM translatablePhrase into @translatablePhraseId, @calendarEntryName
    SET @outerLoop = @@FETCH_STATUS
END
CLOSE translatablePhrase
DEALLOCATE translatablePhrase
DEALLOCATE languageCodeCursor