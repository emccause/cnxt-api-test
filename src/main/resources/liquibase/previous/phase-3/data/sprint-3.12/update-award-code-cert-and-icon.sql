--Update icon-flame.svg
UPDATE component.FILES SET DATA = CONVERT(VARBINARY(MAX),'<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
<g>
    <path id="XMLID_94_" class="brandFill" d="M26.4,51c-7.5,0-13.8-4.6-16.1-11.7c-2.2-6.9,0.2-14.1,6.1-18.3c0.3-0.2,0.6-0.2,0.9-0.1
        c0.3,0.1,0.5,0.4,0.6,0.7c0.4,1.8,1.1,3.4,2.2,4.9c4.7-6.2,4.4-17.1-0.8-24c-0.3-0.4-0.3-0.9,0-1.2c0.3-0.4,0.7-0.5,1.2-0.3
        c7.4,2.9,13.7,8.6,17.7,16c3.6,6.8,5,14.3,3.8,20.7C40.5,46.2,34.8,51,26.4,51z M16.4,23.5c-5.2,4.5-5.6,10.6-4.2,15.1
        C14.2,45,19.8,49,26.4,49c7.3,0,12.3-4.2,13.7-11.6c2.1-11.1-4.4-26.3-17.3-33.1c4.1,7.9,3.5,18.6-1.9,24.5
        c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.7-0.3C18,27.2,17,25.4,16.4,23.5z"/>
    <path id="XMLID_95_" class="brandFill" d="M26.3,44.7c-3,0-5.6-2.2-6.1-5.1c-0.1-0.3,0.1-0.7,0.3-0.9c0.2-0.2,0.6-0.3,0.9-0.2
        c1.2,0.3,3.1-0.8,4.4-2.5c0.5-0.7,1.7-2.6,0.8-4.3c-0.2-0.3-0.2-0.7,0-1c0.2-0.3,0.5-0.5,0.9-0.5c1.2,0,2.2,0.4,3,1.3
        c1.6,1.7,2.2,4.8,2.1,7C32.5,42,29.7,44.7,26.3,44.7z M22.6,40.5c0.7,1.4,2.1,2.3,3.8,2.3c2.3,0,4.2-1.9,4.2-4.2
        c0.1-1.8-0.3-4.3-1.5-5.6c0,0-0.1-0.1-0.1-0.1c0.1,1.4-0.4,2.9-1.5,4.4C26.3,38.7,24.4,40.1,22.6,40.5z"/>
</g>
</svg>')
WHERE FILE_NAME = 'icon-flame.svg'

--Update fancy.svg
--This one was too big to copy/paste - so I converted the binary data into XML and that is what I copied.
--For the update we are converting the XML back into binary before we do the insert.
--How to get the XML from local upload:  select CONVERT(VARCHAR(MAX), data) from component.files where id = 32172
UPDATE component.FILES SET DATA = CONVERT(VARBINARY(MAX),'<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="certificateFancy" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 1000 645" style="enable-background:new 0 0 1000 645;" xml:space="preserve">
<style type="text/css">
    .brandFill{fill:#12D9E0;}
    .whiteFill{fill:#FFFFFF;}
</style>
<g id="XMLID_1_">
    <path id="outerThickLine" class="brandFill" d="M976.4,620.7H21.6v-599h954.8V620.7z M29.6,612.7h938.8v-583H29.6V612.7z"/>
    <rect id="outerLineTop" x="25.6" y="36" class="brandFill" width="946.8" height="2"/>
    <rect id="innerLineTop" x="25.6" y="44" class="brandFill" width="946.4" height="2"/>
    <rect id="innerLineBottom" x="25.6" y="597" class="brandFill" width="946.4" height="2"/>
    <rect id="outerLineBottom" x="25.6" y="604" class="brandFill" width="946.4" height="2"/>
    <rect id="innerLineRight" x="951" y="25.6" class="brandFill" width="2" height="595"/>
    <rect id="outerLineRight" x="960" y="22.6" class="brandFill" width="2" height="597.9"/>
    <rect id="outerLineLeft" x="36" y="21.6" class="brandFill" width="2" height="597.2"/>
    <rect id="innerLineLeft" x="44" y="24" class="brandFill" width="2" height="596.7"/>
    <path id="innerCircle4" class="brandFill" d="M975.5,544.6c0.3,0,0.6,0,0.9,0v-2c-0.3,0-0.6,0-0.9,0c-43.2,0-78.5,34.9-79,78.1h2
        C899,578.6,933.3,544.6,975.5,544.6z"/>
    <path id="outerCircle4" class="brandFill" d="M889.2,614c0-42.5,34.5-77,77-77c3.4,0,6.8,0.2,10.1,0.7v-2
        c-3.3-0.4-6.7-0.7-10.1-0.7c-43.6,0-79,35.4-79,79c0,2.2,0.1,4.4,0.3,6.6h2C889.4,618.5,889.2,616.3,889.2,614z"/>
    <path id="innerCircle3" class="brandFill" d="M25.6,542.9c-1.3,0-2.7,0-4,0.1v2c1.3-0.1,2.7-0.1,4-0.1c42.1,0,76.3,33.9,77,75.8h2
        C103.9,577.7,68.7,542.9,25.6,542.9z"/>
    <path id="outerCircle3" class="brandFill" d="M112.4,612.6c0-43.6-35.4-79-79-79c-4,0-8,0.3-11.8,0.9v2c3.9-0.6,7.8-0.9,11.8-0.9
        c42.5,0,77,34.5,77,77c0,2.7-0.1,5.4-0.4,8.1h2C112.3,618,112.4,615.3,112.4,612.6z"/>
    <path id="innerCircle2" class="brandFill" d="M974.8,99.6c-42.5,0-77-34.5-77-77c0-0.3,0-0.7,0-1h-2c0,0.3,0,0.7,0,1
        c0,43.6,35.4,79,79,79c0.5,0,1,0,1.6,0v-2C975.9,99.6,975.3,99.6,974.8,99.6z"/>
    <path id="outerCircle2" class="brandFill" d="M966.6,108.5c-42.5,0-77-34.5-77-77c0-3.3,0.2-6.6,0.7-9.9h-2
        c-0.4,3.2-0.6,6.5-0.6,9.9c0,43.6,35.4,79,79,79c3.3,0,6.6-0.2,9.8-0.6v-2C973.2,108.3,969.9,108.5,966.6,108.5z"/>
    <path id="innerCircle1" class="brandFill" d="M103,21.6h-2c0,0.6,0,1.2,0,1.9c0,42.5-34.5,77-77,77c-0.8,0-1.6,0-2.4-0.1v0v2
        c0.8,0,1.6,0,2.4,0c43.6,0,79-35.4,79-79C103,22.9,103,22.3,103,21.6z"/>
    <path id="outerCircle1" class="brandFill" d="M111.4,21.6h-2c0.4,3.2,0.7,6.5,0.7,9.9c0,42.5-34.5,77-77,77
        c-3.9,0-7.7-0.3-11.4-0.9v2c3.7,0.5,7.5,0.8,11.4,0.8c43.6,0,79-35.4,79-79C112,28.2,111.8,24.9,111.4,21.6z"/>
    <g id="dashedLine">
        <g id="XMLID_315_">
            <rect id="partialDash2_7_" x="760" y="591.5" class="brandFill" width="2" height="5"/>
            <path id="fullDashedLine_7_" class="brandFill" d="M762,591.5l-2-0.1l0,0L762,591.5L762,591.5z M762,585.5l-2,0l0-10.2l2,0V585.5
                z M762,569.3l-2,0l0-10.2l2,0V569.3z M762,553.1l-2-0.1l0-10.2l2,0.1V553.1z M762,536.9l-2,0l0-10.2l2,0V536.9z M762,520.8l-2,0
                l0-10.2l2,0V520.8z M762,504.6l-2,0l0-10.2l2,0.1V504.6z M762,488.4l-2,0l0-10.2l2,0V488.4z M762,472.2l-2,0l0-10.2l2,0V472.2z
                 M762,456l-2,0l0-10.2l2,0.1V456z M762,439.9l-2,0l0-10.2l2,0V439.9z M762,423.7l-2,0l0-10.2l2,0V423.7z M762,407.5l-2,0l0-10.2
                l2,0V407.5z M762,391.3l-2,0l0-10.2l2,0V391.3z M762,375.1l-2,0l0-10.2l2,0V375.1z M762,359l-2,0l0-10.2l2,0V359z M762,342.8
                l-2-0.1l0-10.2l2,0V342.8z M762,326.6l-2,0l0-10.2l2,0V326.6z M762,310.4l-2,0l0-10.2l2,0.1V310.4z M762,294.2l-2-0.1l0-10.2l2,0
                V294.2z M762,278.1l-2,0l0-10.2l2,0V278.1z M762,261.9l-2,0l0-10.2l2,0.1V261.9z M762,245.7l-2-0.1l0-10.2l2,0.1V245.7z
                 M762,229.5l-2,0l0-10.2l2,0V229.5z M762,213.3l-2,0l0-10.2l2,0V213.3z M762,197.2l-2-0.1l0-10.2l2,0.1V197.2z M762,181l-2,0
                l0-10.2l2,0V181z M762,164.8l-2-0.1l0-10.2l2,0V164.8z M762,148.6l-2-0.1l0-10.2l2,0.1V148.6z M762,132.4l-2,0l0-10.2l2,0V132.4z
                 M762,116.3l-2-0.1l0-10.2l2,0V116.3z M762,100.1l-2,0l0-10.2l2,0.1V100.1z M762,83.9l-2,0l0-10.2l2,0V83.9z M762,67.7l-2-0.1
                l0-10.2l2,0V67.7z"/>
            <rect id="partialDash1_7_" x="760" y="46.5" class="brandFill" width="2" height="5"/>
        </g>
        <g id="XMLID_280_">
            <rect id="partialDash2_6_" x="760" y="591.5" class="brandFill" width="2" height="5"/>
            <path id="fullDashedLine_6_" class="brandFill" d="M762,591.5l-2-0.1l0,0L762,591.5L762,591.5z M762,585.5l-2,0l0-10.2l2,0V585.5
                z M762,569.3l-2,0l0-10.2l2,0V569.3z M762,553.1l-2-0.1l0-10.2l2,0.1V553.1z M762,536.9l-2,0l0-10.2l2,0V536.9z M762,520.8l-2,0
                l0-10.2l2,0V520.8z M762,504.6l-2,0l0-10.2l2,0.1V504.6z M762,488.4l-2,0l0-10.2l2,0V488.4z M762,472.2l-2,0l0-10.2l2,0V472.2z
                 M762,456l-2,0l0-10.2l2,0.1V456z M762,439.9l-2,0l0-10.2l2,0V439.9z M762,423.7l-2,0l0-10.2l2,0V423.7z M762,407.5l-2,0l0-10.2
                l2,0V407.5z M762,391.3l-2,0l0-10.2l2,0V391.3z M762,375.1l-2,0l0-10.2l2,0V375.1z M762,359l-2,0l0-10.2l2,0V359z M762,342.8
                l-2-0.1l0-10.2l2,0V342.8z M762,326.6l-2,0l0-10.2l2,0V326.6z M762,310.4l-2,0l0-10.2l2,0.1V310.4z M762,294.2l-2-0.1l0-10.2l2,0
                V294.2z M762,278.1l-2,0l0-10.2l2,0V278.1z M762,261.9l-2,0l0-10.2l2,0.1V261.9z M762,245.7l-2-0.1l0-10.2l2,0.1V245.7z
                 M762,229.5l-2,0l0-10.2l2,0V229.5z M762,213.3l-2,0l0-10.2l2,0V213.3z M762,197.2l-2-0.1l0-10.2l2,0.1V197.2z M762,181l-2,0
                l0-10.2l2,0V181z M762,164.8l-2-0.1l0-10.2l2,0V164.8z M762,148.6l-2-0.1l0-10.2l2,0.1V148.6z M762,132.4l-2,0l0-10.2l2,0V132.4z
                 M762,116.3l-2-0.1l0-10.2l2,0V116.3z M762,100.1l-2,0l0-10.2l2,0.1V100.1z M762,83.9l-2,0l0-10.2l2,0V83.9z M762,67.7l-2-0.1
                l0-10.2l2,0V67.7z"/>
            <rect id="partialDash1_6_" x="760" y="46.5" class="brandFill" width="2" height="5"/>
        </g>
    </g>
    <g id="bigCircle4">
        <g id="XMLID_65_">
            <path id="XMLID_67_" class="brandFill" d="M976.4,557.4c-34.3,1.6-61.9,29-63.9,63.2h63.9V557.4z"/>
            <path id="XMLID_66_" class="brandFill" d="M976.4,561.4v-8c-36.5,1.7-65.9,30.8-67.9,67.2h8C918.5,588.7,944.3,563.1,976.4,561.4
                z"/>
        </g>
        <g id="XMLID_53_">
            <path id="XMLID_63_" class="whiteFill" d="M976.4,557.4c-34.3,1.6-61.9,29-63.9,63.2h63.9V557.4z"/>
            <path id="XMLID_62_" class="brandFill" d="M976.4,561.4v-8c-36.5,1.7-65.9,30.8-67.9,67.2h8C918.5,588.7,944.3,563.1,976.4,561.4
                z"/>
        </g>
    </g>
    <g id="bigCircle3">
        <g id="XMLID_68_">
            <path id="XMLID_70_" class="brandFill" d="M21.6,557.6v63H88C85.8,585.7,57,558,21.6,557.6z"/>
            <path id="XMLID_69_" class="brandFill" d="M21.6,553.6v8c33.1,0.4,60.2,26.3,62.4,59h8C89.8,583.6,59.1,554,21.6,553.6z"/>
        </g>
        <g id="XMLID_35_">
            <path id="XMLID_61_" class="whiteFill" d="M21.6,557.6v63H88C85.8,585.7,57,558,21.6,557.6z"/>
            <path id="XMLID_36_" class="brandFill" d="M21.6,553.6v8c33.1,0.4,60.2,26.3,62.4,59h8C89.8,583.6,59.1,554,21.6,553.6z"/>
        </g>
    </g>
    <g id="bigCircle2">
        <g id="XMLID_41_">
            <path id="XMLID_51_" class="brandFill" d="M912.4,21.6c0.7,35.5,28.8,64.3,64,66v-66H912.4z"/>
            <path id="XMLID_42_" class="brandFill" d="M916.4,21.6h-8c0.7,37.6,30.6,68.3,68,70v-8C943.4,81.9,917,54.9,916.4,21.6z"/>
        </g>
        <g id="XMLID_37_">
            <path id="XMLID_39_" class="whiteFill" d="M912.4,21.6c0.7,35.5,28.8,64.3,64,66v-66H912.4z"/>
            <path id="XMLID_38_" class="brandFill" d="M916.4,21.6h-8c0.7,37.6,30.6,68.3,68,70v-8C943.4,81.9,917,54.9,916.4,21.6z"/>
        </g>
    </g>
    <g id="bigCircle1">
        <g id="XMLID_58_">
            <path id="XMLID_60_" class="brandFill" d="M21.6,87C21.6,87,21.6,87,21.6,87C58.2,87,87.9,57.9,89,21.6H21.6"/>
            <path id="XMLID_59_" class="brandFill" d="M85,21.6c-1,34-29,61.4-63.3,61.4c0,0-0.1,0-0.1,0v8c0,0,0.1,0,0.1,0
                C60.3,91,91.9,60.1,93,21.6H85z"/>
        </g>
        <g id="XMLID_52_">
            <path id="XMLID_57_" class="whiteFill" d="M21.6,87C21.6,87,21.6,87,21.6,87C58.2,87,87.9,57.9,89,21.6H21.6"/>
            <path id="XMLID_56_" class="brandFill" d="M85,21.6c-1,34-29,61.4-63.3,61.4c0,0-0.1,0-0.1,0v8c0,0,0.1,0,0.1,0
                C60.3,91,91.9,60.1,93,21.6H85z"/>
        </g>
    </g>
    <polygon id="burst4" class="brandFill" points="976.4,578.9 973.8,591.4 968.6,578.5 968.2,592.3 960.8,580.6 962.9,594.3 
        953.6,584.1 958.1,597.2 947,588.8 953.8,600.9 941.5,594.6 950.3,605.3 937,601.3 947.6,610.2 933.8,608.7 945.9,615.6 932,616.5 
        943.6,620.7 976.4,620.7     "/>
    <polygon id="burst3" class="brandFill" points="66.4,616.8 52.5,615.8 64.6,608.9 50.8,610.4 61.4,601.5 48.1,605.4 57,594.8 
        44.6,601 51.5,588.9 40.4,597.3 45,584.2 35.5,594.4 37.7,580.7 30.3,592.4 30,578.5 24.7,591.4 21.9,577.8 21.6,579.5 21.6,620.7 
        55.2,620.7     "/>
    <polygon id="burst2" class="brandFill" points="941.7,21.6 931.9,24.1 945.7,26.3 933,32.1 946.9,31.8 935.5,39.7 949.1,37 
        939.3,46.8 952.2,41.7 944.3,53.1 956.1,45.8 950.3,58.4 960.6,49.1 957.2,62.6 965.7,51.6 964.8,65.4 971.1,53.1 972.7,66.9 
        976.4,54.8 976.4,21.6     "/>
    <polygon id="burst1" class="brandFill" points="52.7,25.4 66.5,23.2 60.3,21.6 21.6,21.6 21.6,52.6 21.6,52.6 25.6,65.9 27.2,52.1 
        33.5,64.5 32.6,50.6 41.1,61.6 37.7,48.2 48,57.5 42.3,44.9 54,52.2 46.2,40.8 59.1,45.9 49.3,36.1 62.9,38.8 51.5,30.9 65.4,31.2 
            "/>
</g>
</svg>')
WHERE FILE_NAME = 'fancy.svg'