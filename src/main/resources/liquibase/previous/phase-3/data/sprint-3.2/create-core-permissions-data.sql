
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

insert into component.PAX_TYPE
( PAX_TYPE_CODE, PAX_TYPE_DESC, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) VALUES
    ( N'HIERARCHY', N'Hierarchy', getdate(), N'dbo', getdate(), N'dbo'),
    ( N'BUSINESS', N'Business', getdate(), N'dbo', getdate(), N'dbo'),
    ( N'PERSON', N'Person', getdate(), N'dbo', getdate(), N'dbo');
GO

--These need to be preloads
insert into component.acl_type (acl_type)
    select 'PAX_TYPE'
    where not exists (select 1 from component.acl_type where acl_type = 'PAX_TYPE');
GO

--add roles for the pax types
insert into component.role (role_code, role_name, role_desc)
    select pt.pax_type_code, left(pt.pax_type_desc,100), pt.pax_type_desc
    from component.pax_type pt
    where not exists (select 1 from component.role where role_code = pt.pax_type_code);
GO

insert into component.acl (role_id,SUBJECT_TABLE,SUBJECT_ID)
    select r.role_id,'PAX_TYPE',pt.id
    from component.role r
    ,component.pax_type pt
    where r.role_code = pt.pax_type_code
    and not exists (select 1 from component.ACL acl where acl.ROLE_ID = r.role_ID and acl.SUBJECT_TABLE = 'PAX_TYPE' and acl.SUBJECT_ID = pt.id)
    order by pt.id;
GO

update component.pax set pax_type_code =  'PERSON';
GO

insert into component.acl (role_id, SUBJECT_TABLE, SUBJECT_ID)
    select role_id, 'PAX', pax_id from component.role_pax;
GO

insert into component.acl (role_id, SUBJECT_TABLE, SUBJECT_ID)
    select role_id, 'GROUPS', group_id from component.role_groups;
GO
