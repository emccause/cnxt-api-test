SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*===============================================================================================*/
/* View: VW_GROUPS_VISIBILITY                                                                    */
/*===============================================================================================*/
-- ============================  REVISIONS  =====================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160322                FOR PUBLIC VISIBILITY USE PARTICIPANT(RECG) INSTEAD OF RECOGNITION 
-- GARCIAF2        20160328                REMOVE "component." SCHEMA 
-- GARCIAF2        20160328                FOR ADMIN VISIBILITY USE: ROLE_CODE in ('ADM', 'CADM')INSTEAD OF ROLE_DESC

-- ===========================  DECLARATIONS  ===================================================

ALTER view [component].[VW_GROUPS_VISIBILITY] as
select
GROUP_ID,
case
when group_config_id is not null then GROUP_CONFIG_VIS
when public_vis is null then admin_vis
else public_vis
end as VISIBILITY
from (
select g.group_id, public_vis.VISIBILTIY as PUBLIC_VIS, admin_vis.VISIBILTIY as ADMIN_VIS, g.GROUP_CONFIG_ID, gcv.VISIBILITY as GROUP_CONFIG_VIS from 

groups g
left join
(
select g.group_id, 'PUBLIC' as VISIBILTIY from role r
inner join acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_CODE = 'RECG'  --Participant
inner join role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join groups g
on g.group_id = acl.target_id
and acl.TARGET_TABLE = 'GROUPS'
) as public_vis
on g.group_id = public_vis.group_id

left join
(
select distinct g.group_id, 'ADMIN' as VISIBILTIY from role r
inner join acl acl
on acl.subject_id = r.role_id
and acl.SUBJECT_TABLE = 'ROLE'
and r.ROLE_CODE in ('ADM', 'CADM')
inner join role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'GVIS'
inner join groups g
on g.group_id = acl.target_id
and acl.TARGET_TABLE = 'GROUPS'
) as admin_vis
on g.group_id = admin_vis.group_id

left join vw_group_config_visibility gcv
on g.group_config_id = gcv.group_config_id

) as data

GO