/****** Object:  View [component].[VW_PAX_ROLE]    Script Date: 3/2/2016 7:47:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
select * from component.pax

select * from component.VW_PAX_ROLE where pax_id = 2 and target_id is null
*/

/*==============================================================*/
/* View: VW_PAX_ROLE                                            */
/*==============================================================*/
CREATE view [component].[VW_PAX_ROLE] as

WITH roles([LEVEL],ROLE_ID,[ROLE],QUALIFIER,TARGET_TABLE,TARGET_ID,SUBJECT_TABLE,SUBJECT_ID,PAX_ID) as (
    --========================================================================================
    --ANCHOR QUERY
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,null as QUALIFIER
        ,acl.TARGET_TABLE
        ,acl.TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,acl.SUBJECT_ID as PAX_ID
    from
        component.ACL
        inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
    where
        acl.SUBJECT_TABLE = 'PAX'

    union
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,null as QUALIFIER
        ,acl.TARGET_TABLE
        ,acl.TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,p.pax_id as PAX_ID
     from
        component.ACL
        inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
        inner join component.PAX_TYPE pt on pt.ID = acl.SUBJECT_ID
        inner join component.pax p on p.pax_type_code = pt.pax_type_code
    where
        acl.SUBJECT_TABLE = 'PAX_TYPE'

    union
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,null as QUALIFIER
        ,acl.TARGET_TABLE
        ,acl.TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,gp.PAX_ID as PAX_ID
    from
        component.ACL
        inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
        inner join component.GROUPS_PAX gp on  gp.GROUP_ID = acl.SUBJECT_ID
    where
        acl.SUBJECT_TABLE = 'GROUPS'

    union
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,'@' + cast(pg.pax_group_id as nvarchar(20)) as QUALIFIER
        ,acl.TARGET_TABLE
        ,acl.TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,pg.PAX_ID
    from
        component.ACL acl
        inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
        inner join component.PAX_GROUP pg on  pg.PAX_GROUP_ID = acl.SUBJECT_ID and pg.THRU_DATE is null
    where
        acl.SUBJECT_TABLE = 'PAX_GROUP'

    union
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(pg.organization_code + ':' + pg.role_code as nvarchar(255)) as ROLE_CODE
        ,'@' + cast(pg.pax_group_id as nvarchar(20)) as QUALIFIER
        ,null as TARGET_TABLE
        ,null as TARGET_ID
        ,null as SUBJECT_TABLE
        ,null as SUBJECT_ID
        ,pg.PAX_ID
    from
        component.pax_group pg
        inner join component.ROLE r on r.ROLE_CODE = pg.ROLE_CODE
    where pg.thru_date is null

    union
    select
        0 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,'@' + cast(pg.pax_group_id as nvarchar(20)) as QUALIFIER
        ,acl.TARGET_TABLE
        ,acl.TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,pg.PAX_ID
    from
        component.ACL acl
        inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID
        inner join component.ORG_GROUP og on  og.ORG_GROUP_ID = acl.SUBJECT_ID
        inner join component.PAX_GROUP pg on  pg.ORGANIZATION_CODE = og.ORGANIZATION_CODE and pg.ROLE_CODE = og.ROLE_CODE and pg.THRU_DATE is null
    where
        acl.SUBJECT_TABLE = 'ORG_GROUP'

    --========================================================================================
    --RECURSIVE QUERY
    union all

    --get all the roles assigned to the anchor roles
    select
        resolved.LEVEL + 1 as LEVEL
        ,r.ROLE_ID
        ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
        ,resolved.QUALIFIER
        ,coalesce(resolved.TARGET_TABLE, acl.TARGET_TABLE) as TARGET_TABLE
        ,coalesce(resolved.TARGET_ID, acl.TARGET_ID) as TARGET_ID
        ,acl.SUBJECT_TABLE
        ,acl.SUBJECT_ID
        ,resolved.PAX_ID
    from roles resolved
        inner join component.acl acl
                on acl.subject_id = resolved.role_id
               and acl.subject_table = 'ROLE'
        inner join component.[role] r on r.role_id = acl.role_id
    /*
    union all
    --get all the roles assigned to the role_types of the anchor roles
    select r.ROLE_ID
          ,cast(r.ROLE_CODE as nvarchar(255))
          ,cast(case when resolved.location is not null then r.ROLE_CODE + '@' + resolved.LOCATION
                     else r.ROLE_CODE
                end as nvarchar(255))
          ,resolved.TARGET_TABLE
          ,resolved.TARGET_ID
          ,resolved.PAX_ID
          ,resolved.LOCATION
      from roles resolved
           inner join component.role_type rt on rt.ROLE_TYPE_CODE = resolved.ROLE_CODE
           inner join component.acl acl
                   on acl.subject_id = rt.ROLE_TYPE_ID
                  and acl.subject_table = 'ROLE_TYPE'
           inner join component.[role] r on r.role_id = acl.role_id
    */
)
select distinct * from roles


GO


