CREATE NONCLUSTERED INDEX [IX_GROUPS_PAX_ID_GROUP_DESC] 
    ON [component].[GROUPS] (
        [PAX_ID],
        [GROUP_DESC]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_GROUPS_GROUP_DESC] 
    ON [component].[GROUPS] (
        [GROUP_DESC]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_GROUPS_PAX_ID_GROUP_CONFIG_ID_GROUP_DESC] 
    ON [component].[GROUPS] (
        [PAX_ID],
        [GROUP_CONFIG_ID],
        [GROUP_DESC]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_PAX_GROUP_ORGANIZATION_CODE_ROLE_CODE_THRU_DATE] 
    ON [component].[PAX_GROUP] (
        [ORGANIZATION_CODE],
        [ROLE_CODE],
        [THRU_DATE]
    )
    INCLUDE (
        [PAX_GROUP_ID],
        [PAX_ID]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_PAX_GROUP_ROLE_CODE_THRU_DATE] 
    ON [component].[PAX_GROUP] (
        [ROLE_CODE],
        [THRU_DATE]
    )
    INCLUDE (
        [PAX_GROUP_ID],
        [PAX_ID],
        [ORGANIZATION_CODE]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_PAX_GROUP_THRU_DATE] 
    ON [component].[PAX_GROUP] (
        [THRU_DATE]
    )
    INCLUDE (
        [PAX_GROUP_ID],
        [PAX_ID],
        [ORGANIZATION_CODE],
        [ROLE_CODE]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_PAX_MISC_VF_NAME_MISC_DATA] 
    ON [component].[PAX_MISC] (
        [VF_NAME],
        [MISC_DATA]
    )
    INCLUDE (
        [PAX_ID]
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO

CREATE NONCLUSTERED INDEX [IX_PAX_PAX_TYPE_CODE] 
    ON [component].[PAX] (
        [PAX_TYPE_CODE] ASC
    )
    WITH (
        PAD_INDEX = OFF,
        STATISTICS_NORECOMPUTE = OFF,
        SORT_IN_TEMPDB = OFF,
        DROP_EXISTING = OFF,
        ONLINE = OFF,
        ALLOW_ROW_LOCKS = ON,
        ALLOW_PAGE_LOCKS = ON
    )
GO