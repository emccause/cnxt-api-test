/****** Object:  View [component].[VW_ACL_ROLE]    Script Date: 2/23/2016 12:59:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
select * from component.role_permission
select * from component.role
select * from component.permission

select * from component.VW_PERMISSION_ROLE
*/

/*==============================================================*/
/* View: VW_PERMISSION_ROLE                                     */
/*==============================================================*/
CREATE view [component].[VW_PERMISSION_ROLE] as

select p.PERMISSION_ID
      ,p.PERMISSION_CODE as [PERMISSION]
      ,p.PERMISSION_TYPE_CODE
      ,r.ROLE_ID
      ,r.ROLE_CODE as [ROLE]
      ,r.ROLE_TYPE_CODE
  from component.role_permission rp
       inner join component.ROLE r on  r.ROLE_ID = rp.ROLE_ID
       inner join component.PERMISSION p on  p.PERMISSION_ID = rp.PERMISSION_ID

GO


