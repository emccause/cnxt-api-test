/****** Object:  View [component].[VW_ACL_ROLE]    Script Date: 2/23/2016 1:05:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*
select * from component.acl
select * from component.role_permission
select * from component.role

select * from component.VW_ACL_ROLE
select * from component.VW_PAX_ROLE
select * from component.VW_PAX_PERMISSION
*/

/*==============================================================*/
/* View: VW_ACL_ROLE                                            */
/*==============================================================*/
CREATE view [component].[VW_ACL_ROLE] as

select r.ROLE_ID
      ,r.ROLE_CODE as [ROLE]
      ,r.ROLE_TYPE_CODE
      ,acl.SUBJECT_TABLE
      ,acl.SUBJECT_ID
      ,acl.TARGET_TABLE
      ,acl.TARGET_ID
  from component.ACL
       inner join component.ROLE r on  r.ROLE_ID = acl.ROLE_ID


GO


