/****** Object:  View [component].[VW_PAX_PERMISSION]    Script Date: 3/2/2016 1:11:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
select * from component.VW_PAX_PERMISSION where pax_id = 1
*/

/*==============================================================*/
/* View: VW_PAX_PERMISSION                                      */
/*==============================================================*/
CREATE view [component].[VW_PAX_PERMISSION] as

select
    pr.[LEVEL]
    ,perm.PERMISSION_ID
    ,perm.PERMISSION_CODE as PERMISSION
    ,pr.ROLE_ID
    ,pr.ROLE
    ,pr.QUALIFIER
    ,pr.TARGET_TABLE
    ,pr.TARGET_ID
    ,pr.SUBJECT_TABLE
    ,pr.SUBJECT_ID
    ,pr.PAX_ID
from
    component.VW_PAX_ROLE pr
    left join component.ROLE_PERMISSION rperm on rperm.role_id = pr.role_id
    left join component.PERMISSION perm on perm.permission_id = rperm.permission_id
--where target_id is null


GO


