

--Insert DISCRETIONARY and TRANSACTION_HEADER for existing raise test data
INSERT INTO component.TRANSACTION_HEADER (PAX_GROUP_ID, PROGRAM_ID, TYPE_CODE, SUB_TYPE_CODE, ACTIVITY_DATE, STATUS_TYPE_CODE, PERFORMANCE_VALUE) VALUES
(4103, 3, 'BUDG', 'RECG', '2017-04-04 11:25:00', 'APPROVED', '2.00'),
(4103, 3, 'BUDG', 'RECG', '2017-04-04 11:26:00', 'APPROVED', '2.00')

INSERT INTO component.DISCRETIONARY (TRANSACTION_HEADER_ID, AMOUNT, PERFORMANCE_DATE, DESCRIPTION, BUDGET_ID_DEBIT, FK1) VALUES
((SELECT ID FROM component.TRANSACTION_HEADER WHERE PAX_GROUP_ID = 4103 AND PROGRAM_ID = 3 AND PERFORMANCE_VALUE = '2.00' AND ACTIVITY_DATE = '2017-04-04 11:25:00'), '2.00', '2017-04-04 11:25:00', 'Raise Test Data', 1, 282075),
((SELECT ID FROM component.TRANSACTION_HEADER WHERE PAX_GROUP_ID = 4103 AND PROGRAM_ID = 3 AND PERFORMANCE_VALUE = '2.00' AND ACTIVITY_DATE = '2017-04-04 11:26:00'), '2.00', '2017-04-04 11:26:00', 'Raise Test Data', 1, 282076)

