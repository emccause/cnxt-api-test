-- update incomplete records
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    42794    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    42796    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    42797    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    42798    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    43126    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    43127    ,'PAYOUT_TYPE','CASH')
INSERT INTO COMPONENT.[TRANSACTION_HEADER_MISC] (TRANSACTION_HEADER_ID,VF_NAME,MISC_DATA) VALUES (    43128    ,'PAYOUT_TYPE','CASH')


DECLARE @batchId BIGINT
INSERT INTO component.BATCH (BATCH_TYPE_CODE, CREATE_DATE, STATUS_TYPE_CODE) VALUES
('CASH_REPORT', GETDATE(), 'NEW')
SET @batchID = (SELECT TOP 1 BATCH_ID FROM component.BATCH WHERE BATCH_TYPE_CODE = 'CASH_REPORT' ORDER BY CREATE_DATE DESC)
INSERT INTO component.BATCH_FILE (BATCH_ID, FILENAME, FILE_UPLOAD) VALUES
(@batchId, Concat('Cash-Issuance-Report_',GETDATE()),
'Transaction ID,Nomination ID,Submitted Date,Recipient''s ID,Recipient''s First Name,Recipient''s Last Name,Submitter''s ID,Submitter''s First Name,Submitter''s Last Name,Approver''s ID,Approver''s First Name,Approver''s Last Name,Award Amount,Payout Type,Program Name,Value,Headline,Program Type,Budget Name,Recognition Status,Approval Date,Issuance Date,Award Code,Batch Id
40525,43116,2014-05-22 16:01:19,6718,Tony,Bergeron,8571,Léah,Harwell,,,,25.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40523,50634,2014-09-25 16:22:16,11504,Jeremy,Oursler,8571,Léah,Harwell,,,,50.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40612,55308,2014-12-03 18:17:58,6632,Rich,Zinser,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
41196,55308,2014-12-03 18:17:58,969,Sunitha,Kumar,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
41197,55308,2014-12-03 18:17:58,8987,Alex,Mudd,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
40524,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40526,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,,APPROVED,,2015-09-16T19:02Z,,23537
40526,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40528,59408,2015-06-04 15:54:10,969,Sunitha,Kumar,8987,Alex,Mudd,,,,500.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40528,59408,2015-06-04 15:54:10,8571,Léah,Harwell,8987,Alex,Mudd,,,,500.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40529,59408,2015-06-04 15:54:10,969,Sunitha,Kumar,8987,Alex,Mudd,,,,250.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,,APPROVED,,2015-09-16T19:02Z,,23537')


DECLARE @batchIdComplete BIGINT
INSERT INTO component.BATCH (BATCH_TYPE_CODE, CREATE_DATE, STATUS_TYPE_CODE) VALUES
('CASH_REPORT', GETDATE(), 'COMPLETE')
SET @batchIdComplete= (SELECT TOP 1 BATCH_ID FROM component.BATCH WHERE BATCH_TYPE_CODE = 'CASH_REPORT' ORDER BY CREATE_DATE DESC)
INSERT INTO component.BATCH_FILE (BATCH_ID, FILENAME, FILE_UPLOAD) VALUES
(@batchIdComplete, Concat('Cash-Issuance-Report_',GETDATE()),
'Transaction ID,Nomination ID,Submitted Date,Recipient''s ID,Recipient''s First Name,Recipient''s Last Name,Submitter''s ID,Submitter''s First Name,Submitter''s Last Name,Approver''s ID,Approver''s First Name,Approver''s Last Name,Award Amount,Payout Type,Program Name,Value,Headline,Program Type,Budget Name,Recognition Status,Approval Date,Issuance Date,Award Code,Batch Id
40525,43116,2014-05-22 16:01:19,6718,Tony,Bergeron,8571,Léah,Harwell,,,,25.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40523,50634,2014-09-25 16:22:16,11504,Jeremy,Oursler,8571,Léah,Harwell,,,,50.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40612,55308,2014-12-03 18:17:58,6632,Rich,Zinser,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
41196,55308,2014-12-03 18:17:58,969,Sunitha,Kumar,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
41197,55308,2014-12-03 18:17:58,8987,Alex,Mudd,8571,Léah,Harwell,,,,10.0,CASH,Compass Kudos,,,PEER_TO_PEER,Leah''s Distributed Test Budget - Company Name - MM,APPROVED,,2015-09-16T19:02Z,,23537
40524,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40526,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,,APPROVED,,2015-09-16T19:02Z,,23537
40526,51651,2015-05-08 00:37:52,8543,Julia,Gilbert,8571,Léah,Harwell,,,,100.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40528,59408,2015-06-04 15:54:10,969,Sunitha,Kumar,8987,Alex,Mudd,,,,500.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40528,59408,2015-06-04 15:54:10,8571,Léah,Harwell,8987,Alex,Mudd,,,,500.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537
40529,59408,2015-06-04 15:54:10,969,Sunitha,Kumar,8987,Alex,Mudd,,,,250.0,CASH,You Rock!,,nomination test,PEER_TO_PEER,,APPROVED,,2015-09-16T19:02Z,,23537')