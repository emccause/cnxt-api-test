IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_APPROVAL_PROCESS_CONFIG_D_1')
BEGIN
EXEC ('CREATE TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_D_1]
ON [component].[APPROVAL_PROCESS_CONFIG]
INSTEAD OF DELETE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
  RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
  RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Set command (or operation) that this trigger is serving 
-------------------------------------------------------------------
SET @operation = ''DELETE''

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_APPROVAL_PROCESS_CONFIG (
  CHANGE_DATE
, CHANGE_ID
, CHANGE_OPERATION
, APPROVAL_PROCESS_CONFIG.ID
, APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
, APPROVAL_PROCESS_CONFIG.CREATE_DATE
, APPROVAL_PROCESS_CONFIG.CREATE_ID
, APPROVAL_PROCESS_CONFIG.UPDATE_DATE
, APPROVAL_PROCESS_CONFIG.UPDATE_ID
)
SELECT  
  @change_date
, @change_id
, @change_operation
, APPROVAL_PROCESS_CONFIG.ID
, APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
, APPROVAL_PROCESS_CONFIG.CREATE_DATE
, APPROVAL_PROCESS_CONFIG.CREATE_ID
, APPROVAL_PROCESS_CONFIG.UPDATE_DATE
, APPROVAL_PROCESS_CONFIG.UPDATE_ID
FROM
  DELETED
, component.APPROVAL_PROCESS_CONFIG
WHERE DELETED.ID = APPROVAL_PROCESS_CONFIG.ID

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
  SET @errmsg = ''%s statement failed with TRIGGER '''''' + OBJECT_NAME(@@PROCID) + ''''''. The failure occurred in database '''''' + DB_NAME() + '''''', table ''''APPROVAL_PROCESS_CONFIG''''.  Execution error @@ERROR: '' + CONVERT(VARCHAR, @err) + ''  The statement has been terminated.''
  SET @severity = 16 -- Indicate errors that can be corrected by the user
  GOTO RAISE_ERROR
END

DELETE  APPROVAL_PROCESS_CONFIG
FROM
  DELETED
, component.APPROVAL_PROCESS_CONFIG
WHERE DELETED.ID = APPROVAL_PROCESS_CONFIG.ID

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END');

END
GO
IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_APPROVAL_PROCESS_CONFIG_U_1')
BEGIN
EXEC ('CREATE TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_U_1]
ON [component].[APPROVAL_PROCESS_CONFIG]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    APPROVAL_PROCESS_CONFIG T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    APPROVAL_PROCESS_CONFIG T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END');

END
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_APPROVAL_PROCESS_CONFIG_U_2')
BEGIN
EXEC ('CREATE TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_U_2]
ON [component].[APPROVAL_PROCESS_CONFIG]
INSTEAD OF UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
  RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
  RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Set command (or operation) that this trigger is serving 
-------------------------------------------------------------------
SET @operation = ''UPDATE''

SET @change_date = GETDATE()

EXEC UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO HISTORY_APPROVAL_PROCESS_CONFIG (
  CHANGE_DATE
, CHANGE_ID
, CHANGE_OPERATION
, APPROVAL_PROCESS_CONFIG.ID
, APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
, APPROVAL_PROCESS_CONFIG.CREATE_DATE
, APPROVAL_PROCESS_CONFIG.CREATE_ID
, APPROVAL_PROCESS_CONFIG.UPDATE_DATE
, APPROVAL_PROCESS_CONFIG.UPDATE_ID
)
SELECT  
  @change_date
, @change_id
, @change_operation
, APPROVAL_PROCESS_CONFIG.ID
, APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
, APPROVAL_PROCESS_CONFIG.CREATE_DATE
, APPROVAL_PROCESS_CONFIG.CREATE_ID
, APPROVAL_PROCESS_CONFIG.UPDATE_DATE
, APPROVAL_PROCESS_CONFIG.UPDATE_ID
FROM
  INSERTED
, APPROVAL_PROCESS_CONFIG
WHERE
  INSERTED.ID = APPROVAL_PROCESS_CONFIG.ID

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
  SET @errmsg = ''%s statement failed with TRIGGER '''''' + OBJECT_NAME(@@PROCID) + ''''''. The failure occurred in database '''''' + DB_NAME() + '''''', table ''''APPROVAL_PROCESS_CONFIG''''.  Execution error @@ERROR: '' + CONVERT(VARCHAR, @err) + ''  The statement has been terminated.''
  SET @severity = 16 -- Indicate errors that can be corrected by the user
  GOTO RAISE_ERROR
END

UPDATE  APPROVAL_PROCESS_CONFIG
SET
  APPROVAL_PROCESS_TYPE_CODE = INSERTED.APPROVAL_PROCESS_TYPE_CODE
, CREATE_DATE = INSERTED.CREATE_DATE
, CREATE_ID = INSERTED.CREATE_ID
, UPDATE_DATE = INSERTED.UPDATE_DATE
, UPDATE_ID = INSERTED.UPDATE_ID
FROM
  INSERTED
, APPROVAL_PROCESS_CONFIG
WHERE
  INSERTED.ID = APPROVAL_PROCESS_CONFIG.ID

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END');

END
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_LIKE_DU_1')
BEGIN
EXEC ('CREATE TRIGGER [component].[TR_LIKE_DU_1]
ON [component].[LIKES]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
  RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
  RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
  SET @operation = ''UPDATE''
ELSE
  SET @operation = ''DELETED''

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_LIKE (
  CHANGE_DATE
, CHANGE_ID
, CHANGE_OPERATION
, LIKE_ID
, PAX_ID
,   TARGET_TABLE
, TARGET_ID
, LIKE_DATE
, CREATE_DATE
, CREATE_ID
, UPDATE_DATE
, UPDATE_ID
)
SELECT  
  @change_date
, @change_id
, @change_operation
, LIKE_ID
, PAX_ID
,   TARGET_TABLE
, TARGET_ID
, LIKE_DATE
, CREATE_DATE
, CREATE_ID
, UPDATE_DATE
, UPDATE_ID
FROM
  DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
  SET @errmsg = ''%s statement failed with TRIGGER '''''' + OBJECT_NAME(@@PROCID) + ''''''. The failure occurred in database '''''' + DB_NAME() + '''''', table ''''LIKE''''.  Execution error @@ERROR: '' + CONVERT(VARCHAR, @err) + ''  The statement has been terminated.''
  SET @severity = 16 -- Indicate errors that can be corrected by the user
  GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END');

END
GO

IF NOT EXISTS (SELECT * FROM sys.triggers WHERE name = 'TR_PAX_IMAGE_U_1')
BEGIN
EXEC ('CREATE TRIGGER [component].[TR_PAX_IMAGE_U_1]
ON [component].[IMAGES]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    IMAGES T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    IMAGES T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END');

END
GO