/*

Script created by SQL Compare version 11.3.0 from Red Gate Software Ltd at 10/16/2015 12:01:28 PM

*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[ACTIVITY_BUSINESS_RULE]'
GO
ALTER TABLE [component].[ACTIVITY_BUSINESS_RULE] DROP CONSTRAINT [FK_ACTIVITY_BUSINESS_RULE_ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[BUSINESS_RULE]'
GO
ALTER TABLE [component].[BUSINESS_RULE] DROP CONSTRAINT [FK_BUSINESS_RULE_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NEWSFEED_ITEM]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM] DROP CONSTRAINT [FK_NEWSFEED_ITEM_NEWSFEED_ITEM]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[NOMINATION]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[NOMINATION] ADD
[PARENT_ID] [bigint] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_RECOGNITION_APPROVAL_HISTORY_BASE]'
GO


-- ==============================  NAME  ======================================
-- UP_RECOGNITION_APPROVAL_HISTORY_BASE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS APPROVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID. THIS PROCEDURE MUST BE CALLED BY
-- ANOTHER PROCEDURE THAT CREATES THE #approvals TEMP TABLE THAT THIS PROCEDURE
-- POPULATES.
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- SCHIERVR        20081030                CREATED
-- THOMSOND        20081110                CHANGED FILTERING ON TOTALS CALCULATIONS
-- THOMSOND        20081120                CHANGED TOTAL POINTS CALCULATIONS
-- DOHOGNTA     20090311                REMOVED THE ENABLE_JUSTIFICATION / Y CONDITION
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_APPROVAL_HISTORY_BASE]
    @approver_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,   @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

DECLARE @PAX_ID BIGINT
SET @PAX_ID = (SELECT PAX_ID FROM PAX_GROUP WHERE PAX_GROUP_ID = @approver_pax_group_id)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE NOMINATION DATA
INSERT INTO #approvals (
    nomination_id
,    nomination_status

,    submittal_date
,    submitter_list

,    receiver_list
,    criteria
,    ecard_id
,    program_activity_id
,    justification_comment_available_flag
,    budget_id
)
SELECT    DISTINCT
    NOMINATION.ID
,    NOMINATION.STATUS_TYPE_CODE
,    NOMINATION.SUBMITTAL_DATE
,    component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION.ID, @locale_code)
,    NOMINATION.ECARD_ID
,    NOMINATION.PROGRAM_ACTIVITY_ID
,    CASE WHEN NOMINATION.COMMENT_JUSTIFICATION IS NULL THEN 0 ELSE 1 END
,    NOMINATION.BUDGET_ID
FROM    NOMINATION
,       RECOGNITION
,       APPROVAL_HISTORY
,        PROGRAM_ACTIVITY_MISC
WHERE   NOMINATION.ID = RECOGNITION.NOMINATION_ID
AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
AND    APPROVAL_HISTORY.PAX_ID = @PAX_ID
AND    NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru
AND    RECOGNITION.STATUS_TYPE_CODE IN ('P', 'A', 'R')
AND    NOMINATION.STATUS_TYPE_CODE IS NOT NULL

-- We now have the base group of nomination records for the report

-- Update the additional fields needed for the report
-- This flag is used to invoke the auto-hide function on the report's Justification column.
-- The report will sum this column.  Any value > 0 indicates Justification column should be visible.
UPDATE    #approvals
SET    #approvals.justification_enabled_flag = 1
FROM    PROGRAM_ACTIVITY_MISC
WHERE    #approvals.PROGRAM_ACTIVITY_ID = PROGRAM_ACTIVITY_MISC.PROGRAM_ACTIVITY_ID
AND    GETDATE() BETWEEN PROGRAM_ACTIVITY_MISC.FROM_DATE AND ISNULL(PROGRAM_ACTIVITY_MISC.THRU_DATE, GETDATE())

-- There are several queries that follow that only need to populate a column if there is only one recipient.
-- This flag may be used as a filter for those queries. 1=true.
UPDATE    #approvals
SET    #approvals.multiple_recipient_flag = 1
WHERE    receiver_list LIKE '%,%'

-- This flag will be used by the report engine to control the display of the "Submitted By" column. 1=true.
UPDATE    #approvals
SET    #approvals.multiple_submitter_flag = 1
WHERE    submitter_list LIKE '%,%'

-- Get the first receiver name from RECOGNITION table.  If this is the only name, the report will use it.
-- Otherwise the name will be replaced with "Multiple Recipients" by the report engine.
UPDATE    #approvals
SET    receiver_name = (
    SELECT TOP 1 ISNULL(PAX.FIRST_NAME, '') + ' ' + ISNULL(PAX.LAST_NAME, '')
    FROM    RECOGNITION
    ,    PAX
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
    AND    #approvals.multiple_recipient_flag <> 1
)

-- Get the approval_process_id for each nomination.  It will be used to determine the number of
-- approvals that have been configured for each nomination.
UPDATE    #approvals
SET    approval_process_id = APPROVAL_PROCESS.ID
FROM    APPROVAL_PROCESS
WHERE    #approvals.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #approvals.PROGRAM_ACTIVITY_ID = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID

-- Get the number of approvals for each nomination
UPDATE    #approvals
SET    approval_count = (
    SELECT    COUNT(1)
    FROM    (
        SELECT APPROVAL_PENDING.ID AS ID
        FROM    RECOGNITION
        ,    APPROVAL_PENDING
        WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
        AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
        AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #approvals.approval_process_id
        UNION
        SELECT    APPROVAL_HISTORY.ID AS ID
        FROM    RECOGNITION
        ,    APPROVAL_HISTORY
        WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
        AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
        AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    ) AS APPROVALS
)

-- Get the first submitter name.  If there are multiple submitters stored in auxiliary_submitter table,
-- then the report engine will replace this name with the word "Multiple".
UPDATE    #approvals
SET    submitter_name = ISNULL(PAX.FIRST_NAME, '') + ' ' + ISNULL(PAX.LAST_NAME, '')
FROM    PAX
,    NOMINATION
WHERE    #approvals.nomination_id = NOMINATION.ID
AND    PAX.PAX_ID = NOMINATION.SUBMITTER_PAX_ID

-- Get the total approved, denied or pending award amount for each nomination
UPDATE    #approvals
SET    total_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.STATUS IN ('P', 'A', 'R')
)

-- Get the total approved points for each nomination
UPDATE    #approvals
SET    total_approved_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS = 'A'
)

-- Get the total denied points for each nomination
UPDATE    #approvals
SET    total_denied_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS = 'R'
)

-- Get the total pending points for each nomination
UPDATE    #approvals
SET    total_pending_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS = 'P'
)

-- The recognition_id is used in the construction of the Ecard hyperlink in the report.
-- This is another case where it is only used if there is only 1 recognition.
UPDATE    #approvals
SET    recognition_id = (
    SELECT    TOP 1 RECOGNITION.ID
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    #approvals.multiple_recipient_flag = 0
)

-- The denial_reason will be displayed in a roll-over on the report but only if there is one recipient.
UPDATE    #approvals
SET    denial_reason = APPROVAL_HISTORY.APPROVAL_NOTES
    FROM    RECOGNITION
    ,    APPROVAL_HISTORY
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
    AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    AND    #approvals.nomination_status = 'R'

-- Get the name of the next approver.  Will be displayed if there is only one next approver or all next approvers are the same.
CREATE TABLE #next_approvers (nomination_id BIGINT,approver nvarchar(80))

INSERT INTO #next_approvers (
    nomination_id
,    approver
)
SELECT    RECOGNITION.NOMINATION_ID,ISNULL(PAX.FIRST_NAME,'') + ' ' + ISNULL(PAX.LAST_NAME,'')
FROM    #approvals
,    PAX
,    RECOGNITION
,    APPROVAL_PENDING
WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #approvals.approval_process_id
AND    APPROVAL_PENDING.PAX_ID = PAX.PAX_ID

-- If multiple different next approver names are in #next_approvers then substitute a '-1'.
-- The report engine will replace '-1' with the words 'Multiple Approvers'
UPDATE #approvals
SET    next_approver_count = (
    SELECT COUNT(DISTINCT approver)
    FROM #next_approvers na
    WHERE #approvals.nomination_id = na.nomination_id
)

UPDATE #approvals
SET    next_approver_name = (
    SELECT DISTINCT approver
    FROM    #next_approvers
    WHERE    #approvals.nomination_id = #next_approvers.nomination_id
    AND    #approvals.next_approver_count = 1
)


-- The approval date is the date of an approval by the pax requesting the report.
-- The date count indicates the number of unique approval dates(Days) there are per nomination at the highest approval level.
CREATE TABLE #filtered_approval_dates (
    nomination_id BIGINT
,    approval_level INT
,    dates DATETIME2
)

INSERT INTO #filtered_approval_dates (
    nomination_id
,    approval_level
,    dates
)
SELECT    NOMINATION_ID
,    APPROVAL_LEVEL
,    DATES
FROM    (SELECT #approvals.nomination_id
    ,    APPROVAL_HISTORY.APPROVAL_LEVEL
    ,    CONVERT(DATETIME2, CONVERT(VARCHAR, APPROVAL_HISTORY.APPROVAL_DATE, 101)) AS DATES
    FROM    #approvals
    ,    RECOGNITION
    ,    APPROVAL_HISTORY
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
    AND    RECOGNITION.STATUS = APPROVAL_HISTORY.APPROVAL_STATUS
    AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    AND    APPROVAL_HISTORY.PAX_ID = @PAX_ID
    ) AS APPROVALS
GROUP BY
    NOMINATION_ID
,    APPROVAL_LEVEL
,    DATES
HAVING    APPROVAL_LEVEL = MAX(APPROVAL_LEVEL)

UPDATE    #approvals
SET    approval_date_count = (
    SELECT COUNT(1)
    FROM #filtered_approval_dates
    WHERE #filtered_approval_dates.nomination_id = #approvals.nomination_id
    GROUP BY #filtered_approval_dates.nomination_id
)

UPDATE    #approvals
SET    approval_date = (
    SELECT     dates
    FROM    #filtered_approval_dates
    WHERE    #approvals.nomination_id = #filtered_approval_dates.NOMINATION_ID
    AND    #approvals.approval_date_count = 1
)

UPDATE    #approvals
SET    program_activity_name = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_NAME
FROM    NOMINATION
,    PROGRAM_ACTIVITY
WHERE    #approvals.program_activity_id = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_ID

-- Update the award type
INSERT INTO #award_types (
    nomination_id
,    award_type
)
SELECT
    a.nomination_id
,    CASE
        WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN
            CASE
                  WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_NAME
            END
         ELSE
            CASE
                WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_DESC
             END
        END AS award_type
FROM    PAYOUT_ITEM PI
RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
    ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
RIGHT OUTER JOIN #approvals a WITH (NOLOCK)
    ON RB.ID  = a.budget_id

UPDATE  #approvals
SET #approvals.award_type = a.award_type
FROM    #approvals
INNER JOIN #award_types A ON #approvals.nomination_id = a.nomination_id

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_RECOGNITION_BUDGET_CSV_REPORT]'
GO


-- ==============================  NAME  ======================================
-- UP_RECOGNITION_BUDGET_CSV_REPORT
-- ===========================  DESCRIPTION  ==================================
-- PRODUCESE CSV OUTPUT TO RESEMBLE THE RECOGNITION BUDGET REPORT.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- SCHIERVR        20081030                CREATED
-- CHIDRIS        20130328                Added locale code
-- DOHOGNTA        20150119                USE STANDALONE JUNCTION TABLE
-- DOHOGNTA        20150120                REMOVE SUCCESSOR_ID DEPENDENCIES
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_BUDGET_CSV_REPORT]
    @sys_user_id BIGINT
,    @report_pax_group_id BIGINT
,    @selected_pax_group_id BIGINT
,    @useselected VARCHAR(10)
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

create table #permission_codes (
    permission_code VARCHAR(10)
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

insert into #permission_codes (permission_code)
    exec component.UP_ASSOCIATION_FETCH_PERMISSIONS @sys_user_id

declare @pax_group_id BIGINT

-- holds 'true', if the user having the permission 'VCRPTL' other wise 'false'
declare @flag nvarchar(10);

-- If @selected_pax_group_id doesn't exist in request then consider @report_pax_group_id as selected_pax_group_id

if isnull(@selected_pax_group_id, '') = ''
begin
    set @pax_group_id = @report_pax_group_id
end
else
begin
    set @pax_group_id = @selected_pax_group_id
end

declare @pax_id BIGINT

-- Getting the pax_id based on @pax_group_id
select @pax_id = pax_id from pax_group where pax_group_id = @pax_group_id

-- Getting @sys_user_id based on @pax_id
select @sys_user_id = sys_user_id from sys_user where pax_id =  @pax_id and STATUS_TYPE_CODE ='A'

-- Getting  the permission_codes based on sys_user_id
--insert into #permission_codes (permission_code)
--    exec UP_ASSOCIATION_FETCH_PERMISSIONS @sys_user_id

select  @flag =  CASE WHEN count(*) > 0 THEN 'true'
                       ELSE  'false'
                  END
from #permission_codes where 'RBVALL' IN  (SELECT permission_code FROM #permission_codes)


SELECT DISTINCT rb.ID, component.UF_TRANSLATE (@locale_code, rbc.RECOGNITION_BUDGET_CATEGORY_NAME, 'RECOGNITION_BUDGET_CATEGORY', 'RECOGNITION_BUDGET_CATEGORY_NAME') AS CATEGORY_NAME,
    rb.RECOGNITION_BUDGET_CATEGORY_CODE AS CATEGORY_CODE,
    rb.RECOGNITION_BUDGET_CATEGORY_CODE AS DETAIL_CATEGORY_CODE,
    component.UF_TRANSLATE (@locale_code,rb.RECOGNITION_BUDGET_NAME,'RECOGNITION_BUDGET','RECOGNITION_BUDGET_NAME') AS BUDGET_NAME, ISNULL(bt.BUDGET,0) AS TOTAL_BUDGET,
    ISNULL(bd.DEBIT,0) AS BUDGET_USED, ISNULL(bt.BUDGET,0) - ISNULL(bd.DEBIT, 0) as REMAINING_BUDGET,
    CAST(CASE WHEN ISNULL(bt.BUDGET,0) = 0 THEN 0
            ELSE (ISNULL(bt.BUDGET,0) - ISNULL(bd.DEBIT,0)) / ISNULL(bt.BUDGET,0)
            END AS numeric(10,2)) AS REMAINING_BUDGET_PERCENT
FROM component.RECOGNITION_BUDGET rb
INNER JOIN component.RECOGNITION_BUDGET_CATEGORY rbc
    ON rb.RECOGNITION_BUDGET_CATEGORY_CODE = rbc.RECOGNITION_BUDGET_CATEGORY_CODE
INNER JOIN component.RECOGNITION_BUDGET_PERMISSION a
    ON rb.ID = a.RECOGNITION_BUDGET_ID
INNER JOIN component.PERMISSION p
    ON a.PERMISSION_ID = p.PERMISSION_ID
        AND p.PERMISSION_TYPE_CODE = 'RBV'
LEFT OUTER JOIN    (
        SELECT rb.RECOGNITION_BUDGET_CATEGORY_CODE AS CATEGORY_CODE,
            rb.RECOGNITION_BUDGET_NAME AS NAME,
            SUM(rba.AMOUNT) AS BUDGET
        FROM component.TRANSACTION_HEADER th
        INNER JOIN component.RECOGNITION_BUDGET_ALLOCATION rba
            ON th.ID = rba.TRANSACTION_HEADER_ID
                AND (th.SUB_TYPE_CODE = N'CR')
                AND (rba.STATUS_CODE IN (N'APPR',N'PEND'))
        INNER JOIN component.RECOGNITION_BUDGET rb
            ON rba.RECOGNITION_BUDGET_ID = rb.ID
        GROUP BY rb.RECOGNITION_BUDGET_CATEGORY_CODE, rb.RECOGNITION_BUDGET_NAME
        ) AS bt -- BUDGET_TOTALS
    ON bt.CATEGORY_CODE = rb.RECOGNITION_BUDGET_CATEGORY_CODE
        AND bt.NAME = rb.RECOGNITION_BUDGET_NAME
LEFT OUTER JOIN (
        SELECT rb.RECOGNITION_BUDGET_CATEGORY_CODE AS CATEGORY_CODE,
            rb.RECOGNITION_BUDGET_NAME AS NAME,
            SUM(rba.AMOUNT) AS DEBIT
        FROM component.TRANSACTION_HEADER th
        INNER JOIN component.RECOGNITION_BUDGET_ALLOCATION rba
            ON th.ID = rba.TRANSACTION_HEADER_ID
                AND th.SUB_TYPE_CODE = N'DR'
                AND (rba.STATUS_CODE IN (N'APPR',N'PEND'))
        INNER JOIN component.RECOGNITION_BUDGET rb
            ON rba.RECOGNITION_BUDGET_ID = rb.ID
        GROUP BY rb.RECOGNITION_BUDGET_CATEGORY_CODE, rb.RECOGNITION_BUDGET_NAME
        ) AS bd -- BUDGET_DEBITS
    ON bd.CATEGORY_CODE = rb.RECOGNITION_BUDGET_CATEGORY_CODE
        AND bd.NAME = rb.RECOGNITION_BUDGET_NAME
WHERE p.PERMISSION_CODE IN (select permission_code from #permission_codes)
ORDER BY CATEGORY_NAME, BUDGET_NAME

select ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ') AS EMPLOYEE
from component.PAX,component.PAX_GROUP
WHERE PAX.PAX_ID = PAX_GROUP.PAX_ID
AND PAX_GROUP.PAX_GROUP_ID = @pax_group_id

SELECT CONVERT(VARCHAR,FROM_DATE, 101) AS FROM_DATE,
       CONVERT(VARCHAR,ISNULL(THRU_DATE, GETDATE()), 101) AS THRU_DATE
FROM component.PROGRAM
WHERE PROGRAM_ID = 1

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_RECOGNITION_CRITERIA_BASE]'
GO


-- ==============================  NAME  ======================================
-- UP_RECOGNITION_CRITERIA_BASE
-- ===========================  DESCRIPTION  ==================================
-- RETURN THE SUM OF INDIVIDUALS, CRITERIA AND RECOGNITION
--
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- PALD00        20081020                CREATED
-- SCHIERVR        20081030                MOVED CODE FROM REPORTS TO BASE.
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_CRITERIA_BASE]
    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,    @recognition_selection_criteria NVARCHAR(4)
AS
BEGIN

SET NOCOUNT ON

-- DECLARE GLOBAL VARIABLES
DECLARE  @pax_id BIGINT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE PAXID
SET @pax_id = (SELECT PAX_ID
               FROM   PAX_GROUP
               WHERE  PAX_GROUP_ID = @pax_group_id)

-- GET ALL THE PAXID IN THE PAX GROUP
CREATE TABLE #pax_hierarchy_list (PAX_ID BIGINT NULL)
INSERT INTO #pax_hierarchy_list SELECT id AS PAX_ID FROM UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX (@pax_id, 1, NULL)

-- INSERT DATA IN THE TEMPORARY TABLE
INSERT INTO #recognition_criteria (
    nomination_id
,    recognition_id
,    submitter_pax_id
,    receiver_pax_id
,    recogniton_criteria_id
,    recogniton_criteria_name
,    recognition_amount
)

SELECT    NOMINATION.ID
,    RECOGNITION.ID
,    NOMINATION.SUBMITTER_PAX_ID
,    RECOGNITION.RECEIVER_PAX_ID
,    RECOGNITION_CRITERIA.ID
,    RECOGNITION_CRITERIA.RECOGNITION_CRITERIA_NAME
,    RECOGNITION.AMOUNT
FROM    RECOGNITION RECOGNITION
INNER JOIN NOMINATION
    ON RECOGNITION.NOMINATION_ID = NOMINATION.ID
INNER JOIN NOMINATION_CRITERIA
    ON NOMINATION.ID = NOMINATION_CRITERIA.NOMINATION_ID
INNER JOIN RECOGNITION_CRITERIA
    ON NOMINATION_CRITERIA.RECOGNITION_CRITERIA_ID = RECOGNITION_CRITERIA.ID
WHERE   (NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru)
AND (RECOGNITION.STATUS_TYPE_CODE = N'A')
AND (NOMINATION.SUBMITTER_PAX_ID IN (SELECT PAX_ID FROM #pax_hierarchy_list)
    OR  RECOGNITION.RECEIVER_PAX_ID IN (SELECT PAX_ID FROM #pax_hierarchy_list))

-- FILTER OUT THE RECORDS AS PER @recognition_selection_criteria
IF (@recognition_selection_criteria = 'RR')
BEGIN
    DELETE #recognition_criteria
    WHERE receiver_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)
END
ELSE
IF (@recognition_selection_criteria = 'RG')
BEGIN
    DELETE #recognition_criteria
    WHERE submitter_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)
END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_BUDGET_INFO]'
GO







-- ==============================  NAME  ======================================
-- UP_GET_BUDGET_INFO
-- ===========================  DESCRIPTION  ==================================
--
-- GIVEN A BUDGET_ID, RETURN BUDGET INFO FOR THAT BUDGET
-- AND ANY CHILD BUDGETS, INCLUDING TOTAL AMOUNT USED FROM THE BUDGET,
-- TOTAL CURRENT MAX BUDGET LIMIT, AND AN IS_USED FLAG
-- USED FOR DELETING BUDGETS.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- HARWELLM        20150519                CREATED
-- HARWELLM        20150529                Modified to use the BUDGET_INFO view
-- DAGARFIN      20151005                   REMOVING PARENT ID SINCE IT'S USED FOR BUDGETS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_BUDGET_INFO]
@budgetId BIGINT, @paxId BIGINT

AS
BEGIN

SET NOCOUNT ON


-- DRIVER
 CREATE TABLE #BUDGET_INFO (
    ID BIGINT NOT NULL,
    BUDGET_TYPE_CODE NVARCHAR(20) NOT NULL,
    BUDGET_NAME NVARCHAR(100) NOT NULL,
    BUDGET_DISPLAY_NAME NVARCHAR(100) NULL,
    BUDGET_DESC NVARCHAR(1000) NULL,
    PARENT_BUDGET_ID BIGINT NULL,
    INDIVIDUAL_GIVING_LIMIT DECIMAL(10,2) NULL,
    FROM_DATE DATETIME2(7) NOT NULL,
    THRU_DATE DATETIME2(7) NULL,
    STATUS_TYPE_CODE NVARCHAR(20) NOT NULL,
    SUB_PROJECT_ID BIGINT NULL,
    BUDGET_CONSTRAINT NVARCHAR(50) NOT NULL,
    PROJECT_NUMBER NVARCHAR(25) NOT NULL,
    SUB_PROJECT_NUMBER NVARCHAR(8) NULL,
    CREATE_DATE DATETIME2(7) NULL,
    ALERT_AMOUNT DECIMAL(10,2) NULL,
    GROUP_CONFIG_ID BIGINT NULL,
    GROUP_ID BIGINT NULL,
    GROUP_NAME NVARCHAR(255) NULL,
    TOTAL_USED DECIMAL(10,2) NULL,
    TOTAL_AMOUNT DECIMAL(10,2) NULL,
    IS_USED NVARCHAR(5) NULL,
    INDIVIDUAL_USED_AMOUNT DECIMAL(10,2) NULL
)

--Get the basic budget info from the BUDGET_INFO view
INSERT INTO #BUDGET_INFO (
    id
,    budget_type_code
,    budget_name
,    budget_display_name
,    budget_desc
,    parent_budget_id
,    individual_giving_limit
,    from_date
,    thru_date
,    status_type_code
,    sub_project_id
,    budget_constraint
,    project_number
,    sub_project_number
,    create_date
,    alert_amount
,    group_config_id
,    group_id
,    group_name
,    total_used
,    total_amount
,    is_used
)
SELECT * FROM component.VW_BUDGET_INFO WHERE ID = @budgetId OR PARENT_BUDGET_ID = @budgetId


-- SUM the total budget used and total amount for child budgets, and save that to the parent budget
create table #parentAmounts (
PARENT_BUDGET_ID BIGINT,
TOTAL_USED DECIMAL(10,2),
TOTAL_AMOUNT DECIMAL(10,2)
)
insert into #parentAmounts select PARENT_BUDGET_ID, SUM(TOTAL_USED) AS 'TOTAL_USED', SUM(TOTAL_AMOUNT) AS 'TOTAL_AMOUNT' from #budget_info where parent_budget_id is not null group by parent_budget_id

--Save TOTAL_USED for parent
update #BUDGET_INFO set TOTAL_USED = (select #parentAmounts.TOTAL_USED from #parentAmounts
where #parentAmounts.PARENT_BUDGET_ID = #BUDGET_INFO.ID)
where ID in (select PARENT_BUDGET_ID from #parentAmounts)

--Save TOTAL_AMOUNT for parent
update #BUDGET_INFO set TOTAL_AMOUNT = (select #parentAmounts.TOTAL_AMOUNT from #parentAmounts
where #parentAmounts.PARENT_BUDGET_ID = #BUDGET_INFO.ID)
where ID in (select PARENT_BUDGET_ID from #parentAmounts)



--Get Individual Budget Used
CREATE TABLE #usedAmounts (
BUDGET_ID BIGINT,
TOTAL_USED DECIMAL(10,2)
)

INSERT INTO #usedAmounts
SELECT DISTINCT
b.ID AS 'BUDGET_ID',
(CASE
    WHEN DEBITS.DEBITS is null then 0
    ELSE DEBITS.DEBITS
END)
-
(CASE
    WHEN CREDITS.CREDITS is null then 0
    ELSE CREDITS.CREDITS
END)
AS 'TOTAL_USED'
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
JOIN component.BUDGET_ALLOCATION ba ON ba.FK1 = r.ID AND ba.STATUS_TYPE_CODE = 'APPROVED'
JOIN component.BUDGET b ON b.ID = ba.BUDGET_ID_DEBIT OR b.ID = ba.BUDGET_ID_CREDIT
LEFT JOIN
(--DEBITS
SELECT ba_debit.BUDGET_ID_DEBIT AS 'ID', SUM(ba_debit.AMOUNT) AS 'DEBITS'
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
JOIN component.BUDGET_ALLOCATION ba_debit ON ba_debit.FK1 = r.ID AND ba_debit.STATUS_TYPE_CODE = 'APPROVED'
JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = ba_debit.TRANSACTION_HEADER_ID AND th_debit.TYPE_CODE = 'BUDG' AND th_debit.SUB_TYPE_CODE <> 'FUND'
GROUP BY ba_debit.BUDGET_ID_DEBIT
--WHERE n.SUBMITTER_PAX_ID = 8571 AND ba_debit.BUDGET_ID_DEBIT = 1
) DEBITS
ON B.ID = DEBITS.ID
LEFT JOIN
(--CREDITS
SELECT ba_credit.BUDGET_ID_CREDIT AS 'ID', SUM(ba_credit.AMOUNT) AS 'CREDITS'
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
JOIN component.BUDGET_ALLOCATION ba_credit ON ba_credit.FK1 = r.ID AND ba_credit.STATUS_TYPE_CODE = 'APPROVED'
JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = ba_credit.TRANSACTION_HEADER_ID AND th_credit.TYPE_CODE = 'BUDG' AND th_credit.SUB_TYPE_CODE <> 'FUND'
GROUP BY ba_credit.BUDGET_ID_CREDIT
--WHERE n.SUBMITTER_PAX_ID = 8571 AND ba_credit.BUDGET_ID_CREDIT = 1
) CREDITS
ON B.ID = CREDITS.ID
WHERE N.SUBMITTER_PAX_ID = @paxId

update #BUDGET_INFO set INDIVIDUAL_USED_AMOUNT = (select #usedAmounts.TOTAL_USED from #usedAmounts
where #usedAmounts.BUDGET_ID = #BUDGET_INFO.ID)
where ID in (select BUDGET_ID from #usedAmounts)

--RESULT_SET
SELECT * FROM #BUDGET_INFO

DROP TABLE #BUDGET_INFO
DROP TABLE #usedAmounts
DROP TABLE #parentAmounts

END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_RECOGNITION_GIVEN_BASE]'
GO


-- ==============================  NAME  ======================================
-- UP_RECOGNITION_GIVEN_BASE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS GIVEN BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID.  THIS PROCEDURE MUST BE CALLED BY
-- ANOTHER PROCEDURE THAT CREATES THE #NOMINATION TEMP TABLE THAT THIS PROCEDURE
-- POPULATES.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- THOMSOND        20081029                CREATED
-- BESSBS        20090518                FIX PROBLEMS WITH THRU_DATE FOR JUSTIFICATION COMMENTS
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_GIVEN_BASE]
    @submitter_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE NOMINATION DATA.  #nomination is created by the calling stored procedure.
INSERT INTO #nomination (
    nomination_id
,    nomination_status

,    submittal_date
,    submitter_list

,    receiver_list

,    comment_type
,    justification_comment_available_flag

,    criteria
,    program_activity_id
,    budget_id
)
SELECT
    NOMINATION.ID
,    NOMINATION.STATUS_TYPE_CODE
,    NOMINATION.SUBMITTAL_DATE
,    component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION(NOMINATION.ID)
,    CASE
        WHEN NOMINATION.ECARD_ID IS NULL THEN 'C'
        ELSE 'E'
    END
,    CASE WHEN NOMINATION.COMMENT_JUSTIFICATION IS NULL THEN 0 ELSE 1 END
,    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION.ID, @locale_code)
,    NOMINATION.PROGRAM_ACTIVITY_ID
,    NOMINATION.BUDGET_ID
FROM    NOMINATION
,    PAX_GROUP
WHERE    NOMINATION.SUBMITTER_PAX_ID = PAX_GROUP.PAX_ID
AND    NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru
AND    NOMINATION.STATUS_TYPE_CODE IN ('P', 'A', 'R', 'F')
AND    PAX_GROUP.PAX_GROUP_ID = @submitter_pax_group_id
AND    NOMINATION.STATUS_TYPE_CODE IS NOT NULL

UPDATE    #nomination
SET    #nomination.justification_flag = 1
FROM    PROGRAM_ACTIVITY_MISC
WHERE    #nomination.PROGRAM_ACTIVITY_ID = PROGRAM_ACTIVITY_MISC.PROGRAM_ACTIVITY_ID
AND    PROGRAM_ACTIVITY_MISC.VF_NAME = 'ENABLE_JUSTIFICATION'
AND    PROGRAM_ACTIVITY_MISC.MISC_DATA IN ('Y', 'O')
AND    GETDATE() BETWEEN PROGRAM_ACTIVITY_MISC.FROM_DATE AND ISNULL(THRU_DATE, GETDATE())

UPDATE    #nomination
SET    receiver_name = (
    SELECT TOP 1 ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
    FROM    RECOGNITION
    ,    PAX
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
    AND    RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE    #nomination
SET    last_approval_date = (
    SELECT    MAX(APPROVAL_HISTORY.APPROVAL_DATE)
    FROM    RECOGNITION
    ,    APPROVAL_HISTORY
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
    AND    RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE    #nomination
SET    approval_process_id = APPROVAL_PROCESS.ID
FROM    #nomination
,    APPROVAL_PROCESS
WHERE    #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #nomination.PROGRAM_ACTIVITY_ID = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID

UPDATE    #nomination
SET    approval_count = (
    SELECT    COUNT(1)
    FROM    (
        SELECT APPROVAL_PENDING.ID AS ID
        FROM    RECOGNITION
        ,    APPROVAL_PENDING
        WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
        AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
        AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
        AND    RECOGNITION.STATUS IN ('P','A','R')
        UNION
        SELECT    APPROVAL_HISTORY.ID AS ID
        FROM    RECOGNITION
        ,    APPROVAL_HISTORY
        WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
        AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
        AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
        AND    RECOGNITION.STATUS IN ('P','A','R')
    ) AS APPROVALS
)

UPDATE    #nomination
SET    submitter_name = ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
FROM    #nomination
,    PAX
,    NOMINATION
WHERE    #nomination.nomination_id = NOMINATION.ID
AND    PAX.PAX_ID = NOMINATION.SUBMITTER_PAX_ID

UPDATE    #nomination
SET    program_activity_name = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_NAME
FROM    nomination
,    PROGRAM_ACTIVITY
WHERE    #nomination.program_activity_id = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_ID

-- Get the total approved, denied or pending award amount for each nomination
UPDATE    #nomination
SET    total_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS IN ('P', 'A', 'R')
)

-- Get the total approved points for each nomination
UPDATE    #nomination
SET    total_approved_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END

    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS = 'A'
)

-- Get the total denied points for each nomination
UPDATE    #nomination
SET    total_denied_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END

    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS = 'R'
)

-- Get the total pending points for each nomination
UPDATE    #nomination
SET    total_pending_points = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS = 'P'
)

UPDATE    #nomination
SET    recognition_id = (
    SELECT    TOP 1 RECOGNITION.ID
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE    #nomination
SET    approval_notes = APPROVAL_HISTORY.APPROVAL_NOTES
FROM    #nomination
,    RECOGNITION
,    APPROVAL_HISTORY
WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
AND    #nomination.nomination_status = 'R'
AND    RECOGNITION.STATUS = 'R'

UPDATE    #nomination
SET    approver_count = (
    SELECT    COUNT(1)
    FROM     RECOGNITION
    ,    APPROVAL_PROCESS
    ,    APPROVAL_PENDING
    WHERE    RECOGNITION.NOMINATION_ID = #nomination.nomination_id
    AND    #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID
    AND    #nomination.PROGRAM_ACTIVITY_ID = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID
    AND    APPROVAL_PROCESS.ID = APPROVAL_PENDING.APPROVAL_PROCESS_ID
    AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
    AND    RECOGNITION.STATUS IN ('P','A','R')
)

-- DO MISCELLANOUS RULES SO THE REPORT DOESN'T HAVE TO
UPDATE    #nomination
SET    last_approval_date = NULL
WHERE    nomination_status = 'P'

UPDATE    #nomination
SET    last_approval_date = submittal_date
WHERE    approval_count = 0
AND    nomination_status <> 'P'

UPDATE    #nomination
SET    approver_name = (
    SELECT
        CASE
            WHEN approver_count > 1 THEN '>1'
            ELSE (    SELECT    ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
                FROM    PAX
                ,    RECOGNITION
                ,    APPROVAL_PENDING
                WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
                AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
                AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
                AND    APPROVAL_PENDING.PAX_ID = PAX.PAX_ID
                AND    RECOGNITION.STATUS IN ('P','A','R'))
        END
)

UPDATE    #nomination
SET    submitter_name =
    CASE
        WHEN #nomination.submitter_list LIKE '%,%' THEN '>1'
        ELSE #nomination.submitter_name
    END

UPDATE    #nomination
SET    receiver_name =
    CASE
        WHEN #nomination.receiver_list LIKE '%,%' THEN '>1'
        ELSE #nomination.receiver_name
    END

UPDATE    #nomination
SET    comment_type =
    CASE
        WHEN #nomination.receiver_list LIKE '%,%' AND #nomination.comment_type = 'E' THEN 'M'
        ELSE #nomination.comment_type
    END

-- Update the award type
INSERT INTO #award_types (
    nomination_id
,    award_type
)
SELECT
    n.nomination_id
,   CASE
        WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN
            CASE
                WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_NAME
            END
        ELSE
            CASE
                WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_DESC
            END
    END AS award_type
FROM    PAYOUT_ITEM PI
RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
    ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
RIGHT OUTER JOIN #nomination n WITH (NOLOCK)
    ON RB.ID = n.budget_id

UPDATE  #nomination
SET #nomination.award_type = a.award_type
FROM    #nomination INNER JOIN #award_types a
    ON #nomination.nomination_id = a.nomination_id

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_RECOGNITION_RECEIVED_BASE]'
GO


-- ==============================  NAME  ======================================
-- UP_RECOGNITION_RECEIVED_BASE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS RECEIVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- THOMSOND        20080819                CREATED
-- THOMSOND        20080829                CHANGE - Added is_recognition_comment
-- SCHIERVE        20081104                CHANGE - Separated into BASE/REPORT/CSV
-- BESSBS        20091118                CHANGE - DATATYPE OF recognition_date FROM VARCHAR(10) TO DATETIME
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_RECEIVED_BASE]
    @pax_group_id BIGINT
,    @submittal_from_date DATETIME2
,    @submittal_thru_date DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

DECLARE @share VARCHAR (10)

SET @share = (
    SELECT
        CASE
            WHEN VALUE_1 IS NULL THEN 'false'
            ELSE VALUE_1
        END
    FROM    APPLICATION_DATA
    WHERE    KEY_NAME = '/app/recognition/socialmedia/enable')

INSERT INTO #recognition (
    nomination_id
,    ecard_id
,    recognition_id
,    recognition_receiver_pax_id
,    recognition_status
,    recognition_amount
,    is_recognition_comment
,    program_activity_id
,    program_activity_name
,    budget_id
)
SELECT
    NOMINATION.ID
,    NOMINATION.ECARD_ID
,    RECOGNITION.ID
,    RECOGNITION.RECEIVER_PAX_ID
,    RECOGNITION.STATUS_TYPE_CODE
,    RECOGNITION.AMOUNT
,    CASE
        WHEN RECOGNITION.COMMENT IS NULL THEN 0
        ELSE 1
    END
,    PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_ID
,    PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_NAME
,    NOMINATION.BUDGET_ID
FROM    NOMINATION
,    RECOGNITION
,    PROGRAM_ACTIVITY
,    PAX
,    PAX_GROUP
WHERE    NOMINATION.ID = RECOGNITION.NOMINATION_ID
AND    NOMINATION.SUBMITTAL_DATE BETWEEN CONVERT(DATETIME2, @submittal_from_date, 102) AND CONVERT(DATETIME2, @submittal_thru_date, 102)
AND    NOMINATION.PROGRAM_ACTIVITY_ID = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_ID
AND    RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
AND    PAX_GROUP.PAX_ID = PAX.PAX_ID
AND    PAX_GROUP.PAX_GROUP_ID = @pax_group_id
AND    RECOGNITION.STATUS_TYPE_CODE = 'A'

UPDATE    #recognition
SET    APPROVAL_PROCESS_ID = APPROVAL_PROCESS.ID
FROM    #recognition
,    APPROVAL_PROCESS
WHERE    #recognition.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #recognition.program_activity_id = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID

UPDATE    #recognition
SET    RECOGNITION_DATE = (
    SELECT    MAX(APPROVAL_HISTORY.APPROVAL_DATE)
    FROM    APPROVAL_HISTORY
    WHERE    #recognition.recognition_id = APPROVAL_HISTORY.TARGET_SUB_ID
    AND    #recognition.approval_process_id = APPROVAL_HISTORY.APPROVAL_PROCESS_ID
)

UPDATE    #recognition
SET    RECOGNITION_CRITERIA = (
    SELECT    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION_ID, @locale_code)
)

UPDATE    #recognition
SET    SUBMITTER_LIST = (
    SELECT component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION_ID)
)

UPDATE    #recognition
SET    SUBMITTED_BY_NAME = (
    SELECT    PAX.FIRST_NAME + ' ' + PAX.LAST_NAME
    FROM    PAX
    ,    NOMINATION
    WHERE    #recognition.NOMINATION_ID = NOMINATION.ID
    AND    NOMINATION.SUBMITTER_PAX_ID = PAX.PAX_ID
)

INSERT INTO #award_types (
    nomination_id
,    award_type
)
SELECT
    r.nomination_id
,   CASE
        WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN
        CASE
            WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
            ELSE PI.PAYOUT_ITEM_NAME
        END
    ELSE
        CASE
            WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
            ELSE PI.PAYOUT_ITEM_DESC
        END
    END AS award_type
FROM    PAYOUT_ITEM PI
RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
    ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
RIGHT OUTER JOIN #recognition r WITH (NOLOCK)
    ON RB.ID = r.budget_id

UPDATE        #recognition
SET #recognition.award_type = a.award_type
FROM    #recognition
INNER JOIN #award_types a
    ON #recognition.nomination_id = a.nomination_id

UPDATE    #recognition
SET    SHARE = @share

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[STAGE_ENROLLMENT]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [MIDDLE_INITIAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [EMAIL_ADDRESS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [PHONE_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [PHONE_EXTENSION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ADDRESS_LINE_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ADDRESS_LINE_2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ADDRESS_LINE_3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ADDRESS_LINE_4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ADDRESS_LINE_5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [CITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [STATE_PROVINCE_REGION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [ZIP_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [COUNTRY_CODE] [nvarchar] (225) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [HIRE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [TERMINATION_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [component].[STAGE_ENROLLMENT] ALTER COLUMN [BIRTH_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ENROLL_FROM_STAGE_TABLE]'
GO










-- ==============================  NAME  ======================================
-- UP_ENROLL_FROM_STAGE_TABLE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A BATCH_ID
-- BATCH_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- RIDERAC1        20150522                CREATED
-- HARWELLM        20150729                ADDED SSO_ENABLED CHECK FOR NEW USER STATUS UPDATES
--                                        ADDED PAX_MISCS FOR PRIVACY AND RECOGNITION PREFERENCES
--                                        MODIFIED PAX_GROUP_MISC VF_NAME TO BE JOB_TITLE INSTEAD OF TITLE
-- HARWELLM        20150804                REPLACE ALL EMPTY STRINGS WITH NULL VALUES
--                                        ADD PEOPLE WITH DIRECT REPORTS TO THE 'MANAGER' GROUP
--                                        ADD NEW USERS TO THE 'ALL' GROUP
-- HARWELLM        20150805                DON'T PROCESS RECORDS WITH ERRORS
-- HARWELLM        20150810                WRITTEN/VERBAL RECOGNITION PREFERENCES SHOULD BE FALSE BY DEFAULT
--                                        CHANGED BATCH STATUS TO 'SUCCESS' INSTEAD OF 'COMPLETE'
--                                        REMOVE PAX FROM MANAGER GROUP IF ALL DIRECT REPORTS ARE REMOVED
-- HARWELLM        20150812                CREATE BATCH_EVENT RECORDS FOR FILENAME
-- HARWELLM        20150817                FIX FOR BATCH_EVENT OKRECS AND BATCH_STATUS
-- HARWELLM        20150818                BATCH_EVENT STATUSES SHOULD ALSO BE 'SUCCESS' INSTEAD OF 'COMPLETE'
--                                        FIXED ISSUE WITH INSERTING INTO THE MANAGER GROUP
--                                        ADDED COLUMN HEADERS TO ERROR FILE
-- HARWELLM        20150827                ADDED PREFERRED FLAG FOR ADDRESS AND PHONE
-- HARWELLM        20150921                CREATED NON-MANAGER ROLE-BASED GROUP
-- HARWELLM        20151001                ADDED FIELD LENGTH AND DATE FORMAT VALIDATIONS
--
-- ===========================  DECLARATIONS  =================================
--CREATE PROCEDURE [component].[UP_ENROLL_FROM_STAGE_TABLE]
ALTER PROCEDURE [component].[UP_ENROLL_FROM_STAGE_TABLE]
    @batch_id BIGINT
AS
BEGIN



--Create the FILE batch_event for this batch
INSERT INTO BATCH_EVENT (BATCH_ID, BATCH_EVENT_TYPE_CODE, MESSAGE, STATUS_TYPE_CODE)
SELECT DISTINCT @batch_id, 'FILE', FILENAME, 'SUCCESS' FROM BATCH_FILE WHERE BATCH_ID = @batch_id

/*
Do validations:
Make sure required fields exist
ERRROR for missing PARTICIPANT_ID, STATUS, PRIMARY_REPORT_ID, FIRST_NAME, LAST_NAME, LOGIN_ID, ADDRESS_line 1,
CITY, STATE, ZIP, COUNTRY_CODE
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '') + ';ParticipantID is blank', PAX_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (participant_id is null or len(participant_id) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Status is blank', SYS_USER_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (STATUS is null or len(STATUS) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PRIMARY_REPORT_ID is blank'
where batch_id = @batch_id and (PRIMARY_REPORT_ID is null or len(PRIMARY_REPORT_ID) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';FIRST_NAME is blank'
where batch_id = @batch_id and (FIRST_NAME is null or len(FIRST_NAME) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LAST_NAME is blank'
where batch_id = @batch_id and (LAST_NAME is null or len(LAST_NAME) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LOGIN_ID is blank', SYS_USER_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (LOGIN_ID is null or len(LOGIN_ID) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_line is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (ADDRESS_LINE_1 is null or len(ADDRESS_LINE_1) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CITY is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (CITY is null or len(CITY) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATE is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (STATE_PROVINCE_REGION is null or len(STATE_PROVINCE_REGION) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ZIP is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (ZIP_CODE is null or len(ZIP_CODE) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COUNTRY_CODE is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (COUNTRY_CODE is null or len(COUNTRY_CODE) = 0);

/* ERROR for wrong STATUS, either REPORT_ID does not exist, LOGIN_ID duplicate, EMAIL syntax, COUNTRY_CODE invalid
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Status is invalid'
where batch_id = @batch_id and STATUS not in ('A','R','S','I');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Duplicate login_id on file', SYS_USER_CHANGE_STATUS = 'E'
where LOGIN_ID in (
select login_id from (
select login_id, count(login_id) as cnt from stage_enrollment where BATCH_ID = @batch_id
group by login_id) cnt_qry
where cnt_qry.cnt > 1)
and BATCH_ID = @batch_id;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Email is invalid', EMAIL_CHANGE_STATUS = 'E'
where batch_id = @batch_id and EMAIL_ADDRESS not like '_%@__%.__%';

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Country_code is invalid', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and COUNTRY_CODE not in (SELECT COUNTRY_CODE FROM COUNTRY);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Primary_report_to is not found'
where batch_id = @batch_id and upper(PRIMARY_REPORT_ID) <> 'TOP' and PRIMARY_REPORT_ID not in
(select CONTROL_NUM from PAX union select PARTICIPANT_ID from STAGE_ENROLLMENT where batch_id = @batch_id)

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';secondary_report_to is not found'
where batch_id = @batch_id and (SECONDARY_REPORT_ID is not null) and len(SECONDARY_REPORT_ID) > 0 and SECONDARY_REPORT_ID not in
(select CONTROL_NUM from PAX union select PARTICIPANT_ID from STAGE_ENROLLMENT where batch_id = @batch_id)


/* ERROR for invalid formats of hire(YYYY-MM-DD), termination(YYYY-MM-DD), birthDate(MM-DD)
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';HIRE_DATE must be in YYYY-MM-DD format'
where batch_id = @batch_id and HIRE_DATE is not null
and ((ISDATE(HIRE_DATE) = 0) or HIRE_DATE NOT LIKE '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TERMINATION_DATE must be in YYYY-MM-DD format'
where batch_id = @batch_id and TERMINATION_DATE is not null
and ((ISDATE(TERMINATION_DATE) = 0) or TERMINATION_DATE NOT LIKE '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';BIRTH_DATE must be in MM-DD format'
where batch_id = @batch_id and BIRTH_DATE is not null
and BIRTH_DATE NOT LIKE '[0-1][0-9]-[0-3][0-9]';


/* ERROR for data in column exceeds max character length
*/
--Column Lengths
DECLARE @len_controlNum INT SET @len_controlNum = 255
DECLARE @len_status INT SET @len_status = 1
DECLARE @len_firstName INT SET @len_firstName = 30
DECLARE @len_lastName INT SET @len_lastName = 40
DECLARE @len_middleInitial INT SET @len_middleInitial = 1
DECLARE @len_miscData INT SET @len_miscData = 255
DECLARE @len_sysUserName INT SET @len_sysUserName = 255
DECLARE @len_sysUserPassword INT SET @len_sysUserPassword = 255
DECLARE @len_emailAddress INT SET @len_emailAddress = 80
DECLARE @len_phone INT SET @len_phone = 15
DECLARE @len_phoneExt INT SET @len_phoneExt = 5
DECLARE @len_address INT SET @len_address = 40
DECLARE @len_city INT SET @len_city = 40
DECLARE @len_state INT SET @len_state = 20
DECLARE @len_zip INT SET @len_zip = 9
DECLARE @len_countryCode INT SET @len_countryCode = 3
DECLARE @len_birthDate INT SET @len_birthDate = 4
DECLARE @len_hireDate INT SET @len_hireDate = 7

--Length Validation
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PARTICIPANT_ID exceeds max character count'
where batch_id = @batch_id and len(PARTICIPANT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATUS exceeds max character count'
where batch_id = @batch_id and len(STATUS) > @len_status;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PRIMARY_REPORT_ID exceeds max character count'
where batch_id = @batch_id and len(PRIMARY_REPORT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';SECONDARY_REPORT_ID exceeds max character count'
where batch_id = @batch_id and len(SECONDARY_REPORT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';FIRST_NAME exceeds max character count'
where batch_id = @batch_id and len(FIRST_NAME) > @len_firstName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LAST_NAME exceeds max character count'
where batch_id = @batch_id and len(LAST_NAME) > @len_lastName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';MIDDLE_INITIAL exceeds max character count'
where batch_id = @batch_id and len(MIDDLE_INITIAL) > @len_middleInitial;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TITLE exceeds max character count'
where batch_id = @batch_id and len(TITLE) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COMPANY_NAME exceeds max character count'
where batch_id = @batch_id and len(COMPANY_NAME) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LOGIN_ID exceeds max character count'
where batch_id = @batch_id and len(LOGIN_ID) > @len_sysUserName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PASSWORD exceeds max character count'
where batch_id = @batch_id and len(PASSWORD) > @len_sysUserPassword;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';EMAIL_ADDRESS exceeds max character count'
where batch_id = @batch_id and len(EMAIL_ADDRESS) > @len_emailAddress;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_NUMBER exceeds max character count'
where batch_id = @batch_id and len(PHONE_NUMBER) > @len_phone;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_EXTENSION exceeds max character count'
where batch_id = @batch_id and len(PHONE_EXTENSION) > @len_phoneExt;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_1 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_1) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_2 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_2) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_3 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_3) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_4 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_4) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_5 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_5) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CITY exceeds max character count'
where batch_id = @batch_id and len(CITY) > @len_city;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATE_PROVINCE_REGION exceeds max character count'
where batch_id = @batch_id and len(STATE_PROVINCE_REGION) > @len_state;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ZIP_CODE exceeds max character count'
where batch_id = @batch_id and len(ZIP_CODE) > @len_zip;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COUNTRY_CODE exceeds max character count'
where batch_id = @batch_id and len(COUNTRY_CODE) > @len_countryCode;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';HIRE_DATE exceeds max character count'
where batch_id = @batch_id and len(HIRE_DATE) > @len_hireDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TERMINATION_DATE exceeds max character count'
where batch_id = @batch_id and len(TERMINATION_DATE) > @len_birthDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';BIRTH_DATE exceeds max character count'
where batch_id = @batch_id and len(BIRTH_DATE) > @len_birthDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';DEPARTMENT exceeds max character count'
where batch_id = @batch_id and len(DEPARTMENT) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COST_CENTER exceeds max character count'
where batch_id = @batch_id and len(COST_CENTER) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';AREA exceeds max character count'
where batch_id = @batch_id and len(AREA) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';GRADE exceeds max character count'
where batch_id = @batch_id and len(GRADE) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_1 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_1) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_2 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_2) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_3 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_3) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_4 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_4) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_5 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_5) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';USER_FUNCTION exceeds max character count'
where batch_id = @batch_id and len(USER_FUNCTION) > @len_miscData;


/**********
Find any columns that have empty strings ('') and save them as NULL (except for the required columns already checked above)
(needed for all DATE columns, doing it to all columns for consistency)
Empty strings get saved as '1900-01-01' when converting them to dates
*/

update STAGE_ENROLLMENT set PRIMARY_REPORT_PAX_ID = NULL where BATCH_ID = @batch_id and len(PRIMARY_REPORT_PAX_ID) = 0
update STAGE_ENROLLMENT set SECONDARY_REPORT_ID = NULL where BATCH_ID = @batch_id and len(SECONDARY_REPORT_ID) = 0
update STAGE_ENROLLMENT set SECONDARY_REPORT_PAX_ID = NULL where BATCH_ID = @batch_id and len(SECONDARY_REPORT_PAX_ID) = 0
update STAGE_ENROLLMENT set MIDDLE_INITIAL = NULL where BATCH_ID = @batch_id and len(MIDDLE_INITIAL) = 0
update STAGE_ENROLLMENT set TITLE = NULL where BATCH_ID = @batch_id and len(TITLE) = 0
update STAGE_ENROLLMENT set COMPANY_NAME = NULL where BATCH_ID = @batch_id and len(COMPANY_NAME) = 0
update STAGE_ENROLLMENT set PASSWORD = NULL where BATCH_ID = @batch_id and len(PASSWORD) = 0
update STAGE_ENROLLMENT set EMAIL_ADDRESS = NULL where BATCH_ID = @batch_id and len(EMAIL_ADDRESS) = 0
update STAGE_ENROLLMENT set PHONE_NUMBER = NULL where BATCH_ID = @batch_id and len(PHONE_NUMBER) = 0
update STAGE_ENROLLMENT set PHONE_EXTENSION = NULL where BATCH_ID = @batch_id and len(PHONE_EXTENSION) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_2 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_2) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_3 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_3) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_4 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_4) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_5 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_5) = 0
update STAGE_ENROLLMENT set HIRE_DATE = NULL where BATCH_ID = @batch_id and len(HIRE_DATE) = 0
update STAGE_ENROLLMENT set TERMINATION_DATE = NULL where BATCH_ID = @batch_id and len(TERMINATION_DATE) = 0
update STAGE_ENROLLMENT set BIRTH_DATE = NULL where BATCH_ID = @batch_id and len(BIRTH_DATE) = 0
update STAGE_ENROLLMENT set DEPARTMENT = NULL where BATCH_ID = @batch_id and len(DEPARTMENT) = 0
update STAGE_ENROLLMENT set COST_CENTER = NULL where BATCH_ID = @batch_id and len(COST_CENTER) = 0
update STAGE_ENROLLMENT set AREA = NULL where BATCH_ID = @batch_id and len(AREA) = 0
update STAGE_ENROLLMENT set GRADE = NULL where BATCH_ID = @batch_id and len(GRADE) = 0
update STAGE_ENROLLMENT set CUSTOM_1 = NULL where BATCH_ID = @batch_id and len(CUSTOM_1) = 0
update STAGE_ENROLLMENT set CUSTOM_2 = NULL where BATCH_ID = @batch_id and len(CUSTOM_2) = 0
update STAGE_ENROLLMENT set CUSTOM_3 = NULL where BATCH_ID = @batch_id and len(CUSTOM_3) = 0
update STAGE_ENROLLMENT set CUSTOM_4 = NULL where BATCH_ID = @batch_id and len(CUSTOM_4) = 0
update STAGE_ENROLLMENT set CUSTOM_5 = NULL where BATCH_ID = @batch_id and len(CUSTOM_5) = 0
update STAGE_ENROLLMENT set USER_FUNCTION = NULL where BATCH_ID = @batch_id and len(USER_FUNCTION) = 0

/**********
 Find pax_ID, pax_group_id and find out if address, email, phone, sys_user need to be added/updated
 */
update STAGE_ENROLLMENT set PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PARTICIPANT_ID)
where BATCH_ID = @batch_id
and ENROLL_ERROR is null;

update se
set PAX_GROUP_ID = pg.pax_group_id
from STAGE_ENROLLMENT se
join PAX_GROUP pg on pg.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id;

update se
set se.ADDRESS_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join ADDRESS ad on ad.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)
and (select 1 from address a where se.pax_id =  a.pax_id and a.ADDRESS_TYPE_CODE = 'BUS'
and (a.ADDRESS1 <> se.ADDRESS_LINE_1 OR a.ADDRESS2 <> se.ADDRESS_LINE_2 OR a.ADDRESS3 <> se.ADDRESS_LINE_3 OR
a.ADDRESS4 <> se.ADDRESS_LINE_4 OR a.ADDRESS4 <> se.ADDRESS_LINE_4 OR a.CITY <> se.CITY OR
a.STATE <> se.STATE_PROVINCE_REGION OR a.ZIP <> se.ZIP_CODE or a.COUNTRY_CODE <> se.COUNTRY_CODE
))=1

update se
set se.ADDRESS_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)
and se.PAX_ID not in
(SELECT pax_ID from address a where se.pax_id =  a.pax_id and a.ADDRESS_TYPE_CODE = 'BUS')

update se
set se.ADDRESS_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)

update se
set se.EMAIL_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join email em on em.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and em.EMAIL_TYPE_CODE = 'BUS'
and em.EMAIL_ADDRESS <> se.EMAIL_ADDRESS
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null)

update se
set se.EMAIL_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null)
and se.PAX_ID not in
(SELECT pax_ID from EMAIL em where se.pax_id = em.pax_id and em.EMAIL_TYPE_CODE = 'BUS')

update se
set se.EMAIL_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null);

--select * from STAGE_ENROLLMENT

update se
set se.PHONE_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join phone ph on ph.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and ph.PHONE_TYPE_CODE = 'BUS'
and (ph.PHONE <> se.PHONE_NUMBER
    OR ph.PHONE_EXT <> se.PHONE_EXTENSION)


update se
set se.PHONE_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and se.PAX_ID not in
(SELECT pax_ID from PHONE ph where se.pax_id =  ph.pax_id and ph.PHONE_TYPE_CODE = 'BUS')

update se
set se.PHONE_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id;

update se
set se.PAX_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join PAX p on p.pax_id = se.pax_id
where se.PAX_ID is not null
and se.ENROLL_ERROR is null
and (se.PAX_CHANGE_STATUS <> 'E' or se.PAX_CHANGE_STATUS is null)
and BATCH_ID = @batch_id
and (p.FIRST_NAME <> se.FIRST_NAME
    OR p.LAST_NAME <> se.LAST_NAME
    OR p.MIDDLE_INITIAL <> se.MIDDLE_INITIAL
    )

update se
set se.PAX_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and (se.PAX_CHANGE_STATUS <> 'E' or se.PAX_CHANGE_STATUS is null)
and BATCH_ID = @batch_id;

update se
set se.PAX_GROUP_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join PAX_GROUP p on p.pax_id = se.pax_id
where se.PAX_ID is not null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and se.TERMINATION_DATE is not null
and p.thru_date is null

update se
set se.PAX_GROUP_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id;


--Check ssoEnabled flag to determine who gets set to NEW status (This is for new users only)
IF (select UPPER(value_1) from component.application_data where key_name = '/app/project_profile/sso_enabled') = 'TRUE'
BEGIN
    --ssoEnabled is TRUE. Only set users to NEW if they do not have an email (emails on the enrollment file are BUS by default)

    --New Users
    UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'NEW' where STATUS = 'A' and BATCH_ID = @batch_id and PAX_ID is null and EMAIL_ADDRESS is null;

    --Existing Users (need to stay NEW unless they send an email)
    UPDATE se set STATUS_TYPE_CODE = 'NEW'
    from STAGE_ENROLLMENT se
    join sys_user su on su.pax_id = se.pax_id
    where se.PAX_ID is not null
    and se.BATCH_ID = @batch_id
    and se.STATUS = 'A'
    and se.EMAIL_ADDRESS is null
    and su.STATUS_TYPE_CODE = 'NEW';
END
ELSE
BEGIN
    --ssoEnabled is FALSE. All active users who do not have a pax_id yet get set to NEW

    --New Users
    UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'NEW' where STATUS = 'A' and BATCH_ID = @batch_id and PAX_ID is null;

    --Existing Users (need to stay NEW)
    UPDATE se set STATUS_TYPE_CODE = 'NEW'
    from STAGE_ENROLLMENT se
    join sys_user su on su.pax_id = se.pax_id
    where se.PAX_ID is not null
    and se.BATCH_ID = @batch_id
    and se.STATUS = 'A'
    and su.STATUS_TYPE_CODE = 'NEW';
END

UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'ACTIVE' where STATUS = 'A' and BATCH_ID = @batch_id and STATUS_TYPE_CODE is null;
UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'INACTIVE' where STATUS = 'I' and BATCH_ID = @batch_id;

update se
set se.SYS_USER_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join sys_user su on su.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null)
and se.ENROLL_ERROR is null
and (su.SYS_USER_NAME <> se.LOGIN_ID
    OR su.STATUS_TYPE_CODE <> se.STATUS_TYPE_CODE)


update se
set se.SYS_USER_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null)
and se.ENROLL_ERROR is null
and se.PAX_ID not in
(SELECT pax_ID from sys_user su where se.pax_id = su.pax_id)

update se
set se.SYS_USER_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null);

insert into pax (LANGUAGE_CODE, PREFERRED_LOCALE, CONTROL_NUM, FIRST_NAME, MIDDLE_INITIAL, LAST_NAME)
select 'en','en_US', PARTICIPANT_ID, FIRST_NAME, MIDDLE_INITIAL, LAST_NAME from STAGE_ENROLLMENT se
where batch_id = @batch_id and PAX_CHANGE_STATUS = 'A';

--reinsert the pax_id's
update STAGE_ENROLLMENT set PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PARTICIPANT_ID)
where BATCH_ID = @batch_id and PAX_ID is null;

insert into pax_GROUP (PAX_ID, ORGANIZATION_CODE, ROLE_CODE, FROM_DATE)
select PAX_ID, 'RECG','RECG', getdate() from STAGE_ENROLLMENT se
where batch_id = @batch_id and PAX_GROUP_CHANGE_STATUS = 'A';

--insert the new pax_group_ids
update se
set PAX_GROUP_ID = pg.pax_group_id
from STAGE_ENROLLMENT se
join PAX_GROUP pg on pg.pax_id = se.pax_id
where se.PAX_GROUP_ID is null
and BATCH_ID = @batch_id;

--update any pax infromation that needs to be updated
update p
set p.FIRST_NAME = se.FIRST_NAME
    , p.LAST_NAME = se.LAST_NAME
    , p.MIDDLE_INITIAL = se.MIDDLE_INITIAL
from PAX p
JOIN STAGE_ENROLLMENT se on p.pax_id = se.pax_id
where
se.PAX_CHANGE_STATUS = 'U'

update pg
set pg.THRU_DATE = se.TERMINATION_DATE
from PAX_GROUP pg
join STAGE_ENROLLMENT se on pg.pax_group_id = se.pax_group_id
where BATCH_ID = @batch_id
and se.PAX_GROUP_CHANGE_STATUS = 'U'


-- Do the adds and updates for sys_user
INSERT into SYS_USER(PAX_ID, SYS_USER_NAME, SYS_USER_PASSWORD, STATUS_TYPE_CODE)
SELECT PAX_ID, LOGIN_ID, PASSWORD, STATUS_TYPE_CODE
from STAGE_ENROLLMENT se
where BATCH_ID =@batch_id
and se.SYS_USER_CHANGE_STATUS = 'A';

update su
set su.SYS_USER_NAME = se.LOGIN_ID
    ,su.STATUS_TYPE_CODE = se.STATUS_TYPE_CODE
from sys_user su
join STAGE_ENROLLMENT se on su.pax_id = se.pax_id
where BATCH_ID = @batch_id
and se.SYS_USER_CHANGE_STATUS = 'U'

--Do the add and update for address
insert into ADDRESS(pax_id, ADDRESS_TYPE_CODE, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ADDRESS5,
CITY, STATE, ZIP, COUNTRY_CODE, PREFERRED)
select se.PAX_ID, 'BUS', se.ADDRESS_LINE_1, se.ADDRESS_LINE_2, se.ADDRESS_LINE_3, se.ADDRESS_LINE_4, se.ADDRESS_LINE_5,
se.CITY, se.STATE_PROVINCE_REGION, se.ZIP_CODE, se.COUNTRY_CODE, 'Y'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and  se.ADDRESS_CHANGE_STATUS = 'A';

update a
set a.ADDRESS1 = se.ADDRESS_LINE_1, a.ADDRESS2 = se.ADDRESS_LINE_2, a.ADDRESS3 = se.ADDRESS_LINE_3,
a.ADDRESS4 = se.ADDRESS_LINE_4, a.ADDRESS5 = se.ADDRESS_LINE_5, a.CITY = se.CITY,
a.STATE = se.STATE_PROVINCE_REGION, a.ZIP = se.ZIP_CODE, a.COUNTRY_CODE = se.COUNTRY_CODE
from ADDRESS a
join STAGE_ENROLLMENT se on a.pax_id = se.pax_id
where se.ADDRESS_CHANGE_STATUS = 'U'
and a.ADDRESS_TYPE_CODE = 'BUS'
and BATCH_ID = @batch_id

--Do the Add and update for email
insert into email(PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS,PREFERRED)
select se.PAX_ID, 'BUS', se.EMAIL_ADDRESS, 'Y'
from STAGE_ENROLLMENT se
where BATCH_ID = @batch_id
and se.EMAIL_CHANGE_STATUS = 'A'
and se.EMAIL_ADDRESS is not null;

update em
set em.EMAIL_ADDRESS = se.EMAIL_ADDRESS
from email em
join STAGE_ENROLLMENT se on em.pax_id = se.pax_id
where BATCH_ID = @batch_id
and em.EMAIL_TYPE_CODE = 'BUS'
and se.EMAIL_CHANGE_STATUS = 'U';

--Do the add and update for phone
insert into phone(PAX_ID, PHONE_TYPE_CODE, PHONE, PHONE_EXT, PREFERRED)
select se.PAX_ID, 'BUS', se.PHONE_NUMBER, se.PHONE_EXTENSION, 'Y'
from STAGE_ENROLLMENT se
where se.PHONE_CHANGE_STATUS = 'A'
and se.PHONE_NUMBER is not null
and BATCH_ID = @batch_id

update ph
set ph.PHONE = se.PHONE_NUMBER
    ,ph.PHONE_EXT = se.PHONE_EXTENSION
from phone ph
join STAGE_ENROLLMENT se
on ph.pax_id = se.pax_id
where se.PHONE_CHANGE_STATUS = 'U'
and BATCH_ID = @batch_id
and ph.PHONE_TYPE_CODE = 'BUS'

--update/insert/delete all the misc column data
/* PAX_MISC - BIRTH_DATE, HIRE_DATE */

--BIRTH_DAY
delete PAX_MISC
where VF_NAME = 'BIRTH_DAY'
and pax_id in (select pax_id from STAGE_ENROLLMENT
where batch_id = @batch_id and BIRTH_DATE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.BIRTH_DATE
from PAX_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_ID = se.PAX_ID
and pm.VF_NAME = 'BIRTH_DAY'
where batch_id = @batch_id
and BIRTH_DATE is not null
and ENROLL_ERROR is null

insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'BIRTH_DAY', BIRTH_DATE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and BIRTH_DATE is not null
and ENROLL_ERROR is null
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'BIRTH_DAY')


--HIRE_DATE
delete PAX_MISC
where VF_NAME = 'HIRE_DATE'
and pax_id in (select pax_id from STAGE_ENROLLMENT
where batch_id = @batch_id and HIRE_DATE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.HIRE_DATE
from PAX_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_ID = se.PAX_ID
and pm.VF_NAME = 'HIRE_DATE'
where batch_id = @batch_id
and HIRE_DATE is not null
and ENROLL_ERROR is null

insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'HIRE_DATE', HIRE_DATE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and HIRE_DATE is not null
and ENROLL_ERROR is null
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'HIRE_DATE')


--For new users, set up their privacy/recognition preferences to default to TRUE
/* SHARE_SA, SHARE_REC, RECEIVE_EMAIL_REC, VERBAL_REC_1, VERBAL_REC_2, VERBAL_REC_3, WRITTEN_REC_1, WRITTEN_REC_2 */

--SHARE_SA (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'SHARE_SA', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'SHARE_SA')

--SHARE_REC (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'SHARE_REC', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'SHARE_REC')

--RECEIVE_EMAIL_REC (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'RECEIVE_EMAIL_REC', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'RECEIVE_EMAIL_REC')

--VERBAL_REC_1 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_1', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_1')

--VERBAL_REC_2 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_2', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_2')

--VERBAL_REC_3 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_3', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_3')

--WRITTEN_REC_1 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'WRITTEN_REC_1', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'WRITTEN_REC_1')

--WRITTEN_REC_2 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'WRITTEN_REC_2', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'WRITTEN_REC_2')



/* PAX_GROUP_MISC - JOB_TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, AREA, GRADE, FUNCTION,
CUSTOM 1-5*/
delete PAX_GROUP_MISC
where VF_NAME = 'JOB_TITLE'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and TITLE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.TITLE
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'JOB_TITLE'
where batch_id = @batch_id
and TITLE is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'JOB_TITLE', TITLE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and TITLE is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'JOB_TITLE')

delete PAX_GROUP_MISC
where VF_NAME = 'COMPANY_NAME'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and COMPANY_NAME is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.COMPANY_NAME
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'COMPANY_NAME'
where batch_id = @batch_id
and COMPANY_NAME is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'COMPANY_NAME', COMPANY_NAME, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and COMPANY_NAME is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'COMPANY_NAME')

--DEPARTMENT
delete PAX_GROUP_MISC
where VF_NAME = 'DEPARTMENT'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and DEPARTMENT is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.DEPARTMENT
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'DEPARTMENT'
where batch_id = @batch_id
and DEPARTMENT is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'DEPARTMENT', DEPARTMENT, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and DEPARTMENT is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'DEPARTMENT')

--COST_CENTER
delete PAX_GROUP_MISC
where VF_NAME = 'COST_CENTER'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and COST_CENTER is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.COST_CENTER
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'COST_CENTER'
where batch_id = @batch_id
and COST_CENTER is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'COST_CENTER', COST_CENTER, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and COST_CENTER is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'COST_CENTER')

--AREA
delete PAX_GROUP_MISC
where VF_NAME = 'AREA'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and AREA is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.AREA
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'AREA'
where batch_id = @batch_id
and AREA is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'AREA', AREA, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and AREA is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'AREA')

--GRADE
delete PAX_GROUP_MISC
where VF_NAME = 'GRADE'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and GRADE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.GRADE
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'GRADE'
where batch_id = @batch_id
and GRADE is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'GRADE', GRADE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and GRADE is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'GRADE')

--FUNCTION
delete PAX_GROUP_MISC
where VF_NAME = 'FUNCTION'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and [USER_FUNCTION] is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.USER_FUNCTION
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'FUNCTION'
where batch_id = @batch_id
and [USER_FUNCTION] is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'FUNCTION', [USER_FUNCTION], getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and [USER_FUNCTION] is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'FUNCTION')

--CUSTOM_1
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_1'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_1 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_1
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_1'
where batch_id = @batch_id
and CUSTOM_1 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_1', CUSTOM_1, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_1 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_1')

--CUSTOM_2
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_2'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_2 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_2
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_2'
where batch_id = @batch_id
and CUSTOM_2 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_2', CUSTOM_2, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_2 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_2')

--CUSTOM_3
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_3'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_3 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_3
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_3'
where batch_id = @batch_id
and CUSTOM_3 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_3', CUSTOM_3, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_3 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_3')

--CUSTOM_4
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_4'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_4 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_4
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_4'
where batch_id = @batch_id
and CUSTOM_4 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_4', CUSTOM_4, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_4 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_4')

--CUSTOM_5
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_5'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_5 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_5
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_5'
where batch_id = @batch_id
and CUSTOM_5 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_5', CUSTOM_5, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_5 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_5')

--Determine the PAX_ID's for the REPORT_TO and SECONDARY_REPORT_TO
update STAGE_ENROLLMENT set PRIMARY_REPORT_PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PRIMARY_REPORT_ID)
where BATCH_ID = @batch_id;

update STAGE_ENROLLMENT set SECONDARY_REPORT_PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = SECONDARY_REPORT_ID)
where BATCH_ID = @batch_id;


--save relationships (manager and secondary manager)
update r
set r.pax_id_2 = se.PRIMARY_REPORT_PAX_ID
from RELATIONSHIP r
join STAGE_ENROLLMENT se
on se.pax_id = r.pax_id_1 and r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null

insert into RELATIONSHIP(PAX_ID_1,PAX_ID_2,RELATIONSHIP_TYPE_CODE)
SELECT se.PAX_ID, se.PRIMARY_REPORT_PAX_ID, 'REPORT_TO'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.pax_id not in
(select pax_id_1 from RELATIONSHIP where RELATIONSHIP_TYPE_CODE = 'REPORT_TO')

update r
set r.pax_id_2 = se.SECONDARY_REPORT_PAX_ID
from RELATIONSHIP r
join STAGE_ENROLLMENT se
on se.pax_id = r.pax_id_1 and r.RELATIONSHIP_TYPE_CODE = 'SECONDARY_REPORT'
where batch_id = @batch_id
and se.SECONDARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null

insert into RELATIONSHIP(PAX_ID_1,PAX_ID_2,RELATIONSHIP_TYPE_CODE)
SELECT se.PAX_ID, se.SECONDARY_REPORT_PAX_ID, 'SECONDARY_REPORT'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.SECONDARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.pax_id not in
(select pax_id_1 from RELATIONSHIP where RELATIONSHIP_TYPE_CODE = 'SECONDARY_REPORT')


--Insert people with direct reports into the 'Managers' group
DECLARE @managerGroupId BIGINT
SELECT @managerGroupId = GROUP_ID from component.groups where group_desc = 'Managers' and GROUP_TYPE_CODE = 'ROLE_BASED'

insert into GROUPS_PAX(GROUP_ID, PAX_ID)
SELECT DISTINCT @managerGroupId, se.PRIMARY_REPORT_PAX_ID
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.PRIMARY_REPORT_PAX_ID not in
(select pax_id from groups_pax
where group_id = @managerGroupId)


--Get a list of all pax_ids that are currently managers
CREATE TABLE #managerIds (
    PAX_ID BIGINT NOT NULL
)

INSERT INTO #managerIds
SELECT DISTINCT PAX_ID_2 FROM component.RELATIONSHIP WHERE RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND PAX_ID_2 IS NOT NULL


--Remove people from the 'Manager' group if they no longer have any direct reports
DELETE FROM component.GROUPS_PAX WHERE GROUP_ID = @managerGroupId AND PAX_ID IN (
SELECT PAX_ID FROM component.PAX where PAX_ID NOT IN (SELECT * FROM #managerIds))


--Insert anyone that isn't a manager into the 'Non-Managers' group
DECLARE @nonManagerGroupId BIGINT
SELECT @nonManagerGroupId = GROUP_ID from component.groups where group_desc = 'Non-Managers' and GROUP_TYPE_CODE = 'ROLE_BASED'

INSERT INTO GROUPS_PAX (GROUP_ID, PAX_ID)
SELECT @nonManagerGroupId, PAX_ID
FROM component.PAX
WHERE PAX_ID NOT IN (SELECT * FROM #managerIds)
AND PAX_ID NOT IN
(SELECT PAX_ID FROM component.GROUPS_PAX
WHERE GROUP_ID = @nonManagerGroupId)


--Insert new PAX into the 'ALL' group
DECLARE @allGroupId BIGINT
SELECT @allGroupId = GROUP_ID from component.groups where group_desc = 'Participants'

insert into GROUPS_PAX(GROUP_ID, PAX_ID)
SELECT @allGroupId, se.PAX_ID
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PAX_ID is not null
and se.PAX_CHANGE_STATUS = 'A'



--Remove the leading semi-colon from any row with an error
Update STAGE_ENROLLMENT set ENROLL_ERROR = SUBSTRING(ENROLL_ERROR,2,len(ENROLL_ERROR)-1)
where ENROLL_ERROR is not NULL and @batch_id = BATCH_ID;

--CREATE BATCH_EVENT RECORDS for RECS, OKRECS, ERRS
INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select BATCH_ID, 'RECS', 'SUCCESS', count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id group by BATCH_ID;

DECLARE @okrecs INTEGER
DECLARE @errs INTEGER
SELECT @okrecs = count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id and ENROLL_ERROR is NULL
SELECT @errs = count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id and ENROLL_ERROR is not NULL

INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select DISTINCT BATCH_ID, 'OKRECS', 'SUCCESS', @okrecs from STAGE_ENROLLMENT where BATCH_ID = @batch_id;

INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select DISTINCT BATCH_ID, 'ERRS', 'SUCCESS', @errs from STAGE_ENROLLMENT where BATCH_ID = @batch_id;


--CREATE BATCH_FILE Record for the error file
--SELECT * from BATCH_FILE where batch_id = 77;
create table #errList (
BATCH_ID bigint,
ERROR_DATA nvarchar(2000)
);

insert into #errList(BATCH_ID,ERROR_DATA)
select BATCH_ID, '"'+isnull([STATUS],' ')+ '","'+isnull(PARTICIPANT_ID,' ')+ '","'+isnull(PRIMARY_REPORT_ID,' ')
+'","'+isnull(SECONDARY_REPORT_ID,' ')+
 '","'+isnull(FIRST_NAME,' ')+'","'+isnull(LAST_NAME,' ')+'","'+isnull(MIDDLE_INITIAL,' ')+'","'+isnull(TITLE,' ')+
 '","'+isnull(COMPANY_NAME,' ')+'","'+isnull(LOGIN_ID,' ')+'","'+isnull(PASSWORD,' ')+'","'+isnull(EMAIL_ADDRESS,' ')+
 '","'+isnull(PHONE_NUMBER,' ')+'","'+isnull(PHONE_EXTENSION,' ')+'","'+isnull(ADDRESS_LINE_1,' ')+'","'+isnull(ADDRESS_LINE_2,' ')+
 '","'+isnull(ADDRESS_LINE_3,' ')+'","'+isnull(ADDRESS_LINE_4,' ')+'","'+isnull(ADDRESS_LINE_5,' ')+'","'+isnull(CITY,' ')+
 '","'+isnull(STATE_PROVINCE_REGION,' ')+'","'+isnull(ZIP_CODE,' ')+'","'+isnull(COUNTRY_CODE,' ')+'","'+isnull(HIRE_DATE,' ')+
 '","'+isnull(TERMINATION_DATE,' ')+'","'+isnull(BIRTH_DATE,' ')+'","'+isnull(DEPARTMENT,' ')+'","'+isnull(COST_CENTER,' ')+
 '","'+isnull(AREA,' ')+'","'+isnull(GRADE,' ')+'","'+isnull(USER_FUNCTION,' ')+'","'+isnull(CUSTOM_1,' ')+
 '","'+isnull(CUSTOM_2,' ')+'","'+isnull(CUSTOM_3,' ')+'","'+isnull(CUSTOM_4,' ')+'","'+isnull(CUSTOM_5,' ')+
 '","'+ENROLL_ERROR+
 '"' from STAGE_ENROLLMENT
 where batch_id = @batch_id
 and ENROLL_ERROR is not null;

DECLARE @EROXR NVARCHAR(MAX)
SELECT @EROXR = COALESCE(@EROXR + CHAR(13)+CHAR(10), '') + ERROR_DATA
FROM #errList

--Add Column Headers to the error file
DECLARE @columnHeaders NVARCHAR(MAX)
SET @columnHeaders = '"Participant Status","Participant Id","Primary Report To ID","Secondary Report To ID","First Name","Last Name","Middle Name","Title","Company Name","Login Id","Password","Email Address","Phone Number","Phone Extension","Primary Address Line 1","Address Line 2","Address Line 3","Address Line 4","Address Line 5","City","State/Province/Region","Zip/Postal Code","Country Code","Hire Date","Termination Date","Birthdate","Department","Cost Center","Area","Grade","Function","Custom1","Custom2","Custom3","Custom4","Custom5","Error Message"'
SELECT @EROXR = COALESCE(@columnHeaders + CHAR(13)+CHAR(10), '') + @EROXR

--Save the errors to BATCH_FILE
update BATCH_FILE SET FILE_UPLOAD_ERROR = @EROXR WHERE BATCH_ID = @batch_id;

END















GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_MARS_ENROLLMENT_UPDATE]'
GO
-- =============================================
-- Author:        EGRE
-- Create date: 2015/08/21
-- Description:    created
-- =============================================
CREATE PROCEDURE [component].[UP_MARS_ENROLLMENT_UPDATE]
AS
BEGIN
    SET NOCOUNT ON;

    declare @lastRanDate datetime
    select @lastRanDate = isnull(max(create_date), '1/1/2000') from batch where batch_type_code = 'ABSPRF'

    select
        case
            when p.control_num like 'ACD%'
            then p.control_num
            else isnull(pm_pid.misc_data, 'PAX:' + cast(p.pax_id as nvarchar))
        end as PARTICIPANT_ID
        ,case dt_pay.payout_type_code
            when 'ABS4' then '004'      --aeis
            when 'ABS5' then '004'      --aeis
            when 'ABS6' then '002'      --aeis
            when 'ABS7' then '003'      --aeis ca?
            else '002'                  --abs
        end as CARD_TYPE
        ,dt_pay.project_number as PROJECT_NUMBER
        ,dt_pay.sub_project_number as SUB_PROJECT_NUMBER
        ,dt_pay.card_num as ACCOUNT_NUMBER
        ,p.first_name as FIRST_NAME
        ,p.last_name as LAST_NAME
        ,p.middle_initial as MIDDLE_INITIAL
        ,upper(substring(p.name_suffix,1,5)) as SUFFIX
        ,case when he.preferred = 'Y' then he.email_address
            when be.preferred = 'Y' then be.email_address
            when be.pax_id is not null then be.email_address
            else he.email_address
        end as EMAIL_ADDRESS
        ,ha.ADDRESS_ID as HOM_ADDRESS_ID
        ,ha.address_type_code as HOM_ADDRESS_TYPE
        ,ha.address1 as HOM_ADDRESS_1
        ,ha.address2 as HOM_ADDRESS_2
        ,ha.address3 as HOM_ADDRESS_3
        ,ha.address4 as HOM_ADDRESS_4
        ,ha.address5 as HOM_ADDRESS_5
        ,ha.address6 as HOM_ADDRESS_6
        ,ha.city as HOM_CITY
        ,ha.country_code as HOM_COUNTRY_CODE
        ,ha.zip as HOM_POSTAL_CODE
        ,upper(ha.state) as HOM_STATE_PROVINCE
        ,ba.ADDRESS_ID as BUS_ADDRESS_ID
        ,ba.address_type_code as BUS_ADDRESS_TYPE
        ,ba.address1 as BUS_ADDRESS_1
        ,ba.address2 as BUS_ADDRESS_2
        ,ba.address3 as BUS_ADDRESS_3
        ,ba.address4 as BUS_ADDRESS_4
        ,ba.address5 as BUS_ADDRESS_5
        ,ba.address6 as BUS_ADDRESS_6
        ,ba.city as BUS_CITY
        ,ba.country_code as BUS_COUNTRY_CODE
        ,ba.zip as BUS_POSTAL_CODE
        ,upper(ba.state) as BUS_STATE_PROVINCE
        ,hp.phone_id as HOM_PHONE_ID
        ,hp.phone as HOM_PHONE_NUMBER
        ,hp.phone_ext as HOM_PHONE_EXT
        ,hp.phone_type_code as HOM_PHONE_TYPE
        ,fp.phone_id as FAX_PHONE_ID
        ,fp.phone as FAX_PHONE_NUMBER
        ,fp.phone_ext as FAX_PHONE_EXT
        ,fp.phone_type_code as FAX_PHONE_TYPE
        ,bp.phone_id as BUS_PHONE_ID
        ,bp.phone as BUS_PHONE_NUMBER
        ,bp.phone_ext as BUS_PHONE_EXT
        ,bp.phone_type_code as BUS_PHONE_TYPE
        ,p.UPDATE_DATE
    from pax p
    inner join (
        select    pay.pax_id ,pa.card_num ,pa.card_type_code as payout_type_code
                ,pay.project_number ,max(pay.sub_project_number) as sub_project_number
                ,case when max(pay.create_date) > @lastRanDate
                    then max(pay.create_date) else @lastRanDate
                end as payout_date
        from payout pay
        inner join pax_account pa on pa.pax_id = pay.pax_id and pa.card_type_code = pay.payout_type_code
        where pay.status = 'I'
        group by pay.pax_id, pa.card_num, pa.card_type_code, pay.project_number
    ) as dt_pay on dt_pay.pax_id = p.pax_id
    left join address ha on ha.pax_id = p.pax_id and ha.address_type_code = 'HOM'
    left join address ba on ba.pax_id = p.pax_id and ba.address_type_code = 'BUS'
    left join phone hp on hp.pax_id = p.pax_id and hp.phone_type_code = 'HOM'
    left join phone fp on fp.pax_id = p.pax_id and fp.phone_type_code = 'BFX'
    left join phone bp on bp.pax_id = p.pax_id and bp.phone_type_code = 'BUS'
    left join email he on he.pax_id = p.pax_id and he.email_type_code = 'HOM'
    left join email be on be.pax_id = p.pax_id and be.email_type_code = 'BUS'
    left join pax_misc pm_pid on (pm_pid.pax_id = p.pax_id
          and pm_pid.vf_name = 'ABS_PID' and pm_pid.thru_date is null)
    where p.update_date > dt_pay.payout_date
    or ha.update_date > dt_pay.payout_date
    or ba.update_date > dt_pay.payout_date
    or he.update_date > dt_pay.payout_date
    or be.update_date > dt_pay.payout_date
    or hp.update_date > dt_pay.payout_date
    or fp.update_date > dt_pay.payout_date
    or bp.update_date > dt_pay.payout_date

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_APPROVAL_PROCESS_CONFIG]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_APPROVAL_PROCESS_CONFIG] ADD
[APPROVAL_LEVEL] [int] NULL,
[APPROVAL_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[APPROVER_PAX_ID] [bigint] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_APPROVAL_PROCESS_CONFIG] DROP
COLUMN [CONFIG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].HISTORY_APPROVAL_PROCESS_CONFIG.PROGRAM_ACTIVITY_ID', N'TARGET_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[COMMENT]'
GO
CREATE TABLE [component].[COMMENT]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TARGET_TABLE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TARGET_ID] [bigint] NOT NULL,
[PAX_ID] [bigint] NOT NULL,
[COMMENT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_COMMENT_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_COMMENT_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_COMMENT_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_COMMENT_UPDATE_ID] DEFAULT (user_name()),
[STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[COMMENT_DATE] [datetime2] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_COMMENT] on [component].[COMMENT]'
GO
ALTER TABLE [component].[COMMENT] ADD CONSTRAINT [PK_COMMENT] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_NEWSFEED_INFO]'
GO
-- ==============================  NAME  ======================================
-- UP_NEWSFEED_INFO
-- ===========================  DESCRIPTION  ==================================
--
-- RETURN A RESULT SET OF NEWSFEED INFORMATION
-- PARAMETERS:
--   @type - SELF, PUBLIC, MANAGER, or DEFAULT. Determines which newsfeed items to display
--   @activityType - Corresponds to NEWSFEED_ITEM_TYPE
--   @paxId - The paxId to be used for SELF newsfeed items (logged in user)
--   @row_min - Used for pagination
--   @row_max - Used for pagination
--
--    SELF FEEDS = My recognitions given and received. Ignores privacy settings
--    PUBLIC FEEDS = All feeds that I am allowed to see (check visibility settings), minus my self-feeds
--  MANAGER FEED = A subset of PUBLIC feed, that includes only recognitions given to my direct reports (ignore visibility settings)
--    DEFAULT FEED = SELF feeds plus PUBLIC feeds
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- HARWELLM        20150603                CREATED
-- HARWELLM        20150625                ADDED LOGIC TO CALCULATE DIRECT_REPORT_COUNT
--                                        ALSO ADDED SUPPORT FOR 'MANAGER' activityType
-- MADRIDO        20150702                CHANGE TO_PAX_ID  FOR SELF ACTIVITIES,
--                                        USING RECEIVER PAX ID INSTEAD OF PAX LOGGED
-- HARWELLM        20150729                ADDED FIX FOR MANAGER LOGIC WHEN ALL VISIBILITIES ARE FALSE
-- MADRIDO        20150812                ADDED LIKE DETAILS
-- KUSEYA       20150820                ADDED COMMENT DETAILS
-- DAGARFIN       20150911                  ADDED CHECKS TO RECOGNITIONS FOR RAISES
-- DMOORE       20150911                 ADDED RAISE DETAILS
-- WRIGHTM        20150910                ADDED MY COMMENT COUNT
-- DMOORE         20150921                 CORRECT RAISED PAX TO GRAB NEWSFEED CORRECT RAISED PAX
-- KUSEYA       20150918                UPDATED COMMENT DETAILS TO ONLY COUNT AND RETRIEVE ACTIVE COMMENTS
-- KILINSKIS      20150924                ADDED 'RECEIVED' TYPE SUPPORT
-- DMOORE         20150929                 UPDATE RAISE COUNTS
-- DAGARFIN       20151005                  ADDED PARENT ID CHECKS FOR NOMINATIONS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_NEWSFEED_INFO]
@type NVARCHAR(8),
@activityType NVARCHAR(50),
@paxId BIGINT,
@loggedInPaxID BIGINT,
@row_min BIGINT,
@row_max BIGINT

AS
BEGIN

SET NOCOUNT ON

--DRIVER
CREATE TABLE #NEWSFEED_INFO (
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    TYPE NVARCHAR(7) NOT NULL,
    ACTIVITY_TYPE NVARCHAR(50) NOT NULL,
    RECIPIENT_COUNT INTEGER NULL,
    DIRECT_REPORT_COUNT INTEGER NULL,
    POINTS_ISSUED DECIMAL(10 ,2) NULL,
    CREATE_DATE DATETIME2(7) NOT NULL,
    NOMINATION_ID BIGINT NOT NULL,
    PROGRAM_ID BIGINT NOT NULL,
    PROGRAM_NAME NVARCHAR(100) NOT NULL,
    ECARD_ID BIGINT NULL,
    HEADLINE NVARCHAR(140) NULL,
    FROM_PAX_ID BIGINT NOT NULL,
    FROM_PAX_LANGUAGE_CODE NVARCHAR(3) NOT NULL,
    FROM_PAX_PREFERRED_LOCALE NVARCHAR(6) NOT NULL,
    FROM_PAX_CONTROL_NUM NVARCHAR(255) NOT NULL,
    FROM_PAX_FIRST_NAME NVARCHAR(30) NULL,
    FROM_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    FROM_PAX_LAST_NAME NVARCHAR(40) NULL,
    FROM_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    FROM_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    FROM_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    FROM_PAX_IMAGES_ID BIGINT NULL,
    FROM_PAX_IMAGES_VERSION INT NULL,
    FROM_PAX_STATUS NVARCHAR(20) NULL,
    FROM_PAX_JOB_TITLE NVARCHAR(255) NULL,
    TO_PAX_ID BIGINT NULL,
    TO_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    TO_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    TO_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    TO_PAX_FIRST_NAME NVARCHAR(30) NULL,
    TO_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    TO_PAX_LAST_NAME NVARCHAR(40) NULL,
    TO_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    TO_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    TO_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    TO_PAX_IMAGES_ID BIGINT NULL,
    TO_PAX_IMAGES_VERSION INT NULL,
    TO_PAX_STATUS NVARCHAR(20) NULL,
    TO_PAX_JOB_TITLE NVARCHAR(255) NULL,
    RECOGNITION_CRITERIA_JSON NVARCHAR(MAX) NULL,
    LIKE_COUNT BIGINT NULL,
    LIKE_PAX_ID BIGINT NULL,
    LIKE_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    LIKE_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    LIKE_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    LIKE_PAX_FIRST_NAME NVARCHAR(30) NULL,
    LIKE_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    LIKE_PAX_LAST_NAME NVARCHAR(40) NULL,
    LIKE_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    LIKE_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    LIKE_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    LIKE_PAX_IMAGES_ID BIGINT NULL,
    LIKE_PAX_IMAGES_VERSION INT NULL,
    LIKE_PAX_STATUS NVARCHAR(20) NULL,
    LIKE_PAX_JOB_TITLE NVARCHAR(255) NULL,
    COMMENT_COUNT BIGINT NULL,
    MY_COMMENT_COUNT BIGINT NULL,
    COMMENT_ID BIGINT NULL,
    COMMENT_DATE DATETIME2(7) NULL,
    COMMENT_TEXT NVARCHAR(255) NULL,
    COMMENT_PAX_ID BIGINT NULL,
    COMMENT_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    COMMENT_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    COMMENT_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    COMMENT_PAX_FIRST_NAME NVARCHAR(30) NULL,
    COMMENT_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    COMMENT_PAX_LAST_NAME NVARCHAR(40) NULL,
    COMMENT_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    COMMENT_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    COMMENT_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    COMMENT_PAX_IMAGES_ID BIGINT NULL,
    COMMENT_PAX_IMAGES_VERSION INT NULL,
    COMMENT_PAX_STATUS NVARCHAR(20) NULL,
    COMMENT_PAX_JOB_TITLE NVARCHAR(255) NULL,
  MY_RAISED_COUNT BIGINT NULL,
    RAISED_COUNT BIGINT NULL,
    RAISED_POINT_TOTAL DECIMAL(10 ,2) NULL,
  RAISED_PAX_ID BIGINT NULL,
  RAISED_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
  RAISED_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
  RAISED_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    RAISED_PAX_FIRST_NAME NVARCHAR(30) NULL,
    RAISED_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    RAISED_PAX_LAST_NAME NVARCHAR(40) NULL,
    RAISED_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    RAISED_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    RAISED_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    RAISED_PAX_STATUS NVARCHAR(20) NULL,
    RAISED_PAX_IMAGES_VERSION INT NULL,
    RAISED_PAX_JOB_TITLE NVARCHAR(255) NULL,
    GIVEN_RAISE BIT NULL
)

CREATE TABLE #SELF_FEEDS (
    NEWSFEED_ITEM_ID BIGINT NOT NULL
)

-- ****** Step 1: Get SELF FEEDS. Will need to use these in some way for every TYPE ******
IF @type <> 'RECEIVED'
  INSERT INTO #SELF_FEEDS
  SELECT ni.ID FROM component.NEWSFEED_ITEM ni
  JOIN
  (
    --RECOGNITIONS GIVEN
    SELECT n.* FROM component.NOMINATION n
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check NEW
    WHERE n.STATUS_TYPE_CODE = 'APPROVED' AND n.SUBMITTER_PAX_ID = @paxId
    UNION
    --RECOGNITIONS RECEIVED
    SELECT n.* FROM component.RECOGNITION r
    JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL-- Parent Id check
    WHERE r.STATUS_TYPE_CODE = 'APPROVED' AND r.RECEIVER_PAX_ID = @paxId
  ) NOMINATIONS
  ON ni.FK1 = NOMINATIONS.ID
ELSE
  INSERT INTO #SELF_FEEDS
  SELECT ni.ID FROM component.NEWSFEED_ITEM ni
  JOIN
  (
    --RECOGNITIONS RECEIVED
    SELECT n.* FROM component.RECOGNITION r
    JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
    WHERE r.STATUS_TYPE_CODE = 'APPROVED' AND r.RECEIVER_PAX_ID = @paxId
  ) NOMINATIONS
  ON ni.FK1 = NOMINATIONS.ID


-- ****** Step 2: Add all SELF feeds to #NEWSFEED_INFO ******
INSERT INTO #NEWSFEED_INFO (
    NEWSFEED_ITEM_ID,
    TYPE,
    ACTIVITY_TYPE,
    CREATE_DATE,
    NOMINATION_ID,
    PROGRAM_ID,
    PROGRAM_NAME,
    ECARD_ID,
    HEADLINE,
    FROM_PAX_ID,
    FROM_PAX_LANGUAGE_CODE,
    FROM_PAX_PREFERRED_LOCALE,
    FROM_PAX_CONTROL_NUM,
    FROM_PAX_FIRST_NAME,
    FROM_PAX_MIDDLE_INITIAL,
    FROM_PAX_LAST_NAME,
    FROM_PAX_NAME_PREFIX,
    FROM_PAX_NAME_SUFFIX,
    FROM_PAX_COMPANY_NAME,
    FROM_PAX_STATUS,
    FROM_PAX_JOB_TITLE
)
SELECT ni.ID, 'SELF' AS 'TYPE', ni.NEWSFEED_ITEM_TYPE_CODE, ni.CREATE_DATE, n.ID as 'NOMINATION_ID', pa.PROGRAM_ACTIVITY_ID, pa.PROGRAM_ACTIVITY_NAME, n.ECARD_ID, n.HEADLINE_COMMENT,
from_pax.PAX_ID AS 'FROM_PAX_ID', from_pax.LANGUAGE_CODE AS 'FROM_LANGUAGE_CODE', from_pax.PREFERRED_LOCALE AS 'FROM_PREFERRED_LOCALE', from_pax.CONTROL_NUM AS 'FROM_CONTROL_NUM',
from_pax.FIRST_NAME AS 'FROM_FIRST_NAME', from_pax.MIDDLE_INITIAL AS 'FROM_MIDDLE_INITIAL', from_pax.LAST_NAME AS 'FROM_LAST_NAME',
from_pax.NAME_PREFIX AS 'FROM_NAME_PREFIX', from_pax.NAME_SUFFIX AS 'FROM_NAME_SUFFIX', from_pax.COMPANY_NAME AS 'FROM_COMPANY_NAME',
from_su.STATUS_TYPE_CODE AS 'FROM_STATUS', from_pgm.MISC_DATA AS 'FROM_JOB_TITLE'
FROM component.NEWSFEED_ITEM ni
JOIN component.NOMINATION n ON n.ID = ni.FK1 AND n.PARENT_ID IS NULL
JOIN component.PROGRAM_ACTIVITY pa ON pa.PROGRAM_ACTIVITY_ID = n.PROGRAM_ACTIVITY_ID
--FROM_PAX INFO
JOIN component.PAX from_pax ON from_pax.PAX_ID = n.SUBMITTER_PAX_ID
JOIN component.SYS_USER from_su ON from_su.PAX_ID = from_pax.PAX_ID
JOIN component.PAX_GROUP from_pg ON from_pg.PAX_ID = from_pax.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC from_pgm ON from_pgm.PAX_GROUP_ID = from_pg.PAX_GROUP_ID AND from_pgm.VF_NAME = 'JOB_TITLE'
WHERE ni.NEWSFEED_ITEM_TYPE_CODE = @activityType AND ni.ID IN (SELECT * FROM #SELF_FEEDS)


--Populate toPax (the recipient needs to be @paxId for SELF feeds)
CREATE TABLE #MY_RECEIVED_FEED (
    NEWSFEED_ITEM_ID BIGINT NOT NULL
)

INSERT INTO #MY_RECEIVED_FEED
SELECT NEWSFEED_ITEM_ID FROM #NEWSFEED_INFO
WHERE FROM_PAX_ID <> @paxId

--Update toPax for recognitions RECEIVED
UPDATE #NEWSFEED_INFO SET
    TO_PAX_ID = p.PAX_ID,
    TO_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = p.CONTROL_NUM,
    TO_PAX_FIRST_NAME = p.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = p.LAST_NAME,
    TO_PAX_NAME_PREFIX = p.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = p.COMPANY_NAME,
    TO_PAX_STATUS = su.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO ni
JOIN #MY_RECEIVED_FEED mrf ON mrf.NEWSFEED_ITEM_ID = ni.NEWSFEED_ITEM_ID
JOIN component.PAX p ON p.PAX_ID = @paxId
JOIN component.SYS_USER su ON su.PAX_ID = @paxId
JOIN component.PAX_GROUP pg ON pg.PAX_ID = @paxId
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'


--Update toPax for recognitions GIVEN
UPDATE #NEWSFEED_INFO SET
    TO_PAX_ID = p.PAX_ID,
    TO_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = p.CONTROL_NUM,
    TO_PAX_FIRST_NAME = p.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = p.LAST_NAME,
    TO_PAX_NAME_PREFIX = p.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = p.COMPANY_NAME,
    TO_PAX_STATUS = su.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO
JOIN component.NEWSFEED_ITEM ni ON ni.ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID
JOIN component.PAX p ON p.PAX_ID = ni.TO_PAX_ID
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'
WHERE ni.ID NOT IN (SELECT * FROM #MY_RECEIVED_FEED)


--Update toPax for recognitions GIVEN to requester pax
UPDATE #NEWSFEED_INFO SET
  TO_PAX_ID = p.PAX_ID,
  TO_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
  TO_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
  TO_PAX_CONTROL_NUM = p.CONTROL_NUM,
  TO_PAX_FIRST_NAME = p.FIRST_NAME,
  TO_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
  TO_PAX_LAST_NAME = p.LAST_NAME,
  TO_PAX_NAME_PREFIX = p.NAME_PREFIX,
  TO_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
  TO_PAX_COMPANY_NAME = p.COMPANY_NAME,
  TO_PAX_STATUS = su.STATUS_TYPE_CODE,
  TO_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO
  JOIN component.NEWSFEED_ITEM ni ON ni.ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID
  JOIN component.RECOGNITION r ON r.NOMINATION_ID = ni.FK1 AND r.PARENT_ID IS NULL
  JOIN component.PAX p ON p.PAX_ID = r.RECEIVER_PAX_ID
  JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
  JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
  LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'
WHERE r.RECEIVER_PAX_ID = @loggedInPaxId


--Update any toPax that have visibility = FALSE
UPDATE #NEWSFEED_INFO SET
    TO_PAX_ID = p.PAX_ID,
    TO_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = p.CONTROL_NUM,
    TO_PAX_FIRST_NAME = p.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = p.LAST_NAME,
    TO_PAX_NAME_PREFIX = p.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = p.COMPANY_NAME,
    TO_PAX_STATUS = su.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO
JOIN component.NEWSFEED_ITEM ni ON ni.ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID
CROSS APPLY ( SELECT TOP 1 * FROM component.NEWSFEED_ITEM_PAX WHERE NEWSFEED_ITEM_ID = ni.ID ) NIP
JOIN component.PAX p ON p.PAX_ID = NIP.PAX_ID
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'
WHERE #NEWSFEED_INFO.TO_PAX_ID IS NULL



--Populate POINTS_ISSUED
CREATE TABLE #POINTS_ISSUED (
    NOMINATION_ID BIGINT NOT NULL,
    POINTS_ISSUED DECIMAL(10,2) NULL
)
--If I am the SUBMITTER, points issued is the total points
INSERT INTO #POINTS_ISSUED
SELECT r.NOMINATION_ID, SUM(r.AMOUNT) AS 'POINTS_ISSUED' FROM component.RECOGNITION r
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.NOMINATION_ID = r.NOMINATION_ID AND r.PARENT_ID IS NULL
WHERE #NEWSFEED_INFO.FROM_PAX_ID = @paxId
AND r.PARENT_ID IS NULL -- Parent Id check
GROUP BY r.NOMINATION_ID

--If I am the RECEIVER, points issued is only my points
INSERT INTO #POINTS_ISSUED
SELECT r.NOMINATION_ID, r.AMOUNT FROM component.RECOGNITION r
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.NOMINATION_ID = r.NOMINATION_ID AND r.PARENT_ID IS NULL
WHERE r.RECEIVER_PAX_ID = @paxId
AND r.PARENT_ID IS NULL -- Parent Id check

UPDATE #NEWSFEED_INFO SET POINTS_ISSUED = (SELECT POINTS_ISSUED FROM #POINTS_ISSUED
WHERE #POINTS_ISSUED.NOMINATION_ID = #NEWSFEED_INFO.NOMINATION_ID)



-- ****** Step 3: Populate all PUBLIC feeds unless @type = SELF or @type = RECEIVED ******
IF @type <> 'SELF' AND @type <> 'RECEIVED'
BEGIN
    INSERT INTO #NEWSFEED_INFO (
        NEWSFEED_ITEM_ID,
        TYPE,
        ACTIVITY_TYPE,
        CREATE_DATE,
        NOMINATION_ID,
        PROGRAM_ID,
        PROGRAM_NAME,
        ECARD_ID,
        HEADLINE,
        FROM_PAX_ID,
        FROM_PAX_LANGUAGE_CODE,
        FROM_PAX_PREFERRED_LOCALE,
        FROM_PAX_CONTROL_NUM,
        FROM_PAX_FIRST_NAME,
        FROM_PAX_MIDDLE_INITIAL,
        FROM_PAX_LAST_NAME,
        FROM_PAX_NAME_PREFIX,
        FROM_PAX_NAME_SUFFIX,
        FROM_PAX_COMPANY_NAME,
        FROM_PAX_STATUS,
        FROM_PAX_JOB_TITLE,
        TO_PAX_ID,
        TO_PAX_LANGUAGE_CODE,
        TO_PAX_PREFERRED_LOCALE,
        TO_PAX_CONTROL_NUM,
        TO_PAX_FIRST_NAME,
        TO_PAX_MIDDLE_INITIAL,
        TO_PAX_LAST_NAME,
        TO_PAX_NAME_PREFIX,
        TO_PAX_NAME_SUFFIX,
        TO_PAX_COMPANY_NAME,
        TO_PAX_STATUS,
        TO_PAX_JOB_TITLE
    )
    SELECT DISTINCT ni.ID, 'PUBLIC' AS 'TYPE', ni.NEWSFEED_ITEM_TYPE_CODE, ni.CREATE_DATE, n.ID as 'NOMINATION_ID', pa.PROGRAM_ACTIVITY_ID, pa.PROGRAM_ACTIVITY_NAME, n.ECARD_ID, n.HEADLINE_COMMENT,
    from_pax.PAX_ID AS 'FROM_PAX_ID', from_pax.LANGUAGE_CODE AS 'FROM_LANGUAGE_CODE', from_pax.PREFERRED_LOCALE AS 'FROM_PREFERRED_LOCALE', from_pax.CONTROL_NUM AS 'FROM_CONTROL_NUM',
    from_pax.FIRST_NAME AS 'FROM_FIRST_NAME', from_pax.MIDDLE_INITIAL AS 'FROM_MIDDLE_INITIAL', from_pax.LAST_NAME AS 'FROM_LAST_NAME',
    from_pax.NAME_PREFIX AS 'FROM_NAME_PREFIX', from_pax.NAME_SUFFIX AS 'FROM_NAME_SUFFIX', from_pax.COMPANY_NAME AS 'FROM_COMPANY_NAME',
    from_su.STATUS_TYPE_CODE AS 'FROM_STATUS', from_pgm.MISC_DATA AS 'FROM_JOB_TITLE',
    to_pax.PAX_ID AS 'TO_PAX_ID', to_pax.LANGUAGE_CODE AS 'TO_LANGUAGE_CODE', to_pax.PREFERRED_LOCALE AS 'TO_PREFERRED_LOCALE', to_pax.CONTROL_NUM AS 'TO_CONTROL_NUM',
    to_pax.FIRST_NAME AS 'TO_FIRST_NAME', to_pax.MIDDLE_INITIAL AS 'TO_MIDDLE_INITIAL', to_pax.LAST_NAME AS 'TO_LAST_NAME',
    to_pax.NAME_PREFIX AS 'TO_NAME_PREFIX', to_pax.NAME_SUFFIX AS 'TO_NAME_SUFFIX', to_pax.COMPANY_NAME AS 'TO_COMPANY_NAME',
    to_su.STATUS_TYPE_CODE AS 'TO_STATUS', to_pgm.MISC_DATA AS 'TO_JOB_TITLE'
    FROM component.NEWSFEED_ITEM ni
    JOIN component.NOMINATION n ON n.ID = ni.FK1 AND n.NEWSFEED_VISIBILITY = 'TRUE'
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
    JOIN component.PROGRAM_ACTIVITY pa ON pa.PROGRAM_ACTIVITY_ID = n.PROGRAM_ACTIVITY_ID
    --FROM_PAX INFO
    JOIN component.PAX from_pax ON from_pax.PAX_ID = n.SUBMITTER_PAX_ID
    JOIN component.SYS_USER from_su ON from_su.PAX_ID = from_pax.PAX_ID
    JOIN component.PAX_GROUP from_pg ON from_pg.PAX_ID = from_pax.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC from_pgm ON from_pgm.PAX_GROUP_ID = from_pg.PAX_GROUP_ID AND from_pgm.VF_NAME = 'JOB_TITLE'
    --TO_PAX INFO
    JOIN component.PAX to_pax ON to_pax.PAX_ID = ni.TO_PAX_ID
    JOIN component.SYS_USER to_su ON to_su.PAX_ID = to_pax.PAX_ID
    JOIN component.PAX_GROUP to_pg ON to_pg.PAX_ID = to_pax.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC to_pgm ON to_pgm.PAX_GROUP_ID = to_pg.PAX_GROUP_ID AND to_pgm.VF_NAME = 'JOB_TITLE'
    WHERE ni.NEWSFEED_ITEM_TYPE_CODE = @activityType AND ni.ID NOT IN (SELECT * FROM #SELF_FEEDS)
END


-- ****** Step 4: Find any newsfeed_items that have all FALSE visibility, but need to show up on MANAGER feeds ******
INSERT INTO #NEWSFEED_INFO (
    NEWSFEED_ITEM_ID,
    TYPE,
    ACTIVITY_TYPE,
    CREATE_DATE,
    NOMINATION_ID,
    PROGRAM_ID,
    PROGRAM_NAME,
    ECARD_ID,
    HEADLINE,
    FROM_PAX_ID,
    FROM_PAX_LANGUAGE_CODE,
    FROM_PAX_PREFERRED_LOCALE,
    FROM_PAX_CONTROL_NUM,
    FROM_PAX_FIRST_NAME,
    FROM_PAX_MIDDLE_INITIAL,
    FROM_PAX_LAST_NAME,
    FROM_PAX_NAME_PREFIX,
    FROM_PAX_NAME_SUFFIX,
    FROM_PAX_COMPANY_NAME,
    FROM_PAX_STATUS,
    FROM_PAX_JOB_TITLE,
    TO_PAX_ID,
    TO_PAX_LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM,
    TO_PAX_FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL,
    TO_PAX_LAST_NAME,
    TO_PAX_NAME_PREFIX,
    TO_PAX_NAME_SUFFIX,
    TO_PAX_COMPANY_NAME,
    TO_PAX_STATUS,
    TO_PAX_JOB_TITLE
)
SELECT ni.ID, 'MANAGER' AS 'TYPE', ni.NEWSFEED_ITEM_TYPE_CODE, ni.CREATE_DATE, n.ID as 'NOMINATION_ID', pa.PROGRAM_ACTIVITY_ID, pa.PROGRAM_ACTIVITY_NAME, n.ECARD_ID, n.HEADLINE_COMMENT,
from_pax.PAX_ID AS 'FROM_PAX_ID', from_pax.LANGUAGE_CODE AS 'FROM_LANGUAGE_CODE', from_pax.PREFERRED_LOCALE AS 'FROM_PREFERRED_LOCALE', from_pax.CONTROL_NUM AS 'FROM_CONTROL_NUM',
from_pax.FIRST_NAME AS 'FROM_FIRST_NAME', from_pax.MIDDLE_INITIAL AS 'FROM_MIDDLE_INITIAL', from_pax.LAST_NAME AS 'FROM_LAST_NAME',
from_pax.NAME_PREFIX AS 'FROM_NAME_PREFIX', from_pax.NAME_SUFFIX AS 'FROM_NAME_SUFFIX', from_pax.COMPANY_NAME AS 'FROM_COMPANY_NAME',
from_su.STATUS_TYPE_CODE AS 'FROM_STATUS', from_pgm.MISC_DATA AS 'FROM_JOB_TITLE',
to_pax.PAX_ID AS 'TO_PAX_ID', to_pax.LANGUAGE_CODE AS 'TO_LANGUAGE_CODE', to_pax.PREFERRED_LOCALE AS 'TO_PREFERRED_LOCALE', to_pax.CONTROL_NUM AS 'TO_CONTROL_NUM',
to_pax.FIRST_NAME AS 'TO_FIRST_NAME', to_pax.MIDDLE_INITIAL AS 'TO_MIDDLE_INITIAL', to_pax.LAST_NAME AS 'TO_LAST_NAME',
to_pax.NAME_PREFIX AS 'TO_NAME_PREFIX', to_pax.NAME_SUFFIX AS 'TO_NAME_SUFFIX', to_pax.COMPANY_NAME AS 'TO_COMPANY_NAME',
to_su.STATUS_TYPE_CODE AS 'TO_STATUS', to_pgm.MISC_DATA AS 'TO_JOB_TITLE'
FROM component.NEWSFEED_ITEM ni
JOIN component.NOMINATION n ON n.ID = ni.FK1
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
JOIN component.PROGRAM_ACTIVITY pa ON pa.PROGRAM_ACTIVITY_ID = n.PROGRAM_ACTIVITY_ID
--FROM_PAX INFO
JOIN component.PAX from_pax ON from_pax.PAX_ID = n.SUBMITTER_PAX_ID
JOIN component.SYS_USER from_su ON from_su.PAX_ID = from_pax.PAX_ID
JOIN component.PAX_GROUP from_pg ON from_pg.PAX_ID = from_pax.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC from_pgm ON from_pgm.PAX_GROUP_ID = from_pg.PAX_GROUP_ID AND from_pgm.VF_NAME = 'JOB_TITLE'
--NEWSFEED_ITEM_PAX TO FIND DIRECT REPORTS
CROSS APPLY (
    SELECT TOP 1 n.* FROM component.NEWSFEED_ITEM_PAX n
    JOIN component.RELATIONSHIP r ON r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND r.PAX_ID_1 = n.PAX_ID AND r.PAX_ID_2 = @paxId
    WHERE n.NEWSFEED_ITEM_ID = ni.ID
) nip
--TO_PAX INFO
JOIN component.PAX to_pax ON to_pax.PAX_ID = nip.PAX_ID
JOIN component.SYS_USER to_su ON to_su.PAX_ID = to_pax.PAX_ID
JOIN component.PAX_GROUP to_pg ON to_pg.PAX_ID = to_pax.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC to_pgm ON to_pgm.PAX_GROUP_ID = to_pg.PAX_GROUP_ID AND to_pgm.VF_NAME = 'JOB_TITLE'
WHERE ni.NEWSFEED_ITEM_TYPE_CODE = @activityType AND ni.ID NOT IN (SELECT NEWSFEED_ITEM_ID FROM #NEWSFEED_INFO)



-- ****** Step 5: Fill in any missing info ******

--Populate FROM_PAX_IMAGES_ID
CREATE TABLE #FROM_IMAGE_IDS (
    PAX_ID BIGINT NOT NULL,
    IMAGES_ID BIGINT NOT NULL
)

INSERT INTO #FROM_IMAGE_IDS
SELECT DISTINCT pi.PAX_ID, pi.IMAGES_ID FROM component.PAX_IMAGES pi
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.FROM_PAX_ID = pi.PAX_ID
JOIN component.IMAGES i ON i.ID = pi.IMAGES_ID AND IMAGE_TYPE_CODE = 'PRFD'

UPDATE #NEWSFEED_INFO SET FROM_PAX_IMAGES_ID = (SELECT IMAGES_ID FROM #FROM_IMAGE_IDS
WHERE #FROM_IMAGE_IDS.PAX_ID = #NEWSFEED_INFO.FROM_PAX_ID)


--Populate TO_PAX_IMAGES_ID
CREATE TABLE #TO_IMAGE_IDS (
    PAX_ID BIGINT NOT NULL,
    IMAGES_ID BIGINT NOT NULL
)

INSERT INTO #TO_IMAGE_IDS
SELECT DISTINCT pi.PAX_ID, pi.IMAGES_ID FROM component.PAX_IMAGES pi
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.TO_PAX_ID = pi.PAX_ID
JOIN component.IMAGES i ON i.ID = pi.IMAGES_ID AND IMAGE_TYPE_CODE = 'PRFD'

UPDATE #NEWSFEED_INFO SET TO_PAX_IMAGES_ID = (SELECT IMAGES_ID FROM #TO_IMAGE_IDS
WHERE #TO_IMAGE_IDS.PAX_ID = #NEWSFEED_INFO.TO_PAX_ID)


--Populate RECIPIENT_COUNT
CREATE TABLE #RECIPIENT_COUNTS (
    NOMINATION_ID BIGINT NOT NULL,
    RECIPIENT_COUNT INTEGER NOT NULL
)

INSERT INTO #RECIPIENT_COUNTS
SELECT r.NOMINATION_ID, COUNT(r.ID) AS 'RECIPIENT_COUNT' FROM component.RECOGNITION r
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.NOMINATION_ID = r.NOMINATION_ID
WHERE r.PARENT_ID IS NULL -- Parent Id check
GROUP BY r.NOMINATION_ID

UPDATE #NEWSFEED_INFO SET RECIPIENT_COUNT = (SELECT RECIPIENT_COUNT FROM #RECIPIENT_COUNTS
WHERE #RECIPIENT_COUNTS.NOMINATION_ID = #NEWSFEED_INFO.NOMINATION_ID)


--Populate DIRECT_REPORT_COUNT
CREATE TABLE #DIRECT_REPORT_COUNTS (
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    DIRECT_REPORT_COUNT INTEGER NOT NULL
)

INSERT INTO #DIRECT_REPORT_COUNTS
SELECT nip.NEWSFEED_ITEM_ID, COUNT(nip.PAX_ID) AS 'DIRECT_REPORTS_COUNT'
FROM component.NEWSFEED_ITEM_PAX nip
JOIN #NEWSFEED_INFO ni ON ni.NEWSFEED_ITEM_ID = nip.NEWSFEED_ITEM_ID
--JOIN #SELF_FEEDS sf ON sf.NEWSFEED_ITEM_ID = nip.NEWSFEED_ITEM_ID
JOIN component.RELATIONSHIP r ON r.PAX_ID_1 = nip.PAX_ID AND r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND r.PAX_ID_2 = @paxId
GROUP BY nip.NEWSFEED_ITEM_ID

UPDATE #NEWSFEED_INFO SET DIRECT_REPORT_COUNT = (SELECT DIRECT_REPORT_COUNT FROM #DIRECT_REPORT_COUNTS
WHERE #DIRECT_REPORT_COUNTS.NEWSFEED_ITEM_ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID)


-- ****** Step 6: Update toPax if any of the recipients are a direct report of the logged in user ******
CREATE TABLE #DIRECT_REPORT (
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    PAX_ID BIGINT NOT NULL
)

--This will save the first direct report ordered by PAX_ID (it is not random like the Java code)
INSERT INTO #DIRECT_REPORT
SELECT nip.NEWSFEED_ITEM_ID, MIN(nip.PAX_ID) AS 'PAX_ID'
FROM #DIRECT_REPORT_COUNTS drc
JOIN component.NEWSFEED_ITEM_PAX nip ON nip.NEWSFEED_ITEM_ID = drc.NEWSFEED_ITEM_ID
JOIN component.RELATIONSHIP r ON r.PAX_ID_1 = nip.PAX_ID AND r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND r.PAX_ID_2 = @paxId
WHERE drc.NEWSFEED_ITEM_ID NOT IN (SELECT * FROM #MY_RECEIVED_FEED)
GROUP BY nip.NEWSFEED_ITEM_ID

--Overwrite the toPax data to be that of a direct report
UPDATE #NEWSFEED_INFO SET
    TO_PAX_ID = p.PAX_ID,
    TO_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = p.CONTROL_NUM,
    TO_PAX_FIRST_NAME = p.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = p.LAST_NAME,
    TO_PAX_NAME_PREFIX = p.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = p.COMPANY_NAME,
    TO_PAX_STATUS = su.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO
JOIN #DIRECT_REPORT ON #DIRECT_REPORT.NEWSFEED_ITEM_ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID
JOIN component.PAX p ON p.PAX_ID = #DIRECT_REPORT.PAX_ID
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'

-- ****** Step 7: Add 'Like' details

CREATE TABLE #LIKE_PAX(
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    PAX_ID BIGINT NOT NULL
)

-- add likes count
UPDATE #NEWSFEED_INFO SET
    LIKE_COUNT =
        ISNULL((SELECT COUNT(LIKE_ID)
            FROM LIKES L  WHERE NEWSFEED_ITEM_ID = L.TARGET_ID
            AND L.TARGET_TABLE = 'NEWSFEED_ITEM'
            GROUP BY L.TARGET_ID ),0)
FROM #NEWSFEED_INFO


-- Get Like pax requestor
INSERT INTO #LIKE_PAX
SELECT LK.TARGET_ID, LK.PAX_ID
FROM component.LIKES LK
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.NEWSFEED_ITEM_ID = LK.TARGET_ID
WHERE LK.TARGET_TABLE = 'NEWSFEED_ITEM' AND LK.PAX_ID = @paxId

-- Get latest person who liked it
INSERT INTO #LIKE_PAX
SELECT LK.TARGET_ID, LK.PAX_ID
FROM #NEWSFEED_INFO
CROSS APPLY (
    SELECT TOP 1 L.TARGET_ID,L.PAX_ID
    FROM component.LIKES L
    WHERE L.TARGET_TABLE = 'NEWSFEED_ITEM'
    AND #NEWSFEED_INFO.NEWSFEED_ITEM_ID = L.TARGET_ID
    order by L.LIKE_DATE DESC
) AS LK
WHERE
    LK.TARGET_ID NOT IN (SELECT NEWSFEED_ITEM_ID FROM #LIKE_PAX)

-- Add likePax: requestor or latest person who liked it
UPDATE #NEWSFEED_INFO SET
    LIKE_PAX_ID = p.PAX_ID,
    LIKE_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    LIKE_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    LIKE_PAX_CONTROL_NUM = p.CONTROL_NUM,
    LIKE_PAX_FIRST_NAME = p.FIRST_NAME,
    LIKE_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    LIKE_PAX_LAST_NAME = p.LAST_NAME,
    LIKE_PAX_NAME_PREFIX = p.NAME_PREFIX,
    LIKE_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    LIKE_PAX_COMPANY_NAME = p.COMPANY_NAME,
    LIKE_PAX_STATUS = su.STATUS_TYPE_CODE,
    LIKE_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #NEWSFEED_INFO
JOIN #LIKE_PAX LP ON LP.NEWSFEED_ITEM_ID = #NEWSFEED_INFO.NEWSFEED_ITEM_ID
JOIN component.PAX p ON p.PAX_ID = LP.PAX_ID
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'

-- ****** Step 8: Get Comment details
-- Setup temp table for most recent comments
CREATE TABLE #RECENT_COMMENT(
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    COMMENT_ID BIGINT NOT NULL,
    PAX_ID BIGINT NOT NULL
)

-- Set comment counts
UPDATE #NEWSFEED_INFO SET
    COMMENT_COUNT =
        ISNULL((SELECT COUNT(C.ID)
            FROM component.COMMENT C
            WHERE NF.NEWSFEED_ITEM_ID = C.TARGET_ID
                 AND C.TARGET_TABLE = 'NEWSFEED_ITEM'
                 AND C.STATUS_TYPE_CODE = 'ACTIVE'
            GROUP BY C.TARGET_ID ), 0)
FROM #NEWSFEED_INFO NF

-- Set my comment counts
UPDATE #NEWSFEED_INFO SET
    MY_COMMENT_COUNT =
        ISNULL((SELECT COUNT(C.ID)
            FROM component.COMMENT C
            WHERE NF.NEWSFEED_ITEM_ID = C.TARGET_ID
                 AND C.TARGET_TABLE = 'NEWSFEED_ITEM'
                 AND C.STATUS_TYPE_CODE = 'ACTIVE'
                 AND @paxId = c.PAX_ID
            GROUP BY C.TARGET_ID ), 0)
FROM #NEWSFEED_INFO NF

-- Get details of most recent comments
INSERT INTO #RECENT_COMMENT
SELECT C1.TARGET_ID,
       C1.ID,
       C1.PAX_ID
FROM component.COMMENT C1
JOIN #NEWSFEED_INFO NF
ON NF.NEWSFEED_ITEM_ID = C1.TARGET_ID
JOIN (SELECT C2.TARGET_ID,
             MAX(C2.COMMENT_DATE) as COMMENT_DATE
      FROM component.COMMENT C2
      WHERE C2.TARGET_TABLE = 'NEWSFEED_ITEM'
           AND C2.STATUS_TYPE_CODE = 'ACTIVE'
      GROUP BY C2.TARGET_ID) C2
ON C1.TARGET_ID = C2.TARGET_ID
AND C1.COMMENT_DATE = C2.COMMENT_DATE

-- Set comment details
UPDATE #NEWSFEED_INFO SET
    COMMENT_ID = C.ID,
    COMMENT_DATE = C.COMMENT_DATE,
    COMMENT_TEXT = C.COMMENT
FROM #NEWSFEED_INFO NF
JOIN #RECENT_COMMENT RC
ON RC.NEWSFEED_ITEM_ID = NF.NEWSFEED_ITEM_ID
JOIN component.COMMENT C
ON C.ID = RC.COMMENT_ID
WHERE C.TARGET_TABLE = 'NEWSFEED_ITEM'

-- Set comment pax details
UPDATE #NEWSFEED_INFO SET
    COMMENT_PAX_ID = P.PAX_ID,
    COMMENT_PAX_LANGUAGE_CODE = P.LANGUAGE_CODE,
    COMMENT_PAX_PREFERRED_LOCALE = P.PREFERRED_LOCALE,
    COMMENT_PAX_CONTROL_NUM = P.CONTROL_NUM,
    COMMENT_PAX_FIRST_NAME = P.FIRST_NAME,
    COMMENT_PAX_MIDDLE_INITIAL = P.MIDDLE_INITIAL,
    COMMENT_PAX_LAST_NAME = P.LAST_NAME,
    COMMENT_PAX_NAME_PREFIX = P.NAME_PREFIX,
    COMMENT_PAX_NAME_SUFFIX = P.NAME_SUFFIX,
    COMMENT_PAX_COMPANY_NAME = P.COMPANY_NAME,
    COMMENT_PAX_STATUS = SU.STATUS_TYPE_CODE,
    COMMENT_PAX_JOB_TITLE = PGM.MISC_DATA
FROM #NEWSFEED_INFO NF
JOIN #RECENT_COMMENT RC ON RC.NEWSFEED_ITEM_ID = NF.NEWSFEED_ITEM_ID
JOIN component.PAX P ON P.PAX_ID = RC.PAX_ID
JOIN component.SYS_USER SU ON SU.PAX_ID = P.PAX_ID
JOIN component.PAX_GROUP PG ON PG.PAX_ID = P.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'

--My Raised Count
UPDATE #NEWSFEED_INFO SET
  MY_RAISED_COUNT =
    ISNULL((SELECT COUNT(R.ID)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = NF.NOMINATION_ID AND N.SUBMITTER_PAX_ID = @loggedInPaxID)
    AND R.STATUS_TYPE_CODE = 'APPROVED'),0)
FROM #NEWSFEED_INFO NF

--Count Raises
UPDATE #NEWSFEED_INFO SET
  RAISED_COUNT =
    ISNULL((SELECT COUNT(R.ID)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = NF.NOMINATION_ID)
    AND R.STATUS_TYPE_CODE = 'APPROVED'),0)
FROM #NEWSFEED_INFO NF

--Count Raises Amounts
UPDATE #NEWSFEED_INFO SET
  RAISED_POINT_TOTAL =
    ISNULL((SELECT SUM(R.AMOUNT)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = NF.NOMINATION_ID)
    AND R.STATUS_TYPE_CODE = 'APPROVED' AND R.RECEIVER_PAX_ID = @loggedInPaxID),0)
FROM #NEWSFEED_INFO NF

-- Determine if login pax have given raise
UPDATE #NEWSFEED_INFO SET
  GIVEN_RAISE =
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM component.NOMINATION N
        JOIN (
          SELECT
            R.NOMINATION_ID,
            R.STATUS_TYPE_CODE
          FROM component.RECOGNITION R
          WHERE R.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')) RAISE
        ON N.ID = RAISE.NOMINATION_ID
        WHERE N.SUBMITTER_PAX_ID = @loggedInPaxID AND N.PARENT_ID = NF.NOMINATION_ID
        ) THEN 1
      ELSE 0
    END
FROM #NEWSFEED_INFO NF

CREATE TABLE #RAISED_PAX(
    PAX_ID BIGINT NOT NULL,
    NEWSFEED_NOMINATION_ID BIGINT NOT NULL
)

-- Get latest person who raised
INSERT INTO #RAISED_PAX
SELECT N.SUBMITTER_PAX_ID, N.PARENT_ID
FROM component.NOMINATION N
    JOIN (
      SELECT
        R.NOMINATION_ID,
        R.STATUS_TYPE_CODE
      FROM component.RECOGNITION R
      WHERE R.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')) RAISE
    ON N.ID = RAISE.NOMINATION_ID
JOIN #NEWSFEED_INFO NF ON N.PARENT_ID = NF.NOMINATION_ID
ORDER BY N.UPDATE_DATE

-- Set Raise Pax details
UPDATE #NEWSFEED_INFO SET
    RAISED_PAX_ID = P.PAX_ID,
    RAISED_PAX_LANGUAGE_CODE = P.LANGUAGE_CODE,
    RAISED_PAX_PREFERRED_LOCALE = P.PREFERRED_LOCALE,
    RAISED_PAX_CONTROL_NUM = P.CONTROL_NUM,
    RAISED_PAX_FIRST_NAME = P.FIRST_NAME,
    RAISED_PAX_MIDDLE_INITIAL = P.MIDDLE_INITIAL,
    RAISED_PAX_LAST_NAME = P.LAST_NAME,
    RAISED_PAX_NAME_PREFIX = P.NAME_PREFIX,
    RAISED_PAX_NAME_SUFFIX = P.NAME_SUFFIX,
    RAISED_PAX_COMPANY_NAME = P.COMPANY_NAME,
    RAISED_PAX_STATUS = SU.STATUS_TYPE_CODE,
    RAISED_PAX_JOB_TITLE = PGM.MISC_DATA,
    RAISED_PAX_IMAGES_VERSION = PI.VERSION
FROM #NEWSFEED_INFO NF
JOIN #RAISED_PAX RP on NF.NOMINATION_ID = RP.NEWSFEED_NOMINATION_ID
JOIN component.PAX P on P.PAX_ID = RP.PAX_ID
JOIN component.SYS_USER SU ON SU.PAX_ID = P.PAX_ID
JOIN component.PAX_GROUP PG ON PG.PAX_ID = P.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
LEFT JOIN component.PAX_IMAGES PI ON PI.PAX_ID = P.PAX_ID


-- GET IMAGE VERSION --
UPDATE    #NEWSFEED_INFO
SET
    TO_PAX_IMAGES_VERSION = to_pax_images.VERSION,
    FROM_PAX_IMAGES_VERSION = from_pax_images.VERSION,
    COMMENT_PAX_IMAGES_VERSION = comment_pax_images.VERSION,
    LIKE_PAX_IMAGES_VERSION = comment_pax_images.VERSION
FROM    #NEWSFEED_INFO NI
LEFT JOIN component.PAX_IMAGES to_pax_images ON to_pax_images.PAX_ID = NI.TO_PAX_ID
LEFT JOIN component.PAX_IMAGES from_pax_images ON from_pax_images.PAX_ID = NI.FROM_PAX_ID
LEFT JOIN component.PAX_IMAGES comment_pax_images ON comment_pax_images.PAX_ID = NI.FROM_PAX_ID
LEFT JOIN component.PAX_IMAGES like_pax_images ON like_pax_images.PAX_ID = NI.FROM_PAX_ID

-- ****** Step 9: Loop through all nominations and build a JSON list of their recognition_criteria IDs ******
CREATE TABLE #CRITERIA_INFO (
    ID INT IDENTITY,
    NOMINATION_ID BIGINT NOT NULL,
    RECOGNITION_CRITERIA_ID BIGINT NOT NULL,
    RECOGNITION_CRITERIA_NAME NVARCHAR(40) NOT NULL,
    RECOGNITION_CRITERIA_JSON NVARCHAR(MAX) NULL
)

INSERT INTO #CRITERIA_INFO
SELECT n.ID AS 'NOMINATION_ID',
    rc.ID AS 'RECOGNITION_CRITERIA_ID',
    rc.RECOGNITION_CRITERIA_NAME AS 'RECOGNITION_CRITERA_NAME', NULL
FROM component.NOMINATION n
JOIN component.NOMINATION_CRITERIA nc ON nc.NOMINATION_ID = n.ID
JOIN component.RECOGNITION_CRITERIA rc ON rc.ID = nc.RECOGNITION_CRITERIA_ID
JOIN #NEWSFEED_INFO ON #NEWSFEED_INFO.NOMINATION_ID = n.ID

DECLARE @total int
DECLARE @count int
DECLARE @previousNomination BIGINT

SET @total = (select COUNT(NOMINATION_ID) from #criteria_info)
SET @count = 1
SET @previousNomination = 0
DECLARE @JSON varchar(MAX)
DECLARE @insertComma NVARCHAR(1)

--Loop through each recognition_criteria for each nomination_ID
WHILE @count <= @total BEGIN

    DECLARE @currentNomination BIGINT
    select @currentNomination = NOMINATION_ID from #criteria_info where ID = @count

    IF @currentNomination <> @previousNomination
    BEGIN
        --This is a new Nomination ID. Save the JSON to the current Nomination_ID, update the previousNominationID, and reset the JSON
        SET @JSON = CONCAT(@JSON, ']') --Close out the JSON
        UPDATE #CRITERIA_INFO SET RECOGNITION_CRITERIA_JSON = @JSON WHERE NOMINATION_ID = @previousNomination
        SET @previousNomination = @currentNomination
        SET @JSON = '['
        SET @insertComma = 'F'
    END

    --Build up the list of recognition_criteria
    IF @insertComma = 'F'
    BEGIN
        --Need to comma separate the recognition criterias if there is more than one
        --SET @JSON = CONCAT(@JSON, '{"id":', (SELECT RECOGNITION_CRITERIA_ID FROM #CRITERIA_INFO WHERE ID = 1),
        --                    ',"displayName":"', (SELECT RECOGNITION_CRITERIA_NAME FROM #CRITERIA_INFO WHERE ID = 1), '"}')

        --Using FOR XML
        SET @JSON = CONCAT(@JSON, (select '{"id":' + CONVERT(NVARCHAR, #CRITERIA_INFO.RECOGNITION_CRITERIA_ID), + ',"displayName":"' + #CRITERIA_INFO.RECOGNITION_CRITERIA_NAME + '"}'
                                    from #CRITERIA_INFO    WHERE #CRITERIA_INFO.ID = @count
                                    FOR XML PATH(''))
                           )
    END


    IF @insertComma = 'T'
    BEGIN
        --Need to comma separate the recognition criterias if there is more than one
        --SET @JSON = CONCAT(@JSON, ',{"id":', (SELECT RECOGNITION_CRITERIA_ID FROM #CRITERIA_INFO WHERE ID = 1),
        --                    ',"displayName":"', (SELECT RECOGNITION_CRITERIA_NAME FROM #CRITERIA_INFO WHERE ID = 1), '"}')

        --Using FOR XML
        SET @JSON = CONCAT(@JSON, (select ',{"id":' + CONVERT(NVARCHAR, #CRITERIA_INFO.RECOGNITION_CRITERIA_ID), + ',"displayName":"' + #CRITERIA_INFO.RECOGNITION_CRITERIA_NAME + '"}'
                                    from #CRITERIA_INFO    WHERE #CRITERIA_INFO.ID = @count
                                    FOR XML PATH(''))
                           )
    END

    --Make this true since now we have the first recognition_criteria
    SET @insertComma = 'T'

    --Increment the counter
    SET @count = @count + 1

END

---Save the last record
SET @JSON = CONCAT(@JSON, ']') --Close out the JSON
UPDATE #CRITERIA_INFO SET RECOGNITION_CRITERIA_JSON = @JSON WHERE NOMINATION_ID = @previousNomination


--Update #NEWSFEED_INFO with the JSON object
UPDATE #NEWSFEED_INFO SET RECOGNITION_CRITERIA_JSON = (SELECT RECOGNITION_CRITERIA_JSON FROM
(SELECT DISTINCT NOMINATION_ID, RECOGNITION_CRITERIA_JSON FROM #CRITERIA_INFO) AS TEMP
WHERE TEMP.NOMINATION_ID = #NEWSFEED_INFO.NOMINATION_ID)


-- ****** Step 10: Return the paginated RESULT_SET based on @type ******

IF @type = 'SELF'
BEGIN
    SELECT * FROM (
        SELECT ROW_NUMBER() OVER (ORDER BY CREATE_DATE DESC) AS 'ROW_NUM', *
        FROM #NEWSFEED_INFO WHERE TYPE = 'SELF'
    ) AS SEARCH_QUERY
    WHERE ROW_NUM >= @row_min
    AND ROW_NUM <= @row_max
    ORDER BY ROW_NUM
END
ELSE IF @type = 'PUBLIC'
BEGIN
    SELECT * FROM (
        SELECT ROW_NUMBER() OVER (ORDER BY CREATE_DATE DESC) AS 'ROW_NUM', *
        FROM #NEWSFEED_INFO WHERE TYPE = 'PUBLIC'
    ) AS SEARCH_QUERY
    WHERE ROW_NUM >= @row_min
    AND ROW_NUM <= @row_max
    ORDER BY ROW_NUM
END
ELSE IF @type = 'MANAGER'
BEGIN
    SELECT * FROM (
        SELECT ROW_NUMBER() OVER (ORDER BY CREATE_DATE DESC) AS 'ROW_NUM', *
        FROM #NEWSFEED_INFO WHERE (TYPE = 'PUBLIC' AND DIRECT_REPORT_COUNT IS NOT NULL) OR TYPE = 'MANAGER'
    ) AS SEARCH_QUERY
    WHERE ROW_NUM >= @row_min
    AND ROW_NUM <= @row_max
    ORDER BY ROW_NUM
END
ELSE -- @type = 'DEFAULT'
BEGIN
    SELECT * FROM (
        SELECT ROW_NUMBER() OVER (ORDER BY CREATE_DATE DESC) AS 'ROW_NUM', *
        FROM #NEWSFEED_INFO
    ) AS SEARCH_QUERY
    WHERE ROW_NUM >= @row_min
    AND ROW_NUM <= @row_max
END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UF_LIST_TO_TABLE]'
GO
ALTER FUNCTION [component].[UF_LIST_TO_TABLE]
  (
    @List NVARCHAR(MAX),
    @Separator CHAR
  )
  RETURNS
    @Table TABLE
    (
    items NVARCHAR(MAX)
    )
AS
  BEGIN
    DECLARE @item NVARCHAR(MAX),
            @pos INT

    IF (@List is NULL) OR (len(@List) < 1)
      RETURN

    SET @pos = 1

    WHILE (@pos != 0)
      BEGIN
        SET @pos = CHARINDEX(@Separator, @List)
        IF (@pos != 0)
          SET @item = LEFT(@List, @pos - 1)
        ELSE
          SET @item = @List

        IF (LEN(@item) > 0)
          INSERT INTO @Table(items) values (@item)

        SET @List = RIGHT(@List, LEN(@List) - @pos)

        IF (LEN(@List) = 0) BREAK
      END
    RETURN
  END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS]'
GO
ALTER PROCEDURE [component].[UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS]
  @programIds NVARCHAR(MAX),
  @userIds NVARCHAR(MAX),
  @startDate DATETIME2,
  @endDate DATETIME2

AS
BEGIN

  SET NOCOUNT ON

  --Temp variables for stat calculation
  DECLARE @TOTAL_USERS DECIMAL,
          @STATUS_TYPE_CODE VARCHAR(8)

  SET @STATUS_TYPE_CODE = 'APPROVED'

  --Set up program IDs
  CREATE TABLE #PROGRAM_ID_TABLE  (
    INPUT_PROGRAM_ID NVARCHAR(MAX)
  )

  INSERT INTO #PROGRAM_ID_TABLE (
    INPUT_PROGRAM_ID
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@programIds, ',')

  --Set up pax IDs
  CREATE TABLE #PAX_ID_TABLE  (
    INPUT_PAX_ID NVARCHAR(MAX)
  )

  INSERT INTO #PAX_ID_TABLE (
    INPUT_PAX_ID
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@userIds, ',')

  --Table for recognition criteria stats (Result table)
  CREATE TABLE #RECOGNITION_CRITERIA_STATS (
    RECOGNITION_CRITERIA_ID BIGINT,
    RECOGNITION_CRITERIA_DISPLAY_NAME NVARCHAR(MAX),
    RECOGNITIONS_RECEIVED BIGINT
  )

  --First, insert relevant recognition criterias into the resulting table (that satisfy given conditions)
  INSERT INTO #RECOGNITION_CRITERIA_STATS (
    RECOGNITION_CRITERIA_ID,
    RECOGNITIONS_RECEIVED
  )
    SELECT recognition_criteria.ID,
      COUNT(DISTINCT recognition.ID)
    FROM component.RECOGNITION recognition
      JOIN component.NOMINATION nomination
        ON nomination.ID = recognition.NOMINATION_ID
      JOIN component.NOMINATION_CRITERIA nomination_criteria
        ON nomination.ID = nomination_criteria.NOMINATION_ID
      JOIN component.RECOGNITION_CRITERIA recognition_criteria
        ON recognition_criteria.ID = nomination_criteria.RECOGNITION_CRITERIA_ID
    WHERE nomination.STATUS_TYPE_CODE = @STATUS_TYPE_CODE
          AND recognition.STATUS_TYPE_CODE = @STATUS_TYPE_CODE
          AND (
            @programIds IS NULL OR
            (nomination.PROGRAM_ACTIVITY_ID in (SELECT INPUT_PROGRAM_ID FROM #PROGRAM_ID_TABLE))
          )
          AND (
            @userIds IS NULL OR
            (nomination.SUBMITTER_PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
             OR recognition.RECEIVER_PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE))
          )
          AND nomination.SUBMITTAL_DATE >= @startDate
          AND nomination.SUBMITTAL_DATE <= @endDate
    GROUP BY recognition_criteria.ID

  --Then update the recognition criteria information that satified the query conditions
  UPDATE #RECOGNITION_CRITERIA_STATS
  SET RECOGNITION_CRITERIA_DISPLAY_NAME = results.display_name
  FROM (
         SELECT recognition_criteria.ID as recognition_criteria_id,
                recognition_criteria.RECOGNITION_CRITERIA_NAME as display_name
         FROM component.RECOGNITION_CRITERIA recognition_criteria
       ) results
  WHERE results.recognition_criteria_id = #RECOGNITION_CRITERIA_STATS.RECOGNITION_CRITERIA_ID

  -- Return the results (stored within the PROGRAM_STATS table)
  SELECT
    RESULT.RECOGNITION_CRITERIA_ID,
    RESULT.RECOGNITION_CRITERIA_DISPLAY_NAME,
    RESULT.RECOGNITIONS_RECEIVED
  FROM (SELECT * FROM #RECOGNITION_CRITERIA_STATS) RESULT

  --Drop temp tables
  DROP TABLE #PROGRAM_ID_TABLE
  DROP TABLE #PAX_ID_TABLE
  DROP TABLE #RECOGNITION_CRITERIA_STATS

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_PARTICIPANT_ACTIVITY_STATS]'
GO
ALTER PROCEDURE [component].[UP_GET_PARTICIPANT_ACTIVITY_STATS]
  @programIds NVARCHAR(MAX),
  @userIds NVARCHAR(MAX),
  @startDate DATETIME2,
  @endDate DATETIME2

AS
BEGIN

  SET NOCOUNT ON

  --Temp variables for stat calculation
  DECLARE @TOTAL_USERS DECIMAL,
          @STATUS_TYPE_CODE VARCHAR(8),
          @TEMP_IDENTIFIER INT

  SET @STATUS_TYPE_CODE = 'APPROVED'
  SET @TEMP_IDENTIFIER = 1

  --Set up program IDs
  CREATE TABLE #PROGRAM_ID_TABLE  (
    INPUT_PROGRAM_ID NVARCHAR(MAX)
  )

  INSERT INTO #PROGRAM_ID_TABLE (
    INPUT_PROGRAM_ID
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@programIds, ',')

  --Set up pax IDs
  CREATE TABLE #PAX_ID_TABLE  (
    INPUT_PAX_ID NVARCHAR(MAX)
  )

  INSERT INTO #PAX_ID_TABLE (
    INPUT_PAX_ID
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@userIds, ',')

  --Table for program stats (Result table)
  CREATE TABLE #ACTIVITY_STATS (
    IDENTIFIER BIGINT,
    RECOGNITIONS_GIVEN BIGINT,
    RECOGNITIONS_RECEIVED BIGINT,
    POINTS_GIVEN BIGINT,
    POINTS_RECEIVED BIGINT,
    LOGIN_COUNT BIGINT
  )

  --Insert giver information into temp table
  INSERT INTO #ACTIVITY_STATS (
    IDENTIFIER,
    RECOGNITIONS_GIVEN
  )
    SELECT @TEMP_IDENTIFIER,
      COUNT(DISTINCT nomination.ID)
    FROM component.NOMINATION nomination
    WHERE nomination.STATUS_TYPE_CODE = @STATUS_TYPE_CODE
          AND (
            @programIds IS NULL OR
            (nomination.PROGRAM_ACTIVITY_ID in (SELECT INPUT_PROGRAM_ID FROM #PROGRAM_ID_TABLE))
          )
          AND (
            @userIds IS NULL OR
            (nomination.SUBMITTER_PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE))
          )
          AND nomination.SUBMITTAL_DATE >= @startDate
          AND nomination.SUBMITTAL_DATE <= @endDate

  --Then, insert receiver information into temp table
  UPDATE #ACTIVITY_STATS
  SET RECOGNITIONS_RECEIVED = results.recognitions_received
  FROM (
         SELECT COUNT(DISTINCT nomination.ID) as recognitions_received
         FROM component.NOMINATION nomination
           JOIN component.RECOGNITION recognition
             ON nomination.ID = recognition.NOMINATION_ID
         WHERE nomination.STATUS_TYPE_CODE = @STATUS_TYPE_CODE
               AND recognition.STATUS_TYPE_CODE = @STATUS_TYPE_CODE
               AND (
                 @programIds IS NULL OR
                 (nomination.PROGRAM_ACTIVITY_ID in (SELECT INPUT_PROGRAM_ID FROM #PROGRAM_ID_TABLE))
               )
               AND (
                 @userIds IS NULL OR
                 (recognition.RECEIVER_PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE))
               )
               AND nomination.SUBMITTAL_DATE >= @startDate
               AND nomination.SUBMITTAL_DATE <= @endDate
       ) results
  WHERE #ACTIVITY_STATS.IDENTIFIER = @TEMP_IDENTIFIER

  --Then insert information about points issued into the temp table
  UPDATE #ACTIVITY_STATS
  SET POINTS_GIVEN = results.points_given
  FROM (
         SELECT ISNULL(SUM(ISNULL(payout.PAYOUT_AMOUNT,0)), 0) as points_given
         FROM component.PAYOUT payout
           JOIN component.EARNINGS_PAYOUT earnings_payout
             ON payout.PAYOUT_ID = earnings_payout.PAYOUT_ID
           JOIN component.EARNINGS earnings
             ON earnings.EARNINGS_ID = earnings_payout.EARNINGS_ID
           JOIN component.TRANSACTION_HEADER_EARNINGS transaction_header_earnings
             ON earnings.EARNINGS_ID = transaction_header_earnings.EARNINGS_ID
           JOIN component.TRANSACTION_HEADER transaction_header
             ON transaction_header.ID = transaction_header_earnings.TRANSACTION_HEADER_ID
           JOIN component.BUDGET_ALLOCATION budget_allocation
             ON transaction_header.ID = budget_allocation.TRANSACTION_HEADER_ID
           JOIN component.RECOGNITION recognition
             ON budget_allocation.FK1 = recognition.ID
           JOIN component.NOMINATION nomination
             ON nomination.ID = recognition.NOMINATION_ID
         WHERE payout.PAYOUT_DATE >= @startDate
               AND payout.PAYOUT_DATE <= @endDate
               AND (
                 @programIds IS NULL OR
                 (budget_allocation.PROGRAM_ACTIVITY_ID in (SELECT INPUT_PROGRAM_ID FROM #PROGRAM_ID_TABLE))
               )
               AND (@userIds IS NULL OR
                    nomination.SUBMITTER_PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
               )
               AND payout.STATUS = 'I'
               AND transaction_header.SUB_TYPE_CODE <> 'FUND'
       ) results
  WHERE #ACTIVITY_STATS.IDENTIFIER = @TEMP_IDENTIFIER


  --Then insert information about points received into the temp table (same joining as points issued, different status filter)
  UPDATE #ACTIVITY_STATS
  SET POINTS_RECEIVED = results.points_received
  FROM (
         SELECT ISNULL(SUM(ISNULL(payout.PAYOUT_AMOUNT,0)), 0) as points_received
         FROM component.PAYOUT payout
           JOIN component.EARNINGS_PAYOUT earnings_payout
             ON payout.PAYOUT_ID = earnings_payout.PAYOUT_ID
           JOIN component.EARNINGS earnings
             ON earnings.EARNINGS_ID = earnings_payout.EARNINGS_ID
           JOIN component.TRANSACTION_HEADER_EARNINGS transaction_header_earnings
             ON earnings.EARNINGS_ID = transaction_header_earnings.EARNINGS_ID
           JOIN component.TRANSACTION_HEADER transaction_header
             ON transaction_header.ID = transaction_header_earnings.TRANSACTION_HEADER_ID
           JOIN component.BUDGET_ALLOCATION budget_allocation
             ON transaction_header.ID = budget_allocation.TRANSACTION_HEADER_ID
         WHERE payout.PAYOUT_DATE >= @startDate
               AND payout.PAYOUT_DATE <= @endDate
               AND (
                 @programIds IS NULL OR
                 (budget_allocation.PROGRAM_ACTIVITY_ID in (SELECT INPUT_PROGRAM_ID FROM #PROGRAM_ID_TABLE))
               )
               AND (@userIds IS NULL OR
                    payout.PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
               )
               AND payout.STATUS = 'I'
               AND transaction_header.SUB_TYPE_CODE <> 'FUND'
       ) results
  WHERE #ACTIVITY_STATS.IDENTIFIER = @TEMP_IDENTIFIER

  -- Finally insert login count information into the table
  UPDATE #ACTIVITY_STATS
  SET LOGIN_COUNT = results.login_count
  FROM (
         SELECT ISNULL(COUNT(login_history.LOGIN_HISTORY_ID), 0) as login_count
         FROM component.LOGIN_HISTORY login_history
           JOIN component.SYS_USER sys_user
             ON sys_user.SYS_USER_ID = login_history.SYS_USER_ID
         WHERE sys_user.PAX_ID IN (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
               AND login_history.LOGIN_DATE >= @startDate
               AND login_history.LOGIN_DATE <= @endDate
       ) results
  WHERE #ACTIVITY_STATS.IDENTIFIER = @TEMP_IDENTIFIER

  -- Return the results (stored within the ACTIVITY_STATS table)
  SELECT
    RESULT.RECOGNITIONS_GIVEN,
    RESULT.RECOGNITIONS_RECEIVED,
    RESULT.POINTS_GIVEN,
    RESULT.POINTS_RECEIVED,
    RESULT.LOGIN_COUNT
  FROM (SELECT TOP 1 * from #ACTIVITY_STATS) RESULT

  --Drop temp tables
  DROP TABLE #PAX_ID_TABLE
  DROP TABLE #PROGRAM_ID_TABLE
  DROP TABLE #ACTIVITY_STATS

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ALERT_INFO_BY_PAX]'
GO

-- ==============================  NAME  ======================================
-- UP_ALERT_INFO_BY_PAX
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS A NOTIFICATIONS DETAILS BY PAX
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- MADRIDO        20150616                CREATED
-- MADRIDO        20150717                Return nominationID as targetID for RECOGNITION notifications.
-- DAGARFIN       20151005                  Adding in Nomination Parent Id check
-- MUDDAM        20150924                Changed MANAGER_RECOGNITION TO_PAX validation logic to go off of DIRECT_REPORT_COUNT instead of RECIPIENT_COUNT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ALERT_INFO_BY_PAX]
@paxId BIGINT,
@delim_alert_status VARCHAR(200),
@delim_alert_types VARCHAR(1000)

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
DECLARE    @delimiter CHAR(1)
DECLARE    @input_status TABLE ([status] NVARCHAR(20))
DECLARE    @input_types TABLE ([type] NVARCHAR(50))

-- INITIALIZATION --
SET    @delimiter = ','

-- Parse delimited input lists (status,types) into list of String --
INSERT INTO @input_status ([status]) SELECT UPPER(token) FROM UF_PARSE_STRING_TO_STRING (@delim_alert_status, @delimiter)

INSERT INTO @input_types ([type]) SELECT UPPER(token) FROM UF_PARSE_STRING_TO_STRING (@delim_alert_types, @delimiter)


 -- DRIVER
 CREATE TABLE #ALERT_INFO (
    ALERT_ID BIGINT NOT NULL,
    ALERT_TYPE NVARCHAR(20) NULL,
    ALERT_STATUS NVARCHAR(20) NULL,
    ALERT_STATUS_DATE DATETIME2(7) NULL,
    ALERT_CREATE_DATE DATETIME2(7) NULL,
    ALERT_TARGET_ID BIGINT NULL,
    TARGET_ID BIGINT NULL,
    HEADLINE NVARCHAR(140) NULL,
    POINTS_ISSUED DECIMAL(10 ,2) NULL,
    PROGRAM_NAME NVARCHAR(100)  NULL,
    DIRECT_REPORT_COUNT INTEGER NULL,
    RECIPIENT_COUNT INTEGER NULL,
    FROM_PAX_ID BIGINT NULL,
    FROM_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    FROM_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    FROM_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    FROM_PAX_FIRST_NAME NVARCHAR(30) NULL,
    FROM_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    FROM_PAX_LAST_NAME NVARCHAR(40) NULL,
    FROM_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    FROM_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    FROM_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    FROM_PAX_IMAGES_VERSION INT NULL,
    FROM_PAX_IMAGES_ID BIGINT NULL,
    FROM_PAX_STATUS NVARCHAR(20) NULL,
    FROM_PAX_JOB_TITLE NVARCHAR(255) NULL,
    TO_PAX_ID BIGINT NULL,
    TO_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    TO_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    TO_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    TO_PAX_FIRST_NAME NVARCHAR(30) NULL,
    TO_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    TO_PAX_LAST_NAME NVARCHAR(40) NULL,
    TO_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    TO_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    TO_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    TO_PAX_IMAGES_VERSION INT NULL,
    TO_PAX_IMAGES_ID BIGINT NULL,
    TO_PAX_STATUS NVARCHAR(20) NULL,
    TO_PAX_JOB_TITLE NVARCHAR(255) NULL
)

-- Get Alert base record. --
INSERT INTO #ALERT_INFO (
    ALERT_ID,
    ALERT_TYPE,
    ALERT_STATUS,
    ALERT_STATUS_DATE,
    ALERT_CREATE_DATE,
    TARGET_ID,
    PROGRAM_NAME)
SELECT
A.ID,A.ALERT_SUB_TYPE_CODE,A.STATUS_TYPE_CODE,
A.STATUS_DATE,A.CREATE_DATE,A.TARGET_ID,P.PROGRAM_ACTIVITY_NAME
FROM component.ALERT A,@input_status ST, @input_types TP,component.PROGRAM_ACTIVITY P
WHERE A.PAX_ID = @paxId
    AND  A.STATUS_TYPE_CODE = ST.status
    AND  A.ALERT_SUB_TYPE_CODE = TP.type
    AND A.PROGRAM_ACTIVITY_ID = P.PROGRAM_ACTIVITY_ID

-- ALERT RECOGNITION TYPES ---------------------------
-- Get Notification info from RECOGNITION table --
DECLARE    @alert_type NVARCHAR(50) = 'RECOGNITION'

UPDATE    #ALERT_INFO
SET
    HEADLINE = N.HEADLINE_COMMENT,
    FROM_PAX_ID = N.SUBMITTER_PAX_ID,
    TO_PAX_ID = R.RECEIVER_PAX_ID,
    POINTS_ISSUED = R.AMOUNT,
    RECIPIENT_COUNT = 1,
    ALERT_TARGET_ID = N.ID
FROM    #ALERT_INFO A
INNER JOIN component.RECOGNITION R ON A.TARGET_ID = R.ID
INNER JOIN component.NOMINATION N ON R.NOMINATION_ID = N.ID
WHERE A.ALERT_TYPE = @alert_type AND R.PARENT_ID IS NULL AND N.PARENT_ID IS NULL -- Parent Id check


-------------- RECOGNITIONS ----------------------------
--------------------------------------------------------

--------------------------------------------------------
-- ALERT MANAGER_RECOGNITION TYPES ---------------------
SET @alert_type = 'MANAGER_RECOGNITION'

-- Get Nomination and receiver info (from-pax). --
UPDATE    #ALERT_INFO
SET
    HEADLINE = N.HEADLINE_COMMENT,
    FROM_PAX_ID = N.SUBMITTER_PAX_ID,
    ALERT_TARGET_ID = A.TARGET_ID
FROM    #ALERT_INFO A
INNER JOIN component.NOMINATION N ON A.TARGET_ID = N.ID
WHERE A.ALERT_TYPE = @alert_type AND N.PARENT_ID IS NULL

-- Get manager's direct reports points and count --
UPDATE    #ALERT_INFO
SET
    TO_PAX_ID = PR.RECEIVER_PAX_ID,
    POINTS_ISSUED = PR.POINTS,
    DIRECT_REPORT_COUNT = PR.DIRECT_REC_COUNT,
    RECIPIENT_COUNT = PR.TOTAL_REC_COUNT
FROM    #ALERT_INFO A
CROSS APPLY (
    SELECT
        TOP 1
        RCG.RECEIVER_PAX_ID, SUM(RCG.AMOUNT) OVER() AS POINTS , COUNT(*) OVER() AS DIRECT_REC_COUNT,
        (SELECT count(ID) FROM component.RECOGNITION R WHERE R.NOMINATION_ID = RCG.NOMINATION_ID AND R.PARENT_ID IS NULL) as TOTAL_REC_COUNT -- Parent Id check
    FROM component.RECOGNITION RCG
    INNER JOIN component.RELATIONSHIP REL
        ON REL.PAX_ID_1 = RCG.RECEIVER_PAX_ID
            AND REL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
            AND PAX_ID_2 = @paxId -- INPUT
    WHERE NOMINATION_ID = A.TARGET_ID AND RCG.PARENT_ID IS NULL) AS PR -- Parent Id check
WHERE A.ALERT_TYPE = @alert_type

-- To-Pax null if there are multiple requestor's direct reports --
UPDATE #ALERT_INFO
SET
    TO_PAX_ID = NULL
WHERE RECIPIENT_COUNT > 1 AND ALERT_TYPE = @alert_type

-- To-Pax null if there are multiple requestor's direct reports --
UPDATE #ALERT_INFO
SET
    TO_PAX_ID = NULL
WHERE DIRECT_REPORT_COUNT > 1 AND ALERT_TYPE = @alert_type

------ MANAGER_RECOGNITIONS ----------------------------
--------------------------------------------------------


--------------------------------------------------------
-- ALERT APPROVAL TYPES ---------------------
SET @alert_type = 'APPROVAL'

-- Get Notification details from APPROVAL_PENDING table --
UPDATE    #ALERT_INFO
SET
    HEADLINE = N.HEADLINE_COMMENT,
    FROM_PAX_ID = N.SUBMITTER_PAX_ID,
    TO_PAX_ID = R.RECEIVER_PAX_ID,
    POINTS_ISSUED = R.AMOUNT,
    ALERT_TARGET_ID = A.TARGET_ID
FROM    #ALERT_INFO A
INNER JOIN component.APPROVAL_PENDING AP ON AP.ID = A.TARGET_ID
INNER JOIN component.RECOGNITION R ON AP.TARGET_SUB_ID = R.ID
INNER JOIN component.NOMINATION N ON R.NOMINATION_ID = N.ID
WHERE A.ALERT_TYPE = @alert_type AND R.PARENT_ID IS NULL AND N.PARENT_ID IS NULL -- Parent Id check

---------------- APPROVALS ----------------------------
--------------------------------------------------------

--------------------------------------------------------
-- ALERT POINT_LOAD TYPES ---------------------
SET @alert_type = 'POINT_LOAD'

-- Get Notification details from APPROVAL_PENDING table --
UPDATE    #ALERT_INFO
SET
    HEADLINE = BA.COMMENT,
    POINTS_ISSUED = BA.AMOUNT,
    ALERT_TARGET_ID = A.TARGET_ID
FROM    #ALERT_INFO A
INNER JOIN component.BUDGET_ALLOCATION BA ON BA.ID = A.TARGET_ID
WHERE A.ALERT_TYPE = @alert_type

---------------- POINT_LOAD ----------------------------
--------------------------------------------------------


-- GET From-Pax DETAILS --
UPDATE    #ALERT_INFO
SET
    FROM_PAX_LANGUAGE_CODE = from_pax.LANGUAGE_CODE,
    FROM_PAX_PREFERRED_LOCALE = from_pax.PREFERRED_LOCALE,
    FROM_PAX_CONTROL_NUM = from_pax.CONTROL_NUM,
    FROM_PAX_FIRST_NAME = from_pax.FIRST_NAME,
    FROM_PAX_MIDDLE_INITIAL = from_pax.MIDDLE_INITIAL,
    FROM_PAX_LAST_NAME = from_pax.LAST_NAME,
    FROM_PAX_NAME_PREFIX = from_pax.NAME_PREFIX,
    FROM_PAX_NAME_SUFFIX = from_pax.NAME_SUFFIX,
    FROM_PAX_COMPANY_NAME  = from_pax.COMPANY_NAME,
    FROM_PAX_STATUS = from_su.STATUS_TYPE_CODE,
    FROM_PAX_JOB_TITLE = from_pgm.MISC_DATA
FROM    #ALERT_INFO A
JOIN component.PAX from_pax ON from_pax.PAX_ID = A.FROM_PAX_ID
JOIN component.SYS_USER from_su ON from_su.PAX_ID = from_pax.PAX_ID
JOIN component.PAX_GROUP from_pg ON from_pg.PAX_ID = from_pax.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC from_pgm ON from_pgm.PAX_GROUP_ID = from_pg.PAX_GROUP_ID AND from_pgm.VF_NAME = 'JOB_TITLE'

-- GET TO-Pax DETAILS --
UPDATE    #ALERT_INFO
SET
    TO_PAX_LANGUAGE_CODE = to_pax.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = to_pax.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = to_pax.CONTROL_NUM,
    TO_PAX_FIRST_NAME = to_pax.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = to_pax.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = to_pax.LAST_NAME,
    TO_PAX_NAME_PREFIX = to_pax.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = to_pax.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = to_pax.COMPANY_NAME,
    TO_PAX_STATUS = to_su.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = to_pgm.MISC_DATA
FROM    #ALERT_INFO A
JOIN component.PAX to_pax ON to_pax.PAX_ID = A.TO_PAX_ID
JOIN component.SYS_USER to_su ON to_su.PAX_ID = to_pax.PAX_ID
JOIN component.PAX_GROUP to_pg ON to_pg.PAX_ID = to_pax.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC to_pgm ON to_pgm.PAX_GROUP_ID = to_pg.PAX_GROUP_ID AND to_pgm.VF_NAME = 'JOB_TITLE'

-- GET IMAGE VERSION --
UPDATE    #ALERT_INFO
SET
    TO_PAX_IMAGES_VERSION = to_pax_images.VERSION,
    FROM_PAX_IMAGES_VERSION = from_pax_images.VERSION
FROM    #ALERT_INFO A
LEFT JOIN component.PAX_IMAGES to_pax_images ON to_pax_images.PAX_ID = A.TO_PAX_ID
LEFT JOIN component.PAX_IMAGES from_pax_images ON from_pax_images.PAX_ID = A.FROM_PAX_ID

SELECT * FROM #ALERT_INFO


END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_MANAGER_BUDGET_STATS]'
GO









-- ==============================  NAME  ======================================
-- UP_MANAGER_BUDGET_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS BUDGET STATS FOR REPORTING PURPOSES INCLUDING...
-- BUDGET USED IN PERIOD
-- BUDGET TOTAL
-- BUDGET AVAILABLE
-- BUDGET EXPIRED
--
-- THIS IS A MANAGER REPORT, SO TOTALS WILL BE BASED UPON
-- INDIVIDUAL_GIVING_LIMIT INSTEAD OF ACTUAL BUDGET TOTALS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- HARWELLM        20150627                CREATED
-- HARWELLM        20150724                MODIFIED TO USE LEFT JOIN FOR TOTAL AMOUNTS
-- DAGARFIN       20150805                  REMOVING PARENT ID SINCE IT'S USED FOR BUDGETS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_MANAGER_BUDGET_STATS]
@delim_budget_id_list VARCHAR(MAX),
@delim_pax_id_list VARCHAR(MAX),
@start_date DATETIME2(7),
@end_date DATETIME2(7)

AS
BEGIN
SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

CREATE TABLE #budgetIdParams (
    ID BIGINT
)

CREATE TABLE #paxIdParams (
    ID BIGINT
)

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF BUDGET_ID(S) TO A LIST OF LONG(S)
INSERT INTO #budgetIdParams ([id]) SELECT token AS budget_id FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_budget_id_list, @delimiter)

-- PARSE DELIMITED LIST OF PAX_ID(S) TO A LIST OF LONG(S)
INSERT INTO #paxidParams ([id]) SELECT token AS pax_id FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_pax_id_list, @delimiter)

--CREATE TABLE TO RETURN VALUES
CREATE TABLE #budgetStats (
    BUDGET_USED_IN_PERIOD DECIMAL(10,2),
    BUDGET_USED_OUTSIDE_PERIOD DECIMAL(10,2),
    BUDGET_TOTAL DECIMAL(10,2),
    BUDGET_AVAILABLE DECIMAL(10,2),
    BUDGET_EXPIRED DECIMAL(10,2)
)


-- ****** Step 1: Calculate the budget total amount for the passed in budgetIds ******

--Total Budget for ONE budget in the list = number of direct reports per budget * individual giving limit
CREATE TABLE #budgetTotals (
    BUDGET_ID BIGINT NOT NULL,
    TOTAL_USERS INTEGER NOT NULL,
    GIVING_LIMIT DECIMAL(10,2) NULL,
    TOTAL_BUDGET DECIMAL(10,2) NULL
)

--Get a count of how many times each budget was used, and each budget's INDIVIDUAL_GIVING_LIMIT
INSERT INTO #budgetTotals
SELECT bp.ID, COUNT(bp.ID), b.INDIVIDUAL_GIVING_LIMIT, NULL
FROM #budgetIdParams bp
JOIN component.BUDGET b ON b.ID = bp.ID
GROUP BY bp.ID, b.INDIVIDUAL_GIVING_LIMIT

--Calculate the budget total for each budget
UPDATE #budgetTotals SET TOTAL_BUDGET = (TOTAL_USERS * GIVING_LIMIT)


-- Set the total on the budgetStats record
INSERT INTO #budgetStats (BUDGET_TOTAL) VALUES (ISNULL((SELECT SUM(TOTAL_BUDGET) FROM #budgetTotals), 0))


-- Remove the duplicate budgetId's and use that list from now on
CREATE TABLE #distinctBudgetIds (
    ID BIGINT
)

INSERT INTO #distinctBudgetIds SELECT DISTINCT * FROM #budgetIdParams


-- ****** Step 2: Calculate Budget Used In/Out of Period for the given PaxIds ******

--Save the total amounts used into a temp table
CREATE TABLE #totalAmountsUsed (
    ID BIGINT NOT NULL,
    IN_PERIOD DECIMAL(10,2) NOT NULL,
    OUT_OF_PERIOD DECIMAL(10,2) NOT NULL
)

INSERT INTO #totalAmountsUsed
SELECT ID, ISNULL(SUM(ISNULL(TOTAL_USED_IN_PERIOD,0)),0), ISNULL(SUM(ISNULL(TOTAL_USED_OUTSIDE_PERIOD,0)),0)
    FROM (
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    SELECT DISTINCT ISNULL(DEBITS.ID, ISNULL(CREDITS.ID, ISNULL(DEBITS_OUTSIDE_PERIOD.ID, ISNULL(CREDITS_OUTSIDE_PERIOD.ID, 0)))) AS 'ID',
    (CASE
        WHEN DEBITS.DEBITS is null then 0
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS is null then 0
        ELSE CREDITS.CREDITS
    END)
    AS 'TOTAL_USED_IN_PERIOD',
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD CALC-------------------------------------------
    (CASE
        WHEN DEBITS_OUTSIDE_PERIOD.DEBITS is null then 0
        ELSE DEBITS_OUTSIDE_PERIOD.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS_OUTSIDE_PERIOD.CREDITS is null then 0
        ELSE CREDITS_OUTSIDE_PERIOD.CREDITS
    END)
    AS 'TOTAL_USED_OUTSIDE_PERIOD'
    FROM component.BUDGET_ALLOCATION ba
    JOIN #distinctBudgetIds b ON b.ID = ba.BUDGET_ID_DEBIT OR b.ID = ba.BUDGET_ID_CREDIT
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN
        (--DEBITS IN PERIOD
        SELECT ba_debit.BUDGET_ID_DEBIT AS 'ID', SUM(ba_debit.AMOUNT) AS 'DEBITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.BUDGET_ALLOCATION ba_debit ON ba_debit.FK1 = r.ID AND ba_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN component.TRANSACTION_HEADER th ON th.ID = ba_debit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE = 'RECG'
        JOIN #distinctBudgetIds bp ON bp.ID = ba_debit.BUDGET_ID_DEBIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE ba_debit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY ba_debit.BUDGET_ID_DEBIT
        ) DEBITS
        ON B.ID = DEBITS.ID
    LEFT JOIN
        (--CREDITS IN PERIOD
        SELECT ba_credit.BUDGET_ID_CREDIT AS 'ID', SUM(ba_credit.AMOUNT) AS 'CREDITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.BUDGET_ALLOCATION ba_credit ON ba_credit.FK1 = r.ID AND ba_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN component.TRANSACTION_HEADER th ON th.ID = ba_credit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE = 'RECG'
        JOIN #distinctBudgetIds bp ON bp.ID = ba_credit.BUDGET_ID_CREDIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE ba_credit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY ba_credit.BUDGET_ID_CREDIT
        ) CREDITS
        ON B.ID = CREDITS.ID
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN
        (--DEBITS OUTSIDE PERIOD
        SELECT ba_debit.BUDGET_ID_DEBIT AS 'ID', SUM(ba_debit.AMOUNT) AS 'DEBITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.BUDGET_ALLOCATION ba_debit ON ba_debit.FK1 = r.ID AND ba_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN component.TRANSACTION_HEADER th ON th.ID = ba_debit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE = 'RECG'
        JOIN #distinctBudgetIds bp ON bp.ID = ba_debit.BUDGET_ID_DEBIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE ba_debit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        GROUP BY ba_debit.BUDGET_ID_DEBIT
        ) DEBITS_OUTSIDE_PERIOD
        ON B.ID = DEBITS_OUTSIDE_PERIOD.ID
    LEFT JOIN
        (--CREDITS OUTSIDE PERIOD
        SELECT ba_credit.BUDGET_ID_CREDIT AS 'ID', SUM(ba_credit.AMOUNT) AS 'CREDITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.BUDGET_ALLOCATION ba_credit ON ba_credit.FK1 = r.ID AND ba_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN component.TRANSACTION_HEADER th ON th.ID = ba_credit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'BUDG' AND th.SUB_TYPE_CODE = 'RECG'
        JOIN #distinctBudgetIds bp ON bp.ID = ba_credit.BUDGET_ID_CREDIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE ba_credit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        GROUP BY ba_credit.BUDGET_ID_CREDIT
        ) CREDITS_OUTSIDE_PERIOD
        ON B.ID = CREDITS_OUTSIDE_PERIOD.ID
WHERE ba.FK1 IS NOT NULL --This ensures we don't get the budget funding transactions
) TOTALS WHERE ID <> 0 GROUP BY ID

DECLARE @totalIn DECIMAL(10,2)
DECLARE @totalOut DECIMAL(10,2)
SET @totalIn = (SELECT SUM(IN_PERIOD) FROM #totalAmountsUsed)
SET @totalOut = (SELECT SUM(OUT_OF_PERIOD) FROM #totalAmountsUsed)

--Set budget used both in and out of period on the budget stats record
UPDATE #budgetStats SET BUDGET_USED_IN_PERIOD = @totalIn, BUDGET_USED_OUTSIDE_PERIOD = @totalOut
FROM #totalAmountsUsed


-- ****** Step 3: Calculate budget available and budget expired (for each budget individually) ******

CREATE TABLE #budgetAvailableExpired (
    BUDGET_ID BIGINT NOT NULL,
    BUDGET_AVAILABLE DECIMAL(10,2) NULL,
    BUDGET_EXPIRED DECIMAL(10,2) NULL
)

--Budget Available is if the budget is currently active and the THRU_DATE has not passed.
INSERT INTO #budgetAvailableExpired
SELECT b.ID, (bt.TOTAL_BUDGET - (ISNULL(ta.IN_PERIOD, 0) + ISNULL(ta.OUT_OF_PERIOD, 0))), 0
FROM #totalAmountsUsed ta
JOIN component.BUDGET b ON b.ID = ta.ID
JOIN #budgetTotals bt ON bt.BUDGET_ID = ta.ID
WHERE b.STATUS_TYPE_CODE = 'ACTIVE'
AND (b.THRU_DATE > getdate() OR b.THRU_DATE IS NULL)

--Budget Expired is if the budget is ACTIVE with a passed THRU_DATE, or if the budget is INACTIVE
INSERT INTO #budgetAvailableExpired
SELECT b.ID, 0, (bt.TOTAL_BUDGET - (ISNULL(ta.IN_PERIOD, 0) + ISNULL(ta.OUT_OF_PERIOD, 0)))
FROM #totalAmountsUsed ta
JOIN component.BUDGET b ON b.ID = ta.ID
JOIN #budgetTotals bt ON bt.BUDGET_ID = ta.ID
WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
(b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())

--If any budgets haven't been used yet, add them to #budgetAvailableExpired so that they get included in the calculations
--ACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT b.ID, bt.TOTAL_BUDGET, 0
FROM #distinctBudgetIds d
JOIN component.BUDGET b ON b.ID = d.ID
JOIN #budgetTotals bt on bt.BUDGET_ID = b.ID
WHERE b.STATUS_TYPE_CODE = 'ACTIVE'
AND (b.THRU_DATE > getdate() OR b.THRU_DATE IS NULL)
AND d.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)

--INACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT b.ID, 0, bt.TOTAL_BUDGET
FROM #distinctBudgetIds d
JOIN component.BUDGET b ON b.ID = d.ID
JOIN #budgetTotals bt on bt.BUDGET_ID = b.ID
WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
(b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
AND d.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)

DECLARE @totalAvailable DECIMAL(10,2)
DECLARE @totalExpired DECIMAL(10,2)
SET @totalAvailable = (SELECT SUM(BUDGET_AVAILABLE) FROM #budgetAvailableExpired)
SET @totalExpired = (SELECT SUM(BUDGET_EXPIRED) FROM #budgetAvailableExpired)

--Set budgetAvailable and budgetExpired on the budget stats record
UPDATE #budgetStats SET BUDGET_AVAILABLE = @totalAvailable, BUDGET_EXPIRED = @totalExpired
FROM #budgetAvailableExpired

SELECT * FROM #budgetStats

END











GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_POINT_LOAD_ACTIVITIES]'
GO
ALTER PROCEDURE [component].[UP_GET_POINT_LOAD_ACTIVITIES]
  @startDate DATETIME2,
  @endDate DATETIME2,
  @statuses NVARCHAR(MAX)

AS
BEGIN

  SET NOCOUNT ON

  --Temp variables for stat calculation
  DECLARE @STAGED_STATUSES NVARCHAR(MAX)

  SET @STAGED_STATUSES = 'NEW,IN_PROGRESS,PENDING_RELEASE'

  --Set up statuses
  CREATE TABLE #STATUS_TABLE  (
    STATUS NVARCHAR(MAX)
  )

  INSERT INTO #STATUS_TABLE (
    STATUS
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@statuses, ',')


  --Set up statuses that represent staged files
  CREATE TABLE #STAGED_STATUS_TABLE  (
    STATUS NVARCHAR(MAX)
  )

  INSERT INTO #STAGED_STATUS_TABLE (
    STATUS
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@STAGED_STATUSES, ',')

  --Table for point load activities (Result table)
  CREATE TABLE #POINT_LOAD_ACTIVITIES (
    BATCH_ID BIGINT PRIMARY KEY,
    STATUS NVARCHAR(MAX),
    START_DATE NVARCHAR(MAX),
    PROCESS_DATE NVARCHAR(MAX),
    FILE_NAME NVARCHAR(MAX),
    TOTAL_RECORDS BIGINT,
    GOOD_RECORDS BIGINT,
    ERROR_RECORDS BIGINT,
    TOTAL_POINTS BIGINT
  )

  --Insert all staged results (for records that have not already been accounted for)
  INSERT INTO #POINT_LOAD_ACTIVITIES (
    BATCH_ID,
    STATUS,
    START_DATE,
    PROCESS_DATE,
    FILE_NAME,
    TOTAL_RECORDS,
    GOOD_RECORDS,
    ERROR_RECORDS,
    TOTAL_POINTS
  )
    SELECT batch.BATCH_ID,
      batch.STATUS_TYPE_CODE,
      start_time_event.MESSAGE,
      process_time_event.message,
      file_name_event.message,
      total_recs_event.message,
      good_recs_event.message,
      err_recs_event.message,
      points.total_points
    FROM component.BATCH batch
      LEFT JOIN component.BATCH_EVENT start_time_event
        ON batch.BATCH_ID = start_time_event.BATCH_ID
           AND start_time_event.BATCH_EVENT_TYPE_CODE = 'STRTIM'
      LEFT JOIN component.BATCH_EVENT process_time_event
        ON batch.BATCH_ID = process_time_event.BATCH_ID
           AND process_time_event.BATCH_EVENT_TYPE_CODE = 'ENDTIM'
      LEFT JOIN component.BATCH_EVENT file_name_event
        ON batch.BATCH_ID = file_name_event.BATCH_ID
           AND file_name_event.BATCH_EVENT_TYPE_CODE = 'FILE'
      LEFT JOIN component.BATCH_EVENT total_recs_event
        ON batch.BATCH_ID = total_recs_event.BATCH_ID
           AND total_recs_event.BATCH_EVENT_TYPE_CODE = 'RECS'
      LEFT JOIN component.BATCH_EVENT good_recs_event
        ON batch.BATCH_ID = good_recs_event.BATCH_ID
           AND good_recs_event.BATCH_EVENT_TYPE_CODE = 'OKRECS'
      LEFT JOIN component.BATCH_EVENT err_recs_event
        ON batch.BATCH_ID = err_recs_event.BATCH_ID
           AND err_recs_event.BATCH_EVENT_TYPE_CODE = 'ERRS'
      LEFT JOIN (
                  SELECT th.BATCH_ID as batch_id,
                         ISNULL(SUM(ba.AMOUNT), 0) total_points
                  FROM component.TRANSACTION_HEADER th
                    LEFT JOIN component.BUDGET_ALLOCATION ba
                      ON th.ID = ba.TRANSACTION_HEADER_ID
                  WHERE th.STATUS_CODE = 'APPR'
                  GROUP BY th.BATCH_ID
                ) points
        ON batch.BATCH_ID = points.batch_id
    WHERE batch.BATCH_TYPE_CODE = 'CPTD'
          AND batch.STATUS_TYPE_CODE IN (SELECT STATUS FROM #STAGED_STATUS_TABLE)

  --Now insert activity information into result table
  INSERT INTO #POINT_LOAD_ACTIVITIES (
    BATCH_ID,
    STATUS,
    START_DATE,
    PROCESS_DATE,
    FILE_NAME,
    TOTAL_RECORDS,
    GOOD_RECORDS,
    ERROR_RECORDS,
    TOTAL_POINTS
  )
    SELECT batch.BATCH_ID,
      batch.STATUS_TYPE_CODE,
      start_time_event.MESSAGE,
      process_time_event.message,
      file_name_event.message,
      total_recs_event.message,
      good_recs_event.message,
      err_recs_event.message,
      points.total_points
    FROM component.BATCH batch
      LEFT JOIN component.BATCH_EVENT start_time_event
        ON batch.BATCH_ID = start_time_event.BATCH_ID
           AND start_time_event.BATCH_EVENT_TYPE_CODE = 'STRTIM'
      LEFT JOIN component.BATCH_EVENT process_time_event
        ON batch.BATCH_ID = process_time_event.BATCH_ID
           AND process_time_event.BATCH_EVENT_TYPE_CODE = 'ENDTIM'
      LEFT JOIN component.BATCH_EVENT file_name_event
        ON batch.BATCH_ID = file_name_event.BATCH_ID
           AND file_name_event.BATCH_EVENT_TYPE_CODE = 'FILE'
      LEFT JOIN component.BATCH_EVENT total_recs_event
        ON batch.BATCH_ID = total_recs_event.BATCH_ID
           AND total_recs_event.BATCH_EVENT_TYPE_CODE = 'RECS'
      LEFT JOIN component.BATCH_EVENT good_recs_event
        ON batch.BATCH_ID = good_recs_event.BATCH_ID
           AND good_recs_event.BATCH_EVENT_TYPE_CODE = 'OKRECS'
      LEFT JOIN component.BATCH_EVENT err_recs_event
        ON batch.BATCH_ID = err_recs_event.BATCH_ID
           AND err_recs_event.BATCH_EVENT_TYPE_CODE = 'ERRS'
      LEFT JOIN (
                  SELECT th.BATCH_ID as batch_id,
                         ISNULL(SUM(ba.AMOUNT), 0) total_points
                  FROM component.TRANSACTION_HEADER th
                    LEFT JOIN component.BUDGET_ALLOCATION ba
                      ON th.ID = ba.TRANSACTION_HEADER_ID
                  WHERE th.STATUS_CODE = 'APPR'
                  GROUP BY th.BATCH_ID
                ) points
        ON batch.BATCH_ID = points.batch_id
      LEFT JOIN #POINT_LOAD_ACTIVITIES existing_activities
        ON existing_activities.BATCH_ID = batch.BATCH_ID
    WHERE batch.BATCH_TYPE_CODE = 'CPTD'
          AND batch.CREATE_DATE >= @startDate
          AND batch.CREATE_DATE <= @endDate
          AND batch.STATUS_TYPE_CODE IN (SELECT STATUS FROM #STATUS_TABLE)
          AND existing_activities.BATCH_ID IS NULL

  -- Return the results (stored within the POINT_LOAD_ACTIVITIES table)
  SELECT
    RESULT.BATCH_ID,
    RESULT.STATUS,
    RESULT.START_DATE,
    RESULT.PROCESS_DATE,
    RESULT.FILE_NAME,
    RESULT.TOTAL_RECORDS,
    RESULT.GOOD_RECORDS,
    RESULT.ERROR_RECORDS,
    RESULT.TOTAL_POINTS
  FROM (SELECT * from #POINT_LOAD_ACTIVITIES) RESULT

  --Drop temp tables
  DROP TABLE #STATUS_TABLE
  DROP TABLE #STAGED_STATUS_TABLE
  DROP TABLE #POINT_LOAD_ACTIVITIES

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO

PRINT N'Altering [component].[UP_NEWSFEED_MESSAGE_OUTPUT]'
GO


-- ==============================  NAME  ======================================
-- UP_NEWSFEED_MESSAGE_OUTPUT
-- ===========================  DESCRIPTION  ==================================
-- THE PURPOSE OF THIS STORED PROCEDURE IS TO FETCH NEWS FEED MESSAGES
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- CHINTHR        20111216                CREATED
-- DOHOGNTA        20150306                REMOVE REFERENCE TO FRIENDSHIP TABLE
-- DOHOGNTA        20150306                REMOVE ASSOCIATION TABLE REFERENCE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- DOHOGNTA        20150316                RENAME TYPE COLUMN, INCREASE SIZE OF DATATYPE
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_NEWSFEED_MESSAGE_OUTPUT]
    @newsfeedid BIGINT
,    @paxid BIGINT
,    @selectedgroup INT
,    @isFriends BIT
AS
BEGIN

SET NOCOUNT ON

DECLARE @RESULTS TABLE (
    ID BIGINT NOT NULL
,    PARENT_ID BIGINT
,    MESSAGE NVARCHAR(2000) NOT NULL
,    NEWSFEED_ITEM_TYPE_CODE NVARCHAR(50) NULL
,    CREATE_DATE DATETIME NOT NULL
,   PARENT_CREATE_DATE DATETIME NULL
)

-- DISTINCT LIST OF GROUP_ID(S) FOR THE GIVEN PAX_ID
DECLARE @GROUPIDS TABLE (
    ID BIGINT NOT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

IF (@isFriends = 0)
BEGIN
    INSERT INTO @GROUPIDS
    SELECT ID FROM UF_ASSOCIATION_FETCH_GROUP_BY_PAX(@PAXID, -1)

    --    DISTINCT NEWS FEED MESSAGES ASSOCIATED FOR THE SELECTED USER GROUPS
    INSERT INTO @RESULTS
    SELECT DISTINCT TOP 50
        NFIT.ID
    ,   PARENT.ID
    ,   NFIT.MESSAGE
    ,   NFIT.NEWSFEED_ITEM_TYPE_CODE
    ,   NFIT.CREATE_DATE
    ,   PARENT.CREATE_DATE
    FROM
        NEWSFEED_ITEM AS NFIT
    LEFT OUTER JOIN NEWSFEED_ITEM PARENT
        ON NFIT.PARENT_ID = PARENT.ID
    INNER JOIN NEWSFEED_FILTER AS NFF
        ON NFIT.NEWSFEED_ITEM_TYPE_CODE = NFF.FILTER_VALUE
        AND NFF.FILTER_TYPE = 'ACTIVITY'
    INNER JOIN ASSOCIATION AS ASCI
        ON NFIT.ID = ASCI.FK1
        AND NFIT.STATUS_TYPE_CODE = 'A'
    AND ASCI.FK2 IN (
        SELECT DISTINCT
            ASSOC.PAX_ID
        FROM
            GROUPS_PAX AS ASSOC
        INNER JOIN GROUPS AS GRP
            ON ASSOC.GROUP_ID = GRP.GROUP_ID
        INNER JOIN @GROUPIDS AS GRPIDS
            ON GRPIDS.ID = GRP.GROUP_ID
        WHERE
            GRP.GROUP_ID = @SELECTEDGROUP
    )
    WHERE
        ASCI.ASSOCIATION_TYPE_ID = 3200
    AND NFIT.NEWSFEED_ID IS NULL
    AND NFF.NEWSFEED_ID = @NEWSFEEDID
    AND  ((NFIT.NEWSFEED_ITEM_TYPE_CODE <> 'RECEIVE_SUBMIT_RECOGNITION' AND NFIT.NEWSFEED_ITEM_TYPE_CODE <> 'MESSAGE') OR NFIT.NEWSFEED_ITEM_TYPE_CODE IS NULL)
    UNION SELECT DISTINCT TOP 50
        NFIT.ID
    ,   PARENT.ID
    ,   NFIT.MESSAGE
    ,   NFIT.NEWSFEED_ITEM_TYPE_CODE
    ,   NFIT.CREATE_DATE
    ,   PARENT.CREATE_DATE
    FROM
        NEWSFEED_ITEM AS NFIT
    LEFT OUTER JOIN NEWSFEED_ITEM PARENT
        ON NFIT.PARENT_ID = PARENT.ID
    INNER JOIN NEWSFEED_FILTER AS NFF
        ON NFIT.NEWSFEED_ITEM_TYPE_CODE = NFF.FILTER_VALUE
        AND NFF.FILTER_TYPE = 'ACTIVITY'
    INNER JOIN ASSOCIATION AS ASCI
        ON NFIT.ID = ASCI.FK1
        AND NFIT.STATUS_TYPE_CODE = 'A'
        AND ASCI.FK2 IN (
            SELECT DISTINCT
                ASSOC.PAX_ID
            FROM
                GROUPS_PAX AS ASSOC
            INNER JOIN GROUPS AS GRP
                ON ASSOC.GROUP_ID = GRP.GROUP_ID
            INNER JOIN @GROUPIDS AS GRPIDS
                ON GRPIDS.ID = GRP.GROUP_ID
            WHERE GRP.GROUP_ID = @SELECTEDGROUP
    )
    LEFT OUTER JOIN PAX_MISC PM
        ON (PM.PAX_ID = ASCI.FK2 AND PM.VF_NAME = 'RECG_NEWSFEED_OPT_OUT')
    WHERE NFIT.NEWSFEED_ITEM_TYPE_CODE = 'RECEIVE_SUBMIT_RECOGNITION'
    AND ASCI.ASSOCIATION_TYPE_ID = 3400
    AND NFIT.NEWSFEED_ID IS NULL
    AND NFF.NEWSFEED_ID = @NEWSFEEDID
    AND (PM.MISC_DATA = 'N' OR PM.MISC_DATA IS NULL)
    ORDER BY
        NFIT.CREATE_DATE DESC

END
--FETCH THE MESSAGES POSTED BY FRIENDS
ELSE
BEGIN
    INSERT INTO @RESULTS
    SELECT DISTINCT
        NFIT.ID
    ,   NFIT.PARENT_ID
    ,   NFIT.MESSAGE
    ,   NFIT.NEWSFEED_ITEM_TYPE_CODE
    ,   NFIT.CREATE_DATE
    ,   PARENT.CREATE_DATE
    FROM
        NEWSFEED_ITEM NFIT
    LEFT OUTER JOIN NEWSFEED_ITEM PARENT
        ON NFIT.PARENT_ID = PARENT.ID
    LEFT OUTER JOIN NEWSFEED_FILTER AS NFF
        ON NFIT.NEWSFEED_ITEM_TYPE_CODE = NFF.FILTER_VALUE
        AND NFF.NEWSFEED_ID = @NEWSFEEDID
    INNER JOIN ASSOCIATION A
        ON A.FK1 = NFIT.ID
        AND ASSOCIATION_TYPE_ID = 3200
    INNER JOIN RELATIONSHIP R
        ON R.PAX_ID_2 = A.FK2
        AND R.PAX_ID_1 = @paxid
    WHERE (NFIT.NEWSFEED_ITEM_TYPE_CODE <> 'RECEIVE_SUBMIT_RECOGNITION' AND NFIT.NEWSFEED_ITEM_TYPE_CODE <> 'MESSAGE')
    OR NFIT.NEWSFEED_ITEM_TYPE_CODE IS NULL
    UNION
    SELECT DISTINCT
        NFIT.ID
    ,   NFIT.PARENT_ID
    ,   NFIT.MESSAGE
    ,   NFIT.NEWSFEED_ITEM_TYPE_CODE
    ,   NFIT.CREATE_DATE
    ,   PARENT.CREATE_DATE
    FROM
        NEWSFEED_ITEM NFIT
    LEFT OUTER JOIN NEWSFEED_ITEM PARENT
        ON NFIT.PARENT_ID = PARENT.ID
    LEFT OUTER JOIN NEWSFEED_FILTER AS NFF
        ON NFIT.NEWSFEED_ITEM_TYPE_CODE = NFF.FILTER_VALUE
        AND NFF.NEWSFEED_ID = @NEWSFEEDID
    INNER JOIN ASSOCIATION A
        ON A.FK1 = NFIT.ID
        AND ASSOCIATION_TYPE_ID = 3400
    INNER JOIN RELATIONSHIP R
        ON R.PAX_ID_2 = A.FK2
        AND R.PAX_ID_1 = @paxid
    LEFT OUTER JOIN PAX_MISC PM
        ON (PM.PAX_ID = A.FK2 AND PM.VF_NAME = 'RECG_NEWSFEED_OPT_OUT')
    WHERE NFIT.NEWSFEED_ITEM_TYPE_CODE = 'RECEIVE_SUBMIT_RECOGNITION'
    AND (PM.MISC_DATA = 'N' OR PM.MISC_DATA IS NULL)
END

-- DISTINCT NEWS FEED MESSAGES SENT THROUGH ADMIN SCREEN (CAT)
INSERT INTO @RESULTS
SELECT DISTINCT TOP 50
    NFIT.ID
,   NULL
,   NFIT.MESSAGE
,   NFIT.NEWSFEED_ITEM_TYPE_CODE
,   NFIT.CREATE_DATE
,   NULL
FROM
    NEWSFEED_ITEM AS NFIT
INNER JOIN NEWSFEED AS NF
    ON NFIT.NEWSFEED_ID = NF.ID
    AND NF.ID = @NEWSFEEDID
INNER JOIN NEWSFEED_FILTER AS NFF
    ON NFF.NEWSFEED_ID = NF.ID
    AND NFF.FILTER_TYPE = 'ENROLLMENT_GROUP'
INNER JOIN NEWSFEED_ITEM_FILTER AS NFIF
    ON NFIF.NEWSFEED_ITEM_ID = NFIT.ID
    AND NFIF.FILTER_TYPE = 'GROUP'
INNER JOIN @GROUPIDS AS GRPID
    ON GRPID.ID = NFIF.FILTER_VALUE
WHERE
    NFIT.STATUS_TYPE_CODE = 'A'
AND (NFIT.NEWSFEED_ITEM_TYPE_CODE <> 'RECEIVE_SUBMIT_RECOGNITION' OR NFIT.NEWSFEED_ITEM_TYPE_CODE IS NULL)
UNION
SELECT DISTINCT TOP 50
    NFIT.ID
,   NULL
,   NFIT.MESSAGE
,   NFIT.NEWSFEED_ITEM_TYPE_CODE
,   NFIT.CREATE_DATE
,   NULL FROM NEWSFEED_ITEM AS NFIT
INNER JOIN NEWSFEED AS NF
    ON NFIT.NEWSFEED_ID = NF.ID
    AND NF.ID = @NEWSFEEDID
INNER JOIN NEWSFEED_FILTER AS NFF
    ON NFF.NEWSFEED_ID = NF.ID
    AND NFF.FILTER_TYPE = 'ENROLLMENT_GROUP'
INNER JOIN NEWSFEED_ITEM_FILTER AS NFIF
    ON NFIF.NEWSFEED_ITEM_ID = NFIT.ID
    AND NFIF.FILTER_TYPE = 'GROUP'
INNER JOIN @GROUPIDS AS GRPID
    ON GRPID.ID = NFIF.FILTER_VALUE
LEFT OUTER JOIN ASSOCIATION ASCI
    ON NF.ID = ASCI.FK1
LEFT OUTER JOIN PAX_MISC PM
    ON (PM.PAX_ID = ASCI.FK2 AND PM.VF_NAME = 'RECG_NEWSFEED_OPT_OUT')
WHERE NFIT.STATUS_TYPE_CODE = 'A'
AND NFIT.NEWSFEED_ITEM_TYPE_CODE = 'RECEIVE_SUBMIT_RECOGNITION'
AND ASCI.ASSOCIATION_TYPE_ID = 3400
AND (PM.MISC_DATA = 'N' OR PM.MISC_DATA IS NULL)
ORDER BY
    NFIT.CREATE_DATE DESC

SELECT TOP 50
    *
FROM
    @RESULTS
ORDER BY
    CREATE_DATE DESC

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_EMAIL_INFO]'
GO





-- ==============================  NAME  ======================================
-- UP_GET_EMAIL_INFO
-- ===========================  DESCRIPTION  ==================================
--
-- RETURN A RESULT SET OF RECOGNITION DETAILS USED TO POPULATE AN EMAIL TEMPLPATE
-- PARAMETERS:
--   @nominationId - The ID of the nomination to send emails for
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- HARWELLM        20150901                FIXED ISSUE WITH DUPLICATE MANAGER EMAILS
-- HARWELLM        20150914                ADDED COLOR SCHEME INFO
-- DAGARFIN        20150911                ADDED CHECKS TO RECOGNITIONS FOR RAISES
-- DAGARFIN       20151005                  ADDED CHECKT TO NOMINATIONS FOR RAISES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_EMAIL_INFO]
@nominationId BIGINT

AS
BEGIN

SET NOCOUNT ON

--DRIVER
CREATE TABLE #emailInfo (
    EMAIL_TYPE NVARCHAR(6) NOT NULL,
    NOMINATION_ID BIGINT NULL,
    RECOGNITION_ID BIGINT NULL,
    NOTIFICATION_ID BIGINT NULL,
    RECIPIENT_PAX_ID BIGINT NOT NULL,
    RECIPIENT_NAME_FIRST NVARCHAR(30) NOT NULL,
    RECIPIENT_NAME_LAST NVARCHAR(40) NOT NULL,
    RECIPIENT_EMAIL NVARCHAR(80) NULL,
    RECIPIENT_COUNT INTEGER NULL,
    DIRECT_REPORT_COUNT INTEGER NULL,
    DIRECT_REPORT_FIRST NVARCHAR(30) NULL,
    DIRECT_REPORT_LAST NVARCHAR(40) NULL,
    SUBMITTER_PAX_ID BIGINT NULL,
    SUBMITTER_NAME_FIRST  NVARCHAR(30) NULL,
    SUBMITTER_NAME_LAST  NVARCHAR(40) NULL,
    PROGRAM_NAME NVARCHAR(100) NULL,
    PROGRAM_TYPE_CODE NVARCHAR(4) NULL,
    POINT_AMOUNT DECIMAL(10,2) NULL,
    ECARD_ID BIGINT NULL,
    COMMENT NVARCHAR(MAX) NULL,
    HEADLINE NVARCHAR(140) NULL,
    PRIMARY_COLOR NVARCHAR(7) NULL,
    SECONDARY_COLOR NVARCHAR(7) NULL,
    TEXT_ON_PRIMARY_COLOR NVARCHAR(7) NULL
)

--Recipient Emails
INSERT INTO #emailInfo (
    EMAIL_TYPE,
    NOMINATION_ID,
    RECOGNITION_ID,
    NOTIFICATION_ID,
    POINT_AMOUNT,
    RECIPIENT_PAX_ID,
    RECIPIENT_NAME_FIRST,
    RECIPIENT_NAME_LAST,
    RECIPIENT_EMAIL,
    RECIPIENT_COUNT,
    DIRECT_REPORT_COUNT
)
SELECT
    'RECGPX' AS 'EMAIL_TYPE', --Recognition Participant
    @nominationID AS 'NOMINATION_ID',
    r.ID AS 'RECOGNITION_ID',
    a.ID AS 'NOTIFICATION_ID',
    ISNULL(r.AMOUNT, 0),
    p_rec.PAX_ID AS 'TO_PAX_ID',
    p_rec.FIRST_NAME AS 'TO_FIRST_NAME',
    p_rec.LAST_NAME AS 'TO_LAST_NAME',
    e_rec.EMAIL_ADDRESS,
    0 AS 'RECIPIENT_COUNT',
    0 AS 'DIRECT_REPORT_COUNT'
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
JOIN component.ALERT a ON a.TARGET_ID = r.ID AND a.ALERT_SUB_TYPE_CODE = 'RECOGNITION'
JOIN component.PAX p_rec ON p_rec.PAX_ID = r.RECEIVER_PAX_ID
LEFT JOIN component.EMAIL e_rec ON e_rec.PAX_ID = p_rec.PAX_ID AND e_rec.PREFERRED = 'Y'
WHERE n.ID = @nominationID


--Manager Emails
INSERT INTO #emailInfo (
    EMAIL_TYPE,
    NOMINATION_ID,
    NOTIFICATION_ID,
    RECIPIENT_PAX_ID,
    RECIPIENT_NAME_FIRST,
    RECIPIENT_NAME_LAST,
    RECIPIENT_EMAIL
)
SELECT DISTINCT
    'RECGMN' AS 'EMAIL_TYPE', --Recognition Manager
    @nominationID AS 'NOMINATION_ID',
    a.ID AS 'NOTIFICATION_ID',
    p_man.PAX_ID AS 'TO_PAX_ID',
    p_man.FIRST_NAME AS 'TO_FIRST_NAME',
    p_man.LAST_NAME AS 'TO_LAST_NAME',
    e_man.EMAIL_ADDRESS
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = r.RECEIVER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
JOIN component.ALERT a ON a.TARGET_ID = n.ID AND a.ALERT_SUB_TYPE_CODE = 'MANAGER_RECOGNITION' AND A.PAX_ID = rl.PAX_ID_2
JOIN component.PAX p_man ON p_man.PAX_ID = rl.PAX_ID_2
LEFT JOIN component.EMAIL e_man ON e_man.PAX_ID = p_man.PAX_ID AND e_man.PREFERRED = 'Y'
WHERE n.ID = @nominationID


--DIRECT_REPORT_COUNT FOR MANAGER EMAILS
CREATE TABLE #directReports(
    MANAGER_ID BIGINT NOT NULL,
    DIRECT_REPORT_COUNT INTEGER NOT NULL,
    DIRECT_REPORT_AMOUNT DECIMAL(10,2) NULL,
    RECIPIENT_COUNT INTEGER NULL
)

--DIRECT_REPORT_COUNT and DIRECT_REPORT_AMOUNTS
INSERT INTO #directReports (MANAGER_ID, DIRECT_REPORT_COUNT, DIRECT_REPORT_AMOUNT)
SELECT rl.PAX_ID_2, COUNT(rl.ID) AS 'DIRECT_REPORT_COUNT', ISNULL(SUM(r.AMOUNT), 0) AS 'DIRECT_REPORT_AMOUNT'
FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL AND n.PARENT_ID IS NULL -- Parent Id check
JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = r.RECEIVER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
WHERE n.ID = @nominationID
GROUP BY rl.PAX_ID_2, r.AMOUNT

--RECIPIENT_COUNT
UPDATE #directReports SET RECIPIENT_COUNT = (
SELECT COUNT(r.ID) AS 'RECIPIENT_COUNT' FROM component.NOMINATION n
JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.PARENT_ID IS NULL -- Parent Id check
WHERE n.ID = @nominationID)


--Update info for DIRECT_REPORT_COUNT, RECIPIENT_COUNT, and POINT_AMOUNT for manager emails
UPDATE #emailInfo SET
    DIRECT_REPORT_COUNT = dr.DIRECT_REPORT_COUNT,
    POINT_AMOUNT = dr.DIRECT_REPORT_AMOUNT,
    RECIPIENT_COUNT = dr.RECIPIENT_COUNT
FROM #directReports dr
JOIN #emailInfo ei ON ei.RECIPIENT_PAX_ID = dr.MANAGER_ID
WHERE ei.EMAIL_TYPE = 'RECGMN'


--DIRECT_REPORT RECIPIENT (For mangager emails)
CREATE TABLE #directReportRecipients (
    MANAGER_ID BIGINT NOT NULL,
    PAX_ID BIGINT NOT NULL
)

INSERT INTO #directReportRecipients
SELECT DISTINCT rl.PAX_ID_2, r.RECEIVER_PAX_ID FROM #emailInfo ei
JOIN component.RELATIONSHIP rl ON rl.PAX_ID_2 = ei.RECIPIENT_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
JOIN component.RECOGNITION r ON r.RECEIVER_PAX_ID = rl.PAX_ID_1 AND r.PARENT_ID IS NULL -- Parent Id check
WHERE ei.EMAIL_TYPE = 'RECGMN' AND r.NOMINATION_ID = @nominationId

UPDATE #emailInfo SET
    DIRECT_REPORT_FIRST = p_dr.FIRST_NAME,
    DIRECT_REPORT_LAST = p_dr.LAST_NAME
FROM #emailInfo
CROSS APPLY ( SELECT TOP 1 * FROM #directReportRecipients WHERE MANAGER_ID = #emailInfo.RECIPIENT_PAX_ID ) drr
JOIN component.PAX p_dr ON p_dr.PAX_ID = drr.PAX_ID
where #emailInfo.EMAIL_TYPE = 'RECGMN'



--SHARED INFO
UPDATE #emailInfo SET
    SUBMITTER_PAX_ID = p_sub.PAX_ID,
    SUBMITTER_NAME_FIRST = p_sub.FIRST_NAME,
    SUBMITTER_NAME_LAST = p_sub.LAST_NAME,
    PROGRAM_NAME = pa.PROGRAM_ACTIVITY_NAME,
    PROGRAM_TYPE_CODE = pa.PROGRAM_ACTIVITY_TYPE_CODE,
    COMMENT = n.COMMENT,
    HEADLINE = n.HEADLINE_COMMENT,
    ECARD_ID = n.ECARD_ID
FROM component.NOMINATION n
JOIN component.PROGRAM_ACTIVITY pa ON pa.PROGRAM_ACTIVITY_ID = n.PROGRAM_ACTIVITY_ID
JOIN component.PAX p_sub ON p_sub.PAX_ID = n.SUBMITTER_PAX_ID
WHERE n.ID = @nominationID AND n.PARENT_ID IS NULL


--Client Color Scheme
UPDATE #emailInfo SET PRIMARY_COLOR = VALUE_1
FROM component.APPLICATION_DATA
WHERE KEY_NAME = '/app/project_profile/colors/primaryColor'

UPDATE #emailInfo SET SECONDARY_COLOR = VALUE_1
FROM component.APPLICATION_DATA
WHERE KEY_NAME = '/app/project_profile/colors/secondaryColor'

UPDATE #emailInfo SET TEXT_ON_PRIMARY_COLOR = VALUE_1
FROM component.APPLICATION_DATA
WHERE KEY_NAME = '/app/project_profile/colors/textOnPrimaryColor'


select * from #emailInfo

DROP TABLE #emailInfo
DROP TABLE #directReports
DROP TABLE #directReportRecipients

END








GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_NEWSFEED_INFO_BY_ID]'
GO
-- ==============================  NAME  ======================================
-- UP_NEWSFEED_INFO_BY_ID
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS A SPECIFIC DETAIL ACTIVITY FEED INFORMATION.
-- DETAIL IS ONLY RETRIEVED FOR THE LOGGED IN USER'S ACTIVITY,
-- BUT IT CAN ALSO BE CALLED FOR ACTIVITY NOT RELEVANT TO THE LOGGED IN USER,
-- IN WHICH CASE THE SENSITIVE DATA WON'T BE POPULATED
--
-- ============================  REVISIONS  ========================================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- MADRIDO        20150603                CREATED
-- HARWELLM        20150625                ADDED LOGIC TO CALCULATE DIRECT_REPORT_COUNT
--                                        ALSO CHANGED TO GET JOB_TITLE FROM PAX_GROUP_MISC INSTEAD OF PAX_MISC
-- HARWELLM        20150724                RETURN IMAGES_ID INSTEAD OF PAX_IMAGES_ID TO FIX 'Subquery returned more than 1 value' BUG
-- HARWELLM        20150729                DON'T RETURN RECORDS IF THE USER DOES NOT HAVE THE VISIBILITY PERMISSIONS TO SEE THEM
-- MADRIDO        20150813                ADDED LIKE DETAILS
-- KUSEYA       20150820                ADDED COMMENT DETAILS
-- MADRIDO        20150902                ADD RECOGNITION'S COMMENT FOR MANAGERS
-- DAGARFIN       20150911                  ADDED CHECKS TO RECOGNITIONS FOR RAISES
-- DMOORE         20150911                 ADDED RAISE DETAILS
-- WRIGHTM        20150910                ADDED MY COMMENT COUNT
-- DMOORE         20150917                 UPDATE RAISED PAX
-- DMOORE         20150921                 CORRECT RAISED PAX TO GRAB NEWSFEED CORRECT RAISED PAX
-- KUSEYA         20150918                UPDATED COMMENT DETAILS TO ONLY COUNT AND RETRIEVE ACTIVE COMMENTS
-- DMOORE         20150929                 UPDATE RAISE LOGIC
-- DAGARFIN       20151005                  ADDED PARENT ID CHECKS FOR NOMINATIONS
--
-- ===========================  DECLARATIONS  ======================================================

ALTER PROCEDURE [component].[UP_NEWSFEED_INFO_BY_ID]
@activityId BIGINT,
@paxId BIGINT

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;

 -- DRIVER
 CREATE TABLE #ACTIVITY_DETAILS (
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    ACTIVITY_TYPE NVARCHAR(50) NOT NULL,
    RECIPIENT_COUNT INTEGER NULL,
    DIRECT_REPORT_COUNT INTEGER NULL,
    POINTS_ISSUED DECIMAL(10 ,2) NULL,
    CREATE_DATE DATETIME2(7) NOT NULL,
    NOMINATION_ID BIGINT NOT NULL,
    PROGRAM_ID BIGINT NOT NULL,
    PROGRAM_NAME NVARCHAR(100) NOT NULL,
    ECARD_ID BIGINT NULL,
    HEADLINE NVARCHAR(140) NULL,
    FROM_PAX_ID BIGINT NOT NULL,
    FROM_PAX_LANGUAGE_CODE NVARCHAR(3) NOT NULL,
    FROM_PAX_PREFERRED_LOCALE NVARCHAR(6) NOT NULL,
    FROM_PAX_CONTROL_NUM NVARCHAR(255) NOT NULL,
    FROM_PAX_FIRST_NAME NVARCHAR(30) NULL,
    FROM_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    FROM_PAX_LAST_NAME NVARCHAR(40) NULL,
    FROM_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    FROM_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    FROM_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    FROM_PAX_IMAGES_ID BIGINT NULL,
    FROM_PAX_IMAGES_VERSION INT NULL,
    FROM_PAX_STATUS NVARCHAR(20) NULL,
    FROM_PAX_JOB_TITLE NVARCHAR(255) NULL,
    TO_PAX_ID BIGINT NULL,
    TO_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    TO_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    TO_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    TO_PAX_FIRST_NAME NVARCHAR(30) NULL,
    TO_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    TO_PAX_LAST_NAME NVARCHAR(40) NULL,
    TO_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    TO_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    TO_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    TO_PAX_IMAGES_ID BIGINT NULL,
    TO_PAX_IMAGES_VERSION INT NULL,
    TO_PAX_STATUS NVARCHAR(20) NULL,
    TO_PAX_JOB_TITLE NVARCHAR(255) NULL,
    RECOGNITION_CRITERIA_JSON NVARCHAR(MAX) NULL,
    COMMENT NVARCHAR(MAX) NULL,
    LIKE_COUNT BIGINT NULL,
    LIKE_PAX_ID BIGINT NULL,
    LIKE_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    LIKE_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    LIKE_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    LIKE_PAX_FIRST_NAME NVARCHAR(30) NULL,
    LIKE_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    LIKE_PAX_LAST_NAME NVARCHAR(40) NULL,
    LIKE_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    LIKE_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    LIKE_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    LIKE_PAX_IMAGES_ID BIGINT NULL,
    LIKE_PAX_IMAGES_VERSION INT NULL,
    LIKE_PAX_STATUS NVARCHAR(20) NULL,
    LIKE_PAX_JOB_TITLE NVARCHAR(255) NULL,
    COMMENT_COUNT BIGINT NULL,
    MY_COMMENT_COUNT BIGINT NULL,
    COMMENT_ID BIGINT NULL,
    COMMENT_DATE DATETIME2(7) NULL,
    COMMENT_TEXT NVARCHAR(255) NULL,
    COMMENT_PAX_ID BIGINT NULL,
    COMMENT_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
    COMMENT_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
    COMMENT_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    COMMENT_PAX_FIRST_NAME NVARCHAR(30) NULL,
    COMMENT_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    COMMENT_PAX_LAST_NAME NVARCHAR(40) NULL,
    COMMENT_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    COMMENT_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    COMMENT_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    COMMENT_PAX_IMAGES_ID BIGINT NULL,
    COMMENT_PAX_IMAGES_VERSION INT NULL,
    COMMENT_PAX_STATUS NVARCHAR(20) NULL,
    COMMENT_PAX_JOB_TITLE NVARCHAR(255) NULL,
    MY_RAISED_COUNT BIGINT NULL,
    RAISED_COUNT BIGINT NULL,
    RAISED_POINT_TOTAL DECIMAL(10 ,2) NULL,
  RAISED_PAX_ID BIGINT NULL,
  RAISED_PAX_LANGUAGE_CODE NVARCHAR(3) NULL,
  RAISED_PAX_PREFERRED_LOCALE NVARCHAR(6) NULL,
  RAISED_PAX_CONTROL_NUM NVARCHAR(255) NULL,
    RAISED_PAX_FIRST_NAME NVARCHAR(30) NULL,
    RAISED_PAX_MIDDLE_INITIAL NVARCHAR(1) NULL,
    RAISED_PAX_LAST_NAME NVARCHAR(40) NULL,
    RAISED_PAX_NAME_PREFIX NVARCHAR(10) NULL,
    RAISED_PAX_NAME_SUFFIX NVARCHAR(10) NULL,
    RAISED_PAX_COMPANY_NAME NVARCHAR(255) NULL,
    RAISED_PAX_STATUS NVARCHAR(20) NULL,
    RAISED_PAX_IMAGES_VERSION INT NULL,
    RAISED_PAX_JOB_TITLE NVARCHAR(255) NULL,
    GIVEN_RAISE BIT NULL
)

--Save a list of the logged in user's direct reports (if any)
 CREATE TABLE #DIRECT_REPORTS (
    PAX_ID BIGINT NOT NULL
)

INSERT INTO #DIRECT_REPORTS SELECT PAX_ID_1 FROM component.RELATIONSHIP WHERE RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND PAX_ID_2 = @paxId


-- ACTIVITY DETAILS BASIC INFO
INSERT INTO #ACTIVITY_DETAILS (
    NEWSFEED_ITEM_ID,
    ACTIVITY_TYPE,
    CREATE_DATE,
    FROM_PAX_ID,
    FROM_PAX_LANGUAGE_CODE,
    FROM_PAX_PREFERRED_LOCALE,
    FROM_PAX_CONTROL_NUM,
    FROM_PAX_FIRST_NAME,
    FROM_PAX_MIDDLE_INITIAL,
    FROM_PAX_LAST_NAME,
    FROM_PAX_NAME_PREFIX,
    FROM_PAX_NAME_SUFFIX,
    FROM_PAX_COMPANY_NAME,
    FROM_PAX_IMAGES_ID,
    FROM_PAX_STATUS,
    FROM_PAX_JOB_TITLE,
    NOMINATION_ID,
    PROGRAM_ID,
    PROGRAM_NAME,
    ECARD_ID,
    HEADLINE,
    TO_PAX_ID)
SELECT DISTINCT
NW.ID,NW.NEWSFEED_ITEM_TYPE_CODE,
NW.CREATE_DATE,
--FROMPAX
FPX.PAX_ID, FPX.LANGUAGE_CODE,FPX.PREFERRED_LOCALE,FPX.CONTROL_NUM,FPX.FIRST_NAME,
FPX.MIDDLE_INITIAL,FPX.LAST_NAME,FPX.NAME_PREFIX,FPX.NAME_SUFFIX,FPX.COMPANY_NAME,
FI.ID,FSY.STATUS_TYPE_CODE, PGM.MISC_DATA,
--NOMINATION
NM.ID,
NM.PROGRAM_ACTIVITY_ID,
PA.PROGRAM_ACTIVITY_NAME,
NM.ECARD_ID,
NM.HEADLINE_COMMENT,
--TOPAX
NW.TO_PAX_ID
FROM component.NEWSFEED_ITEM NW
    INNER JOIN component.NOMINATION NM ON NM.ID = NW.FK1
    INNER JOIN component.PROGRAM_ACTIVITY PA ON PA.PROGRAM_ACTIVITY_ID = NM.PROGRAM_ACTIVITY_ID
    INNER JOIN component.PAX FPX ON NW.FROM_PAX_ID = FPX.PAX_ID
    INNER JOIN component.SYS_USER FSY ON FSY.PAX_ID = FPX.PAX_ID
    INNER JOIN component.PAX_GROUP PG ON PG.PAX_ID = FPX.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = NM.ID AND r.PARENT_ID IS NULL AND NM.PARENT_ID IS NULL -- Parent Id check
    LEFT JOIN (
        SELECT TOP 1 I.ID, p.PAX_ID FROM component.PAX_IMAGES p
        JOIN component.IMAGES i ON i.ID = p.IMAGES_ID AND i.IMAGE_TYPE_CODE = 'PRFD'
        WHERE PAX_ID = @paxId
    ) FI ON FI.PAX_ID = FPX.PAX_ID
    --The below makes sure we only return a newsfeed_item record if:
    --One of the NEWSFEED_ITEM_PAX visibilities is TRUE OR
    --Requestor PAX is the: submitter, recipient, or manager
    CROSS APPLY (
        SELECT TOP 1 * FROM component.NEWSFEED_ITEM_PAX
        WHERE NEWSFEED_ITEM_ID = NW.ID
        AND (    NEWSFEED_VISIBILITY = 'TRUE' --Public visibility
                OR PAX_ID = @paxId --Logged in user is a recipient
                OR NW.FROM_PAX_ID = @paxId --Logged in user is the submitter
                OR PAX_ID IN (SELECT * FROM #DIRECT_REPORTS) --Logged in user is the manager of a recipient
        )
    ) NIP
WHERE NW.ID = @activityId


--GET TO-PAX INFO
SELECT TOP 1
--toPAX
    PX.PAX_ID, PX.LANGUAGE_CODE,PX.PREFERRED_LOCALE,PX.CONTROL_NUM,PX.FIRST_NAME,
    PX.MIDDLE_INITIAL,PX.LAST_NAME,PX.NAME_PREFIX,PX.NAME_SUFFIX,PX.COMPANY_NAME,
    SY.STATUS_TYPE_CODE , PGM.MISC_DATA AS JOB_TITLE,
    PI.ID AS PROFILE_PICTURE
INTO #TOPAX
FROM component.NEWSFEED_ITEM_PAX NIP
    INNER JOIN  #ACTIVITY_DETAILS A ON A.NEWSFEED_ITEM_ID = NIP.NEWSFEED_ITEM_ID
    INNER JOIN component.PAX PX ON NIP.PAX_ID = PX.PAX_ID
    INNER JOIN component.SYS_USER SY ON SY.PAX_ID = PX.PAX_ID
    INNER JOIN component.PAX_GROUP PG ON PG.PAX_ID = PX.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
    LEFT JOIN
        (SELECT PI.ID,PI.PAX_ID
            FROM component.PAX_IMAGES PI
                INNER JOIN component.IMAGES FI ON FI.ID = PI.IMAGES_ID AND FI.IMAGE_TYPE_CODE = 'PRFD'
        ) AS PI ON PI.PAX_ID = PX.PAX_ID
WHERE
    NIP.PAX_ID = @paxId -- RETURN PAX LOGGED DETAILS
    OR ( NOT EXISTS (SELECT N.ID FROM component.NEWSFEED_ITEM_PAX N
                    WHERE N.NEWSFEED_ITEM_ID = NIP.NEWSFEED_ITEM_ID
                            AND N.PAX_ID = @paxId
                          AND N.PAX_ID <> A.FROM_PAX_ID)
         AND (A.TO_PAX_ID = NIP.PAX_ID -- OTHERWISE, RETURN NEWSFEED_ITEM TO-PAX
                OR A.FROM_PAX_ID = @paxId )
        )


--If there is no TO_PAX in the NEWSFEED_ITEM table, we need to populate the toPax if the logged in user is the giver of the nomination or a manager of the recipient
DECLARE @fromPaxId BIGINT
SET @fromPaxId = (SELECT FROM_PAX_ID FROM component.NEWSFEED_ITEM WHERE ID = @activityId)
IF @fromPaxId = @paxId OR @fromPaxId IN (SELECT * FROM #DIRECT_REPORTS)
BEGIN
    INSERT INTO #TOPAX
    SELECT TOP 1
--toPAX
    PX.PAX_ID, PX.LANGUAGE_CODE,PX.PREFERRED_LOCALE,PX.CONTROL_NUM,PX.FIRST_NAME,
    PX.MIDDLE_INITIAL,PX.LAST_NAME,PX.NAME_PREFIX,PX.NAME_SUFFIX,PX.COMPANY_NAME,
    SY.STATUS_TYPE_CODE , PGM.MISC_DATA AS JOB_TITLE,
    PI.ID AS PROFILE_PICTURE
    FROM component.NEWSFEED_ITEM_PAX NIP
    INNER JOIN component.PAX PX ON NIP.PAX_ID = PX.PAX_ID
    INNER JOIN component.SYS_USER SY ON SY.PAX_ID = PX.PAX_ID
    INNER JOIN component.PAX_GROUP PG ON PG.PAX_ID = PX.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
    LEFT JOIN component.PAX_IMAGES PI ON PI.PAX_ID = PX.PAX_ID
    LEFT JOIN component.IMAGES FI ON  FI.ID = PI.IMAGES_ID AND FI.IMAGE_TYPE_CODE = 'PRFD'
    WHERE NIP.NEWSFEED_ITEM_ID = @activityId
END


--UPDATE ACTIVITY DETAILS WITH TO-PAX INFO
UPDATE AD SET
    TO_PAX_ID = TP.PAX_ID,
    TO_PAX_LANGUAGE_CODE = TP.LANGUAGE_CODE,
    TO_PAX_PREFERRED_LOCALE = TP.PREFERRED_LOCALE,
    TO_PAX_CONTROL_NUM = TP.CONTROL_NUM,
    TO_PAX_FIRST_NAME = TP.FIRST_NAME,
    TO_PAX_MIDDLE_INITIAL = TP.MIDDLE_INITIAL,
    TO_PAX_LAST_NAME = TP.LAST_NAME,
    TO_PAX_NAME_PREFIX = TP.NAME_PREFIX,
    TO_PAX_NAME_SUFFIX = TP.NAME_SUFFIX,
    TO_PAX_COMPANY_NAME = TP.COMPANY_NAME,
    TO_PAX_IMAGES_ID = TP.PROFILE_PICTURE,
    TO_PAX_STATUS = TP.STATUS_TYPE_CODE,
    TO_PAX_JOB_TITLE = TP.JOB_TITLE
FROM #ACTIVITY_DETAILS AD,#TOPAX TP

-- GET RECIPIENTS COUNT INFO
UPDATE #ACTIVITY_DETAILS SET RECIPIENT_COUNT =
    (SELECT COUNT(*)
        FROM component.NEWSFEED_ITEM_PAX NIP
            INNER JOIN #ACTIVITY_DETAILS AD ON NIP.NEWSFEED_ITEM_ID = AD.NEWSFEED_ITEM_ID
    )


-- GET DIRECT REPORTS COUNT INFO
UPDATE #ACTIVITY_DETAILS SET DIRECT_REPORT_COUNT =
    (SELECT COUNT(*)
        FROM component.NEWSFEED_ITEM_PAX NIP
            INNER JOIN #ACTIVITY_DETAILS AD ON NIP.NEWSFEED_ITEM_ID = AD.NEWSFEED_ITEM_ID
            JOIN component.RELATIONSHIP R ON R.PAX_ID_1 = NIP.PAX_ID
                AND R.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
                AND R.PAX_ID_2 = @paxId
    )

--Save the PAX info for the direct report if there is one
--Do NOT update this if the logged in user is the recipient
IF (SELECT TO_PAX_ID FROM #ACTIVITY_DETAILS WHERE NEWSFEED_ITEM_ID = @activityId) != @paxId
BEGIN
    SELECT TOP 1
    --toPAX
        PX.PAX_ID, PX.LANGUAGE_CODE,PX.PREFERRED_LOCALE,PX.CONTROL_NUM,PX.FIRST_NAME,
        PX.MIDDLE_INITIAL,PX.LAST_NAME,PX.NAME_PREFIX,PX.NAME_SUFFIX,PX.COMPANY_NAME,
        SY.STATUS_TYPE_CODE , PGM.MISC_DATA AS JOB_TITLE,
        PI.ID AS PROFILE_PICTURE
    INTO #DIRECT_REPORT
    FROM component.NEWSFEED_ITEM_PAX NIP
    JOIN #ACTIVITY_DETAILS A ON A.NEWSFEED_ITEM_ID = NIP.NEWSFEED_ITEM_ID AND A.DIRECT_REPORT_COUNT > 0
    JOIN component.RELATIONSHIP R ON R.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND R.PAX_ID_1 = NIP.PAX_ID AND R.PAX_ID_2 = @paxId
    INNER JOIN component.PAX PX ON NIP.PAX_ID = PX.PAX_ID
    INNER JOIN component.SYS_USER SY ON SY.PAX_ID = PX.PAX_ID
    INNER JOIN component.PAX_GROUP PG ON PG.PAX_ID = PX.PAX_ID
    LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
    LEFT JOIN component.PAX_IMAGES PI ON PI.PAX_ID = PX.PAX_ID
    LEFT JOIN component.IMAGES FI ON  FI.ID = PI.IMAGES_ID AND FI.IMAGE_TYPE_CODE = 'PRFD'

    --UPDATE ACTIVITY DETAILS WITH DIRECT_REPORT INFO
    UPDATE AD SET
        TO_PAX_ID = DR.PAX_ID,
        TO_PAX_LANGUAGE_CODE = DR.LANGUAGE_CODE,
        TO_PAX_PREFERRED_LOCALE = DR.PREFERRED_LOCALE,
        TO_PAX_CONTROL_NUM = DR.CONTROL_NUM,
        TO_PAX_FIRST_NAME = DR.FIRST_NAME,
        TO_PAX_MIDDLE_INITIAL = DR.MIDDLE_INITIAL,
        TO_PAX_LAST_NAME = DR.LAST_NAME,
        TO_PAX_NAME_PREFIX = DR.NAME_PREFIX,
        TO_PAX_NAME_SUFFIX = DR.NAME_SUFFIX,
        TO_PAX_COMPANY_NAME = DR.COMPANY_NAME,
        TO_PAX_IMAGES_ID = DR.PROFILE_PICTURE,
        TO_PAX_STATUS = DR.STATUS_TYPE_CODE,
        TO_PAX_JOB_TITLE = DR.JOB_TITLE
    FROM #ACTIVITY_DETAILS AD,#DIRECT_REPORT DR
    WHERE AD.DIRECT_REPORT_COUNT > 0

    DROP TABLE #DIRECT_REPORT
END


-- GET POINTS ISSUED INFO
UPDATE #ACTIVITY_DETAILS SET POINTS_ISSUED =
    (SELECT
             SUM(R.AMOUNT)
        FROM component.RECOGNITION R
            INNER JOIN #ACTIVITY_DETAILS AD ON R.NOMINATION_ID = AD.NOMINATION_ID
        WHERE R.STATUS_TYPE_CODE = 'APPROVED' AND
        R.PARENT_ID IS NULL AND -- Parent Id check
            (R.RECEIVER_PAX_ID = @paxId     -- PAX LOGGED IS RECEIVER, RETURN ITS POINTS
            OR ( NOT EXISTS (SELECT RC.ID FROM component.RECOGNITION RC
                    WHERE RC.NOMINATION_ID = AD.NOMINATION_ID
                            AND R.RECEIVER_PAX_ID  = @paxId
                            AND RC.PARENT_ID IS NULL) -- Parent Id check
                 AND AD.FROM_PAX_ID = @paxId    -- PAX LOGGED IS GIVER, RETURN TOTAL POINTS
                )
            )
        GROUP BY R.NOMINATION_ID
    )

--GET PRIVATE COMMENT
UPDATE #ACTIVITY_DETAILS SET COMMENT =
    ( SELECT N.COMMENT
        FROM NOMINATION N
            INNER JOIN #ACTIVITY_DETAILS AD ON AD.NOMINATION_ID = N.ID AND N.PARENT_ID IS NULL
        WHERE AD.FROM_PAX_ID = @paxId OR AD.TO_PAX_ID = @paxId-- PAX IS RELATED TO NEWSFEED, RETURN PRIVATE COMMENT
            OR AD.FROM_PAX_ID IN (SELECT DR.PAX_ID FROM #DIRECT_REPORTS DR)
            OR AD.TO_PAX_ID IN (SELECT DR.PAX_ID FROM #DIRECT_REPORTS DR)

        )

-- ******* GET LIKE DETAILS ******************
--Get like count
UPDATE #ACTIVITY_DETAILS SET
    LIKE_COUNT =
        ISNULL((SELECT COUNT(LIKE_ID)
            FROM LIKES L  WHERE NEWSFEED_ITEM_ID = L.TARGET_ID
            AND L.TARGET_TABLE = 'NEWSFEED_ITEM'
            GROUP BY L.TARGET_ID ),0)

-- Get Like pax requestor
UPDATE #ACTIVITY_DETAILS SET
    LIKE_PAX_ID =
        (SELECT LK.PAX_ID
         FROM component.LIKES LK
         WHERE NEWSFEED_ITEM_ID = LK.TARGET_ID
                AND LK.TARGET_TABLE = 'NEWSFEED_ITEM' AND LK.PAX_ID = @paxId )

-- Get latest person who liked it if paxId has not liked it
UPDATE #ACTIVITY_DETAILS SET
    LIKE_PAX_ID =
        (SELECT TOP 1 LK.PAX_ID
         FROM component.LIKES LK
         WHERE NEWSFEED_ITEM_ID = LK.TARGET_ID
            AND LK.TARGET_TABLE = 'NEWSFEED_ITEM'
         ORDER BY LK.LIKE_DATE DESC)
WHERE LIKE_PAX_ID IS NULL

-- Add likePax details: requestor or latest person who liked it
UPDATE #ACTIVITY_DETAILS SET
    LIKE_PAX_ID = p.PAX_ID,
    LIKE_PAX_LANGUAGE_CODE = p.LANGUAGE_CODE,
    LIKE_PAX_PREFERRED_LOCALE = p.PREFERRED_LOCALE,
    LIKE_PAX_CONTROL_NUM = p.CONTROL_NUM,
    LIKE_PAX_FIRST_NAME = p.FIRST_NAME,
    LIKE_PAX_MIDDLE_INITIAL = p.MIDDLE_INITIAL,
    LIKE_PAX_LAST_NAME = p.LAST_NAME,
    LIKE_PAX_NAME_PREFIX = p.NAME_PREFIX,
    LIKE_PAX_NAME_SUFFIX = p.NAME_SUFFIX,
    LIKE_PAX_COMPANY_NAME = p.COMPANY_NAME,
    LIKE_PAX_STATUS = su.STATUS_TYPE_CODE,
    LIKE_PAX_JOB_TITLE = pgm.MISC_DATA
FROM #ACTIVITY_DETAILS AD
JOIN component.PAX p ON p.PAX_ID = AD.LIKE_PAX_ID
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm.VF_NAME = 'JOB_TITLE'


-- ******* GET COMMENT DETAILS ******************
-- Setup temp table for most recent comments
CREATE TABLE #RECENT_COMMENT(
    NEWSFEED_ITEM_ID BIGINT NOT NULL,
    COMMENT_ID BIGINT NOT NULL,
    PAX_ID BIGINT NOT NULL
)

-- Set comment counts
UPDATE #ACTIVITY_DETAILS SET
    COMMENT_COUNT =
        ISNULL((SELECT COUNT(C.ID)
            FROM component.COMMENT C
            WHERE AD.NEWSFEED_ITEM_ID = C.TARGET_ID
                 AND C.TARGET_TABLE = 'NEWSFEED_ITEM'
                 AND C.STATUS_TYPE_CODE = 'ACTIVE'
            GROUP BY C.TARGET_ID ), 0)
FROM #ACTIVITY_DETAILS AD

-- Set my comment counts
UPDATE #ACTIVITY_DETAILS SET
    MY_COMMENT_COUNT =
        ISNULL((SELECT COUNT(C.ID)
            FROM component.COMMENT C
            WHERE AD.NEWSFEED_ITEM_ID = C.TARGET_ID
                 AND C.TARGET_TABLE = 'NEWSFEED_ITEM'
                 AND C.STATUS_TYPE_CODE = 'ACTIVE'
                 AND C.PAX_ID = @paxId
            GROUP BY C.TARGET_ID ), 0)
FROM #ACTIVITY_DETAILS AD

-- Get details of most recent comments
INSERT INTO #RECENT_COMMENT
SELECT C1.TARGET_ID,
       C1.ID,
       C1.PAX_ID
FROM component.COMMENT C1
JOIN #ACTIVITY_DETAILS AD
ON AD.NEWSFEED_ITEM_ID = C1.TARGET_ID
JOIN (SELECT C2.TARGET_ID,
             MAX(C2.COMMENT_DATE) as COMMENT_DATE
      FROM component.COMMENT C2
      WHERE C2.TARGET_TABLE = 'NEWSFEED_ITEM'
        AND C2.STATUS_TYPE_CODE = 'ACTIVE'
      GROUP BY C2.TARGET_ID) C2
ON C1.TARGET_ID = C2.TARGET_ID
AND C1.COMMENT_DATE = C2.COMMENT_DATE

-- Set comment details
UPDATE #ACTIVITY_DETAILS SET
    COMMENT_ID = C.ID,
    COMMENT_DATE = C.COMMENT_DATE,
    COMMENT_TEXT = C.COMMENT
FROM #ACTIVITY_DETAILS AD
JOIN #RECENT_COMMENT RC
ON RC.NEWSFEED_ITEM_ID = AD.NEWSFEED_ITEM_ID
JOIN component.COMMENT C
ON C.ID = RC.COMMENT_ID
WHERE C.TARGET_TABLE = 'NEWSFEED_ITEM'

-- Set comment pax details
UPDATE #ACTIVITY_DETAILS SET
    COMMENT_PAX_ID = P.PAX_ID,
    COMMENT_PAX_LANGUAGE_CODE = P.LANGUAGE_CODE,
    COMMENT_PAX_PREFERRED_LOCALE = P.PREFERRED_LOCALE,
    COMMENT_PAX_CONTROL_NUM = P.CONTROL_NUM,
    COMMENT_PAX_FIRST_NAME = P.FIRST_NAME,
    COMMENT_PAX_MIDDLE_INITIAL = P.MIDDLE_INITIAL,
    COMMENT_PAX_LAST_NAME = P.LAST_NAME,
    COMMENT_PAX_NAME_PREFIX = P.NAME_PREFIX,
    COMMENT_PAX_NAME_SUFFIX = P.NAME_SUFFIX,
    COMMENT_PAX_COMPANY_NAME = P.COMPANY_NAME,
    COMMENT_PAX_STATUS = SU.STATUS_TYPE_CODE,
    COMMENT_PAX_JOB_TITLE = PGM.MISC_DATA
FROM #ACTIVITY_DETAILS AD
JOIN #RECENT_COMMENT RC ON RC.NEWSFEED_ITEM_ID = AD.NEWSFEED_ITEM_ID
JOIN component.PAX P ON P.PAX_ID = RC.PAX_ID
JOIN component.SYS_USER SU ON SU.PAX_ID = P.PAX_ID
JOIN component.PAX_GROUP PG ON PG.PAX_ID = P.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'

-- BEGIN MP-3060

--My Raised Count
UPDATE #ACTIVITY_DETAILS SET
  MY_RAISED_COUNT =
    ISNULL((SELECT COUNT(R.ID)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = AD.NOMINATION_ID AND N.SUBMITTER_PAX_ID = @paxId)
    AND R.STATUS_TYPE_CODE = 'APPROVED'),0)
FROM #ACTIVITY_DETAILS AD

--Count Raises
UPDATE #ACTIVITY_DETAILS SET
  RAISED_COUNT =
    ISNULL((SELECT COUNT(R.ID)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = AD.NOMINATION_ID)
    AND R.STATUS_TYPE_CODE = 'APPROVED'),0)
FROM #ACTIVITY_DETAILS AD

--Count Raises Amounts
UPDATE #ACTIVITY_DETAILS SET
  RAISED_POINT_TOTAL =
    ISNULL((SELECT SUM(R.AMOUNT)
    FROM component.RECOGNITION R
    WHERE R.NOMINATION_ID IN (
      SELECT
        N.ID
      FROM component.NOMINATION N
      WHERE N.PARENT_ID = AD.NOMINATION_ID)
    AND R.STATUS_TYPE_CODE = 'APPROVED' AND R.RECEIVER_PAX_ID = @paxId),0)
FROM #ACTIVITY_DETAILS AD

-- Determine if login pax have given raise
UPDATE #ACTIVITY_DETAILS SET
  GIVEN_RAISE =
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM component.NOMINATION N
        JOIN (
          SELECT
            R.NOMINATION_ID,
            R.STATUS_TYPE_CODE
          FROM component.RECOGNITION R
          WHERE R.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')) RAISE
        ON N.ID = RAISE.NOMINATION_ID
        WHERE N.SUBMITTER_PAX_ID = @paxId AND N.PARENT_ID = AD.NOMINATION_ID
      ) THEN 1
      ELSE 0
    END
FROM #ACTIVITY_DETAILS AD

CREATE TABLE #RAISED_PAX(
    PAX_ID BIGINT NOT NULL,
    NEWSFEED_NOMINATION_ID BIGINT NOT NULL
)

-- Get latest person who raised
INSERT INTO #RAISED_PAX
SELECT N.SUBMITTER_PAX_ID, N.PARENT_ID
FROM component.NOMINATION N
    JOIN (
      SELECT
        R.NOMINATION_ID,
        R.STATUS_TYPE_CODE
      FROM component.RECOGNITION R
      WHERE R.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')) RAISE
    ON N.ID = RAISE.NOMINATION_ID
JOIN #ACTIVITY_DETAILS AD ON N.PARENT_ID = AD.NOMINATION_ID
ORDER BY N.UPDATE_DATE

-- Set Raise Pax details
UPDATE #ACTIVITY_DETAILS SET
    RAISED_PAX_ID = P.PAX_ID,
    RAISED_PAX_LANGUAGE_CODE = P.LANGUAGE_CODE,
    RAISED_PAX_PREFERRED_LOCALE = P.PREFERRED_LOCALE,
    RAISED_PAX_CONTROL_NUM = P.CONTROL_NUM,
    RAISED_PAX_FIRST_NAME = P.FIRST_NAME,
    RAISED_PAX_MIDDLE_INITIAL = P.MIDDLE_INITIAL,
    RAISED_PAX_LAST_NAME = P.LAST_NAME,
    RAISED_PAX_NAME_PREFIX = P.NAME_PREFIX,
    RAISED_PAX_NAME_SUFFIX = P.NAME_SUFFIX,
    RAISED_PAX_COMPANY_NAME = P.COMPANY_NAME,
    RAISED_PAX_STATUS = SU.STATUS_TYPE_CODE,
    RAISED_PAX_JOB_TITLE = PGM.MISC_DATA,
    RAISED_PAX_IMAGES_VERSION = PI.VERSION
FROM #ACTIVITY_DETAILS AD
JOIN #RAISED_PAX RP on AD.NOMINATION_ID = RP.NEWSFEED_NOMINATION_ID
JOIN component.PAX P on P.PAX_ID = RP.PAX_ID
JOIN component.SYS_USER SU ON SU.PAX_ID = P.PAX_ID
JOIN component.PAX_GROUP PG ON PG.PAX_ID = P.PAX_ID
LEFT JOIN component.PAX_GROUP_MISC PGM ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID AND PGM.VF_NAME = 'JOB_TITLE'
LEFT JOIN component.PAX_IMAGES PI ON PI.PAX_ID = P.PAX_ID

-- GET IMAGE VERSION --
UPDATE    #ACTIVITY_DETAILS
SET
    TO_PAX_IMAGES_VERSION = to_pax_images.VERSION,
    FROM_PAX_IMAGES_VERSION = from_pax_images.VERSION,
    COMMENT_PAX_IMAGES_VERSION = comment_pax_images.VERSION
FROM    #ACTIVITY_DETAILS AD
LEFT JOIN component.PAX_IMAGES to_pax_images ON to_pax_images.PAX_ID = AD.TO_PAX_ID
LEFT JOIN component.PAX_IMAGES from_pax_images ON from_pax_images.PAX_ID = AD.FROM_PAX_ID
LEFT JOIN component.PAX_IMAGES comment_pax_images ON comment_pax_images.PAX_ID = AD.FROM_PAX_ID


--UPDATE RECOGNITION CRITERIA
DECLARE @recCriteria NVARCHAR(MAX);

SET @recCriteria = (
    SELECT '{"id":'+CONVERT(NVARCHAR,RC.ID)+', "displayName":"'+ RC.RECOGNITION_CRITERIA_NAME+'"},'
    FROM component.RECOGNITION_CRITERIA RC
    INNER JOIN component.NOMINATION_CRITERIA NC ON NC.RECOGNITION_CRITERIA_ID = RC.ID
    INNER JOIN #ACTIVITY_DETAILS AD ON AD.NOMINATION_ID = NC.NOMINATION_ID
    ORDER BY RC.ID
    FOR XML PATH('')
    )

IF @recCriteria IS NOT NULL
BEGIN
    SET @recCriteria = '['+LEFT(@recCriteria, LEN(@recCriteria) - 1)+']'

    UPDATE #ACTIVITY_DETAILS SET RECOGNITION_CRITERIA_JSON = @recCriteria
END



SELECT * FROM #ACTIVITY_DETAILS


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_SPECIAL_CHARACTER_REPLACEMENT]'
GO


/*==============================================================*/
/* View: VW_SPECIAL_CHARACTER_REPLACEMENT                       */
/*==============================================================*/
CREATE view [component].[VW_SPECIAL_CHARACTER_REPLACEMENT] as

SELECT 'À' AS 'SYMBOL',192 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Á' AS 'SYMBOL',193 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Â' AS 'SYMBOL',194 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Ã' AS 'SYMBOL',195 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Ä' AS 'SYMBOL',196 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Å' AS 'SYMBOL',197 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'Æ' AS 'SYMBOL',198 AS 'VALUE','ae' AS 'REPLACEMENT' UNION
SELECT 'Ç' AS 'SYMBOL',199 AS 'VALUE','c' AS 'REPLACEMENT' UNION
SELECT 'È' AS 'SYMBOL',200 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'É' AS 'SYMBOL',201 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'Ê' AS 'SYMBOL',202 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'Ë' AS 'SYMBOL',203 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'Ì' AS 'SYMBOL',204 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'Í' AS 'SYMBOL',205 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'Î' AS 'SYMBOL',206 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'Ï' AS 'SYMBOL',207 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'Ð' AS 'SYMBOL',208 AS 'VALUE','d' AS 'REPLACEMENT' UNION
SELECT 'Ñ' AS 'SYMBOL',209 AS 'VALUE','n' AS 'REPLACEMENT' UNION
SELECT 'Ò' AS 'SYMBOL',210 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'Ó' AS 'SYMBOL',211 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'Ô' AS 'SYMBOL',212 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'Õ' AS 'SYMBOL',213 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'Ö' AS 'SYMBOL',214 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT '×' AS 'SYMBOL',215 AS 'VALUE','x' AS 'REPLACEMENT' UNION
SELECT 'Ø' AS 'SYMBOL',216 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'Ù' AS 'SYMBOL',217 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'Ú' AS 'SYMBOL',218 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'Û' AS 'SYMBOL',219 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'Ü' AS 'SYMBOL',220 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'Ý' AS 'SYMBOL',221 AS 'VALUE','y' AS 'REPLACEMENT' UNION
SELECT 'à' AS 'SYMBOL',224 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'á' AS 'SYMBOL',225 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'â' AS 'SYMBOL',226 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'ã' AS 'SYMBOL',227 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'ä' AS 'SYMBOL',228 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'å' AS 'SYMBOL',229 AS 'VALUE','a' AS 'REPLACEMENT' UNION
SELECT 'æ' AS 'SYMBOL',230 AS 'VALUE','ae' AS 'REPLACEMENT' UNION
SELECT 'ç' AS 'SYMBOL',231 AS 'VALUE','c' AS 'REPLACEMENT' UNION
SELECT 'è' AS 'SYMBOL',232 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'é' AS 'SYMBOL',233 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'ê' AS 'SYMBOL',234 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'ë' AS 'SYMBOL',235 AS 'VALUE','e' AS 'REPLACEMENT' UNION
SELECT 'ì' AS 'SYMBOL',236 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'í' AS 'SYMBOL',237 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'î' AS 'SYMBOL',238 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'ï' AS 'SYMBOL',239 AS 'VALUE','i' AS 'REPLACEMENT' UNION
SELECT 'ð' AS 'SYMBOL',240 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ñ' AS 'SYMBOL',241 AS 'VALUE','n' AS 'REPLACEMENT' UNION
SELECT 'ò' AS 'SYMBOL',242 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ó' AS 'SYMBOL',243 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ô' AS 'SYMBOL',244 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'õ' AS 'SYMBOL',245 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ö' AS 'SYMBOL',246 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ø' AS 'SYMBOL',248 AS 'VALUE','o' AS 'REPLACEMENT' UNION
SELECT 'ù' AS 'SYMBOL',249 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'ú' AS 'SYMBOL',250 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'û' AS 'SYMBOL',251 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'ü' AS 'SYMBOL',252 AS 'VALUE','u' AS 'REPLACEMENT' UNION
SELECT 'ý' AS 'SYMBOL',253 AS 'VALUE','y' AS 'REPLACEMENT' UNION
SELECT 'ÿ' AS 'SYMBOL',255 AS 'VALUE','y' AS 'REPLACEMENT'

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UF_REPLACE_SPECIAL_CHARACTERS]'
GO


-- ==============================  NAME  ======================================
-- UF_REPLACE_SPECIAL_CHARACTERS
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A CHARACTER STRING, REPLACE ALL EXTENDED ASCII CHARACTERS WITH
-- BASIC ASCII CHARACTERS SO THEY CAN BE SEARCHED ON
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- HARWELLM        20150731                CREATED
--
-- ===========================  DECLARATIONS  =================================

CREATE FUNCTION [component].[UF_REPLACE_SPECIAL_CHARACTERS] (
    @string NVARCHAR(MAX)
)
RETURNS @result_set TABLE (
    token NVARCHAR(MAX) NULL)
AS
BEGIN

DECLARE @position INT
DECLARE @token NVARCHAR(MAX)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SET @token = @string

--Replace special characters one at a time
SELECT @token = REPLACE(@token, symbol, replacement) from [component].VW_SPECIAL_CHARACTER_REPLACEMENT


INSERT @result_set SELECT @token

-- RETURN RESULT SET
RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[PROGRAM_AWARD_TIERS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- RUN BEFORE RUNNING ALTER_PROGRAM_TIER_TABLE
UPDATE component.PROGRAM_AWARD_TIERS
SET ALLOW_RAISING = 0
WHERE ALLOW_RAISING IS NULL

ALTER TABLE [component].[PROGRAM_AWARD_TIERS] ALTER COLUMN [ALLOW_RAISING] [bit] NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[PROGRAM_FILES]'
GO
CREATE TABLE [component].[PROGRAM_FILES]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[PROGRAM_ID] [bigint] NOT NULL,
[FILES_ID] [bigint] NOT NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PROGRAM_IMAGES_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PROGRAM_IMAGES_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PROGRAM_IMAGES_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PROGRAM_IMAGES_UPDATE_ID] DEFAULT (user_name()),
[VERSION] [int] NULL CONSTRAINT [DF_PROGRAM_IMAGES_VERSION] DEFAULT ((1))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PROGRAM_FILES] on [component].[PROGRAM_FILES]'
GO
ALTER TABLE [component].[PROGRAM_FILES] ADD CONSTRAINT [PK_PROGRAM_FILES] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[LIKES]'
GO
ALTER TABLE [component].[LIKES] ADD CONSTRAINT [DF_LIKE_CREATE_ID] DEFAULT (user_name()) FOR [CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[LIKES] ADD CONSTRAINT [DF_LIKE_UPDATE_ID] DEFAULT (user_name()) FOR [UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[ACTIVITY_BUSINESS_RULE]'
GO
ALTER TABLE [component].[ACTIVITY_BUSINESS_RULE] ADD CONSTRAINT [FK_ACTIVITY_BUSINESS_RULE_PROGRAM_ACTIVITY] FOREIGN KEY ([PROGRAM_ACTIVITY_ID]) REFERENCES [component].[PROGRAM_ACTIVITY] ([PROGRAM_ACTIVITY_ID])
GO
ALTER TABLE [component].[ACTIVITY_BUSINESS_RULE] ADD CONSTRAINT [FK_ACTIVITY_BUSINESS_RULE_ACTIVITY_BUSINESS_RULE] FOREIGN KEY ([SUCCESSOR_ID]) REFERENCES [component].[ACTIVITY_BUSINESS_RULE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[BUSINESS_RULE]'
GO
ALTER TABLE [component].[BUSINESS_RULE] ADD CONSTRAINT [FK_BUSINESS_RULE_BUSINESS_RULE] FOREIGN KEY ([SUCCESSOR_ID]) REFERENCES [component].[BUSINESS_RULE] ([BUSINESS_RULE_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[COMMENT]'
GO
ALTER TABLE [component].[COMMENT] ADD CONSTRAINT [FK_COMMENT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PROGRAM_FILES]'
GO
ALTER TABLE [component].[PROGRAM_FILES] ADD CONSTRAINT [FK_PROGRAM_FILES_PROGRAM] FOREIGN KEY ([PROGRAM_ID]) REFERENCES [component].[PROGRAM_ACTIVITY] ([PROGRAM_ACTIVITY_ID])
GO
ALTER TABLE [component].[PROGRAM_FILES] ADD CONSTRAINT [FK_PROGRAM_FILES_FILES] FOREIGN KEY ([FILES_ID]) REFERENCES [component].[IMAGES] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NEWSFEED_ITEM]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM] ADD CONSTRAINT [FK_NEWSFEED_ITEM_NEWSFEED_ITEM] FOREIGN KEY ([PARENT_ID]) REFERENCES [component].[NEWSFEED_ITEM] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[Test]'
GO
ALTER TABLE [component].[Test] ADD CONSTRAINT [FK_TEST_RESUMETYPE] FOREIGN KEY ([ResumeTypeID]) REFERENCES [component].[ResumeType] ([ResumeTypeID])
GO
ALTER TABLE [component].[Test] ADD CONSTRAINT [FK_TEST_TESTTYPE] FOREIGN KEY ([TestTypeID]) REFERENCES [component].[TestType] ([TestTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_APPROVAL_PROCESS_CONFIG_D_1] on [component].[APPROVAL_PROCESS_CONFIG]'
GO


-- ==============================  NAME  ======================================
-- TR_APPROVAL_PROCESS_CONFIG_D_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
--
-- Supports: text, ntext, or image columns
--
-- Error:
--
-- Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables.
--
-- Documentation:
--
-- In a DELETE, INSERT, or UPDATE trigger, SQL Server does not allow text, ntext, or image column
-- references in the inserted and deleted tables if the compatibility level is set to 70. The text,
-- ntext, and image values in the inserted and deleted tables cannot be accessed. To retrieve the new
-- value in either an INSERT or UPDATE trigger, join the inserted table with the original update
-- table. When the compatibility level is 65 or lower, null values are returned for inserted or
-- deleted text, ntext, or image columns that allow null values; zero-length strings are returned if
-- the columns are not nullable.
--
-- If the compatibility level is 80 or higher, SQL Server allows the update of
-- text, ntext, or image columns through the INSTEAD OF trigger on tables or
-- views.
--
-- SYNTAX
-- sp_dbcmptlevel [ @dbname = ] name
--
-- The current compatibility level is 80.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- dohognta        20081201                Created
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_D_1]
ON [component].[APPROVAL_PROCESS_CONFIG]
INSTEAD OF DELETE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT

DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Set command (or operation) that this trigger is serving
-------------------------------------------------------------------
SET @operation = 'DELETE'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_APPROVAL_PROCESS_CONFIG (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    APPROVAL_PROCESS_CONFIG.ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.TARGET_ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_LEVEL
,    APPROVAL_PROCESS_CONFIG.APPROVAL_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.APPROVER_PAX_ID
,    APPROVAL_PROCESS_CONFIG.CREATE_DATE
,    APPROVAL_PROCESS_CONFIG.CREATE_ID
,    APPROVAL_PROCESS_CONFIG.UPDATE_DATE
,    APPROVAL_PROCESS_CONFIG.UPDATE_ID
)
SELECT
    @change_date
,    @change_id
,    @change_operation
,    APPROVAL_PROCESS_CONFIG.ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.TARGET_ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_LEVEL
,    APPROVAL_PROCESS_CONFIG.APPROVAL_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.APPROVER_PAX_ID
,    APPROVAL_PROCESS_CONFIG.CREATE_DATE
,    APPROVAL_PROCESS_CONFIG.CREATE_ID
,    APPROVAL_PROCESS_CONFIG.UPDATE_DATE
,    APPROVAL_PROCESS_CONFIG.UPDATE_ID
FROM
    DELETED
,    component.APPROVAL_PROCESS_CONFIG
WHERE    DELETED.ID = APPROVAL_PROCESS_CONFIG.ID

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''APPROVAL_PROCESS_CONFIG''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

DELETE    APPROVAL_PROCESS_CONFIG
FROM
    DELETED
,    component.APPROVAL_PROCESS_CONFIG
WHERE    DELETED.ID = APPROVAL_PROCESS_CONFIG.ID

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_APPROVAL_PROCESS_CONFIG_U_2] on [component].[APPROVAL_PROCESS_CONFIG]'
GO


-- ==============================  NAME  ======================================
-- TR_APPROVAL_PROCESS_CONFIG_U_2
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
--
-- Supports: text, ntext, or image columns
--
-- Error:
--
-- Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables.
--
-- Documentation:
--
-- In a DELETE, INSERT, or UPDATE trigger, SQL Server does not allow text, ntext, or image column
-- references in the inserted and deleted tables if the compatibility level is set to 70. The text,
-- ntext, and image values in the inserted and deleted tables cannot be accessed. To retrieve the new
-- value in either an INSERT or UPDATE trigger, join the inserted table with the original update
-- table. When the compatibility level is 65 or lower, null values are returned for inserted or
-- deleted text, ntext, or image columns that allow null values; zero-length strings are returned if
-- the columns are not nullable.
--
-- If the compatibility level is 80 or higher, SQL Server allows the update of
-- text, ntext, or image columns through the INSTEAD OF trigger on tables or
-- views.
--
-- SYNTAX
-- sp_dbcmptlevel  [ @dbname = ] name
--
-- The current compatibility level is 80.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- dohognta        20070207                Created
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_U_2]
ON [component].[APPROVAL_PROCESS_CONFIG]
INSTEAD OF UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT

DECLARE @change_date DATETIME
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Set command (or operation) that this trigger is serving
-------------------------------------------------------------------
SET @operation = 'UPDATE'

SET @change_date = GETDATE()

EXEC UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO HISTORY_APPROVAL_PROCESS_CONFIG (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    APPROVAL_PROCESS_CONFIG.ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.TARGET_ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_LEVEL
,    APPROVAL_PROCESS_CONFIG.APPROVAL_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.APPROVER_PAX_ID
,    APPROVAL_PROCESS_CONFIG.CREATE_DATE
,    APPROVAL_PROCESS_CONFIG.CREATE_ID
,    APPROVAL_PROCESS_CONFIG.UPDATE_DATE
,    APPROVAL_PROCESS_CONFIG.UPDATE_ID
)
SELECT
    @change_date
,    @change_id
,    @change_operation
,    APPROVAL_PROCESS_CONFIG.ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.TARGET_ID
,    APPROVAL_PROCESS_CONFIG.APPROVAL_LEVEL
,    APPROVAL_PROCESS_CONFIG.APPROVAL_TYPE_CODE
,    APPROVAL_PROCESS_CONFIG.APPROVER_PAX_ID
,    APPROVAL_PROCESS_CONFIG.CREATE_DATE
,    APPROVAL_PROCESS_CONFIG.CREATE_ID
,    APPROVAL_PROCESS_CONFIG.UPDATE_DATE
,    APPROVAL_PROCESS_CONFIG.UPDATE_ID
FROM
    INSERTED
,    APPROVAL_PROCESS_CONFIG
WHERE
    INSERTED.ID = APPROVAL_PROCESS_CONFIG.ID

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''APPROVAL_PROCESS_CONFIG''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

UPDATE    APPROVAL_PROCESS_CONFIG
SET
    APPROVAL_PROCESS_TYPE_CODE = INSERTED.APPROVAL_PROCESS_TYPE_CODE
,    TARGET_ID = INSERTED.TARGET_ID
,    APPROVAL_LEVEL = INSERTED.APPROVAL_LEVEL
,    APPROVAL_TYPE_CODE = INSERTED.APPROVAL_TYPE_CODE
,    APPROVER_PAX_ID = INSERTED.APPROVER_PAX_ID
,    CREATE_DATE = INSERTED.CREATE_DATE
,    CREATE_ID = INSERTED.CREATE_ID
,    UPDATE_DATE = INSERTED.UPDATE_DATE
,    UPDATE_ID = INSERTED.UPDATE_ID
FROM
    INSERTED
,    APPROVAL_PROCESS_CONFIG
WHERE
    INSERTED.ID = APPROVAL_PROCESS_CONFIG.ID

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_LIKE_DU_1] on [component].[LIKES]'
GO




ALTER TRIGGER [component].[TR_LIKE_DU_1]
ON [component].[LIKES]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT

DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_LIKES (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    LIKE_ID
,    PAX_ID
,   TARGET_TABLE
,    TARGET_ID
,    LIKE_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT
    @change_date
,    @change_id
,    @change_operation
,    LIKE_ID
,    PAX_ID
,   TARGET_TABLE
,    TARGET_ID
,    LIKE_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''LIKES''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO