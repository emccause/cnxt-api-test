CREATE SCHEMA [component] AUTHORIZATION [dbo]
GO

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = 'tenant_client')
CREATE LOGIN [tenant_client] WITH PASSWORD=N'n^2GGSlxO4', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

CREATE USER [tenant_client] FOR LOGIN [tenant_client] WITH DEFAULT_SCHEMA=[component]
GO

GRANT CONNECT TO [tenant_client] AS [dbo]
GO

EXEC sp_addrolemember 'db_owner', 'tenant_client';
