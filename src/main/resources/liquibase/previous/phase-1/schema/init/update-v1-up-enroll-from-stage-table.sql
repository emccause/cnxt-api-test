/****** Object:  StoredProcedure [component].[UP_ENROLL_FROM_STAGE_TABLE]    Script Date: 10/12/2015 13:46:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_ENROLL_FROM_STAGE_TABLE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A BATCH_ID
-- BATCH_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- RIDERAC1        20150522                CREATED
-- HARWELLM        20150729                ADDED SSO_ENABLED CHECK FOR NEW USER STATUS UPDATES
--                                        ADDED PAX_MISCS FOR PRIVACY AND RECOGNITION PREFERENCES
--                                        MODIFIED PAX_GROUP_MISC VF_NAME TO BE JOB_TITLE INSTEAD OF TITLE
-- HARWELLM        20150804                REPLACE ALL EMPTY STRINGS WITH NULL VALUES
--                                        ADD PEOPLE WITH DIRECT REPORTS TO THE 'MANAGER' GROUP
--                                        ADD NEW USERS TO THE 'ALL' GROUP
-- HARWELLM        20150805                DON'T PROCESS RECORDS WITH ERRORS
-- HARWELLM        20150810                WRITTEN/VERBAL RECOGNITION PREFERENCES SHOULD BE FALSE BY DEFAULT
--                                        CHANGED BATCH STATUS TO 'SUCCESS' INSTEAD OF 'COMPLETE'
--                                        REMOVE PAX FROM MANAGER GROUP IF ALL DIRECT REPORTS ARE REMOVED
-- HARWELLM        20150812                CREATE BATCH_EVENT RECORDS FOR FILENAME
-- HARWELLM        20150817                FIX FOR BATCH_EVENT OKRECS AND BATCH_STATUS
-- HARWELLM        20150818                BATCH_EVENT STATUSES SHOULD ALSO BE 'SUCCESS' INSTEAD OF 'COMPLETE'
--                                        FIXED ISSUE WITH INSERTING INTO THE MANAGER GROUP
--                                        ADDED COLUMN HEADERS TO ERROR FILE
-- HARWELLM        20150827                ADDED PREFERRED FLAG FOR ADDRESS AND PHONE
-- HARWELLM        20150921                CREATED NON-MANAGER ROLE-BASED GROUP
-- HARWELLM        20151001                ADDED FIELD LENGTH AND DATE FORMAT VALIDATIONS
-- HARWELLM        20151009                REMOVE NON-NUMERIC CHARACTERS FROM PHONE NUMBER
-- HARWELLM        20151012                ADDED NULL CHECKS ON PHONE/EXT ISNUMERIC CHECK, FIXED DATE LENGTHS
-- HARWELLM        20151021                REMOVE PAX FROM NON-MANAGER GROUP IF THEY GET NEW DIRECT REPORTS
--
-- ===========================  DECLARATIONS  =================================
--CREATE PROCEDURE [component].[UP_ENROLL_FROM_STAGE_TABLE]
ALTER PROCEDURE [component].[UP_ENROLL_FROM_STAGE_TABLE]
    @batch_id BIGINT
AS
BEGIN



--Create the FILE batch_event for this batch
INSERT INTO BATCH_EVENT (BATCH_ID, BATCH_EVENT_TYPE_CODE, MESSAGE, STATUS_TYPE_CODE)
SELECT DISTINCT @batch_id, 'FILE', FILENAME, 'SUCCESS' FROM BATCH_FILE WHERE BATCH_ID = @batch_id

/*
Do validations:
Make sure required fields exist
ERRROR for missing PARTICIPANT_ID, STATUS, PRIMARY_REPORT_ID, FIRST_NAME, LAST_NAME, LOGIN_ID, ADDRESS_line 1,
CITY, STATE, ZIP, COUNTRY_CODE
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '') + ';ParticipantID is blank', PAX_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (participant_id is null or len(participant_id) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Status is blank', SYS_USER_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (STATUS is null or len(STATUS) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PRIMARY_REPORT_ID is blank'
where batch_id = @batch_id and (PRIMARY_REPORT_ID is null or len(PRIMARY_REPORT_ID) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';FIRST_NAME is blank'
where batch_id = @batch_id and (FIRST_NAME is null or len(FIRST_NAME) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LAST_NAME is blank'
where batch_id = @batch_id and (LAST_NAME is null or len(LAST_NAME) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LOGIN_ID is blank', SYS_USER_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (LOGIN_ID is null or len(LOGIN_ID) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_line is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (ADDRESS_LINE_1 is null or len(ADDRESS_LINE_1) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CITY is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (CITY is null or len(CITY) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATE is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (STATE_PROVINCE_REGION is null or len(STATE_PROVINCE_REGION) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ZIP is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (ZIP_CODE is null or len(ZIP_CODE) = 0);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COUNTRY_CODE is blank', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and (COUNTRY_CODE is null or len(COUNTRY_CODE) = 0);

/* ERROR for wrong STATUS, either REPORT_ID does not exist, LOGIN_ID duplicate, EMAIL syntax, COUNTRY_CODE invalid
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Status is invalid'
where batch_id = @batch_id and STATUS not in ('A','R','S','I');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Duplicate login_id on file', SYS_USER_CHANGE_STATUS = 'E'
where LOGIN_ID in (
select login_id from (
select login_id, count(login_id) as cnt from stage_enrollment where BATCH_ID = @batch_id
group by login_id) cnt_qry
where cnt_qry.cnt > 1)
and BATCH_ID = @batch_id;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Email is invalid', EMAIL_CHANGE_STATUS = 'E'
where batch_id = @batch_id and EMAIL_ADDRESS not like '_%@__%.__%';

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Country_code is invalid', ADDRESS_CHANGE_STATUS = 'E'
where batch_id = @batch_id and COUNTRY_CODE not in (SELECT COUNTRY_CODE FROM COUNTRY);

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';Primary_report_to is not found'
where batch_id = @batch_id and upper(PRIMARY_REPORT_ID) <> 'TOP' and PRIMARY_REPORT_ID not in
(select CONTROL_NUM from PAX union select PARTICIPANT_ID from STAGE_ENROLLMENT where batch_id = @batch_id)

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';secondary_report_to is not found'
where batch_id = @batch_id and (SECONDARY_REPORT_ID is not null) and len(SECONDARY_REPORT_ID) > 0 and SECONDARY_REPORT_ID not in
(select CONTROL_NUM from PAX union select PARTICIPANT_ID from STAGE_ENROLLMENT where batch_id = @batch_id)


/* ERROR for invalid formats of hire(YYYY-MM-DD), termination(YYYY-MM-DD), birthDate(MM-DD)
*/
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';HIRE_DATE must be in YYYY-MM-DD format'
where batch_id = @batch_id and HIRE_DATE is not null
and ((ISDATE(HIRE_DATE) = 0) or HIRE_DATE NOT LIKE '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TERMINATION_DATE must be in YYYY-MM-DD format'
where batch_id = @batch_id and TERMINATION_DATE is not null
and ((ISDATE(TERMINATION_DATE) = 0) or TERMINATION_DATE NOT LIKE '[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]');

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';BIRTH_DATE must be in MM-DD format'
where batch_id = @batch_id and BIRTH_DATE is not null
and BIRTH_DATE NOT LIKE '[0-1][0-9]-[0-3][0-9]';

/****
Format all phone numbers to remove any non-numeric characters
MARS/ABS cannot accept phone numbers unless they are only numeric.

Right now we are removing these characters as well as empty spaces:
- ( ) , . / +
*/

--Remove formatting from phone number and phone extension
UPDATE STAGE_ENROLLMENT set PHONE_NUMBER =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PHONE_NUMBER, '+', ''), '/', ''), '-', ''), '(', ''), ')', ''), '.', ''), ',', ''), ' ', '')
WHERE batch_id = @batch_id

UPDATE STAGE_ENROLLMENT set PHONE_EXTENSION =
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(PHONE_EXTENSION, '+', ''), '/', ''), '-', ''), '(', ''), ')', ''), '.', ''), ',', ''), ' ', '')
WHERE batch_id = @batch_id

--Error if phone number or phone extension still contains non-numeric characters
UPDATE STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_NUMBER must be only numbers'
WHERE BATCH_ID = @batch_id and PHONE_NUMBER is not null and ISNUMERIC(PHONE_NUMBER) = 0

UPDATE STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_EXTENSION must be only numbers'
WHERE BATCH_ID = @batch_id and PHONE_EXTENSION is not null and ISNUMERIC(PHONE_EXTENSION) = 0



/* ERROR for data in column exceeds max character length
*/
--Column Lengths
DECLARE @len_controlNum INT SET @len_controlNum = 255
DECLARE @len_status INT SET @len_status = 1
DECLARE @len_firstName INT SET @len_firstName = 30
DECLARE @len_lastName INT SET @len_lastName = 40
DECLARE @len_middleInitial INT SET @len_middleInitial = 1
DECLARE @len_miscData INT SET @len_miscData = 255
DECLARE @len_sysUserName INT SET @len_sysUserName = 255
DECLARE @len_sysUserPassword INT SET @len_sysUserPassword = 255
DECLARE @len_emailAddress INT SET @len_emailAddress = 80
DECLARE @len_phone INT SET @len_phone = 15
DECLARE @len_phoneExt INT SET @len_phoneExt = 5
DECLARE @len_address INT SET @len_address = 40
DECLARE @len_city INT SET @len_city = 40
DECLARE @len_state INT SET @len_state = 20
DECLARE @len_zip INT SET @len_zip = 9
DECLARE @len_countryCode INT SET @len_countryCode = 3
DECLARE @len_birthDate INT SET @len_birthDate = 5
DECLARE @len_hireDate INT SET @len_hireDate = 10

--Length Validation
update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PARTICIPANT_ID exceeds max character count'
where batch_id = @batch_id and len(PARTICIPANT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATUS exceeds max character count'
where batch_id = @batch_id and len(STATUS) > @len_status;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PRIMARY_REPORT_ID exceeds max character count'
where batch_id = @batch_id and len(PRIMARY_REPORT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';SECONDARY_REPORT_ID exceeds max character count'
where batch_id = @batch_id and len(SECONDARY_REPORT_ID) > @len_controlNum;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';FIRST_NAME exceeds max character count'
where batch_id = @batch_id and len(FIRST_NAME) > @len_firstName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LAST_NAME exceeds max character count'
where batch_id = @batch_id and len(LAST_NAME) > @len_lastName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';MIDDLE_INITIAL exceeds max character count'
where batch_id = @batch_id and len(MIDDLE_INITIAL) > @len_middleInitial;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TITLE exceeds max character count'
where batch_id = @batch_id and len(TITLE) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COMPANY_NAME exceeds max character count'
where batch_id = @batch_id and len(COMPANY_NAME) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';LOGIN_ID exceeds max character count'
where batch_id = @batch_id and len(LOGIN_ID) > @len_sysUserName;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PASSWORD exceeds max character count'
where batch_id = @batch_id and len(PASSWORD) > @len_sysUserPassword;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';EMAIL_ADDRESS exceeds max character count'
where batch_id = @batch_id and len(EMAIL_ADDRESS) > @len_emailAddress;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_NUMBER exceeds max character count'
where batch_id = @batch_id and len(PHONE_NUMBER) > @len_phone;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';PHONE_EXTENSION exceeds max character count'
where batch_id = @batch_id and len(PHONE_EXTENSION) > @len_phoneExt;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_1 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_1) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_2 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_2) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_3 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_3) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_4 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_4) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ADDRESS_LINE_5 exceeds max character count'
where batch_id = @batch_id and len(ADDRESS_LINE_5) > @len_address;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CITY exceeds max character count'
where batch_id = @batch_id and len(CITY) > @len_city;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';STATE_PROVINCE_REGION exceeds max character count'
where batch_id = @batch_id and len(STATE_PROVINCE_REGION) > @len_state;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';ZIP_CODE exceeds max character count'
where batch_id = @batch_id and len(ZIP_CODE) > @len_zip;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COUNTRY_CODE exceeds max character count'
where batch_id = @batch_id and len(COUNTRY_CODE) > @len_countryCode;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';HIRE_DATE exceeds max character count'
where batch_id = @batch_id and len(HIRE_DATE) > @len_hireDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';TERMINATION_DATE exceeds max character count'
where batch_id = @batch_id and len(TERMINATION_DATE) > @len_hireDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';BIRTH_DATE exceeds max character count'
where batch_id = @batch_id and len(BIRTH_DATE) > @len_birthDate;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';DEPARTMENT exceeds max character count'
where batch_id = @batch_id and len(DEPARTMENT) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';COST_CENTER exceeds max character count'
where batch_id = @batch_id and len(COST_CENTER) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';AREA exceeds max character count'
where batch_id = @batch_id and len(AREA) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';GRADE exceeds max character count'
where batch_id = @batch_id and len(GRADE) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_1 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_1) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_2 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_2) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_3 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_3) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_4 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_4) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';CUSTOM_5 exceeds max character count'
where batch_id = @batch_id and len(CUSTOM_5) > @len_miscData;

update STAGE_ENROLLMENT set ENROLL_ERROR = ISNULL(ENROLL_ERROR, '')  + ';USER_FUNCTION exceeds max character count'
where batch_id = @batch_id and len(USER_FUNCTION) > @len_miscData;


/**********
Find any columns that have empty strings ('') and save them as NULL (except for the required columns already checked above)
(needed for all DATE columns, doing it to all columns for consistency)
Empty strings get saved as '1900-01-01' when converting them to dates
*/

update STAGE_ENROLLMENT set PRIMARY_REPORT_PAX_ID = NULL where BATCH_ID = @batch_id and len(PRIMARY_REPORT_PAX_ID) = 0
update STAGE_ENROLLMENT set SECONDARY_REPORT_ID = NULL where BATCH_ID = @batch_id and len(SECONDARY_REPORT_ID) = 0
update STAGE_ENROLLMENT set SECONDARY_REPORT_PAX_ID = NULL where BATCH_ID = @batch_id and len(SECONDARY_REPORT_PAX_ID) = 0
update STAGE_ENROLLMENT set MIDDLE_INITIAL = NULL where BATCH_ID = @batch_id and len(MIDDLE_INITIAL) = 0
update STAGE_ENROLLMENT set TITLE = NULL where BATCH_ID = @batch_id and len(TITLE) = 0
update STAGE_ENROLLMENT set COMPANY_NAME = NULL where BATCH_ID = @batch_id and len(COMPANY_NAME) = 0
update STAGE_ENROLLMENT set PASSWORD = NULL where BATCH_ID = @batch_id and len(PASSWORD) = 0
update STAGE_ENROLLMENT set EMAIL_ADDRESS = NULL where BATCH_ID = @batch_id and len(EMAIL_ADDRESS) = 0
update STAGE_ENROLLMENT set PHONE_NUMBER = NULL where BATCH_ID = @batch_id and len(PHONE_NUMBER) = 0
update STAGE_ENROLLMENT set PHONE_EXTENSION = NULL where BATCH_ID = @batch_id and len(PHONE_EXTENSION) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_2 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_2) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_3 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_3) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_4 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_4) = 0
update STAGE_ENROLLMENT set ADDRESS_LINE_5 = NULL where BATCH_ID = @batch_id and len(ADDRESS_LINE_5) = 0
update STAGE_ENROLLMENT set HIRE_DATE = NULL where BATCH_ID = @batch_id and len(HIRE_DATE) = 0
update STAGE_ENROLLMENT set TERMINATION_DATE = NULL where BATCH_ID = @batch_id and len(TERMINATION_DATE) = 0
update STAGE_ENROLLMENT set BIRTH_DATE = NULL where BATCH_ID = @batch_id and len(BIRTH_DATE) = 0
update STAGE_ENROLLMENT set DEPARTMENT = NULL where BATCH_ID = @batch_id and len(DEPARTMENT) = 0
update STAGE_ENROLLMENT set COST_CENTER = NULL where BATCH_ID = @batch_id and len(COST_CENTER) = 0
update STAGE_ENROLLMENT set AREA = NULL where BATCH_ID = @batch_id and len(AREA) = 0
update STAGE_ENROLLMENT set GRADE = NULL where BATCH_ID = @batch_id and len(GRADE) = 0
update STAGE_ENROLLMENT set CUSTOM_1 = NULL where BATCH_ID = @batch_id and len(CUSTOM_1) = 0
update STAGE_ENROLLMENT set CUSTOM_2 = NULL where BATCH_ID = @batch_id and len(CUSTOM_2) = 0
update STAGE_ENROLLMENT set CUSTOM_3 = NULL where BATCH_ID = @batch_id and len(CUSTOM_3) = 0
update STAGE_ENROLLMENT set CUSTOM_4 = NULL where BATCH_ID = @batch_id and len(CUSTOM_4) = 0
update STAGE_ENROLLMENT set CUSTOM_5 = NULL where BATCH_ID = @batch_id and len(CUSTOM_5) = 0
update STAGE_ENROLLMENT set USER_FUNCTION = NULL where BATCH_ID = @batch_id and len(USER_FUNCTION) = 0

/**********
 Find pax_ID, pax_group_id and find out if address, email, phone, sys_user need to be added/updated
 */
update STAGE_ENROLLMENT set PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PARTICIPANT_ID)
where BATCH_ID = @batch_id
and ENROLL_ERROR is null;

update se
set PAX_GROUP_ID = pg.pax_group_id
from STAGE_ENROLLMENT se
join PAX_GROUP pg on pg.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id;

update se
set se.ADDRESS_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join ADDRESS ad on ad.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)
and (select 1 from address a where se.pax_id =  a.pax_id and a.ADDRESS_TYPE_CODE = 'BUS'
and (a.ADDRESS1 <> se.ADDRESS_LINE_1 OR a.ADDRESS2 <> se.ADDRESS_LINE_2 OR a.ADDRESS3 <> se.ADDRESS_LINE_3 OR
a.ADDRESS4 <> se.ADDRESS_LINE_4 OR a.ADDRESS4 <> se.ADDRESS_LINE_4 OR a.CITY <> se.CITY OR
a.STATE <> se.STATE_PROVINCE_REGION OR a.ZIP <> se.ZIP_CODE or a.COUNTRY_CODE <> se.COUNTRY_CODE
))=1

update se
set se.ADDRESS_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)
and se.PAX_ID not in
(SELECT pax_ID from address a where se.pax_id =  a.pax_id and a.ADDRESS_TYPE_CODE = 'BUS')

update se
set se.ADDRESS_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.ADDRESS_CHANGE_STATUS <> 'E' or se.ADDRESS_CHANGE_STATUS is null)

update se
set se.EMAIL_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join email em on em.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and em.EMAIL_TYPE_CODE = 'BUS'
and em.EMAIL_ADDRESS <> se.EMAIL_ADDRESS
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null)

update se
set se.EMAIL_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null)
and se.PAX_ID not in
(SELECT pax_ID from EMAIL em where se.pax_id = em.pax_id and em.EMAIL_TYPE_CODE = 'BUS')

update se
set se.EMAIL_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.EMAIL_CHANGE_STATUS <> 'E' or se.EMAIL_CHANGE_STATUS is null);

--select * from STAGE_ENROLLMENT

update se
set se.PHONE_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join phone ph on ph.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and ph.PHONE_TYPE_CODE = 'BUS'
and (ph.PHONE <> se.PHONE_NUMBER
    OR ph.PHONE_EXT <> se.PHONE_EXTENSION)


update se
set se.PHONE_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and se.PAX_ID not in
(SELECT pax_ID from PHONE ph where se.pax_id =  ph.pax_id and ph.PHONE_TYPE_CODE = 'BUS')

update se
set se.PHONE_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id;

update se
set se.PAX_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join PAX p on p.pax_id = se.pax_id
where se.PAX_ID is not null
and se.ENROLL_ERROR is null
and (se.PAX_CHANGE_STATUS <> 'E' or se.PAX_CHANGE_STATUS is null)
and BATCH_ID = @batch_id
and (p.FIRST_NAME <> se.FIRST_NAME
    OR p.LAST_NAME <> se.LAST_NAME
    OR p.MIDDLE_INITIAL <> se.MIDDLE_INITIAL
    )

update se
set se.PAX_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and (se.PAX_CHANGE_STATUS <> 'E' or se.PAX_CHANGE_STATUS is null)
and BATCH_ID = @batch_id;

update se
set se.PAX_GROUP_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join PAX_GROUP p on p.pax_id = se.pax_id
where se.PAX_ID is not null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and se.TERMINATION_DATE is not null
and p.thru_date is null

update se
set se.PAX_GROUP_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id;


--Check ssoEnabled flag to determine who gets set to NEW status (This is for new users only)
IF (select UPPER(value_1) from component.application_data where key_name = '/app/project_profile/sso_enabled') = 'TRUE'
BEGIN
    --ssoEnabled is TRUE. Only set users to NEW if they do not have an email (emails on the enrollment file are BUS by default)

    --New Users
    UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'NEW' where STATUS = 'A' and BATCH_ID = @batch_id and PAX_ID is null and EMAIL_ADDRESS is null;

    --Existing Users (need to stay NEW unless they send an email)
    UPDATE se set STATUS_TYPE_CODE = 'NEW'
    from STAGE_ENROLLMENT se
    join sys_user su on su.pax_id = se.pax_id
    where se.PAX_ID is not null
    and se.BATCH_ID = @batch_id
    and se.STATUS = 'A'
    and se.EMAIL_ADDRESS is null
    and su.STATUS_TYPE_CODE = 'NEW';
END
ELSE
BEGIN
    --ssoEnabled is FALSE. All active users who do not have a pax_id yet get set to NEW

    --New Users
    UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'NEW' where STATUS = 'A' and BATCH_ID = @batch_id and PAX_ID is null;

    --Existing Users (need to stay NEW)
    UPDATE se set STATUS_TYPE_CODE = 'NEW'
    from STAGE_ENROLLMENT se
    join sys_user su on su.pax_id = se.pax_id
    where se.PAX_ID is not null
    and se.BATCH_ID = @batch_id
    and se.STATUS = 'A'
    and su.STATUS_TYPE_CODE = 'NEW';
END

UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'ACTIVE' where STATUS = 'A' and BATCH_ID = @batch_id and STATUS_TYPE_CODE is null;
UPDATE STAGE_ENROLLMENT set STATUS_TYPE_CODE = 'INACTIVE' where STATUS = 'I' and BATCH_ID = @batch_id;

update se
set se.SYS_USER_CHANGE_STATUS = 'U'
from STAGE_ENROLLMENT se
join sys_user su on su.pax_id = se.pax_id
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null)
and se.ENROLL_ERROR is null
and (su.SYS_USER_NAME <> se.LOGIN_ID
    OR su.STATUS_TYPE_CODE <> se.STATUS_TYPE_CODE)


update se
set se.SYS_USER_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is not null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null)
and se.ENROLL_ERROR is null
and se.PAX_ID not in
(SELECT pax_ID from sys_user su where se.pax_id = su.pax_id)

update se
set se.SYS_USER_CHANGE_STATUS = 'A'
from STAGE_ENROLLMENT se
where se.PAX_ID is null
and se.ENROLL_ERROR is null
and BATCH_ID = @batch_id
and (se.SYS_USER_CHANGE_STATUS <> 'E' or se.SYS_USER_CHANGE_STATUS is null);

insert into pax (LANGUAGE_CODE, PREFERRED_LOCALE, CONTROL_NUM, FIRST_NAME, MIDDLE_INITIAL, LAST_NAME)
select 'en','en_US', PARTICIPANT_ID, FIRST_NAME, MIDDLE_INITIAL, LAST_NAME from STAGE_ENROLLMENT se
where batch_id = @batch_id and PAX_CHANGE_STATUS = 'A';

--reinsert the pax_id's
update STAGE_ENROLLMENT set PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PARTICIPANT_ID)
where BATCH_ID = @batch_id and PAX_ID is null;

insert into pax_GROUP (PAX_ID, ORGANIZATION_CODE, ROLE_CODE, FROM_DATE)
select PAX_ID, 'RECG','RECG', getdate() from STAGE_ENROLLMENT se
where batch_id = @batch_id and PAX_GROUP_CHANGE_STATUS = 'A';

--insert the new pax_group_ids
update se
set PAX_GROUP_ID = pg.pax_group_id
from STAGE_ENROLLMENT se
join PAX_GROUP pg on pg.pax_id = se.pax_id
where se.PAX_GROUP_ID is null
and BATCH_ID = @batch_id;

--update any pax infromation that needs to be updated
update p
set p.FIRST_NAME = se.FIRST_NAME
    , p.LAST_NAME = se.LAST_NAME
    , p.MIDDLE_INITIAL = se.MIDDLE_INITIAL
from PAX p
JOIN STAGE_ENROLLMENT se on p.pax_id = se.pax_id
where
se.PAX_CHANGE_STATUS = 'U'

update pg
set pg.THRU_DATE = se.TERMINATION_DATE
from PAX_GROUP pg
join STAGE_ENROLLMENT se on pg.pax_group_id = se.pax_group_id
where BATCH_ID = @batch_id
and se.PAX_GROUP_CHANGE_STATUS = 'U'


-- Do the adds and updates for sys_user
INSERT into SYS_USER(PAX_ID, SYS_USER_NAME, SYS_USER_PASSWORD, STATUS_TYPE_CODE)
SELECT PAX_ID, LOGIN_ID, PASSWORD, STATUS_TYPE_CODE
from STAGE_ENROLLMENT se
where BATCH_ID =@batch_id
and se.SYS_USER_CHANGE_STATUS = 'A';

update su
set su.SYS_USER_NAME = se.LOGIN_ID
    ,su.STATUS_TYPE_CODE = se.STATUS_TYPE_CODE
from sys_user su
join STAGE_ENROLLMENT se on su.pax_id = se.pax_id
where BATCH_ID = @batch_id
and se.SYS_USER_CHANGE_STATUS = 'U'

--Do the add and update for address
insert into ADDRESS(pax_id, ADDRESS_TYPE_CODE, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ADDRESS5,
CITY, STATE, ZIP, COUNTRY_CODE, PREFERRED)
select se.PAX_ID, 'BUS', se.ADDRESS_LINE_1, se.ADDRESS_LINE_2, se.ADDRESS_LINE_3, se.ADDRESS_LINE_4, se.ADDRESS_LINE_5,
se.CITY, se.STATE_PROVINCE_REGION, se.ZIP_CODE, se.COUNTRY_CODE, 'Y'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and  se.ADDRESS_CHANGE_STATUS = 'A';

update a
set a.ADDRESS1 = se.ADDRESS_LINE_1, a.ADDRESS2 = se.ADDRESS_LINE_2, a.ADDRESS3 = se.ADDRESS_LINE_3,
a.ADDRESS4 = se.ADDRESS_LINE_4, a.ADDRESS5 = se.ADDRESS_LINE_5, a.CITY = se.CITY,
a.STATE = se.STATE_PROVINCE_REGION, a.ZIP = se.ZIP_CODE, a.COUNTRY_CODE = se.COUNTRY_CODE
from ADDRESS a
join STAGE_ENROLLMENT se on a.pax_id = se.pax_id
where se.ADDRESS_CHANGE_STATUS = 'U'
and a.ADDRESS_TYPE_CODE = 'BUS'
and BATCH_ID = @batch_id

--Do the Add and update for email
insert into email(PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS,PREFERRED)
select se.PAX_ID, 'BUS', se.EMAIL_ADDRESS, 'Y'
from STAGE_ENROLLMENT se
where BATCH_ID = @batch_id
and se.EMAIL_CHANGE_STATUS = 'A'
and se.EMAIL_ADDRESS is not null;

update em
set em.EMAIL_ADDRESS = se.EMAIL_ADDRESS
from email em
join STAGE_ENROLLMENT se on em.pax_id = se.pax_id
where BATCH_ID = @batch_id
and em.EMAIL_TYPE_CODE = 'BUS'
and se.EMAIL_CHANGE_STATUS = 'U';

--Do the add and update for phone
insert into phone(PAX_ID, PHONE_TYPE_CODE, PHONE, PHONE_EXT, PREFERRED)
select se.PAX_ID, 'BUS', se.PHONE_NUMBER, se.PHONE_EXTENSION, 'Y'
from STAGE_ENROLLMENT se
where se.PHONE_CHANGE_STATUS = 'A'
and se.PHONE_NUMBER is not null
and BATCH_ID = @batch_id

update ph
set ph.PHONE = se.PHONE_NUMBER
    ,ph.PHONE_EXT = se.PHONE_EXTENSION
from phone ph
join STAGE_ENROLLMENT se
on ph.pax_id = se.pax_id
where se.PHONE_CHANGE_STATUS = 'U'
and BATCH_ID = @batch_id
and ph.PHONE_TYPE_CODE = 'BUS'

--update/insert/delete all the misc column data
/* PAX_MISC - BIRTH_DATE, HIRE_DATE */

--BIRTH_DAY
delete PAX_MISC
where VF_NAME = 'BIRTH_DAY'
and pax_id in (select pax_id from STAGE_ENROLLMENT
where batch_id = @batch_id and BIRTH_DATE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.BIRTH_DATE
from PAX_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_ID = se.PAX_ID
and pm.VF_NAME = 'BIRTH_DAY'
where batch_id = @batch_id
and BIRTH_DATE is not null
and ENROLL_ERROR is null

insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'BIRTH_DAY', BIRTH_DATE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and BIRTH_DATE is not null
and ENROLL_ERROR is null
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'BIRTH_DAY')


--HIRE_DATE
delete PAX_MISC
where VF_NAME = 'HIRE_DATE'
and pax_id in (select pax_id from STAGE_ENROLLMENT
where batch_id = @batch_id and HIRE_DATE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.HIRE_DATE
from PAX_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_ID = se.PAX_ID
and pm.VF_NAME = 'HIRE_DATE'
where batch_id = @batch_id
and HIRE_DATE is not null
and ENROLL_ERROR is null

insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'HIRE_DATE', HIRE_DATE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and HIRE_DATE is not null
and ENROLL_ERROR is null
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'HIRE_DATE')


--For new users, set up their privacy/recognition preferences to default to TRUE
/* SHARE_SA, SHARE_REC, RECEIVE_EMAIL_REC, VERBAL_REC_1, VERBAL_REC_2, VERBAL_REC_3, WRITTEN_REC_1, WRITTEN_REC_2 */

--SHARE_SA (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'SHARE_SA', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'SHARE_SA')

--SHARE_REC (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'SHARE_REC', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'SHARE_REC')

--RECEIVE_EMAIL_REC (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'RECEIVE_EMAIL_REC', 'true', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'RECEIVE_EMAIL_REC')

--VERBAL_REC_1 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_1', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_1')

--VERBAL_REC_2 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_2', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_2')

--VERBAL_REC_3 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'VERBAL_REC_3', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'VERBAL_REC_3')

--WRITTEN_REC_1 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'WRITTEN_REC_1', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'WRITTEN_REC_1')

--WRITTEN_REC_2 (Insert default of TRUE for new users)
insert into PAX_MISC(PAX_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_ID, 'WRITTEN_REC_2', 'false', getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and PAX_CHANGE_STATUS = 'A'
and pax_id not in
(SELECT PAX_ID from PAX_MISC where VF_NAME = 'WRITTEN_REC_2')



/* PAX_GROUP_MISC - JOB_TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, AREA, GRADE, FUNCTION,
CUSTOM 1-5*/
delete PAX_GROUP_MISC
where VF_NAME = 'JOB_TITLE'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and TITLE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.TITLE
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'JOB_TITLE'
where batch_id = @batch_id
and TITLE is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'JOB_TITLE', TITLE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and TITLE is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'JOB_TITLE')

delete PAX_GROUP_MISC
where VF_NAME = 'COMPANY_NAME'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and COMPANY_NAME is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.COMPANY_NAME
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'COMPANY_NAME'
where batch_id = @batch_id
and COMPANY_NAME is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'COMPANY_NAME', COMPANY_NAME, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and COMPANY_NAME is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'COMPANY_NAME')

--DEPARTMENT
delete PAX_GROUP_MISC
where VF_NAME = 'DEPARTMENT'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and DEPARTMENT is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.DEPARTMENT
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'DEPARTMENT'
where batch_id = @batch_id
and DEPARTMENT is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'DEPARTMENT', DEPARTMENT, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and DEPARTMENT is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'DEPARTMENT')

--COST_CENTER
delete PAX_GROUP_MISC
where VF_NAME = 'COST_CENTER'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and COST_CENTER is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.COST_CENTER
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'COST_CENTER'
where batch_id = @batch_id
and COST_CENTER is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'COST_CENTER', COST_CENTER, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and COST_CENTER is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'COST_CENTER')

--AREA
delete PAX_GROUP_MISC
where VF_NAME = 'AREA'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and AREA is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.AREA
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'AREA'
where batch_id = @batch_id
and AREA is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'AREA', AREA, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and AREA is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'AREA')

--GRADE
delete PAX_GROUP_MISC
where VF_NAME = 'GRADE'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and GRADE is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.GRADE
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'GRADE'
where batch_id = @batch_id
and GRADE is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'GRADE', GRADE, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and GRADE is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'GRADE')

--FUNCTION
delete PAX_GROUP_MISC
where VF_NAME = 'FUNCTION'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and [USER_FUNCTION] is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.USER_FUNCTION
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'FUNCTION'
where batch_id = @batch_id
and [USER_FUNCTION] is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'FUNCTION', [USER_FUNCTION], getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and [USER_FUNCTION] is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'FUNCTION')

--CUSTOM_1
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_1'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_1 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_1
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_1'
where batch_id = @batch_id
and CUSTOM_1 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_1', CUSTOM_1, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_1 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_1')

--CUSTOM_2
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_2'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_2 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_2
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_2'
where batch_id = @batch_id
and CUSTOM_2 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_2', CUSTOM_2, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_2 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_2')

--CUSTOM_3
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_3'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_3 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_3
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_3'
where batch_id = @batch_id
and CUSTOM_3 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_3', CUSTOM_3, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_3 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_3')

--CUSTOM_4
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_4'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_4 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_4
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_4'
where batch_id = @batch_id
and CUSTOM_4 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_4', CUSTOM_4, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_4 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_4')

--CUSTOM_5
delete PAX_GROUP_MISC
where VF_NAME = 'CUSTOM_5'
and PAX_GROUP_ID in (select pax_group_id from STAGE_ENROLLMENT
where batch_id = @batch_id and CUSTOM_5 is null and ENROLL_ERROR is null)

update pm
set pm.MISC_DATA = se.CUSTOM_5
from PAX_GROUP_MISC pm
join STAGE_ENROLLMENT se on pm.PAX_GROUP_ID = se.PAX_GROUP_ID
and pm.VF_NAME = 'CUSTOM_5'
where batch_id = @batch_id
and CUSTOM_5 is not null
and ENROLL_ERROR is null

insert into PAX_GROUP_MISC(PAX_GROUP_ID, VF_NAME, MISC_DATA, FROM_DATE)
SELECT PAX_GROUP_ID, 'CUSTOM_5', CUSTOM_5, getdate()
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and CUSTOM_5 is not null
and ENROLL_ERROR is null
and PAX_GROUP_ID not in
(SELECT PAX_GROUP_ID from PAX_GROUP_MISC where VF_NAME = 'CUSTOM_5')

--Determine the PAX_ID's for the REPORT_TO and SECONDARY_REPORT_TO
update STAGE_ENROLLMENT set PRIMARY_REPORT_PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = PRIMARY_REPORT_ID)
where BATCH_ID = @batch_id;

update STAGE_ENROLLMENT set SECONDARY_REPORT_PAX_ID =
(Select p.PAX_ID from PAX p where p.CONTROL_NUM = SECONDARY_REPORT_ID)
where BATCH_ID = @batch_id;


--save relationships (manager and secondary manager)
update r
set r.pax_id_2 = se.PRIMARY_REPORT_PAX_ID
from RELATIONSHIP r
join STAGE_ENROLLMENT se
on se.pax_id = r.pax_id_1 and r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null

insert into RELATIONSHIP(PAX_ID_1,PAX_ID_2,RELATIONSHIP_TYPE_CODE)
SELECT se.PAX_ID, se.PRIMARY_REPORT_PAX_ID, 'REPORT_TO'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.pax_id not in
(select pax_id_1 from RELATIONSHIP where RELATIONSHIP_TYPE_CODE = 'REPORT_TO')

update r
set r.pax_id_2 = se.SECONDARY_REPORT_PAX_ID
from RELATIONSHIP r
join STAGE_ENROLLMENT se
on se.pax_id = r.pax_id_1 and r.RELATIONSHIP_TYPE_CODE = 'SECONDARY_REPORT'
where batch_id = @batch_id
and se.SECONDARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null

insert into RELATIONSHIP(PAX_ID_1,PAX_ID_2,RELATIONSHIP_TYPE_CODE)
SELECT se.PAX_ID, se.SECONDARY_REPORT_PAX_ID, 'SECONDARY_REPORT'
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.SECONDARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.pax_id not in
(select pax_id_1 from RELATIONSHIP where RELATIONSHIP_TYPE_CODE = 'SECONDARY_REPORT')


--Insert people with direct reports into the 'Managers' group
DECLARE @managerGroupId BIGINT
SELECT @managerGroupId = GROUP_ID from component.groups where group_desc = 'Managers' and GROUP_TYPE_CODE = 'ROLE_BASED'

insert into GROUPS_PAX(GROUP_ID, PAX_ID)
SELECT DISTINCT @managerGroupId, se.PRIMARY_REPORT_PAX_ID
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PRIMARY_REPORT_PAX_ID is not null
and ENROLL_ERROR is null
and se.PRIMARY_REPORT_PAX_ID not in
(select pax_id from groups_pax
where group_id = @managerGroupId)


--Get a list of all pax_ids that are currently managers
CREATE TABLE #managerIds (
    PAX_ID BIGINT NOT NULL
)

INSERT INTO #managerIds
SELECT DISTINCT PAX_ID_2 FROM component.RELATIONSHIP WHERE RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND PAX_ID_2 IS NOT NULL


--Remove people from the 'Manager' group if they no longer have any direct reports
DELETE FROM component.GROUPS_PAX WHERE GROUP_ID = @managerGroupId AND PAX_ID IN (
SELECT PAX_ID FROM component.PAX where PAX_ID NOT IN (SELECT * FROM #managerIds))


--Insert anyone that isn't a manager into the 'Non-Managers' group
DECLARE @nonManagerGroupId BIGINT
SELECT @nonManagerGroupId = GROUP_ID from component.groups where group_desc = 'Non-Managers' and GROUP_TYPE_CODE = 'ROLE_BASED'

INSERT INTO GROUPS_PAX (GROUP_ID, PAX_ID)
SELECT @nonManagerGroupId, PAX_ID
FROM component.PAX
WHERE PAX_ID NOT IN (SELECT * FROM #managerIds)
AND PAX_ID NOT IN
(SELECT PAX_ID FROM component.GROUPS_PAX
WHERE GROUP_ID = @nonManagerGroupId)


--Remove people from the 'Non-Manager' group if they have new direct reports
DELETE FROM component.GROUPS_PAX WHERE GROUP_ID = @nonManagerGroupId AND PAX_ID IN (SELECT * FROM #managerIds)


--Insert new PAX into the 'ALL' group
DECLARE @allGroupId BIGINT
SELECT @allGroupId = GROUP_ID from component.groups where group_desc = 'Participants'

insert into GROUPS_PAX(GROUP_ID, PAX_ID)
SELECT @allGroupId, se.PAX_ID
from STAGE_ENROLLMENT se
where batch_id = @batch_id
and se.PAX_ID is not null
and se.PAX_CHANGE_STATUS = 'A'



--Remove the leading semi-colon from any row with an error
Update STAGE_ENROLLMENT set ENROLL_ERROR = SUBSTRING(ENROLL_ERROR,2,len(ENROLL_ERROR)-1)
where ENROLL_ERROR is not NULL and @batch_id = BATCH_ID;

--CREATE BATCH_EVENT RECORDS for RECS, OKRECS, ERRS
INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select BATCH_ID, 'RECS', 'SUCCESS', count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id group by BATCH_ID;

DECLARE @okrecs INTEGER
DECLARE @errs INTEGER
SELECT @okrecs = count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id and ENROLL_ERROR is NULL
SELECT @errs = count(BATCH_ID) from STAGE_ENROLLMENT where BATCH_ID = @batch_id and ENROLL_ERROR is not NULL

INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select DISTINCT BATCH_ID, 'OKRECS', 'SUCCESS', @okrecs from STAGE_ENROLLMENT where BATCH_ID = @batch_id;

INSERT INTO BATCH_EVENT(BATCH_ID, BATCH_EVENT_TYPE_CODE, STATUS_TYPE_CODE, MESSAGE)
select DISTINCT BATCH_ID, 'ERRS', 'SUCCESS', @errs from STAGE_ENROLLMENT where BATCH_ID = @batch_id;


--CREATE BATCH_FILE Record for the error file
--SELECT * from BATCH_FILE where batch_id = 77;
create table #errList (
BATCH_ID bigint,
ERROR_DATA nvarchar(2000)
);

insert into #errList(BATCH_ID,ERROR_DATA)
select BATCH_ID, '"'+isnull([STATUS],' ')+ '","'+isnull(PARTICIPANT_ID,' ')+ '","'+isnull(PRIMARY_REPORT_ID,' ')
+'","'+isnull(SECONDARY_REPORT_ID,' ')+
 '","'+isnull(FIRST_NAME,' ')+'","'+isnull(LAST_NAME,' ')+'","'+isnull(MIDDLE_INITIAL,' ')+'","'+isnull(TITLE,' ')+
 '","'+isnull(COMPANY_NAME,' ')+'","'+isnull(LOGIN_ID,' ')+'","'+isnull(PASSWORD,' ')+'","'+isnull(EMAIL_ADDRESS,' ')+
 '","'+isnull(PHONE_NUMBER,' ')+'","'+isnull(PHONE_EXTENSION,' ')+'","'+isnull(ADDRESS_LINE_1,' ')+'","'+isnull(ADDRESS_LINE_2,' ')+
 '","'+isnull(ADDRESS_LINE_3,' ')+'","'+isnull(ADDRESS_LINE_4,' ')+'","'+isnull(ADDRESS_LINE_5,' ')+'","'+isnull(CITY,' ')+
 '","'+isnull(STATE_PROVINCE_REGION,' ')+'","'+isnull(ZIP_CODE,' ')+'","'+isnull(COUNTRY_CODE,' ')+'","'+isnull(HIRE_DATE,' ')+
 '","'+isnull(TERMINATION_DATE,' ')+'","'+isnull(BIRTH_DATE,' ')+'","'+isnull(DEPARTMENT,' ')+'","'+isnull(COST_CENTER,' ')+
 '","'+isnull(AREA,' ')+'","'+isnull(GRADE,' ')+'","'+isnull(USER_FUNCTION,' ')+'","'+isnull(CUSTOM_1,' ')+
 '","'+isnull(CUSTOM_2,' ')+'","'+isnull(CUSTOM_3,' ')+'","'+isnull(CUSTOM_4,' ')+'","'+isnull(CUSTOM_5,' ')+
 '","'+ENROLL_ERROR+
 '"' from STAGE_ENROLLMENT
 where batch_id = @batch_id
 and ENROLL_ERROR is not null;

DECLARE @EROXR NVARCHAR(MAX)
SELECT @EROXR = COALESCE(@EROXR + CHAR(13)+CHAR(10), '') + ERROR_DATA
FROM #errList

--Add Column Headers to the error file
DECLARE @columnHeaders NVARCHAR(MAX)
SET @columnHeaders = '"Participant Status","Participant Id","Primary Report To ID","Secondary Report To ID","First Name","Last Name","Middle Name","Title","Company Name","Login Id","Password","Email Address","Phone Number","Phone Extension","Primary Address Line 1","Address Line 2","Address Line 3","Address Line 4","Address Line 5","City","State/Province/Region","Zip/Postal Code","Country Code","Hire Date","Termination Date","Birthdate","Department","Cost Center","Area","Grade","Function","Custom1","Custom2","Custom3","Custom4","Custom5","Error Message"'
SELECT @EROXR = COALESCE(@columnHeaders + CHAR(13)+CHAR(10), '') + @EROXR

--Save the errors to BATCH_FILE
update BATCH_FILE SET FILE_UPLOAD_ERROR = @EROXR WHERE BATCH_ID = @batch_id;

END

GO