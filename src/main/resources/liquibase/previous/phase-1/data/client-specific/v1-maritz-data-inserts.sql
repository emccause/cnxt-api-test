
--SQL for 'maritz' Client specific data inserts
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/support_email', 'maritzculturenext@maritz.com')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/primaryColor', '#235BA8')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/secondaryColor', '#001E60')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/textOnPrimaryColor', '#FFFFFF')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/display_name', 'Maritz Inc')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/short_name', 'maritz')
 --will likely be 'http://previw.mymaritz.com/Collections/Applications/We.aspx' for UAT when SSO is ready with UI URL
 -- and   'http://www.mymaritz.com/Collections/Applications/We.aspx' for prod
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/partnerSsoUrl', 'https://u-maritz-api.culturenxt.com/maritz-m365/sso-simulator.html')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/clientUrl', 'http://maritz.uat.m365cloud.com')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/sso_enabled', 'true')
 
 
--messages data
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/adminOverviewHelpText', '<em>Need Help?</em> Email Us.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginWelcomeMessage', 'Welcome to your reward & recognition community! Please enter your Network ID and Password to continue.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginUsernamePlaceholder', 'Network ID')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginUsernamePlaceholder', 'Password')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginSSOWelcomeMessage', 'Welcome to your reward & recognition community! Please enter your Network ID and Password to continue.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/logoutSSOMessage', 'You have logged out of the system. To access this site, go through MyMaritz.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/SSOAccessErrorMessage', 'Sorry, the system is currently unavailable to service your request. Please try again later.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/partnerSSOLinkText', 'www.mymaritz.com')

 ----- use below updates if keys already exist 
 
 update component.APPLICATION_DATA set VALUE_1='maritzculturenext@maritz.com'
 where  KEY_NAME='/app/project_profile/support_email'
update component.APPLICATION_DATA set VALUE_1='#235BA8'
 where  KEY_NAME='/app/project_profile/colors/primaryColor'
update component.APPLICATION_DATA set VALUE_1='#1E4D8F'
 where  KEY_NAME='/app/project_profile/colors/secondaryColor'
update component.APPLICATION_DATA set VALUE_1='#FFFFFF'
 where  KEY_NAME='/app/project_profile/colors/textOnPrimaryColor'
update component.APPLICATION_DATA set VALUE_1='Maritz Inc'
 where  KEY_NAME='/app/project_profile/display_name'
update component.APPLICATION_DATA set VALUE_1='maritz'
 where  KEY_NAME='/app/project_profile/short_name'
update component.APPLICATION_DATA set VALUE_1='http://www.mymaritz.com/Collections/Applications/We.aspx'
 where  KEY_NAME='/app/project_profile/partnerSsoUrl'
update component.APPLICATION_DATA set VALUE_1='https://maritz.culturenxt.com'
 where  KEY_NAME='/app/project_profile/clientUrl'
update component.APPLICATION_DATA set VALUE_1='true'
 where  KEY_NAME='/app/project_profile/sso_enabled'
 
 update component.APPLICATION_DATA set VALUE_2='<em>Need Help?</em> Email Us.'
 where  KEY_NAME='/app/project_messages/adminOverviewHelpText'
 update component.APPLICATION_DATA set VALUE_2='Welcome to your reward & recognition community! Please enter your Network ID and Password to continue'
 where  KEY_NAME='/app/project_messages/loginWelcomeMessage'
 update component.APPLICATION_DATA set VALUE_2='Network ID'
 where  KEY_NAME='/app/project_messages/loginUsernamePlaceholder'
 update component.APPLICATION_DATA set VALUE_2='Password'
 where  KEY_NAME='/app/project_messages/loginPasswordPlaceholder'
 update component.APPLICATION_DATA set VALUE_2='Welcome to your reward & recognition community! Please enter your Network ID and Password to continue.'
 where  KEY_NAME='/app/project_messages/loginSSOWelcomeMessage'
 update component.APPLICATION_DATA set VALUE_2='You have logged out of the system. To access this site, go through MyMaritz.'
 where  KEY_NAME='/app/project_messages/logoutSSOMessage'
 update component.APPLICATION_DATA set VALUE_2='Sorry, the system is currently unavailable to service your request. Please try again later.'
 where  KEY_NAME='/app/project_messages/SSOAccessErrorMessage'
 
 

 --email addresses
 update component.APPLICATION_DATA set VALUE_1='Maritz CultureNext <maritzculturenext@maritz.com>'
 where  KEY_NAME='/app/email_processor/sending_email_address'

 --will need to be null in prod when ready
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/email_processor/send_to_all_addresses', 'david.adam@maritz.com')

--ABS setup for issuance, ABS audit report
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientname', 'Maritz-MMS')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientnumber', '1494')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/mars/subclientnumber/FCP', '1494.2')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/emailaddress', 'abs@maritz.com')
 
update component.APPLICATION_DATA set VALUE_1='COST_CENTER', VALUE_2='SUBMITTER'
 where  KEY_NAME='/app/vendorprocessing/subproject/override'


 
--details for ABS demographics update, account creation 
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/processeddate', '01/01/2015')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/defaultawardtype/US', 'FCP')
--insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
-- values (1, '/app/demographicupdates/defaultawardtype/nonUS', 'FCP')


insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/FCP/defaultprojectnumber', 'S07449')

insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/FCP/defaultsubprojectnumber', 'QA')
 
 --Entry needed for FTL flow with SSO users
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/login/first_time_date', '01/01/2015')
          

 --PAYOUT_TYPE,PAYOUT_VENDOR etc
insert into component.EARNINGS_TYPE (EARNINGS_TYPE_CODE, EARNINGS_TYPE_DESC)
 values ('FCP', 'Earnings Desc for FCP') 
insert into component.PAYOUT_TYPE (PAYOUT_TYPE_CODE,PAYOUT_TYPE_DESC, DISPLAY_STATUS)
 values ('FCP', 'Product Code used by Maritz', 'A')
insert into component.CARD_TYPE (CARD_TYPE_CODE,CARD_TYPE_NAME, DISPLAY_SEQUENCE, DISPLAY_STATUS)
 values ('FCP', 'ABS', 1, 'A')
insert into component.PAYOUT_VENDOR (PAYOUT_VENDOR_NAME)
 values ('MARS')
insert into component.PAYOUT_ITEM (PAYOUT_VENDOR_ID, PAYOUT_ITEM_NAME, PAYOUT_ITEM_DESC, PAYOUT_ITEM_AMOUNT, PRODUCT_CODE)
 values ((select PAYOUT_VENDOR_ID from component.PAYOUT_VENDOR where PAYOUT_VENDOR_NAME='MARS'), 'FCP', 'Product Code used by Maritz', 1.0, 'FCP')
 
 
 --PROJECT NUMBER
insert into component.PROJECT (PROGRAM_ID, PROJECT_NUMBER, PROJECT_DESC) 
 values ('1', 'S07449', 'Initial Project Number')

--Admin user setup (Maritz Admin) for non-maritz tenant
--This creates a maritz admin user and sets them up to be able to log in
INSERT INTO component.PAX (LANGUAGE_CODE, PREFERRED_LOCALE, CONTROL_NUM, FIRST_NAME, LAST_NAME)
values ('en', 'en_US', 'admin', 'Maritz', 'Admin')

DECLARE @paxId BIGINT
SET @paxId = SCOPE_IDENTITY()

INSERT INTO component.SYS_USER (PAX_ID, SYS_USER_NAME, SYS_USER_PASSWORD, SYS_USER_PASSWORD_DATE, STATUS_TYPE_CODE)
values (@paxId, 'admin', 'CultureNext2015', GETDATE(), 'ACTIVE')

INSERT INTO component.PAX_GROUP (PAX_ID, ORGANIZATION_CODE, ROLE_CODE, FROM_DATE)
values (@paxId, 'MTZ', 'ADM', GETDATE())

INSERT INTO component.EMAIL (PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS, PREFERRED)
values (@paxId, 'BUS', 'david.adam@maritz.com', 'Y')

DECLARE @adminGroupId BIGINT
SELECT @adminGroupId = GROUP_ID from component.GROUPS where GROUP_TYPE_CODE = 'ROLE_BASED' and GROUP_DESC = 'Administrators'

INSERT INTO component.GROUPS_PAX (GROUP_ID, PAX_ID)
values (@adminGroupId, @paxId)

 -- Loading properties from application_data (can overwrite any property from properties file by putting the property name in key_name)
--
--insert into component.application_data(key_name, value_1) values ('token.rest.key', 'KEY_GOES_HERE')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.subclient', 'FCP=1494.2')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.product', 'FCP=FCP')
 
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.user', 'CultureNext_Prod')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.password', 'FZUWSR3J')

 ----THE table issue, this particular ABR entry is used in earnings/payouts job. and version has a trigger
 --update jobs to only use ABR with a specific name, type & succesor_id of null

DROP TRIGGER [component].[TR_ACTIVITY_BUSINESS_RULE_IU_1]
GO
 update component.ACTIVITY_BUSINESS_RULE set VERSION = '2' where ID = 12
 update component.ACTIVITY_BUSINESS_RULE set VERSION = '4' where ID = 27

 ---
 --re-create above trigger

 ---group- config
 SET IDENTITY_INSERT [component].[GROUP_CONFIG] ON
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (2, N'ENRL_FIELD', 4, N'AREA', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=AREA', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:06:10.3770000', N'dbo', '2015-04-15 21:06:10.3770000', N'dbo', N'Area')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (3, N'ENRL_FIELD', 4, N'CITY', N'ADDRESS.CITY', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:08:39.8030000', N'dbo', '2015-04-15 21:08:39.8030000', N'dbo', N'City')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (6, N'ENRL_FIELD', 4, N'COMPANY_NAME', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=COMPANY_NAME', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Company Name')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (7, N'ENRL_FIELD', 4, N'COST_CENTER', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=COST_CENTER', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Cost Center')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (8, N'ENRL_FIELD', 4, N'DEPARTMENT', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=DEPARTMENT', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Department')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (9, N'ENRL_FIELD', 4, N'FUNCTION', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=FUNCTION', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Function')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (10, N'ENRL_FIELD', 4, N'GRADE', N'PAX_GROUP_MISC.MISC_DATA,VF_NAME=GRADE', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Grade')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (11, N'ENRL_FIELD', 4, N'POSTAL_CODE', N'ADDRESS.ZIP', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'Postal Code')
INSERT INTO [component].[GROUP_CONFIG] ([ID], [GROUP_CONFIG_TYPE_CODE], [GROUP_TYPE_ID], [GROUP_CONFIG_NAME], [CONFIG], [FROM_DATE], [THRU_DATE], [STATUS_TYPE_CODE], [BUILD_SEQUENCE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [GROUP_CONFIG_DESC]) VALUES (12, N'ENRL_FIELD', 4, N'STATE', N'ADDRESS.STATE', NULL, NULL, N'INACTIVE', NULL, '2015-04-15 21:21:06.4400000', N'dbo', '2015-04-15 21:21:06.4400000', N'dbo', N'State/Province')
SET IDENTITY_INSERT [component].[GROUP_CONFIG] OFF

--make below users 'admin's
insert into component.GROUPS_PAX (GROUP_ID, PAX_ID) values
(6, 160), (6, 730), (6, 84), (6, 373), (6, 729)

update component.SYS_USER set SYS_USER_PASSWORD='Abcd1234' where pax_id in ( 160, 730, 84, 373, 729)

