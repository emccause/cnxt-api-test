
--SQL for Client specific data inserts
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/support_email', 'supportM365@maritz.com')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/primaryColor', '#FF00AA')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/secondaryColor', '#44BBFF')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/colors/textOnPrimaryColor', '#FFFFFF')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/display_name', 'Acme Corp')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/short_name', 'acme')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/partnerSsoUrl', 'https://partner.com/sso-login')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/clientUrl', 'http://acme.uat.m365cloud.com')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/project_profile/sso_enabled', 'false')

 
--messages data
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/adminOverviewHelpText', '<em>Need Help?</em> Try reaching us at 1-800-333-5444')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginWelcomeMessage', 'Welcome to your reward & recognition community! Please enter your Login ID and Password to continue.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginUsernamePlaceholder', 'Enter Your Login ID')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginUsernamePlaceholder', 'Password')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/loginSSOWelcomeMessage', 'Admin Login Message')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/logoutSSOMessage', 'You have logged out of the system. To access this site, go through your internal network/website.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/SSOAccessErrorMessage', 'Sorry, the system is currently unavailable to service your request. Please try again later.')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_2)
 values (1, '/app/project_messages/partnerSSOLinkText', 'mymaritz.com')

 --email addresses
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/email_processor/sending_email_address', 'CultureNext <supportM365@maritz.com>')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/email_processor/send_to_all_addresses', 'supportM365@maritz.com;aa.bb@cc.com')

--ABS setup for issuance, ABS audit report
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientname', 'Culture Next')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientnumber', '1111')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/mars/subclientnumber/AAA', '1111.1')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/mars/subclientnumber/BBB', '1111.2')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/emailaddress', 'Culture Next')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1, VALUE2)
 values (1, '/app/vendorprocessing/subproject/override', 'COST_CENTER', 'SUBMITTER')

--details for ABS demographics update, account creation 
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/processeddate', '01/01/2015')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/defaultawardtype/US', 'AAA')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/defaultawardtype/nonUS', 'BBB')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/AAA/defaultprojectnumber', 'SSS111')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/AAA/defaultsubprojectnumber', '1')
 
 --Entries needed for ABS Audit Report
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.subclient', 'AAA=1111.1')
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.product', 'AAA=AAA')
 
 --Entry needed for FTL flow with SSO users
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/login/first_time_date', '01/01/2015')
          

 --PAYOUT_TYPE,PAYOUT_VENDOR etc
 insert into component.EARNINGS_TYPE (EARNINGS_TYPE_CODE, EARNINGS_TYPE_DESC, DISPLAY_STATUS)
  values ('AAA', 'Product Code used by ClientA', 'A')
insert into component.PAYOUT_TYPE (PAYOUT_TYPE_CODE,PAYOUT_TYPE_DESC, DISPLAY_STATUS)
 values ('AAA', 'Product Code used by ClientA', 'A')
insert into component.CARD_TYPE (CARD_TYPE_CODE,CARD_TYPE_NAME, DISPLAY_SEQUENCE, DISPLAY_STATUS)
 values ('AAA', 'ABS', 1, 'A')
insert into component.PAYOUT_VENDOR (PAYOUT_VENDOR_NAME)
 values ('MARS')
insert into component.PAYOUT_ITEM (PAYOUT_VENDOR_ID, PAYOUT_ITEM_NAME, PAYOUT_ITEM_DESC, PAYOUT_ITEM_AMOUNT, PRODUCT_CODE)
 values ((select PAYOUT_VENDOR_ID from component.PAYOUT_VENDOR where PAYOUT_VENDOR_NAME='MARS'), 'Product Code used by ClientA', 'Product Code used by ClientA', 1.0, 'AAA')
 
 --PROJECT NUMBER
insert into component.PROJECT (PROGRAM_ID, PROJECT_NUMBER, PROJECT_DESC) 
 values ('1', 'SSS111', 'Pre-Load Project Number')
 
--Admin user setup (Maritz Admin) for non-maritz tenant
--This creates a maritz admin user and sets them up to be able to log in
INSERT INTO component.PAX (LANGUAGE_CODE, PREFERRED_LOCALE, CONTROL_NUM, FIRST_NAME, LAST_NAME)
values ('en', 'en_US', 'admin', 'Maritz', 'Admin')

DECLARE @paxId BIGINT
SET @paxId = SCOPE_IDENTITY()

INSERT INTO component.SYS_USER (PAX_ID, SYS_USER_NAME, SYS_USER_PASSWORD, SYS_USER_PASSWORD_DATE, STATUS_TYPE_CODE)
values (@paxId, 'admin', 'CultureNext2015', GETDATE(), 'ACTIVE')

INSERT INTO component.PAX_GROUP (PAX_ID, ORGANIZATION_CODE, ROLE_CODE, FROM_DATE)
values (@paxId, 'MTZ', 'ADM', GETDATE())

INSERT INTO component.EMAIL (PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS, PREFERRED)
values (@paxId, 'BUS', 'admin@maritz.com', 'Y')

DECLARE @adminGroupId BIGINT
SELECT @adminGroupId = GROUP_ID from component.GROUPS where GROUP_TYPE_CODE = 'ROLE_BASED' and GROUP_DESC = 'Administrators'

INSERT INTO component.GROUPS_PAX (GROUP_ID, PAX_ID)
values (@adminGroupId, @paxId)

 -- Loading properties from application_data (can overwrite any property from properties file by putting the property name in key_name)
insert into component.application_data(key_name, value_1) values ('token.rest.key', 'KEY_GOES_HERE')
insert into component.application_data(key_name, value_1) values ('mars.subclient', 'BLAH')
insert into component.application_data(key_name, value_1) values ('mars.product', 'BLAH')
