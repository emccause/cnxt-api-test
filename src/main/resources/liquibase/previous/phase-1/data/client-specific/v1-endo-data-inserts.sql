--SQL for 'endorewards' Client specific data inserts
 
 
--messages data

 ----- use below updates if keys already exist 
  update component.APPLICATION_DATA set VALUE_1='endorewards@maritz.com'
 where  KEY_NAME='/app/project_profile/support_email'
update component.APPLICATION_DATA set VALUE_1='#f1b434'
 where  KEY_NAME='/app/project_profile/colors/primaryColor'
update component.APPLICATION_DATA set VALUE_1='#707372'
 where  KEY_NAME='/app/project_profile/colors/secondaryColor'
update component.APPLICATION_DATA set VALUE_1='#FFFFFF'
 where  KEY_NAME='/app/project_profile/colors/textOnPrimaryColor'
update component.APPLICATION_DATA set VALUE_1='Endo Rewards'
 where  KEY_NAME='/app/project_profile/display_name'
update component.APPLICATION_DATA set VALUE_1='endorewards'
 where  KEY_NAME='/app/project_profile/short_name'
update component.APPLICATION_DATA set VALUE_1='https://u-endorewards-api.culturenxt.com/maritz-m365/sso-simulator.html'
 where  KEY_NAME='/app/project_profile/partnerSsoUrl'
update component.APPLICATION_DATA set VALUE_1='https://u-endorewards.culturenxt.com'
 where  KEY_NAME='/app/project_profile/clientUrl'
update component.APPLICATION_DATA set VALUE_1='true'
 where  KEY_NAME='/app/project_profile/sso_enabled'
 
 update component.APPLICATION_DATA set VALUE_2='If you need assistance, please email endorewards@maritz.com.'
 where  KEY_NAME='/app/project_messages/adminOverviewHelpText'
 update component.APPLICATION_DATA set VALUE_2='Welcome to your reward & recognition community! Please enter your Network ID and Password to continue'
 where  KEY_NAME='/app/project_messages/loginWelcomeMessage'
 update component.APPLICATION_DATA set VALUE_2='Network ID'
 where  KEY_NAME='/app/project_messages/loginUsernamePlaceholder'
 update component.APPLICATION_DATA set VALUE_2='Password'
 where  KEY_NAME='/app/project_messages/loginPasswordPlaceholder'
 update component.APPLICATION_DATA set VALUE_2='Welcome to your reward & recognition community! Please enter your Network ID and Password to continue.'
 where  KEY_NAME='/app/project_messages/loginSSOWelcomeMessage'
 update component.APPLICATION_DATA set VALUE_2='You have logged out of the system. To access this site, go through Endo Rewards.'
 where  KEY_NAME='/app/project_messages/logoutSSOMessage'
 update component.APPLICATION_DATA set VALUE_2='Sorry, the system is currently unavailable to service your request. Please try again later.'
 where  KEY_NAME='/app/project_messages/SSOAccessErrorMessage'
 update component.APPLICATION_DATA set VALUE_2='endo.com'
 where  KEY_NAME='/app/project_messages/partnerSSOLinkText'
 
  --email addresses
 update component.APPLICATION_DATA set VALUE_1='Endo Rewards <endorewards@maritz.com>'
 where  KEY_NAME='/app/email_processor/sending_email_address'

 --will need to be null in prod when ready
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/email_processor/send_to_all_addresses', 'david.adam@maritz.com')

--ABS setup for issuance, ABS audit report
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientname', 'Endo Rewards')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/clientnumber', '30908')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/mars/subclientnumber/EYD', '30908.2')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/vendorprocessing/abs/emailaddress', 'abs@maritz.com')
 
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, '/app/demographicupdates/defaultawardtype/US', 'EYD')
 
          
 --Entry needed for PROD only (stage is the default in property files
 --PROD
 --insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 --values (1, 'mars.user', 'CultureNext_Prod')
 --insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 --values (1, 'mars.password', 'FZUWSR3J')
 --insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 --values (1, 'sso.test.enabled', 'TRUE')
 
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.subclient', 'EYD=30908.2')
insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1)
 values (1, 'mars.product', 'EYD=EYD')

 --PAYOUT_TYPE,PAYOUT_VENDOR etc
--insert into component.EARNINGS_TYPE (EARNINGS_TYPE_CODE, EARNINGS_TYPE_DESC)
 --values ('EYD', 'Earnings Desc for EYD') 
--insert into component.PAYOUT_TYPE (PAYOUT_TYPE_CODE,PAYOUT_TYPE_DESC, DISPLAY_STATUS)
 --values ('EYD', 'Product Code used by Endo', 'A')
insert into component.CARD_TYPE (CARD_TYPE_CODE,CARD_TYPE_NAME, DISPLAY_SEQUENCE, DISPLAY_STATUS)
 values ('EYD', 'EYD', 1, 'A')
 -- below exisit already
--insert into component.PAYOUT_VENDOR (PAYOUT_VENDOR_NAME)
-- values ('EYD')
--insert into component.PAYOUT_ITEM (PAYOUT_VENDOR_ID, PAYOUT_ITEM_NAME, PAYOUT_ITEM_DESC, PAYOUT_ITEM_AMOUNT, PRODUCT_CODE)
 --values ((select PAYOUT_VENDOR_ID from component.PAYOUT_VENDOR where PAYOUT_VENDOR_NAME='EYD'), 'EYD', 'Product Code used by Endo', 1.0, 'EYD')
 
 
 --PROJECT NUMBER
insert into component.PROJECT (PROGRAM_ID, PROJECT_NUMBER, PROJECT_DESC) 
 values ('1', 'S04758', 'Initial Project Number')

 -- ***** MP-3273 Add EY flag at project level *****
 insert into component.APPLICATION_DATA (PROGRAM_ID, KEY_NAME, VALUE_1) values
(1, '/app/project_profile/EY_enabled', 'TRUE')
--Admin user setup (Maritz Admin) for non-maritz tenant

INSERT INTO component.EMAIL (PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS, PREFERRED)
values (2, 'BUS', 'david.adam@maritz.com', 'Y')

DECLARE @adminGroupId BIGINT
SELECT @adminGroupId = GROUP_ID from component.GROUPS where GROUP_TYPE_CODE = 'ROLE_BASED' and GROUP_DESC = 'Administrators'

INSERT INTO component.GROUPS_PAX (GROUP_ID, PAX_ID)
values (@adminGroupId, 2)


---
INSERT INTO component.PAX (LANGUAGE_CODE, PREFERRED_LOCALE, CONTROL_NUM, FIRST_NAME, LAST_NAME)
values ('en', 'en_US', 'adamde', 'David', 'Adam')

DECLARE @paxId BIGINT
SET @paxId = SCOPE_IDENTITY()

INSERT INTO component.SYS_USER (PAX_ID, SYS_USER_NAME, SYS_USER_PASSWORD, SYS_USER_PASSWORD_DATE, STATUS_TYPE_CODE)
values (@paxId, 'adamde', 'CultureNext2015', GETDATE(), 'ACTIVE')

INSERT INTO component.PAX_GROUP (PAX_ID, ORGANIZATION_CODE, ROLE_CODE, FROM_DATE)
values (@paxId, 'MTZ', 'ADM', GETDATE())

INSERT INTO component.EMAIL (PAX_ID, EMAIL_TYPE_CODE, EMAIL_ADDRESS, PREFERRED)
values (@paxId, 'BUS', 'david.adam@maritz.com', 'Y')

DECLARE @adminGroupId2 BIGINT
SELECT @adminGroupId2 = GROUP_ID from component.GROUPS where GROUP_TYPE_CODE = 'ROLE_BASED' and GROUP_DESC = 'Administrators'

INSERT INTO component.GROUPS_PAX (GROUP_ID, PAX_ID)
values (@adminGroupId2,@paxId)

