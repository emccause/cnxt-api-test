/****** Object:  Trigger [component].[TR_TRANSACTION_HEADER_DU_1]    Script Date: 10/9/2017 9:17:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- TR_TRANSACTION_HEADER_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta        20070207                Created
-- ericksrt        20150819                Removed UP_SECURITY_GET_USER_NAME reference
-- HARWELLM        20171009    MP-10121    ADD PROXY_PAX_ID COLUMN
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_TRANSACTION_HEADER_DU_1]
ON [component].[TRANSACTION_HEADER]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

--EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

--SET @change_id = ISNULL(@change_id, USER_NAME())
SET @change_id = USER_NAME()

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_TRANSACTION_HEADER (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    PAX_GROUP_ID
,    TRANSACTION_CLAIM_ID
,    BATCH_ID
,    PERIOD_ID
,    PROGRAM_ID
,    TYPE_CODE
,    SUB_TYPE_CODE
,    ACTIVITY_DATE
,    STATUS_TYPE_CODE
,    STATUS_CHANGE_DETAIL
,    ACCOUNT_NUMBER
,    PERFORMANCE_METRIC
,    PERFORMANCE_VALUE
,    COUNTRY_CODE
,    PROXY_PAX_ID
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    PAX_GROUP_ID
,    TRANSACTION_CLAIM_ID
,    BATCH_ID
,    PERIOD_ID
,    PROGRAM_ID
,    TYPE_CODE
,    SUB_TYPE_CODE
,    ACTIVITY_DATE
,    STATUS_TYPE_CODE
,    STATUS_CHANGE_DETAIL
,    ACCOUNT_NUMBER
,    PERFORMANCE_METRIC
,    PERFORMANCE_VALUE
,    COUNTRY_CODE
,    PROXY_PAX_ID
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''TRANSACTION_HEADER''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO


