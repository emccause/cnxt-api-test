
GO
/****** Object:  Trigger [component].[TR_ADDRESS_TYPE_IU_1]    Script Date: 10/22/2020 8:20:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_ADDRESS_TYPE_IU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to ensure that the display sequence column is
-- populated.
-- 
-- Insert:
-- If a display sequence is provided, then that display sequence is used
-- regardless if it exists and regardless if it is sequential.
-- 
-- If a display sequence is not provided, then the max display sequence that was in
-- the table prior to the operation plus one is assigned.
-- 
-- Update:
-- If a display sequence is provided, then that display sequence is used
-- regardless if it exists and regardless if it is sequential.
-- 
-- If a display sequence is not provided, then the display sequence that was in
-- the record prior to the operation is retained.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- dohognta		20060628        Created
-- burgestl		20201021	update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_ADDRESS_TYPE_IU_1]
ON [component].[ADDRESS_TYPE]
FOR INSERT, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
	RETURN

DECLARE @operation VARCHAR(50) 
DECLARE @sql VARCHAR(1000)
DECLARE @err INT
DECLARE @errmsg VARCHAR(500)
DECLARE @severity INT
DECLARE @rowcount INT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SET NOCOUNT ON

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM DELETED)
	SET @operation = 'UPDATE'
ELSE
	SET @operation = 'INSERT'

-------------------------------------------------------------------
-- Ensure that the display sequence column is populated.
-------------------------------------------------------------------

IF UPDATE(DISPLAY_SEQUENCE)
BEGIN
	DECLARE	@display_sequence INT

	-------------------------------------------------------------------
	-- Update
	-------------------------------------------------------------------
	IF EXISTS (SELECT 1 FROM INSERTED T1 inner join  DELETED T2 on T1.ADDRESS_TYPE_ID = T2.ADDRESS_TYPE_ID where T1.DISPLAY_SEQUENCE IS NULL AND T2.DISPLAY_SEQUENCE IS NOT NULL)
	BEGIN
		SELECT	@display_sequence = DISPLAY_SEQUENCE FROM DELETED

		UPDATE	T1
		SET	DISPLAY_SEQUENCE = @display_sequence
		FROM    component.ADDRESS_TYPE T1
		inner join INSERTED T2
		on   T1.ADDRESS_TYPE_ID = T2.ADDRESS_TYPE_ID

	        SELECT @err = @@ERROR
	END
	-------------------------------------------------------------------
	-- Insert
	-------------------------------------------------------------------
	ELSE IF EXISTS (SELECT 1 FROM INSERTED T1 WHERE T1.DISPLAY_SEQUENCE IS NULL)
	BEGIN
		SELECT	@display_sequence = MAX(ISNULL(DISPLAY_SEQUENCE, 0)) + 1 FROM ADDRESS_TYPE

		UPDATE	T1
		SET DISPLAY_SEQUENCE = @display_sequence
		FROM    component.ADDRESS_TYPE T1
		inner join INSERTED T2
		on   T1.ADDRESS_TYPE_ID = T2.ADDRESS_TYPE_ID

	        SELECT @err = @@ERROR
	END
    
	-------------------------------------------------------------------
	-- If an error occurs then raise an error;
	-------------------------------------------------------------------
	IF @err <> 0
	BEGIN
		SET @errmsg = '%s statement failed with TRIGGER constraint ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + '.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
		SET @severity = 16 -- Indicate errors that can be corrected by the user
		GOTO RAISE_ERROR
	END
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
