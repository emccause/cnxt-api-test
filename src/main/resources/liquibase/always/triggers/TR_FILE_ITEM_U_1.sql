
GO
/****** Object:  Trigger [component].[TR_FILE_ITEM_U_1]    Script Date: 10/29/2020 9:57:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_FILE_ITEM_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	20040101        		Created
-- burgestl		20201021			update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_FILE_ITEM_U_1]
ON [component].[FILE_ITEM]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    FILE_ITEM T1
    inner join INSERTED T2
    on   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    FILE_ITEM T1
    inner join INSERTED T2
    on   T1.ID = T2.ID
END

RETURN

END
