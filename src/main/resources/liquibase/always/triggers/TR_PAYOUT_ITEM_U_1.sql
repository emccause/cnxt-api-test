
GO
/****** Object:  Trigger [component].[TR_PAYOUT_ITEM_U_1]    Script Date: 10/30/2020 10:53:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_PAYOUT_ITEM_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- dohognta		20040101        Created
-- burgestl		20201023	update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_PAYOUT_ITEM_U_1]
ON [component].[PAYOUT_ITEM]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PAYOUT_ITEM T1
    INNER JOIN INSERTED T2
    ON   T1.PAYOUT_ITEM_ID = T2.PAYOUT_ITEM_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PAYOUT_ITEM T1
    INNER JOIN INSERTED T2
    ON   T1.PAYOUT_ITEM_ID = T2.PAYOUT_ITEM_ID
END

RETURN

END
