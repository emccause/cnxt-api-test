
GO
/****** Object:  Trigger [component].[TR_ORG_GROUP_D_1]    Script Date: 10/30/2020 10:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
*******************************************************************************
** TR_ORG_GROUP_D_1
** Check that records do not exist in PAX_GROUP that are associated with the 
** current ORG_GROUP record being deleted. If there are associated records,  
** raise an error and rollback the transaction(s).
********************************************************************************
*/
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- burgestl		20201023				update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_ORG_GROUP_D_1]
ON [component].[ORG_GROUP]
FOR DELETE
AS
BEGIN

SET NOCOUNT ON

DECLARE @rowCount int
DECLARE @errMsg varchar(255)

SELECT @rowCount = COUNT(*) 
FROM DELETED D 
inner join PAX_GROUP P ON D.ORGANIZATION_CODE = P.ORGANIZATION_CODE
WHERE D.ROLE_CODE = P.ROLE_CODE
  AND (P.THRU_DATE > getdate() OR P.THRU_DATE IS NULL)

IF @rowCount = 1 
BEGIN
  SET @errMsg = @rowCount 
  SET @errMsg = ' ' + @errMsg + ' active record has been found in PAX_GROUP associated with ORG_GROUP record(s) being deleted. Reassign it before deleting from ORG_GROUP.'
  RAISERROR (@errMsg, 16, 1)
  ROLLBACK TRANSACTION
  RETURN
END
IF @rowCount > 1 
BEGIN
  SET @errMsg = @rowCount 
  SET @errMsg = ' ' + @errMsg + ' active records have been found in PAX_GROUP associated with ORG_GROUP record(s) being deleted. Reassign them before deleting from ORG_GROUP.'
  RAISERROR (@errMsg, 16, 1)
  ROLLBACK TRANSACTION
  RETURN
END

RETURN

END
