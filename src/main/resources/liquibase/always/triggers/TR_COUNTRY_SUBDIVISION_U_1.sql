
GO
/****** Object:  Trigger [component].[TR_COUNTRY_SUBDIVISION_U_1]    Script Date: 10/27/2020 3:36:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_COUNTRY_SUBDIVISION_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	20040101        		Created
-- burgestl		20201023				update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_COUNTRY_SUBDIVISION_U_1]
ON [component].[COUNTRY_SUBDIVISION]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    COUNTRY_SUBDIVISION T1
    INNER JOIN INSERTED T2
    ON   T1.COUNTRY_SUBDIVISION_ID = T2.COUNTRY_SUBDIVISION_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    COUNTRY_SUBDIVISION T1
    INNER JOIN INSERTED T2
    ON   T1.COUNTRY_SUBDIVISION_ID = T2.COUNTRY_SUBDIVISION_ID
END

RETURN

END
