
GO
/****** Object:  Trigger [component].[TR_NOMINATION_TYPE_CONFIG_D_1]    Script Date: 10/30/2020 9:14:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_NOMINATION_TYPE_CONFIG_D_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- Supports: text, ntext, or image columns 
-- 
-- Error:
-- 
-- Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables.
-- 
-- Documentation:
-- 
-- In a DELETE, INSERT, or UPDATE trigger, SQL Server does not allow text, ntext, or image column
-- references in the inserted and deleted tables if the compatibility level is set to 70. The text, 
-- ntext, and image values in the inserted and deleted tables cannot be accessed. To retrieve the new 
-- value in either an INSERT or UPDATE trigger, join the inserted table with the original update 
-- table. When the compatibility level is 65 or lower, null values are returned for inserted or 
-- deleted text, ntext, or image columns that allow null values; zero-length strings are returned if 
-- the columns are not nullable.
-- 
-- If the compatibility level is 80 or higher, SQL Server allows the update of
-- text, ntext, or image columns through the INSTEAD OF trigger on tables or 
-- views.
-- 
-- SYNTAX
-- sp_dbcmptlevel [ @dbname = ] name 
-- 
-- The current compatibility level is 80.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- dohognta		20081201        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
-- burgestl		20201023		update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_NOMINATION_TYPE_CONFIG_D_1]
ON [component].[NOMINATION_TYPE_CONFIG]
INSTEAD OF DELETE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
	RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
	RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Set command (or operation) that this trigger is serving 
-------------------------------------------------------------------
SET @operation = 'DELETE'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_NOMINATION_TYPE_CONFIG (
	CHANGE_DATE
,	CHANGE_ID
,	CHANGE_OPERATION
,	NOMINATION_TYPE_CONFIG.ID
,	NOMINATION_TYPE_CONFIG.NOMINATION_TYPE_CODE
,	NOMINATION_TYPE_CONFIG.PROGRAM_ID
,	NOMINATION_TYPE_CONFIG.CONFIG
,	NOMINATION_TYPE_CONFIG.CREATE_DATE
,	NOMINATION_TYPE_CONFIG.CREATE_ID
,	NOMINATION_TYPE_CONFIG.UPDATE_DATE
,	NOMINATION_TYPE_CONFIG.UPDATE_ID
)
SELECT	
	@change_date
,	@change_id
,	@change_operation
,	NOMINATION_TYPE_CONFIG.ID
,	NOMINATION_TYPE_CONFIG.NOMINATION_TYPE_CODE
,	NOMINATION_TYPE_CONFIG.PROGRAM_ID
,	NOMINATION_TYPE_CONFIG.CONFIG
,	NOMINATION_TYPE_CONFIG.CREATE_DATE
,	NOMINATION_TYPE_CONFIG.CREATE_ID
,	NOMINATION_TYPE_CONFIG.UPDATE_DATE
,	NOMINATION_TYPE_CONFIG.UPDATE_ID
FROM
	DELETED
	inner join	component.NOMINATION_TYPE_CONFIG
	on	DELETED.ID = NOMINATION_TYPE_CONFIG.ID

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
	SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''NOMINATION_TYPE_CONFIG''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
	SET @severity = 16 -- Indicate errors that can be corrected by the user
	GOTO RAISE_ERROR
END

DELETE	NOMINATION_TYPE_CONFIG
FROM
	DELETED
	inner join	component.NOMINATION_TYPE_CONFIG
	on	DELETED.ID = NOMINATION_TYPE_CONFIG.ID

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
