
GO
/****** Object:  Trigger [component].[TR_PAX_GROUP_IU_1]    Script Date: 10/30/2020 10:35:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- dohognta		20040101        Created
-- burgestl		20201023	update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================
ALTER TRIGGER [component].[TR_PAX_GROUP_IU_1]
ON [component].[PAX_GROUP]
FOR INSERT, UPDATE
AS
BEGIN

SET NOCOUNT ON

DECLARE @paxOwnerGroupID int
DECLARE @orgOwnerGroupID int 
DECLARE @parentRoleCode nvarchar(50)
DECLARE @parentOrgCode nvarchar(50)
DECLARE @childRoleCode nvarchar(50)
DECLARE @childOrgCode nvarchar(50)
DECLARE @rowCount int
DECLARE @passFlag int
DECLARE @errMsg nvarchar(255)

/*
** Check against ORG_GROUP that owner is of appropriate role code and organization.
** If they do not match, raise an error and rollback the transaction(s).
*/
IF UPDATE(OWNER_GROUP_ID) OR UPDATE(ORGANIZATION_CODE) OR UPDATE(ROLE_CODE)
BEGIN

	SELECT @rowCount = COUNT(*) FROM inserted

  -------------------------------------------------------------------
	-- For dummy case
  -------------------------------------------------------------------
	IF @rowCount = 0 RETURN

  -------------------------------------------------------------------
	-- For single row case
  -------------------------------------------------------------------
	IF @rowCount = 1
	BEGIN
		-- Use cursor to allow for multiple records with same role code/org code combination but
		-- different OWNER_GROUP_ID.		
		DECLARE c1 CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
			SELECT O.OWNER_GROUP_ID
			FROM INSERTED I
			INNER JOIN ORG_GROUP O ON I.ORGANIZATION_CODE = O.ORGANIZATION_CODE
			WHERE I.ROLE_CODE = O.ROLE_CODE

		OPEN c1

		FETCH NEXT FROM c1 INTO @orgOwnerGroupID

		-- Check if any entries were found in ORG_GROUP for this organization and role code.
		IF @@FETCH_STATUS != 0
		BEGIN
			SET @errMsg = ' ORGANIZATION_CODE/ROLE_CODE combination not found in ORG_GROUP.'
			RAISERROR (@errMsg, 16, 1)
			CLOSE c1
			DEALLOCATE c1
			ROLLBACK TRANSACTION
			RETURN
		END

		-- Get parent ID from current record
		SELECT 	@paxOwnerGroupID = OWNER_GROUP_ID FROM INSERTED

		-- Allow NULL owner/parent
		IF (@paxOwnerGroupID IS NULL) AND (@orgOwnerGroupID IS NULL) 
		RETURN

		-- Get parent's role code and org code from pax_group
		SELECT 	@parentRoleCode = ROLE_CODE,
	    		@parentOrgCode = ORGANIZATION_CODE
		FROM  PAX_GROUP 
		WHERE PAX_GROUP_ID = @paxOwnerGroupID

		SET @passFlag = 0
		
		-- Check that parent's organization and role code match that in ORG_GROUP.
		WHILE @@FETCH_STATUS = 0
		AND @passFlag = 0
		BEGIN
			IF EXISTS (SELECT 1 
				   FROM ORG_GROUP 
				   WHERE ORG_GROUP_ID = @orgOwnerGroupID 
				     AND ORGANIZATION_CODE = @parentOrgCode 
				     AND ROLE_CODE = @parentRoleCode)
			BEGIN
				SET @passFlag = 1
			END

			SET @orgOwnerGroupID = NULL

			FETCH NEXT FROM c1 INTO @orgOwnerGroupID
		END

		CLOSE c1
		DEALLOCATE c1

		IF @passFlag = 0
		BEGIN
			SET @errMsg = 'Invalid OWNER_GROUP_ID for this organization and role code.'
			RAISERROR (@errMsg, 16, 1)
			ROLLBACK TRANSACTION
			RETURN
		END
	END

  -------------------------------------------------------------------
	-- For multiple row case
  -------------------------------------------------------------------
	IF @rowCount > 1
	BEGIN
		-- Get parent ID from current record
		DECLARE c2 CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR 
			SELECT OWNER_GROUP_ID, ORGANIZATION_CODE, ROLE_CODE FROM INSERTED

		OPEN c2

		FETCH NEXT FROM c2 INTO @paxOwnerGroupID, @childOrgCode, @childRoleCode

		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @passFlag = 0

			-- Use cursor to allow for multiple records with same role code/org code 
			-- combination but different OWNER_GROUP_ID.		
			DECLARE c1 CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
				SELECT OWNER_GROUP_ID
				FROM ORG_GROUP 
				WHERE ORGANIZATION_CODE = @childOrgCode
				  AND ROLE_CODE = @childRoleCode

			OPEN c1

			-- Fetch first record.
			FETCH NEXT FROM c1 INTO @orgOwnerGroupID

			-- Check if any entries were found in ORG_GROUP for this organization and role code.
			IF @@FETCH_STATUS != 0
			BEGIN
				SET @errMsg = ' ORGANIZATION_CODE/ROLE_CODE combination not found in ORG_GROUP: ORGANIZATION_CODE='
				SET @errMsg = @errMsg + @childOrgCode + ',ROLE_CODE=' + @childRoleCode
				RAISERROR (@errMsg, 16, 1)
				CLOSE c1
				DEALLOCATE c1
				CLOSE c2
				DEALLOCATE c2
				ROLLBACK TRANSACTION
				RETURN
			END

			-- Allow NULL owner/parent
			IF (@paxOwnerGroupID IS NULL) AND (@orgOwnerGroupID IS NULL)
			BEGIN
				SET @passFlag = 1
			END
			ELSE
			BEGIN
				-- Get parent's role code and org code from pax_group
				SELECT	@parentRoleCode = ROLE_CODE,
					@parentOrgCode = ORGANIZATION_CODE
				FROM  PAX_GROUP 
				WHERE PAX_GROUP_ID = @paxOwnerGroupID

				-- Check that parent's organization and role code match that in ORG_GROUP.
				WHILE @@FETCH_STATUS = 0
				AND @passFlag = 0
				BEGIN
					IF EXISTS (SELECT 1
						   FROM ORG_GROUP 
						   WHERE ORG_GROUP_ID = @orgOwnerGroupID 
						     AND ORGANIZATION_CODE = @parentOrgCode 
						     AND ROLE_CODE = @parentRoleCode)
					BEGIN
						SET @passFlag = 1
					END

					SET @orgOwnerGroupID = NULL
	
					FETCH NEXT FROM c1 INTO @orgOwnerGroupID
				END
			END

			IF @passFlag = 0
			BEGIN
				SET @errMsg = 'Invalid OWNER_GROUP_ID for organization and role code: OWNER_GROUP_ID='
				SET @errMsg = @errMsg + CAST(@paxOwnerGroupID AS nvarchar(20)) + ',ORGANIZATION_CODE=' + @childOrgCode + ',ROLE_CODE=' + @childRoleCode
				RAISERROR (@errMsg, 16, 1)
				CLOSE c1
				DEALLOCATE c1
				CLOSE c2
				DEALLOCATE c2
				ROLLBACK TRANSACTION
				RETURN
			END

			CLOSE c1
			DEALLOCATE c1

			SET @paxOwnerGroupID = NULL
			SET @childRoleCode = NULL
			SET @childOrgCode = NULL
			SET @parentRoleCode = NULL
			SET @parentOrgCode = NULL

			FETCH NEXT FROM c2 INTO @paxOwnerGroupID, @childOrgCode, @childRoleCode
		END

		CLOSE c2
		DEALLOCATE c2
	END
END

RETURN

END
