
GO
/****** Object:  Trigger [component].[TR_ASSOCIATION_U_1]    Script Date: 06/01/2021 10:55:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================  NAME  ======================================
-- TR_ASSOCIATION_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE                CHANGE DESCRIPTION
-- dohognta       20040101                Created
-- hernancl		  20210601	            update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_ASSOCIATION_U_1]
ON [component].[ASSOCIATION]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    ASSOCIATION T1
    INNER JOIN  INSERTED T2
    ON   T1.ASSOCIATION_ID = T2.ASSOCIATION_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    ASSOCIATION T1
    INNER JOIN   INSERTED T2
    ON   T1.ASSOCIATION_ID = T2.ASSOCIATION_ID
END

RETURN

END