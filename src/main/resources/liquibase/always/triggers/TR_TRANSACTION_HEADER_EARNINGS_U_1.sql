
GO
/****** Object:  Trigger [component].[TR_TRANSACTION_HEADER_EARNINGS_U_1]    Script Date: 11/2/2020 6:44:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_TRANSACTION_HEADER_EARNINGS_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	20040101        		Created
-- burgestl		20201023				update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_TRANSACTION_HEADER_EARNINGS_U_1]
ON [component].[TRANSACTION_HEADER_EARNINGS]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    TRANSACTION_HEADER_EARNINGS T1
    INNER JOIN INSERTED T2
    ON   T1.TRANSACTION_HEADER_EARNINGS_ID = T2.TRANSACTION_HEADER_EARNINGS_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    TRANSACTION_HEADER_EARNINGS T1
    INNER JOIN INSERTED T2
    ON   T1.TRANSACTION_HEADER_EARNINGS_ID = T2.TRANSACTION_HEADER_EARNINGS_ID
END

RETURN

END
