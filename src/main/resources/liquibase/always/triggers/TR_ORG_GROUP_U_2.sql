
GO
/****** Object:  Trigger [component].[TR_ORG_GROUP_U_2]    Script Date: 10/30/2020 10:23:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**********************************************************************************
** TR_ORG_GROUP_U_2
** Check that records do not exist in PAX_GROUP that are associated with the 
** current ORG_GROUP record being changed. If there are associated records, raise 
** an error and rollback the transaction(s).
***********************************************************************************
*/
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- burgestl		20201023				update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_ORG_GROUP_U_2]
ON [component].[ORG_GROUP]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

DECLARE @rowCount int
DECLARE @errMsg varchar(255)

IF UPDATE(OWNER_GROUP_ID) OR UPDATE(ORGANIZATION_CODE) OR UPDATE(ROLE_CODE)
BEGIN
  SELECT @rowCount = COUNT(*) 
  FROM DELETED D
  INNER JOIN PAX_GROUP P ON D.ORGANIZATION_CODE = P.ORGANIZATION_CODE
  WHERE D.ROLE_CODE = P.ROLE_CODE
    AND (P.THRU_DATE > getdate() OR P.THRU_DATE IS NULL)

  IF @rowCount = 1 
  BEGIN
    SET @errMsg = @rowCount 
    SET @errMsg = ' ' + @errMsg + ' active record has been found in PAX_GROUP associated with ORG_GROUP record(s) being changed. Reassign it before changing ORG_GROUP.'
    RAISERROR (@errMsg, 16, 1)
    ROLLBACK TRANSACTION
    RETURN
  END
  IF @rowCount > 1 
  BEGIN
    SET @errMsg = @rowCount 
    SET @errMsg = ' ' + @errMsg + ' active records have been found in PAX_GROUP associated with ORG_GROUP record(s) being changed. Reassign them before changing ORG_GROUP.'
    RAISERROR (@errMsg, 16, 1)
    ROLLBACK TRANSACTION
    RETURN
  END
END

RETURN

END
