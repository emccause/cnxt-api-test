
GO
/****** Object:  Trigger [component].[TR_APPLICATION_DATA_U_1]    Script Date: 10/22/2020 2:06:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_APPLICATION_DATA_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	20040101        		Created
-- burgestl		20201021	            update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_APPLICATION_DATA_U_1]
ON [component].[APPLICATION_DATA]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    APPLICATION_DATA T1
    INNER JOIN INSERTED T2
    on   T1.APPLICATION_DATA_ID = T2.APPLICATION_DATA_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    APPLICATION_DATA T1
    INNER JOIN INSERTED T2
    on   T1.APPLICATION_DATA_ID = T2.APPLICATION_DATA_ID
END

RETURN

END
