
GO
/****** Object:  Trigger [component].[TR_SALE_ITEM_HEADER_U_1]    Script Date: 10/30/2020 3:42:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_SALE_ITEM_HEADER_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- chidris		20170621        Created
-- burgestl		20201023	update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_SALE_ITEM_HEADER_U_1]
ON [component].[SALE_ITEM_HEADER]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SALE_ITEM_HEADER T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SALE_ITEM_HEADER T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END

RETURN

END
