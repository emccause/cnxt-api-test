
GO
/****** Object:  Trigger [component].[TR_BUDGET_MISC_U_1]    Script Date: 10/23/2020 1:00:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_BUDGET_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	20040101        		Created
-- burgestl		20201023				update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_BUDGET_MISC_U_1]
ON [component].[BUDGET_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    BUDGET_MISC T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    BUDGET_MISC T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END

RETURN

END
