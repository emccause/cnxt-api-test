
GO
/****** Object:  Trigger [component].[TR_SYS_USER_U_1]    Script Date: 11/2/2020 6:20:06 AM ******/
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- chidris		20170621    Created
-- burgestl		20201023	update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [component].[TR_SYS_USER_U_1]
ON [component].[SYS_USER]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

/*
**  UPDATE THE UPDATE_DATE AND UPDATE_ID
**  IF APPLICATION PROVIDED UPDATE_ID, USE IT
**  ELSE, USE USER_NAME()
*/

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SYS_USER T1
    INNER JOIN INSERTED T2
    ON   T1.SYS_USER_ID = T2.SYS_USER_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SYS_USER T1
    INNER JOIN INSERTED T2
    ON   T1.SYS_USER_ID = T2.SYS_USER_ID
END

/*
**  UPDATE THE SYS_USER_PASSWORD_DATE IF THE PASSWORD WAS CHANGED
*/

IF  UPDATE(SYS_USER_PASSWORD)
BEGIN
    UPDATE  T1
    SET     SYS_USER_PASSWORD_DATE = GETDATE()
    FROM    SYS_USER T1
    INNER JOIN INSERTED T2
    ON   T1.SYS_USER_ID = T2.SYS_USER_ID
END

/*
**  UPDATE THE STATUS_DATE IF THE STATUS_TYPE_CODE WAS CHANGED
*/

IF  UPDATE(STATUS_TYPE_CODE)
BEGIN
    UPDATE  T1
    SET     STATUS_DATE = GETDATE()
    FROM    SYS_USER T1
    INNER JOIN INSERTED T2
    ON   T1.SYS_USER_ID = T2.SYS_USER_ID
END

RETURN

END
