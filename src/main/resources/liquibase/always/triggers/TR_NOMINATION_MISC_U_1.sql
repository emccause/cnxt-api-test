
GO
/****** Object:  Trigger [component].[TR_NOMINATION_MISC_U_1]    Script Date: 10/30/2020 9:12:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_NOMINATION_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		  DATE			STORY		CHANGE DESCRIPTION
-- osmanos		20161024					Created
-- burgestl		20201023					update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_NOMINATION_MISC_U_1]
ON [component].[NOMINATION_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    NOMINATION_MISC T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    NOMINATION_MISC T1
    INNER JOIN INSERTED T2
    ON   T1.ID = T2.ID
END

RETURN

END
