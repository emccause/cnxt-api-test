
GO
/****** Object:  Trigger [component].[TR_ASSOCIATION_TYPE_U_2]    Script Date: 10/23/2020 9:32:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_ASSOCIATION_TYPE_U_2
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to prevent data manipulation in the 
-- ASSOCIATION_TYPE table, if the record(s) being manipulated are referenced by another
-- table.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- dohognta   	31 Aug 2004     			Created
-- burgestl		20201023	            update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_ASSOCIATION_TYPE_U_2]
ON [component].[ASSOCIATION_TYPE]
FOR UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
  RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SET NOCOUNT ON

SET @operation = 'UPDATE'

-------------------------------------------------------------------
-- If the command (or operation) that this trigger is serving 
-- actually changed the values it (the trigger) is concerned with, then proceed,
-- else, do nothing
-------------------------------------------------------------------
IF UPDATE(TABLE1) OR UPDATE(TABLE2)
BEGIN
  -------------------------------------------------------------------
  -- Get count of association type record(s) where data is changing and
  -- where referenced by the association table
  -------------------------------------------------------------------
  SELECT  @rowcount = COUNT(1)
  FROM  INSERTED I
  INNER JOIN DELETED D on I.ASSOCIATION_TYPE_ID = D.ASSOCIATION_TYPE_ID
  INNER JOIN ASSOCIATION A on I.ASSOCIATION_TYPE_ID = A.ASSOCIATION_TYPE_ID
  WHERE (I.TABLE1 <> D.TABLE1 OR I.TABLE2 <> D.TABLE2)
  
  SELECT @err = @@ERROR

  -------------------------------------------------------------------
  -- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
  -------------------------------------------------------------------
  IF @err <> 0
  BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ENFORCED REFERENTIAL INTEGRITY, COLUMN FOREIGN KEY constraint ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''ASSOCIATION_TYPE''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
  END

  -------------------------------------------------------------------
  -- If the record(s) being manipulated are referenced by another table, then raise an error
  -------------------------------------------------------------------
  IF @rowcount > 0
  BEGIN
    SET @errmsg = '%s statement conflicted with TRIGGER ENFORCED REFERENTIAL INTEGRITY, COLUMN FOREIGN KEY constraint ''' + OBJECT_NAME(@@PROCID) + '''. The conflict occurred in database ''' + DB_NAME() + ''', table ''ASSOCIATION_TYPE'', column ''ASSOCIATION_TYPE_ID''. The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
  END
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
