if not exists (select 1 from sys.views where object_id = object_id('component.VW_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS'))
begin
exec ('create view component.VW_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS as select 1 as c1')
end
    SET ANSI_NULLS ON
    GO

    SET QUOTED_IDENTIFIER ON
    GO

    -- ==============================  NAME  ======================================
    -- View: VW_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS
    -- ===========================  DESCRIPTION  ==================================
    --
    -- Pulls  information participant's enrollment errors.
    --
    -- ============================  REVISIONS  ===================================
    -- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
    -- ARENASW      20210601     CNXT-533        CREATED

    ALTER VIEW component.VW_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS
    AS
    select p.last_name, p.first_name, p.control_num control_number, b.update_date error_datetime,
    	substring(be.message, charindex('message=', be.message)+8, 200) error
    from (
        select be.target_id pax_id, max(be.batch_id) last_batch_id
        from component.batch b
        	join component.batch_event be on b.BATCH_ID = be.BATCH_ID and be.target_table = 'PAX'
        where b.batch_type_code = 'MARS_ENRL'
        	-- No ABS account exists for the participant
        	and not exists (select 1
        						from component.pax_account pa
        							where pa.pax_id = be.target_id)
        group by be.target_id
    ) lea -- last enrollment attempt
    join component.batch_event be with (nolock) on be.batch_id = lea.last_batch_id and be.target_table = 'PAX' and be.target_id = lea.pax_id
    join component.batch b with (nolock) on b.batch_id = lea.last_batch_id
    join component.pax p with (nolock) on p.pax_id = lea.pax_id

   GO