-- ==============================  NAME  ======================================
-- View: VW_PAX_PERMISSION       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411    NONE        REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_PAX_PERMISSION] as

SELECT 
    VPR.[LEVEL]
,    R.PERMISSION_ID
,    R.PERMISSION_CODE AS PERMISSION
,    VPR.ROLE_ID
,    VPR.ROLE
,    VPR.QUALIFIER
,    VPR.TARGET_TABLE
,    VPR.TARGET_ID
,    VPR.SUBJECT_TABLE
,    VPR.SUBJECT_ID
,    VPR.PAX_ID
FROM 
    COMPONENT.VW_PAX_ROLE VPR
LEFT JOIN COMPONENT.ROLE_PERMISSION RM
    ON RM.ROLE_ID = VPR.ROLE_ID
LEFT JOIN COMPONENT.PERMISSION R
    ON R.PERMISSION_ID = RM.PERMISSION_ID
GO