-- ==============================  NAME  ======================================
-- VW_SPIN_AWARD_POOL
-- ===========================  DESCRIPTION  ==================================
-- Returns award pool information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111212    NONE        Created
-- KNIGHTGA        20150818    NONE        Updated for UDM 3
-- KNIGHTGA        20170113    NONE        Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_AWARD_POOL] as
SELECT
    PG.PROGRAM_ID

,    CAST(PGA.AWARD_AMOUNT AS INT) AS DENOMINATION
,    MAX(PGA.QUANTITY) AS QUANTITY
,    SUM(CASE WHEN th.ID IS NOT NULL AND TH.PAX_GROUP_ID IS NULL THEN 1 ELSE 0 END) AS AWARDS_AVAILABLE
,    SUM(CASE WHEN th.ID IS NOT NULL AND TH.PAX_GROUP_ID IS NOT NULL THEN 1 ELSE 0 END) AS AWARDS_USED
FROM
    COMPONENT.PROGRAM_GAME PG
INNER JOIN COMPONENT.PROGRAM_GAME_AWARD PGA
    ON PGA.PROGRAM_GAME_ID = PG.ID
left join component.spin_award sa
       on sa.program_game_award_id = pga.id
LEFT JOIN COMPONENT.TRANSACTION_HEADER TH 
    ON TH.ID = sa.TRANSACTION_HEADER_ID
    AND TH.STATUS_TYPE_CODE IN ('APPROVED', 'HOLD')
GROUP BY
    PG.PROGRAM_ID
,    CAST(PGA.AWARD_AMOUNT AS INT)
GO