/****** Object:  View [component].[VW_BUDGET_TOTALS]    Script Date: 6/12/2017 12:39:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_TREE 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170802    MP-10730    CREATED VIEW TO RETURN ALL BUDGETS UNDER A ROOT BUDGET
--
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW component.VW_BUDGET_TREE
AS
WITH BUDGET_NODE(BUDGET_ID, ROOT) AS
(
    SELECT
        ID AS BUDGET_ID,
        ID
    FROM component.BUDGET
    UNION ALL
    SELECT
        BUDGET.ID,
        BUDGET_NODE.ROOT
    FROM component.BUDGET
    INNER JOIN BUDGET_NODE
        ON BUDGET.PARENT_BUDGET_ID = BUDGET_NODE.BUDGET_ID
)
SELECT * FROM BUDGET_NODE