-- ==============================  NAME  ======================================
-- View: VW_PAX_GROUP_GROUPS     
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_PAX_GROUP_GROUPS] as
SELECT  FK1 AS PAX_GROUP_ID
,       FK2 AS GROUP_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 500
GO