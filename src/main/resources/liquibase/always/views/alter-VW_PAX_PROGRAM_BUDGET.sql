GO

/****** Object:  View [component].[VW_PAX_PROGRAM_BUDGET]    Script Date: 3/25/2020 10:47:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- View: VW_PAX_PROGRAM_BUDGET       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170303    MP-9648        CREATED
-- burgestl      20191230                   Fix log runing query
-- select * from [component].[VW_PAX_PROGRAM_BUDGET]
-- arenasw       20200603    CNXT-120       removing implicit conversion in favor of explicit one.
-- FLORESC       2021-03-30  CNXT-462       Removed 'OR' in LEFT JOIN to improve performance.
-- ===========================  DECLARATIONS  =================================
ALTER VIEW [component].[VW_PAX_PROGRAM_BUDGET] AS


-- one level deep (enrollment group)
--if object_id('tempdb..#temp') is not null begin 	drop table #temp end
with tTemp  (group_id, pax_id )  as
(
select distinct group_id, pax_id 
--into #temp
from component.groups_pax (nolock)
union all
-- two levels deep (enrollment group in a custom group)
select g.group_id, gp.pax_id 
from component.groups g (nolock)
inner join component.ASSOCIATION assoc (nolock) on assoc.fk1 = g.group_id
										  and assoc.association_type_id = 1000
inner join component.groups_pax gp (nolock) on assoc.fk2 = gp.group_id
union all
-- three levels deep (enrollment group in a custom group in a personal group)
select g.group_id, gp.pax_id 
from component.groups g (nolock)
inner join component.ASSOCIATION assoc (nolock) on assoc.fk1 = g.group_id
                                          and assoc.association_type_id = 1000
inner join component.ASSOCIATION assoc2 (nolock) on assoc2.fk1 = assoc.fk2
                                 and assoc2.association_type_id = 1000
inner join component.groups_pax gp (nolock) on assoc2.fk2 = gp.group_id
)


, tIS_USABLE (ID,	BUDGET_ID,	 VF_NAME,	 MISC_DATA,	MISC_DATE,	MISC_TYPE_CODE,	CREATE_DATE,	CREATE_ID,	UPDATE_DATE,	UPDATE_ID) as
(
 select ID,	BUDGET_ID,	 VF_NAME,	convert(varchar(30), MISC_DATA) MISC_DATA,	MISC_DATE,	MISC_TYPE_CODE,	CREATE_DATE,	CREATE_ID,	UPDATE_DATE,	UPDATE_ID
--into #IS_USABLE
 from component.BUDGET_MISC (nolock)
 where VF_NAME = 'IS_USABLE'
 and MISC_DATA = 'TRUE'
 )
, tTOP_LEVEL_BUDGET_ID (ID,	BUDGET_ID,	 VF_NAME,	MISC_DATA,	MISC_DATE,	MISC_TYPE_CODE,	CREATE_DATE,	CREATE_ID,	UPDATE_DATE,	UPDATE_ID) as
(
select ID,	BUDGET_ID,	 VF_NAME,	convert(int, MISC_DATA) MISC_DATA,	MISC_DATE,	MISC_TYPE_CODE,	CREATE_DATE,	CREATE_ID,	UPDATE_DATE,	UPDATE_ID
--into #TOP_LEVEL_BUDGET_ID
 from component.BUDGET_MISC (nolock)
 where VF_NAME = 'TOP_LEVEL_BUDGET_ID'
)


	, tOne (BUDGET_ID, PROGRAM_ID, PAX_ID) as
	(
    SELECT DISTINCT
        PROGRAM_BUDGET.BUDGET_ID AS BUDGET_ID,
        PROGRAM_BUDGET.PROGRAM_ID,
        CASE WHEN GTM.pax_id IS NOT NULL THEN GTM.PAX_ID ELSE ACL.SUBJECT_ID END AS PAX_ID
    FROM component.PROGRAM_BUDGET (nolock)
    INNER JOIN component.ACL (nolock) ON PROGRAM_BUDGET.ID = ACL.TARGET_ID AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
    INNER JOIN component.ROLE (nolock) ON ACL.ROLE_ID = ROLE.ROLE_ID AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
    LEFT JOIN tTemp --component.VW_GROUP_TOTAL_MEMBERSHIP 
	          GTM (nolock) ON ACL.SUBJECT_TABLE = 'GROUPS' AND ACL.SUBJECT_ID = GTM.group_id  
    )

, tTwo (BUDGET_ID, PAX_ID) as
(
SELECT
        ACL.TARGET_ID AS BUDGET_ID,
        CASE WHEN GTM.pax_id IS NOT NULL THEN GTM.PAX_ID ELSE ACL.SUBJECT_ID END AS PAX_ID
    FROM component.ACL (nolock)
    INNER JOIN component.ROLE (nolock) ON ACL.ROLE_ID = ROLE.ROLE_ID AND ROLE.ROLE_CODE = 'BUDGET_USER' 
    LEFT JOIN  tTemp --component.VW_GROUP_TOTAL_MEMBERSHIP 
	                                                        GTM (nolock) ON ACL.SUBJECT_TABLE = 'GROUPS' AND ACL.SUBJECT_ID = GTM.group_id
    WHERE ACL.TARGET_TABLE = 'BUDGET'
), PARAMS as
(
SELECT
    today = convert(datetime2(7),getdate(),0)
)

SELECT DISTINCT
PAX.PAX_ID,
BUDGET_GIVE_ELIGIBILTY.PROGRAM_ID,
 CASE WHEN BUDGET_USER.BUDGET_ID IS NOT NULL THEN BUDGET_USER.BUDGET_ID ELSE BUDGET_GIVE_ELIGIBILTY.BUDGET_ID END AS BUDGET_ID
FROM component.PAX (nolock)
INNER JOIN PARAMS ON 1=1
LEFT JOIN (
	select * from tOne
) BUDGET_GIVE_ELIGIBILTY
    ON (
        BUDGET_GIVE_ELIGIBILTY.PAX_ID = PAX.PAX_ID
    )
LEFT JOIN component.BUDGET CHILD_BUDGET (nolock)
    ON BUDGET_GIVE_ELIGIBILTY.BUDGET_ID = CHILD_BUDGET.PARENT_BUDGET_ID
LEFT JOIN tTOP_LEVEL_BUDGET_ID --component.BUDGET_MISC (nolock)
		BUDGET_MISC (nolock)
    ON BUDGET_GIVE_ELIGIBILTY.BUDGET_ID = BUDGET_MISC.MISC_DATA
    --AND BUDGET_MISC.VF_NAME = 'TOP_LEVEL_BUDGET_ID'
LEFT JOIN (
select * from tTwo
) BUDGET_USER
    ON (
        BUDGET_USER.PAX_ID = PAX.PAX_ID
        AND (
            CHILD_BUDGET.ID = BUDGET_USER.BUDGET_ID
            OR
            BUDGET_MISC.BUDGET_ID = BUDGET_USER.BUDGET_ID
        )
    )
LEFT JOIN tIS_USABLE -- component.BUDGET_MISC 
          IS_USABLE (nolock)
    ON IS_USABLE.BUDGET_ID = BUDGET_GIVE_ELIGIBILTY.BUDGET_ID
LEFT JOIN component.BUDGET (nolock)
ON BUDGET.ID = BUDGET_GIVE_ELIGIBILTY.BUDGET_ID --OR BUDGET.ID = BUDGET_USER.BUDGET_ID
WHERE BUDGET.FROM_DATE < today AND (BUDGET.THRU_DATE IS NULL OR BUDGET.THRU_DATE > today) -- Only ACTIVE budgets
AND ( IS_USABLE.MISC_DATA = 'TRUE' 
)
union all

SELECT DISTINCT
PAX.PAX_ID,
BUDGET_GIVE_ELIGIBILTY.PROGRAM_ID,
 CASE WHEN BUDGET_USER.BUDGET_ID IS NOT NULL THEN BUDGET_USER.BUDGET_ID ELSE BUDGET_GIVE_ELIGIBILTY.BUDGET_ID END AS BUDGET_ID
FROM component.PAX (nolock)
INNER JOIN PARAMS ON 1=1
LEFT JOIN (
	select * from tOne
) BUDGET_GIVE_ELIGIBILTY
    ON (
        BUDGET_GIVE_ELIGIBILTY.PAX_ID = PAX.PAX_ID
    )
LEFT JOIN component.BUDGET CHILD_BUDGET (nolock)
    ON BUDGET_GIVE_ELIGIBILTY.BUDGET_ID = CHILD_BUDGET.PARENT_BUDGET_ID
LEFT JOIN tTOP_LEVEL_BUDGET_ID --component.BUDGET_MISC (nolock)
		BUDGET_MISC (nolock)
    ON BUDGET_GIVE_ELIGIBILTY.BUDGET_ID = BUDGET_MISC.MISC_DATA
LEFT JOIN (
	select * from tTwo
) BUDGET_USER
    ON (
        BUDGET_USER.PAX_ID = PAX.PAX_ID
        AND (
            CHILD_BUDGET.ID = BUDGET_USER.BUDGET_ID
            OR
            BUDGET_MISC.BUDGET_ID = BUDGET_USER.BUDGET_ID
        )
    )
LEFT JOIN tIS_USABLE -- component.BUDGET_MISC 
          IS_USABLE (nolock)
    ON IS_USABLE.BUDGET_ID = BUDGET_GIVE_ELIGIBILTY.BUDGET_ID
LEFT JOIN component.BUDGET (nolock)
ON BUDGET.ID = BUDGET_GIVE_ELIGIBILTY.BUDGET_ID -- OR BUDGET.ID = BUDGET_USER.BUDGET_ID
WHERE BUDGET.FROM_DATE < today AND (BUDGET.THRU_DATE IS NULL OR BUDGET.THRU_DATE > today) -- Only ACTIVE budgets
AND (BUDGET_USER.BUDGET_ID IS NOT NULL
)


GO


