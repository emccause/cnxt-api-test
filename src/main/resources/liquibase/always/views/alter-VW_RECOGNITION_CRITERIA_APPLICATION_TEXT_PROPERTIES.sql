-- ==============================  NAME  ======================================
-- View: VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES      
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES] (RECOGNITION_CRITERIA_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS RECOGNITION_CRITERIA_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1400
GO