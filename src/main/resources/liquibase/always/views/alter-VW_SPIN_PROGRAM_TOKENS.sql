-- ==============================  NAME  ======================================
-- VW_SPIN_PROGRAM_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by program_activity_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111102    NONE        Created
-- KNIGHTGA        20150818    NONE        Updated for UDM 3
-- KNIGHTGA        20170113    NONE        Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_PROGRAM_TOKENS] as
SELECT
    TH.PROGRAM_ID AS PROGRAM_ID
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN case when TH.SUB_TYPE_CODE = 'IOU_TOKEN' then -1 else 1 end else 0 END) AS TOKENS_AVAILABLE
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN 0 ELSE 1 END) AS TOKENS_USED

,    count(th.id) AS TOKENS_TOTAL
FROM
    COMPONENT.TRANSACTION_HEADER TH
INNER JOIN COMPONENT.SPIN_TOKEN st
    ON st.TRANSACTION_HEADER_ID = TH.ID
WHERE TH.TYPE_CODE = 'SPIN_TOKEN'
  AND TH.STATUS_TYPE_CODE = 'APPROVED'
  AND TH.SUB_TYPE_CODE in ('SPIN_TOKEN', 'IOU_TOKEN')
GROUP BY
    TH.PROGRAM_ID
GO