-- ==============================  NAME  ======================================
-- View: VW_PROXY_PAX_PAX      
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_PROXY_PAX_PAX] as
SELECT  FK1 AS PROXY_PAX_ID
,       FK2 AS PAX_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2100
GO