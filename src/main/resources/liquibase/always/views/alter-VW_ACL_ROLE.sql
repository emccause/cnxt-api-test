-- ==============================  NAME  ======================================
-- View: VW_ACL_ROLE   
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411    NONE        CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_ACL_ROLE] as

SELECT
    R.ROLE_ID
,    R.ROLE_CODE AS [ROLE]
,    R.ROLE_TYPE_CODE
,    A.SUBJECT_TABLE
,    A.SUBJECT_ID
,    A.TARGET_TABLE
,    A.TARGET_ID
FROM    
    COMPONENT.ACL A
INNER JOIN COMPONENT.ROLE R
    ON  R.ROLE_ID = A.ROLE_ID
GO