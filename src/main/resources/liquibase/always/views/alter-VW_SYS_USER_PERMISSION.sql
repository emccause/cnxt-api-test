-- ==============================  NAME  ======================================
-- View: VW_SYS_USER_PERMISSION       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SYS_USER_PERMISSION] (PAX_ID, PERMISSION_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS SYS_USER_ID
,       FK2 AS PERMISSION_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2200
GO