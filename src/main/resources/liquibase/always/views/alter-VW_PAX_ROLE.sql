-- ==============================  NAME  ======================================
-- View: VW_PAX_ROLE       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411    NONE        REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- MUDDAM        20160505    NONE        UPDATES FOR CORE 4.5.0 - The @ qualifier was changed to owner_group_id from pax_group_id
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_PAX_ROLE] as
WITH ROLES (
    [LEVEL]
,    ROLE_ID
,    [ROLE]
,    QUALIFIER
,    TARGET_TABLE
,    TARGET_ID
,    SUBJECT_TABLE
,    SUBJECT_ID
,    PAX_ID
) AS (
    --========================================================================================
    --ANCHOR QUERY
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    A.SUBJECT_ID AS PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    WHERE
        A.SUBJECT_TABLE = 'PAX'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    P.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.PAX_TYPE PT
        ON PT.ID = A.SUBJECT_ID
    INNER JOIN COMPONENT.PAX P
        ON P.PAX_TYPE_CODE = PT.PAX_TYPE_CODE
    WHERE
        A.SUBJECT_TABLE = 'PAX_TYPE'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    GP.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.GROUPS_PAX GP
        ON GP.GROUP_ID = A.SUBJECT_ID
    WHERE
        A.SUBJECT_TABLE = 'GROUPS'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.PAX_GROUP_ID = A.SUBJECT_ID
        AND PG.THRU_DATE IS NULL
    WHERE
        A.SUBJECT_TABLE = 'PAX_GROUP'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(PG.ORGANIZATION_CODE + ':' + PG.ROLE_CODE AS NVARCHAR(255)) AS ROLE_CODE
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    NULL AS TARGET_TABLE
    ,    NULL AS TARGET_ID
    ,    NULL AS SUBJECT_TABLE
    ,    NULL AS SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.PAX_GROUP PG
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_CODE = PG.ROLE_CODE
    WHERE
        PG.THRU_DATE IS NULL

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.ORG_GROUP OG
        ON OG.ORG_GROUP_ID = A.SUBJECT_ID
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.ORGANIZATION_CODE = OG.ORGANIZATION_CODE
        AND PG.ROLE_CODE = OG.ROLE_CODE
        AND PG.THRU_DATE IS NULL
    WHERE
        A.SUBJECT_TABLE = 'ORG_GROUP'
    --========================================================================================
    --RECURSIVE QUERY
 --   UNION ALL

    ----get all the roles assigned to the anchor roles
    --select
    --     resolved.LEVEL + 1 as LEVEL
    --    ,r.ROLE_ID
    --    ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
    --    ,resolved.QUALIFIER
    --    ,coalesce(resolved.TARGET_TABLE, acl.TARGET_TABLE) as TARGET_TABLE
    --    ,coalesce(resolved.TARGET_ID, acl.TARGET_ID) as TARGET_ID
    --    ,acl.SUBJECT_TABLE
    --    ,acl.SUBJECT_ID
    --    ,resolved.PAX_ID
    --from roles resolved
    --      inner join component.acl acl
    --              on acl.subject_id = resolved.role_id
    --            and acl.subject_table = 'ROLE'
    --     inner join component.[role] r on r.role_id = acl.role_id
 --  where acl.role_id <> acl.subject_id
    -- and (
 --           (resolved.target_table is null and acl.target_table is null)
 --           or
 --           (resolved.target_table = acl.target_table and resolved.target_id = acl.target_id)
 --           or
 --           (resolved.target_table is null and acl.target_table is not null and resolved.target_id is null and acl.target_id is not null)
 --        )
)

SELECT DISTINCT
    [LEVEL]
,    ROLE_ID
,    [ROLE]
,    QUALIFIER
,    TARGET_TABLE
,    TARGET_ID
,    SUBJECT_TABLE
,    SUBJECT_ID
,    PAX_ID
FROM
    ROLES
GO