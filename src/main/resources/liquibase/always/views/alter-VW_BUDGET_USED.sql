/****** Object:  View [component].[VW_BUDGET_USED]    Script Date: 6/12/2017 12:39:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_USED 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170612    MP-9785        Created view to determine if a budget or any sub-budget has been used - tied to a program or has transactions
--
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW component.VW_BUDGET_USED AS
WITH BUDGET_NODE(BUDGET_ID, ROOT) AS
(
    SELECT
        ID AS BUDGET_ID,
        ID
    FROM component.BUDGET
    UNION ALL
    SELECT
        BUDGET.ID,
        BUDGET_NODE.ROOT
    FROM component.BUDGET
    INNER JOIN BUDGET_NODE
        ON BUDGET.PARENT_BUDGET_ID = BUDGET_NODE.BUDGET_ID
)
SELECT
    ROOT AS BUDGET_ID,
    CASE
        WHEN MIN(TRANSACTION_HEADER.ID) IS NOT NULL THEN 'TRUE'
        WHEN MIN(PROGRAM_BUDGET.PROGRAM_ID) IS NOT NULL THEN 'TRUE'
        ELSE 'FALSE'
    END AS IS_USED
FROM BUDGET_NODE
LEFT JOIN component.DISCRETIONARY
    ON DISCRETIONARY.BUDGET_ID_CREDIT = BUDGET_NODE.BUDGET_ID
    OR DISCRETIONARY.BUDGET_ID_DEBIT = BUDGET_NODE.BUDGET_ID
LEFT JOIN component.TRANSACTION_HEADER
    ON TRANSACTION_HEADER.ID = DISCRETIONARY.TRANSACTION_HEADER_ID 
    AND TRANSACTION_HEADER.TYPE_CODE = 'DISC'
    AND TRANSACTION_HEADER.SUB_TYPE_CODE <> 'FUND' 
    AND TRANSACTION_HEADER.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
LEFT JOIN component.PROGRAM_BUDGET
    ON BUDGET_NODE.BUDGET_ID = PROGRAM_BUDGET.BUDGET_ID
GROUP BY ROOT