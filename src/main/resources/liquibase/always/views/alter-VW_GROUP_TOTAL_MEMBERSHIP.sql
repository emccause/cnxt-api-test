GO

/****** Object:  View [component].[VW_GROUP_TOTAL_MEMBERSHIP]    Script Date: 3/25/2020 10:50:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- View: VW_GROUP_TOTAL_MEMBERSHIP
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- KUMARSJ      20170307    MP-9674     DROP nested select and use UNION ALL for performance
-- burgestl		20200113				  added with (nolock)
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_GROUP_TOTAL_MEMBERSHIP] as

--with allGroups (group_id, pax_id) as
--(
 --select distinct group_id, pax_id from component.groups_pax with (nolock)
--)




-- one level deep (enrollment group)
select distinct group_id, pax_id from component.groups_pax with (nolock)   --:31 seconds / 32 seconds with nolock
union all
-- two levels deep (enrollment group in a custom group)
select g.group_id, gp.pax_id from component.groups g   with (nolock)
inner join component.ASSOCIATION assoc  with (nolock)
on assoc.fk1 = g.group_id
and assoc.association_type_id = 1000
inner join component.groups_pax gp with (nolock)
on assoc.fk2 = gp.group_id
union all
-- three levels deep (enrollment group in a custom group in a personal group)
select g.group_id, gp.pax_id from component.groups g with (nolock)
inner join component.ASSOCIATION assoc with (nolock)
on assoc.fk1 = g.group_id
and assoc.association_type_id = 1000
inner join component.ASSOCIATION assoc2 with (nolock)
on assoc2.fk1 = assoc.fk2
and assoc2.association_type_id = 1000
inner join component.groups_pax gp with (nolock)
on assoc2.fk2 = gp.group_id


GO


