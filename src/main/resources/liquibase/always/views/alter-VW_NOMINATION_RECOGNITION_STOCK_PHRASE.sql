-- ==============================  NAME  ======================================
-- View: VW_NOMINATION_RECOGNITION_STOCK_PHRASE     
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_NOMINATION_RECOGNITION_STOCK_PHRASE] (NOMINATION_ID, RECOGNITION_STOCK_PHRASE_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS NOMINATION_ID
,       FK2 AS RECOGNITION_STOCK_PHRASE_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2400
GO