-- ==============================  NAME  ======================================
-- View: VW_PROGRAM_RECOGNITION_STOCK_PHRASE       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_PROGRAM_RECOGNITION_STOCK_PHRASE] (PROGRAM_ID, RECOGNITION_STOCK_PHRASE_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS RECOGNITION_STOCK_PHRASE_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2000
GO