/****** Object:  View [component].[VW_BUDGET_INFO_LIGHTWEIGHT]    Script Date: 6/12/2017 12:39:20 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_INFO_LIGHTWEIGHT 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170612    MP-9785        Reworked VW_BUDGET_INFO to remove calculated columns that lead to poor performance
-- MUDDAM        20170928    MP-8001        Moved BUDGET_NAME to BUDGET_CODE, BUDGET_DISPLAY_NAME to BUDGET_NAME
-- FARFANLA        20171130    MP-11051    ADDING OVERRIDE FIELDS (PROJECT, SUB_PROJECT, LOCATION)
--
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW [component].[VW_BUDGET_INFO_LIGHTWEIGHT] AS
    SELECT 
        b.ID
    ,    b.BUDGET_TYPE_CODE
    ,    b.BUDGET_CODE AS BUDGET_NAME
    ,    CASE
            WHEN PARENT_ENTITY_NAME.MISC_DATA IS NOT NULL AND TIED_ENTITY_NAME.MISC_DATA IS NOT NULL 
                THEN b.BUDGET_NAME + ' - ' +  PARENT_ENTITY_NAME.MISC_DATA + ' - ' + TIED_ENTITY_NAME.MISC_DATA
            WHEN TIED_ENTITY_NAME.MISC_DATA IS NOT NULL 
                THEN b.BUDGET_NAME + ' - ' + TIED_ENTITY_NAME.MISC_DATA
            ELSE b.BUDGET_NAME 
        END AS DISPLAY_NAME
    ,    b.BUDGET_DESC
    ,    b.PARENT_BUDGET_ID
    ,    b.INDIVIDUAL_GIVING_LIMIT
    ,    b.FROM_DATE
    ,    b.THRU_DATE
    ,    b.STATUS_TYPE_CODE
    ,    b.SUB_PROJECT_ID
    ,    b.BUDGET_CONSTRAINT
    ,    p.PROJECT_NUMBER
    ,    sp.SUB_PROJECT_NUMBER
    ,    b.CREATE_DATE
    ,    b.ALERT_AMOUNT
    ,    GROUPS_INFO.GROUP_CONFIG_ID
    ,    GROUPS_INFO.GROUP_ID
    ,    GROUPS_INFO.GROUP_DESC as 'GROUP_NAME'
    ,    A.SUBJECT_ID AS 'BUDGET_OWNER'
    ,    ACL_ALLOCATOR.SUBJECT_ID AS 'BUDGET_ALLOCATOR'
    ,     ALLOCATED_IN_MISC.MISC_DATA AS 'ALLOCATED_IN'
    ,     ALLOCATED_OUT_MISC.MISC_DATA AS 'ALLOCATED_OUT'
    ,    ISNULL(IS_USABLE_MISC.MISC_DATA,'TRUE') AS 'IS_USABLE'
    ,    OVERRIDE_PROJ.PROJECT_NUMBER AS 'OVERRIDE_PROJECT_NUMBER'
    ,    OVERRIDE_SUB_PROJ.SUB_PROJECT_NUMBER AS 'OVERRIDE_SUB_PROJECT_NUMBER'
    ,    OVERRIDE_MISC.VF_NAME AS 'OVERRIDE_LOCATION'
    FROM component.BUDGET b
    LEFT JOIN component.SUB_PROJECT sp ON sp.ID = b.SUB_PROJECT_ID
    LEFT JOIN component.PROJECT p ON p.ID = sp.PROJECT_ID

    LEFT JOIN ( --Groups Info
        SELECT ACL.TARGET_ID AS 'BUDGET_ID', g.GROUP_ID, g.GROUP_DESC, ISNULL(g.GROUP_CONFIG_ID, gc.ID) AS 'GROUP_CONFIG_ID'
        FROM component.ACL
        JOIN component.ROLE r ON r.ROLE_ID = ACL.ROLE_ID
        LEFT JOIN component.GROUPS g ON g.GROUP_ID = ACL.SUBJECT_ID AND ACL.SUBJECT_TABLE = 'GROUPS'
        LEFT JOIN component.GROUP_CONFIG gc ON gc.ID = ACL.SUBJECT_ID AND ACL.SUBJECT_TABLE = 'GROUP_CONFIG'
        WHERE r.ROLE_CODE = 'BUDGET_USER'
        AND ACL.TARGET_TABLE = 'BUDGET' 
        AND ACL.SUBJECT_TABLE IN ('GROUPS', 'GROUP_CONFIG')
    ) GROUPS_INFO ON GROUPS_INFO.BUDGET_ID = b.ID
    -- BUDGET OWNER
    LEFT JOIN component.ROLE R ON R.ROLE_CODE = 'BUDGET_OWNER'
    LEFT JOIN component.ACL A ON A.ROLE_ID = R.ROLE_ID 
        AND A.TARGET_TABLE = 'BUDGET' 
        AND A.TARGET_ID = B.ID
        AND A.SUBJECT_TABLE = 'PAX'
    -- BUDGET ALLOCATOR
    LEFT JOIN component.ROLE R_ALLOCATOR ON R_ALLOCATOR.ROLE_CODE = 'BUDGET_ALLOCATOR'
    LEFT JOIN component.ACL ACL_ALLOCATOR ON ACL_ALLOCATOR.ROLE_ID = R_ALLOCATOR.ROLE_ID 
        AND ACL_ALLOCATOR.TARGET_TABLE = 'BUDGET' 
        AND ACL_ALLOCATOR.TARGET_ID = B.ID
        AND ACL_ALLOCATOR.SUBJECT_TABLE = 'PAX'
    -- Allocated budget - diplay name    
    LEFT JOIN component.BUDGET_MISC PARENT_ENTITY_NAME 
        ON PARENT_ENTITY_NAME.BUDGET_ID = B.ID 
            AND PARENT_ENTITY_NAME.VF_NAME = 'PARENT_ENTITY_NAME '
    LEFT JOIN component.BUDGET_MISC TIED_ENTITY_NAME 
        ON TIED_ENTITY_NAME.BUDGET_ID = B.ID 
            AND TIED_ENTITY_NAME.VF_NAME = 'TIED_ENTITY_NAME '        
    -- Allocated budget - amount allocated
    LEFT JOIN component.BUDGET_MISC ALLOCATED_IN_MISC
        ON ALLOCATED_IN_MISC.BUDGET_ID = B.ID 
            AND ALLOCATED_IN_MISC.VF_NAME = 'ALLOCATED_IN'
    LEFT JOIN component.BUDGET_MISC ALLOCATED_OUT_MISC
        ON ALLOCATED_OUT_MISC.BUDGET_ID = B.ID 
            AND ALLOCATED_OUT_MISC.VF_NAME = 'ALLOCATED_OUT'
    -- Is Usable - budget misc
    LEFT JOIN component.BUDGET_MISC IS_USABLE_MISC
        ON IS_USABLE_MISC.BUDGET_ID = B.ID 
            AND IS_USABLE_MISC.VF_NAME = 'IS_USABLE'
    -- Override Sub Project - budget misc
    LEFT JOIN component.BUDGET_MISC OVERRIDE_MISC
        ON OVERRIDE_MISC.BUDGET_ID = B.ID 
            AND OVERRIDE_MISC.MISC_TYPE_CODE = 'SUB_PROJECT_OVERRIDE'
    -- Override Sub Project - Sub Project
    LEFT JOIN component.SUB_PROJECT OVERRIDE_SUB_PROJ
        ON OVERRIDE_SUB_PROJ.ID = OVERRIDE_MISC.MISC_DATA 
    -- Override Project - Project
    LEFT JOIN component.PROJECT OVERRIDE_PROJ
        ON OVERRIDE_PROJ.ID = OVERRIDE_SUB_PROJ.PROJECT_ID 
GO


