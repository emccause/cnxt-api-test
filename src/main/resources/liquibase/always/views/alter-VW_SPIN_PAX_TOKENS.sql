-- ==============================  NAME  ======================================
-- VW_SPIN_PAX_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by pax_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20111212    NONE        Created
-- KNIGHTGA        20150818    NONE        Updated for UDM 3
-- KNIGHTGA        20170113    NONE        Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
-- KNIGHTGA        20170126    NONE        Added TOKEN_AWARDS column
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_PAX_TOKENS] as
SELECT
    TH.PROGRAM_ID AS PROGRAM_ID

,    PG.PAX_ID AS PAX_ID
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN case when TH.SUB_TYPE_CODE = 'IOU_TOKEN' then -1 else 1 end else 0 END) AS TOKENS_AVAILABLE
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NOT NULL THEN 1 ELSE 0 END) AS TOKENS_USED
,    cast(SUM(CASE WHEN PGA.ID IS NOT NULL THEN pga.AWARD_AMOUNT ELSE 0 END) as INTEGER) AS TOKEN_AWARDS
,    count(th.id) AS TOKENS_TOTAL
FROM
    TRANSACTION_HEADER TH
INNER JOIN SPIN_TOKEN st
    ON st.TRANSACTION_HEADER_ID = TH.ID
INNER JOIN PAX_GROUP PG
    ON PG.PAX_GROUP_ID = TH.PAX_GROUP_ID
LEFT JOIN SPIN_AWARD sa
    ON sa.ID = st.SPIN_AWARD_ID
LEFT JOIN PROGRAM_GAME_AWARD pga
    ON pga.ID = sa.PROGRAM_GAME_AWARD_ID
WHERE TH.TYPE_CODE = 'SPIN_TOKEN'
  AND TH.STATUS_TYPE_CODE = 'APPROVED'
  AND TH.SUB_TYPE_CODE in ('SPIN_TOKEN', 'IOU_TOKEN')
GROUP BY
    TH.PROGRAM_ID
,    PG.PAX_ID
GO