GO

/****** Object:  View [component].[VW_BUDGET_TOTALS]    Script Date: 3/25/2020 10:53:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_TOTALS 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170612    MP-9785        Created view to determine TOTAL_AMOUNT and TOTAL_USED for a budget and all sub-budgets
--
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW [component].[VW_BUDGET_TOTALS] AS
WITH BUDGET_NODE(BUDGET_ID, ROOT) AS
(
    SELECT
        ID AS BUDGET_ID,
        ID
    FROM component.BUDGET with (nolock)
    WHERE component.BUDGET.STATUS_TYPE_CODE <> 'INACTIVE'
    UNION ALL
    SELECT
        BUDGET.ID,
        BUDGET_NODE.ROOT
    FROM component.BUDGET with (nolock)
    INNER JOIN BUDGET_NODE
        ON BUDGET.PARENT_BUDGET_ID = BUDGET_NODE.BUDGET_ID
)
SELECT
BUDGET_NODE.ROOT AS BUDGET_ID,
SUM(ISNULL(TOTAL_USED.TOTAL_USED, 0)) AS 'TOTAL_USED',
SUM(ISNULL(TOTAL_AMOUNT.TOTAL_AMOUNT, 0)) AS 'TOTAL_AMOUNT'
FROM BUDGET_NODE with (nolock)
JOIN component.BUDGET b with (nolock) ON b.ID = BUDGET_NODE.BUDGET_ID
LEFT JOIN ( --TOTAL_USED
    --Calculate the total budget used
    SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
    (CASE 
        WHEN DEBITS.DEBITS IS NULL THEN 0 
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS IS NULL THEN 0 
        ELSE CREDITS.CREDITS  
    END)
    AS 'TOTAL_USED'
    FROM component.BUDGET b with (nolock)
    LEFT JOIN (
        --DEBITS
        SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.BUDGET b with (nolock) 
        LEFT JOIN component.DISCRETIONARY disc_debit with (nolock) ON disc_debit.BUDGET_ID_DEBIT = b.ID 
        JOIN component.TRANSACTION_HEADER th_debit with (nolock) ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE <> 'FUND' 
            AND th_debit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
        GROUP BY b.ID ) DEBITS
        ON b.ID = DEBITS.ID
    LEFT JOIN (
        --CREDITS
        SELECT b.ID, SUM(disc_credit.AMOUNT) 'CREDITS'
        FROM component.BUDGET b with (nolock) 
        LEFT JOIN component.DISCRETIONARY disc_credit with (nolock) ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN component.TRANSACTION_HEADER th_credit with (nolock) ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE <> 'FUND'
            AND th_credit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
        GROUP BY b.ID ) CREDITS 
    ON b.ID = CREDITS.ID
) TOTAL_USED
    ON b.ID = TOTAL_USED.ID
LEFT JOIN ( --TOTAL_AMOUNT
    --Calculate the total budget amount
    SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
    (CASE
        WHEN CREDITS.CREDITS IS NULL THEN 0 
        ELSE CREDITS.CREDITS  
    END)
    -
    (CASE 
        WHEN DEBITS.DEBITS IS NULL THEN 0 
        ELSE DEBITS.DEBITS
    END)
    AS 'TOTAL_AMOUNT'
    FROM component.BUDGET b with (nolock)
    LEFT JOIN (
        --DEBITS
        SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.BUDGET b with (nolock) 
        LEFT JOIN component.DISCRETIONARY disc_debit with (nolock) ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN component.TRANSACTION_HEADER th_debit with (nolock) ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        GROUP BY b.ID ) DEBITS
        ON b.ID = DEBITS.ID
    LEFT JOIN (
        --CREDITS
        SELECT b.ID, SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.BUDGET b with (nolock) 
        LEFT JOIN component.DISCRETIONARY disc_credit with (nolock) ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN component.TRANSACTION_HEADER th_credit with (nolock) ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
    GROUP BY b.ID ) CREDITS 
    ON b.ID = CREDITS.ID 
) TOTAL_AMOUNT
    ON b.ID = TOTAL_AMOUNT.ID
-- BUDGET OWNER
LEFT JOIN component.ROLE R with (nolock) ON R.ROLE_CODE = 'BUDGET_OWNER'
LEFT JOIN component.ACL A with (nolock) ON A.ROLE_ID = R.ROLE_ID 
    AND A.TARGET_TABLE = 'BUDGET' 
    AND A.TARGET_ID = b.ID
    AND A.SUBJECT_TABLE = 'PAX'
LEFT JOIN component.PAX p with (nolock) ON p.PAX_ID = A.SUBJECT_ID
-- Allocated budget - diplay name    
LEFT JOIN component.BUDGET_MISC PARENT_ENTITY_NAME with (nolock) 
    ON PARENT_ENTITY_NAME.BUDGET_ID = b.ID
        AND PARENT_ENTITY_NAME.VF_NAME = 'PARENT_ENTITY_NAME '
LEFT JOIN component.BUDGET_MISC TIED_ENTITY_NAME with (nolock) 
    ON TIED_ENTITY_NAME.BUDGET_ID = b.ID
        AND TIED_ENTITY_NAME.VF_NAME = 'TIED_ENTITY_NAME '
GROUP BY ROOT


GO


