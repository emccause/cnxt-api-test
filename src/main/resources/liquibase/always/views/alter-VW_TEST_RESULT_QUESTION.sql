/*==============================================================*/
/* View: VW_TEST_RESULT_QUESTION                                */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_QUESTION
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result questions by test result id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150925    NONE        Created
-- KNIGHTGA        20151028    NONE        Updated for latest UDM 4 changes
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_TEST_RESULT_QUESTION] as
SELECT
    TR.TEST_ID

,    TRQ.TEST_RESULT_ID
,    TQ.TEST_SECTION_ID
,    TRQ.ID AS TEST_RESULT_QUESTION_ID
,    TQ.ID AS TEST_QUESTION_ID
,    TQ.TEST_QUESTION_TYPE_CODE
,    TQ.QUESTION_TEXT
,    TQ.QUESTION_WEIGHT
,    TQ.MAX_ANSWERS
,    TQ.REQ_ANSWERS
,    TQ.REFERENCE_URL
,    TQ.REFERENCE_NAME
,    TQ.IMAGE_URL
,    ISNULL(TRQ.DISPLAY_SEQUENCE, TQ.DISPLAY_SEQUENCE) AS DISPLAY_SEQUENCE
FROM
    COMPONENT.TEST_RESULT_QUESTION TRQ
INNER JOIN COMPONENT.TEST_QUESTION TQ
    ON TQ.ID = TRQ.TEST_QUESTION_ID
INNER JOIN COMPONENT.TEST_RESULT TR
    ON TR.ID = TRQ.TEST_RESULT_ID
GO