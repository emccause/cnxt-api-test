GO

/****** Object:  View [component].[VW_BUDGET_USERS]    Script Date: 3/25/2020 10:52:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_USERS 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MARTINC        20170613    MP-9785        Created view to return budget users
--
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW [component].[VW_BUDGET_USERS] AS
SELECT A.TARGET_ID AS BUDGET_ID, A.SUBJECT_TABLE AS BUDGET_USER_TYPE, A.SUBJECT_ID AS BUDGET_USER_ID
FROM component.ACL A with (nolock)
INNER JOIN component.ROLE R  with (nolock)
    ON A.ROLE_ID = R.ROLE_ID 
WHERE R.ROLE_CODE = 'BUDGET_USER'
    AND (A.SUBJECT_TABLE = 'PAX' OR A.SUBJECT_TABLE = 'GROUPS') 
    AND A.TARGET_TABLE = 'BUDGET'


GO


