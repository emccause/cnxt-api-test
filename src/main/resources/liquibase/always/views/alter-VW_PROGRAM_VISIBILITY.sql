-- ==============================  NAME  ======================================
-- View: VW_PROGRAM_VISIBILITY       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_PROGRAM_VISIBILITY] as

select
PROGRAM_ID,
case when public_vis is null then admin_vis
else public_vis
end as VISIBILITY
from (
select PRGM.PROGRAM_ID, public_vis.VISIBILTIY as PUBLIC_VIS, admin_vis.VISIBILTIY as ADMIN_VIS from 

PROGRAM PRGM
left join
(
select PRGM.PROGRAM_ID, 'PUBLIC' as VISIBILTIY from role r
inner join acl acl
on acl.subject_id = r.role_id
and acl.subject_table = 'ROLE'
and r.ROLE_DESC = 'RECOGNITION'
inner join role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'PVIS'
inner join PROGRAM PRGM
on PRGM.PROGRAM_ID = acl.target_id
and acl.target_table = 'PROGRAM'
) as public_vis
on PRGM.PROGRAM_ID = public_vis.PROGRAM_ID

left join
(
select distinct PRGM.PROGRAM_ID, 'ADMIN' as VISIBILTIY from role r
inner join acl acl
on acl.subject_id = r.role_id
and acl.subject_table = 'ROLE'
and r.ROLE_DESC like '%Admin%'
inner join role vis_role
on vis_role.ROLE_ID = acl.role_id
and vis_role.role_code = 'PVIS'
inner join PROGRAM PRGM
on PRGM.PROGRAM_ID = acl.target_id
and acl.target_table = 'PROGRAM'
) as admin_vis
on PRGM.PROGRAM_ID = admin_vis.PROGRAM_ID) as data
GO


