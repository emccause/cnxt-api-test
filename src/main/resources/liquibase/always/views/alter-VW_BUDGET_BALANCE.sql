-- ==============================  NAME  ======================================
-- VW_BUDGET_BALANCE
-- ===========================  DESCRIPTION  ==================================
-- Return the budget balance
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA     20150914    NONE        Created
-- HARWELLM        20161205    MP-8559        FIX BUDGET_TOTAL TO HANDLE BUDGET ALLOCATION
-- KNIGHTGA     20170116    NONE        Updated for the new UDM 5 spin tables (SPIN_AWARD)
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER VIEW [component].[VW_BUDGET_BALANCE]
AS

/*
select * from component.VW_BUDGET_BALANCE
select * from component.VW_BUDGET_BALANCE where program_id = 50

select bb.* 
  from component.VW_BUDGET_BALANCE bb
       inner join component.program_budget pb on pb.budget_id = bb.budget_id
 where pb.program_id = 50
*/

SELECT b.ID as BUDGET_ID
      ,isnull(discInfo.FUNDING, 0) as FUNDING
      ,isnull(discInfo.DEBITS, 0) + isnull(trInfo.DEBITS, 0) + isnull(saInfo.DEBITS, 0) as DEBITS
      ,isnull(discInfo.CREDITS, 0) + isnull(trInfo.CREDITS, 0) as CREDITS
      ,isnull(discInfo.BALANCE, 0) + isnull(trInfo.BALANCE, 0) + isnull(saInfo.BALANCE, 0) as BALANCE
  FROM component.BUDGET b
       left join (
                    select b.ID as budget_id
                          ,sum(case when th.sub_type_code = 'FUND' and disc.BUDGET_ID_DEBIT = b.id then -disc.amount 
                                    when th.sub_type_code = 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount
                                    else 0 
                                end) as FUNDING
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount 
                                    else 0 
                               end ) as CREDITS
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_DEBIT = b.id then disc.amount 
                                    else 0 
                               end ) as DEBITS
                          ,sum(case when disc.BUDGET_ID_DEBIT = b.id then -disc.amount else disc.amount end) as BALANCE
                     FROM component.DISCRETIONARY disc 
                          inner join component.BUDGET b on b.id = isnull(disc.BUDGET_ID_CREDIT, disc.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = disc.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as discInfo
              ON discInfo.budget_id = b.ID
       left join (
                    select b.ID as budget_id
                          ,sum(case when tr.BUDGET_ID_CREDIT = b.id then tr.earnings_amount else 0 end) AS CREDITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then tr.earnings_amount else 0 end) AS DEBITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then -tr.earnings_amount else tr.earnings_amount end) as BALANCE
                     FROM component.transaction_result tr
                          inner join component.BUDGET b on b.id = isnull(tr.BUDGET_ID_CREDIT, tr.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = tr.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as trInfo
              ON trInfo.budget_id = b.ID
       left join (
                    select sa.BUDGET_ID
                          ,sum(pga.award_amount) as DEBITS
                          ,sum(-pga.award_amount) as BALANCE
                     FROM component.SPIN_AWARD sa                         
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = sa.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'APPROVED')
                          inner join component.program_game_award pga on pga.id = sa.program_game_award_id
                     where sa.BUDGET_ID is not null
                     GROUP BY sa.budget_id
                 ) as saInfo
              ON saInfo.budget_id = b.id
GO