-- ==============================  NAME  ======================================
-- View: VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES    
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES] (APPLICATION_FIELD_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS APPLICATION_FIELD_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1700
GO