-- ==============================  NAME  ======================================
-- View: VW_PERMISSION_ROLE        
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411    NONE        REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
alter view [component].[VW_PERMISSION_ROLE] as

SELECT
    P.PERMISSION_ID
,    P.PERMISSION_CODE AS [PERMISSION]
,    P.PERMISSION_TYPE_CODE
,    R.ROLE_ID
,    R.ROLE_CODE AS [ROLE]
,    R.ROLE_TYPE_CODE
FROM
    COMPONENT.ROLE_PERMISSION RP
INNER JOIN COMPONENT.ROLE R
    ON  R.ROLE_ID = RP.ROLE_ID
INNER JOIN COMPONENT.PERMISSION P
    ON  P.PERMISSION_ID = RP.PERMISSION_ID
GO