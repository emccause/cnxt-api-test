-- ==============================  NAME  ======================================
-- View: VW_GROUPS_VISIBILITY     
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  =====================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160322    NONE        FOR PUBLIC VISIBILITY USE PARTICIPANT(RECG) INSTEAD OF RECOGNITION 
-- GARCIAF2        20160328    NONE        REMOVE "component." SCHEMA 
-- GARCIAF2        20160328    NONE        FOR ADMIN VISIBILITY USE: ROLE_CODE in ('ADM', 'CADM')INSTEAD OF ROLE_DESC
-- GARCIAF2        20160411    NONE        CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  ===================================================

ALTER view [component].[VW_GROUPS_VISIBILITY] as
SELECT
GROUP_ID,
CASE
WHEN GROUP_CONFIG_ID IS NOT NULL THEN GROUP_CONFIG_VIS
WHEN PUBLIC_VIS IS NULL THEN ADMIN_VIS
ELSE PUBLIC_VIS
END AS VISIBILITY
FROM (
SELECT G.GROUP_ID, PUBLIC_VIS.VISIBILTIY AS PUBLIC_VIS, ADMIN_VIS.VISIBILTIY AS ADMIN_VIS, G.GROUP_CONFIG_ID, GCV.VISIBILITY AS GROUP_CONFIG_VIS FROM 

COMPONENT.GROUPS G
LEFT JOIN
(
SELECT G.GROUP_ID, 'PUBLIC' AS VISIBILTIY FROM COMPONENT.ROLE R
INNER JOIN COMPONENT.ACL ACL
ON ACL.SUBJECT_ID = R.ROLE_ID
AND ACL.SUBJECT_TABLE = 'ROLE'
AND R.ROLE_CODE = 'RECG'  --Participant
INNER JOIN COMPONENT.ROLE VIS_ROLE
ON VIS_ROLE.ROLE_ID = ACL.ROLE_ID
AND VIS_ROLE.ROLE_CODE = 'GVIS'
INNER JOIN COMPONENT.GROUPS G
ON G.GROUP_ID = ACL.TARGET_ID
AND ACL.TARGET_TABLE = 'GROUPS'
) AS PUBLIC_VIS
ON G.GROUP_ID = PUBLIC_VIS.GROUP_ID

LEFT JOIN
(
SELECT DISTINCT G.GROUP_ID, 'ADMIN' AS VISIBILTIY FROM COMPONENT.ROLE R
INNER JOIN COMPONENT.ACL ACL
ON ACL.SUBJECT_ID = R.ROLE_ID
AND ACL.SUBJECT_TABLE = 'ROLE'
AND R.ROLE_CODE IN ('ADM', 'CADM')
INNER JOIN COMPONENT.ROLE VIS_ROLE
ON VIS_ROLE.ROLE_ID = ACL.ROLE_ID
AND VIS_ROLE.ROLE_CODE = 'GVIS'
INNER JOIN COMPONENT.GROUPS G
ON G.GROUP_ID = ACL.TARGET_ID
AND ACL.TARGET_TABLE = 'GROUPS'
) AS ADMIN_VIS
ON G.GROUP_ID = ADMIN_VIS.GROUP_ID

LEFT JOIN COMPONENT.VW_GROUP_CONFIG_VISIBILITY GCV
ON G.GROUP_CONFIG_ID = GCV.GROUP_CONFIG_ID

) AS DATA
GO