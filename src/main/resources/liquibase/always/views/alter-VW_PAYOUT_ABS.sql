/****** Object:  View [component].[VW_PAYOUT_ABS]    Script Date: 3/14/2017 10:11:11 AM ******/
if not exists (select 1 from sys.views where object_id = object_id('component.VW_PAYOUT_ABS'))
begin
    exec ('create view component.VW_PAYOUT_ABS as select 1 as c1')
end
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- VW_PAYOUT_ABS
-- ===========================  DESCRIPTION  ==================================
-- Returns payout data for creating a ABS record
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- SHARPCA     20210305    Created
-- SHARPCA	   20210416    Added HOM, BUS, PHONE EXT, TYPE and HOM & BUS ADDRESS_TYPE_CODE
-- SHARPCA     20210419    updated HOM & BUS to NIGHT & DAY for PHONE_TYPE_CODE, Removed PHONE_EXT
--
-- ===========================  DECLARATIONS  =================================

ALTER view [component].[VW_PAYOUT_ABS] as
select pay.payout_amount * pi.payout_item_amount as POINTS
      ,pay.payout_id as INDICATIVE_DATA1
      ,coalesce(pm_pid.misc_data, p.control_num) as PARTICIPANT_ID
      ,p.last_name as PARTICIPANT_LAST_NAME
      ,p.first_name as PARTICIPANT_FIRST_NAME
      ,left(p.MIDDLE_NAME,1) as PARTICIPANT_MIDDLE_INITIAL
      ,p.name_suffix as PARTICIPANT_NAME_SUFFIX
      ,p.name_prefix as PARTICIPANT_NAME_PREFIX
      ,p.language_code as LANGUAGE_ID
      ,case when ha.preferred = 'Y' then ha.address1
            when ba.preferred = 'Y' then ba.address1
            when ba.pax_id is not null then ba.address1
            else ha.address1
       end as STREET_ADDRESS1
      ,case when ha.preferred = 'Y' then ha.address2
            when ba.preferred = 'Y' then ba.address2
            when ba.pax_id is not null then ba.address2
            else ha.address2
       end as STREET_ADDRESS2
      ,case when ha.preferred = 'Y' then ha.city
            when ba.preferred = 'Y' then ba.city
            when ba.pax_id is not null then ba.city
            else ha.city
       end as CITY
      ,case when ha.preferred = 'Y' then upper(ha.state)
            when ba.preferred = 'Y' then upper(ba.state)
            when ba.pax_id is not null then upper(ba.state)
            else upper(ha.state)
       end as STATE_PROVINCE
      ,case when ha.preferred = 'Y' then ha.zip
            when ba.preferred = 'Y' then ba.zip
            when ba.pax_id is not null then ba.zip
            else ha.zip
       end as ZIP
      ,case when ha.preferred = 'Y' then ha.country_code
            when ba.preferred = 'Y' then ba.country_code
            when ba.pax_id is not null then ba.country_code
            else ha.country_code
       end as COUNTRY
	   ,case when ha.PREFERRED = 'Y' then ha.ADDRESS_TYPE_CODE
		    when ba.PREFERRED = 'Y' then ba.ADDRESS_TYPE_CODE
		    when ba.PAX_ID is not null then ba.ADDRESS_TYPE_CODE
		    else ha.ADDRESS_TYPE_CODE
	  end as ADDRESS_TYPE_CODE
      ,case when he.preferred = 'Y' then he.email_address
            when be.preferred = 'Y' then be.email_address
            when be.pax_id is not null then be.email_address
            else he.email_address
       end as EMAIL_ADDRESS
      ,p.ssn as PARTICIPANT_SSN
      ,ph_hom.phone as PARTICIPANT_NIGHT_PHONE
	  ,ph_hom.PHONE_TYPE_CODE as NIGHT_PHONE_TYPE_CODE
      ,ph_bus.phone as PARTICIPANT_DAY_PHONE
	  ,ph_bus.PHONE_TYPE_CODE as DAY_PHONE_TYPE_CODE
      ,pay.payout_desc as TRANSACTION_DESCRIPTION_OVERRIDE
      ,pa.card_num as CARD_ABS_ACCOUNT_NUMBER
      ,pay.sub_project_number as SUB_PROJECT_NUMBER
      ,pay.project_number as PROJECT_NUMBER
      ,pi.PRODUCT_CODE
      ,pi.BUSINESS_UNIT
	  ,pay.STATUS_TYPE_CODE
	  ,pay.PAX_ID
	  ,pay.PAYOUT_ID
	  ,coalesce(pi.CARD_TYPE_CODE, pay.PAYOUT_TYPE_CODE) as CARD_TYPE_CODE
	  ,pv.PAYOUT_VENDOR_NAME
	  ,pay.PAYOUT_ITEM_ID
	  ,pay.STATUS_DATE
	  ,pay.PAYOUT_DATE
	  ,pa.PAX_ACCOUNT_ID
  from component.payout pay
       inner join component.payout_item pi on pi.PAYOUT_ITEM_ID = pay.PAYOUT_ITEM_ID
       inner join component.PAYOUT_VENDOR pv
              on pv.PAYOUT_VENDOR_ID = pi.PAYOUT_VENDOR_ID
       inner join component.pax p on p.pax_id = pay.pax_id
       left join component.address ha
              on ha.pax_id = p.pax_id
             and ha.address_type_code = 'HOME'
       left join component.address ba
              on ba.pax_id = p.pax_id
             and ba.address_type_code = 'BUSINESS'
       left join component.email he
              on he.pax_id = p.pax_id
             and he.email_type_code = 'HOME'
       left join component.email be
              on be.pax_id = p.pax_id
             and be.email_type_code = 'BUSINESS'
       left join component.phone ph_hom
              on ph_hom.pax_id = p.pax_id
             and ph_hom.phone_type_code = 'HOME'
       left join component.phone ph_bus
              on ph_bus.pax_id = p.pax_id
             and ph_bus.phone_type_code = 'BUSINESS'
       left join component.pax_account pa
              on pa.pax_id = p.pax_id
             and pa.CARD_TYPE_CODE = coalesce(pi.CARD_TYPE_CODE, pay.PAYOUT_TYPE_CODE)
             and pa.ISSUED = 'Y'
             and pa.STATUS_TYPE_CODE = 'ACTIVE'
       left join component.pax_misc pm_pid
              on pm_pid.pax_id = p.pax_id
             and pm_pid.vf_name = 'ABS_PID'

GO