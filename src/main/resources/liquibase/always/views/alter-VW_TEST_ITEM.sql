/*==============================================================*/
/* View: VW_TEST_ITEM                                           */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_ITEM
-- ===========================  DESCRIPTION  ==================================
-- Returns the taken/available tests for the given AUDIENCE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20151014    NONE        Created
-- KNIGHTGA        20151029    NONE        Updated for latest UDM 4 changes
-- KNIGHTGA        20151109    NONE        Updated th status codes to HOLD and APPROVED
-- KNIGHTGA        20160512    NONE        Replaced VW_ACL_RESOLVE_ROLES with VW_PAX_ROLE (UDM5)
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_TEST_ITEM] as
SELECT
    T.ID AS TEST_ID

,    T.TEST_TYPE_CODE
,    T.NAME
,    T.DESCRIPTION
,    T.CAN_RESUME
,    T.FROM_DATE
,    T.THRU_DATE
,    DT1.NUMBER_QUESTIONS
,    TR.ID AS TEST_RESULT_ID
,    TH.ACTIVITY_DATE AS TAKEN_DATE
,    TR.COMPLETION_DATE
,    TR.CORRECT_COUNT
,    TR.CORRECT_PERCENTAGE
,    TR.SCORE
,    TH.STATUS_TYPE_CODE
,    TH.PAX_GROUP_ID
,    ACL.PAX_ID
FROM
    COMPONENT.TEST T
INNER JOIN (
    SELECT
        T.ID AS TEST_ID
    ,    COUNT(TQ.ID) AS NUMBER_QUESTIONS
    FROM
        COMPONENT.TEST T
    INNER JOIN COMPONENT.TEST_SECTION TS
        ON TS.TEST_ID = T.ID
        AND TS.STATUS_TYPE_CODE = 'ACTIVE'
    INNER JOIN COMPONENT.TEST_QUESTION TQ
        ON TQ.TEST_SECTION_ID = TS.ID
        AND TQ.STATUS_TYPE_CODE = 'ACTIVE'
    GROUP BY T.ID
    ) AS DT1
    ON DT1.TEST_ID = T.ID
INNER JOIN COMPONENT.VW_PAX_ROLE ACL
    ON ACL.TARGET_ID = T.ID
    AND ACL.TARGET_TABLE = 'TEST'
    AND ACL.ROLE = 'AUDIENCE'
LEFT JOIN (
    SELECT
        TR.TEST_ID
    ,    PG.PAX_ID
    ,    MAX(TR.ID) AS TEST_RESULT_ID
    ,    COUNT(TR.ID) AS NUMBER_ATTEMPTS
    FROM
        COMPONENT.TEST_RESULT TR
    INNER JOIN COMPONENT.TRANSACTION_HEADER TH
        ON TH.ID = TR.TRANSACTION_HEADER_ID
        AND TH.STATUS_TYPE_CODE IN ('HOLD', 'APPROVED')
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.PAX_ID = TH.PAX_GROUP_ID
    GROUP BY TR.TEST_ID, PG.PAX_ID
    ) AS DT2
    ON DT2.TEST_ID = T.ID
    AND DT2.PAX_ID = ACL.PAX_ID
LEFT JOIN COMPONENT.TEST_RESULT TR
    INNER JOIN COMPONENT.TRANSACTION_HEADER TH
        ON TH.ID = TR.TRANSACTION_HEADER_ID
        AND TH.STATUS_TYPE_CODE IN ('HOLD', 'APPROVED')
    ON TR.ID = DT2.TEST_RESULT_ID
WHERE    T.STATUS_TYPE_CODE = 'ACTIVE'
AND    ((TR.ID IS NULL 
    AND GETDATE() BETWEEN T.FROM_DATE AND ISNULL(T.THRU_DATE, GETDATE()))
OR
    (TR.ID IS NOT NULL)
)
GO