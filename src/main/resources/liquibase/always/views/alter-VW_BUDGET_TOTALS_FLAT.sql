/****** Object:  View [component].[VW_BUDGET_TOTALS_FLAT]    Script Date: 7/5/2017 1:37:31 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  =========================================================================
-- View: VW_BUDGET_TOTALS_FLAT 
-- ===========================  DESCRIPTION  =====================================================================
--
-- ============================  REVISIONS  ======================================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170705    MP-10548    Created view to determine TOTAL_AMOUNT and TOTAL_USED for a budget (not including children)
--CHONGAJP       20200526    CNXT-129     CASE REPLA
-- ===========================  DECLARATIONS  ======================================================================

ALTER VIEW [component].[VW_BUDGET_TOTALS_FLAT] AS
SELECT B.ID AS BUDGET_ID,
SUM(ISNULL(TOTAL_USED.TOTAL_USED, 0)) AS 'TOTAL_USED',
SUM(ISNULL(TOTAL_AMOUNT.TOTAL_AMOUNT, 0)) AS 'TOTAL_AMOUNT'
FROM COMPONENT.BUDGET b
    LEFT JOIN ( --TOTAL_USED
    --Calculate the total budget used
    SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
    (ISNULL(DEBITS.DEBITS,0) - ISNULL(CREDITS.CREDITS,0))  AS 'TOTAL_USED'
    FROM component.BUDGET b
    LEFT JOIN (
        --DEBITS
        SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.BUDGET b 
        LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID 
        JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE <> 'FUND' 
            AND th_debit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
        GROUP BY b.ID ) DEBITS
        ON b.ID = DEBITS.ID
    LEFT JOIN (
        --CREDITS
        SELECT b.ID, SUM(disc_credit.AMOUNT) 'CREDITS'
        FROM component.BUDGET b 
        LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE <> 'FUND'
            AND th_credit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
        GROUP BY b.ID ) CREDITS 
    ON b.ID = CREDITS.ID
) TOTAL_USED
    ON b.ID = TOTAL_USED.ID
LEFT JOIN ( --TOTAL_AMOUNT
    --Calculate the total budget amount
    SELECT b.ID, DEBITS.DEBITS, CREDITS.CREDITS,
    (ISNULL(CREDITS.CREDITS,0) - ISNULL(DEBITS.DEBITS,0))  AS 'TOTAL_AMOUNT'
    FROM component.BUDGET b
    LEFT JOIN (
        --DEBITS
        SELECT b.ID, SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.BUDGET b 
        LEFT JOIN component.DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        GROUP BY b.ID ) DEBITS
        ON b.ID = DEBITS.ID
    LEFT JOIN (
        --CREDITS
        SELECT b.ID, SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.BUDGET b 
        LEFT JOIN component.DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
    GROUP BY b.ID ) CREDITS 
    ON b.ID = CREDITS.ID 
) TOTAL_AMOUNT
    ON b.ID = TOTAL_AMOUNT.ID
GROUP BY B.ID
GO


