/*==============================================================*/
/* View: VW_TEST_RESULT_ANSWER                                  */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_ANSWER
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result answers by test question id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA        20150925    NONE        Created
-- KNIGHTGA        20151026    NONE        Added NEXT_TEST_SECTION_ID
-- KNIGHTGA        20151028    NONE        Updated for latest UDM 4 changes
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_TEST_RESULT_ANSWER] as
SELECT
    TR.TEST_ID

,    TRQ.TEST_RESULT_ID
,    TQ.TEST_SECTION_ID
,    TRQ.ID AS TEST_RESULT_QUESTION_ID
,    TQ.ID AS TEST_QUESTION_ID
,    TRA.ID AS TEST_RESULT_ANSWER_ID
,    TA.ID AS TEST_ANSWER_ID
,    TA.ANSWER_TEXT
,    TA.ANSWER_WEIGHT
,    TA.USER_PROVIDED
,    TRA.USER_RESPONSE
,    TRA.USER_SELECTED
,    TRA.DISPLAY_SEQUENCE
,    TA.NEXT_TEST_SECTION_ID
FROM    
    COMPONENT.TEST_RESULT_ANSWER TRA
INNER JOIN COMPONENT.TEST_ANSWER TA
    ON TRA.TEST_ANSWER_ID = TA.ID
INNER JOIN COMPONENT.TEST_RESULT_QUESTION TRQ
    ON TRQ.ID = TRA.TEST_RESULT_QUESTION_ID
INNER JOIN COMPONENT.TEST_QUESTION TQ
    ON TQ.ID = TRQ.TEST_QUESTION_ID
INNER JOIN COMPONENT.TEST_RESULT TR
    ON TR.ID = TRQ.TEST_RESULT_ID
GO