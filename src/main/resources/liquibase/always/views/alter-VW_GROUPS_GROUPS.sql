-- ==============================  NAME  ======================================
-- View: VW_GROUPS_GROUPS     
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20160411                REMOVE ".COMPONENT" AND CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_GROUPS_GROUPS] (GROUP_ID_1, GROUP_ID_2, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS GROUP_ID_1
,       FK2 AS GROUP_ID_2
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1000
GO