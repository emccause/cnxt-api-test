-- ==============================  NAME  ======================================
-- View: VW_USER_GROUPS_SEARCH_DATA
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result questions by test result id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KNIGHTGA     20150925                Created
-- KNIGHTGA     20151028                Updated for latest UDM 4 changes
-- GARCIAF2     20160411                CREATE "VIEW" FOLDER FOR LIQUIBASE VIEW UPDATES
-- FARFANLE     20171108    MP-11037    ADD "COUNTRY_CODE" FIELD
-- MUDDAM       20170908    MP-8001        UPDATES FOR LOCALE CHANGES (PREFERRED_LOCALE -> PAX_MISC, LANGUAGE_LOCALE -> PREFERRED_LOCALE)
--  CRISTEJ     20200226                UPDATES FOR VARCHAR(MAX) replacement
-- BURGESTL     20200407    CRE-55      Need to add email address to results to be used for look up
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_USER_GROUPS_SEARCH_DATA] as

with defaultLocale (PAX_MISC_ID,    PAX_ID, VF_NAME,    MISC_DATA,  MISC_DATE,  MISC_TYPE_CODE, PROXY_PAX_ID) as
(
select PAX_MISC_ID, PAX_ID, VF_NAME,    convert(varchar(20), MISC_DATA) MISC_DATA,  MISC_DATE,  MISC_TYPE_CODE, PROXY_PAX_ID from  component.PAX_MISC where vf_name ='DEFAULT_LOCALE'
)

, jobTitle (PAX_GROUP_MISC_ID,  PAX_GROUP_ID,   VF_NAME,    MISC_DATA,  MISC_DATE,  MISC_TYPE_CODE) as
(
    select PAX_GROUP_MISC_ID,   PAX_GROUP_ID,   VF_NAME,    MISC_DATA,  MISC_DATE,  MISC_TYPE_CODE from  component.PAX_GROUP_MISC where vf_name = 'JOB_TITLE'
)

SELECT DISTINCT
    CASE
        WHEN LAST_NAME IS NULL AND FIRST_NAME IS NULL THEN COMPANY_NAME
        ELSE FIRST_NAME + ' ' + LAST_NAME
    END AS NAME
,    'P' AS SEARCH_TYPE
,    SU.STATUS_TYPE_CODE
,    P.PAX_ID
,    P.LANGUAGE_CODE
,    DEFAULT_LOCALE_MISC.MISC_DATA AS PREFERRED_LOCALE
,    P.PREFERRED_LOCALE AS LANGUAGE_LOCALE
,    P.CONTROL_NUM
,   P.FIRST_NAME
,    P.MIDDLE_NAME
,   P.LAST_NAME
,    P.NAME_PREFIX
,    P.NAME_SUFFIX
,    P.COMPANY_NAME
,    (
    SELECT
        MAX(PF.VERSION)
    FROM component.PAX_FILES PF with (nolock)
    WHERE PF.PAX_ID = P.PAX_ID GROUP BY PF.PAX_ID
) AS VERSION
,    (
    SELECT TOP 1
        MISC_DATA
    FROM component.PAX_GROUP_MISC with (nolock)
    WHERE PAX_GROUP_ID = (
        SELECT TOP 1
            PAX_GROUP_ID
        FROM component.PAX_GROUP with (nolock)
        WHERE PAX_ID = P.PAX_ID
        ORDER BY (
            CASE
                WHEN FROM_DATE IS NULL THEN 1
                ELSE 0
            END
        ) DESC, FROM_DATE DESC
    ) AND VF_NAME = 'JOB_TITLE'
) AS JOB_TITLE
,    SU.SYS_USER_ID
,    RL.PAX_ID_2 AS 'MANAGER_PAX_ID'
,    A.COUNTRY_CODE
,    NULL AS GROUP_ID
,    NULL AS GROUP_CONFIG_ID
,    NULL AS FIELD
,    NULL AS FIELD_DISPLAY_NAME
,    NULL AS GROUP_NAME
,    NULL AS GROUP_TYPE
,    NULL AS VISIBILITY
,    NULL AS CREATE_DATE
,    NULL AS PAX_COUNT
,    NULL AS GROUP_COUNT
,    NULL AS TOTAL_PAX_COUNT
,    NULL AS GROUP_PAX_ID
,    E.EMAIL_ADDRESS
FROM component.PAX P with (nolock)
JOIN component.SYS_USER SU with (nolock)
    ON (P.PAX_ID = SU.PAX_ID)
LEFT JOIN component.PAX_FILES PF with (nolock)
    ON PF.PAX_ID = P.PAX_ID
LEFT JOIN component.RELATIONSHIP RL with (nolock)
    ON RL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    AND RL.PAX_ID_1 = P.PAX_ID
LEFT JOIN defaultLocale
    --component.PAX_MISC
    DEFAULT_LOCALE_MISC with (nolock)
    ON P.PAX_ID = DEFAULT_LOCALE_MISC.PAX_ID
    AND DEFAULT_LOCALE_MISC.VF_NAME = 'DEFAULT_LOCALE'
LEFT JOIN component.ADDRESS A with (nolock)
    ON A.PAX_ID = P.PAX_ID
    AND A.PREFERRED = 'Y'
LEFT join component.EMAIL E with (nolock)
    ON E.PAX_ID = P.PAX_ID
    AND E.PREFERRED = 'Y'
UNION
SELECT DISTINCT
    G.GROUP_DESC AS NAME
,    'G' AS SEARCH_TYPE
,    G.STATUS_TYPE_CODE
,    NULL AS PAX_ID
,    NULL AS LANGUAGE_CODE
,    NULL AS PREFERRED_LOCALE
,   NULL AS LANGUAGE_LOCALE
,    NULL AS CONTROL_NUM
,    NULL AS FIRST_NAME
,    NULL AS MIDDLE_NAME
,    NULL AS LAST_NAME
,    NULL AS NAME_PREFIX
,    NULL AS NAME_SUFFIX
,    NULL AS COMPANY_NAME
,    NULL AS VERSION
,    NULL AS JOB_TITLE
,    NULL AS SYS_USER_ID
,    NULL AS MANAGER_PAX_ID
,    NULL AS COUNTRY_CODE
,    G.GROUP_ID
,    G.GROUP_CONFIG_ID
,    GC.GROUP_CONFIG_NAME AS FIELD
,    GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME
,    G.GROUP_DESC AS GROUP_NAME
,    GT.GROUP_TYPE_DESC AS GROUP_TYPE
,    GV.VISIBILITY
,    G.CREATE_DATE
,    (
    SELECT
        COUNT(*)
    FROM component.GROUPS_PAX with (nolock)
    WHERE GROUP_ID = G.GROUP_ID
) AS PAX_COUNT
,    (
    SELECT
        COUNT(*)
    FROM component.VW_GROUPS_GROUPS with (nolock)
    WHERE GROUP_ID_1 = G.GROUP_ID
) AS GROUP_COUNT
,    (
    SELECT
        COUNT(*)
    FROM component.VW_GROUP_TOTAL_MEMBERSHIP with (nolock)
    WHERE GROUP_ID = G.GROUP_ID
) AS TOTAL_PAX_COUNT
,   G.PAX_ID AS GROUP_PAX_ID
,   NULL EMAIL_ADDRESS
FROM component.GROUPS G with (nolock)
LEFT JOIN component.GROUP_CONFIG GC with (nolock)
    ON G.GROUP_CONFIG_ID = GC.ID
LEFT JOIN component.VW_GROUPS_VISIBILITY GV with (nolock)
    ON GV.GROUP_ID = G.GROUP_ID
INNER JOIN component.GROUP_TYPE GT with (nolock)
    ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE
GO


