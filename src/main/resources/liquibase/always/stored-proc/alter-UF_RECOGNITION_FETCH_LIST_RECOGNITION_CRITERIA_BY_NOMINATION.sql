/****** Object:  UserDefinedFunction [component].[UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_BY_NOMINATION]    Script Date: 10/16/2020 12:38:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_BY_NOMINATION
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A NOMINATION_ID
-- RETURN A COMMA DELIMITED LIST OF RECOGNITION_CRITERIA_ID(S)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE				CHANGE DESCRIPTION
-- DOHOGNTA   	20080618  				CREATED
-- DOHOGNTA   	20150309  				CHANGE IDENTITIES TO BIGINT
-- burgestl	20201103				Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_BY_NOMINATION] (
  @nomination_id BIGINT)
RETURNS VARCHAR(1000)
AS
BEGIN

DECLARE @delimited_values VARCHAR(1000)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZE
SET @delimited_values = NULL

-- GENERATE COMMA-SEPERATED LIST
SELECT  @delimited_values = COALESCE(@delimited_values + ',', '') + CONVERT(VARCHAR, RECOGNITION_CRITERIA.[ID])
FROM  NOMINATION_CRITERIA
INNER JOIN RECOGNITION_CRITERIA ON NOMINATION_CRITERIA.RECOGNITION_CRITERIA_ID = RECOGNITION_CRITERIA.[ID]
WHERE NOMINATION_CRITERIA.NOMINATION_ID = @nomination_id
ORDER BY
  RECOGNITION_CRITERIA.[ID]

-- RETURN VALUE
RETURN @delimited_values

END

go