SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- ENABLE_DISABLE_CARD
-- ===========================  DESCRIPTION  ==================================
--
-- UPDATES THE STATUS OF RIDEAU CARD TYPES TO ACTIVE IN THE PAX_ACCOUNT TABLE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20161018    MP-8185        SPROC TO UPDATE RIDEAU ACCOUNT STATUS
-- HARWELLM        20161031    MP-8733        FIX KEY_NAME FOR SERVICE ANNIVERSARY PROVIDER
-- HARWELLM        20161101    MP-8742        GET GROUP MEMBERSHIP VIA ACL JOIN INSTEAD OF ROLE_GROUPS
-- HARWELLM        20170101    MP-8745        REMOVE UNNECESSARY STATUS UPDATES
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================
ALTER PROCEDURE COMPONENT.UP_ENABLE_DISABLE_CARD
AS
  BEGIN

    IF (select UPPER(value) from COMPONENT.APPLICATION_DATA  where key_name = 'projectProfile.serviceAnniversaryEnabled') = 'TRUE'
    BEGIN
        IF (select UPPER(value) from COMPONENT.APPLICATION_DATA where key_name = 'projectProfile.serviceAnniversary.provider') = 'RIDEAU'
        BEGIN
            --make all rideau accounts ACTIVE 
            UPDATE COMPONENT.PAX_ACCOUNT SET STATUS_TYPE_CODE ='ACTIVE', ISSUED='Y' WHERE PAX_ID IN(
                SELECT pax_id
                FROM  COMPONENT.PAX_ACCOUNT WHERE CARD_TYPE_CODE='RIDEAU'
            ) AND CARD_TYPE_CODE='RIDEAU'
            AND (STATUS_TYPE_CODE <> 'ACTIVE' OR ISSUED <> 'Y')
            
             --then find all groups that are assigned the DISABLE permission and make those corresponding accounts INACTIVE.
             UPDATE COMPONENT.PAX_ACCOUNT SET STATUS_TYPE_CODE ='INACTIVE', ISSUED='N' WHERE PAX_ID IN(
                SELECT DISTINCT PAX_ID
                FROM component.ROLE r
                JOIN component.ACL acl ON acl.ROLE_ID = r.ROLE_ID AND acl.SUBJECT_TABLE = 'GROUPS'
                JOIN component.GROUPS_PAX gp ON gp.GROUP_ID = acl.SUBJECT_ID
                WHERE r.ROLE_CODE = 'DISABLE_SERVICE_ANNIVERSARY'
            ) AND CARD_TYPE_CODE='RIDEAU'
            AND (STATUS_TYPE_CODE <> 'INACTIVE' OR ISSUED <> 'N')
        END
    END
    IF (select UPPER(value) from COMPONENT.APPLICATION_DATA  where key_name = 'projectProfile.serviceAnniversaryEnabled') = 'FALSE'
    BEGIN
        IF (select UPPER(value) from COMPONENT.APPLICATION_DATA where key_name = 'projectProfile.serviceAnniversary.provider') = 'RIDEAU'
        BEGIN 
                --make all rideau accounts INACTIVE
                UPDATE COMPONENT.PAX_ACCOUNT SET STATUS_TYPE_CODE ='INACTIVE', ISSUED = 'N' WHERE PAX_ID IN(
                    SELECT pax_id
                    FROM  COMPONENT.PAX_ACCOUNT WHERE CARD_TYPE_CODE='RIDEAU'
                ) AND CARD_TYPE_CODE='RIDEAU'
                AND (STATUS_TYPE_CODE <> 'INACTIVE' OR ISSUED <> 'N')
                
                -- then find all groups that are assigned the ENABLE permission and make those accounts ACTIVE.
                 UPDATE COMPONENT.PAX_ACCOUNT SET STATUS_TYPE_CODE ='ACTIVE', ISSUED='Y' WHERE PAX_ID IN(
                    SELECT DISTINCT PAX_ID
                    FROM component.ROLE r
                    JOIN component.ACL acl ON acl.ROLE_ID = r.ROLE_ID AND acl.SUBJECT_TABLE = 'GROUPS'
                    JOIN component.GROUPS_PAX gp ON gp.GROUP_ID = acl.SUBJECT_ID
                    WHERE r.ROLE_CODE = 'ENABLE_SERVICE_ANNIVERSARY'
                ) AND CARD_TYPE_CODE='RIDEAU'
                AND (STATUS_TYPE_CODE <> 'ACTIVE' OR ISSUED <> 'Y')
        END    
    END

 END

GO