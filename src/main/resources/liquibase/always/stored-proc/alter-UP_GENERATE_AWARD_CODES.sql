/****** Object:  StoredProcedure [UP_GENERATE_AWARD_CODES]    Script Date: 05/13/2016 12:34:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GENERATE_AWARD_CODES
-- ===========================  DESCRIPTION  ==================================
--
-- GIVEN A BATCH ID THAT CORRESPONDS TO AN AWARD_CODE BATCH, GENERATE REQUIRED DATA
-- GENERATES NOMINATION, RECOGNITION, RECOGNITION_MISC, TRANSACTION_HEADER, CRITERIA AND DISCRETIONARY ENTRIES
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20160513                CREATED
-- GARCIAF        20160525                ADD CRITERIA ENTRIES
-- GARCIAF        20160526                ADD COMMENT ENTRY
-- MUDDAM        20170327    MP-9805        HANDLE PAYOUT_TYPE
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
-- DDAYS          20210108   ADD Logging
-- BRIONEMC		 2021-09-29  CAM-18		  ADD MISSING TRANSACTION_HEADER_MISC ENTRIES
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GENERATE_AWARD_CODES]
@batchId BIGINT

AS
BEGIN TRY

DECLARE @loggingStartDate  DATETIME2(7),
@loggingEndDate  DATETIME2(7),
@loggingParameters NVARCHAR(MAX),
@loggingSprocName nvarchar(150),
@loggingDebug int
---------------------------------------------- CHECKING LOGGING ENABLED
SELECT  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    IF (@loggingDebug=1)
BEGIN
SET @loggingSprocName = 'UP_GENERATE_AWARD_CODES'
SET @loggingStartDate = getdate()
SET @loggingParameters = '@batchId=' + isnull(cast(@batchId as varchar),'null') --BIGINT

END

BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- CREATE NOMINATION ENTRIES
    INSERT INTO component.NOMINATION (
        BATCH_ID,
        PROGRAM_ID,
        SUBMITTER_PAX_ID,
        SUBMITTAL_DATE,
        NEWSFEED_VISIBILITY,
        BUDGET_ID,
        HEADLINE_COMMENT,
        COMMENT,
        STATUS_TYPE_CODE,
        NOMINATION_TYPE_CODE
    )
SELECT
    BATCH_DRIVER.BATCH_ID,
    PROGRAM_EVENT.MESSAGE,
    SUBMITTER_EVENT.MESSAGE,
    GETDATE(),
    CASE
        WHEN PROGRAM_VISIBILITY.MISC_DATA = 'NONE' THEN 'FALSE'
        ELSE 'TRUE'
        END,
    BUDGET_EVENT.MESSAGE,
    HEADLINE_EVENT.MESSAGE,
    COMMENT_EVENT.MESSAGE,
    'PENDING',
    'RECG'
FROM (
         SELECT
             @batchId AS BATCH_ID
         FROM component.UF_LIST_TO_TABLE(
                 (SELECT FILE_UPLOAD FROM component.BATCH_FILE WHERE BATCH_ID = @batchId),
                 ','
             )
     ) AS BATCH_DRIVER
         INNER JOIN component.BATCH_EVENT PROGRAM_EVENT
                    ON BATCH_DRIVER.BATCH_ID = PROGRAM_EVENT.BATCH_ID
                        AND PROGRAM_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_PROGRAM_ID'
         INNER JOIN component.BATCH_EVENT SUBMITTER_EVENT
                    ON BATCH_DRIVER.BATCH_ID = SUBMITTER_EVENT.BATCH_ID
                        AND SUBMITTER_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_SUBMITTER_PAX_ID'
         INNER JOIN component.BATCH_EVENT BUDGET_EVENT
                    ON BATCH_DRIVER.BATCH_ID = BUDGET_EVENT.BATCH_ID
                        AND BUDGET_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_BUDGET_ID'
         INNER JOIN component.BATCH_EVENT HEADLINE_EVENT
                    ON BATCH_DRIVER.BATCH_ID = HEADLINE_EVENT.BATCH_ID
                        AND HEADLINE_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_HEADLINE'
         INNER JOIN component.BATCH_EVENT COMMENT_EVENT
                    ON BATCH_DRIVER.BATCH_ID = COMMENT_EVENT.BATCH_ID
                        AND COMMENT_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_COMMENT'
         INNER JOIN component.PROGRAM_MISC PROGRAM_VISIBILITY
                    ON PROGRAM_EVENT.MESSAGE = PROGRAM_VISIBILITY.PROGRAM_ID
                        AND PROGRAM_VISIBILITY.VF_NAME = 'NEWSFEED_VISIBILITY'

     -- CREATE NOMINATION_MISC ENTRIES

    INSERT INTO component.NOMINATION_MISC (
    NOMINATION_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    NOM.ID,
    'PAYOUT_TYPE',
    PAYOUT_TYPE_EVENT.MESSAGE,
    GETDATE()
FROM component.NOMINATION NOM
         INNER JOIN component.BATCH_EVENT PAYOUT_TYPE_EVENT
                    ON NOM.BATCH_ID = PAYOUT_TYPE_EVENT.BATCH_ID
                        AND PAYOUT_TYPE_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_PAYOUT_TYPE'
WHERE NOM.BATCH_ID = @batchId

      -- CREATE RECOGNITION ENTRIES
    INSERT INTO component.RECOGNITION (
    NOMINATION_ID,
    PROGRAM_AWARD_TIER_ID,
    AMOUNT,
    VIEWED,
    STATUS_TYPE_CODE
)
SELECT
    NOM.ID,
    AWARD_TIER_EVENT.MESSAGE,
    AMOUNT_EVENT.MESSAGE,
    'N',
    'PENDING'
FROM component.NOMINATION NOM
         INNER JOIN component.BATCH_EVENT AWARD_TIER_EVENT
                    ON NOM.BATCH_ID = AWARD_TIER_EVENT.BATCH_ID
                        AND AWARD_TIER_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_AWARD_TIER_ID'
         INNER JOIN component.BATCH_EVENT AMOUNT_EVENT
                    ON NOM.BATCH_ID = AMOUNT_EVENT.BATCH_ID
                        AND AMOUNT_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_AMOUNT'
WHERE NOM.BATCH_ID = @batchId

      -- CREATE CERT_ID RECOGNITION MISC ENTRIES
    INSERT INTO component.RECOGNITION_MISC (
    RECOGNITION_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    REC.ID,
    'CERT_ID',
    CERT_EVENT.MESSAGE,
    GETDATE()
FROM component.RECOGNITION REC
         INNER JOIN component.NOMINATION NOM
                    ON REC.NOMINATION_ID = NOM.ID
         INNER JOIN component.BATCH_EVENT CERT_EVENT
                    ON NOM.BATCH_ID = CERT_EVENT.BATCH_ID
                        AND CERT_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_CERT_ID'
WHERE NOM.BATCH_ID = @batchId

      -- CREATE GIVE_ANONYMOUS RECOGNITION MISC ENTRIES
    INSERT INTO component.RECOGNITION_MISC (
    RECOGNITION_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    REC.ID,
    'GIVE_ANONYMOUS',
    GIVE_ANONYMOUS_EVENT.MESSAGE,
    GETDATE()
FROM component.RECOGNITION REC
         INNER JOIN component.NOMINATION NOM
                    ON REC.NOMINATION_ID = NOM.ID
         INNER JOIN component.BATCH_EVENT GIVE_ANONYMOUS_EVENT
                    ON NOM.BATCH_ID = GIVE_ANONYMOUS_EVENT.BATCH_ID
                        AND GIVE_ANONYMOUS_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_GIVE_ANONYMOUS'
WHERE NOM.BATCH_ID = @batchId

      -- CREATE AWARD_CODE RECOGNITION MISC ENTRIES
    INSERT INTO component.RECOGNITION_MISC (
    RECOGNITION_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    REC_DATA.ID,
    'AWARD_CODE',
    AWARD_CODE_DATA.AWARD_CODE,
    GETDATE()
FROM (
         SELECT
             REC.ID,
             ROW_NUMBER() OVER (ORDER BY REC.ID) AS NUMBER
         FROM component.RECOGNITION REC
                  INNER JOIN component.NOMINATION NOM
                             ON REC.NOMINATION_ID = NOM.ID
         WHERE NOM.BATCH_ID = @batchId
     ) REC_DATA
         INNER JOIN (
    SELECT
        ITEMS AS AWARD_CODE,
        ROW_NUMBER() OVER (ORDER BY ITEMS) AS NUMBER
    FROM component.UF_LIST_TO_TABLE(
            (SELECT FILE_UPLOAD FROM component.BATCH_FILE WHERE BATCH_ID = @batchId),
            ','
        )
) AWARD_CODE_DATA
                    ON REC_DATA.NUMBER = AWARD_CODE_DATA.NUMBER

     -- CREATE TRANSACTION_HEADER ENTRIES
    INSERT INTO component.TRANSACTION_HEADER (
    BATCH_ID,
    TYPE_CODE,
    SUB_TYPE_CODE,
    ACTIVITY_DATE,
    STATUS_TYPE_CODE,
    PERFORMANCE_VALUE,
    PROGRAM_ID
)
SELECT
    NOM.BATCH_ID,
    'DISC',
    'RECG',
    GETDATE(),
    'PENDING',
    REC.AMOUNT,
    NOM.PROGRAM_ID
FROM component.NOMINATION NOM
         INNER JOIN component.RECOGNITION REC
                    ON NOM.ID = REC.NOMINATION_ID
WHERE NOM.BATCH_ID = @batchId

      -- CREATE TRANSACTION_HEADER_MISC ENTRIES

    INSERT INTO component.TRANSACTION_HEADER_MISC (
    TRANSACTION_HEADER_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    TH.ID,
    'PAYOUT_TYPE',
    PAYOUT_TYPE_EVENT.MESSAGE,
    GETDATE()
FROM component.TRANSACTION_HEADER TH
         INNER JOIN component.BATCH_EVENT PAYOUT_TYPE_EVENT
                    ON TH.BATCH_ID = PAYOUT_TYPE_EVENT.BATCH_ID
                        AND PAYOUT_TYPE_EVENT.BATCH_EVENT_TYPE_CODE = 'AWARD_CODE_PAYOUT_TYPE'
WHERE TH.BATCH_ID = @batchId

      -- CREATE DISCRETIONARY ENTRIES
    INSERT INTO component.DISCRETIONARY (
    TRANSACTION_HEADER_ID,
    AMOUNT,
    PERFORMANCE_DATE,
    DESCRIPTION,
    BUDGET_ID_DEBIT,
    TARGET_TABLE,
    TARGET_ID
)
SELECT
    TH_DATA.ID,
    TH_DATA.PERFORMANCE_VALUE,
    GETDATE(),
    'RECOGNITION ISSUANCE',
    REC_DATA.BUDGET_ID,
    'RECOGNITION',
    REC_DATA.ID
FROM (
         SELECT
             ID,
             PERFORMANCE_VALUE,
             ROW_NUMBER() OVER (ORDER BY ID) AS NUMBER
         FROM component.TRANSACTION_HEADER
         WHERE BATCH_ID = @batchId
     ) AS TH_DATA
         INNER JOIN (
    SELECT
        REC.ID,
        NOM.BUDGET_ID,
        ROW_NUMBER() OVER (ORDER BY REC.ID) AS NUMBER
    FROM component.RECOGNITION REC
             INNER JOIN component.NOMINATION NOM
                        ON REC.NOMINATION_ID = NOM.ID
    WHERE NOM.BATCH_ID = @batchId
) AS REC_DATA
                    ON TH_DATA.NUMBER = REC_DATA.NUMBER
                    
--DEFAULT PROJECT AND SUB_PROJECT IN TRANSACTION_HEADER_MISC              
   --DEFAULT_PROJECT_NUMBER
 INSERT INTO component.TRANSACTION_HEADER_MISC (
    TRANSACTION_HEADER_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    th.ID,
    'DEFAULT_PROJECT_NUMBER',
    p.PROJECT_NUMBER,
    GETDATE()
FROM component.TRANSACTION_HEADER th	
	INNER JOIN component.DISCRETIONARY d ON d.TRANSACTION_HEADER_ID = th.ID
	INNER JOIN component.BUDGET b ON b.ID = d.BUDGET_ID_DEBIT
	INNER JOIN component.SUB_PROJECT sp ON sp.ID = b.SUB_PROJECT_ID
	INNER JOIN component.PROJECT p ON p.ID = sp.PROJECT_ID
	WHERE th.BATCH_ID = @batchId

--DEFAULT_SUB_PROJECT_NUMBER
 INSERT INTO component.TRANSACTION_HEADER_MISC (
    TRANSACTION_HEADER_ID,
    VF_NAME,
    MISC_DATA,
    MISC_DATE
)
SELECT
    th.ID,
    'DEFAULT_SUB_PROJECT_NUMBER',
    sp.SUB_PROJECT_NUMBER,
    GETDATE()
FROM component.TRANSACTION_HEADER th	
	INNER JOIN component.DISCRETIONARY d ON d.TRANSACTION_HEADER_ID = th.ID
	INNER JOIN component.BUDGET b ON b.ID = d.BUDGET_ID_DEBIT
	INNER JOIN component.SUB_PROJECT sp ON sp.ID = b.SUB_PROJECT_ID
	WHERE th.BATCH_ID = @batchId

     --CREATE RECOGNITION CRITERIA ENTRIES
    INSERT INTO component.NOMINATION_CRITERIA (
    NOMINATION_ID,
    RECOGNITION_CRITERIA_ID
)
SELECT NOM.ID, items FROM [component].[UF_LIST_TO_TABLE] (
    (SELECT MESSAGE  FROM [component].[BATCH_EVENT]
    WHERE BATCH_ID = @batchId
    AND BATCH_EVENT_TYPE_CODE='AWARD_CODE_CRITERIA_IDS'),',') AS REC_CRITERIA
    INNER JOIN NOMINATION NOM ON BATCH_ID = @batchId

END
--(+)Logging
if (@loggingDebug=1)
begin
set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error
	 --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging
GO