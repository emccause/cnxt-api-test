/****** Object:  StoredProcedure [UP_BUDGET_GIVING_LIMIT_REPORT]    Script Date: 06/09/2016 12:34:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_BUDGET_GIVING_LIMIT_REPORT
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS BUDGET GIVING LIMIT REPORT CONTENT, FILTERED BY STATUS AND BUDGET ID
-- PARAMS:
-- @delimBudgetIds: Comma delimited budgetId list
-- @delimStatus: ACTIVE, ENDED, SHCEDULED, INACTIVE, ARCHIVED
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MADRIDO        20160609                CREATED
-- MUDDAM        20170106    MP-8056        UPDATED REFERENCES TO PROGRAM_BUDGET_ELIGIBILITY TO USE ACL INSTEAD
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- MUDDAM        20170928    MP-8001        Moved BUDGET_NAME to BUDGET_CODE, BUDGET_DISPLAY_NAME to BUDGET_NAME
-- KUMARSJ        20171120    MP-11332    USED the latest BUDGET Views
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_GIVING_LIMIT_REPORT]
    @delimBudgetIds VARCHAR(MAX), 
    @delimStatus VARCHAR(MAX) 

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    -- Set default status
    IF @delimStatus IS NULL BEGIN
        SET @delimStatus = 'ACTIVE,ENDED,SCHEDULED,INACTIVE,ARCHIVED'
    END 
    

    DECLARE    @delimiter CHAR(1) = ','

    -- Parse delimited input lists of budgetIds into table #input_budget_id
    SELECT token AS BUDGET_ID 
    INTO #input_budget_id
    FROM UF_PARSE_STRING_TO_INTEGER (@delimBudgetIds, @delimiter)

    -- Parse delimited input lists of status into table #input_status
    SELECT UPPER(token) AS STATUS_TYPE
    INTO #input_status
    FROM UF_PARSE_STRING_TO_STRING (@delimStatus, @delimiter)

    
    -- Filtering budgets by status and id.
    SELECT ID AS BUDGET_ID, BUDGET_TYPE_CODE 
    INTO #budgetsFiltered
    FROM 
        BUDGET B
    INNER JOIN 
        #input_status ST
        ON 
        B.STATUS_TYPE_CODE = 'ACTIVE'
        AND ( 
            ST.STATUS_TYPE = 'ACTIVE'
                AND ( 
                    (GETDATE() BETWEEN B.FROM_DATE AND B.THRU_DATE)
                    OR (GETDATE() > B.FROM_DATE AND B.THRU_DATE IS NULL)
                )
            OR ST.STATUS_TYPE = 'SCHEDULED' AND GETDATE() < B.FROM_DATE
            OR ST.STATUS_TYPE = 'ENDED' AND GETDATE() > B.THRU_DATE
        )
        OR (B.STATUS_TYPE_CODE = 'INACTIVE' AND ST.STATUS_TYPE = 'INACTIVE')
        OR (B.STATUS_TYPE_CODE = 'ARCHIVED' AND ST.STATUS_TYPE = 'ARCHIVED')
    WHERE 
        B.INDIVIDUAL_GIVING_LIMIT IS NOT NULL
        AND (
            EXISTS (
                SELECT BUDGET_ID FROM #input_budget_id FB WHERE B.ID = FB.BUDGET_ID OR B.PARENT_BUDGET_ID = FB.BUDGET_ID 
            )
            OR @delimBudgetIds IS NULL)
    
    -- Who is available to use SIMPLE budgets --
    SELECT 
        DISTINCT B.BUDGET_ID, ACL.SUBJECT_TABLE, ACL.SUBJECT_ID 
        INTO #budgetConfig
    FROM #budgetsFiltered B
    INNER JOIN 
        PROGRAM_BUDGET PB 
        ON PB.BUDGET_ID = B.BUDGET_ID
    INNER JOIN ACL ACL
        ON PB.ID = ACL.TARGET_ID
        AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
    INNER JOIN ROLE ROLE
        ON ACL.ROLE_ID = ROLE.ROLE_ID
        AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
    WHERE BUDGET_TYPE_CODE = 'SIMPLE'
        
    -- Who is eligible to give from a DISTRIBUTED budget --
    INSERT INTO #budgetConfig
    SELECT DISTINCT B.BUDGET_ID, 'GROUPS', A.FK2 
    FROM #budgetsFiltered B
    INNER JOIN 
        ASSOCIATION A
        ON ASSOCIATION_TYPE_ID = 3600
            AND A.FK1 = B.BUDGET_ID
    WHERE BUDGET_TYPE_CODE = 'DISTRIBUTED'
        

    -- Get participants amount used by budget #distinctPaxBudget
    SELECT 
        BDG_PAX.BUDGET_ID
        , BDG_PAX.PAX_ID
        , SUM(ISNULL(AMNT_USED.AMOUNT,0)) AS AMOUNT_USED
    INTO 
        #distinctPaxBudget
    FROM ( -- Distinct participants by budget
        SELECT DISTINCT 
            BCFG.BUDGET_ID, ISNULL(GP.PAX_ID,BCFG.SUBJECT_ID) AS PAX_ID    
        FROM 
            #budgetConfig BCFG
        LEFT JOIN 
            GROUPS_PAX GP 
            ON BCFG.SUBJECT_TABLE = 'GROUPS' 
            AND BCFG.SUBJECT_ID = GP.GROUP_ID
    ) AS BDG_PAX
    LEFT JOIN ( -- Get amount used by participant on each budget
        SELECT 
            DISC.BUDGET_ID_DEBIT, NOM.SUBMITTER_PAX_ID AS PAX_ID, DISC.AMOUNT
        FROM DISCRETIONARY DISC
        INNER JOIN RECOGNITION REC
            ON REC.ID = DISC.TARGET_ID
            AND DISC.TARGET_TABLE = 'RECOGNITION'
        INNER JOIN NOMINATION NOM
            ON NOM.ID = REC.NOMINATION_ID
        INNER JOIN TRANSACTION_HEADER TH
            ON TH.ID = DISC.TRANSACTION_HEADER_ID 
            AND TH.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
        ) AS AMNT_USED 
        ON AMNT_USED.BUDGET_ID_DEBIT = BDG_PAX.BUDGET_ID
        AND AMNT_USED.PAX_ID = BDG_PAX.PAX_ID
    GROUP BY BDG_PAX.BUDGET_ID, BDG_PAX.PAX_ID


    -- Get distinct participant details #participantDetails
    SELECT DISTINCT
        P.PAX_ID
        , P.CONTROL_NUM 
        , P.FIRST_NAME 
        , P.LAST_NAME
        , SU.STATUS_TYPE_CODE
    INTO 
        #participantDetails
    FROM #distinctPaxBudget DPB
    INNER JOIN PAX P 
        ON DPB.PAX_ID = P.PAX_ID
    LEFT JOIN SYS_USER SU 
        ON P.PAX_ID = SU.PAX_ID

 
    -- Budget giving limit report content
    SELECT DISTINCT
        B.ID AS BUDGET_ID
        , B.BUDGET_NAME
        , B.DISPLAY_NAME AS BUDGET_DISPLAY_NAME
        , ISNULL(B.INDIVIDUAL_GIVING_LIMIT,0) AS BUDGET_GIVING_LIMIT
        , B.FROM_DATE AS START_DATE
        , B.THRU_DATE AS END_DATE
        , ISNULL(BTF.TOTAL_USED,0) AS BUDGET_USED
        , ISNULL(BTF.TOTAL_AMOUNT,0) - ISNULL(BTF.TOTAL_USED,0) AS BUDGET_REMAINING 
        , PD.CONTROL_NUM
        , PD.FIRST_NAME
        , PD.LAST_NAME
        , PD.STATUS_TYPE_CODE AS STATUS
        , ISNULL(PB.AMOUNT_USED,0) AS PARTICIPANT_AMOUNT_USED
    FROM 
        VW_BUDGET_INFO_LIGHTWEIGHT B
    INNER JOIN component.VW_BUDGET_TOTALS_FLAT BTF
        on B.ID = BTF.BUDGET_ID
    INNER JOIN 
        #budgetsFiltered FB ON FB.BUDGET_ID = B.ID
    LEFT JOIN 
        #distinctPaxBudget PB ON B.ID = PB.BUDGET_ID
    LEFT JOIN
        #participantDetails PD ON PB.PAX_ID = PD.PAX_ID
    ORDER BY B.ID, PD.CONTROL_NUM
    
    DROP TABLE #participantDetails
    DROP TABLE #distinctPaxBudget
    DROP TABLE #budgetsFiltered
    DROP TABLE #budgetConfig
    DROP TABLE #input_status
    DROP TABLE #input_budget_id

END
GO