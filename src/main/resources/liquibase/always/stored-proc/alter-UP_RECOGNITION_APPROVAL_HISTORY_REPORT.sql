/****** Object:  StoredProcedure [component].[UP_RECOGNITION_APPROVAL_HISTORY_REPORT]    Script Date: 10/15/2015 11:00:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_APPROVAL_HISTORY_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS APPROVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- THOMSOND        20080709                CREATED
-- THOMSOND        20080725                CHANGED @approver_pax_id to @approver_pax_group_id
-- BESSBS        20080825                Fixed issues associated with last_approval_date and next_approver
-- THOMSOND        20080923                Rewrite
-- THOMSOND        20080924                Modified Next Approver code
-- THOMSOND        20090929                Added filter to award_amount summation
-- THOMSOND        20090929                Added several columns for total points for each status type of A, P and R
-- THOMSOND        20090930                Added a filter to not include approval_history with status of P
-- THOMSOND        20090930                Reworked next_approver_name logic.
-- PALD00        20081023                Removed the multiple recipient flag check to fix DEFECT 783
-- SCHIERVR        20081030                Moved base code to BASE version for CSV report use.
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- DOHOGNTA     20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151113                UDM4 CHANGES
-- WRIGHTM        20151117                CHANGED SATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_APPROVAL_HISTORY_REPORT]
    @approver_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,   @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON


-- DRIVER
CREATE TABLE #approvals (
    submittal_date datetime2 NULL

,    receiver_list VARCHAR(1000) NULL
,    multiple_recipient_flag INT NULL DEFAULT 0
,    receiver_name VARCHAR(80) NULL

,    approval_process_id BIGINT NULL
,    denial_reason NVARCHAR(MAX)
,    approval_count INT NULL
,    approval_date_count INT NULL
,    approval_date datetime2 NULL

,    submitter_list VARCHAR(1000) NULL
,    multiple_submitter_flag INT NULL DEFAULT 0
,    submitter_name VARCHAR(80) NULL

,    next_approver_name VARCHAR(80) NULL
,    next_approver_count INT NULL

,    ecard_id BIGINT NULL
,    recognition_id    BIGINT NULL
,    justification_enabled_flag INT NULL DEFAULT 0
,    justification_comment_available_flag INT NULL DEFAULT 0

,    nomination_status NVARCHAR(10) NULL

,    criteria NVARCHAR(1024) NULL

,    total_points INT NULL
,    total_pending_points INT NULL
,    total_denied_points INT NULL
,    total_approved_points INT NULL

,    total_recognition_amount INT NULL
,    nomination_id BIGINT NOT NULL

,    program_id BIGINT NULL
,    program_name NVARCHAR(40) NULL
,    award_type NVARCHAR(40) NULL
,    budget_id BIGINT NULL
)
CREATE TABLE #award_types (
    nomination_id BIGINT NULL
,    award_type NVARCHAR(40) NULL    
)
-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_APPROVAL_HISTORY_BASE @approver_pax_group_id, @submittal_date_from, @submittal_date_thru, @locale_code

UPDATE #approvals
SET    nomination_status =
    CASE
        WHEN nomination_status = 'PENDING' THEN 'Pending'
        WHEN nomination_status = 'APPROVED' THEN 'Approved'
        WHEN nomination_status = 'REJECTED' THEN 'Denied'
        WHEN nomination_status = 'COMPLETE' THEN 'Complete'
    END

-- FORMAT THE OUTPUT
SELECT
    submittal_date
,    receiver_list
,    receiver_name
,    ecard_id
,    component.UF_TRANSLATE (@locale_code,nomination_status, 'Reporting', 'Reporting') AS nomination_status
,    next_approver_name
,    next_approver_count
,    approval_date
,    approval_date_count
,    submitter_list
,    submitter_name
,    component.UF_TRANSLATE (@locale_code, program_name, 'PROGRAM', 'PROGRAM_NAME') AS program_name
,    total_points 
,    CASE
        WHEN award_type = 'Points' 
        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
        ELSE component.UF_TRANSLATE (@locale_code, award_type, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC') 
    END as award_type
,    total_pending_points
,    total_denied_points
,    total_approved_points
,    criteria
,    recognition_id
,    nomination_id
,    denial_reason
,    justification_enabled_flag
,    justification_comment_available_flag
,    multiple_recipient_flag
,    multiple_submitter_flag
,    budget_id
FROM    #approvals
ORDER BY
    submittal_date DESC

END

GO

