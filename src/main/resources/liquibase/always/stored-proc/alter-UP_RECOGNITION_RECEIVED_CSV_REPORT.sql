/****** Object:  StoredProcedure [component].[UP_RECOGNITION_RECEIVED_CSV_REPORT]    Script Date: 10/20/2020 3:08:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_RECEIVED_CSV_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS RECEIVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR      	DATE        STORY     	CHANGE DESCRIPTION
-- THOMSOND     20080819                CREATED
-- THOMSOND     20080829                CHANGE - Added is_recognition_comment
-- SCHIERVE     20081104                CHANGE - Separated into BASE/REPORT/CSV
-- BESSBS       20091118                CHANGE - DATATYPE OF recognition_date FROM VARCHAR(10) TO DATETIME
-- KOTHAH       20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS      20121212                CHANGE - Added locale code
-- DOHOGNTA     20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM      	20151113                UDM4 CHANGES
-- burgestl	20201021		Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_RECEIVED_CSV_REPORT]
    @pax_group_id BIGINT
,    @submittal_from_date datetime2
,    @submittal_thru_date datetime2
,    @locale_code varchar(6)
AS
BEGIN

SET NOCOUNT ON

CREATE TABLE #recognition (
    nomination_id BIGINT NOT NULL

,    submitter_list VARCHAR(1000) NULL
,    submitted_by_name VARCHAR(80) NULL

,    ecard_id BIGINT NULL

,    recognition_id    BIGINT NULL
,    recognition_receiver_pax_id BIGINT NULL
,    recognition_amount INT NULL
,    is_recognition_comment INT NULL
,    recognition_status NVARCHAR(10) NULL
,    recognition_date datetime2 NULL
,    recognition_criteria NVARCHAR(1024) NULL

,    approval_process_id BIGINT NULL

,    program_id BIGINT NULL
,    program_name NVARCHAR(40) NULL
,    award_type NVARCHAR(40) NULL
,    share VARCHAR (10) NULL
,    budget_id BIGINT NULL
)

CREATE TABLE #award_types (
    nomination_id BIGINT NULL
,    award_type NVARCHAR(40) NULL    
)


EXECUTE component.UP_RECOGNITION_RECEIVED_BASE @pax_group_id, @submittal_from_date, @submittal_thru_date,@locale_code

SELECT 
    #recognition.RECOGNITION_DATE
,    CASE
        WHEN #recognition.SUBMITTER_LIST LIKE '%,%' THEN 'Multiple Submitters'
        ELSE #recognition.SUBMITTED_BY_NAME
    END AS SUBMITTER
,    CASE
        WHEN #recognition.ECARD_ID IS NOT NULL THEN 'Ecard'
        WHEN #recognition.IS_RECOGNITION_COMMENT = 1 THEN 'Comment'
        ELSE NULL
    END AS COMMENTS
,    component.UF_TRANSLATE (@locale_code,#recognition.PROGRAM_NAME,'PROGRAM','PROGRAM_NAME') AS RECOGNITION_PROGRAM
,    #recognition.RECOGNITION_CRITERIA
,    CASE
        WHEN #recognition.RECOGNITION_AMOUNT >0 THEN #recognition.RECOGNITION_AMOUNT
        ELSE  null
    END  AS AWARD_AMOUNT
,    CASE
        WHEN award_type='Points' 
        THEN component.UF_TRANSLATE (@locale_code,'Points','REPORTING','REPORTING')
        ELSE    component.UF_TRANSLATE (@locale_code,award_type,'PAYOUT_ITEM','PAYOUT_ITEM_DESC') 
    END AS AWARD_TYPE
,    #recognition.NOMINATION_ID AS TRACKING_NUMBER
,    #recognition.ECARD_ID
,    #recognition.RECOGNITION_ID
,    #recognition.RECOGNITION_RECEIVER_PAX_ID
,    NOMINATION.COMMENT as RECOGNITION_COMMENT
FROM    #recognition
INNER JOIN NOMINATION ON #recognition.NOMINATION_ID = NOMINATION.ID
ORDER BY
    #recognition.RECOGNITION_DATE DESC

SELECT    ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ') AS EMPLOYEE
FROM PAX
INNER JOIN PAX_GROUP
ON PAX.PAX_ID = PAX_GROUP.PAX_ID
WHERE PAX_GROUP.PAX_GROUP_ID = @pax_group_id

END

GO
