GO

/****** Object:  StoredProcedure [component].[UP_GET_ELIGIBLE_BUDGETS_FOR_PAX]    Script Date: 3/25/2020 11:05:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_ELIGIBLE_BUDGETS_FOR_PAX
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS A LIST OF BUDGET ID'S THAT THE SPECIFIED PAX LIST CAN USE.
-- THIS MEANS THE BUDGET IS TIED TO A PROGRAM, AND HAS IS_USABLE = TRUE.
--
-- Params:
-- @delimPaxIdList = comma separated list of pax IDs to find budgets for
-- @givingLimit = if 0, returns all budgets. 
-- if 1, will only return ALLOCATED children budgets and budgets with giving limits
-- @includeEndedBudgets = if 0, only ACTIVE budgets will be returned.
-- if 1, ENDED budgets will be returned as well as ACTIVE ones.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170627    MP-10404    CREATED
-- MUDDAM        20171030    MP-8001        UDM7 UPDATES
-- SUDIPC       20180117    MP-11533    MANAGER-BUDGET ISSUE
-- FREINERT     20180219    MP-11716    FIXING TYPE CONVERSION ISSUE
-- burgestl		20200103	  CRE			Runs to long
-- DDAYS        20210110   ADD Logging removed commented out code see previous revisions for that code.
-- ===========================  DECLARATIONS  =================================

--select * from component.pax where first_name = '--Alan' and last_name = 'May'

--exec [component].[UP_GET_ELIGIBLE_BUDGETS_FOR_PAX] 38311, true, true
--ID	PAX_COUNT	BUDGET_TYPE_CODE	INDIVIDUAL_GIVING_LIMIT	STATUS_TYPE_CODE
--12	1			SIMPLE				120000.00				ACTIVE
--5143	1			ALLOCATED			NULL					ACTIVE

--declare --
ALTER PROCEDURE [component].[UP_GET_ELIGIBLE_BUDGETS_FOR_PAX]
@delimPaxIdList VARCHAR(MAX),
@givingLimit BIT,
@includeEndedBudgets BIT

AS
SET NOCOUNT ON
BEGIN

--(+)Logging
BEGIN TRY
DECLARE @loggingStartDate  DATETIME2(7),
		@loggingEndDate  DATETIME2(7),
		@loggingParameters NVARCHAR(MAX),
		@loggingSprocName nvarchar(150),
		@loggingDebug int
---------------------------------------------- CHECKING LOGGING ENABLED
SELECT  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    IF (@loggingDebug=1)
BEGIN
		SET @loggingSprocName = 'UP_GET_ELIGIBLE_BUDGETS_FOR_PAX'
		SET @loggingStartDate = getdate()
		SET @loggingParameters = '@delimPaxIdList=' + isnull(cast(@delimPaxIdList as varchar),'null') --BIGINT
							+ ' :: @givingLimit=' +isnull(cast(@givingLimit as nvarchar), 'null') -- NVARCHAR(MAX)
							+ ' :: @includeEndedBudgets=' + isnull(cast(@includeEndedBudgets as varchar),'null') --BIGINT
END

DECLARE @delimiter CHAR(1) = ',';

--Separate or
if object_id('tempdb..#temp5') is not null	drop table #temp5
Create table #temp5 (BUDGET_ID int, PAX_ID int)

    insert #temp5 (BUDGET_ID, PAX_ID)
SELECT DISTINCT PB.BUDGET_ID,
                CASE
                    WHEN ACL.SUBJECT_TABLE = 'PAX' THEN ACL.SUBJECT_ID
                    WHEN ACL.SUBJECT_TABLE = 'GROUPS' THEN GTM.PAX_ID
                    END AS 'PAX_ID'
FROM component.PROGRAM P
         JOIN component.PROGRAM_BUDGET PB ON P.PROGRAM_ID = PB.PROGRAM_ID
         JOIN component.ACL ACL
              ON PB.ID = ACL.TARGET_ID
                  AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
         JOIN component.ROLE ROLE
              ON ACL.ROLE_ID = ROLE.ROLE_ID
                  AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
         LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM
                   ON ACL.SUBJECT_ID = GTM.GROUP_ID
                       AND ACL.SUBJECT_TABLE = 'GROUPS'
WHERE	(
                     ACL.SUBJECT_TABLE = 'PAX'
                 AND EXISTS	(
                             SELECT items
                             FROM component.UF_LIST_TO_TABLE_ID(@delimPaxIdList, @delimiter)
                             WHERE items = ACL.SUBJECT_ID
                         )
             )
union

SELECT DISTINCT PB.BUDGET_ID,
                CASE
                    WHEN ACL.SUBJECT_TABLE = 'PAX' THEN ACL.SUBJECT_ID
                    WHEN ACL.SUBJECT_TABLE = 'GROUPS' THEN GTM.PAX_ID
                    END AS 'PAX_ID'
FROM component.PROGRAM P
         JOIN component.PROGRAM_BUDGET PB ON P.PROGRAM_ID = PB.PROGRAM_ID
         JOIN component.ACL ACL
              ON PB.ID = ACL.TARGET_ID
                  AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
         JOIN component.ROLE ROLE
              ON ACL.ROLE_ID = ROLE.ROLE_ID
                  AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
         LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM
                   ON ACL.SUBJECT_ID = GTM.GROUP_ID
                       AND ACL.SUBJECT_TABLE = 'GROUPS'
WHERE	 EXISTS(
                  SELECT items
                  FROM component.UF_LIST_TO_TABLE_ID(@delimPaxIdList, @delimiter)
                  WHERE items = GTM.PAX_ID
              )

    if object_id('tempdb..#temp3') is not null	drop table #temp3
SELECT convert(int, MISC_DATA ) MISC_DATA
into #temp3
FROM component.BUDGET_MISC
WHERE VF_NAME = 'TOP_LEVEL_BUDGET_ID' AND BUDGET_ID IN (SELECT DISTINCT TARGET_ID FROM component.ACL WHERE TARGET_TABLE = 'BUDGET' AND SUBJECT_TABLE = 'PAX')

    if object_id('tempdb..#temp4') is not null	drop table #temp4
Create table #temp4 (TARGET_ID int, PAX_ID int)

    Insert #temp4 (TARGET_ID, PAX_ID)
SELECT DISTINCT BD_USER.TARGET_ID,
                CASE
                    WHEN BD_USER.SUBJECT_TABLE = 'PAX' THEN BD_USER.SUBJECT_ID
                    WHEN BD_USER.SUBJECT_TABLE = 'GROUPS' THEN GTM.PAX_ID
                    END AS 'PAX_ID'
FROM component.ACL BD_USER
         JOIN component.ROLE R_BD_USER ON R_BD_USER.ROLE_ID = BD_USER.ROLE_ID AND R_BD_USER.ROLE_CODE = 'BUDGET_USER'
         JOIN component.BUDGET B ON B.PARENT_BUDGET_ID IS NOT NULL
         LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM ON GTM.group_id = BD_USER.SUBJECT_ID AND BD_USER.SUBJECT_TABLE = 'GROUPS'
         LEFT JOIN component.UF_LIST_TO_TABLE_ID(@delimPaxIdList, @delimiter) pax on gtm.pax_id = pax.items or BD_USER.SUBJECT_ID = pax.items
WHERE pax.items is not null
  AND	(
    (B.BUDGET_TYPE_CODE = 'ALLOCATED'
        AND BD_USER.TARGET_TABLE = 'BUDGET'
        AND BD_USER.SUBJECT_TABLE = 'PAX'
        AND EXISTS (
             SELECT pb.ID
             FROM component.PROGRAM_BUDGET pb
                      INNER JOIN
                  (
                      --Gather above convert component.BUDGET_MISC MISC_DATA to int in #temp3
                      select * from #temp3
                  ) AS bm ON pb.BUDGET_ID = bm.MISC_DATA
         )
        )
    )
union
SELECT DISTINCT BD_USER.TARGET_ID,
                CASE
                    WHEN BD_USER.SUBJECT_TABLE = 'PAX' THEN BD_USER.SUBJECT_ID
                    WHEN BD_USER.SUBJECT_TABLE = 'GROUPS' THEN GTM.PAX_ID
                    END AS 'PAX_ID'
	--into #temp2
FROM component.ACL BD_USER
         JOIN component.ROLE R_BD_USER ON R_BD_USER.ROLE_ID = BD_USER.ROLE_ID AND R_BD_USER.ROLE_CODE = 'BUDGET_USER'
         JOIN component.BUDGET B ON B.PARENT_BUDGET_ID IS NOT NULL
         LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM ON GTM.group_id = BD_USER.SUBJECT_ID AND BD_USER.SUBJECT_TABLE = 'GROUPS'
         LEFT JOIN component.UF_LIST_TO_TABLE_ID(@delimPaxIdList, @delimiter) pax on gtm.pax_id = pax.items or BD_USER.SUBJECT_ID = pax.items
WHERE pax.items is not null
  AND	(
    (B.BUDGET_TYPE_CODE = 'DISTRIBUTED'
        AND BD_USER.TARGET_TABLE = 'BUDGET'
        AND BD_USER.SUBJECT_TABLE = 'GROUPS'
        AND EXISTS (
             SELECT pb.ID
             FROM component.PROGRAM_BUDGET pb
             WHERE pb.BUDGET_ID = B.PARENT_BUDGET_ID
         )
        )
    )

SELECT B.ID, COUNT(ELIGIBLE_BUDGETS.PAX_ID) AS 'PAX_COUNT', B.BUDGET_TYPE_CODE, B.INDIVIDUAL_GIVING_LIMIT,
       CASE
           WHEN B.FROM_DATE > GETDATE() THEN 'SCHEDULED'
           WHEN B.THRU_DATE < GETDATE() THEN 'ENDED'
           ELSE B.STATUS_TYPE_CODE
           END AS 'STATUS_TYPE_CODE'
FROM component.BUDGET B
         JOIN (
    --This will return SIMPLE and top-level DISTRIBUTED and ALLOCATED
    --Pulled records for without or logic and with using union above
    Select * from #temp5
    UNION
    --This will return the child DISTRIBUTED and ALLOCATED budgets the pax can give from (as long as their top level parent is assigned to a program)
    --Pulled records for without or logic and with using union above
    select * from #temp4

) AS ELIGIBLE_BUDGETS ON B.ID = ELIGIBLE_BUDGETS.BUDGET_ID
         LEFT JOIN component.BUDGET_MISC BM_USABLE ON B.ID = BM_USABLE.BUDGET_ID AND BM_USABLE.VF_NAME = 'IS_USABLE'
WHERE
--Exclude SCHEDULED and INACTIVE budgets
        B.STATUS_TYPE_CODE = 'ACTIVE' AND B.FROM_DATE < GETDATE()
  AND ( --Ended budgets filter
            @includeEndedBudgets = 1
        OR
            ((@includeEndedBudgets IS NULL OR @includeEndedBudgets = 0) AND (B.THRU_DATE IS NULL OR B.THRU_DATE > GETDATE()))
    )
--Budget is usable
  AND (BM_USABLE.MISC_DATA IS NULL OR BM_USABLE.MISC_DATA = 'TRUE')
--Giving limit filter
  AND (
            @givingLimit = 0
        OR (@givingLimit = 1 AND (B.INDIVIDUAL_GIVING_LIMIT IS NOT NULL OR B.BUDGET_TYPE_CODE = 'ALLOCATED'))
    )
GROUP BY B.ID, B.BUDGET_TYPE_CODE, B.INDIVIDUAL_GIVING_LIMIT, B.STATUS_TYPE_CODE, B.FROM_DATE, B.THRU_DATE

--(+)Logging
    if (@loggingDebug=1)
begin
set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error
	 --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging
END

GO