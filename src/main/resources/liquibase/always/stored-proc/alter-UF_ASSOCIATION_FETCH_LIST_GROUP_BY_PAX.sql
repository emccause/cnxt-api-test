/****** Object:  UserDefinedFunction [component].[UF_ASSOCIATION_FETCH_LIST_GROUP_BY_PAX]    Script Date: 10/16/2020 12:41:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_LIST_GROUP_BY_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_ID
-- RETURN A COMMA DELIMITED LIST OF DISTINCT GROUP DESCRIPTION(S)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE				CHANGE DESCRIPTION
-- DOHOGNTA   	20080707  				CREATED
-- DOHOGNTA   	20150309  				CHANGE IDENTITIES TO BIGINT
-- burgestl		20201021				update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_ASSOCIATION_FETCH_LIST_GROUP_BY_PAX] (
  @pax_id BIGINT
, @level_increment INT)
RETURNS VARCHAR(1000)
AS
BEGIN

DECLARE @delimited_values VARCHAR(1000)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZE
SET @delimited_values = NULL

-- GENERATE COMMA-SEPERATED LIST
SELECT  @delimited_values = COALESCE(@delimited_values + ',', '') + CONVERT(VARCHAR, GROUPS.GROUP_DESC)
FROM  GROUPS
INNER JOIN (SELECT [id] AS GROUP_ID FROM UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, @level_increment)) AS LIST
ON GROUPS.GROUP_ID = LIST.GROUP_ID
ORDER BY GROUPS.GROUP_DESC

-- RETURN VALUE
RETURN @delimited_values

END


GO
