GO

/****** Object:  StoredProcedure [component].[UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS]    Script Date: 3/25/2020 11:04:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- gets recognition criteria use stats for a provided program / users
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- <author>        <date>                    Initial creation
-- MUDDAM        20160226                Added AUTO_APPROVED status code to recognitions 
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- MUDDAM        20170608    MP-9772        Improve performance by removing temp tables and 
--                                        using joins over sub-queries
-- MARTINC        20170608    MP-9772        Cleanup of whitespace
-- burgestl			20200109				Add no locks
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS]
    @programIds NVARCHAR(MAX),
    @userIds NVARCHAR(MAX),
    @startDate DATETIME2,
    @endDate DATETIME2

AS
BEGIN

SET NOCOUNT ON

    SELECT recognition_criteria.ID AS RECOGNITION_CRITERIA_ID,
        MIN(recognition_criteria.RECOGNITION_CRITERIA_NAME) AS RECOGNITION_CRITERIA_DISPLAY_NAME,
        COUNT(DISTINCT recognition.ID) AS RECOGNITIONS_RECEIVED
    FROM component.RECOGNITION recognition with (nolock)
        JOIN component.NOMINATION with (nolock)
            ON nomination.ID = recognition.NOMINATION_ID
        JOIN component.NOMINATION_CRITERIA with (nolock)
            ON nomination.ID = nomination_criteria.NOMINATION_ID
        JOIN component.RECOGNITION_CRITERIA with (nolock)
            ON recognition_criteria.ID = nomination_criteria.RECOGNITION_CRITERIA_ID
        LEFT JOIN component.PAX with (nolock)
            ON nomination.SUBMITTER_PAX_ID = PAX.PAX_ID OR recognition.RECEIVER_PAX_ID = PAX.PAX_ID
        LEFT JOIN component.UF_LIST_TO_TABLE_ID(@userIds, ',') users
            ON pax.pax_id = users.items
        LEFT JOIN component.UF_LIST_TO_TABLE_ID(@programIds, ',') prgm
            ON nomination.PROGRAM_ID = prgm.items
    WHERE nomination.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND recognition.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND (
            @programIds IS NULL OR
            prgm.items IS NOT NULL
        )
        AND (
            @userIds IS NULL
            OR users.items IS NOT NULL
        )
        AND nomination.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
    GROUP BY recognition_criteria.ID
    ORDER BY recognition_criteria.ID

END
GO


