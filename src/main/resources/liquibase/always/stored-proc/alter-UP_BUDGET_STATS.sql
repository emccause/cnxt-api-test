/****** Object:  StoredProcedure [UP_BUDGET_STATS]    Script Date: 10/15/2015 09:39:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ==============================  NAME  ======================================
-- UP_BUDGET_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS BUDGET STATS FOR REPORTING PURPOSES BASED ON TRANSACTIONAL DATA (tied to DISCRETIONARY, not based on giving data ie. nomination)
-- RETURNS THE FOLLOWING STATS...
-- BUDGET USED IN PERIOD
-- BUDGET TOTAL
-- BUDGET AVAILABLE
-- BUDGET EXPIRED
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HAGOPIWL        20150612                CREATED
-- HARWELLM        20150721                MODIFED TO FIX BUG WITH BUDGET AVAILABLE/EXPIRED
-- MUDDAM        20150826                INCREASE DECIMAL FIELD SIZE, CHANGE TO NOT DOUBLE CHILD BUDGET DATA IF PARENT IS ALSO PASSED
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151116                UDM4 CHANGES
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- MADRIDO        20170123    MP-9011        CONSIDERING PENDING TRANSACTION ON USED STATS
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_STATS]
@delim_budget_id_list VARCHAR(MAX), 
@start_date DATETIME2(7),
@end_date DATETIME2(7)

AS
BEGIN
SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

CREATE TABLE #budgetIdParams (
ID BIGINT
)

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF BUDGET_ID(S) TO A LIST OF LONG(S)
INSERT INTO #budgetIdParams ([id]) SELECT token AS budget_id FROM UF_PARSE_STRING_TO_INTEGER (@delim_budget_id_list, @delimiter)

--CREATE TABLE TO RETURN VALUES
CREATE TABLE #budgetStats (
    BUDGET_USED_IN_PERIOD DECIMAL(18,2),
    BUDGET_USED_OUTSIDE_PERIOD DECIMAL(18,2),
    BUDGET_TOTAL DECIMAL(18,2),
    BUDGET_AVAILABLE DECIMAL(18,2),
    BUDGET_EXPIRED DECIMAL(18,2)
)

--Set budget used both in and out of period
INSERT INTO #budgetStats 
(BUDGET_USED_IN_PERIOD)
SELECT ISNULL(SUM(ISNULL(TOTAL_USED_IN_PERIOD,0)),0)
    FROM (
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    SELECT DISTINCT b.ID,
    (CASE
        WHEN DEBITS.DEBITS is null then 0
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS is null then 0
        ELSE CREDITS.CREDITS
    END)
    AS 'TOTAL_USED_IN_PERIOD'
    FROM DISCRETIONARY DISC         
    JOIN BUDGET b ON b.ID = DISC.BUDGET_ID_DEBIT OR b.ID = DISC.BUDGET_ID_CREDIT
    JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
    JOIN TRANSACTION_HEADER th
    on DISC.transaction_header_id = th.id and th.status_type_code  IN ('APPROVED', 'PENDING')
    AND DISC.PERFORMANCE_DATE BETWEEN @start_date AND @end_date    
    LEFT JOIN 
        (--DEBITS IN PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM DISCRETIONARY disc_debit          
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE <> 'FUND'
            AND th_debit.STATUS_TYPE_CODE  IN ('APPROVED', 'PENDING')
        WHERE disc_debit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_debit.BUDGET_ID_DEBIT
        ) DEBITS
        ON B.ID = DEBITS.ID OR B.PARENT_BUDGET_ID = DEBITS.ID            
    LEFT JOIN
        (--CREDITS IN PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM DISCRETIONARY disc_credit             
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE <> 'FUND'
            AND th_credit.STATUS_TYPE_CODE  IN ('APPROVED', 'PENDING')
        WHERE disc_credit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
        ) CREDITS
        ON B.ID = CREDITS.ID OR B.PARENT_BUDGET_ID = CREDITS.ID    
) TOTALS        

--------------------------------------------BEGIN BUDGET_USED_OUTSIDE_PERIOD-------------------------------------------

--Set budget used both in and out of period
DECLARE @total_used_outside_period DECIMAL(18,2)

select @total_used_outside_period = SUM(ISNULL(SUM_TOTAL_USED_OUTSIDE_PERIOD,0)) 
    FROM (
    SELECT  SUM(TOTAL_USED_OUTSIDE_PERIOD) SUM_TOTAL_USED_OUTSIDE_PERIOD
        FROM (    
        SELECT DISTINCT
        (CASE
            WHEN DEBITS_OUTSIDE_PERIOD.DEBITS is null then 0
            ELSE DEBITS_OUTSIDE_PERIOD.DEBITS
        END)
        -
        (CASE
            WHEN CREDITS_OUTSIDE_PERIOD.CREDITS is null then 0
            ELSE CREDITS_OUTSIDE_PERIOD.CREDITS
        END)
        AS 'TOTAL_USED_OUTSIDE_PERIOD'    
        FROM DISCRETIONARY DISC         
        JOIN BUDGET b ON b.ID = DISC.BUDGET_ID_DEBIT OR b.ID = DISC.BUDGET_ID_CREDIT
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        JOIN TRANSACTION_HEADER th
        on DISC.transaction_header_id = th.id and th.status_type_code  IN ('APPROVED', 'PENDING')
        AND DISC.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date    
        LEFT JOIN 
            (--DEBITS OUTSIDE PERIOD
            SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
            FROM DISCRETIONARY disc_debit        
            JOIN TRANSACTION_HEADER th_debit 
                ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
                AND th_debit.TYPE_CODE = 'DISC' 
                AND th_debit.SUB_TYPE_CODE <> 'FUND'
                AND th_debit.STATUS_TYPE_CODE  IN ('APPROVED', 'PENDING')
            AND disc_debit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
            GROUP BY disc_debit.BUDGET_ID_DEBIT
            ) DEBITS_OUTSIDE_PERIOD
            ON B.ID = DEBITS_OUTSIDE_PERIOD.ID OR B.PARENT_BUDGET_ID = DEBITS_OUTSIDE_PERIOD.ID
        LEFT JOIN
            (--CREDITS OUTSIDE PERIOD
            SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
            FROM DISCRETIONARY disc_credit            
            JOIN TRANSACTION_HEADER th_credit 
                ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
                AND th_credit.TYPE_CODE = 'DISC' 
                AND th_credit.SUB_TYPE_CODE <> 'FUND'
                AND th_credit.STATUS_TYPE_CODE  IN ('APPROVED', 'PENDING')
            AND disc_credit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
            GROUP BY disc_credit.BUDGET_ID_CREDIT
            ) CREDITS_OUTSIDE_PERIOD
            ON B.ID = CREDITS_OUTSIDE_PERIOD.ID    OR B.PARENT_BUDGET_ID = CREDITS_OUTSIDE_PERIOD.ID
    ) TOTALS) summary

UPDATE #budgetStats set BUDGET_USED_OUTSIDE_PERIOD = @total_used_outside_period
--------------------------------------------END BUDGET_USED_OUTSIDE_PERIOD-------------------------------------------


--------------------------------------------BEGIN CALCULATE TOTALS------------------------------------------------
DECLARE @total_debits DECIMAL(18,2)
DECLARE @total_credits DECIMAL(18,2)

CREATE TABLE #tempTotalBudget (
    ID BIGINT NOT NULL,
    TRANSACTION_HEADER_ID BIGINT NOT NULL,
    CREDITS DECIMAL(18,2) NULL,
    DEBITS DECIMAL(18,2) NULL,
)

--DEBITS TOTAL
insert into #tempTotalBudget (id, transaction_header_id, debits)
    SELECT distinct b.id, th_debit.id, disc_debit.amount
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'                
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        where th_debit.id not in (select TRANSACTION_HEADER_ID from #tempTotalBudget)
        
select @total_debits = SUM(ISNULL(DEBITS,0)) FROM #tempTotalBudget
    
--CREDITS TOTAL
insert into #tempTotalBudget (id, transaction_header_id, credits)    
        SELECT distinct b.id, th_credit.id, disc_credit.amount
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'        
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        where th_credit.id not in (select TRANSACTION_HEADER_ID from #tempTotalBudget)

select @total_credits = SUM(ISNULL(CREDITS,0)) FROM #tempTotalBudget

-- Set the total on the budgetStats record
UPDATE #budgetStats
SET BUDGET_TOTAL = ISNULL(ISNULL(@total_credits,0)-ISNULL(@total_debits,0),0)
-----------------------------------------------------END CALCULATE TOTALS------------------------------------------------


-----------------------------------------------------BEGIN CALCULATE INDIVIDUAL BUDGET AMOUNTS------------------------------------------------
--Save the total amounts used into a temp table
CREATE TABLE #totalAmountsUsed (
    ID BIGINT NOT NULL,
    TOTAL DECIMAL(18,2) NOT NULL,
    USED DECIMAL(18,2) NOT NULL
)

CREATE TABLE #tempTotals (
    ID BIGINT NOT NULL,
    PARENT_ID BIGINT NULL,
    TRANSACTION_HEADER_ID BIGINT NOT NULL,
    FUND_CREDITS DECIMAL(18,2) NULL,
    FUND_DEBITS DECIMAL(18,2) NULL,
    USED_CREDITS DECIMAL(18,2) NULL,
    USED_DEBITS DECIMAL(18,2) NULL
)

--FUND DEBITS
INSERT INTO #tempTotals (ID, PARENT_ID, TRANSACTION_HEADER_ID, FUND_DEBITS)
--INSERT INTO #tempTotals (ID, PARENT_ID, FUND_DEBITS)
SELECT distinct b.ID, b.PARENT_BUDGET_ID, th_debit.ID, ISNULL(disc_debit.AMOUNT,0) AS 'DEBITS'
--SELECT b.ID, b.PARENT_BUDGET_ID, SUM(ISNULL(disc_debit.AMOUNT,0)) AS 'DEBITS'
FROM BUDGET b 
LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
    AND th_debit.TYPE_CODE = 'DISC' 
    AND th_debit.SUB_TYPE_CODE = 'FUND' 
    AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
JOIN #budgetIdParams bip ON bip.ID = b.ID OR B.PARENT_BUDGET_ID = b.ID
WHERE th_debit.ID NOT IN (SELECT TRANSACTION_HEADER_ID FROM #tempTotals)
--GROUP BY b.ID, b.PARENT_BUDGET_ID

--FUND CREDITS
INSERT INTO #tempTotals (ID, PARENT_ID, TRANSACTION_HEADER_ID, FUND_CREDITS)
--INSERT INTO #tempTotals (ID, PARENT_ID, FUND_CREDITS)
SELECT distinct b.ID, b.PARENT_BUDGET_ID, th_credit.ID, ISNULL(disc_credit.AMOUNT,0) AS 'CREDITS'
--SELECT b.ID, b.PARENT_BUDGET_ID, SUM(ISNULL(disc_credit.AMOUNT,0)) AS 'CREDITS'
FROM BUDGET b 
LEFT JOIN DISCRETIONARY disc_credit 
    ON disc_credit.BUDGET_ID_CREDIT = b.ID            
JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
    AND th_credit.TYPE_CODE = 'DISC' 
    AND th_credit.SUB_TYPE_CODE = 'FUND' 
    AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
WHERE th_credit.ID NOT IN (SELECT TRANSACTION_HEADER_ID FROM #tempTotals)

--USED DEBITS
INSERT INTO #tempTotals (ID, PARENT_ID, TRANSACTION_HEADER_ID, USED_DEBITS)
SELECT distinct b.ID, b.PARENT_BUDGET_ID, th_debit.ID, ISNULL(disc_debit.AMOUNT,0) AS 'DEBITS'
FROM BUDGET b 
LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
    AND th_debit.TYPE_CODE = 'DISC' 
    AND th_debit.SUB_TYPE_CODE <> 'FUND' 
    AND th_debit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
WHERE th_debit.ID NOT IN (SELECT TRANSACTION_HEADER_ID FROM #tempTotals)

--USED CREDITS
INSERT INTO #tempTotals (ID, PARENT_ID, TRANSACTION_HEADER_ID, USED_CREDITS)
SELECT distinct b.ID, b.PARENT_BUDGET_ID, th_credit.ID, ISNULL(disc_credit.AMOUNT,0) AS 'CREDITS'
FROM BUDGET b 
LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
    AND th_credit.TYPE_CODE = 'DISC' 
    AND th_credit.SUB_TYPE_CODE <> 'FUND' 
    AND th_credit.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')
JOIN #budgetIdParams bip ON bip.ID = b.ID OR B.PARENT_BUDGET_ID = bip.ID
WHERE th_credit.ID NOT IN (SELECT TRANSACTION_HEADER_ID FROM #tempTotals)

--Consolodate the #tempTotals rows into #totalAmountsUsed

--Distributed budget IDs
INSERT INTO #totalAmountsUsed
SELECT tt.PARENT_ID,
SUM(ISNULL(tt.FUND_CREDITS,0) - ISNULL(tt.FUND_DEBITS,0)) AS 'TOTAL', SUM(ISNULL(tt.USED_DEBITS,0) - ISNULL(tt.USED_CREDITS,0)) AS 'USED'
FROM #tempTotals tt
JOIN #budgetIdParams bp ON bp.id = tt.PARENT_ID
GROUP BY tt.PARENT_ID

--Simple budget IDs
INSERT INTO #totalAmountsUsed
SELECT tt.ID,
SUM(ISNULL(tt.FUND_CREDITS,0) - ISNULL(tt.FUND_DEBITS,0)) AS 'TOTAL', SUM(ISNULL(tt.USED_DEBITS,0) - ISNULL(tt.USED_CREDITS,0)) AS 'USED'
FROM #tempTotals tt
JOIN #budgetIdParams bp ON bp.id = tt.id
WHERE tt.PARENT_ID is null or tt.PARENT_ID not in (select id from #totalAmountsUsed)
GROUP BY tt.ID

-----------------------------------------------------END CALCULATE INDIVIDUAL BUDGET AMOUNTS------------------------------------------------------------


-----------------------------------------------------BEGIN CALCULATE BUDGET AVAILABLE/EXPIRED------------------------------------------------------------
CREATE TABLE #budgetAvailableExpired (
    BUDGET_ID BIGINT NOT NULL,
    BUDGET_AVAILABLE DECIMAL(18,2) NULL,
    BUDGET_EXPIRED DECIMAL(18,2) NULL
)

--To determine if a budget should be available or expired, look at the end_date in the date range. (Does not matter what the budget looks like today)
--If the budget is INACTIVE or THRU_DATE < @end_date, it is expired. Otherwise available.
--There will always be a HISTORY_BUDGET record so just use that

--Budget Available is if the budget was ACTIVE on the specified @end_date and THRU_DATE > @end_date
INSERT INTO #budgetAvailableExpired
SELECT hb.ID, (ISNULL(ta.TOTAL,0) - ISNULL(ta.USED,0)), 0
FROM #totalAmountsUsed ta
--CROSS APPLY will select only one row when joining on a one to many relationship
CROSS APPLY ( SELECT TOP 1 * FROM HISTORY_BUDGET WHERE ID = ta.ID AND CHANGE_DATE <= @end_date  ORDER BY CHANGE_DATE DESC ) hb
WHERE hb.STATUS_TYPE_CODE = 'ACTIVE'
AND (hb.THRU_DATE > @end_date OR hb.THRU_DATE IS NULL)

--Budget Expired is if the budget was ACTIVE with THRU_DATE < @end_date, or if the budget was INACTIVE on the specified @end_date
INSERT INTO #budgetAvailableExpired
SELECT hb.ID, 0, (ISNULL(ta.TOTAL,0) - ISNULL(ta.USED,0))
FROM #totalAmountsUsed ta
--CROSS APPLY will select only one row when joining on a one to many relationship
CROSS APPLY ( SELECT TOP 1 * FROM HISTORY_BUDGET WHERE ID = ta.ID AND CHANGE_DATE <= @end_date ORDER BY CHANGE_DATE DESC ) hb
WHERE hb.STATUS_TYPE_CODE = 'INACTIVE' OR
(hb.STATUS_TYPE_CODE = 'ACTIVE' AND hb.THRU_DATE <= @end_date)

--If any budgets haven't been used yet, add them to #budgetAvailableExpired so that they get included in the calculations
--ACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT hb.ID, ta.TOTAL, 0
FROM #budgetIdParams bp
--CROSS APPLY will select only one row when joining on a one to many relationship
CROSS APPLY ( SELECT TOP 1 * FROM HISTORY_BUDGET WHERE ID = bp.ID AND CHANGE_DATE <= @end_date  ORDER BY CHANGE_DATE DESC ) hb
JOIN #totalAmountsUsed ta ON ta.ID = bp.ID
WHERE bp.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)
AND hb.STATUS_TYPE_CODE = 'ACTIVE'
AND (hb.THRU_DATE > @end_date OR hb.THRU_DATE IS NULL)

--INACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT hb.ID, 0, ta.TOTAL
FROM #budgetIdParams bp
--CROSS APPLY will select only one row when joining on a one to many relationship
CROSS APPLY ( SELECT TOP 1 * FROM HISTORY_BUDGET WHERE ID = bp.ID AND CHANGE_DATE <= @end_date  ORDER BY CHANGE_DATE DESC ) hb
JOIN #totalAmountsUsed ta ON ta.ID = bp.ID
WHERE bp.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)
AND hb.STATUS_TYPE_CODE = 'INACTIVE' OR
(bp.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired) AND hb.STATUS_TYPE_CODE = 'ACTIVE' AND hb.THRU_DATE <= @end_date)

DECLARE @totalAvailable DECIMAL(18,2)
DECLARE @totalExpired DECIMAL(18,2)
SET @totalAvailable = (SELECT SUM(BUDGET_AVAILABLE) FROM #budgetAvailableExpired)
SET @totalExpired = (SELECT SUM(BUDGET_EXPIRED) FROM #budgetAvailableExpired)

--Set budgetAvailable and budgetExpired on the budget stats record
UPDATE #budgetStats SET BUDGET_AVAILABLE = @totalAvailable, BUDGET_EXPIRED = @totalExpired
FROM #budgetAvailableExpired
-----------------------------------------------------END CALCULATE BUDGET AVAILABLE/EXPIRED------------------------------------------------------------

SELECT * FROM #budgetStats 

DROP TABLE #budgetStats
DROP TABLE #budgetIdParams
DROP TABLE #totalAmountsUsed
DROP TABLE #tempTotalBudget
DROP TABLE #budgetAvailableExpired
DROP TABLE #tempTotals
END


GO

