/****** Object:  StoredProcedure [component].[UP_WEEKLY_DIGEST]    Script Date: 9/22/2016 12:07:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ===============================================
-- UP_WEEKLY_DIGEST
-- ===========================  DESCRIPTION  ===========================================
-- Pulls data needed for the weekly digest email. 
-- The data will be tied to a batch_id and saved in the WEEKLY_DIGEST table.
-- Current data:
--        * My Budget Balance (budgets with giving limits only)
-- Aggregate of last week's data:
--        * Recognitions given and received by me
--        * Likes I have received
--        * Comments I have received
--        * Recognitions given and received by my team
--        * Likes my team has given and received
--        * Comments my team has given and received
--        * Number of my pending approvals
-- Summary of upcoming calendar events (in the next week)
--        * Anniversaries in and out of my network
--        * Birthdays in and out of my network
--        * Observances and other events
-- 
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20160913    MP-7916        CREATED
-- KUMARSJ        20161101    MP-8737        UPDATED MY_BUDGET AND TOTAL_USED
-- HARWELLM        20161108    MP-8833        FIX MY_COMMENTS_RECEIVED CALCULATION
-- HARWELLM        20161109    MP-8838        ADD NETWORK CONNECTIONS COUNT
-- HARWELLM        20161114    MP-8844        FIX MY_BUDGET CALCULATION
-- HARWELLM        20161114    MP-8869        ADD PERMISSIONS COLUMNS
-- MUDDAM        20170109    MP-8056        REMOVED REFERENCES TO PROGRAM_BUDGET_ELIGIBILITY
-- HARWELLM        20170124    MP-9392        FIX MY BUDGET TO INCLUDE ALLOCATED BUDGETS
-- HARWELLM        20170130    MP-9033        ADD @returnAll PARAMTER TO UP_INDIVIDUAL_BUDGET_REMAINING CALL
-- HARWELLM        20170410    MP-9881        REMOVE PERMISSIONS FROM QUERY
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  ==========================================

ALTER PROCEDURE [component].[UP_WEEKLY_DIGEST]
@batchId BIGINT

AS 
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

DECLARE @startDate DATETIME2(0)
DECLARE @endDate DATETIME2(0)

--Format the startDate to be the beginning of last Sunday
SET @startDate = (SELECT DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), -1))
--Format the endDate to be 23:59:59 of Saturday
SET @endDate = (SELECT CONVERT(CHAR(10), (DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 5)), 121) + ' 23:59:59')

--These dates will be used for the "Upcoming Events" section. They are for the upcoming week instead of the prior week
DECLARE @startDateEvents DATETIME2(0)
DECLARE @endDateEvents DATETIME2(0)
SET @startDateEvents = (SELECT DATEADD(wk, DATEDIFF(wk, 0, GETDATE()), -1))
SET @endDateEvents = (SELECT CONVERT(CHAR(10), (DATEADD(wk, DATEDIFF(wk, 0, GETDATE()), 5)), 121) + ' 23:59:59')

DECLARE @likingEnabled NVARCHAR(5)
DECLARE @commentingEnabled NVARCHAR(5)
DECLARE @networkEnabled NVARCHAR(5)

SET @likingEnabled = (SELECT VALUE FROM component.APPLICATION_DATA WHERE KEY_NAME = 'projectProfile.likingEnabled')
SET @commentingEnabled = (SELECT VALUE FROM component.APPLICATION_DATA WHERE KEY_NAME = 'projectProfile.commentingEnabled')
SET @networkEnabled = (SELECT VALUE FROM component.APPLICATION_DATA WHERE KEY_NAME = 'projectProfile.networkConnectionsEnabled')

INSERT INTO component.WEEKLY_DIGEST (BATCH_ID, START_DATE, END_DATE, PAX_ID, CONTROL_NUM, PREFERRED_LOCALE, EMAIL_ADDRESS, 
RECEIVE_EMAIL_DIGEST, DIRECT_REPORT_COUNT, NETWORK_COUNT, REC_GIVEN_PAX, REC_RECEIVED_PAX, LIKE_RECEIVED_PAX, COMMENT_RECEIVED_PAX, 
REC_GIVEN_TEAM, REC_RECEIVED_TEAM, LIKE_RECEIVED_TEAM, COMMENT_RECEIVED_TEAM, LIKE_GIVEN_TEAM, COMMENT_GIVEN_TEAM, MY_APPROVALS,
NETWORK_ANNIVERSARIES, NETWORK_BIRTHDAYS, OBSERVANCES, OTHER_EVENTS, NON_NETWORK_ANNIVERSARIES, NON_NETWORK_BIRTHDAYS)
SELECT DISTINCT @batchId AS 'BATCH_ID', @startDate AS 'START_DATE', @endDate AS 'END_DATE',
p.PAX_ID, p.CONTROL_NUM, p.PREFERRED_LOCALE, e.EMAIL_ADDRESS, pm.MISC_DATA AS 'RECEIVE_EMAIL_DIGEST',
ISNULL(DIRECT_REPORT_COUNT.TOTAL, 0) AS 'DIRECT_REPORT_COUNT',
ISNULL(NETWORK_COUNT.TOTAL, 0) AS 'NETWORK_COUNT',
ISNULL(REC_GIVEN_PAX, 0) AS 'REC_GIVEN_PAX',
ISNULL(REC_RECEIVED_PAX, 0) AS 'REC_RECEIVED_PAX',
ISNULL(LIKE_RECEIVED_PAX, 0) AS 'LIKE_RECEIVED_PAX',
ISNULL(COMMENT_RECEIVED_PAX, 0) AS 'COMMENT_RECEIVED_PAX',
ISNULL(REC_GIVEN_TEAM, 0) AS 'REC_GIVEN_TEAM',
ISNULL(REC_RECEIVED_TEAM, 0) AS 'REC_RECEIVED_TEAM',
ISNULL(LIKE_RECEIVED_TEAM, 0) AS 'LIKE_RECEIVED_TEAM',
ISNULL(COMMENT_RECEIVED_TEAM, 0) AS 'COMMENT_RECEIVED_TEAM',
ISNULL(LIKE_GIVEN_TEAM, 0) AS 'LIKE_GIVEN_TEAM',
ISNULL(COMMENT_GIVEN_TEAM, 0) AS 'COMMENT_GIVEN_TEAM',
ISNULL(APPROVALS.TOTAL, 0) AS 'MY_APPROVALS',
ISNULL(NETWORK_ANNIVERSARIES.TOTAL, 0) AS 'NETWORK_ANNIVERSARIES', 
ISNULL(NETWORK_BIRTHDAYS.TOTAL, 0) AS 'NETWORK_BIRTHDAYS', 
ISNULL(OBSERVANCES.TOTAL, 0) AS 'OBSERVANCES',
ISNULL(OTHER_EVENTS.TOTAL, 0) AS 'OTHER_EVENTS',
ISNULL((ISNULL(TOTAL_ANNIVERSARIES.TOTAL, 0) - ISNULL(NETWORK_ANNIVERSARIES.TOTAL, 0)), 0) AS 'NON_NETWORK_ANNIVERSARIES',
ISNULL((ISNULL(TOTAL_BIRTHDAYS.TOTAL, 0) - ISNULL(NETWORK_BIRTHDAYS.TOTAL, 0)), 0) AS 'NON_NETWORK_BIRTHDAYS'
FROM component.PAX p
JOIN component.PAX_MISC pm ON pm.PAX_ID = p.PAX_ID AND pm.VF_NAME = 'RECEIVE_EMAIL_DIGEST'
JOIN component.SYS_USER su ON su.PAX_ID = p.PAX_ID AND su.STATUS_TYPE_CODE NOT IN ('RESTRICTED', 'INACTIVE')
JOIN component.EMAIL e ON e.PAX_ID = p.PAX_ID AND e.PREFERRED = 'Y'
LEFT JOIN ( --Direct Report Count
    SELECT COUNT(DISTINCT PAX_ID_1) AS 'TOTAL', PAX_ID_2 AS 'PAX_ID'
    FROM component.RELATIONSHIP
    WHERE RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    GROUP BY PAX_ID_2
) DIRECT_REPORT_COUNT ON DIRECT_REPORT_COUNT.PAX_ID = p.PAX_ID
LEFT JOIN ( --Network Count
    SELECT COUNT(DISTINCT PAX_ID_2) AS 'TOTAL', PAX_ID_1 AS 'PAX_ID'
    FROM component.RELATIONSHIP 
    WHERE RELATIONSHIP_TYPE_CODE = 'FRIENDS'
    GROUP BY PAX_ID_1
) NETWORK_COUNT ON NETWORK_COUNT.PAX_ID = p.PAX_ID
LEFT JOIN ( --Recognitions Given by me
    SELECT COUNT(r.ID) AS 'REC_GIVEN_PAX', n.SUBMITTER_PAX_ID
    FROM component.NOMINATION n
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
    WHERE n.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
    AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
    AND r.PARENT_ID IS NULL --Suppress Raises
    GROUP BY n.SUBMITTER_PAX_ID
) REC_GIVEN_PAX ON REC_GIVEN_PAX.SUBMITTER_PAX_ID = p.PAX_ID
LEFT JOIN ( --Recognitions Received by me
    SELECT COUNT(r.ID) AS 'REC_RECEIVED_PAX', r.RECEIVER_PAX_ID
    FROM component.RECOGNITION r
    JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID
    WHERE n.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
    AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
    AND r.PARENT_ID IS NULL --Suppress Raises
    GROUP BY r.RECEIVER_PAX_ID
) REC_RECEIVED_PAX ON REC_RECEIVED_PAX.RECEIVER_PAX_ID = p.PAX_ID
LEFT JOIN ( --Likes Received by me
    SELECT SUM(LIKE_COUNT) AS 'LIKE_RECEIVED_PAX', PAX_ID
    FROM (
        --Likes on recognitions I have GIVEN
        SELECT COUNT(DISTINCT l.LIKE_ID) AS 'LIKE_COUNT', n.SUBMITTER_PAX_ID AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.LIKES l ON l.TARGET_ID = ni.ID AND l.TARGET_TABLE = 'NEWSFEED_ITEM'
            AND l.PAX_ID <> n.SUBMITTER_PAX_ID --Don't count likes the logged in pax has given
        WHERE l.LIKE_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        GROUP BY n.SUBMITTER_PAX_ID
        UNION ALL
        --Likes on recognitions I have RECEIVED
        SELECT COUNT(DISTINCT l.LIKE_ID) AS 'LIKE_COUNT', r.RECEIVER_PAX_ID AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.LIKES l ON l.TARGET_ID = ni.ID AND l.TARGET_TABLE = 'NEWSFEED_ITEM'
            AND l.PAX_ID <> r.RECEIVER_PAX_ID --Don't count likes the logged in pax has given
        WHERE l.LIKE_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        GROUP BY r.RECEIVER_PAX_ID
    ) TOTALS
    GROUP BY TOTALS.PAX_ID
) LIKE_RECEIVED_PAX ON LIKE_RECEIVED_PAX.PAX_ID = p.PAX_ID
LEFT JOIN ( --Comments Received by me
    SELECT SUM(COMMENT_COUNT) AS 'COMMENT_RECEIVED_PAX', PAX_ID
    FROM (
        --Comments on recognitions I have GIVEN
        SELECT COUNT(DISTINCT c.ID) AS 'COMMENT_COUNT', n.SUBMITTER_PAX_ID AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.COMMENT c ON c.TARGET_ID = ni.ID AND c.TARGET_TABLE = 'NEWSFEED_ITEM'
            AND c.PAX_ID <> n.SUBMITTER_PAX_ID --Don't count comments the logged in pax has submitted
        WHERE c.COMMENT_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        AND c.STATUS_TYPE_CODE = 'ACTIVE'
        GROUP BY n.SUBMITTER_PAX_ID
        UNION ALL
        --Comments on recognitions I have RECEIVED
        SELECT COUNT(DISTINCT c.ID) AS 'COMMENT_COUNT', r.RECEIVER_PAX_ID AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.COMMENT c ON c.TARGET_ID = ni.ID AND c.TARGET_TABLE = 'NEWSFEED_ITEM' 
            AND c.PAX_ID <> r.RECEIVER_PAX_ID --Don't count comments the logged in pax has submitted
        WHERE c.COMMENT_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        AND c.STATUS_TYPE_CODE = 'ACTIVE'
        GROUP BY r.RECEIVER_PAX_ID
    ) TOTALS
    GROUP BY TOTALS.PAX_ID
) COMMENT_RECEIVED_PAX ON COMMENT_RECEIVED_PAX.PAX_ID = p.PAX_ID
LEFT JOIN ( --Recognitions Given by my team
    SELECT COUNT(r.ID) AS 'REC_GIVEN_TEAM', rl.PAX_ID_2 AS 'PAX_ID'
    FROM component.NOMINATION n
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
    JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = n.SUBMITTER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    WHERE n.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
    AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
    AND r.PARENT_ID IS NULL --Suppress Raises
    GROUP BY rl.PAX_ID_2
) REC_GIVEN_TEAM ON REC_GIVEN_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --Recognitions Received by my team
    SELECT COUNT(r.ID) AS 'REC_RECEIVED_TEAM', rl.PAX_ID_2 AS 'PAX_ID'
    FROM component.NOMINATION n
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
    JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = r.RECEIVER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    WHERE n.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
    AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
    AND r.PARENT_ID IS NULL --Suppress Raises
    GROUP BY rl.PAX_ID_2
) REC_RECEIVED_TEAM ON REC_RECEIVED_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --Likes Received by my team
    SELECT SUM(LIKE_COUNT) AS 'LIKE_RECEIVED_TEAM', PAX_ID
    FROM (
        --Likes RECEIVED on recognitions my team has GIVEN
        SELECT COUNT(DISTINCT l.LIKE_ID) AS 'LIKE_COUNT', rl.PAX_ID_2 AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = n.SUBMITTER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.LIKES l ON l.TARGET_ID = ni.ID AND l.TARGET_TABLE = 'NEWSFEED_ITEM'
        WHERE l.LIKE_DATE BETWEEN @startDate AND @endDate
        AND n.STATUS_TYPE_CODE = 'APPROVED'
        --Do not include likes my team has given (that is in a below section)
        AND l.PAX_ID NOT IN (SELECT PAX_ID_1 FROM component.RELATIONSHIP WHERE PAX_ID_2 = rl.PAX_ID_2 AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO')
        GROUP BY rl.PAX_ID_2
        UNION ALL
        --Likes RECEIVED on recognitions my team has RECEIVED
        SELECT COUNT(DISTINCT l.LIKE_ID) AS 'LIKE_COUNT', rl.PAX_ID_2 AS 'PAX_ID'
        FROM component.RECOGNITION r
        JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = r.RECEIVER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = r.NOMINATION_ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.LIKES l ON l.TARGET_ID = ni.ID AND l.TARGET_TABLE = 'NEWSFEED_ITEM'
        WHERE l.LIKE_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        --Do not include likes my team has given (that is in a below section)
        AND l.PAX_ID NOT IN (SELECT PAX_ID_1 FROM component.RELATIONSHIP WHERE PAX_ID_2 = rl.PAX_ID_2 AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO')
        GROUP BY rl.PAX_ID_2
    ) TOTALS
    GROUP BY TOTALS.PAX_ID
) LIKE_RECEIVED_TEAM ON LIKE_RECEIVED_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --Comments RECEIEVED by my team
    SELECT SUM(COMMENT_COUNT) AS 'COMMENT_RECEIVED_TEAM', PAX_ID
    FROM (
        --Comments RECEIVED on recognitions my team has GIVEN
        SELECT COUNT(DISTINCT c.ID) AS 'COMMENT_COUNT', rl.PAX_ID_2 AS 'PAX_ID'
        FROM component.NOMINATION n
        JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = n.SUBMITTER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.COMMENT c ON c.TARGET_ID = ni.ID AND c.TARGET_TABLE = 'NEWSFEED_ITEM'
        WHERE c.COMMENT_DATE BETWEEN @startDate AND @endDate
        AND n.STATUS_TYPE_CODE = 'APPROVED'
        AND c.STATUS_TYPE_CODE = 'ACTIVE'
        --Do not include comments my team has given (that is in a below section)
        AND c.PAX_ID NOT IN (SELECT PAX_ID_1 FROM component.RELATIONSHIP WHERE PAX_ID_2 = rl.PAX_ID_2 AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO')
        GROUP BY rl.PAX_ID_2
        UNION ALL
        --Comments RECEIVED on recognitions my team has RECEIVED
        SELECT COUNT(DISTINCT c.ID) AS 'COMMENT_COUNT', rl.PAX_ID_2 AS 'PAX_ID'
        FROM component.RECOGNITION r
        JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = r.RECEIVER_PAX_ID AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
        JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = r.NOMINATION_ID AND ni.TARGET_TABLE = 'NOMINATION'
        JOIN component.COMMENT c ON c.TARGET_ID = ni.ID AND c.TARGET_TABLE = 'NEWSFEED_ITEM' 
        WHERE c.COMMENT_DATE BETWEEN @startDate AND @endDate
        AND r.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
        AND r.PARENT_ID IS NULL --Suppress Raises
        AND c.STATUS_TYPE_CODE = 'ACTIVE'
        --Do not include comments my team has given (that is in a below section)
        AND c.PAX_ID NOT IN (SELECT PAX_ID_1 FROM component.RELATIONSHIP WHERE PAX_ID_2 = rl.PAX_ID_2 AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO')
        GROUP BY rl.PAX_ID_2
    ) TOTALS
    GROUP BY TOTALS.PAX_ID
) COMMENT_RECEIVED_TEAM ON COMMENT_RECEIVED_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --Likes GIVEN by my team
    SELECT COUNT(DISTINCT l.LIKE_ID) AS 'LIKE_GIVEN_TEAM', rl.PAX_ID_2 AS 'PAX_ID'
    FROM component.RELATIONSHIP rl
    JOIN component.LIKES l ON l.PAX_ID = rl.PAX_ID_1 AND l.TARGET_TABLE = 'NEWSFEED_ITEM'
    JOIN component.NEWSFEED_ITEM ni ON ni.ID = l.TARGET_ID
    JOIN component.NOMINATION n ON n.ID = ni.TARGET_ID AND ni.TARGET_TABLE = 'NOMINATION' AND n.STATUS_TYPE_CODE = 'APPROVED'
    WHERE rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    AND l.LIKE_DATE BETWEEN @startDate AND @endDate
    GROUP By rl.PAX_ID_2
) LIKE_GIVEN_TEAM ON LIKE_GIVEN_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --Comments GIVEN by my team
    SELECT COUNT(DISTINCT c.ID) AS 'COMMENT_GIVEN_TEAM', rl.PAX_ID_2 AS 'PAX_ID'
    FROM component.RELATIONSHIP rl
    JOIN component.COMMENT c ON c.PAX_ID = rl.PAX_ID_1 AND c.TARGET_TABLE = 'NEWSFEED_ITEM'
    JOIN component.NEWSFEED_ITEM ni ON ni.ID = c.TARGET_ID
    JOIN component.NOMINATION n ON n.ID = ni.TARGET_ID AND ni.TARGET_TABLE = 'NOMINATION' AND n.STATUS_TYPE_CODE = 'APPROVED'
    WHERE rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    AND c.COMMENT_DATE BETWEEN @startDate AND @endDate
    AND c.STATUS_TYPE_CODE = 'ACTIVE'
    GROUP By rl.PAX_ID_2
) COMMENT_GIVEN_TEAM ON COMMENT_GIVEN_TEAM.PAX_ID = p.PAX_ID
LEFT JOIN ( --My Approvals
    SELECT COUNT(DISTINCT ap.ID) AS 'TOTAL', ap.PAX_ID
    FROM component.ALERT a
    JOIN component.NOMINATION n ON n.ID = a.TARGET_ID
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID AND r.STATUS_TYPE_CODE = 'PENDING'
    JOIN component.APPROVAL_PENDING ap ON ap.TARGET_ID = r.ID AND ap.PAX_ID = a.PAX_ID AND ap.TARGET_TABLE = 'RECOGNITION'
    WHERE a.ALERT_SUB_TYPE_CODE = 'RECOGNITION_APPROVAL'
    GROUP BY ap.PAX_ID
) APPROVALS ON APPROVALS.PAX_ID = p.PAX_ID
LEFT JOIN ( --Anniversaries in my network
    SELECT COUNT(ce.ID) AS 'TOTAL', r.PAX_ID_1 AS 'PAX_ID'
    FROM component.CALENDAR_ENTRY ce
    JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
    JOIN component.RELATIONSHIP r ON r.PAX_ID_2 = ce.TARGET_ID AND ce.TARGET_TABLE = 'PAX' AND r.RELATIONSHIP_TYPE_CODE = 'FRIENDS'
    WHERE cst.CALENDAR_SUB_TYPE_CODE = 'SERVICE_ANNIVERSARY'
    AND ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
    GROUP BY r.PAX_ID_1
) NETWORK_ANNIVERSARIES ON NETWORK_ANNIVERSARIES.PAX_ID = p.PAX_ID
LEFT JOIN ( --Birthdays in my network
    SELECT COUNT(ce.ID) AS 'TOTAL', r.PAX_ID_1 AS 'PAX_ID'
    FROM component.CALENDAR_ENTRY ce
    JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
    JOIN component.RELATIONSHIP r ON r.PAX_ID_2 = ce.TARGET_ID and ce.TARGET_TABLE = 'PAX' AND r.RELATIONSHIP_TYPE_CODE = 'FRIENDS'
    WHERE cst.CALENDAR_SUB_TYPE_CODE = 'BIRTHDAY'
    AND ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
    GROUP BY r.PAX_ID_1
) NETWORK_BIRTHDAYS ON NETWORK_BIRTHDAYS.PAX_ID = p.PAX_ID
LEFT JOIN ( --Observances
    SELECT COUNT(ce.ID) AS 'TOTAL'
    FROM component.CALENDAR_ENTRY ce
    JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
    JOIN component.CALENDAR_TYPE ct ON ct.ID = cst.CALENDAR_TYPE_ID and ct.CALENDAR_TYPE_CODE = 'HOLIDAY'
    WHERE ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
    OR (ce.FROM_DATE < @startDateEvents AND ce.THRU_DATE > @endDateEvents)
) OBSERVANCES ON 1=1
LEFT JOIN ( --Other events
    SELECT SUM(TOTAL) AS 'TOTAL' , PAX_ID FROM (
        --My custom calendar events by PAX
        SELECT COUNT(DISTINCT ce.ID) AS 'TOTAL', acl.SUBJECT_ID AS 'PAX_ID'
        FROM component.CALENDAR_ENTRY ce
        JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
        JOIN component.CALENDAR_TYPE ct ON ct.ID = cst.CALENDAR_TYPE_ID and ct.CALENDAR_TYPE_CODE = 'CUSTOM'
        LEFT JOIN component.ACL acl ON acl.TARGET_ID = ce.ID AND acl.TARGET_TABLE = 'CALENDAR' AND acl.SUBJECT_TABLE = 'PAX'
        WHERE ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
        OR (ce.FROM_DATE < @startDateEvents AND ce.THRU_DATE > @endDateEvents)
        GROUP BY acl.SUBJECT_ID
        UNION ALL
        --My custom calendar events by GROUP
        SELECT COUNT(DISTINCT ce.ID) AS 'TOTAL', gp.PAX_ID
        FROM component.CALENDAR_ENTRY ce
        JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
        JOIN component.CALENDAR_TYPE ct ON ct.ID = cst.CALENDAR_TYPE_ID and ct.CALENDAR_TYPE_CODE = 'CUSTOM'
        LEFT JOIN component.ACL acl ON acl.TARGET_ID = ce.ID AND acl.TARGET_TABLE = 'CALENDAR' AND acl.SUBJECT_TABLE = 'GROUPS'
        LEFT JOIN component.GROUPS_PAX gp ON gp.GROUP_ID = acl.SUBJECT_ID AND acl.SUBJECT_TABLE = 'GROUPS'
        WHERE ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
        OR (ce.FROM_DATE < @startDateEvents AND ce.THRU_DATE > @endDateEvents)
        GROUP BY gp.PAX_ID
    ) TOTALS
    GROUP BY PAX_ID
) OTHER_EVENTS ON OTHER_EVENTS.PAX_ID = p.PAX_ID
LEFT JOIN ( --Total Anniversaries
    SELECT COUNT(ce.ID) AS 'TOTAL'
    FROM component.CALENDAR_ENTRY ce
    JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
    WHERE cst.CALENDAR_SUB_TYPE_CODE = 'SERVICE_ANNIVERSARY'
    AND ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
) TOTAL_ANNIVERSARIES ON 1=1
LEFT JOIN ( --Total Birthdays
    SELECT COUNT(ce.ID) AS 'TOTAL'
    FROM component.CALENDAR_ENTRY ce
    JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID
    WHERE cst.CALENDAR_SUB_TYPE_CODE = 'BIRTHDAY'
    AND ce.FROM_DATE BETWEEN @startDateEvents AND @endDateEvents
) TOTAL_BIRTHDAYS ON 1=1


--This temp table will be used to save the results of UP_INDIVIDUAL_ELIGIBLE_BUDGET_USED
CREATE TABLE #budgetInfo (
    PAX_ID BIGINT,
    MY_BUDGET DECIMAL(10,2)
)

--Get every pax's indidual budget remaining
INSERT INTO #budgetInfo EXEC component.UP_INDIVIDUAL_BUDGET_REMAINING 1, 0

--Save the budget balances to WEEKLY_DIGEST
UPDATE wd 
SET wd.MY_BUDGET = 
CASE 
    WHEN bi.MY_BUDGET IS NULL THEN 0
    WHEN bi.MY_BUDGET < 0 THEN 0
    ELSE bi.MY_BUDGET
END
FROM component.WEEKLY_DIGEST wd 
JOIN #budgetInfo bi ON bi.PAX_ID = wd.PAX_ID
WHERE wd.BATCH_ID = @batchId

--Replace all NULL values with 0 for budgets w/o giving limit
UPDATE component.WEEKLY_DIGEST SET MY_BUDGET = 0 WHERE MY_BUDGET IS NULL AND BATCH_ID = @batchId

DROP TABLE #budgetInfo

--Final Result Set
SELECT * FROM component.WEEKLY_DIGEST WHERE BATCH_ID = @batchId

END;
GO