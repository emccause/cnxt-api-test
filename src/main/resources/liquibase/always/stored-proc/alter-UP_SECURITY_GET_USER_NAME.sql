SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_SECURITY_GET_USER_NAME
-- ===========================  DESCRIPTION  ==================================
-- THE PURPOSE OF THIS STORED PROCEDURE IS TO RETURN THE USER NAME OF A 
-- GIVEN SPID.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- DOHOGNTA     20070201    CREATED
-- KNIGHTGA     20200604    Updated to use CONTEXT_INFO instead of table for efficiency
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_SECURITY_GET_USER_NAME]
    @user_name nvarchar(80) OUTPUT
AS
BEGIN
SET NOCOUNT ON

--convert context info from varbinary(100) to nvarchar(50)
DECLARE @contextInfo NVARCHAR(50)
select @contextInfo = REPLACE(CAST(CONTEXT_INFO() AS NVARCHAR(50)) COLLATE Latin1_General_100_BIN2, 0x00, '')

select @user_name = case when len(@contextInfo) = 0 then null else @contextInfo end

END
