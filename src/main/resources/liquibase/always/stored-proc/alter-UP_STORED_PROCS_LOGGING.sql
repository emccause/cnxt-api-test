/****** Object:  StoredProcedure [component].[UP_STORED_PROCS_LOGGING]    Script Date: 11/18/2020 1:40:15 PM ******/
IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_STORED_PROCS_LOGGING' AND schema_id = SCHEMA_ID('component'))
    BEGIN
        exec ('create PROCEDURE component.UP_STORED_PROCS_LOGGING as BEGIN select 1 as c1 END')
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_STORED_PROCS_LOGGING
-- ===========================  DESCRIPTION  ==================================
--
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        STORY        CHANGE DESCRIPTION
-- burgestl		20201116	CNXT-335	Create STORED Procedure to gather information from other stored procedures
--										To capture Calling stored procedures name and parameter setting and start and end time
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_STORED_PROCS_LOGGING]
@callingProcedureName nvarchar(150),
@paramters nvarchar(max),
@startDate DATETIME2(7),
@errorException nvarchar(250)

AS
BEGIN
SET NOCOUNT ON;

declare @errorExceptionCount int
declare @endDate DATETIME2(7)
set @endDate = getdate()


BEGIN TRY 
	if (@callingProcedureName is null) 
	begin 
		set @errorException = ': Stored Procedure name is null :' 
	end
END TRY  
BEGIN CATCH
	set @errorException = ': Stored Procedure name is null :' 
END CATCH;  

BEGIN TRY 
	if (@paramters is null) 
	begin 
		set @errorException = @errorException + ': Parameter is null :' 
	end
END TRY  
BEGIN CATCH
	set @errorException = @errorException + ': Parameter is null :' 
END CATCH; 


BEGIN TRY  
	if (@startDate is null or isdate(cast(@startDate as DATETIME)) <> 1)
	
	begin
		select @errorException = @errorException + ': Start date is invalid :'
	end

END TRY  
BEGIN CATCH  
	select @errorException = @errorException + ': Start date is invalid :'
END CATCH;


if (len(@errorException) = 0) 
begin
	set @errorException = null
end

insert [component].[STORED_PROCS_LOGGING] ([PROCEDURE], [PARAMETERS],[START_DATE], END_DATE, ERROR_EXCEPTION )
select @callingProcedureName, @paramters, @startDate, @endDate,@errorException
	
end


