SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ===============================================
-- UP_GET_AWARD_TYPE_BY_PAYOUT_TYPE
-- ===========================  DESCRIPTION  ===========================================
-- GET Award Type
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- KUMARSJ       20181227    TIR-140      CREATED
--
-- ===========================  DECLARATIONS  ==========================================
ALTER PROCEDURE component.UP_GET_AWARD_TYPE_BY_PAYOUT_TYPE
  @payoutTypeCode NVARCHAR(50)

AS 
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

SELECT DISTINCT(PI.PAYOUT_ITEM_ID) AS ID,  
          PAYOUT_TYPE,  
          DISPLAY_NAME,  
          SHORT_DESC,  
          LONG_DESC,  
          AWARD_VALUE,  
          ABS,  
          SUB_CLIENT,  
          LOOKUP_PIVOTED.PROJECT_NUMBER,  
          LOOKUP_PIVOTED.SUB_PROJECT_NUMBER,  
          CATALOG_URL,  
          CATALOG_DEEP_LINK,  
           -- Does payout match either a program misc or earnings entry?
           case
                     when exists
                     (
                           select null
                           from component.PROGRAM_MISC as PM
                           where PM.MISC_DATA = @payoutTypeCode
                                  AND PM.VF_NAME = 'SELECTED_AWARD_TYPE'
                     ) then 'TRUE'
                     when exists
                     (
                           select null
                           from component.EARNINGS as E
                           where E.EARNINGS_TYPE_CODE = @payoutTypeCode
                     ) then 'TRUE'
                     else 'FALSE'
              end as IS_USED
       FROM
       ( 
          SELECT LOOKUP_CATEGORY_CODE , 
                LOOKUP_CODE , 
                LOOKUP_DESC  
          FROM component.LOOKUP  
          WHERE LOOKUP_TYPE_CODE ='AWARD_TYPE' 
       ) AS LOOKUP_TO_BE_PIVOTED PIVOT
       (
              MAX(LOOKUP_DESC) FOR LOOKUP_CODE IN
              (
                     PAYOUT_TYPE,
                     DISPLAY_NAME, SHORT_DESC, LONG_DESC, AWARD_VALUE, ABS, SUB_CLIENT, PROJECT_NUMBER,  
                     SUB_PROJECT_NUMBER, CATALOG_URL, CATALOG_DEEP_LINK
              )
       )
       AS LOOKUP_PIVOTED  
       INNER JOIN component.PAYOUT_ITEM PI  
              ON PI.PAYOUT_ITEM_NAME = LOOKUP_PIVOTED.PAYOUT_TYPE  
       WHERE PAYOUT_TYPE = @payoutTypeCode
       ORDER BY DISPLAY_NAME 
END --SP
GO