
GO
/****** Object:  StoredProcedure [component].[UP_PROGRAM_ELIGIBILITY]    Script Date: 11/30/2020 2:11:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_PROGRAM_ELIGIBILITY
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS TRUE IF THE USER IS ELIGIBLE TO GIVE/RECEIVE OUT OF SPECIFIC TYPES OF PROGRAMS
--
-- @paxId - The pax to check eligibility for
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY    CHANGE DESCRIPTION
-- HARWELLM        20170306    MP-9673    CREATED
-- SHARPCA         20201130    CNXT-356   ADDED LOGGING
-- KROEGEGK        20201201    CNXT-360   IMPROVE PERFORMANCE
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_PROGRAM_ELIGIBILITY]
@paxId BIGINT

AS
BEGIN


--(+)Logging
BEGIN TRY
declare @loggingStartDate  DATETIME2(7),
        @loggingEndDate  DATETIME2(7),
        @loggingParameters NVARCHAR(MAX),
        @loggingSprocName nvarchar(150),
        @loggingDebug int


--insert component.application_data (key_name, value)
--select 'sql.logging','true'
select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

if (@loggingDebug=1)
begin
set @loggingSprocName = 'UP_PROGRAM_ELIGIBILITY'
set @loggingStartDate = getdate()
set @loggingParameters = '@paxId=' + isnull(cast(@paxId as varchar),'null') --BIGINT
end
--(-)Logging

SET NOCOUNT ON;

SET NOCOUNT ON;
SELECT GROUP_ID INTO #tmpGroupIds FROM component.VW_GROUP_TOTAL_MEMBERSHIP WHERE PAX_ID = @paxId;

WITH RECOGNITION (ROLE_CODE, PROGRAM_ID) AS (
    SELECT r.ROLE_CODE, MIN(p.PROGRAM_ID) AS 'PROGRAM_ID'
    FROM component.ROLE r
    INNER JOIN component.ACL acl ON acl.ROLE_ID = r.ROLE_ID AND acl.TARGET_TABLE = 'PROGRAM'
    INNER JOIN component.PROGRAM p ON p.PROGRAM_ID = acl.TARGET_ID AND p.STATUS_TYPE_CODE = 'ACTIVE' AND p.FROM_DATE < GETDATE()
    WHERE r.ROLE_CODE in ('PGIV', 'PREC')
    AND (
        (acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = @paxId)
        OR (acl.SUBJECT_TABLE = 'GROUPS' AND acl.SUBJECT_ID IN (SELECT GROUP_ID FROM #tmpGroupIds))
    )
    AND p.PROGRAM_TYPE_CODE IN ('ECRD', 'PTP', 'MGRD')
    AND (p.THRU_DATE IS NULL OR p.THRU_DATE > GETDATE())

    GROUP BY r.ROLE_CODE
),
AWARD_CODE (ROLE_CODE, PRINTABLE, DOWNLOAD) AS (
    SELECT r.ROLE_CODE, MAX(pm_print.MISC_DATA) AS 'PRINTABLE', MAX(pm_download.MISC_DATA) AS 'DOWNLOAD'
    FROM component.ROLE r
    INNER JOIN component.ACL acl ON acl.ROLE_ID = r.ROLE_ID AND acl.TARGET_TABLE = 'PROGRAM'
    INNER JOIN component.PROGRAM p ON p.PROGRAM_ID = acl.TARGET_ID AND p.STATUS_TYPE_CODE = 'ACTIVE' AND p.FROM_DATE < GETDATE()
    LEFT JOIN component.PROGRAM_MISC pm_print ON pm_print.PROGRAM_ID = p.PROGRAM_ID AND pm_print.VF_NAME = 'IS_PRINTABLE'
    LEFT JOIN component.PROGRAM_MISC pm_download ON pm_download.PROGRAM_ID = p.PROGRAM_ID AND pm_download.VF_NAME = 'IS_DOWNLOADABLE'
    WHERE r.ROLE_CODE in ('PGIV', 'PREC')
    AND (
        (acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = @paxId)
        OR (acl.SUBJECT_TABLE = 'GROUPS' AND acl.SUBJECT_ID in (SELECT GROUP_ID FROM #tmpGroupIds))
    )
    AND p.PROGRAM_TYPE_CODE IN ('AWARD_CODE')
    AND (p.THRU_DATE IS NULL OR p.THRU_DATE > GETDATE())
    GROUP BY r.ROLE_CODE
),
POINT_LOAD (ROLE_CODE, PROGRAM_ID) AS (
    SELECT r.ROLE_CODE, MIN(p.PROGRAM_ID) AS 'PROGRAM_ID'
    FROM component.ROLE r
    INNER JOIN component.ACL acl ON acl.ROLE_ID = r.ROLE_ID AND acl.TARGET_TABLE = 'PROGRAM'
    INNER JOIN component.PROGRAM p ON p.PROGRAM_ID = acl.TARGET_ID AND p.STATUS_TYPE_CODE = 'ACTIVE' AND p.FROM_DATE < GETDATE()
    WHERE r.ROLE_CODE in ('PGIV', 'PREC')
    AND (
        (acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = @paxId)
        OR (acl.SUBJECT_TABLE = 'GROUPS' AND acl.SUBJECT_ID in (SELECT GROUP_ID FROM #tmpGroupIds))
    )
    AND p.PROGRAM_TYPE_CODE IN ('PNTU')
    AND (p.THRU_DATE IS NULL OR p.THRU_DATE > GETDATE())
    GROUP BY r.ROLE_CODE
)

SELECT r.ROLE_CODE, 
CASE
    WHEN recg.PROGRAM_ID IS NOT NULL THEN 'TRUE'
END AS 'RECOGNITION',
ac.PRINTABLE, ac.DOWNLOAD,
CASE
    WHEN pl.PROGRAM_ID IS NOT NULL THEN 'TRUE'
END AS 'POINT_LOAD'
FROM component.ROLE r
LEFT JOIN RECOGNITION recg ON recg.ROLE_CODE = r.ROLE_CODE
LEFT JOIN AWARD_CODE ac ON ac.ROLE_CODE = r.ROLE_CODE
LEFT JOIN POINT_LOAD pl ON pl.ROLE_CODE = r.ROLE_CODE
WHERE r.ROLE_CODE IN ('PGIV', 'PREC');



--(+)Logging
if (@loggingDebug=1)
begin
    set @loggingEndDate = getdate()
    -- Generate a divide-by-zero error
    --SELECT  1 / 0 AS Error;
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY  
BEGIN CATCH
    if (@loggingDebug=1)
    begin
        declare @errorMessage nvarchar(250)
        SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
        exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
    end
END CATCH
--(-)Logging

END;

