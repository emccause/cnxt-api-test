SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_PAX_INFO
-- ===========================  DESCRIPTION  ==================================
-- FETCHES DEMOGRAPHIC AND PAX_GROUP_MISC DATA FOR EACH PAX_ID PASSED IN
--
-- FIELDS INCLUDE:
--    PAX_ID
--    EMAIL_ADDRESS
--    CITY
--    STATE
--    ZIP
--    COUNTRY_CODE
--    TITLE
--    COMPANY_NAME
--    DEPARTMENT
--    COST_CENTER
--    FUNCTION
--    GRADE
--    AREA
--    CUSTOM_1
--    CUSTOM_2
--    CUSTOM_3
--    CUSTOM_4
--    CUSTOM_5
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170118    MP-9910        CREATED
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_PAX_INFO]
@paxIdList NVARCHAR(MAX)

AS
BEGIN
SET NOCOUNT ON;

DECLARE    @delimiter CHAR(1)
SET    @delimiter = ',';

SELECT
p.PAX_ID,
e.EMAIL_ADDRESS, 
a.CITY, 
a.STATE, 
a.ZIP, 
a.COUNTRY_CODE,
pgm_title.MISC_DATA AS 'TITLE', 
pgm_companyName.MISC_DATA AS 'COMPANY_NAME', 
pgm_department.MISC_DATA AS 'DEPARTMENT',
pgm_costCenter.MISC_DATA AS 'COST_CENTER', 
pgm_function.MISC_DATA AS 'PAX_FUNCTION', 
pgm_grade.MISC_DATA AS 'GRADE', 
pgm_area.MISC_DATA AS 'AREA',
pgm_custom1.MISC_DATA AS 'CUSTOM_1', 
pgm_custom2.MISC_DATA AS 'CUSTOM_2', 
pgm_custom3.MISC_DATA AS 'CUSTOM_3', 
pgm_custom4.MISC_DATA AS 'CUSTOM_4', 
pgm_custom5.MISC_DATA AS 'CUSTOM_5'
FROM component.PAX p
JOIN component.PAX_GROUP pg ON pg.PAX_ID = p.PAX_ID
LEFT JOIN component.EMAIL e ON e.PAX_ID = p.PAX_ID AND e.PREFERRED = 'Y'
LEFT JOIN component.ADDRESS a ON a.PAX_ID = p.PAX_ID AND a.PREFERRED = 'Y'
LEFT JOIN component.PAX_GROUP_MISC pgm_title 
    ON pgm_title.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_title.VF_NAME = 'JOB_TITLE'
LEFT JOIN component.PAX_GROUP_MISC pgm_companyName 
    ON pgm_companyName.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_companyName.VF_NAME = 'COMPANY_NAME'
LEFT JOIN component.PAX_GROUP_MISC pgm_department 
    ON pgm_department.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_department.VF_NAME = 'DEPARTMENT'
LEFT JOIN component.PAX_GROUP_MISC pgm_costCenter 
    ON pgm_costCenter.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_costCenter.VF_NAME = 'COST_CENTER'
LEFT JOIN component.PAX_GROUP_MISC pgm_area 
    ON pgm_area.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_area.VF_NAME = 'AREA'
LEFT JOIN component.PAX_GROUP_MISC pgm_grade 
    ON pgm_grade.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_grade.VF_NAME = 'GRADE'
LEFT JOIN component.PAX_GROUP_MISC pgm_function 
    ON pgm_function.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_function.VF_NAME = 'FUNCTION'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom1 
    ON pgm_custom1.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom1.VF_NAME = 'CUSTOM_1'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom2 
    ON pgm_custom2.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom2.VF_NAME = 'CUSTOM_2'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom3 
    ON pgm_custom3.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom3.VF_NAME = 'CUSTOM_3'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom4 
    ON pgm_custom4.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom4.VF_NAME = 'CUSTOM_4'
LEFT JOIN component.PAX_GROUP_MISC pgm_custom5 
    ON pgm_custom5.PAX_GROUP_ID = pg.PAX_GROUP_ID AND pgm_custom5.VF_NAME = 'CUSTOM_5'
LEFT JOIN component.UF_LIST_TO_TABLE_ID(@paxIdList, @delimiter) PAX_IDS ON PAX_IDS.items = p.PAX_ID
WHERE @paxIdList IS NULL OR  PAX_IDS.items IS NOT NULL

END

