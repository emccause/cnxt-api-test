/****** Object:  StoredProcedure [UP_ABS_AUDIT_REPORT]    Script Date: 10/14/2015 12:27:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- [UP_ABS_AUDIT_REPORT]
-- ===========================  DESCRIPTION  ==================================
-- GIVEN FROM_DATE AND THRU_DATE
-- RETURN DETAILS for ABS ISSUANCES GROUPED BY PROJECT, SUB_PROJECT 
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY       CHANGE DESCRIPTION
-- KUMARSJ      20150908    			INITIAL PROC
-- MUDDAM       20151102                UDM3 CHANGES
-- MUDDAM       20151113                UDM4 CHANGES
-- GARCIAF2     20160325                REMOVE "component." SCHEMA
-- GARCIAF2     20160816    MP-5458     CHANGE KEY_NAMES TO vendorProcessing.abs.clientNumber,
--                                      vendorProcessing.abs.clientName and vendorprocessing.abs.subclientnumber
-- HARWELLM     20160901    MP-8150     Update substring function to match the new key_name
-- MUDDAM       20170411    MP-8001     UDM5 CHANGES
-- burgestl		20201021				Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ABS_AUDIT_REPORT]
    @FROM_DATE NVARCHAR(30)
,    @THRU_DATE NVARCHAR(30)
AS
BEGIN

SET NOCOUNT ON

-- DRIVER
        DECLARE @CLIENT_NUM NVARCHAR(10)
        DECLARE @CLIENT_NAME NVARCHAR(50)
        DECLARE @AVAIALBE_SUBCLIENT_NUMBER NVARCHAR(10)
        DECLARE @SUBCLIENT_NUM_COUNT INT

        --SET @FROM_DATE = '01/15/2015'
        --SET @THRU_DATE = '09/15/2015'

        IF (@FROM_DATE = '-1')
        BEGIN
            SET @FROM_DATE = (SELECT CONVERT(NVARCHAR, GETDATE()-1, 102))
        END
        IF (@THRU_DATE = '-1')
        BEGIN
            SET @THRU_DATE = (SELECT CONVERT(NVARCHAR, GETDATE()-1, 102))
        END

        SELECT @CLIENT_NUM = VALUE FROM APPLICATION_DATA 
         WHERE KEY_NAME IN ('vendorProcessing.abs.clientNumber') 

        SELECT @CLIENT_NAME = VALUE FROM APPLICATION_DATA 
         WHERE KEY_NAME IN ('vendorProcessing.abs.clientName')

        DECLARE @AVAILABLE_SUBCLIENT_NUM TABLE (
            KEY_NAME NVARCHAR(50),
            PAYOUT_TYPE_CODE NVARCHAR(10),
            SUBCLIENT_NUM NVARCHAR(10)
        )

        INSERT INTO @AVAILABLE_SUBCLIENT_NUM
             SELECT KEY_NAME, PT.PAYOUT_TYPE_CODE, VALUE 
			 FROM component.APPLICATION_DATA AD
             INNER JOIN component.PAYOUT_TYPE PT ON PT.PAYOUT_TYPE_CODE = SUBSTRING(AD.KEY_NAME, 39 , 5)
             WHERE KEY_NAME LIKE 'vendorprocessing.mars.subclientnumber%'
            UNION
			 SELECT KEY_NAME, PT.PAYOUT_TYPE_CODE, VALUE 
             FROM component.APPLICATION_DATA AD
             inner join component.PAYOUT_VENDOR PV on PV.PAYOUT_VENDOR_NAME = 'ABS'
             inner join component.PAYOUT_ITEM PIT on PV.PAYOUT_VENDOR_ID = PIT.PAYOUT_VENDOR_ID
             inner join component.PAYOUT_TYPE PT on PT.PAYOUT_TYPE_DESC = PIT.PAYOUT_ITEM_NAME
             WHERE AD.KEY_NAME LIKE 'vendorprocessing.abs.subclientnumber%'

        DECLARE @PAYOUT_DETAILS TABLE (
            PAYOUT_ID INT,
            PAX_ID INT,
            PROJECT_NUMBER NVARCHAR(25),
            SUB_PROJECT_NUMBER NVARCHAR(10),
            PARTICIPANT_ID NVARCHAR(255),
            FIRST_NAME NVARCHAR(255),
            LAST_NAME NVARCHAR(255),
            ISSUANCE_AMOUNT DECIMAL(10,2),
            ACCOUNT_NUMBER NVARCHAR(20),
            PAYOUT_TYPE_CODE NVARCHAR(50),
            PRODUCT_CODE NVARCHAR(50)
        )

        INSERT INTO @PAYOUT_DETAILS
            SELECT DISTINCT(P.PAYOUT_ID)
                    , PAX.PAX_ID
                    , P.PROJECT_NUMBER
                    , P.SUB_PROJECT_NUMBER
                    , PAX.CONTROL_NUM
                    , PAX.FIRST_NAME
                    , PAX.LAST_NAME
                    , P.PAYOUT_AMOUNT
                    , PA.CARD_NUM
                    , P.PAYOUT_TYPE_CODE
                    , 'PPP' --PIT.PRODUCT_CODE
            FROM component.PAYOUT AS P  
                INNER JOIN component.PAX AS PAX ON P.PAX_ID = PAX.PAX_ID
                INNER JOIN component.PAYOUT_TYPE PT ON P.PAYOUT_TYPE_CODE = PT.PAYOUT_TYPE_CODE
                INNER JOIN component.PAYOUT_ITEM PIT ON PT.PAYOUT_TYPE_DESC = PIT.PAYOUT_ITEM_NAME
                INNER JOIN component.PAX_ACCOUNT AS PA ON PA.PAX_ID = PAX.PAX_ID 
                    AND ISSUED = 'Y' AND PA.CARD_TYPE_CODE = P.PAYOUT_TYPE_CODE
                INNER JOIN component.HISTORY_PAYOUT AS HP ON P.PAYOUT_ID = HP.PAYOUT_ID
                INNER JOIN component.PAYOUT_VENDOR AS PV ON PV.PAYOUT_VENDOR_ID = PIT.PAYOUT_VENDOR_ID
                       AND PV.PAYOUT_VENDOR_NAME IN ('MARS','ABS')
            WHERE P.STATUS_TYPE_CODE = 'ISSUED' 
                AND P.STATUS_DATE >= @FROM_DATE AND P.STATUS_DATE < (CONVERT(DATETIME, @THRU_DATE, 102)+1)
                AND HP.STATUS_TYPE_CODE = 'ISSUED' AND HP.CHANGE_DATE >= @FROM_DATE AND HP.CHANGE_DATE < (CONVERT(DATETIME, @THRU_DATE, 102)+1)

        SELECT CONVERT(VARCHAR, GETDATE(), 106) AS REPORT_DATE, @FROM_DATE AS FROMDATE, @THRU_DATE AS THRUDATE

        DECLARE @DISTINCT_SUBCLIENT_NUM TABLE ( ROWNUM INT IDENTITY(1,1) NOT NULL, SUBCLIENT_NUM NVARCHAR(10))
        INSERT INTO @DISTINCT_SUBCLIENT_NUM SELECT DISTINCT SUBCLIENT_NUM FROM @AVAILABLE_SUBCLIENT_NUM ORDER BY SUBCLIENT_NUM DESC

        SELECT * from @AVAILABLE_SUBCLIENT_NUM 

        SELECT @SUBCLIENT_NUM_COUNT = COUNT(1) FROM @DISTINCT_SUBCLIENT_NUM

        DECLARE @i INT = @SUBCLIENT_NUM_COUNT
        WHILE @i > 0
        BEGIN 

            SELECT @AVAIALBE_SUBCLIENT_NUMBER = SUBCLIENT_NUM FROM @DISTINCT_SUBCLIENT_NUM WHERE ROWNUM = @i

            SELECT 'subclient' AS RSLT_INDICATOR, @CLIENT_NUM AS CLIENT_NUM, @AVAIALBE_SUBCLIENT_NUMBER AS SUBCLIENT_NUM, @CLIENT_NAME AS CLIENT_NAME

            SELECT 'payoutdetail' AS RSLT_INDICATOR, * FROM @PAYOUT_DETAILS RES
                INNER JOIN @AVAILABLE_SUBCLIENT_NUM SN ON SN.PAYOUT_TYPE_CODE = RES.PAYOUT_TYPE_CODE
                WHERE SN.SUBCLIENT_NUM = @AVAIALBE_SUBCLIENT_NUMBER

            SELECT 'total' AS RSLT_INDICATOR, SUM(ISSUANCE_AMOUNT) AS ISSUANCE_AMOUNT_TOTAL FROM @PAYOUT_DETAILS RES
                INNER JOIN @AVAILABLE_SUBCLIENT_NUM SN ON SN.PAYOUT_TYPE_CODE = RES.PAYOUT_TYPE_CODE
                WHERE SN.SUBCLIENT_NUM = @AVAIALBE_SUBCLIENT_NUMBER

            SELECT 'totalbyprjsubprj' AS RSLT_INDICATOR, PROJECT_NUMBER, SUB_PROJECT_NUMBER, SUM(ISSUANCE_AMOUNT) AS ISSUANCE_AMOUNT 
            FROM @PAYOUT_DETAILS RES
                INNER JOIN @AVAILABLE_SUBCLIENT_NUM SN ON SN.PAYOUT_TYPE_CODE = RES.PAYOUT_TYPE_CODE
            WHERE SN.SUBCLIENT_NUM = @AVAIALBE_SUBCLIENT_NUMBER AND (PROJECT_NUMBER IS NOT NULL OR SUB_PROJECT_NUMBER IS NOT NULL)
            GROUP BY PROJECT_NUMBER, SUB_PROJECT_NUMBER 

        SET @i = @i - 1
        END

END
GO
