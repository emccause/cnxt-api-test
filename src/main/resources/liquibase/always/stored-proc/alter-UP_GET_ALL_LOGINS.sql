/****** Object:  StoredProcedure [UP_GET_ALL_LOGINS]    Script Date: 10/15/2015 09:43:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_ALL_LOGINS
-- ===========================  DESCRIPTION  ==================================
--
-- GETS THE NUMBER OF LOGINS FOR THE SPECIFIED PROGRAMS. FILTERS BASED ON SUBMITTAL DATE
-- DATE RANGE AND ALSO BY SPECIFIC USERS (AS GIVERS OR RECEIVERS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- YUKIOM        20150622                CREATED
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_ALL_LOGINS]
    @userIds NVARCHAR(MAX),
    @startDate DATETIME2,
    @endDate DATETIME2

AS
  BEGIN

    SET NOCOUNT ON

    --Set up pax IDs
    CREATE TABLE #PAX_ID_TABLE  (
      INPUT_PAX_ID NVARCHAR(MAX)
    )

    INSERT INTO #PAX_ID_TABLE (
      INPUT_PAX_ID
    )
      SELECT * FROM [component].UF_LIST_TO_TABLE(@userIds, ',')

    --Table for program stats (Result table)
    CREATE TABLE #LOGIN_COUNT (
      LOGIN_COUNT BIGINT
    )

    INSERT INTO #LOGIN_COUNT (
      LOGIN_COUNT
    )
      SELECT ISNULL(COUNT(login_history.LOGIN_HISTORY_ID), 0)
      FROM LOGIN_HISTORY login_history
        JOIN SYS_USER sys_user
          ON sys_user.SYS_USER_ID = login_history.SYS_USER_ID
      WHERE sys_user.PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
            AND login_history.LOGIN_DATE >= @startDate
            AND login_history.LOGIN_DATE <= @endDate

    -- Return the results (stored within the LOGIN_COUNT table)
    SELECT
      RESULT.LOGIN_COUNT
    FROM (SELECT TOP 1 LOGIN_COUNT from #LOGIN_COUNT) RESULT

    --Drop temp tables
    DROP TABLE #PAX_ID_TABLE
    DROP TABLE #LOGIN_COUNT

  END
GO

