/****** Object:  StoredProcedure [UP_BUDGET_TRANSACTIONAL_STATS]    Script Date: 10/15/2015 09:40:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_BUDGET_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS BUDGET STATS FOR REPORTING PURPOSES BASED ON TRANSACTIONAL DATA (tied to DISCRETIONARY, not based on giving data ie. nomination)
-- RETURNS THE FOLLOWING STATS...
-- BUDGET USED IN PERIOD
-- BUDGET TOTAL
-- BUDGET AVAILABLE
-- BUDGET EXPIRED
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HAGOPIWL        20150612                CREATED
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151116                UDM4 CHANGES
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_TRANSACTIONAL_STATS]
@delim_budget_id_list VARCHAR(MAX), 
@start_date DATETIME2(7),
@end_date DATETIME2(7)

AS
BEGIN
SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

CREATE TABLE #budgetIdParams (
ID BIGINT
)

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF BUDGET_ID(S) TO A LIST OF LONG(S)
INSERT INTO #budgetIdParams ([id]) SELECT token AS budget_id FROM UF_PARSE_STRING_TO_INTEGER (@delim_budget_id_list, @delimiter)

--CREATE TABLE TO RETURN VALUES
CREATE TABLE #budgetStats (
    BUDGET_USED_IN_PERIOD DECIMAL(10,2),
    BUDGET_USED_OUTSIDE_PERIOD DECIMAL(10,2),
    BUDGET_TOTAL DECIMAL(10,2),
    BUDGET_AVAILABLE DECIMAL(10,2),
    BUDGET_EXPIRED DECIMAL(10,2)
)

--Set budget used both in and out of period
INSERT INTO #budgetStats 
(BUDGET_USED_IN_PERIOD)
SELECT ISNULL(SUM(ISNULL(TOTAL_USED_IN_PERIOD,0)),0)
    FROM (
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    SELECT DISTINCT 
    (CASE
        WHEN DEBITS.DEBITS is null then 0
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS is null then 0
        ELSE CREDITS.CREDITS
    END)
    AS 'TOTAL_USED_IN_PERIOD'
    FROM DISCRETIONARY DISC         
    JOIN BUDGET b ON b.ID = DISC.BUDGET_ID_DEBIT OR b.ID = DISC.BUDGET_ID_CREDIT
    JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
    JOIN TRANSACTION_HEADER th
    on DISC.transaction_header_id = th.id and th.status_type_code = 'APPROVED'
    AND DISC.PERFORMANCE_DATE BETWEEN @start_date AND @end_date    
    LEFT JOIN 
        (--DEBITS IN PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM DISCRETIONARY disc_debit          
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE <> 'FUND'
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        WHERE disc_debit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_debit.BUDGET_ID_DEBIT
        ) DEBITS
        ON B.ID = DEBITS.ID OR B.PARENT_BUDGET_ID = DEBITS.ID            
    LEFT JOIN
        (--CREDITS IN PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM DISCRETIONARY disc_credit             
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE <> 'FUND'
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
        WHERE disc_credit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
        ) CREDITS
        ON B.ID = CREDITS.ID OR B.PARENT_BUDGET_ID = CREDITS.ID    
) TOTALS        

--------------------------------------------BEGIN BUDGET_USED_OUTSIDE_PERIOD-------------------------------------------

--Set budget used both in and out of period
DECLARE @total_used_outside_period DECIMAL(10,2)

select @total_used_outside_period = SUM(ISNULL(SUM_TOTAL_USED_OUTSIDE_PERIOD,0)) 
    FROM (
    SELECT  SUM(TOTAL_USED_OUTSIDE_PERIOD) SUM_TOTAL_USED_OUTSIDE_PERIOD
        FROM (    
        SELECT DISTINCT
        (CASE
            WHEN DEBITS_OUTSIDE_PERIOD.DEBITS is null then 0
            ELSE DEBITS_OUTSIDE_PERIOD.DEBITS
        END)
        -
        (CASE
            WHEN CREDITS_OUTSIDE_PERIOD.CREDITS is null then 0
            ELSE CREDITS_OUTSIDE_PERIOD.CREDITS
        END)
        AS 'TOTAL_USED_OUTSIDE_PERIOD'    
        FROM DISCRETIONARY DISC         
        JOIN BUDGET b ON b.ID = DISC.BUDGET_ID_DEBIT OR b.ID = DISC.BUDGET_ID_CREDIT
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        JOIN TRANSACTION_HEADER th
        on DISC.transaction_header_id = th.id and th.status_type_code = 'APPROVED'
        AND DISC.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date    
        LEFT JOIN 
            (--DEBITS OUTSIDE PERIOD
            SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
            FROM DISCRETIONARY disc_debit        
            JOIN TRANSACTION_HEADER th_debit 
                ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
                AND th_debit.TYPE_CODE = 'DISC' 
                AND th_debit.SUB_TYPE_CODE <> 'FUND'
                AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
            AND disc_debit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
            GROUP BY disc_debit.BUDGET_ID_DEBIT
            ) DEBITS_OUTSIDE_PERIOD
            ON B.ID = DEBITS_OUTSIDE_PERIOD.ID OR B.PARENT_BUDGET_ID = DEBITS_OUTSIDE_PERIOD.ID
        LEFT JOIN
            (--CREDITS OUTSIDE PERIOD
            SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
            FROM DISCRETIONARY disc_credit            
            JOIN TRANSACTION_HEADER th_credit 
                ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
                AND th_credit.TYPE_CODE = 'DISC' 
                AND th_credit.SUB_TYPE_CODE <> 'FUND'
                AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
            AND disc_credit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
            GROUP BY disc_credit.BUDGET_ID_CREDIT
            ) CREDITS_OUTSIDE_PERIOD
            ON B.ID = CREDITS_OUTSIDE_PERIOD.ID    OR B.PARENT_BUDGET_ID = CREDITS_OUTSIDE_PERIOD.ID
    ) TOTALS) summary

UPDATE #budgetStats set BUDGET_USED_OUTSIDE_PERIOD = @total_used_outside_period
--------------------------------------------END BUDGET_USED_OUTSIDE_PERIOD-------------------------------------------

--------------------------------------------BEGIN CALCULATE TOTALS------------------------------------------------
DECLARE @total_debits DECIMAL(10,2)
DECLARE @total_credits DECIMAL(10,2)

--DEBITS TOTAL
select @total_debits = SUM(ISNULL(DEBITS,0)) FROM (
    SELECT SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'                
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        GROUP BY b.ID ) TOTAL_DEBITS
        
--CREDITS TOTAL
select @total_credits = SUM(ISNULL(CREDITS,0)) FROM (
        SELECT b.ID, SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'        
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR b.PARENT_BUDGET_ID = bip.ID
        GROUP BY b.ID ) TOTAL_CREDITS 

-- Set the total on the budgetStats record
UPDATE #budgetStats
SET BUDGET_TOTAL = ISNULL(ISNULL(@total_credits,0)-ISNULL(@total_debits,0),0)
-----------------------------------------------------END CALCULATE TOTALS------------------------------------------------




-----------------------------------------------------BEGIN CALCULATE BUDGET AVAILABLE------------------------------------------------
DECLARE @budget_fund_debits DECIMAL(10,2)
DECLARE @budget_fund_credits DECIMAL(10,2)
DECLARE @budget_used_debits DECIMAL(10,2)
DECLARE @budget_used_credits DECIMAL(10,2)

--budgetFund DEBITS TOTAL (active on last day of date range)
select @budget_fund_debits = SUM(DEBITS) FROM (
        SELECT b.id, SUM(ISNULL(disc_debit.AMOUNT,0)) AS 'DEBITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID OR B.PARENT_BUDGET_ID = b.ID
            AND b.STATUS_TYPE_CODE = 'ACTIVE'
            AND (B.THRU_DATE > getdate() OR B.THRU_DATE IS NULL)     
        GROUP BY b.ID
        ) disc_TOTAL_DEBITS

--budgetFund CREDITS TOTAL (active on last day of date range)
select @budget_fund_credits = SUM(CREDITS) FROM (            
        SELECT b.id, SUM(ISNULL(disc_credit.AMOUNT,0)) AS 'CREDITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit 
            ON disc_credit.BUDGET_ID_CREDIT = b.ID            
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
            AND b.STATUS_TYPE_CODE = 'ACTIVE'
            AND (B.THRU_DATE >= getdate() OR B.THRU_DATE IS NULL)     
        GROUP BY b.ID
        ) disc_TOTAL_CREDITS

--budgetUsed DEBITS TOTAL
select @budget_used_debits = SUM(DEBITS) FROM (
        SELECT b.id, SUM(ISNULL(disc_debit.AMOUNT,0)) AS 'DEBITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE <> 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
            AND b.STATUS_TYPE_CODE = 'ACTIVE'
            AND (B.THRU_DATE >= getdate() OR B.THRU_DATE IS NULL)     
        GROUP BY b.ID
        ) disc_TOTAL_DEBITS_USED
        
--budgetUsed CREDITS TOTAL
select @budget_used_credits = SUM(CREDITS) FROM (
        SELECT b.ID, SUM(ISNULL(disc_credit.AMOUNT,0)) AS 'CREDITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE <> 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
            AND b.STATUS_TYPE_CODE = 'ACTIVE'
            AND (B.THRU_DATE >= getdate() OR B.THRU_DATE IS NULL)     
        GROUP BY b.ID
        ) disc_TOTAL_CREDITS_USED

-- Set the budget available on the budgetStats record
UPDATE #budgetStats
SET BUDGET_AVAILABLE = ISNULL(ISNULL(@budget_fund_credits,0) - ISNULL(@budget_fund_debits,0) - ISNULL(@budget_used_debits,0) - ISNULL(@budget_used_credits,0),0)
-----------------------------------------------------END CALCULATE BUDGET AVAILABLE------------------------------------------------



-----------------------------------------------------BEGIN CALCULATE BUDGET EXPIRED------------------------------------------------
DECLARE @expired_budget_fund_debits DECIMAL(10,2)
DECLARE @expired_budget_fund_credits DECIMAL(10,2)
DECLARE @expired_budget_used_debits DECIMAL(10,2)
DECLARE @expired_budget_used_credits DECIMAL(10,2)

--budgetFund DEBITS TOTAL
select @expired_budget_fund_debits = ISNULL(SUM(DEBITS),0) FROM (
        SELECT SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE = 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
        WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
        (b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
        GROUP BY b.ID 
        ) disc_TOTAL_DEBITS

--budgetFund CREDITS TOTAL
select @expired_budget_fund_credits = ISNULL(SUM(CREDITS),0) FROM (            
        SELECT SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE = 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
        WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
        (b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
        GROUP BY b.ID 
        ) disc_TOTAL_CREDITS

--budgetUsed DEBITS TOTAL
select @expired_budget_used_debits = ISNULL(SUM(DEBITS),0) FROM (
        SELECT SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_debit ON disc_debit.BUDGET_ID_DEBIT = b.ID
        JOIN TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND th_debit.TYPE_CODE = 'DISC' 
            AND th_debit.SUB_TYPE_CODE <> 'FUND' 
            AND th_debit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
        WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
        (b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
        ) disc_TOTAL_DEBITS_USED
        
--budgetUsed CREDITS TOTAL
select @expired_budget_used_credits = ISNULL(SUM(CREDITS),0) FROM (
        SELECT SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM BUDGET b 
        LEFT JOIN DISCRETIONARY disc_credit ON disc_credit.BUDGET_ID_CREDIT = b.ID
        JOIN TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND th_credit.TYPE_CODE = 'DISC' 
            AND th_credit.SUB_TYPE_CODE <> 'FUND' 
            AND th_credit.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #budgetIdParams bip ON bip.ID = b.ID  OR B.PARENT_BUDGET_ID = bip.ID
        WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
        (b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
        GROUP BY b.ID 
        ) disc_TOTAL_CREDITS_USED

-----------------------------------------------------END CALCULATE BUDGET EXPIRED------------------------------------------------

-- Set the budget expired on the budgetStats record
UPDATE #budgetStats
SET BUDGET_EXPIRED = ISNULL(ISNULL(@expired_budget_fund_credits,0) - ISNULL(@expired_budget_fund_debits,0) - ISNULL(@expired_budget_used_debits,0) - ISNULL(@expired_budget_used_credits,0),0)

SELECT * FROM #budgetStats 

DROP TABLE #budgetStats
END




GO

