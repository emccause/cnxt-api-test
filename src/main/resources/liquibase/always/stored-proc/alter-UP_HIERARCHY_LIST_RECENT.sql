/****** Object:  StoredProcedure [component].[UP_HIERARCHY_LIST_RECENT]    Script Date: 10/21/2020 11:08:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_HIERARCHY_LIST_RECENT
-- ===========================  DESCRIPTION  ==================================
-- TODO: REWRITE / DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        STORY	CHANGE DESCRIPTION
-- DOHOGNTA	20050720                CREATED
-- DOHOGNTA	20150309                CHANGE IDENTITIES TO BIGINT
-- burgestl	20201021		update join to sql 2017 
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_LIST_RECENT]
    @organization_code nvarchar(4)
,    @pax_id BIGINT
,    @show_children bit = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

declare @level_code int
declare @status varchar(8)
declare @pax_group_id BIGINT
declare @pax_group_index int

create table #pax_hierarchy (
    pax_id BIGINT
,    level_code int
,    pax_group_id BIGINT
,    owner_group_id BIGINT
,    role_code nvarchar(4)
,    entity_name varchar(100)
,    thru_date datetime2
,    status varchar(8)
,    role_desc varchar(255)
,    hierarchy_index int
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- ============================================================================
-- initialization
-- ============================================================================

set    @level_code = 0

insert into #pax_hierarchy
select    pax_group.pax_id
,    @level_code
,    pax_group.pax_group_id
,    owner_group_id
,    pax_group.role_code
,    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
,    thru_date
,    'active'
,    role_desc
,    null
from    pax_group
inner join    pax on pax_group.pax_id = pax.pax_id
inner join    role on pax_group.role_code = role.role_code
where organization_code = @organization_code
and    pax_group.pax_id = @pax_id
and    getdate() between from_date and isnull(thru_date, getdate())

if    @@rowcount = 0
begin
    set @status = 'inactive'

    insert into #pax_hierarchy
    select    pax_group.pax_id
    ,    @level_code
    ,    pax_group.pax_group_id
    ,    owner_group_id
    ,    pax_group.role_code
    ,    case
            when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
            else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
        end entity_name
    ,    thru_date
    ,    'inactive'
    ,    role_desc
    ,    null
    from    pax_group
    inner join pax on pax_group.pax_id = pax.pax_id
    inner join role on pax_group.role_code = role.role_code
    where organization_code = @organization_code
    and    pax_group.pax_id = @pax_id
    and    getdate() >= from_date
    and    thru_date = 
        (
            select    max(thru_date)
            from    pax_group 
            where    pax_group.pax_id = @pax_id
            and    organization_code = @organization_code
        )
end
else
begin
    set @status = 'active'
end

declare c_pax_group cursor
read_only
for
select distinct owner_group_id
from    pax_group
where    organization_code = @organization_code
and    pax_group.pax_id = @pax_id
and    getdate() between from_date and isnull(thru_date, getdate())

open c_pax_group
fetch next from c_pax_group into @pax_group_id

set @pax_group_index = 0
while @@fetch_status = 0 begin
    set    @pax_group_index = @pax_group_index + 1
    
    update    #pax_hierarchy
    set    hierarchy_index = @pax_group_index
    where    owner_group_id = @pax_group_id

    while @pax_group_id is not null 
    begin
        set @level_code = @level_code + 1
    
        insert into #pax_hierarchy
        select    pax_group.pax_id
        ,    @level_code
        ,    pax_group.pax_group_id
        ,    owner_group_id
        ,    pax_group.role_code
        ,    case
                when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
                else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
            end entity_name
        ,    thru_date
        ,    'active'
        ,    '' as role_desc
        ,    @pax_group_index
        from    pax_group
        inner join pax on pax_group.pax_id = pax.pax_id
        where pax_group.pax_group_id = @pax_group_id

        update    #pax_hierarchy 
        set    status = 'inactive' 
        where    pax_group_id = @pax_group_id
        and    thru_date is not null

        select    @pax_group_id = owner_group_id
        from    pax_group
        where    pax_group_id = @pax_group_id
    end

    fetch next from c_pax_group into @pax_group_id
end
close c_pax_group
deallocate c_pax_group

select    @pax_id as pax_id
,    @organization_code as organization_code
,    pax_group_id
,    @status as status
,    role_desc
from    #pax_hierarchy
where    level_code = 0

select    *
from    #pax_hierarchy
order by
    level_code desc

if @show_children = 1 and @status = 'active' 
begin
    select    pax_group.pax_id
    ,    pax_group.pax_group_id
    ,    pax_group.role_code
    ,    case
            when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
            else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
        end entity_name
    ,    role.role_desc
    ,    'active' as status
    ,    hierarchy_index
    from    pax_group
    inner join pax on pax_group.pax_id = pax.pax_id
    inner join #pax_hierarchy on pax_group.owner_group_id = #pax_hierarchy.pax_group_id
    inner join role on pax_group.role_code = role.role_code
    where #pax_hierarchy.level_code = 0
    and    getdate() between from_date and isnull(pax_group.thru_date, getdate())
    order by
        hierarchy_index
    ,    entity_name
    ,    pax_group.role_code
end

end

GO
