/****** Object:  StoredProcedure [component].[UP_SPIN_PLAY]    Script Date: 10/16/2020 12:47:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_SPIN_PLAY
-- ===========================  DESCRIPTION  ==================================
-- Claims the next available spin award using the next available token
--
-- ============================  REVISIONS  ===================================
-- AUTHOR	DATE		CHANGE DESCRIPTION
-- KNIGHTGA     20120110    	Created
-- KNIGHTGA     20140410    	Removed table name from award query
-- KNIGHTGA     20150818    	Updated for UDM 3
-- burgestl		20201103		Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_SPIN_PLAY] (@programId bigint, @paxId bigint, @mode nvarchar(50))
AS
BEGIN
SET NOCOUNT ON

/*
begin transaction

exec component.UP_SPIN_PLAY 50, 1, 'SINGLE'

rollback
commit

declare @programId int, @paxGroupId int, @mode nvarchar(50)
set @programId = 50
set @paxGroupId = 1
set @mode = 'SINGLE'
begin transaction
*/

--===============================================================================
--STEP 1: Find a token transaction for the given program and pax
--===============================================================================

declare @paxGroupId bigint, @tokenTxId bigint, @tokenType nvarchar(50)

select top 1 @tokenTxId = th.[id]
      ,@paxGroupId = th.pax_group_id
      ,@tokenType = th.sub_type_code
  from component.transaction_header th
       inner join component.discretionary disc 
               on disc.transaction_header_id = th.id
              and disc.amount > 0 
       inner join component.pax_group pg 
               on pg.pax_group_id = th.pax_group_id 
              and pg.pax_id = @paxId
       inner join component.vw_spin_pax_tokens spt
               on spt.program_id = th.program_id 
              and spt.pax_id = pg.pax_id 
              and isnull(spt.tokens_available,0) > 0 
 where th.type_code = 'DISC' 
   and (
         (th.sub_type_code = 'SPIN_TOKEN' and th.STATUS_TYPE_CODE = 'APPROVED')
          or
         (th.sub_type_code = 'SPIN_TOKEN_AWARD' and th.status_type_code = 'HOLD')
       )
   and th.program_id = @programId
   and th.transaction_claim_id is null

--===============================================================================
--STEP 2: Create a transaction claim record to link the token to the award
--===============================================================================

declare @transactionClaimId bigint

if isnull(@tokenTxId,-1) != -1
begin

insert into component.transaction_claim (pax_group_id, status_type_code, status_change_detail)
values (@paxGroupId, 'APPROVED', @mode)
select @transactionClaimId = @@identity

end


--===============================================================================
--STEP 3: Update the token with the claim id
--===============================================================================

if isnull(@transactionClaimId,-1) != -1
begin

update component.transaction_header
   set transaction_claim_id = @transactionClaimId
 where id = @tokenTxId

end


--===============================================================================
--STEP 4: Update the next available award with the claim id and pax_group_id
--        NOTE: This only applies to SPIN_TOKENs
--===============================================================================

declare @awardTxId bigint

if isnull(@transactionClaimId,-1) != -1 and @tokenType = 'SPIN_TOKEN'
begin

update component.transaction_header
   set transaction_claim_id = @transactionClaimId
      ,pax_group_id = @paxGroupId
      ,status_type_code = 'APPROVED'
      ,activity_date = getdate()
      ,@awardTxId = id
 where id in (
                select top 1 th.[id] as TRANSACTION_HEADER_ID 
                  from component.transaction_header th 
                       inner join component.discretionary disc 
                               on disc.transaction_header_id = th.id
                       inner join component.vw_spin_award_pool sap
                              on sap.program_id = th.program_id 
                             and isnull(sap.awards_available, 0) > 0 
                 where th.type_code = 'DISC' 
                   and th.sub_type_code = 'SPIN_AWARD' 
                   and th.status_type_code = 'HOLD' 
                   and th.program_id = @programId
                   and th.transaction_claim_id is null 
                 order by th.performance_metric
             )

--===============================================================================
--STEP 5: Update the claimed award with data from the token
--===============================================================================

if isnull(@awardTxId,-1) != -1
begin

update award 
   set award.performance_date = getdate()
      ,award.description = token.description 
  from component.discretionary award
  INNER JOIN discretionary token ON award.transaction_header_id = @awardTxId
   where token.transaction_header_id = @tokenTxId

end

end

else if isnull(@tokenTxId,-1) != -1 and @tokenType = 'SPIN_TOKEN_AWARD'
begin

--for SPIN_TOKEN_AWARD, the award transaction is the same as the token transaction
set @awardTxId = @tokenTxId

end



--===============================================================================
--STEP 6: Select data about the claimed award as the result set
--===============================================================================

if isnull(@tokenTxId,-1) != -1 and isnull(@awardTxId,-1) != -1
begin

select @transactionClaimId as CLAIM_ID
      ,disc.amount as DENOMINATION
      ,null as ERROR_MESSAGE
  from discretionary disc
 where disc.transaction_header_id = @awardTxId

end
else 
begin

select null as CLAIM_ID
      ,null as DENOMINATION
      ,case when isnull(@tokenTxId,-1) = -1 then 'NO_TOKENS' 
            when isnull(@awardTxId,-1) = -1 then 'NO_AWARDS'
            else null
       end as ERROR_MESSAGE

end


/*
rollback
commit
*/


END

GO
