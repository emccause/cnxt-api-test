--==============================  NAME  ======================================
-- UF_CHECK_DATE
-- ===========================  DESCRIPTION  ==================================
--    AUTHOR        DATE        STORY        DESCRIPTION
--    GARCIAF2    20160804    MP-5719        Update Enrollment Validation to be less Explicit
--
-- ============================================================================
-- RETURN DATA
-- PARAMETERS:
--  @in_date
-- NORMAL DATE CASES:
-- SELECT [component].[UF_CHECK_DATE] ('2016-07-01') AS DATE_CASE_1 --RESULT: 2016-07-01
-- SELECT [component].[UF_CHECK_DATE] ('2016/7/2') AS DATE_CASE_2 --RESULT: 2016-07-02
-- SELECT [component].[UF_CHECK_DATE] ('2016.7.3') AS DATE_CASE_3 --RESULT: 2016-07-03
-- SELECT [component].[UF_CHECK_DATE] ('2016.7.4') AS DATE_CASE_4   --RESULT: 2016-07-04
-- SELECT [component].[UF_CHECK_DATE] ('2016-7-5') AS DATE_CASE_5   --RESULT: 2016-07-05
-- SELECT [component].[UF_CHECK_DATE] ('2016/07/01') AS DATE_CASE_6   --RESULT: 2016-07-01
-- SELECT [component].[UF_CHECK_DATE] ('2016/17/01') AS DATE_CASE_7  --RESULT: NULL
-- SELECT [component].[UF_CHECK_DATE] ('2016/07/41') AS DATE_CASE_8  --RESULT: NULL
-- BIRTH DATE CASES:
-- SELECT [component].[UF_CHECK_DATE] ('2-3') AS TEST_CASE_1 --02-03
-- SELECT [component].[UF_CHECK_DATE] ('03/03') AS TEST_CASE_2 --03-03
-- SELECT [component].[UF_CHECK_DATE] ('04.03') AS TEST_CASE_3 --04-03
-- SELECT [component].[UF_CHECK_DATE] ('13.03') AS TEST_CASE_4 --NULL
-- SELECT [component].[UF_CHECK_DATE] ('02-50') AS TEST_CASE_5 --NULL
-- ============================  REVISIONS  =============================

ALTER FUNCTION [component].[UF_CHECK_DATE]
(@in_date nvarchar(50))
RETURNS nvarchar(10)
AS
BEGIN
        declare @Return nvarchar(50)
        DECLARE @len_birthDate INT
        SET @len_birthDate = 5

        SET @in_date = REPLACE(REPLACE (@in_date,'.','-'),'/','-')
                
        if (len(@in_date) >5) 
        BEGIN --NORMAL DATE

            select @return = CASE WHEN ISDATE(@in_date) = 1
            THEN CASE WHEN CAST(@in_date as DATE) BETWEEN '1/1/1900' AND '1/1/2099'
                    THEN CONVERT(VARCHAR(10),CAST(@in_date AS DATE),120)
                    ELSE null
                    END
            ELSE null
            END
        END
        ELSE
        BEGIN --BIRTHDAY_TYPE
            DECLARE @new_birth_date nvarchar(10)
            SET @new_birth_date= ('2000-'+@in_date)
    
            SELECT
                 @return = CASE WHEN ISDATE(@new_birth_date) = 1
                    THEN CASE WHEN CAST(@new_birth_date as DATE) BETWEEN '1/1/1999' AND '1/1/2001'
                    THEN SUBSTRING(CONVERT(VARCHAR(10),CAST(@new_birth_date AS DATE),120),6,10)
                    ELSE null
                    END
                ELSE null
            END
        END
        return @Return
END