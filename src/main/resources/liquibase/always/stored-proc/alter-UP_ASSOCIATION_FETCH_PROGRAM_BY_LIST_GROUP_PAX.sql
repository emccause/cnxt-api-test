/****** Object:  StoredProcedure [component].[UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX]    Script Date: 10/15/2015 09:36:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S) AND/OR A DELIMITED LIST OF PAX_ID(S)
-- RETURN A DISTINCT LIST OF PAX, PROGRAM (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        21051113                UDM4 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX]
    @delim_list_group VARCHAR(4000)
,    @delim_list_pax VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @wrk TABLE (pax_id BIGINT, program_id BIGINT)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- FETCH ID(S)
INSERT INTO @wrk (pax_id, program_id) SELECT pax_id, program_id FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX (@delim_list_group, @delim_list_pax)

-- GET DISTINCT LIST
SELECT DISTINCT
    PAX.PAX_ID
,    PAX.LANGUAGE_CODE
,    PAX.PREFERRED_LOCALE
,    PAX.CONTROL_NUM
,    PAX.SSN
,    PAX.FIRST_NAME
,    PAX.MIDDLE_NAME
,    PAX.LAST_NAME
,    PAX.NAME_PREFIX
,    PAX.NAME_SUFFIX
,    PAX.COMPANY_NAME
,    PAX.TAX_NUM
,    PAX.ALTERNATE_NAME
,    PAX.[HASH]
,    PROGRAM.PROGRAM_ID
,    PROGRAM.PROGRAM_ID
,    PROGRAM.PROGRAM_NAME
,    PROGRAM.PROGRAM_DESC
FROM    @wrk AS WRK
INNER JOIN PAX
    ON WRK.pax_id = PAX.PAX_ID
INNER JOIN PAX_GROUP
    ON PAX.PAX_ID = PAX_GROUP.PAX_ID
    AND GETDATE() BETWEEN PAX_GROUP.FROM_DATE AND ISNULL(PAX_GROUP.THRU_DATE, GETDATE())
LEFT OUTER JOIN PROGRAM
    ON WRK.program_id = PROGRAM.PROGRAM_ID
    AND GETDATE() BETWEEN PROGRAM.FROM_DATE AND ISNULL(PROGRAM.THRU_DATE, GETDATE())
ORDER BY
    PAX.PAX_ID
,    PROGRAM.PROGRAM_NAME

END

GO

