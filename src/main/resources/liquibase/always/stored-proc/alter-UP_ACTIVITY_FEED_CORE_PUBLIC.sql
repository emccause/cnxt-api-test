
GO
/****** Object:  StoredProcedure [component].[UP_ACTIVITY_FEED_CORE_PUBLIC]    Script Date: 3/10/2020 2:37:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_ACTIVITY_FEED_CORE_PUBLIC
-- ===========================  DESCRIPTION  ==================================
-- FETCHES A PAGE OF DATA FOR THE LOGGED IN PAX FROM ALL ITEMS THAT THE LOGGED IN PAX CAN SEE (NO AUDIENCE RESTRICTION)
-- TAKES VISIBILITY INTO ACCOUNT
-- PAGINATION IS BASED ON CREATE DATE OF THE NEWSFEED_ITEM ENTRY (CURRENTLY CREATED AT TIME OF NOMINATION CREATION)
--
-- WILL LIMIT BASED ON NEWSFEED_ITEM_ID OR NOMINATION_ID_LIST IF PROVIDED
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20160830    MP-8064        CREATED
-- MUDDAM        20160916    MP-8275        RE-ADDED SUPPORT FOR NEWSFEED_VISIBILITY
-- MUDDAM        20161005    MP-8065        UPDATED FOR SUPPORT FOR NOTIFICATIONS (MAINLY FOR PENDING APPROVALS)
-- MUDDAM        20161215    MP-9134        INCLUDE DEFAULT_PAX_RECEIVED AS AN OPTION IN APPROVAL BYPASS
-- GARCIAF2      20170209    MP-9512        OVERRIDE VISIBILITY WHEN NEWSFEED_VISIBILITY = NONE AND RECEIVER IS LOGGED.
-- HARWELLM      20170502    MP-9904        NOTIFY_OTHERS SHOULD GET SUBMITTER VIEW OF ACTIVITY FEED TILE
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- MUDDAM        20180531    MP-12228       QUERY OPTIMIZATIONS
-- DAYSD01       20200510					QUERY OPTIMIZATION REDUCE NUMBER OF ROWS OPERATED ON CNXT-175
-- BURGESTL		 20201117	 CNXT-335		Add Database Logging for Stored Procs
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ACTIVITY_FEED_CORE_PUBLIC]
@LOGGED_IN_PAX_ID BIGINT,
@ACTIVITY_TYPE_LIST NVARCHAR(MAX),
@PAGE_NUMBER BIGINT,
@PAGE_SIZE BIGINT,
@NEWSFEED_ITEM_ID BIGINT,
@IGNORE_STATUS BIT,
@OVERRIDE_VISIBILITY BIT

AS
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN
--(+)Logging
BEGIN TRY
declare @loggingStartDate  DATETIME2(7),
		@loggingEndDate  DATETIME2(7),
		@loggingParameters NVARCHAR(MAX),
		@loggingSprocName nvarchar(150),
		@loggingDebug int


--insert component.application_data (key_name, value)
--select 'sql.logging','true'
select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

if (@loggingDebug=1)
begin
set @loggingSprocName = 'UP_ACTIVITY_FEED_CORE_PUBLIC'
set @loggingStartDate = getdate()
set @loggingParameters = '@LOGGED_IN_PAX_ID=' + isnull(cast(@LOGGED_IN_PAX_ID as varchar),'null') --BIGINT
					+ ' :: @ACTIVITY_TYPE_LIST=' +isnull(@ACTIVITY_TYPE_LIST, 'null') -- NVARCHAR(MAX)
					+ ' :: @PAGE_NUMBER=' + isnull(cast(@PAGE_NUMBER as varchar),'null') --BIGINT
					+ ' :: @PAGE_SIZE=' + isnull(cast(@PAGE_SIZE as varchar),'null') --BIGINT
					+ ' :: @NEWSFEED_ITEM_ID=' + isnull(cast(@NEWSFEED_ITEM_ID as varchar),'null') --BIGINT
					+ ' :: @IGNORE_STATUS=' + isnull(cast(@IGNORE_STATUS as varchar),'null') --BIT
					+ ' :: @OVERRIDE_VISIBILITY=' + isnull(cast(@OVERRIDE_VISIBILITY as varchar),'null') --BIT
end
--(-)Logging

DECLARE @SINCE_DATE DATE, @ROWS_AMOUNT INT, @sys_user_name varchar(55), @NEWSFEED_ITEM_ID_LIST NVARCHAR(MAX)  --
SET @ROWS_AMOUNT = 100
SELECT @since_Date = getdate() - 360

	IF @NEWSFEED_ITEM_ID IS NULL
		BEGIN
			 SELECT  @NEWSFEED_ITEM_ID_LIST = STUFF(((
						 (SELECT top (@ROWS_AMOUNT)  ',' + cast(id as varchar(9)) FROM [component].[NEWSFEED_ITEM]
						   WHERE FROM_PAX_ID is not null and UPDATE_DATE  between @SINCE_DATE and getdate() order by UPDATE_DATE desc
									 FOR XML PATH('')) )), 1, 1, '')
		Select @NEWSFEED_ITEM_ID = 0;
		END


if object_id('tempdb..#A') is not null   drop table #A
        SELECT
            NOMINATION_ID,
            NEWSFEED_ITEM_ID,
            CREATE_DATE,
            NOTIFY_OTHER_PAX_ID

                     ,GIVE_ANONYMOUS
                     ,SUBMITTER_PAX_ID SUBMITTER_PAX_ID_possible
                     , @IGNORE_STATUS IGNORE_STATUS
                     ,NEW_APPROVALS_RECEIVER_PAX_ID
                     ,OLD_APPROVALS_RECEIVER_PAX_ID
                     ,DEFAULT_PAX_RECEIVED
                     ,NEWSFEED_VISIBILITY
                     , @OVERRIDE_VISIBILITY OVERRIDE_VISIBILITY
                     , LOGGED_IN_PAX_RECEIVED
                     ,DIRECT_REPORT_PAX_RECEIVED
                     INTO #A
        FROM (
            SELECT
                NOM.ID AS NOMINATION_ID,
                NFI.ID AS NEWSFEED_ITEM_ID,
                NFI.CREATE_DATE,
                NFI.NEWSFEED_VISIBILITY,
                DIRECT_REPORT_RECEIVED.RECEIVER_PAX_ID AS DIRECT_REPORT_PAX_RECEIVED,
                NEW_APPROVALS_DATA.RECEIVER_PAX_ID AS NEW_APPROVALS_RECEIVER_PAX_ID,
                OLD_APPROVALS_DATA.RECEIVER_PAX_ID AS OLD_APPROVALS_RECEIVER_PAX_ID,
                ( -- DEFAULT_PAX_RECEIVED
                    SELECT TOP 1
                        RECG.RECEIVER_PAX_ID
                    FROM component.RECOGNITION RECG
                    LEFT JOIN component.NOMINATION SUB_NOM
                        ON RECG.NOMINATION_ID = SUB_NOM.ID
                    LEFT JOIN component.AUXILIARY_NOTIFICATION NOTIFY_OTHER
                        ON NOTIFY_OTHER.NOMINATION_ID = RECG.NOMINATION_ID
                        AND NOTIFY_OTHER.PAX_ID = @LOGGED_IN_PAX_ID
                    LEFT JOIN component.PAX_MISC SHARE_PREF
                        ON RECG.RECEIVER_PAX_ID = SHARE_PREF.PAX_ID
                        AND SHARE_PREF.VF_NAME = 'SHARE_REC'
                        AND SHARE_PREF.MISC_DATA = 'TRUE'
                    WHERE SUB_NOM.ID = NOM.ID
                        AND ( -- BYPASS FOR APPROVER VIEW
                            @IGNORE_STATUS = 1
                            OR RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                        )
                        AND (
                            NFI.NEWSFEED_VISIBILITY = 'PUBLIC' -- IF NEWSFEED_VISIBILITY IS PUBLIC, YOU CAN SEE EVERYONE
                            OR SUB_NOM.SUBMITTER_PAX_ID = @LOGGED_IN_PAX_ID -- SUBMITTER CAN SEE UNLESS NEWSFEED_VISIBILITY = NONE
                            OR NOTIFY_OTHER.PAX_ID = @LOGGED_IN_PAX_ID -- NOTIFY_OTHER CAN SEE UNLESS NEWSFEED_VISIBILITY = NONE
                            OR ( -- IF NEWSFEED_VISIBILITY IS RECIPIENT_PREF, FIND SOMEONE WHO WANTS TO SHARE REC PUBLICLY
                                NFI.NEWSFEED_VISIBILITY = 'RECIPIENT_PREF'
                                AND SHARE_PREF.MISC_DATA = 'TRUE'
                            )
                        )
                ) AS DEFAULT_PAX_RECEIVED,
                NOM.SUBMITTER_PAX_ID AS SUBMITTER_PAX_ID,
                ( -- GIVE_ANONYMOUS
                    SELECT TOP 1
                        RECG_MISC.MISC_DATA
                    FROM component.RECOGNITION RECG
                    LEFT JOIN component.RECOGNITION_MISC RECG_MISC
                        ON RECG_MISC.RECOGNITION_ID = RECG.ID
                        AND RECG_MISC.VF_NAME = 'GIVE_ANONYMOUS'
                    WHERE RECG.NOMINATION_ID = NOM.ID
                        AND RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                ) AS GIVE_ANONYMOUS,
                ( -- LOGGED_IN_PAX_RECEIVED
                    SELECT TOP 1
                        RECEIVER_PAX_ID
                    FROM component.RECOGNITION
                    WHERE NOMINATION_ID = NOM.ID
                        AND RECEIVER_PAX_ID = @LOGGED_IN_PAX_ID
                        AND STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                ) AS LOGGED_IN_PAX_RECEIVED,
                NOTIFY_OTHER_PAX.PAX_ID AS NOTIFY_OTHER_PAX_ID
            FROM component.NEWSFEED_ITEM NFI
            LEFT JOIN component.NOMINATION NOM
                ON NFI.TARGET_ID = NOM.ID
                AND NFI.TARGET_TABLE = 'NOMINATION'
            LEFT JOIN ( -- DIRECT_REPORT_RECEIVED
                SELECT
                    RECG.NOMINATION_ID,
                    MIN(RECG.RECEIVER_PAX_ID) AS RECEIVER_PAX_ID -- ID OF A DIRECT REPORT WHO RECEIVED RECG FOR THIS NOM
                FROM component.RECOGNITION RECG
                INNER JOIN component.RELATIONSHIP DIRECT_REPORTS
                    ON RECG.RECEIVER_PAX_ID = DIRECT_REPORTS.PAX_ID_1
                    AND DIRECT_REPORTS.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
                    AND DIRECT_REPORTS.PAX_ID_2 = @LOGGED_IN_PAX_ID
                LEFT JOIN component.RECOGNITION RAISED_RECG
                    ON RAISED_RECG.PARENT_ID = RECG.ID
                    AND RAISED_RECG.RECEIVER_PAX_ID = DIRECT_REPORTS.PAX_ID_1
                    AND RAISED_RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                LEFT JOIN component.NOMINATION RAISED_NOM
                    ON RAISED_RECG.NOMINATION_ID = RAISED_NOM.ID
                WHERE RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                    AND RECG.PARENT_ID IS NULL
                GROUP BY RECG.NOMINATION_ID
            ) DIRECT_REPORT_RECEIVED
                ON NOM.ID = DIRECT_REPORT_RECEIVED.NOMINATION_ID
            LEFT JOIN ( -- NEW_APPROVALS_DATA
                SELECT
                    RECG.NOMINATION_ID,
                    MIN(RECG.RECEIVER_PAX_ID) AS RECEIVER_PAX_ID
                FROM component.RECOGNITION RECG
                LEFT JOIN component.APPROVAL_PENDING APPROVALS
                    ON RECG.ID = APPROVALS.TARGET_ID
                    AND APPROVALS.TARGET_TABLE = 'RECOGNITION'
                WHERE RECG.PARENT_ID IS NULL
                    AND APPROVALS.PAX_ID = @LOGGED_IN_PAX_ID
                GROUP BY RECG.NOMINATION_ID
            ) NEW_APPROVALS_DATA
                ON NOM.ID = NEW_APPROVALS_DATA.NOMINATION_ID
            LEFT JOIN ( -- OLD_APPROVALS_DATA
                SELECT
                    RECG.NOMINATION_ID,
                    MIN(RECG.RECEIVER_PAX_ID) AS RECEIVER_PAX_ID
                FROM component.RECOGNITION RECG
                LEFT JOIN component.APPROVAL_HISTORY APPROVALS
                    ON RECG.ID = APPROVALS.TARGET_ID
                    AND APPROVALS.TARGET_TABLE = 'RECOGNITION'
                WHERE RECG.PARENT_ID IS NULL
                    AND APPROVALS.PAX_ID = @LOGGED_IN_PAX_ID
                GROUP BY RECG.NOMINATION_ID
            ) OLD_APPROVALS_DATA
                ON NOM.ID = OLD_APPROVALS_DATA.NOMINATION_ID
            LEFT JOIN ( -- NOTIFY_OTHERS
                SELECT
                    NOMINATION_ID,
                    PAX_ID
                FROM component.AUXILIARY_NOTIFICATION
                WHERE PAX_ID = @LOGGED_IN_PAX_ID
            ) NOTIFY_OTHER_PAX
                ON NOM.ID = NOTIFY_OTHER_PAX.NOMINATION_ID
            WHERE NFI.NEWSFEED_ITEM_TYPE_CODE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@ACTIVITY_TYPE_LIST, ','))
                AND ( -- BYPASS FOR APPROVER VIEW
                    @IGNORE_STATUS = 1
                    OR NFI.STATUS_TYPE_CODE = 'APPROVED'
                )
    --            AND (@NEWSFEED_ITEM_ID IS NULL OR NFI.ID = @NEWSFEED_ITEM_ID) --DD 05.12.2020 removed

        AND (NFI.ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@NEWSFEED_ITEM_ID_LIST, ','))--dd
        OR NFI.ID = @NEWSFEED_ITEM_ID)

        ) RAW_DATA
SELECT
COUNT(*) OVER() AS TOTAL_RESULTS,
NOMINATION_ID, NEWSFEED_ITEM_ID, CREATE_DATE, SUBMITTER_PAX_ID, RECIPIENT_PAX_ID, NOTIFY_OTHER_PAX_ID
FROM (
       SELECT
               CASE
                -- THERE SHOULD ALWAYS BE A DEFAULT SUBMITTER, SO NO NEED TO NULL CHECK
            WHEN GIVE_ANONYMOUS = 'TRUE'
                    THEN 0
                ELSE SUBMITTER_PAX_ID_possible
            END AS SUBMITTER_PAX_ID,
            CASE
                WHEN IGNORE_STATUS = 1 -- APPROVALS VIEW BYPASS
                    THEN CASE
                        WHEN NEW_APPROVALS_RECEIVER_PAX_ID IS NOT NULL
                            THEN NEW_APPROVALS_RECEIVER_PAX_ID
                        WHEN OLD_APPROVALS_RECEIVER_PAX_ID IS NOT NULL
                            THEN OLD_APPROVALS_RECEIVER_PAX_ID
                        ELSE DEFAULT_PAX_RECEIVED
                    END
                WHEN NEWSFEED_VISIBILITY = 'NONE'
                    THEN CASE
                    WHEN OVERRIDE_VISIBILITY = 1 AND LOGGED_IN_PAX_RECEIVED IS NOT NULL
                    THEN LOGGED_IN_PAX_RECEIVED
                    ELSE NULL
                END
                WHEN LOGGED_IN_PAX_RECEIVED IS NOT NULL -- LOGGED IN PAX HAS PRIORITY
                    THEN LOGGED_IN_PAX_RECEIVED
                WHEN DIRECT_REPORT_PAX_RECEIVED IS NOT NULL
                    THEN DIRECT_REPORT_PAX_RECEIVED
                ELSE DEFAULT_PAX_RECEIVED
            END AS RECIPIENT_PAX_ID,
       *
       FROM #A RAW_DATA
) DATA
WHERE (DATA.SUBMITTER_PAX_ID IS NOT NULL AND DATA.RECIPIENT_PAX_ID IS NOT NULL
       ) OR DATA.NOTIFY_OTHER_PAX_ID IS NOT NULL
       ORDER BY DATA.CREATE_DATE DESC
OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS FETCH NEXT @PAGE_SIZE ROWS ONLY

--(+)Logging
if (@loggingDebug=1)
begin
	set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error  
	  --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY  
BEGIN CATCH
	if (@loggingDebug=1)
	begin
		declare @errorMessage nvarchar(250) 
		SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE() 
		exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
	end
END CATCH
--(-)Logging

END;
