/****** Object:  StoredProcedure [component].[UP_RECOGNITION_RECEIVED_REPORT]    Script Date: 10/15/2015 11:09:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_RECEIVED_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS RECEIVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- THOMSOND        20080819                CREATED
-- THOMSOND        20080829                CHANGE - Added is_recognition_comment
-- SCHIERVE        20081104                CHANGE - Separated into BASE/REPORT/CSV
-- BESSBS        20090401                CHANGE - DATATYPE OF recognition_date FROM VARCHAR(10) TO DATETIME
-- BESSBS        20091118                CHANGE - DATATYPE OF recognition_date FROM VARCHAR(10) TO DATETIME
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151113                UDM4 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_RECEIVED_REPORT]
    @pax_group_id BIGINT
,    @submittal_from_date DATETIME2
,    @submittal_thru_date DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

CREATE TABLE #recognition (
    nomination_id BIGINT NOT NULL

,    submitter_list VARCHAR(1000) NULL
,    submitted_by_name VARCHAR(80) NULL

,    ecard_id BIGINT NULL

,    recognition_id    BIGINT NULL
,    recognition_receiver_pax_id BIGINT NULL
,    recognition_amount INT NULL
,    is_recognition_comment INT NULL
,    recognition_status VARCHAR(10) NULL
,    recognition_date DATETIME2 NULL
,    recognition_criteria NVARCHAR(1024) NULL

,    approval_process_id BIGINT NULL

,    program_id BIGINT NULL
,    program_name NVARCHAR(40) NULL

,    award_type NVARCHAR(40) NULL
,    share VARCHAR (10) NULL
,    budget_id BIGINT NULL
)

CREATE TABLE #award_types (
    nomination_id BIGINT NULL
,    award_type NVARCHAR(40) NULL
)

EXECUTE component.UP_RECOGNITION_RECEIVED_BASE @pax_group_id, @submittal_from_date, @submittal_thru_date, @locale_code

SELECT 
    RECOGNITION_DATE
,    CASE
        WHEN SUBMITTER_LIST LIKE '%,%' THEN 'M'
        ELSE SUBMITTED_BY_NAME
    END AS SUBMITTER
,    CASE
        WHEN ECARD_ID IS NOT NULL THEN 'E'
        WHEN IS_RECOGNITION_COMMENT = 1 THEN 'C'
        ELSE NULL
    END AS COMMENTS
,    component.UF_TRANSLATE (@locale_code, PROGRAM_NAME, 'PROGRAM', 'PROGRAM_NAME') AS RECOGNITION_PROGRAM
,    RECOGNITION_CRITERIA
,    CASE
            WHEN recognition_amount >0 THEN recognition_amount
            ELSE  null
    END  AS AWARD_AMOUNT
,    CASE
        WHEN award_type = 'Points'
        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'REPORTING', 'REPORTING')
        ELSE component.UF_TRANSLATE (@locale_code, award_type, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC')
    END AS AWARD_TYPE
,    NOMINATION_ID AS TRACKING_NUMBER
,    SHARE
,    ECARD_ID
,    RECOGNITION_ID
,    RECOGNITION_RECEIVER_PAX_ID
,    budget_id
FROM    #recognition
ORDER BY
    RECOGNITION_DATE DESC

END

GO

