/****** Object:  StoredProcedure [component].[UP_RECOGNITION_CRITERIA_REPORT_SUM]    Script Date: 10/15/2015 11:02:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_CRITERIA_REPORT_SUM
-- ===========================  DESCRIPTION  ==================================
-- RETURN THE SUM OF INDIVIDUALS, CRITERIA AND RECOGNITION
--
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- PALD00        20081020                CREATED
-- SCHIERVR        20081030                MADE MORE DYNAMIC TO ASSIST IN CSV CREATION.
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_CRITERIA_REPORT_SUM]
    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,     @recognition_selection_criteria NVARCHAR(4)
AS
BEGIN

SET NOCOUNT ON

--DRIVER
CREATE TABLE #recognition_criteria (
    nomination_id BIGINT NOT NULL
,    recognition_id BIGINT NOT NULL
,    submitter_pax_id BIGINT NULL
,    receiver_pax_id BIGINT NULL
,    recogniton_criteria_id BIGINT
,    recogniton_criteria_name NVARCHAR(200)
,    recognition_amount INT
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_CRITERIA_BASE @submittal_date_from, @submittal_date_thru, @pax_group_id, @recognition_selection_criteria

SELECT
    COUNT(DISTINCT recognition_id) AS 'Sum_Recognitions'
,    COUNT(DISTINCT receiver_pax_id) AS 'Sum_Receivers'
,    SUM(recognition_amount) AS 'Sum_Awards'
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria

END

GO

