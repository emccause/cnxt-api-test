-- ==============================  NAME  ===============================================
-- UP_BULK_UPLOAD_ADVANCED_REC
-- ===========================  DESCRIPTION  ===========================================
-- VALIDATE:
-- Any required fields are missing (award amount ONLY if budgetID is populated, recipient ID, 
-- public headline, program ID, and submitterID)
-- Budget ID exists and is active, else validate budget name
-- Program ID exists and is active, else validate program name
-- Budget is tied to program
-- Recipient ID ties to a valid CONTROL_NUM, and that user is not in INACTIVE or RESTRICTED status
-- Submitter ID ties to a valid CONTROL_NUM, and that user is not in INACTIVE or RESTRICTED status
-- Ecard ID exists and is tied to the program
-- Value ID exists and is tied to the program, else validate value name
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20170503    MP-10015    CREATED
-- MUDDAM        20170515    MP-10042    Fixed CRC validation issues, added logic to populate TEMP_NOMINATION_ID if no errors
--                                            Made BUDGET_ID nullable (as long as award amount is null as well)
--                                            Must have Award Type if Award Amount is populated
-- HARWELLM        20170523    MP-10042    AwardType should be either POINTS or CASH. Also don't group nominations by AwardAmount
-- HARWELLM        20170531    MP-10347    FileName should not be cut off at 30 characters
-- MARTINC        20170602    MP-10358    Added ISNULL with a default value of 0 to Batch Event items
-- MUDDAM        20170607    MP-10339    Changed BUDGET_ID to ID in the subquery for active budget validation
-- ARENASW      20170615    MP-10442    Added PENDING_RELEASE, QUEUED, IN_PROGRESS as files states to be considered for duplicate names.
-- GARCIAF2        20170816    MP-10830    Report error when Public Headline or Private Message are over required limit of characters.
-- GARCIAF2        20171117    MP-10664    Update validation for Award Type: PAYOUT_ITEM & LOOKUP tables
-- CHAVEZDH        20171117    MP-10779    Allow the ability to upload duplicated pax ids records.
-- CHAVEZDH        20171207    MP-10461    Validate Program_Name, Budget_Name, Value.
-- CHAVEZDH        20171220    MP-11430    Assign "Award Type" value to STAGE_ADVANCE_REC from PROGRAM_MISC
-- FREINERT     20180122    MP-11582    Fixing value ID validation to default to value name if ID is invalid.
-- CHAVEZDH        20180220    MP-11129    Add PPPX conversion for award_amount, save original_amount.
-- FARFANLE        20180328    MP-11869    Add PPPX applied flag to return result set.
-- FARFANLE        20180329    MP-11879    Add PAX table to PPPX query.
-- CHAVEZDH        20180403    MP-11913    Round up amounts to the next integer after pppx convertion.
--
-- ===========================  DECLARATIONS  ==========================================

ALTER PROCEDURE component.UP_BULK_UPLOAD_ADVANCED_REC
@batchId BIGINT,
@userId NVARCHAR(MAX),
@fileCRC NVARCHAR(MAX),
@fileName NVARCHAR(255)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

DECLARE @numberOfRecords INT
DECLARE @numberOfRecordsWithErrors INT
DECLARE @totalAwardAmount INT
DECLARE @totalNominations INT
DECLARE @duplicatedFileName INT
DECLARE @duplicatedFileCrc INT
DECLARE @pppIndexApplied INT
DECLARE @pppIndexEnabledForProgram NVARCHAR(5)

    ------------------------------------------------------------------------------------------
    -- FILE VALIDATION
    ------------------------------------------------------------------------------------------
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Public headline is blank; '
    WHERE BATCH_ID = @batchId AND (PUBLIC_HEADLINE IS NULL OR len(PUBLIC_HEADLINE) = 0);
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Public headline is over required limit of 140 characters; '
    WHERE BATCH_ID = @batchId AND (PUBLIC_HEADLINE IS NOT NULL AND len(PUBLIC_HEADLINE) = 140);

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Private message is over required limit of 2000 characters; '
    WHERE BATCH_ID = @batchId AND (len(PRIVATE_MESSAGE) >= 2000);
    
     UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Recipient ID is blank; '
    WHERE BATCH_ID = @batchId AND (RECIPIENT_ID IS NULL OR len(RECIPIENT_ID) = 0);
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Submitter ID is blank; '
    WHERE BATCH_ID = @batchId AND (SUBMITTER_ID IS NULL OR len(SUBMITTER_ID) = 0);
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'When Budget ID or Budget Name is populated Award amount can not be blank; '
    WHERE BATCH_ID = @batchId 
        AND ((BUDGET_ID IS NOT NULL OR len(BUDGET_ID) > 0) 
        OR (BUDGET_NAME IS NOT NULL OR len(BUDGET_NAME) > 0)) AND AWARD_AMOUNT IS NULL;
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'When Award Amount is populated Budget ID can not be blank; '
    WHERE BATCH_ID = @batchId 
        AND (AWARD_AMOUNT IS NOT NULL OR len(AWARD_AMOUNT) > 0) 
        AND (BUDGET_ID IS NULL AND BUDGET_NAME IS NULL);
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Budget Name or ID is invalid/missing; '
    WHERE BATCH_ID = @batchId 
        AND (
            ((BUDGET_ID IS NOT NULL AND len(BUDGET_ID) > 0 AND BUDGET_ID NOT IN 
                (SELECT ID FROM component.BUDGET
                WHERE STATUS_TYPE_CODE ='ACTIVE'
                AND FROM_DATE < GETDATE()
                AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
                AND 
                ((BUDGET_NAME IS NULL OR len(BUDGET_NAME) = 0)
                    OR
                    (BUDGET_NAME IS NOT NULL AND len(BUDGET_NAME) > 0 AND BUDGET_NAME NOT IN 
                    (SELECT BUDGET_NAME FROM component.BUDGET
                    WHERE STATUS_TYPE_CODE ='ACTIVE'
                    AND FROM_DATE < GETDATE()
                    AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
                )
            )
            OR ((BUDGET_NAME IS NOT NULL AND len(BUDGET_NAME) > 0 AND BUDGET_NAME NOT IN 
                (SELECT BUDGET_NAME FROM component.BUDGET
                WHERE STATUS_TYPE_CODE ='ACTIVE'
                AND FROM_DATE < GETDATE()
                AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
                AND 
                ((BUDGET_ID IS NULL OR len(BUDGET_ID) = 0)
                    OR (BUDGET_ID IS NOT NULL AND len(BUDGET_ID) > 0 AND BUDGET_ID NOT IN 
                    (SELECT ID FROM component.BUDGET
                    WHERE STATUS_TYPE_CODE ='ACTIVE'
                    AND FROM_DATE < GETDATE()
                    AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
                )
            )
        )

    UPDATE SAR SET SAR.BUDGET_ID = B.ID FROM component.STAGE_ADVANCED_REC SAR
        JOIN component.BUDGET B ON B.BUDGET_NAME = SAR.BUDGET_NAME 
        AND B.STATUS_TYPE_CODE ='ACTIVE'
        AND B.FROM_DATE < GETDATE()
        AND (B.THRU_DATE IS NULL OR B.THRU_DATE > GETDATE()) 
    WHERE BATCH_ID = @batchId 
        AND (
            (SAR.BUDGET_ID IS NULL OR len(SAR.BUDGET_ID) = 0)
            OR SAR.BUDGET_ID IS NOT NULL AND len(SAR.BUDGET_ID) > 0 AND SAR.BUDGET_ID NOT IN 
            (SELECT ID FROM component.BUDGET
            WHERE STATUS_TYPE_CODE ='ACTIVE'
            AND FROM_DATE < GETDATE()
            AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE()))
        ) 
        AND SAR.BUDGET_NAME NOT IN 
            (SELECT BUDGET_NAME FROM (SELECT BUDGET_NAME, COUNT (ID) AS CNT FROM component.BUDGET GROUP BY BUDGET_NAME
            HAVING COUNT (ID) > 1) AS CNT_BUDGET )

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Duplicate Budget Name, please provide valid Budget ID; '
    WHERE BATCH_ID = @batchId 
        AND BUDGET_NAME IN 
        (SELECT BUDGET_NAME FROM (SELECT BUDGET_NAME, COUNT (ID) AS CNT FROM component.BUDGET GROUP BY BUDGET_NAME
        HAVING COUNT (ID) > 1) AS CNT_BUDGET )
        AND ((BUDGET_ID IS NULL OR len(BUDGET_ID) = 0)
            OR BUDGET_ID IS NOT NULL AND len(BUDGET_ID) > 0 AND
            BUDGET_ID NOT IN 
            (SELECT ID FROM component.BUDGET
            WHERE STATUS_TYPE_CODE ='ACTIVE'
            AND FROM_DATE < GETDATE()
            AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Program Name or ID is invalid/missing; '
    WHERE BATCH_ID = @batchId 
        AND (
                ((PROGRAM_ID IS NULL OR len(PROGRAM_ID) = 0)
                OR PROGRAM_ID IS NOT NULL AND len(PROGRAM_ID) > 0 AND PROGRAM_ID NOT IN 
                (SELECT PROGRAM_ID FROM component.PROGRAM
                WHERE STATUS_TYPE_CODE ='ACTIVE'
                AND FROM_DATE < GETDATE()
                AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
            AND 
                ((PROGRAM_NAME IS NULL OR len(PROGRAM_NAME) = 0)
                OR PROGRAM_NAME IS NOT NULL AND len(PROGRAM_NAME) > 0 AND PROGRAM_NAME NOT IN 
                (SELECT PROGRAM_NAME FROM component.PROGRAM
                WHERE STATUS_TYPE_CODE ='ACTIVE'
                AND FROM_DATE < GETDATE()
                AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE())))
        )

    UPDATE SAR SET SAR.PROGRAM_ID = P.PROGRAM_ID FROM component.STAGE_ADVANCED_REC SAR
        JOIN component.PROGRAM P ON P.PROGRAM_NAME = SAR.PROGRAM_NAME 
        AND P.STATUS_TYPE_CODE ='ACTIVE'
        AND P.FROM_DATE < GETDATE()
        AND (P.THRU_DATE IS NULL OR P.THRU_DATE > GETDATE()) 
    WHERE BATCH_ID = @batchId 
        AND (
            (SAR.PROGRAM_ID IS NULL OR len(SAR.PROGRAM_ID) = 0)
            OR SAR.PROGRAM_ID IS NOT NULL AND len(SAR.PROGRAM_ID) > 0 AND SAR.PROGRAM_ID NOT IN 
            (SELECT PROGRAM_ID FROM component.PROGRAM
            WHERE STATUS_TYPE_CODE ='ACTIVE'
            AND FROM_DATE < GETDATE()
            AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE()))
        ) 

    UPDATE SAR SET SAR.AWARD_TYPE = PM.MISC_DATA FROM component.STAGE_ADVANCED_REC SAR
        JOIN component.PROGRAM_MISC PM ON PM.PROGRAM_ID = SAR.PROGRAM_ID
        AND PM.VF_NAME = 'SELECTED_AWARD_TYPE'
    WHERE BATCH_ID = @batchId 
    AND SAR.AWARD_AMOUNT IS NOT NULL

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Invalid or inactive Recipient ID; '
    WHERE BATCH_ID = @batchId AND (RECIPIENT_ID IS NOT NULL AND len(RECIPIENT_ID) > 0 AND RECIPIENT_ID NOT IN 
    (SELECT P.CONTROL_NUM
    FROM component.PAX P 
    LEFT JOIN component.SYS_USER S ON P.PAX_ID=S.PAX_ID
    where S.STATUS_TYPE_CODE NOT IN ('INACTIVE','RESTRICTED'))
    )
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Invalid or inactive Submitter ID; '
    WHERE BATCH_ID = @batchId AND (SUBMITTER_ID IS NOT NULL AND len(SUBMITTER_ID) > 0 AND SUBMITTER_ID NOT IN 
    (SELECT P.CONTROL_NUM
    FROM component.PAX P 
    LEFT JOIN component.SYS_USER S ON P.PAX_ID=S.PAX_ID
    where S.STATUS_TYPE_CODE NOT IN ('INACTIVE','RESTRICTED'))
    )

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Value Name or ID is invalid; '
    WHERE BATCH_ID = @batchId 
        AND (
                ((VALUE_ID IS NULL OR len(VALUE_ID) = 0)
            AND
                (VALUE IS NOT NULL AND len(VALUE) > 0 AND VALUE NOT IN
                (SELECT SAR.VALUE FROM component.STAGE_ADVANCED_REC SAR
                JOIN component.RECOGNITION_CRITERIA RC ON RC.RECOGNITION_CRITERIA_NAME=SAR.VALUE
                JOIN component.PROGRAM_CRITERIA PC ON PC.PROGRAM_ID =SAR.PROGRAM_ID AND PC.RECOGNITION_CRITERIA_ID=RC.ID)))
            OR
                ((VALUE_ID IS NOT NULL AND len(VALUE_ID) > 0 AND VALUE_ID NOT IN
                (SELECT SAR.VALUE_ID FROM component.STAGE_ADVANCED_REC SAR
                JOIN component.RECOGNITION_CRITERIA RC ON RC.ID=SAR.VALUE_ID
                JOIN component.PROGRAM_CRITERIA PC ON PC.PROGRAM_ID =SAR.PROGRAM_ID AND PC.RECOGNITION_CRITERIA_ID=SAR.VALUE_ID))
            AND
                ((VALUE IS NULL or len(VALUE) = 0)
                OR VALUE IS NOT NULL AND len(VALUE) > 0 AND VALUE NOT IN
                (SELECT SAR.VALUE FROM component.STAGE_ADVANCED_REC SAR
                JOIN component.RECOGNITION_CRITERIA RC ON RC.RECOGNITION_CRITERIA_NAME=SAR.VALUE
                JOIN component.PROGRAM_CRITERIA PC ON PC.PROGRAM_ID =SAR.PROGRAM_ID AND PC.RECOGNITION_CRITERIA_ID=RC.ID)))
        )

    UPDATE SAR SET SAR.VALUE_ID = RC.ID
    FROM  component.STAGE_ADVANCED_REC SAR 
        JOIN component.RECOGNITION_CRITERIA RC ON RC.RECOGNITION_CRITERIA_NAME=SAR.VALUE
        JOIN component.PROGRAM_CRITERIA PC ON PC.PROGRAM_ID =SAR.PROGRAM_ID AND PC.RECOGNITION_CRITERIA_ID=RC.ID
    WHERE BATCH_ID = @batchId AND (
        (SAR.VALUE_ID IS NULL OR len(SAR.VALUE_ID) = 0)
        OR SAR.VALUE_ID IS NOT NULL AND len(SAR.VALUE_ID) > 0 AND SAR.VALUE_ID NOT IN 
        (SELECT SAR.VALUE_ID FROM component.STAGE_ADVANCED_REC SAR 
            JOIN component.RECOGNITION_CRITERIA RC ON RC.ID=SAR.VALUE_ID
            JOIN component.PROGRAM_CRITERIA PC ON PC.PROGRAM_ID =SAR.PROGRAM_ID AND PC.RECOGNITION_CRITERIA_ID=SAR.VALUE_ID
        )
    )

    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Invalid Ecard ID for program; '
    WHERE BATCH_ID = @batchId AND (ECARD_ID IS NOT NULL AND len(ECARD_ID) > 0 AND ECARD_ID NOT IN ( 
     SELECT DISTINCT (SAR.ECARD_ID)
      FROM component.STAGE_ADVANCED_REC SAR 
      JOIN component.ECARD E ON E.ID=SAR.ECARD_ID
      JOIN component.PROGRAM_ECARD PE ON PE.PROGRAM_ID =SAR.PROGRAM_ID AND PE.ECARD_ID=SAR.ECARD_ID
        )
    )
    
    UPDATE component.STAGE_ADVANCED_REC SET ERROR_MESSAGE = ISNULL(ERROR_MESSAGE, '') + 'Invalid value for Make recognition private, must be TRUE or FALSE;'
    WHERE BATCH_ID = @batchId AND (MAKE_RECOGNITION_PRIVATE IS NOT NULL AND MAKE_RECOGNITION_PRIVATE NOT IN('TRUE','FALSE','true','false'));

    -- CHECK FOR PPP_INDEX ENABLED AND SAVE IF NEEDED ON AWARD_AMOUNT
    SELECT @pppIndexApplied = COUNT(MISC_DATA) FROM component.STAGE_ADVANCED_REC SAR
        INNER JOIN component.PAX P on P.CONTROL_NUM = SAR.RECIPIENT_ID
        INNER JOIN component.ADDRESS A on A.PAX_ID = P.PAX_ID AND A.PREFERRED = 'Y'
        INNER JOIN component.COUNTRY_AWARD_INDEX CAI on CAI.COUNTRY_CODE = A.COUNTRY_CODE
        INNER JOIN component.PROGRAM_MISC PM ON PM.PROGRAM_ID = SAR.PROGRAM_ID
            AND PM.VF_NAME = 'PPP_INDEX_ENABLED'
            AND UPPER(RTRIM(LTRIM(PM.MISC_DATA))) = 'TRUE'
    WHERE SAR.BATCH_ID = @batchId
        AND CAI.AWARD_INDEX IS NOT NULL

    UPDATE component.STAGE_ADVANCED_REC SET ORIGINAL_AMOUNT = AWARD_AMOUNT WHERE BATCH_ID = @batchId

    UPDATE SAR
        SET SAR.AWARD_AMOUNT =
        CASE 
            WHEN UPPER(RTRIM(LTRIM(PM.MISC_DATA))) = 'TRUE' AND CAI.AWARD_INDEX IS NOT NULL THEN 
            CEILING(SAR.AWARD_AMOUNT * CAI.AWARD_INDEX)
        ELSE 
            SAR.AWARD_AMOUNT
        END  
    FROM component.STAGE_ADVANCED_REC SAR
        INNER JOIN component.PAX P on P.CONTROL_NUM = SAR.RECIPIENT_ID
        INNER JOIN component.ADDRESS A on A.PAX_ID = P.PAX_ID AND A.PREFERRED = 'Y'
        LEFT JOIN component.COUNTRY_AWARD_INDEX CAI on CAI.COUNTRY_CODE = A.COUNTRY_CODE
        LEFT JOIN component.PROGRAM_MISC PM on PM.PROGRAM_ID = SAR.PROGRAM_ID
            AND PM.VF_NAME = 'PPP_INDEX_ENABLED'
    WHERE BATCH_ID = @batchId

    SELECT 
    @totalAwardAmount = SUM(AWARD_AMOUNT)
    , @numberOfRecords=count(BATCH_ID)
    ,@totalNominations = (select count(*) from (
        SELECT
            BATCH_ID,
            PUBLIC_HEADLINE,
            PRIVATE_MESSAGE,
            PROGRAM_ID,
            BUDGET_ID,
            AWARD_TYPE,
            SUBMITTER_ID,
            ECARD_ID,
            VALUE_ID,
            MAKE_RECOGNITION_PRIVATE
        FROM (
            SELECT ROW_NUMBER() OVER(PARTITION BY BATCH_ID,
                    PUBLIC_HEADLINE,
                    PRIVATE_MESSAGE,
                    PROGRAM_ID,
                    BUDGET_ID,
                    AWARD_TYPE,
                    SUBMITTER_ID,
                    ECARD_ID,
                    VALUE_ID,
                    MAKE_RECOGNITION_PRIVATE, 
                    RECIPIENT_ID 
                ORDER BY BATCH_ID,
                    PUBLIC_HEADLINE,
                    PRIVATE_MESSAGE,
                    PROGRAM_ID,
                    BUDGET_ID,
                    AWARD_TYPE,
                    SUBMITTER_ID,
                    ECARD_ID,
                    VALUE_ID,
                    MAKE_RECOGNITION_PRIVATE, 
                    RECIPIENT_ID) ROWCNT,
                RECIPIENT_ID,
                BATCH_ID,
                PUBLIC_HEADLINE,
                PRIVATE_MESSAGE,
                PROGRAM_ID,
                BUDGET_ID,
                AWARD_TYPE,
                SUBMITTER_ID,
                ECARD_ID,
                VALUE_ID,
                MAKE_RECOGNITION_PRIVATE
            FROM component.STAGE_ADVANCED_REC 
            WHERE BATCH_ID = @batchId
        ) TEMP
        GROUP BY BATCH_ID,
            PUBLIC_HEADLINE,
            PRIVATE_MESSAGE,
            PROGRAM_ID,
            BUDGET_ID,
            AWARD_TYPE,
            SUBMITTER_ID,
            ECARD_ID,
            VALUE_ID,
            MAKE_RECOGNITION_PRIVATE, 
            ROWCNT 
    ) as distinct_nominations )
    ,@numberOfRecordsWithErrors = (SELECT count (ID) as 'RECORDS_WITH_ERRORS'
        FROM component.STAGE_ADVANCED_REC
        WHERE ERROR_MESSAGE IS NOT NULL
        AND BATCH_ID=@batchId)
    FROM component.STAGE_ADVANCED_REC 
    WHERE BATCH_ID = @batchId
    
    ------------------------------------------------------------------------------------------
    -- INSERT BATCH_EVENTS
    ------------------------------------------------------------------------------------------
    --INSERT FILE NAME
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'FILE',@fileName,'COMPLETE')

    --ADD USER_ID
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'USERID',@userId,'COMPLETE')
    
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'RECS',ISNULL(@numberOfRecords, 0),'COMPLETE')
        
    --TOTAL AWARD AMOUNT
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'TOTAL_AWARD_AMOUNT',ISNULL(@totalAwardAmount, 0),'COMPLETE')
 
     --TOTAL NUMBER OF NOMINATIONS
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'NOMINATION_COUNT',ISNULL(@totalNominations, 0),'COMPLETE')

    --DUPLICATED FILE CRC
    SET @duplicatedFileCrc =(SELECT count(*) AS 'FILE_CRC_MAYBE_DUPLICATED'
    FROM component.BATCH_EVENT WHERE BATCH_EVENT_TYPE_CODE='FILCRC'
    AND BATCH_ID != @batchId
    AND MESSAGE=@fileCRC)

    DECLARE @duplicatedFileCrcBoolean nvarchar(5)
    SET @duplicatedFileCrcBoolean = CASE WHEN @duplicatedFileCrc > 0 THEN 'TRUE' ELSE 'FALSE' END
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'DUPLICATE_FILE_CONTENTS',@duplicatedFileCrcBoolean,'COMPLETE')
    
    --INSERT FILE CRC
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'FILCRC',@fileCRC,'COMPLETE')
 
    --DUPLICATED FILE NAME
    SET @duplicatedFileName=( SELECT COUNT(*) AS 'DUPLICATED_FILE' FROM component.BATCH B
    LEFT JOIN component.BATCH_FILE BF ON BF.BATCH_ID=B.BATCH_ID
    WHERE B.STATUS_TYPE_CODE IN ('PENDING_RELEASE','QUEUED','IN_PROGRESS','COMPLETE')
    AND BATCH_TYPE_CODE='ADV_REC_BULK_UPLOAD'
    AND BF.FILENAME=@fileName)

    DECLARE @duplicatedFileNameBoolean nvarchar(5)
    SET @duplicatedFileNameBoolean = CASE WHEN @duplicatedFileName > 0 THEN 'TRUE' ELSE 'FALSE' END
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId,'DUPLICATE_FILE_NAME',@duplicatedFileNameBoolean,'COMPLETE')

    --PPP INDEX APPLIED 
    SET @pppIndexEnabledForProgram = CASE WHEN @pppIndexApplied > 0 THEN 'TRUE' ELSE 'FALSE' END
    INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
    (@batchId, 'PPP_INDEX_APPLIED', @pppIndexEnabledForProgram, 'COMPLETE')
    
    ------------------------------------------------------------------------------------------
    -- UPDATE BATCH_STATUS
    ------------------------------------------------------------------------------------------     
    IF @numberOfRecordsWithErrors > 0
    BEGIN
        INSERT INTO component.BATCH_EVENT (BATCH_ID,BATCH_EVENT_TYPE_CODE,MESSAGE,STATUS_TYPE_CODE) VALUES
        (@batchId,'ERR',@numberOfRecordsWithErrors,'COMPLETE')
        
        UPDATE component.BATCH
        SET STATUS_TYPE_CODE='FAILED'
        WHERE BATCH_ID=@batchId
    END
    ELSE
    BEGIN
        UPDATE component.BATCH 
        SET STATUS_TYPE_CODE='PENDING_RELEASE'
        WHERE BATCH_ID=@batchId

        UPDATE component.STAGE_ADVANCED_REC
            SET TEMP_NOMINATION_ID = RESULTS.TEMP_NOMINATION_ID
        FROM component.STAGE_ADVANCED_REC BASE
        INNER JOIN (
            SELECT DENSE_RANK() OVER(ORDER BY BATCH_ID,
                    PUBLIC_HEADLINE,
                    PRIVATE_MESSAGE,
                    PROGRAM_ID,
                    BUDGET_ID,
                    AWARD_TYPE,
                    SUBMITTER_ID,
                    ECARD_ID,
                    VALUE_ID,
                    MAKE_RECOGNITION_PRIVATE, 
                    ROWCNT) AS TEMP_NOMINATION_ID,
                ROWCNT, 
                ID,
                RECIPIENT_ID,
                BATCH_ID,                                        -- BATCH_ID IS REQUIRED - WILL ALWAYS EXIST
                PUBLIC_HEADLINE,                                -- PUBLIC_HEADLINE IS REQUIRED
                PRIVATE_MESSAGE,                                -- PRIVATE_MESSAGE IS OPTIONAL
                PROGRAM_ID,                                        -- PROGRAM_ID IS REQUIRED
                BUDGET_ID,                                        -- BUDGET_ID IS OPTIONAL - MIGHT NOT HAVE POINTS
                AWARD_TYPE,                                        -- AWARD_TYPE IS OPTIONAL - MIGHT NOT HAVE POINTS
                SUBMITTER_ID,                                    -- SUBMITTER_ID IS REQUIRED
                ECARD_ID,                                        -- ECARD_ID IS OPTIONAL - MIGHT NOT USE ONE
                VALUE_ID,                                        -- VALUE_ID IS OPTIONAL - NOT ALL PROGRAMS REQUIRE
                MAKE_RECOGNITION_PRIVATE                        -- MAKE_RECOGNITION_PRIVATE IS OPTIONAL - WILL DEFAULT TO FALSE
            FROM (
                SELECT ROW_NUMBER() OVER(PARTITION BY BATCH_ID,
                            PUBLIC_HEADLINE,
                            PRIVATE_MESSAGE,
                            PROGRAM_ID,
                            BUDGET_ID,
                            AWARD_TYPE,
                            SUBMITTER_ID,
                            ECARD_ID,
                            VALUE_ID,
                            MAKE_RECOGNITION_PRIVATE, 
                            RECIPIENT_ID 
                        ORDER BY BATCH_ID,
                            PUBLIC_HEADLINE,
                            PRIVATE_MESSAGE,
                            PROGRAM_ID,
                            BUDGET_ID,
                            AWARD_TYPE,
                            SUBMITTER_ID,
                            ECARD_ID,
                            VALUE_ID,
                            MAKE_RECOGNITION_PRIVATE, 
                            RECIPIENT_ID,
                            ID) 
                    ROWCNT,
                    ID, 
                    RECIPIENT_ID,
                    BATCH_ID,
                    PUBLIC_HEADLINE,
                    PRIVATE_MESSAGE,
                    PROGRAM_ID,
                    BUDGET_ID,
                    AWARD_TYPE,
                    SUBMITTER_ID,
                    ECARD_ID,
                    VALUE_ID,
                    MAKE_RECOGNITION_PRIVATE
                FROM component.STAGE_ADVANCED_REC 
                WHERE BATCH_ID = @batchId
            ) TEMP
        ) RESULTS
        ON BASE.ID = RESULTS.ID 
        
    END

    SELECT @batchId AS 'BATCH_ID', @duplicatedFileCrcBoolean AS 'DUPLICATE_FILE_CONTENTS', @duplicatedFileNameBoolean AS 'DUPLICATE_FILE_NAME',
            @totalNominations 'NOMINATION_COUNT', @totalAwardAmount AS 'TOTAL_AWARD_AMOUNT', @numberOfRecords AS 'TOTAL_ROWS', @fileName AS 'FILE_NAME',
            (SELECT STATUS_TYPE_CODE FROM component.BATCH WHERE BATCH_ID = @batchId) AS 'STATUS',
            @pppIndexEnabledForProgram AS 'PPP_INDEX_APPLIED'
            
END --SP