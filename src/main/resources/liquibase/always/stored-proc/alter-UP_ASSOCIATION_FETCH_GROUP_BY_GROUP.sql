/****** Object:  StoredProcedure [UP_ASSOCIATION_FETCH_GROUP_BY_GROUP]    Script Date: 10/14/2015 12:31:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_GROUP_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID AND LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS)
-- RETURN ALL ASSOCIATED GROUP_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_GROUP_BY_GROUP]
    @group_id BIGINT
,    @level_increment BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS GROUP_ID
FROM    UF_ASSOCIATION_FETCH_GROUP_BY_GROUP (@group_id, @level_increment)

END

GO

