/****** Object:  UserDefinedFunction [component].[UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP]    Script Date: 10/16/2020 11:23:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS), LEVEL OFFSET
-- RETURN ALL ASSOCIATED PAX_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR	DATE		CHANGE DESCRIPTION
-- DOHOGNTA  	20110221  	CREATED
-- DOHOGNTA   	20150309  	CHANGE IDENTITIES TO BIGINT
-- burgestl	20201103	Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP] (
  @pax_group_id BIGINT
, @level_increment INT
, @level_offset INT)
RETURNS @result_set TABLE (
  [id] BIGINT NULL
, [level] INT)
AS
BEGIN

DECLARE @level INT
DECLARE @level_previous INT
DECLARE @node TABLE ([id] BIGINT, [level] INT)
DECLARE @rowcount BIGINT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET @level = 0
SET @level_previous = 0
SET @rowcount = 0

-- INSERT SEED RECORD(S)
INSERT INTO @node ([id], [level]) SELECT pax_group_id, @level FROM PAX_GROUP WHERE PAX_GROUP_ID = @pax_group_id

SET @rowcount = @@rowcount

-- PROCESS UNTIL NO MORE ROWS OR OFFSET IS MET
WHILE (@rowcount > 0) AND (@level_increment = -1) AND ((@level_offset IS NULL) OR ((@level_offset IS NOT NULL) AND (ABS(@level) < (@level_offset))))
BEGIN
  -- INCREMENT LEVEL
  SET @level_previous = @level
  SET @level = @level + @level_increment

  -- INSERT ASSOCIATED RECORD(S) FOR RECORD(S) OF PREVIOUS LEVEL
  INSERT INTO @node (
    [id]
  , [level])
  SELECT PAX_GROUP.OWNER_GROUP_ID, @level  
  FROM @node AS NODE
  inner join PAX_GROUP on NODE.[id] = PAX_GROUP.PAX_GROUP_ID
  WHERE NODE.[level] = @level_previous
  AND PAX_GROUP.OWNER_GROUP_ID IS NOT NULL

  SET @rowcount = @@rowcount
END -- WHILE

-- PROCESS UNTIL NO MORE ROWS OR OFFSET IS MET
WHILE (@rowcount > 0) AND (@level_increment = 1) AND ((@level_offset IS NULL) OR ((@level_offset IS NOT NULL) AND (ABS(@level) < (@level_offset))))
BEGIN
  -- INCREMENT LEVEL
  SET @level_previous = @level
  SET @level = @level + @level_increment

  -- INSERT ASSOCIATED RECORD(S) FOR RECORD(S) OF PREVIOUS LEVEL
  INSERT INTO @node (
    [id]
  , [level])
  SELECT
    PAX_GROUP.PAX_GROUP_ID
  , @level
  FROM @node AS NODE
  inner join PAX_GROUP on NODE.[id] = PAX_GROUP.OWNER_GROUP_ID
  WHERE NODE.[level] = @level_previous
  AND PAX_GROUP.OWNER_GROUP_ID IS NOT NULL

  SET @rowcount = @@rowcount
END -- WHILE

-- RETURN LIST OF PAX(S)
INSERT @result_set SELECT [id], [level] FROM @node AS NODE

-- RETURN RESULT SET
RETURN

END
GO


