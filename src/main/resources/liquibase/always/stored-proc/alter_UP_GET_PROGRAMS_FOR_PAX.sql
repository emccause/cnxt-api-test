IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_GET_PROGRAMS_FOR_PAX' AND schema_id = SCHEMA_ID('component'))
BEGIN
exec ('create PROCEDURE component.UP_GET_PROGRAMS_FOR_PAX as BEGIN select 1 as c1 END')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_GET_PROGRAMS_FOR_PAX
-- ===========================  DESCRIPTION  ==================================
-- DDAYS		20201130	CNXT-359 Add logging UP_GET_PROGRAMS
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_PROGRAMS_FOR_PAX]
    @PAX_ID BIGINT,
    @ROLE_NAMES_LIST NVARCHAR(MAX),
    @PROGRAM_TYPE_NAME NVARCHAR(MAX),
    @IS_GIVE_REC_ELEGIBILITY NVARCHAR(10)
AS
BEGIN
SET NOCOUNT ON

BEGIN TRY
DECLARE @loggingStartDate DATETIME2(7),
@loggingEndDate DATETIME2(7),
@loggingParameters NVARCHAR(MAX),
@loggingSprocName nvarchar(150),
@loggingDebug int

--insert component.application_data (key_name, value)
--select 'sql.logging','true'
SELECT @loggingDebug = CASE WHEN (isnull(value,'') ='true') THEN 1 ELSE 0 END FROM component.application_data where key_name ='sql.logging';

IF (@loggingDebug=1)
BEGIN
SET @loggingSprocName ='UP_GET_PROGRAMS_FOR_PAX'
SET @loggingStartDate = getdate()
SET @loggingParameters ='@PAX_ID ='+ ISNULL(cast(@PAX_ID as varchar),'null') --BIGINT
		+ ' :: @ROLE_NAMES_LIST=' +isnull(@ROLE_NAMES_LIST, 'null') -- NVARCHAR(MAX)
		+ ' :: @PROGRAM_TYPE_NAME=' + isnull(cast(@PROGRAM_TYPE_NAME as varchar),'null') -- NVARCHAR(MAX)
		+ ' ::@IS_GIVE_REC_ELEGIBILITY=' + isnull(cast(@IS_GIVE_REC_ELEGIBILITY as varchar),'null') -- NVARCHAR(MAX)

END;

SELECT DISTINCT
    P.PROGRAM_ID AS PROGRAM_ID,
    P.PROGRAM_NAME AS PROGRAM_NAME,
    P.PROGRAM_DESC AS PROGRAM_DESCRIPTION_SHORT,
    P.PROGRAM_LONG_DESC AS PROGRAM_DESCRIPTION_LONG,
    PT.PROGRAM_TYPE_NAME AS PROGRAM_TYPE,
    P.PROGRAM_CATEGORY_CODE AS PROGRAM_CATEGORY_CODE,
    CREATOR_PAX.PAX_ID AS CREATOR_ID,
    CREATOR_PAX.LANGUAGE_CODE AS CREATOR_LANGUAGE_CODE,
    CREATOR_PAX.PREFERRED_LOCALE AS CREATOR_PREFERRED_LOCALE,
    CREATOR_PAX.CONTROL_NUM AS CREATOR_CONTROL_NUM,
    CREATOR_PAX.FIRST_NAME AS CREATOR_FIRST_NAME,
    CREATOR_PAX.MIDDLE_NAME AS CREATOR_MIDDLE_NAME,
    CREATOR_PAX.LAST_NAME AS CREATOR_LAST_NAME,
    CREATOR_PAX.NAME_PREFIX AS CREATOR_NAME_PREFIX,
    CREATOR_PAX.NAME_SUFFIX AS CREATOR_NAME_SUFFIX,
    CREATOR_PAX.COMPANY_NAME AS CREATOR_COMPANY_NAME,
    CREATOR_PAX.STATUS_TYPE_CODE AS CREATOR_STATUS,
    CREATOR_PAX.JOB_TITLE AS CREATOR_JOB_TITLE,
    CREATOR_PAX.MANAGER_PAX_ID AS CREATOR_MANAGER_ID,
    P.CREATE_DATE AS CREATE_DATE,
    P.FROM_DATE AS FROM_DATE,
    P.THRU_DATE AS THRU_DATE,
    P.STATUS_TYPE_CODE AS STATUS,
    (
        SELECT
            COUNT(*)
        FROM
            component.LIKES
        WHERE
                TARGET_TABLE = 'PROGRAM'
          AND TARGET_ID = P.PROGRAM_ID
    )
        as LIKE_COUNT,
    LIKE_PAX.STATUS_TYPE_CODE,
    LIKE_PAX.PAX_ID,
    LIKE_PAX.LANGUAGE_CODE,
    LIKE_PAX.PREFERRED_LOCALE,
    LIKE_PAX.CONTROL_NUM,
    LIKE_PAX.FIRST_NAME,
    LIKE_PAX.MIDDLE_NAME,
    LIKE_PAX.LAST_NAME,
    LIKE_PAX.NAME_PREFIX,
    LIKE_PAX.NAME_SUFFIX,
    LIKE_PAX.COMPANY_NAME,
    LIKE_PAX.VERSION,
    LIKE_PAX.JOB_TITLE,
    LIKE_PAX.MANAGER_PAX_ID,
    PROGRAM_IMAGES.FILE_ID AS PROGRAM_IMAGE_ID
FROM
    component.ACL
        INNER JOIN
    component.ROLE
    ON ROLE.ROLE_ID = ACL.ROLE_ID
        INNER JOIN
    component.PROGRAM P
    ON P.PROGRAM_ID = ACL.TARGET_ID
        INNER JOIN
    component.PROGRAM_TYPE PT
    ON P.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE
        INNER JOIN
    component.VW_PROGRAM_VISIBILITY PV
    ON PV.PROGRAM_ID = P.PROGRAM_ID
        INNER JOIN
    component.VW_USER_GROUPS_SEARCH_DATA CREATOR_PAX
    ON CREATOR_PAX.PAX_ID = P.CREATOR_PAX_ID
        LEFT JOIN
    (
        SELECT
            L1.TARGET_ID,
            L1.PAX_ID
        FROM
            component.LIKES L1
                LEFT OUTER JOIN
            component.LIKES L2
            ON L2.TARGET_TABLE = L1.TARGET_TABLE
                AND L2.TARGET_ID = L1.TARGET_ID
                AND L2.LIKE_DATE > L1.LIKE_DATE
        WHERE
            L2.LIKE_ID IS NULL
          AND L1.TARGET_TABLE = 'PROGRAM'
    )
        AS LATEST_LIKE_PAX
    ON P.PROGRAM_ID = LATEST_LIKE_PAX.TARGET_ID
        LEFT JOIN
    (
        SELECT
            TARGET_ID,
            PAX_ID
        FROM
            component.LIKES
        WHERE
                PAX_ID = @PAX_ID
          AND TARGET_TABLE = 'PROGRAM'
    )
        AS USER_LIKE_PAX
    ON P.PROGRAM_ID = USER_LIKE_PAX.TARGET_ID
        LEFT JOIN
    component.VW_USER_GROUPS_SEARCH_DATA LIKE_PAX
    ON
            CASE
                WHEN
                        USER_LIKE_PAX.PAX_ID IS NOT NULL
                        AND LIKE_PAX.PAX_ID = USER_LIKE_PAX.PAX_ID
                    THEN
                    1
                WHEN
                        USER_LIKE_PAX.PAX_ID IS NULL
                        AND LIKE_PAX.PAX_ID = LATEST_LIKE_PAX.PAX_ID
                    THEN
                    1
                ELSE
                    0
                END
            = 1
        LEFT OUTER JOIN
    (
        SELECT
            prgmf.TARGET_ID AS PROGRAM_ID,
            files.ID as FILE_ID
        FROM
            component.FILE_ITEM prgmf
                INNER JOIN
            component.FILES files
            ON prgmf.FILES_ID = files.ID
        WHERE
                files.FILE_TYPE_CODE = 'PROGRAM_IMAGE'
          AND TARGET_TABLE = 'PROGRAM'
    )
        AS PROGRAM_IMAGES
    ON P.PROGRAM_ID = PROGRAM_IMAGES.PROGRAM_ID
        LEFT JOIN
    component.RELATIONSHIP RL
    ON RL.PAX_ID_1 = LIKE_PAX.PAX_ID
        AND RL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
WHERE
        @ROLE_NAMES_LIST LIKE '%' + ROLE.ROLE_NAME + '%'
  AND ACL.TARGET_TABLE = 'PROGRAM'
  AND P.STATUS_TYPE_CODE = 'ACTIVE'
  AND P.FROM_DATE <= GETDATE()
  AND
    (
            P.THRU_DATE IS NULL
            OR P.THRU_DATE >= GETDATE()
        )
  AND
    (( ACL.SUBJECT_TABLE = 'GROUPS'
        AND ACL.SUBJECT_ID IN
            (
                SELECT
                    GROUP_ID
                FROM
                    component.VW_GROUP_TOTAL_MEMBERSHIP
                WHERE
                        PAX_ID = @PAX_ID
            )
         )
        OR
     (
                 ACL.SUBJECT_TABLE = 'PAX'
             AND ACL.SUBJECT_ID = @PAX_ID
         )
        )
  AND @PROGRAM_TYPE_NAME LIKE '%' + PT.PROGRAM_TYPE_NAME + '%'
  AND
    (
                'TRUE' = @IS_GIVE_REC_ELEGIBILITY
            OR PV.VISIBILITY = 'PUBLIC'
            OR PV.VISIBILITY IS NULL
        )
    IF (@loggingDebug=1)
BEGIN
      SET @loggingEndDate =getdate()
      -- Generate a divide-by-zero error--SELECT 1 / 0 AS Error;
      EXEC [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
END

END TRY

BEGIN CATCH
IF (@loggingDebug=1)
BEGIN
        DECLARE @errorMessage nvarchar(250)
SELECT @errorMessage =':: ERROR_NUMBER='+cast(ERROR_NUMBER() as varchar) +' :: ERROR_MESSAGE='+ERROR_MESSAGE()
    EXEC [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
END
END CATCH

END;