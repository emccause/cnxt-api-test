/****** Object:  StoredProcedure [component].[UP_RECOGNITION_APPROVAL_HISTORY_BASE]    Script Date: 10/15/2015 10:59:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_APPROVAL_HISTORY_BASE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS APPROVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID. THIS PROCEDURE MUST BE CALLED BY
-- ANOTHER PROCEDURE THAT CREATES THE #approvals TEMP TABLE THAT THIS PROCEDURE
-- POPULATES.
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- SCHIERVR        20081030                CREATED
-- THOMSOND        20081110                CHANGED FILTERING ON TOTALS CALCULATIONS
-- THOMSOND        20081120                CHANGED TOTAL POINTS CALCULATIONS
-- DOHOGNTA     20090311                REMOVED THE ENABLE_JUSTIFICATION / Y CONDITION
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- hernancl		 20210601	            update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_APPROVAL_HISTORY_BASE]
    @approver_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,   @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

DECLARE @PAX_ID BIGINT
SET @PAX_ID = (SELECT PAX_ID FROM PAX_GROUP WHERE PAX_GROUP_ID = @approver_pax_group_id)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE NOMINATION DATA
INSERT INTO #approvals (
    nomination_id
,    nomination_status

,    submittal_date
,    submitter_list

,    receiver_list
,    criteria
,    ecard_id
,    program_id
,    justification_comment_available_flag
,    budget_id
)
SELECT    DISTINCT
    NOMINATION.ID
,    NOMINATION.STATUS_TYPE_CODE
,    NOMINATION.SUBMITTAL_DATE
,    component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION.ID, @locale_code)
,    NOMINATION.ECARD_ID
,    NOMINATION.PROGRAM_ID
,    CASE WHEN NOMINATION.COMMENT_JUSTIFICATION IS NULL THEN 0 ELSE 1 END
,    NOMINATION.BUDGET_ID
FROM    NOMINATION
,       RECOGNITION
,       APPROVAL_HISTORY
,        PROGRAM_MISC
WHERE   NOMINATION.ID = RECOGNITION.NOMINATION_ID
AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
AND    APPROVAL_HISTORY.PAX_ID = @PAX_ID
AND    NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru
AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
AND    NOMINATION.STATUS_TYPE_CODE IS NOT NULL

-- We now have the base group of nomination records for the report

-- Update the additional fields needed for the report
-- This flag is used to invoke the auto-hide function on the report's Justification column.
-- The report will sum this column.  Any value > 0 indicates Justification column should be visible.
UPDATE    #approvals
SET    #approvals.justification_enabled_flag = 1
FROM    PROGRAM_MISC
WHERE    #approvals.PROGRAM_ID = PROGRAM_MISC.PROGRAM_ID
AND    GETDATE() BETWEEN PROGRAM_MISC.FROM_DATE AND ISNULL(PROGRAM_MISC.THRU_DATE, GETDATE())

-- There are several queries that follow that only need to populate a column if there is only one recipient.
-- This flag may be used as a filter for those queries. 1=true.
UPDATE    #approvals
SET    #approvals.multiple_recipient_flag = 1
WHERE    receiver_list LIKE '%,%'

-- This flag will be used by the report engine to control the display of the "Submitted By" column. 1=true.
UPDATE    #approvals
SET    #approvals.multiple_submitter_flag = 1
WHERE    submitter_list LIKE '%,%'

-- Get the first receiver name from RECOGNITION table.  If this is the only name, the report will use it. 
-- Otherwise the name will be replaced with "Multiple Recipients" by the report engine.
UPDATE    #approvals
SET    receiver_name = (
    SELECT TOP 1 ISNULL(PAX.FIRST_NAME, '') + ' ' + ISNULL(PAX.LAST_NAME, '')
    FROM    RECOGNITION
    INNER JOIN PAX ON RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    #approvals.multiple_recipient_flag <> 1
)

-- Get the approval_process_id for each nomination.  It will be used to determine the number of
-- approvals that have been configured for each nomination.
UPDATE    #approvals
SET    approval_process_id = APPROVAL_PROCESS.ID
FROM    APPROVAL_PROCESS
WHERE    #approvals.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #approvals.PROGRAM_ID = APPROVAL_PROCESS.PROGRAM_ID

-- Get the number of approvals for each nomination
UPDATE    #approvals
SET    approval_count = (
    SELECT    COUNT(1)
    FROM    (
        SELECT APPROVAL_PENDING.ID AS ID
        FROM    RECOGNITION
        INNER JOIN    APPROVAL_PENDING ON RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID
        WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
        AND    APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'
        AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #approvals.approval_process_id
        UNION
        SELECT    APPROVAL_HISTORY.ID AS ID
        FROM    RECOGNITION
        INNER JOIN    APPROVAL_HISTORY ON  RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID
        WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
        AND    APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
        AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    ) AS APPROVALS
)

-- Get the first submitter name.  If there are multiple submitters stored in auxiliary_submitter table,
-- then the report engine will replace this name with the word "Multiple".
UPDATE    #approvals
SET    submitter_name = ISNULL(PAX.FIRST_NAME, '') + ' ' + ISNULL(PAX.LAST_NAME, '')
FROM    PAX
INNER JOIN   NOMINATION ON PAX.PAX_ID = NOMINATION.SUBMITTER_PAX_ID
WHERE    #approvals.nomination_id = NOMINATION.ID

-- Get the total approved, denied or pending award amount for each nomination
UPDATE    #approvals
SET    total_points = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING','APPROVED','REJECTED')
)

-- Get the total approved points for each nomination
UPDATE    #approvals
SET    total_approved_points = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'APPROVED'
)

-- Get the total denied points for each nomination
UPDATE    #approvals
SET    total_denied_points = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'REJECTED'
)

-- Get the total pending points for each nomination
UPDATE    #approvals
SET    total_pending_points = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT, 0) > 0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'PENDING'
)

-- The recognition_id is used in the construction of the Ecard hyperlink in the report.
-- This is another case where it is only used if there is only 1 recognition.
UPDATE    #approvals
SET    recognition_id = (
    SELECT    TOP 1 RECOGNITION.ID
    FROM    RECOGNITION
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    #approvals.multiple_recipient_flag = 0
)

-- The denial_reason will be displayed in a roll-over on the report but only if there is one recipient.
UPDATE    #approvals
SET    denial_reason = APPROVAL_HISTORY.APPROVAL_NOTES
    FROM    RECOGNITION
    INNER JOIN  APPROVAL_HISTORY ON RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
    AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    AND    #approvals.nomination_status = 'REJECTED'

-- Get the name of the next approver.  Will be displayed if there is only one next approver or all next approvers are the same.
CREATE TABLE #next_approvers (nomination_id BIGINT,approver nvarchar(80))

INSERT INTO #next_approvers (
    nomination_id
,    approver
)
SELECT    RECOGNITION.NOMINATION_ID,ISNULL(PAX.FIRST_NAME,'') + ' ' + ISNULL(PAX.LAST_NAME,'')
FROM    #approvals
INNER JOIN  RECOGNITION ON  #approvals.nomination_id = RECOGNITION.NOMINATION_ID
INNER JOIN  APPROVAL_PENDING ON RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID
INNER JOIN  PAX ON APPROVAL_PENDING.PAX_ID = PAX.PAX_ID
WHERE  APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'
AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #approvals.approval_process_id

-- If multiple different next approver names are in #next_approvers then substitute a '-1'.
-- The report engine will replace '-1' with the words 'Multiple Approvers'
UPDATE #approvals 
SET    next_approver_count = (
    SELECT COUNT(DISTINCT approver) 
    FROM #next_approvers na
    WHERE #approvals.nomination_id = na.nomination_id
)

UPDATE #approvals 
SET    next_approver_name = (
    SELECT DISTINCT approver
    FROM    #next_approvers
    WHERE    #approvals.nomination_id = #next_approvers.nomination_id
    AND    #approvals.next_approver_count = 1
)


-- The approval date is the date of an approval by the pax requesting the report.
-- The date count indicates the number of unique approval dates(Days) there are per nomination at the highest approval level.
CREATE TABLE #filtered_approval_dates (
    nomination_id BIGINT
,    approval_level INT
,    dates DATETIME2
)

INSERT INTO #filtered_approval_dates (
    nomination_id
,    approval_level
,    dates
)
SELECT    NOMINATION_ID
,    APPROVAL_LEVEL
,    DATES
FROM    (SELECT #approvals.nomination_id
    ,    APPROVAL_HISTORY.APPROVAL_LEVEL
    ,    CONVERT(DATETIME2, CONVERT(VARCHAR, APPROVAL_HISTORY.APPROVAL_DATE, 101)) AS DATES
    FROM    #approvals
    ,    RECOGNITION
    ,    APPROVAL_HISTORY
    WHERE    #approvals.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
    AND    RECOGNITION.STATUS = APPROVAL_HISTORY.APPROVAL_STATUS
    AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #approvals.approval_process_id
    AND    APPROVAL_HISTORY.PAX_ID = @PAX_ID
    ) AS APPROVALS
GROUP BY
    NOMINATION_ID
,    APPROVAL_LEVEL
,    DATES
HAVING    APPROVAL_LEVEL = MAX(APPROVAL_LEVEL)

UPDATE    #approvals    
SET    approval_date_count = (
    SELECT COUNT(1)
    FROM #filtered_approval_dates
    WHERE #filtered_approval_dates.nomination_id = #approvals.nomination_id
    GROUP BY #filtered_approval_dates.nomination_id    
)

UPDATE    #approvals
SET    approval_date = (
    SELECT     dates
    FROM    #filtered_approval_dates
    WHERE    #approvals.nomination_id = #filtered_approval_dates.NOMINATION_ID
    AND    #approvals.approval_date_count = 1
)

UPDATE    #approvals
SET    program_name = PROGRAM.PROGRAM_NAME
FROM    NOMINATION
,    PROGRAM
WHERE    #approvals.program_id = PROGRAM.PROGRAM_ID

-- Update the award type
INSERT INTO #award_types (
    nomination_id
,    award_type
)
SELECT
    a.nomination_id
,    CASE
        WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN
            CASE
                  WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_NAME
            END
         ELSE
            CASE
                WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points' THEN 'Points'
                ELSE PI.PAYOUT_ITEM_DESC
             END
        END AS award_type
FROM    PAYOUT_ITEM PI
RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
    ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
RIGHT OUTER JOIN #approvals a WITH (NOLOCK)
    ON RB.ID  = a.budget_id

UPDATE  #approvals
SET #approvals.award_type = a.award_type
FROM    #approvals
INNER JOIN #award_types A ON #approvals.nomination_id = a.nomination_id

END

GO

