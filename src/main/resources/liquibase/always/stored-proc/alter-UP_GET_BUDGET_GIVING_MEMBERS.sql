
/****** Object:  StoredProcedure [component].[UP_GET_BUDGET_GIVING_MEMBERS]    Script Date: 6/18/2018 8:40:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_BUDGET_GIVING_MEMBERS
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS A LIST OF THE GIVING MEMBERS FOR EACH BUDGET FOR A GIVEIN
-- PROGRAM ID.
--
-- Params:
-- @programId = the program id used to find all budgets and their giving members
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- IRWINMB1      20180618    MP-12314     CREATED - TO HELP ADDRESS CAESARS
--                                            PERFORMANCE ISSUES
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_BUDGET_GIVING_MEMBERS]
    @programId BIGINT

AS
BEGIN
    SET NOCOUNT ON;

    WITH 
    MY_PROGRAM_BUDGET(ID, BUDGET_ID) AS
    (
        SELECT ID, BUDGET_ID
        FROM component.PROGRAM_BUDGET
        WHERE PROGRAM_ID = @programId
    ),
    PAX_LIST(PAX_ID, BUDGET_ID) AS 
    (
        SELECT SUBJECT_ID
        , MY_PROGRAM_BUDGET.BUDGET_ID
        FROM component.ACL
        JOIN MY_PROGRAM_BUDGET ON MY_PROGRAM_BUDGET.ID = ACL.TARGET_ID
            AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
        JOIN component.ROLE ON ROLE.ROLE_ID = ACL.ROLE_ID
            AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
        WHERE ACL.SUBJECT_TABLE = 'PAX'
    ),
    GROUP_LIST(GROUP_ID, BUDGET_ID) AS
    (
        SELECT SUBJECT_ID
        , MY_PROGRAM_BUDGET.BUDGET_ID
        FROM component.ACL
        JOIN MY_PROGRAM_BUDGET ON MY_PROGRAM_BUDGET.ID = ACL.TARGET_ID
            AND ACL.TARGET_TABLE = 'PROGRAM_BUDGET'
        JOIN component.ROLE ON ROLE.ROLE_ID = ACL.ROLE_ID
            AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
        WHERE ACL.SUBJECT_TABLE = 'GROUPS'
    ),
    ELIGIBILE_ENTITIES AS
    (
        SELECT GROUP_LIST.BUDGET_ID, VW_USER_GROUPS_SEARCH_DATA.* 
        FROM component.VW_USER_GROUPS_SEARCH_DATA
        JOIN GROUP_LIST ON GROUP_LIST.GROUP_ID = VW_USER_GROUPS_SEARCH_DATA.GROUP_ID
        UNION
        SELECT PAX_LIST.BUDGET_ID, VW_USER_GROUPS_SEARCH_DATA.* 
        FROM component.VW_USER_GROUPS_SEARCH_DATA
        JOIN PAX_LIST ON PAX_LIST.PAX_ID = VW_USER_GROUPS_SEARCH_DATA.PAX_ID
    )
    SELECT *
    FROM ELIGIBILE_ENTITIES
    ORDER BY BUDGET_ID

END

GO