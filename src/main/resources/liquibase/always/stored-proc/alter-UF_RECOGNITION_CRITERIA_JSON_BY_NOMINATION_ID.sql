
-- ==============================  NAME  ======================================
-- UF_RECOGNITION_CRITERIA_JSON_BY_NOMINATION_ID
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A NOMINATION_ID
-- RETURN A JSON OF RECOGNITION_CRITERIA(S)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- YELLAMS        20160310                CREATED
--
-- ===========================  DECLARATIONS  =================================
-- SElECT [component].[UF_RECOGNITION_CRITERIA_JSON_BY_NOMINATION_ID](80809)
ALTER FUNCTION [component].[UF_RECOGNITION_CRITERIA_JSON_BY_NOMINATION_ID] (
    @nomination_id BIGINT)
RETURNS NVARCHAR(MAX)
AS
BEGIN

DECLARE    @recCriteria NVARCHAR(MAX)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZE
SET    @recCriteria = NULL



SET @recCriteria = (
    SELECT '{"id":'+CONVERT(NVARCHAR,RC.ID)+', "displayName":"'+ RC.RECOGNITION_CRITERIA_NAME+'"},'
    FROM RECOGNITION_CRITERIA RC
    INNER JOIN NOMINATION_CRITERIA NC ON NC.RECOGNITION_CRITERIA_ID = RC.ID
    AND  NC.NOMINATION_ID =@nomination_id
    ORDER BY RC.ID
    FOR XML PATH('')
    )

IF @recCriteria IS NOT NULL
BEGIN
    SET @recCriteria = '['+LEFT(@recCriteria, LEN(@recCriteria) - 1)+']'
    
    --Fix for &amp; in the Criteria Values
    SET @recCriteria = REPLACE(@recCriteria, '&amp;', '&')

END

-- RETURN VALUE
RETURN @recCriteria

END
