/****** Object:  StoredProcedure [component].[UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT]    Script Date: 10/15/2015 11:11:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, ACTIVITY_DATE_FROM, ACTIVITY_DATE_THRU
-- RETURN (TODO)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20110921                CREATED
-- DOHOGNTA        20150120                REMOVE SUCCESSOR_ID DEPENDENCIES
-- ERICKSRT        20150818                UPDATE PRODUCT_TYPE_NAME AND PRODUCT_CATAGORY_NAME
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT]
    @pax_group_id INT
,    @activity_date_from DATETIME2
,    @activity_date_thru DATETIME2
AS
BEGIN

SET NOCOUNT ON

DECLARE @count_product_category INT
DECLARE @count_product_type INT
DECLARE @count_product INT
DECLARE @count_top INT

DECLARE @result_set TABLE (
    label NVARCHAR(40)
,    code NVARCHAR(4)
,    value INT
,    type NVARCHAR(20)
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- determine if there are different category code or product types
SELECT
    @count_product_category = COUNT(DISTINCT PRODUCT.PRODUCT_CATEGORY_CODE)
,    @count_product_type = COUNT(DISTINCT PRODUCT.PRODUCT_TYPE_CODE)
,    @count_product = COUNT(DISTINCT PRODUCT.PRODUCT_ID)
FROM
    SALE WITH (NOLOCK)
JOIN TRANSACTION_HEADER WITH (NOLOCK)
    ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
JOIN PAX_GROUP WITH (NOLOCK)
    ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
    AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
JOIN SALE_ITEM WITH (NOLOCK)
    ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
JOIN PRODUCT WITH (NOLOCK)
    ON SALE_ITEM.SKU = PRODUCT.SKU
    AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
WHERE
    TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru

-- product categories are multiple
IF (@count_product_category > 1)
BEGIN 
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT_CATEGORY.PRODUCT_CATEGORY_DESC
    ,    PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT_CATEGORY' 
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    JOIN PRODUCT_CATEGORY WITH (NOLOCK)
        ON PRODUCT.PRODUCT_CATEGORY_CODE = PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    WHERE
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT_CATEGORY.PRODUCT_CATEGORY_DESC
    ,    PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    ORDER BY
        SUM(SALE_ITEM.QUANTITY) DESC

    IF (@count_product_category > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        JOIN PRODUCT_CATEGORY WITH (NOLOCK)
            ON PRODUCT.PRODUCT_CATEGORY_CODE = PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END
ELSE IF (@count_product_type > 1)
BEGIN
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT_TYPE.PRODUCT_TYPE_DESC
    ,    PRODUCT_TYPE.PRODUCT_TYPE_CODE
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT_TYPE'
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    JOIN PRODUCT_TYPE WITH (NOLOCK)
        ON PRODUCT.PRODUCT_TYPE_CODE = PRODUCT_TYPE.PRODUCT_TYPE_CODE
    WHERE
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT_TYPE.PRODUCT_TYPE_DESC
    ,    PRODUCT_TYPE.PRODUCT_TYPE_CODE
    ORDER BY
        SUM(SALE_ITEM.QUANTITY) DESC

    -- prod type count more than 5 
    IF (@count_product_type > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        JOIN PRODUCT_TYPE WITH (NOLOCK)
            ON PRODUCT.PRODUCT_TYPE_CODE = PRODUCT_TYPE.PRODUCT_TYPE_CODE
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END
ELSE 
BEGIN
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT.PRODUCT_NAME
    ,    ''            
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT'
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    WHERE    
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT.PRODUCT_NAME

    IF (@count_product > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END

SELECT * FROM @result_set 

END
GO