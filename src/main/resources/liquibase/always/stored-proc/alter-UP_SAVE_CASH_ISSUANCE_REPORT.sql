
-- ==============================  NAME  ===============================================
-- UP_SAVE_CASH_ISSUANCE_REPORT
-- ===========================  DESCRIPTION  ===========================================
-- SAVE THE CSV REPORT.
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20170329    MP-8927        CREATED
--
-- ===========================  DECLARATIONS  ==========================================
ALTER PROCEDURE [component].[UP_SAVE_CASH_ISSUANCE_REPORT]
(@inputBatchId BIGINT,
 @file NVARCHAR(MAX),
 @fileName NVARCHAR(MAX))

AS 
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

DECLARE @intErrorCode INT
DECLARE @batch_id BIGINT
DECLARE @payout_id BIGINT
DECLARE @intNumberOfPayouts_id BIGINT

    BEGIN
        BEGIN TRANSACTION
        INSERT INTO  [component].[BATCH_FILE] (BATCH_ID,FILENAME,FILE_UPLOAD) VALUES(@inputBatchId,@fileName,@file);

        SELECT @intErrorCode = @@ERROR
        IF (@intErrorCode <> 0) GOTO PROBLEM_SAVE_FILE
        
        COMMIT TRANSACTION

        SELECT @inputBatchId AS 'BATCH_ID', @intNumberOfPayouts_id AS 'NUMBER_OF_PAYOUTS', @intErrorCode AS 'ERROR_CODE'

        PROBLEM_SAVE_FILE:
             IF (@intErrorCode <> 0) 
             BEGIN
                PRINT 'Unexpected error occurred!'
                ROLLBACK TRANSACTION
                UPDATE  [component].[BATCH] SET STATUS_TYPE_CODE='ERROR' WHERE BATCH_ID= @inputBatchId
            END
    END--ACTION

END --SP
GO