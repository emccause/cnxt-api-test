
/****** Object:  StoredProcedure [component].[UP_BUDGET_USED_BY_PAX]    Script Date: 07/10/2017 16:35:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_BUDGET_USED_BY_PAX
-- ===========================  DESCRIPTION  ==================================
--
-- Returns a total of how much budget has been used by the list of passed
-- in pax ids, with respect to the given date range
--
-- BUDGET ID
-- BUDGET USED IN PERIOD
-- BUDGET USED OUT OF PERIOD
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170710    MP-10404    CREATED
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_USED_BY_PAX]
@delim_pax_id_list VARCHAR(MAX),
@start_date DATETIME2(7),
@end_date DATETIME2(7)

AS
BEGIN
SET NOCOUNT ON

DECLARE @delimiter CHAR(1) = ',';

-------------------------------------------- CALCULATE TOTAL AMOUNT USED --------------------------------------------
SELECT BUDGET_ID, STATUS_TYPE_CODE, 
ISNULL(SUM(ISNULL(TOTAL_USED_IN_PERIOD,0)),0) AS USED_IN_PERIOD, 
ISNULL(SUM(ISNULL(TOTAL_USED_OUTSIDE_PERIOD,0)),0) AS USED_OUT_OF_PERIOD
FROM (
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    SELECT DISTINCT ISNULL(DEBITS.ID, ISNULL(CREDITS.ID, ISNULL(DEBITS_OUTSIDE_PERIOD.ID, ISNULL(CREDITS_OUTSIDE_PERIOD.ID, 0)))) AS 'BUDGET_ID',
    CASE
        WHEN B.FROM_DATE > GETDATE() THEN 'SCHEDULED'
        WHEN B.THRU_DATE < GETDATE() THEN 'ENDED'
        ELSE B.STATUS_TYPE_CODE
    END AS 'STATUS_TYPE_CODE',
    (CASE
        WHEN DEBITS.DEBITS IS NULL THEN 0
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS IS NULL THEN 0
        ELSE CREDITS.CREDITS
    END)
    AS 'TOTAL_USED_IN_PERIOD',  
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD CALC-------------------------------------------
    (CASE
        WHEN DEBITS_OUTSIDE_PERIOD.DEBITS IS NULL THEN 0
        ELSE DEBITS_OUTSIDE_PERIOD.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS_OUTSIDE_PERIOD.CREDITS IS NULL THEN 0
        ELSE CREDITS_OUTSIDE_PERIOD.CREDITS
    END)
    AS 'TOTAL_USED_OUTSIDE_PERIOD'  
    FROM component.DISCRETIONARY DISC
    LEFT JOIN component.BUDGET B
        ON (
            DISC.BUDGET_ID_CREDIT = B.ID
            OR DISC.BUDGET_ID_DEBIT = B.ID
        )
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN ( --DEBITS IN PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.DISCRETIONARY disc_debit
        JOIN component.TRANSACTION_HEADER TH 
            ON TH.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND TH.TYPE_CODE = 'DISC' 
            AND TH.SUB_TYPE_CODE <> 'FUND' 
            AND TH.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
        JOIN component.RECOGNITION r ON r.ID = disc_debit.TARGET_ID AND disc_debit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID
        WHERE disc_debit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        AND EXISTS ( --Only count things the specified pax has submitted
            SELECT items
            FROM component.UF_LIST_TO_TABLE_ID(@delim_pax_id_list, @delimiter)
            WHERE items = n.SUBMITTER_PAX_ID
        )
        GROUP BY disc_debit.BUDGET_ID_DEBIT
    ) DEBITS ON B.ID = DEBITS.ID
    LEFT JOIN ( --CREDITS IN PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.DISCRETIONARY disc_credit
        JOIN component.TRANSACTION_HEADER TH 
            ON TH.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND TH.TYPE_CODE = 'DISC' 
            AND TH.SUB_TYPE_CODE <> 'FUND' 
            AND TH.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
        WHERE disc_credit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
    ) CREDITS ON B.ID = CREDITS.ID        
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN ( --DEBITS OUTSIDE PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.DISCRETIONARY disc_debit
        JOIN component.TRANSACTION_HEADER TH 
            ON TH.ID = disc_debit.TRANSACTION_HEADER_ID 
            AND TH.TYPE_CODE = 'DISC' 
            AND TH.SUB_TYPE_CODE <> 'FUND' 
            AND TH.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
        JOIN component.RECOGNITION r ON r.ID = disc_debit.TARGET_ID AND disc_debit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID
        WHERE disc_debit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        AND EXISTS ( --Only count things the specified pax has submitted
            SELECT items
            FROM component.UF_LIST_TO_TABLE_ID(@delim_pax_id_list, @delimiter)
            WHERE items = n.SUBMITTER_PAX_ID
        )
        GROUP BY disc_debit.BUDGET_ID_DEBIT
    ) DEBITS_OUTSIDE_PERIOD ON B.ID = DEBITS_OUTSIDE_PERIOD.ID
    LEFT JOIN ( --CREDITS OUTSIDE PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.DISCRETIONARY disc_credit
        JOIN component.TRANSACTION_HEADER TH 
            ON TH.ID = disc_credit.TRANSACTION_HEADER_ID 
            AND TH.TYPE_CODE = 'DISC' 
            AND TH.SUB_TYPE_CODE <> 'FUND' 
            AND TH.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
        WHERE disc_credit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
    ) CREDITS_OUTSIDE_PERIOD ON B.ID = CREDITS_OUTSIDE_PERIOD.ID 
) TOTALS WHERE BUDGET_ID <> 0 GROUP BY BUDGET_ID, STATUS_TYPE_CODE

END

GO