SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[component].[UF_GET_SERVICE_ANNIVERSARIES]') AND type IN (N'FN', N'IF', N'TF'))
BEGIN
    DROP FUNCTION [component].[UF_GET_SERVICE_ANNIVERSARIES]
END
GO


-- ==============================  NAME  ======================================
-- Function: UF_GET_SERVICE_ANNIVERSARIES       
-- ===========================  DESCRIPTION  ==================================
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- FLORESC   	 2021-06-24				  Get service Annivarsaries from milestone programs

CREATE FUNCTION [component].[UF_GET_SERVICE_ANNIVERSARIES] (@day_before_or_after bigint)
RETURNS @result_set TABLE (
MONTH_DAY varchar(30),
  YEARS_OF_SERVICE bigint,
SA_MONTH bigint,
MILESTONE_DATE date,
CONTROL_NUM varchar(150),
FIRST_NAME nvarchar(150),
LAST_NAME nvarchar(150),
STATUS varchar(50),
PAX_ID bigint,
TRANSACTION_ID bigint,
SUBMITTED_DATE datetime,
PROGRAM_ID bigint,
PROGRAM_NAME nvarchar(300),
AWARD_AMOUNT varchar(300),
NOMINATION_ID bigint,
BUDGET_ID bigint,
BUDGET_NAME nvarchar(150)
  )
AS
BEGIN

declare @program_id_active as table(
PROGRAM_ID nvarchar(30)
)
insert into @program_id_active 
select distinct p.PROGRAM_ID from component.PROGRAM p join component.PROGRAM_ACTIVITY pa on p.PROGRAM_ID = pa.PROGRAM_ID and pa.PROGRAM_ACTIVITY_NAME = 'SERVICE_ANNIVERSARY'
where p.PROGRAM_TYPE_CODE = 'MILESTONE' and (p.THRU_DATE is null or p.THRU_DATE > GETDATE());

declare @program_id_years as table(
PROGRAM_ID nvarchar(30),
YEARS_OF_SERVICE nvarchar(30)
)
insert into @program_id_years 
select pa.PROGRAM_ID,
case when (pam.VF_NAME = 'REPEATING' and pam.MISC_DATA = 'true') then 'EVERY_YEAR'
else pam.MISC_DATA 
end as 'YEARS_OF_SERVICE'

from component.PROGRAM_ACTIVITY pa 
join component.PROGRAM_ACTIVITY_MISC pam on pa.PROGRAM_ACTIVITY_ID = pam.PROGRAM_ACTIVITY_ID 
where
pa.PROGRAM_ID in (select * from @program_id_active) 
and (pam.VF_NAME ='YEARS_OF_SERVICE' or (pam.VF_NAME = 'REPEATING' and pam.MISC_DATA = 'true'))

declare @current_month_day as nvarchar(100);
set @current_month_day = '%'+ (FORMAT (getdate()+(@day_before_or_after), 'MM-dd'))

declare @current_year as nvarchar(100);
set @current_year = year(getdate()+(@day_before_or_after))

 DECLARE @groupsPaxData TABLE (PROGRAM_ID bigint, SUBJECT_TABLE nvarchar(30), SUBJECT_ID bigint)
 INSERT INTO @groupsPaxData
 select TARGET_ID, SUBJECT_TABLE, SUBJECT_ID from component.ACL acl
 join component.ROLE r on acl.ROLE_ID = r.ROLE_ID and r.ROLE_CODE ='PREC'--Receivers
 where acl.TARGET_TABLE = 'PROGRAM' 
 and acl.TARGET_ID in (
 select * from @program_id_active 
) 
 and acl.SUBJECT_TABLE in ('GROUPS','PAX');

declare @table1 as table(
YEARS_OF_SERVICE varchar(30),
SA_MONTH bigint,
MILESTONE_DATE date,
CONTROL_NUM varchar(150),
FIRST_NAME nvarchar(150),
LAST_NAME nvarchar(150),
STATUS varchar(50),
PAX_ID bigint,
TRANSACTION_ID bigint,
SUBMITTED_DATE datetime,
PROGRAM_ID bigint,
PROGRAM_NAME nvarchar(300),
AWARD_AMOUNT varchar(300),
NOMINATION_ID bigint,
BUDGET_ID bigint,
BUDGET_NAME nvarchar(150)
)

declare @programPax as table(
PROGRAM_ID bigint,
PAX_ID bigint
)
insert into @programPax
select  tgpd.PROGRAM_ID, gp.PAX_ID from component.GROUPS_PAX gp join @groupsPaxData tgpd on gp.GROUP_ID = tgpd.SUBJECT_ID and tgpd.SUBJECT_TABLE = 'GROUPS'
union all 
(select PROGRAM_ID, SUBJECT_ID from @groupsPaxData where SUBJECT_TABLE = 'PAX')


insert @table1 (YEARS_OF_SERVICE,SA_MONTH,MILESTONE_DATE, CONTROL_NUM, FIRST_NAME, LAST_NAME,STATUS, PAX_ID 
,TRANSACTION_ID, SUBMITTED_DATE, PROGRAM_ID, PROGRAM_NAME, AWARD_AMOUNT,NOMINATION_ID,BUDGET_ID, BUDGET_NAME
)

SELECT 
(SELECT DATEDIFF(year,pm.MISC_DATA,getDAte())) as 'YEARS OF SERVICE',
(SELECT FORMAT (CAST(pm.MISC_DATA as date), 'MM') as date) as SA_MONTH,pm.MISC_DATA AS MILESTONE_DATE,p.CONTROL_NUM, p.FIRST_NAME, p.LAST_NAME,
s.STATUS_TYPE_CODE as 'STATUS',
 pm.PAX_ID, td.TRANSACTION_ID, td.SUBMITTED_DATE, pp.PROGRAM_ID, td.PROGRAM_NAME, td.AWARD_AMOUNT, td.NOMINATION_ID, td.BUDGET_ID, td.BUDGET_NAME
 
	   FROM component.PAX_MISC  pm
	   join component.SYS_USER s on pm.PAX_ID = s.PAX_ID 
	   join component.PAX p on pm.PAX_ID = p.PAX_ID	   
	   join @programPax pp on p.PAX_ID = pp.PAX_ID 	   
	   left join component.VW_TRANSACTION_DETAILS td on p.PAX_ID = td.RECIPIENT_PAX_ID
	   and td.PROGRAM_ID  = pp.PROGRAM_ID
	   and year(td.SUBMITTED_DATE) = @current_year
	   where pm.VF_NAME = 'HIRE_DATE'       
	   and pm.MISC_DATA like @current_month_day
	   and  s.STATUS_TYPE_CODE not in ('INACTIVE', 'RESTRICTED')
	   order by SA_MONTH, pm.MISC_DATA;

insert into @result_set
select  distinct substring(right(cast(milestone_date as varchar(23)), 10),6,9) as MONTH_DAY, t.* 
from @table1 t 
join @program_id_years y on t.PROGRAM_ID = y.PROGRAM_ID where t.YEARS_OF_SERVICE = y.YEARS_OF_SERVICE 
or( y.YEARS_OF_SERVICE = 'EVERY_YEAR' )
  
order by MONTH_DAY; 

-- RETURN RESULT SET
RETURN


END