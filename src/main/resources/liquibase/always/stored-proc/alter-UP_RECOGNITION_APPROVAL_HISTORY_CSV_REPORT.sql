/****** Object:  StoredProcedure [component].[UP_RECOGNITION_APPROVAL_HISTORY_CSV_REPORT]    Script Date: 10/21/2020 10:54:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_APPROVAL_HISTORY_CSV_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS APPROVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID 
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE		STORY	CHANGE DESCRIPTION
-- SCHIERVR	20081030                CREATED
-- THOMSOND     20081125                MODIFIED OUTPUT SELECT STATEMENT
-- KOTHAH       20110916                CHANGED TO GET THE AWARD TYPES
-- BESSBS       20130426                ADD FROM AND THRU DATE TO RESULTS
-- MUDDAM       20151113                UDM4 CHANGES
-- WRIGHTM      20151117                CHANGED SATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
-- burgestl	20201021		update join to sql 2017 
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_APPROVAL_HISTORY_CSV_REPORT]
    @approver_pax_group_id INT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- DRIVER
CREATE TABLE #approvals (
    submittal_date DATETIME2 NULL

,    receiver_list VARCHAR(1000) NULL
,    multiple_recipient_flag INT NULL DEFAULT 0
,    receiver_name VARCHAR(80) NULL

,    approval_process_id INT NULL
,    denial_reason NVARCHAR(MAX)
,    approval_count INT NULL
,    approval_date_count INT NULL
,    approval_date DATETIME2 NULL

,    submitter_list VARCHAR(1000) NULL
,    multiple_submitter_flag INT NULL DEFAULT 0
,    submitter_name VARCHAR(80) NULL

,    next_approver_name VARCHAR(80) NULL
,    next_approver_count INT NULL

,    ecard_id INT NULL
,    recognition_id    INT NULL
,    justification_enabled_flag INT NULL DEFAULT 0
,    justification_comment_available_flag INT NULL DEFAULT 0

,    nomination_status NVARCHAR(10) NULL

,    criteria NVARCHAR(1024) NULL

,    total_points INT NULL
,    total_pending_points INT NULL
,    total_denied_points INT NULL
,    total_approved_points INT NULL

,    total_recognition_amount INT NULL
,    nomination_id INT NOT NULL

,    program_id INT NULL
,    program_name NVARCHAR(40) NULL
,    award_type NVARCHAR(40) NULL
,    budget_id INT NULL
)

CREATE TABLE #award_types (
    nomination_id INT NULL
,    award_type NVARCHAR(40) NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_APPROVAL_HISTORY_BASE @approver_pax_group_id, @submittal_date_from, @submittal_date_thru, @locale_code

UPDATE #approvals
SET    nomination_status =
    CASE
        WHEN nomination_status = 'PENDING' THEN 'Pending'
        WHEN nomination_status = 'APPROVED' THEN 'Approved'
        WHEN nomination_status = 'REJECTED' THEN 'Denied'
        WHEN nomination_status = 'COMPLETE' THEN 'Complete'
    END
,    submitter_name =
    CASE WHEN multiple_submitter_flag = 1 THEN 'Multiple' 
         ELSE submitter_name
    END
,    receiver_name =
    CASE WHEN multiple_recipient_flag = 1 THEN 'Multiple' 
         ELSE receiver_name
    END
,    next_approver_name = 
    CASE WHEN next_approver_count > 1 THEN 'Multiple'
         ELSE next_approver_name
    END

-- FORMAT THE OUTPUT
SELECT
    #approvals.submittal_date
,    receiver_name
,    NOMINATION.COMMENT
,    NOMINATION.COMMENT_JUSTIFICATION
,    component.UF_TRANSLATE (@locale_code, nomination_status, 'Reporting', 'Reporting') AS nomination_status
,    denial_reason
,    next_approver_name
,    approval_date
,    submitter_name
,    component.UF_TRANSLATE (@locale_code, program, 'PROGRAM', 'program') AS program_name
,    criteria
,    CASE
        WHEN award_type = 'Points' 
        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
        ELSE component.UF_TRANSLATE (@locale_code, award_type, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC')
    END as award_type
,    total_points 
,    nomination_id
FROM    #approvals
INNER JOIN NOMINATION ON #approvals.nomination_id = NOMINATION.ID
ORDER BY
    submittal_date DESC

SELECT
    ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ') AS EMPLOYEE
FROM PAX
JOIN PAX_GROUP
    ON PAX_GROUP.PAX_ID = PAX.PAX_ID AND PAX_GROUP.PAX_GROUP_ID = @approver_pax_group_id

SELECT @submittal_date_from AS FROM_DATE, @submittal_date_thru AS THRU_DATE

END


GO
