/****** Object:  StoredProcedure [component].[UP_TRANSLATION_VALUES_EXTRACT]    Script Date: 10/15/2015 11:12:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_TRANSLATION_VALUES_EXTRACT
-- ===========================  DESCRIPTION  ==================================
-- GET A DUMP OF EVERYTHING IN THE TRANSLATION TABLES SO WE CAN GET A CSV 
-- FOR IMPORT/EXPORT
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20130328                CREATED
-- DOHOGNTA        20150119                CHANGE GLOBAL TEMPORARY TABLE TO LOCAL TEMPORARY TABLE
-- DOHOGNTA        20150306                REMOVE ORG_ROLE_MISC TABLE REFERENCE
-- HARWELLM        20160801                REPLACE ACTIVE COLUMN WITH STATUS_TYPE_CODE
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_TRANSLATION_VALUES_EXTRACT]
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

CREATE TABLE #results(
   DISPLAY_NAME NVARCHAR(40) NOT NULL
,   PHRASE NVARCHAR(MAX) NULL
,   LOCALE_CODE NVARCHAR(6) NULL
,   TRANSLATIONS NVARCHAR(MAX) NULL);

DECLARE @displayName VARCHAR(100)
DECLARE @vfName VARCHAR(100)
DECLARE @tableName VARCHAR(50)
DECLARE @columnName VARCHAR(50)
DECLARE @localeCode VARCHAR(10)
DECLARE @toTranslate VARCHAR(50)
DECLARE @translations VARCHAR(50)
DECLARE @CNT INT 

DECLARE translation_cursor CURSOR FOR 
SELECT
   DISPLAY_NAME
,   TABLE_NAME
,   COLUMN_NAME
FROM
   TRANSLATABLE
WHERE
   STATUS_TYPE_CODE = 'ACTIVE'

DECLARE @sql VARCHAR(500)

OPEN translation_cursor 
FETCH NEXT FROM translation_cursor INTO @displayName, @tableName, @columnName
WHILE @@FETCH_STATUS = 0
BEGIN

IF (EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'component.' AND TABLE_NAME = @tablename))
BEGIN
   IF (@tableName = 'AnswerOption' OR @tableName = 'Question' OR @tableName = 'NEWSFEED_TYPE' OR @tableName = 'TestSection')
   BEGIN
       SET @sql = 'INSERT INTO #results SELECT ''' + @displayName + ''',' + @columnName + ',''en_US'', '''' FROM component..' + @tablename + ' WHERE ' + @columnName + ' IS NOT NULL'
    END
    ELSE IF (@tableName = 'PAX_MISC' OR @tableName='PAX_GROUP_MISC')    
    BEGIN
    SET @vfName = 'VF_NAME'
       SET @sql = 'INSERT INTO #results SELECT ''' + @displayName+' - ' + '''+' + @vfName + ',' + @columnName + ',''en_US'', '''' FROM component..' + @tablename + ' WHERE ' + @columnName + ' IS NOT NULL'
    END
    ELSE
    BEGIN
       SET @sql = 'INSERT INTO #results SELECT DISTINCT ''' + @displayName + ''',' + @columnName + ',''en_US'',   '''' FROM component..' + @tablename + ' WHERE ' + @columnName + ' IS NOT NULL'
    END
    
    EXEC (@sql )
END   
   FETCH NEXT FROM translation_cursor INTO @displayName, @tableName, @columnName
END  
CLOSE translation_cursor
DEALLOCATE translation_cursor

SELECT
    '[!"' + DISPLAY_NAME + '"' AS DISPLAY_NAME
,    '[!"' + CAST(PHRASE AS NVARCHAR(MAX)) + '"' AS PHRASE
,    LOCALE_CODE
,    TRANSLATIONS
FROM
   #results

DROP TABLE #results
END

GO

