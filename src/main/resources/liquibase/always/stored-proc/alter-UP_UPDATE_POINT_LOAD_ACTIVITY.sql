/****** Object:  StoredProcedure [UP_UPDATE_POINT_LOAD_ACTIVITY]    Script Date: 10/15/2015 11:13:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_UPDATE_POINT_LOAD_ACTIVITY
-- ===========================  DESCRIPTION  ==================================
--
-- UPDATES THE STATUS OF POINT UPLOAD BATCH RECORDS.
-- THIS ALLOWS POINTS TO BE CANCELLED, REPROCESSED, OR RELEASED
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- YUKIOM        20150708                CREATED
-- HARWELLM        20150928                AN ACTION OF 'REPROCESS' SHOULD SET THE BATCH STATUS
--                                        TO NEW INSTEAD OF IN_PROGRESS
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151116                UDM4 CHANGES
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- FARFANLE        20180214    MP-11136    ADD PPP_INDEX_APPLIED FLAG
-- ARENASW        20180424    MP-11970    FIX ISSUE WHEN HANDLING DUPLICATED BATCH EVENTS STATUS CODE

-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_UPDATE_POINT_LOAD_ACTIVITY]
    @batchId BIGINT,
    @action NVARCHAR(MAX)

AS
  BEGIN

    SET NOCOUNT ON

    --Table for point load activities (Result table)
    CREATE TABLE #POINT_LOAD_ACTIVITY (
      BATCH_ID BIGINT PRIMARY KEY,
      STATUS NVARCHAR(MAX),
      START_DATE NVARCHAR(MAX),
      PROCESS_DATE NVARCHAR(MAX),
      FILE_NAME NVARCHAR(MAX),
      TOTAL_RECORDS BIGINT,
      GOOD_RECORDS BIGINT,
      ERROR_RECORDS BIGINT,
      TOTAL_POINTS BIGINT,
      PPP_INDEX_APPLIED NVARCHAR(MAX)
    )

    --Temp variable for statuses
    DECLARE @STATUS NVARCHAR(MAX),
            @TRANSACTION_STATUS_CODE NVARCHAR(MAX),
            @ACTIONS NVARCHAR(MAX)

    SET @ACTIONS = 'CANCEL,RELEASE,REPROCESS,FAILED_RELEASE,FAILED_CANCEL'

    --Set up actions
    CREATE TABLE #ACTIONS_TABLE  (
      ACTION NVARCHAR(MAX)
    )

    INSERT INTO #ACTIONS_TABLE (
      ACTION
    )
      SELECT * FROM [component].UF_LIST_TO_TABLE(@ACTIONS, ',')

    --Calculate total points for each batch
    CREATE TABLE #POINTS (
      BATCH_ID BIGINT PRIMARY KEY,
      TOTAL_POINTS BIGINT
    )

    INSERT INTO #POINTS (
      BATCH_ID,
      TOTAL_POINTS
    )
      SELECT th.BATCH_ID,
        ISNULL(SUM(ISNULL(DISC.AMOUNT, 0)), 0)
      FROM TRANSACTION_HEADER th
        JOIN BATCH batch
          ON batch.BATCH_ID = th.BATCH_ID
             AND batch.BATCH_TYPE_CODE = 'CPTD'
        LEFT JOIN DISCRETIONARY DISC
          ON th.ID = DISC.TRANSACTION_HEADER_ID
      WHERE th.BATCH_ID IS NOT NULL
      GROUP BY th.BATCH_ID

    --Check action (safety measure)
    IF EXISTS (SELECT TOP 1 ACTION FROM #ACTIONS_TABLE WHERE @action = ACTION)
      BEGIN
        --Setup the correct statuses depending on the action
        IF @action = 'CANCEL'
          BEGIN
            SET @STATUS = 'CANCELLED'
            SET @TRANSACTION_STATUS_CODE = 'REJECTED'
          END
        ELSE IF @action = 'REPROCESS'
          BEGIN
            SET @STATUS = 'NEW'
            SET @TRANSACTION_STATUS_CODE = 'PENDING'
          END
        ELSE IF @action = 'RELEASE'
          BEGIN
            SET @STATUS = 'COMPLETE'
            SET @TRANSACTION_STATUS_CODE = 'APPROVED'
          END
        ELSE IF @action = 'FAILED_RELEASE'
          BEGIN
            SET @STATUS = 'FAILED_RELEASE'
            SET @TRANSACTION_STATUS_CODE = NULL
          END
        ELSE IF @action = 'FAILED_CANCEL'
          BEGIN
            SET @STATUS = 'FAILED_CANCEL'
            SET @TRANSACTION_STATUS_CODE = NULL
          END

        --Insert the point load activity information
        INSERT INTO #POINT_LOAD_ACTIVITY (
          BATCH_ID,
          START_DATE,
          PROCESS_DATE,
          FILE_NAME,
          TOTAL_RECORDS,
          GOOD_RECORDS,
          ERROR_RECORDS,
          TOTAL_POINTS,
          PPP_INDEX_APPLIED
        )
        select BATCH_ID,START_TIME,PROCESSED_TIME,FILE_NAME,TOTAL_RECS,OK_RECS, ERR_RECS,total_points,PPP_INDEX_APPLIED from (
          SELECT batch.BATCH_ID,
            start_time_event.MESSAGE AS 'START_TIME',
            process_time_event.message AS 'PROCESSED_TIME',
            file_name_event.message AS 'FILE_NAME',
            total_recs_event.message  AS 'TOTAL_RECS',
            good_recs_event.message AS 'OK_RECS',
            err_recs_event.message AS 'ERR_RECS',
            #POINTS.TOTAL_POINTS AS 'TOTAL_POINTS',
            pppx_applied_event.message  AS 'PPP_INDEX_APPLIED',
            ROW_NUMBER() over (partition by batch.batch_id 
                  order by batch.batch_id, ISNULL(start_time_event.MESSAGE, CONVERT(DATETIME2(0), batch.CREATE_DATE, 120)) desc) as ROWNUMBER
          FROM BATCH batch
            LEFT JOIN BATCH_EVENT start_time_event
              ON batch.BATCH_ID = start_time_event.BATCH_ID
                 AND start_time_event.BATCH_EVENT_TYPE_CODE = 'STRTIM'
            LEFT JOIN BATCH_EVENT process_time_event
              ON batch.BATCH_ID = process_time_event.BATCH_ID
                 AND process_time_event.BATCH_EVENT_TYPE_CODE = 'ENDTIM'
            LEFT JOIN BATCH_EVENT file_name_event
              ON batch.BATCH_ID = file_name_event.BATCH_ID
                 AND file_name_event.BATCH_EVENT_TYPE_CODE = 'FILE'
            LEFT JOIN BATCH_EVENT total_recs_event
              ON batch.BATCH_ID = total_recs_event.BATCH_ID
                 AND total_recs_event.BATCH_EVENT_TYPE_CODE = 'RECS'
            LEFT JOIN BATCH_EVENT good_recs_event
              ON batch.BATCH_ID = good_recs_event.BATCH_ID
                 AND good_recs_event.BATCH_EVENT_TYPE_CODE = 'OKRECS'
            LEFT JOIN BATCH_EVENT err_recs_event
              ON batch.BATCH_ID = err_recs_event.BATCH_ID
                 AND err_recs_event.BATCH_EVENT_TYPE_CODE = 'ERRS'
            LEFT JOIN #POINTS
              ON batch.BATCH_ID = #POINTS.BATCH_ID
            LEFT JOIN BATCH_EVENT pppx_applied_event
              ON batch.BATCH_ID = pppx_applied_event.BATCH_ID
                 AND pppx_applied_event.BATCH_EVENT_TYPE_CODE = 'PPP_INDEX_APPLIED'
          WHERE batch.BATCH_TYPE_CODE = 'CPTD'
                AND batch.BATCH_ID = @batchId
            )as result where ROWNUMBER = 1
            
        --Update the batch status
        UPDATE BATCH
        SET STATUS_TYPE_CODE = @STATUS
        WHERE BATCH_ID = @batchId

        IF @TRANSACTION_STATUS_CODE IS NOT NULL
          BEGIN
            UPDATE TRANSACTION_HEADER
            SET STATUS_TYPE_CODE = @TRANSACTION_STATUS_CODE
            WHERE BATCH_ID = @batchId
          END

        --Finally, update the batch information with the updated status
        UPDATE #POINT_LOAD_ACTIVITY
        SET STATUS = @STATUS
        WHERE BATCH_ID = @batchId

      END

    -- Return the updated result (stored within the POINT_LOAD_ACTIVITY table)
    SELECT
      RESULT.BATCH_ID,
      RESULT.STATUS,
      RESULT.START_DATE,
      RESULT.PROCESS_DATE,
      RESULT.FILE_NAME,
      RESULT.TOTAL_RECORDS,
      RESULT.GOOD_RECORDS,
      RESULT.ERROR_RECORDS,
      RESULT.TOTAL_POINTS,
      RESULT.PPP_INDEX_APPLIED
    FROM (SELECT * from #POINT_LOAD_ACTIVITY) RESULT

    --Drop temp tables
    DROP TABLE #ACTIONS_TABLE
    DROP TABLE #POINTS
    DROP TABLE #POINT_LOAD_ACTIVITY

  END

GO

