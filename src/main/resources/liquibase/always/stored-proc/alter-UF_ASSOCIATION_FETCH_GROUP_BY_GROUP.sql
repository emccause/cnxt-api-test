/****** Object:  UserDefinedFunction [component].[UF_ASSOCIATION_FETCH_GROUP_BY_GROUP]    Script Date: 10/16/2020 12:28:24 PM ******/
SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO
-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_GROUP_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID AND LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS)
-- RETURN ALL ASSOCIATED GROUP_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
-- 
-- NOTE: BREAK LOGIC TO TERMINATE INFINITE LOOP RESULTING FROM CIRCULAR REFERENCES IS 
-- IMPLEMENTED IN THE FORM OF A LEVEL LIMIT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE				CHANGE DESCRIPTION
-- DOHOGNTA   	20080530  				CREATED
-- DOHOGNTA   	20150309  				CHANGE IDENTITIES TO BIGINT
-- burgestl	20201021				update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_ASSOCIATION_FETCH_GROUP_BY_GROUP] (
  @group_id BIGINT
, @level_increment INT)
RETURNS @result_set TABLE (
  [id] BIGINT NULL)
AS
BEGIN

DECLARE @association_type_id INT
DECLARE @level INT
DECLARE @level_limit INT
DECLARE @level_previous INT
DECLARE @association TABLE ([association_id] BIGINT, [association_type_id] BIGINT NULL, [fk1] BIGINT NULL, [fk2] BIGINT NULL)
DECLARE @node TABLE ([id] BIGINT, [level] INT)
DECLARE @rowcount INT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET @association_type_id = 1000
SET @level = 0
SET @level_limit = 15
SET @level_previous = 0
SET @rowcount = 0

-- INSERT SEED RECORD
INSERT INTO @node ([id], [level]) SELECT @group_id, @level

SET @rowcount = @@rowcount

IF (@rowcount > 0)
BEGIN
  INSERT INTO @association ([association_id], [association_type_id], [fk1], [fk2]) SELECT ASSOCIATION_ID, ASSOCIATION_TYPE_ID, FK1, FK2 FROM ASSOCIATION WHERE ASSOCIATION_TYPE_ID = @association_type_id
END

-- PROCESS UNTIL NO MORE ROWS OR THE LEVEL LIMIT IS MET
WHILE (@rowcount > 0) AND (ABS(@level) < @level_limit) AND (@level_increment = -1)
BEGIN
  -- INCREMENT LEVEL
  SET @level_previous = @level
  SET @level = @level + @level_increment

  -- INSERT ASSOCIATED RECORD(S) FOR RECORD(S) OF PREVIOUS LEVEL
  INSERT INTO @node (
    [id]
  , [level])
  SELECT  ASSOCIATION.[fk1]
  , @level
  FROM  @association AS ASSOCIATION
  INNER JOIN @node AS NODE ON ASSOCIATION.[fk2] = NODE.[id]
  WHERE NODE.[level] = @level_previous
  
  SET @rowcount = @@rowcount
END -- WHILE

-- PROCESS UNTIL NO MORE ROWS OR THE LEVEL LIMIT IS MET
WHILE (@rowcount > 0) AND (ABS(@level) < @level_limit) AND (@level_increment = 1)
BEGIN
  -- INCREMENT LEVEL
  SET @level_previous = @level
  SET @level = @level + @level_increment

  -- INSERT ASSOCIATED RECORD(S) FOR RECORD(S) OF PREVIOUS LEVEL
  INSERT INTO @node (
    [id]
  , [level])
  SELECT  ASSOCIATION.[fk2]
  , @level
  FROM  @association AS ASSOCIATION
  INNER JOIN @node AS NODE ON ASSOCIATION.[fk1] = NODE.[id]
  WHERE NODE.[level] = @level_previous

  SET @rowcount = @@rowcount
END -- WHILE

-- RETURN DISTINCT LIST OF GROUP(S)
INSERT @result_set SELECT DISTINCT GROUPS.GROUP_ID FROM @node AS NODE INNER JOIN GROUPS ON NODE.[id] = GROUPS.GROUP_ID

-- RETURN RESULT SET
RETURN

END

GO