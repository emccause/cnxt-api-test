
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_BUDGET_PAGINATED_SEARCH
-- ===========================  DESCRIPTION  ==================================
-- Return a paginated budget list according to the filters entered.
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY    CHANGE DESCRIPTION
-- MADRIDO        20161115    MP-8553    CREATED
-- MUDDAM        20161221    MP-9183    INCLUDE "TOP" LEVEL BUDGETS THAT ALLOCATOR PAX IS TIED TO EVEN IF THEY HAVE A PARENT
-- HARWELLM        20170202    MP-9436    INCLUDE ALL CHILDREN OF OWNED BUDGETS
-- GARCIAF        20170213    MP-9471    ADD "hasAmount" PARAM
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_PAGINATED_SEARCH]
@budgetOwner NVARCHAR(MAX),
@displayName NVARCHAR(MAX),
@budgetAllocator NVARCHAR(MAX),
@scheduledIncluded NVARCHAR(5),
@activeIncluded NVARCHAR(5),
@endedIncluded NVARCHAR(5),
@archivedIncluded NVARCHAR(5),
@includeChildren  NVARCHAR(5),
@hasGivingLimit  NVARCHAR(5),
@hasAmount NVARCHAR(5),
@pageNumber INT,
@pageSize INT

AS
BEGIN
SET NOCOUNT ON;

WITH
ALLOCATED_BUDGETS (BUDGET_ID) AS
(
    SELECT B.ID
    FROM component.BUDGET B
    INNER JOIN component.ACL A ON B.ID = A.TARGET_ID
    INNER JOIN component.ROLE R ON A.ROLE_ID = R.ROLE_ID
    WHERE R.ROLE_CODE = 'BUDGET_ALLOCATOR'
        AND A.SUBJECT_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@budgetAllocator, ','))
    UNION ALL
    SELECT B.Id
    FROM component.BUDGET B
    INNER JOIN ALLOCATED_BUDGETS AB ON B.PARENT_BUDGET_ID = AB.BUDGET_ID
    WHERE  @includeChildren = 'TRUE'
),
OWNED_BUDGETS (BUDGET_ID) AS
(
    SELECT B.ID
    FROM component.BUDGET B
    INNER JOIN component.ACL A ON B.ID = A.TARGET_ID
    INNER JOIN component.ROLE R ON A.ROLE_ID = R.ROLE_ID
    WHERE R.ROLE_CODE = 'BUDGET_OWNER'
        AND A.SUBJECT_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@budgetOwner, ','))
    UNION ALL
    SELECT B.Id
    FROM component.BUDGET B
    INNER JOIN OWNED_BUDGETS OB ON B.PARENT_BUDGET_ID = OB.BUDGET_ID
    WHERE  @includeChildren = 'TRUE'
),
 QUERY AS(

SELECT DISTINCT V.*
,V.BUDGET_DISPLAY_NAME AS DISPLAY_NAME
,VG.NAME
,VG.SEARCH_TYPE
,VG.PAX_ID
,VG.LANGUAGE_CODE
,VG.PREFERRED_LOCALE
,VG.LANGUAGE_LOCALE
,VG.CONTROL_NUM
,VG.FIRST_NAME
,VG.MIDDLE_NAME
,VG.LAST_NAME
,VG.NAME_PREFIX
,VG.NAME_SUFFIX
,VG.COMPANY_NAME
,VG.VERSION
,VG.JOB_TITLE
,VG.SYS_USER_ID
,VG.MANAGER_PAX_ID
FROM component.VW_BUDGET_INFO V
LEFT JOIN ALLOCATED_BUDGETS AB ON AB.BUDGET_ID = V.ID
LEFT JOIN OWNED_BUDGETS OB ON OB.BUDGET_ID = V.ID
LEFT JOIN component.VW_USER_GROUPS_SEARCH_DATA VG ON VG.PAX_ID = V.BUDGET_OWNER
WHERE
(@budgetAllocator IS NULL OR  AB.BUDGET_ID IS NOT NULL)

AND (@budgetOwner IS NULL OR OB.BUDGET_ID IS NOT NULL)
AND (
        @displayName IS NULL
        OR V.BUDGET_DISPLAY_NAME LIKE @displayName
    )
AND (
                -- SCHEDULED
    (
        (
            @scheduledIncluded IS NULL
            OR @scheduledIncluded = 'TRUE'
        )
        AND V.STATUS_TYPE_CODE = 'ACTIVE'
        AND V.FROM_DATE > GETDATE()
    )
                -- ACTIVE
    OR (
        (
            @activeIncluded IS NULL
            OR @activeIncluded = 'TRUE'
        )
        AND V.STATUS_TYPE_CODE = 'ACTIVE'
        AND V.FROM_DATE < GETDATE()
        AND (
            V.THRU_DATE IS NULL
            OR V.THRU_DATE > GETDATE()
        )
    )
                -- ENDED
    OR (
        (
            @endedIncluded IS NULL
            OR @endedIncluded = 'TRUE'
        )
        AND V.STATUS_TYPE_CODE = 'ACTIVE'
        AND V.THRU_DATE < GETDATE()
    )
                -- ARCHIVED
    OR (
        (
            @archivedIncluded IS NULL
            OR @archivedIncluded = 'TRUE'
        )
        AND V.STATUS_TYPE_CODE = 'ARCHIVED'
    )
)
AND (
                 -- DO NOT INCLUDE CHILD BUDGETS
        (
            (@includeChildren IS NULL
                OR @includeChildren = 'FALSE')
            AND V.PARENT_BUDGET_ID IS NULL
        )
        OR
                -- INCLUDE CHILD BUDGETS
        (
            @includeChildren = 'TRUE'
            OR @budgetAllocator IS NOT NULL
        )
)
AND (
                 -- DO NOT FILTER BY INDIVIDUAL GIVING LIMIT
        (
            (@hasGivingLimit IS NULL
                OR @hasGivingLimit = 'FALSE')
        )
    OR
                -- SELECT ONLY RECORDS WHERE INDIVIDUAL GIVING LIMIT IS NOT NULL
    (
        (@hasGivingLimit = 'TRUE')
        AND V.INDIVIDUAL_GIVING_LIMIT IS NOT NULL
    )
)
AND (
                 -- DO NOT FILTER BY HAS AMOUNT
    (
            (@hasAmount IS NULL
            OR @hasAmount = 'FALSE')
    )

    OR
                -- SELECT ONLY RECORDS WHERE TOTAL_AMOUNT <> 0 AND USED_AMOUNT <> 0
    (
        (@hasAmount = 'TRUE')
        AND (V.TOTAL_USED <> 0 OR V.TOTAL_AMOUNT <> 0)
    )
)
)

SELECT * FROM QUERY CROSS JOIN (SELECT COUNT(*) AS TOTAL_RESULTS FROM QUERY) AS TOTAL_RESULTS
ORDER BY DISPLAY_NAME
OFFSET @pageNumber ROWS FETCH NEXT @pageSize ROWS ONLY


END

