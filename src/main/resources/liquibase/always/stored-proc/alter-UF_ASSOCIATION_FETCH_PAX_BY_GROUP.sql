/****** Object:  UserDefinedFunction [component].[UF_ASSOCIATION_FETCH_PAX_BY_GROUP]    Script Date: 10/16/2020 12:25:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PAX_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID
-- RETURN A DISTINCT LIST OF PAX_ID(S) ASSOCIATED WITH THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE				CHANGE DESCRIPTION
-- DOHOGNTA   	20080530  				CREATED
-- DOHOGNTA   	20150119  				USE STANDALONE JUNCTION TABLE
-- DOHOGNTA   	20150304  				TABLE RENAME
-- DOHOGNTA   	20150309  				CHANGE IDENTITIES TO BIGINT
-- burgestl		20201103				Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_ASSOCIATION_FETCH_PAX_BY_GROUP] (
  @group_id BIGINT)
RETURNS @result_set TABLE (
  [id] BIGINT NULL)
AS
BEGIN

DECLARE @fk TABLE ([id] BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET LIST OF DESCENDANT GROUP(S); INCLUDING THE GIVEN GROUP
INSERT @fk SELECT [id] FROM UF_ASSOCIATION_FETCH_GROUP_BY_GROUP (@group_id, 1)

-- GET LIST OF PAX(S) ASSOCIATED WITH GROUP(S)
INSERT  @result_set
SELECT  DISTINCT
  GROUPS_PAX.PAX_ID
FROM GROUPS_PAX
INNER JOIN @fk AS GROUPS ON GROUPS_PAX.GROUP_ID = GROUPS.[id]

-- RETURN RESULT SET
RETURN

END

GO
