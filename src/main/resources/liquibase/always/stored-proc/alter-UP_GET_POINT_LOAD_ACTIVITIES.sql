/****** Object:  StoredProcedure [UP_GET_POINT_LOAD_ACTIVITIES]    Script Date: 10/15/2015 10:00:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_POINT_LOAD_ACTIVITIES
-- ===========================  DESCRIPTION  ==================================
--
-- GETS THE ACTIVITIES FOR POINT LOAD PROGRAMS AND INFORMATION ABOUT EACH
-- ACTIVITY INCLUDING RECORD, POINT, AND EVENT TIME INFORMATION. FILTERS
-- BASED ON CREATION DATE RANGE AND STATUS.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- YUKIOM        20150701                CREATED
-- YUKIOM        20150708                MODIFYING TO INCLUDE CORRECT LOGIC FOR TOTAL POINTS.
-- MUDDAM        20151116                UDM4 CHANGES
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- HARWELLM        20170113    MP-9284        POPULATE START_DATE FOR 'NEW' BATCHES
-- GARCIAF2        20170203    MP-8914        DISPLAY TOTAL POINTS FOR PENDING STATUS
-- FARFANLE        20180214    MP-11136    DISPLAY PPP_INDEX_APPLIED FLAG
-- ARENASW        20180219    MP-11970    GET APPLICATION ERROR WHEN TRYING TO GET BACK INTO PROGRAM AFTER REPROCESSING FILE
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_POINT_LOAD_ACTIVITIES]
  @startDate DATETIME2,
  @endDate DATETIME2,
  @statuses NVARCHAR(MAX)

AS
BEGIN

  SET NOCOUNT ON

  --Temp variables for stat calculation
  DECLARE @STAGED_STATUSES NVARCHAR(MAX)

  SET @STAGED_STATUSES = 'NEW,IN_PROGRESS,PENDING_RELEASE'

  --Set up statuses
  CREATE TABLE #STATUS_TABLE  (
    STATUS NVARCHAR(MAX)
  )

  INSERT INTO #STATUS_TABLE (
    STATUS
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@statuses, ',')


  --Set up statuses that represent staged files
  CREATE TABLE #STAGED_STATUS_TABLE  (
    STATUS NVARCHAR(MAX)
  )

  INSERT INTO #STAGED_STATUS_TABLE (
    STATUS
  )
    SELECT * FROM [component].UF_LIST_TO_TABLE(@STAGED_STATUSES, ',')

  --Table for point load activities (Result table)
  CREATE TABLE #POINT_LOAD_ACTIVITIES (
    BATCH_ID BIGINT PRIMARY KEY,
    STATUS NVARCHAR(MAX),
    START_DATE NVARCHAR(MAX),
    PROCESS_DATE NVARCHAR(MAX),
    FILE_NAME NVARCHAR(MAX),
    TOTAL_RECORDS BIGINT,
    GOOD_RECORDS BIGINT,
    ERROR_RECORDS BIGINT,
    TOTAL_POINTS BIGINT,
    PPP_INDEX_APPLIED NVARCHAR(MAX)
  )

  --Insert all staged results (for records that have not already been accounted for)
  INSERT INTO #POINT_LOAD_ACTIVITIES (
    BATCH_ID,
    STATUS,
    START_DATE,
    PROCESS_DATE,
    FILE_NAME,
    TOTAL_RECORDS,
    GOOD_RECORDS,
    ERROR_RECORDS,
    TOTAL_POINTS,
    PPP_INDEX_APPLIED
  )
    select BATCH_ID,STATUS_TYPE_CODE,START_TIME,PROCESSED_TIME,FILE_NAME,TOTAL_RECS,OK_RECS, ERR_RECS, total_points,PPP_INDEX_APPLIED from (
    SELECT batch.BATCH_ID,
      batch.STATUS_TYPE_CODE,
      ISNULL(start_time_event.MESSAGE, CONVERT(DATETIME2(0), batch.CREATE_DATE, 120)) AS 'START_TIME',
      process_time_event.message AS 'PROCESSED_TIME',
      file_name_event.message AS 'FILE_NAME',
      total_recs_event.message AS 'TOTAL_RECS',
      good_recs_event.message AS 'OK_RECS',
      err_recs_event.message AS 'ERR_RECS',
      points.total_points,
      pppx_applied_event.message AS 'PPP_INDEX_APPLIED',
      ROW_NUMBER() over (partition by batch.batch_id 
                              order by batch.batch_id, ISNULL(start_time_event.MESSAGE, CONVERT(DATETIME2(0), batch.CREATE_DATE, 120)) desc) as ROWNUMBER
    FROM component.BATCH batch
      LEFT JOIN component.BATCH_EVENT start_time_event
        ON batch.BATCH_ID = start_time_event.BATCH_ID
           AND start_time_event.BATCH_EVENT_TYPE_CODE = 'STRTIM'
      LEFT JOIN component.BATCH_EVENT process_time_event
        ON batch.BATCH_ID = process_time_event.BATCH_ID
           AND process_time_event.BATCH_EVENT_TYPE_CODE = 'ENDTIM'
      LEFT JOIN component.BATCH_EVENT file_name_event
        ON batch.BATCH_ID = file_name_event.BATCH_ID
           AND file_name_event.BATCH_EVENT_TYPE_CODE = 'FILE'
      LEFT JOIN component.BATCH_EVENT total_recs_event
        ON batch.BATCH_ID = total_recs_event.BATCH_ID
           AND total_recs_event.BATCH_EVENT_TYPE_CODE = 'RECS'
      LEFT JOIN component.BATCH_EVENT good_recs_event
        ON batch.BATCH_ID = good_recs_event.BATCH_ID
           AND good_recs_event.BATCH_EVENT_TYPE_CODE = 'OKRECS'
      LEFT JOIN component.BATCH_EVENT err_recs_event
        ON batch.BATCH_ID = err_recs_event.BATCH_ID
           AND err_recs_event.BATCH_EVENT_TYPE_CODE = 'ERRS'
      LEFT JOIN (
                  SELECT th.BATCH_ID as batch_id,
                         ISNULL(SUM(DISC.AMOUNT), 0) total_points
                  FROM component.TRANSACTION_HEADER th
                    LEFT JOIN component.DISCRETIONARY DISC
                      ON th.ID = DISC.TRANSACTION_HEADER_ID
                  WHERE th.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
                  GROUP BY th.BATCH_ID
                ) points
        ON batch.BATCH_ID = points.batch_id
      LEFT JOIN component.BATCH_EVENT pppx_applied_event
        ON batch.BATCH_ID = pppx_applied_event.BATCH_ID
           AND pppx_applied_event.BATCH_EVENT_TYPE_CODE = 'PPP_INDEX_APPLIED'
    WHERE batch.BATCH_TYPE_CODE = 'CPTD'
          AND batch.STATUS_TYPE_CODE IN (SELECT STATUS FROM #STATUS_TABLE)
  ) as result where ROWNUMBER = 1

  --Now insert activity information into result table
  INSERT INTO #POINT_LOAD_ACTIVITIES (
    BATCH_ID,
    STATUS,
    START_DATE,
    PROCESS_DATE,
    FILE_NAME,
    TOTAL_RECORDS,
    GOOD_RECORDS,
    ERROR_RECORDS,
    TOTAL_POINTS,
    PPP_INDEX_APPLIED
  )
    select BATCH_ID,STATUS_TYPE_CODE,START_TIME,PROCESSED_TIME,FILE_NAME,TOTAL_RECS,OK_RECS, ERR_RECS, 
    total_points,PPP_INDEX_APPLIED from (
    SELECT batch.BATCH_ID,
      batch.STATUS_TYPE_CODE,
      ISNULL(start_time_event.MESSAGE, CONVERT(DATETIME2(0), batch.CREATE_DATE, 120)) AS 'START_TIME',
      process_time_event.message AS 'PROCESSED_TIME',
      file_name_event.message AS 'FILE_NAME',
      total_recs_event.message AS 'TOTAL_RECS',
      good_recs_event.message AS 'OK_RECS',
      err_recs_event.message AS 'ERR_RECS',
      points.total_points,
      pppx_applied_event.message AS 'PPP_INDEX_APPLIED',
      ROW_NUMBER() over (partition by batch.batch_id 
          order by batch.batch_id, ISNULL(start_time_event.MESSAGE, CONVERT(DATETIME2(0), batch.CREATE_DATE, 120)) desc) as ROWNUMBER
    FROM component.BATCH batch
      LEFT JOIN component.BATCH_EVENT start_time_event
        ON batch.BATCH_ID = start_time_event.BATCH_ID
           AND start_time_event.BATCH_EVENT_TYPE_CODE = 'STRTIM'
      LEFT JOIN component.BATCH_EVENT process_time_event
        ON batch.BATCH_ID = process_time_event.BATCH_ID
           AND process_time_event.BATCH_EVENT_TYPE_CODE = 'ENDTIM'
      LEFT JOIN component.BATCH_EVENT file_name_event
        ON batch.BATCH_ID = file_name_event.BATCH_ID
           AND file_name_event.BATCH_EVENT_TYPE_CODE = 'FILE'
      LEFT JOIN component.BATCH_EVENT total_recs_event
        ON batch.BATCH_ID = total_recs_event.BATCH_ID
           AND total_recs_event.BATCH_EVENT_TYPE_CODE = 'RECS'
      LEFT JOIN component.BATCH_EVENT good_recs_event
        ON batch.BATCH_ID = good_recs_event.BATCH_ID
           AND good_recs_event.BATCH_EVENT_TYPE_CODE = 'OKRECS'
      LEFT JOIN component.BATCH_EVENT err_recs_event
        ON batch.BATCH_ID = err_recs_event.BATCH_ID
           AND err_recs_event.BATCH_EVENT_TYPE_CODE = 'ERRS'
      LEFT JOIN (
                  SELECT th.BATCH_ID as batch_id,
                         ISNULL(SUM(DISC.AMOUNT), 0) total_points
                  FROM component.TRANSACTION_HEADER th
                    LEFT JOIN component.DISCRETIONARY DISC
                      ON th.ID = DISC.TRANSACTION_HEADER_ID
                  WHERE th.STATUS_TYPE_CODE IN ('APPROVED','PENDING')
                  GROUP BY th.BATCH_ID
                ) points
        ON batch.BATCH_ID = points.batch_id
      LEFT JOIN #POINT_LOAD_ACTIVITIES existing_activities
        ON existing_activities.BATCH_ID = batch.BATCH_ID
      LEFT JOIN component.BATCH_EVENT pppx_applied_event
        ON batch.BATCH_ID = pppx_applied_event.BATCH_ID
           AND pppx_applied_event.BATCH_EVENT_TYPE_CODE = 'PPP_INDEX_APPLIED'
    WHERE batch.BATCH_TYPE_CODE = 'CPTD'
          AND batch.CREATE_DATE >= @startDate
          AND batch.CREATE_DATE <= @endDate
          AND batch.STATUS_TYPE_CODE IN (SELECT STATUS FROM #STATUS_TABLE)
          AND existing_activities.BATCH_ID IS NULL
) as result where ROWNUMBER = 1


  -- Return the results (stored within the POINT_LOAD_ACTIVITIES table)
  SELECT
    RESULT.BATCH_ID,
    RESULT.STATUS,
    RESULT.START_DATE,
    RESULT.PROCESS_DATE,
    RESULT.FILE_NAME,
    RESULT.TOTAL_RECORDS,
    RESULT.GOOD_RECORDS,
    RESULT.ERROR_RECORDS,
    RESULT.TOTAL_POINTS,
    RESULT.PPP_INDEX_APPLIED
  FROM (SELECT * from #POINT_LOAD_ACTIVITIES) RESULT

  --Drop temp tables
  DROP TABLE #STATUS_TABLE
  DROP TABLE #STAGED_STATUS_TABLE
  DROP TABLE #POINT_LOAD_ACTIVITIES

END
GO

