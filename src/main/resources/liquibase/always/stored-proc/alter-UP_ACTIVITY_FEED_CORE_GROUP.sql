SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_ACTIVITY_FEED_CORE_GROUP
-- ===========================  DESCRIPTION  ==================================
-- FETCHES A PAGE OF DATA FOR THE PROVIDED GROUP FROM ALL ITEMS GIVEN OR RECEIVED BY THE PATH PAX
-- TAKES VISIBILITY INTO ACCOUNT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170718    MP-10334    Created query
-- MARTINC        20170720    MP-10334    Modified query to work as a proc
-- MUDDAM        20170825    MP-10890    Updated per DBA team to fix execution plan creation issue
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
-- HARWELLM        20180314    MP-11820    ADDED MAXDOP PER DBA RECOMMENDATIONS
-- BURGESTL		 20201117	 CNXT-335		Add Database Logging for Stored Procs
-- BIERMASC      20210720    CNXT-410    Complete revamp to improve performance
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ACTIVITY_FEED_CORE_GROUP]
@LOGGED_IN_PAX_ID BIGINT,
@PATH_GROUP_ID BIGINT,
@ACTIVITY_TYPE_LIST NVARCHAR(MAX),
@FILTER NVARCHAR(MAX),
@PAGE_NUMBER INT,
@PAGE_SIZE INT,
@NEWSFEED_ITEM_ID BIGINT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

--(+)Logging
BEGIN TRY
declare @loggingStartDate  DATETIME2(7),
		 @loggingEndDate  DATETIME2(7),
		@loggingParameters NVARCHAR(MAX),
		@loggingSprocName nvarchar(150),
		@loggingDebug int

select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    if (@loggingDebug=1)
set @loggingSprocName = 'UP_ACTIVITY_FEED_CORE_GROUP'
set @loggingStartDate = getdate()
set @loggingParameters = '@LOGGED_IN_PAX_ID=' + isnull(cast(@LOGGED_IN_PAX_ID as varchar),'null') --BIGINT
    + ' :: @PATH_GROUP_ID=' + isnull(cast(@PATH_GROUP_ID as varchar),'null') --BIGINT
    + ' :: @ACTIVITY_TYPE_LIST=' +isnull(@ACTIVITY_TYPE_LIST, 'null') -- NVARCHAR(MAX)
    + ' :: @FILTER=' +isnull(@FILTER, 'null') -- NVARCHAR(MAX)
    + ' :: @PAGE_NUMBER=' + isnull(cast(@PAGE_NUMBER as varchar),'null') --BIGINT
    + ' :: @PAGE_SIZE=' + isnull(cast(@PAGE_SIZE as varchar),'null') --BIGINT
    + ' :: @NEWSFEED_ITEM_ID=' + isnull(cast(@NEWSFEED_ITEM_ID as varchar),'null') --BIGINT
--(-)Logging

CREATE TABLE #ACTIVITY_TYPE (
    CODE NVARCHAR(50)
)
    INSERT INTO #ACTIVITY_TYPE
SELECT * FROM component.UF_LIST_TO_TABLE(@ACTIVITY_TYPE_LIST, ',')

CREATE TABLE #GROUP_MEMBER(
                              ID BIGINT,
                              DIRECT_REPORT_FLAG BIT DEFAULT 0
)

    INSERT INTO #GROUP_MEMBER(ID)
SELECT GTM.PAX_ID FROM component.VW_GROUP_TOTAL_MEMBERSHIP GTM
WHERE GTM.GROUP_ID = @PATH_GROUP_ID

-- Mark which group members are direct reports to the current user
UPDATE GM
SET DIRECT_REPORT_FLAG = 1
    FROM #GROUP_MEMBER GM
		INNER JOIN component.RELATIONSHIP R ON (R.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND R.PAX_ID_1 = GM.ID AND R.PAX_ID_2 = @LOGGED_IN_PAX_ID)

CREATE TABLE #NEWSFEED_ITEM (
                                NEWSFEED_ITEM_ID BIGINT PRIMARY KEY,
                                NOMINATION_ID BIGINT NOT NULL,  -- Ideally, there should be a unique index on this column, but there are multiple newsfeed items referring to the same nomination.
    --	select target_table, target_id, count(*) count
    --		from component.newsfeed_item
    --	group by target_table, target_id
    --	having count(*) > 1
    --	order by target_table, target_id
    -- See CNXT-719.
                                SUBMITTER_ID BIGINT NOT NULL,
                                REPORTED_RECIPIENT_ID BIGINT NOT NULL,
                                GIVEN_ANONYMOUSLY_FLAG BIT NOT NULL DEFAULT 0,
                                GIVEN_TO_GROUP_FLAG BIT,
                                CREATE_DATE DATETIME2(7) NOT NULL,
                                UPDATE_DATE DATETIME2(7) NOT NULL
)

-- Future activity feeds will take start/end dates as parameters.
DECLARE @now DATETIME2(7) = GETDATE()
	DECLARE @startDate DATETIME2(7) = DATEADD(YEAR, -1, @NOW)

	-- Recognitions given by the current user to one or more members of the group (report only a single recipient)
	INSERT INTO #NEWSFEED_ITEM (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
SELECT NFI.ID, N.ID, @LOGGED_IN_PAX_ID,
       ABS(MIN(
               CASE
                   WHEN GM.DIRECT_REPORT_FLAG = 1 THEN
                       -R.RECEIVER_PAX_ID  -- This will ensure that a direct report (if one exists) will be chosen as the reported recipient as the minimum; the ABS() will strip off the negative sign.
                   ELSE
                       R.RECEIVER_PAX_ID
                   END)),
       NFI.CREATE_DATE, NFI.UPDATE_DATE
FROM component.NOMINATION N
         INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
         INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
    INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
    INNER JOIN #GROUP_MEMBER GM ON GM.ID = R.RECEIVER_PAX_ID
WHERE NFI.NEWSFEED_VISIBILITY <> 'NONE'
  AND NFI.STATUS_TYPE_CODE = 'APPROVED'
  AND N.SUBMITTER_PAX_ID = @LOGGED_IN_PAX_ID
GROUP BY NFI.ID, N.ID, N.SUBMITTER_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE

    -- Recognitions given by a group member to the current user
INSERT INTO #NEWSFEED_ITEM (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
SELECT NFI.ID, N.ID, N.SUBMITTER_PAX_ID, @LOGGED_IN_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
FROM component.NOMINATION N
         INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
         INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
    INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
    INNER JOIN #GROUP_MEMBER GM ON GM.ID = N.SUBMITTER_PAX_ID
WHERE NFI.NEWSFEED_VISIBILITY <> 'NONE'
  AND NFI.STATUS_TYPE_CODE = 'APPROVED'
  AND R.RECEIVER_PAX_ID = @LOGGED_IN_PAX_ID

    -- Recognitions given by the current user -- when the current user is a member of the group -- to anyone (report only a single recipient)
    -- NOTE:  This is not mutually exclusive with the inserts above; a recognition could be given by the current user to multiple people, including a group member.
    MERGE INTO #NEWSFEED_ITEM AS TARGET
    USING (
    SELECT NFI.ID NEWSFEED_ITEM_ID, N.ID NOMINATION_ID, @LOGGED_IN_PAX_ID SUBMITTER_ID, MIN(R.RECEIVER_PAX_ID) REPORTED_RECIPIENT_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
    FROM component.NOMINATION N
    INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
    INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
    INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
    INNER JOIN #GROUP_MEMBER GM ON GM.ID = N.SUBMITTER_PAX_ID
    WHERE NFI.NEWSFEED_VISIBILITY <> 'NONE'
    AND NFI.STATUS_TYPE_CODE = 'APPROVED'
    AND N.SUBMITTER_PAX_ID = @LOGGED_IN_PAX_ID
    GROUP BY NFI.ID, N.ID, N.SUBMITTER_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
    ) AS SOURCE ON (TARGET.NEWSFEED_ITEM_ID = SOURCE.NEWSFEED_ITEM_ID)
    WHEN NOT MATCHED THEN
INSERT (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
VALUES (SOURCE.NEWSFEED_ITEM_ID, SOURCE.NOMINATION_ID, SOURCE.SUBMITTER_ID, SOURCE.REPORTED_RECIPIENT_ID, SOURCE.CREATE_DATE, SOURCE.UPDATE_DATE);

-- Recognitions given to the current user, when the current user is part of the group
-- NOTE:  This is not mutually exclusive with the inserts above; a recognition could be given to the current user (who is part of the group), by another group member.
MERGE INTO #NEWSFEED_ITEM AS TARGET
    USING (
        SELECT NFI.ID NEWSFEED_ITEM_ID, N.ID NOMINATION_ID, N.SUBMITTER_PAX_ID SUBMITTER_ID, R.RECEIVER_PAX_ID REPORTED_RECIPIENT_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
        FROM component.NOMINATION N
                 INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
                 INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
            INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
            INNER JOIN #GROUP_MEMBER RGM ON RGM.ID = R.RECEIVER_PAX_ID
        WHERE NFI.NEWSFEED_VISIBILITY <> 'NONE'
          AND NFI.STATUS_TYPE_CODE = 'APPROVED'
          AND R.RECEIVER_PAX_ID = @LOGGED_IN_PAX_ID
    ) AS SOURCE ON (TARGET.NEWSFEED_ITEM_ID = SOURCE.NEWSFEED_ITEM_ID)
    WHEN NOT MATCHED THEN
        INSERT (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
            VALUES (SOURCE.NEWSFEED_ITEM_ID, SOURCE.NOMINATION_ID, SOURCE.SUBMITTER_ID, SOURCE.REPORTED_RECIPIENT_ID, SOURCE.CREATE_DATE, SOURCE.UPDATE_DATE);

-- Recognitions given to a group member who is a direct report of the current user (report only a single recipient) by someone other than the current user
-- NOTE:  This is not mutually exclusive with the insert directly above; a recognition could be given to multiple people, including the current user and a direct report to the current user.
MERGE INTO #NEWSFEED_ITEM AS TARGET
    USING (
        SELECT NFI.ID NEWSFEED_ITEM_ID, N.ID NOMINATION_ID, N.SUBMITTER_PAX_ID SUBMITTER_ID, MIN(R.RECEIVER_PAX_ID) REPORTED_RECIPIENT_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
        FROM component.NOMINATION N
                 INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
                 INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
            INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
            INNER JOIN #GROUP_MEMBER GM ON GM.ID = R.RECEIVER_PAX_ID
        WHERE NFI.NEWSFEED_VISIBILITY <> 'NONE'
          AND NFI.STATUS_TYPE_CODE = 'APPROVED'
          AND N.SUBMITTER_PAX_ID <> @LOGGED_IN_PAX_ID
          AND GM.DIRECT_REPORT_FLAG = 1
        GROUP BY NFI.ID, N.ID, N.SUBMITTER_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
    ) AS SOURCE ON (TARGET.NEWSFEED_ITEM_ID = SOURCE.NEWSFEED_ITEM_ID)
    WHEN NOT MATCHED THEN
        INSERT (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
            VALUES (SOURCE.NEWSFEED_ITEM_ID, SOURCE.NOMINATION_ID, SOURCE.SUBMITTER_ID, SOURCE.REPORTED_RECIPIENT_ID, SOURCE.CREATE_DATE, SOURCE.UPDATE_DATE);

-- Recognitions given to a group member who is a not direct report of the current user (report only a single recipient) by someone other than the current user, where the recognition is public and the recipient has chosen to share
-- NOTE:  This is not mutually exclusive with the insert directly above; a recognition could be given to multiple people, some of whom are direct reports of the current user and some whom are not, or the recognition could be given to the current user.
MERGE INTO #NEWSFEED_ITEM AS TARGET
    USING (
        SELECT NFI.ID NEWSFEED_ITEM_ID, N.ID NOMINATION_ID, N.SUBMITTER_PAX_ID SUBMITTER_ID, MIN(R.RECEIVER_PAX_ID) REPORTED_RECIPIENT_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
        FROM component.NOMINATION N
                 INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
                 INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
            INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.ID AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
            INNER JOIN #GROUP_MEMBER GM ON (GM.ID = R.RECEIVER_PAX_ID)
            INNER JOIN component.PAX_MISC SHARE_PREFERENCE ON (SHARE_PREFERENCE.VF_NAME = 'SHARE_REC' AND SHARE_PREFERENCE.PAX_ID = R.RECEIVER_PAX_ID)
        WHERE NFI.NEWSFEED_VISIBILITY NOT IN ('PRIVATE', 'NONE')
          AND NFI.STATUS_TYPE_CODE = 'APPROVED'
          AND N.SUBMITTER_PAX_ID <> @LOGGED_IN_PAX_ID
          AND GM.DIRECT_REPORT_FLAG = 0
          AND SHARE_PREFERENCE.MISC_DATA = 'TRUE'
        GROUP BY NFI.ID, N.ID, N.SUBMITTER_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
    ) AS SOURCE ON (TARGET.NEWSFEED_ITEM_ID = SOURCE.NEWSFEED_ITEM_ID)
    WHEN NOT MATCHED THEN
        INSERT (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
            VALUES (SOURCE.NEWSFEED_ITEM_ID, SOURCE.NOMINATION_ID, SOURCE.SUBMITTER_ID, SOURCE.REPORTED_RECIPIENT_ID, SOURCE.CREATE_DATE, SOURCE.UPDATE_DATE);

-- Recognitions given by a group member which is public and the recipient has chosen to share
-- NOTE: This is not mutually exclusive with the inserts above, since group members can give to group members.
MERGE INTO #NEWSFEED_ITEM AS TARGET
    USING (
        SELECT NFI.ID NEWSFEED_ITEM_ID, N.ID NOMINATION_ID, N.SUBMITTER_PAX_ID SUBMITTER_ID, MIN(R.RECEIVER_PAX_ID) REPORTED_RECIPIENT_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
        FROM component.NOMINATION N
                 INNER JOIN component.NEWSFEED_ITEM NFI ON (NFI.TARGET_TABLE = 'NOMINATION' AND NFI.TARGET_ID = N.ID AND NFI.UPDATE_DATE BETWEEN @startDate AND @now)
                 INNER JOIN #ACTIVITY_TYPE AT ON (NFI.NEWSFEED_ITEM_TYPE_CODE = AT.CODE)
            INNER JOIN component.RECOGNITION R ON (R.NOMINATION_ID = N.iD AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED'))
            INNER JOIN #GROUP_MEMBER GM ON (GM.ID = N.SUBMITTER_PAX_ID)
            INNER JOIN component.PAX_MISC SHARE_PREFERENCE ON (SHARE_PREFERENCE.VF_NAME = 'SHARE_REC' AND SHARE_PREFERENCE.PAX_ID = R.RECEIVER_PAX_ID)
        WHERE NFI.NEWSFEED_VISIBILITY NOT IN ('PRIVATE', 'NONE')
          AND NFI.STATUS_TYPE_CODE = 'APPROVED'
          AND SHARE_PREFERENCE.MISC_DATA = 'TRUE'
        GROUP BY NFI.ID, N.ID, N.SUBMITTER_PAX_ID, NFI.CREATE_DATE, NFI.UPDATE_DATE
    ) AS SOURCE ON (TARGET.NEWSFEED_ITEM_ID = SOURCE.NEWSFEED_ITEM_ID)
    WHEN NOT MATCHED THEN
        INSERT (NEWSFEED_ITEM_ID, NOMINATION_ID, SUBMITTER_ID, REPORTED_RECIPIENT_ID, CREATE_DATE, UPDATE_DATE)
            VALUES (SOURCE.NEWSFEED_ITEM_ID, SOURCE.NOMINATION_ID, SOURCE.SUBMITTER_ID, SOURCE.REPORTED_RECIPIENT_ID, SOURCE.CREATE_DATE, SOURCE.UPDATE_DATE);

-- Mark as those newsfeed items which were given anonymously as such
UPDATE NFI
SET GIVEN_ANONYMOUSLY_FLAG = 1
    FROM #NEWSFEED_ITEM NFI
		JOIN component.RECOGNITION R ON (R.NOMINATION_ID = NFI.NOMINATION_ID)
    JOIN component.RECOGNITION_MISC GA ON (GA.VF_NAME = 'GIVE_ANONYMOUS' AND GA.RECOGNITION_ID = R.ID AND GA.MISC_DATA = 'TRUE')

-- Mark the anonymously-given recognitions that were received by a member of the group.
UPDATE NFI
SET GIVEN_TO_GROUP_FLAG = 1
    FROM #NEWSFEED_ITEM NFI
			JOIN component.RECOGNITION R ON (R.NOMINATION_ID = NFI.NOMINATION_ID)
    JOIN #GROUP_MEMBER GM ON (GM.ID = R.RECEIVER_PAX_ID)
WHERE NFI.GIVEN_ANONYMOUSLY_FLAG = 1

-- Delete any recognitions which were anonymously given, unless they were received by a member of the group.
DELETE
FROM #NEWSFEED_ITEM
WHERE GIVEN_ANONYMOUSLY_FLAG = 1 AND GIVEN_TO_GROUP_FLAG IS NULL

-- Mask the giver on the remaining anonymously-given recognitions (which were received by a member of the group).
UPDATE #NEWSFEED_ITEM
SET SUBMITTER_ID = 0
WHERE GIVEN_ANONYMOUSLY_FLAG = 1

DECLARE @TOTAL_NUMBER_OF_ROWS INT
SELECT @TOTAL_NUMBER_OF_ROWS = COUNT(*)
FROM #NEWSFEED_ITEM

SELECT @TOTAL_NUMBER_OF_ROWS TOTAL_RESULTS,
       NOMINATION_ID,
       NEWSFEED_ITEM_ID,
       CREATE_DATE,
       SUBMITTER_ID SUBMITTER_PAX_ID,
       REPORTED_RECIPIENT_ID RECIPIENT_PAX_ID
FROM #NEWSFEED_ITEM
ORDER BY CREATE_DATE DESC
OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS FETCH NEXT @PAGE_SIZE ROWS ONLY

DROP TABLE #ACTIVITY_TYPE
DROP TABLE #GROUP_MEMBER
DROP TABLE #NEWSFEED_ITEM

    --(+)Logging
    if (@loggingDebug=1)
begin
	set @loggingEndDate = getdate()

	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
END
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging

END;


GO