SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_BUDGET_ELIGIBILITY
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS ELIGIBLE BUDGETS FOR PARTICIPANT AND INDIVIDUAL USED
--
-- @paxId - The pax who is eligible to use the budgets returned
-- @budgetIdList - List of budget ids from server cache
-- @givingLimit - Filter budgets that have giving limits
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20161214    MP-8735        CREATED
-- HARWELLM        20161216    MP-8559        REMOVE DUPLICATE RECORDS
-- HARWELLM        20160130    MP-9435        ONLY RETURN ACTIVE BUDGETS
-- MUDDAM        20170303    MP-9648        MOVED BUDGET RESOLUTION TO SEPARATE PROCESS
-- HARWELLM        20170403    MP-8044        EXCLUDE REVERSED TRANSACTIONS
-- MUDDAM        201700922    MP-8001        UDM5 CHANGES
-- KUMARSJ        20171120    MP-11332    REMOVED TOTAL_USED, TOTAL_AMOUNT,IS_USED FOR PERFORMANCE INTO SEPARATE CALLS
-- FLORESC        2021-02-22  CRE-22339   Added CAST AS FLOAT on validation for ALLOCATED Budgets to avoid errors with scientific notations on allocated in and allocated out values
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_ELIGIBILITY]
@paxId BIGINT,
@budgetIdList NVARCHAR(MAX),
@givingLimit NVARCHAR(5)

AS
BEGIN
SET NOCOUNT ON;

-- BUDGET INFO
SELECT V.ID,
       V.BUDGET_TYPE_CODE,
       V.BUDGET_NAME,
       V.DISPLAY_NAME AS BUDGET_DISPLAY_NAME,
       V.BUDGET_DESC,
       V.PARENT_BUDGET_ID,
       CASE         
         WHEN V.BUDGET_TYPE_CODE = 'ALLOCATED' THEN (CAST(CAST(V.ALLOCATED_IN AS FLOAT) AS DECIMAL(10, 2)) - CAST(CAST(V.ALLOCATED_OUT AS FLOAT) AS DECIMAL(10, 2)))
         ELSE V.INDIVIDUAL_GIVING_LIMIT
       END AS INDIVIDUAL_GIVING_LIMIT,
       V.FROM_DATE,
       V.THRU_DATE,
       V.STATUS_TYPE_CODE,
       V.SUB_PROJECT_ID,
       V.BUDGET_CONSTRAINT,
       V.PROJECT_NUMBER,
       V.SUB_PROJECT_NUMBER,
       V.CREATE_DATE,
       V.GROUP_CONFIG_ID,
       V.GROUP_ID,
       V.GROUP_NAME,
       V.BUDGET_OWNER,
       V.BUDGET_ALLOCATOR,
       V.ALLOCATED_IN,
       V.ALLOCATED_OUT,
       V.IS_USABLE
,(CASE
    WHEN DEBITS.DEBITS is null then 0
    ELSE DEBITS.DEBITS
END)
-
(CASE
    WHEN CREDITS.CREDITS is null then 0
    ELSE CREDITS.CREDITS
END)
AS 'INDIVIDUAL_USED_AMOUNT'
FROM component.UF_LIST_TO_TABLE_ID(@budgetIdList, ',') ID_LIST
INNER JOIN component.VW_BUDGET_INFO_LIGHTWEIGHT V
    ON V.ID = ID_LIST.items
inner JOIN component.BUDGET_MISC AL_DIST_USABLE
    ON AL_DIST_USABLE.BUDGET_ID = V.ID
        AND AL_DIST_USABLE.VF_NAME = 'IS_USABLE'
        AND AL_DIST_USABLE.MISC_DATA = 'TRUE'
-- Calculate individual used amount
LEFT JOIN  
    ( 
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.NOMINATION n 
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID 
        JOIN component.DISCRETIONARY disc_debit ON disc_debit.TARGET_ID = r.ID AND disc_debit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON disc_debit.TRANSACTION_HEADER_ID = th.ID
            AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED', 'REVERSED')
        JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID
            AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE <> 'FUND'
        WHERE N.SUBMITTER_PAX_ID = @paxId
        GROUP BY disc_debit.BUDGET_ID_DEBIT
    ) DEBITS
    ON V.ID = DEBITS.ID
LEFT JOIN
    (
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.DISCRETIONARY disc_credit ON disc_credit.TARGET_ID = r.ID AND disc_credit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON disc_credit.TRANSACTION_HEADER_ID = th.ID
            AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED', 'REVERSED')
        JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID
            AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE <> 'FUND'
        WHERE N.SUBMITTER_PAX_ID = @paxId
        GROUP BY disc_credit.BUDGET_ID_CREDIT
    ) CREDITS
    ON V.ID = CREDITS.ID
WHERE FROM_DATE < GETDATE() AND (THRU_DATE IS NULL OR THRU_DATE > GETDATE()) --Only ACTIVE budgets
    AND (
        @givingLimit IS NULL
        OR @givingLimit = 'FALSE'
        OR (@givingLimit = 'TRUE' AND (V.INDIVIDUAL_GIVING_LIMIT IS NOT NULL OR V.BUDGET_TYPE_CODE = 'ALLOCATED'))
    )
ORDER BY V.ID;

END