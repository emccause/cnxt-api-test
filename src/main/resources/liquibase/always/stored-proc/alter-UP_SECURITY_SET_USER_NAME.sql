SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_SECURITY_SET_USER_NAME
-- ===========================  DESCRIPTION  ==================================
-- THE PURPOSE OF THIS STORED PROCEDURE IS TO ASSOCIATE A GIVEN
-- USER NAME WITH A SPID.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA      20070201    CREATED
-- KNIGHTGA      20200604    Updated to use CONTEXT_INFO instead of table for efficiency
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_SECURITY_SET_USER_NAME]
     @user_name nvarchar(50)
AS
BEGIN

SET NOCOUNT ON

if @user_name is not null begin
    DECLARE @contextInfo VARBINARY(100) = CONVERT(VARBINARY(100), @user_name);
    SET CONTEXT_INFO @contextInfo;
end
else begin
    SET CONTEXT_INFO 0x0
end

END