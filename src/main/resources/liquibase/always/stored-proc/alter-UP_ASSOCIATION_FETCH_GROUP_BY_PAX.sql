/****** Object:  StoredProcedure [UP_ASSOCIATION_FETCH_GROUP_BY_PAX]    Script Date: 10/14/2015 12:31:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_GROUP_BY_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_ID
-- RETURN A DISTINCT LIST OF GROUP_ID(S) FOR THE GIVEN PAX_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_GROUP_BY_PAX]
    @pax_id BIGINT
-- TODO:,    @level_increment INT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS GROUP_ID
-- FROM    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, @level_increment)
FROM    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, -1)

END

GO

