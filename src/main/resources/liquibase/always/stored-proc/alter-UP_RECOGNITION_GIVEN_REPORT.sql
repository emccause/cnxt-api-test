/****** Object:  StoredProcedure [component].[UP_RECOGNITION_GIVEN_REPORT]    Script Date: 10/15/2015 11:08:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_GIVEN_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS GIVEN BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- THOMSOND        20080702                CREATED
-- THOMSOND        20080725                CHANGED @submitter_pax_id TO @submitter_pax_group_id
-- THOMSOND        20080902                CHANGED TYPE ON APPROVAL_NOTES TO NTEXT
-- BESSBS        20080919                ADDED 'Multiple Ecards' COMMENT AND CHANGED ENABLE_JUSTIFICATION CONDITIONS
-- THOMSOND        20080929                CHANGED SEVERAL QUERIES TO EXCLUDE RECOGNITION RECORDS WIT A STATUS OF 'X'
-- THOMSOND        20090929                REPLACED comment_justification with justification_comment_available_flag 
-- KOTHAH        20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151113                UDM4 CHANGES
-- WRIGHTM        20151117                CHANGED SATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_GIVEN_REPORT]
    @submitter_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- DRIVER
CREATE TABLE #nomination (
    nomination_id BIGINT NOT NULL
,    nomination_status NVARCHAR(10) NULL

,    submittal_date DATETIME2 NULL
,    submitter_list VARCHAR(1000) NULL
,    submitter_name VARCHAR(80) NULL

,    recognition_id    BIGINT NULL
,    total_points INT NULL
,    total_pending_points INT NULL
,    total_denied_points INT NULL
,    total_approved_points INT NULL

,    receiver_list VARCHAR(1000) NULL
,    receiver_name VARCHAR(80) NULL

,    approval_process_id BIGINT NULL
,    approval_notes NVARCHAR(MAX) NULL
,    approval_count INT NULL

,    approver_count INT NULL
,    approver_name VARCHAR(80)

,    last_approval_date DATETIME2 NULL

,    comment NVARCHAR(MAX) NULL
,    comment_type char(1) NULL
,    justification_comment_available_flag INT NULL DEFAULT 0
,    justification_flag INT NULL DEFAULT 0

,    criteria NVARCHAR(1024)

,    program_id BIGINT NULL
,    program_name NVARCHAR(40) NULL
,    award_type VARCHAR(40) NULL
,    budget_id BIGINT NULL
)

CREATE TABLE #award_types (
    nomination_id BIGINT NULL
,    award_type NVARCHAR(40) NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_GIVEN_BASE @submitter_pax_group_id, @submittal_date_from, @submittal_date_thru, @locale_code

UPDATE #nomination
SET    nomination_status =
    CASE
        WHEN nomination_status = 'PENDING' THEN 'Pending'
        WHEN nomination_status = 'APPROVAL' THEN 'Approved'
        WHEN nomination_status = 'REJECTED' THEN 'Denied'
        WHEN nomination_status = 'COMPLETE' THEN 'Complete'
    END

SELECT
    submittal_date
,    receiver_list
,    receiver_name
,    comment_type as comment
,    justification_comment_available_flag
,    component.UF_TRANSLATE (@locale_code, nomination_status, 'Reporting', 'Reporting') AS nomination_status
,    approver_name
,    last_approval_date
,    approval_count
,    submitter_list
,    submitter_name
,    component.UF_TRANSLATE (@locale_code, program_name, 'PROGRAM', 'PROGRAM_NAME') AS program_name
,    program_id
,    total_points 
,    CASE
        WHEN award_type = 'Points'
        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
        ELSE component.UF_TRANSLATE (@locale_code, award_type, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC')
    END AS award_type
,    total_pending_points
,    total_denied_points
,    total_approved_points
,    criteria
,    recognition_id
,    nomination_id
,    approval_notes
,    justification_flag
,    budget_id
FROM    #nomination
ORDER BY
    submittal_date DESC

END

GO

