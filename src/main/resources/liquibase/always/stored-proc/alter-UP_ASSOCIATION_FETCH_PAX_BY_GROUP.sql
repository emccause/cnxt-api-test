/****** Object:  StoredProcedure [component].[UP_ASSOCIATION_FETCH_PAX_BY_GROUP]    Script Date: 10/14/2015 12:36:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID
-- RETURN A DISTINCT LIST OF PAX_ID(S) ASSOCIATED WITH THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
-- DOHOGNTA        20150304                REMOVE ASSOCIATION REFERENCE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_BY_GROUP]
    @group_id BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS PAX_ID
FROM    UF_ASSOCIATION_FETCH_PAX_BY_GROUP (@group_id)

END

GO

