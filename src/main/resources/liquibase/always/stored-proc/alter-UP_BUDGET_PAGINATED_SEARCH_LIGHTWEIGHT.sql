SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT
-- ===========================  DESCRIPTION  ==================================
-- Return a paginated budget list according to the filters entered.
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20170612    MP-9785        Reworked UP_BUDGET_PAGINATED_SEARCH to remove calculated columns that lead to poor performance
-- GARCIAF2        20170726    MP-10335    ADD BUDGET USER
-- HARWELLM        20170731    MP-10716    INCLUDE SIMPLE BUDGETS WHEN SEARCHING BY BUDGET_USER
-- MUDDAM        20170802    MP-10730    ADDED PROGRAM_ID FILTER, ALSO SWITCHED BUDGET USER TO A LIST
-- HARWELLM        20170803    MP-10730    CHECK BUDGET_GIVING_ELIGIBILITY WHEN BOTH BUDGET USER AND PROGRAM ID ARE PASSED IN
-- MUDDAM        20170822    MP-10858    BROKE PAX/PROGRAM SEARCH OUT INTO A SEPARATE PROC DUE TO PERFORMANCE ISSUES
-- FREINERT     20171221    MP-10773    ADDING BUDGET TYPE PARAM
-- DDAYS  20210505 Change behavior check for null on @endedIncluded reference
-- burgestl	     20210518	CRE-22786     For New Tenant with not program active budget are not found this is the fix
-- ===========================  DECLARATIONS  =================================
ALTER PROCEDURE [component].[UP_BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT]
@budgetOwner NVARCHAR(MAX),
@displayName NVARCHAR(MAX),
@budgetAllocator NVARCHAR(MAX),
@scheduledIncluded NVARCHAR(5),
@activeIncluded NVARCHAR(5),
@endedIncluded NVARCHAR(5),
@archivedIncluded NVARCHAR(5),
@includeChildren  NVARCHAR(5),
@hasGivingLimit  NVARCHAR(5),
@hasAmount NVARCHAR(5),
@offset INT,
@pageSize INT,
@budgetType NVARCHAR(MAX)

AS

BEGIN
SET NOCOUNT ON;
--(+)Logging
BEGIN TRY
DECLARE @loggingStartDate  DATETIME2(7),
@loggingEndDate  DATETIME2(7),
@loggingParameters NVARCHAR(MAX),
@loggingSprocName nvarchar(150),
@loggingDebug int
---------------------------------------------- CHECKING LOGGING ENABLED
SELECT  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

IF (@loggingDebug=1)
BEGIN
SET @loggingSprocName = 'UP_BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT'
SET @loggingStartDate = getdate()
SET @loggingParameters =  '@budgetOwner=' + isnull(cast(@budgetOwner as varchar(max)),'null') --NVARCHAR(MAX)
+ ' :: @displayName=' +isnull(cast(@displayName as nvarchar(max)), 'null') -- NVARCHAR(MAX)
+ ' :: @budgetAllocator=' +isnull(cast(@budgetAllocator as nvarchar(max)), 'null') -- NVARCHAR(MAX)
+ ' :: @scheduledIncluded=' +isnull(cast(@scheduledIncluded as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @activeIncluded=' +isnull(cast(@activeIncluded as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @endedIncluded =' +isnull(cast(@endedIncluded  as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @archivedIncluded=' +isnull(cast(@archivedIncluded as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @includeChildren=' +isnull(cast(@includeChildren as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @hasGivingLimit=' +isnull(cast(@hasGivingLimit as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @hasAmount=' +isnull(cast(@hasAmount as nvarchar), 'null') -- NVARCHAR(MAX
+ ' :: @offset=' +isnull(cast(@offset as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @pageSize=' +isnull(cast(@pageSize as nvarchar), 'null') -- NVARCHAR(MAX)
+ ' :: @budgetType=' +isnull(cast(@budgetType as nvarchar), 'null') -- NVARCHAR(MAX)

END;

WITH
ALLOCATED_BUDGETS (BUDGET_ID) AS
(
  SELECT B.ID
  FROM component.BUDGET B
    INNER JOIN component.ACL A ON B.ID = A.TARGET_ID
    INNER JOIN component.ROLE R ON A.ROLE_ID = R.ROLE_ID
  WHERE R.ROLE_CODE = 'BUDGET_ALLOCATOR'
  AND A.SUBJECT_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@budgetAllocator, ','))
  UNION ALL
  SELECT B.Id
  FROM component.BUDGET B
    INNER JOIN ALLOCATED_BUDGETS AB ON B.PARENT_BUDGET_ID = AB.BUDGET_ID
  WHERE  @includeChildren = 'TRUE'
),
OWNED_BUDGETS (BUDGET_ID) AS
(
  SELECT B.ID
  FROM component.BUDGET B
    INNER JOIN component.ACL A ON B.ID = A.TARGET_ID
    INNER JOIN component.ROLE R ON A.ROLE_ID = R.ROLE_ID
  WHERE R.ROLE_CODE = 'BUDGET_OWNER'
  AND A.SUBJECT_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@budgetOwner, ','))
  UNION ALL
  SELECT B.Id
  FROM component.BUDGET B
    INNER JOIN OWNED_BUDGETS OB ON B.PARENT_BUDGET_ID = OB.BUDGET_ID
  WHERE  @includeChildren = 'TRUE'
),
QUERY AS(
  SELECT DISTINCT V.*
  FROM component.VW_BUDGET_INFO_LIGHTWEIGHT V
    LEFT JOIN ALLOCATED_BUDGETS AB ON AB.BUDGET_ID = V.ID
    LEFT JOIN OWNED_BUDGETS OB ON OB.BUDGET_ID = V.ID
  WHERE v.PROJECT_NUMBER IS NOT NULL
  AND (@budgetAllocator IS NULL OR  AB.BUDGET_ID IS NOT NULL)
  AND (@budgetOwner IS NULL OR OB.BUDGET_ID IS NOT NULL)
  AND (
  @displayName IS NULL
  OR V.DISPLAY_NAME COLLATE Latin1_general_CI_AI LIKE @displayName COLLATE Latin1_general_CI_AI
  )
  AND (
  -- SCHEDULED
  (
  (
  @scheduledIncluded IS NULL
  OR @scheduledIncluded = 'TRUE'
  )
  AND V.STATUS_TYPE_CODE = 'ACTIVE'
  AND V.FROM_DATE > GETDATE()
  )
  -- ACTIVE
  OR (
  (
  @activeIncluded IS NULL
  OR @activeIncluded = 'TRUE'
  )
  AND V.STATUS_TYPE_CODE = 'ACTIVE'
  AND V.FROM_DATE < GETDATE()
  AND (
  V.THRU_DATE IS NULL
  OR V.THRU_DATE > GETDATE()
  )
  )
  -- ENDED
  OR (
  (
  @endedIncluded IS NULL
  OR @endedIncluded = 'TRUE'
  )
  AND V.STATUS_TYPE_CODE = 'ACTIVE'
  AND V.THRU_DATE < GETDATE()
  )
  -- ARCHIVED
  OR (
  (
  @archivedIncluded IS NULL
  OR @archivedIncluded = 'TRUE'
  )
  AND V.STATUS_TYPE_CODE = 'ARCHIVED'
  )
  )
  AND (
  -- DO NOT INCLUDE CHILD BUDGETS
  (
  (@includeChildren IS NULL
  OR @includeChildren = 'FALSE')
  AND V.PARENT_BUDGET_ID IS NULL
  )
  OR
  -- INCLUDE CHILD BUDGETS
  (
  @includeChildren = 'TRUE'
  OR @budgetAllocator IS NOT NULL
  )
  )
  AND (
  -- DO NOT FILTER BY INDIVIDUAL GIVING LIMIT
  (
  (@hasGivingLimit IS NULL
  OR @hasGivingLimit = 'FALSE')
  )
  OR
  -- SELECT ONLY RECORDS WHERE INDIVIDUAL GIVING LIMIT IS NOT NULL
  (
  (@hasGivingLimit = 'TRUE')
  AND V.INDIVIDUAL_GIVING_LIMIT IS NOT NULL
  )
  )
  AND (
  -- DO NOT FILTER BY HAS AMOUNT
  (
  (@hasAmount IS NULL
  OR @hasAmount = 'FALSE')
  )

  OR
  -- SELECT ONLY RECORDS WHERE TOTAL_AMOUNT <> 0 AND USED_AMOUNT <> 0
  (
  (@hasAmount = 'TRUE')
  AND (V.ALLOCATED_IN <> '0' AND V.ALLOCATED_IN <> V.ALLOCATED_OUT)
  )
  )
  AND (
  -- SELECT BUDGETS WITH GIVEN BUDGET TYPE
  @budgetType IS NULL
  OR V.BUDGET_TYPE_CODE = @budgetType
  )
)

SELECT
COUNT(*) OVER() AS TOTAL_RESULTS,
*
FROM QUERY where ((thru_date is not null or @endedIncluded = 'true')) OR (@endedIncluded is null) OR (@endedIncluded = 'false' AND thru_date is null)

ORDER BY DISPLAY_NAME
OFFSET @offset ROWS FETCH NEXT @pageSize ROWS ONLY


--(+)Logging
if (@loggingDebug=1)
begin
set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error
	 --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @loggingEndDate
end
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging

END