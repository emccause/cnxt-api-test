GO
/****** Object:  StoredProcedure [component].[UP_TRANSACTION_SEARCH]    Script Date: 11/16/2020 9:16:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_TRANSACTION_SEARCH
-- ===========================  DESCRIPTION  ==================================
--
-- SEARCHES FOR TRANSACTIONS. Can filter on:
--
-- startDate, endDate - date range filters
-- awardCodeList - comma separated list of award codes to search for
-- programIdList - comma separated list of program IDs to filter search results on
-- programTypeList - comma separated list of program types to filter search results on
-- statusList - comma separated list of recognition statuses to fileter search results on
-- recipientPaxList - comma separated list of paxIds to filter the search by recipient
-- submitterPaxList - comma separated list of paxIds to filter the search by submitter
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170413    MP-9831        CREATED - Pulled SQL from TransactionDetailsDao
-- MUDDAM        20170505    MP-10179    Switched order by to CREATE DATE DESC
-- MUDDAM        20170908    MP-8001        UPDATES FOR LOCALE CHANGES (PREFERRED_LOCALE -> PAX_MISC, LANGUAGE_LOCALE -> PREFERRED_LOCALE)
-- BURGESTL		 20201113    CNXT-289		Switch UF_... to be populated into variable table
--                                          Change logic to get the what information is need and what paging items need to be retreived
--                                          Then go after those few records and the remaining information need to reduce its data footprint
-- BURGESTL		 20201117	 CNXT-335		Add Database Logging for Stored Procs
-- ===========================  DECLARATIONS  =================================

--declare --
ALTER PROCEDURE [component].[UP_TRANSACTION_SEARCH]
@loggedInPaxId BIGINT,
@startDate DATETIME2(7),
@endDate DATETIME2(7),
@awardCodeList NVARCHAR(MAX),
@programIdList BIGINT,
@programTypeList NVARCHAR(MAX),
@statusList NVARCHAR(MAX),
@recipientPaxList NVARCHAR(MAX),
@submitterPaxList NVARCHAR(MAX),
@pageNumber BIGINT,
@pageSize BIGINT

AS
BEGIN
SET NOCOUNT ON;

--exec [component].[UP_TRANSACTION_SEARCH] 18537, null, null, null,null,null,null,null,null,40, 100000	--10
--exec [component].[UP_TRANSACTION_SEARCH] 18537, null, null, null,null,null,null,null,null,0, 20	--10
--(+)Logging
--select * from [component].[STORED_PROCS_LOGGING] order by 1 desc
--(+)Logging
BEGIN TRY
declare @loggingStartDate  DATETIME2(7),
		@loggingEndDate  DATETIME2(7),
		@loggingParameters NVARCHAR(MAX),
		@loggingSprocName nvarchar(150),
		@loggingDebug int


--insert component.application_data (key_name, value)
--select 'sql.logging','true'
select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

if (@loggingDebug=1)
begin
set @loggingSprocName = 'UP_TRANSACTION_SEARCH'
set @loggingStartDate = getdate()
set @loggingParameters = '@loggedInPaxId=' + isnull(cast(@loggedInPaxId as varchar),'null') --BIGINT
					+ ' :: @startDate=' + isnull(cast(@startDate as varchar),'null') -- DATETIME2(7)
					+ ' :: @endDate=' + isnull(cast(@endDate as varchar), 'null') -- DATETIME2(7)
					+ ' :: @awardCodeList=' +isnull(@awardCodeList, 'null') -- NVARCHAR(MAX)
					+ ' :: @programIdList=' + isnull(cast(@programIdList as varchar),'null') --BIGINT
					+ ' :: @programTypeList=' + isnull(@programTypeList,'null') -- NVARCHAR(MAX)
					+ ' :: @statusList=' + isnull(@statusList,'null') -- NVARCHAR(MAX)
					+ ' :: @recipientPaxList=' + isnull(@recipientPaxList,'null') -- NVARCHAR(MAX)
					+ ' :: @submitterPaxList=' + isnull(@submitterPaxList,'null') -- NVARCHAR(MAX)
					+ ' :: @pageNumber=' + isnull(cast(@pageNumber as varchar),'null') --BIGINT
					+ ' :: @pageSize=' + isnull(cast(@pageSize as varchar),'null') --BIGINT
end
--(-)Logging





DECLARE    @delimiter CHAR(1)
SET    @delimiter = ',';


DECLARE @T_recipientPaxList TABLE(
PAX_ID bigint null
)
insert @T_recipientPaxList (PAX_ID) SELECT * FROM component.UF_LIST_TO_TABLE_ID(@recipientPaxList, @delimiter)


DECLARE @T_programIdList TABLE(
PROGRAM_ID bigint null
)
insert @T_programIdList (PROGRAM_ID) SELECT * FROM component.UF_LIST_TO_TABLE_ID(@programIdList, @delimiter)


DECLARE @T_awardCodeList TABLE(
MISC_DATA nvarchar(256) null
)
insert @T_awardCodeList (MISC_DATA) SELECT * FROM component.UF_LIST_TO_TABLE(@awardCodeList, @delimiter)


DECLARE @T_statusList TABLE(
STATUS_TYPE_CODE nvarchar(50) null
)
insert @T_statusList (STATUS_TYPE_CODE) SELECT * FROM component.UF_LIST_TO_TABLE(@statusList, @delimiter)


DECLARE @T_submitterPaxList TABLE(
PAX_ID bigint null
)
insert @T_submitterPaxList (PAX_ID)  SELECT * FROM component.UF_LIST_TO_TABLE(@submitterPaxList, @delimiter)

DECLARE @T_programTypeList TABLE(
PROGRAM_TYPE_NAME nvarchar(100) null
)
insert @T_programTypeList (PROGRAM_TYPE_NAME) SELECT * FROM component.UF_LIST_TO_TABLE(@programTypeList, @delimiter)



DECLARE @A TABLE(
TRANSACTION_ID bigint null,
[CREATE_DATE] [datetime2](7) NOT NULL,
NOMINATION_ID bigint null
,TOTAL_RESULTS int);


DECLARE @E TABLE(
em varchar(250));


WITH QUERY AS (
SELECT 
NULL AS TRANSACTION_ID,
NOM.SUBMITTAL_DATE AS CREATE_DATE,
NOM.ID AS NOMINATION_ID
FROM  
component.NOMINATION												NOM			--*******
INNER JOIN component.PROGRAM										P			--*******
    ON NOM.PROGRAM_ID = P.PROGRAM_ID 
INNER JOIN component.PROGRAM_TYPE									PT			--*******
    ON P.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE 
CROSS APPLY ( 
    SELECT TOP 1 R.ID, R.RECEIVER_PAX_ID, REL.PAX_ID_2 AS MANAGER_PAX 
    FROM component.RECOGNITION R 
    LEFT JOIN component.RELATIONSHIP REL 
        ON REL.RELATIONSHIP_TYPE_CODE='REPORT_TO' 
            AND R.RECEIVER_PAX_ID = REL.PAX_ID_1 
    WHERE R.NOMINATION_ID = NOM.ID 
        AND (@recipientPaxList IS NULL OR R.RECEIVER_PAX_ID IN (SELECT * FROM @T_recipientPaxList)) 
    ORDER BY  
        CASE 
            WHEN R.RECEIVER_PAX_ID = @loggedInPaxId THEN 1 -- Logged pax
            WHEN R.RECEIVER_PAX_ID IN (SELECT * FROM @T_recipientPaxList) THEN 2 -- Searched pax
            WHEN REL.PAX_ID_2 = @loggedInPaxId THEN 3 -- Manager pax
        ELSE 4 END, -- Others
        R.RECEIVER_PAX_ID 
) AS REC  
INNER JOIN component.PAX											SUBMITTER_PAX   
    ON NOM.SUBMITTER_PAX_ID = SUBMITTER_PAX.PAX_ID   
INNER JOIN component.SYS_USER										SUBMITTER_SYS_USER   
    ON SUBMITTER_PAX.PAX_ID = SUBMITTER_SYS_USER.PAX_ID     
LEFT JOIN component.RECOGNITION_MISC								REC_MISC				--*******
    ON REC.ID = REC_MISC.RECOGNITION_ID 
    AND REC_MISC.VF_NAME = 'AWARD_CODE' 
WHERE 
NOM.PARENT_ID IS NULL   -- Raises are not included
AND (@startDate IS NULL OR NOM.SUBMITTAL_DATE > @startDate) 
AND (@endDate IS NULL OR NOM.SUBMITTAL_DATE < @endDate)
AND (@programIdList IS NULL OR P.PROGRAM_ID IN (SELECT * FROM @T_programIdList))
AND (@awardCodeList IS NULL OR REC_MISC.MISC_DATA IN (SELECT * FROM @T_awardCodeList))
AND (@statusList IS NULL OR NOM.STATUS_TYPE_CODE IN (SELECT * FROM  @T_statusList))
AND (@submitterPaxList IS NULL OR NOM.SUBMITTER_PAX_ID IN (SELECT * FROM @T_submitterPaxList))
AND (@programTypeList IS NULL OR PT.PROGRAM_TYPE_NAME IN (SELECT * FROM @T_programTypeList))

-- Point Upload Transaction Query
UNION ALL 
SELECT 
TH.ID AS TRANSACTION_ID,
TH.CREATE_DATE AS CREATE_DATE,
NULL AS NOMINATION_ID
FROM 
component.TRANSACTION_HEADER TH 
INNER JOIN component.DISCRETIONARY DISC 
    ON DISC.TRANSACTION_HEADER_ID = TH.ID 
INNER JOIN component.PROGRAM P 
    ON TH.PROGRAM_ID = P.PROGRAM_ID 
INNER JOIN component.PROGRAM_TYPE PT 
    ON P.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE 
INNER JOIN component.PAX_GROUP RECIPIENT_PAX_GROUP 
    ON TH.PAX_GROUP_ID = RECIPIENT_PAX_GROUP.PAX_GROUP_ID 
INNER JOIN component.PAX RECIPIENT_PAX 
    ON RECIPIENT_PAX_GROUP.PAX_ID = RECIPIENT_PAX.PAX_ID   
WHERE PT.PROGRAM_TYPE_CODE = 'PNTU'   
    AND (@startDate IS NULL OR TH.CREATE_DATE > @startDate) 
    AND (@endDate IS NULL OR TH.CREATE_DATE < @endDate) 
    AND (@programIdList IS NULL OR P.PROGRAM_ID IN (SELECT * FROM @T_programIdList))
    AND (@statusList IS NULL OR TH.STATUS_TYPE_CODE IN (SELECT * FROM  @T_statusList))
    AND (@recipientPaxList IS NULL OR RECIPIENT_PAX.PAX_ID IN (SELECT * FROM @T_recipientPaxList))
    AND (@programTypeList IS NULL OR PT.PROGRAM_TYPE_NAME IN (SELECT * FROM @T_programTypeList))
    AND @submitterPaxList IS NULL
    AND @awardCodeList IS NULL
)
Insert @A (TRANSACTION_ID, CREATE_DATE,NOMINATION_ID,TOTAL_RESULTS)
SELECT *  FROM QUERY CROSS JOIN (SELECT COUNT(*) AS TOTAL_RESULTS FROM QUERY) AS TOTAL_RESULTS 
ORDER BY CREATE_DATE DESC
OFFSET @pageNumber ROWS FETCH NEXT @pageSize ROWS ONLY  --This @A will only hold to items to pass back to UI



--Here we go after just those few records in @A to gether the small records details to pass back to UI
SELECT 
NULL AS TRANSACTION_ID,
NOM.SUBMITTAL_DATE AS CREATE_DATE,
CASE
    WHEN ((SELECT COUNT(DISTINCT(STATUS_TYPE_CODE)) FROM component.RECOGNITION WHERE NOMINATION_ID = NOM.ID) > 1) THEN 'MIXED'
    ELSE NOM.STATUS_TYPE_CODE
END AS STATUS,
NOM.ID AS NOMINATION_ID,
REC_MISC.MISC_DATA AS AWARD_CODE,  
SUBMITTER_PAX.PAX_ID AS SUBMITTER_ID,  
SUBMITTER_SYS_USER.STATUS_TYPE_CODE AS SUBMITTER_STATUS_TYPE_CODE,  
SUBMITTER_PAX.LANGUAGE_CODE AS SUBMITTER_LANGUAGE_CODE, 
SUBMITTER_PAX_DEFAULT_LOCALE.MISC_DATA AS SUBMITTER_LOCALE,  
SUBMITTER_PAX.PREFERRED_LOCALE AS SUBMITTER_LANGUAGE_LOCALE,  
SUBMITTER_PAX.CONTROL_NUM AS SUBMITTER_CONTROL_NUM,  
SUBMITTER_PAX.FIRST_NAME AS SUBMITTER_FIRST_NAME,  
SUBMITTER_PAX.MIDDLE_NAME AS SUBMITTER_MIDDLE_NAME,  
SUBMITTER_PAX.LAST_NAME AS SUBMITTER_LAST_NAME,  
SUBMITTER_PAX.NAME_PREFIX AS SUBMITTER_NAME_PREFIX,  
SUBMITTER_PAX.NAME_SUFFIX AS SUBMITTER_NAME_SUFFIX,  
SUBMITTER_PAX.COMPANY_NAME AS SUBMITTER_COMPANY_NAME,  
(  
    SELECT   
        MAX(PF.VERSION)   
    FROM component.PAX_FILES PF   
    WHERE PF.PAX_ID = SUBMITTER_PAX.PAX_ID   
    GROUP BY PF.PAX_ID   
) AS SUBMITTER_VERSION,   
(   
    SELECT TOP 1   
        MISC_DATA   
    FROM component.PAX_GROUP_MISC   
    WHERE PAX_GROUP_ID = (   
        SELECT TOP 1   
            PAX_GROUP_ID   
        FROM component.PAX_GROUP   
        WHERE PAX_ID = SUBMITTER_PAX.PAX_ID   
        ORDER BY (   
            CASE   
                WHEN FROM_DATE IS NULL   
                THEN 1   
                ELSE 0  
            END   
            ) DESC,   
            FROM_DATE DESC   
    ) AND VF_NAME = 'JOB_TITLE'   
) AS SUBMITTER_JOB_TITLE,   
SUBMITTER_SYS_USER.SYS_USER_ID AS SUBMITTER_SYS_USER_ID,   
SUBMITTER_MANAGER.PAX_ID_2 AS SUBMITTER_MANAGER_PAX_ID,   
RECIPIENT_PAX.PAX_ID AS RECIPIENT_ID,   
RECIPIENT_SYS_USER.STATUS_TYPE_CODE AS RECIPIENT_STATUS_TYPE_CODE,   
RECIPIENT_PAX.LANGUAGE_CODE AS RECIPIENT_LANGUAGE_CODE,   
RECIPIENT_PAX_DEFAULT_LOCALE.MISC_DATA AS RECIPIENT_LOCALE,   
RECIPIENT_PAX.PREFERRED_LOCALE AS RECIPIENT_LANGUAGE_LOCALE,   
RECIPIENT_PAX.CONTROL_NUM AS RECIPIENT_CONTROL_NUM,   
RECIPIENT_PAX.FIRST_NAME AS RECIPIENT_FIRST_NAME,   
RECIPIENT_PAX.MIDDLE_NAME AS RECIPIENT_MIDDLE_NAME,   
RECIPIENT_PAX.LAST_NAME AS RECIPIENT_LAST_NAME,   
RECIPIENT_PAX.NAME_PREFIX AS RECIPIENT_NAME_PREFIX,   
RECIPIENT_PAX.NAME_SUFFIX AS RECIPIENT_NAME_SUFFIX,   
RECIPIENT_PAX.COMPANY_NAME AS RECIPIENT_COMPANY_NAME,   
(   
    SELECT   
        MAX(PF.VERSION)   
    FROM component.PAX_FILES PF   
WHERE PF.PAX_ID = RECIPIENT_PAX.PAX_ID   
    GROUP BY PF.PAX_ID   
) AS RECIPIENT_VERSION,   
(   
    SELECT TOP 1   
        MISC_DATA   
    FROM component.PAX_GROUP_MISC   
    WHERE PAX_GROUP_ID = (   
        SELECT TOP 1   
        PAX_GROUP_ID   
        FROM component.PAX_GROUP   
        WHERE PAX_ID = RECIPIENT_PAX.PAX_ID   
        ORDER BY (   
            CASE   
                WHEN FROM_DATE IS NULL   
                THEN 1   
                ELSE 0   
            END   
            ) DESC,   
            FROM_DATE DESC   
    ) AND VF_NAME = 'JOB_TITLE'   
) AS RECIPIENT_JOB_TITLE,   
RECIPIENT_SYS_USER.SYS_USER_ID AS RECIPIENT_SYS_USER_ID,   
REC.MANAGER_PAX AS RECIPIENT_MANAGER_PAX_ID, 
P.PROGRAM_NAME, 
PT.PROGRAM_TYPE_NAME AS PROGRAM_TYPE, 
(SELECT SUM(ISNULL(AMOUNT,0)) FROM component.RECOGNITION WHERE NOMINATION_ID = NOM.ID) AS AWARD_AMOUNT, 
(SELECT ISNULL(COUNT(ID),0) FROM component.RECOGNITION R WHERE NOMINATION_ID = NOM.ID) AS RECIPIENT_COUNT, 
NOM_MISC.MISC_DATA AS PAYOUT_TYPE 
,A.TOTAL_RESULTS
FROM  
component.NOMINATION NOM 
INNER JOIN component.PROGRAM P 
    ON NOM.PROGRAM_ID = P.PROGRAM_ID 
INNER JOIN @A a on a.NOMINATION_ID = NOM.ID
INNER JOIN component.PROGRAM_TYPE PT 
    ON P.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE 
CROSS APPLY ( 
    SELECT TOP 1 R.ID, R.RECEIVER_PAX_ID, REL.PAX_ID_2 AS MANAGER_PAX 
    FROM component.RECOGNITION R 
    LEFT JOIN component.RELATIONSHIP REL 
        ON REL.RELATIONSHIP_TYPE_CODE='REPORT_TO' 
            AND R.RECEIVER_PAX_ID = REL.PAX_ID_1 
    WHERE R.NOMINATION_ID = NOM.ID 
        AND (@recipientPaxList IS NULL OR R.RECEIVER_PAX_ID IN (SELECT * FROM @T_recipientPaxList)) 
    ORDER BY  
        CASE 
            WHEN R.RECEIVER_PAX_ID = @loggedInPaxId THEN 1 -- Logged pax
            WHEN R.RECEIVER_PAX_ID IN (SELECT * FROM @T_recipientPaxList) THEN 2 -- Searched pax
            WHEN REL.PAX_ID_2 = @loggedInPaxId THEN 3 -- Manager pax
        ELSE 4 END, -- Others
        R.RECEIVER_PAX_ID 
) AS REC  
INNER JOIN component.PAX SUBMITTER_PAX   
    ON NOM.SUBMITTER_PAX_ID = SUBMITTER_PAX.PAX_ID   
INNER JOIN component.SYS_USER SUBMITTER_SYS_USER   
    ON SUBMITTER_PAX.PAX_ID = SUBMITTER_SYS_USER.PAX_ID     
LEFT JOIN component.PAX RECIPIENT_PAX   
    ON REC.RECEIVER_PAX_ID = RECIPIENT_PAX.PAX_ID   
LEFT JOIN component.SYS_USER RECIPIENT_SYS_USER   
    ON RECIPIENT_PAX.PAX_ID = RECIPIENT_SYS_USER.PAX_ID   
LEFT JOIN component.RELATIONSHIP SUBMITTER_MANAGER   
    ON SUBMITTER_PAX.PAX_ID = SUBMITTER_MANAGER.PAX_ID_1   
    AND SUBMITTER_MANAGER.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' 
LEFT JOIN component.RECOGNITION_MISC REC_MISC   
    ON REC.ID = REC_MISC.RECOGNITION_ID 
    AND REC_MISC.VF_NAME = 'AWARD_CODE' 
LEFT JOIN component.NOMINATION_MISC NOM_MISC 
    ON NOM.ID = NOM_MISC.NOMINATION_ID 
    AND NOM_MISC.VF_NAME = 'PAYOUT_TYPE'
LEFT JOIN component.PAX_MISC SUBMITTER_PAX_DEFAULT_LOCALE
    ON SUBMITTER_PAX.PAX_ID = SUBMITTER_PAX_DEFAULT_LOCALE.PAX_ID
    AND SUBMITTER_PAX_DEFAULT_LOCALE.VF_NAME = 'DEFAULT_LOCALE'
LEFT JOIN component.PAX_MISC RECIPIENT_PAX_DEFAULT_LOCALE
    ON RECIPIENT_PAX.PAX_ID = RECIPIENT_PAX_DEFAULT_LOCALE.PAX_ID
    AND RECIPIENT_PAX_DEFAULT_LOCALE.VF_NAME = 'DEFAULT_LOCALE'
UNION ALL 
SELECT 
TH.ID AS TRANSACTION_ID,
TH.CREATE_DATE AS CREATE_DATE,
TH.STATUS_TYPE_CODE AS STATUS,
NULL AS NOMINATION_ID,
NULL AS AWARD_CODE,
NULL AS SUBMITTER_PAX_ID,  
NULL AS SUBMITTER_PAX_STATUS_TYPE_CODE,  
NULL AS SUBMITTER_PAX_LANGUAGE_CODE,  
NULL AS SUBMITTER_PAX_LOCALE,  
NULL AS SUBMITTER_PAX_LANGUAGE_LOCALE,  
NULL AS SUBMITTER_PAX_CONTROL_NUM,  
NULL AS SUBMITTER_PAX_FIRST_NAME,  
NULL AS SUBMITTER_PAX_MIDDLE_NAME,  
NULL AS SUBMITTER_PAX_LAST_NAME,  
NULL AS SUBMITTER_PAX_NAME_PREFIX,  
NULL AS SUBMITTER_PAX_NAME_SUFFIX,  
NULL AS SUBMITTER_PAX_COMPANY_NAME,  
NULL AS SUBMITTER_PAX_VERSION,
NULL AS SUBMITTER_PAX_JOB_TITLE,
NULL AS SUBMITTER_PAX_SYS_USER_ID,
NULL AS SUBMITTER_PAX_MANAGER_PAX_ID, 
RECIPIENT_PAX.PAX_ID AS RECIPIENT_PAX_ID,  
RECIPIENT_SYS_USER.STATUS_TYPE_CODE AS RECIPIENT_PAX_STATUS_TYPE_CODE,  
RECIPIENT_PAX.LANGUAGE_CODE AS RECIPIENT_PAX_LANGUAGE_CODE,  
RECIPIENT_PAX_DEFAULT_LOCALE.MISC_DATA AS RECIPIENT_PAX_LOCALE,  
RECIPIENT_PAX.PREFERRED_LOCALE AS RECIPIENT_PAX_LANGUAGE_LOCALE,  
RECIPIENT_PAX.CONTROL_NUM AS RECIPIENT_PAX_CONTROL_NUM,  
RECIPIENT_PAX.FIRST_NAME AS RECIPIENT_PAX_FIRST_NAME,  
RECIPIENT_PAX.MIDDLE_NAME AS RECIPIENT_PAX_MIDDLE_NAME,  
RECIPIENT_PAX.LAST_NAME AS RECIPIENT_PAX_LAST_NAME,  
RECIPIENT_PAX.NAME_PREFIX AS RECIPIENT_PAX_NAME_PREFIX,  
RECIPIENT_PAX.NAME_SUFFIX AS RECIPIENT_PAX_NAME_SUFFIX,  
RECIPIENT_PAX.COMPANY_NAME AS RECIPIENT_PAX_COMPANY_NAME,  
(  
    SELECT   
        MAX(PF.VERSION)   
    FROM component.PAX_FILES PF   
WHERE PF.PAX_ID = RECIPIENT_PAX.PAX_ID   
    GROUP BY PF.PAX_ID   
) AS RECIPIENT_PAX_VERSION,   
(   
    SELECT TOP 1   
        MISC_DATA   
    FROM component.PAX_GROUP_MISC   
    WHERE PAX_GROUP_ID = (   
        SELECT TOP 1   
        PAX_GROUP_ID   
        FROM component.PAX_GROUP   
        WHERE PAX_ID = RECIPIENT_PAX.PAX_ID   
        ORDER BY (   
            CASE   
                WHEN FROM_DATE IS NULL   
                THEN 1   
                ELSE 0   
            END   
            ) DESC,   
            FROM_DATE DESC   
    ) AND VF_NAME = 'JOB_TITLE'   
) AS RECIPIENT_PAX_JOB_TITLE,   
RECIPIENT_SYS_USER.SYS_USER_ID AS RECIPIENT_PAX_SYS_USER_ID,   
RECIPIENT_MANAGER.PAX_ID_2 AS RECIPIENT_PAX_MANAGER_PAX_ID, 
P.PROGRAM_NAME, 
PT.PROGRAM_TYPE_NAME AS PROGRAM_TYPE, 
ISNULL(DISC.AMOUNT,0) AS AWARD_AMOUNT,  
1 AS RECIPIENT_COUNT,  -- There is only one recipient per point upload transaction
TH_MISC.MISC_DATA AS PAYOUT_TYPE 
,A.TOTAL_RESULTS
FROM 
component.TRANSACTION_HEADER TH 
INNER JOIN component.DISCRETIONARY DISC 
    ON DISC.TRANSACTION_HEADER_ID = TH.ID 
INNER JOIN @A a on a.TRANSACTION_ID = TH.ID
INNER JOIN component.PROGRAM P 
    ON TH.PROGRAM_ID = P.PROGRAM_ID 
INNER JOIN component.PROGRAM_TYPE PT 
    ON P.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE 
INNER JOIN component.PAX_GROUP RECIPIENT_PAX_GROUP 
    ON TH.PAX_GROUP_ID = RECIPIENT_PAX_GROUP.PAX_GROUP_ID 
INNER JOIN component.PAX RECIPIENT_PAX 
    ON RECIPIENT_PAX_GROUP.PAX_ID = RECIPIENT_PAX.PAX_ID   
INNER JOIN component.SYS_USER RECIPIENT_SYS_USER 
    ON RECIPIENT_PAX.PAX_ID = RECIPIENT_SYS_USER.PAX_ID   
LEFT JOIN component.RELATIONSHIP RECIPIENT_MANAGER 
    ON RECIPIENT_PAX.PAX_ID = RECIPIENT_MANAGER.PAX_ID_1   
    AND RECIPIENT_MANAGER.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' 
LEFT JOIN component.TRANSACTION_HEADER_MISC TH_MISC 
    ON TH.ID = TH_MISC.TRANSACTION_HEADER_ID 
    AND TH_MISC.VF_NAME = 'PAYOUT_TYPE'
LEFT JOIN component.PAX_MISC RECIPIENT_PAX_DEFAULT_LOCALE
    ON RECIPIENT_PAX.PAX_ID = RECIPIENT_PAX_DEFAULT_LOCALE.PAX_ID
    AND RECIPIENT_PAX_DEFAULT_LOCALE.VF_NAME = 'DEFAULT_LOCALE'
WHERE PT.PROGRAM_TYPE_CODE = 'PNTU'   
ORDER BY CREATE_DATE DESC

--(+)Logging
if (@loggingDebug=1)
begin
	set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error  
	  --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY  
BEGIN CATCH
	if (@loggingDebug=1)
	begin
		declare @errorMessage nvarchar(250) 
		SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE() 
		exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
	end
END CATCH
--(-)Logging
END


