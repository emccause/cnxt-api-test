/****** Object:  StoredProcedure [component].[UP_PAX_SEARCH_ADMIN_QUERY]    Script Date: 4/29/2021 1:40:15 PM ******/
IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_PAX_SEARCH_ADMIN_QUERY' AND schema_id = SCHEMA_ID('component'))
BEGIN
exec ('create PROCEDURE component.UP_PAX_SEARCH_ADMIN_QUERY as BEGIN select 1 as c1 END')
END
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ==============================  NAME  ======================================
-- UP_PAX_SEARCH_ADMIN
-- ===========================  DESCRIPTION  ==================================
--
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- SHARPCA		 4/28/2021	 CNXT-488	  CREATED
-- BRIONEMC		 09/07/2021	 CAM-8		  SHOW USERS WITH EMAIL_ADDRESS OR MANAGER NOT SET. USERS WITH TWO EMAIL_ADRESS SHOWS ONLY ONE EMAIL_ADDRESS.
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_PAX_SEARCH_ADMIN_QUERY]

@page_number INT,
@page_size INT,
@first_name nvarchar(100),
@last_name nvarchar(100),
@control_num nvarchar(255),
@user_name nvarchar(255),
@manager_control_number nvarchar(255),
@email_address nvarchar(255)


AS

BEGIN
SET NOCOUNT ON

-- Logging snippet begin

BEGIN TRY
declare @loggingStartDate  DATETIME2(7), @loggingEndDate  DATETIME2(7), @loggingParameters NVARCHAR(MAX), @loggingSprocName nvarchar(150), @loggingDebug int

select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    if (@loggingDebug=1)
begin

set @loggingSprocName = 'UP_PAX_SEARCH_ADMIN_QUERY'
set @loggingStartDate = getdate()
set @loggingParameters = '@page_number=' + isnull(cast(@page_number as varchar), 'null') --BIGINT
+ ' :: @page_size=' + isnull(cast(@page_size as varchar), 'null') --BIGINT
+ ' :: @first_name=' + isnull(@first_name, 'null') -- NVARCHAR(MAX)
+ ' :: @last_name=' + isnull(@last_name, 'null') -- NVARCHAR(MAX)
+ ' :: @control_num=' + isnull(@control_num, 'null') -- NVARCHAR(MAX)
+ ' :: @user_name=' + isnull(@user_name, 'null') -- NVARCHAR(MAX)
+ ' :: @email_address=' + isnull(@email_address, 'null') -- NVARCHAR(MAX)
+ ' :: @manager_control_number=' + isnull(@manager_control_number, 'null') -- NVARCHAR(MAX)

end

--Logging snippet end

CREATE TABLE #temp
(
    PAX_ID BIGINT
    ,	CONTROL_NUMBER NVARCHAR(50)
    ,	FIRST_NAME NVARCHAR(500)
    ,	LAST_NAME NVARCHAR(500)
    ,	USER_NAME NVARCHAR(50)
    ,	STATUS NVARCHAR(20)
    ,	EMAIL_ADDRESS NVARCHAR(100)
    ,	MANAGER_CONTROL_NUMBER NVARCHAR(50)
    ,	JOB_TITLE NVARCHAR(500)
    ,	STATE NVARCHAR(50)
    ,	COUNTRY NVARCHAR(50)
)

    INSERT INTO #TEMP
select p.PAX_ID
     , p.CONTROL_NUM
     , p.FIRST_NAME
     , p.LAST_NAME
     , su.SYS_USER_NAME
     , su.STATUS_type_code
     , e.EMAIL_ADDRESS
     , P2.CONTROL_NUM
     , pgm.misc_data
     , a.STATE
     , a.COUNTRY_CODE
from component.PAX p
         left join component.ADDRESS a on p.PAX_ID = a.PAX_ID and a.address_type_code = 'BUSINESS'
         left join component.SYS_USER su on p.PAX_ID = su.PAX_ID
          OUTER APPLY (
			SELECT TOP 1 em.* FROM component.EMAIL em
			WHERE em.PAX_ID=p.PAX_ID
			ORDER BY 
				CASE
				   WHEN em.preferred='Y' THEN 2 
				   WHEN em.preferred='N' THEN 1 				   
				END desc
		)e
         inner join component.PAX_GROUP pg on p.pax_id = pg.pax_id and pg.role_code = 'RECG'
         left join component.PAX_GROUP_MISC pgm on pg.PAX_GROUP_ID = pgm.PAX_GROUP_ID and pgm.vf_name = 'JOB_TITLE'
         left join component.RELATIONSHIP r on p.PAX_ID = r.PAX_ID_1 and r.relationship_type_code = 'REPORT_TO'
         left join component.pax p2 on r.pax_id_2 = p2.pax_id
where (@first_name is null OR p.first_name like @first_name + '%')
  AND (@last_name is null OR p.last_name like @last_name + '%')
  and p.control_num = isnull(@control_num, p.control_num)
  and su.SYS_USER_NAME = isnull(@user_name, su.SYS_USER_NAME)
  and (e.EMAIL_ADDRESS = isnull(@email_address,e.EMAIL_ADDRESS) OR e.EMAIL_ADDRESS is null)
  and (p2.CONTROL_NUM = isnull(@manager_control_number,p2.CONTROL_NUM) OR p2.CONTROL_NUM is null)

SELECT
    COUNT(*) OVER() AS TOTAL_RESULTS,
        * FROM (
                   SELECT CONTROL_NUMBER, FIRST_NAME, LAST_NAME, USER_NAME, STATUS, EMAIL_ADDRESS, MANAGER_CONTROL_NUMBER, JOB_TITLE, STATE, COUNTRY
                   FROM #temp ) TEMP
ORDER BY LAST_NAME, FIRST_NAME ASC
OFFSET ((@page_number - 1) * @page_size) ROWS FETCH NEXT @page_size ROWS ONLY


drop table #temp

--(+)Logging
    if (@loggingDebug=1)
begin
    set @loggingEndDate = getdate()
    -- Generate a divide-by-zero error
    --SELECT  1 / 0 AS Error;
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY

BEGIN CATCH
if (@loggingDebug=1)
begin
        declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage

end
END CATCH
--(-)Logging

END


GO


