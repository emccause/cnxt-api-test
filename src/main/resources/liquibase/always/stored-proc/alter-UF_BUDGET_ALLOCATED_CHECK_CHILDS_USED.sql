SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ==========================================================
-- UF_BUDGET_ALLOCATED_CHECK_CHILDS_USED
-- ===========================  DESCRIPTION  ======================================================
--
-- CHECKS IF ANY OF THE CHILDREN BUDGETS HAVE BEEN USED IN PROGRAM
--
-- ============================  REVISIONS  =======================================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20161102    MP-8650        SPROC TO CHECK IF ANY OF THE CHILD BUDGETS HAVE BEEN USED
-- HARWELLM        20161213    MP-9070        RETURN ONLY ONE 'IS_USED' RECORD IF THE BUDGET IS ASSIGNED TO MULTIPLE PROGRAMS
--
-- ===========================  DECLARATIONS  =====================================================
ALTER FUNCTION component.UF_BUDGET_ALLOCATED_CHECK_CHILDS_USED
(
    @PARENT_ALLOCATED BIGINT
)
RETURNS TABLE
AS
RETURN
(
 WITH BUDGET_LIST AS (
  SELECT 
   B.ID 
  ,B.PARENT_BUDGET_ID 
  FROM BUDGET B
  WHERE B.ID=@PARENT_ALLOCATED
  
  UNION ALL

  SELECT 
   B.ID
  ,B.PARENT_BUDGET_ID 
  FROM BUDGET B
  JOIN BUDGET_LIST AS BL ON B.PARENT_BUDGET_ID=BL.ID

) 
SELECT 
    (CASE
        WHEN SUM(TH.PERFORMANCE_VALUE) IS NOT NULL THEN 'true'
        WHEN SUM(TH.PERFORMANCE_VALUE) IS NULL THEN (        
            SELECT TOP 1
                CASE 
                    WHEN PB.PROGRAM_ID IS NOT NULL 
                    THEN 'true' 
                    ELSE null
                END AS IS_USED
                FROM  PROGRAM_BUDGET PB
                 WHERE  PB.BUDGET_ID = @PARENT_ALLOCATED
        )
        ELSE null
    END )AS IS_USED
FROM BUDGET_LIST BL
LEFT JOIN DISCRETIONARY disc 
    ON disc.BUDGET_ID_CREDIT =BL.ID 
    OR disc.BUDGET_ID_DEBIT = BL.ID
LEFT JOIN TRANSACTION_HEADER th on th.ID = disc.TRANSACTION_HEADER_ID 
     AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE <> 'FUND' 
     AND th.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING')

)
GO