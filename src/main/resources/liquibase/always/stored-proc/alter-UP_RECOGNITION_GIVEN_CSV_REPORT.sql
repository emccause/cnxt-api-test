/****** Object:  StoredProcedure [component].[UP_RECOGNITION_GIVEN_CSV_REPORT]    Script Date: 10/20/2020 3:20:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_GIVEN_CSV_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS GIVEN BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR   	DATE        STORY  	CHANGE DESCRIPTION
-- THOMSOND 	20081028                CREATED
-- KOTHAH       20110916                CHANGED TO GET THE AWARD TYPES
-- CHIDRIS      20130328                ADDED LOCALE CODE
-- BESSBS       20130423                ADDED FROM/THRU DATES FOR L10N
-- DOHOGNTA     20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM       20151113                UDM4 CHANGES
-- WRIGHTM      20151117                CHANGED SATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
-- burgestl	20201021		Update join to sql 2017
-- 
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_GIVEN_CSV_REPORT]
    @submitter_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- DRIVER
CREATE TABLE #nomination (
    nomination_id BIGINT NOT NULL
,    nomination_status NVARCHAR(10) NULL

,    submittal_date DATETIME2 NULL
,    submitter_list NVARCHAR(1000) NULL
,    submitter_name NVARCHAR(80) NULL

,    recognition_id    BIGINT NULL
,    total_points INT NULL
,    total_pending_points INT NULL
,    total_denied_points INT NULL
,    total_approved_points INT NULL

,    receiver_list VARCHAR(1000) NULL
,    receiver_name VARCHAR(80) NULL

,    approval_process_id BIGINT NULL
,    approval_notes NVARCHAR(MAX) NULL
,    approval_count INT NULL

,    approver_count INT NULL
,    approver_name VARCHAR(80)

,    last_approval_date DATETIME2 NULL

,    comment NVARCHAR(MAX) NULL
,    comment_type char(1) NULL
,    justification_comment_available_flag INT NULL DEFAULT 0
,    justification_flag INT NULL DEFAULT 0

,    criteria NVARCHAR(1024)

,    program_id BIGINT NULL
,    program_name NVARCHAR(40) NULL
,    award_type NVARCHAR(40) NULL
,    budget_id BIGINT NULL
)

CREATE TABLE #award_types (
    nomination_id BIGINT NULL
,    award_type NVARCHAR(40) NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_GIVEN_BASE @submitter_pax_group_id,    @submittal_date_from,    @submittal_date_thru, @locale_code

UPDATE #nomination
SET    nomination_status =
    CASE
        WHEN nomination_status = 'PENDING' THEN 'Pending'
        WHEN nomination_status = 'APPROVED' THEN 'Approved'
        WHEN nomination_status = 'REJECTED' THEN 'Denied'
        WHEN nomination_status = 'COMPLETE' THEN 'Complete'
    END
,    approver_name = 
    CASE WHEN approver_name = '>1' THEN 'Multiple'
         ELSE approver_name END
,    submitter_name =
    CASE WHEN submitter_name = '>1' THEN 'Multiple' 
         ELSE submitter_name END
,    receiver_name =
    CASE WHEN receiver_name = '>1' THEN 'Multiple' 
         ELSE receiver_name END

SELECT    CONVERT(VARCHAR, #nomination.submittal_date, 101) as SUBMIT_DATE
,    receiver_name
,    NOMINATION.COMMENT
,    NOMINATION.COMMENT_JUSTIFICATION
,    component.UF_TRANSLATE (@locale_code, nomination_status, 'Reporting', 'Reporting') AS nomination_status
,    approval_notes
,    approver_name
,    CONVERT(VARCHAR, last_approval_date, 101) AS APPROVAL_DATE
,    submitter_name
,    component.UF_TRANSLATE (@locale_code, program_name, 'PROGRAM', 'PROGRAM_NAME') AS program_name
,    criteria
,    total_points
,    CASE
        WHEN award_type = 'Points'
        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
        ELSE component.UF_TRANSLATE (@locale_code, award_type, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC')
    END AS award_type
,    nomination_id AS Tracking_Number
FROM #nomination 
INNER JOIN NOMINATION ON #nomination.nomination_id = NOMINATION.ID

SELECT ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ') AS EMPLOYEE
FROM PAX 
INNER JOIN PAX_GROUP ON PAX.PAX_ID = PAX_GROUP.PAX_ID 
WHERE PAX_GROUP.PAX_GROUP_ID = @submitter_pax_group_id


SELECT @submittal_date_from AS FROM_DATE, @submittal_date_thru AS THRU_DATE

END

GO
