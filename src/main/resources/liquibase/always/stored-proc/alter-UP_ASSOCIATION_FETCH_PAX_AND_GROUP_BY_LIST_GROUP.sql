/****** Object:  StoredProcedure [component].[UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP]    Script Date: 10/14/2015 12:32:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S)
-- RETURN A DISTINCT LIST OF PAX INFORMATION AND THE CORRESPONDING GROUP INFORMATION
--
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		STORY		CHANGE DESCRIPTION
-- DOHOGNTA     20080530                CREATED
-- DANDUR       20120730                MODIFIED FOR FETCHING MULTIPLE GROUPS
-- DOHOGNTA     20150304                REMOVE ASSOCIATION TABLE REFERENCE
-- DOHOGNTA     20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM       20151113                UDM4 CHANGES
-- burgestl		20201021				update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP]
    @delim_list_group VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)
DECLARE    @fk TABLE ([id] BIGINT)

-- DRIVER
DECLARE @driver TABLE (
    pax_id BIGINT NOT NULL
,    group_id BIGINT NOT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF GROUP_ID(S) TO A LIST OF INTEGER(S)
INSERT INTO @fk ([id]) SELECT token AS GROUP_ID FROM UF_PARSE_STRING_TO_INTEGER (@delim_list_group, @delimiter)

-- LOAD PAX DIRECTLY ASSOCIATED WITH EACH GROUP(S); ONLY LOAD THE FIRST PAX / GROUP RELATIONSHIP
INSERT INTO @driver (
  pax_id
, group_id
)
SELECT
  GROUPS_PAX.PAX_ID
, GROUPS_PAX.GROUP_ID
FROM    
    GROUPS_PAX
inner join @fk AS driver on GROUPS_PAX.GROUP_ID = driver.[id]
WHERE GROUPS_PAX.GROUP_ID IN (
        SELECT    GROUPS_PAX2.GROUP_ID
        FROM    GROUPS_PAX AS GROUPS_PAX2
        inner join @fk AS driver2 on GROUPS_PAX2.GROUP_ID = driver2.[id]
        WHERE    GROUPS_PAX2.PAX_ID = GROUPS_PAX.PAX_ID
            
)

SELECT    PAX.PAX_ID
,    PAX.CONTROL_NUM
,    PAX.FIRST_NAME
,    PAX.MIDDLE_NAME
,    PAX.LAST_NAME
,    GROUPS.GROUP_DESC
FROM    @driver AS driver
inner join PAX on driver.pax_id = PAX.PAX_ID
inner join GROUPS on driver.group_id = GROUPS.GROUP_ID
ORDER BY PAX.LAST_NAME

END

GO

