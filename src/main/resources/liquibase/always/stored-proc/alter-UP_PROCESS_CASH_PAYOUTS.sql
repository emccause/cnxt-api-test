
-- ==============================  NAME  ===============================================
-- UP_PROCESS_CASH_PAYOUTS
-- ===========================  DESCRIPTION  ===========================================
-- Create a BATCH_ID OF TYPE CASH_REPORT AND PROCESS ALL THE PAYOUTS
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20170329    MP-8927        CREATED
-- GARCIAF2        20170512    MP-10229    BATCH NEEDS TO BE 'IN_PROGRESS' INSTEAD OF 'NEW'
-- GARCIAF2        20170629    MP-10500    IMPROVE PERFORMANCE
--
-- ===========================  DECLARATIONS  ==========================================
ALTER PROCEDURE [component].[UP_PROCESS_CASH_PAYOUTS]

AS 
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

DECLARE @intErrorCode INT
DECLARE @batch_id BIGINT
DECLARE @payout_id BIGINT

                INSERT INTO component.BATCH (BATCH_TYPE_CODE,STATUS_TYPE_CODE) VALUES('CASH_REPORT','IN_PROGRESS')
                SET @batch_id= @@IDENTITY

                BEGIN TRANSACTION
                    UPDATE component.PAYOUT
                    SET BATCH_ID=@batch_id, STATUS_TYPE_CODE='ISSUED'
                    WHERE EXISTS (
                        SELECT P.PAYOUT_ID FROM component.PAYOUT P
                        JOIN component.EARNINGS_PAYOUT EP ON EP.PAYOUT_ID=P.PAYOUT_ID
                        JOIN component.TRANSACTION_HEADER_EARNINGS THE ON THE.EARNINGS_ID=EP.EARNINGS_ID
                        JOIN component.TRANSACTION_HEADER_MISC THM ON THE.TRANSACTION_HEADER_ID=THM.TRANSACTION_HEADER_ID 
                            AND THM.VF_NAME='PAYOUT_TYPE'
                        WHERE p.STATUS_TYPE_CODE='PROCESSED' 
                        AND P.BATCH_ID IS NULL
                        AND THM.MISC_DATA='CASH'
                        AND P.PAYOUT_ID=PAYOUT.PAYOUT_ID
                    )
                    
                    SELECT @intErrorCode = @@ERROR
                    IF (@intErrorCode <> 0) GOTO PROBLEM_GRAB_CASH_PAYOUTS

                COMMIT TRANSACTION
             SELECT @batch_id AS 'BATCH_ID', @intErrorCode AS 'ERROR_CODE'

            PROBLEM_GRAB_CASH_PAYOUTS:
             IF (@intErrorCode <> 0) 
             BEGIN
                PRINT 'Unexpected error occurred!'
                ROLLBACK TRANSACTION
                UPDATE component.BATCH SET STATUS_TYPE_CODE='ERROR' WHERE BATCH_ID= @batch_id
            END

END --SP
GO