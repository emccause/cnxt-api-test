/****** Object:  StoredProcedure [component].[UP_PARTICIPANT_TRANSACTIONS_REPORT]    Script Date: 10/02/2018 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_PARTICIPANT_TRANSACTIONS_REPORT
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS TRASNSACTION DETAILS FOR SPECIFIC PARTICIPANTS IN ALL TYPES OF PROGRAMS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20160608                   CREATED
-- HARWELLM        20160627                   SUPPORT ISSUED STATUS
-- GARCIAF2        20160628                   SORT RECORDS BY SUBMITTED_DATE
-- GARCIAF2        20160701                   UPDATE POSTAL_CODE AND COUNTRY_CODE COLUMNS
-- HARWELLM        20160706    MP-7586        SUPPORT REJECTED STATUS INSTEAD OF DENIED
-- MADRIDO        20160722    MP-7794        SWITCH REJECTED TO DENIED
-- MARTINC        20160824    MP-7999        ADDED THE LOOP JOIN OPTION TO TRY TO IMPROVE PERFORMANCE
-- GARCIAF2        20170320    MP-8597        CHANGE COLUMN POINTS TO AWARD_AMOUNT
--                                        ADD PAYOUT COLUMN
-- GARCIAF2        20170322    MP-8927        ADD BATCH_ID COLUMN
-- ARENASW        20170626    MP-10266    PERFORMANCE IMPROVEMENT FOR PARTICIPANT TRANSACTION REPORT
-- MARTINC      20170731    MP-10594    ADD PAX_ID AS A PARAM
-- FREINERT     20170803    MP-10631    CHANGING BUDGET DISPLAY NAME
-- PRUSIKSE		20181002				DROPPED REDUNDANT STATMENT UNDER ISSUED STATUS
-- FLORESC		2021-04-09	CNXT-473	Set "LEFT JOIN" instead of "MERGE INTO" to improve performance.
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_PARTICIPANT_TRANSACTIONS_REPORT]
@start_date DATETIME2(7),
@end_date DATETIME2(7),
@pax_id BIGINT,
@group_id BIGINT,
@pax_team BIGINT,
@pax_org BIGINT,
@delim_type_list NVARCHAR(MAX),
@delim_status_list NVARCHAR(MAX),
@delim_program_type_list NVARCHAR(MAX),
@program_id BIGINT,
@batch_id BIGINT
 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

-- Logging snippet begin

BEGIN TRY
declare @loggingStartDate  DATETIME2(7), @loggingEndDate  DATETIME2(7), @loggingParameters NVARCHAR(MAX), @loggingSprocName nvarchar(150), @loggingDebug int

select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

if (@loggingDebug=1)
begin

set @loggingSprocName = 'UP_PARTICIPANT_TRANSACTIONS_REPORT'
set @loggingStartDate = getdate()
set @loggingParameters = '@pax_id=' + isnull(cast(@pax_id as varchar), 'null') --BIGINT
+ ' :: @start_date=' + isnull(cast(@start_date as varchar), 'null') -- DATETIME2(7)
+ ' :: @end_date=' + isnull(cast(@end_date as varchar), 'null') -- DATETIME2(7)
+ ' :: @group_id=' +isnull(cast(@group_id as varchar), 'null') -- BIGINT
+ ' :: @pax_team=' + isnull(cast(@pax_team as varchar), 'null') --BIGINT
+ ' :: @pax_org=' + isnull(cast(@pax_org as varchar), 'null') -- BIGINT
+ ' :: @delim_type_list=' + isnull(@delim_type_list, 'null') -- NVARCHAR(MAX)
+ ' :: @delim_status_list=' + isnull(@delim_status_list, 'null') -- NVARCHAR(MAX)
+ ' :: @delim_program_type_list=' + isnull(@delim_program_type_list, 'null') -- NVARCHAR(MAX)
+ ' :: @program_id=' + isnull(cast(@program_id as varchar), 'null') --BIGINT
+ ' :: @batch_id=' + isnull(cast(@batch_id as varchar), 'null') --BIGINT
end

--Logging snippet end

DECLARE @PROGRAM_TYPE XML
SELECT @PROGRAM_TYPE = CAST('<A>'+ REPLACE(@delim_program_type_list,',','</A><A>')+ '</A>' AS XML);

-- PARSE DELIMITED LIST OF STATUS(ES) TO A LIST OF STRING(S)
SET @delim_status_list = REPLACE(@delim_status_list, 'APPROVED', 'APPROVED,AUTO_APPROVED')
SET @delim_status_list = REPLACE(@delim_status_list, 'REJECTED', 'REJECTED,REJECTED_RESTRICTED,REJECTED_INACTIVE')
SET @delim_status_list = REPLACE(@delim_status_list, 'PENDING', 'PENDING,HOLD')
SET @delim_status_list = REPLACE(@delim_status_list, 'ISSUED', 'ISSUED,APPROVED,AUTO_APPROVED')

DECLARE @STATUS_TYPE XML
SELECT @STATUS_TYPE = CAST('<A>'+ REPLACE(@delim_status_list,',','</A><A>')+ '</A>' AS XML);

DECLARE @issued_only NVARCHAR(5)
SET @issued_only = (SELECT CASE WHEN @delim_status_list LIKE '%ISSUED%' THEN 'TRUE'ELSE 'FALSE' END)


--Build up the audience
--One of these three should be populated to determine audience:  groupId, paxTeam, or paxOrg
CREATE TABLE #audience (
    PAX_ID BIGINT NOT NULL
)

INSERT INTO #audience EXEC component.UP_HISTORICAL_AUDIENCE @pax_id, @pax_team, @pax_org, @group_id, @start_date, @end_date
ALTER TABLE #AUDIENCE ADD PRIMARY KEY (PAX_ID)

IF OBJECT_ID('tempdb..#TransactionDetails') IS NOT NULL 
BEGIN
    DROP TABLE #TransactionDetails
end

SELECT 
    td.TRANSACTION_ID, 
    td.NOMINATION_ID, 
    td.SUBMITTED_DATE,
    td.RECIPIENT_PAX_ID, 
    td.RECIPIENT_ID, 
    td.RECIPIENT_FIRST_NAME, 
    td.RECIPIENT_LAST_NAME,
    td.SUBMITTER_PAX_ID, 
    td.SUBMITTER_ID, 
    td.SUBMITTER_FIRST_NAME, 
    td.SUBMITTER_LAST_NAME,
    td.APPROVER_PAX_ID, 
    td.APPROVER_ID, 
    td.APPROVER_FIRST_NAME, 
    td.APPROVER_LAST_NAME,
    td.PROGRAM_ID, 
    td.PROGRAM_NAME, 
    td.PROGRAM_TYPE,
    td.VALUE,
    td.HEADLINE,
    CASE
        WHEN td.RECOGNITION_STATUS = 'REJECTED' THEN 'DENIED'
        WHEN td.RECOGNITION_STATUS = 'REJECTED_INACTIVE' THEN 'DENIED_INACTIVE'
        WHEN td.RECOGNITION_STATUS = 'REJECTED_RESTRICTED' THEN 'DENIED_RESTRICTED'
        ELSE td.RECOGNITION_STATUS
    END AS 'RECOGNITION_STATUS',
    td.AWARD_AMOUNT, 
    td.PAYOUT_TYPE, 
    td.BATCH_ID,
    bil.DISPLAY_NAME AS 'BUDGET_NAME',
    td.BUDGET_STATUS,
    td.BUDGET_ID, 
    td.APPROVAL_DATE, 
    td.ISSUANCE_DATE, 
    td.AWARD_CODE,
    td.COMMENT
	
	into #TransactionDetails 

    FROM component.VW_TRANSACTION_DETAILS td
    LEFT JOIN (SELECT t.value('.', 'varchar(100)') AS CODE FROM @STATUS_TYPE.nodes('/A') AS x(t)) STATUS_TYPE
        ON TD.RECOGNITION_STATUS = STATUS_TYPE.CODE
    LEFT JOIN (SELECT t.value('.', 'varchar(100)') AS CODE FROM @PROGRAM_TYPE.nodes('/A') AS x(t)) PROGRAM_TYPE
        ON TD.PROGRAM_TYPE = PROGRAM_TYPE.CODE
    LEFT JOIN component.VW_BUDGET_INFO_LIGHTWEIGHT bil ON bil.ID = td.BUDGET_ID
    WHERE
    (@batch_id IS NULL AND
    --Date Filter
    (
        (td.SUBMITTED_DATE BETWEEN @start_date AND @end_date)
        OR
        (td.SUBMITTED_DATE > @start_date AND @end_date IS NULL)
    )
    --Status Filter
    AND 
    (@delim_status_list IS NULL OR STATUS_TYPE.CODE IS NOT NULL)
    --Program Filter
    AND 
    (
    (@delim_program_type_list IS NULL OR PROGRAM_TYPE.CODE IS NOT NULL)
    )
    AND (@program_id IS NULL OR td.PROGRAM_ID = @program_id)
    --Type Filter
    AND (
        (@delim_type_list LIKE '%GIVEN%' AND td.SUBMITTER_PAX_ID in (select * from #audience))
        OR (@delim_type_list LIKE '%RECEIVED%' AND td.RECIPIENT_PAX_ID in (select * from #audience))
    )
    --ISSUED status
    
    --DBA STEVE PRUSIK 20181002
    AND (@issued_only = 'FALSE' OR (@issued_only = 'TRUE' AND td.ISSUANCE_DATE IS NOT NULL))
    AND (@batch_id IS NULL OR (td.PAYOUT_TYPE='CASH' AND td.BATCH_ID=@batch_id)))
    --DBA STEVE PRUSIK 20181002 - THIS OR STATEMENT IS REDUNDANT
    --OR (td.PAYOUT_TYPE='CASH' AND td.BATCH_ID=@batch_id)

CREATE NONCLUSTERED INDEX ix_PAX_ID ON #TransactionDetails (RECIPIENT_PAX_ID,SUBMITTER_PAX_ID,APPROVER_PAX_ID);

IF OBJECT_ID('tempdb..#PAX_MISC_INFO') IS NOT NULL 
BEGIN
    DROP TABLE #PAX_MISC_INFO
end
select * into #PAX_MISC_INFO from component.VW_PAX_MISC_INFO
CREATE NONCLUSTERED INDEX ix_PAX_ID ON #PAX_MISC_INFO (PAX_ID);

select
	td.TRANSACTION_ID, td.NOMINATION_ID, td.SUBMITTED_DATE,td.RECIPIENT_PAX_ID, td.RECIPIENT_ID,td.RECIPIENT_FIRST_NAME, td.RECIPIENT_LAST_NAME, 
	td.SUBMITTER_PAX_ID, td.SUBMITTER_ID,td.SUBMITTER_FIRST_NAME,td.SUBMITTER_LAST_NAME,
	td.APPROVER_PAX_ID,td.APPROVER_ID,td.APPROVER_FIRST_NAME,td.APPROVER_LAST_NAME,
	td.PROGRAM_ID, 
	td.PROGRAM_NAME,
    td.PROGRAM_TYPE,
    td.VALUE,
    td.HEADLINE,
    td.RECOGNITION_STATUS,
    td.AWARD_AMOUNT,
    td.PAYOUT_TYPE,
    td.BATCH_ID,
    td.BUDGET_NAME,
    td.BUDGET_STATUS,
    td.BUDGET_ID,
    td.APPROVAL_DATE,
    td.ISSUANCE_DATE,
    td.AWARD_CODE,

	pmi_r.EMAIL_ADDRESS as 'RECIPIENT_EMAIL',
	pmi_r.CITY as 'RECIPIENT_CITY',
	pmi_r.STATE as 'RECIPIENT_STATE',
	pmi_r.ZIP as 'RECIPIENT_POSTAL_CODE',
	pmi_r.COUNTRY_CODE as 'RECIPIENT_COUNTRY_CODE',
	pmi_r.title as 'RECIPIENT_TITLE',
	pmi_r.COMPANY_NAME as 'RECIPIENT_COMPANY_NAME',
	pmi_r.DEPARTMENT as 'RECIPIENT_DEPARTMENT',
	pmi_r.COST_CENTER as 'RECIPIENT_COST_CENTER',
	pmi_r.PAX_FUNCTION as 'RECIPIENT_FUNCTION',
	pmi_r.GRADE as 'RECIPIENT_GRADE',
	pmi_r.AREA as 'RECIPIENT_AREA',
	pmi_r.CUSTOM_1 as 'RECIPIENT_CUSTOM_1',
	pmi_r.CUSTOM_2 as 'RECIPIENT_CUSTOM_2',
	pmi_r.CUSTOM_3 as 'RECIPIENT_CUSTOM_3',
	pmi_r.CUSTOM_4 as 'RECIPIENT_CUSTOM_4',
	pmi_r.CUSTOM_5 as 'RECIPIENT_CUSTOM_5',
	
	pmi_s.EMAIL_ADDRESS as 'SUBMITTER_EMAIL',
	pmi_s.CITY as 'SUBMITTER_CITY',
	pmi_s.STATE as 'SUBMITTER_STATE',
	pmi_s.ZIP as 'SUBMITTER_POSTAL_CODE',
	pmi_s.COUNTRY_CODE as 'SUBMITTER_COUNTRY_CODE',
	pmi_s.title as 'SUBMITTER_TITLE',
	pmi_s.COMPANY_NAME as 'SUBMITTER_COMPANY_NAME',
	pmi_s.DEPARTMENT as 'SUBMITTER_DEPARTMENT',
	pmi_s.COST_CENTER as 'SUBMITTER_COST_CENTER',
	pmi_s.PAX_FUNCTION as 'SUBMITTER_FUNCTION',
	pmi_s.GRADE as 'SUBMITTER_GRADE',
	pmi_s.AREA as 'SUBMITTER_AREA',
	pmi_s.CUSTOM_1 as 'SUBMITTER_CUSTOM_1',
	pmi_s.CUSTOM_2 as 'SUBMITTER_CUSTOM_2',
	pmi_s.CUSTOM_3 as 'SUBMITTER_CUSTOM_3',
	pmi_s.CUSTOM_4 as 'SUBMITTER_CUSTOM_4',
	pmi_s.CUSTOM_5 as 'SUBMITTER_CUSTOM_5',
	
	pmi_a.EMAIL_ADDRESS as 'APPROVER_EMAIL',
	pmi_a.CITY as 'APPROVER_CITY',
	pmi_a.STATE as 'APPROVER_STATE',
	pmi_a.ZIP as 'APPROVER_POSTAL_CODE',
	pmi_a.COUNTRY_CODE as 'APPROVER_COUNTRY_CODE',
	pmi_a.title as 'APPROVER_TITLE',
	pmi_a.COMPANY_NAME as 'APPROVER_COMPANY_NAME',
	pmi_a.DEPARTMENT as 'APPROVER_DEPARTMENT',
	pmi_a.COST_CENTER as 'APPROVER_COST_CENTER',
	pmi_a.PAX_FUNCTION as 'APPROVER_FUNCTION',
	pmi_a.GRADE as 'APPROVER_GRADE',
	pmi_a.AREA as 'APPROVER_AREA',
	pmi_a.CUSTOM_1 as 'APPROVER_CUSTOM_1',
	pmi_a.CUSTOM_2 as 'APPROVER_CUSTOM_2',
	pmi_a.CUSTOM_3 as 'APPROVER_CUSTOM_3',
	pmi_a.CUSTOM_4 as 'APPROVER_CUSTOM_4',
	pmi_a.CUSTOM_5 as 'APPROVER_CUSTOM_5',
	td.COMMENT

	from #TransactionDetails td 
	left join #PAX_MISC_INFO pmi_r on td.RECIPIENT_PAX_ID = pmi_r.PAX_ID
	left join #PAX_MISC_INFO pmi_s on td.SUBMITTER_PAX_ID = pmi_s.PAX_ID
	left join #PAX_MISC_INFO pmi_a on td.APPROVER_PAX_ID = pmi_a.PAX_ID
	ORDER BY td.SUBMITTED_DATE

DROP TABLE #TransactionDetails
DROP TABLE #audience

--(+)Logging
if (@loggingDebug=1)
begin
    set @loggingEndDate = getdate()
    -- Generate a divide-by-zero error
    --SELECT  1 / 0 AS Error;
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY

BEGIN CATCH
    if (@loggingDebug=1)
    begin
        declare @errorMessage nvarchar(250)
        SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
        exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage

    end
END CATCH
--(-)Logging

END;
GO