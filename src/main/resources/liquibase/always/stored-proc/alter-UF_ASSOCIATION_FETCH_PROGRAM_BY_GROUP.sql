/****** Object:  UserDefinedFunction [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP]    Script Date: 10/16/2020 12:22:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID
-- RETURN A DISTINCT LIST OF PROGRAM_ACTIVITY_ID(S) ASSOCIATED WITH THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR		DATE		CHANGE DESCRIPTION
-- DOHOGNTA		20080530	CREATED
-- DOHOGNTA		20150309	CHANGE IDENTITIES TO BIGINT
-- ERICKSRT     	20150811    	CHANGE PROGRAM_ACTIVITY_ID to PROGRAM_ID
-- burgestl		20201103	Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER FUNCTION [component].[UF_ASSOCIATION_FETCH_PROGRAM_BY_GROUP] (
	@group_id BIGINT)
RETURNS @result_set TABLE (
	[id] BIGINT NULL)
AS
BEGIN

DECLARE @fk TABLE ([id] BIGINT NULL)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET LIST OF ANCESTOR GROUP(S); INCLUDING THE GIVEN GROUP
INSERT @fk SELECT [id] FROM UF_ASSOCIATION_FETCH_GROUP_BY_GROUP (@group_id, -1)

-- GET LIST OF PROGRAM_ACTIVITY(S) ASSOCIATED WITH GROUP(S)
INSERT	@result_set
SELECT	DISTINCT
	ASSOCIATION.FK1 AS PROGRAM_ID
FROM ASSOCIATION
INNER JOIN @fk AS GROUPS ON ASSOCIATION.FK2 = GROUPS.[id]
WHERE ASSOCIATION.ASSOCIATION_TYPE_ID = 400 -- PROGRAM_ACTIVITY_GROUPS

-- RETURN RESULT SET
RETURN

END

GO
