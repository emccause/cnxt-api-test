/****** Object:  StoredProcedure [component].[UP_RECOGNITION_EXTRACT_REPORT]    Script Date: 10/15/2015 11:02:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_EXTRACT_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN NONE OR ANY OF THE CRITERIA PARAMETERS
-- RETURN A RESULT SET OF NOMINATION / RECOGNITION REPORT DETAIL DATA
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- THOMSOND        20080904                CREATED
-- THOMSOND        20080910                CHANGED several of the filtering conditions
-- BESSBS        20080910                CHANGED the way empty group and program lists are checked
-- THOMSOND        20080924                DELETED the conditional logic to parse @delim_list_program
-- THOMSOND        20091001                CHANGED #recognition table select to include pax as submitter or receiver
-- PALD00        20081010                ADDED code to REPLACE ORG/ROLE GROUP DESCRIPTION
-- SCHIERVR        20081103                ADDED @recognition_selection_criteria
-- DOHOGNTA        20081120                PERFORMANCE TUNING / FUNCTION CALLS
-- RIDERAC1        20100720                ADDED index for #pax_hierarchy_list
-- KOTHAH        20111010                CHANGED TO GET THE AWARD TYPES
-- DANDUR        20120306                CHANGED TO RESTRICT THE PROGRAMS BASED ON SELECTION
-- CHINTHR        20120326                ADDED CONDITION TO SELECT PROGRAM_MISC BETWEEN fromdate AND thrudate
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150306                INCREASE THE SIZE OF DATATYPE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151113                UDM4 CHANGES
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- hernancl		 20210601	            update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_EXTRACT_REPORT]
    @delim_list_program VARCHAR(1000)
,    @delim_list_budgets VARCHAR(1000)
,    @status_type_code NVARCHAR(50)
,    @receiver_first_name NVARCHAR(30)
,    @receiver_last_name NVARCHAR(40)
,    @submitter_first_name NVARCHAR(30)
,    @submitter_last_name NVARCHAR(40)
,    @delim_list_group VARCHAR(1000)
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,    @recognition_selection_criteria NVARCHAR(4)
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- GLOBAL VARIABLES
DECLARE    @delimiter CHAR(1)
SET    @delimiter = ','

-- PARSE THE SELECTED PROGRAM IDS 
CREATE TABLE #criteria_list_program (program_id BIGINT NULL)
IF (@delim_list_program IS NOT NULL) AND (LEN(RTRIM(@delim_list_program)) > 0 AND @delim_list_program != 'none')
BEGIN
    INSERT INTO #criteria_list_program SELECT token FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_list_program, @delimiter)
END

-- PARSE THE SELECTED BUDGET IDS 
CREATE TABLE #criteria_list_budget (budget_id BIGINT NULL)
IF (@delim_list_budgets IS NOT NULL) AND (LEN(RTRIM(@delim_list_budgets)) > 0 AND @delim_list_budgets != 'none')
BEGIN
    INSERT INTO #criteria_list_budget SELECT token FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_list_budgets, @delimiter)
END

-- BUILD A LIST OF RECIPIENT PAX IDS FOR ALL DESCENDENTS OF THE GIVEN PAX_GROUP_ID
CREATE TABLE #pax_hierarchy_list (pax_id BIGINT NULL)
DECLARE    @requester_pax_id BIGINT
SET    @requester_pax_id = (SELECT PAX_ID FROM PAX_GROUP WHERE PAX_GROUP_ID = @pax_group_id)
INSERT INTO #pax_hierarchy_list SELECT id AS PAX_ID FROM component.UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX (@requester_pax_id, 1, NULL)

-- CREATE AN INDEX TO MAKE THE INSERT INTO THE #recognition TABLE MORE EFFICIENT
CREATE INDEX IX_PAX_HIERARCHY_LIST_1 ON #pax_hierarchy_list (pax_id)

-- BUILD A LIST OF ALL PAX IN THE HIERARCHIES OF SELECTED GROUPS
CREATE TABLE #criteria_group_pax (pax_id BIGINT NULL)
IF (@delim_list_group IS NOT NULL) AND (LEN(RTRIM(@delim_list_group)) > 0 AND @delim_list_group != 'none')
BEGIN
    INSERT INTO #criteria_group_pax SELECT pax_id FROM component.UF_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP (@delim_list_group, @delimiter)
    -- REMOVE PAX IDS FROM #PAX_HIERARCHY_LIST IF THEY ARE NOT ALSO A MEMBER OF #CRITERIA_GROUP_PAX
    DELETE #pax_hierarchy_list WHERE #pax_hierarchy_list.PAX_ID NOT IN (SELECT PAX_ID FROM #criteria_group_pax)
END

-- DRIVER
CREATE TABLE #recognition (
    nomination_id BIGINT NOT NULL
,    recognition_id BIGINT NOT NULL

,    submitter_pax_id BIGINT NULL
,    submitter_control_number NVARCHAR(255) NULL
,    submitter_first_name NVARCHAR(30) NULL
,    submitter_last_name NVARCHAR(40) NULL

,    submitter_manager_pax_id BIGINT NULL
,    submitter_manager_name NVARCHAR(75) NULL
,    submitter_manager_control_num NVARCHAR(255) NULL
,    submitter_groups VARCHAR(1000) NULL

,    receiver_pax_id BIGINT NULL
,    receiver_control_number NVARCHAR(255) NULL
,    receiver_first_name NVARCHAR(30) NULL
,    receiver_last_name NVARCHAR(40) NULL

,    receiver_manager_pax_id BIGINT NULL
,    receiver_manager_name NVARCHAR(75) NULL
,    receiver_manager_control_num NVARCHAR(255) NULL
,    receiver_groups VARCHAR(1000) NULL

,    approval_process_id BIGINT NULL
,    program_id BIGINT NULL
,    program_name NVARCHAR(400)
,    ecard_id BIGINT NULL
,    nomination_comments NVARCHAR(MAX) NULL

,    last_approval_date DATETIME2 NULL

,    approver_pax_id BIGINT NULL
,    approver_name NVARCHAR(75) NULL

,    criteria NVARCHAR(1000)
,    is_recognition_comment INT NULL
,    nomination_submittal_date DATETIME2
,    recognition_status NCHAR NULL
,    denial_comment NVARCHAR(MAX) NULL
,    recognition_budget NVARCHAR(204) NULL
,    recognition_budget_id BIGINT NULL
,    amount DECIMAL
,    award_type NVARCHAR(40) NULL
)

-- BUILD A MASTER LIST OF RECOGNITION RECORDS BASED UPON #PAX_HIERARCHY_LIST, FROM_DATE AND THRU_DATE
INSERT INTO #recognition (
    nomination_id
,    submitter_pax_id
,    submitter_control_number
,    submitter_first_name
,    submitter_last_name

,    recognition_id
,    receiver_pax_id
,    receiver_control_number
,    receiver_first_name

,    receiver_last_name
,    program_id
,    recognition_budget_id
,    ecard_id
,    nomination_submittal_date
,    recognition_status
,    amount
)
SELECT    DISTINCT
    NOMINATION.ID
,    NOMINATION.SUBMITTER_PAX_ID
,    sub_pax.CONTROL_NUM
,    sub_pax.FIRST_NAME
,    sub_pax.LAST_NAME

,    RECOGNITION.ID
,    RECOGNITION.RECEIVER_PAX_ID
,    rec_pax.CONTROL_NUM
,    rec_pax.FIRST_NAME
,    rec_pax.LAST_NAME
,    NOMINATION.PROGRAM_ID
,    NOMINATION.RECOGNITION_BUDGET_ID
,    ECARD_ID
,    NOMINATION.SUBMITTAL_DATE
,    RECOGNITION.STATUS
,    CASE
            WHEN RECOGNITION.AMOUNT >0 THEN RECOGNITION.AMOUNT
            ELSE  null
    END AS AMOUNT
FROM    
    NOMINATION,RECOGNITION
,    #pax_hierarchy_list
,    pax sub_pax
,    pax rec_pax
WHERE     NOMINATION.ID = RECOGNITION.NOMINATION_ID
AND    NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru
AND    RECOGNITION.STATUS_TYPE_CODE IN ('APPROVED', 'PENDING', 'REJECTED')
AND    NOMINATION.SUBMITTER_PAX_ID = sub_pax.pax_id
AND    RECOGNITION.RECEIVER_PAX_ID = rec_pax.pax_id
AND    (RECOGNITION.RECEIVER_PAX_ID = #pax_hierarchy_list.pax_id
OR    NOMINATION.SUBMITTER_PAX_ID = #pax_hierarchy_list.pax_id)

-- GET RID OF RECEIVER FIRST NAMES THAT DON'T MATCH
IF (@receiver_first_name IS NOT NULL) AND (LEN(RTRIM(@receiver_first_name)) > 0)
BEGIN
    DELETE #recognition WHERE receiver_first_name NOT LIKE '%'+@receiver_first_name+'%'
END

-- GET RID OF RECEIVER LAST NAMES THAT DON'T MATCH
IF (@receiver_last_name IS NOT NULL) AND (LEN(RTRIM(@receiver_last_name)) > 0)
BEGIN
    DELETE #recognition WHERE receiver_last_name NOT LIKE '%'+@receiver_last_name+'%'
END

-- GET RID OF SUBMITTER FIRST NAMES THAT DON'T MATCH
IF (@submitter_first_name IS NOT NULL) AND (LEN(RTRIM(@submitter_first_name)) > 0)
BEGIN
    DELETE #recognition WHERE submitter_first_name NOT LIKE '%'+@submitter_first_name+'%'
END

-- GET RID OF SUBMITTER LAST NAMES THAT DON'T MATCH
IF (@submitter_last_name IS NOT NULL) AND (LEN(RTRIM(@submitter_last_name)) > 0)
BEGIN
    DELETE #recognition WHERE submitter_last_name NOT LIKE '%'+@submitter_last_name+'%'
END

-- GET RID OF UNWANTED STATUS'
if(@status_type_code IS NOT NULL) AND (LEN(RTRIM(@status_type_code)) > 0) AND @status_type_code != '%'
BEGIN
    DELETE #recognition WHERE recognition_status <> @status_type_code
END

-- GET RID OF UNWANTED PROGRAMS
if((SELECT COUNT(1) FROM #criteria_list_program) > 0)
BEGIN
    DELETE #recognition WHERE program_id not in (SELECT program_id FROM #criteria_list_program)
END

-- GET RID OF UNWANTED BUDGETS
if((SELECT COUNT(1) FROM #criteria_list_budget) > 0)
BEGIN    
    DELETE #recognition
    WHERE recognition_budget_id NOT IN (SELECT budget_id FROM #criteria_list_budget) OR recognition_budget_id IS NULL
END

-- FILTER OUT THE RECORDS AS PER @recognition_selection_criteria
IF (@recognition_selection_criteria = 'RR')
BEGIN
    DELETE #recognition
    WHERE receiver_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)
END
ELSE
IF (@recognition_selection_criteria = 'RG')
BEGIN
    DELETE #recognition
    WHERE submitter_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)    
END

-- WE NOW HAVE THE COMPLETE SET OF RECOGNITION RECORDS WE WANT TO WORK WITH
-- UPDATE THE DETAILS

UPDATE    #recognition 
SET    approval_process_id = APPROVAL_PROCESS.ID 
FROM    APPROVAL_PROCESS 
WHERE    APPROVAL_PROCESS.TARGET_ID = #recognition.NOMINATION_ID 
AND    APPROVAL_PROCESS.APPROVAL_PROCESS_TYPE_CODE = 'RECG'

-- PROGRAM NAMES
UPDATE    #recognition
SET program_name = component.UF_TRANSLATE (@locale_code, PROGRAM_NAME, 'PROGRAM', 'PROGRAM_NAME')
FROM    PROGRAM 
WHERE    PROGRAM.PROGRAM_ID = #recognition.program_id

-- SUBMITTER MANAGER
UPDATE    #recognition
SET    submitter_manager_pax_id = PAX_REPORT_TO.REPORT_TO_PAX_ID
FROM    PAX_REPORT_TO
WHERE    #recognition.submitter_pax_id = PAX_REPORT_TO.PAX_ID

UPDATE    #recognition
SET    submitter_manager_name = ISNULL(PAX.LAST_NAME + ', ' + PAX.FIRST_NAME, NULL)
,    submitter_manager_control_num = PAX.CONTROL_NUM
FROM    PAX
WHERE    PAX.PAX_ID = #recognition.submitter_manager_pax_id

-- RECEIVER MANAGER
UPDATE    #recognition
SET    receiver_manager_pax_id = PAX_REPORT_TO.REPORT_TO_PAX_ID
FROM    PAX_REPORT_TO
WHERE    #recognition.receiver_pax_id = PAX_REPORT_TO.PAX_ID

UPDATE #recognition
SET    receiver_manager_name = ISNULL(PAX.LAST_NAME + ', ' + PAX.FIRST_NAME, NULL)
,    receiver_manager_control_num = PAX.CONTROL_NUM
FROM    PAX
WHERE    PAX.PAX_ID = #recognition.receiver_manager_pax_id

-- APPROVAL DATE
UPDATE    #recognition
SET    last_approval_date = 
        (SELECT    MAX(APPROVAL_HISTORY.APPROVAL_DATE)
         FROM    APPROVAL_HISTORY
         WHERE    #recognition.recognition_id = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
         AND    recognition_status <> 'PENDING')

-- LAST APPROVER
UPDATE    #recognition
SET    approver_pax_id = APPROVAL_PENDING.PAX_ID
FROM    APPROVAL_PENDING
WHERE    #recognition.approval_process_id = APPROVAL_PENDING.APPROVAL_PROCESS_ID
AND    #recognition.recognition_id = APPROVAL_PENDING.TARGET_ID AND APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'

UPDATE #recognition
SET     approver_name = ISNULL(PAX.LAST_NAME + ', ' + PAX.FIRST_NAME, NULL)
FROM     PAX
WHERE    #recognition.approver_pax_id = PAX.PAX_ID

-- DENIAL COMMENTS
UPDATE #recognition
SET    denial_comment = APPROVAL_HISTORY.APPROVAL_NOTES
FROM    APPROVAL_HISTORY
WHERE    #recognition.recognition_id = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #recognition.approval_process_id
AND    recognition_status = 'REJECTED'
AND    APPROVAL_HISTORY.APPROVAL_STATUS = 'REJECTED'

-- SUBMITTER GROUPS
CREATE TABLE #recognition_submitter (submitter_pax_id BIGINT NULL, submitter_groups VARCHAR(1000) NULL)

INSERT INTO #recognition_submitter (submitter_pax_id) SELECT DISTINCT submitter_pax_id FROM #recognition

UPDATE    #recognition_submitter
SET    submitter_groups = component.UF_ASSOCIATION_FETCH_LIST_GROUP_BY_PAX( #recognition_submitter.submitter_pax_id, -1 )

CREATE INDEX IX_TEST_1 ON #recognition (submitter_pax_id)

UPDATE    #recognition
SET    #recognition.submitter_groups = #recognition_submitter.submitter_groups
FROM    #recognition
INNER JOIN    #recognition_submitter
ON    #recognition.submitter_pax_id = #recognition_submitter.submitter_pax_id

-- RECEIVER GROUPS
CREATE TABLE #recognition_receiver (receiver_pax_id BIGINT NULL, receiver_groups VARCHAR(1000) NULL)

INSERT INTO #recognition_receiver (receiver_pax_id) SELECT DISTINCT receiver_pax_id FROM #recognition

UPDATE    #recognition_receiver
SET    receiver_groups = component.UF_ASSOCIATION_FETCH_LIST_GROUP_BY_PAX( #recognition_receiver.receiver_pax_id, -1 )

CREATE INDEX IX_TEST_2 ON #recognition (receiver_pax_id)

UPDATE    #recognition
SET    #recognition.receiver_groups = #recognition_receiver.receiver_groups
FROM    #recognition
INNER JOIN  #recognition_receiver
ON    #recognition.receiver_pax_id = #recognition_receiver.receiver_pax_id

-- CRITERIA LIST
CREATE TABLE #recognition_critera (nomination_id BIGINT NULL, criteria NVARCHAR(1000) NULL)

INSERT INTO #recognition_critera (nomination_id) SELECT DISTINCT nomination_id FROM #recognition

UPDATE    #recognition_critera
SET    criteria = component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION( #recognition_critera.nomination_id, @locale_code )

CREATE INDEX IX_TEST_3 ON #recognition (nomination_id)

UPDATE    #recognition
SET    #recognition.criteria = #recognition_critera.criteria
FROM    #recognition
INNER JOIN     #recognition_critera
ON    #recognition.nomination_id = #recognition_critera.nomination_id

-- BUDGET 
UPDATE #recognition
SET    recognition_budget = ISNULL(RECOGNITION_BUDGET_CATEGORY_CODE, '') + ' - ' + ISNULL(component.UF_TRANSLATE (@locale_code, RECOGNITION_BUDGET_NAME, 'RECOGNITION_BUDGET', 'RECOGNITION_BUDGET_NAME'), '')
FROM    NOMINATION
INNER JOIN RECOGNITION_BUDGET ON  NOMINATION.RECOGNITION_BUDGET_ID = RECOGNITION_BUDGET.ID
WHERE    #recognition.nomination_id = NOMINATION.ID

-- COMMENTS
UPDATE    #recognition
SET    nomination_comments = NOMINATION.COMMENT
FROM    NOMINATION
WHERE    #recognition.nomination_id = NOMINATION.ID

-- AWARDTYPES
UPDATE    #recognition
SET    award_type = (
    SELECT    AWARD_TYPE        
    FROM    RECOGNITION_BUDGET
    WHERE    #recognition.recognition_budget_id = RECOGNITION_BUDGET.id
)

-- REPLACE ORG/ROLE GROUP DESCRIPTION
UPDATE #recognition SET submitter_groups = REPLACE(submitter_groups, 'RECG_RECG', 'Recognition Participant')
UPDATE #recognition SET receiver_groups = REPLACE(receiver_groups, 'RECG_RECG', 'Recognition Participant')

-- FORMAT THE OUTPUT
SELECT    nomination_submittal_date AS SUBMITTAL_DATE
,    receiver_first_name AS RECIPIENT_FIRST_NAME
,    receiver_last_name AS RECIPIENT_LAST_NAME
,    receiver_control_number AS RECIPIENT_ID
,    submitter_first_name AS SUBMITTER_FIRST_NAME
,    submitter_last_name AS SUBMITTER_LAST_NAME
,    submitter_control_number AS SUBMITTER_ID
,    program_name AS PROGRAM_NAME
,    CONVERT(NVARCHAR(10), amount) AS AWARD_AMOUNT
,    award_type AS AWARD_TYPE
,    recognition_budget AS BUDGET_USED
,    ecard_id AS ECARD_ID
,    nomination_comments AS COMMENT
,    criteria AS CRITERIA
,    nomination_id AS TRACKING_NUMBER
,    recognition_status AS STATUS
,    approver_name AS NEXT_APPROVER
,    last_approval_date AS FINAL_APPROVAL_DATE
,    denial_comment AS DENIAL_REASON
,    receiver_groups AS RECIPIENT_GROUPS
,    receiver_manager_control_num AS RECIPIENT_MANAGER_ID
,    receiver_manager_name AS RECIPIENT_MANAGER
,    submitter_groups AS SUBMITTER_GROUPS
,    submitter_manager_control_num AS SUBMITTER_MANAGER_ID
,    submitter_manager_name AS SUBMITTER_MANAGER
,    recognition_id AS RECOGNITION_ID
FROM    #recognition 
ORDER BY
    nomination_submittal_date DESC

END

GO

