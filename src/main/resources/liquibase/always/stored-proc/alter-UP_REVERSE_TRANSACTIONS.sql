/****** Object:  StoredProcedure [component].[UP_REVERSE_TRANSACTIONS]    Script Date: 1/15/2018 8:04:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_REVERSE_TRANSACTIONS
-- ===========================  DESCRIPTION  ==================================
--
-- REVERSES ALL ITEMS ASSOCIATED WITH THE RECOGNITION TRANSACTIONS PASSED IN LIST OF IDS
-- THE TRANSACTION IS NOT MODIFIED.
--
-- WILL UPDATE STATUS TO "REVERSED" FOR:
-- NOMINATION, only if all RECOGNITION records are REVERSED
-- RECOGNITION, TRANSACTION_HEADER, COMMENT
-- ALERT (if it is a manager or approval alert, only update status if all direct reports / approvals have been reversed)
-- NEWSFEED_ITEM, only if the entire NOMINATION record is REVERSED
-- APPROVAL_HISTORY - Creates a record saving who performed the reversal and the reversal comments
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20170328    MP-8044        CREATED    
-- HARWELLM        20170427    MP-9907        SUPPRESS NOTIFY_OTHER ALERTS
-- HARWELLM        20170512    MP-10225    FIX RECOGNITION_APPROVED AND RECOGNITION_PENDING_APPROVAL ALERTS
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- KNIGHTGA     20180115    MP-11368    IMPROVEMENTS TO RECOGNITION REVERSAL BASE ON CORE UPGRADE
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_REVERSE_TRANSACTIONS]
@delim_transaction_id_list NVARCHAR(MAX),
@reversalComment NVARCHAR(MAX),
@paxId BIGINT

AS
BEGIN
SET NOCOUNT ON;

DECLARE    @delimiter CHAR(1)
SET    @delimiter = ',';

--Reverse all RECOGNITION records
UPDATE recg SET STATUS_TYPE_CODE = 'REVERSED', COMMENT = @reversalComment
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID 
    AND recg.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'

--Reverse any NOMINATION(s) for which all RECOGNITION(s) have been reversed
UPDATE n SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID
LEFT JOIN component.RECOGNITION recg_all ON recg_all.NOMINATION_ID = n.ID 
    AND recg_all.STATUS_TYPE_CODE <> 'REVERSED'
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND recg_all.NOMINATION_ID IS NULL --Exclude nominations that have some other statuses tied to them still

--Create an APPROVAL_HISTORY record so we know who performed this batch of reversals
INSERT INTO component.APPROVAL_HISTORY (PAX_ID, TARGET_ID, TARGET_TABLE, APPROVAL_DATE, APPROVAL_NOTES, STATUS_TYPE_CODE)
SELECT @paxId,
CASE WHEN th.SUB_TYPE_CODE = 'DISC' THEN th.ID ELSE disc.TARGET_ID END AS 'TARGET_ID', 
CASE WHEN th.SUB_TYPE_CODE = 'DISC' THEN 'TRANSACTION_HEADER' ELSE 'RECOGNITION' END AS 'TARGET_TABLE',
GETDATE(), @reversalComment, 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.TRANSACTION_HEADER th ON th.ID = disc.TRANSACTION_HEADER_ID
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))

--Reverse all ALERT records tied to the NOMINATION_ID (recipient level)
--These are simple because there is a single NOMINATION/RECOGNITION set for each raise and award code. Multiple RECOGNITION records need more complex checking.
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID 
    AND n.STATUS_TYPE_CODE = 'REVERSED'
JOIN component.ALERT a ON a.TARGET_ID = n.ID
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'
AND a.ALERT_SUB_TYPE_CODE IN ('RAISE_RECEIVER', 'MANAGER_RAISE_RECEIVER', 'MANAGER_AWARD_CODE', 'NOTIFY_OTHER')

--Reverse all ALERT records tied to the NOMINATION_ID (your recognition has been approved)
--For these we have to make sure that every recognition tied to the alert is REVERSED. Otherwise the alert must remain as-is
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.ALERT a
LEFT JOIN (
    SELECT DISTINCT a.ID, COUNT(DISTINCT ah_appr.ID) AS 'APPROVED_COUNT', COUNT(DISTINCT ah_rev.ID) AS 'REVERSED_COUNT'
    FROM component.DISCRETIONARY disc
    JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
    JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID
    LEFT JOIN component.RECOGNITION recg_all ON recg_all.NOMINATION_ID = n.ID
    JOIN component.ALERT a ON a.TARGET_ID = n.ID
    JOIN component.ALERT_MISC am ON am.ALERT_ID = a.ID AND am.VF_NAME = 'APPROVAL_PAX'
    JOIN component.APPROVAL_HISTORY ah_appr ON ah_appr.PAX_ID = am.MISC_DATA 
        AND ah_appr.TARGET_ID = recg_all.ID AND ah_appr.TARGET_TABLE = 'RECOGNITION' 
        AND ah_appr.STATUS_TYPE_CODE = 'APPROVED' --All of the approvals tied to the alert
    LEFT JOIN component.APPROVAL_HISTORY ah_rev ON ah_rev.TARGET_ID = ah_appr.TARGET_ID AND ah_rev.TARGET_TABLE = ah_appr.TARGET_TABLE 
        AND ah_rev.STATUS_TYPE_CODE = 'REVERSED' --All of the reversals tied to the alert
    WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
    AND a.ALERT_SUB_TYPE_CODE IN ('RECOGNITION_APPROVED')
    GROUP BY a.ID
) TOTALS ON TOTALS.ID = a.ID
WHERE TOTALS.APPROVED_COUNT = TOTALS.REVERSED_COUNT

--Reverse all ALERT records tied to the NOMINATION_ID (approval required)
--For these we have to make sure that every recognition tied to the alert is REVERSED. Otherwise the alert must remain as-is
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.ALERT a
LEFT JOIN (
    SELECT DISTINCT a.ID, COUNT(DISTINCT ah_appr.ID) AS 'APPROVED_COUNT', COUNT(DISTINCT ah_rev.ID) AS 'REVERSED_COUNT'
    FROM component.DISCRETIONARY disc
    JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
    JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID
    JOIN component.ALERT a ON a.TARGET_ID = n.ID
    LEFT JOIN component.APPROVAL_PENDING ap ON ap.TARGET_ID = recg.ID AND ap.TARGET_TABLE = 'RECOGNITION'
    JOIN component.RECOGNITION recg_all ON recg_all.NOMINATION_ID = n.ID
    JOIN component.APPROVAL_HISTORY ah_appr ON ah_appr.TARGET_ID = recg_all.ID AND ah_appr.TARGET_TABLE = 'RECOGNITION' 
        AND ah_appr.PAX_ID = a.PAX_ID AND ah_appr.STATUS_TYPE_CODE = 'APPROVED' --All of the approvals I performed
    LEFT JOIN component.APPROVAL_HISTORY ah_rev ON ah_rev.TARGET_ID = ah_appr.TARGET_ID AND ah_rev.TARGET_TABLE = ah_appr.TARGET_TABLE 
        AND ah_rev.STATUS_TYPE_CODE = 'REVERSED' --The recognitions that I approved and have since been reversed
    WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
    AND a.ALERT_SUB_TYPE_CODE IN ('RECOGNITION_APPROVAL')
    AND ap.ID IS NULL --If there are still approvals pending, do nothing
    GROUP BY a.ID
) TOTALS ON TOTALS.ID = a.ID
WHERE TOTALS.APPROVED_COUNT = TOTALS.REVERSED_COUNT

--Reverse all ALERT records tied to the NOMINATION_ID (manager level)
--For these we have to make sure the manager doesn't have any other direct reports tied to the ALERT that were not reversed
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = recg.RECEIVER_PAX_ID 
    AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
JOIN component.ALERT a ON a.TARGET_ID = recg.NOMINATION_ID AND a.PAX_ID = rl.PAX_ID_2
LEFT JOIN ( 
    SELECT DISTINCT a.ID
    FROM component.DISCRETIONARY disc
    JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
    JOIN component.RECOGNITION recg_appr ON recg_appr.NOMINATION_ID = recg.NOMINATION_ID 
        AND recg_appr.STATUS_TYPE_CODE <> 'REVERSED'
    JOIN component.RELATIONSHIP rl ON rl.PAX_ID_1 = recg_appr.RECEIVER_PAX_ID 
        AND rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
    JOIN component.ALERT a ON a.TARGET_ID = recg_appr.NOMINATION_ID 
        AND a.PAX_ID = rl.PAX_ID_2 
        AND a.ALERT_SUB_TYPE_CODE = 'MANAGER_RECOGNITION'
    WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
        AND disc.TARGET_TABLE = 'RECOGNITION'
) al_exclude ON a.ID = al_exclude.ID
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'
AND a.ALERT_SUB_TYPE_CODE IN ('MANAGER_RECOGNITION')
AND al_exclude.ID IS NULL

--Reverse all ALERT records tied to the RECOGNITION_ID
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED' 
FROM component.DISCRETIONARY disc
JOIN component.ALERT a ON a.TARGET_ID = disc.TARGET_ID 
    AND a.ALERT_SUB_TYPE_CODE = 'RECOGNITION'
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))

--Reverse all ALERT records tied to the TRANSACTION_HEADER_ID
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.ALERT a
WHERE a.TARGET_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND a.ALERT_SUB_TYPE_CODE = 'POINT_LOAD'

--Reverse all ALERT records tied to the NEWSFEED_ITEM_ID (submitter's alerts)
--These should only be REVERSED if the NOMINATION record is REVERSED
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID AND n.STATUS_TYPE_CODE = 'REVERSED'
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND NI.TARGET_TABLE = 'NOMINATION'
JOIN component.ALERT a ON a.TARGET_ID = ni.ID 
AND a.ALERT_SUB_TYPE_CODE IN ('LIKE_RECOGNITION_SUBMITTER', 'COMMENT_RECOGNITION_SUBMITTER')
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'

--Reverse all ALERT records tied to the NEWSFEED_ITEM_ID (recipients's alerts)
--Only reverse alerts tied to the pax_id who was reversed
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = recg.NOMINATION_ID AND NI.TARGET_TABLE = 'NOMINATION'
JOIN component.ALERT a ON a.TARGET_ID = ni.ID 
    AND a.PAX_ID = recg.RECEIVER_PAX_ID
    AND a.ALERT_SUB_TYPE_CODE IN ('LIKE_RECOGNITION_RECEIVER', 'COMMENT_RECOGNITION_RECEIVER')
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'

--Reverse all ALERT records tied to the NEWSFEED_ITEM_ID (approval required)
--For these we have to make sure there are no recognitions tied to the alert that were not reversed
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND NI.TARGET_TABLE = 'NOMINATION'
JOIN component.ALERT a ON a.TARGET_ID = ni.ID 
    AND a.ALERT_SUB_TYPE_CODE = 'RECOGNITION_PENDING_APPROVAL'
LEFT JOIN component.RECOGNITION recg_all ON recg_all.NOMINATION_ID = n.ID 
    AND recg_all.STATUS_TYPE_CODE <> 'REVERSED'
WHERE disc.TRANSACTION_HEADER_ID in (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'
AND recg_all.NOMINATION_ID IS NULL --Exclude nominations that have non-reversed recognitions tied to them still

--Reverse all ALERT records tied to the COMMENT_ID
UPDATE a SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = recg.NOMINATION_ID AND NI.TARGET_TABLE = 'NOMINATION'
JOIN component.COMMENT c ON c.TARGET_ID = ni.ID 
    AND c.TARGET_TABLE = 'NEWSFEED_ITEM'
JOIN component.ALERT a ON a.TARGET_ID = c.ID 
    AND a.ALERT_SUB_TYPE_CODE = 'REPORT_COMMENT_ABUSE'
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'

--Reverse all COMMENT records (only if the Nomination is REVERSED)
UPDATE c SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NOMINATION n ON n.ID = recg.NOMINATION_ID 
    AND n.STATUS_TYPE_CODE = 'REVERSED'
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = recg.NOMINATION_ID AND NI.TARGET_TABLE = 'NOMINATION'
JOIN component.COMMENT c ON c.TARGET_ID = ni.ID 
    AND c.TARGET_TABLE = 'NEWSFEED_ITEM'
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'

--Only update NEWSFEED_ITEM status if every recognition on the nomination was REVERSED
UPDATE ni SET STATUS_TYPE_CODE = 'REVERSED'
FROM component.DISCRETIONARY disc
JOIN component.RECOGNITION recg ON recg.ID = disc.TARGET_ID
JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = recg.NOMINATION_ID AND NI.TARGET_TABLE = 'NOMINATION'
LEFT JOIN component.RECOGNITION recg_all ON recg_all.NOMINATION_ID = recg.NOMINATION_ID 
    AND recg_all.STATUS_TYPE_CODE <> 'REVERSED'
WHERE disc.TRANSACTION_HEADER_ID IN (SELECT * FROM component.UF_LIST_TO_TABLE_ID(@delim_transaction_id_list, @delimiter))
AND disc.TARGET_TABLE = 'RECOGNITION'
AND recg_all.ID IS NULL --Exclude NEWSFEED_ITEM records that have non-REVERSED recognitions tied to them still

END