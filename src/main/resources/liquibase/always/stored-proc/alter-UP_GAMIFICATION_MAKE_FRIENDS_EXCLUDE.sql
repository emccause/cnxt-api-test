/****** Object:  StoredProcedure [component].[UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE]    Script Date: 10/15/2015 09:42:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, RETURN A LIST OF PAX RECORDS TO BE EXCLUDED FROM THE
-- "MAKE FRIENDS" SCREEN
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- BESSBS        20110221                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE]
    @pax_group_id BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT
    PAX.PAX_ID
,    PAX.CONTROL_NUM
FROM    PAX 
JOIN    PAX_GROUP 
    ON PAX.PAX_ID = PAX_GROUP.PAX_ID
WHERE    PAX_GROUP.PAX_GROUP_ID NOT IN (SELECT [id] FROM UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id, 1, NULL))
AND    PAX_GROUP.ORGANIZATION_CODE <> 'RECG'

END

GO

