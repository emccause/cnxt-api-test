SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[component].[UP_CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [component].[UP_CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER]
END
GO
-- ==============================  NAME  ======================================
-- CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER
-- ===========================  DESCRIPTION  ==================================
--
-- INSERTS INTO TRANSACTION MISCELANIOUS HEADER
-- PARAMETERS:
--   @NOMINATION_ID - The ID of the nomination to send emails for
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY      CHANGE DESCRIPTION
-- SHARPCA      20201111    CNXT-325    CREATED
-- burgestl		20201122				Fix did not match class taken from
--                                      culturenext.transaction.dao.impl.TransactionHeaderMiscDaoImpl.java
--
-- ===========================  DECLARATIONS  =================================

CREATE PROCEDURE component.UP_CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER --32
AS
BEGIN
    INSERT INTO component.TRANSACTION_HEADER_MISC (TRANSACTION_HEADER_ID, VF_NAME, MISC_DATA, MISC_DATE)
	SELECT th.ID, 'BUDGET_OVERRIDE_SUB_PROJECT_NUMBER', sp.SUB_PROJECT_NUMBER, GETDATE()
	FROM component.TRANSACTION_HEADER th
	LEFT JOIN component.TRANSACTION_HEADER_EARNINGS the ON the.TRANSACTION_HEADER_ID = th.ID
	LEFT JOIN component.TRANSACTION_HEADER_MISC thm ON thm.TRANSACTION_HEADER_ID = th.ID AND thm.VF_NAME = 'BUDGET_OVERRIDE_SUB_PROJECT_NUMBER'
	JOIN component.DISCRETIONARY d ON d.TRANSACTION_HEADER_ID = th.ID
	JOIN component.BUDGET_MISC bm ON bm.BUDGET_ID = d.BUDGET_ID_DEBIT AND bm.MISC_TYPE_CODE = 'SUB_PROJECT_OVERRIDE' AND bm.VF_NAME = 'US_OVERRIDE'
	JOIN component.RECOGNITION r ON r.ID = d.TARGET_ID AND d.TARGET_TABLE = 'RECOGNITION'
	JOIN component.ADDRESS a ON a.PAX_ID = r.RECEIVER_PAX_ID AND a.PREFERRED = 'Y' AND a.COUNTRY_CODE IN ('US', 'PR')
	JOIN component.SUB_PROJECT sp ON sp.ID = CAST(bm.MISC_DATA AS BIGINT)
	WHERE th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE IN ('RECG', 'DISC')
	AND thm.MISC_DATA IS NULL
	AND the.EARNINGS_ID IS NULL

END
GO
