/****** Object:  StoredProcedure [UP_HIERARCHY_DENORMALIZE]    Script Date: 10/15/2015 10:26:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_HIERARCHY_DENORMALIZE
-- ===========================  DESCRIPTION  ==================================
-- DENORMALIZES' THE PAX_GROUP TABLE FROM A CONCENTRIX DATABASE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20040810                CREATED
-- DOHOGNTA        20150306                REMOVE REFERENCES TO DEPRECATED SYSTEM OBJECTS
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_DENORMALIZE]
    @verbose int = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

-- LOGGING TAG
DECLARE @prc varchar(50)
DECLARE @msg varchar(1000)
DECLARE @err int
DECLARE @sql varchar(4000)
DECLARE @beg_time datetime2
DECLARE @end_time datetime2
DECLARE @rowcount int
DECLARE @rowcount_sum int
DECLARE @user_role varchar(50)

-- OBJECT OWNER
DECLARE @tab_own varchar(10)

-- ORG_GROUP (W/ LEVELS)
DECLARE @tab_org varchar(50)
DECLARE @tab_org_work varchar(50)
DECLARE @tab_org_temp varchar(50)

-- HIERARCHY (DENORMALIZED)
DECLARE @tab_hie varchar(50)
DECLARE @tab_hie_work varchar(50)
DECLARE @tab_hie_temp varchar(50)

CREATE TABLE #HIERARCHY_STATISTICS (
    DATA INT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-----------------------------------------------------------
-- INITIALIZATION
-----------------------------------------------------------

SET    @prc = 'UP_HIERARCHY_DENORMALIZE'
SET    @rowcount = 0
SET    @rowcount_sum = 0
SET    @user_role = 'APP_WebAdmin'
SET    @tab_own = 'component.'
SET    @tab_hie = 'HIERARCHY'
SET    @tab_hie_work = @tab_hie + '_WORK'
SET    @tab_hie_temp = @tab_hie + '_TEMP'
SET    @tab_org = 'HIERARCHY_ORG_GROUP'
SET    @tab_org_work = @tab_org + '_WORK'
SET    @tab_org_temp = @tab_org + '_TEMP'

-----------------------------------------------------------
-- CREATE WORKING VERSION OF ORG_GROUP (HIERARCHY DEFINITION)
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE WORKING VERSION OF ORG_GROUP (HIERARCHY DEFINITION)'
    PRINT    @msg
END

-- DROP TABLE IF IT EXISTS
EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org_work + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_org_work + '')

EXEC ('
CREATE TABLE ' + @tab_own + @tab_org_work + ' (
    [ORG_GROUP_ID] [int] NULL ,
    [OWNER_GROUP_ID] [int] NULL ,
    [ORGANIZATION_CODE] [nvarchar] (4) NULL ,
    [ROLE_CODE] [nvarchar] (4) NULL ,
    [LEVEL] [int] NULL
) ON [PRIMARY]'
)

SET @sql =
'INSERT INTO ' + @tab_own + @tab_org_work + ' (
    ORG_GROUP_ID
,    OWNER_GROUP_ID
,    ORGANIZATION_CODE
,    ROLE_CODE
,    [LEVEL]
) SELECT
    ORG_GROUP_ID
,    OWNER_GROUP_ID
,    ORGANIZATION_CODE
,    ROLE_CODE
,    NULL [LEVEL]
FROM    ' + @tab_own + 'ORG_GROUP
ORDER BY
    ORG_GROUP_ID'

-- CREATE WORKING VERSION OF ORG_GROUP TABLE AND LOAD WITH DATA
EXEC(@sql)

SELECT    @rowcount = @@ROWCOUNT
,    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ' + @tab_org_work + ' COUNT = [' + CONVERT(VARCHAR, @rowcount) + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- ASSIGN LEVELS
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ASSIGN LEVELS'
    PRINT    @msg
END

DECLARE    @i int
SET    @i = 1

-- ASSIGN LEVEL 1
SET @sql =
'UPDATE    ' + @tab_own + @tab_org_work + '
SET    [LEVEL] = ' + CONVERT(VARCHAR, @I) + '
WHERE    OWNER_GROUP_ID IS NULL'

EXEC(@sql)

SELECT    @rowcount = @@ROWCOUNT
,    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

-- ASSIGN LEVELS UNTIL THERE ARE NO MORE (BOTTOM)
WHILE    @rowcount > 0
BEGIN
    SET    @rowcount_sum = @rowcount_sum + @rowcount

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': ROW COUNT LEVEL' + CONVERT(VARCHAR, @i) + ' = [' + CONVERT(VARCHAR, @rowcount) + ']'
        PRINT    @msg
    END

    SET @sql =
    'UPDATE    T2
    SET    [LEVEL] = ' + CONVERT(VARCHAR, @i + 1) + '
    FROM    ' + @tab_own + @tab_org_work + ' T1
    ,    ' + @tab_own + @tab_org_work + ' T2
    WHERE    T1.ORG_GROUP_ID = T2.OWNER_GROUP_ID
    AND    T1.[LEVEL] = ' + CONVERT(VARCHAR, @i)
    
    -- UPDATE WORKING VERSION OF ORG_GROUP TABLE; ASSIGN LEVEL VALUE TO RECORD(S)
    EXEC(@sql)
    
    SELECT    @rowcount = @@ROWCOUNT
    ,    @err = @@ERROR
    
    IF @err <> 0 RETURN -1 -- EXITING HERE!

    SET @i = @i + 1
END

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ROW COUNT TOTAL  = [' + CONVERT(VARCHAR, @rowcount_sum) + ']'
    PRINT    @msg
    SET    @msg = @prc + ': MAX DEPTH OF ALL HIERARCIES = [' + CONVERT(VARCHAR, @i - 1) + ']'
    PRINT    @msg
    PRINT    ''
END

DECLARE @max int
DELETE    #HIERARCHY_STATISTICS
EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT ISNULL(MAX([LEVEL]), 0) FROM ' + @tab_own + @tab_org_work )
SELECT    @max = DATA FROM #HIERARCHY_STATISTICS

-----------------------------------------------------------
-- CREATE TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE TABLE'
    PRINT    @msg
END

-- DROP TABLE IF IT ALREADY EXISTS
EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie_work + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_hie_work + '')

-- BUILD CREATE TABLE STATEMENT
DECLARE @j int
DECLARE @cre varchar(900)
DECLARE @col varchar(800)

SET    @cre = 'CREATE TABLE ' + @tab_own + @tab_hie_work + ' (ROW_ID INT IDENTITY NOT NULL, PAX_ID INT NOT NULL'
SET    @col = ''

SET    @j = @max
WHILE    @j > 0
BEGIN
    -- THE TABLE WIDTH SHOULD CORESPOND TO THE HIERARCHY DEPTH
    SET @col = ', ROLE' + CONVERT(VARCHAR, @j) + ' VARCHAR(4) NULL' + ', LEVEL' + CONVERT(VARCHAR, @j) + ' INT NULL' + @col
    SET @j = @j - 1
END

SET    @cre = @cre + @col + ')'

-- EXECUTE CREATE TABLE STATEMENT
EXEC    (@cre)

SET    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE STATEMENT = [' + @cre + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- POPULATE TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': POPULATE TABLE'
    PRINT    @msg
    SELECT    @beg_time = GETDATE()
    SET    @msg = @prc + ': INSERT STATEMENT; BEG_TIME = [' + CONVERT(VARCHAR, @beg_time) + ']'
    PRINT    @msg
END

DECLARE @k int
DECLARE @l int

DECLARE @ins varchar(500)
DECLARE @sel varchar(500)
DECLARE @fro varchar(500)
DECLARE @whe varchar(1000)
DECLARE @ord varchar(500)

-- INITIALIZE COUNTER
SET    @l = 1
WHILE    @l <= @max
BEGIN
    SET    @sql = ''
    SET    @ins = 'INSERT INTO ' + @tab_own + @tab_hie_work + ' ('
    SET    @sel = ' SELECT DISTINCT'
    SET    @fro = ' FROM'
    SET    @whe = ' WHERE'
    SET    @ord = ' ORDER BY'

    -- INITIALIZE COUNTER
    SET    @k = 1
    WHILE    @k <= @l
    BEGIN
        IF @k = 1
        BEGIN -- FIRST LINE
            SET    @ins = @ins + 'LEVEL' + CONVERT(VARCHAR, @k) + ', ROLE' + CONVERT(VARCHAR, @k)
            SET    @sel = @sel + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID' + ', PG_L' + CONVERT(VARCHAR, @k) + '.ROLE_CODE'
            SET    @fro = @fro + ' ' + @tab_own + 'PAX_GROUP PG_L' + CONVERT(VARCHAR, @k)
            IF @l > 1
            BEGIN
                SET    @whe = @whe + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID = PG_L' + CONVERT(VARCHAR, @k + 1) + '.OWNER_GROUP_ID'
            END
            SET    @ord = @ord + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID'
        END
        ELSE  -- OTHER LINES
        BEGIN
            SET    @ins = @ins + ', LEVEL' + CONVERT(VARCHAR, @k) + ', ROLE' + CONVERT(VARCHAR, @k)
            SET    @sel = @sel + ', PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID' + ', PG_L' + CONVERT(VARCHAR, @k) + '.ROLE_CODE'
            SET    @fro = @fro + ', ' + @tab_own + 'PAX_GROUP PG_L' + CONVERT(VARCHAR, @k)
            IF @k < @l
            BEGIN
                SET    @whe = @whe + ' AND PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID = PG_L' + CONVERT(VARCHAR, @k + 1) + '.OWNER_GROUP_ID'
            END
            SET    @ord = @ord + ', PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID'
        END
        
        -- INCREMENT COUNT
        SET    @k = @k + 1
    END -- WHILE
    
    SET    @ins = @ins + ', PAX_ID)'
    SET    @sel = @sel + ', PG_L' + CONVERT(VARCHAR, @k - 1) + '.PAX_ID' 
    IF @l > 1
        SET    @whe = @whe + ' AND PG_L1.OWNER_GROUP_ID IS NULL'
    ELSE
        SET    @whe = @whe + ' PG_L1.OWNER_GROUP_ID IS NULL'
    SET    @sql = @ins + @sel + @fro + @whe + @ord

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': INSERT STATEMENT = [' + @sql + ']'
        PRINT    @msg
    END

    -- INSERT DENORMALIZED HIERARCHY RECORD(S)
    EXEC    (@sql)

    SELECT    @rowcount = @@ROWCOUNT
    ,    @err = @@ERROR

    IF @err <> 0 RETURN -1 -- EXITING HERE!

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': ROW COUNT LEVEL' + CONVERT(VARCHAR, @l) + ' = [' + CONVERT(VARCHAR, @rowcount) + ']'
        PRINT    @msg
    END

    -- INCREMENT COUNT
    SET    @l = @l + 1
END -- WHILE

IF @verbose = 1
BEGIN
    SELECT    @end_time = GETDATE()
    SET    @msg = @prc + ': INSERT STATEMENT; END_TIME = [' + CONVERT(VARCHAR, @end_time) + ']'
    PRINT    @msg
    SET    @msg = @prc + ': INSERT STATEMENT; EXE_TIME = [' + CONVERT(VARCHAR, DATEDIFF(ms, @beg_time, @end_time)) + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- DISPLAY TABLE STATISTICS
-----------------------------------------------------------

SELECT    @rowcount = COUNT(1) FROM PAX_GROUP

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': DISPLAY STATISTICS'
    PRINT    @msg
     SET    @msg = @prc + ': ROW COUNT = [' + CONVERT(VARCHAR, @rowcount) + '] (PAX_GROUP)'
    PRINT    @msg
    DELETE    #HIERARCHY_STATISTICS
    EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT COUNT(1) FROM ' + @tab_own + @tab_hie_work )
    SELECT    @rowcount = DATA FROM #HIERARCHY_STATISTICS
     SET    @msg = @prc + ': ROW COUNT = [' + CONVERT(VARCHAR, @rowcount) + '] (' + @tab_hie_work + ')'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- SWAP TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': SWAP TABLE'
    PRINT    @msg
END

DELETE    #HIERARCHY_STATISTICS
EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT COUNT(1) FROM ' + @tab_own + @tab_hie_work)
SELECT    @rowcount = DATA FROM #HIERARCHY_STATISTICS
-- SEE IF THERE ARE ANY RECORDS IN THE SOURCE TABLE
IF (@rowcount) > 0
BEGIN
    -- APPLY PERMISSIONS
--    EXEC    ('GRANT  SELECT  ON ' + @tab_own + @tab_org_work + '  TO [' + @user_role + ']')
--    EXEC    ('GRANT  SELECT  ON ' + @tab_own + @tab_hie_work + '  TO [' + @user_role + ']')

    -- SWAP TABLES
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) EXEC SP_RENAME ''' + @tab_own + @tab_org + ''', ''' + @tab_org_temp + '''')
    EXEC    ('SP_RENAME ''' + @tab_own + @tab_org_work + ''', ''' + @tab_org + '''')

    -- DROP TABLE IF IT EXISTS
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org_temp + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_org_temp + '')

    -- SWAP TABLES
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) EXEC SP_RENAME ''' + @tab_own + @tab_hie + ''', ''' + @tab_hie_temp + '''')
    EXEC    ('SP_RENAME ''' + @tab_own + @tab_hie_work + ''', ''' + @tab_hie + '''')

    -- DROP TABLE IF IT EXISTS
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie_temp + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_hie_temp + '')

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': SWAP TABLE = [COMPLETE]'
        PRINT    @msg
        PRINT    ''
    END
END -- IF
ELSE
BEGIN
    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': SWAP TABLE = [INCOMPLETE]'
        PRINT    @msg
        PRINT    ''
    END
END

END

GO

