/****** Object:  StoredProcedure [component].[UP_EARNINGS_BY_CHILD]    Script Date: 10/15/2015 09:41:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_EARNINGS_BY_CHILD
-- ===========================  DESCRIPTION  ==================================
-- TODO: DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- VANTEDS        20120214                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_EARNINGS_BY_CHILD]
    @pax_group_id BIGINT
,    @from_date DATETIME2
,    @thru_date DATETIME2
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @results TABLE (
        pax_group_id BIGINT
    ,    earnings_amount INT
    ,    grandchild_count INT
    ) 
    
    DECLARE @results_final TABLE (
        pax_group_id BIGINT
    ,    earnings_amount INT
    ,    grandchild_count INT
    ,    company_name NVARCHAR(255)
    ,    first_name NVARCHAR(30)
    ,    last_name NVARCHAR(40)
    )
    
    DECLARE @child_pax_group_id BIGINT
    
    DECLARE c_list CURSOR FOR SELECT PAX_GROUP_ID FROM PAX_GROUP WHERE OWNER_GROUP_ID = @pax_group_id
    OPEN c_list
    
    FETCH NEXT FROM c_list INTO @child_pax_group_id
    
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        INSERT INTO @results (pax_group_id, earnings_amount)
        SELECT
            @child_pax_group_id
        ,    SUM(P.PAYOUT_AMOUNT)
        FROM
            UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@child_pax_group_id, 1, NULL)
        JOIN PAX_GROUP PG
            ON [ID] = PG.PAX_GROUP_ID
        JOIN TRANSACTION_HEADER TH
            ON PG.PAX_GROUP_ID = TH.PAX_GROUP_ID
            AND TH.ACTIVITY_DATE >= CONVERT(DATETIME2, @from_date, 102)
            AND TH.ACTIVITY_DATE < CONVERT(DATETIME2, @thru_date, 102)
        JOIN TRANSACTION_HEADER_EARNINGS THE
            ON THE.TRANSACTION_HEADER_ID = TH.[ID]
        JOIN EARNINGS_PAYOUT EP
            ON EP.EARNINGS_ID = THE.EARNINGS_ID
        JOIN PAYOUT P
            ON P.PAYOUT_ID = EP.PAYOUT_ID
            AND P.STATUS_TYPE_CODE = 'ISSUED'
    
        UPDATE @results set grandchild_count = (
            SELECT
                COUNT(OG2.ORG_GROUP_ID)
            FROM
                UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@child_pax_group_id,1,1) fn
            ,    ORG_GROUP OG
            ,    ORG_GROUP OG2
            ,    PAX_GROUP PG
            WHERE PG.PAX_GROUP_ID = FN.[ID]
            AND FN.LEVEL <> 0
            AND PG.ORGANIZATION_CODE = OG.ORGANIZATION_CODE
            AND PG.ROLE_CODE = OG.ROLE_CODE
            AND OG.ORG_GROUP_ID = OG2.OWNER_GROUP_ID)
        WHERE PAX_GROUP_ID = @CHILD_PAX_GROUP_ID

        FETCH NEXT FROM c_list INTO @child_pax_group_id
    END
    
    CLOSE c_list
    DEALLOCATE c_list
    
    INSERT INTO @results_final
    SELECT     r.pax_group_id
    ,    r.earnings_amount
    ,    r.grandchild_count
    ,    P.COMPANY_NAME
    ,    P.FIRST_NAME
    ,    P.LAST_NAME
    FROM
        @results r
    JOIN PAX_GROUP PG
        ON r.pax_group_id = PG.PAX_GROUP_ID
    JOIN PAX P
        ON PG.PAX_ID = P.PAX_ID
    WHERE r.earnings_amount IS NOT NULL
        
    IF (SELECT COUNT(*) FROM @results_final) > 0
    BEGIN
         SELECT * FROM @results_final
    END
    ELSE
    BEGIN
         SELECT 'No Data' as label, 0 as value
    END
END

GO

