SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_CALCULATE_SUB_PROJECT_OVERRIDE
-- ===========================  DESCRIPTION  ==================================
--
-- INSERTS ANY MISSING TRANSACTION_HEADER_MISC RECORDS FOR A NOMINATION
-- BASED ON THE CONFIGURATION SET UP IN APPLICATION_DATA (vendorprocessing.subproject.override)
-- VF_NAME = 'CONFIGURABLE_SUB_PROJECT_NUMBER'
--
-- The configuration can be set up to use either recipient or submitter values
-- and can pull data from any of the GROUP_CONFIG data
-- Examples:
-- COST_CENTER/SUBMITTER
-- AREA/RECIPIENT
-- DEPARTMENT/SUBMITTER
-- CITY/RECIPIENT
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20180309    MP-11775    CREATED
-- HARWELLM        20180319    MP-11837    EXCLUDE RECORDS THAT HAVE ALREADY BEEN PROCESSED
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_CALCULATE_SUB_PROJECT_OVERRIDE]

AS
BEGIN
SET NOCOUNT ON;

CREATE TABLE #miscData (
    TRANSACTION_HEADER_ID BIGINT,
    MISC_DATA NVARCHAR(MAX),
    MISC_DATE DATETIME,
    RECEIVER_PAX BIGINT,
    SUBMITTER_PAX BIGINT
);
INSERT INTO #miscData
SELECT DISTINCT th.ID, NULL, GETDATE(), r.RECEIVER_PAX_ID, n.SUBMITTER_PAX_ID
FROM component.TRANSACTION_HEADER th
LEFT JOIN component.TRANSACTION_HEADER_EARNINGS the ON the.TRANSACTION_HEADER_ID = th.ID
LEFT JOIN component.TRANSACTION_HEADER_MISC thm ON thm.TRANSACTION_HEADER_ID = th.ID AND thm.VF_NAME = 'CONFIGURABLE_SUB_PROJECT_NUMBER'
JOIN component.DISCRETIONARY d ON d.TRANSACTION_HEADER_ID = th.ID
JOIN component.RECOGNITION r ON r.ID = d.TARGET_ID AND d.TARGET_TABLE = 'RECOGNITION'
JOIN component.NOMINATION n ON n.ID = r.NOMINATION_ID
WHERE th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE IN ('RECG', 'DISC')
AND thm.MISC_DATA IS NULL 
AND the.EARNINGS_ID IS NULL --Exclude transactions that have already been processed


--Find out what GROUP_CONFIG field they want to override the subproject number with and get that data
CREATE TABLE #overrideData (
    GROUP_CONFIG_NAME NVARCHAR(100) NULL,
    CONFIG NVARCHAR(MAX) NULL,
    DATA_TYPE NVARCHAR(9) NULL
);

WITH QUERY(VALUE_1, VALUE_2) AS (
    SELECT
        LEFT(VALUE, Charindex('/', VALUE) - 1),
        RIGHT(VALUE, Charindex('/', Reverse(VALUE)) - 1)
    from component.APPLICATION_DATA
    where KEY_NAME = 'vendorprocessing.subproject.override'
) 
INSERT INTO #overrideData SELECT
gc.GROUP_CONFIG_NAME, gc.CONFIG, QUERY.VALUE_2 AS VALUE
FROM QUERY
JOIN component.GROUP_CONFIG gc
ON QUERY.VALUE_1 = gc.GROUP_CONFIG_NAME
AND gc. GROUP_CONFIG_TYPE_CODE = 'ENRL_FIELD'

IF (SELECT TOP 1 CONFIG FROM #overrideData) LIKE 'PAX_GROUP%'
BEGIN
    IF (SELECT TOP 1 DATA_TYPE FROM #overrideData) = 'SUBMITTER'
    BEGIN
        --Submitter's PAX_GROUP record
        UPDATE misc SET MISC_DATA = SUBSTRING(pgm.MISC_DATA, 1, 8)
        FROM #miscData misc
        JOIN component.PAX_GROUP pg ON pg.PAX_ID = misc.SUBMITTER_PAX
        LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID
        WHERE pgm.MISC_DATA IS NOT NULL
        AND pgm.VF_NAME = (SELECT TOP 1 GROUP_CONFIG_NAME FROM #overrideData)
    END
    ELSE IF (SELECT TOP 1 DATA_TYPE FROM #overrideData) = 'RECIPIENT'
    BEGIN
        --Recipient's PAX_GROUP record
        UPDATE misc SET MISC_DATA = SUBSTRING(pgm.MISC_DATA, 1, 8)
        FROM #miscData misc
        JOIN component.PAX_GROUP pg ON pg.PAX_ID = misc.RECEIVER_PAX
        LEFT JOIN component.PAX_GROUP_MISC pgm ON pgm.PAX_GROUP_ID = pg.PAX_GROUP_ID
        WHERE pgm.MISC_DATA IS NOT NULL
        AND pgm.VF_NAME = (SELECT TOP 1 GROUP_CONFIG_NAME FROM #overrideData)
    END
END
ELSE IF (SELECT TOP 1 CONFIG FROM #overrideData) LIKE 'ADDRESS%'
BEGIN
    DECLARE @SQL NVARCHAR(255)
    DECLARE @params NVARCHAR(100)
    SET @params = '@override NVARCHAR(100) OUTPUT'

    IF (SELECT TOP 1 DATA_TYPE FROM #overrideData) = 'SUBMITTER'
    BEGIN
        --Submitter's ADDRESS record
        SET @SQL = N'UPDATE misc SET MISC_DATA = SUBSTRING(ISNULL(' + 
            (SELECT TOP 1 GROUP_CONFIG_NAME FROM #overrideData) + 
            ' , misc.MISC_DATA), 1, 8) FROM #miscData misc
              JOIN component.ADDRESS a ON a.PREFERRED = ''Y''
              AND a.PAX_ID = misc.SUBMITTER_PAX'
        exec sp_executesql @SQL
    END
    ELSE IF (SELECT TOP 1 DATA_TYPE FROM #overrideData) = 'RECIPIENT'
    BEGIN
        --Recipient's ADDRESS record
        SET @SQL = N'UPDATE misc SET MISC_DATA = SUBSTRING(ISNULL(' + 
            (SELECT TOP 1 GROUP_CONFIG_NAME FROM #overrideData) + 
            ' , misc.MISC_DATA), 1, 8) FROM #miscData misc
              JOIN component.ADDRESS a ON a.PREFERRED = ''Y''
              AND a.PAX_ID = misc.RECEIVER_PAX'
        exec sp_executesql @SQL
    END
END

INSERT INTO component.TRANSACTION_HEADER_MISC (TRANSACTION_HEADER_ID, VF_NAME, MISC_DATA, MISC_DATE)
SELECT misc.TRANSACTION_HEADER_ID, 'CONFIGURABLE_SUB_PROJECT_NUMBER', misc.MISC_DATA, GETDATE() FROM #miscData misc WHERE misc.MISC_DATA IS NOT NULL

DROP TABLE #miscData
DROP TABLE #overrideData

END

GO