/****** Object:  StoredProcedure [component].[UP_RECOGNITION_GIVEN_HIERARCHY_REPORT]    Script Date: 10/20/2020 3:27:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_GIVEN_HIERARCHY_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS GIVEN BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID.  THIS PROCEDURE MUST BE CALLED BY
-- ANOTHER PROCEDURE THAT CREATES THE #NOMINATION TEMP TABLE THAT THIS PROCEDURE
-- POPULATES.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR    	DATE        STORY     	CHANGE DESCRIPTION
-- DANDUR       20120428                CREATED
-- CHIDRIS   	20130328                ADDED LOCALE CODE
-- DOHOGNTA     20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM       20151102                UDM3 CHANGES
-- MUDDAM       20151113                UDM4 CHANGES
-- MUDDAM       20170922    MP-8001    	UDM5 CHANGES
-- burgestl	20201021		Update join to sql 2017
-- hernancl		 20210601	            update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_GIVEN_HIERARCHY_REPORT]
    @submitter_pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @is_manager    NCHAR(1)
,    @is_manager_report_to NCHAR(1)
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

CREATE TABLE #pax (
    pax_id BIGINT NOT NULL )

DECLARE @submitter_pax_id BIGINT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT @submitter_pax_id =  PAX_ID FROM PAX_GROUP where PAX_GROUP_ID = @submitter_pax_group_id

IF(@is_manager = 'Y')
BEGIN
  IF(@is_manager_report_to = 'Y')
  BEGIN
    INSERT INTO #pax
    SELECT P.PAX_ID 
    FROM PAX_REPORT_TO PRT WITH (NOLOCK)
    INNER JOIN PAX_GROUP PG WITH (NOLOCK)
        ON PG.PAX_GROUP_ID = @submitter_pax_group_id
    AND (PRT.REPORT_TO_PAX_ID = PG.PAX_ID OR PRT.PAX_ID = PG.PAX_ID)
    INNER JOIN PAX AS P     WITH (NOLOCK)
        ON PRT.PAX_ID = P.PAX_ID
  END
  ELSE
  BEGIN
    INSERT INTO #pax
    SELECT [id] FROM UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX (@submitter_pax_id, 1, NULL)  
  END
END
ELSE 
BEGIN
  INSERT INTO #pax
  VALUES(@submitter_pax_id)
END

-- GET THE NOMINATION DATA.  #nomination is created by the calling stored procedure.
INSERT INTO #nomination (
    nomination_id
,    nomination_status

,    submittal_date
,    submitter_list

,    receiver_list

,    comment_type
,    justification_comment_available_flag

,    criteria
,    program_id
,    budget_id
)
SELECT
    NOMINATION.ID
,    NOMINATION.STATUS
,    NOMINATION.SUBMITTAL_DATE
,    component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION.ID)
,    component.UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION(NOMINATION.ID)
,    CASE
        WHEN NOMINATION.ECARD_ID IS NULL THEN 'C'
        ELSE 'E'
    END
,    CASE WHEN NOMINATION.COMMENT_JUSTIFICATION IS NULL THEN 0 ELSE 1 END
,    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION.ID, @locale_code)
,    NOMINATION.PROGRAM_ID
,    NOMINATION.RECOGNITION_BUDGET_ID
FROM NOMINATION 
INNER JOIN #pax ON NOMINATION.SUBMITTER_PAX_ID = #pax.pax_id 
WHERE NOMINATION.SUBMITTAL_DATE 
BETWEEN @submittal_date_from AND @submittal_date_thru 
AND NOMINATION.STATUS_TYPE_CODE IN('PENDING', 'APPROVED', 'REJECTED', 'FINISHED') 
AND NOMINATION.STATUS_TYPE_CODE IS NOT NULL

UPDATE    #nomination
SET    #nomination.justification_flag = 1
FROM    PROGRAM_MISC
WHERE    #nomination.PROGRAM_ID = PROGRAM_MISC.PROGRAM_ID
AND    PROGRAM_MISC.VF_NAME = 'ENABLE_JUSTIFICATION'
AND    PROGRAM_MISC.MISC_DATA IN ('Y', 'O')
AND    GETDATE() BETWEEN PROGRAM_MISC.FROM_DATE AND ISNULL(THRU_DATE, GETDATE())

UPDATE    #nomination
SET    receiver_name = (
    SELECT TOP 1 ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
    FROM    RECOGNITION
    INNER JOIN    PAX
    ON    RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
)

UPDATE    #nomination
SET    last_approval_date = (
    SELECT    MAX(APPROVAL_HISTORY.APPROVAL_DATE)
    FROM    RECOGNITION
    INNER JOIN     APPROVAL_HISTORY
    ON    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
)

UPDATE    #nomination
SET    approval_process_id = APPROVAL_PROCESS.ID
FROM    #nomination
INNER JOIN     APPROVAL_PROCESS
ON    #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #nomination.PROGRAM_ID = APPROVAL_PROCESS.PROGRAM_ID

UPDATE    #nomination
SET    approval_count = (
    SELECT    COUNT(1) 
    FROM    (
        SELECT APPROVAL_PENDING.ID AS ID
        FROM    RECOGNITION
        INNER JOIN     APPROVAL_PENDING
        ON    RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID
        WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
        AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
        AND APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'
        AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
        UNION
        SELECT    APPROVAL_HISTORY.ID AS ID
        FROM    RECOGNITION
        INNER JOIN     APPROVAL_HISTORY
        ON RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID
        WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
        AND APROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
        AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
        AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
    ) AS APPROVALS
)

UPDATE    #nomination
SET    submitter_name = ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
FROM    #nomination
,    PAX
,    NOMINATION
WHERE    #nomination.nomination_id = NOMINATION.ID
AND    PAX.PAX_ID = NOMINATION.SUBMITTER_PAX_ID

UPDATE    #nomination
SET    program_name = component.UF_TRANSLATE (@locale_code, PROGRAM.PROGRAM_NAME, 'PROGRAM', 'PROGRAM_NAME')
FROM    nomination
,    PROGRAM
WHERE    #nomination.program_id = PROGRAM.PROGRAM_ID

-- Get the total approved, denied or pending award amount for each nomination
UPDATE    #nomination
SET    total_points = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
)

-- Get the total approved points for each nomination
UPDATE    #nomination
SET    total_points_approved = (
    SELECT
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END

    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'APPROVED'
)

-- Get the total denied points for each nomination
UPDATE    #nomination
SET    total_points_denied = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END

    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'REJECTED'
)

-- Get the total pending points for each nomination
UPDATE    #nomination
SET    total_points_pending = (
    SELECT    
        CASE
            WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
            ELSE  null
        END
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND ISNULL(RECOGNITION.AMOUNT,0) >0
    AND    RECOGNITION.STATUS_TYPE_CODE = 'PENDING'
)

UPDATE    #nomination
SET    recognition_id = (
    SELECT    TOP 1 RECOGNITION.ID
    FROM    RECOGNITION
    WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
)

UPDATE    #nomination
SET    approval_notes = APPROVAL_HISTORY.APPROVAL_NOTES
FROM    #nomination
,    RECOGNITION
,    APPROVAL_HISTORY
WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
AND    RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
AND    APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
AND    #nomination.nomination_status = 'REJECTED'
AND    RECOGNITION.STATUS_TYPE_CODE = 'REJECTED'

UPDATE    #nomination    
SET    approver_count = (
    SELECT    COUNT(1)
    FROM     RECOGNITION
    ,    APPROVAL_PROCESS
    ,    APPROVAL_PENDING
    WHERE    RECOGNITION.NOMINATION_ID = #nomination.nomination_id
    AND    #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID 
    AND    #nomination.PROGRAM_ID = APPROVAL_PROCESS.PROGRAM_ID 
    AND    APPROVAL_PROCESS.ID = APPROVAL_PENDING.APPROVAL_PROCESS_ID 
    AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID AND APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'
    AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED')
)
    
-- DO MISCELLANOUS RULES SO THE REPORT DOESN'T HAVE TO
UPDATE    #nomination
SET    last_approval_date = NULL
WHERE    nomination_status = 'PENDING'

UPDATE    #nomination
SET    last_approval_date = submittal_date
WHERE    approval_count = 0
AND    nomination_status <> 'PENDING'

UPDATE    #nomination
SET    approver_name = (
    SELECT
        CASE
            WHEN approver_count > 1 THEN 'Multiple Approval'
            ELSE (    SELECT    ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
                FROM    PAX
                ,    RECOGNITION
                ,    APPROVAL_PENDING
                WHERE    #nomination.nomination_id = RECOGNITION.NOMINATION_ID
                AND    RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID AND APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION'
                AND    APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
                AND    APPROVAL_PENDING.PAX_ID = PAX.PAX_ID
                AND    RECOGNITION.STATUS_TYPE_CODE IN ('PENDING', 'APPROVED', 'REJECTED'))
        END
)

UPDATE    #nomination
SET    submitter_name = 
    CASE
        WHEN #nomination.submitter_list LIKE '%,%' THEN 'Multiple Submitters'
        ELSE #nomination.submitter_name
    END

UPDATE    #nomination
SET    receiver_name =
    CASE
        WHEN #nomination.receiver_list LIKE '%,%' THEN 'Multiple Recipients'
        ELSE #nomination.receiver_name
    END

UPDATE    #nomination
SET    comment_type =
    CASE
        WHEN #nomination.receiver_list LIKE '%,%' AND #nomination.comment_type = 'E' THEN 'M'
        ELSE #nomination.comment_type
    END
    
 --Update the award type
INSERT INTO #award_types(
    nomination_id 
,    award_type
,    payout_item_id
)
    SELECT    n.nomination_id,
            CASE 
             WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN 
                  CASE           
                      WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
                        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
                    ELSE PI.PAYOUT_ITEM_NAME
                  END
             ELSE
                 CASE
                    WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
                        THEN component.UF_TRANSLATE (@locale_code, 'Points', 'Reporting', 'Reporting')
                    ELSE component.UF_TRANSLATE (@locale_code, PI.PAYOUT_ITEM_DESC, 'PAYOUT_ITEM', 'PAYOUT_ITEM_DESC')
                 END
            END AS award_type,PI.PAYOUT_ITEM_ID
    FROM    PAYOUT_ITEM PI
    RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
        ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
    RIGHT OUTER JOIN #nomination n WITH (NOLOCK)
        ON RB.ID  = n.budget_id


UPDATE    #nomination 
SET    #nomination.award_type = a.award_type
    FROM #nomination  INNER JOIN #award_types A
        ON #nomination.nomination_id = a.nomination_id 

--
UPDATE    #nomination
SET    #nomination.payout_item_id = a.payout_item_id
FROM #nomination INNER JOIN #award_types A
        ON #nomination.nomination_id = a.nomination_id

END

GO
