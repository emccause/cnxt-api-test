/****** Object:  StoredProcedure [component].[UP_RECOGNITION_CRITERIA_BASE]    Script Date: 10/15/2015 11:01:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_CRITERIA_BASE
-- ===========================  DESCRIPTION  ==================================
-- RETURN THE SUM OF INDIVIDUALS, CRITERIA AND RECOGNITION
--
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- PALD00        20081020                CREATED
-- SCHIERVR        20081030                MOVED CODE FROM REPORTS TO BASE.
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- WRIGHTM        20151117                CHANGED STATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_CRITERIA_BASE]
    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,    @recognition_selection_criteria NVARCHAR(4)
AS
BEGIN

SET NOCOUNT ON

-- DECLARE GLOBAL VARIABLES
DECLARE  @pax_id BIGINT

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE PAXID
SET @pax_id = (SELECT PAX_ID
               FROM   PAX_GROUP
               WHERE  PAX_GROUP_ID = @pax_group_id)

-- GET ALL THE PAXID IN THE PAX GROUP
CREATE TABLE #pax_hierarchy_list (PAX_ID BIGINT NULL)
INSERT INTO #pax_hierarchy_list SELECT id AS PAX_ID FROM UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX (@pax_id, 1, NULL)

-- INSERT DATA IN THE TEMPORARY TABLE
INSERT INTO #recognition_criteria (
    nomination_id
,    recognition_id
,    submitter_pax_id
,    receiver_pax_id
,    recogniton_criteria_id
,    recogniton_criteria_name
,    recognition_amount
)

SELECT    NOMINATION.ID
,    RECOGNITION.ID
,    NOMINATION.SUBMITTER_PAX_ID
,    RECOGNITION.RECEIVER_PAX_ID
,    RECOGNITION_CRITERIA.ID
,    RECOGNITION_CRITERIA.RECOGNITION_CRITERIA_NAME
,    RECOGNITION.AMOUNT
FROM    RECOGNITION
INNER JOIN NOMINATION
    ON RECOGNITION.NOMINATION_ID = NOMINATION.ID
INNER JOIN NOMINATION_CRITERIA
    ON NOMINATION.ID = NOMINATION_CRITERIA.NOMINATION_ID
INNER JOIN RECOGNITION_CRITERIA
    ON NOMINATION_CRITERIA.RECOGNITION_CRITERIA_ID = RECOGNITION_CRITERIA.ID
WHERE   (NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru)
AND (RECOGNITION.STATUS_TYPE_CODE = N'APPROVED')
AND (NOMINATION.SUBMITTER_PAX_ID IN (SELECT PAX_ID FROM #pax_hierarchy_list)
    OR  RECOGNITION.RECEIVER_PAX_ID IN (SELECT PAX_ID FROM #pax_hierarchy_list))

-- FILTER OUT THE RECORDS AS PER @recognition_selection_criteria
IF (@recognition_selection_criteria = 'RR')
BEGIN
    DELETE #recognition_criteria
    WHERE receiver_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)
END
ELSE
IF (@recognition_selection_criteria = 'RG')
BEGIN
    DELETE #recognition_criteria
    WHERE submitter_pax_id NOT IN (SELECT PAX_ID FROM #pax_hierarchy_list)    
END

END

GO

