/****** Object:  StoredProcedure [component].[UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS]    Script Date: 6/1/2018 10:08:14 AM ******/
IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS' AND schema_id = SCHEMA_ID('component'))
    BEGIN
        exec ('create PROCEDURE component.UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS as BEGIN select 1 as c1 END')
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS
-- ===========================  DESCRIPTION  ==================================
-- FETCHES A PAGE OF DATA FOR THE LOGGED IN PAX FROM ALL ITEMS GIVEN OR RECEIVED BY THE LOGGED IN PAX AND THEIR DIRECT REPORTS
-- TAKES VISIBILITY INTO ACCOUNT
-- PAGINATION IS BASED ON CREATE DATE OF THE NEWSFEED_ITEM ENTRY (CURRENTLY CREATED AT TIME OF NOMINATION CREATION)
--
-- WILL LIMIT BASED ON NEWSFEED_ITEM_ID IF PROVIDED
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- MUDDAM        20160830    MP-8064        CREATED
-- MUDDAM        20160916    MP-8275        RE-ADDED SUPPORT FOR NEWSFEED_VISIBILITY
-- MUDDAM        20161005    MP-8065        REMOVED STATUS_TYPE_LIST PARAM SINCE IT'S NOT NEEDED
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
-- IRWINMB1      20180531	 MP-12228       QUERY OPTIMIZATIONS
-- BURGESTL		 20201117	 CNXT-335		Add Database Logging for Stored Procs
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS]
@LOGGED_IN_PAX_ID BIGINT,
@ACTIVITY_TYPE_LIST NVARCHAR(MAX),
@FILTER NVARCHAR(MAX),
@PAGE_NUMBER BIGINT,
@PAGE_SIZE BIGINT,
@NEWSFEED_ITEM_ID BIGINT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

--(+)Logging
BEGIN TRY
declare @loggingStartDate  DATETIME2(7),
		@loggingEndDate  DATETIME2(7),
		@loggingParameters NVARCHAR(MAX),
		@loggingSprocName nvarchar(150),
		@loggingDebug int

select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

if (@loggingDebug=1)
begin
set @loggingSprocName = 'UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS'
set @loggingStartDate = getdate()
set @loggingParameters = '@LOGGED_IN_PAX_ID=' + isnull(cast(@LOGGED_IN_PAX_ID as varchar),'null') --BIGINT
					+ ' :: @ACTIVITY_TYPE_LIST=' +isnull(@ACTIVITY_TYPE_LIST, 'null') -- NVARCHAR(MAX)
					+ ' :: @FILTER=' +isnull(@FILTER, 'null') -- NVARCHAR(MAX)
					+ ' :: @PAGE_NUMBER=' + isnull(cast(@PAGE_NUMBER as varchar),'null') --BIGINT
					+ ' :: @PAGE_SIZE=' + isnull(cast(@PAGE_SIZE as varchar),'null') --BIGINT
					+ ' :: @NEWSFEED_ITEM_ID=' + isnull(cast(@NEWSFEED_ITEM_ID as varchar),'null') --BIGINT
end
--(-)Logging

SELECT
    COUNT(*) OVER() AS TOTAL_RESULTS,
    *
FROM (
        SELECT 
            NOMINATION_ID,
            NEWSFEED_ITEM_ID,
            CREATE_DATE,
            CASE
                WHEN DIRECT_REPORT_SUBMITTED IS NOT NULL -- DIRECT REPORT HAS PRIORITY
                    THEN CASE
                        WHEN GIVE_ANONYMOUS = 'TRUE' -- DON'T INCLUDE ANONYMOUS AWARD CODES IF THEY'RE NOT RECEIVED BY DIRECT REPORT
                            THEN NULL
                        ELSE DIRECT_REPORT_SUBMITTED
                    END
                WHEN (
                    @FILTER LIKE '%RECEIVED%' -- IF WE DON'T WANT DIRECT REPORT RECEIVED, DIRECT REPORT SUBMITTER MUST BE POPULATED
                    AND (
                        DIRECT_REPORT_PAX_RECEIVED IS NOT NULL
                        OR LOGGED_IN_PAX_RECEIVED IS NOT NULL
                    )
                ) -- DIRECT REPORT OR LOGGED IN PAX RECEIVED, DON'T CARE ABOUT SUBMITTERS
                    THEN CASE -- HANDLE GIVE_ANONYMOUS
                        WHEN GIVE_ANONYMOUS = 'TRUE'
                            THEN 0
                        ELSE DEFAULT_PAX_SUBMITTED
                    END
                ELSE NULL -- DON'T SHOW
            END AS SUBMITTER_PAX_ID,
            CASE
                WHEN NEWSFEED_VISIBILITY = 'NONE' -- NEWSFEED_VISIBILITY = NONE -> DO NOT SHOW TO ANYONE, EVEN SUBMITTER / RECEIVER
                    THEN NULL
                WHEN LOGGED_IN_PAX_RECEIVED IS NOT NULL -- LOGGED IN PAX RECEIVED HAS PRIORITY
                    THEN LOGGED_IN_PAX_RECEIVED
                WHEN DIRECT_REPORT_PAX_RECEIVED IS NOT NULL -- DIRECT REPORT RECIPIENT HAS PRIORITY EVEN IF DIRECT REPORT SUBMITTER
                    THEN DIRECT_REPORT_PAX_RECEIVED
                WHEN (
                    @FILTER LIKE '%GIVEN%' -- IF WE DON'T WANT DIRECT REPORT GIVEN, DIRECT REPORT RECEIVED MUST BE POPULATED
                    AND DIRECT_REPORT_SUBMITTED IS NOT NULL
                ) -- IF DIRECT REPORT SUBMITTED, DON'T CARE ABOUT SUBMITTER
                    THEN DEFAULT_PAX_RECEIVED
                ELSE NULL -- DON'T SHOW
            END AS RECIPIENT_PAX_ID
        FROM (
            SELECT
                NOM.ID AS NOMINATION_ID,
                NFI.ID AS NEWSFEED_ITEM_ID,
                NFI.CREATE_DATE,
                NFI.NEWSFEED_VISIBILITY,
                DIRECT_REPORT_SUBMITTED.SUBMITTER_PAX_ID AS DIRECT_REPORT_SUBMITTED, -- LOGGED IN PAX AND DIRECT_REPORTS
                NOM.SUBMITTER_PAX_ID AS DEFAULT_PAX_SUBMITTED,
                ( -- GIVE_ANONYMOUS
                    SELECT TOP 1
                        RECG_MISC.MISC_DATA
                    FROM component.RECOGNITION RECG
                    LEFT JOIN component.RECOGNITION_MISC RECG_MISC
                        ON RECG_MISC.RECOGNITION_ID = RECG.ID
                        AND RECG_MISC.VF_NAME = 'GIVE_ANONYMOUS'
                    WHERE RECG.NOMINATION_ID = NOM.ID
                        AND RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                ) AS GIVE_ANONYMOUS,
                ( -- LOGGED_IN_PAX_RECEIVED
                    SELECT TOP 1
                        RECEIVER_PAX_ID
                    FROM component.RECOGNITION
                    WHERE NOMINATION_ID = NOM.ID
                        AND RECEIVER_PAX_ID = @LOGGED_IN_PAX_ID
                        AND STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                ) AS LOGGED_IN_PAX_RECEIVED,
                DIRECT_REPORT_PAX_RECEIVED.RECEIVER_PAX_ID AS DIRECT_REPORT_PAX_RECEIVED,
                ( -- DEFAULT_PAX_RECEIVED
                    SELECT TOP 1
                        RECG.RECEIVER_PAX_ID
                    FROM component.RECOGNITION RECG
                    LEFT JOIN component.NOMINATION SUB_NOM
                        ON RECG.NOMINATION_ID = SUB_NOM.ID
                    LEFT JOIN component.PAX_MISC SHARE_PREF
                        ON RECG.RECEIVER_PAX_ID = SHARE_PREF.PAX_ID
                        AND SHARE_PREF.VF_NAME = 'SHARE_REC'
                        AND SHARE_PREF.MISC_DATA = 'TRUE'
                    WHERE SUB_NOM.ID = NOM.ID
                        AND RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                        AND (
                            NFI.NEWSFEED_VISIBILITY = 'PUBLIC' -- IF NEWSFEED_VISIBILITY IS PUBLIC, YOU CAN SEE EVERYONE
                            OR SUB_NOM.SUBMITTER_PAX_ID = @LOGGED_IN_PAX_ID -- SUBMITTER CAN SEE UNLESS NEWSFEED_VISIBILITY = NONE
                            OR ( -- IF NEWSFEED_VISIBILITY IS RECIPIENT_PREF, FIND SOMEONE WHO WANTS TO SHARE REC PUBLICLY
                                NFI.NEWSFEED_VISIBILITY = 'RECIPIENT_PREF'
                                AND SHARE_PREF.MISC_DATA = 'TRUE'
                            )
                        )
                ) AS DEFAULT_PAX_RECEIVED
            FROM component.NEWSFEED_ITEM NFI
            LEFT JOIN component.NOMINATION NOM
                ON NFI.TARGET_ID = NOM.ID
                AND NFI.TARGET_TABLE = 'NOMINATION'
            LEFT JOIN ( -- DIRECT_REPORT_SUBMITTED
                SELECT DISTINCT
                    NOM.ID AS NOMINATION_ID,
                    NOM.SUBMITTER_PAX_ID AS SUBMITTER_PAX_ID
                FROM component.NOMINATION NOM
                LEFT JOIN component.RECOGNITION RECG
                    ON NOM.ID = RECG.NOMINATION_ID
                LEFT JOIN component.RELATIONSHIP DIRECT_REPORTS
                    ON NOM.SUBMITTER_PAX_ID = DIRECT_REPORTS.PAX_ID_1
                    AND DIRECT_REPORTS.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
                    AND DIRECT_REPORTS.PAX_ID_2 = @LOGGED_IN_PAX_ID
                WHERE (
                    DIRECT_REPORTS.PAX_ID_1 IS NOT NULL -- EITHER DIRECT REPORT SUBMITTER
                    OR NOM.SUBMITTER_PAX_ID = @LOGGED_IN_PAX_ID -- OR LOGGED IN PAX (SINCE THERE'S ONLY ONE SUBMITTER)
                )
                    AND RECG.STATUS_TYPE_CODE  IN ('APPROVED', 'AUTO_APPROVED')
            ) DIRECT_REPORT_SUBMITTED
                ON NOM.ID = DIRECT_REPORT_SUBMITTED.NOMINATION_ID
            LEFT JOIN ( -- DIRECT_REPORT_PAX_RECEIVED
                SELECT
                    RECG.NOMINATION_ID,
                    MIN(RECG.RECEIVER_PAX_ID) AS RECEIVER_PAX_ID
                FROM component.RECOGNITION RECG
                LEFT JOIN component.RELATIONSHIP DIRECT_REPORTS
                    ON RECG.RECEIVER_PAX_ID = DIRECT_REPORTS.PAX_ID_1
                    AND DIRECT_REPORTS.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
                    AND DIRECT_REPORTS.PAX_ID_2 = @LOGGED_IN_PAX_ID
                WHERE DIRECT_REPORTS.PAX_ID_1 IS NOT NULL -- DIRECT REPORT RECEIVED, LOGGED IN PAX RECEIVED IS HANDLED ABOVE
                    AND RECG.PARENT_ID IS NULL
                    AND RECG.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED')
                GROUP BY RECG.NOMINATION_ID
            ) DIRECT_REPORT_PAX_RECEIVED
                ON NOM.ID = DIRECT_REPORT_PAX_RECEIVED.NOMINATION_ID
            WHERE NFI.NEWSFEED_ITEM_TYPE_CODE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@ACTIVITY_TYPE_LIST, ','))
                AND NFI.STATUS_TYPE_CODE = 'APPROVED'
                AND (@NEWSFEED_ITEM_ID IS NULL OR NFI.ID = @NEWSFEED_ITEM_ID)
        ) RAW_DATA
    ) DATA WHERE DATA.SUBMITTER_PAX_ID IS NOT NULL AND DATA.RECIPIENT_PAX_ID IS NOT NULL
ORDER BY DATA.CREATE_DATE DESC
OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS FETCH NEXT @PAGE_SIZE ROWS ONLY

--(+)Logging
if (@loggingDebug=1)
begin
	set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error  
	  --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY  
BEGIN CATCH
	if (@loggingDebug=1)
	begin
		declare @errorMessage nvarchar(250) 
		SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE() 
		exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
	end
END CATCH
--(-)Logging

END;