/****** Object:  StoredProcedure [component].[UP_INDIVIDUAL_ELIGIBLE_BUDGET_USED]    Script Date: 01/27/2017 11:28:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_INDIVIDUAL_BUDGET_REMAINING
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS THE TOTAL BUDGET AMOUNT EACH PAX HAS LEFT TO SPEND
--
-- @givingLimit - Filter budgets that have giving limits. 0=FALSE, 1=TRUE
-- @returnAll - Return all data if 1 or return the calculated remaining budget if 0
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY    CHANGE DESCRIPTION
-- HARWELLM        20160126    MP-9392    CREATED
-- HARWELLM        20160127    MP-9033    ADD FLAG TO RETURN THE TOTAL AND USED AMOUNTS OR THE REMAINING AMOUNT
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- KUMARSJ        20171120    MP-11332    USED the latest BUDGET Views
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_INDIVIDUAL_BUDGET_REMAINING]
@givingLimit BIT, @returnAll BIT

AS
BEGIN
SET NOCOUNT ON;

CREATE TABLE #ELIGIBLE_BUDGETS (
    BUDGET_ID BIGINT,
    PAX_ID BIGINT
)
INSERT INTO #ELIGIBLE_BUDGETS --ALl budgets tied to programs
    SELECT DISTINCT b.ID,
    CASE 
        WHEN a.SUBJECT_TABLE = 'PAX' THEN SUBJECT_ID
        WHEN a.SUBJECT_TABLE = 'GROUPS' THEN vw.PAX_ID
    END AS 'PAX_ID'    
    FROM component.BUDGET B 
    JOIN component.PROGRAM_BUDGET PB ON B.ID = PB.BUDGET_ID
    INNER JOIN component.ACL a
        ON PB.ID = a.TARGET_ID
        AND a.TARGET_TABLE = 'PROGRAM_BUDGET'
    INNER JOIN component.ROLE ROLE
        ON a.ROLE_ID = ROLE.ROLE_ID
        AND ROLE.ROLE_CODE = 'PROGRAM_BUDGET_GIVER'
    LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP vw
        ON vw.GROUP_ID = a.SUBJECT_ID AND a.SUBJECT_TABLE = 'GROUPS'
    ORDER BY b.ID
    
    
CREATE TABLE #BUDGET_USER (
    BUDGET_ID BIGINT,
    PAX_ID BIGINT
)
INSERT INTO #BUDGET_USER --All budgets with BUDGET_USER permission
    SELECT DISTINCT a.TARGET_ID,
    CASE 
        WHEN a.SUBJECT_TABLE = 'PAX' THEN SUBJECT_ID
        WHEN a.SUBJECT_TABLE = 'GROUPS' THEN vw.PAX_ID
    END AS 'PAX_ID'    
    FROM component.ACL a
    INNER JOIN component.ROLE r ON a.ROLE_ID = r.ROLE_ID AND r.ROLE_CODE = 'BUDGET_USER' 
    LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP vw
        ON vw.GROUP_ID = a.SUBJECT_ID AND a.SUBJECT_TABLE = 'GROUPS'
    WHERE a.TARGET_TABLE = 'BUDGET' and a.SUBJECT_TABLE in ('PAX', 'GROUPS')
    ORDER BY a.TARGET_ID
    

CREATE TABLE #BUDGET_USER_ELIGIBLE (
    BUDGET_ID BIGINT,
    PAX_ID BIGINT
)

--SIMPLE budgets
INSERT INTO #BUDGET_USER_ELIGIBLE 
SELECT eb.*
FROM #ELIGIBLE_BUDGETS eb
JOIN component.BUDGET b ON b.ID = eb.BUDGET_ID
JOIN component.BUDGET_MISC bm ON bm.BUDGET_ID = b.ID and bm.VF_NAME = 'IS_USABLE' AND bm.MISC_DATA = 'TRUE'
WHERE b.BUDGET_TYPE_CODE = 'SIMPLE'

--DISTRIBUTED budgets
INSERT INTO #BUDGET_USER_ELIGIBLE
SELECT distinct bu.*
FROM #ELIGIBLE_BUDGETS eb
JOIN component.BUDGET b_parent ON b_parent.ID = eb.BUDGET_ID
JOIN component.BUDGET b_child ON b_child.PARENT_BUDGET_ID = b_parent.ID
JOIN component.BUDGET_MISC bm ON bm.BUDGET_ID = b_child.ID AND bm.VF_NAME = 'IS_USABLE' AND bm.MISC_DATA = 'TRUE'
JOIN #BUDGET_USER bu ON bu.BUDGET_ID = b_child.ID AND bu.PAX_ID = eb.PAX_ID
where b_parent.BUDGET_TYPE_CODE = 'DISTRIBUTED'

--ALLOCATED budgets
INSERT INTO #BUDGET_USER_ELIGIBLE
SELECT distinct bu.*
FROM #ELIGIBLE_BUDGETS eb
JOIN component.BUDGET b_parent ON b_parent.ID = eb.BUDGET_ID
JOIN component.BUDGET_MISC bm ON bm.MISC_DATA = b_parent.ID AND bm.VF_NAME = 'TOP_LEVEL_BUDGET_ID'
JOIN component.BUDGET b_child ON b_child.ID = bm.BUDGET_ID
JOIN component.BUDGET_MISC bm_usable ON bm_usable.BUDGET_ID = b_child.ID AND bm_usable.VF_NAME = 'IS_USABLE' AND bm_usable.MISC_DATA = 'TRUE'
JOIN #BUDGET_USER bu ON bu.BUDGET_ID = b_child.ID AND bu.PAX_ID = eb.PAX_ID
WHERE b_parent.BUDGET_TYPE_CODE = 'ALLOCATED'


--Table to hold used amounts grouped by budget and pax
CREATE TABLE #USED_AMOUNTS (
    PAX_ID BIGINT,
    BUDGET_ID BIGINT,
    USED_AMOUNT DECIMAL(10,2),
    TOTAL_AMOUNT DECIMAL(10,2),
    STATUS NVARCHAR(50)
)

INSERT INTO #USED_AMOUNTS
SELECT distinct p.PAX_ID, bue.BUDGET_ID,
(CASE
    WHEN DEBITS.TOTAL_DEBITS is null then 0 
    ELSE DEBITS.TOTAL_DEBITS 
END)
-
(CASE
    WHEN CREDITS.TOTAL_CREDITS is null then 0
    ELSE CREDITS.TOTAL_CREDITS
END)
AS 'BUDGET_USED', NULL,
CASE
    WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED'
    WHEN b.THRU_DATE < GETDATE() THEN 'ENDED'
    ELSE 'ACTIVE'
END AS 'STATUS'
FROM component.PAX p
JOIN #BUDGET_USER_ELIGIBLE bue ON bue.PAX_ID = p.PAX_ID
JOIN component.BUDGET b ON b.ID = bue.BUDGET_ID AND b.STATUS_TYPE_CODE = 'ACTIVE'
LEFT JOIN ( --DEBITS
    select n.SUBMITTER_PAX_ID, SUM(disc_debit.AMOUNT) AS 'TOTAL_DEBITS', disc_debit.BUDGET_ID_DEBIT
    FROM component.NOMINATION n 
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID 
    JOIN component.DISCRETIONARY disc_debit ON disc_debit.TARGET_ID = r.ID and disc_debit.TARGET_TABLE = 'RECOGNITION' 
    JOIN component.TRANSACTION_HEADER th ON th.ID = disc_debit.TRANSACTION_HEADER_ID
        AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED')
        AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE <> 'FUND' 
    JOIN component.BUDGET b ON b.ID = disc_debit.BUDGET_ID_DEBIT
        AND (
            @givingLimit IS NULL 
            OR @givingLimit = 0 
            OR (@givingLimit = 1 AND (b.INDIVIDUAL_GIVING_LIMIT IS NOT NULL OR b.BUDGET_TYPE_CODE = 'ALLOCATED'))
        )
    JOIN #BUDGET_USER_ELIGIBLE bue ON bue.BUDGET_ID = b.ID AND n.SUBMITTER_PAX_ID = bue.PAX_ID
    GROUP BY n.SUBMITTER_PAX_ID, disc_debit.BUDGET_ID_DEBIT
) DEBITS ON DEBITS.SUBMITTER_PAX_ID = p.PAX_ID AND DEBITS.BUDGET_ID_DEBIT = bue.BUDGET_ID
LEFT JOIN ( --CREDITS
    select n.SUBMITTER_PAX_ID, SUM(disc_credit.AMOUNT) AS 'TOTAL_CREDITS', disc_credit.BUDGET_ID_CREDIT
    FROM component.NOMINATION n 
    JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID 
    JOIN component.DISCRETIONARY disc_credit ON disc_credit.TARGET_ID = r.ID and disc_credit.TARGET_TABLE = 'RECOGNIION' 
    JOIN component.TRANSACTION_HEADER th ON th.ID = disc_credit.TRANSACTION_HEADER_ID
        AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED')
        AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE <> 'FUND' 
    JOIN component.BUDGET b ON b.ID = disc_credit.BUDGET_ID_CREDIT
        AND (
            @givingLimit IS NULL 
            OR @givingLimit = 0
            OR (@givingLimit = 1 AND (b.INDIVIDUAL_GIVING_LIMIT IS NOT NULL OR b.BUDGET_TYPE_CODE = 'ALLOCATED'))
        )
    JOIN #BUDGET_USER_ELIGIBLE bue ON bue.BUDGET_ID = b.ID AND n.SUBMITTER_PAX_ID = bue.PAX_ID
    GROUP BY n.SUBMITTER_PAX_ID, disc_credit.BUDGET_ID_CREDIT
) CREDITS ON CREDITS.SUBMITTER_PAX_ID = p.PAX_ID AND CREDITS.BUDGET_ID_CREDIT = bue.BUDGET_ID


--Get total amounts for each budget, factoring giving limit
UPDATE ua 
    SET ua.TOTAL_AMOUNT = data.TOTAL_AMOUNT
FROM #USED_AMOUNTS ua
LEFT JOIN (
    SELECT distinct bue.PAX_ID, bue.BUDGET_ID,
        CASE
            WHEN b.BUDGET_TYPE_CODE = 'ALLOCATED' THEN btf.TOTAL_AMOUNT
            WHEN b.INDIVIDUAL_GIVING_LIMIT IS NULL AND @givingLimit = 0 THEN btf.TOTAL_AMOUNT
            ELSE b.INDIVIDUAL_GIVING_LIMIT
        END
    AS 'TOTAL_AMOUNT'
    FROM #BUDGET_USER_ELIGIBLE bue
    JOIN component.budget b ON b.ID = bue.BUDGET_ID
    INNER JOIN component.VW_BUDGET_TOTALS_FLAT btf
    on b.ID = BTF.BUDGET_ID
    WHERE (
        @givingLimit IS NULL 
        OR @givingLimit = 0 
        OR (@givingLimit = 1 AND (b.INDIVIDUAL_GIVING_LIMIT IS NOT NULL OR b.BUDGET_TYPE_CODE = 'ALLOCATED'))
    )
) data ON data.BUDGET_ID = ua.BUDGET_ID and data.PAX_ID = ua.PAX_ID

--Remove the records that will result in negatives (points were spent and then giving limit was adjusted - valid scenario)
DELETE FROM #USED_AMOUNTS WHERE USED_AMOUNT > TOTAL_AMOUNT OR TOTAL_AMOUNT IS NULL

--If returnAll is TRUE, return the results without aggregating the data
IF @returnAll = 1 
BEGIN
    SELECT * FROM #USED_AMOUNTS ORDER BY PAX_ID, BUDGET_ID
END 
ELSE
BEGIN
    --Sum up all of the remaining amounts and group by pax
    SELECT PAX_ID, SUM((TOTAL_AMOUNT - USED_AMOUNT)) AS 'BUDGET_REMAINING' FROM #USED_AMOUNTS WHERE STATUS = 'ACTIVE' GROUP BY PAX_ID ORDER BY PAX_ID
END


END

GO
