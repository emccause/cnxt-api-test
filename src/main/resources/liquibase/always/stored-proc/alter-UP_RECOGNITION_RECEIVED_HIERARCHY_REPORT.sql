/****** Object:  StoredProcedure [component].[UP_RECOGNITION_RECEIVED_HIERARCHY_REPORT]    Script Date: 10/15/2015 11:09:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_RECOGNITION_RECEIVED_HIERARCHY_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS RECEIVED BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- CHINTHR        20120428                CREATED
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
-- MUDDAM        20151113                UDM4 CHANGES
-- GARCIAF2        20160816    MP-5458        CHANGE KEY_NAME TO: recognition.socialmedia.enable
-- MUDDAM        20170922    MP-8001        UDM5 CHANGES
-- hernancl		  20210601	            update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_RECEIVED_HIERARCHY_REPORT]
    @pax_group_id BIGINT
,    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @is_manager    NCHAR(1)
,    @is_manager_report_to NCHAR(1)
,    @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

CREATE TABLE #pax (
    pax_id BIGINT NOT NULL 
)

DECLARE @receiver_pax_id BIGINT
DECLARE @share NVARCHAR(10)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT @receiver_pax_id =  PAX_ID FROM PAX_GROUP where PAX_GROUP_ID = @pax_group_id

IF(@is_manager = 'Y')
BEGIN
    IF(@is_manager_report_to ='Y')
    BEGIN
        INSERT INTO #pax
        SELECT P.PAX_ID 
        FROM PAX_REPORT_TO PRT WITH (NOLOCK)
        INNER JOIN PAX_GROUP PG WITH (NOLOCK)
        ON PG.PAX_GROUP_ID = @pax_group_id    AND 
        (PRT.REPORT_TO_PAX_ID = PG.PAX_ID  OR PRT.PAX_ID = PG.PAX_ID)
        INNER JOIN PAX AS P     WITH (NOLOCK)
        ON PRT.PAX_ID = P.PAX_ID
    END
    ELSE
    BEGIN
        INSERT INTO #pax
        SELECT [id] FROM UF_HIERARCHY_FETCH_PAX_REPORT_TO_BY_PAX(@receiver_pax_id, 1, NULL)
    END
END
ELSE 
BEGIN
    INSERT INTO #pax
    VALUES(@receiver_pax_id)
END

SET @share = (SELECT CASE
    WHEN VALUE IS NULL THEN 'false'
    ELSE VALUE
    END AS share
FROM APPLICATION_DATA WHERE KEY_NAME = 'recognition.socialmedia.enable')

INSERT INTO #recognition (
    nomination_id
,    ecard_id
,    recognition_id
,    recognition_status
,    recognition_amount
,    is_recognition_comment
,    program_id
,    program_name
,    budget_id
)
SELECT
    NOMINATION.ID
,    NOMINATION.ECARD_ID
,    RECOGNITION.ID
,    RECOGNITION.STATUS_TYPE_CODE
,    RECOGNITION.AMOUNT
,    CASE
        WHEN RECOGNITION.COMMENT IS NULL THEN 0
        ELSE 1
    END
,    PROGRAM.PROGRAM_ID
,    PROGRAM.PROGRAM_NAME
,    NOMINATION.RECOGNITION_BUDGET_ID
FROM    NOMINATION
INNER JOIN RECOGNITION ON  NOMINATION.ID = RECOGNITION.NOMINATION_ID
INNER JOIN PROGRAM ON NOMINATION.PROGRAM_ID = PROGRAM.PROGRAM_ID
INNER JOIN PAX ON RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
INNER JOIN #pax ON PAX.PAX_ID = #pax.PAX_ID
WHERE NOMINATION.SUBMITTAL_DATE BETWEEN CONVERT(datetime2, @submittal_date_from, 102) AND CONVERT(datetime2, @submittal_date_thru, 102)
AND    RECOGNITION.STATUS_TYPE_CODE = 'APPROVED'

UPDATE    #recognition
SET    APPROVAL_PROCESS_ID = APPROVAL_PROCESS.ID
FROM    #recognition
INNER JOIN   APPROVAL_PROCESS
ON    #recognition.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND    #recognition.program_id = APPROVAL_PROCESS.PROGRAM_ID

UPDATE    #recognition
SET    RECOGNITION_DATE = (
    SELECT    MAX(APPROVAL_HISTORY.APPROVAL_DATE)
    FROM    APPROVAL_HISTORY
    WHERE    #recognition.recognition_id = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION'
    AND    #recognition.approval_process_id = APPROVAL_HISTORY.APPROVAL_PROCESS_ID
)

UPDATE    #recognition
SET    RECOGNITION_CRITERIA = (
    SELECT    component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION_ID, @locale_code)
)

UPDATE    #recognition
SET    SUBMITTER_LIST = (
    SELECT component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION_ID)
)

UPDATE    #recognition
SET    SUBMITTED_BY_NAME = (
    SELECT    PAX.FIRST_NAME + ' ' + PAX.LAST_NAME
    FROM    PAX
    INNER JOIN    NOMINATION
    ON    NOMINATION.SUBMITTER_PAX_ID = PAX.PAX_ID
    WHERE   #recognition.NOMINATION_ID = NOMINATION.ID
)

--Update the award type
INSERT INTO #award_types(
    nomination_id 
,    award_type
,    payout_item_id
)
    SELECT    r.nomination_id,
            CASE 
             WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN 
                  CASE           
                      WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
                        THEN 'Points'
                    ELSE PI.PAYOUT_ITEM_NAME
                  END
             ELSE
                 CASE
                    WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
                        THEN 'Points'
                    ELSE PI.PAYOUT_ITEM_DESC
                 END
            END AS award_type,PI.PAYOUT_ITEM_ID
    FROM    PAYOUT_ITEM PI
    RIGHT OUTER JOIN RECOGNITION_BUDGET RB WITH (NOLOCK)
        ON RB.AWARD_TYPE = PI.PAYOUT_ITEM_NAME
    RIGHT OUTER JOIN #recognition r WITH (NOLOCK)
        ON RB.ID  = r.budget_id


UPDATE    #recognition 
SET    #recognition.award_type = a.award_type
    FROM #recognition  INNER JOIN #award_types A
        ON #recognition.nomination_id = a.nomination_id 

UPDATE    #recognition 
SET    #recognition.payout_item_id = a.payout_item_id
    FROM #recognition  INNER JOIN #award_types A
        ON #recognition.nomination_id = a.nomination_id 
        
UPDATE    #recognition
SET    share = @share

END

GO
