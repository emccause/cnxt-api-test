/****** Object:  StoredProcedure [component].[UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP]    Script Date: 10/15/2015 09:36:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S)
-- RETURN A DISTINCT LIST OF PAX (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20080530                CREATED
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP]
    @delim_list_group VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET    @delimiter = ','

SELECT    pax_id AS PAX_ID
FROM    UF_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP (@delim_list_group, @delimiter)

END

GO

