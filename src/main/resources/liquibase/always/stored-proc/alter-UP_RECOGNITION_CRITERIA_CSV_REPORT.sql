/****** Object:  StoredProcedure [component].[UP_RECOGNITION_CRITERIA_CSV_REPORT]    Script Date: 10/21/2020 10:49:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_CRITERIA_CSV_REPORT
-- ===========================  DESCRIPTION  ==================================
-- RETURN THE SUM OF INDIVIDUALS, CRITERIA AND RECOGNITION
--
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        STORY	CHANGE DESCRIPTION
-- SCHIERVR	20081030                CREATED
-- PALD00       20081112                CHANGED THE FIELDS FROM RECEIVER PAX ID TO NOMINATION ID
-- CHIDRIS      20130328                ADDED LOCALE CODE
-- DOHOGNTA     20150309                CHANGE IDENTITIES TO BIGINT
-- WRIGHTM      20151117                CHANGED STATUS TO STATUS_TYPE_CODE ALONG WITH SINGLE CHAR STATUS CODES TO STRINGS
-- burgestl	20201021		Update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_CRITERIA_CSV_REPORT]
    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,    @recognition_selection_criteria NVARCHAR(4)
,   @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

--DRIVER
CREATE TABLE #recognition_criteria (
    nomination_id BIGINT NOT NULL
,    recognition_id BIGINT NOT NULL
,    submitter_pax_id BIGINT NULL
,    receiver_pax_id BIGINT NULL
,    recogniton_criteria_id BIGINT
,    recogniton_criteria_name NVARCHAR(200)
,    recognition_amount INT
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_CRITERIA_BASE @submittal_date_from, @submittal_date_thru, @pax_group_id, @recognition_selection_criteria

DECLARE @sum_ind int
DECLARE @sum_recog int
DECLARE @sum_awd int

SELECT
    @sum_ind = COUNT(distinct nomination_id)
,    @sum_recog = COUNT(distinct recognition_id)
,    @sum_awd = SUM(recognition_amount)
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria

-- SELECT THE FINAL RESULT SETS
SELECT
    COUNT(DISTINCT nomination_id) 'num_individuals'
,    COUNT(DISTINCT recognition_id) 'num_recognitions'
,    SUM(recognition_amount) AS 'award_points'
,    component.UF_TRANSLATE (@locale_code, recogniton_criteria_name, 'RECOGNITION_CRITERIA', 'RECOGNITION_CRITERIA_NAME') AS 'criteria'
,    CAST((COUNT(DISTINCT nomination_id) / CAST(@sum_ind as float))
        * 100 AS NUMERIC(9,2)) AS 'criteria_per_individual'
,    CAST((COUNT(DISTINCT recognition_id) / CAST(@sum_recog as float))
        * 100 AS NUMERIC(9,2)) AS 'criteria_per_recognition'
,    CAST((SUM(recognition_amount) / CAST(@sum_awd as float))
        * 100 AS NUMERIC(9,2)) as 'issued_award_percentage'
FROM
    #recognition_criteria
GROUP BY
    recogniton_criteria_name

SELECT ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ') AS EMPLOYEE
FROM PAX
inner join PAX_GROUP ON PAX.PAX_ID = PAX_GROUP.PAX_ID
WHERE PAX_GROUP.PAX_GROUP_ID = @pax_group_id

SELECT
    @sum_ind AS 'sum_num_individuals'
,    @sum_recog AS 'sum_num_recognitions'
,    @sum_awd AS 'sum_award_points'

END

GO
