/****** Object:  StoredProcedure [component].[UP_ABS_DEMOGRAPHICS_UPDATE]    Script Date: 4/27/2020 8:25:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================  NAME  ===============================================
-- UP_ABS_DEMOGRAPHICS_UPDATE
-- ===========================  DESCRIPTION  ===========================================
-- Based on the accountHoldersFlag, fetches the participant records to perform demographic updates.
-- The project number and sub project number are defaulted based on the card type code
-- If the participant does not hold an account, the project number and sub project number are decided based on the
-- card type code which inturn depends on the country code.
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- VANTEDS        20130810                CREATED
-- HARWELLM        20150922                CHANGED SYS_USER.STATUS_TYPE_CODE FROM I TO INACTIVE
-- MUDDAM        20151113                UDM4 CHANGES
-- HARWELLM        20151221                TRIM ENROLLMENT FIELDS FOR ABS LENGTH REQUIREMENTS
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- GARCIAF2        20160816    MP-5458        CHANGE KEY_NAMES TO
--                                         demographicUpdates.processedDate
--                                         demographicupdates.accountholdersonly
--                                         demographicUpdates.defaultAwardType.US
--                                         demographicUpdates.defaultAwardType.nonUS
--                                         demographicupdates.'+ @usAwardType + '.defaultprojectnumber
--                                         demographicupdates.'+ @usAwardType + '.defaultsubprojectnumber
--                                         demographicupdates.'+ @nonUSAwardType + '.defaultprojectnumber'
--                                         demographicupdates.'+ @nonUSAwardType + '.defaultsubprojectnumber
--                                         demographicupdates.'+ [PAXACCOUNT.CARD_TYPE_CODE] + '.defaultprojectnumber
--                                         demographicupdates.'+ [PAXACCOUNT.CARD_TYPE_CODE] + '.defaultsubprojectnumber
-- KUMARSJ        20170814    MP-10804    Exclude RIDEAU Service Anniversary pax-accounts from this insert & update
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
-- MUDDAM        20171212    MP-11111    Added support for dynamic award types, rebuilt for clarity and performance
-- MUDDAM        20180219    MP-11719    Added pagination to let us avoid processing too many records at one time
--                                            Also, removed unused columns and added check to only return active card types
-- SHARPCA		20200424	CNXT-9		Limit first_name to 20 chars and last_name to 24 chars
--
-- ===========================  DECLARATIONS  ==========================================

ALTER PROCEDURE [component].[UP_ABS_DEMOGRAPHICS_UPDATE]
@PAGE_NUMBER INT,
@PAGE_SIZE INT
AS
BEGIN
    SET NOCOUNT ON

DECLARE @processedDate DATETIME2;
SELECT @processedDate = ISNULL((SELECT VALUE FROM component.APPLICATION_DATA WHERE KEY_NAME = 'demographicUpdates.processedDate'), '01/01/2015');

SELECT
    -- pax account creation details
    PAX_ACCOUNT_MATRIX.CARD_TYPE_CODE, -- this is the code we want to create
    PAX_ACCOUNT.CARD_NUM, -- this will be null if we haven't created one yet
    PAX.PAX_ID,
    PAX.CONTROL_NUM,
    PAX_ACCOUNT.USER_NAME,
    PAX_ACCOUNT.PASSWORD,
    PROJECT_NUMBER_LOOKUP.LOOKUP_DESC AS PROJECT_NUMBER,
    SUB_PROJECT_NUMBER_LOOKUP.LOOKUP_DESC AS SUB_PROJECT_NUMBER,
    -- Pax demographics base
    CASE
        WHEN LEN(PAX.LAST_NAME) > 24 THEN SUBSTRING(PAX.LAST_NAME, 1, 24)
        ELSE PAX.LAST_NAME
    END	AS LAST_NAME,
    CASE
        WHEN LEN(PAX.FIRST_NAME) > 20 THEN SUBSTRING(PAX.FIRST_NAME, 1, 20)
        ELSE PAX.FIRST_NAME
    END AS FIRST_NAME,
    PAX.MIDDLE_NAME,
    PAX.NAME_SUFFIX,
    PREFERRED_EMAIL.EMAIL_ADDRESS,
    -- Pax address details
    PREFERRED_ADDRESS.ADDRESS1,
    PREFERRED_ADDRESS.ADDRESS2,
    PREFERRED_ADDRESS.ADDRESS3,
    PREFERRED_ADDRESS.ADDRESS4,
    PREFERRED_ADDRESS.ADDRESS5,
    PREFERRED_ADDRESS.ADDRESS6,
    PREFERRED_ADDRESS.CITY,
    PREFERRED_ADDRESS.STATE,
    PREFERRED_ADDRESS.ZIP,
    PREFERRED_ADDRESS.COUNTRY_CODE,
    -- Pax phone details
    NIGHT_PHONE.PHONE AS PARTICIPANT_NIGHT_PHONE,
    FAX_PHONE.PHONE AS PARTICIPANT_FAX_PHONE,
    DAY_PHONE.PHONE AS PARTICIPANT_DAY_PHONE
FROM component.PAX
INNER JOIN component.SYS_USER
    ON SYS_USER.PAX_ID = PAX.PAX_ID
    AND SYS_USER.STATUS_TYPE_CODE != 'INACTIVE'
INNER JOIN component.PAX_GROUP
    ON PAX_GROUP.PAX_ID = PAX.PAX_ID
    AND PAX_GROUP.ORGANIZATION_CODE != 'MTZ'
    AND PAX_GROUP.THRU_DATE IS NULL
LEFT JOIN (
    SELECT
        PAX.PAX_ID,
        CARD_TYPE.CARD_TYPE_CODE
    FROM component.PAX
    CROSS JOIN component.CARD_TYPE
    LEFT JOIN component.PAX_ACCOUNT
        ON PAX.PAX_ID = PAX_ACCOUNT.PAX_ID
        AND CARD_TYPE.CARD_TYPE_CODE = PAX_ACCOUNT.CARD_TYPE_CODE
    WHERE CARD_TYPE.CARD_TYPE_CODE != 'RIDEAU'
    AND CARD_TYPE.DISPLAY_STATUS_TYPE_CODE = 'ACTIVE'
) PAX_ACCOUNT_MATRIX
    ON PAX_ACCOUNT_MATRIX.PAX_ID = PAX.PAX_ID
LEFT JOIN component.PAX_ACCOUNT
    ON PAX_ACCOUNT_MATRIX.CARD_TYPE_CODE = PAX_ACCOUNT.CARD_TYPE_CODE
    AND PAX_ACCOUNT_MATRIX.PAX_ID = PAX_ACCOUNT.PAX_ID
LEFT JOIN component.ADDRESS PREFERRED_ADDRESS
    ON PREFERRED_ADDRESS.PAX_ID = PAX.PAX_ID
    AND PREFERRED_ADDRESS.PREFERRED = 'Y'
LEFT JOIN component.PHONE DAY_PHONE
    ON DAY_PHONE.PAX_ID = PAX.PAX_ID
    AND DAY_PHONE.PHONE_TYPE_CODE = 'BUSINESS'
LEFT JOIN component.PHONE FAX_PHONE
    ON FAX_PHONE.PAX_ID = PAX.PAX_ID
    AND FAX_PHONE.PHONE_TYPE_CODE = 'BUSINESS_FAX'
LEFT JOIN component.PHONE NIGHT_PHONE
    ON NIGHT_PHONE.PAX_ID = PAX.PAX_ID
    AND NIGHT_PHONE.PHONE_TYPE_CODE = 'HOME'
LEFT JOIN component.EMAIL PREFERRED_EMAIL
    ON PREFERRED_EMAIL.PAX_ID = PAX.PAX_ID
    AND PREFERRED_EMAIL.PREFERRED = 'Y'
LEFT JOIN component.LOOKUP PROJECT_NUMBER_LOOKUP
    ON PAX_ACCOUNT_MATRIX.CARD_TYPE_CODE = PROJECT_NUMBER_LOOKUP.LOOKUP_CATEGORY_CODE
    AND PROJECT_NUMBER_LOOKUP.LOOKUP_CODE = 'PROJECT_NUMBER'
LEFT JOIN component.LOOKUP SUB_PROJECT_NUMBER_LOOKUP
    ON PAX_ACCOUNT_MATRIX.CARD_TYPE_CODE = SUB_PROJECT_NUMBER_LOOKUP.LOOKUP_CATEGORY_CODE
    AND SUB_PROJECT_NUMBER_LOOKUP.LOOKUP_CODE = 'SUB_PROJECT_NUMBER'
WHERE PAX_ACCOUNT.CARD_NUM IS NULL -- new records
    OR PAX_ACCOUNT.CREATE_DATE > @processedDate -- records created in a previous iteration, here to keep record total the same
    OR PAX.UPDATE_DATE > @processedDate -- rest of these are just demographics validation
    OR DAY_PHONE.UPDATE_DATE > @processedDate
    OR FAX_PHONE.UPDATE_DATE > @processedDate
    OR NIGHT_PHONE.UPDATE_DATE > @processedDate
    OR PREFERRED_ADDRESS.UPDATE_DATE > @processedDate
    OR PREFERRED_EMAIL.UPDATE_DATE > @processedDate
ORDER BY PAX.PAX_ID, PAX_ACCOUNT_MATRIX.CARD_TYPE_CODE
    OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS
    FETCH NEXT @PAGE_SIZE ROWS ONLY

END

