IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX' AND schema_id = SCHEMA_ID('component'))
BEGIN
exec ('create PROCEDURE component.UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX as BEGIN select 1 as c1 END')
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ===============================================
-- UP_GET_AWARD_TYPE_BY_PAYOUT_TYPE
-- ===========================  DESCRIPTION  ===========================================
-- Return all activity that the specified participant was either a giver or receiver of UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- Darryl Days   20210110    TIR-140      Add logging.
-- FLORESC       20210823    CAM-5        Added an extra value (ID) in the order by clause to guarantee same result set on every call.
--
-- ===========================  DECLARATIONS  ==========================================

ALTER PROCEDURE [component].UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX
    @pax_id BIGINT,
    @is_admin NVARCHAR(10),
    @audience NVARCHAR(MAX),
    @calendar_sub_types NVARCHAR(MAX),
    @start_date DATE,
    @end_date DATE,
    @start_page int,
    @page_size int
AS
BEGIN
SET NOCOUNT ON
BEGIN TRY

DECLARE @loggingStartDate  DATETIME2(7),
@loggingEndDate  DATETIME2(7),
@loggingParameters NVARCHAR(MAX),
@loggingSprocName nvarchar(150),
@loggingDebug int
---------------------------------------------- CHECKING LOGGING ENABLED
SELECT  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    IF (@loggingDebug=1)
BEGIN
SET @loggingSprocName = 'UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX'
SET @loggingStartDate = getdate()
SET @loggingParameters = '@pax_id=' + isnull(cast(@pax_id as varchar),'null') --BIGINT
+ ' :: @is_admin=' +isnull(@is_admin, 'null') -- NVARCHAR(10)
+ ' :: @audience=' + isnull(cast(@audience as varchar),'null') -- NVARCHAR(MAX)
+ ' :: @calendar_sub_types=' + isnull(cast(@calendar_sub_types as varchar),'null') -- NVARCHAR(MAX)
+ ' :: @start_date=' + isnull(cast(@start_date as varchar),'null') --DATE
+ ' :: @end_date=' + isnull(cast(@end_date as varchar),'null') --DATE
+ ' : :@start_page=' + isnull(cast(@start_page as varchar),'null') -- BIGINT
+ ' :: @page_size=' + isnull(cast(@page_size as varchar),'null') -- BIGINT
END

;WITH QUERY AS (
    SELECT
        DISTINCT CE.ID,
                 CE.CALENDAR_ENTRY_NAME,
                 CE.CALENDAR_ENTRY_DESC,
                 CE.FROM_DATE,
                 CE.THRU_DATE,
                 CE.TARGET_ID,
                 CST.CALENDAR_SUB_TYPE_CODE
    FROM
        component.CALENDAR_ENTRY CE
            INNER JOIN component.CALENDAR_SUB_TYPE CST ON
                CE.CALENDAR_SUB_TYPE_ID = CST.ID
            LEFT JOIN component.PAX_MISC PM ON
                    CE.TARGET_ID = PM.PAX_ID
                AND CE.TARGET_TABLE = 'PAX'
                AND PM.VF_NAME IN ('SHARE_SA','SHARE_BDAY')
            LEFT JOIN component.RELATIONSHIP R ON
                    CE.TARGET_ID = R.PAX_ID_1
                AND CE.TARGET_TABLE = 'PAX'
                AND R.RELATIONSHIP_TYPE_CODE = 'REPORT_TO'
            LEFT JOIN component.SYS_USER SU ON
                    SU.PAX_ID = CE.TARGET_ID
                AND CE.TARGET_TABLE = 'PAX'
                AND SU.STATUS_TYPE_CODE <> 'INACTIVE'
            LEFT JOIN component.PAX_MISC PMHD ON
                    PMHD.PAX_ID = SU.PAX_ID
                AND PMHD.VF_NAME = 'HIRE_DATE'
            LEFT JOIN component.ACL AGP ON
                    AGP.TARGET_TABLE = 'CALENDAR'
                AND AGP.SUBJECT_TABLE = 'GROUPS'
                AND CE.ID = AGP.TARGET_ID
            LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GM ON
                    AGP.SUBJECT_ID = GM.GROUP_ID
                AND GM.PAX_ID = @pax_id
            LEFT JOIN component.ACL APX ON
                    APX.TARGET_TABLE = 'CALENDAR'
                AND APX.SUBJECT_TABLE = 'PAX'
                AND CE.ID = APX.TARGET_ID
            LEFT JOIN component.RELATIONSHIP R_NETWORK ON
                    R_NETWORK.PAX_ID_1 = @pax_id
                AND R_NETWORK.RELATIONSHIP_TYPE_CODE = 'FRIENDS'
                AND ( R_NETWORK.PAX_ID_2 = APX.SUBJECT_ID
                OR (R_NETWORK.PAX_ID_2 = CE.TARGET_ID
                    AND CE.TARGET_TABLE = 'PAX') )
    WHERE
        (
                (
                            CST.CALENDAR_SUB_TYPE_CODE = 'SERVICE_ANNIVERSARY'
                        AND (SU.PAX_ID <> NULL OR SU.STATUS_TYPE_CODE <> 'INACTIVE')
                        AND DATEDIFF(year,CAST(PMHD.MISC_DATA AS DATE), GETDATE())>0
                        AND (PM.VF_NAME IS NULL OR
                             (PM.VF_NAME = 'SHARE_SA' AND (PM.MISC_DATA = 'TRUE'
                                 OR (PM.MISC_DATA = 'FALSE'
                                     AND (R.PAX_ID_2 = @pax_id
                                         OR (CE.TARGET_ID = @pax_id
                                             AND CE.TARGET_TABLE = 'PAX')) ) ) ) )
                    )
                OR
                (
                            CST.CALENDAR_SUB_TYPE_CODE = 'BIRTHDAY'
                        AND (SU.PAX_ID <> NULL OR SU.STATUS_TYPE_CODE <> 'INACTIVE')
                        AND (PM.VF_NAME IS NULL	OR (PM.VF_NAME = 'SHARE_BDAY'
                        AND (PM.MISC_DATA = 'TRUE'
                            OR (PM.MISC_DATA = 'FALSE'
                                AND (R.PAX_ID_2 = @pax_id
                                    OR (CE.TARGET_ID = @pax_id
                                        AND CE.TARGET_TABLE = 'PAX')) ) ) ) )
                    )
                OR CST.CALENDAR_SUB_TYPE_CODE NOT IN ('SERVICE_ANNIVERSARY','BIRTHDAY')
            )
      AND ('TRUE' = @is_admin
        OR ('FALSE' = @is_admin
            AND (GM.PAX_ID = @pax_id
                OR APX.SUBJECT_ID = @pax_id
                OR R_NETWORK.PAX_ID_2 IS NOT NULL
                OR CST.CALENDAR_SUB_TYPE_CODE <> 'CUSTOM') ))
      AND CST.DISPLAY_STATUS_TYPE_CODE = 'ACTIVE'
      AND ('ALL' = @audience
        OR (
                   ('NETWORK' = @audience OR 'ME' = @audience)
                   AND ( (CE.TARGET_ID = @pax_id AND CE.TARGET_TABLE = 'PAX')
                   OR GM.PAX_ID = @pax_id OR APX.SUBJECT_ID = @pax_id
                   OR (R_NETWORK.PAX_ID_2 IS NOT NULL
                       AND 'NETWORK' = @audience) )))

      AND ((@start_date IS NOT NULL AND @end_date IS NULL AND (CE.FROM_DATE >= @start_date OR (CE.FROM_DATE <= @start_date AND CE.THRU_DATE >= @start_date)))
        OR ((@start_date IS NOT NULL AND @end_date IS NOT NULL) OR (CE.FROM_DATE BETWEEN @start_date AND @end_date
            OR CE.THRU_DATE BETWEEN @start_date AND @end_date
            OR ( CE.FROM_DATE <= @start_date
                AND CE.THRU_DATE >= @start_date) )))


      AND (@calendar_sub_types IS NULL OR LEN(RTRIM(LTRIM(@calendar_sub_types))) = 0
        OR  @calendar_sub_types LIKE '%'+ CST.CALENDAR_SUB_TYPE_CODE + '%')
)
 SELECT * FROM QUERY
                   CROSS JOIN (
    SELECT
        COUNT(*) AS TOTAL_RESULTS
    FROM
        QUERY) AS TOTAL_RESULTS
 ORDER BY
     FROM_DATE, ID desc
 OFFSET @start_page ROWS FETCH NEXT @page_size ROWS ONLY;

--(+)Logging
if (@loggingDebug=1)
begin
set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error
	 --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging

END
