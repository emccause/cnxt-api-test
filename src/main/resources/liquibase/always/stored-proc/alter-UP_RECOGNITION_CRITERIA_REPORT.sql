

-- ==============================  NAME  ======================================
-- UP_RECOGNITION_CRITERIA_REPORT
-- ===========================  DESCRIPTION  ==================================
-- RETURN THE SUM OF INDIVIDUALS, CRITERIA AND RECOGNITION
--
-- BASED ON THE GIVEN PARAMETERS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- PALD00        20081020                CREATED
-- SCHIERVR        20081030                MADE MORE DYNAMIC TO ASSIST IN CSV CREATION.
-- PALD00        20081112                MADE CHANGES TO GET ALL THE DATA FROM ONE PROCEDURE AS REQUIRED BY BIRT REPORT
-- CHIDRIS        20130328                ADDED LOCALE CODE
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814                Updates for UDM 3
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RECOGNITION_CRITERIA_REPORT]
    @submittal_date_from DATETIME2
,    @submittal_date_thru DATETIME2
,    @pax_group_id BIGINT
,    @recognition_selection_criteria NVARCHAR(4)
,   @locale_code NVARCHAR(50)
AS
BEGIN

SET NOCOUNT ON

--DRIVER
CREATE TABLE #recognition_criteria (
    nomination_id BIGINT NOT NULL
,    recognition_id BIGINT NOT NULL
,    submitter_pax_id BIGINT NULL
,    receiver_pax_id BIGINT NULL
,    recogniton_criteria_id BIGINT
,    recogniton_criteria_name NVARCHAR(200)
,    recognition_amount INT
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

EXECUTE component.UP_RECOGNITION_CRITERIA_BASE @submittal_date_from, @submittal_date_thru, @pax_group_id, @recognition_selection_criteria

CREATE TABLE #recognition_criteria_sum (
    recognition_count FLOAT
,    recogniton_criteria_name NVARCHAR(200)
,    recognition_total FLOAT
,    nomination_count FLOAT
,    nomination_total FLOAT
,    award_count FLOAT
,    award_sum FLOAT
,    receivers_total FLOAT
)

INSERT INTO #recognition_criteria_sum (    recognition_count 
,    nomination_count
,    award_count
,    recogniton_criteria_name
) SELECT
    COUNT(DISTINCT recognition_id) 'Number of Recognitions'
,    COUNT(DISTINCT nomination_id) 'Number of Nominations'
,    SUM(recognition_amount) AS 'Award Points per Criteria'
,    recogniton_criteria_name AS 'Criteria'
FROM
    #recognition_criteria
GROUP BY
    recogniton_criteria_name

UPDATE #recognition_criteria_sum SET recognition_total = (SELECT
    COUNT(DISTINCT recognition_id) AS 'Sum_Recognitions'
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria )

UPDATE #recognition_criteria_sum SET nomination_total = (SELECT
    COUNT(DISTINCT nomination_id) AS 'Sum_Nominations'
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria )

UPDATE #recognition_criteria_sum SET award_sum = (SELECT
    SUM(recognition_amount)
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria )

UPDATE #recognition_criteria_sum SET receivers_total = (SELECT
        COUNT(DISTINCT receiver_pax_id) AS 'Sum_Receivers'
FROM (
    select distinct nomination_id, recognition_id, receiver_pax_id,
        submitter_pax_id, recognition_amount
    from #recognition_criteria
) as recognition_criteria )

-- SELECT THE FINAL RESULT SET
SELECT
    recognition_count  AS  'Number of Recognitions'
,    nomination_count AS 'Number of Individuals'
,    award_count  AS 'Award Points per Criteria'
,    component.UF_TRANSLATE (@locale_code, recogniton_criteria_name, 'RECOGNITION_CRITERIA', 'RECOGNITION_CRITERIA_NAME') AS 'Criteria'
,    recognition_total  AS 'Sum_Recognitions'
,    nomination_total  AS 'Sum_Nominations'
,    award_sum  AS 'Sum_Awards'
,    receivers_total  AS 'Sum_Receivers'
FROM
    #recognition_criteria_sum

END
GO