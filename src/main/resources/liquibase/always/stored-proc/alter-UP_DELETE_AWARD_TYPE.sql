SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ===============================================
-- UP_DELETE_AWARD_TYPE
-- ===========================  DESCRIPTION  ===========================================
-- Delete Award Type
-- ============================  REVISIONS  ============================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- GARCIAF2        20171101    MP-11050    CREATED
-- HARWELLM        20180116    MP-11547    DELETE CARD_TYPE AFTER PAYOUT_ITEM TO AVOID FK ERRORS
-- HARWELLM        20180119    MP-11585    DELETE TRANSLATABLE_PHRASE RECORDS
--
-- ===========================  DECLARATIONS  ==========================================
ALTER PROCEDURE component.UP_DELETE_AWARD_TYPE
  @payoutItemId BIGINT

AS 
BEGIN    
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON

DECLARE @intErrorCode INT

DECLARE @payoutTypeCode nvarchar(50)
SET @payoutTypeCode = ( SELECT PAYOUT_ITEM_NAME FROM component.PAYOUT_ITEM
                        WHERE PAYOUT_ITEM_ID = @payoutItemId )

    BEGIN TRANSACTION
        
        DELETE tp
        FROM component.LOOKUP l
        JOIN component.TRANSLATABLE_PHRASE tp ON tp.TARGET_ID = l.ID
        JOIN component.TRANSLATABLE t ON t.ID = tp.TRANSLATABLE_ID
        WHERE l.LOOKUP_TYPE_CODE = 'AWARD_TYPE'
        AND l.LOOKUP_CATEGORY_CODE = @payoutTypeCode
        AND l.LOOKUP_CODE IN ('DISPLAY_NAME', 'LONG_DESC')
        AND t.TABLE_NAME = 'LOOKUP' AND t.COLUMN_NAME = 'LOOKUP_DESC'
        
        DELETE FROM component.LOOKUP
        WHERE LOOKUP_TYPE_CODE = 'AWARD_TYPE'
        AND LOOKUP_CATEGORY_CODE = @payoutTypeCode

        DELETE FROM component.LOOKUP_CATEGORY
         WHERE LOOKUP_CATEGORY_CODE  = @payoutTypeCode

        DELETE FROM component.EARNINGS_TYPE
        WHERE EARNINGS_TYPE_CODE = @payoutTypeCode
      
        DELETE FROM component.PAYOUT_TYPE
        WHERE PAYOUT_TYPE_CODE = @payoutTypeCode

        DELETE FROM component.PAYOUT_ITEM
        WHERE PAYOUT_ITEM_NAME  = @payoutTypeCode
        
        IF EXISTS ( SELECT * FROM component.CARD_TYPE WHERE CARD_TYPE_CODE = @payoutTypeCode )
          BEGIN
            DELETE FROM component.CARD_TYPE 
            WHERE CARD_TYPE_CODE = @payoutTypeCode
          END
                    
        SELECT @intErrorCode = @@ERROR
        IF (@intErrorCode <> 0) GOTO ERROR_DELETING_AWARD_TYPES --if errors, jump to roll back the transaction

        COMMIT TRANSACTION
        SELECT @payoutItemId AS 'PAYOUT_ITEM_ID', @intErrorCode AS 'ERROR_CODE'

        ERROR_DELETING_AWARD_TYPES:
        IF (@intErrorCode <> 0) 
        BEGIN
            PRINT 'Unexpected error occurred!'
            ROLLBACK TRANSACTION
        END

END --SP
GO