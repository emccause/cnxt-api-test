/****** Object:  StoredProcedure [component].[UP_BUDGET_DETAILS]    Script Date: 10/21/2020 11:35:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- UP_BUDGET_DETAILS
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, FROM_DATE AND THRU_DATE
-- RETURN A RESULT SET OF NOMINATIONS / RECOGNITIONS GIVEN BY THE PAX
-- ASSOCIATED WITH THE GIVEN PAX_GROUP_ID.  THIS PROCEDURE MUST BE CALLED BY
-- ANOTHER PROCEDURE THAT CREATES THE #NOMINATION TEMP TABLE THAT THIS PROCEDURE
-- POPULATES.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR	DATE		STORY		CHANGE DESCRIPTION
-- VANTEDS      20120501  			CREATED
-- CHIDRIS    	20130328  			ADDED LOCALE CODE
-- DOHOGNTA   	20150309  			CHANGE IDENTITIES TO BIGINT
-- burgestl	20201103			Update join to sql 2017
-- hernancl		 20210601	            update join to sql 2017
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_BUDGET_DETAILS]
  @submittal_date_from DATETIME2
, @submittal_date_thru DATETIME2
, @budget_id BIGINT
, @program_activity_id BIGINT
, @locale_code NVARCHAR(6)
AS
BEGIN

SET NOCOUNT ON

-- DRIVER
CREATE TABLE #nomination (
  nomination_id BIGINT NOT NULL
, nomination_status NVARCHAR(10) NULL

, submittal_date DATETIME2 NULL
, submitter_list NVARCHAR(1000) NULL
, submitter_name NVARCHAR(80) NULL

, recognition_id  BIGINT NULL
, total_points INT NULL
, total_points_pending INT NULL
, total_points_denied INT NULL
, total_points_approved INT NULL

, receiver_list NVARCHAR(1000) NULL
, receiver_name NVARCHAR(80) NULL

, approval_process_id BIGINT NULL
, approval_notes NVARCHAR(MAX) NULL
, approval_count INT NULL

, approver_count INT NULL
, approver_name NVARCHAR(80)

, last_approval_date DATETIME2 NULL

, comment NVARCHAR(MAX) NULL
, comment_type NCHAR(1) NULL
, justification_comment_available_flag INT NULL DEFAULT 0
, justification_flag INT NULL DEFAULT 0

, criteria NVARCHAR(1024)

, program_activity_id BIGINT NULL
, program_activity_name NVARCHAR(40) NULL
, award_type NVARCHAR(40) NULL
, budget_id BIGINT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- GET THE NOMINATION DATA.  #nomination is created by the calling stored procedure.
INSERT INTO #nomination (
  nomination_id
, nomination_status
, submittal_date
, submitter_list
, receiver_list
, comment_type
, justification_comment_available_flag
, criteria
, program_activity_id
, budget_id
)
SELECT
  NOMINATION.ID
, NOMINATION.STATUS_TYPE_CODE
, NOMINATION.SUBMITTAL_DATE
, component.UF_RECOGNITION_FETCH_LIST_SUBMITTER_PAX_BY_NOMINATION(NOMINATION.ID)
, component.UF_RECOGNITION_FETCH_LIST_RECEIVER_PAX_BY_NOMINATION(NOMINATION.ID)
, CASE
    WHEN NOMINATION.ECARD_ID IS NULL THEN 'C'
    ELSE 'E'
  END
, CASE WHEN NOMINATION.COMMENT_JUSTIFICATION IS NULL THEN 0 ELSE 1 END
, component.UF_RECOGNITION_FETCH_LIST_RECOGNITION_CRITERIA_NAME_BY_NOMINATION(NOMINATION.ID, @locale_code)
, NOMINATION.PROGRAM_ACTIVITY_ID
, NOMINATION.BUDGET_ID
FROM  NOMINATION
INNER JOIN RECOGNITION_BUDGET ON
RECOGNITION_BUDGET.ID = NOMINATION.BUDGET_ID
WHERE NOMINATION.SUBMITTAL_DATE BETWEEN @submittal_date_from AND @submittal_date_thru
AND NOMINATION.STATUS_TYPE_CODE IN ('P', 'A', 'R', 'F')
AND RECOGNITION_BUDGET.ID = @budget_id
AND NOMINATION.STATUS_TYPE_CODE IS NOT NULL 
AND (@program_activity_id = '-1' OR NOMINATION.PROGRAM_ACTIVITY_ID = @program_activity_id)

UPDATE  #nomination
SET #nomination.justification_flag = 1
FROM  PROGRAM_ACTIVITY_MISC
WHERE #nomination.PROGRAM_ACTIVITY_ID = PROGRAM_ACTIVITY_MISC.PROGRAM_ACTIVITY_ID
AND PROGRAM_ACTIVITY_MISC.VF_NAME = 'ENABLE_JUSTIFICATION'
AND PROGRAM_ACTIVITY_MISC.MISC_DATA IN ('Y', 'O')
AND GETDATE() BETWEEN PROGRAM_ACTIVITY_MISC.FROM_DATE AND ISNULL(THRU_DATE, GETDATE())

UPDATE  #nomination
SET receiver_name = (
  SELECT TOP 1 ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
  FROM  RECOGNITION
  INNER JOIN  PAX ON RECOGNITION.RECEIVER_PAX_ID = PAX.PAX_ID
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE  #nomination
SET last_approval_date = (
  SELECT  MAX(APPROVAL_HISTORY.APPROVAL_DATE)
  FROM  RECOGNITION
  INNER JOIN APPROVAL_HISTORY ON RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE  #nomination
SET approval_process_id = APPROVAL_PROCESS.ID
FROM  #nomination
INNER JOIN APPROVAL_PROCESS
ON #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID
AND #nomination.PROGRAM_ACTIVITY_ID = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID

UPDATE  #nomination
SET approval_count = (
  SELECT  COUNT(1) 
  FROM  (
    SELECT APPROVAL_PENDING.ID AS ID
    FROM  RECOGNITION
    INNER JOIN APPROVAL_PENDING ON RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
    WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
    AND RECOGNITION.STATUS IN ('P','A','R')
    UNION
    SELECT  APPROVAL_HISTORY.ID AS ID
    FROM  RECOGNITION
    INNER JOIN APPROVAL_HISTORY ON RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
    WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
    AND APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
    AND RECOGNITION.STATUS IN ('P','A','R')
  ) AS APPROVALS
)

UPDATE  #nomination
SET submitter_name = ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
FROM  #nomination
INNER JOIN NOMINATION ON #nomination.nomination_id = NOMINATION.ID
INNER JOIN  PAX ON PAX.PAX_ID = NOMINATION.SUBMITTER_PAX_ID

UPDATE  #nomination
SET program_activity_name = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_NAME
FROM  nomination
, PROGRAM_ACTIVITY
WHERE #nomination.program_activity_id = PROGRAM_ACTIVITY.PROGRAM_ACTIVITY_ID

-- Get the total approved, denied or pending award amount for each nomination
UPDATE  #nomination
SET total_points = (
  SELECT  
    CASE
      WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
      ELSE NULL
    END
  FROM  RECOGNITION
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND ISNULL(RECOGNITION.AMOUNT, 0) >0
  AND RECOGNITION.STATUS IN ('P', 'A', 'R')
)

-- Get the total approved points for each nomination
UPDATE  #nomination
SET total_points_approved = (
  SELECT
    CASE
      WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
      ELSE NULL
    END

  FROM  RECOGNITION
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND ISNULL(RECOGNITION.AMOUNT,0) >0 
  AND RECOGNITION.STATUS = 'A'
)

-- Get the total denied points for each nomination
UPDATE  #nomination
SET total_points_denied = (
  SELECT  
    CASE
      WHEN SUM(RECOGNITION.AMOUNT) > 0 THEN SUM(RECOGNITION.AMOUNT)
      ELSE NULL
    END

  FROM  RECOGNITION
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND ISNULL(RECOGNITION.AMOUNT,0) > 0
  AND RECOGNITION.STATUS = 'R'
)

-- Get the total pending points for each nomination
UPDATE  #nomination
SET total_points_pending = (
  SELECT  
    CASE
      WHEN SUM(RECOGNITION.AMOUNT) >0 THEN SUM(RECOGNITION.AMOUNT)
      ELSE NULL
    END
  FROM  RECOGNITION
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND ISNULL(RECOGNITION.AMOUNT,0) > 0
  AND RECOGNITION.STATUS = 'P'
)

UPDATE  #nomination
SET recognition_id = (
  SELECT  TOP 1 RECOGNITION.ID
  FROM  RECOGNITION
  WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
  AND RECOGNITION.STATUS IN ('P','A','R')
)

UPDATE  #nomination
SET approval_notes = APPROVAL_HISTORY.APPROVAL_NOTES
FROM  #nomination
, RECOGNITION
, APPROVAL_HISTORY
WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
AND RECOGNITION.ID = APPROVAL_HISTORY.TARGET_SUB_ID
AND APPROVAL_HISTORY.APPROVAL_PROCESS_ID = #nomination.approval_process_id
AND #nomination.nomination_status = 'R'
AND RECOGNITION.STATUS = 'R'

UPDATE  #nomination 
SET approver_count = (
  SELECT  COUNT(1)
  FROM  RECOGNITION
  , APPROVAL_PROCESS
  , APPROVAL_PENDING
  WHERE RECOGNITION.NOMINATION_ID = #nomination.nomination_id
  AND #nomination.nomination_id = APPROVAL_PROCESS.TARGET_ID 
  AND #nomination.PROGRAM_ACTIVITY_ID = APPROVAL_PROCESS.PROGRAM_ACTIVITY_ID 
  AND APPROVAL_PROCESS.ID = APPROVAL_PENDING.APPROVAL_PROCESS_ID 
  AND RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
  AND RECOGNITION.STATUS IN ('P','A','R')
)
  
-- Do miscellanous rules so the report does not have to
UPDATE  #nomination
SET last_approval_date = NULL
WHERE nomination_status = 'P'

UPDATE  #nomination
SET last_approval_date = submittal_date
WHERE approval_count = 0
AND nomination_status <> 'P'

UPDATE  #nomination
SET approver_name = (
  SELECT
    CASE
      WHEN approver_count > 1 THEN '>1'
      ELSE (  SELECT  ISNULL(PAX.FIRST_NAME, ' ') + ' ' + ISNULL(PAX.LAST_NAME, ' ')
        FROM  PAX
        INNER JOIN APPROVAL_PENDING ON APPROVAL_PENDING.PAX_ID = PAX.PAX_ID
        INNER JOIN RECOGNITION ON RECOGNITION.ID = APPROVAL_PENDING.TARGET_SUB_ID
        WHERE #nomination.nomination_id = RECOGNITION.NOMINATION_ID
        AND APPROVAL_PENDING.APPROVAL_PROCESS_ID = #nomination.approval_process_id
        AND RECOGNITION.STATUS IN ('P','A','R'))
    END
)

UPDATE  #nomination
SET submitter_name = 
  CASE
    WHEN #nomination.submitter_list LIKE '%,%' THEN '>1'
    ELSE #nomination.submitter_name
  END

UPDATE  #nomination
SET receiver_name =
  CASE
    WHEN #nomination.receiver_list LIKE '%,%' THEN '>1'
    ELSE #nomination.receiver_name
  END

UPDATE  #nomination
SET comment_type =
  CASE
    WHEN #nomination.receiver_list LIKE '%,%' AND #nomination.comment_type = 'E' THEN 'M'
    ELSE #nomination.comment_type
  END
  
-- Update the award type
UPDATE  #nomination
SET award_type = CASE 
       WHEN PI.PAYOUT_ITEM_DESC IS NULL THEN 
          CASE      
            WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
            THEN 'Points'
          ELSE PI.PAYOUT_ITEM_NAME
          END
       ELSE
         CASE
          WHEN PI.PAYOUT_ITEM_NAME = 'Dynamic US/Global Points'
            THEN 'Points'
          ELSE PI.PAYOUT_ITEM_DESC
         END
      END 
  FROM  PAYOUT_ITEM PI,
        RECOGNITION_BUDGET RB,
        #nomination n 
    WHERE RB.ID  = n.budget_id
  AND PI.PAYOUT_ITEM_NAME = RB.AWARD_TYPE

SELECT
  CONVERT(nvarchar, submittal_date, 101) AS submittal_date
, receiver_name
, comment_type
, nomination_status
, approver_name
, CONVERT(nvarchar, last_approval_date, 101) AS last_approval_date
, submitter_name
, program_activity_name
, criteria
, total_points
, award_type
, nomination_id
, total_points_pending
, total_points_denied
, total_points_approved
, nomination_id
, recognition_id
FROM  #nomination
ORDER BY
   submittal_date DESC

END

GO
