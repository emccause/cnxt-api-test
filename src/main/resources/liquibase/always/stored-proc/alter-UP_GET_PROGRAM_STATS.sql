GO

/****** Object:  StoredProcedure [component].[UP_GET_PROGRAM_STATS]    Script Date: 3/25/2020 10:54:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_GET_PROGRAMS_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- GETS THE ACTIVITIES FOR RECOGNITIONS GIVEN AND RECEIVED FOR EACH
-- ACTIVITY INCLUDING RECORD, PERCENTAGES, POINT INFORMATION. FILTERS
-- BASED ON CREATION DATE RANGE AND GROUPINGS.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- ZINSERRJ        20160222                MODIFYING TOTAL-GIVERS AND RECEIVERS TO USE FILTER.
-- YELLAMS        20160223                Added AUTO_APPROVED status code to recognitions.
-- ZINSERRJ        20160310                MP6340 include points upload in total given
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- GARCIAF2        20170613    MP-9773        PERFORMANCE IMPROVEMENT
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
-- burgestl			20200109				Add no locks
-- DDAYS          20210108   ADD Logging
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GET_PROGRAM_STATS]
  @programIds NVARCHAR(MAX),
  @userIds NVARCHAR(MAX),
  @startDate DATETIME2,
  @endDate DATETIME2
AS
BEGIN

  SET NOCOUNT ON

BEGIN TRY

DECLARE @loggingStartDate  DATETIME2(7),
@loggingEndDate  DATETIME2(7),
@loggingParameters NVARCHAR(MAX),
@loggingSprocName nvarchar(150),
@loggingDebug int
---------------------------------------------- CHECKING LOGGING ENABLED
SELECT  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    IF (@loggingDebug=1)
BEGIN
SET @loggingSprocName = 'UP_GET_PROGRAM_STATS'
SET @loggingStartDate = getdate()
SET @loggingParameters = '@programIds=' + isnull(cast(@programIds as varchar),'null') --BIGINT
        + ' :: @userIds=' +isnull(@userIds, 'null') -- NVARCHAR(MAX)
        + ' :: @startDate=' +isnull(cast(@startDate as nvarchar(25)) , 'null') -- NVARCHAR(MAX)
        + ' :: @endDate=' +isnull(cast(@endDate as nvarchar(25)), 'null') -- NVARCHAR(MAX)
END

DECLARE @PROGRAMLIST XML
SELECT @PROGRAMLIST = CAST('<A>'+ REPLACE(@programIds,',','</A><A>')+ '</A>' AS XML)

DECLARE @USERLIST XML
SELECT @USERLIST = CAST('<A>'+ REPLACE(@userIds,',','</A><A>')+ '</A>' AS XML)

--Temp variables for stat calculation
DECLARE @TOTAL_GIVERS DECIMAL(16, 0),
          @TOTAL_RECEIVERS DECIMAL(16, 0),
          @TEMP_IDENTIFIER INT

  SET @TEMP_IDENTIFIER = 1

  --Table for program stats (Result table)
CREATE TABLE #PROGRAM_STATS (
                                IDENTIFIER BIGINT,
                                RECOGNITIONS_GIVEN BIGINT,
                                RECOGNITIONS_RECEIVED BIGINT,
                                PERCENT_ELIGIBLE_USERS_GIVING DECIMAL(16, 13),
                                PERCENT_ELIGIBLE_USERS_RECEIVING DECIMAL(16, 13),
                                POINTS_GIVEN BIGINT,
                                POINTS_RECEIVED BIGINT
)

    -- Get the total amount of givers and receivers (for nomination total calculation)
    -- Retrieves the pax IDs stored directly as well as the pax IDs associated with the group IDs that are stored directly
    -- and gets the distinct pax IDs of the union of the previous results

    SET @TOTAL_GIVERS = (
    SELECT ISNULL(COUNT(DISTINCT result.SUBJECT_ID), 0)
    FROM (
           (SELECT SUBJECT_ID
            FROM ACL acl with (nolock)
              JOIN ROLE role with (nolock)
                ON acl.ROLE_ID = role.ROLE_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
                ON PROGRAM_LIST.items = acl.TARGET_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
                  ON USER_LIST.items = acl.SUBJECT_ID
            WHERE acl.TARGET_TABLE = 'PROGRAM'
                  AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
                  AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
                  AND acl.SUBJECT_TABLE = 'PAX'
                  AND role.ROLE_CODE = 'PGIV')
           UNION ALL
           (SELECT groups_pax.PAX_ID as 'SUBJECT_ID'
            FROM groups_pax groups_pax with (nolock)
              JOIN groups groups with (nolock)
                ON groups.GROUP_ID = groups_pax.GROUP_ID
              JOIN acl acl with (nolock)
                ON acl.SUBJECT_ID = groups.GROUP_ID
              JOIN role role with (nolock)
                ON acl.ROLE_ID = role.ROLE_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
                  ON PROGRAM_LIST.items = TARGET_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
                ON USER_LIST.items = groups_pax.PAX_ID
            WHERE acl.TARGET_TABLE = 'PROGRAM'
                AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
                AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
                AND acl.SUBJECT_TABLE = 'GROUPS'
                AND role.ROLE_CODE = 'PGIV')
         ) result
  )

  SET @TOTAL_RECEIVERS = (
    SELECT ISNULL(COUNT(DISTINCT result.SUBJECT_ID), 0)
    FROM (
           (SELECT SUBJECT_ID
            FROM ACL acl with (nolock)
              JOIN ROLE role with (nolock)
                ON acl.ROLE_ID = role.ROLE_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
                  ON PROGRAM_LIST.items = acl.TARGET_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
                  ON USER_LIST.items = acl.SUBJECT_ID
            WHERE acl.TARGET_TABLE = 'PROGRAM'
                  AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
                  AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
                  AND acl.SUBJECT_TABLE = 'PAX'
                  AND role.ROLE_CODE = 'PREC')
           UNION ALL
           (SELECT groups_pax.PAX_ID as 'SUBJECT_ID'
            FROM groups_pax groups_pax with (nolock)
              JOIN groups groups with (nolock)
                ON groups.GROUP_ID = groups_pax.GROUP_ID
              JOIN acl acl
                ON acl.SUBJECT_ID = groups.GROUP_ID
              JOIN role role
                ON acl.ROLE_ID = role.ROLE_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
                 ON PROGRAM_LIST.items = TARGET_ID
              LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
                ON USER_LIST.items = groups_pax.PAX_ID
            WHERE acl.TARGET_TABLE = 'PROGRAM'
                  AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
                  AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
                  AND acl.SUBJECT_TABLE = 'GROUPS'
                  AND role.ROLE_CODE = 'PREC')
         ) result
  )

  -- Determine number of givers and receivers for specific program or all programs (depending if programId was given)

  --Insert giver information into temp table
  INSERT INTO #PROGRAM_STATS (
    IDENTIFIER,
    RECOGNITIONS_GIVEN,
    PERCENT_ELIGIBLE_USERS_GIVING
  )
SELECT @TEMP_IDENTIFIER,
       COUNT(DISTINCT recognition.ID),
       CASE @TOTAL_GIVERS
           WHEN 0 THEN 0
           ELSE COUNT(DISTINCT nomination.SUBMITTER_PAX_ID) / @TOTAL_GIVERS
           END
FROM NOMINATION nomination with (nolock)
        JOIN RECOGNITION recognition with (nolock)
ON nomination.ID=recognition.NOMINATION_ID
    LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
    ON PROGRAM_LIST.items = nomination.PROGRAM_ID
    LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
    ON USER_LIST.items = nomination.SUBMITTER_PAX_ID
WHERE recognition.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED')
  AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
  AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
  AND nomination.SUBMITTAL_DATE BETWEEN @startDate AND @endDate

--Then, insert receiver information into temp table
UPDATE #PROGRAM_STATS
SET RECOGNITIONS_RECEIVED = results.recognitions_received,
    PERCENT_ELIGIBLE_USERS_RECEIVING = results.percent_receiving
    FROM (
         SELECT COUNT(DISTINCT recognition.ID) as recognitions_received,
                (CASE @TOTAL_RECEIVERS
                 WHEN 0 THEN 0
                 ELSE COUNT(DISTINCT recognition.RECEIVER_PAX_ID) / @TOTAL_RECEIVERS
                 END) as percent_receiving
         FROM NOMINATION nomination with (nolock)
            JOIN RECOGNITION recognition with (nolock)
                ON nomination.ID = recognition.NOMINATION_ID
            LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
                ON PROGRAM_LIST.items = nomination.PROGRAM_ID
            LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
                ON USER_LIST.items = recognition.RECEIVER_PAX_ID
         WHERE recognition.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED')
               AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
               AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
               AND nomination.SUBMITTAL_DATE BETWEEN @startDate AND @endDate
       ) results
WHERE #PROGRAM_STATS.IDENTIFIER = @TEMP_IDENTIFIER

--Finally update the points
UPDATE #PROGRAM_STATS
SET POINTS_GIVEN = results.points_given
    FROM (
         SELECT ISNULL(SUM(ISNULL(payout.PAYOUT_AMOUNT,0)), 0) as points_given
         FROM PAYOUT payout with (nolock)
           JOIN EARNINGS_PAYOUT earnings_payout with (nolock)
             ON payout.PAYOUT_ID = earnings_payout.PAYOUT_ID
           JOIN EARNINGS earnings with (nolock)
             ON earnings.EARNINGS_ID = earnings_payout.EARNINGS_ID
           JOIN TRANSACTION_HEADER_EARNINGS transaction_header_earnings  with (nolock)
             ON earnings.EARNINGS_ID = transaction_header_earnings.EARNINGS_ID
           JOIN TRANSACTION_HEADER transaction_header  with (nolock)
             ON transaction_header.ID = transaction_header_earnings.TRANSACTION_HEADER_ID
           JOIN DISCRETIONARY discretionary  with (nolock)
             ON transaction_header.ID = discretionary.TRANSACTION_HEADER_ID
           LEFT JOIN RECOGNITION recognition  with (nolock)
             ON discretionary.TARGET_ID = recognition.ID AND discretionary.TARGET_TABLE = 'RECOGNITION'
           LEFT JOIN NOMINATION nomination with (nolock)
             ON nomination.ID = recognition.NOMINATION_ID
           LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
             ON PROGRAM_LIST.items = transaction_header.PROGRAM_ID
           LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
             ON USER_LIST.items = nomination.SUBMITTER_PAX_ID
         WHERE payout.PAYOUT_DATE BETWEEN @startDate AND @endDate
               AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
               AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
               AND payout.STATUS_TYPE_CODE = 'ISSUED'
               AND transaction_header.SUB_TYPE_CODE <> 'FUND'
       ) results
WHERE #PROGRAM_STATS.IDENTIFIER = @TEMP_IDENTIFIER

--Then insert information about points received into the temp table (same joining as points issued, different status filter)
UPDATE #PROGRAM_STATS
SET POINTS_RECEIVED = results.points_received
    FROM (
         SELECT ISNULL(SUM(ISNULL(payout.PAYOUT_AMOUNT,0)), 0) as points_received
         FROM PAYOUT payout with (nolock)
           JOIN EARNINGS_PAYOUT earnings_payout with (nolock)
             ON payout.PAYOUT_ID = earnings_payout.PAYOUT_ID
           JOIN EARNINGS earnings with (nolock)
             ON earnings.EARNINGS_ID = earnings_payout.EARNINGS_ID
           JOIN TRANSACTION_HEADER_EARNINGS transaction_header_earnings with (nolock)
             ON earnings.EARNINGS_ID = transaction_header_earnings.EARNINGS_ID
           JOIN TRANSACTION_HEADER transaction_header with (nolock)
             ON transaction_header.ID = transaction_header_earnings.TRANSACTION_HEADER_ID
           JOIN DISCRETIONARY discretionary with (nolock)
             ON transaction_header.ID = discretionary.TRANSACTION_HEADER_ID
           LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @PROGRAMLIST.nodes('/A') AS x(t)) PROGRAM_LIST
             ON PROGRAM_LIST.items = transaction_header.PROGRAM_ID
           LEFT JOIN (SELECT CAST(t.value('.', 'int') AS BIGINT) AS items FROM @USERLIST.nodes('/A') AS x(t)) USER_LIST
             ON USER_LIST.items = payout.PAX_ID
         WHERE payout.PAYOUT_DATE BETWEEN @startDate AND @endDate
               AND (@programIds IS NULL OR PROGRAM_LIST.items IS NOT NULL)
               AND (@userIds IS NULL OR USER_LIST.items IS NOT NULL)
               AND payout.STATUS_TYPE_CODE = 'ISSUED'
               AND transaction_header.SUB_TYPE_CODE <> 'FUND'
       ) results
WHERE #PROGRAM_STATS.IDENTIFIER = @TEMP_IDENTIFIER

-- Return the results (stored within the PROGRAM_STATS table)
SELECT
    RESULT.RECOGNITIONS_GIVEN,
    RESULT.RECOGNITIONS_RECEIVED,
    RESULT.PERCENT_ELIGIBLE_USERS_GIVING,
    RESULT.PERCENT_ELIGIBLE_USERS_RECEIVING,
    RESULT.POINTS_GIVEN,
    RESULT.POINTS_RECEIVED
FROM (SELECT TOP 1 * FROM #PROGRAM_STATS) RESULT

--Drop temp tables
DROP TABLE #PROGRAM_STATS

    --(+)Logging
    if (@loggingDebug=1)
begin
set @loggingEndDate = getdate()
	-- Generate a divide-by-zero error
	 --SELECT  1 / 0 AS Error;
	exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY
BEGIN CATCH
if (@loggingDebug=1)
begin
		declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
end
END CATCH
--(-)Logging

END

GO

