IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_RELEASE_NOMINATION_EMAILS' AND schema_id = SCHEMA_ID('component'))
    BEGIN
        exec ('create PROCEDURE component.UP_RELEASE_NOMINATION_EMAILS as BEGIN select 1 as c1 END')
    END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_RELEASE_NOMINATION_EMAILS
-- ===========================  DESCRIPTION  ==================================
-- Sets the send date of pending emails for nominations when all payouts have been issued
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20200424    Created
-- KNIGHTGA     20200427    Added alert updating
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_RELEASE_NOMINATION_EMAILS] (
    @payoutId bigint
)
AS BEGIN
SET NOCOUNT ON

create table #nomination_ids (
    NOMINATION_ID BIGINT NOT NULL PRIMARY KEY
)

--get the nomination ids that have all payouts marked as issued for the given payout id
;with NOMINATION_BATCH (NOMINATION_ID, BATCH_ID) AS (
    select be.TARGET_ID as NOMINATION_ID
          ,be.BATCH_ID
      from component.earnings_payout ep
           inner join component.TRANSACTION_HEADER_EARNINGS the on the.EARNINGS_ID = ep.EARNINGS_ID
           inner join component.TRANSACTION_HEADER th
                   on th.id = the.TRANSACTION_HEADER_ID
                  and th.TYPE_CODE = 'DISC'
                  and th.SUB_TYPE_CODE = 'RECG'
           inner join component.DISCRETIONARY d on d.TRANSACTION_HEADER_ID = th.id
           inner join component.batch_event be
                   on be.BATCH_ID = th.BATCH_ID
                  and be.BATCH_EVENT_TYPE_CODE = 'NTHLKE'
                  and be.TARGET_TABLE = 'NOMINATION'
     where ep.PAYOUT_ID = @payoutId
)
,NOMINATION_ISSUED (NOMINATION_ID) AS (
    select nb.NOMINATION_ID
      from NOMINATION_BATCH nb
           inner join component.TRANSACTION_HEADER th on th.BATCH_ID = nb.BATCH_ID
           inner join component.TRANSACTION_HEADER_EARNINGS the on the.TRANSACTION_HEADER_ID = th.ID
           inner join component.EARNINGS_PAYOUT ep on ep.EARNINGS_ID = the.EARNINGS_ID
           inner join component.payout p
                   on p.payout_id = ep.PAYOUT_ID
                  and p.STATUS_TYPE_CODE != 'REJECTED'
        group by nb.NOMINATION_ID
        having count(p.payout_id) = sum(case when p.status_type_code = 'ISSUED' then 1 else 0 end)
)
insert into #nomination_ids (NOMINATION_ID)
select ni.NOMINATION_ID from NOMINATION_ISSUED ni

--update the send date for emails tied to the nomination
update em
   set em.send_date = getdate()
  from #nomination_ids ni
       inner join component.EMAIL_MESSAGE em
               on em.REFERENCE_FK = ni.NOMINATION_ID
              and em.STATUS_TYPE_CODE = 'PENDING'
              and em.SEND_DATE = '12/31/9999'
       inner join component.EVENT_TYPE et
               on et.ID = em.EVENT_TYPE_ID
              and et.EVENT_TYPE_CODE in ('RECGMN','NOTIFY_OTHER')

--update the send date for emails tied to the recognitions for the nomination
update em
   set em.send_date = getdate()
  from #nomination_ids ni
       inner join component.RECOGNITION r on r.NOMINATION_ID = ni.NOMINATION_ID
       inner join component.EMAIL_MESSAGE em
               on em.REFERENCE_FK = r.ID
              and em.STATUS_TYPE_CODE = 'PENDING'
              and em.SEND_DATE = '12/31/9999'
       inner join component.EVENT_TYPE et
               on et.ID = em.EVENT_TYPE_ID
              and et.EVENT_TYPE_CODE = 'RECGPX'

--update the alerts tied to the nomination
update a
   set a.STATUS_TYPE_CODE = 'NEW'
      ,a.STATUS_DATE = getdate()
  from #nomination_ids ni
       inner join component.ALERT a
               on a.TARGET_ID = ni.NOMINATION_ID
              and a.TARGET_TABLE = 'NOMINATION'
              and a.STATUS_TYPE_CODE = 'HOLD'

--update the alerts tied to the recognition
update a
   set a.STATUS_TYPE_CODE = 'NEW'
      ,a.STATUS_DATE = getdate()
  from #nomination_ids ni
       inner join component.RECOGNITION r on r.NOMINATION_ID = ni.NOMINATION_ID
       inner join component.ALERT a
               on a.TARGET_ID = r.ID
              and a.TARGET_TABLE = 'RECOGNITION'
              and a.STATUS_TYPE_CODE = 'HOLD'

drop table #nomination_ids

END