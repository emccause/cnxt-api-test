SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[component].[UP_CREATE_THM_PAYOUT_TYPE]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [component].[UP_CREATE_THM_PAYOUT_TYPE]
END
GO
-- ==============================  NAME  ======================================
-- UP_CREATE_THM_PAYOUT_TYPE
-- ===========================  DESCRIPTION  ==================================
--
-- INSERTS INTO TRANSACTION MISCELANIOUS HEADER
-- PARAMETERS:
--   @NOMINATION_ID - The ID of the nomination to send emails for
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- SHARPCA      20201111    CNXT-325       CREATED
--
-- ===========================  DECLARATIONS  =================================

CREATE PROCEDURE component.UP_CREATE_THM_PAYOUT_TYPE
AS
BEGIN
    INSERT INTO component.TRANSACTION_HEADER_MISC (TRANSACTION_HEADER_ID, VF_NAME, MISC_DATA, MISC_DATE)
	SELECT th.ID, 'PAYOUT_TYPE', pm.MISC_DATA, GETDATE()
	FROM component.TRANSACTION_HEADER th
	LEFT JOIN component.TRANSACTION_HEADER_EARNINGS the ON the.TRANSACTION_HEADER_ID = th.ID
	LEFT JOIN component.TRANSACTION_HEADER_MISC thm ON thm.TRANSACTION_HEADER_ID = th.ID AND thm.VF_NAME = 'PAYOUT_TYPE'
	JOIN component.PROGRAM_MISC pm ON pm.PROGRAM_ID = th.PROGRAM_ID AND pm.VF_NAME = 'SELECTED_AWARD_TYPE'
	WHERE th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE IN ('RECG', 'DISC')
	AND thm.MISC_DATA IS NULL
	AND the.EARNINGS_ID IS NULL

END
GO