/****** Object:  StoredProcedure [component].[UP_EMAIL_HISTORY_SEARCH]    Script Date: 4/29/2021 1:40:15 PM ******/
IF NOT EXISTS(SELECT * FROM [sys].[procedures] WHERE name = 'UP_EMAIL_HISTORY_SEARCH' AND schema_id = SCHEMA_ID('component'))
BEGIN
exec ('create PROCEDURE component.UP_EMAIL_HISTORY_SEARCH as BEGIN select 1 as c1 END')
END
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_EMAIL_HISTORY_SEARCH
-- ===========================  DESCRIPTION  ==================================
--
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- SHARPCA		 4/28/2021	 CNXT-488	  CREATED
--
-- ===========================  DECLARATIONS  =================================


/****** Object:  StoredProcedure [component].[UP_EMAIL_HISTORY_SEARCH]    Script Date: 5/26/2021 2:26:07 PM ******/


ALTER PROCEDURE [component].[UP_EMAIL_HISTORY_SEARCH]

@page_number INT,
@page_size INT,
@control_num nvarchar(255)

AS

BEGIN
SET NOCOUNT ON

-- Logging snippet begin

BEGIN TRY
declare @loggingStartDate  DATETIME2(7), @loggingEndDate  DATETIME2(7), @loggingParameters NVARCHAR(MAX), @loggingSprocName nvarchar(150), @loggingDebug int

select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    if (@loggingDebug=1)
begin

set @loggingSprocName = 'UP_PAX_SEARCH_ADMIN_QUERY'
set @loggingStartDate = getdate()
set @loggingParameters = '@page_number=' + isnull(cast(@page_number as varchar), 'null') --BIGINT
+ ' :: @page_size=' + isnull(cast(@page_size as varchar), 'null') --BIGINT
+ ' :: @control_num=' + isnull(@control_num, 'null') -- NVARCHAR(MAX)

end

--Logging snippet end

SELECT
    COUNT(*) OVER() AS TOTAL_RESULTS,
        * FROM (
                   SELECT EM.SENT_DATE, EM.SUBJECT, ET.EVENT_TYPE_DESC FROM component.EMAIL_MESSAGE EM
                                                                                INNER JOIN component.EVENT_TYPE ET ON EM.EVENT_TYPE_ID = ET.ID
                                                                                INNER JOIN component.PAX P ON EM.PAX_ID = P.PAX_ID
                   WHERE P.CONTROL_NUM = @control_num AND EM.STATUS_TYPE_CODE = 'SENT' AND EM.ERROR is NULL
               ) RAW_DATA
ORDER BY SENT_DATE DESC
OFFSET ((@page_number - 1) * @page_size) ROWS FETCH NEXT @page_size ROWS ONLY



--(+)Logging
    if (@loggingDebug=1)
begin
    set @loggingEndDate = getdate()
    -- Generate a divide-by-zero error
    --SELECT  1 / 0 AS Error;
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
end
END TRY

BEGIN CATCH
if (@loggingDebug=1)
begin
        declare @errorMessage nvarchar(250)
SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
    exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage

end
END CATCH
--(-)Logging

END






GO


