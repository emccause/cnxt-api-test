/****** Object:  StoredProcedure [component].[UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT]    Script Date: 10/15/2015 10:59:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, ACTIVITY_DATE_FROM, ACTIVITY_DATE_THRU
-- RETURN ALL ISSUED PAYOUT AMOUNTS FOR DESCENDANT(S) OF SPECIFIED PAX_GROUP_ID
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20110921                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
-- MUDDAM        20151102                UDM3 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT]
    @pax_group_id BIGINT
,    @activity_date_from DATETIME2
,    @activity_date_thru DATETIME2
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @result_set table (
        pax_group_id BIGINT,
        payout_amount INT,
        levels_remaining INT
    ) 
    
    DECLARE @pax_group_id_descendant BIGINT
    
-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

    DECLARE c_list CURSOR FOR SELECT PAX_GROUP_ID FROM PAX_GROUP WHERE OWNER_GROUP_ID = @pax_group_id
    OPEN c_list
    
    FETCH NEXT FROM c_list INTO @pax_group_id_descendant
    
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        INSERT INTO @result_set (
            pax_group_id
        ,    payout_amount
        )
        SELECT    @pax_group_id_descendant
        ,    SUM(PAYOUT.PAYOUT_AMOUNT) 
        FROM    UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id_descendant, 1, NULL) 
        JOIN PAX_GROUP
            ON [ID] = PAX_GROUP.PAX_GROUP_ID
        JOIN TRANSACTION_HEADER
            ON PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID 
            AND TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
        JOIN TRANSACTION_HEADER_EARNINGS
            ON TRANSACTION_HEADER_EARNINGS.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.[ID]
        JOIN EARNINGS_PAYOUT
            ON EARNINGS_PAYOUT.EARNINGS_ID = TRANSACTION_HEADER_EARNINGS.EARNINGS_ID
        JOIN PAYOUT
            ON PAYOUT.PAYOUT_ID = EARNINGS_PAYOUT.PAYOUT_ID
            AND PAYOUT.STATUS_TYPE_CODE = 'ISSUED'
    
        UPDATE    @result_set
        SET    levels_remaining = (
                    SELECT COUNT(DISTINCT ORG_GROUP_2.ORG_GROUP_ID)
                    FROM     UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id_descendant, 1, 1) FN
                    ,    ORG_GROUP ORG_GROUP_1 -- PARENT
                    ,    ORG_GROUP ORG_GROUP_2 -- CHILD
                    ,    PAX_GROUP
                    WHERE PAX_GROUP.PAX_GROUP_ID = FN.[ID]
                    AND FN.LEVEL <> 0
                    AND PAX_GROUP.ORGANIZATION_CODE = ORG_GROUP_1.ORGANIZATION_CODE
                    AND PAX_GROUP.ROLE_CODE = ORG_GROUP_1.ROLE_CODE
                    AND ORG_GROUP_1.ORG_GROUP_ID = ORG_GROUP_2.OWNER_GROUP_ID
        )
        WHERE    pax_group_id = @pax_group_id_descendant
        AND    payout_amount IS NOT NULL

        FETCH NEXT FROM c_list INTO @pax_group_id_descendant
    END
    
    CLOSE c_list
    DEALLOCATE c_list

    IF (SELECT COUNT(1) FROM @result_set WHERE payout_amount IS NOT NULL) > 0
    BEGIN
        SELECT     result_set.pax_group_id
        ,    result_set.payout_amount
        ,    result_set.levels_remaining
        ,    PAX.COMPANY_NAME AS company_name
        ,    PAX.FIRST_NAME AS first_name
        ,    PAX.LAST_NAME AS last_name
        FROM @result_set result_set
        JOIN PAX_GROUP
            ON result_set.pax_group_id = PAX_GROUP.PAX_GROUP_ID
        JOIN PAX
          ON PAX_GROUP.PAX_ID = PAX.PAX_ID
        WHERE result_set.payout_amount IS NOT NULL
    END
    ELSE
    BEGIN
        SELECT 'No Data' as label, 0 as value
    END
END

GO

