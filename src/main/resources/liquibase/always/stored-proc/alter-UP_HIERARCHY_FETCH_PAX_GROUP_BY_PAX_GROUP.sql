/****** Object:  StoredProcedure [component].[UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP]    Script Date: 10/15/2015 10:52:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- ==============================  NAME  ======================================
-- UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID AND LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS)
-- RETURN ALL ASSOCIATED PAX_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- DOHOGNTA        20110221                CREATED
-- DOHOGNTA        20150309                CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP]
    @pax_group_id BIGINT
,    @level_increment INT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS PAX_ID
FROM    UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP (@pax_group_id, @level_increment, NULL)

END

GO

