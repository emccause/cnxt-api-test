/****** Object:  StoredProcedure [component].[UP_MANAGER_BUDGET_STATS]    Script Date: 10/15/2015 10:54:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_MANAGER_BUDGET_STATS
-- ===========================  DESCRIPTION  ==================================
--
-- RETURNS BUDGET STATS FOR REPORTING PURPOSES INCLUDING...
-- BUDGET USED IN PERIOD
-- BUDGET TOTAL
-- BUDGET AVAILABLE
-- BUDGET EXPIRED
--
-- THIS IS A MANAGER REPORT, SO TOTALS WILL BE BASED UPON 
-- INDIVIDUAL_GIVING_LIMIT INSTEAD OF ACTUAL BUDGET TOTALS
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- HARWELLM        20150627                CREATED
-- HARWELLM        20150724                MODIFIED TO USE LEFT JOIN FOR TOTAL AMOUNTS
-- DAGARFIN        20150805                REMOVING PARENT ID SINCE IT'S USED FOR BUDGETS
-- MUDDAM        20151102                UDM3 UPDATES
-- MUDDAM        20151116                UDM4 UPDATES
-- GARCIAF2        20160325                REMOVE "component." SCHEMA
-- HARWELLM        20170525    MP-10042    STOP RESTRICTING BUDGET TO 10,2 PRECISION
-- MUDDAM        20170411    MP-8001        UDM5 CHANGES
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_MANAGER_BUDGET_STATS]
@delim_budget_id_list VARCHAR(MAX),
@delim_pax_id_list VARCHAR(MAX),
@start_date DATETIME2(7),
@end_date DATETIME2(7)

AS
BEGIN
SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

CREATE TABLE #budgetIdParams (
    ID BIGINT
)

CREATE TABLE #paxIdParams (
    ID BIGINT
)

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF BUDGET_ID(S) TO A LIST OF LONG(S)
INSERT INTO #budgetIdParams ([id]) SELECT token AS budget_id FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_budget_id_list, @delimiter)

-- PARSE DELIMITED LIST OF PAX_ID(S) TO A LIST OF LONG(S)
INSERT INTO #paxidParams ([id]) SELECT token AS pax_id FROM component.UF_PARSE_STRING_TO_INTEGER (@delim_pax_id_list, @delimiter)

--CREATE TABLE TO RETURN VALUES
CREATE TABLE #budgetStats (
    BUDGET_USED_IN_PERIOD DECIMAL(22,2),
    BUDGET_USED_OUTSIDE_PERIOD DECIMAL(22,2),
    BUDGET_TOTAL DECIMAL(22,2),
    BUDGET_AVAILABLE DECIMAL(22,2),
    BUDGET_EXPIRED DECIMAL(22,2)
)


-- ****** Step 1: Calculate the budget total amount for the passed in budgetIds ******

--Total Budget for ONE budget in the list = number of direct reports per budget * individual giving limit
CREATE TABLE #budgetTotals (
    BUDGET_ID BIGINT NOT NULL,
    TOTAL_USERS INTEGER NOT NULL,
    GIVING_LIMIT DECIMAL(22,2) NULL,
    TOTAL_BUDGET DECIMAL(22,2) NULL
)

--Get a count of how many times each budget was used, and each budget's INDIVIDUAL_GIVING_LIMIT
INSERT INTO #budgetTotals 
SELECT bp.ID, COUNT(bp.ID), b.INDIVIDUAL_GIVING_LIMIT, NULL
FROM #budgetIdParams bp 
JOIN component.BUDGET b ON b.ID = bp.ID
GROUP BY bp.ID, b.INDIVIDUAL_GIVING_LIMIT

--Calculate the budget total for each budget
UPDATE #budgetTotals SET TOTAL_BUDGET = (TOTAL_USERS * GIVING_LIMIT)


-- Set the total on the budgetStats record
INSERT INTO #budgetStats (BUDGET_TOTAL) VALUES (ISNULL((SELECT SUM(TOTAL_BUDGET) FROM #budgetTotals), 0))


-- Remove the duplicate budgetId's and use that list from now on
CREATE TABLE #distinctBudgetIds (
    ID BIGINT
)

INSERT INTO #distinctBudgetIds SELECT DISTINCT * FROM #budgetIdParams


-- ****** Step 2: Calculate Budget Used In/Out of Period for the given PaxIds ******

--Save the total amounts used into a temp table
CREATE TABLE #totalAmountsUsed (
    ID BIGINT NOT NULL,
    IN_PERIOD DECIMAL(22,2) NOT NULL,
    OUT_OF_PERIOD DECIMAL(22,2) NOT NULL
)

INSERT INTO #totalAmountsUsed 
SELECT ID, ISNULL(SUM(ISNULL(TOTAL_USED_IN_PERIOD,0)),0), ISNULL(SUM(ISNULL(TOTAL_USED_OUTSIDE_PERIOD,0)),0)
    FROM (
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    SELECT DISTINCT ISNULL(DEBITS.ID, ISNULL(CREDITS.ID, ISNULL(DEBITS_OUTSIDE_PERIOD.ID, ISNULL(CREDITS_OUTSIDE_PERIOD.ID, 0)))) AS 'ID',
    (CASE
        WHEN DEBITS.DEBITS is null then 0
        ELSE DEBITS.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS.CREDITS is null then 0
        ELSE CREDITS.CREDITS
    END)
    AS 'TOTAL_USED_IN_PERIOD',    
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD CALC-------------------------------------------
    (CASE
        WHEN DEBITS_OUTSIDE_PERIOD.DEBITS is null then 0
        ELSE DEBITS_OUTSIDE_PERIOD.DEBITS
    END)
    -
    (CASE
        WHEN CREDITS_OUTSIDE_PERIOD.CREDITS is null then 0
        ELSE CREDITS_OUTSIDE_PERIOD.CREDITS
    END)
    AS 'TOTAL_USED_OUTSIDE_PERIOD'    
    FROM component.DISCRETIONARY DISC         
    JOIN #distinctBudgetIds b ON b.ID = DISC.BUDGET_ID_DEBIT OR b.ID = DISC.BUDGET_ID_CREDIT
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_IN_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN
        (--DEBITS IN PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.DISCRETIONARY disc_debit ON disc_debit.TARGET_ID = r.ID and disc_debit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON th.ID = disc_debit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE = 'RECG' and th.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #distinctBudgetIds bp ON bp.ID = disc_debit.BUDGET_ID_DEBIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE disc_debit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_debit.BUDGET_ID_DEBIT
        ) DEBITS
        ON B.ID = DEBITS.ID            
    LEFT JOIN
        (--CREDITS IN PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.DISCRETIONARY disc_credit ON disc_credit.TARGET_ID = r.ID and disc_credit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON th.ID = disc_credit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE = 'RECG' and th.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #distinctBudgetIds bp ON bp.ID = disc_credit.BUDGET_ID_CREDIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE disc_credit.PERFORMANCE_DATE BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
        ) CREDITS
        ON B.ID = CREDITS.ID        
    ------------------------------------------------------------------------------------------------------------------------------------
    --------------------------------------------BUDGET_USED_OUTSIDE_PERIOD--------------------------------------------
    ------------------------------------------------------------------------------------------------------------------------------------
    LEFT JOIN 
        (--DEBITS OUTSIDE PERIOD
        SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.DISCRETIONARY disc_debit ON disc_debit.TARGET_ID = r.ID and disc_debit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON th.ID = disc_debit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE = 'RECG' and th.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #distinctBudgetIds bp ON bp.ID = disc_debit.BUDGET_ID_DEBIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE disc_debit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        GROUP BY disc_debit.BUDGET_ID_DEBIT
        ) DEBITS_OUTSIDE_PERIOD
        ON B.ID = DEBITS_OUTSIDE_PERIOD.ID
    LEFT JOIN
        (--CREDITS OUTSIDE PERIOD
        SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS'
        FROM component.NOMINATION n
        JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID
        JOIN component.DISCRETIONARY disc_credit ON disc_credit.TARGET_ID = r.ID and disc_credit.TARGET_TABLE = 'RECOGNITION'
        JOIN component.TRANSACTION_HEADER th ON th.ID = disc_credit.TRANSACTION_HEADER_ID AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE = 'RECG' and th.STATUS_TYPE_CODE = 'APPROVED'
        JOIN #distinctBudgetIds bp ON bp.ID = disc_credit.BUDGET_ID_CREDIT
        JOIN #paxIdParams pp ON pp.ID = n.SUBMITTER_PAX_ID
        WHERE disc_credit.PERFORMANCE_DATE NOT BETWEEN @start_date AND @end_date
        GROUP BY disc_credit.BUDGET_ID_CREDIT
        ) CREDITS_OUTSIDE_PERIOD
        ON B.ID = CREDITS_OUTSIDE_PERIOD.ID    
WHERE DISC.TARGET_TABLE = 'RECOGNITION' AND DISC.TARGET_ID IS NOT NULL --This ensures we don't get the budget funding transactions
) TOTALS WHERE ID <> 0 GROUP BY ID

DECLARE @totalIn DECIMAL(22,2)
DECLARE @totalOut DECIMAL(22,2)
SET @totalIn = (SELECT SUM(IN_PERIOD) FROM #totalAmountsUsed)
SET @totalOut = (SELECT SUM(OUT_OF_PERIOD) FROM #totalAmountsUsed)

--Set budget used both in and out of period on the budget stats record
UPDATE #budgetStats SET BUDGET_USED_IN_PERIOD = @totalIn, BUDGET_USED_OUTSIDE_PERIOD = @totalOut
FROM #totalAmountsUsed


-- ****** Step 3: Calculate budget available and budget expired (for each budget individually) ******

CREATE TABLE #budgetAvailableExpired (
    BUDGET_ID BIGINT NOT NULL,
    BUDGET_AVAILABLE DECIMAL(22,2) NULL,
    BUDGET_EXPIRED DECIMAL(22,2) NULL
)

--Budget Available is if the budget is currently active and the THRU_DATE has not passed.
INSERT INTO #budgetAvailableExpired
SELECT b.ID, (bt.TOTAL_BUDGET - (ISNULL(ta.IN_PERIOD, 0) + ISNULL(ta.OUT_OF_PERIOD, 0))), 0
FROM #totalAmountsUsed ta
JOIN component.BUDGET b ON b.ID = ta.ID
JOIN #budgetTotals bt ON bt.BUDGET_ID = ta.ID
WHERE b.STATUS_TYPE_CODE = 'ACTIVE'
AND (b.THRU_DATE > getdate() OR b.THRU_DATE IS NULL)

--Budget Expired is if the budget is ACTIVE with a passed THRU_DATE, or if the budget is INACTIVE
INSERT INTO #budgetAvailableExpired
SELECT b.ID, 0, (bt.TOTAL_BUDGET - (ISNULL(ta.IN_PERIOD, 0) + ISNULL(ta.OUT_OF_PERIOD, 0)))
FROM #totalAmountsUsed ta
JOIN component.BUDGET b ON b.ID = ta.ID
JOIN #budgetTotals bt ON bt.BUDGET_ID = ta.ID
WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
(b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())

--If any budgets haven't been used yet, add them to #budgetAvailableExpired so that they get included in the calculations
--ACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT b.ID, bt.TOTAL_BUDGET, 0
FROM #distinctBudgetIds d
JOIN component.BUDGET b ON b.ID = d.ID
JOIN #budgetTotals bt on bt.BUDGET_ID = b.ID
WHERE b.STATUS_TYPE_CODE = 'ACTIVE'
AND (b.THRU_DATE > getdate() OR b.THRU_DATE IS NULL)
AND d.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)

--INACTIVE budgets that haven't been used
INSERT INTO #budgetAvailableExpired
SELECT b.ID, 0, bt.TOTAL_BUDGET
FROM #distinctBudgetIds d
JOIN component.BUDGET b ON b.ID = d.ID
JOIN #budgetTotals bt on bt.BUDGET_ID = b.ID
WHERE b.STATUS_TYPE_CODE = 'INACTIVE' OR
(b.STATUS_TYPE_CODE = 'ACTIVE' AND b.THRU_DATE < getdate())
AND d.ID NOT IN (SELECT BUDGET_ID FROM #budgetAvailableExpired)

DECLARE @totalAvailable DECIMAL(22,2)
DECLARE @totalExpired DECIMAL(22,2)
SET @totalAvailable = (SELECT SUM(BUDGET_AVAILABLE) FROM #budgetAvailableExpired)
SET @totalExpired = (SELECT SUM(BUDGET_EXPIRED) FROM #budgetAvailableExpired)

--Set budgetAvailable and budgetExpired on the budget stats record
UPDATE #budgetStats SET BUDGET_AVAILABLE = @totalAvailable, BUDGET_EXPIRED = @totalExpired
FROM #budgetAvailableExpired

SELECT * FROM #budgetStats   

END












GO

