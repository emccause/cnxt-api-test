/****** Object:  StoredProcedure [component].[UP_HIERARCHY_LIST]    Script Date: 10/21/2020 11:31:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- UP_HIERARCHY_LIST
-- ===========================  DESCRIPTION  ==================================
-- TODO: REWRITE / DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR  	DATE        STORY	CHANGE DESCRIPTION
-- DOHOGNTA	20050720                CREATED
-- DOHOGNTA	20150309                CHANGE IDENTITIES TO BIGINT
-- burgestl	20201021		Update join to sql 2017 
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_LIST]
    @organization_code nvarchar(4)
,    @pax_id BIGINT
,    @show_children bit = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

create table #pax_hierarchy (
    pax_id    BIGINT,
    level_code    int,
    pax_group_id    BIGINT,
    owner_group_id    BIGINT,
    role_code        nvarchar(4),
    entity_name        varchar(100)
)

declare @level_code as int

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

set @level_code = 0

insert into #pax_hierarchy
select pax_group.pax_id, @level_code, pax_group.pax_group_id, owner_group_id, pax_group.role_code,
case
    when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
    else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
end entity_name
from pax_group 
inner join pax on pax_group.pax_id = pax.pax_id
where organization_code = @organization_code
and pax_group.pax_id = @pax_id
and thru_date is null
and getdate() between from_date and isnull(thru_date, getdate())

declare @pax_group_id as BIGINT
select @pax_group_id = owner_group_id
from #pax_hierarchy

while @pax_group_id is not null begin

    set @level_code = @level_code + 1

    insert into #pax_hierarchy
    select pax_group.pax_id, @level_code, pax_group.pax_group_id, owner_group_id, pax_group.role_code,
    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
    from pax_group
	inner join pax on pax_group.pax_id = pax.pax_id
    where pax_group.pax_group_id = @pax_group_id
    and pax_group.thru_date is null
    and getdate() between from_date and isnull(thru_date, getdate())

    select @pax_group_id = owner_group_id
    from pax_group
    where pax_group_id = @pax_group_id
end

select @pax_id as pax_id, @organization_code as organization_code, (select pax_group_id from #pax_hierarchy where level_code = 0) as pax_group_id

select * from #pax_hierarchy order by level_code desc

if @show_children = 1 begin
    select pax_group.pax_id, pax_group.pax_group_id, pax_group.role_code,
    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
    from pax_group
	inner join pax on pax_group.pax_id = pax.pax_id
	inner join #pax_hierarchy on pax_group.owner_group_id = #pax_hierarchy.pax_group_id
    where #pax_hierarchy.level_code = 0
    and pax_group.thru_date is null 
    and getdate() between from_date and isnull(thru_date, getdate())
    order by entity_name, pax_group.role_code
end

END

GO
