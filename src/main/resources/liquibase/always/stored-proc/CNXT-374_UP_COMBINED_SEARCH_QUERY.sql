SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[component].[UP_COMBINED_SEARCH_QUERY]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [component].[UP_COMBINED_SEARCH_QUERY]
END
GO

-- ==============================  NAME  ======================================
-- UP_COMBINED_SEARCH_QUERY
-- ===========================  DESCRIPTION  ==================================
-- Code called from SearchDaoImpl.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- ARENASW       20201217    CNXT-374     Moved from inline sql to sp
-- BRIONEMC      20210714    CNXT-572     Show active users in groups counts only when search is to give recognition.
-- ===========================  DECLARATIONS  =================================

CREATE PROCEDURE component.UP_COMBINED_SEARCH_QUERY 
		@SEARCH_STRING_SQL_PARAM NVARCHAR(500),
       	@SEARCH_STRING_RAW_SQL_PARAM NVARCHAR(500),
       	@SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM NVARCHAR(500),
       	@VISIBILITY_LIST_SQL_PARAM NVARCHAR(500),
       	@LOGGED_IN_PAX_ID_SQL_PARAM NVARCHAR(500),
       	@STATUS_LIST_SQL_PARAM NVARCHAR(500) ,
       	@GROUP_TYPE_LIST_SQL_PARAM NVARCHAR(500),
       	@EXCLUDE_SELF_SQL_PARAM NVARCHAR(500),
       	@PAGE_NUMBER INT,
       	@PAGE_SIZE INT
AS
BEGIN
BEGIN TRY
    declare @loggingStartDate  DATETIME2(7),
            @loggingEndDate  DATETIME2(7),
            @loggingParameters NVARCHAR(MAX),
            @loggingSprocName nvarchar(150),
            @loggingDebug int


    --insert component.application_data (key_name, value)
    --select 'sql.logging','true'
    select  @loggingDebug = case when (isnull(value, '') =  'true') then 1 else 0 end from component.application_data where key_name = 'sql.logging'

    if (@loggingDebug=1)
    begin
        set @loggingSprocName = 'UP_COMBINED_SEARCH_QUERY'
        set @loggingStartDate = getdate()
        set @loggingParameters = '@SEARCH_STRING_SQL_PARAM='+ isnull(cast(@SEARCH_STRING_SQL_PARAM as varchar),'null')
            + ' :: @SEARCH_STRING_RAW_SQL_PARAM=' + isnull(cast(@SEARCH_STRING_RAW_SQL_PARAM as varchar),'null')
            + ' :: @SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM=' + isnull(cast(@SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM as varchar),'null')
            + ' :: @VISIBILITY_LIST_SQL_PARAM='+ isnull(cast(@VISIBILITY_LIST_SQL_PARAM as varchar),'null')
            + ' :: @LOGGED_IN_PAX_ID_SQL_PARAM='+ isnull(cast(@LOGGED_IN_PAX_ID_SQL_PARAM as varchar),'null')
            + ' :: @STATUS_LIST_SQL_PARAM='+isnull(cast(@STATUS_LIST_SQL_PARAM as varchar),'null')
            + ' :: @GROUP_TYPE_LIST_SQL_PARAM='+isnull(cast(@GROUP_TYPE_LIST_SQL_PARAM as varchar),'null')
            + ' :: @EXCLUDE_SELF_SQL_PARAM='+isnull(cast(@EXCLUDE_SELF_SQL_PARAM as varchar),'null')
            + ' :: @PAGE_NUMBER='+isnull(cast(@PAGE_NUMBER as varchar),'null')
            + ' :: @PAGE_SIZE='+isnull(cast(@PAGE_SIZE as varchar),'null')
    end
    --(-)Logging
   IF(@EXCLUDE_SELF_SQL_PARAM =1)--It's a search to give a recognition
	BEGIN
		SELECT COUNT(*) OVER() AS TOTAL_RESULTS, * FROM 
		(select * FROM component.VW_USER_GROUPS_SEARCH_DATA_ACTIVE_PARTICIPANT_COUNTS
		WHERE (NAME COLLATE Latin1_general_CI_AI LIKE @SEARCH_STRING_SQL_PARAM   COLLATE Latin1_general_CI_AI
			OR CONTROL_NUM = @SEARCH_STRING_RAW_SQL_PARAM   COLLATE Latin1_general_CI_AI
			OR EMAIL_ADDRESS = @SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM   COLLATE Latin1_general_CI_AI)
			AND (
				(GROUP_ID IS NULL)
				OR (
					GROUP_ID IS NOT NULL
					AND (
					VISIBILITY IN (SELECT * FROM component.UF_LIST_TO_TABLE(@VISIBILITY_LIST_SQL_PARAM  , ','))
					)
				)
				OR (
					GROUP_ID IS NOT NULL
					AND VISIBILITY IS NULL
					AND GROUP_PAX_ID = @LOGGED_IN_PAX_ID_SQL_PARAM
				)
			)
			AND (
				@STATUS_LIST_SQL_PARAM   IS NULL
				OR STATUS_TYPE_CODE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@STATUS_LIST_SQL_PARAM  , ','))
			)
			AND (
				GROUP_TYPE IS NULL
				OR (
					@GROUP_TYPE_LIST_SQL_PARAM   IS NULL OR
						GROUP_TYPE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@GROUP_TYPE_LIST_SQL_PARAM  , ','))
				)
			)
			AND (@EXCLUDE_SELF_SQL_PARAM   = 0 OR (PAX_ID IS NULL OR PAX_ID != @LOGGED_IN_PAX_ID_SQL_PARAM  ))
		) RAW_DATA
		ORDER BY NAME ASC
		OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS FETCH NEXT @PAGE_SIZE ROWS ONLY
		
	END
	ELSE
		BEGIN 
			SELECT COUNT(*) OVER() AS TOTAL_RESULTS, 
			* FROM (select * FROM component.VW_USER_GROUPS_SEARCH_DATA 
			WHERE (NAME COLLATE Latin1_general_CI_AI LIKE @SEARCH_STRING_SQL_PARAM   COLLATE Latin1_general_CI_AI
			OR CONTROL_NUM = @SEARCH_STRING_RAW_SQL_PARAM   COLLATE Latin1_general_CI_AI
			OR EMAIL_ADDRESS = @SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM   COLLATE Latin1_general_CI_AI)
			AND (
				(GROUP_ID IS NULL)
				OR (
					GROUP_ID IS NOT NULL
					AND (
					VISIBILITY IN (SELECT * FROM component.UF_LIST_TO_TABLE(@VISIBILITY_LIST_SQL_PARAM  , ','))
					)
				)
				OR (
					GROUP_ID IS NOT NULL
					AND VISIBILITY IS NULL
					AND GROUP_PAX_ID = @LOGGED_IN_PAX_ID_SQL_PARAM
				)
			)
			AND (
				@STATUS_LIST_SQL_PARAM   IS NULL
				OR STATUS_TYPE_CODE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@STATUS_LIST_SQL_PARAM  , ','))
			)
			AND (
				GROUP_TYPE IS NULL
				OR (
					@GROUP_TYPE_LIST_SQL_PARAM   IS NULL OR
						GROUP_TYPE IN (SELECT * FROM component.UF_LIST_TO_TABLE(@GROUP_TYPE_LIST_SQL_PARAM  , ','))
				)
			)
			AND (@EXCLUDE_SELF_SQL_PARAM   = 0 OR (PAX_ID IS NULL OR PAX_ID != @LOGGED_IN_PAX_ID_SQL_PARAM  ))
		) RAW_DATA
		ORDER BY NAME ASC
		OFFSET ((@PAGE_NUMBER - 1) * @PAGE_SIZE) ROWS FETCH NEXT @PAGE_SIZE ROWS ONLY

		END
    --(+)Logging
    if (@loggingDebug=1)
    begin
        set @loggingEndDate = getdate()
        -- Generate a divide-by-zero error
          --SELECT  1 / 0 AS Error;
        exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, null
    end
    END TRY
    BEGIN CATCH
        if (@loggingDebug=1)
        begin
            declare @errorMessage nvarchar(250)
            SELECT @errorMessage = ':: ERROR_NUMBER=' + cast(ERROR_NUMBER() as varchar) + ' :: ERROR_MESSAGE=' + ERROR_MESSAGE()
            exec  [component].[UP_STORED_PROCS_LOGGING] @loggingSprocName, @loggingParameters, @loggingStartDate, @errorMessage
        end
    END CATCH
    --(-)Logging
END