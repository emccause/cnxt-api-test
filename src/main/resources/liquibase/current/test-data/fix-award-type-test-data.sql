
CREATE TABLE #PayoutVendorDeleteList (ID BIGINT)
INSERT INTO #PayoutVendorDeleteList SELECT PAYOUT_VENDOR_ID FROM component.PAYOUT_VENDOR WHERE PAYOUT_VENDOR_NAME IN ('DYNE', 'GC', 'GENERIC', 'Cash Check')

CREATE TABLE #PayoutItemDeleteList (ID BIGINT)
INSERT INTO #PayoutItemDeleteList SELECT PAYOUT_ITEM_ID FROM component.PAYOUT_ITEM WHERE PAYOUT_ITEM_NAME IN ('Dynamic US/Global Points', 'EY', 'DCB', 'GCB')

-- 1. Update PAYOUT records to use different PAYOUT_ITEM_ID's and PAYOUT_TYPE_CODE's
UPDATE component.PAYOUT SET PAYOUT_TYPE_CODE = 'DPP', PAYOUT_ITEM_ID = (SELECT PAYOUT_ITEM_ID FROM component.PAYOUT_ITEM WHERE PAYOUT_ITEM_NAME = 'DPP')

-- 2. Update EARNINGS records to use different EARNINGS_TYPE_CODE's
UPDATE component.EARNINGS SET EARNINGS_TYPE_CODE = 'DPP'

-- 3. Add the DPP CARD_TYPE because somehow that was missing
INSERT INTO component.CARD_TYPE (CARD_TYPE_CODE, CARD_TYPE_DESC, DISPLAY_SEQUENCE, DISPLAY_STATUS_TYPE_CODE) VALUES
('DPP', 'Shop Rewards', 1, 'ACTIVE')

-- 4. Update PAX_ACCOUNT records to use different CARD_TYPE_CODE's
UPDATE component.PAX_ACCOUNT SET CARD_TYPE_CODE = 'DPP' WHERE CARD_TYPE_CODE <> 'RIDEAU'

-- 5. Delete PAYOUT_ITEM records
DELETE FROM component.PAYOUT_ITEM WHERE PAYOUT_ITEM_ID IN (SELECT * FROM #PayoutItemDeleteList)

-- 6. Create a new PAYOUT_ITEM record to be used for the "delete award type" unit test
INSERT INTO component.PAYOUT_ITEM (PAYOUT_VENDOR_ID, PAYOUT_ITEM_NAME, PAYOUT_ITEM_DESC, PAYOUT_ITEM_AMOUNT) VALUES
(8, 'UNIT_TEST', 'Do not use this! It exists for the delete award type unit test', '1.00')

-- 7. Delete PAYOUT_VENDOR records
DELETE FROM component.PAYOUT_VENDOR WHERE PAYOUT_VENDOR_ID IN (SELECT * FROM #PayoutVendorDeleteList)

-- 8. Delete PAYOUT_TYPE records
DELETE FROM component.PAYOUT_TYPE WHERE PAYOUT_TYPE_CODE IN ('DYNE', 'EYD', 'DCB', 'GCB', '5PP', 'DCT', 'HPP')

-- 9. Delete EARNINGS_TYPE records 
DELETE FROM component.EARNINGS_TYPE WHERE EARNINGS_TYPE_CODE IN ('DYNE', 'EYD', '5PP', 'DCT', 'HPP')

-- 10. Delete CARD_TYPE records
DELETE FROM component.CARD_TYPE WHERE CARD_TYPE_CODE IN ('DCB', 'GCB', 'FCP', 'DCP', 'EYD')

-- 11. Insert missing CARD_TYPE record
INSERT INTO component.CARD_TYPE (CARD_TYPE_CODE, CARD_TYPE_DESC, DISPLAY_SEQUENCE, DISPLAY_STATUS_TYPE_CODE) VALUES
('SAA', 'Service Anniversary', 2, 'ACTIVE')

DROP TABLE #PayoutVendorDeleteList
DROP TABLE #PayoutItemDeleteList