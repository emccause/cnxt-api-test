alter table component.APPROVAL_PENDING add TARGET_TABLE nvarchar(128) null
EXEC sp_rename 'component.APPROVAL_PENDING.TARGET_SUB_ID', 'TARGET_ID', 'COLUMN'

alter table component.HISTORY_APPROVAL_PENDING add TARGET_TABLE nvarchar(128) null
EXEC sp_rename 'component.HISTORY_APPROVAL_PENDING.TARGET_SUB_ID', 'TARGET_ID', 'COLUMN'


/****** Object:  Index [IX_APPROVAL_PENDING_3]    Script Date: 9/18/2017 8:44:10 AM ******/
DROP INDEX [IX_APPROVAL_PENDING_3] ON [component].[APPROVAL_PENDING]
GO

/****** Object:  Index [IX_APPROVAL_PENDING_3]    Script Date: 9/18/2017 8:44:10 AM ******/
CREATE NONCLUSTERED INDEX [IX_APPROVAL_PENDING_3] ON [component].[APPROVAL_PENDING]
(
    [TARGET_TABLE] ASC,
    [TARGET_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



/****** Object:  Trigger [component].[TR_APPROVAL_PENDING_DU_1]    Script Date: 9/18/2017 8:21:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_APPROVAL_PENDING_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_APPROVAL_PENDING_DU_1]
ON [component].[APPROVAL_PENDING]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_APPROVAL_PENDING (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    PAX_ID
,    APPROVAL_PROCESS_ID
,    TARGET_ID
,    TARGET_TABLE
,    NEXT_APPROVER_PAX_ID
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    PAX_ID
,    APPROVAL_PROCESS_ID
,    TARGET_ID
,    TARGET_TABLE
,    NEXT_APPROVER_PAX_ID
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''APPROVAL_PENDING''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
