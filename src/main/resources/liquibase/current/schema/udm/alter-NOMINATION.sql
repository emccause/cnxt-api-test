/****** Object:  [NOMINATION]    Script Date: 10/15/2021 7:53:00 AM ******/
ALTER TABLE component.NOMINATION
ADD IDENTIFIER VARCHAR (50) NULL;
GO
/****** Object:  Index [IX_IDENTIFIER]    Script Date: 10/15/2021 7:53:00 AM ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE NAME = N'IX_IDENTIFIER') BEGIN
DROP INDEX [IX_IDENTIFIER] ON [component].[NOMINATION]
END
GO
/****** Object:  Index [IX_IDENTIFIER]    Script Date: 10/15/2021 7:53:00 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_IDENTIFIER] ON component.NOMINATION (IDENTIFIER)
WHERE IDENTIFIER IS NOT NULL;
GO