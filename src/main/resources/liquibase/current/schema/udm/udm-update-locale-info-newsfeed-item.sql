/*
Run this script on:

        52.23.232.184.udm_upgrade    -  This database will be modified

to synchronize it with:

        52.23.232.184.local_udm

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 10/20/2017 11:24:47 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LOCALE_INFO_4] from [component].[LOCALE_INFO]'
GO
DROP INDEX [IX_LOCALE_INFO_4] ON [component].[LOCALE_INFO]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[NEWSFEED_ITEM]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[NEWSFEED_ITEM] ALTER COLUMN [TARGET_TABLE] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[LOCALE_INFO]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[LOCALE_INFO] ADD
[LOCALE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[LOCALE_INFO] ALTER COLUMN [LOCALE_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO
