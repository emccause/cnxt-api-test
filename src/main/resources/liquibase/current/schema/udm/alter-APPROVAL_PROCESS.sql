alter table component.APPROVAL_PROCESS add TARGET_TABLE nvarchar(128) null
alter table component.APPROVAL_PROCESS drop column TARGET_CLASSNAME

alter table component.HISTORY_APPROVAL_PROCESS add TARGET_TABLE nvarchar(128) null
alter table component.HISTORY_APPROVAL_PROCESS drop column TARGET_CLASSNAME


/****** Object:  Index [IX_APPROVAL_PROCESS_1]    Script Date: 9/18/2017 8:50:45 AM ******/
DROP INDEX [IX_APPROVAL_PROCESS_1] ON [component].[APPROVAL_PROCESS]
GO

/****** Object:  Index [IX_APPROVAL_PROCESS_1]    Script Date: 9/18/2017 8:50:45 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_APPROVAL_PROCESS_1] ON [component].[APPROVAL_PROCESS]
(
    [APPROVAL_PROCESS_TYPE_CODE] ASC,
    [TARGET_TABLE] ASC,
    [TARGET_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Trigger [component].[TR_APPROVAL_PROCESS_DU_1]    Script Date: 9/18/2017 8:26:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================  NAME  ======================================
-- TR_APPROVAL_PROCESS_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_APPROVAL_PROCESS_DU_1]
ON [component].[APPROVAL_PROCESS]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_APPROVAL_PROCESS (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    APPROVAL_PROCESS_TYPE_CODE
,    PROGRAM_ID
,    TARGET_ID
,    TARGET_TABLE
,    PENDING_LEVEL
,    LAST_APPROVAL_DATE
,    LAST_APPROVAL_ID
,    STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    APPROVAL_PROCESS_TYPE_CODE
,    PROGRAM_ID
,    TARGET_ID
,    TARGET_TABLE
,    PENDING_LEVEL
,    LAST_APPROVAL_DATE
,    LAST_APPROVAL_ID
,    STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''APPROVAL_PROCESS''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END