/*
Run this script on:

        52.23.232.184.udm_upgrade    -  This database will be modified

to synchronize it with:

        52.23.232.184.local_udm

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 10/20/2017 11:20:42 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[EARNINGS_PAYOUT]'
GO
ALTER TABLE [component].[EARNINGS_PAYOUT] DROP CONSTRAINT [FK_EARNINGS_PAYOUT_ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Updating ACTIVITY_BUSINESS_RULE_ID on [component].[EARNINGS_PAYOUT]'
GO
UPDATE EP
SET ACTIVITY_BUSINESS_RULE_ID = ABR.ID
FROM component.EARNINGS_PAYOUT EP
INNER JOIN component.ACTIVITY_BUSINESS_RULE ABR
ON EP.ACTIVITY_BUSINESS_RULE_ID = ABR.ACTIVITY_BUSINESS_RULE_ID
AND ABR.SUCCESSOR_ID IS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[RANK]'
GO
ALTER TABLE [component].[RANK] DROP CONSTRAINT [FK_RANK_ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[TRANSACTION_HEADER_EARNINGS]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS] DROP CONSTRAINT [FK_TRANSACTION_HEADER_EARNINGS_ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Updating ACTIVITY_BUSINESS_RULE_ID on [component].[TRANSACTION_HEADER_EARNINGS]'
GO
UPDATE THE
SET ACTIVITY_BUSINESS_RULE_ID = ABR.ID
FROM component.TRANSACTION_HEADER_EARNINGS THE
INNER JOIN component.ACTIVITY_BUSINESS_RULE ABR
ON THE.ACTIVITY_BUSINESS_RULE_ID = ABR.ACTIVITY_BUSINESS_RULE_ID
AND ABR.SUCCESSOR_ID IS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT] DROP CONSTRAINT [FK_TRANSACTION_HEADER_OBJECTIVE_RESULT_ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[TRANSACTION_RESULT]'
GO
ALTER TABLE [component].[TRANSACTION_RESULT] DROP CONSTRAINT [FK_TRANSACTION_RESULT_PROJECT]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[LIKES]'
GO
ALTER TABLE [component].[LIKES] DROP CONSTRAINT [FK_LIKES_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [CK_BUDGET_MISC_VF_NAME]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [PK_BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [DF_BUDGET_MISC_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [DF_BUDGET_MISC_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [DF_BUDGET_MISC_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] DROP CONSTRAINT [DF_BUDGET_MISC_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] DROP CONSTRAINT [PK_NOMINATION_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] DROP CONSTRAINT [DF_NOMINATION_MISC_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] DROP CONSTRAINT [DF_NOMINATION_MISC_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] DROP CONSTRAINT [DF_NOMINATION_MISC_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] DROP CONSTRAINT [DF_NOMINATION_MISC_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ACTIVITY_BUSINESS_RULE_5] from [component].[ACTIVITY_BUSINESS_RULE]'
GO
DROP INDEX [IX_ACTIVITY_BUSINESS_RULE_5] ON [component].[ACTIVITY_BUSINESS_RULE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_BUDGET_MISC_VF_NAME_MISC_DATA] from [component].[BUDGET_MISC]'
GO
DROP INDEX [IX_BUDGET_MISC_VF_NAME_MISC_DATA] ON [component].[BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_BUDGET_MISC_2] from [component].[BUDGET_MISC]'
GO
DROP INDEX [IX_BUDGET_MISC_2] ON [component].[BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_HISTORY_PAX_GROUP_ORG_ROLE_CODE_THRU_DATE] from [component].[HISTORY_PAX_GROUP]'
GO
DROP INDEX [IX_HISTORY_PAX_GROUP_ORG_ROLE_CODE_THRU_DATE] ON [component].[HISTORY_PAX_GROUP]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LIKES_1] from [component].[LIKES]'
GO
DROP INDEX [IX_LIKES_1] ON [component].[LIKES]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RANK_1] from [component].[RANK]'
GO
DROP INDEX [IX_RANK_1] ON [component].[RANK]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TRANSACTION_HEADER_OBJECTIVE_RESULT_3] from [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]'
GO
DROP INDEX [IX_TRANSACTION_HEADER_OBJECTIVE_RESULT_3] ON [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_BUDGET_MISC_1] from [component].[BUDGET_MISC]'
GO
DROP INDEX [IX_BUDGET_MISC_1] ON [component].[BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_ACTIVITY_BUSINESS_RULE_IU_1] from [component].[ACTIVITY_BUSINESS_RULE]'
GO
DROP TRIGGER [component].[TR_ACTIVITY_BUSINESS_RULE_IU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_BUDGET_MISC_DU_1] from [component].[BUDGET_MISC]'
GO
DROP TRIGGER [component].[TR_BUDGET_MISC_DU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_BUDGET_MISC_U_1] from [component].[BUDGET_MISC]'
GO
DROP TRIGGER [component].[TR_BUDGET_MISC_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_NOMINATION_MISC_U_1] from [component].[NOMINATION_MISC]'
GO
DROP TRIGGER [component].[TR_NOMINATION_MISC_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Cleaning up [component].[ACTIVITY_BUSINESS_RULE]'
GO
DELETE FROM component.ACTIVITY_BUSINESS_RULE WHERE ID NOT IN (
    SELECT DISTINCT ABR.ID
    FROM component.EARNINGS_PAYOUT EP
    INNER JOIN component.ACTIVITY_BUSINESS_RULE ABR
        ON EP.ACTIVITY_BUSINESS_RULE_ID = ABR.ID
    UNION ALL
    SELECT DISTINCT ABR.ID
    FROM component.TRANSACTION_HEADER_EARNINGS THE
    INNER JOIN component.ACTIVITY_BUSINESS_RULE ABR
        ON THE.ACTIVITY_BUSINESS_RULE_ID = ABR.ID
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Cleaning up [component].[BUSINESS_RULE]'
GO
DELETE FROM component.BUSINESS_RULE
WHERE BUSINESS_RULE_ID NOT IN (
    SELECT BUSINESS_RULE_ID FROM component.ACTIVITY_BUSINESS_RULE
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PAX_ROLE]'
GO
/*==============================================================*/
/* View: VW_PAX_ROLE                                            */
/*==============================================================*/
ALTER view [component].[VW_PAX_ROLE] as
WITH ROLES (
    [LEVEL]
,    ROLE_ID
,    [ROLE]
,    QUALIFIER
,    TARGET_TABLE
,    TARGET_ID
,    SUBJECT_TABLE
,    SUBJECT_ID
,    PAX_ID
) AS (
    --========================================================================================
    --ANCHOR QUERY
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    A.SUBJECT_ID AS PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    WHERE
        A.SUBJECT_TABLE = 'PAX'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    P.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.PAX_TYPE PT
        ON PT.ID = A.SUBJECT_ID
    INNER JOIN COMPONENT.PAX P
        ON P.PAX_TYPE_CODE = PT.PAX_TYPE_CODE
    WHERE
        A.SUBJECT_TABLE = 'PAX_TYPE'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    NULL AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    GP.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.GROUPS_PAX GP
        ON GP.GROUP_ID = A.SUBJECT_ID
    WHERE
        A.SUBJECT_TABLE = 'GROUPS'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.PAX_GROUP_ID = A.SUBJECT_ID
        AND PG.THRU_DATE IS NULL
    WHERE
        A.SUBJECT_TABLE = 'PAX_GROUP'

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(PG.ORGANIZATION_CODE + ':' + PG.ROLE_CODE AS NVARCHAR(255)) AS ROLE_CODE
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    NULL AS TARGET_TABLE
    ,    NULL AS TARGET_ID
    ,    NULL AS SUBJECT_TABLE
    ,    NULL AS SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.PAX_GROUP PG
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_CODE = PG.ROLE_CODE
    WHERE
        PG.THRU_DATE IS NULL

    UNION
    SELECT
        0 AS LEVEL
    ,    R.ROLE_ID
    ,    CAST(R.ROLE_CODE AS NVARCHAR(255)) AS [ROLE]
    ,    '@' + CAST(PG.OWNER_GROUP_ID AS NVARCHAR(20)) AS QUALIFIER
    ,    A.TARGET_TABLE
    ,    A.TARGET_ID
    ,    A.SUBJECT_TABLE
    ,    A.SUBJECT_ID
    ,    PG.PAX_ID
    FROM
        COMPONENT.ACL A
    INNER JOIN COMPONENT.ROLE R
        ON R.ROLE_ID = A.ROLE_ID
    INNER JOIN COMPONENT.ORG_GROUP OG
        ON OG.ORG_GROUP_ID = A.SUBJECT_ID
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.ORGANIZATION_CODE = OG.ORGANIZATION_CODE
        AND PG.ROLE_CODE = OG.ROLE_CODE
        AND PG.THRU_DATE IS NULL
    WHERE
        A.SUBJECT_TABLE = 'ORG_GROUP'
    --========================================================================================
    --RECURSIVE QUERY
 --   UNION ALL

    ----get all the roles assigned to the anchor roles
    --select
    --     resolved.LEVEL + 1 as LEVEL
    --    ,r.ROLE_ID
    --    ,cast(r.ROLE_CODE as nvarchar(255)) as [ROLE]
    --    ,resolved.QUALIFIER
    --    ,coalesce(resolved.TARGET_TABLE, acl.TARGET_TABLE) as TARGET_TABLE
    --    ,coalesce(resolved.TARGET_ID, acl.TARGET_ID) as TARGET_ID
    --    ,acl.SUBJECT_TABLE
    --    ,acl.SUBJECT_ID
    --    ,resolved.PAX_ID
    --from roles resolved
    --      inner join component.acl acl
    --              on acl.subject_id = resolved.role_id
    --            and acl.subject_table = 'ROLE'
    --     inner join component.[role] r on r.role_id = acl.role_id
 --  where acl.role_id <> acl.subject_id
    -- and (
 --           (resolved.target_table is null and acl.target_table is null)
 --           or
 --           (resolved.target_table = acl.target_table and resolved.target_id = acl.target_id)
 --           or
 --           (resolved.target_table is null and acl.target_table is not null and resolved.target_id is null and acl.target_id is not null)
 --        )
)

SELECT DISTINCT
    [LEVEL]
,    ROLE_ID
,    [ROLE]
,    QUALIFIER
,    TARGET_TABLE
,    TARGET_ID
,    SUBJECT_TABLE
,    SUBJECT_ID
,    PAX_ID
FROM
    ROLES
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_TEST_ITEM]'
GO
/*==============================================================*/
/* View: VW_TEST_ITEM                                           */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_ITEM
-- ===========================  DESCRIPTION  ==================================
-- Returns the taken/available tests for the given AUDIENCE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20151014    Created
-- KNIGHTGA     20151029    Updated for latest UDM 4 changes
-- KNIGHTGA        20151109    Updated th status codes to HOLD and APPROVED
-- KNIGHTGA        20160512    Replaced VW_ACL_RESOLVE_ROLES with VW_PAX_ROLE (UDM5)
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_TEST_ITEM] as
SELECT
    T.ID AS TEST_ID
,    T.TEST_TYPE_CODE
,    T.NAME
,    T.DESCRIPTION
,    T.CAN_RESUME
,    T.FROM_DATE
,    T.THRU_DATE
,    DT1.NUMBER_QUESTIONS
,    TR.ID AS TEST_RESULT_ID
,    TH.ACTIVITY_DATE AS TAKEN_DATE
,    TR.COMPLETION_DATE
,    TR.CORRECT_COUNT
,    TR.CORRECT_PERCENTAGE
,    TR.SCORE
,    TH.STATUS_TYPE_CODE
,    TH.PAX_GROUP_ID
,    ACL.PAX_ID
FROM
    COMPONENT.TEST T
INNER JOIN (
    SELECT
        T.ID AS TEST_ID
    ,    COUNT(TQ.ID) AS NUMBER_QUESTIONS
    FROM
        COMPONENT.TEST T
    INNER JOIN COMPONENT.TEST_SECTION TS
        ON TS.TEST_ID = T.ID
        AND TS.STATUS_TYPE_CODE = 'ACTIVE'
    INNER JOIN COMPONENT.TEST_QUESTION TQ
        ON TQ.TEST_SECTION_ID = TS.ID
        AND TQ.STATUS_TYPE_CODE = 'ACTIVE'
    GROUP BY T.ID
    ) AS DT1
    ON DT1.TEST_ID = T.ID
INNER JOIN COMPONENT.VW_PAX_ROLE ACL
    ON ACL.TARGET_ID = T.ID
    AND ACL.TARGET_TABLE = 'TEST'
    AND ACL.ROLE = 'AUDIENCE'
LEFT JOIN (
    SELECT
        TR.TEST_ID
    ,    PG.PAX_ID
    ,    MAX(TR.ID) AS TEST_RESULT_ID
    ,    COUNT(TR.ID) AS NUMBER_ATTEMPTS
    FROM
        COMPONENT.TEST_RESULT TR
    INNER JOIN COMPONENT.TRANSACTION_HEADER TH
        ON TH.ID = TR.TRANSACTION_HEADER_ID
        AND TH.STATUS_TYPE_CODE IN ('HOLD', 'APPROVED')
    INNER JOIN COMPONENT.PAX_GROUP PG
        ON PG.PAX_ID = TH.PAX_GROUP_ID
    GROUP BY TR.TEST_ID, PG.PAX_ID
    ) AS DT2
    ON DT2.TEST_ID = T.ID
    AND DT2.PAX_ID = ACL.PAX_ID
LEFT JOIN COMPONENT.TEST_RESULT TR
    INNER JOIN COMPONENT.TRANSACTION_HEADER TH
        ON TH.ID = TR.TRANSACTION_HEADER_ID
        AND TH.STATUS_TYPE_CODE IN ('HOLD', 'APPROVED')
    ON TR.ID = DT2.TEST_RESULT_ID
WHERE    T.STATUS_TYPE_CODE = 'ACTIVE'
AND    ((TR.ID IS NULL 
    AND GETDATE() BETWEEN T.FROM_DATE AND ISNULL(T.THRU_DATE, GETDATE()))
OR
    (TR.ID IS NOT NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT] DROP
COLUMN [VERSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TRANSACTION_HEADER_OBJECTIVE_RESULT_3] on [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]'
GO
CREATE NONCLUSTERED INDEX [IX_TRANSACTION_HEADER_OBJECTIVE_RESULT_3] ON [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT] ([ACTIVITY_BUSINESS_RULE_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[TRANSLATABLE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[TRANSLATABLE] ALTER COLUMN [STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[PROGRAM]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM] ADD
[CLOSE_DATE] [datetime2] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[PAYOUT_ITEM]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PAYOUT_ITEM] ADD
[CARD_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[TRANSACTION_HEADER_EARNINGS]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS] DROP
COLUMN [VERSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[EARNINGS_PAYOUT]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[EARNINGS_PAYOUT] DROP
COLUMN [VERSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_DATABUS_PROGRAM_ISSUANCE_REPORT]'
GO
--ALTER  TABLE component.TRANSACTION_HEADER_EARNINGS ADD PROGRAM_ID BIGINT

ALTER PROCEDURE [component].[UP_DATABUS_PROGRAM_ISSUANCE_REPORT] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'paxId',
  @sortDir              NVARCHAR(1) = 'A',
  @programId            BIGINT
) AS BEGIN
  SET NOCOUNT ON

  DECLARE @recordCount INT,
  @pageCount INT,
  @startId INT,
  @endId INT

  CREATE TABLE #tempTable(
    TEMP_TABLE_ID INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    PAX_ID                        BIGINT,
    PAX_GROUP_ID                BIGINT,
    FIRST_NAME                    NVARCHAR(255),
    LAST_NAME                    NVARCHAR(255),
    EARNINGS_ID                    BIGINT,
    EARNINGS_AMOUNT                DECIMAL(10,2),
    PAYOUT_ID                    BIGINT,
    PAYOUT_DATE                    DATETIME2(7),
    PAYOUT_STATUS                NVARCHAR(255),
    PAYOUT_PENDING_AMOUNT        DECIMAL(10,2),
    PAYOUT_ISSUED_AMOUNT            DECIMAL(10,2)
  )

  CREATE TABLE #ordered (
    [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    data_id INT                 NOT NULL
  )

  INSERT INTO #tempTable(PAX_ID, PAX_GROUP_ID, FIRST_NAME, LAST_NAME, EARNINGS_ID, EARNINGS_AMOUNT)
        select p.pax_id, pg.pax_group_id, p.first_name, p.last_name, e.earnings_id, e.earnings_amount
        from component.TRANSACTION_HEADER_EARNINGS the
        join component.TRANSACTION_HEADER tr on tr.ID = the.TRANSACTION_HEADER_ID
        join component.PAX_GROUP pg on tr.PAX_GROUP_ID = pg.PAX_GROUP_ID
        join component.PAX p on p.PAX_ID = pg.PAX_ID
        join component.EARNINGS e on the.EARNINGS_ID = e.EARNINGS_ID
        where the.PROGRAM_ID = @programId;

  UPDATE #tempTable
  SET PAYOUT_ID = p.PAYOUT_ID,
  PAYOUT_DATE = p.PAYOUT_DATE,
  PAYOUT_STATUS = p.STATUS_TYPE_CODE,
  PAYOUT_ISSUED_AMOUNT = CASE WHEN p.STATUS_TYPE_CODE = 'ISSUED' THEN p.PAYOUT_AMOUNT END,
  PAYOUT_PENDING_AMOUNT = CASE WHEN p.STATUS_TYPE_CODE in ('PENDING','PROCESSED','IN_PROGRESS','REQUESTED') THEN p.PAYOUT_AMOUNT END
  FROM #tempTable tt
  JOIN component.EARNINGS_PAYOUT ep on EP.EARNINGS_ID = tt.EARNINGS_ID
  JOIN component.PAYOUT p on p.PAYOUT_ID = ep.PAYOUT_ID

  INSERT INTO #ordered (data_id)
        SELECT tt.TEMP_TABLE_ID
        FROM #tempTable tt
        ORDER BY
          CASE @sortDir
          WHEN 'D'
            THEN
              CASE @sortBy
              WHEN 'paxId'
                THEN tt.pax_id
              WHEN 'firstName'
                THEN tt.first_name
              WHEN 'lastName'
                THEN tt.last_name
              END
          END DESC,
          CASE @sortDir
          WHEN 'A'
            THEN
              CASE @sortBy
                WHEN 'paxId'
                THEN tt.pax_id
              WHEN 'firstName'
                THEN tt.first_name
              WHEN 'lastName'
                THEN tt.last_name
              END
          END ASC

  SELECT @recordCount = @@ROWCOUNT

  IF @recordsPerPage = -1
    BEGIN
      SET @recordsPerPage = @recordCount
      SET @pageNumber = 1
    END

  IF @recordsPerPage = 0
    BEGIN
      SET @recordsPerPage = 1
    END

  --Make sure the requested page number is within the valid range of pages (if not, then fix it)
  SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
  IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
  IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

  SET @endId = @pageNumber * @recordsPerPage
  SET @startId = (@endId - @recordsPerPage) + 1

  SELECT PAX_ID,
    PAX_GROUP_ID,
    FIRST_NAME,
    LAST_NAME,
    EARNINGS_ID,
    EARNINGS_AMOUNT,
    PAYOUT_ID,
    PAYOUT_DATE,
    PAYOUT_STATUS,
    PAYOUT_PENDING_AMOUNT,
    PAYOUT_ISSUED_AMOUNT,
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
  FROM #ordered o
    INNER JOIN #tempTable tt ON tt.TEMP_TABLE_ID = o.data_id
  WHERE o.id BETWEEN @startId AND @endId
  ORDER BY o.id

  DROP TABLE #tempTable
  DROP TABLE #ordered
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[MESSAGE_TYPE]'
GO
CREATE TABLE [component].[MESSAGE_TYPE]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MESSAGE_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MESSAGE_TYPE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MESSAGE_TYPE_DESC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_SEQUENCE] [int] NULL,
[DISPLAY_STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_MESSAGE_TYPE_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_MESSAGE_TYPE_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_MESSAGE_TYPE_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_MESSAGE_TYPE_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_MESSAGE_TYPE] on [component].[MESSAGE_TYPE]'
GO
ALTER TABLE [component].[MESSAGE_TYPE] ADD CONSTRAINT [PK_MESSAGE_TYPE] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_MESSAGE_TYPE_1] on [component].[MESSAGE_TYPE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MESSAGE_TYPE_1] ON [component].[MESSAGE_TYPE] ([MESSAGE_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_MESSAGE_TYPE_U_1] on [component].[MESSAGE_TYPE]'
GO
-- ==============================  NAME  ======================================
-- TR_MESSAGE_TYPE_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_MESSAGE_TYPE_U_1]
ON [component].[MESSAGE_TYPE]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    MESSAGE_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    MESSAGE_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[SUB_PROJECT]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[SUB_PROJECT] ADD
[FROM_DATE] [datetime2] NULL,
[THRU_DATE] [datetime2] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S) AND/OR A DELIMITED LIST OF PAX_ID(S)
-- RETURN A DISTINCT LIST OF PAX, PROGRAM (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
-- ERICKSRT     20150811    CHANGE PROGRAM_ACTIVITY_ID to PROGRAM_ID
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX]
    @delim_list_group VARCHAR(4000)
,    @delim_list_pax VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @wrk TABLE (pax_id BIGINT, PROGRAM_id BIGINT)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- FETCH ID(S)
INSERT INTO @wrk (pax_id, PROGRAM_id) SELECT pax_id, PROGRAM_id FROM UF_ASSOCIATION_FETCH_PROGRAM_BY_LIST_GROUP_PAX (@delim_list_group, @delim_list_pax)

-- GET DISTINCT LIST
SELECT DISTINCT
    PAX.PAX_ID
,    PAX.LANGUAGE_CODE
,    PAX.PREFERRED_LOCALE
,    PAX.CONTROL_NUM
,    PAX.SSN
,    PAX.FIRST_NAME
,    PAX.MIDDLE_NAME
,    PAX.LAST_NAME
,    PAX.NAME_PREFIX
,    PAX.NAME_SUFFIX
,    PAX.COMPANY_NAME
,    PAX.TAX_NUM
,    PAX.ALTERNATE_NAME
,    PAX.[HASH]
,    PROGRAM.PROGRAM_ID
,    PROGRAM.PROGRAM_ID
,    PROGRAM.PROGRAM_NAME
,    PROGRAM.PROGRAM_DESC
FROM    @wrk AS WRK
INNER JOIN PAX
    ON WRK.pax_id = PAX.PAX_ID
INNER JOIN PAX_GROUP
    ON PAX.PAX_ID = PAX_GROUP.PAX_ID
    AND GETDATE() BETWEEN PAX_GROUP.FROM_DATE AND ISNULL(PAX_GROUP.THRU_DATE, GETDATE())
LEFT OUTER JOIN PROGRAM
    ON WRK.PROGRAM_id = PROGRAM.PROGRAM_ID
    AND GETDATE() BETWEEN PROGRAM.FROM_DATE AND ISNULL(PROGRAM.THRU_DATE, GETDATE())
ORDER BY
    PAX.PAX_ID
,    PROGRAM.PROGRAM_NAME

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
CREATE TABLE [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TRANSACTION_HEADER_EARNINGS_ID] [bigint] NOT NULL,
[MESSAGE_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MESSAGE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_TRANSACTION_HEADER_EARNINGS_MESSAGE_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_TRANSACTION_HEADER_EARNINGS_MESSAGE_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_TRANSACTION_HEADER_EARNINGS_MESSAGE_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_TRANSACTION_HEADER_EARNINGS_MESSAGE_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_TRANSACTION_HEADER_EARNINGS_MESSAGE] on [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE] ADD CONSTRAINT [PK_TRANSACTION_HEADER_EARNINGS_MESSAGE] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TRANSACTION_HEADER_EARNINGS_MESSAGE_1] on [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
CREATE NONCLUSTERED INDEX [IX_TRANSACTION_HEADER_EARNINGS_MESSAGE_1] ON [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE] ([MESSAGE_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TRANSACTION_HEADER_EARNINGS_MESSAGE_2] on [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
CREATE NONCLUSTERED INDEX [IX_TRANSACTION_HEADER_EARNINGS_MESSAGE_2] ON [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE] ([TRANSACTION_HEADER_EARNINGS_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_TRANSACTION_HEADER_EARNINGS_MESSAGE_U_1] on [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
-- ==============================  NAME  ======================================
-- TR_TRANSACTION_HEADER_EARNINGS_MESSAGE_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20170622    Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_TRANSACTION_HEADER_EARNINGS_MESSAGE_U_1]
ON [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    TRANSACTION_HEADER_EARNINGS_MESSAGE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    TRANSACTION_HEADER_EARNINGS_MESSAGE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_GROUPS_GROUPS]'
GO
/*==============================================================*/
/* View: VW_GROUPS_GROUPS                                       */
/*==============================================================*/
ALTER view [component].[VW_GROUPS_GROUPS] (GROUP_ID_1, GROUP_ID_2, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS GROUP_ID_1
,       FK2 AS GROUP_ID_2
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1000
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PROXY_PAX_PAX]'
GO
/*==============================================================*/
/* View: VW_PROXY_PAX_PAX                                       */
/*==============================================================*/
ALTER view [component].[VW_PROXY_PAX_PAX] as
SELECT  FK1 AS PROXY_PAX_ID
,       FK2 AS PAX_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2100
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_SPIN_PAX_TOKENS]'
GO
-- ==============================  NAME  ======================================
-- VW_SPIN_PAX_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by pax_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20111212    Created
-- KNIGHTGA     20150818    Updated for UDM 3
-- KNIGHTGA     20170113    Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
-- KNIGHTGA     20170126    Added TOKEN_AWARDS column
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_PAX_TOKENS] as
SELECT
    TH.PROGRAM_ID AS PROGRAM_ID
,    PG.PAX_ID AS PAX_ID
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN case when TH.SUB_TYPE_CODE = 'IOU_TOKEN' then -1 else 1 end else 0 END) AS TOKENS_AVAILABLE
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NOT NULL THEN 1 ELSE 0 END) AS TOKENS_USED
,    cast(SUM(CASE WHEN PGA.ID IS NOT NULL THEN pga.AWARD_AMOUNT ELSE 0 END) as INTEGER) AS TOKEN_AWARDS
,    count(th.id) AS TOKENS_TOTAL
FROM
    TRANSACTION_HEADER TH
INNER JOIN SPIN_TOKEN st
    ON st.TRANSACTION_HEADER_ID = TH.ID
INNER JOIN PAX_GROUP PG
    ON PG.PAX_GROUP_ID = TH.PAX_GROUP_ID
LEFT JOIN SPIN_AWARD sa
    ON sa.ID = st.SPIN_AWARD_ID
LEFT JOIN PROGRAM_GAME_AWARD pga
    ON pga.ID = sa.PROGRAM_GAME_AWARD_ID
WHERE TH.TYPE_CODE = 'SPIN_TOKEN'
  AND TH.STATUS_TYPE_CODE = 'APPROVED'
  AND TH.SUB_TYPE_CODE in ('SPIN_TOKEN', 'IOU_TOKEN')
GROUP BY
    TH.PROGRAM_ID
,    PG.PAX_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_SPIN_AWARD_POOL]'
GO
-- ==============================  NAME  ======================================
-- VW_SPIN_AWARD_POOL
-- ===========================  DESCRIPTION  ==================================
-- Returns award pool information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20111212    Created
-- KNIGHTGA     20150818    Updated for UDM 3
-- KNIGHTGA     20170113    Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_AWARD_POOL] as
SELECT
    PG.PROGRAM_ID
,    CAST(PGA.AWARD_AMOUNT AS INT) AS DENOMINATION
,    MAX(PGA.QUANTITY) AS QUANTITY
,    SUM(CASE WHEN th.ID IS NOT NULL AND TH.PAX_GROUP_ID IS NULL THEN 1 ELSE 0 END) AS AWARDS_AVAILABLE
,    SUM(CASE WHEN th.ID IS NOT NULL AND TH.PAX_GROUP_ID IS NOT NULL THEN 1 ELSE 0 END) AS AWARDS_USED
FROM
    COMPONENT.PROGRAM_GAME PG
INNER JOIN COMPONENT.PROGRAM_GAME_AWARD PGA
    ON PGA.PROGRAM_GAME_ID = PG.ID
left join component.spin_award sa
       on sa.program_game_award_id = pga.id
LEFT JOIN COMPONENT.TRANSACTION_HEADER TH 
    ON TH.ID = sa.TRANSACTION_HEADER_ID
    AND TH.STATUS_TYPE_CODE IN ('APPROVED', 'HOLD')
GROUP BY
    PG.PROGRAM_ID
,    CAST(PGA.AWARD_AMOUNT AS INT)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PAYOUT_WS01]'
GO
-- ==============================  NAME  ======================================
-- VW_PAYOUT_WS01
-- ===========================  DESCRIPTION  ==================================
-- Returns payout data for creating a WS01 record
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20170213    Created
-- KNIGHTGA        20170314    Renamed column TRANS_COMMENT to TRANSACTION_DESCRIPTION_OVERRIDE to match ws01 spec
-- KNIGHTGA        20170413    Fixed column names that end with numbers (INDICATIVE_DATA_1 -> INDICATIVE_DATA1)
-- KNIGHTGA        20170508    Multiply payout.amount by payout_item.payout_item_amount
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_PAYOUT_WS01] as
select pay.payout_amount * pi.payout_item_amount as POINTS
      ,pay.payout_id as INDICATIVE_DATA1
      ,coalesce(pm_pid.misc_data, p.control_num) as PARTICIPANT_ID
      ,p.last_name as PARTICIPANT_LAST_NAME
      ,p.first_name as PARTICIPANT_FIRST_NAME
      ,left(p.MIDDLE_NAME,1) as PARTICIPANT_MIDDLE_INITIAL
      ,p.name_suffix as PARTICIPANT_NAME_SUFFIX
      ,p.name_prefix as PARTICIPANT_NAME_PREFIX
      ,p.language_code as LANGUAGE_ID
      ,case when ha.preferred = 'Y' then ha.address1
            when ba.preferred = 'Y' then ba.address1
            when ba.pax_id is not null then ba.address1
            else ha.address1
       end as STREET_ADDRESS1
      ,case when ha.preferred = 'Y' then ha.address2
            when ba.preferred = 'Y' then ba.address2
            when ba.pax_id is not null then ba.address2
            else ha.address2
       end as STREET_ADDRESS2
      ,case when ha.preferred = 'Y' then ha.city
            when ba.preferred = 'Y' then ba.city
            when ba.pax_id is not null then ba.city
            else ha.city
       end as CITY
      ,case when ha.preferred = 'Y' then upper(ha.state)
            when ba.preferred = 'Y' then upper(ba.state)
            when ba.pax_id is not null then upper(ba.state)
            else upper(ha.state)
       end as STATE_PROVINCE
      ,case when ha.preferred = 'Y' then ha.zip
            when ba.preferred = 'Y' then ba.zip
            when ba.pax_id is not null then ba.zip
            else ha.zip
       end as ZIP
      ,case when ha.preferred = 'Y' then ha.country_code
            when ba.preferred = 'Y' then ba.country_code
            when ba.pax_id is not null then ba.country_code
            else ha.country_code
       end as COUNTRY
      ,case when he.preferred = 'Y' then he.email_address
            when be.preferred = 'Y' then be.email_address
            when be.pax_id is not null then be.email_address
            else he.email_address
       end as EMAIL_ADDRESS
      ,p.ssn as PARTICIPANT_SSN
      ,ph_hom.phone as PARTICIPANT_NIGHT_PHONE
      ,ph_bus.phone as PARTICIPANT_DAY_PHONE
      ,pay.payout_desc as TRANSACTION_DESCRIPTION_OVERRIDE
      ,pa.card_num as CARD_ABS_ACCOUNT_NUMBER
      ,pay.sub_project_number as SUB_PROJECT_NUMBER
      ,pay.project_number as PROJECT_NUMBER
      ,pi.PRODUCT_CODE
      ,pi.BUSINESS_UNIT
      ,pay.STATUS_TYPE_CODE
      ,pay.PAX_ID
      ,pay.PAYOUT_ID
  from component.payout pay
       inner join component.payout_item pi on pi.PAYOUT_ITEM_ID = pay.PAYOUT_ITEM_ID
       inner join component.PAYOUT_VENDOR pv
              on pv.PAYOUT_VENDOR_ID = pi.PAYOUT_VENDOR_ID
             and pv.PAYOUT_VENDOR_NAME = 'ABS'
       inner join component.pax p on p.pax_id = pay.pax_id
       left join component.address ha
              on ha.pax_id = p.pax_id
             and ha.address_type_code = 'HOME'
       left join component.address ba
              on ba.pax_id = p.pax_id
             and ba.address_type_code = 'BUSINESS'
       left join component.email he
              on he.pax_id = p.pax_id
             and he.email_type_code = 'HOME'
       left join component.email be
              on be.pax_id = p.pax_id
             and be.email_type_code = 'BUSINESS'
       left join component.phone ph_hom
              on ph_hom.pax_id = p.pax_id
             and ph_hom.phone_type_code = 'HOME'
       left join component.phone ph_bus
              on ph_bus.pax_id = p.pax_id
             and ph_bus.phone_type_code = 'BUSINESS'
       left join component.pax_account pa
              on pa.pax_id = p.pax_id
             and pa.card_type_code = pay.payout_type_code
       left join component.pax_misc pm_pid
              on pm_pid.pax_id = p.pax_id
             and pm_pid.vf_name = 'ABS_PID'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_BUDGET_MISC]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_BUDGET_MISC] ADD
[MISC_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[HISTORY_BUDGET_MISC].[BUDGET_MISC_ID]', N'ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_BUDGET_MISC] ALTER COLUMN [MISC_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HISTORY_BUDGET_MISC_2] on [component].[HISTORY_BUDGET_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_HISTORY_BUDGET_MISC_2] ON [component].[HISTORY_BUDGET_MISC] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [component].[BUDGET_MISC]'
GO
CREATE TABLE [component].[RG_Recovery_1_BUDGET_MISC]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[BUDGET_ID] [bigint] NOT NULL,
[VF_NAME] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MISC_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MISC_DATE] [datetime2] NULL,
[MISC_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_BUDGET_MISC_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BUDGET_MISC_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_BUDGET_MISC_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_BUDGET_MISC_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_1_BUDGET_MISC] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [component].[RG_Recovery_1_BUDGET_MISC]([ID], [BUDGET_ID], [VF_NAME], [MISC_DATA], [MISC_DATE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID]) SELECT [BUDGET_MISC_ID], [BUDGET_ID], [VF_NAME], [MISC_DATA], [MISC_DATE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID] FROM [component].[BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_1_BUDGET_MISC] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [component].[BUDGET_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[RG_Recovery_1_BUDGET_MISC]', N'BUDGET_MISC', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BUDGET_MISC] on [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] ADD CONSTRAINT [PK_BUDGET_MISC] PRIMARY KEY NONCLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BUDGET_MISC_2] on [component].[BUDGET_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_BUDGET_MISC_2] ON [component].[BUDGET_MISC] ([BUDGET_ID], [MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BUDGET_MISC_1] on [component].[BUDGET_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_BUDGET_MISC_1] ON [component].[BUDGET_MISC] ([BUDGET_ID], [VF_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_BUDGET_MISC_DU_1] on [component].[BUDGET_MISC]'
GO
-- ==============================  NAME  ======================================
-- TR_BUDGET_MISC_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta        20070207                Created
-- ericksrt        20150819                Removed UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_BUDGET_MISC_DU_1]
ON [component].[BUDGET_MISC]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

--EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

--SET @change_id = ISNULL(@change_id, USER_NAME())
SET @change_id = USER_NAME()

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_BUDGET_MISC (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    BUDGET_ID
,    VF_NAME
,    MISC_DATA
,    MISC_DATE
,   MISC_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    BUDGET_ID
,    VF_NAME
,    MISC_DATA
,    MISC_DATE
,    MISC_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''BUDGET_MISC''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_BUDGET_MISC_U_1] on [component].[BUDGET_MISC]'
GO
-- ==============================  NAME  ======================================
-- TR_BUDGET_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta       20040101                Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_BUDGET_MISC_U_1]
ON [component].[BUDGET_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    BUDGET_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    BUDGET_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_BUDGET_BALANCE]'
GO
-- ==============================  NAME  ======================================
-- VW_BUDGET_BALANCE
-- ===========================  DESCRIPTION  ==================================
-- Return the budget balance
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20150914    Created
-- KNIGHTGA     20170116    Updated for the new UDM 5 spin tables (SPIN_AWARD)
--
-- ===========================  DECLARATIONS  =================================

ALTER VIEW [component].[VW_BUDGET_BALANCE]
AS

/*
select * from component.VW_BUDGET_BALANCE
select * from component.VW_BUDGET_BALANCE where program_id = 50

select bb.* 
  from component.VW_BUDGET_BALANCE bb
       inner join component.program_budget pb on pb.budget_id = bb.budget_id
 where pb.program_id = 50
*/

SELECT b.ID as BUDGET_ID
      ,isnull(discInfo.FUNDING, 0) as FUNDING
      ,isnull(discInfo.DEBITS, 0) + isnull(trInfo.DEBITS, 0) + isnull(saInfo.DEBITS, 0) as DEBITS
      ,isnull(discInfo.CREDITS, 0) + isnull(trInfo.CREDITS, 0) as CREDITS
      ,isnull(discInfo.BALANCE, 0) + isnull(trInfo.BALANCE, 0) + isnull(saInfo.BALANCE, 0) as BALANCE
  FROM component.BUDGET b
       left join (
                    select b.ID as budget_id
                          ,sum(case when th.sub_type_code = 'FUND' and disc.BUDGET_ID_DEBIT = b.id then -disc.amount 
                                    when th.sub_type_code = 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount
                                    else 0 
                                end) as FUNDING
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_CREDIT = b.id then disc.amount 
                                    else 0 
                               end ) as CREDITS
                          ,sum(case when th.sub_type_code != 'FUND' and disc.BUDGET_ID_DEBIT = b.id then disc.amount 
                                    else 0 
                               end ) as DEBITS
                          ,sum(case when disc.BUDGET_ID_DEBIT = b.id then -disc.amount else disc.amount end) as BALANCE
                     FROM component.DISCRETIONARY disc 
                          inner join component.BUDGET b on b.id = isnull(disc.BUDGET_ID_CREDIT, disc.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = disc.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as discInfo
              ON discInfo.budget_id = b.ID
       left join (
                    select b.ID as budget_id
                          ,sum(case when tr.BUDGET_ID_CREDIT = b.id then tr.earnings_amount else 0 end) AS CREDITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then tr.earnings_amount else 0 end) AS DEBITS
                          ,sum(case when tr.BUDGET_ID_DEBIT = b.id then -tr.earnings_amount else tr.earnings_amount end) as BALANCE
                     FROM component.transaction_result tr
                          inner join component.BUDGET b on b.id = isnull(tr.BUDGET_ID_CREDIT, tr.BUDGET_ID_DEBIT)
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = tr.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'PENDING', 'APPROVED')
                     GROUP BY b.ID
                 ) as trInfo
              ON trInfo.budget_id = b.ID
       left join (
                    select sa.BUDGET_ID
                          ,sum(pga.award_amount) as DEBITS
                          ,sum(-pga.award_amount) as BALANCE
                     FROM component.SPIN_AWARD sa                         
                          inner join component.TRANSACTION_HEADER th
                                  on th.ID = sa.TRANSACTION_HEADER_ID 
                                 and th.STATUS_TYPE_CODE in ('HOLD', 'APPROVED')
                          inner join component.program_game_award pga on pga.id = sa.program_game_award_id
                     where sa.BUDGET_ID is not null
                     GROUP BY sa.budget_id
                 ) as saInfo
              ON saInfo.budget_id = b.id
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE]'
GO
-- ==============================  NAME  ======================================
-- UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, RETURN A LIST OF PAX RECORDS TO BE EXCLUDED FROM THE
-- "MAKE FRIENDS" SCREEN
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- BESSBS        20110221    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_GAMIFICATION_MAKE_FRIENDS_EXCLUDE]
    @pax_group_id BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT
    PAX.PAX_ID
,    PAX.CONTROL_NUM
FROM    PAX 
JOIN    PAX_GROUP 
    ON PAX.PAX_ID = PAX_GROUP.PAX_ID
WHERE    PAX_GROUP.PAX_GROUP_ID NOT IN (SELECT [id] FROM UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id, 1, NULL))
AND    PAX_GROUP.ORGANIZATION_CODE <> 'RECG'

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[SALE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[SALE] ADD
[SELLER_NAME] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SELLER_NUMBER] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_DATABUS_RECENT_EARNINGS]'
GO
ALTER PROCEDURE [component].[UP_DATABUS_RECENT_EARNINGS] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'earningDate',
  @sortDir              NVARCHAR(1) = 'A',
  @paxId                BIGINT
) AS BEGIN
  SET NOCOUNT ON

  DECLARE @recordCount INT,
  @pageCount INT,
  @startId INT,
  @endId INT

  CREATE TABLE #tempTable(
    TEMP_TABLE_ID INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    PAX_ID                        BIGINT,
    PAX_GROUP_ID                BIGINT,
    TRANSACTION_HEADER_ID        BIGINT,
    TYPE_CODE                    NVARCHAR(255),
    SUB_TYPE_CODE                NVARCHAR(255),
    EARNINGS_ID                    BIGINT,
    SKU                            NVARCHAR(255),
    PRODUCT_NAME                NVARCHAR(255),
    CUSTOMER_NAME                NVARCHAR(255),
    INVOICE_NUMBER                NVARCHAR(255),
    EARNING_DATE                DATETIME2(7),
    PAYOUT_ID                    BIGINT,
    PAYOUT_AMOUNT                DECIMAL(10,2),
    PAYOUT_STATUS                NVARCHAR(255),
    PAYOUT_DESC                 NVARCHAR(255),
    PROGRAM_ID                    BIGINT,
    PROGRAM_NAME                NVARCHAR(255)
  )

  CREATE TABLE #ordered (
    [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    data_id INT                 NOT NULL
  )

  INSERT INTO #tempTable(PAX_ID, PAX_GROUP_ID, TRANSACTION_HEADER_ID, TYPE_CODE, SUB_TYPE_CODE, EARNINGS_ID, EARNING_DATE,
        PAYOUT_ID, PAYOUT_AMOUNT, PAYOUT_STATUS, PAYOUT_DESC, PROGRAM_ID, PROGRAM_NAME)
        select pg.PAX_ID, pg.PAX_GROUP_ID, th.ID, th.TYPE_CODE, th.SUB_TYPE_CODE, e.EARNINGS_ID, pay.PAYOUT_DATE, pay.PAYOUT_ID, pay.PAYOUT_AMOUNT,
        pay.STATUS_TYPE_CODE, pay.PAYOUT_DESC, pr.PROGRAM_ID, pr.PROGRAM_NAME
from component.TRANSACTION_HEADER th
        join component.PAX_GROUP pg on pg.PAX_GROUP_ID = th.PAX_GROUP_ID
        join component.TRANSACTION_HEADER_EARNINGS the on the.TRANSACTION_HEADER_ID = th.ID
        join component.EARNINGS e on e.EARNINGS_ID = the.EARNINGS_ID
        join component.EARNINGS_PAYOUT ep on ep.EARNINGS_ID = e.EARNINGS_ID
        join component.PAYOUT pay on ep.PAYOUT_ID = pay.PAYOUT_ID
        left outer join component.PROGRAM pr on pr.PROGRAM_ID = the.PROGRAM_ID
        where pg.PAX_ID = @paxId
        and pay.STATUS_TYPE_CODE in ('ISSUED','REQUESTED')
        order by e.CREATE_DATE desc;

  UPDATE #tempTable
  SET CUSTOMER_NAME = s.CUSTOMER_NAME,
  INVOICE_NUMBER = s.INVOICE_NUMBER
  FROM #tempTable tt
  JOIN component.SALE s on s.TRANSACTION_HEADER_ID = tt.TRANSACTION_HEADER_ID

  UPDATE #tempTable
  SET SKU = si.SKU,
  PRODUCT_NAME = prod.PRODUCT_NAME
  FROM #tempTable tt
  JOIN component.SALE_ITEM si on si.TRANSACTION_HEADER_ID = tt.TRANSACTION_HEADER_ID
  left outer join component.PRODUCT prod on si.PRODUCT_ID = prod.PRODUCT_ID


  INSERT INTO #ordered (data_id)
        SELECT tt.TEMP_TABLE_ID
        FROM #tempTable tt
        ORDER BY
          CASE @sortDir
          WHEN 'D'
            THEN
              CASE @sortBy
              WHEN 'earningDate'
                THEN tt.EARNING_DATE
              WHEN 'subType'
                THEN tt.SUB_TYPE_CODE
              WHEN 'programName'
                THEN tt.program_name
              END
          END DESC,
          CASE @sortDir
          WHEN 'A'
            THEN
              CASE @sortBy
              WHEN 'earningDate'
                THEN tt.EARNING_DATE
              WHEN 'subType'
                THEN tt.SUB_TYPE_CODE
              WHEN 'programName'
                THEN tt.program_name
              END
          END ASC

  SELECT @recordCount = @@ROWCOUNT

  PRINT('RECORD_COUNT: ' + convert(varchar, @recordCount));

  IF @recordsPerPage = -1
    BEGIN
      SET @recordsPerPage = @recordCount
      SET @pageNumber = 1
    END

  IF @recordsPerPage = 0
    BEGIN
      SET @recordsPerPage = 1
    END

  --Make sure the requested page number is within the valid range of pages (if not, then fix it)
  SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
  IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
  IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

  SET @endId = @pageNumber * @recordsPerPage
  SET @startId = (@endId - @recordsPerPage) + 1

  SELECT
    PAX_ID,
    PAX_GROUP_ID,
    TRANSACTION_HEADER_ID,
    TYPE_CODE,
    SUB_TYPE_CODE,
    EARNINGS_ID,
    SKU,
    PRODUCT_NAME,
    CUSTOMER_NAME,
    INVOICE_NUMBER,
    EARNING_DATE,
    PAYOUT_ID,
    PAYOUT_AMOUNT,
    PAYOUT_STATUS,
    PAYOUT_DESC,
    PROGRAM_ID,
    PROGRAM_NAME,
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
  FROM #ordered o
    INNER JOIN #tempTable tt ON tt.TEMP_TABLE_ID = o.data_id
  WHERE o.id BETWEEN @startId AND @endId
  ORDER BY o.id

  DROP TABLE #tempTable
  DROP TABLE #ordered
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[LIKES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[LIKES] ALTER COLUMN [TARGET_TABLE] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LIKES_1] on [component].[LIKES]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LIKES_1] ON [component].[LIKES] ([PAX_ID], [TARGET_TABLE], [TARGET_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_TEST_RESULT_ANSWER]'
GO
/*==============================================================*/
/* View: VW_TEST_RESULT_ANSWER                                  */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_ANSWER
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result answers by test question id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20150925    Created
-- KNIGHTGA     20151026    Added NEXT_TEST_SECTION_ID
-- KNIGHTGA     20151028    Updated for latest UDM 4 changes
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_TEST_RESULT_ANSWER] as
SELECT
    TR.TEST_ID
,    TRQ.TEST_RESULT_ID
,    TQ.TEST_SECTION_ID
,    TRQ.ID AS TEST_RESULT_QUESTION_ID
,    TQ.ID AS TEST_QUESTION_ID
,    TRA.ID AS TEST_RESULT_ANSWER_ID
,    TA.ID AS TEST_ANSWER_ID
,    TA.ANSWER_TEXT
,    TA.ANSWER_WEIGHT
,    TA.USER_PROVIDED
,    TRA.USER_RESPONSE
,    TRA.USER_SELECTED
,    TRA.DISPLAY_SEQUENCE
,    TA.NEXT_TEST_SECTION_ID
FROM    
    COMPONENT.TEST_RESULT_ANSWER TRA
INNER JOIN COMPONENT.TEST_ANSWER TA
    ON TRA.TEST_ANSWER_ID = TA.ID
INNER JOIN COMPONENT.TEST_RESULT_QUESTION TRQ
    ON TRQ.ID = TRA.TEST_RESULT_QUESTION_ID
INNER JOIN COMPONENT.TEST_QUESTION TQ
    ON TQ.ID = TRQ.TEST_QUESTION_ID
INNER JOIN COMPONENT.TEST_RESULT TR
    ON TR.ID = TRQ.TEST_RESULT_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES]'
GO
/*==============================================================*/
/* View: VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES    */
/*==============================================================*/
ALTER view [component].[VW_RECOGNITION_CRITERIA_APPLICATION_TEXT_PROPERTIES] (RECOGNITION_CRITERIA_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS RECOGNITION_CRITERIA_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1400
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[FILE_ITEM]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[FILE_ITEM] ALTER COLUMN [TARGET_TABLE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FILE_ITEM_1] on [component].[FILE_ITEM]'
GO
CREATE NONCLUSTERED INDEX [IX_FILE_ITEM_1] ON [component].[FILE_ITEM] ([FILES_ID], [TARGET_ID], [TARGET_TABLE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_FILE_ITEM_U_1] on [component].[FILE_ITEM]'
GO
-- ==============================  NAME  ======================================
-- TR_FILE_ITEM_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        STORY        CHANGE DESCRIPTION
-- dohognta       20040101                Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_FILE_ITEM_U_1]
ON [component].[FILE_ITEM]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    FILE_ITEM T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    FILE_ITEM T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, ACTIVITY_DATE_FROM, ACTIVITY_DATE_THRU
-- RETURN ALL ISSUED PAYOUT AMOUNTS FOR DESCENDANT(S) OF SPECIFIED PAX_GROUP_ID
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20110921    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814    CHANGED STATUS to STATUS_TYPE_CODE
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_PAYOUT_ISSUED_FOR_DESCENDANT_REPORT]
    @pax_group_id BIGINT
,    @activity_date_from DATETIME2
,    @activity_date_thru DATETIME2
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @result_set table (
        pax_group_id BIGINT,
        payout_amount INT,
        levels_remaining INT
    ) 
    
    DECLARE @pax_group_id_descendant BIGINT
    
-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

    DECLARE c_list CURSOR FOR SELECT PAX_GROUP_ID FROM PAX_GROUP WHERE OWNER_GROUP_ID = @pax_group_id
    OPEN c_list
    
    FETCH NEXT FROM c_list INTO @pax_group_id_descendant
    
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        INSERT INTO @result_set (
            pax_group_id
        ,    payout_amount
        )
        SELECT    @pax_group_id_descendant
        ,    SUM(PAYOUT.PAYOUT_AMOUNT) 
        FROM    UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id_descendant, 1, NULL) 
        JOIN PAX_GROUP
            ON [ID] = PAX_GROUP.PAX_GROUP_ID
        JOIN TRANSACTION_HEADER
            ON PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID 
            AND TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
        JOIN TRANSACTION_HEADER_EARNINGS
            ON TRANSACTION_HEADER_EARNINGS.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.[ID]
        JOIN EARNINGS_PAYOUT
            ON EARNINGS_PAYOUT.EARNINGS_ID = TRANSACTION_HEADER_EARNINGS.EARNINGS_ID
        JOIN PAYOUT
            ON PAYOUT.PAYOUT_ID = EARNINGS_PAYOUT.PAYOUT_ID
            AND PAYOUT.STATUS_TYPE_CODE = 'ISSUED'
    
        UPDATE    @result_set
        SET    levels_remaining = (
                    SELECT COUNT(DISTINCT ORG_GROUP_2.ORG_GROUP_ID)
                    FROM     UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id_descendant, 1, 1) FN
                    ,    ORG_GROUP ORG_GROUP_1 -- PARENT
                    ,    ORG_GROUP ORG_GROUP_2 -- CHILD
                    ,    PAX_GROUP
                    WHERE PAX_GROUP.PAX_GROUP_ID = FN.[ID]
                    AND FN.LEVEL <> 0
                    AND PAX_GROUP.ORGANIZATION_CODE = ORG_GROUP_1.ORGANIZATION_CODE
                    AND PAX_GROUP.ROLE_CODE = ORG_GROUP_1.ROLE_CODE
                    AND ORG_GROUP_1.ORG_GROUP_ID = ORG_GROUP_2.OWNER_GROUP_ID
        )
        WHERE    pax_group_id = @pax_group_id_descendant
        AND    payout_amount IS NOT NULL

        FETCH NEXT FROM c_list INTO @pax_group_id_descendant
    END
    
    CLOSE c_list
    DEALLOCATE c_list

    IF (SELECT COUNT(1) FROM @result_set WHERE payout_amount IS NOT NULL) > 0
    BEGIN
        SELECT     result_set.pax_group_id
        ,    result_set.payout_amount
        ,    result_set.levels_remaining
        ,    PAX.COMPANY_NAME AS company_name
        ,    PAX.FIRST_NAME AS first_name
        ,    PAX.LAST_NAME AS last_name
        FROM @result_set result_set
        JOIN PAX_GROUP
            ON result_set.pax_group_id = PAX_GROUP.PAX_GROUP_ID
        JOIN PAX
          ON PAX_GROUP.PAX_ID = PAX.PAX_ID
        WHERE result_set.payout_amount IS NOT NULL
    END
    ELSE
    BEGIN
        SELECT 'No Data' as label, 0 as value
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_DATABUS_BLAST_EMAIL_FIELDS]'
GO
ALTER PROCEDURE [component].[UP_DATABUS_BLAST_EMAIL_FIELDS] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'paxId',
  @sortDir              NVARCHAR(1) = 'A',
  @groupIdList          NVARCHAR(max)

) AS BEGIN
  SET NOCOUNT ON

  DECLARE @recordCount INT,
  @pageCount INT,
  @startId INT,
  @endId INT

  CREATE TABLE #tempTable(
    tempTableId INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [pax_id]             BIGINT,
    [first_name]         NVARCHAR(255),
    [last_name]          NVARCHAR(255),
    [email_address]      NVARCHAR(255),
    [is_unsubscribed]    NVARCHAR(1),
    [client_name]        NVARCHAR(255),
    [ADDRESS1]           NVARCHAR(255),
    [ADDRESS2]           NVARCHAR(255),
    [ADDRESS3]           NVARCHAR(255),
    [CITY]               NVARCHAR(255),
    [STATE]              NVARCHAR(255),
    [ZIP]                NVARCHAR(255),
    [DEALER]             NVARCHAR(255),
    [ZONE]               NVARCHAR(255),
    [DISTRICT]           NVARCHAR(255)
  )

  CREATE TABLE #GROUP_ID_TABLE  (
    INPUT_GROUP_ID NVARCHAR(MAX)
  )

  INSERT INTO #GROUP_ID_TABLE (
    INPUT_GROUP_ID
  )
  SELECT * FROM [component].UF_LIST_TO_TABLE(@groupIdList, ',')

  CREATE TABLE #ordered (
    [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    data_id INT                 NOT NULL
  )

  INSERT INTO #tempTable
  (
    [pax_id],
    [first_name],
    [last_name],
    [email_address],
    [is_unsubscribed]
  )
select distinct
p.PAX_ID, p.first_name, p.last_name,
case when isnull(em_h.PREFERRED, 'N') = 'Y' then em_h.EMAIL_ADDRESS
     when isnull(em_b.PREFERRED, 'N') = 'Y' then em_b.EMAIL_ADDRESS
     else isnull(em_h.email_address, em_b.email_address)
     end as EMAIL_ADDRESS,
isnull(pm.misc_data, 'N') as UNSUBSCRIBED
from component.pax p
join component.groups_pax gp
on p.PAX_ID = gp.PAX_ID
left join component.email em_h on em_h.pax_id = p.pax_id and em_h.EMAIL_TYPE_CODE = 'HOME'
left join component.email em_b on em_b.pax_id = p.pax_id and em_b.EMAIL_TYPE_CODE = 'BUSINESS'
left join component.pax_misc pm on pm.pax_id = p.pax_id and pm.vf_name = 'UNSUBSCRIBED'
where gp.group_id in (Select input_group_id from #GROUP_ID_TABLE)
and (em_h.EMAIL_ID is not null or em_b.EMAIL_ID is not null);

UPDATE #tempTable set [client_name] = (SELECT VALUE from component.APPLICATION_DATA
where key_name = 'client.name');

UPDATE #tempTable
SET
ADDRESS1 = case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.ADDRESS1
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.ADDRESS1
      else isnull(ad_h.ADDRESS1, ad_b.ADDRESS1)
      end,
ADDRESS2 =case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.ADDRESS2
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.ADDRESS2
      else isnull(ad_h.ADDRESS2, ad_b.ADDRESS2)
      end,
ADDRESS3 = case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.ADDRESS3
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.ADDRESS3
      else isnull(ad_h.ADDRESS3, ad_b.ADDRESS3)
      end,
CITY = case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.CITY
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.CITY
      else isnull(ad_h.CITY, ad_b.CITY)
      end,
STATE = case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.STATE
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.STATE
      else isnull(ad_h.STATE, ad_b.STATE)
      end,
ZIP = case when isnull(ad_h.PREFERRED, 'N') = 'Y' then ad_h.ZIP
      when isnull(ad_b.PREFERRED, 'N') = 'Y' then ad_b.ZIP
      else isnull(ad_h.ZIP, ad_b.ZIP)
      end
      from #tempTable tt
      left join component.ADDRESS ad_h on ad_h.pax_id = tt.pax_id and ad_h.ADDRESS_TYPE_CODE = 'HOME'
      left join component.ADDRESS ad_b on ad_b.pax_id = tt.pax_id and ad_b.ADDRESS_TYPE_CODE = 'BUSINESS'

 CREATE TABLE #tempHierarchy(
    tempTableId INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [pax_id]             BIGINT,
    [ROLE_CODE]          NVARCHAR(255),
    [PAX_NAME]           NVARCHAR(255)
  )

  INSERT into #tempHierarchy(pax_id, ROLE_CODE, PAX_NAME)
    (SELECT DISTINCT STARTING_PAX_ID, ROLE_CODE, PAX_NAME FROM [component].[VW_PAX_HIERARCHY_TREE_ABOVE], #tempTable
WHERE #tempTable.PAX_ID = STARTING_PAX_ID)

UPDATE #tempTable SET
DEALER = (SELECT PAX_NAME FROM #tempHierarchy
WHERE ROLE_CODE = 'DEALER' and #tempTable.PAX_ID = #tempHierarchy.pax_id)

UPDATE #tempTable SET
ZONE = (SELECT PAX_NAME FROM #tempHierarchy
WHERE ROLE_CODE = 'ZONE' and #tempTable.PAX_ID = #tempHierarchy.pax_id)

UPDATE #tempTable SET
DISTRICT = (SELECT PAX_NAME FROM #tempHierarchy
WHERE ROLE_CODE = 'DISTRICT' and #tempTable.PAX_ID = #tempHierarchy.pax_id)

   INSERT INTO #ordered (data_id)
        SELECT tt.tempTableId
        FROM #tempTable tt
        ORDER BY
          CASE @sortDir
          WHEN 'D'
            THEN
              CASE @sortBy
              WHEN 'paxId'
                THEN tt.pax_id
              WHEN 'firstName'
                THEN tt.first_name
              WHEN 'lastName'
                THEN tt.last_name
              END
          END DESC,
          CASE @sortDir
          WHEN 'A'
            THEN
              CASE @sortBy
                WHEN 'paxId'
                THEN tt.pax_id
              WHEN 'firstName'
                THEN tt.first_name
              WHEN 'lastName'
                THEN tt.last_name
              END
          END ASC

  SELECT @recordCount = @@ROWCOUNT

  IF @recordsPerPage = -1
    BEGIN
      SET @recordsPerPage = @recordCount
      SET @pageNumber = 1
    END

  IF @recordsPerPage = 0
    BEGIN
      SET @recordsPerPage = 1
    END

  --Make sure the requested page number is within the valid range of pages (if not, then fix it)
  SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
  IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
  IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

  SET @endId = @pageNumber * @recordsPerPage
  SET @startId = (@endId - @recordsPerPage) + 1

  SELECT
    pax_id,
    [first_name],
    [last_name],
    email_address,
    [is_unsubscribed],
    [client_name],
    [ADDRESS1],
    [ADDRESS2],
    [ADDRESS3],
    [CITY],
    [STATE],
    [ZIP],
    [DEALER],
    [ZONE],
    [DISTRICT],
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
  FROM #ordered o
    INNER JOIN #tempTable tt ON tt.tempTableId = o.data_id
  WHERE o.id BETWEEN @startId AND @endId
  ORDER BY o.id

  DROP TABLE #GROUP_ID_TABLE
  DROP TABLE #tempTable
  DROP TABLE #tempHierarchy
  DROP TABLE #ordered

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_GET_ALL_LOGINS]'
GO
-- ==============================  NAME  ======================================
-- UP_GET_ALL_LOGINS
-- ===========================  DESCRIPTION  ==================================
--
-- GETS THE NUMBER OF LOGINS FOR THE SPECIFIED PROGRAMS. FILTERS BASED ON SUBMITTAL DATE
-- DATE RANGE AND ALSO BY SPECIFIC USERS (AS GIVERS OR RECEIVERS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- YUKIOM        20150622    CREATED
--
-- ===========================  DECLARATIONS  =================================

ALTER procedure [component].[UP_GET_ALL_LOGINS]
 @userIds NVARCHAR(MAX),
    @startDate DATETIME2,
    @endDate DATETIME2

AS
BEGIN

SET NOCOUNT ON

--Set up pax IDs
CREATE TABLE #PAX_ID_TABLE  (
  INPUT_PAX_ID NVARCHAR(MAX)
)

INSERT INTO #PAX_ID_TABLE (
  INPUT_PAX_ID
)
SELECT * FROM [component].UF_LIST_TO_TABLE(@userIds, ',')

--Table for program stats (Result table)
CREATE TABLE #LOGIN_COUNT (
  LOGIN_COUNT BIGINT
)

INSERT INTO #LOGIN_COUNT (
  LOGIN_COUNT
)
SELECT ISNULL(COUNT(login_history.LOGIN_HISTORY_ID), 0)
FROM component.LOGIN_HISTORY login_history
JOIN component.SYS_USER sys_user
  ON sys_user.SYS_USER_ID = login_history.SYS_USER_ID
WHERE sys_user.PAX_ID in (SELECT INPUT_PAX_ID FROM #PAX_ID_TABLE)
AND login_history.LOGIN_DATE >= @startDate
AND login_history.LOGIN_DATE <= @endDate

-- Return the results (stored within the LOGIN_COUNT table)
SELECT
  RESULT.LOGIN_COUNT
FROM (SELECT TOP 1 LOGIN_COUNT from #LOGIN_COUNT) RESULT

--Drop temp tables
DROP TABLE #PAX_ID_TABLE
DROP TABLE #LOGIN_COUNT

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_EARNINGS_REPORT_CATEGORY_APPLICATION_TEXT_PROPERTIES]'
GO
/*===============================================================*/
/* View: VW_EARNINGS_REPORT_CATEGORY_APPLICATION_TEXT_PROPERTIES */
/*===============================================================*/
ALTER view [component].[VW_EARNINGS_REPORT_CATEGORY_APPLICATION_TEXT_PROPERTIES] (EARNINGS_REPORT_CATEGORY_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS EARNINGS_REPORT_CATEGORY_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1600
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [component].[NOMINATION_MISC]'
GO
CREATE TABLE [component].[RG_Recovery_2_NOMINATION_MISC]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[NOMINATION_ID] [bigint] NOT NULL,
[VF_NAME] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MISC_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MISC_DATE] [datetime2] NULL,
[MISC_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_NOMINATION_MISC_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_NOMINATION_MISC_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_NOMINATION_MISC_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_NOMINATION_MISC_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_2_NOMINATION_MISC] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [component].[RG_Recovery_2_NOMINATION_MISC]([ID], [NOMINATION_ID], [VF_NAME], [MISC_DATA], [MISC_DATE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID]) SELECT [NOMINATION_MISC_ID], [NOMINATION_ID], [VF_NAME], [MISC_DATA], [MISC_DATE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID] FROM [component].[NOMINATION_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_2_NOMINATION_MISC] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [component].[NOMINATION_MISC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[RG_Recovery_2_NOMINATION_MISC]', N'NOMINATION_MISC', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_NOMINATION_MISC] on [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] ADD CONSTRAINT [PK_NOMINATION_MISC] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_NOMINATION_MISC_2] on [component].[NOMINATION_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_NOMINATION_MISC_2] ON [component].[NOMINATION_MISC] ([NOMINATION_ID], [MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_NOMINATION_MISC_1] on [component].[NOMINATION_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_NOMINATION_MISC_1] ON [component].[NOMINATION_MISC] ([NOMINATION_ID], [VF_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_NOMINATION_MISC_U_1] on [component].[NOMINATION_MISC]'
GO
-- ==============================  NAME  ======================================
-- TR_NOMINATION_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR          DATE            STORY         CHANGE DESCRIPTION
-- osmanos        20161024                  Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_NOMINATION_MISC_U_1]
ON [component].[NOMINATION_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    NOMINATION_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    NOMINATION_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_RECOGNITION_STOCK_PHRASE_APPLICATION_TEXT_PROPERTIES]'
GO
/*===============================================================*/
/* View: VW_RECOGNITION_STOCK_PHRASE_APPLICATION_TEXT_PROPERTIES */
/*===============================================================*/
ALTER view [component].[VW_RECOGNITION_STOCK_PHRASE_APPLICATION_TEXT_PROPERTIES] (RECOGNITION_STOCK_PHRASE_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS RECOGNITION_STOCK_PHRASE_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1500
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_TEST_RESULT_QUESTION]'
GO
/*==============================================================*/
/* View: VW_TEST_RESULT_QUESTION                                */
/*==============================================================*/
-- ==============================  NAME  ======================================
-- VW_TEST_RESULT_QUESTION
-- ===========================  DESCRIPTION  ==================================
-- Returns the test result questions by test result id
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20150925    Created
-- KNIGHTGA     20151028    Updated for latest UDM 4 changes
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_TEST_RESULT_QUESTION] as
SELECT
    TR.TEST_ID
,    TRQ.TEST_RESULT_ID
,    TQ.TEST_SECTION_ID
,    TRQ.ID AS TEST_RESULT_QUESTION_ID
,    TQ.ID AS TEST_QUESTION_ID
,    TQ.TEST_QUESTION_TYPE_CODE
,    TQ.QUESTION_TEXT
,    TQ.QUESTION_WEIGHT
,    TQ.MAX_ANSWERS
,    TQ.REQ_ANSWERS
,    TQ.REFERENCE_URL
,    TQ.REFERENCE_NAME
,    TQ.IMAGE_URL
,    ISNULL(TRQ.DISPLAY_SEQUENCE, TQ.DISPLAY_SEQUENCE) AS DISPLAY_SEQUENCE
FROM
    COMPONENT.TEST_RESULT_QUESTION TRQ
INNER JOIN COMPONENT.TEST_QUESTION TQ
    ON TQ.ID = TRQ.TEST_QUESTION_ID
INNER JOIN COMPONENT.TEST_RESULT TR
    ON TR.ID = TRQ.TEST_RESULT_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_HIERARCHY_DENORMALIZE]'
GO
-- ==============================  NAME  ======================================
-- UP_HIERARCHY_DENORMALIZE
-- ===========================  DESCRIPTION  ==================================
-- DENORMALIZES' THE PAX_GROUP TABLE FROM A CONCENTRIX DATABASE
--
-- ============================  REVISIONS  ===================================
-- AUTHOR    DATE        CHANGE DESCRIPTION
-- DOHOGNTA    20040810    CREATED
-- DOHOGNTA    20150306    REMOVE REFERENCES TO DEPRECATED SYSTEM OBJECTS
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_DENORMALIZE]
    @verbose int = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

-- LOGGING TAG
DECLARE @prc varchar(50)
DECLARE @msg varchar(1000)
DECLARE @err int
DECLARE @sql varchar(4000)
DECLARE @beg_time datetime2
DECLARE @end_time datetime2
DECLARE @rowcount int
DECLARE @rowcount_sum int
DECLARE @user_role varchar(50)

-- OBJECT OWNER
DECLARE @tab_own varchar(10)

-- ORG_GROUP (W/ LEVELS)
DECLARE @tab_org varchar(50)
DECLARE @tab_org_work varchar(50)
DECLARE @tab_org_temp varchar(50)

-- HIERARCHY (DENORMALIZED)
DECLARE @tab_hie varchar(50)
DECLARE @tab_hie_work varchar(50)
DECLARE @tab_hie_temp varchar(50)

CREATE TABLE #HIERARCHY_STATISTICS (
    DATA INT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-----------------------------------------------------------
-- INITIALIZATION
-----------------------------------------------------------

SET    @prc = 'UP_HIERARCHY_DENORMALIZE'
SET    @rowcount = 0
SET    @rowcount_sum = 0
SET    @user_role = 'APP_WebAdmin'
SET    @tab_own = 'component.'
SET    @tab_hie = 'HIERARCHY'
SET    @tab_hie_work = @tab_hie + '_WORK'
SET    @tab_hie_temp = @tab_hie + '_TEMP'
SET    @tab_org = 'HIERARCHY_ORG_GROUP'
SET    @tab_org_work = @tab_org + '_WORK'
SET    @tab_org_temp = @tab_org + '_TEMP'

-----------------------------------------------------------
-- CREATE WORKING VERSION OF ORG_GROUP (HIERARCHY DEFINITION)
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE WORKING VERSION OF ORG_GROUP (HIERARCHY DEFINITION)'
    PRINT    @msg
END

-- DROP TABLE IF IT EXISTS
EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org_work + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_org_work + '')

EXEC ('
CREATE TABLE ' + @tab_own + @tab_org_work + ' (
    [ORG_GROUP_ID] [int] NULL ,
    [OWNER_GROUP_ID] [int] NULL ,
    [ORGANIZATION_CODE] [nvarchar] (4) NULL ,
    [ROLE_CODE] [nvarchar] (4) NULL ,
    [LEVEL] [int] NULL
) ON [PRIMARY]'
)

SET @sql =
'INSERT INTO ' + @tab_own + @tab_org_work + ' (
    ORG_GROUP_ID
,    OWNER_GROUP_ID
,    ORGANIZATION_CODE
,    ROLE_CODE
,    [LEVEL]
) SELECT
    ORG_GROUP_ID
,    OWNER_GROUP_ID
,    ORGANIZATION_CODE
,    ROLE_CODE
,    NULL [LEVEL]
FROM    ' + @tab_own + 'ORG_GROUP
ORDER BY
    ORG_GROUP_ID'

-- CREATE WORKING VERSION OF ORG_GROUP TABLE AND LOAD WITH DATA
EXEC(@sql)

SELECT    @rowcount = @@ROWCOUNT
,    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ' + @tab_org_work + ' COUNT = [' + CONVERT(VARCHAR, @rowcount) + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- ASSIGN LEVELS
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ASSIGN LEVELS'
    PRINT    @msg
END

DECLARE    @i int
SET    @i = 1

-- ASSIGN LEVEL 1
SET @sql =
'UPDATE    ' + @tab_own + @tab_org_work + '
SET    [LEVEL] = ' + CONVERT(VARCHAR, @I) + '
WHERE    OWNER_GROUP_ID IS NULL'

EXEC(@sql)

SELECT    @rowcount = @@ROWCOUNT
,    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

-- ASSIGN LEVELS UNTIL THERE ARE NO MORE (BOTTOM)
WHILE    @rowcount > 0
BEGIN
    SET    @rowcount_sum = @rowcount_sum + @rowcount

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': ROW COUNT LEVEL' + CONVERT(VARCHAR, @i) + ' = [' + CONVERT(VARCHAR, @rowcount) + ']'
        PRINT    @msg
    END

    SET @sql =
    'UPDATE    T2
    SET    [LEVEL] = ' + CONVERT(VARCHAR, @i + 1) + '
    FROM    ' + @tab_own + @tab_org_work + ' T1
    ,    ' + @tab_own + @tab_org_work + ' T2
    WHERE    T1.ORG_GROUP_ID = T2.OWNER_GROUP_ID
    AND    T1.[LEVEL] = ' + CONVERT(VARCHAR, @i)
    
    -- UPDATE WORKING VERSION OF ORG_GROUP TABLE; ASSIGN LEVEL VALUE TO RECORD(S)
    EXEC(@sql)
    
    SELECT    @rowcount = @@ROWCOUNT
    ,    @err = @@ERROR
    
    IF @err <> 0 RETURN -1 -- EXITING HERE!

    SET @i = @i + 1
END

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': ROW COUNT TOTAL  = [' + CONVERT(VARCHAR, @rowcount_sum) + ']'
    PRINT    @msg
    SET    @msg = @prc + ': MAX DEPTH OF ALL HIERARCIES = [' + CONVERT(VARCHAR, @i - 1) + ']'
    PRINT    @msg
    PRINT    ''
END

DECLARE @max int
DELETE    #HIERARCHY_STATISTICS
EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT ISNULL(MAX([LEVEL]), 0) FROM ' + @tab_own + @tab_org_work )
SELECT    @max = DATA FROM #HIERARCHY_STATISTICS

-----------------------------------------------------------
-- CREATE TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE TABLE'
    PRINT    @msg
END

-- DROP TABLE IF IT ALREADY EXISTS
EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie_work + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_hie_work + '')

-- BUILD CREATE TABLE STATEMENT
DECLARE @j int
DECLARE @cre varchar(900)
DECLARE @col varchar(800)

SET    @cre = 'CREATE TABLE ' + @tab_own + @tab_hie_work + ' (ROW_ID INT IDENTITY NOT NULL, PAX_ID INT NOT NULL'
SET    @col = ''

SET    @j = @max
WHILE    @j > 0
BEGIN
    -- THE TABLE WIDTH SHOULD CORESPOND TO THE HIERARCHY DEPTH
    SET @col = ', ROLE' + CONVERT(VARCHAR, @j) + ' VARCHAR(4) NULL' + ', LEVEL' + CONVERT(VARCHAR, @j) + ' INT NULL' + @col
    SET @j = @j - 1
END

SET    @cre = @cre + @col + ')'

-- EXECUTE CREATE TABLE STATEMENT
EXEC    (@cre)

SET    @err = @@ERROR

IF @err <> 0 RETURN -1 -- EXITING HERE!

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': CREATE STATEMENT = [' + @cre + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- POPULATE TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': POPULATE TABLE'
    PRINT    @msg
    SELECT    @beg_time = GETDATE()
    SET    @msg = @prc + ': INSERT STATEMENT; BEG_TIME = [' + CONVERT(VARCHAR, @beg_time) + ']'
    PRINT    @msg
END

DECLARE @k int
DECLARE @l int

DECLARE @ins varchar(500)
DECLARE @sel varchar(500)
DECLARE @fro varchar(500)
DECLARE @whe varchar(1000)
DECLARE @ord varchar(500)

-- INITIALIZE COUNTER
SET    @l = 1
WHILE    @l <= @max
BEGIN
    SET    @sql = ''
    SET    @ins = 'INSERT INTO ' + @tab_own + @tab_hie_work + ' ('
    SET    @sel = ' SELECT DISTINCT'
    SET    @fro = ' FROM'
    SET    @whe = ' WHERE'
    SET    @ord = ' ORDER BY'

    -- INITIALIZE COUNTER
    SET    @k = 1
    WHILE    @k <= @l
    BEGIN
        IF @k = 1
        BEGIN -- FIRST LINE
            SET    @ins = @ins + 'LEVEL' + CONVERT(VARCHAR, @k) + ', ROLE' + CONVERT(VARCHAR, @k)
            SET    @sel = @sel + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID' + ', PG_L' + CONVERT(VARCHAR, @k) + '.ROLE_CODE'
            SET    @fro = @fro + ' ' + @tab_own + 'PAX_GROUP PG_L' + CONVERT(VARCHAR, @k)
            IF @l > 1
            BEGIN
                SET    @whe = @whe + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID = PG_L' + CONVERT(VARCHAR, @k + 1) + '.OWNER_GROUP_ID'
            END
            SET    @ord = @ord + ' PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID'
        END
        ELSE  -- OTHER LINES
        BEGIN
            SET    @ins = @ins + ', LEVEL' + CONVERT(VARCHAR, @k) + ', ROLE' + CONVERT(VARCHAR, @k)
            SET    @sel = @sel + ', PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID' + ', PG_L' + CONVERT(VARCHAR, @k) + '.ROLE_CODE'
            SET    @fro = @fro + ', ' + @tab_own + 'PAX_GROUP PG_L' + CONVERT(VARCHAR, @k)
            IF @k < @l
            BEGIN
                SET    @whe = @whe + ' AND PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID = PG_L' + CONVERT(VARCHAR, @k + 1) + '.OWNER_GROUP_ID'
            END
            SET    @ord = @ord + ', PG_L' + CONVERT(VARCHAR, @k) + '.PAX_GROUP_ID'
        END
        
        -- INCREMENT COUNT
        SET    @k = @k + 1
    END -- WHILE
    
    SET    @ins = @ins + ', PAX_ID)'
    SET    @sel = @sel + ', PG_L' + CONVERT(VARCHAR, @k - 1) + '.PAX_ID' 
    IF @l > 1
        SET    @whe = @whe + ' AND PG_L1.OWNER_GROUP_ID IS NULL'
    ELSE
        SET    @whe = @whe + ' PG_L1.OWNER_GROUP_ID IS NULL'
    SET    @sql = @ins + @sel + @fro + @whe + @ord

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': INSERT STATEMENT = [' + @sql + ']'
        PRINT    @msg
    END

    -- INSERT DENORMALIZED HIERARCHY RECORD(S)
    EXEC    (@sql)

    SELECT    @rowcount = @@ROWCOUNT
    ,    @err = @@ERROR

    IF @err <> 0 RETURN -1 -- EXITING HERE!

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': ROW COUNT LEVEL' + CONVERT(VARCHAR, @l) + ' = [' + CONVERT(VARCHAR, @rowcount) + ']'
        PRINT    @msg
    END

    -- INCREMENT COUNT
    SET    @l = @l + 1
END -- WHILE

IF @verbose = 1
BEGIN
    SELECT    @end_time = GETDATE()
    SET    @msg = @prc + ': INSERT STATEMENT; END_TIME = [' + CONVERT(VARCHAR, @end_time) + ']'
    PRINT    @msg
    SET    @msg = @prc + ': INSERT STATEMENT; EXE_TIME = [' + CONVERT(VARCHAR, DATEDIFF(ms, @beg_time, @end_time)) + ']'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- DISPLAY TABLE STATISTICS
-----------------------------------------------------------

SELECT    @rowcount = COUNT(1) FROM component.PAX_GROUP

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': DISPLAY STATISTICS'
    PRINT    @msg
     SET    @msg = @prc + ': ROW COUNT = [' + CONVERT(VARCHAR, @rowcount) + '] (PAX_GROUP)'
    PRINT    @msg
    DELETE    #HIERARCHY_STATISTICS
    EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT COUNT(1) FROM ' + @tab_own + @tab_hie_work )
    SELECT    @rowcount = DATA FROM #HIERARCHY_STATISTICS
     SET    @msg = @prc + ': ROW COUNT = [' + CONVERT(VARCHAR, @rowcount) + '] (' + @tab_hie_work + ')'
    PRINT    @msg
    PRINT    ''
END

-----------------------------------------------------------
-- SWAP TABLE
-----------------------------------------------------------

IF @verbose = 1
BEGIN
    SET    @msg = @prc + ': SWAP TABLE'
    PRINT    @msg
END

DELETE    #HIERARCHY_STATISTICS
EXEC    ('INSERT INTO #HIERARCHY_STATISTICS (DATA) SELECT COUNT(1) FROM ' + @tab_own + @tab_hie_work)
SELECT    @rowcount = DATA FROM #HIERARCHY_STATISTICS
-- SEE IF THERE ARE ANY RECORDS IN THE SOURCE TABLE
IF (@rowcount) > 0
BEGIN
    -- APPLY PERMISSIONS
--    EXEC    ('GRANT  SELECT  ON ' + @tab_own + @tab_org_work + '  TO [' + @user_role + ']')
--    EXEC    ('GRANT  SELECT  ON ' + @tab_own + @tab_hie_work + '  TO [' + @user_role + ']')

    -- SWAP TABLES
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) EXEC SP_RENAME ''' + @tab_own + @tab_org + ''', ''' + @tab_org_temp + '''')
    EXEC    ('SP_RENAME ''' + @tab_own + @tab_org_work + ''', ''' + @tab_org + '''')

    -- DROP TABLE IF IT EXISTS
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_org_temp + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_org_temp + '')

    -- SWAP TABLES
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) EXEC SP_RENAME ''' + @tab_own + @tab_hie + ''', ''' + @tab_hie_temp + '''')
    EXEC    ('SP_RENAME ''' + @tab_own + @tab_hie_work + ''', ''' + @tab_hie + '''')

    -- DROP TABLE IF IT EXISTS
    EXEC    ('IF EXISTS (select * from sys.objects where object_id = object_id(N''' + @tab_own + @tab_hie_temp + ''') and OBJECTPROPERTY(object_id, N''IsUserTable'') = 1) DROP TABLE ' + @tab_own + @tab_hie_temp + '')

    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': SWAP TABLE = [COMPLETE]'
        PRINT    @msg
        PRINT    ''
    END
END -- IF
ELSE
BEGIN
    IF @verbose = 1
    BEGIN
        SET    @msg = @prc + ': SWAP TABLE = [INCOMPLETE]'
        PRINT    @msg
        PRINT    ''
    END
END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[FEED_ITEM]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[FEED_ITEM] ALTER COLUMN [TARGET_TABLE] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[FEED_ITEM] ALTER COLUMN [TARGET_ID] [bigint] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[BUSINESS_RULE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[BUSINESS_RULE] DROP
COLUMN [CLASSNAME]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BUSINESS_RULE_3] on [component].[BUSINESS_RULE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BUSINESS_RULE_3] ON [component].[BUSINESS_RULE] ([BUSINESS_RULE_NAME], [BUSINESS_RULE_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_GROUP_BY_GROUP]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_GROUP_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID AND LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS)
-- RETURN ALL ASSOCIATED GROUP_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_GROUP_BY_GROUP]
    @group_id BIGINT
,    @level_increment BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS GROUP_ID
FROM    UF_ASSOCIATION_FETCH_GROUP_BY_GROUP (@group_id, @level_increment)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP]'
GO
-- ==============================  NAME  ======================================
-- UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID AND LEVEL INCREMENT (-1 = ANCESTORS, 1 = DESCENDANTS)
-- RETURN ALL ASSOCIATED PAX_ID(S), INCLUSIVE OF PARAMETER; IN DIRECTION SPECIFIED BY LEVEL INCREMENT
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20110221    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP]
    @pax_group_id BIGINT
,    @level_increment INT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS PAX_ID
FROM    UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP (@pax_group_id, @level_increment, NULL)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PAX_PERMISSION]'
GO
/*==============================================================*/
/* View: VW_PAX_PERMISSION                                      */
/*==============================================================*/
ALTER view [component].[VW_PAX_PERMISSION] as
SELECT 
    VPR.[LEVEL]
,    R.PERMISSION_ID
,    R.PERMISSION_CODE AS PERMISSION
,    VPR.ROLE_ID
,    VPR.ROLE
,    VPR.QUALIFIER
,    VPR.TARGET_TABLE
,    VPR.TARGET_ID
,    VPR.SUBJECT_TABLE
,    VPR.SUBJECT_ID
,    VPR.PAX_ID
FROM 
    COMPONENT.VW_PAX_ROLE VPR
LEFT JOIN COMPONENT.ROLE_PERMISSION RM
    ON RM.ROLE_ID = VPR.ROLE_ID
LEFT JOIN COMPONENT.PERMISSION R
    ON R.PERMISSION_ID = RM.PERMISSION_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_GROUP_BY_PAX]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_GROUP_BY_PAX
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_ID
-- RETURN A DISTINCT LIST OF GROUP_ID(S) FOR THE GIVEN PAX_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_GROUP_BY_PAX]
    @pax_id BIGINT
-- TODO:,    @level_increment INT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS GROUP_ID
-- FROM    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, @level_increment)
FROM    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, -1)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[SALE_ITEM_HEADER]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[SALE_ITEM_HEADER] ADD
[ITEM_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_HIERARCHY_LIST]'
GO
-- ==============================  NAME  ======================================
-- UP_HIERARCHY_LIST
-- ===========================  DESCRIPTION  ==================================
-- TODO: REWRITE / DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20050720    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_LIST]
    @organization_code nvarchar(4)
,    @pax_id BIGINT
,    @show_children bit = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

create table #pax_hierarchy (
    pax_id    BIGINT,
    level_code    int,
    pax_group_id    BIGINT,
    owner_group_id    BIGINT,
    role_code        nvarchar(4),
    entity_name        varchar(100)
)

declare @level_code as int

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

set @level_code = 0

insert into #pax_hierarchy
select pax_group.pax_id, @level_code, pax_group.pax_group_id, owner_group_id, pax_group.role_code,
case
    when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
    else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
end entity_name
from pax_group, pax
where pax_group.pax_id = pax.pax_id
and organization_code = @organization_code
and pax_group.pax_id = @pax_id
and thru_date is null
and getdate() between from_date and isnull(thru_date, getdate())

declare @pax_group_id as BIGINT
select @pax_group_id = owner_group_id
from #pax_hierarchy

while @pax_group_id is not null begin

    set @level_code = @level_code + 1

    insert into #pax_hierarchy
    select pax_group.pax_id, @level_code, pax_group.pax_group_id, owner_group_id, pax_group.role_code,
    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
    from pax_group, pax
    where pax_group.pax_id = pax.pax_id
    and pax_group.pax_group_id = @pax_group_id
    and pax_group.thru_date is null
    and getdate() between from_date and isnull(thru_date, getdate())

    select @pax_group_id = owner_group_id
    from pax_group
    where pax_group_id = @pax_group_id
end

select @pax_id as pax_id, @organization_code as organization_code, (select pax_group_id from #pax_hierarchy where level_code = 0) as pax_group_id

select * from #pax_hierarchy order by level_code desc

if @show_children = 1 begin
    select pax_group.pax_id, pax_group.pax_group_id, pax_group.role_code,
    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
    from pax_group, pax, #pax_hierarchy
    where pax_group.pax_id = pax.pax_id
    and pax_group.owner_group_id = #pax_hierarchy.pax_group_id
    and #pax_hierarchy.level_code = 0
    and pax_group.thru_date is null 
    and getdate() between from_date and isnull(thru_date, getdate())
    order by entity_name, pax_group.role_code
end

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PERMISSION_ROLE]'
GO
/*==============================================================*/
/* View: VW_PERMISSION_ROLE                                     */
/*==============================================================*/
ALTER view [component].[VW_PERMISSION_ROLE] as
SELECT
    P.PERMISSION_ID
,    P.PERMISSION_CODE AS [PERMISSION]
,    P.PERMISSION_TYPE_CODE
,    R.ROLE_ID
,    R.ROLE_CODE AS [ROLE]
,    R.ROLE_TYPE_CODE
FROM
    COMPONENT.ROLE_PERMISSION RP
INNER JOIN COMPONENT.ROLE R
    ON  R.ROLE_ID = RP.ROLE_ID
INNER JOIN COMPONENT.PERMISSION P
    ON  P.PERMISSION_ID = RP.PERMISSION_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[EMAIL_MESSAGE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[EMAIL_MESSAGE] ADD
[MESSAGE_CONTENT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RECIPIENT] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUBJECT] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SENDER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RECIPIENT_CC] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RECIPIENT_BCC] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_NOMINATION_RECOGNITION_STOCK_PHRASE]'
GO
/*==============================================================*/
/* View: VW_NOMINATION_RECOGNITION_STOCK_PHRASE                 */
/*==============================================================*/
ALTER view [component].[VW_NOMINATION_RECOGNITION_STOCK_PHRASE] (NOMINATION_ID, RECOGNITION_STOCK_PHRASE_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS NOMINATION_ID
,       FK2 AS RECOGNITION_STOCK_PHRASE_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2400
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[ACTIVITY_BUSINESS_RULE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[ACTIVITY_BUSINESS_RULE] ADD
[RULE_NUMBER] [int] NULL,
[RULE_ID] [bigint] NULL,
[METRIC_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[METRIC_IDS] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
UPDATE [component].[ACTIVITY_BUSINESS_RULE] SET [RULE_ID] = [ACTIVITY_BUSINESS_RULE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[ACTIVITY_BUSINESS_RULE] DROP
COLUMN [ACTIVITY_BUSINESS_RULE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ACTIVITY_BUSINESS_RULE_7] on [component].[ACTIVITY_BUSINESS_RULE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ACTIVITY_BUSINESS_RULE_7] ON [component].[ACTIVITY_BUSINESS_RULE] ([RULE_ID], [PROGRAM_ID], [VERSION] DESC)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[PAX_EVENT_COMMUNICATION]'
GO
CREATE TABLE [component].[PAX_EVENT_COMMUNICATION]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[PAX_ID] [bigint] NOT NULL,
[EVENT_COMMUNICATION_ID] [bigint] NOT NULL,
[STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PAX_EVENT_COMMUNICATION_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_EVENT_COMMUNICATION_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PAX_EVENT_COMMUNICATION_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_EVENT_COMMUNICATION_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PAX_EVENT_COMMUNICATION] on [component].[PAX_EVENT_COMMUNICATION]'
GO
ALTER TABLE [component].[PAX_EVENT_COMMUNICATION] ADD CONSTRAINT [PK_PAX_EVENT_COMMUNICATION] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_EVENT_COMMUNICATION_1] on [component].[PAX_EVENT_COMMUNICATION]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PAX_EVENT_COMMUNICATION_1] ON [component].[PAX_EVENT_COMMUNICATION] ([PAX_ID], [EVENT_COMMUNICATION_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PAX_EVENT_COMMUNICATION_U_1] on [component].[PAX_EVENT_COMMUNICATION]'
GO
-- ==============================  NAME  ======================================
-- TR_PAX_EVENT_COMMUNICATION_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PAX_EVENT_COMMUNICATION_U_1]
ON [component].[PAX_EVENT_COMMUNICATION]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PAX_EVENT_COMMUNICATION T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PAX_EVENT_COMMUNICATION T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_SPIN_PROGRAM_TOKENS]'
GO
-- ==============================  NAME  ======================================
-- VW_SPIN_PROGRAM_TOKENS
-- ===========================  DESCRIPTION  ==================================
-- Returns token information by program_activity_id for reporting.
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20111102    Created
-- KNIGHTGA     20150818   Updated for UDM 3
-- KNIGHTGA     20170113    Updated for UDM 5 (SPIN_TOKEN, SPIN_AWARD tables)
--
-- ===========================  DECLARATIONS  =================================
ALTER view [component].[VW_SPIN_PROGRAM_TOKENS] as
SELECT
    TH.PROGRAM_ID AS PROGRAM_ID
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN case when TH.SUB_TYPE_CODE = 'IOU_TOKEN' then -1 else 1 end else 0 END) AS TOKENS_AVAILABLE
,    SUM(CASE WHEN ST.SPIN_AWARD_ID IS NULL THEN 0 ELSE 1 END) AS TOKENS_USED
,    count(th.id) AS TOKENS_TOTAL
FROM
    COMPONENT.TRANSACTION_HEADER TH
INNER JOIN COMPONENT.SPIN_TOKEN st
    ON st.TRANSACTION_HEADER_ID = TH.ID
WHERE TH.TYPE_CODE = 'SPIN_TOKEN'
  AND TH.STATUS_TYPE_CODE = 'APPROVED'
  AND TH.SUB_TYPE_CODE in ('SPIN_TOKEN', 'IOU_TOKEN')
GROUP BY
    TH.PROGRAM_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S)
-- RETURN A DISTINCT LIST OF PAX INFORMATION AND THE CORRESPONDING GROUP INFORMATION
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DANDUR        20120730    MODIFIED FOR FETCHING MULTIPLE GROUPS
-- DOHOGNTA        20150304    REMOVE ASSOCIATION TABLE REFERENCE
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_AND_GROUP_BY_LIST_GROUP]
    @delim_list_group VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)
DECLARE    @fk TABLE ([id] BIGINT)

-- DRIVER
DECLARE @driver TABLE (
    pax_id BIGINT NOT NULL
,    group_id BIGINT NOT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET    @delimiter = ','

-- PARSE DELIMITED LIST OF GROUP_ID(S) TO A LIST OF INTEGER(S)
INSERT INTO @fk ([id]) SELECT token AS GROUP_ID FROM UF_PARSE_STRING_TO_INTEGER (@delim_list_group, @delimiter)

-- LOAD PAX DIRECTLY ASSOCIATED WITH EACH GROUP(S); ONLY LOAD THE FIRST PAX / GROUP RELATIONSHIP
INSERT INTO @driver (
    pax_id
,    group_id
)
SELECT
    GROUPS_PAX.PAX_ID
,    GROUPS_PAX.GROUP_ID
FROM    
    GROUPS_PAX
,    @fk AS driver
WHERE    
    GROUPS_PAX.GROUP_ID = driver.[id]
AND    GROUPS_PAX.GROUP_ID IN (
        SELECT    GROUPS_PAX2.GROUP_ID
        FROM    GROUPS_PAX AS GROUPS_PAX2
        ,    @fk AS driver2
        WHERE    GROUPS_PAX2.PAX_ID = GROUPS_PAX.PAX_ID
        AND    GROUPS_PAX2.GROUP_ID = driver2.[id]
)

SELECT    PAX.PAX_ID
,    PAX.CONTROL_NUM
,    PAX.FIRST_NAME
,    PAX.MIDDLE_NAME
,    PAX.LAST_NAME
,    GROUPS.GROUP_DESC
FROM    @driver AS driver
,    PAX
,    GROUPS
WHERE    driver.pax_id = PAX.PAX_ID
AND    driver.group_id = GROUPS.GROUP_ID
ORDER BY PAX.LAST_NAME

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_DATABUS_SPINS_AVAILABLE]'
GO
ALTER PROCEDURE [component].[UP_DATABUS_SPINS_AVAILABLE] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'programName',
  @sortDir              NVARCHAR(1) = 'A',
  @paxId                BIGINT
) AS BEGIN
  SET NOCOUNT ON

/* for testing
DECLARE
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'spinDate',
  @sortDir              NVARCHAR(1) = 'A',
  @paxId                BIGINT

set @paxId = 1
*/

DECLARE @recordCount INT, @pageCount INT, @startId INT, @endId INT

CREATE TABLE #tempTable(
    [ID]                    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [PROGRAM_ID]            BIGINT,
    [PROGRAM_NAME]          NVARCHAR(100),
    [SPINS_REMAINING]       INTEGER,
    [SPINS_TAKEN]           INTEGER,
    [SPIN_EARNINGS]         INTEGER,
    [START_DATE]            datetime2(7),
    [EXPIRATION_DATE]       datetime2(7),
    [TOTAL_SPINS_REMAINING] INTEGER,
    [TOTAL_SPINS_TAKEN]     INTEGER,
    [TOTAL_SPIN_EARNINGS]   INTEGER
);

CREATE TABLE #ordered (
    [ID]        BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    [DATA_ID]   BIGINT                 NOT NULL
);

INSERT INTO #tempTable (
    [PROGRAM_ID],
    [PROGRAM_NAME],
    [SPINS_REMAINING],
    [SPINS_TAKEN],
    [SPIN_EARNINGS],
    [START_DATE],
    [EXPIRATION_DATE]
)
select prg.PROGRAM_ID
      ,prg.PROGRAM_NAME
      ,spt.TOKENS_AVAILABLE
      ,spt.TOKENS_USED
      ,spt.TOKEN_AWARDS
      ,isnull(pa.FROM_DATE, prg.FROM_DATE) as START_DATE
      ,isnull(pa.THRU_DATE, prg.THRU_DATE) as EXPIRATION_DATE
 from component.VW_SPIN_PAX_TOKENS spt
      inner join component.program prg on prg.PROGRAM_ID = spt.PROGRAM_ID
      left join component.PROGRAM_ACTIVITY pa
             on pa.PROGRAM_ID = prg.PROGRAM_ID
            and pa.PROGRAM_ACTIVITY_TYPE_CODE = 'SPIN'
 where spt.pax_id = @paxId

--update the totals
update #tempTable
  set TOTAL_SPINS_REMAINING = isnull(dt.TOTAL_SPINS_REMAINING, 0)
     ,TOTAL_SPINS_TAKEN = isnull(dt.TOTAL_SPINS_TAKEN, 0)
     ,TOTAL_SPIN_EARNINGS = isnull(dt.TOTAL_SPIN_EARNINGS, 0)
 from (
        select TOTAL_SPINS_REMAINING = sum(tt.SPINS_REMAINING)
              ,TOTAL_SPINS_TAKEN = sum(tt.SPINS_TAKEN)
              ,TOTAL_SPIN_EARNINGS = sum(tt.SPIN_EARNINGS)
          from #tempTable tt
      ) as dt


INSERT INTO #ordered (DATA_ID)
    SELECT tt.ID
    FROM #tempTable tt
    ORDER BY
         CASE @sortDir WHEN 'D' THEN
                CASE @sortBy
                    WHEN 'programName' THEN tt.[PROGRAM_NAME]
                    WHEN 'spinsRemaining' THEN  right('00000000000000000000' + cast(tt.SPINS_REMAINING as nvarchar(20)), 20)
                    WHEN 'spinsTaken' THEN right('00000000000000000000' + cast(tt.SPINS_TAKEN as nvarchar(20)), 20)
                    WHEN 'spinEarnings' then right('00000000000000000000' + cast(tt.SPIN_EARNINGS as nvarchar(20)), 20)
                    WHEN 'expirationDate' THEN convert(nvarchar(20), tt.EXPIRATION_DATE, 112)
                END
         END DESC
        ,CASE @sortDir WHEN 'A' THEN
            CASE @sortBy
                WHEN 'programName' THEN tt.[PROGRAM_NAME]
                    WHEN 'spinsRemaining' THEN  right('00000000000000000000' + cast(tt.SPINS_REMAINING as nvarchar(20)), 20)
                    WHEN 'spinsTaken' THEN right('00000000000000000000' + cast(tt.SPINS_TAKEN as nvarchar(20)), 20)
                    WHEN 'spinEarnings' then right('00000000000000000000' + cast(tt.SPIN_EARNINGS as nvarchar(20)), 20)
                WHEN 'expirationDate' THEN convert(nvarchar(20), tt.EXPIRATION_DATE, 112)
            END
         END ASC

SELECT @recordCount = @@ROWCOUNT

IF @recordsPerPage = -1 BEGIN
    SET @recordsPerPage = @recordCount
    SET @pageNumber = 1
END

IF @recordsPerPage = 0 BEGIN
    SET @recordsPerPage = 1
END

--Make sure the requested page number is within the valid range of pages (if not, then fix it)
SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
IF @pageNumber > @pageCount BEGIN
    SET @pageNumber = @pageCount
END
IF @pageNumber < 1 BEGIN
    SET @pageNumber = 1
END

SET @endId = @pageNumber * @recordsPerPage
SET @startId = (@endId - @recordsPerPage) + 1


SELECT
    [PROGRAM_ID],
    [PROGRAM_NAME],
    [SPINS_REMAINING],
    [SPINS_TAKEN],
    [SPIN_EARNINGS],
    [START_DATE],
    [EXPIRATION_DATE],
    [TOTAL_SPINS_REMAINING],
    [TOTAL_SPINS_TAKEN],
    [TOTAL_SPIN_EARNINGS],
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
FROM #ordered o
    INNER JOIN #tempTable tt ON tt.id = o.data_id
WHERE o.id BETWEEN @startId AND @endId
ORDER BY o.id

DROP TABLE #tempTable
DROP TABLE #ordered

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_LIKES]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_LIKES] ALTER COLUMN [TARGET_TABLE] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_HIERARCHY_LIST_RECENT]'
GO
-- ==============================  NAME  ======================================
-- UP_HIERARCHY_LIST_RECENT
-- ===========================  DESCRIPTION  ==================================
-- TODO: REWRITE / DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20050720    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_HIERARCHY_LIST_RECENT]
    @organization_code nvarchar(4)
,    @pax_id BIGINT
,    @show_children bit = 0
AS
BEGIN

SET NOCOUNT ON
SET CONCAT_NULL_YIELDS_NULL OFF

declare @level_code int
declare @status varchar(8)
declare @pax_group_id BIGINT
declare @pax_group_index int

create table #pax_hierarchy (
    pax_id BIGINT
,    level_code int
,    pax_group_id BIGINT
,    owner_group_id BIGINT
,    role_code nvarchar(4)
,    entity_name varchar(100)
,    thru_date datetime2
,    status varchar(8)
,    role_desc varchar(255)
,    hierarchy_index int
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- ============================================================================
-- initialization
-- ============================================================================

set    @level_code = 0

insert into #pax_hierarchy
select    pax_group.pax_id
,    @level_code
,    pax_group.pax_group_id
,    owner_group_id
,    pax_group.role_code
,    case
        when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
        else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
    end entity_name
,    thru_date
,    'active'
,    role_desc
,    null
from    pax_group
,    pax
,    role
where    pax_group.pax_id = pax.pax_id
and    pax_group.role_code = role.role_code
and    organization_code = @organization_code
and    pax_group.pax_id = @pax_id
and    getdate() between from_date and isnull(thru_date, getdate())

if    @@rowcount = 0
begin
    set @status = 'inactive'

    insert into #pax_hierarchy
    select    pax_group.pax_id
    ,    @level_code
    ,    pax_group.pax_group_id
    ,    owner_group_id
    ,    pax_group.role_code
    ,    case
            when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
            else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
        end entity_name
    ,    thru_date
    ,    'inactive'
    ,    role_desc
    ,    null
    from    pax_group
    ,    pax
    ,    role
    where    pax_group.pax_id = pax.pax_id
    and    pax_group.role_code = role.role_code
    and    organization_code = @organization_code
    and    pax_group.pax_id = @pax_id
    and    getdate() >= from_date
    and    thru_date = 
        (
            select    max(thru_date)
            from    pax_group 
            where    pax_group.pax_id = @pax_id
            and    organization_code = @organization_code
        )
end
else
begin
    set @status = 'active'
end

declare c_pax_group cursor
read_only
for
select distinct owner_group_id
from    pax_group
where    organization_code = @organization_code
and    pax_group.pax_id = @pax_id
and    getdate() between from_date and isnull(thru_date, getdate())

open c_pax_group
fetch next from c_pax_group into @pax_group_id

set @pax_group_index = 0
while @@fetch_status = 0 begin
    set    @pax_group_index = @pax_group_index + 1
    
    update    #pax_hierarchy
    set    hierarchy_index = @pax_group_index
    where    owner_group_id = @pax_group_id

    while @pax_group_id is not null 
    begin
        set @level_code = @level_code + 1
    
        insert into #pax_hierarchy
        select    pax_group.pax_id
        ,    @level_code
        ,    pax_group.pax_group_id
        ,    owner_group_id
        ,    pax_group.role_code
        ,    case
                when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
                else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
            end entity_name
        ,    thru_date
        ,    'active'
        ,    '' as role_desc
        ,    @pax_group_index
        from    pax_group
        ,    pax
        where    pax_group.pax_id = pax.pax_id
        and    pax_group.pax_group_id = @pax_group_id

        update    #pax_hierarchy 
        set    status = 'inactive' 
        where    pax_group_id = @pax_group_id
        and    thru_date is not null

        select    @pax_group_id = owner_group_id
        from    pax_group
        where    pax_group_id = @pax_group_id
    end

    fetch next from c_pax_group into @pax_group_id
end
close c_pax_group
deallocate c_pax_group

select    @pax_id as pax_id
,    @organization_code as organization_code
,    pax_group_id
,    @status as status
,    role_desc
from    #pax_hierarchy
where    level_code = 0

select    *
from    #pax_hierarchy
order by
    level_code desc

if @show_children = 1 and @status = 'active' 
begin
    select    pax_group.pax_id
    ,    pax_group.pax_group_id
    ,    pax_group.role_code
    ,    case
            when pax.last_name is null then pax_group.role_code + ' - ' + pax.company_name
            else pax_group.role_code + ' - ' + isnull(pax.first_name, '') + ' ' + isnull(pax.last_name, '') 
        end entity_name
    ,    role.role_desc
    ,    'active' as status
    ,    hierarchy_index
    from    pax_group
    ,    pax
    ,    #pax_hierarchy
    ,    role
    where    pax_group.pax_id = pax.pax_id
    and    pax_group.owner_group_id = #pax_hierarchy.pax_group_id
    and    pax_group.role_code = role.role_code
    and    #pax_hierarchy.level_code = 0
    and    getdate() between from_date and isnull(pax_group.thru_date, getdate())
    order by
        hierarchy_index
    ,    entity_name
    ,    pax_group.role_code
end

end
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_SALE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_SALE] ADD
[SELLER_NAME] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SELLER_NUMBER] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PAX_GROUP_GROUPS]'
GO
/*==============================================================*/
/* View: VW_PAX_GROUP_GROUPS                                    */
/*==============================================================*/
ALTER view [component].[VW_PAX_GROUP_GROUPS] as
SELECT  FK1 AS PAX_GROUP_ID
,       FK2 AS GROUP_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 500
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_ACL_ROLE]'
GO
/*==============================================================*/
/* View: VW_ACL_ROLE                                            */
/*==============================================================*/
ALTER view [component].[VW_ACL_ROLE] as
SELECT
    R.ROLE_ID
,    R.ROLE_CODE AS [ROLE]
,    R.ROLE_TYPE_CODE
,    A.SUBJECT_TABLE
,    A.SUBJECT_ID
,    A.TARGET_TABLE
,    A.TARGET_ID
FROM    
    COMPONENT.ACL A
INNER JOIN COMPONENT.ROLE R
    ON  R.ROLE_ID = A.ROLE_ID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PROGRAM_APPLICATION_FIELD]'
GO
/*==============================================================*/
/* View: VW_PROGRAM_APPLICATION_FIELD                           */
/*==============================================================*/
ALTER view [component].[VW_PROGRAM_APPLICATION_FIELD] (PROGRAM_ID, APPLICATION_FIELD_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS APPLICATION_FIELD_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1800
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_SYS_USER_PERMISSION]'
GO
/*==============================================================*/
/* View: VW_SYS_USER_PERMISSION                                 */
/*==============================================================*/
ALTER view [component].[VW_SYS_USER_PERMISSION] (PAX_ID, PERMISSION_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS SYS_USER_ID
,       FK2 AS PERMISSION_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2200
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_PAX_BY_GROUP]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_BY_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A GROUP_ID
-- RETURN A DISTINCT LIST OF PAX_ID(S) ASSOCIATED WITH THE GIVEN GROUP_ID (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DOHOGNTA        20150304    REMOVE ASSOCIATION REFERENCE
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_BY_GROUP]
    @group_id BIGINT
AS
BEGIN

SET NOCOUNT ON

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

SELECT    [id] AS PAX_ID
FROM    UF_ASSOCIATION_FETCH_PAX_BY_GROUP (@group_id)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[METRIC]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[METRIC] ADD
[TRANSACTION_HEADER_ID] [bigint] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_SALES_INCENTIVE_PERFORMANCE_EXTRACT_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_SALES_INCENTIVE_PERFORMANCE_EXTRACT_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN (TODO)
-- RETURN (TODO)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20110926    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
--
-- ===========================  DECLARATIONS  =================================
ALTER PROCEDURE [component].[UP_SALES_INCENTIVE_PERFORMANCE_EXTRACT_REPORT]
    @pax_group_id BIGINT
,    @sale_date_from DATETIME2
,    @sale_date_thru DATETIME2
,    @delimited_list_group VARCHAR(500)
,    @delimited_list_product_category NVARCHAR(500)
,    @delimited_list_product_type NVARCHAR(500)
,    @delimited_list_product VARCHAR(500)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)  
SET    @delimiter = ','

CREATE TABLE #criteria_list_group (
    group_id BIGINT NULL
)

CREATE TABLE #criteria_list_product_type (
    product_type_code NVARCHAR(50) NULL
)

CREATE TABLE #criteria_list_product_category (
    product_category_code NVARCHAR(50) NULL
)

CREATE TABLE #criteria_list_product (
    product_id BIGINT NULL
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-----------------------------------------------------------
-- INITIALIZATION
-----------------------------------------------------------

-- PARSE THE SELECTED GROUP IDS   
IF (@delimited_list_group IS NOT NULL) AND (LEN(@delimited_list_group) > 0 AND @delimited_list_group != 'none')
BEGIN
    INSERT INTO #criteria_list_group SELECT token FROM UF_PARSE_STRING_TO_INTEGER (@delimited_list_group, @delimiter)
END

-- PARSE THE SELECTED PRODUCT CATEGORY
IF (@delimited_list_product_category IS NOT NULL) AND (LEN(@delimited_list_product_category) > 0 AND @delimited_list_product_category != 'none')
BEGIN
    INSERT INTO #criteria_list_product_category SELECT token FROM UF_PARSE_STRING_TO_STRING(@delimited_list_product_category , @delimiter)
END

-- PARSE THE SELECTED PRODUCT TYPE
IF (@delimited_list_product_type IS NOT NULL) AND (LEN(@delimited_list_product_type) > 0 AND @delimited_list_product_type != 'none')
BEGIN
    INSERT INTO #criteria_list_product_type SELECT token FROM  UF_PARSE_STRING_TO_STRING(@delimited_list_product_type, @delimiter)
END

-- PARSE THE SELECTED PRODUCT
IF (@delimited_list_product IS NOT NULL) AND (LEN(@delimited_list_product) > 0 AND @delimited_list_product != 'none')
BEGIN
    INSERT INTO #criteria_list_product SELECT token FROM UF_PARSE_STRING_TO_INTEGER (@delimited_list_product, @delimiter)
END

-- JOIN THE PRODUCT TABLE WITH THE CRITERIA
SELECT
    PRODUCT.PRODUCT_ID
INTO
    #temp_criteria
FROM
    PRODUCT
INNER JOIN #criteria_list_product_category criteria_list_product_category
    ON PRODUCT.PRODUCT_CATEGORY_CODE = criteria_list_product_category.product_category_code
INNER JOIN #criteria_list_product_type criteria_list_product_type
    ON PRODUCT.PRODUCT_TYPE_CODE = criteria_list_product_type.product_type_code
INNER JOIN #criteria_list_product criteria_list_product
    ON PRODUCT.PRODUCT_ID = criteria_list_product.product_id

SELECT DISTINCT
    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CONTROL_NUM
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PARTICIPANT_NAME
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ROLE_NAME
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.SALE_DATE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PURCHASE_DATE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC_DATE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.OTHER_ID
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ADDITIONAL_SALES_ID
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PENETRATIONS
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.INVOICE_NUMBER
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CUSTOMER_NAME
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CUSTOMER_NUMBER
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ADDRESS1
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ADDRESS2
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ADDRESS3
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CITY
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.STATE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.ZIP
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CUSTOMER_COUNTRY_CODE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.EMAIL_ADDRESS
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PHONE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.SALE_TYPE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.SKU
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PRODUCT_NAME
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.SERIAL_NUMBER
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.UNITS
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.UNIT_PRICE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.DOCUMENTATION_TYPE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.STATUS
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.STATUS_CHANGE_DETAIL
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.CREATE_DATE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC1
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC2
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC3
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC4
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.MISC5
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.EXTENDED_PRICE
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.COMPANY_NAME
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.TOTAL_EARNINGS
,    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.TOTAL_PAYOUT
FROM
    UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@pax_group_id, 1, 20) UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP
INNER JOIN RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT
    ON UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP.[id] = RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.PAX_GROUP_ID
LEFT OUTER JOIN #criteria_list_group criteria_list_group
    ON RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.GROUP_ID = criteria_list_group.GROUP_ID
INNER JOIN #temp_criteria temp_criteria
    ON RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.product_id = temp_criteria.PRODUCT_ID
    OR RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.product_id IS NULL
WHERE
    RPT_SALES_INCENTIVE_PERFORMANCE_EXTRACT.SALE_DATE BETWEEN @sale_date_from AND @sale_date_thru

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES]'
GO
/*==============================================================*/
/* View: VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES       */
/*==============================================================*/
ALTER view [component].[VW_APPLICATION_FIELD_APPLICATION_TEXT_PROPERTIES] (APPLICATION_FIELD_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS APPLICATION_FIELD_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1700
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PROGRAM_APPLICATION_TEXT_PROPERTIES]'
GO
/*==============================================================*/
/* View: VW_PROGRAM_APPLICATION_TEXT_PROPERTIES                 */
/*==============================================================*/
ALTER view [component].[VW_PROGRAM_APPLICATION_TEXT_PROPERTIES] (PROGRAM_ID, APPLICATION_TEXT_PROPERTIES_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS APPLICATION_TEXT_PROPERTIES_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 1900
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP]'
GO
-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A DELIMITED LIST OF GROUP_ID(S)
-- RETURN A DISTINCT LIST OF PAX (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP]
    @delim_list_group VARCHAR(4000)
AS
BEGIN

SET NOCOUNT ON

DECLARE    @delimiter CHAR(1)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- INITIALIZATION
SET    @delimiter = ','

SELECT    pax_id AS PAX_ID
FROM    UF_ASSOCIATION_FETCH_PAX_BY_LIST_GROUP (@delim_list_group, @delimiter)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A PAX_GROUP_ID, ACTIVITY_DATE_FROM, ACTIVITY_DATE_THRU
-- RETURN (TODO)
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20110921    CREATED
-- DOHOGNTA        20150120    REMOVE SUCCESSOR_ID DEPENDENCIES
-- ERICKSRT     20150818    UPDATE PRODUCT_TYPE_NAME AND PRODUCT_CATAGORY_NAME
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_SALES_INCENTIVE_SUMMARY_DASHBOARD_REPORT]
    @pax_group_id INT
,    @activity_date_from DATETIME2
,    @activity_date_thru DATETIME2
AS
BEGIN

SET NOCOUNT ON

DECLARE @count_product_category INT
DECLARE @count_product_type INT
DECLARE @count_product INT
DECLARE @count_top INT

DECLARE @result_set TABLE (
    label NVARCHAR(40)
,    code NVARCHAR(4)
,    value INT
,    type NVARCHAR(20)
)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-- determine if there are different category code or product types
SELECT
    @count_product_category = COUNT(DISTINCT PRODUCT.PRODUCT_CATEGORY_CODE)
,    @count_product_type = COUNT(DISTINCT PRODUCT.PRODUCT_TYPE_CODE)
,    @count_product = COUNT(DISTINCT PRODUCT.PRODUCT_ID)
FROM
    SALE WITH (NOLOCK)
JOIN TRANSACTION_HEADER WITH (NOLOCK)
    ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
JOIN PAX_GROUP WITH (NOLOCK)
    ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
    AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
JOIN SALE_ITEM WITH (NOLOCK)
    ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
JOIN PRODUCT WITH (NOLOCK)
    ON SALE_ITEM.SKU = PRODUCT.SKU
    AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
WHERE
    TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru

-- product categories are multiple
IF (@count_product_category > 1)
BEGIN 
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT_CATEGORY.PRODUCT_CATEGORY_DESC
    ,    PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT_CATEGORY' 
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    JOIN PRODUCT_CATEGORY WITH (NOLOCK)
        ON PRODUCT.PRODUCT_CATEGORY_CODE = PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    WHERE
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT_CATEGORY.PRODUCT_CATEGORY_DESC
    ,    PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
    ORDER BY
        SUM(SALE_ITEM.QUANTITY) DESC

    IF (@count_product_category > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        JOIN PRODUCT_CATEGORY WITH (NOLOCK)
            ON PRODUCT.PRODUCT_CATEGORY_CODE = PRODUCT_CATEGORY.PRODUCT_CATEGORY_CODE
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END
ELSE IF (@count_product_type > 1)
BEGIN
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT_TYPE.PRODUCT_TYPE_DESC
    ,    PRODUCT_TYPE.PRODUCT_TYPE_CODE
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT_TYPE'
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    JOIN PRODUCT_TYPE WITH (NOLOCK)
        ON PRODUCT.PRODUCT_TYPE_CODE = PRODUCT_TYPE.PRODUCT_TYPE_CODE
    WHERE
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT_TYPE.PRODUCT_TYPE_DESC
    ,    PRODUCT_TYPE.PRODUCT_TYPE_CODE
    ORDER BY
        SUM(SALE_ITEM.QUANTITY) DESC

    -- prod type count more than 5 
    IF (@count_product_type > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        JOIN PRODUCT_TYPE WITH (NOLOCK)
            ON PRODUCT.PRODUCT_TYPE_CODE = PRODUCT_TYPE.PRODUCT_TYPE_CODE
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END
ELSE 
BEGIN
    -- select only top 5
    INSERT INTO @result_set
    SELECT TOP 5
        PRODUCT.PRODUCT_NAME
    ,    ''            
    ,    SUM(SALE_ITEM.QUANTITY)
    ,    'PRODUCT'
    FROM
        SALE WITH (NOLOCK)
    JOIN TRANSACTION_HEADER WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
    JOIN PAX_GROUP WITH (NOLOCK)
        ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
        AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
    JOIN SALE_ITEM WITH (NOLOCK)
        ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
    JOIN PRODUCT WITH (NOLOCK)
        ON SALE_ITEM.SKU = PRODUCT.SKU
        AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
    WHERE    
        TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    GROUP BY
        PRODUCT.PRODUCT_NAME

    IF (@count_product > 5)
    BEGIN
        -- top 5 items count
        SELECT @count_top = SUM(VALUE) FROM @result_set
        
        -- total sum substract the top 5 to get count of other
        INSERT INTO @result_set
        SELECT
            'Others'
        ,    ''
        ,    SUM(SALE_ITEM.QUANTITY) - @count_top
        ,    ''
        FROM
            SALE WITH (NOLOCK)
        JOIN TRANSACTION_HEADER WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID
        JOIN PAX_GROUP WITH (NOLOCK)
            ON PAX_GROUP.PAX_GROUP_ID = @pax_group_id
            AND PAX_GROUP.PAX_GROUP_ID = TRANSACTION_HEADER.PAX_GROUP_ID
        JOIN SALE_ITEM WITH (NOLOCK)
            ON SALE.TRANSACTION_HEADER_ID = SALE_ITEM.TRANSACTION_HEADER_ID
        JOIN PRODUCT WITH (NOLOCK)
            ON SALE_ITEM.SKU = PRODUCT.SKU
            AND SALE_ITEM.PRODUCT_ID = PRODUCT.PRODUCT_ID
        WHERE
            TRANSACTION_HEADER.ACTIVITY_DATE BETWEEN @activity_date_from AND @activity_date_thru
    END
END

SELECT * FROM @result_set 

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[RANK]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[RANK] DROP
COLUMN [VERSION]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RANK_1] on [component].[RANK]'
GO
CREATE NONCLUSTERED INDEX [IX_RANK_1] ON [component].[RANK] ([ACTIVITY_BUSINESS_RULE_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_DATABUS_EARNINGS_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_DATABUS_EARNINGS_REPORT
-- ===========================  DESCRIPTION  ==================================
-- Returns pax earnings information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- poolda       20170803    Created
-- ===========================  DECLARATIONS  =================================
ALTER PROCEDURE [component].[UP_DATABUS_EARNINGS_REPORT] (
    @pageNumber      INT = 1,
    @recordsPerPage  INT = -1,
       @paxGroupId      BIGINT,
    @programId       BIGINT = null,
    @startPayoutDate DATETIME2(7) = null,
    @endPayoutDate   DATETIME2(7) = null

) AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @recordCount INT,
    @pageCount INT,
    @startId INT,
    @endId INT

    CREATE TABLE #tempTable (
        tempTableId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        pax_group_id   BIGINT,
        owner_group_id BIGINT,
        pax_id         BIGINT,
        control_num    NVARCHAR(255),
        first_name     NVARCHAR(100),
        last_name      NVARCHAR(100),
        role_code      NVARCHAR(50),
        corp_name      NVARCHAR(255),
        region_name    NVARCHAR(255),
        zone_name      NVARCHAR(255),
        district_name  NVARCHAR(255),
        dealer_name    NVARCHAR(255),
        promotion_name NVARCHAR(100),
        promotion_id   BIGINT,
        quantity       INT,
        revenue        DECIMAL(10,2),
        payout_type    NVARCHAR(50),
        payout         DECIMAL(10,2),
        payout_date    DATE
    )

    INSERT into #tempTable (
        pax_group_id,
        owner_group_id,
        pax_id,
        control_num,
        first_name,
        last_name,
        role_code,
        promotion_name,
        promotion_id,
        quantity,
        revenue,
        payout_type,
        payout,
        payout_date
    ) SELECT
            pg.PAX_GROUP_ID
            ,pg.OWNER_GROUP_ID
            ,pax.PAX_ID
            ,pax.CONTROL_NUM
            ,pax.FIRST_NAME
            ,pax.LAST_NAME
            ,pg.ROLE_CODE
            ,pro.PROGRAM_NAME
            ,pro.PROGRAM_ID
            ,ISNULL(sih.QUANTITY, 1) as 'QUANTITY'
            ,sih.SALE_TOTAL
            ,pay.PAYOUT_TYPE_CODE
            ,pay.PAYOUT_AMOUNT
            ,pay.PAYOUT_DATE
        FROM component.PAX_GROUP pg 
            JOIN component.PAX pax ON pax.PAX_ID = pg.PAX_ID
            JOIN component.TRANSACTION_HEADER th on th.PAX_GROUP_ID = pg.PAX_GROUP_ID
            JOIN component.PROGRAM pro on pro.PROGRAM_ID = th.PROGRAM_ID
            left outer JOIN component.SALE sa on sa.TRANSACTION_HEADER_ID = th.ID
            left outer JOIN component.SALE_ITEM_HEADER sih on sih.SALE_ID = sa.ID
            JOIN component.TRANSACTION_HEADER_EARNINGS the on the.TRANSACTION_HEADER_ID = th.ID
            JOIN component.EARNINGS e on e.EARNINGS_ID = the.EARNINGS_ID
            JOIN component.EARNINGS_PAYOUT ep on ep.EARNINGS_ID = e.EARNINGS_ID
            JOIN component.PAYOUT pay on pay.PAYOUT_ID = ep.PAYOUT_ID
        WHERE pg.owner_group_id = @paxGroupId and pg.thru_date is null
        AND (
                (@startPayoutDate is null OR @endPayoutDate is null)
            OR
                (pay.PAYOUT_DATE BETWEEN @startPayoutDate AND @endPayoutDate)
            )
        AND (
                (@programId is null)
            OR
                (@programId = pro.PROGRAM_ID)
            )


    UPDATE #tempTable
    SET dealer_name = dealer_pax.COMPANY_NAME,
        district_name = district_pax.COMPANY_NAME,
        zone_name = zone_pax.COMPANY_NAME,
        region_name = region_pax.COMPANY_NAME,
        corp_name = corp_pax.COMPANY_NAME
    FROM #tempTable
        left outer JOIN component.PAX_GROUP dealer_pax_group ON dealer_pax_group.PAX_GROUP_ID = #tempTable.OWNER_GROUP_ID
        left outer JOIN component.PAX dealer_pax ON dealer_pax.PAX_ID = dealer_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP district_pax_group ON district_pax_group.PAX_GROUP_ID = dealer_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX district_pax ON district_pax.PAX_ID = district_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP zone_pax_group ON zone_pax_group.PAX_GROUP_ID = district_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX zone_pax ON zone_pax.PAX_ID = zone_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP region_pax_group ON region_pax_group.PAX_GROUP_ID = zone_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX region_pax ON region_pax.PAX_ID = region_pax_group.PAX_ID
        left outer join component.PAX_GROUP corp_pax_group ON corp_pax_group.PAX_GROUP_ID = region_pax_group.OWNER_GROUP_ID
        left outer join component.PAX corp_pax ON corp_pax.PAX_ID = corp_pax_group.PAX_ID


    -- Left justify hierarchy column data
    DECLARE
    @counter     INT = 1,
    @max         INT = 0,
    @minColumnId INT = 0
    SELECT @max = COUNT(1) FROM #tempTable

    CREATE TABLE #tempHierarchyRow (
        columnId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        value       nvarchar(255)
    )

    WHILE @counter <= @max
    BEGIN
        INSERT INTO #tempHierarchyRow
            SELECT corp_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT region_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT zone_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT district_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT dealer_name FROM #tempTable WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow WHERE value is null;

        SELECT @minColumnId = min(columnId) FROM #tempHierarchyRow
        UPDATE #tempTable SET corp_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId)  WHERE tempTableId = @counter
        UPDATE #tempTable SET region_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 1)  WHERE tempTableId = @counter
        UPDATE #tempTable SET zone_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 2)  WHERE tempTableId = @counter
        UPDATE #tempTable SET district_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 3)  WHERE tempTableId = @counter
        UPDATE #tempTable SET dealer_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 4)  WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow

        SET @counter = @counter + 1
    END
    DROP TABLE #tempHierarchyRow
    -- End of left justify hierarchy column data

    CREATE TABLE #reportTable (
        report_table_id BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        control_num    NVARCHAR(255),
        first_name     NVARCHAR(100),
        last_name      NVARCHAR(100),
        role_code      NVARCHAR(50),
        corp_name      NVARCHAR(255),
        region_name    NVARCHAR(255),
        zone_name      NVARCHAR(255),
        district_name  NVARCHAR(255),
        dealer_name    NVARCHAR(255),
        promotion_name NVARCHAR(100),
        promotion_id   BIGINT,
        quantity       INT,
        revenue        DECIMAL(10,2),
        payout_type    NVARCHAR(50),
        payout         DECIMAL(10,2),
        payout_date    DATE
    )

    INSERT into #reportTable (
        control_num,
        first_name,
        last_name,
        role_code,
        corp_name,
        region_name,
        zone_name,
        district_name,
        dealer_name,
        promotion_name,
        promotion_id,
        quantity,
        revenue,
        payout_type,
        payout,
        payout_date
    )
    SELECT
        control_num,
        first_name,
        last_name,
        role_code,
        corp_name,
        region_name,
        zone_name,
        district_name,
        dealer_name,
        promotion_name,
        promotion_id,
        sum(quantity),
        sum(revenue),
        payout_type,
        sum(payout),
        payout_date
    FROM #tempTable
    WHERE payout > 0
    GROUP BY control_num, promotion_id, payout_date, first_name, last_name, role_code,
        corp_name, region_name, zone_name, district_name, dealer_name,
        promotion_name, payout_type
    ORDER BY region_name, zone_name, district_name, dealer_name, last_name, first_name, promotion_name, payout_date

    SELECT @recordCount = @@ROWCOUNT

    DROP TABLE #tempTable

    IF @recordsPerPage = -1
    BEGIN
        SET @recordsPerPage = @recordCount
        SET @pageNumber = 1
    END

    IF @recordsPerPage = 0
    BEGIN
        SET @recordsPerPage = 1
    END

    --Make sure the requested page number is within the valid range of pages (if not, then fix it)
    SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
    IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
    IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

    SET @endId = @pageNumber * @recordsPerPage
    SET @startId = (@endId - @recordsPerPage) + 1

    -- Adjust dates, i.e. push start date back to beginning of day and end date out to end of day
    set @startPayoutDate = convert(date, @startPayoutDate)
    set @endPayoutDate = convert(date, dateadd(day, 1, @endPayoutDate))

    SELECT 
        control_num     AS 'CONTROL_NUM',
        first_name      AS 'FIRST_NAME',
        last_name       AS 'LAST_NAME',
        role_code       AS 'ROLE_CODE',
        corp_name       AS 'HIERARCHY_L1',
        region_name     AS 'HIERARCHY_L2',
        zone_name       AS 'HIERARCHY_L3',
        district_name   AS 'HIERARCHY_L4',
        dealer_name     AS 'HIERARCHY_L5',
        promotion_name  AS 'PROMOTION_NAME',
        promotion_id    AS 'PROMOTION_ID',
        quantity        AS 'QUANTITY',
        revenue         AS 'REVENUE',
        payout_type     AS 'PAYOUT_TYPE',
        payout          AS 'PAYOUT',
        payout_date     AS 'PAYOUT_DATE',
        @recordCount    AS 'RECORD_COUNT',
        @recordsPerPage AS 'RECORDS_PER_PAGE',
        @pageNumber     AS 'PAGE_NUMBER',
        @pageCount      AS 'PAGE_COUNT',
        @startId        AS 'START_ID',
        @endId          AS 'END_ID'
    FROM #ReportTable
    
    DROP TABLE #reportTable

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[CAMPAIGN]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[CAMPAIGN] DROP
COLUMN [FROM_DATE],
COLUMN [THRU_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[STAGE_HIERARCHY]'
GO
CREATE TABLE [component].[STAGE_HIERARCHY]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[BATCH_ID] [bigint] NOT NULL,
[CONTROL_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_PREFIX] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_SUFFIX] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPANY_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LANGUAGE_PREFERENCE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROLE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROLE_START_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ROLE_END_DATE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOGIN_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PASSWORD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL_ADDRESS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_EXTENSION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_6] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_PROVINCE_REGION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY_CODE] [nvarchar] (225) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORGANIZATION_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARENT_PAX_ID] [bigint] NULL,
[PARENT_PAX_GROUP_ID] [bigint] NULL,
[PARENT_CONTROL_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARENT_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PARENT_ROLE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HIRE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TERMINATION_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTOM_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTOM_2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTOM_3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTOM_4] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CUSTOM_5] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAX_ID] [bigint] NULL,
[PAX_GROUP_ID] [bigint] NULL,
[ADDRESS_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PHONE_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SYS_USER_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS_TYPE_CODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAX_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PAX_GROUP_CHANGE_STATUS] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USER_FUNCTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HIERARCHY_ERROR] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_STAGE_HIERARCHY_CREATE_DATE] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_STAGE_ENROLLMENT] on [component].[STAGE_HIERARCHY]'
GO
ALTER TABLE [component].[STAGE_HIERARCHY] ADD CONSTRAINT [PK_STAGE_HIERARCHY] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_STAGE_HIERARCHY] on [component].[STAGE_HIERARCHY]'
GO
CREATE NONCLUSTERED INDEX [IX_STAGE_HIERARCHY] ON [component].[STAGE_HIERARCHY] ([CONTROL_NUM])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_GET_SORTED_STAGE_HIERARCHY_FOR_BATCH]'
GO
-- ==============================  NAME  ======================================
-- UP_GET_SORTED_STAGE_HIERARCHY_FOR_BATCH
-- ===========================  DESCRIPTION  ==================================
-- Returns a sorted list of records in STAGE_HIERARCHY for a given batch ID.
-- The list used by the second step of the Hierarchy ETL.
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- irwinmb1     20170831    Created
-- irwinmb1     20170919    Fixes infinite loop
-- ===========================  DECLARATIONS  =================================

CREATE PROCEDURE [component].[UP_GET_SORTED_STAGE_HIERARCHY_FOR_BATCH]
    @batchid BIGINT
AS
BEGIN
    SET NOCOUNT ON
    SET CONCAT_NULL_YIELDS_NULL OFF

    CREATE TABLE #tmp (
        ID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY
        ,STAGE_HIERARCHY_ID BIGINT NOT NULL
        ,CONTROL_NUM VARCHAR(255) NULL
    )

    WHILE EXISTS (
        SELECT TOP 1 * 
        FROM component.STAGE_HIERARCHY 
        WHERE BATCH_ID = @batchid 
        AND ID NOT IN (select STAGE_HIERARCHY_ID FROM #tmp)
    )
    BEGIN
        INSERT INTO #tmp (STAGE_HIERARCHY_ID, CONTROL_NUM)
        SELECT ID, CONTROL_NUM 
        FROM component.STAGE_HIERARCHY 
        WHERE BATCH_ID = @batchid
        AND (
            PARENT_CONTROL_NUM IS NULL 
            OR PARENT_CONTROL_NUM NOT IN (SELECT CONTROL_NUM FROM component.STAGE_HIERARCHY WHERE BATCH_ID = @batchid AND CONTROL_NUM IS NOT NULL)
            OR PARENT_CONTROL_NUM IN (SELECT CONTROL_NUM FROM #tmp) 
        ) 
        AND ID NOT IN (SELECT STAGE_HIERARCHY_ID FROM #tmp)
    END

    SELECT sh.ID
    , sh.BATCH_ID
    , sh.CONTROL_NUM
    , sh.STATUS
    , sh.NAME_PREFIX
    , sh.FIRST_NAME
    , sh.MIDDLE_NAME
    , sh.LAST_NAME
    , sh.NAME_SUFFIX
    , sh.COMPANY_NAME
    , sh.LANGUAGE_PREFERENCE
    , sh.ROLE_CODE
    , sh.ROLE_START_DATE
    , sh.ROLE_END_DATE
    , sh.LOGIN_ID
    , sh.PASSWORD
    , sh.EMAIL_ADDRESS
    , sh.PHONE_NUMBER
    , sh.PHONE_EXTENSION
    , sh.ADDRESS_LINE_1 as ADDRESSLINE1
    , sh.ADDRESS_LINE_2 as ADDRESSLINE2
    , sh.ADDRESS_LINE_3 as ADDRESSLINE3
    , sh.ADDRESS_LINE_4 as ADDRESSLINE4
    , sh.ADDRESS_LINE_5 as ADDRESSLINE5
    , sh.ADDRESS_LINE_6 as ADDRESSLINE6
    , sh.CITY
    , sh.STATE_PROVINCE_REGION
    , sh.ZIP_CODE
    , sh.COUNTRY_CODE
    , sh.ORGANIZATION_CODE
    , sh.PARENT_PAX_ID
    , sh.PARENT_PAX_GROUP_ID
    , sh.PARENT_CONTROL_NUM
    , sh.PARENT_NAME
    , sh.PARENT_ROLE_CODE
    , sh.HIRE_DATE
    , sh.TERMINATION_DATE
    , sh.CUSTOM_1 as CUSTOM1
    , sh.CUSTOM_2 as CUSTOM2
    , sh.CUSTOM_3 as CUSTOM3
    , sh.CUSTOM_4 as CUSTOM4
    , sh.CUSTOM_5 as CUSTOM5
    , sh.PAX_ID
    , sh.PAX_GROUP_ID
    , sh.ADDRESS_CHANGE_STATUS
    , sh.EMAIL_CHANGE_STATUS
    , sh.PHONE_CHANGE_STATUS
    , sh.SYS_USER_CHANGE_STATUS
    , sh.STATUS_TYPE_CODE
    , sh.PAX_CHANGE_STATUS
    , sh.PAX_GROUP_CHANGE_STATUS
    , sh.USER_FUNCTION
    , sh.HIERARCHY_ERROR
    , sh.CREATE_DATE
    FROM component.STAGE_HIERARCHY sh
    LEFT JOIN #tmp t ON t.STAGE_HIERARCHY_ID = sh.ID
    WHERE sh.BATCH_ID = @batchid
    ORDER BY CASE WHEN t.ID IS NULL THEN 1 ELSE 0 END, t.ID

    DROP TABLE #tmp

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[VW_PROGRAM_RECOGNITION_STOCK_PHRASE]'
GO
/*==============================================================*/
/* View: VW_PROGRAM_RECOGNITION_STOCK_PHRASE                    */
/*==============================================================*/
ALTER view [component].[VW_PROGRAM_RECOGNITION_STOCK_PHRASE] (PROGRAM_ID, RECOGNITION_STOCK_PHRASE_ID, CREATE_DATE, CREATE_ID, UPDATE_DATE, UPDATE_ID) as
SELECT  FK1 AS PROGRAM_ID
,       FK2 AS RECOGNITION_STOCK_PHRASE_ID
,       CREATE_DATE
,       CREATE_ID
,       UPDATE_DATE
,       UPDATE_ID
FROM    ASSOCIATION
WHERE   ASSOCIATION_TYPE_ID = 2000
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_DATABUS_WALLET_SUMMARY]'
GO
CREATE PROCEDURE [component].[UP_DATABUS_WALLET_SUMMARY] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'programName',
  @sortDir              NVARCHAR(1) = 'A',
  @depositedOnly        NVARCHAR(1) = 'N',
  @paxId                BIGINT
) AS BEGIN
  SET NOCOUNT ON

  DECLARE @recordCount INT,
  @pageCount INT,
  @startId INT,
  @endId INT

  CREATE TABLE #tempTable(
    TEMP_TABLE_ID INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    PROGRAM_ID                    BIGINT,
    PROGRAM_NAME                NVARCHAR(255),
    CARD_TYPE_CODE                NVARCHAR(255),
    AMOUNT_PENDING                DECIMAL(10,2),
    AMOUNT_DEPOSITED                DECIMAL(10,2)
  )

  CREATE TABLE #ordered (
    [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    data_id INT                 NOT NULL
  )

  INSERT INTO #tempTable(PROGRAM_ID, PROGRAM_NAME, CARD_TYPE_CODE, AMOUNT_PENDING, AMOUNT_DEPOSITED)
  select prg.PROGRAM_ID
    , prg.PROGRAM_NAME
    , pay_i.CARD_TYPE_CODE
    ,isnull(sum(case when pay.status_type_code = 'REQUESTED' then pay.payout_amount else 0 end),0) as AMOUNT_PENDING 
    ,isnull(sum(case when pay.status_type_code = 'ISSUED' then pay.payout_amount else 0 end),0) as AMOUNT_DEPOSITED 
  from component.payout pay 
    inner join component.earnings_payout ep on ep.payout_id = pay.payout_id 
    inner join component.earnings e on e.earnings_id = ep.earnings_id 
    inner join component.TRANSACTION_HEADER_EARNINGS the on the.EARNINGS_ID = ep.EARNINGS_ID
    inner join component.TRANSACTION_HEADER th on th.ID = the.TRANSACTION_HEADER_ID
    inner join component.PROGRAM prg on prg.PROGRAM_ID = th.PROGRAM_ID
    inner join component.PAYOUT_ITEM pay_i on pay_i.PAYOUT_ITEM_ID = pay.PAYOUT_ITEM_ID
  where pay.pax_id = @paxId
    AND (
            (@depositedOnly = 'Y' AND pay.STATUS_TYPE_CODE = 'ISSUED')
                OR
            (@depositedOnly = 'N' AND pay.status_type_code in ('REQUESTED','ISSUED'))
        )
  Group by prg.PROGRAM_ID, prg.PROGRAM_NAME, pay_i.CARD_TYPE_CODE

  INSERT INTO #ordered (data_id)
    SELECT tt.TEMP_TABLE_ID
    FROM #tempTable tt
    ORDER BY
        CASE @sortDir
        WHEN 'D'
        THEN
            CASE @sortBy
            WHEN 'programName'
            THEN tt.PROGRAM_NAME
            WHEN 'cardTypeCode'
            THEN tt.CARD_TYPE_CODE
            END
        END DESC,
        CASE @sortDir
        WHEN 'A'
        THEN
            CASE @sortBy
            WHEN 'programName'
            THEN tt.PROGRAM_NAME
            WHEN 'cardTypeCode'
            THEN tt.CARD_TYPE_CODE
            END
        END ASC

  SELECT @recordCount = @@ROWCOUNT

  IF @recordsPerPage = -1
    BEGIN
      SET @recordsPerPage = @recordCount
      SET @pageNumber = 1
    END

  IF @recordsPerPage = 0
    BEGIN
      SET @recordsPerPage = 1
    END

  --Make sure the requested page number is within the valid range of pages (if not, then fix it)
  SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
  IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
  IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

  SET @endId = @pageNumber * @recordsPerPage
  SET @startId = (@endId - @recordsPerPage) + 1

  SELECT
    PROGRAM_ID,
    PROGRAM_NAME,
    CARD_TYPE_CODE,
    AMOUNT_PENDING,
    AMOUNT_DEPOSITED,
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
  FROM #ordered o
    INNER JOIN #tempTable tt ON tt.TEMP_TABLE_ID = o.data_id
  WHERE o.id BETWEEN @startId AND @endId
  ORDER BY o.id

  DROP TABLE #tempTable
  DROP TABLE #ordered
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[UP_EARNINGS_BY_CHILD]'
GO
-- ==============================  NAME  ======================================
-- UP_EARNINGS_BY_CHILD
-- ===========================  DESCRIPTION  ==================================
-- TODO: DOCUMENT
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- VANTEDS        20120214    CREATED
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150814    UPDATES FOR UDM 3
--
-- ===========================  DECLARATIONS  =================================

ALTER PROCEDURE [component].[UP_EARNINGS_BY_CHILD]
    @pax_group_id BIGINT
,    @from_date DATETIME2
,    @thru_date DATETIME2
AS
BEGIN
    SET NOCOUNT ON

    DECLARE @results TABLE (
        pax_group_id BIGINT
    ,    earnings_amount INT
    ,    grandchild_count INT
    ) 
    
    DECLARE @results_final TABLE (
        pax_group_id BIGINT
    ,    earnings_amount INT
    ,    grandchild_count INT
    ,    company_name NVARCHAR(255)
    ,    first_name NVARCHAR(30)
    ,    last_name NVARCHAR(40)
    )
    
    DECLARE @child_pax_group_id BIGINT
    
    DECLARE c_list CURSOR FOR SELECT PAX_GROUP_ID FROM PAX_GROUP WHERE OWNER_GROUP_ID = @pax_group_id
    OPEN c_list
    
    FETCH NEXT FROM c_list INTO @child_pax_group_id
    
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        INSERT INTO @results (pax_group_id, earnings_amount)
        SELECT
            @child_pax_group_id
        ,    SUM(P.PAYOUT_AMOUNT)
        FROM
            UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@child_pax_group_id, 1, NULL)
        JOIN PAX_GROUP PG
            ON [ID] = PG.PAX_GROUP_ID
        JOIN TRANSACTION_HEADER TH
            ON PG.PAX_GROUP_ID = TH.PAX_GROUP_ID
            AND TH.ACTIVITY_DATE >= CONVERT(DATETIME2, @from_date, 102)
            AND TH.ACTIVITY_DATE < CONVERT(DATETIME2, @thru_date, 102)
        JOIN TRANSACTION_HEADER_EARNINGS THE
            ON THE.TRANSACTION_HEADER_ID = TH.[ID]
        JOIN EARNINGS_PAYOUT EP
            ON EP.EARNINGS_ID = THE.EARNINGS_ID
        JOIN PAYOUT P
            ON P.PAYOUT_ID = EP.PAYOUT_ID
            AND P.STATUS_TYPE_CODE = 'ISSUED'
    
        UPDATE @results set grandchild_count = (
            SELECT
                COUNT(OG2.ORG_GROUP_ID)
            FROM
                UF_HIERARCHY_FETCH_PAX_GROUP_BY_PAX_GROUP(@child_pax_group_id,1,1) fn
            ,    ORG_GROUP OG
            ,    ORG_GROUP OG2
            ,    PAX_GROUP PG
            WHERE PG.PAX_GROUP_ID = FN.[ID]
            AND FN.LEVEL <> 0
            AND PG.ORGANIZATION_CODE = OG.ORGANIZATION_CODE
            AND PG.ROLE_CODE = OG.ROLE_CODE
            AND OG.ORG_GROUP_ID = OG2.OWNER_GROUP_ID)
        WHERE PAX_GROUP_ID = @CHILD_PAX_GROUP_ID

        FETCH NEXT FROM c_list INTO @child_pax_group_id
    END
    
    CLOSE c_list
    DEALLOCATE c_list
    
    INSERT INTO @results_final
    SELECT     r.pax_group_id
    ,    r.earnings_amount
    ,    r.grandchild_count
    ,    P.COMPANY_NAME
    ,    P.FIRST_NAME
    ,    P.LAST_NAME
    FROM
        @results r
    JOIN PAX_GROUP PG
        ON r.pax_group_id = PG.PAX_GROUP_ID
    JOIN PAX P
        ON PG.PAX_ID = P.PAX_ID
    WHERE r.earnings_amount IS NOT NULL
        
    IF (SELECT COUNT(*) FROM @results_final) > 0
    BEGIN
         SELECT * FROM @results_final
    END
    ELSE
    BEGIN
         SELECT 'No Data' as label, 0 as value
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[EVENT_COMMUNICATION]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[EVENT_COMMUNICATION] ADD
[PRECEDENCE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[EVENT_COMMUNICATION] ALTER COLUMN [TEMPLATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_DATABUS_WALLET_DETAIL]'
GO
create PROCEDURE [component].[UP_DATABUS_WALLET_DETAIL] (
  @pageNumber           INT = 1,
  @recordsPerPage       INT = -1,
  @sortBy               NVARCHAR(50) = 'dateDeposited',
  @sortDir              NVARCHAR(1) = 'D',
  @depositedOnly        NVARCHAR(1) = 'N',
  @paxId                BIGINT,
  @programId            BIGINT
) AS BEGIN
  SET NOCOUNT ON

  DECLARE @recordCount INT,
  @pageCount INT,
  @startId INT,
  @endId INT

  CREATE TABLE #tempTable(
    TEMP_TABLE_ID INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    PROGRAM_ID                    BIGINT,
    PROGRAM_NAME                NVARCHAR(255),
    DESCRIPTION                    NVARCHAR(255),
    CARD_TYPE_CODE                NVARCHAR(255),
    AMOUNT_PENDING                DECIMAL(10,2),
    AMOUNT_DEPOSITED            DECIMAL(10,2),
    DATE_DEPOSITED                DATETIME2(7)
  )

  CREATE TABLE #ordered (
    [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
    data_id INT                 NOT NULL
  )

  INSERT INTO #tempTable(PROGRAM_ID, PROGRAM_NAME, DESCRIPTION, CARD_TYPE_CODE, AMOUNT_PENDING, AMOUNT_DEPOSITED, DATE_DEPOSITED)
  select prg.PROGRAM_ID
    , prg.PROGRAM_NAME
    , pay.payout_desc as [DESCRIPTION]  
    , pay_i.CARD_TYPE_CODE
    , isnull(case when pay.status_type_code = 'REQUESTED' then pay.payout_amount else 0 end,0) as AMOUNT_PENDING  
    , isnull(case when pay.status_type_code = 'ISSUED' then pay.payout_amount else 0 end,0) as AMOUNT_DEPOSITED  
    , case when pay.status_type_code = 'ISSUED' then pay.payout_date else null end as DATE_DEPOSITED  
  from component.payout pay 
    inner join component.earnings_payout ep on ep.payout_id = pay.payout_id 
    inner join component.earnings e on e.earnings_id = ep.earnings_id 
    inner join component.TRANSACTION_HEADER_EARNINGS the on the.EARNINGS_ID = ep.EARNINGS_ID
    inner join component.TRANSACTION_HEADER th on th.ID = the.TRANSACTION_HEADER_ID
    inner join component.PROGRAM prg on prg.PROGRAM_ID = th.PROGRAM_ID
    inner join component.PAYOUT_ITEM pay_i on pay_i.PAYOUT_ITEM_ID = pay.PAYOUT_ITEM_ID
  where pay.pax_id = @paxId
    and prg.PROGRAM_ID = @programId
    AND (
            (@depositedOnly = 'Y' AND pay.STATUS_TYPE_CODE = 'ISSUED')
                OR
            (@depositedOnly = 'N' AND pay.status_type_code in ('REQUESTED','ISSUED'))
        )

  INSERT INTO #ordered (data_id)
    SELECT tt.TEMP_TABLE_ID
    FROM #tempTable tt
    ORDER BY
        CASE @sortDir
        WHEN 'D'
        THEN
            CASE @sortBy
            WHEN 'dateDeposited'
            THEN tt.DATE_DEPOSITED
            WHEN 'programName'
            THEN tt.PROGRAM_NAME
            WHEN 'cardTypeCode'
            THEN tt.CARD_TYPE_CODE
            END
        END DESC,
        CASE @sortDir
        WHEN 'A'
        THEN
            CASE @sortBy
            WHEN 'dateDeposited'
            THEN tt.DATE_DEPOSITED
            WHEN 'programName'
            THEN tt.PROGRAM_NAME
            WHEN 'cardTypeCode'
            THEN tt.CARD_TYPE_CODE
            END
        END ASC

  SELECT @recordCount = @@ROWCOUNT

  IF @recordsPerPage = -1
    BEGIN
      SET @recordsPerPage = @recordCount
      SET @pageNumber = 1
    END

  IF @recordsPerPage = 0
    BEGIN
      SET @recordsPerPage = 1
    END

  --Make sure the requested page number is within the valid range of pages (if not, then fix it)
  SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
  IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
  IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

  SET @endId = @pageNumber * @recordsPerPage
  SET @startId = (@endId - @recordsPerPage) + 1

  SELECT
    PROGRAM_ID,
    PROGRAM_NAME,
    DESCRIPTION,
    CARD_TYPE_CODE,
    AMOUNT_PENDING,
    AMOUNT_DEPOSITED,
    DATE_DEPOSITED,
    @recordCount    AS RECORD_COUNT,
    @recordsPerPage AS RECORDS_PER_PAGE,
    @pageNumber     AS PAGE_NUMBER,
    @pageCount      AS PAGE_COUNT,
    @startId        AS START_ID,
    @endId          AS END_ID
  FROM #ordered o
    INNER JOIN #tempTable tt ON tt.TEMP_TABLE_ID = o.data_id
  WHERE o.id BETWEEN @startId AND @endId
  ORDER BY o.id

  DROP TABLE #tempTable
  DROP TABLE #ordered
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_BUDGET_1] on [component].[BUDGET]'
GO
CREATE NONCLUSTERED INDEX [IX_BUDGET_1] ON [component].[BUDGET] ([BUDGET_CATEGORY_CODE], [BUDGET_TYPE_CODE], [BUDGET_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FILES_2] on [component].[FILES]'
GO
CREATE NONCLUSTERED INDEX [IX_FILES_2] ON [component].[FILES] ([FILE_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_FILES_1] on [component].[FILES]'
GO
CREATE NONCLUSTERED INDEX [IX_FILES_1] ON [component].[FILES] ([NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[FEED_ITEM]'
GO
ALTER TABLE [component].[FEED_ITEM] ADD CONSTRAINT [DF_FEED_ITEM_FEED_ITEM_DATE] DEFAULT (getdate()) FOR [FEED_ITEM_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[BUDGET_MISC]'
GO
ALTER TABLE [component].[BUDGET_MISC] ADD CONSTRAINT [FK_BUDGET_MISC_BUDGET] FOREIGN KEY ([BUDGET_ID]) REFERENCES [component].[BUDGET] ([ID])
GO
ALTER TABLE [component].[BUDGET_MISC] ADD CONSTRAINT [FK_BUDGET_MISC_MISC_TYPE] FOREIGN KEY ([MISC_TYPE_CODE]) REFERENCES [component].[MISC_TYPE] ([MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[EARNINGS_PAYOUT]'
GO
ALTER TABLE [component].[EARNINGS_PAYOUT] ADD CONSTRAINT [FK_EARNINGS_PAYOUT_ACTIVITY_BUSINESS_RULE] FOREIGN KEY ([ACTIVITY_BUSINESS_RULE_ID]) REFERENCES [component].[ACTIVITY_BUSINESS_RULE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[FILE_ITEM]'
GO
ALTER TABLE [component].[FILE_ITEM] ADD CONSTRAINT [FK_FILE_ITEM_FILES] FOREIGN KEY ([FILES_ID]) REFERENCES [component].[FILES] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE] ADD CONSTRAINT [FK_TRANSACTION_HEADER_EARNINGS_MESSAGE_MESSAGE_TYPE] FOREIGN KEY ([MESSAGE_TYPE_CODE]) REFERENCES [component].[MESSAGE_TYPE] ([MESSAGE_TYPE_CODE])
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS_MESSAGE] ADD CONSTRAINT [FK_TRANSACTION_HEADER_EARNINGS_MESSAGE_TRANSACTION_HEADER_EARNINGS] FOREIGN KEY ([TRANSACTION_HEADER_EARNINGS_ID]) REFERENCES [component].[TRANSACTION_HEADER_EARNINGS] ([TRANSACTION_HEADER_EARNINGS_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[MESSAGE_TYPE]'
GO
ALTER TABLE [component].[MESSAGE_TYPE] ADD CONSTRAINT [FK_MESSAGE_TYPE_STATUS_TYPE] FOREIGN KEY ([DISPLAY_STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[METRIC]'
GO
ALTER TABLE [component].[METRIC] ADD CONSTRAINT [FK_METRIC_TRANSACTION_HEADER] FOREIGN KEY ([TRANSACTION_HEADER_ID]) REFERENCES [component].[TRANSACTION_HEADER] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NOMINATION_MISC]'
GO
ALTER TABLE [component].[NOMINATION_MISC] ADD CONSTRAINT [FK_NOMINATION_ID] FOREIGN KEY ([NOMINATION_ID]) REFERENCES [component].[NOMINATION] ([ID])
GO
ALTER TABLE [component].[NOMINATION_MISC] ADD CONSTRAINT [FK_NOMINATION_MISC_MISC_TYPE] FOREIGN KEY ([MISC_TYPE_CODE]) REFERENCES [component].[MISC_TYPE] ([MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_EVENT_COMMUNICATION]'
GO
ALTER TABLE [component].[PAX_EVENT_COMMUNICATION] ADD CONSTRAINT [FK_PAX_EVENT_COMMUNICATION_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
ALTER TABLE [component].[PAX_EVENT_COMMUNICATION] ADD CONSTRAINT [FK_PAX_EVENT_COMMUNICATION_EVENT_COMMUNICATION] FOREIGN KEY ([EVENT_COMMUNICATION_ID]) REFERENCES [component].[EVENT_COMMUNICATION] ([ID])
GO
ALTER TABLE [component].[PAX_EVENT_COMMUNICATION] ADD CONSTRAINT [FK_PAX_EVENT_COMMUNICATION_STATUS_TYPE] FOREIGN KEY ([STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAYOUT_ITEM]'
GO
ALTER TABLE [component].[PAYOUT_ITEM] ADD CONSTRAINT [FK_PAYOUT_ITEM_CARD_TYPE] FOREIGN KEY ([CARD_TYPE_CODE]) REFERENCES [component].[CARD_TYPE] ([CARD_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[RANK]'
GO
ALTER TABLE [component].[RANK] ADD CONSTRAINT [FK_RANK_ACTIVITY_BUSINESS_RULE] FOREIGN KEY ([ACTIVITY_BUSINESS_RULE_ID]) REFERENCES [component].[ACTIVITY_BUSINESS_RULE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[TRANSACTION_HEADER_EARNINGS]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_EARNINGS] ADD CONSTRAINT [FK_TRANSACTION_HEADER_EARNINGS_ACTIVITY_BUSINESS_RULE] FOREIGN KEY ([ACTIVITY_BUSINESS_RULE_ID]) REFERENCES [component].[ACTIVITY_BUSINESS_RULE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT]'
GO
ALTER TABLE [component].[TRANSACTION_HEADER_OBJECTIVE_RESULT] ADD CONSTRAINT [FK_TRANSACTION_HEADER_OBJECTIVE_RESULT_ACTIVITY_BUSINESS_RULE] FOREIGN KEY ([ACTIVITY_BUSINESS_RULE_ID]) REFERENCES [component].[ACTIVITY_BUSINESS_RULE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[TRANSLATABLE]'
GO
ALTER TABLE [component].[TRANSLATABLE] ADD CONSTRAINT [FK_TRANSLATABLE_STATUS_TYPE] FOREIGN KEY ([STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[LIKES]'
GO
ALTER TABLE [component].[LIKES] ADD CONSTRAINT [FK_LIKES_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_SALE_DU_1] on [component].[SALE]'
GO
-- ==============================  NAME  ======================================
-- TR_SALE_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
-- yellams      20170410        Added SELLER_NAME, SELLER_NUMBER
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_SALE_DU_1]
ON [component].[SALE]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_SALE (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    TRANSACTION_HEADER_ID
,    ADDITIONAL_SALES_ID
,    OTHER_ID
,    INVOICE_NUMBER
,    SALE_TOTAL
,    SALE_TYPE
,    SALE_DATE
,    PURCHASE_DATE
,    MISC_DATE
,    CUSTOMER_NAME
,    CUSTOMER_NUMBER
,    CUSTOMER_COUNTRY_CODE
,    ADDRESS1
,    ADDRESS2
,    ADDRESS3
,    ADDRESS4
,    ADDRESS5
,    ADDRESS6
,    CITY
,    STATE
,    ZIP
,    EMAIL_ADDRESS
,    PHONE
,    DOCUMENTATION_TYPE
,    PENETRATION
,    SELLER_NAME 
,    SELLER_NUMBER
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    TRANSACTION_HEADER_ID
,    ADDITIONAL_SALES_ID
,    OTHER_ID
,    INVOICE_NUMBER
,    SALE_TOTAL
,    SALE_TYPE
,    SALE_DATE
,    PURCHASE_DATE
,    MISC_DATE
,    CUSTOMER_NAME
,    CUSTOMER_NUMBER
,    CUSTOMER_COUNTRY_CODE
,    ADDRESS1
,    ADDRESS2
,    ADDRESS3
,    ADDRESS4
,    ADDRESS5
,    ADDRESS6
,    CITY
,    STATE
,    ZIP
,    EMAIL_ADDRESS
,    PHONE
,    DOCUMENTATION_TYPE
,    PENETRATION
,    SELLER_NAME 
,    SELLER_NUMBER
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''SALE''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO
