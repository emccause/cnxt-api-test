/*
Run this script on:

        52.23.232.184.udm_upgrade    -  This database will be modified

to synchronize it with:

        fensqlmlmd11.CCX171D

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 9/8/2017 3:14:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_LANGUAGE_LOCALE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[PAX]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PAX] DROP
COLUMN [LANGUAGE_LOCALE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_PAX]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[HISTORY_PAX] DROP
COLUMN [LANGUAGE_LOCALE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_PAX_DU_1] on [component].[PAX]'
GO


-- ==============================  NAME  ======================================
-- TR_PAX_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR                          DATE                     CHANGE DESCRIPTION
-- dohognta                        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_PAX_DU_1]
ON [component].[PAX]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
                RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
                RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT

DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
                SET @operation = 'UPDATE'
ELSE
                SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_PAX (
                CHANGE_DATE
,               CHANGE_ID
,               CHANGE_OPERATION
,               PAX_ID
,               LANGUAGE_CODE
,               PREFERRED_LOCALE
,               CONTROL_NUM
,               SSN
,               FIRST_NAME
,               MIDDLE_NAME
,               LAST_NAME
,               NAME_PREFIX
,               NAME_SUFFIX
,               COMPANY_NAME
,               TAX_NUM
,               ALTERNATE_NAME
,               HASH
,               PAX_TYPE_CODE
,               CREATE_DATE
,               CREATE_ID
,               UPDATE_DATE
,               UPDATE_ID
)
SELECT  
                @change_date
,               @change_id
,               @change_operation
,               PAX_ID
,               LANGUAGE_CODE
,               PREFERRED_LOCALE
,               CONTROL_NUM
,               SSN
,               FIRST_NAME
,               MIDDLE_NAME
,               LAST_NAME
,               NAME_PREFIX
,               NAME_SUFFIX
,               COMPANY_NAME
,               TAX_NUM
,               ALTERNATE_NAME
,               HASH
,               PAX_TYPE_CODE
,               CREATE_DATE
,               CREATE_ID
,               UPDATE_DATE
,               UPDATE_ID
FROM
                DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
                SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''PAX''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
                SET @severity = 16 -- Indicate errors that can be corrected by the user
                GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
                IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
                PRINT 'The database update failed'
END
GO
