
CREATE TABLE [component].[HISTORY_PAX_IDENTITY]
(
[HISTORY_ID] [bigint] NOT NULL IDENTITY(1, 1),
[ID] [bigint] NULL,
[PAX_ID] [bigint] NULL,
[PAX_IDENTITY_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDENTIFIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IDENTITY_DATE] [datetime2](7) NULL,
[CREATE_DATE] [datetime2](7) NULL,
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_DATE] [datetime2](7) NULL,
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHANGE_DATE] [datetime2](7) NULL CONSTRAINT [DF_HISTORY_PAX_IDENTITY_CHANGE_DATE] DEFAULT (getdate()),
[CHANGE_ID] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHANGE_OPERATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO

ALTER TABLE [component].[HISTORY_PAX_IDENTITY] ADD CONSTRAINT [PK_HISTORY_PAX_IDENTITY] PRIMARY KEY CLUSTERED  ([HISTORY_ID])
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_HISTORY_PAX_IDENTITY_2] ON [component].[HISTORY_PAX_IDENTITY] ([ID])
GO

CREATE TABLE [component].[PAX_IDENTITY]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[PAX_ID] [bigint] NOT NULL,
[PAX_IDENTITY_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDENTIFIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IDENTITY_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_PAX_IDENTITY_IDENTITY_DATE] DEFAULT (getdate()),
[CREATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_PAX_IDENTITY_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_IDENTITY_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_PAX_IDENTITY_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_IDENTITY_UPDATE_ID] DEFAULT (user_name())
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_PAX_IDENTITY_1] ON [component].[PAX_IDENTITY] ([PAX_IDENTITY_TYPE_CODE], [IDENTIFIER])
GO

ALTER TABLE [component].[PAX_IDENTITY] ADD CONSTRAINT [PK_PAX_IDENTITY] PRIMARY KEY CLUSTERED  ([ID])
GO

CREATE NONCLUSTERED INDEX [IX_PAX_IDENTITY_2] ON [component].[PAX_IDENTITY] ([PAX_ID])
GO


-- ==============================  NAME  ======================================
-- TR_PAX_IDENTITY_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PAX_IDENTITY_DU_1]
ON [component].[PAX_IDENTITY]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())
SET @change_id = USER_NAME()

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_PAX_IDENTITY (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    PAX_ID
,    PAX_IDENTITY_TYPE_CODE
,    IDENTIFIER
,    IDENTITY_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    PAX_ID
,    PAX_IDENTITY_TYPE_CODE
,    IDENTIFIER
,    IDENTITY_DATE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''PAX_IDENTITY''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PAX_IDENTITY_U_1] on [component].[PAX_IDENTITY]'
GO



-- ==============================  NAME  ======================================
-- TR_PAX_IDENTITY_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PAX_IDENTITY_U_1]
ON [component].[PAX_IDENTITY]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PAX_IDENTITY T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PAX_IDENTITY T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[PAX_IDENTITY_TYPE]'
GO
CREATE TABLE [component].[PAX_IDENTITY_TYPE]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[PAX_IDENTITY_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PAX_IDENTITY_TYPE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PAX_IDENTITY_TYPE_DESC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_SEQUENCE] [int] NULL,
[DISPLAY_STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_PAX_IDENTITY_TYPE_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_IDENTITY_TYPE_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2](7) NOT NULL CONSTRAINT [DF_PAX_IDENTITY_TYPE_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_IDENTITY_TYPE_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PAX_IDENTITY_TYPE] on [component].[PAX_IDENTITY_TYPE]'
GO
ALTER TABLE [component].[PAX_IDENTITY_TYPE] ADD CONSTRAINT [PK_PAX_IDENTITY_TYPE] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_IDENTITY_TYPE_1] on [component].[PAX_IDENTITY_TYPE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PAX_IDENTITY_TYPE_1] ON [component].[PAX_IDENTITY_TYPE] ([PAX_IDENTITY_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_IDENTITY]'
GO
ALTER TABLE [component].[PAX_IDENTITY] ADD CONSTRAINT [FK_PAX_IDENTITY_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
ALTER TABLE [component].[PAX_IDENTITY] ADD CONSTRAINT [FK_PAX_IDENTITY_PAX_IDENTITY_TYPE] FOREIGN KEY ([PAX_IDENTITY_TYPE_CODE]) REFERENCES [component].[PAX_IDENTITY_TYPE] ([PAX_IDENTITY_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_IDENTITY_TYPE]'
GO
ALTER TABLE [component].[PAX_IDENTITY_TYPE] ADD CONSTRAINT [FK_PAX_IDENTITY_TYPE_STATUS_TYPE] FOREIGN KEY ([DISPLAY_STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO