/*
Run this script on:

        52.23.232.184.udm_upgrade    -  This database will be modified

to synchronize it with:

        fensqlmlmd11.CCX171D

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.3.3.4490 from Red Gate Software Ltd at 9/1/2017 1:34:19 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PRODUCT_CATEGORY_NODE]'
GO
-- ==============================  NAME  ======================================
-- VW_PRODUCT_CATEGORY_NODE
-- ===========================  DESCRIPTION  ==================================
-- Returns product category node info
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- KNIGHTGA     20170621    Created
-- poolda       20170621    Add product type code
-- poolda       20170627    Add 'in use' flag
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_PRODUCT_CATEGORY_NODE] as
SELECT 
    pc.PRODUCT_CATEGORY_ID as NODE_ID
    ,pc.OWNER_ID as PARENT_NODE_ID
    ,pc.PRODUCT_CATEGORY_CODE
    ,pc.PRODUCT_CATEGORY_NAME
    ,pc.PRODUCT_CATEGORY_DESC
    ,null as PRODUCT_ID
    ,null as PRODUCT_TYPE_CODE
    ,null as PRODUCT_NAME
    ,null as PRODUCT_DESC
    ,null as AMOUNT
    ,null as ASSIST_AMOUNT
    ,null as AUDIT_REQUIRED
    ,null as COUNTRY_CODE
    ,null as SKU
    ,null as FROM_DATE
    ,null as THRU_DATE
    ,'Y' as IS_ACTIVE
    ,case when exists (select 1 from component.PRODUCT_CATEGORY pc2 where pc2.owner_id = pc.PRODUCT_CATEGORY_ID) then 'Y' else 'N' end as HAS_CHILDREN
    ,'N' as IN_USE
FROM
    COMPONENT.PRODUCT_CATEGORY pc
WHERE not exists (select 1 from COMPONENT.PRODUCT p where p.PRODUCT_CATEGORY_CODE = pc.PRODUCT_CATEGORY_CODE)
UNION
SELECT 
    pc.PRODUCT_CATEGORY_ID as NODE_ID
    ,pc.OWNER_ID as PARENT_NODE_ID
    ,pc.PRODUCT_CATEGORY_CODE
    ,pc.PRODUCT_CATEGORY_NAME
    ,pc.PRODUCT_CATEGORY_DESC
    ,p.PRODUCT_ID
    ,p.PRODUCT_TYPE_CODE
    ,p.PRODUCT_NAME
    ,p.PRODUCT_DESC
    ,p.AMOUNT
    ,p.ASSIST_AMOUNT
    ,p.AUDIT_REQUIRED
    ,p.COUNTRY_CODE
    ,p.SKU
    ,p.FROM_DATE
    ,p.THRU_DATE
    ,CASE
        WHEN GETDATE() BETWEEN COALESCE(FROM_DATE, GETDATE()) and COALESCE(THRU_DATE, GETDATE()) THEN 'Y'
        ELSE 'N'
    END AS IS_ACTIVE
    ,case when exists (select 1 from component.PRODUCT_CATEGORY pc2 where pc2.owner_id = pc.PRODUCT_CATEGORY_ID) then 'Y' else 'N' end as HAS_CHILDREN
    ,case when exists (select 1 from COMPONENT.PRODUCT p where p.PRODUCT_CATEGORY_CODE = pc.PRODUCT_CATEGORY_CODE) then 'Y' else 'N' end as IN_USE
FROM 
    COMPONENT.PRODUCT p
    inner join COMPONENT.PRODUCT_CATEGORY pc on pc.PRODUCT_CATEGORY_CODE = p.PRODUCT_CATEGORY_CODE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PRODUCT_CATEGORY_TREE_BELOW]'
GO
-- ==============================  NAME  ======================================
-- VW_PRODUCT_CATEGORY_TREE_BELOW
-- ===========================  DESCRIPTION  ==================================
-- Returns product category info below a starting point
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA        20170621    Created
-- poolda       20170621    Add product type code
-- poolda       20170622    Reverse Tree Above and Tree Below, they were backwards
-- poolda       20170627    Add 'in use' flag
-- ===========================  DECLARATIONS  =================================
CREATE view [component].[VW_PRODUCT_CATEGORY_TREE_BELOW] as

WITH TREE (
    [LEVEL]
,    NODE_ID
,    PARENT_NODE_ID
    ,PRODUCT_CATEGORY_CODE
    ,PRODUCT_CATEGORY_NAME
    ,PRODUCT_CATEGORY_DESC
    ,PRODUCT_ID
    ,PRODUCT_TYPE_CODE
    ,PRODUCT_NAME
    ,PRODUCT_DESC
    ,AMOUNT
    ,ASSIST_AMOUNT
    ,AUDIT_REQUIRED
    ,COUNTRY_CODE
    ,SKU
    ,FROM_DATE
    ,THRU_DATE
    ,IS_ACTIVE
    ,HAS_CHILDREN
    ,IN_USE
,    STARTING_NODE_ID
,    STARTING_PARENT_NODE_ID
    ,STARTING_PRODUCT_CATEGORY_CODE
    ,STARTING_PRODUCT_CATEGORY_NAME
    ,STARTING_PRODUCT_CATEGORY_DESC
    ,STARTING_PRODUCT_ID
    ,STARTING_PRODUCT_TYPE_CODE
    ,STARTING_PRODUCT_NAME
    ,STARTING_PRODUCT_DESC
    ,STARTING_AMOUNT
    ,STARTING_ASSIST_AMOUNT
    ,STARTING_AUDIT_REQUIRED
    ,STARTING_COUNTRY_CODE
    ,STARTING_SKU
    ,STARTING_FROM_DATE
    ,STARTING_THRU_DATE
    ,STARTING_IS_ACTIVE
    ,STARTING_HAS_CHILDREN
    ,STARTING_IN_USE
) AS (
SELECT 0 AS [LEVEL]
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
FROM
    component.vw_product_category_node hn
UNION ALL
SELECT HT.[LEVEL] + 1
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
,    HT.STARTING_NODE_ID
,    HT.STARTING_PARENT_NODE_ID
    ,HT.STARTING_PRODUCT_CATEGORY_CODE
    ,HT.STARTING_PRODUCT_CATEGORY_NAME
    ,HT.STARTING_PRODUCT_CATEGORY_DESC
    ,HT.STARTING_PRODUCT_ID
    ,HT.STARTING_PRODUCT_TYPE_CODE
    ,HT.STARTING_PRODUCT_NAME
    ,HT.STARTING_PRODUCT_DESC
    ,HT.STARTING_AMOUNT
    ,HT.STARTING_ASSIST_AMOUNT
    ,HT.STARTING_AUDIT_REQUIRED
    ,HT.STARTING_COUNTRY_CODE
    ,HT.STARTING_SKU
    ,HT.STARTING_FROM_DATE
    ,HT.STARTING_THRU_DATE
    ,HT.STARTING_IS_ACTIVE
    ,HT.STARTING_HAS_CHILDREN
    ,HT.STARTING_IN_USE
FROM
    TREE HT
INNER JOIN component.vw_product_category_node hn
--    ON HN.PARENT_NODE_ID = HT.NODE_ID
    ON HN.NODE_ID = HT.PARENT_NODE_ID
)

SELECT
    [LEVEL]
,    NODE_ID
,    PARENT_NODE_ID
    ,PRODUCT_CATEGORY_CODE
    ,PRODUCT_CATEGORY_NAME
    ,PRODUCT_CATEGORY_DESC
    ,PRODUCT_ID
    ,PRODUCT_TYPE_CODE
    ,PRODUCT_NAME
    ,PRODUCT_DESC
    ,AMOUNT
    ,ASSIST_AMOUNT
    ,AUDIT_REQUIRED
    ,COUNTRY_CODE
    ,SKU
    ,FROM_DATE
    ,THRU_DATE
    ,IS_ACTIVE
    ,HAS_CHILDREN
    ,IN_USE
,    STARTING_NODE_ID
,    STARTING_PARENT_NODE_ID
    ,STARTING_PRODUCT_CATEGORY_CODE
    ,STARTING_PRODUCT_CATEGORY_NAME
    ,STARTING_PRODUCT_CATEGORY_DESC
    ,STARTING_PRODUCT_ID
    ,STARTING_PRODUCT_TYPE_CODE
    ,STARTING_PRODUCT_NAME
    ,STARTING_PRODUCT_DESC
    ,STARTING_AMOUNT
    ,STARTING_ASSIST_AMOUNT
    ,STARTING_AUDIT_REQUIRED
    ,STARTING_COUNTRY_CODE
    ,STARTING_SKU
    ,STARTING_FROM_DATE
    ,STARTING_THRU_DATE
    ,STARTING_IS_ACTIVE
    ,STARTING_HAS_CHILDREN
    ,STARTING_IN_USE
FROM
    TREE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_DATABUS_ENROLLMENT_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_DATABUS_ENROLLMENT_REPORT
-- ===========================  DESCRIPTION  ==================================
-- Returns pax enrollment information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- poolda       20170721    Created
-- poolda       20170725    Change paxId to controlNum
-- poolda       20170731    Left justify hierarchical columns
-- ===========================  DECLARATIONS  =================================
CREATE PROCEDURE [component].[UP_DATABUS_ENROLLMENT_REPORT] (
    @pageNumber     INT = 1,
    @recordsPerPage INT = -1,
    @sortBy         NVARCHAR(50) = 'X', -- Not used
    @sortDir        NVARCHAR(1) = 'X', -- Not used
       @paxGroupId     BIGINT,
    @activePaxOnly  NVARCHAR(1)

) AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @recordCount INT,
    @pageCount INT,
    @startId INT,
    @endId INT

    CREATE TABLE #tempTable (
        tempTableId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        pax_group_id   bigint,
        owner_group_id bigint,
        pax_id         bigint,
        control_num    nvarchar(255),
        first_name     nvarchar(100),
        last_name      nvarchar(100),
        role_code      nvarchar(50),
        status         nvarchar(50),
        email          nvarchar(255),
        terms_date     datetime2(7),
        corp_name      nvarchar(255),
        region_name    nvarchar(255),
        zone_name      nvarchar(255),
        district_name  nvarchar(255),
        dealer_name    nvarchar(255)
    )

    INSERT into #tempTable (
        pax_group_id,
        owner_group_id,
        pax_id,
        control_num,
        role_code,
        status
    ) SELECT node_id, parent_node_id, pax_id, pax_control_num, role_code, is_active
        FROM component.VW_PAX_HIERARCHY_TREE_BELOW
        WHERE starting_node_id = @paxGroupId
        AND (
            @activePaxOnly = 'Y' AND (THRU_DATE is null OR THRU_DATE > GETDATE())
            OR
            (@activePaxOnly = 'N')
        )

    SELECT @recordCount = @@ROWCOUNT    

    UPDATE #tempTable
    SET first_name = px.FIRST_NAME,
        last_name = px.LAST_NAME,
        email = e.EMAIL_ADDRESS,
        terms_date = pe.TERMS_AND_CONDITIONS_DATE
    FROM component.PAX px
        left outer join component.EMAIL e on e.pax_id = px.pax_id AND e.email_type_code = 'business'
        left outer join component.PROGRAM_ENROLLMENT pe on pe.pax_id = px.pax_id
    WHERE px.pax_id = #tempTable.pax_id;

    UPDATE #tempTable
    SET dealer_name = dealer_pax.COMPANY_NAME,
        district_name = district_pax.COMPANY_NAME,
        zone_name = zone_pax.COMPANY_NAME,
        region_name = region_pax.COMPANY_NAME,
        corp_name = corp_pax.COMPANY_NAME
    FROM #tempTable
        left outer JOIN component.PAX_GROUP dealer_pax_group ON dealer_pax_group.PAX_GROUP_ID = #tempTable.OWNER_GROUP_ID
        left outer JOIN component.PAX dealer_pax ON dealer_pax.PAX_ID = dealer_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP district_pax_group ON district_pax_group.PAX_GROUP_ID = dealer_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX district_pax ON district_pax.PAX_ID = district_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP zone_pax_group ON zone_pax_group.PAX_GROUP_ID = district_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX zone_pax ON zone_pax.PAX_ID = zone_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP region_pax_group ON region_pax_group.PAX_GROUP_ID = zone_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX region_pax ON region_pax.PAX_ID = region_pax_group.PAX_ID
        left outer join component.PAX_GROUP corp_pax_group ON corp_pax_group.PAX_GROUP_ID = region_pax_group.OWNER_GROUP_ID
        left outer join component.PAX corp_pax ON corp_pax.PAX_ID = corp_pax_group.PAX_ID


    -- Left justify hierarchy column data
    DECLARE
    @counter     INT = 1,
    @max         INT = 0,
    @minColumnId INT = 0
    SELECT @max = COUNT(1) FROM #tempTable

    CREATE TABLE #tempHierarchyRow (
        columnId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        value       nvarchar(255)
    )

    WHILE @counter <= @max
    BEGIN
        INSERT INTO #tempHierarchyRow
            SELECT corp_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT region_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT zone_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT district_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT dealer_name FROM #tempTable WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow WHERE value is null;

        SELECT @minColumnId = min(columnId) FROM #tempHierarchyRow
        UPDATE #tempTable SET corp_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId)  WHERE tempTableId = @counter
        UPDATE #tempTable SET region_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 1)  WHERE tempTableId = @counter
        UPDATE #tempTable SET zone_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 2)  WHERE tempTableId = @counter
        UPDATE #tempTable SET district_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 3)  WHERE tempTableId = @counter
        UPDATE #tempTable SET dealer_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 4)  WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow

        SET @counter = @counter + 1
    END
    DROP TABLE #tempHierarchyRow
    -- End of left justify hierarchy column data


    IF @recordsPerPage = -1
    BEGIN
        SET @recordsPerPage = @recordCount
        SET @pageNumber = 1
    END

    IF @recordsPerPage = 0
    BEGIN
        SET @recordsPerPage = 1
    END

    --Make sure the requested page number is within the valid range of pages (if not, then fix it)
    SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
    IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
    IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

    SET @endId = @pageNumber * @recordsPerPage
    SET @startId = (@endId - @recordsPerPage) + 1

    SELECT 
        control_num   AS 'CONTROL_NUM',
        first_name    AS 'FIRST_NAME',
        last_name     AS 'LAST_NAME',
        role_code     AS 'ROLE_CODE',
        email         AS 'EMAIL_ADDRESS',
        corp_name     AS 'HIERARCHY_L1',
        region_name   AS 'HIERARCHY_L2',
        zone_name     AS 'HIERARCHY_L3',
        district_name AS 'HIERARCHY_L4',
        dealer_name   AS 'HIERARCHY_L5',
        status        AS 'STATUS',
        terms_date    AS 'TERMS_AND_CONDITIONS_DATE',
        @recordCount    AS RECORD_COUNT,
        @recordsPerPage AS RECORDS_PER_PAGE,
        @pageNumber     AS PAGE_NUMBER,
        @pageCount      AS PAGE_COUNT,
        @startId        AS START_ID,
        @endId          AS END_ID
    FROM #tempTable
    ORDER BY region_name, zone_name, district_name, dealer_name, LAST_NAME, FIRST_NAME

    DROP TABLE #tempTable

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[SALE_ITEM_HEADER]'
GO
CREATE TABLE [component].[SALE_ITEM_HEADER]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SALE_ID] [bigint] NOT NULL,
[PRODUCT_ID] [bigint] NULL,
[SERIAL_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SALE_PRICE] [decimal] (10, 2) NULL,
[SALE_TOTAL] [decimal] (10, 2) NULL,
[QUANTITY] [decimal] (10, 2) NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SALE_ITEM_HEADER] on [component].[SALE_ITEM_HEADER]'
GO
ALTER TABLE [component].[SALE_ITEM_HEADER] ADD CONSTRAINT [PK_SALE_ITEM_HEADER] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_ITEM_HEADER_2] on [component].[SALE_ITEM_HEADER]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_ITEM_HEADER_2] ON [component].[SALE_ITEM_HEADER] ([PRODUCT_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_ITEM_HEADER_1] on [component].[SALE_ITEM_HEADER]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_ITEM_HEADER_1] ON [component].[SALE_ITEM_HEADER] ([SALE_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_DATABUS_EARNINGS_REPORT]'
GO
-- ==============================  NAME  ======================================
-- UP_DATABUS_EARNINGS_REPORT
-- ===========================  DESCRIPTION  ==================================
-- Returns pax earnings information
--
-- ============================  REVISIONS  ===================================
-- AUTHOR       DATE        CHANGE DESCRIPTION
-- poolda       20170803    Created
-- ===========================  DECLARATIONS  =================================
CREATE PROCEDURE [component].[UP_DATABUS_EARNINGS_REPORT] (
    @pageNumber      INT = 1,
    @recordsPerPage  INT = -1,
       @paxGroupId      BIGINT,
    @programId       BIGINT = null,
    @startPayoutDate DATETIME2(7) = null,
    @endPayoutDate   DATETIME2(7) = null

) AS
BEGIN
    SET NOCOUNT ON;

    DECLARE @recordCount INT,
    @pageCount INT,
    @startId INT,
    @endId INT

    CREATE TABLE #tempTable (
        tempTableId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        pax_group_id   BIGINT,
        owner_group_id BIGINT,
        pax_id         BIGINT,
        control_num    NVARCHAR(255),
        first_name     NVARCHAR(100),
        last_name      NVARCHAR(100),
        role_code      NVARCHAR(50),
        corp_name      NVARCHAR(255),
        region_name    NVARCHAR(255),
        zone_name      NVARCHAR(255),
        district_name  NVARCHAR(255),
        dealer_name    NVARCHAR(255),
        promotion_name NVARCHAR(100),
        promotion_id   BIGINT,
        quantity       INT,
        revenue        DECIMAL(10,2),
        payout_type    NVARCHAR(50),
        payout         DECIMAL(10,2),
        payout_date    DATE
    )

    INSERT into #tempTable (
        pax_group_id,
        owner_group_id,
        pax_id,
        control_num,
        first_name,
        last_name,
        role_code,
        promotion_name,
        promotion_id,
        quantity,
        revenue,
        payout_type,
        payout,
        payout_date
    ) SELECT
            pg.PAX_GROUP_ID
            ,pg.OWNER_GROUP_ID
            ,pax.PAX_ID
            ,pax.CONTROL_NUM
            ,pax.FIRST_NAME
            ,pax.LAST_NAME
            ,pg.ROLE_CODE
            ,pro.PROGRAM_NAME
            ,pro.PROGRAM_ID
            ,ISNULL(sih.QUANTITY, 1) as 'QUANTITY'
            ,sih.SALE_TOTAL
            ,pay.PAYOUT_TYPE_CODE
            ,pay.PAYOUT_AMOUNT
            ,pay.PAYOUT_DATE
        FROM component.PAX_GROUP pg 
            JOIN component.PAX pax ON pax.PAX_ID = pg.PAX_ID
            JOIN component.TRANSACTION_HEADER th on th.PAX_GROUP_ID = pg.PAX_GROUP_ID
            JOIN component.PROGRAM pro on pro.PROGRAM_ID = th.PROGRAM_ID
            left outer JOIN component.SALE sa on sa.TRANSACTION_HEADER_ID = th.ID
            left outer JOIN component.SALE_ITEM_HEADER sih on sih.SALE_ID = sa.ID
            JOIN component.TRANSACTION_HEADER_EARNINGS the on the.TRANSACTION_HEADER_ID = th.ID
            JOIN component.EARNINGS e on e.EARNINGS_ID = the.EARNINGS_ID
            JOIN component.EARNINGS_PAYOUT ep on ep.EARNINGS_ID = e.EARNINGS_ID
            JOIN component.PAYOUT pay on pay.PAYOUT_ID = ep.PAYOUT_ID
        WHERE pg.owner_group_id = @paxGroupId and pg.thru_date is null
        AND (
                (@startPayoutDate is null OR @endPayoutDate is null)
            OR
                (pay.PAYOUT_DATE BETWEEN @startPayoutDate AND @endPayoutDate)
            )
        AND (
                (@programId is null)
            OR
                (@programId = pro.PROGRAM_ID)
            )


    UPDATE #tempTable
    SET dealer_name = dealer_pax.COMPANY_NAME,
        district_name = district_pax.COMPANY_NAME,
        zone_name = zone_pax.COMPANY_NAME,
        region_name = region_pax.COMPANY_NAME,
        corp_name = corp_pax.COMPANY_NAME
    FROM #tempTable
        left outer JOIN component.PAX_GROUP dealer_pax_group ON dealer_pax_group.PAX_GROUP_ID = #tempTable.OWNER_GROUP_ID
        left outer JOIN component.PAX dealer_pax ON dealer_pax.PAX_ID = dealer_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP district_pax_group ON district_pax_group.PAX_GROUP_ID = dealer_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX district_pax ON district_pax.PAX_ID = district_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP zone_pax_group ON zone_pax_group.PAX_GROUP_ID = district_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX zone_pax ON zone_pax.PAX_ID = zone_pax_group.PAX_ID
        left outer JOIN component.PAX_GROUP region_pax_group ON region_pax_group.PAX_GROUP_ID = zone_pax_group.OWNER_GROUP_ID
        left outer JOIN component.PAX region_pax ON region_pax.PAX_ID = region_pax_group.PAX_ID
        left outer join component.PAX_GROUP corp_pax_group ON corp_pax_group.PAX_GROUP_ID = region_pax_group.OWNER_GROUP_ID
        left outer join component.PAX corp_pax ON corp_pax.PAX_ID = corp_pax_group.PAX_ID


    -- Left justify hierarchy column data
    DECLARE
    @counter     INT = 1,
    @max         INT = 0,
    @minColumnId INT = 0
    SELECT @max = COUNT(1) FROM #tempTable

    CREATE TABLE #tempHierarchyRow (
        columnId    BIGINT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        value       nvarchar(255)
    )

    WHILE @counter <= @max
    BEGIN
        INSERT INTO #tempHierarchyRow
            SELECT corp_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT region_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT zone_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT district_name FROM #tempTable WHERE tempTableId = @counter
        INSERT INTO #tempHierarchyRow
            SELECT dealer_name FROM #tempTable WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow WHERE value is null;

        SELECT @minColumnId = min(columnId) FROM #tempHierarchyRow
        UPDATE #tempTable SET corp_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId)  WHERE tempTableId = @counter
        UPDATE #tempTable SET region_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 1)  WHERE tempTableId = @counter
        UPDATE #tempTable SET zone_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 2)  WHERE tempTableId = @counter
        UPDATE #tempTable SET district_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 3)  WHERE tempTableId = @counter
        UPDATE #tempTable SET dealer_name = 
            (SELECT value FROM #tempHierarchyRow WHERE columnId = @minColumnId + 4)  WHERE tempTableId = @counter

        DELETE FROM #tempHierarchyRow

        SET @counter = @counter + 1
    END
    DROP TABLE #tempHierarchyRow
    -- End of left justify hierarchy column data

    CREATE TABLE #reportTable (
        control_num    NVARCHAR(255),
        first_name     NVARCHAR(100),
        last_name      NVARCHAR(100),
        role_code      NVARCHAR(50),
        corp_name      NVARCHAR(255),
        region_name    NVARCHAR(255),
        zone_name      NVARCHAR(255),
        district_name  NVARCHAR(255),
        dealer_name    NVARCHAR(255),
        promotion_name NVARCHAR(100),
        promotion_id   BIGINT,
        quantity       INT,
        revenue        DECIMAL(10,2),
        payout_type    NVARCHAR(50),
        payout         DECIMAL(10,2),
        payout_date    DATE
    )

    INSERT into #reportTable (
        control_num,
        first_name,
        last_name,
        role_code,
        corp_name,
        region_name,
        zone_name,
        district_name,
        dealer_name,
        promotion_name,
        promotion_id,
        quantity,
        revenue,
        payout_type,
        payout,
        payout_date
    )
    SELECT
        control_num,
        first_name,
        last_name,
        role_code,
        corp_name,
        region_name,
        zone_name,
        district_name,
        dealer_name,
        promotion_name,
        promotion_id,
        count(quantity),
        sum(revenue),
        payout_type,
        sum(payout),
        payout_date
    FROM #tempTable
    WHERE payout > 0
    GROUP BY control_num, promotion_id, payout_date, first_name, last_name, role_code,
        corp_name, region_name, zone_name, district_name, dealer_name,
        promotion_name, payout_type
    ORDER BY region_name, zone_name, district_name, dealer_name, last_name, first_name, promotion_name, payout_date

    SELECT @recordCount = @@ROWCOUNT

    DROP TABLE #tempTable

    IF @recordsPerPage = -1
    BEGIN
        SET @recordsPerPage = @recordCount
        SET @pageNumber = 1
    END

    IF @recordsPerPage = 0
    BEGIN
        SET @recordsPerPage = 1
    END

    --Make sure the requested page number is within the valid range of pages (if not, then fix it)
    SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
    IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
    IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

    SET @endId = @pageNumber * @recordsPerPage
    SET @startId = (@endId - @recordsPerPage) + 1

    -- Adjust dates, i.e. push start date back to beginning of day and end date out to end of day
    set @startPayoutDate = convert(date, @startPayoutDate)
    set @endPayoutDate = convert(date, dateadd(day, 1, @endPayoutDate))

    SELECT 
        control_num     AS 'CONTROL_NUM',
        first_name      AS 'FIRST_NAME',
        last_name       AS 'LAST_NAME',
        role_code       AS 'ROLE_CODE',
        corp_name       AS 'HIERARCHY_L1',
        region_name     AS 'HIERARCHY_L2',
        zone_name       AS 'HIERARCHY_L3',
        district_name   AS 'HIERARCHY_L4',
        dealer_name     AS 'HIERARCHY_L5',
        promotion_name  AS 'PROMOTION_NAME',
        promotion_id    AS 'PROMOTION_ID',
        quantity        AS 'QUANTITY',
        revenue         AS 'REVENUE',
        payout_type     AS 'PAYOUT_TYPE',
        payout          AS 'PAYOUT',
        payout_date     AS 'PAYOUT_DATE',
        @recordCount    AS 'RECORD_COUNT',
        @recordsPerPage AS 'RECORDS_PER_PAGE',
        @pageNumber     AS 'PAGE_NUMBER',
        @pageCount      AS 'PAGE_COUNT',
        @startId        AS 'START_ID',
        @endId          AS 'END_ID'
    FROM #ReportTable
    
    DROP TABLE #reportTable

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_ASSOCIATION_FETCH_PERMISSIONS]'
GO


-- ==============================  NAME  ======================================
-- UP_ASSOCIATION_FETCH_PERMISSIONS
-- ===========================  DESCRIPTION  ==================================
-- GIVEN A SYS_USER_ID
-- RETURN ALL ASSOCIATED PERMISSION_CODE(S)
-- 
-- ASSOCIATION_TYPE_ID / ASSOCIATION_TYPE_DESC
--  900 / PAX_PERMISSION
-- 1100 / GROUPS_PERMISSION (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
-- 2200 / SYS_USER_PERMISSION
-- 2300 / ROLE_PERMISSION
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- DOHOGNTA        20080530    CREATED
-- DOHOGNTA        20150119    USE STANDALONE JUNCTION TABLE
-- DOHOGNTA        20150309    CHANGE IDENTITIES TO BIGINT
-- KNIGHTGA     20150408    Updated to use UDM TABLES
--
-- ===========================  DECLARATIONS  =================================

CREATE PROCEDURE [component].[UP_ASSOCIATION_FETCH_PERMISSIONS]
    @sys_user_id BIGINT
AS
BEGIN

SET NOCOUNT ON

/*
select * from component.sys_user

exec component.[UP_ASSOCIATION_FETCH_PERMISSIONS] 1
exec component.[UP_ASSOCIATION_FETCH_PERMISSIONS] 351

declare @sys_user_id BIGINT
set @sys_user_id = 1
*/


DECLARE    @result_set TABLE ([id] VARCHAR(50))

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

/*
--DECLARE    @fk TABLE ([id] BIGINT)

-- FETCH ID(S)
INSERT INTO @fk ([id]) SELECT PAX_ID FROM SYS_USER WHERE SYS_USER_ID = @sys_user_id

-- PAX_PERMISSION
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    PAX_PERMISSION
,    PERMISSION
,    @fk AS driver
WHERE    
    PAX_PERMISSION.PERMISSION_ID = PERMISSION.PERMISSION_ID
AND    PAX_PERMISSION.PAX_ID = driver.[id]
*/

DECLARE    @pax_id BIGINT
SELECT    @pax_id = PAX_ID FROM SYS_USER WHERE SYS_USER_ID = @sys_user_id

-- ROLE_PAX
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    ROLE_PAX
    inner join ROLE_PERMISSION on ROLE_PERMISSION.ROLE_ID = ROLE_PAX.ROLE_ID
    inner join PERMISSION on PERMISSION.PERMISSION_ID = ROLE_PERMISSION.PERMISSION_ID
WHERE ROLE_PAX.PAX_ID = @pax_id

/*
-- PAX_GROUPS (GIVE CONSIDERATION TO GROUPS WITHIN GROUPS)
DECLARE    @pax_id BIGINT
SELECT    @pax_id = PAX_ID FROM SYS_USER WHERE SYS_USER_ID = @sys_user_id

DELETE @fk

-- GET LIST OF ANCESTOR GROUP(S); INCLUDING THE GIVEN GROUP
INSERT @fk SELECT [id] FROM UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, -1)

-- PAX_GROUPS > GROUPS_PERMISSION
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    GROUPS_PERMISSION
,    PERMISSION
,    @fk AS driver
WHERE    
    GROUPS_PERMISSION.PERMISSION_ID = PERMISSION.PERMISSION_ID
AND    GROUPS_PERMISSION.GROUP_ID = driver.[id]

DELETE @fk
*/

-- GROUPS_PAX
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    UF_ASSOCIATION_FETCH_GROUP_BY_PAX (@pax_id, -1) as GROUPS
    inner join ROLE_GROUPS on ROLE_GROUPS.GROUP_ID = GROUPS.id
    inner join ROLE_PERMISSION on ROLE_PERMISSION.ROLE_ID = ROLE_GROUPS.ROLE_ID
    inner join PERMISSION on PERMISSION.PERMISSION_ID = ROLE_PERMISSION.PERMISSION_ID

/*
INSERT INTO @fk ([id]) SELECT @sys_user_id

-- SYS_USER_PERMISSION (2200)
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    ASSOCIATION
,    PERMISSION
,    @fk AS driver
WHERE    
    ASSOCIATION.FK2 = PERMISSION.PERMISSION_ID
AND    ASSOCIATION.ASSOCIATION_TYPE_ID = 2200
AND    ASSOCIATION.FK1 = driver.[id]

DELETE @fk
*/

/*
-- GET LIST OF ROLE_ID FOR ALL ACTIVE PAX_GROUP ASSOCIATED WITH GIVEN SYS_USER_ID
INSERT INTO @fk ([id]) SELECT ROLE.ROLE_ID FROM SYS_USER, PAX_GROUP, ROLE WHERE SYS_USER.PAX_ID = PAX_GROUP.PAX_ID AND PAX_GROUP.ROLE_CODE = ROLE.ROLE_CODE AND GETDATE() BETWEEN PAX_GROUP.FROM_DATE AND ISNULL(THRU_DATE, GETDATE()) AND SYS_USER.SYS_USER_ID = @sys_user_id

-- ROLE_PERMISSION (2300)
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    ASSOCIATION
,    PERMISSION
,    @fk AS driver
WHERE    
    ASSOCIATION.FK2 = PERMISSION.PERMISSION_ID
AND    ASSOCIATION.ASSOCIATION_TYPE_ID = 2300
AND    ASSOCIATION.FK1 = driver.[id]
*/

-- PAX_GROUP
INSERT INTO @result_set ([id])
SELECT
    PERMISSION.PERMISSION_CODE
FROM
    PAX_GROUP
    inner join ROLE on ROLE.ROLE_CODE = PAX_GROUP.ROLE_CODE
    inner join ROLE_PERMISSION on ROLE_PERMISSION.ROLE_ID = ROLE.ROLE_ID
    inner join PERMISSION on PERMISSION.PERMISSION_ID = ROLE_PERMISSION.PERMISSION_ID
WHERE PAX_GROUP.PAX_ID = @pax_id
  AND getdate() between PAX_GROUP.FROM_DATE and isnull(PAX_GROUP.THRU_DATE, getdate())


-- RETURN DISTINCT LIST OF PERMISSION_CODE(S)
SELECT DISTINCT [id] AS PERMISSION_CODE FROM @result_set ORDER BY 1

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_FEED_WITH_LIKES]'
GO
-- ==============================  NAME  ======================================
-- VW_FEED_WITH_LIKES
-- ===========================  DESCRIPTION  ==================================
-- Returns achievement feed data with linkable data to the LIKES table
--
-- ============================  REVISIONS  ===================================
-- AUTHOR            DATE            CHANGE DESCRIPTION
-- holtzhdm     20170510    Created
--

CREATE VIEW [component].[VW_FEED_WITH_LIKES] (FEED_ITEM_ID, FEED_ITEM_DATE, FEED_ITEM_TYPE_CODE, FEED_ITEM_MESSAGE,
        LIKE_COUNT, PAX_ID) AS

        select
                fi.ID
                ,fi.FEED_ITEM_DATE
                ,fi.FEED_ITEM_TYPE_CODE
                ,fi.FEED_MESSAGE
                ,COUNT(li.target_id) as LIKE_COUNT
                ,p.PAX_ID
        from component.feed_item fi
        join component.PAX p
        on fi.SUBJECT_ID = p.PAX_ID
        left join component.likes li
        on fi.id = li.TARGET_ID
        and li.TARGET_TABLE = 'FEED_ITEM'
        group by
           li.TARGET_ID, fi.id, fi.feed_item_date, fi.feed_item_type_code, fi.feed_message, p.PAX_ID;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[UP_DATABUS_ACHIEVEMENT_FEED]'
GO
CREATE PROCEDURE [component].[UP_DATABUS_ACHIEVEMENT_FEED](
  @pageNumber        INT = 1,
  @recordsPerPage    INT = -1,
  @achievementTypeCodes NVARCHAR(MAX),
  @paxId             BIGINT

) AS BEGIN
  SET NOCOUNT ON

  CREATE TABLE #tempTable (
    [rowNumber]           INT,
    [feed_item_id]        BIGINT,
    [feed_item_date]      DATETIME2(7),
    [feed_item_type_code] NVARCHAR(50),
    [feed_message]        NVARCHAR(MAX),
    [like_count]          BIGINT,
    [pax_id]              BIGINT
  )

  INSERT INTO #tempTable

    SELECT ROW_NUMBER() OVER (ORDER BY vw.feed_item_date DESC) rowNumber,
      vw.FEED_ITEM_ID,
      vw.FEED_ITEM_DATE,
      vw.FEED_ITEM_TYPE_CODE,
      vw.FEED_ITEM_MESSAGE,
      vw.like_count,
      vw.PAX_ID
    FROM component.VW_FEED_WITH_LIKES vw
    WHERE vw.PAX_ID = ISNULL(@paxId, vw.PAX_ID)
          AND (@achievementTypeCodes IS NULL
          OR vw.FEED_ITEM_TYPE_CODE IN
             (SELECT token FROM component.UF_PARSE_STRING_TO_STRING(@achievementTypeCodes, ',')))

  DECLARE @rowCount INT = @@rowcount
  DECLARE @startingRowNumber INT
  DECLARE @endingRowNumber INT

  IF ((@recordsPerPage IS NOT NULL) AND (@recordsPerPage > 0))
    BEGIN
      --Make sure the requested page number is within the valid range of pages (if not, then fix it)
      IF ((@pageNumber IS NOT NULL) AND (@pageNumber > 0))
        BEGIN
          DECLARE @pageCount INT
          SET @pageCount = (@rowCount + @recordsPerPage - 1) / @recordsPerPage
          IF (@pageNumber > @pageCount)
            SET @pageNumber = @pageCount
        END
      ELSE
        SET @pageNumber = 1

      SET @startingRowNumber = (@pageNumber - 1) * @recordsPerPage + 1
      SET @endingRowNumber = @startingRowNumber + @recordsPerPage - 1
    END
  ELSE BEGIN
    SET @pageNumber = 1
    SET @startingRowNumber = 1
    SET @endingRowNumber = @rowCount
  END


  SELECT *
  FROM #tempTable
  WHERE rowNumber BETWEEN @startingRowNumber AND @endingRowNumber
  ORDER BY rowNumber

  DROP TABLE #tempTable

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_SALE_ITEM_HEADER_U_1] on [component].[SALE_ITEM_HEADER]'
GO
-- ==============================  NAME  ======================================
-- TR_SALE_ITEM_HEADER_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20170621        Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_SALE_ITEM_HEADER_U_1]
ON [component].[SALE_ITEM_HEADER]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SALE_ITEM_HEADER T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SALE_ITEM_HEADER T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[SALE_ITEM_HEADER_MISC]'
GO
CREATE TABLE [component].[SALE_ITEM_HEADER_MISC]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SALE_ITEM_HEADER_ID] [bigint] NOT NULL,
[VF_NAME] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MISC_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MISC_DATE] [datetime2] NULL,
[MISC_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_MISC_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_MISC_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_MISC_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_ITEM_HEADER_MISC_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SALE_ITEM_HEADER_MISC] on [component].[SALE_ITEM_HEADER_MISC]'
GO
ALTER TABLE [component].[SALE_ITEM_HEADER_MISC] ADD CONSTRAINT [PK_SALE_ITEM_HEADER_MISC] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_ITEM_HEADER_MISC_2] on [component].[SALE_ITEM_HEADER_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_ITEM_HEADER_MISC_2] ON [component].[SALE_ITEM_HEADER_MISC] ([SALE_ITEM_HEADER_ID], [MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_ITEM_HEADER_MISC_1] on [component].[SALE_ITEM_HEADER_MISC]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_ITEM_HEADER_MISC_1] ON [component].[SALE_ITEM_HEADER_MISC] ([SALE_ITEM_HEADER_ID], [VF_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_SALE_ITEM_HEADER_MISC_U_1] on [component].[SALE_ITEM_HEADER_MISC]'
GO
-- ==============================  NAME  ======================================
-- TR_SALE_ITEM_HEADER_MISC_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20160621    Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_SALE_ITEM_HEADER_MISC_U_1]
ON [component].[SALE_ITEM_HEADER_MISC]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SALE_ITEM_HEADER_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SALE_ITEM_HEADER_MISC T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[SALE_INDIVIDUAL_TYPE]'
GO
CREATE TABLE [component].[SALE_INDIVIDUAL_TYPE]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SALE_INDIVIDUAL_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SALE_INDIVIDUAL_TYPE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SALE_INDIVIDUAL_TYPE_DESC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_SEQUENCE] [int] NULL,
[DISPLAY_STATUS_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_TYPE_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_TYPE_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_TYPE_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_TYPE_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SALE_INDIVIDUAL_TYPE] on [component].[SALE_INDIVIDUAL_TYPE]'
GO
ALTER TABLE [component].[SALE_INDIVIDUAL_TYPE] ADD CONSTRAINT [PK_SALE_INDIVIDUAL_TYPE] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_INDIVIDUAL_TYPE_1] on [component].[SALE_INDIVIDUAL_TYPE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SALE_INDIVIDUAL_TYPE_1] ON [component].[SALE_INDIVIDUAL_TYPE] ([SALE_INDIVIDUAL_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_SALE_INDIVIDUAL_TYPE_U_1] on [component].[SALE_INDIVIDUAL_TYPE]'
GO
-- ==============================  NAME  ======================================
-- TR_SALE_INDIVIDUAL_TYPE_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20160621    Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_SALE_INDIVIDUAL_TYPE_U_1]
ON [component].[SALE_INDIVIDUAL_TYPE]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SALE_INDIVIDUAL_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SALE_INDIVIDUAL_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[INDIVIDUAL]'
GO
CREATE TABLE [component].[INDIVIDUAL]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[FIRST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_INITIAL] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LANGUAGE_CODE] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS1] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZIP] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HOME_PHONE] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDITIONAL_PHONE] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL_ADDRESS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_INDIVIDUAL_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_INDIVIDUAL_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_INDIVIDUAL_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_INDIVIDUAL_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_INDIVIDUAL] on [component].[INDIVIDUAL]'
GO
ALTER TABLE [component].[INDIVIDUAL] ADD CONSTRAINT [PK_INDIVIDUAL] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_INDIVIDUAL_2] on [component].[INDIVIDUAL]'
GO
CREATE NONCLUSTERED INDEX [IX_INDIVIDUAL_2] ON [component].[INDIVIDUAL] ([LANGUAGE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_INDIVIDUAL_1] on [component].[INDIVIDUAL]'
GO
CREATE NONCLUSTERED INDEX [IX_INDIVIDUAL_1] ON [component].[INDIVIDUAL] ([LAST_NAME], [FIRST_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_INDIVIDUAL_U_1] on [component].[INDIVIDUAL]'
GO
-- ==============================  NAME  ======================================
-- TR_INDIVIDUAL_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20170622    Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_INDIVIDUAL_U_1]
ON [component].[INDIVIDUAL]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    INDIVIDUAL T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    INDIVIDUAL T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[SALE_INDIVIDUAL]'
GO
CREATE TABLE [component].[SALE_INDIVIDUAL]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SALE_INDIVIDUAL_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SALE_ID] [bigint] NOT NULL,
[INDIVIDUAL_ID] [bigint] NOT NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_SALE_INDIVIDUAL_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SALE_INDIVIDUAL] on [component].[SALE_INDIVIDUAL]'
GO
ALTER TABLE [component].[SALE_INDIVIDUAL] ADD CONSTRAINT [PK_SALE_INDIVIDUAL] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_INDIVIDUAL_2] on [component].[SALE_INDIVIDUAL]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_INDIVIDUAL_2] ON [component].[SALE_INDIVIDUAL] ([INDIVIDUAL_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_SALE_INDIVIDUAL_1] on [component].[SALE_INDIVIDUAL]'
GO
CREATE NONCLUSTERED INDEX [IX_SALE_INDIVIDUAL_1] ON [component].[SALE_INDIVIDUAL] ([SALE_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_SALE_INDIVIDUAL_U_1] on [component].[SALE_INDIVIDUAL]'
GO
-- ==============================  NAME  ======================================
-- TR_SALE_INDIVIDUAL_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20170622    Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_SALE_INDIVIDUAL_U_1]
ON [component].[SALE_INDIVIDUAL]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    SALE_INDIVIDUAL T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    SALE_INDIVIDUAL T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[RDR]'
GO
CREATE TABLE [component].[RDR]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SALE_ITEM_HEADER_ID] [bigint] NOT NULL,
[SALES_PERSON_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SALES_MANAGER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEALER_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RDR_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MAKE] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MODEL] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[COLOR] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOLD_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MODEL_YEAR] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MANUFACTURED_DATE] [datetime2] NULL,
[RETAIL_DATE] [datetime2] NULL,
[REVERSE_DATE] [datetime2] NULL,
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_RDR_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_RDR_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_RDR_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_RDR_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_RDR] on [component].[RDR]'
GO
ALTER TABLE [component].[RDR] ADD CONSTRAINT [PK_RDR] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RDR_1] on [component].[RDR]'
GO
CREATE NONCLUSTERED INDEX [IX_RDR_1] ON [component].[RDR] ([SALE_ITEM_HEADER_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_RDR_U_1] on [component].[RDR]'
GO
-- ==============================  NAME  ======================================
-- TR_RDR_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- chidris        20170622    Created
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_RDR_U_1]
ON [component].[RDR]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    RDR T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    RDR T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [component].[VW_PRODUCT_CATEGORY_TREE_ABOVE]'
GO
-- ==============================  NAME  ======================================
-- VW_PRODUCT_CATEGORY_TREE_ABOVE
-- ===========================  DESCRIPTION  ==================================
-- Returns product category info above a starting point
--
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- KNIGHTGA        20170621    Created
-- poolda       20170621    Add product type code
-- poolda       20170622    Reverse Tree Above and Tree Below, they were backwards
-- poolda       20170627    Add 'in use' flag
-- ===========================  DECLARATIONS  =================================
create view [component].[VW_PRODUCT_CATEGORY_TREE_ABOVE] as

WITH TREE (
    [LEVEL]
,    NODE_ID
,    PARENT_NODE_ID
    ,PRODUCT_CATEGORY_CODE
    ,PRODUCT_CATEGORY_NAME
    ,PRODUCT_CATEGORY_DESC
    ,PRODUCT_ID
    ,PRODUCT_TYPE_CODE
    ,PRODUCT_NAME
    ,PRODUCT_DESC
    ,AMOUNT
    ,ASSIST_AMOUNT
    ,AUDIT_REQUIRED
    ,COUNTRY_CODE
    ,SKU
    ,FROM_DATE
    ,THRU_DATE
    ,IS_ACTIVE
    ,HAS_CHILDREN
    ,IN_USE
,    STARTING_NODE_ID
,    STARTING_PARENT_NODE_ID
    ,STARTING_PRODUCT_CATEGORY_CODE
    ,STARTING_PRODUCT_CATEGORY_NAME
    ,STARTING_PRODUCT_CATEGORY_DESC
    ,STARTING_PRODUCT_ID
    ,STARTING_PRODUCT_TYPE_CODE
    ,STARTING_PRODUCT_NAME
    ,STARTING_PRODUCT_DESC
    ,STARTING_AMOUNT
    ,STARTING_ASSIST_AMOUNT
    ,STARTING_AUDIT_REQUIRED
    ,STARTING_COUNTRY_CODE
    ,STARTING_SKU
    ,STARTING_FROM_DATE
    ,STARTING_THRU_DATE
    ,STARTING_IS_ACTIVE
    ,STARTING_HAS_CHILDREN
    ,STARTING_IN_USE
) AS (
SELECT 0 AS [LEVEL]
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
FROM
    component.vw_product_category_node hn
UNION ALL
SELECT HT.[LEVEL] + 1
,    HN.NODE_ID
,    HN.PARENT_NODE_ID
    ,HN.PRODUCT_CATEGORY_CODE
    ,HN.PRODUCT_CATEGORY_NAME
    ,HN.PRODUCT_CATEGORY_DESC
    ,HN.PRODUCT_ID
    ,HN.PRODUCT_TYPE_CODE
    ,HN.PRODUCT_NAME
    ,HN.PRODUCT_DESC
    ,HN.AMOUNT
    ,HN.ASSIST_AMOUNT
    ,HN.AUDIT_REQUIRED
    ,HN.COUNTRY_CODE
    ,HN.SKU
    ,HN.FROM_DATE
    ,HN.THRU_DATE
    ,HN.IS_ACTIVE
    ,HN.HAS_CHILDREN
    ,HN.IN_USE
,    HT.STARTING_NODE_ID
,    HT.STARTING_PARENT_NODE_ID
    ,HT.STARTING_PRODUCT_CATEGORY_CODE
    ,HT.STARTING_PRODUCT_CATEGORY_NAME
    ,HT.STARTING_PRODUCT_CATEGORY_DESC
    ,HT.STARTING_PRODUCT_ID
    ,HT.STARTING_PRODUCT_TYPE_CODE
    ,HT.STARTING_PRODUCT_NAME
    ,HT.STARTING_PRODUCT_DESC
    ,HT.STARTING_AMOUNT
    ,HT.STARTING_ASSIST_AMOUNT
    ,HT.STARTING_AUDIT_REQUIRED
    ,HT.STARTING_COUNTRY_CODE
    ,HT.STARTING_SKU
    ,HT.STARTING_FROM_DATE
    ,HT.STARTING_THRU_DATE
    ,HT.STARTING_IS_ACTIVE
    ,HT.STARTING_HAS_CHILDREN
    ,HT.STARTING_IN_USE
FROM
    TREE HT
INNER JOIN component.vw_product_category_node hn
--    ON HN.NODE_ID = HT.PARENT_NODE_ID
    ON HN.PARENT_NODE_ID = HT.NODE_ID
)

SELECT
    [LEVEL]
,    NODE_ID
,    PARENT_NODE_ID
    ,PRODUCT_CATEGORY_CODE
    ,PRODUCT_CATEGORY_NAME
    ,PRODUCT_CATEGORY_DESC
    ,PRODUCT_ID
    ,PRODUCT_TYPE_CODE
    ,PRODUCT_NAME
    ,PRODUCT_DESC
    ,AMOUNT
    ,ASSIST_AMOUNT
    ,AUDIT_REQUIRED
    ,COUNTRY_CODE
    ,SKU
    ,FROM_DATE
    ,THRU_DATE
    ,IS_ACTIVE
    ,HAS_CHILDREN
    ,IN_USE
,    STARTING_NODE_ID
,    STARTING_PARENT_NODE_ID
    ,STARTING_PRODUCT_CATEGORY_CODE
    ,STARTING_PRODUCT_CATEGORY_NAME
    ,STARTING_PRODUCT_CATEGORY_DESC
    ,STARTING_PRODUCT_ID
    ,STARTING_PRODUCT_TYPE_CODE
    ,STARTING_PRODUCT_NAME
    ,STARTING_PRODUCT_DESC
    ,STARTING_AMOUNT
    ,STARTING_ASSIST_AMOUNT
    ,STARTING_AUDIT_REQUIRED
    ,STARTING_COUNTRY_CODE
    ,STARTING_SKU
    ,STARTING_FROM_DATE
    ,STARTING_THRU_DATE
    ,STARTING_IS_ACTIVE
    ,STARTING_HAS_CHILDREN
    ,STARTING_IN_USE
FROM
    TREE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[SALE_ITEM_HEADER_MISC]'
GO
ALTER TABLE [component].[SALE_ITEM_HEADER_MISC] ADD CONSTRAINT [CK_SALE_ITEM_HEADER_MISC] CHECK (([VF_NAME] IS NULL OR isnumeric(left([VF_NAME],(1)))=(0)))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[SALE_INDIVIDUAL]'
GO
ALTER TABLE [component].[SALE_INDIVIDUAL] ADD CONSTRAINT [FK_SALE_INDIVIDUAL_INDIVIDUAL] FOREIGN KEY ([INDIVIDUAL_ID]) REFERENCES [component].[INDIVIDUAL] ([ID])
GO
ALTER TABLE [component].[SALE_INDIVIDUAL] ADD CONSTRAINT [FK_SALE_INDIVIDUAL_SALE_INDIVIDUAL_TYPE] FOREIGN KEY ([SALE_INDIVIDUAL_TYPE_CODE]) REFERENCES [component].[SALE_INDIVIDUAL_TYPE] ([SALE_INDIVIDUAL_TYPE_CODE])
GO
ALTER TABLE [component].[SALE_INDIVIDUAL] ADD CONSTRAINT [FK_SALE_INDIVIDUAL_SALE] FOREIGN KEY ([SALE_ID]) REFERENCES [component].[SALE] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[INDIVIDUAL]'
GO
ALTER TABLE [component].[INDIVIDUAL] ADD CONSTRAINT [FK_INDIVIDUAL_LANGUAGE] FOREIGN KEY ([LANGUAGE_CODE]) REFERENCES [component].[LANGUAGE] ([LANGUAGE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[RDR]'
GO
ALTER TABLE [component].[RDR] ADD CONSTRAINT [FK_RDR_SALE_ITEM_HEADER] FOREIGN KEY ([SALE_ITEM_HEADER_ID]) REFERENCES [component].[SALE_ITEM_HEADER] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[SALE_INDIVIDUAL_TYPE]'
GO
ALTER TABLE [component].[SALE_INDIVIDUAL_TYPE] ADD CONSTRAINT [FK_SALE_INDIVIDUAL_TYPE_STATUS_TYPE] FOREIGN KEY ([DISPLAY_STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[SALE_ITEM_HEADER_MISC]'
GO
ALTER TABLE [component].[SALE_ITEM_HEADER_MISC] ADD CONSTRAINT [FK_SALE_ITEM_HEADER_MISC_SALE_ITEM_HEADER] FOREIGN KEY ([SALE_ITEM_HEADER_ID]) REFERENCES [component].[SALE_ITEM_HEADER] ([ID])
GO
ALTER TABLE [component].[SALE_ITEM_HEADER_MISC] ADD CONSTRAINT [FK_SALE_ITEM_HEADER_MISC_MISC_TYPE] FOREIGN KEY ([MISC_TYPE_CODE]) REFERENCES [component].[MISC_TYPE] ([MISC_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[SALE_ITEM_HEADER]'
GO
ALTER TABLE [component].[SALE_ITEM_HEADER] ADD CONSTRAINT [FK_SALE_ITEM_HEADER_SALE] FOREIGN KEY ([SALE_ID]) REFERENCES [component].[SALE] ([ID])
GO
ALTER TABLE [component].[SALE_ITEM_HEADER] ADD CONSTRAINT [FK_SALE_ITEM_HEADER_PRODUCT] FOREIGN KEY ([PRODUCT_ID]) REFERENCES [component].[PRODUCT] ([PRODUCT_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO
