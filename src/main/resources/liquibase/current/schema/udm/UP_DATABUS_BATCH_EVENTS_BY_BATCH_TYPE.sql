SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [component].[UP_DATABUS_BATCH_EVENTS_BY_BATCH_TYPE] (
    @pageNumber           INT = 1,
    @recordsPerPage       INT = -1,
    @batchType            NVARCHAR(50) = 'TRIGGERED_TASK', --required
    @parentChildRecs      INT = 1, -- 0 = include parent and child events, 1 = include child batch events only, 2 = include parent batch events only
    @eventType            NVARCHAR(50) = 'TASK_NAME',  --only report parent batches having this BATCH_EVENT_TYPE_CODE
    @eventName            NVARCHAR(100) = '', --e.g. 'processEnrollmentPointFiles', a valid value for @eventType. if @eventType is given and this field is blank, then only check for the existence of @eventType row
    @sortBy               NVARCHAR(50) = 'parentCreateDate',
    @sortDir              NVARCHAR(1) = 'A'
) AS BEGIN
    SET NOCOUNT ON

    DECLARE @recordCount INT,
        @pageCount INT,
        @startId INT,
        @endId INT

    CREATE TABLE #tempTable(
        TEMP_TABLE_ID               INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        PARENT_BATCH_ID                BIGINT,
        PARENT_BATCH_TYPE_CODE        NVARCHAR(50),
        PARENT_STATUS_CODE            NVARCHAR(50),
        PARENT_CREATE_DATE            DATETIME2(7),
        BATCH_ID                    BIGINT,
        STRTIM                       NVARCHAR(25),
        ENDTIM                       NVARCHAR(25),
        TASK_NAME                      NVARCHAR(100),
        ERRFIL                      NVARCHAR(255),
        [FILE]                      NVARCHAR(255),
        STEP                         NVARCHAR(255),
        RECS                         NVARCHAR(15),
        ERRS                         NVARCHAR(15),
        TOTAL_POINTS                   NVARCHAR(20)
    )

    IF @batchType IS NULL OR LTRIM(RTRIM(@batchType)) = '' SET @batchType = 'TRIGGERED_TASK'
    IF @parentChildRecs NOT IN (0,1,2) SET @parentChildRecs = 1
    IF @eventType IS NULL OR LTRIM(RTRIM(@eventType)) = '' SET @eventType = 'TASK_NAME'
    IF @eventName IS NULL OR LTRIM(RTRIM(@eventName)) = '' SET @eventName = ''

    --do this if parent requested
    IF (@parentChildRecs = 0  OR  @parentChildRecs = 2)
    BEGIN
        INSERT INTO #tempTable (PARENT_BATCH_ID, PARENT_BATCH_TYPE_CODE, PARENT_STATUS_CODE, PARENT_CREATE_DATE, BATCH_ID)
        SELECT DISTINCT bat.BATCH_ID AS PARENT_BATCH_ID, bat.BATCH_TYPE_CODE AS PARENT_BATCH_TYPE_CODE, bat.STATUS_TYPE_CODE AS PARENT_STATUS_CODE, bat.CREATE_DATE AS PARENT_CREATE_DATE,
               bat.BATCH_ID AS BATCH_ID
          FROM component.BATCH AS bat
         INNER JOIN component.BATCH_EVENT AS be ON be.BATCH_ID = bat.BATCH_ID
         WHERE bat.BATCH_TYPE_CODE = @batchType
           AND ( (@eventType = '' AND @eventName = '') OR
                 ((NOT @eventType = '') AND @eventName = '' AND EXISTS (SELECT * FROM component.BATCH_EVENT WHERE BATCH_ID = be.BATCH_ID AND BATCH_EVENT_TYPE_CODE = @eventType)) OR
                 ((NOT @eventType = '') AND NOT(@eventName = '') AND EXISTS (SELECT * FROM component.BATCH_EVENT WHERE BATCH_ID = be.BATCH_ID AND BATCH_EVENT_TYPE_CODE = @eventType AND [MESSAGE] = @eventName)))
    END

    --do this if child requested
    IF (@parentChildRecs = 0  OR  @parentChildRecs = 1)
    BEGIN
        INSERT INTO #tempTable (PARENT_BATCH_ID, PARENT_BATCH_TYPE_CODE, PARENT_STATUS_CODE, PARENT_CREATE_DATE, BATCH_ID)
        SELECT DISTINCT bat.BATCH_ID AS PARENT_BATCH_ID, bat.BATCH_TYPE_CODE AS PARENT_BATCH_TYPE_CODE, bat.STATUS_TYPE_CODE AS PARENT_STATUS_CODE, bat.CREATE_DATE AS PARENT_CREATE_DATE,
               CASE WHEN be.BATCH_EVENT_ID IS NULL THEN bat.BATCH_ID ELSE CASE WHEN ISNUMERIC(LTRIM(RTRIM(be.[MESSAGE]))) = 1 THEN CONVERT(bigint, LTRIM(RTRIM(be.[MESSAGE]))) ELSE -1 END END
          FROM component.BATCH AS bat
         LEFT OUTER JOIN component.BATCH_EVENT AS be ON (be.BATCH_ID = bat.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'CHILD_BATCH_ID')
         WHERE bat.BATCH_TYPE_CODE = @batchType
           AND ( (@eventType = '' AND @eventName = '') OR
                 ((NOT @eventType = '') AND @eventName = '' AND EXISTS (SELECT * FROM component.BATCH_EVENT WHERE BATCH_ID = (CASE WHEN be.BATCH_EVENT_ID IS NULL THEN bat.BATCH_ID ELSE be.BATCH_ID END) AND BATCH_EVENT_TYPE_CODE = @eventType)) OR
                 ((NOT @eventType = '') AND NOT(@eventName = '') AND EXISTS (SELECT * FROM component.BATCH_EVENT WHERE BATCH_ID = (CASE WHEN be.BATCH_EVENT_ID IS NULL THEN bat.BATCH_ID ELSE be.BATCH_ID END) AND BATCH_EVENT_TYPE_CODE = @eventType AND [MESSAGE] = @eventName)))
    END

    UPDATE #tempTable
       SET STRTIM = (SELECT TOP 1 SUBSTRING([MESSAGE],1,25) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'STRTIM')

    UPDATE #tempTable
       SET ENDTIM = (SELECT TOP 1 SUBSTRING([MESSAGE],1,25) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'ENDTIM')

    UPDATE #tempTable
       SET TASK_NAME = (SELECT TOP 1 SUBSTRING([MESSAGE],1,100) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.PARENT_BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'TASK_NAME')

    UPDATE #tempTable
       SET ERRFIL = (SELECT TOP 1 SUBSTRING([MESSAGE],1,255) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'ERRFIL')

    UPDATE #tempTable
       SET [FILE] = (SELECT TOP 1 SUBSTRING([MESSAGE],1,255) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'FILE')

    UPDATE #tempTable
       SET STEP = (SELECT TOP 1 SUBSTRING([MESSAGE],1,255) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'STEP')

    UPDATE #tempTable
       SET RECS = (SELECT TOP 1 SUBSTRING([MESSAGE],1,15) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'RECS')

    UPDATE #tempTable
       SET ERRS = (SELECT TOP 1 SUBSTRING([MESSAGE],1,15) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'ERRS')

    UPDATE #tempTable
       SET TOTAL_POINTS = (SELECT TOP 1 SUBSTRING([MESSAGE],1,20) FROM component.BATCH_EVENT WHERE BATCH_ID = #tempTable.BATCH_ID AND BATCH_EVENT_TYPE_CODE = 'TOTAL_POINTS')

    --select * from #tempTable

    CREATE TABLE #ordered (
        [id]    INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
        data_id INT                 NOT NULL
    )

    INSERT INTO #ordered (data_id)
    SELECT tt.TEMP_TABLE_ID
    FROM #tempTable tt
    ORDER BY
        --must break out case stmt's by datatype precedence, else conversion errors:  https://dba.stackexchange.com/questions/4162/conversion-failed-error-with-order-by-case-expression
        CASE @sortDir
        WHEN 'D'
        THEN
            CASE @sortBy
                WHEN 'parentBatchId'
                THEN tt.PARENT_BATCH_ID
                WHEN 'batchId'
                THEN tt.BATCH_ID
                WHEN 'recs'
                THEN CASE WHEN ISNUMERIC(tt.RECS) = 1 THEN CONVERT(int, tt.RECS) ELSE -1 END
                WHEN 'errs'
                THEN CASE WHEN ISNUMERIC(tt.ERRS) = 1 THEN CONVERT(int, tt.ERRS) ELSE -1 END
                WHEN 'totalPoints'
                THEN CASE WHEN ISNUMERIC(tt.TOTAL_POINTS) = 1 THEN CONVERT(decimal(12,2), tt.TOTAL_POINTS) ELSE -1 END
            END
        END DESC,
        CASE @sortDir
        WHEN 'D'
        THEN
            CASE @sortBy
                WHEN 'parentCreateDate'
                THEN tt.PARENT_CREATE_DATE
            END
        END DESC,
        CASE @sortDir
        WHEN 'D'
        THEN
            CASE @sortBy
                WHEN 'startTime'
                THEN tt.STRTIM
                WHEN 'endTime'
                THEN tt.ENDTIM
                WHEN 'fileName'
                THEN tt.[FILE]
            END
        END DESC,
        CASE @sortDir
        WHEN 'A'
        THEN
            CASE @sortBy
                WHEN 'parentBatchId'
                THEN tt.PARENT_BATCH_ID
                WHEN 'batchId'
                THEN tt.BATCH_ID
                WHEN 'recs'
                THEN CASE WHEN ISNUMERIC(tt.RECS) = 1 THEN CONVERT(int, tt.RECS) ELSE -1 END
                WHEN 'errs'
                THEN CASE WHEN ISNUMERIC(tt.ERRS) = 1 THEN CONVERT(int, tt.ERRS) ELSE -1 END
                WHEN 'totalPoints'
                THEN CASE WHEN ISNUMERIC(tt.TOTAL_POINTS) = 1 THEN CONVERT(decimal(12,2), tt.TOTAL_POINTS) ELSE -1 END
            END
        END ASC,
        CASE @sortDir
        WHEN 'A'
        THEN
            CASE @sortBy
                WHEN 'parentCreateDate'
                THEN tt.PARENT_CREATE_DATE
            END
        END ASC,
        CASE @sortDir
        WHEN 'A'
        THEN
            CASE @sortBy
                WHEN 'startTime'
                THEN tt.STRTIM
                WHEN 'endTime'
                THEN tt.ENDTIM
                WHEN 'fileName'
                THEN tt.[FILE]
            END
        END ASC,
        CASE @sortBy  --secondary sort unless the date already specified
            WHEN 'parentCreateDate'
            THEN getdate()
            ELSE tt.PARENT_CREATE_DATE
        END
        DESC

    SELECT @recordCount = @@ROWCOUNT

    PRINT('RECORD_COUNT: ' + convert(varchar, @recordCount));

    IF @recordsPerPage = -1
    BEGIN
        SET @recordsPerPage = @recordCount
        SET @pageNumber = 1
    END

    IF @recordsPerPage = 0
    BEGIN
        SET @recordsPerPage = 1
    END

    --Make sure the requested page number is within the valid range of pages (if not, then fix it)
    SET @pageCount = (@recordCount + @recordsPerPage - 1) / @recordsPerPage
    IF @pageNumber > @pageCount
    BEGIN SET @pageNumber = @pageCount END
    IF @pageNumber < 1
    BEGIN SET @pageNumber = 1 END

    SET @endId = @pageNumber * @recordsPerPage
    SET @startId = (@endId - @recordsPerPage) + 1

    SELECT
        PARENT_BATCH_ID,
        PARENT_BATCH_TYPE_CODE,
        PARENT_STATUS_CODE,
        PARENT_CREATE_DATE,
        BATCH_ID,
        STRTIM,
        ENDTIM,
        TASK_NAME,
        ERRFIL,
        [FILE],
        STEP,
        RECS,
        ERRS,
        TOTAL_POINTS,
        @recordCount    AS RECORD_COUNT,
        @recordsPerPage AS RECORDS_PER_PAGE,
        @pageNumber     AS PAGE_NUMBER,
        @pageCount      AS PAGE_COUNT,
        @startId        AS START_ID,
        @endId          AS END_ID
    FROM #ordered o
    INNER JOIN #tempTable tt ON tt.TEMP_TABLE_ID = o.data_id
    WHERE o.id BETWEEN @startId AND @endId
    ORDER BY o.id

    DROP TABLE #tempTable
    DROP TABLE #ordered
END
GO


