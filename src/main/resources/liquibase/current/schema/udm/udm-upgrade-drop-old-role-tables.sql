/*
Run this script on:

        dev-future-database.tenant_cnxt    -  This database will be modified

to synchronize it with:

        dev-future-database.tenant_udm

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.1.3 from Red Gate Software Ltd at 4/13/2017 2:17:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[EVENT_CONFIG]'
GO
ALTER TABLE [component].[EVENT_CONFIG] DROP CONSTRAINT [PK_EVENT_CONFIG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[EVENT_CONFIG]'
GO
ALTER TABLE [component].[EVENT_CONFIG] DROP CONSTRAINT [DF_EVENT_CONFIG_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[EVENT_CONFIG]'
GO
ALTER TABLE [component].[EVENT_CONFIG] DROP CONSTRAINT [DF_EVENT_CONFIG_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[EVENT_CONFIG]'
GO
ALTER TABLE [component].[EVENT_CONFIG] DROP CONSTRAINT [DF_EVENT_CONFIG_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[EVENT_CONFIG]'
GO
ALTER TABLE [component].[EVENT_CONFIG] DROP CONSTRAINT [DF_EVENT_CONFIG_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_ROLE_GROUPS]'
GO
ALTER TABLE [component].[HISTORY_ROLE_GROUPS] DROP CONSTRAINT [PK_HISTORY_ROLE_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_ROLE_GROUPS]'
GO
ALTER TABLE [component].[HISTORY_ROLE_GROUPS] DROP CONSTRAINT [DF_HISTORY_ROLE_GROUPS_CHANGE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_ROLE_PAX]'
GO
ALTER TABLE [component].[HISTORY_ROLE_PAX] DROP CONSTRAINT [PK_HISTORY_ROLE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_ROLE_PAX]'
GO
ALTER TABLE [component].[HISTORY_ROLE_PAX] DROP CONSTRAINT [DF_HISTORY_ROLE_PAX_CHANGE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_GROUPS]'
GO
ALTER TABLE [component].[ROLE_GROUPS] DROP CONSTRAINT [PK_ROLE_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_GROUPS]'
GO
ALTER TABLE [component].[ROLE_GROUPS] DROP CONSTRAINT [DF_ROLE_GROUPS_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_GROUPS]'
GO
ALTER TABLE [component].[ROLE_GROUPS] DROP CONSTRAINT [DF_ROLE_GROUPS_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_GROUPS]'
GO
ALTER TABLE [component].[ROLE_GROUPS] DROP CONSTRAINT [DF_ROLE_GROUPS_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_GROUPS]'
GO
ALTER TABLE [component].[ROLE_GROUPS] DROP CONSTRAINT [DF_ROLE_GROUPS_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [PK_ROLE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [DF_ROLE_PAX_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [DF_ROLE_PAX_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [DF_ROLE_PAX_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [DF_ROLE_PAX_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ROLE_GROUPS_1] from [component].[ROLE_GROUPS]'
GO
DROP INDEX [IX_ROLE_GROUPS_1] ON [component].[ROLE_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ROLE_PAX_1] from [component].[ROLE_PAX]'
GO
DROP INDEX [IX_ROLE_PAX_1] ON [component].[ROLE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_EVENT_CONFIG_1] from [component].[EVENT_CONFIG]'
GO
DROP INDEX [IX_EVENT_CONFIG_1] ON [component].[EVENT_CONFIG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_EVENT_CONFIG_U_1] from [component].[EVENT_CONFIG]'
GO
DROP TRIGGER [component].[TR_EVENT_CONFIG_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_ROLE_GROUPS_DU_1] from [component].[ROLE_GROUPS]'
GO
DROP TRIGGER [component].[TR_ROLE_GROUPS_DU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_ROLE_GROUPS_U_1] from [component].[ROLE_GROUPS]'
GO
DROP TRIGGER [component].[TR_ROLE_GROUPS_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_ROLE_PAX_DU_1] from [component].[ROLE_PAX]'
GO
DROP TRIGGER [component].[TR_ROLE_PAX_DU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_ROLE_PAX_U_1] from [component].[ROLE_PAX]'
GO
DROP TRIGGER [component].[TR_ROLE_PAX_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[HISTORY_ROLE_PAX]'
GO
DROP TABLE [component].[HISTORY_ROLE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[EVENT_CONFIG]'
GO
DROP TABLE [component].[EVENT_CONFIG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[HISTORY_ROLE_GROUPS]'
GO
DROP TABLE [component].[HISTORY_ROLE_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[VW_ACL_RESOLVE_ROLES]'
GO
DROP VIEW [component].[VW_ACL_RESOLVE_ROLES]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[UP_ASSOCIATION_FETCH_PERMISSIONS]'
GO
DROP PROCEDURE [component].[UP_ASSOCIATION_FETCH_PERMISSIONS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[ROLE_GROUPS]'
GO
DROP TABLE [component].[ROLE_GROUPS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [component].[ROLE_PAX]'
GO
DROP TABLE [component].[ROLE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO