/*
Run this script on:

        dev-future-database.tenant_cnxt    -  This database will be modified

to synchronize it with:

        dev-future-database.tenant_udm

You are recommended to back up your database before running this script

Script created by SQL Compare version 11.1.3 from Red Gate Software Ltd at 4/11/2017 5:02:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [FK_PAX_LANGUAGE]
ALTER TABLE [component].[PAX] DROP CONSTRAINT [FK_PAX_PAX_TYPE]
ALTER TABLE [component].[PAX] DROP CONSTRAINT [FK_PAX_LOCALE_INFO]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[ADDRESS]'
GO
ALTER TABLE [component].[ADDRESS] DROP CONSTRAINT [FK_ADDRESS_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[ALERT]'
GO
ALTER TABLE [component].[ALERT] DROP CONSTRAINT [FK_ALERT_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[ALIAS_HISTORY]'
GO
ALTER TABLE [component].[ALIAS_HISTORY] DROP CONSTRAINT [FK_ALIAS_HISTORY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[APPROVAL_HISTORY]'
GO
ALTER TABLE [component].[APPROVAL_HISTORY] DROP CONSTRAINT [FK_APPROVAL_HISTORY_PAX_1]
ALTER TABLE [component].[APPROVAL_HISTORY] DROP CONSTRAINT [FK_APPROVAL_HISTORY_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[APPROVAL_PENDING]'
GO
ALTER TABLE [component].[APPROVAL_PENDING] DROP CONSTRAINT [FK_APPROVAL_PENDING_PAX_1]
ALTER TABLE [component].[APPROVAL_PENDING] DROP CONSTRAINT [FK_APPROVAL_PENDING_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[AUXILIARY_NOTIFICATION]'
GO
ALTER TABLE [component].[AUXILIARY_NOTIFICATION] DROP CONSTRAINT [FK_AUXILIARY_NOTIFICATION_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[AUXILIARY_SUBMITTER]'
GO
ALTER TABLE [component].[AUXILIARY_SUBMITTER] DROP CONSTRAINT [FK_AUXILIARY_SUBMITTER_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[BUDGET_ALERT]'
GO
ALTER TABLE [component].[BUDGET_ALERT] DROP CONSTRAINT [FK_BUDGET_ALERT_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[COMMENT]'
GO
ALTER TABLE [component].[COMMENT] DROP CONSTRAINT [FK_COMMENT_PAX_1]
ALTER TABLE [component].[COMMENT] DROP CONSTRAINT [FK_COMMENT_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[DEVICE]'
GO
ALTER TABLE [component].[DEVICE] DROP CONSTRAINT [FK_DEVICE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[EARNINGS]'
GO
ALTER TABLE [component].[EARNINGS] DROP CONSTRAINT [FK_EARNINGS_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[EMAIL_MESSAGE]'
GO
ALTER TABLE [component].[EMAIL_MESSAGE] DROP CONSTRAINT [FK_EMAIL_MESSAGE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[EMAIL]'
GO
ALTER TABLE [component].[EMAIL] DROP CONSTRAINT [FK_EMAIL_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[GROUPS]'
GO
ALTER TABLE [component].[GROUPS] DROP CONSTRAINT [FK_GROUPS_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[GROUPS_PAX]'
GO
ALTER TABLE [component].[GROUPS_PAX] DROP CONSTRAINT [FK_GROUPS_PAX_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[LIKES]'
GO
ALTER TABLE [component].[LIKES] DROP CONSTRAINT [FK_LIKES_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NEWSFEED_ITEM]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM] DROP CONSTRAINT [FK_NEWSFEED_ITEM_PAX_1]
ALTER TABLE [component].[NEWSFEED_ITEM] DROP CONSTRAINT [FK_NEWSFEED_ITEM_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NEWSFEED_ITEM_PAX]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM_PAX] DROP CONSTRAINT [FK_NEWSFEED_ITEM_PAX_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NOMINATION]'
GO
ALTER TABLE [component].[NOMINATION] DROP CONSTRAINT [FK_NOMINATION_PAX_1]
ALTER TABLE [component].[NOMINATION] DROP CONSTRAINT [FK_NOMINATION_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NOTE]'
GO
ALTER TABLE [component].[NOTE] DROP CONSTRAINT [FK_NOTE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[NOTIFICATION]'
GO
ALTER TABLE [component].[NOTIFICATION] DROP CONSTRAINT [FK_NOTIFICATION_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_ACCOUNT]'
GO
ALTER TABLE [component].[PAX_ACCOUNT] DROP CONSTRAINT [FK_PAX_ACCOUNT_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_COMMUNICATION_PREFERENCE]'
GO
ALTER TABLE [component].[PAX_COMMUNICATION_PREFERENCE] DROP CONSTRAINT [FK_PAX_COMMUNICATION_PREFERENCE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_DATA]'
GO
ALTER TABLE [component].[PAX_DATA] DROP CONSTRAINT [FK_PAX_DATA_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_FILES]'
GO
ALTER TABLE [component].[PAX_FILES] DROP CONSTRAINT [FK_PAX_FILES_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_GROUP]'
GO
ALTER TABLE [component].[PAX_GROUP] DROP CONSTRAINT [FK_PAX_GROUP_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAX_MISC]'
GO
ALTER TABLE [component].[PAX_MISC] DROP CONSTRAINT [FK_PAX_MISC_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PAYOUT]'
GO
ALTER TABLE [component].[PAYOUT] DROP CONSTRAINT [FK_PAYOUT_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PHONE]'
GO
ALTER TABLE [component].[PHONE] DROP CONSTRAINT [FK_PHONE_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PROGRAM_ENROLLMENT]'
GO
ALTER TABLE [component].[PROGRAM_ENROLLMENT] DROP CONSTRAINT [FK_PROGRAM_ENROLLMENT_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PROGRAM]'
GO
ALTER TABLE [component].[PROGRAM] DROP CONSTRAINT [FK_PROGRAM_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[RECOGNITION]'
GO
ALTER TABLE [component].[RECOGNITION] DROP CONSTRAINT [FK_RECOGNITION_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[RELATIONSHIP]'
GO
ALTER TABLE [component].[RELATIONSHIP] DROP CONSTRAINT [FK_RELATIONSHIP_PAX_1]
ALTER TABLE [component].[RELATIONSHIP] DROP CONSTRAINT [FK_RELATIONSHIP_PAX_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[ROLE_PAX]'
GO
ALTER TABLE [component].[ROLE_PAX] DROP CONSTRAINT [FK_ROLE_PAX_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[SYS_USER]'
GO
ALTER TABLE [component].[SYS_USER] DROP CONSTRAINT [FK_SYS_USER_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_PAX]'
GO
ALTER TABLE [component].[HISTORY_PAX] DROP CONSTRAINT [CKC_SSN_HISTORY_]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_PAX]'
GO
ALTER TABLE [component].[HISTORY_PAX] DROP CONSTRAINT [PK_HISTORY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_PAX]'
GO
ALTER TABLE [component].[HISTORY_PAX] DROP CONSTRAINT [DF_HISTORY_PAX_CHANGE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [CKC_SSN_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [PK_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_PREFERRED_LOCALE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PAX]'
GO
ALTER TABLE [component].[PAX] DROP CONSTRAINT [DF_PAX_LANGUAGE_LOCALE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_PAX_ID_UPDATE_DATE] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_PAX_ID_UPDATE_DATE] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_5] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_5] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_2] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_2] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_3] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_3] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_1] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_1] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_4] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_4] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PAX_PAX_TYPE_CODE] from [component].[PAX]'
GO
DROP INDEX [IX_PAX_PAX_TYPE_CODE] ON [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_PAX_DU_1] from [component].[PAX]'
GO
DROP TRIGGER [component].[TR_PAX_DU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_PAX_U_1] from [component].[PAX]'
GO
DROP TRIGGER [component].[TR_PAX_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [component].[PAX]'
GO
CREATE TABLE [component].[RG_Recovery_1_PAX]
(
[PAX_ID] [bigint] NOT NULL IDENTITY(1, 1),
[LANGUAGE_CODE] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PREFERRED_LOCALE] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_PREFERRED_LOCALE] DEFAULT ('en-US'),
[CONTROL_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SSN] [component].[TY_ENCRYPTED_DATA_50] NULL,
[FIRST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_PREFIX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_SUFFIX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPANY_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_NUM] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALTERNATE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HASH] [bigint] NULL,
[PAX_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LANGUAGE_LOCALE] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_LANGUAGE_LOCALE] DEFAULT ('en_US'),
[CREATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PAX_CREATE_DATE] DEFAULT (getdate()),
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_CREATE_ID] DEFAULT (user_name()),
[UPDATE_DATE] [datetime2] NOT NULL CONSTRAINT [DF_PAX_UPDATE_DATE] DEFAULT (getdate()),
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PAX_UPDATE_ID] DEFAULT (user_name())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_1_PAX] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [component].[RG_Recovery_1_PAX]([PAX_ID], [LANGUAGE_CODE], [PREFERRED_LOCALE], [CONTROL_NUM], [SSN], [FIRST_NAME], [MIDDLE_NAME], [LAST_NAME], [NAME_PREFIX], [NAME_SUFFIX], [COMPANY_NAME], [TAX_NUM], [ALTERNATE_NAME], [HASH], [PAX_TYPE_CODE], [LANGUAGE_LOCALE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID]) SELECT [PAX_ID], [LANGUAGE_CODE], [PREFERRED_LOCALE], [CONTROL_NUM], [SSN], [FIRST_NAME], [MIDDLE_NAME], [LAST_NAME], [NAME_PREFIX], [NAME_SUFFIX], [COMPANY_NAME], [TAX_NUM], [ALTERNATE_NAME], [HASH], [PAX_TYPE_CODE], [LANGUAGE_LOCALE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID] FROM [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_1_PAX] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[component].[PAX]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[component].[RG_Recovery_1_PAX]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [component].[PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[RG_Recovery_1_PAX]', N'PAX', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PAX] on [component].[PAX]'
GO
ALTER TABLE [component].[PAX] ADD CONSTRAINT [PK_PAX] PRIMARY KEY CLUSTERED  ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_5] on [component].[PAX]'
GO
CREATE NONCLUSTERED INDEX [IX_PAX_5] ON [component].[PAX] ([LANGUAGE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_2] on [component].[PAX]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PAX_2] ON [component].[PAX] ([CONTROL_NUM])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_3] on [component].[PAX]'
GO
CREATE NONCLUSTERED INDEX [IX_PAX_3] ON [component].[PAX] ([SSN])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_1] on [component].[PAX]'
GO
CREATE NONCLUSTERED INDEX [IX_PAX_1] ON [component].[PAX] ([LAST_NAME], [FIRST_NAME])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PAX_4] on [component].[PAX]'
GO
CREATE NONCLUSTERED INDEX [IX_PAX_4] ON [component].[PAX] ([HASH])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [component].[HISTORY_PAX]'
GO
CREATE TABLE [component].[RG_Recovery_2_HISTORY_PAX]
(
[HISTORY_ID] [bigint] NOT NULL IDENTITY(1, 1),
[PAX_ID] [bigint] NULL,
[LANGUAGE_CODE] [nchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PREFERRED_LOCALE] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CONTROL_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [component].[TY_ENCRYPTED_DATA_50] NULL,
[FIRST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_PREFIX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NAME_SUFFIX] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COMPANY_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_NUM] [nvarchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ALTERNATE_NAME] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HASH] [bigint] NULL,
[PAX_TYPE_CODE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LANGUAGE_LOCALE] [nchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [datetime2] NULL,
[CREATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_DATE] [datetime2] NULL,
[UPDATE_ID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHANGE_DATE] [datetime2] NULL CONSTRAINT [DF_HISTORY_PAX_CHANGE_DATE] DEFAULT (getdate()),
[CHANGE_ID] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CHANGE_OPERATION] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_2_HISTORY_PAX] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [component].[RG_Recovery_2_HISTORY_PAX]([HISTORY_ID], [PAX_ID], [LANGUAGE_CODE], [PREFERRED_LOCALE], [CONTROL_NUM], [SSN], [FIRST_NAME], [MIDDLE_NAME], [LAST_NAME], [NAME_PREFIX], [NAME_SUFFIX], [COMPANY_NAME], [TAX_NUM], [ALTERNATE_NAME], [HASH], [PAX_TYPE_CODE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [CHANGE_DATE], [CHANGE_ID], [CHANGE_OPERATION]) SELECT [HISTORY_ID], [PAX_ID], [LANGUAGE_CODE], [PREFERRED_LOCALE], [CONTROL_NUM], [SSN], [FIRST_NAME], [MIDDLE_NAME], [LAST_NAME], [NAME_PREFIX], [NAME_SUFFIX], [COMPANY_NAME], [TAX_NUM], [ALTERNATE_NAME], [HASH], [PAX_TYPE_CODE], [CREATE_DATE], [CREATE_ID], [UPDATE_DATE], [UPDATE_ID], [CHANGE_DATE], [CHANGE_ID], [CHANGE_OPERATION] FROM [component].[HISTORY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [component].[RG_Recovery_2_HISTORY_PAX] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[component].[HISTORY_PAX]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[component].[RG_Recovery_2_HISTORY_PAX]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [component].[HISTORY_PAX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[RG_Recovery_2_HISTORY_PAX]', N'HISTORY_PAX', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_HISTORY_PAX] on [component].[HISTORY_PAX]'
GO
ALTER TABLE [component].[HISTORY_PAX] ADD CONSTRAINT [PK_HISTORY_PAX] PRIMARY KEY CLUSTERED  ([HISTORY_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HISTORY_PAX_2] on [component].[HISTORY_PAX]'
GO
CREATE NONCLUSTERED INDEX [IX_HISTORY_PAX_2] ON [component].[HISTORY_PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PAX_DU_1] on [component].[PAX]'
GO
-- ==============================  NAME  ======================================
-- TR_PAX_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PAX_DU_1]
ON [component].[PAX]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_PAX (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    PAX_ID
,    LANGUAGE_CODE
,    PREFERRED_LOCALE
,    CONTROL_NUM
,    SSN
,    FIRST_NAME
,    MIDDLE_NAME
,    LAST_NAME
,    NAME_PREFIX
,    NAME_SUFFIX
,    COMPANY_NAME
,    TAX_NUM
,    ALTERNATE_NAME
,    HASH
,    PAX_TYPE_CODE
,    LANGUAGE_LOCALE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    PAX_ID
,    LANGUAGE_CODE
,    PREFERRED_LOCALE
,    CONTROL_NUM
,    SSN
,    FIRST_NAME
,    MIDDLE_NAME
,    LAST_NAME
,    NAME_PREFIX
,    NAME_SUFFIX
,    COMPANY_NAME
,    TAX_NUM
,    ALTERNATE_NAME
,    HASH
,    PAX_TYPE_CODE
,    LANGUAGE_LOCALE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''PAX''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PAX_U_1] on [component].[PAX]'
GO
-- ==============================  NAME  ======================================
-- TR_PAX_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PAX_U_1]
ON [component].[PAX]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PAX T1
    ,       INSERTED T2
    WHERE   T1.PAX_ID = T2.PAX_ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PAX T1
    ,       INSERTED T2
    WHERE   T1.PAX_ID = T2.PAX_ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[HISTORY_PAX]'
GO
ALTER TABLE [component].[HISTORY_PAX] ADD CONSTRAINT [CKC_SSN_HISTORY_] CHECK (([SSN] IS NULL OR (len([SSN])-(len([SSN])/(4))*(4))=(0)))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[PAX]'
GO
ALTER TABLE [component].[PAX] ADD CONSTRAINT [CKC_SSN_PAX] CHECK (([SSN] IS NULL OR (len([SSN])-(len([SSN])/(4))*(4))=(0)))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX]'
GO
ALTER TABLE [component].[PAX] ADD CONSTRAINT [FK_PAX_LANGUAGE] FOREIGN KEY ([LANGUAGE_CODE]) REFERENCES [component].[LANGUAGE] ([LANGUAGE_CODE])
ALTER TABLE [component].[PAX] ADD CONSTRAINT [FK_PAX_PAX_TYPE] FOREIGN KEY ([PAX_TYPE_CODE]) REFERENCES [component].[PAX_TYPE] ([PAX_TYPE_CODE])
ALTER TABLE [component].[PAX] ADD CONSTRAINT [FK_PAX_LOCALE_INFO] FOREIGN KEY ([PREFERRED_LOCALE]) REFERENCES [component].[LOCALE_INFO] ([LOCALE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[ADDRESS]'
GO
ALTER TABLE [component].[ADDRESS] ADD CONSTRAINT [FK_ADDRESS_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[ALERT]'
GO
ALTER TABLE [component].[ALERT] ADD CONSTRAINT [FK_ALERT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[ALIAS_HISTORY]'
GO
ALTER TABLE [component].[ALIAS_HISTORY] ADD CONSTRAINT [FK_ALIAS_HISTORY_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[APPROVAL_HISTORY]'
GO
ALTER TABLE [component].[APPROVAL_HISTORY] ADD CONSTRAINT [FK_APPROVAL_HISTORY_PAX_1] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[APPROVAL_HISTORY] ADD CONSTRAINT [FK_APPROVAL_HISTORY_PAX_2] FOREIGN KEY ([PROXY_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[APPROVAL_PENDING]'
GO
ALTER TABLE [component].[APPROVAL_PENDING] ADD CONSTRAINT [FK_APPROVAL_PENDING_PAX_1] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[APPROVAL_PENDING] ADD CONSTRAINT [FK_APPROVAL_PENDING_PAX_2] FOREIGN KEY ([NEXT_APPROVER_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[AUXILIARY_NOTIFICATION]'
GO
ALTER TABLE [component].[AUXILIARY_NOTIFICATION] ADD CONSTRAINT [FK_AUXILIARY_NOTIFICATION_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[AUXILIARY_SUBMITTER]'
GO
ALTER TABLE [component].[AUXILIARY_SUBMITTER] ADD CONSTRAINT [FK_AUXILIARY_SUBMITTER_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[BUDGET_ALERT]'
GO
ALTER TABLE [component].[BUDGET_ALERT] ADD CONSTRAINT [FK_BUDGET_ALERT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[COMMENT]'
GO
ALTER TABLE [component].[COMMENT] ADD CONSTRAINT [FK_COMMENT_PAX_1] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[COMMENT] ADD CONSTRAINT [FK_COMMENT_PAX_2] FOREIGN KEY ([ADMIN_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[DEVICE]'
GO
ALTER TABLE [component].[DEVICE] ADD CONSTRAINT [FK_DEVICE_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[EARNINGS]'
GO
ALTER TABLE [component].[EARNINGS] ADD CONSTRAINT [FK_EARNINGS_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[EMAIL_MESSAGE]'
GO
ALTER TABLE [component].[EMAIL_MESSAGE] ADD CONSTRAINT [FK_EMAIL_MESSAGE_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[EMAIL]'
GO
ALTER TABLE [component].[EMAIL] ADD CONSTRAINT [FK_EMAIL_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[GROUPS]'
GO
ALTER TABLE [component].[GROUPS] ADD CONSTRAINT [FK_GROUPS_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[GROUPS_PAX]'
GO
ALTER TABLE [component].[GROUPS_PAX] ADD CONSTRAINT [FK_GROUPS_PAX_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[LIKES]'
GO
ALTER TABLE [component].[LIKES] ADD CONSTRAINT [FK_LIKES_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NEWSFEED_ITEM]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM] ADD CONSTRAINT [FK_NEWSFEED_ITEM_PAX_1] FOREIGN KEY ([TO_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[NEWSFEED_ITEM] ADD CONSTRAINT [FK_NEWSFEED_ITEM_PAX_2] FOREIGN KEY ([FROM_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NEWSFEED_ITEM_PAX]'
GO
ALTER TABLE [component].[NEWSFEED_ITEM_PAX] ADD CONSTRAINT [FK_NEWSFEED_ITEM_PAX_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NOMINATION]'
GO
ALTER TABLE [component].[NOMINATION] ADD CONSTRAINT [FK_NOMINATION_PAX_1] FOREIGN KEY ([SUBMITTER_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[NOMINATION] ADD CONSTRAINT [FK_NOMINATION_PAX_2] FOREIGN KEY ([PROXY_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NOTE]'
GO
ALTER TABLE [component].[NOTE] ADD CONSTRAINT [FK_NOTE_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[NOTIFICATION]'
GO
ALTER TABLE [component].[NOTIFICATION] ADD CONSTRAINT [FK_NOTIFICATION_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_ACCOUNT]'
GO
ALTER TABLE [component].[PAX_ACCOUNT] ADD CONSTRAINT [FK_PAX_ACCOUNT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_COMMUNICATION_PREFERENCE]'
GO
ALTER TABLE [component].[PAX_COMMUNICATION_PREFERENCE] ADD CONSTRAINT [FK_PAX_COMMUNICATION_PREFERENCE_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_DATA]'
GO
ALTER TABLE [component].[PAX_DATA] ADD CONSTRAINT [FK_PAX_DATA_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_FILES]'
GO
ALTER TABLE [component].[PAX_FILES] ADD CONSTRAINT [FK_PAX_FILES_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_GROUP]'
GO
ALTER TABLE [component].[PAX_GROUP] ADD CONSTRAINT [FK_PAX_GROUP_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAX_MISC]'
GO
ALTER TABLE [component].[PAX_MISC] ADD CONSTRAINT [FK_PAX_MISC_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PAYOUT]'
GO
ALTER TABLE [component].[PAYOUT] ADD CONSTRAINT [FK_PAYOUT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PHONE]'
GO
ALTER TABLE [component].[PHONE] ADD CONSTRAINT [FK_PHONE_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PROGRAM_ENROLLMENT]'
GO
ALTER TABLE [component].[PROGRAM_ENROLLMENT] ADD CONSTRAINT [FK_PROGRAM_ENROLLMENT_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PROGRAM]'
GO
ALTER TABLE [component].[PROGRAM] ADD CONSTRAINT [FK_PROGRAM_PAX] FOREIGN KEY ([CREATOR_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[RECOGNITION]'
GO
ALTER TABLE [component].[RECOGNITION] ADD CONSTRAINT [FK_RECOGNITION_PAX] FOREIGN KEY ([RECEIVER_PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[RELATIONSHIP]'
GO
ALTER TABLE [component].[RELATIONSHIP] ADD CONSTRAINT [FK_RELATIONSHIP_PAX_1] FOREIGN KEY ([PAX_ID_1]) REFERENCES [component].[PAX] ([PAX_ID])
ALTER TABLE [component].[RELATIONSHIP] ADD CONSTRAINT [FK_RELATIONSHIP_PAX_2] FOREIGN KEY ([PAX_ID_2]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[SYS_USER]'
GO
ALTER TABLE [component].[SYS_USER] ADD CONSTRAINT [FK_SYS_USER_PAX] FOREIGN KEY ([PAX_ID]) REFERENCES [component].[PAX] ([PAX_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO