alter table component.APPROVAL_PROCESS_CONFIG add TARGET_TABLE nvarchar(128) null

alter table component.HISTORY_APPROVAL_PROCESS_CONFIG add TARGET_TABLE nvarchar(128) null


/****** Object:  Index [IX_APPROVAL_PROCESS_1]    Script Date: 9/18/2017 8:47:10 AM ******/
DROP INDEX [IX_APPROVAL_PROCESS_1] ON [component].[APPROVAL_PROCESS]
GO

/****** Object:  Index [IX_APPROVAL_PROCESS_1]    Script Date: 9/18/2017 8:47:10 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_APPROVAL_PROCESS_1] ON [component].[APPROVAL_PROCESS]
(
    [APPROVAL_PROCESS_TYPE_CODE] ASC,
    [TARGET_TABLE] ASC,
    [TARGET_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


/****** Object:  Trigger [TR_APPROVAL_PROCESS_CONFIG_D_1]    Script Date: 9/18/2017 9:01:36 AM ******/
DROP TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_D_1]
GO

/****** Object:  Trigger [TR_APPROVAL_PROCESS_CONFIG_U_2]    Script Date: 9/18/2017 9:02:00 AM ******/
DROP TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_U_2]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ==============================  NAME  ======================================
-- TR_APPROVAL_PROCESS_CONFIG_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_APPROVAL_PROCESS_CONFIG_DU_1]
ON [component].[APPROVAL_PROCESS_CONFIG]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_APPROVAL_PROCESS_CONFIG (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_TYPE_CODE
,    APPROVAL_LEVEL
,    APPROVER_PAX_ID
,    TARGET_ID
,    TARGET_TABLE
,    UPDATE_ID
,    UPDATE_DATE
,    CREATE_ID
,    CREATE_DATE
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    APPROVAL_PROCESS_TYPE_CODE
,    APPROVAL_TYPE_CODE
,    APPROVAL_LEVEL
,    APPROVER_PAX_ID
,    TARGET_ID
,    TARGET_TABLE
,    UPDATE_ID
,    UPDATE_DATE
,    CREATE_ID
,    CREATE_DATE
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''APPROVAL_PROCESS_CONFIG''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO

ALTER TABLE [component].[APPROVAL_PROCESS_CONFIG] ENABLE TRIGGER [TR_APPROVAL_PROCESS_CONFIG_DU_1]
GO


