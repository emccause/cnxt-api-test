/*
Rename PROGRAM_AWARD_TIERS_TYPE to PROGRAM_AWARD_TIER_TYPE
and PROGRAM_AWARD_TIERS to PROGRAM_AWARD_TIER
*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [FK_PROGRAM_AWARD_TIERS_PROGRAM]
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [FK_PROGRAM_AWARD_TIERS_PROGRAM_AWARD_TIERS_TYPE]
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [FK_PROGRAM_AWARD_TIERS_STATUS_TYPE_1]
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [FK_PROGRAM_AWARD_TIERS_STATUS_TYPE_2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [FK_PROGRAM_AWARD_TIERS_TYPE_STATUS_TYPE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [component].[RECOGNITION]'
GO
ALTER TABLE [component].[RECOGNITION] DROP CONSTRAINT [FK_RECOGNITION_PROGRAM_AWARD_TIERS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[HISTORY_PROGRAM_AWARD_TIERS] DROP CONSTRAINT [PK_HISTORY_PROGRAM_AWARD_TIERS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[HISTORY_PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[HISTORY_PROGRAM_AWARD_TIERS] DROP CONSTRAINT [DF_HISTORY_PROGRAM_AWARD_TIERS_CHANGE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [PK_PROGRAM_AWARD_TIERS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [PK_PROGRAM_AWARD_TIERS_TYPE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_TYPE_CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_TYPE_CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_TYPE_UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIERS_TYPE] DROP CONSTRAINT [DF_PROGRAM_AWARD_TIERS_TYPE_UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_HISTORY_PROGRAM_AWARD_TIERS_2] from [component].[HISTORY_PROGRAM_AWARD_TIERS]'
GO
DROP INDEX [IX_HISTORY_PROGRAM_AWARD_TIERS_2] ON [component].[HISTORY_PROGRAM_AWARD_TIERS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PROGRAM_AWARD_TIERS_TYPE_1] from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
DROP INDEX [IX_PROGRAM_AWARD_TIERS_TYPE_1] ON [component].[PROGRAM_AWARD_TIERS_TYPE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_PROGRAM_AWARD_TIERS_DU_1] from [component].[PROGRAM_AWARD_TIERS]'
GO
DROP TRIGGER [component].[TR_PROGRAM_AWARD_TIERS_DU_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_PROGRAM_AWARD_TIERS_U_1] from [component].[PROGRAM_AWARD_TIERS]'
GO
DROP TRIGGER [component].[TR_PROGRAM_AWARD_TIERS_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [component].[TR_PROGRAM_AWARD_TIERS_TYPE_U_1] from [component].[PROGRAM_AWARD_TIERS_TYPE]'
GO
DROP TRIGGER [component].[TR_PROGRAM_AWARD_TIERS_TYPE_U_1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[PROGRAM_AWARD_TIERS]', N'PROGRAM_AWARD_TIER', N'OBJECT'
GO
EXEC sp_rename N'[component].[HISTORY_PROGRAM_AWARD_TIERS]', N'HISTORY_PROGRAM_AWARD_TIER', N'OBJECT'
GO
EXEC sp_rename N'[component].[PROGRAM_AWARD_TIERS_TYPE]', N'PROGRAM_AWARD_TIER_TYPE', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[RECOGNITION]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[RECOGNITION].[PROGRAM_AWARD_TIERS_ID]', N'PROGRAM_AWARD_TIER_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PROGRAM_AWARD_TIER_U_1] on [component].[PROGRAM_AWARD_TIER]'
GO
-- ==============================  NAME  ======================================
-- TR_PROGRAM_AWARD_TIER_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PROGRAM_AWARD_TIER_U_1]
ON [component].[PROGRAM_AWARD_TIER]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PROGRAM_AWARD_TIER T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PROGRAM_AWARD_TIER T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[HISTORY_RECOGNITION]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[HISTORY_RECOGNITION].[PROGRAM_AWARD_TIERS_ID]', N'PROGRAM_AWARD_TIER_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PROGRAM_AWARD_TIER_DU_1] on [component].[PROGRAM_AWARD_TIER]'
GO
-- ==============================  NAME  ======================================
-- TR_PROGRAM_AWARD_TIER_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PROGRAM_AWARD_TIER_DU_1]
ON [component].[PROGRAM_AWARD_TIER]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_PROGRAM_AWARD_TIER (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    PROGRAM_ID
,    MIN_AMOUNT
,    MAX_AMOUNT
,    INCREMENT
,    PROGRAM_AWARD_TIER_TYPE_CODE
,    STATUS_TYPE_CODE
,    RAISING_STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    PROGRAM_ID
,    MIN_AMOUNT
,    MAX_AMOUNT
,    INCREMENT
,    PROGRAM_AWARD_TIER_TYPE_CODE
,    STATUS_TYPE_CODE
,    RAISING_STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''PROGRAM_AWARD_TIER''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[PROGRAM_AWARD_TIER_TYPE].[PROGRAM_AWARD_TIERS_TYPE_CODE]', N'PROGRAM_AWARD_TIER_TYPE_CODE', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[PROGRAM_AWARD_TIER_TYPE].[PROGRAM_AWARD_TIERS_TYPE_NAME]', N'PROGRAM_AWARD_TIER_TYPE_NAME', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[component].[PROGRAM_AWARD_TIER_TYPE].[PROGRAM_AWARD_TIERS_TYPE_DESC]', N'PROGRAM_AWARD_TIER_TYPE_DESC', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PROGRAM_AWARD_TIER_TYPE] on [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [PK_PROGRAM_AWARD_TIER_TYPE] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PROGRAM_AWARD_TIER_TYPE_1] on [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PROGRAM_AWARD_TIER_TYPE_1] ON [component].[PROGRAM_AWARD_TIER_TYPE] ([PROGRAM_AWARD_TIER_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [component].[TR_PROGRAM_AWARD_TIER_TYPE_U_1] on [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
-- ==============================  NAME  ======================================
-- TR_PROGRAM_AWARD_TIER_TYPE_U_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to update the UPDATE_DATE and UPDATE_ID.
-- If the application provides the UPDATE_ID, use it else, use USER_NAME()
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20040101        Created
-- 
-- ===========================  DECLARATIONS  =================================

CREATE TRIGGER [component].[TR_PROGRAM_AWARD_TIER_TYPE_U_1]
ON [component].[PROGRAM_AWARD_TIER_TYPE]
FOR UPDATE
AS
BEGIN

SET NOCOUNT ON

IF  UPDATE(UPDATE_ID)
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    FROM    PROGRAM_AWARD_TIER_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END
ELSE
BEGIN
    UPDATE  T1
    SET     UPDATE_DATE = GETDATE()
    ,       UPDATE_ID = USER_NAME()
    FROM    PROGRAM_AWARD_TIER_TYPE T1
    ,       INSERTED T2
    WHERE   T1.ID = T2.ID
END

RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_HISTORY_PROGRAM_AWARD_TIER] on [component].[HISTORY_PROGRAM_AWARD_TIER]'
GO
ALTER TABLE [component].[HISTORY_PROGRAM_AWARD_TIER] ADD CONSTRAINT [PK_HISTORY_PROGRAM_AWARD_TIER] PRIMARY KEY CLUSTERED  ([HISTORY_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_PROGRAM_AWARD_TIER] on [component].[PROGRAM_AWARD_TIER]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [PK_PROGRAM_AWARD_TIER] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HISTORY_PROGRAM_AWARD_TIER_2] on [component].[HISTORY_PROGRAM_AWARD_TIER]'
GO
CREATE NONCLUSTERED INDEX [IX_HISTORY_PROGRAM_AWARD_TIER_2] ON [component].[HISTORY_PROGRAM_AWARD_TIER] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[HISTORY_PROGRAM_AWARD_TIER]'
GO
ALTER TABLE [component].[HISTORY_PROGRAM_AWARD_TIER] ADD CONSTRAINT [DF_HISTORY_PROGRAM_AWARD_TIER_CHANGE_DATE] DEFAULT (getdate()) FOR [CHANGE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[PROGRAM_AWARD_TIER]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_CREATE_DATE] DEFAULT (getdate()) FOR [CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_CREATE_ID] DEFAULT (user_name()) FOR [CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_UPDATE_DATE] DEFAULT (getdate()) FOR [UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_UPDATE_ID] DEFAULT (user_name()) FOR [UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_TYPE_CREATE_DATE] DEFAULT (getdate()) FOR [CREATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_TYPE_CREATE_ID] DEFAULT (user_name()) FOR [CREATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_TYPE_UPDATE_DATE] DEFAULT (getdate()) FOR [UPDATE_DATE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [DF_PROGRAM_AWARD_TIER_TYPE_UPDATE_ID] DEFAULT (user_name()) FOR [UPDATE_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PROGRAM_AWARD_TIER]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [FK_PROGRAM_AWARD_TIER_PROGRAM] FOREIGN KEY ([PROGRAM_ID]) REFERENCES [component].[PROGRAM] ([PROGRAM_ID])
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [FK_PROGRAM_AWARD_TIER_PROGRAM_AWARD_TIER_TYPE] FOREIGN KEY ([PROGRAM_AWARD_TIER_TYPE_CODE]) REFERENCES [component].[PROGRAM_AWARD_TIER_TYPE] ([PROGRAM_AWARD_TIER_TYPE_CODE])
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [FK_PROGRAM_AWARD_TIER_STATUS_TYPE_1] FOREIGN KEY ([STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER] ADD CONSTRAINT [FK_PROGRAM_AWARD_TIER_STATUS_TYPE_2] FOREIGN KEY ([RAISING_STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[PROGRAM_AWARD_TIER_TYPE]'
GO
ALTER TABLE [component].[PROGRAM_AWARD_TIER_TYPE] ADD CONSTRAINT [FK_PROGRAM_AWARD_TIER_TYPE_STATUS_TYPE] FOREIGN KEY ([DISPLAY_STATUS_TYPE_CODE]) REFERENCES [component].[STATUS_TYPE] ([STATUS_TYPE_CODE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [component].[RECOGNITION]'
GO
ALTER TABLE [component].[RECOGNITION] ADD CONSTRAINT [FK_RECOGNITION_PROGRAM_AWARD_TIER] FOREIGN KEY ([PROGRAM_AWARD_TIER_ID]) REFERENCES [component].[PROGRAM_AWARD_TIER] ([ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [component].[TR_RECOGNITION_DU_1] on [component].[RECOGNITION]'
GO
-- ==============================  NAME  ======================================
-- TR_RECOGNITION_DU_1
-- ===========================  DESCRIPTION  ==================================
-- The purpose of this trigger is to insert an audit record into the history table
-- upon given operation.
-- 
-- ============================  REVISIONS  ===================================
-- AUTHOR        DATE        CHANGE DESCRIPTION
-- dohognta        20070207        Created
-- ericksrt     20150819        Removed UP_SECURITY_GET_USER_NAME reference
-- dohognta     20160527        Added UP_SECURITY_GET_USER_NAME reference
--
-- ===========================  DECLARATIONS  =================================

ALTER TRIGGER [component].[TR_RECOGNITION_DU_1]
ON [component].[RECOGNITION]
FOR DELETE, UPDATE
AS
BEGIN

-------------------------------------------------------------------
-- If no rows are changed, then do nothing
-------------------------------------------------------------------
IF @@ROWCOUNT = 0
    RETURN

-------------------------------------------------------------------
-- If the trigger level is greater than one, then do nothing
-------------------------------------------------------------------
IF ( (SELECT TRIGGER_NESTLEVEL() ) > 1 )
    RETURN

DECLARE @operation VARCHAR(50)
DECLARE @err INT
DECLARE @errmsg VARCHAR(255)
DECLARE @severity INT
DECLARE @rowcount INT
 
DECLARE @change_date DATETIME2
DECLARE @change_id NVARCHAR(80)
DECLARE @change_operation NVARCHAR(80)

-- ============================================================================
-- ==============================  CODE  ======================================
-- ============================================================================

-------------------------------------------------------------------
-- Determine the command (or operation) that this trigger is serving 
-------------------------------------------------------------------
IF EXISTS (SELECT TOP 1 * FROM INSERTED)
    SET @operation = 'UPDATE'
ELSE
    SET @operation = 'DELETED'

SET @change_date = GETDATE()

EXEC component.UP_SECURITY_GET_USER_NAME @change_id OUTPUT

SET @change_id = ISNULL(@change_id, USER_NAME())

SET @change_operation = LEFT(@operation, 1)

-------------------------------------------------------------------
-- Insert record into history table
-------------------------------------------------------------------
INSERT INTO component.HISTORY_RECOGNITION (
    CHANGE_DATE
,    CHANGE_ID
,    CHANGE_OPERATION
,    ID
,    NOMINATION_ID
,    RECEIVER_PAX_ID
,    GROUP_ID
,    PROGRAM_AWARD_TIER_ID
,    PARENT_ID
,    AMOUNT
,    VIEWED
,    PENDING_LEVEL
,    COMMENT_JUSTIFICATION
,    COMMENT
,    STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
)
SELECT    
    @change_date
,    @change_id
,    @change_operation
,    ID
,    NOMINATION_ID
,    RECEIVER_PAX_ID
,    GROUP_ID
,    PROGRAM_AWARD_TIER_ID
,    PARENT_ID
,    AMOUNT
,    VIEWED
,    PENDING_LEVEL
,    COMMENT_JUSTIFICATION
,    COMMENT
,    STATUS_TYPE_CODE
,    CREATE_DATE
,    CREATE_ID
,    UPDATE_DATE
,    UPDATE_ID
FROM
    DELETED

SELECT @err = @@ERROR

-------------------------------------------------------------------
-- If an error occurs then raise an error; the referenced object may not exists or some other problem exists
-------------------------------------------------------------------
IF @err <> 0
BEGIN
    SET @errmsg = '%s statement failed with TRIGGER ''' + OBJECT_NAME(@@PROCID) + '''. The failure occurred in database ''' + DB_NAME() + ''', table ''RECOGNITION''.  Execution error @@ERROR: ' + CONVERT(VARCHAR, @err) + '  The statement has been terminated.'
    SET @severity = 16 -- Indicate errors that can be corrected by the user
    GOTO RAISE_ERROR
END

RETURN

-------------------------------------------------------------------
-- Error handling
-------------------------------------------------------------------
RAISE_ERROR:

RAISERROR (@errmsg, @severity, 1, @operation)
ROLLBACK TRANSACTION
RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
    IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
    PRINT 'The database update failed'
END
GO
