DECLARE @targetId BIGINT
DECLARE @translatableID BIGINT

BEGIN
    IF NOT EXISTS (SELECT * FROM component.APPLICATION_DATA WHERE KEY_NAME = 'notification.email.subject.APR_REMINDER')
    BEGIN

    insert into component.APPLICATION_DATA (KEY_NAME,VALUE) values ('notification.email.subject.APR_REMINDER', 'REMINDER: Your Approval is Required')

    SET @translatableID = (SELECT T.ID FROM component.TRANSLATABLE T WHERE T.TABLE_NAME = 'APPLICATION_DATA' AND T.COLUMN_NAME = 'VALUE')
    SET @targetId = (SELECT AD.APPLICATION_DATA_ID FROM component.APPLICATION_DATA AD WHERE AD.KEY_NAME = 'notification.email.subject.APR_REMINDER')

insert into component.TRANSLATABLE_PHRASE (LOCALE_CODE,TRANSLATABLE_ID,TARGET_ID ,[TRANSLATION],STATUS_TYPE_CODE) values
    ('de_DE', @translatableID, @targetId, 'Erinnerung: Ihre Zustimmung ist erforderlich', 'ACTIVE'),
    ('en_GB', @translatableID, @targetId, 'Reminder: Your approval is required', 'ACTIVE'),
    ('es_ES', @translatableID, @targetId, 'Recordatorio: se requiere su aprobación', 'ACTIVE'),
    ('es_MX', @translatableID, @targetId, 'Recordatorio: se requiere su aprobación', 'ACTIVE'),
    ('fr_CA', @translatableID, @targetId, 'Rappel: votre approbation est requise', 'ACTIVE'),
    ('fr_FR', @translatableID, @targetId, 'Rappel: votre approbation est requise', 'ACTIVE'),
    ('hi_IN', @translatableID, @targetId, 'अनुस्मारक: आपकी स्वीकृति आवश्यक है', 'ACTIVE'),
    ('it_IT', @translatableID, @targetId, 'Promemoria: è richiesta la tua autorizzazione', 'ACTIVE'),
    ('ja_JP', @translatableID, @targetId, 'リマインダー：あなたの承認が必要です', 'ACTIVE'),
    ('nl_NL', @translatableID, @targetId, 'Herinnering: uw goedkeuring is vereist', 'ACTIVE'),
    ('pt_BR', @translatableID, @targetId, 'Lembrete: sua aprovação é necessária', 'ACTIVE'),
    ('pt_PT', @translatableID, @targetId, 'Lembrete: sua aprovação é necessária', 'ACTIVE'),
    ('ru_RU', @translatableID, @targetId, 'Напоминание: требуется ваше одобрение', 'ACTIVE'),
    ('zh_CN', @translatableID, @targetId, '提醒：需要您的批准', 'ACTIVE'),
    ('zh_TW', @translatableID, @targetId, '提醒：需要您的批准', 'ACTIVE')
    END
END