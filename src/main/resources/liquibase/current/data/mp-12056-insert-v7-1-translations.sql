

-- *************************** Step 1: Make sure all of the TRANSLATABLE_PHRASE entries exist *************************
DECLARE @targetId BIGINT
DECLARE @translatableID BIGINT

--Get the translatable ID for the APPLICATION_DATA table
SET @translatableID = (SELECT ID FROM component.TRANSLATABLE WHERE TABLE_NAME = 'APPLICATION_DATA' AND COLUMN_NAME = 'VALUE')

--Loop through all application data IDs and make sure an entry exists in TRANSLATABLE_PHRASE. If not, create it with null translations for now.
DECLARE id_cursor CURSOR FOR
    SELECT DISTINCT APPLICATION_DATA_ID FROM component.APPLICATION_DATA WHERE KEY_NAME IN ('notification.email.subject.MILESTONE_REMINDER_BIRTHDAY','notification.email.subject.MILESTONE_REMINDER_SERVICE_ANNIVERSARY')
            
OPEN id_cursor
    
FETCH NEXT FROM id_cursor
    INTO @targetId
    
WHILE @@FETCH_STATUS = 0
BEGIN 
    IF NOT EXISTS (SELECT * FROM component.TRANSLATABLE_PHRASE WHERE TRANSLATABLE_ID = @translatableID AND TARGET_ID = @targetId)
    BEGIN
        INSERT INTO component.TRANSLATABLE_PHRASE (LOCALE_CODE, TRANSLATABLE_ID, TARGET_ID, TRANSLATION, STATUS_TYPE_CODE) VALUES
        ('de_DE', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('en_GB', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('es_ES', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('es_MX', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('fr_CA', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('fr_FR', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('hi_IN', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('it_IT', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('ja_JP', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('nl_NL', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('pt_BR', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('pt_PT', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('ru_RU', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('zh_CN', @translatableID, @targetId, NULL, 'ACTIVE'),
        ('zh_TW', @translatableID, @targetId, NULL, 'ACTIVE')
    END

    FETCH NEXT FROM id_cursor
    INTO @targetId
END
CLOSE id_cursor;
DEALLOCATE id_cursor;

-- *************************** Step 2: Update the TRANSLATION column for each TRANSLATABLE_PHRASE entry ******************************

--Reset IDs
SET @targetId = null
SET @translatableID = null

--Get the translatable ID for the APPLICATION_DATA table
SET @translatableID = (SELECT ID FROM component.TRANSLATABLE WHERE TABLE_NAME = 'APPLICATION_DATA' AND COLUMN_NAME = 'VALUE')

-- *********** Application Data 1: notification.email.subject.MILESTONE_REMINDER_BIRTHDAY ***********
--Reset IDs
SET @targetId = null
SET @targetId = (SELECT APPLICATION_DATA_ID FROM component.APPLICATION_DATA WHERE KEY_NAME = 'notification.email.subject.MILESTONE_REMINDER_BIRTHDAY')

    --de_DE
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Geburtstagserinnerung' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'de_DE'

    --en_GB
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Birthday reminder' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'en_GB'

    --es_ES
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Recordatorio de cumpleaños' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'es_ES'

    --es_MX
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Recordatorio de cumpleaños' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'es_MX'

    --fr_CA
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Rappel d''anniversaire' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'fr_CA'

    --fr_FR
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Rappel d''anniversaire' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'fr_FR'

    --hi_IN
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'जन्मदिन अनुस्मारक' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'hi_IN'

    --it_IT
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Promemoria compleanno' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'it_IT'

    --ja_JP
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'誕生日リマインダー' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'ja_JP'

    --nl_NL
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'U Verjaardagsherinnering' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'nl_NL'

    --pt_BR
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Aniversário de nascimento' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'pt_BR'

    --pt_PT
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Lembrete de aniversário' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'pt_PT'

    --ru_RU
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Напоминание о дне рождения' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'ru_RU'

    --zh_CN
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'生日提醒' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'zh_CN'

    --zh_TW
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'生日提醒' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'zh_TW'

-- *********** Application Data 2: Repeat for all application data's if needed ***********
-- *********** Application Data 2: notification.email.subject.MILESTONE_REMINDER_BIRTHDAY ***********
--Reset IDs
SET @targetId = null
SET @targetId = (SELECT APPLICATION_DATA_ID FROM component.APPLICATION_DATA WHERE KEY_NAME = 'notification.email.subject.MILESTONE_REMINDER_SERVICE_ANNIVERSARY')

    --de_DE
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Jubiläumserinnerung' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'de_DE'

    --en_GB
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Service anniversary reminder' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'en_GB'

    --es_ES
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Recordatorio de aniversario de servicio' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'es_ES'

    --es_MX
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Recordatorio de aniversario de servicio' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'es_MX'

    --fr_CA
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Rappel d''anniversaire de service' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'fr_CA'

    --fr_FR
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Rappel d''anniversaire de service' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'fr_FR'

    --hi_IN
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'सेवा वर्षगांठ अनुस्मारक' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'hi_IN'

    --it_IT
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Promemoria anniversario di servizio' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'it_IT'

    --ja_JP
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'勤続周年記念リマインダー' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'ja_JP'

    --nl_NL
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Herinnering werkjubileum' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'nl_NL'

    --pt_BR
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Lembrete de aniversário de serviço' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'pt_BR'

    --pt_PT
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Lembrete de aniversário do serviço' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'pt_PT'

    --ru_RU
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'Напоминание о годовщине трудовой деятельности' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'ru_RU'

    --zh_CN
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'服务周年纪念提醒' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'zh_CN'

    --zh_TW
    UPDATE component.TRANSLATABLE_PHRASE SET TRANSLATION = N'服务周年纪念提醒' 
    WHERE TRANSLATABLE_ID = @translatableId AND TARGET_ID = @targetId AND LOCALE_CODE = 'zh_TW'

-- *********** Application Data 2: Repeat for all application data's if needed ***********
-- *************************** Step 3: Update any other TRANSLATABLE records if needed (CALENDAR_ENTRY, FILES) ******************************
--Don't forget to reset IDs each time!
