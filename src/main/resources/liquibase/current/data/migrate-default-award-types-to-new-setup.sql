
DECLARE @defaultAwardTypeUS NVARCHAR(255)
SET  @defaultAwardTypeUS  = (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = 'demographicUpdates.defaultAwardType.US')
            
IF @defaultAwardTypeUS IS NOT NULL
BEGIN 
            
    IF NOT EXISTS(SELECT * FROM component.LOOKUP_CATEGORY WHERE LOOKUP_CATEGORY_CODE = @defaultAwardTypeUS)
    BEGIN
        INSERT INTO component.LOOKUP_CATEGORY (LOOKUP_CATEGORY_CODE) VALUES (@defaultAwardTypeUS)
                
        --LOOKUP
        INSERT INTO component.LOOKUP (LOOKUP_TYPE_CODE, LOOKUP_CATEGORY_CODE, LOOKUP_CODE, LOOKUP_DESC) VALUES
        ('AWARD_TYPE', @defaultAwardTypeUS, 'PAYOUT_TYPE', @defaultAwardTypeUS),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'DISPLAY_NAME', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = 'projectProfile.awards.pointsName')),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'AWARD_VALUE', '1'),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'ABS', 'TRUE'),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'SUB_CLIENT', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('vendorprocessing.mars.subclientnumber.', @defaultAwardTypeUS))),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'PROJECT_NUMBER', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('demographicupdates.', @defaultAwardTypeUS, '.defaultprojectnumber'))),
        ('AWARD_TYPE', @defaultAwardTypeUS, 'SUB_PROJECT_NUMBER', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('demographicupdates.', @defaultAwardTypeUS, '.defaultsubprojectnumber')))
                         
    END
                
    --UPDATE EXISTING RECORDS TO USE demographicUpdates.defaultAwardType.US
    UPDATE component.PROGRAM_MISC
    SET MISC_DATA = @defaultAwardTypeUS, MISC_DATE = GETDATE()
    WHERE VF_NAME ='SELECTED_AWARD_TYPE'
                
    UPDATE component.PROGRAM_MISC
    SET VF_NAME ='SELECTED_AWARD_TYPE', MISC_DATA = @defaultAwardTypeUS, MISC_DATE = GETDATE()
    WHERE VF_NAME = 'PAYOUT_TYPE_POINTS' AND MISC_DATA = 'TRUE'
                
    DELETE FROM component.PROGRAM_MISC
    WHERE VF_NAME IN ('PAYOUT_TYPE_POINTS','PAYOUT_TYPE_CASH')
END

DECLARE @defaultAwardTypeNonUS NVARCHAR(255)
SET  @defaultAwardTypeNonUS  = (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = 'demographicUpdates.defaultAwardType.nonUS')
            
IF @defaultAwardTypeNonUS IS NOT NULL AND @defaultAwardTypeNonUS <> @defaultAwardTypeUS
BEGIN 
            
    IF NOT EXISTS(SELECT * FROM component.LOOKUP_CATEGORY WHERE LOOKUP_CATEGORY_CODE = @defaultAwardTypeNonUS)
    BEGIN
        INSERT INTO component.LOOKUP_CATEGORY (LOOKUP_CATEGORY_CODE) VALUES (@defaultAwardTypeNonUS)
                
        --LOOKUP
        INSERT INTO component.LOOKUP (LOOKUP_TYPE_CODE, LOOKUP_CATEGORY_CODE, LOOKUP_CODE, LOOKUP_DESC) VALUES
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'PAYOUT_TYPE', @defaultAwardTypeNonUS),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'DISPLAY_NAME', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = 'projectProfile.awards.pointsName')),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'AWARD_VALUE', '1'),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'ABS', 'TRUE'),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'SUB_CLIENT', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('vendorprocessing.mars.subclientnumber.', @defaultAwardTypeNonUS))),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'PROJECT_NUMBER', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('demographicupdates.', @defaultAwardTypeNonUS, '.defaultprojectnumber'))),
        ('AWARD_TYPE', @defaultAwardTypeNonUS, 'SUB_PROJECT_NUMBER', (SELECT VALUE_1 FROM component.APPLICATION_DATA WHERE KEY_NAME = CONCAT('demographicupdates.', @defaultAwardTypeNonUS, '.defaultsubprojectnumber')))                 
    END
END