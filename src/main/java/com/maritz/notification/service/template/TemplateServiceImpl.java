package com.maritz.notification.service.template;


import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.core.bean.Registry;

import com.maritz.notification.service.template.provider.TemplateProvider;


import avro.shaded.com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TemplateServiceImpl implements TemplateService {


	protected Registry<TemplateProvider> templateProviderRegistry;
	final Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	public TemplateServiceImpl setTemplateProviderRegistry(Registry<TemplateProvider> templateProviderRegistry) {
		this.templateProviderRegistry = templateProviderRegistry;
		return this;
	}


	@Override
	public String renderTemplate(String templateName, String localeName, Map<String,Object> variables) {
	    Locale locale = getLocale(localeName);
	    Locale defaultLocale = getLocale(null);
		for (TemplateProvider templateProvider : templateProviderRegistry.getCollection()) {
			if (templateProvider.canProvide(templateName, locale)) {
			    return templateProvider.renderTemplate(templateName, locale, variables);
			}else {
				if (templateProvider.canProvide(templateName, defaultLocale)) {
					logger.info("Could not resolve locale: " + locale + " for template: " + templateName + ", used default template language: en_US");
				    return templateProvider.renderTemplate(templateName, defaultLocale, variables);
				}
			}
		}

		throw new IllegalArgumentException("could not resolve: " + templateName);
	}

    @Override
    public String renderString(String content, Map<String, Object> variables) {
        //the first template provider is used for strings
        TemplateProvider templateProvider = Iterables.get(templateProviderRegistry.getCollection(), 0, null);
        if (templateProvider == null) throw new IllegalArgumentException("no template providers found");

        return templateProvider.renderString(content, variables);
    }

    protected Locale getLocale(String localeName) {
        if (localeName == null) return new Locale("en", "US");

        //localeName is language_country: en_US
        String[] params = localeName.split("_");
        if (params.length != 2) throw new IllegalArgumentException("Invalid locale: " + localeName);

        String language = params[0].toLowerCase();
        String country = params[1].toUpperCase();
        return new Locale(language, country);
    }

}
