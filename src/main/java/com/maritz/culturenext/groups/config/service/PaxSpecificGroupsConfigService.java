package com.maritz.culturenext.groups.config.service;

import org.springframework.http.ResponseEntity;

import com.maritz.culturenext.groups.config.dto.PaxSpecificGroupsConfigDTO;

public interface PaxSpecificGroupsConfigService {
    
    /**
     * Get the group specific configuration for the logged in pax. Currently only returns colors and logoID.
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/82214942/GET+Group+specific+config+for+pax
     */
    public PaxSpecificGroupsConfigDTO getPaxSpecificGroupsConfig(Long paxId);
    
    /**
     * This method retrieves the pax logo image file from the db
     *
     * @param image The image file from the db
     * @param paxId The pax id 
     * @return
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/93126678/GET+Group+specific+logo+for+pax
     */
    ResponseEntity<Object> getPaxSpecificGroupsLogo(Long paxId);

    /**
     * This method retrieves the pax hero image file from the db
     *
     * @param image The image file from the db
     * @param paxId The pax id
     * @return
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/138608641/GET+Group+specific+hero+image+for+pax
     */
    ResponseEntity<Object> getPaxSpecificGroupsHeroImage(Long paxId);
}
