package com.maritz.culturenext.groups.config.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.security.annotation.SecurityPolicy;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.groups.config.service.GroupConfigService;
import com.maritz.culturenext.permission.constants.PermissionConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("groups")
@Api(value = "/config", description = "all calls dealing with groups config")

public class GroupsConfigRestService {
    @Inject private GroupConfigService groupConfigService; 

    //rest/groups/~/config
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')")
    @RequestMapping(method = RequestMethod.POST, value = "{groupId}/config")
    @ApiOperation(value="Create group config", notes="This method allows a user to create group config")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful creation of group config"), 
    @ApiResponse(code = 404, message = "Group Id not found") })
    @Permission("PUBLIC")
    public GroupConfigDTO createGroupConfig(
            @ApiParam(value = "Group Id for the new group config") @PathVariable(GROUP_ID_REST_PARAM) Long groupId,
            @RequestBody GroupConfigDTO groupsConfigRequestDto
        ) throws Throwable {
        
        return groupConfigService.createUpdateGroupConfig(groupId, groupsConfigRequestDto);
    }
    
    //rest/groups/~/config
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')")
    @RequestMapping(method = { RequestMethod.PUT, RequestMethod.PATCH }, value = "{groupId}/config")
    @ApiOperation(value="Update group config", notes="This method allows a user to update group config")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of group config"), 
            @ApiResponse(code = 404, message = "Group Id not found") })
    @Permission("PUBLIC")
    public GroupConfigDTO updateGroupConfig(
            @ApiParam(value = "Update group config record") @PathVariable(GROUP_ID_REST_PARAM) Long groupId,
            @RequestBody GroupConfigDTO groupsConfigRequestDto
        ) throws Throwable {

        return groupConfigService.createUpdateGroupConfig(groupId, groupsConfigRequestDto);
    }
    
    //rest/groups/config
    @RequestMapping(method = RequestMethod.GET, value = "/config")
    @ApiOperation(value="Get all the groups", notes="This method allows a user to get all groups")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrive of all groups"), 
            @ApiResponse(code = 404, message = "Groups not found") })
    @Permission("PUBLIC")
    public List<GroupConfigDTO> getAllGroupConfig() {
        
        return groupConfigService.getAllGroupConfig();
    }

    //rest/groups/~/config/logo
    @RequestMapping(method = RequestMethod.GET, value = "{groupId}/config/logo")
    @ApiOperation(value="Get group specific logo", notes="This method allows a user to get group specific logo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrive of group specific logo"),
            @ApiResponse(code = 404, message = "Group specific logo not found") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getGroupSpecificLogo(
        @ApiParam(value = "Group Id to retrieve group specific logo", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId 
        ){    

        return groupConfigService.getGroupSpecificLogo(groupId);
    }

    //rest/groups/~/config/logo
    @RequestMapping(method = RequestMethod.POST, value = "{groupId}/config/logo")
    @ApiOperation(value="Create group specific logo", notes="This method allows a user to create group specific logo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful create group specific logo"),
            @ApiResponse(code = 404, message = "Group specific logo not found") })
    @Permission("PUBLIC")
    public IdDTO createGroupSpecificLogo(
        @ApiParam(value = "Group Id to create group specific logo", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId,
        @ApiParam(value = "Target file to upload.") @RequestParam(value = IMAGE_REST_PARAM, required = true) MultipartFile logo
        ) throws Throwable {
        
        return groupConfigService.createGroupSpecificLogo(groupId, logo);
    }
    
    //rest/groups/~/config/logo
    @RequestMapping(method = RequestMethod.DELETE, value = "{groupId}/config/logo")
    @ApiOperation(value = "Delete group specific logo", notes = " This method allows the deletion of a group specific logo")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted group specific logo") })
    @Permission("PUBLIC")
    public void deleteGroupSpecificLogo (
        @ApiParam(value = "Group Id of logo we want to delete", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId
        ) throws Throwable {

        groupConfigService.deleteGroupImage(groupId, FileTypes.GROUP_LOGO.name());
    }

    //rest/groups/~/config/hero
    @RequestMapping(method = RequestMethod.GET, value = "{groupId}/config/hero")
    @ApiOperation(value="Get group specific hero image", notes="This method allows a user to get group specific hero image")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrive of group specific hero image"),
            @ApiResponse(code = 404, message = "Group specific hero image not found") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getGroupSpecificHeroImage(
        @ApiParam(value = "Group Id to retrieve group specific hero image", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId 
        ){    

        return groupConfigService.getGroupSpecificHeroImage(groupId);
    }

    //rest/groups/~/config/hero
    @RequestMapping(method = RequestMethod.POST, value = "{groupId}/config/hero")
    @ApiOperation(value="Create group specific hero image", notes="This method allows a user to create group specific hero image")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful create group specific hero image"), 
            @ApiResponse(code = 404, message = "Group specific hero image not found") })
    @Permission("PUBLIC")
    public IdDTO createGroupSpecificHeroImage(
        @ApiParam(value = "Group Id to create group specific hero image", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId,
        @ApiParam(value = "Target file to upload.") @RequestParam(value = IMAGE_REST_PARAM, required = true) MultipartFile heroImage
        ) throws Throwable {
        
        return groupConfigService.createGroupSpecificHeroImage(groupId, heroImage);
    }
    
    //rest/groups/~/config/hero
    @RequestMapping(method = RequestMethod.DELETE, value = "{groupId}/config/hero")
    @ApiOperation(value = "Delete group specific hero image", notes = " This method allows the deletion of a group specific hero image")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully deleted group specific hero image") })
    @Permission("PUBLIC")
    public void deleteGroupSpecificHeroImage (
        @ApiParam(value = "Group Id of hero image we want to delete", required = true) @PathVariable(GROUP_ID_REST_PARAM) Long groupId
        ) throws Throwable {
            
        groupConfigService.deleteGroupImage(groupId, FileTypes.GROUP_HERO_IMAGE.name());
    }
}
