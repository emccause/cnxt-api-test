package com.maritz.culturenext.groups.config.constants;

import com.maritz.core.rest.ErrorMessage;
public class GroupConfigConstants {

    public static final String GROUP_ID = "groupId";
    public static final String HERO_IMAGE_ID = "heroImageId";
    public static final String LOGO_ID = "logoId";
    public static final String PRIMARY_COLOR = "primaryColor";
    public static final String SECONDARY_COLOR = "secondaryColor";
    public static final String TEXT_ON_PRIMARY_COLOR = "textOnPrimaryColor";
    public static final String JSON_FILE = "jsonFile";
    
    // Error message strings
    public static final String ERROR_MISSING_GROUP_ID = "ERROR_MISSING_GROUP_ID";
    public static final String ERROR_MISSING_GROUP_ID_MSG = "Missing Group Id";
    public static final String ERROR_GROUP_IDS_DONT_MATCH = "ERROR_GROUP_IDS_DONT_MATCH";
    public static final String ERROR_GROUP_IDS_DONT_MATCH_MSG = "Group Id´s don´t match";
    public static final String ERROR_MISSING_JSON_FILE = "ERROR_MISSING_JSON_FILE";
    public static final String ERROR_MISSING_JSON_FILE_MSG = "Missing Json File";        
    
    public static final String ERROR_QUERY_GET_ALL_GROUPS_CONFIGS = "The query to get all groups config has failed: ";
    public static final String ERROR_QUERY_GET_ONE_GROUP_CONFIG = "The query to get one group config has failed: ";
    public static final String ERROR_QUERY_INSERT_UPDATES_GROUPS_CONFIGS = "The query to insert/update groups configs has failed:";
            
    public static final ErrorMessage MISSING_JSON_FILE = new ErrorMessage()
            .setCode(ERROR_MISSING_JSON_FILE)
            .setMessage(ERROR_MISSING_JSON_FILE_MSG)
            .setField(JSON_FILE);
    
    public static final ErrorMessage MISSING_GROUP_ID = new ErrorMessage()
            .setCode(ERROR_MISSING_GROUP_ID)
            .setMessage(ERROR_MISSING_GROUP_ID_MSG)
            .setField(GROUP_ID);
    
    public static final ErrorMessage GROUP_IDS_DONT_MATCH = new ErrorMessage()
            .setCode(ERROR_GROUP_IDS_DONT_MATCH)
            .setMessage(ERROR_GROUP_IDS_DONT_MATCH_MSG)
            .setField(GROUP_ID);
}
