package com.maritz.culturenext.groups.config.dto;

import com.maritz.culturenext.project.dto.ColorsProfileDTO;

public class GroupConfigDTO {

    private Long groupId;
    private ColorsProfileDTO colors;
    private Long logoId;
    private Long heroImageId;

    public Long getGroupId() {
        return groupId;
    }
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    public ColorsProfileDTO getColors() {
        return colors;
    }
    public void setColors(ColorsProfileDTO colors) {
        this.colors = colors;
    }
    public Long getLogoId() {
        return logoId;
    }
    public void setLogoId(Long logoId) {
        this.logoId = logoId;
    }
    public Long getHeroImageId() {
        return heroImageId;
    }
    public void setHeroImageId(Long heroImageId) {
        this.heroImageId = heroImageId;
    }
}
