package com.maritz.culturenext.groups.config.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.enums.FileItemTypeEnum;
import com.maritz.culturenext.groups.config.dao.PaxSpecificGroupsConfigDao;

@Component
public class PaxSpecificGroupsConfigDaoImpl extends AbstractDaoImpl implements PaxSpecificGroupsConfigDao {

    private static final String PAX_ID_PARAM = "PAX_ID";
    private static final String KEY_NAME_LIST_PARAM = "KEY_NAME_LIST";
    private static final String QUERY = "SELECT " +
            "GM.VF_NAME AS KEYNAME, " +
            "MIN(GM.MISC_DATA) AS VALUE, " +
            "COUNT(GM.MISC_DATA) AS NUMBER " +
            "FROM component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
            "INNER JOIN component.GROUPS_MISC GM " +
            "ON GTM.group_id = GM.GROUP_ID " +
            "WHERE GTM.pax_id = :" + PAX_ID_PARAM + " " +
            "AND GM.VF_NAME IN (:" + KEY_NAME_LIST_PARAM + ") " +
            "GROUP BY GM.VF_NAME ";

    private static final String PAX_ID_PLACEHOLDER = "PAX_ID_PLACEHOLDER";
    private static final String QUERY_GET_PAX_SPECIFIC_LOGO =
            "SELECT F.* FROM COMPONENT.FILE_ITEM FI  " +
            "INNER JOIN COMPONENT.FILES F ON F.ID=FI.FILES_ID  " +
            "WHERE FI.TARGET_TABLE='" + FileItemTypeEnum.GROUPS.name() + "' " +
            "AND FI.TARGET_ID IN ( " +
                "SELECT group_id " +
                "FROM component.VW_GROUP_TOTAL_MEMBERSHIP " +
                "WHERE PAX_ID=:" + PAX_ID_PLACEHOLDER +
            " ) " +
            "AND FILE_TYPE_CODE = 'GROUP_LOGO'";

    private static final String QUERY_GET_PAX_SPECIFIC_HERO_IMAGE =
            "SELECT F.* FROM COMPONENT.FILE_ITEM FI  " +
            "INNER JOIN COMPONENT.FILES F ON F.ID=FI.FILES_ID  " +
            "WHERE FI.TARGET_TABLE='" + FileItemTypeEnum.GROUPS.name() + "' " +
            "AND FI.TARGET_ID IN ( " +
                "SELECT group_id " +
                "FROM component.VW_GROUP_TOTAL_MEMBERSHIP " +
                "WHERE PAX_ID=:" + PAX_ID_PLACEHOLDER +
            " ) " +
            "AND FILE_TYPE_CODE = 'GROUP_HERO_IMAGE'";
    
    @Override
    public List<Map<String, Object>> getGroupBasedConfiguration(Long paxId, List<String> keyNameList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        
        params.addValue(PAX_ID_PARAM, paxId);
        params.addValue(KEY_NAME_LIST_PARAM, keyNameList);
        
        return namedParameterJdbcTemplate.query(QUERY, params, new ColumnMapRowMapper());
    }

    @Override
    public Files getPaxSpecificLogo(Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PLACEHOLDER, paxId);

        List<Files> nodes = namedParameterJdbcTemplate.query(QUERY_GET_PAX_SPECIFIC_LOGO, params,
                new ConcentrixBeanRowMapper<Files>()
                        .setBeanClass(Files.class)
                        .setTable(jitBuilder.getTable(Files.class))
                        .build());
        //If count == 1 then return the image, else return a 404
        if(nodes != null && !nodes.isEmpty() && nodes.size()==1){
            return nodes.get(0);
        }

        return null;
    }

    @Override
    public Files getPaxSpecificHeroImage(Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PLACEHOLDER, paxId);

        List<Files> nodes = namedParameterJdbcTemplate.query(QUERY_GET_PAX_SPECIFIC_HERO_IMAGE, params,
                new ConcentrixBeanRowMapper<Files>()
                        .setBeanClass(Files.class)
                        .setTable(jitBuilder.getTable(Files.class))
                        .build());
        //If count == 1 then return the image, else return a 404
        if(nodes != null && !nodes.isEmpty() && nodes.size()==1){
            return nodes.get(0);
        }
        
        return null;
    }
}
