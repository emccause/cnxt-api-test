package com.maritz.culturenext.groups.config.service.impl;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.culturenext.images.service.FileImageService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.groups.config.constants.PaxSpecificGroupsConfigConstants;
import com.maritz.culturenext.groups.config.dao.PaxSpecificGroupsConfigDao;
import com.maritz.culturenext.groups.config.dto.PaxSpecificGroupsConfigDTO;
import com.maritz.culturenext.groups.config.service.PaxSpecificGroupsConfigService;
import com.maritz.culturenext.project.dto.ColorsProfileDTO;

@Service
public class PaxSpecificGroupsConfigServiceImpl implements PaxSpecificGroupsConfigService {

    @Inject private PaxSpecificGroupsConfigDao paxSpecificGroupsConfigDao;
    @Inject private FileImageService fileImageService;

    @Override
    public PaxSpecificGroupsConfigDTO getPaxSpecificGroupsConfig(Long paxId) {
        List<Map<String, Object>> configs = paxSpecificGroupsConfigDao.getGroupBasedConfiguration(paxId, ProjectConstants.GROUP_SPECIFIC_KEY_LIST);

        PaxSpecificGroupsConfigDTO paxSpecificGroupsConfigObj = new PaxSpecificGroupsConfigDTO();

        Files groupLogo = paxSpecificGroupsConfigDao.getPaxSpecificLogo(paxId);

        if ( groupLogo != null ) {
            paxSpecificGroupsConfigObj.setLogoId(groupLogo.getId());
        }

        Files groupHeroImage = paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(paxId);

        if ( groupHeroImage != null) {
            paxSpecificGroupsConfigObj.setHeroImageId(groupHeroImage.getId());
        }
        
        String primaryColor = null;
        String secondaryColor = null;
        String textOnPrimaryColor = null;

        if (!CollectionUtils.isEmpty(configs)) {
            for (Map<String, Object> node : configs) {
                String keyName = (String) node.get(PaxSpecificGroupsConfigConstants.KEYNAME),
                        value = (String) node.get(PaxSpecificGroupsConfigConstants.VALUE);

                if ((Integer) node.get(PaxSpecificGroupsConfigConstants.NUMBER) == 1 && keyName != null) {
                    if (keyName.equals(ProjectConstants.PRIMARY_COLOR_KEY)) {
                        primaryColor = value;
                    } else if (keyName.equals(ProjectConstants.SECONDARY_COLOR_KEY)) {
                        secondaryColor = value;
                    } else if (keyName.equals(ProjectConstants.TEXT_ON_PRIMARY_COLOR_KEY)) {
                        textOnPrimaryColor = value;
                    }
                } else {
                    return paxSpecificGroupsConfigObj;
                }
            }

            ColorsProfileDTO colors = new ColorsProfileDTO();
            colors.setPrimaryColor(primaryColor);
            colors.setSecondaryColor(secondaryColor);
            colors.setTextOnPrimaryColor(textOnPrimaryColor);
            paxSpecificGroupsConfigObj.setColors(colors);

            return paxSpecificGroupsConfigObj;
        } else {
            return paxSpecificGroupsConfigObj;
        }
    }

    @Override
    public ResponseEntity<Object> getPaxSpecificGroupsLogo(Long paxId) {

        Files groupLogo = paxSpecificGroupsConfigDao.getPaxSpecificLogo(paxId);
        if ( groupLogo == null ) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return fileImageService.prepareImageForResponse(paxId, groupLogo);
    }

    /**
     * This method retrieves the pax hero image file from the db and returns it
     *
     * //@param groupId The group id
     * @return Hero Image
     */
    @Override
    public ResponseEntity<Object> getPaxSpecificGroupsHeroImage(Long paxId) {
        Files groupHeroImage = paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(paxId);
        return fileImageService.prepareImageForResponse(paxId, groupHeroImage);
    }
}
