package com.maritz.culturenext.groups.config.dao;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Files;

public interface PaxSpecificGroupsConfigDao {
    List<Map<String, Object>> getGroupBasedConfiguration(Long paxId, List<String> keyNameList);
    
    /**
     * Gets pax specific logo using the group id.
     * If the pax has one logo, then return the image, else return null
     * 
     * @param groupId
     * @return Files with the logo
     */
    Files getPaxSpecificLogo(Long groupId);

    /**
     * Gets pax specific hero image using the group id.
     * If the pax has one hero image, then return the image, else return null
     *
     * @param groupId
     * @return Files with the hero image
     */
    Files getPaxSpecificHeroImage(Long groupId);
}
