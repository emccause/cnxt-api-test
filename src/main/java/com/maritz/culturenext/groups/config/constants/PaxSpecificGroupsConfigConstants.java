package com.maritz.culturenext.groups.config.constants;

public class PaxSpecificGroupsConfigConstants {
    public static final String KEYNAME = "KEYNAME";
    public static final String VALUE = "VALUE";
    public static final String NUMBER = "NUMBER";
}