package com.maritz.culturenext.groups.config.dto;

import com.maritz.culturenext.project.dto.ColorsProfileDTO;

public class PaxSpecificGroupsConfigDTO {
    private ColorsProfileDTO colors;
    private Long logoId;
    private Long heroImageId;

    public Long getLogoId() {
        return logoId;
    }

    public void setLogoId(Long logoId) {
        this.logoId = logoId;
    }

    public Long getHeroImageId() {
        return heroImageId;
    }

    public void setHeroImageId(Long imageId) {
        this.heroImageId = imageId;
    }

    public ColorsProfileDTO getColors() {
        return colors;
    }

    public void setColors(ColorsProfileDTO colors) {
        this.colors = colors;
    }
}
