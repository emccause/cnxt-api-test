package com.maritz.culturenext.groups.config.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.dto.IdDTO;

public interface GroupConfigService {

    /**
     * Validate and then create/update the group config
     * 
     * @param groupId Id of the group
     * @param groupConfigDTO Object to update with
     * @return GroupConfigDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/82214952/Create+group+config
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/82214959/Update+group+config
     */
    GroupConfigDTO createUpdateGroupConfig(Long groupId, GroupConfigDTO groupConfigDTO);
    
    /**
     * Return all group config objects
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/82673668/GET+all+group+configs
     */
    List<GroupConfigDTO> getAllGroupConfig();
    
    /**
     * This method retrieves the group logo image file from the db
     *
     * @param image The image file from the db
     * @param groupId The group id 
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/82673776/GET+group+specific+logo
     */
    ResponseEntity<Object> getGroupSpecificLogo(Long groupId);
    
    /**
     * This method save the group logo image file to db and returns the File Id in IdDTO
     *
     * @param image The image file to save
     * @param groupId The group id 
     * @return IdDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/83558407/Create+group+specific+logo
     */
    IdDTO createGroupSpecificLogo(Long groupId,MultipartFile image);
    
    /**
     * Return group specific hero image for given group
     * 
     * @param groupId Id of the group
     * @return ResponseEntity&lt;Object&gt;
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/142704644/GET+Group+Specific+Hero+Image
     */
    ResponseEntity<Object> getGroupSpecificHeroImage(Long groupId);
    
    /**
     * Save a group specific hero image and return the file id
     * 
     * @param groupId Id of the group
     * @param image Image file
     * @return IdDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/142835716/Create+Group+Specific+Hero+Image
     */
    IdDTO createGroupSpecificHeroImage(Long groupId, MultipartFile image);
    
    /**
     * Delete the file and file item for a given group and file type
     * 
     * @param groupId - ID of group to delete file for
     * @param fileType - FileType we want to delete
     * @return void
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/142901252/DELETE+Group+Specific+Hero+Image
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/142966796/DELETE+Group+Specific+Logo
     */
    void deleteGroupImage(Long groupId, String fileType);
}
