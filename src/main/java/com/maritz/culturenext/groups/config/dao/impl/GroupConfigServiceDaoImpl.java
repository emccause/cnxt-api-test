package com.maritz.culturenext.groups.config.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.groups.config.constants.GroupConfigConstants;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.project.dto.ColorsProfileDTO;

@Repository
public class GroupConfigServiceDaoImpl  extends AbstractDaoImpl implements GroupConfigServiceDao {

    private static final String FILE_TYPE_PLACEHOLDER = "FILE_TYPE_PLACEHOLDER";
    private static final String GROUP_ID_PLACEHOLDER = "GROUP_ID_PLACEHOLDER";
    private static final String PRIMARY_COLOR_PLACEHOLDER = "PRIMARY_COLOR_PLACEHOLDER";
    private static final String SECONDARY_COLOR_PLACEHOLDER = "SECONDARY_COLOR_PLACEHOLDER";
    private static final String TEXT_ON_PRIMARY_COLOR_PLACEHOLDER = "TEXT_ON_PRIMARY_COLOR_PLACEHOLDER";
    
    private static final String QUERY_GET_ALL_GROUPS_CONFIGS = 
        "SELECT g.GROUP_ID, COLORS.PRIMARY_COLOR, COLORS.SECONDARY_COLOR, COLORS.TEXT_ON_PRIMARY_COLOR, " +
        "(SELECT fi.FILES_ID " +
            "FROM component.FILE_ITEM fi " +  
            "JOIN component.FILES f " +
                "ON f.ID = fi.FILES_ID " + 
            "WHERE fi.TARGET_TABLE = 'GROUPS' " + 
                "AND fi.TARGET_ID = g.GROUP_ID " +
                "AND f.FILE_TYPE_CODE = 'GROUP_LOGO') AS 'LOGO_ID', " +
        "(SELECT fi.FILES_ID " +
            "FROM component.FILE_ITEM fi " +   
            "JOIN component.FILES f " +
                "ON f.ID = fi.FILES_ID " + 
            "WHERE fi.TARGET_TABLE = 'GROUPS' " + 
                "AND fi.TARGET_ID = g.GROUP_ID " +
                "AND f.FILE_TYPE_CODE = 'GROUP_HERO_IMAGE') AS 'HERO_IMAGE_ID'" +
        "FROM component.GROUPS g " +
        "LEFT JOIN ( " +
            "SELECT GROUP_ID,PRIMARY_COLOR,SECONDARY_COLOR,TEXT_ON_PRIMARY_COLOR " +
            "FROM " +
            "(SELECT GROUP_ID,VF_NAME,MISC_DATA " +
            "FROM component.GROUPS_MISC) AS GROUPS_MISC_TO_BE_PIVOTED " +  
            "PIVOT " +   
            "( " +
            "MAX(MISC_DATA) " +
            "FOR VF_NAME IN (PRIMARY_COLOR, SECONDARY_COLOR, TEXT_ON_PRIMARY_COLOR) " +
            ") AS  GROUPS_MISC_PIVOTED " +
        ") COLORS ON COLORS.GROUP_ID = g.GROUP_ID " +
        "LEFT JOIN component.FILE_ITEM fi ON fi.TARGET_ID = g.GROUP_ID AND fi.TARGET_TABLE = 'GROUPS' " +
        "WHERE ( " +
            "COLORS.PRIMARY_COLOR IS NOT NULL " +
            "OR COLORS.SECONDARY_COLOR IS NOT NULL " +
            "OR COLORS.TEXT_ON_PRIMARY_COLOR IS NOT NULL " +
            "OR fi.FILES_ID IS NOT NULL " +
        ")";
    
    private static final String QUERY_GET_ONE_GROUP_CONFIG = 
        "SELECT * FROM ( " + QUERY_GET_ALL_GROUPS_CONFIGS  + " ) CONFIGS WHERE CONFIGS.GROUP_ID = :" + GROUP_ID_PLACEHOLDER ;
    
    private static final String QUERY_INSERT_UPDATES_GROUPS_CONFIGS = 
        "IF NOT EXISTS ( " +
        "        SELECT * FROM component.GROUPS_MISC WHERE  " +
        "        GROUP_ID= :" + GROUP_ID_PLACEHOLDER + " AND VF_NAME='PRIMARY_COLOR' " +
        ") " +
        "BEGIN " +
        "    INSERT INTO component.GROUPS_MISC (GROUP_ID,VF_NAME,MISC_DATA,MISC_DATE) " +
        "    VALUES (:" + GROUP_ID_PLACEHOLDER + ",'PRIMARY_COLOR', :" + PRIMARY_COLOR_PLACEHOLDER + ",GETDATE()) " +
        "END " +
        "ELSE " +
        "BEGIN " +
        "    UPDATE  component.GROUPS_MISC " +
        "    SET MISC_DATA= :" + PRIMARY_COLOR_PLACEHOLDER + ", " +
        "    MISC_DATE= GETDATE() " +
        "    WHERE GROUP_ID=:" + GROUP_ID_PLACEHOLDER +" " + 
        "    AND VF_NAME='PRIMARY_COLOR' " +
        "END " +
        
        "IF NOT EXISTS ( " +
        "        SELECT * FROM component.GROUPS_MISC WHERE " +
        "        GROUP_ID= :" + GROUP_ID_PLACEHOLDER + " AND VF_NAME='SECONDARY_COLOR' " +
        ") " +
        "BEGIN " +
        "    INSERT INTO component.GROUPS_MISC (GROUP_ID,VF_NAME,MISC_DATA,MISC_DATE) " +
        "    VALUES (:" + GROUP_ID_PLACEHOLDER + ",'SECONDARY_COLOR', :" + SECONDARY_COLOR_PLACEHOLDER + ",GETDATE()) " +
        "END " +
        "ELSE " +
        "BEGIN " +
        "    UPDATE component.GROUPS_MISC " +
        "    SET MISC_DATA= :" + SECONDARY_COLOR_PLACEHOLDER + ", " +
        "    MISC_DATE= GETDATE() " +
        "    WHERE GROUP_ID=:" + GROUP_ID_PLACEHOLDER + 
        "    AND VF_NAME='SECONDARY_COLOR' " +
        "END " +
        
        "IF NOT EXISTS ( " +
        "        SELECT * FROM component.GROUPS_MISC WHERE " +
        "        GROUP_ID=:" + GROUP_ID_PLACEHOLDER + " AND VF_NAME='TEXT_ON_PRIMARY_COLOR' " +
        ") " +
        "BEGIN " +
        "    INSERT INTO component.GROUPS_MISC (GROUP_ID,VF_NAME,MISC_DATA,MISC_DATE) " + 
        "    VALUES (:" + GROUP_ID_PLACEHOLDER + ",'TEXT_ON_PRIMARY_COLOR',:" + TEXT_ON_PRIMARY_COLOR_PLACEHOLDER + ",GETDATE()) " +
        "END "+
        "ELSE " +
        "BEGIN " +
        "    UPDATE  component.GROUPS_MISC " +
        "    SET MISC_DATA=:" + TEXT_ON_PRIMARY_COLOR_PLACEHOLDER + ", " +
        "    MISC_DATE= GETDATE() " +
        "    WHERE GROUP_ID=:" + GROUP_ID_PLACEHOLDER +" " + 
        "    AND VF_NAME='TEXT_ON_PRIMARY_COLOR' " +
        "END " ;
    
    private static final String QUERY_GET_GROUP_SPECIFIC_FILE =
            "SELECT F.* " +
            "FROM COMPONENT.FILE_ITEM FI " +
            "INNER JOIN COMPONENT.FILES F " +
            "ON F.ID=FI.FILES_ID " + 
            "WHERE FI.TARGET_TABLE='" + TableName.GROUPS.name() + "' " +
            "AND FI.TARGET_ID=:" + GROUP_ID_PLACEHOLDER + " " +
            "AND F.FILE_TYPE_CODE = :" + FILE_TYPE_PLACEHOLDER;

    private static final String DELETE_GROUP_SPECIFIC_FILE =
            "DECLARE @FILE_ID BIGINT; " +
    
            "SELECT " + 
                "@FILE_ID = FILES.ID " + 
            "FROM component.FILE_ITEM " + 
            "INNER JOIN component.FILES " + 
                "ON FILE_ITEM.FILES_ID = FILES.ID " + 
            "WHERE FILE_ITEM.TARGET_TABLE = '" + TableName.GROUPS.name() + "' " + 
                "AND FILE_ITEM.TARGET_ID = :" + GROUP_ID_PLACEHOLDER + " " + 
                "AND FILES.FILE_TYPE_CODE = :" + FILE_TYPE_PLACEHOLDER + "; " +
                
            "DELETE FROM component.FILE_ITEM WHERE FILES_ID = @FILE_ID; " + 
            "DELETE FROM component.FILES WHERE ID = @FILE_ID;";
    
    private static final String UPDATE_RIDEAU_STATUS_QUERY = "EXEC component.UP_ENABLE_DISABLE_CARD";
    
    @Override
    public List<GroupConfigDTO> getAllGroupConfig() {

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(
                QUERY_GET_ALL_GROUPS_CONFIGS, new CamelCaseMapRowMapper());

        ArrayList<GroupConfigDTO> resultsList = new ArrayList<GroupConfigDTO>();
        for (Map<String, Object> node : nodes) {

            GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
            Long groupId = (Long) node.get(GroupConfigConstants.GROUP_ID);
            groupConfigDTO.setGroupId(groupId);
            groupConfigDTO.setColors(new ColorsProfileDTO(node));
            if (node.get(GroupConfigConstants.LOGO_ID) != null) {
                groupConfigDTO.setLogoId((Long) node.get(GroupConfigConstants.LOGO_ID));
            }
            if (node.get(GroupConfigConstants.HERO_IMAGE_ID) != null) {
                groupConfigDTO.setHeroImageId((Long) node.get(GroupConfigConstants.HERO_IMAGE_ID));
            }
            resultsList.add(groupConfigDTO);
        }

        return resultsList;
    }
    
    @Override
    public GroupConfigDTO createUpdateGroupConfig(GroupConfigDTO groupConfigDTO) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_PLACEHOLDER, groupConfigDTO.getGroupId());
        params.addValue(PRIMARY_COLOR_PLACEHOLDER, 
                groupConfigDTO.getColors() != null ? groupConfigDTO.getColors().getPrimaryColor() : null);
        params.addValue(SECONDARY_COLOR_PLACEHOLDER, 
                groupConfigDTO.getColors() != null ? groupConfigDTO.getColors().getSecondaryColor() : null);
        params.addValue(TEXT_ON_PRIMARY_COLOR_PLACEHOLDER, 
                groupConfigDTO.getColors() != null ? groupConfigDTO.getColors().getTextOnPrimaryColor() : null);
        
        namedParameterJdbcTemplate.update(QUERY_INSERT_UPDATES_GROUPS_CONFIGS, params);
        
        return getOneGroupConfig(groupConfigDTO.getGroupId());
    }

    
    private GroupConfigDTO getOneGroupConfig(Long groupId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(
                QUERY_GET_ONE_GROUP_CONFIG, params, new CamelCaseMapRowMapper());

        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        if (!nodes.isEmpty()) {
            groupConfigDTO.setGroupId((Long)nodes.get(0).get(GroupConfigConstants.GROUP_ID));
            groupConfigDTO.setColors(new ColorsProfileDTO(nodes.get(0)));
        }
        
        return groupConfigDTO;
    }
    
    @Override
    public void updateRideauAccountStatus() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(UPDATE_RIDEAU_STATUS_QUERY, params);
    }

    @Override
    public Files getGroupSpecificFile(Long groupId, String fileType) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(FILE_TYPE_PLACEHOLDER, fileType);
        
        List<Files> nodes = namedParameterJdbcTemplate.query(QUERY_GET_GROUP_SPECIFIC_FILE, params, 
                new ConcentrixBeanRowMapper<Files>()
                        .setBeanClass(Files.class)
                        .setTable(jitBuilder.getTable(Files.class))
                        .build());
        
        if (!nodes.isEmpty()) {
            return nodes.get(0);
        }
        
        return null;
    }
    
    @Override
    public void deleteGroupSpecificFile(Long groupId, String fileType) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(FILE_TYPE_PLACEHOLDER, fileType);
        
        namedParameterJdbcTemplate.update(DELETE_GROUP_SPECIFIC_FILE, params);        
    }
}
