package com.maritz.culturenext.groups.config.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.enums.FileItemTypeEnum;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.groups.config.constants.GroupConfigConstants;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.groups.config.service.GroupConfigService;
import com.maritz.culturenext.images.constants.ImageConstants;

import static com.maritz.culturenext.images.constants.FileImageTypes.GROUP_HERO_IMAGE;
import static com.maritz.culturenext.images.constants.FileImageTypes.GROUP_LOGO;

@Component
public class GroupConfigServiceImpl implements GroupConfigService {
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private GroupConfigServiceDao groupConfigServiceDao;
    @Inject private FileImageService fileImageService;
    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;
    private FilesDao filesDao;

    @Inject
    public GroupConfigServiceImpl setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public GroupConfigServiceImpl setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    @Inject
    public GroupConfigServiceImpl setFilesDao(FilesDao filesDao) {
        this.filesDao = filesDao;
        return this;
    }

    @Override
    public GroupConfigDTO createUpdateGroupConfig(Long groupId, GroupConfigDTO groupConfigDTO) {

        validateGroupData(groupId,groupConfigDTO);

        //Handle the rest of the creates/updates for GROUPS_MISC
        return groupConfigServiceDao.createUpdateGroupConfig(groupConfigDTO);
    }

    @Override
    public List<GroupConfigDTO> getAllGroupConfig() {
        return groupConfigServiceDao.getAllGroupConfig();
    }

    private void validateGroupData(Long groupId, GroupConfigDTO groupConfigDto) {
        if (groupId == null || groupConfigDto.getGroupId() == null) {
            throw new ErrorMessageException().addErrorMessage(GroupConfigConstants.MISSING_GROUP_ID);
        }

        if (!groupConfigDto.getGroupId().equals(groupId)) {
            throw new ErrorMessageException().addErrorMessage(GroupConfigConstants.GROUP_IDS_DONT_MATCH);
        }
    }

    @Override
    public ResponseEntity<Object> getGroupSpecificLogo(Long groupId) {

        Files groupLogo = groupConfigServiceDao.getGroupSpecificFile(groupId, FileTypes.GROUP_LOGO.name());

        if (groupLogo == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return fileImageService.prepareImageForResponse(groupId, groupLogo);
    }

    @Transactional
    @Override
    public IdDTO createGroupSpecificLogo(Long groupId, MultipartFile image) {
        return createGroupImage(groupId, image, GROUP_LOGO);
    }

    private IdDTO createGroupImage(long groupId, MultipartFile image, FileImageTypes fileImageType) {
        Files file = filesDao.findOneByFileImageTypeAndTargetId(fileImageType, groupId);
        FileItem fileItem;
        if (file != null) {
            fileItem = filesDao.findOneFileItemByFileImageTypeAndTargetIdAndFile(fileImageType, groupId, file);
        } else {
            file = new Files();
            file.setFileTypeCode(fileImageType.name());

            fileItem = new FileItem();
        }
        file.setName(image.getName());
        file.setMediaType(image.getContentType());
        filesDao.save(file);

        try {
            StreamUtils.fromInputStream(file, "data", image.getInputStream());
        } catch (IOException e) {
            throw new ErrorMessageException(ImageConstants.IMAGE_PROCESS_MESSAGE, e);
        }

        fileItem.setFilesId(file.getId());
        fileItem.setTargetId(groupId);
        fileItem.setTargetTable(fileImageType.getTargetTable().name());
        filesDao.saveFileItem(fileItem);

        if (environmentUtil.writeToCdn()) {
            gcpUtil.uploadFile(file.getId(), image);
        }

        return new IdDTO(file.getId());
    }

    @Override
    public ResponseEntity<Object> getGroupSpecificHeroImage(Long groupId) {

        Files groupHeroImage = groupConfigServiceDao.getGroupSpecificFile(groupId, FileTypes.GROUP_HERO_IMAGE.name());
        return fileImageService.prepareImageForResponse(groupId, groupHeroImage);
    }

    @Transactional
    @Override
    public IdDTO createGroupSpecificHeroImage(Long groupId, MultipartFile image) {
        return createGroupImage(groupId, image, GROUP_HERO_IMAGE);    }
    
    @Override
    public void deleteGroupImage(Long groupId, String fileType) {
        groupConfigServiceDao.deleteGroupSpecificFile(groupId, fileType);
    }
}
