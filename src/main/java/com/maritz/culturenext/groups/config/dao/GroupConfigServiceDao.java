package com.maritz.culturenext.groups.config.dao;

import java.util.List;

import com.maritz.core.jpa.entity.Files;

import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;

public interface GroupConfigServiceDao {

    /**
     * Create or update a group config
     * 
     * @param GroupConfigDTO Json object to update/create with
     * @return GroupConfigDTO
     */
    GroupConfigDTO createUpdateGroupConfig(GroupConfigDTO GroupConfigDTO);
    
    /**
     * Return all group configs that have a custom value
     * 
     * @return List&lt;GroupConfigDTO&gt;
     */
    List<GroupConfigDTO> getAllGroupConfig();
    
    /**
     * Updates the status of the RIDEAU pax_account records 
     * based upon global and group permissions
     */
    void updateRideauAccountStatus();
    
    /**
     * Return the File of a given file type in a target table
     * 
     * @param fileType Type of file (example: GROUP_LOGO)
     * @param targetId Id we want to target in the table
     * @return Files record;
     */
    Files getGroupSpecificFile(Long groupId, String fileType);
    
    /**
     * Delete the file and file item tied to the given group id / file type
     * 
     * @param groupId id of group to delete file for
     * @param fileType FILE_TYPE of file to delete
     */
    void deleteGroupSpecificFile(Long groupId, String fileType);
}
