package com.maritz.culturenext.groups.config.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.groups.config.dto.PaxSpecificGroupsConfigDTO;
import com.maritz.culturenext.groups.config.service.PaxSpecificGroupsConfigService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class PaxSpecificGroupsConfigRestService {
    private static final String PATH = "participants/{paxId}/groups/config";
    private static final String PATH_PARTICIPANTS_PAX_GROUPS_LOGO = "participants/{paxId}/groups/logo";
    private static final String PATH_PARTICIPANTS_PAX_GROUPS_HERO_IMAGE = "participants/{paxId}/groups/hero-image";
    
    @Inject private PaxSpecificGroupsConfigService paxSpecificGroupsConfigService;
    @Inject private Security security;
    
    //rest/participants/~/groups/config
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves pax specific group configuration",
                notes = "This method allows a user to retrieve group specific conguration for every group a pax belongs to")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of group configuration", response = PaxSpecificGroupsConfigDTO.class),
            @ApiResponse(code = 400, message = "Request error")
            }
    )
    @Permission("PUBLIC")
    public PaxSpecificGroupsConfigDTO getPaxSpecificGroupsConfig(
            @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return paxSpecificGroupsConfigService.getPaxSpecificGroupsConfig(paxId);
    }

    //rest/participants/~/groups/logo
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = PATH_PARTICIPANTS_PAX_GROUPS_LOGO, method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves pax specific logo",
            notes = "This method allows a user to retrieve pax specific logo")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of pax logo"),
            @ApiResponse(code = 400, message = "Request error")
    }
    )
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPaxSpecificGroupsLogo(
            @ApiParam(value = "PaxId of the requesting user", required = true) 
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(paxId);
    }

    //rest/participants/~/groups/hero-image
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = PATH_PARTICIPANTS_PAX_GROUPS_HERO_IMAGE, method = RequestMethod.GET)
    @ApiOperation(value = "Retrieve pax specific hero image",
            notes = "This method allows a user to retrieve pax specific hero image")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of pax hero image"),
            @ApiResponse(code = 400, message = "Request error")
    }
    )
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPaxSpecificGroupsHeroImage(
            @ApiParam(value = "PaxId of the requesting user", required = true)
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(paxId);
    }
}