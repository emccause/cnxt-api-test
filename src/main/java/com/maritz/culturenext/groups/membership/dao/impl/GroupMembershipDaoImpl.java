package com.maritz.culturenext.groups.membership.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.culturenext.groups.membership.constants.GroupConstants;
import com.maritz.culturenext.groups.membership.dao.GroupMembershipDao;

@Repository
public class GroupMembershipDaoImpl extends AbstractDaoImpl implements GroupMembershipDao {
    
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<GroupsPax> groupsPaxDao;

    //GROUP_CONFIG COLUMNS
    private static final String GROUP_CONFIG_ID_PARAM  = "groupConfigId";
    private static final String GROUP_CONFIG_VALUE_PARAM = "groupConfigValue";
    private static final String GROUP_ID_PARAM = "groupId";
    private static final String GROUP_PAX_ID_PARAM = "groupPaxId";
    private static final String GROUP_UPDATE_DATE_PARAM = "groupUpdateDate";
    private static final String PAX_ID_PARAM= "paxId";
    private static final String STATUS_TYPE_PARAM = "statusTypeCode";

    private static final String QUERY_PAX_LIST_PLACEHOLDER = "{PAX_LIST_PLACEHOLDER}";
    private static final String GROUP_CONFIG_COLUMN_PLACEHOLDER = "{GROUP_CONFIG_COLUMN_PLACEHOLDER}";
    private static final String GROUP_CONFIG_TABLE_PLACEHOLDER = "{GROUP_CONFIG_TABLE_PLACEHOLDER}";
    private static final String QUERY_PAX_GROUP_CONFIG_JOIN_PLACEHOLDER = "{QUERY_PAX_GROUP_CONFIG_JOIN_PLACEHOLDER}";
    private static final String QUERY_GROUP_CONFIG_FILTER_PLACEHOLDER = "{QUERY_GROUP_CONFIG_FILTER_PLACEHOLDER}";

    private static String SQL_REMOVE_GROUP_CONFIG =
            "DELETE gp FROM component.GROUPS g "
            + "JOIN component.GROUPS_PAX gp ON gp.GROUP_ID = g.GROUP_ID "
            + "JOIN component.PAX_GROUP pg ON pg.PAX_ID = gp.PAX_ID "
            + "WHERE g.GROUP_CONFIG_ID = :" + GROUP_CONFIG_ID_PARAM
            + " AND gp.PAX_ID NOT IN (" + QUERY_PAX_LIST_PLACEHOLDER +")";

    private static String SQL_SEARCH_PAX_GROUP =
            "SELECT pg2.PAX_ID FROM component.PAX_GROUP_MISC pgm "
            + "JOIN component.PAX_GROUP pg2 ON pg2.PAX_GROUP_ID = pgm.PAX_GROUP_ID "
            + "WHERE pgm." + GROUP_CONFIG_COLUMN_PLACEHOLDER + " = :" + GROUP_CONFIG_VALUE_PARAM;

    private static String SQL_SEARCH_PAX_ID =
            "SELECT PAX_ID FROM component." + GROUP_CONFIG_TABLE_PLACEHOLDER
            + " WHERE " + GROUP_CONFIG_COLUMN_PLACEHOLDER + " IS NOT NULL ";

    private static String SQL_GROUP_CONFIG_TABLE =
            "SELECT DISTINCT " + GROUP_CONFIG_COLUMN_PLACEHOLDER
            + " AS "+ GroupConstants.SELECTED_FIELD + ", PAX_ID "
            + " FROM component." + GROUP_CONFIG_TABLE_PLACEHOLDER +" T1 "
            + QUERY_PAX_GROUP_CONFIG_JOIN_PLACEHOLDER + " " + QUERY_GROUP_CONFIG_FILTER_PLACEHOLDER;

    private static String SQL_PAX_GROUP_JOIN = " INNER JOIN component.PAX_GROUP t2 on (t1.PAX_GROUP_ID=t2.PAX_GROUP_ID) ";

    private static String SQL_GROUP_CONFIG_FILTER = " WHERE " + GROUP_CONFIG_COLUMN_PLACEHOLDER + " = :" + GROUP_CONFIG_VALUE_PARAM;

    private static String SQL_ADDRESS_CONFIG_FILTER = " WHERE PREFERRED = 'Y' ";

    private static final String UPDATE_GROUP_STATUS_QUERY =
        "UPDATE component.GROUPS " +
        "SET STATUS_TYPE_CODE = :" + STATUS_TYPE_PARAM + ", " +
        "GROUP_UPDATE_DATE = :" + GROUP_UPDATE_DATE_PARAM + " " +
        "WHERE GROUP_CONFIG_ID = :" + GROUP_CONFIG_ID_PARAM + " " +
        "AND STATUS_TYPE_CODE <> :" + STATUS_TYPE_PARAM;

    private static final String GROUPS_PAX_QUERY =
        "SELECT gp.ID " +
        "FROM component.GROUPS_PAX gp " +
        "JOIN component.GROUPS g ON g.GROUP_ID = gp.GROUP_ID " +
        "WHERE gp.PAX_ID = :" + PAX_ID_PARAM + " " +
        "AND gp.GROUP_ID <> :" + GROUP_ID_PARAM + " " +
        "AND g.GROUP_CONFIG_ID = :" + GROUP_CONFIG_ID_PARAM;

    private static final String DELETE_GROUPS_PAX_QUERY =
        "DELETE " +
        "FROM component.GROUPS_PAX " +
        "WHERE ID = :" + GROUP_PAX_ID_PARAM;

   /**
     * Creates select script to obtain groupDesc & pax_id data based on
     * group_config.config
     *
     * @param config
     *            . EXAMPLE: TABLE.FIELD,CONFIG-FIELD={value}
     * @return list groupDescription & pax_id
     */
    @Override
    public List<Map<String, Object>> getGroupsData(String config) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String selectScript = null;

        if (config != null && !config.isEmpty()) {
            String[] elemsArray = config.split(GroupConstants.DELIMITER_COMMA);
            String[] elems;
            if (elemsArray.length > 0) {
                String firstElem = elemsArray[0];
                elems = firstElem.split(GroupConstants.DELIMITER_POINT);

                selectScript = SQL_GROUP_CONFIG_TABLE;
                selectScript = selectScript.replace(GROUP_CONFIG_COLUMN_PLACEHOLDER, elems[1]);
                selectScript = selectScript.replace(GROUP_CONFIG_TABLE_PLACEHOLDER, elems[0]);

                if (elems[0].equalsIgnoreCase(GroupConstants.PAX_GROUP_MISC_COLUMN)) {
                    selectScript = selectScript.replace(QUERY_PAX_GROUP_CONFIG_JOIN_PLACEHOLDER,SQL_PAX_GROUP_JOIN);
                } else {
                    selectScript = selectScript.replace(QUERY_PAX_GROUP_CONFIG_JOIN_PLACEHOLDER,"");
                }

                if (elemsArray.length > 1) {
                    elems = elemsArray[1].split(GroupConstants.DELIMITER_EQUAL); // { [VF_NAME],[Area] }

                    selectScript = selectScript.replace(QUERY_GROUP_CONFIG_FILTER_PLACEHOLDER, SQL_GROUP_CONFIG_FILTER);
                    selectScript = selectScript.replace(GROUP_CONFIG_COLUMN_PLACEHOLDER, elems[0]);
                    params.addValue(GROUP_CONFIG_VALUE_PARAM,  elems[1]);
                } else {
                    selectScript = selectScript.replace(QUERY_GROUP_CONFIG_FILTER_PLACEHOLDER, SQL_ADDRESS_CONFIG_FILTER);
                }
            }
        }

        List<Map<String, Object>> nodes = new ArrayList<>();
        if (selectScript != null) {
            nodes = namedParameterJdbcTemplate.query(selectScript,
                    params, new ColumnMapRowMapper());
        }
        return nodes;
    }

    /**
     * If a user has removed any of their enrollment data, they need to be
     * removed from the enrollment group that they were previously in.
     *
     * Ex: I was in the 'IT' Department. I got placed in the 'IT' group.
     * On the next enrollment file, my department was blank.
     * My PAX_GROUP_MISC entry for DEPARTMENT gets deleted, and therefore
     * I should no longer be in the 'IT' group.
     *
     * @param config
     *    EXAMPLE: TABLE.FIELD,CONFIG-FIELD={value}
     */
    public void cleanupGroupAssignments(GroupConfig groupConfig) {

        MapSqlParameterSource params = new MapSqlParameterSource();

        String sql = null;

        if (groupConfig.getConfig() != null && !groupConfig.getConfig().isEmpty()) {
            String[] elemsArray = groupConfig.getConfig().split(GroupConstants.DELIMITER_COMMA);
            String[] elems;
            if (elemsArray.length > 0) {
                String firstElem = elemsArray[0];
                elems = firstElem.split(GroupConstants.DELIMITER_POINT);
                sql = SQL_REMOVE_GROUP_CONFIG;
                if (elems[0].equalsIgnoreCase(GroupConstants.PAX_GROUP_MISC_COLUMN)) {
                    if (elemsArray.length > 1) {
                        elems = elemsArray[1].split(GroupConstants.DELIMITER_EQUAL); // { [VF_NAME],[Area] }

                        sql = sql.replace(QUERY_PAX_LIST_PLACEHOLDER, SQL_SEARCH_PAX_GROUP);

                        sql = sql.replace(GROUP_CONFIG_COLUMN_PLACEHOLDER, elems[0]); // COLUMN
                        params.addValue(GROUP_CONFIG_VALUE_PARAM, elems[1]); // VALUE

                    }
                } else {
                    sql = sql.replace(QUERY_PAX_LIST_PLACEHOLDER, SQL_SEARCH_PAX_ID);

                    sql = sql.replace(GROUP_CONFIG_TABLE_PLACEHOLDER, elems[0]); // TABLE
                    sql = sql.replace(GROUP_CONFIG_COLUMN_PLACEHOLDER, elems[1]); // COLUMN
                }
            }
        }

        if (sql != null) {
            params.addValue(GROUP_CONFIG_ID_PARAM, groupConfig.getId()); // VALUE
            namedParameterJdbcTemplate.update(sql, params);
        }
    }

    /**
     * Update groups status for a specific groupConfigId
     * @param newStatus
     * @param groupConfigId
     */
    @Override
    public void bulkUpdateGroupStatus(String newStatus, Long groupConfigId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(STATUS_TYPE_PARAM, newStatus);
        params.addValue(GROUP_CONFIG_ID_PARAM, groupConfigId);
        params.addValue(GROUP_UPDATE_DATE_PARAM, new Date());

        namedParameterJdbcTemplate.update(UPDATE_GROUP_STATUS_QUERY, params);
    }

    /**
     * Create new group wit statusTypeCode if there isn't any for a specific groupDesc and groupConfigId.
     * @param groupDesc
     * @param groupConfigId
     * @param statusTypeCode
     */
    @Override
    public Groups insertGroup(String groupDesc, Long groupConfigId, String statusTypeCode) {

        Groups group = groupsDao.findBy()
                .where(GroupConstants.GROUP_CONFIG_ID_FIELD).eq(groupConfigId)
                .and(GroupConstants.GROUP_DESC_FIELD).eq(groupDesc).findOne();

        if (group == null) {
            group = new Groups();
            group.setGroupTypeCode( GroupConstants.GROUP_TYPE_ENROLLMENT);
            group.setGroupDesc(groupDesc);
            group.setGroupConfigId(groupConfigId);
            group.setStatusTypeCode(statusTypeCode);
            group.setGroupCreateDate(new Date());

            groupsDao.save(group);
        }

        return group;
    }


    /**
     * Create new groupsPax if not exist. Remove existing ones if pax has changed enrollment group.
     * @param groupId
     * @param paxId
     * @param groupConfigId
     */
    @Override
    public GroupsPax insertGroupPax(Long groupId, Long paxId, Long groupConfigId) {
        GroupsPax groupsPax = groupsPaxDao.findBy()
                .where(GroupConstants.GROUP_ID_FIELD).eq(groupId)
                .and(GroupConstants.PAX_ID_FIELD).eq(paxId).findOne();

        if(groupsPax == null) {
            //If the user is switching enrollment groups, remove them from the old one
            deleteExistingGroupsPax(paxId, groupId, groupConfigId);

            // Add the user to the enrollment group
            groupsPax = new GroupsPax();
            groupsPax.setGroupId(groupId);
            groupsPax.setPaxId(paxId);

            groupsPaxDao.save(groupsPax);
        }

        return groupsPax;
    }

    /**
     * Removes an entry from groups_pax
     * @param paxId
     * @param groupId
     * @param groupConfigId
     */
    private void deleteExistingGroupsPax(Long paxId, Long groupId, Long groupConfigId) {

        //First check to see if the user is currently in an enrollment group
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PARAM, paxId);
        params.addValue(GROUP_ID_PARAM, groupId);
        params.addValue(GROUP_CONFIG_ID_PARAM, groupConfigId);

        List<Long> nodes = namedParameterJdbcTemplate.queryForList(GROUPS_PAX_QUERY, params, Long.class);

        //If an entry was returned, delete it
        if (!CollectionUtils.isEmpty(nodes)) {
            params.addValue(GROUP_PAX_ID_PARAM, nodes.get(0));
            namedParameterJdbcTemplate.update(DELETE_GROUPS_PAX_QUERY, params);
        }

    }

}
