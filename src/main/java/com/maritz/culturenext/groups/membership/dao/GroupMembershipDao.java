package com.maritz.culturenext.groups.membership.dao;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.GroupsPax;

public interface GroupMembershipDao {

    void bulkUpdateGroupStatus(String newStatus, Long groupConfigId);

    Groups insertGroup(String groupDesc, Long groupConfigId, String statusTypeCode);

    GroupsPax insertGroupPax(Long groupId, Long paxId, Long groupConfigId);

    List<Map<String, Object>> getGroupsData(String config);

    void cleanupGroupAssignments(GroupConfig groupConfig);

}
