package com.maritz.culturenext.groups.membership;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.GroupConfigRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.culturenext.groups.membership.constants.GroupConstants;
import com.maritz.culturenext.groups.membership.dao.GroupMembershipDao;

@Service
public class ProcessGroupMembership {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private BatchService batchService;
    @Inject private BatchRepository batchRepository;
    @Inject private GroupConfigRepository groupConfigRepository;
    @Inject private GroupMembershipDao groupMembershipDao;

    @TriggeredTask
    public void processGroupMembership() {
        logger.info("Started group membership job.");
        checkBatchTableForQueuedGroupBatches();
        logger.info("Finished group membership job.");
    }

    /**
     * This method kicks off the group processing job. First it looks for any GRPPRC batches
     * and sets them to IN_PROGRESS if they exist. It then pulls all group_config data from
     * the database and processes each one in the manageEnrollmentGroups method.
     * Finally, we run a cleanup method to make sure there are no PAX left in
     * groups that they no longer belong to.
     *
     * Currently this job is scheduled to run every 15 mins Monday-Friday.
     */
    public void checkBatchTableForQueuedGroupBatches() {

        SimpleDateFormat sdf = new SimpleDateFormat(GroupConstants.DATE_FORMAT);

        //Get QUEUED batch, there should only ever be one QUEUED batch at a time
        Batch batch = batchRepository.findBy()
            .where(GroupConstants.STATUS_TYPE_CODE_FIELD).eq(StatusTypeCode.QUEUED.name())
            .and(GroupConstants.BATCH_TYPE_CODE_FIELD).eq(GroupConstants.BATCH_TYPE_GROUP)
            .findOne();
        
        if (batch != null) {

            //Update the batch's status to IN_PROGRESS
            batchService.updateBatchStatus(batch, StatusTypeCode.IN_PROGRESS.name());

            //Create Start-Time batch_event
            Date startTimeObject = new Date();
            String startTime = sdf.format(startTimeObject);
            batchService.createBatchEvent( batch.getBatchId(), GroupConstants.START_TIME_EVENT,
                    StatusTypeCode.SUCCESS.name(), null, null, null, startTime);
        } else {
            logger.info("Batch not found!");
        }

        //The job runs even without a batch to keep the groups up to date
        //Get all of the config info for the enrollment groups
        List<GroupConfig> groupConfigEnrollList = groupConfigRepository.findBy()
            .where(GroupConstants.GROUP_CONFIG_TYPE_CODE_FIELD)
            .eq(GroupConstants.GROUP_TYPE_ENROLLMENT_FIELD)
            .findAll();
        
        for (GroupConfig groupConfig: groupConfigEnrollList) {
            manageEnrollmentGroup(groupConfig);

            //If a user's enrollment data is deleted, remove them from their previous group
            groupMembershipDao.cleanupGroupAssignments(groupConfig);
        }

        if (batch != null) {
            //Update the batch's status to SUCCESS
            batchService.updateBatchStatus(batch, StatusTypeCode.SUCCESS.name());

            //Create End-Time batch_event
            Date endTimeObject = new Date();
            String endTime = sdf.format(endTimeObject);
            logger.info("Create End-Time bacth_event. BatchId {} endTime {}", batch.getBatchId(), endTime);

            batchService.createBatchEvent(batch.getBatchId(), GroupConstants.END_TIME_EVENT,
                    StatusTypeCode.SUCCESS.name(),null,null,null, endTime);

        }
    }

    /**
     * This method takes each GROUP_CONFIG record one by one and processes it.
     * If the group_config is ACTIVE:
     *         First it will find a list of all PAX with any enrollment data associated to that group_config.
     *         If a specific group does not exist yet, it will get created.
     *         The PAX are then either updated or inserted into the corresponding groups
     * If the group_config is INACTIVE:
     *         All of the groups associated to this group_config get inactivated 
     * @param groupConfig
     */
    private void manageEnrollmentGroup(GroupConfig groupConfig) {        
        logger.info("Begin - manageEnrollmentGroup for groupConfig: " + groupConfig.getGroupConfigName());

        if (StatusTypeCode.ACTIVE.name().equalsIgnoreCase(groupConfig.getStatusTypeCode())) {

            //Activate all groups tied to the ACTIVE groupConfigId
            groupMembershipDao.bulkUpdateGroupStatus(StatusTypeCode.ACTIVE.name(), groupConfig.getId());

            //Create any new enrollment groups
            //Update group membership for existing groups
            Groups group = null;
            List<Map<String, Object>> groupDataList =
                groupMembershipDao.getGroupsData(groupConfig.getConfig());

            for (Map<String, Object> node : groupDataList) {
                
                group = groupMembershipDao.insertGroup(
                        (String) node.get(GroupConstants.SELECTED_FIELD),
                        groupConfig.getId(),
                        StatusTypeCode.ACTIVE.name());

                groupMembershipDao.insertGroupPax(
                        group.getGroupId(),
                        (long) node.get(GroupConstants.PAX_ID_COLUMN),
                        groupConfig.getId());
            }
        }
        else if (StatusTypeCode.INACTIVE.name().equalsIgnoreCase(groupConfig.getStatusTypeCode())) {
            //Inactivate all groups tied to the INACTIVE groupConfigId
            groupMembershipDao.bulkUpdateGroupStatus(StatusTypeCode.INACTIVE.name(), groupConfig.getId());
        }
        
        logger.info("End - manageEnrollmentGroup for groupConfig: " + groupConfig.getGroupConfigName());
    }

}