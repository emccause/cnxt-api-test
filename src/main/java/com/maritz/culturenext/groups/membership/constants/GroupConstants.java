package com.maritz.culturenext.groups.membership.constants;

public class GroupConstants {

    public static final String START_TIME_EVENT = "STRTIM";
    public static final String END_TIME_EVENT = "ENDTIM";

    // Fields
    public static final String PAX_ID_FIELD =  "paxId";
    public static final String GROUP_ID_FIELD =  "groupId";
    public static final String GROUP_CONFIG_TYPE_CODE_FIELD = "groupConfigTypeCode";
    public static final String STATUS_TYPE_CODE_FIELD = "statusTypeCode";
    public static final String BATCH_TYPE_CODE_FIELD = "batchTypeCode";
    public static final String GROUP_CONFIG_ID_FIELD = "groupConfigId";
    public static final String GROUP_DESC_FIELD =  "groupDesc";

    // Columns
    public static final String PAX_ID_COLUMN = "PAX_ID";
    public static final String PAX_GROUP_MISC_COLUMN = "PAX_GROUP_MISC";
    public static final String GROUP_ID_COLUMN = "GROUP_ID";
    public static final String ID_COLUMN = "ID";
    public static final String SELECTED_FIELD = "SELECTED_FIELD";

    // Format
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String BATCH_TYPE_GROUP = "GRPPRC";
    public static final String GROUP_TYPE_ENROLLMENT_FIELD = "ENRL_FIELD";



    public static final String GROUP_TYPE_ENROLLMENT = "ENRL";

    // Delimiter
    public static final String DELIMITER_COMMA = ",";
    public static final String DELIMITER_POINT = "\\.";
    public static final String DELIMITER_EQUAL = "=";

}
