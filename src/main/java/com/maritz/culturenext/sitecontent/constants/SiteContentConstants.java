package com.maritz.culturenext.sitecontent.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class SiteContentConstants {

    //Valid site content statuses
    public static final List<String> validStatuses = Arrays.asList(
                StatusTypeCode.ACTIVE.name(), StatusTypeCode.PENDING.toString(), StatusTypeCode.INACTIVE.name());

    public static String COLUMN_DATA = "DATA";
    public static String COLUMN_FILES = "FILES";
    public static String COLUMN_FILES_DATA = "FILES/DATA";
    
    public static String DISPLAY_NAME = "displayName";
    
    public static String CONTENT = "content";
    public static String SITE_CONTENT_ERROR_NOT_ADMIN = "Only Admin can access this.";
    public static String ERROR_SITE_CONTENT_INVALID_TYPE = "ERROR_SITE_CONTENT_INVALID_TYPE";
    public static String ERROR_SITE_CONTENT_INVALID_TYPE_MSG = "Invalid Site Content Type";
    public static String ERROR_SITE_CONTENT_INVALID_STATUS = "INVALID_STATUS";
    public static String ERROR_SITE_CONTENT_INVALID_STATUS_MSG = "Invalid status requested.";
    public static String ERROR_SITE_CONTENT_INVALID_CONTENT = "INVALID_CONTENT";
    public static String ERROR_SITE_CONTENT_INVALID_CONTENT_MSG = "Invalid content.";
    public static String ERROR_SITE_MISSING_REQUEST_BODY = "ERROR_SITE_MISSING_REQUEST_BODY";
    public static String ERROR_SITE_MISSING_REQUEST_BODY_MSG = "Missing request body.";

    public static String ERROR_SITE_CONTENT_FILE_DOESNOT_EXIST = "File does not exists for:";
    public static String ERROR_SITE_CONTENT_TRANSLATABLE_ID_DOESNOT_EXIST = "Translatable Id does not exists for:";
    public static String ERROR_SITE_CONTENT_TRANSLATABLE_PHRASE_DOESNOT_EXIST = "Translatable Id does not exists for:";
    public static String ERROR_SITE_CONTENT = "ERROR_SITE_CONTENT";
    public static String ERROR_SITE_CONTENT_MSG = "Site Content Error";
    
    public static final ErrorMessage ERROR_SITE_CONTENT_MESSAGE =new ErrorMessage()
            .setCode(SiteContentConstants.ERROR_SITE_CONTENT)
            .setField(SiteContentConstants.CONTENT)
            .setMessage(SiteContentConstants.ERROR_SITE_CONTENT_MSG);
    
    public static final ErrorMessage ERROR_SITE_CONTENT_INVALID_TYPE_MESSAGE = new ErrorMessage()
            .setCode(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_TYPE)
            .setField(ProjectConstants.TYPE)
            .setMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_TYPE_MSG);
    
    public static final ErrorMessage SITE_CONTENT_ERROR_NOT_ADMIN_MESSAGE = new ErrorMessage()
            .setCode(SiteContentConstants.SITE_CONTENT_ERROR_NOT_ADMIN)
            .setField(SiteContentConstants.CONTENT)
            .setMessage(SiteContentConstants.SITE_CONTENT_ERROR_NOT_ADMIN);
    
    public static final ErrorMessage ERROR_SITE_MISSING_REQUEST_BODY_MESSAGE = new ErrorMessage()
            .setCode(SiteContentConstants.ERROR_SITE_MISSING_REQUEST_BODY)
            .setField(ProjectConstants.REQUEST)
            .setMessage(SiteContentConstants.ERROR_SITE_MISSING_REQUEST_BODY_MSG);
    
    public static final ErrorMessage ERROR_SITE_CONTENT_INVALID_STATUS_MESSAGE =new ErrorMessage()
            .setCode(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_STATUS)
            .setField(ProjectConstants.STATUS)
            .setMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_STATUS_MSG);
    
    public static final ErrorMessage ERROR_SITE_CONTENT_INVALID_CONTENT_MESSAGE =new ErrorMessage()
            .setCode(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_CONTENT)
            .setField(SiteContentConstants.CONTENT)
            .setMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_CONTENT_MSG);
}
