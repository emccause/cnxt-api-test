package com.maritz.culturenext.sitecontent.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.sitecontent.dto.SiteContentDTO;
import com.maritz.culturenext.sitecontent.services.SiteContentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/site-content", description = "all calls dealing with site content")
@RequestMapping("site-content")
public class SiteContentRestService {

    @Inject private SiteContentService siteContentService;

    //rest/site-content
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(method = { RequestMethod.PUT, RequestMethod.PATCH })
    @ApiOperation(value="Update Site Content", notes="This method allows a admin user to update site content")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful update of site content", response = SiteContentDTO.class), 
                            @ApiResponse(code = 403, message = "Non-admin accesses endpoint")
                            }
                )
    @Permission("PUBLIC")
    public SiteContentDTO updateSiteContent(
            @RequestBody SiteContentDTO siteContentRequestObject
        ) throws Throwable {
        
        if (siteContentRequestObject == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return siteContentService.updateSiteContent(siteContentRequestObject);
    }
    
    //rest/site-content/type
    @RequestMapping(method = RequestMethod.GET, value = "/{type}")
    @ApiOperation(value = "Get translation of site content")
    @Permission("PUBLIC")
    public @ResponseBody SiteContentDTO getSiteContent(
            @ApiParam(value = "PaxId of the requesting user") @PathVariable(TYPE_REST_PARAM) String type,
            @ApiParam(value = "Language Code for translation of site content") 
            @RequestParam(value = "languageCode", required = false) String languageCode
            ) throws Throwable {

        if(type == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        return siteContentService.getSiteContent(type, languageCode);
    }
    
    

}