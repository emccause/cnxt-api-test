package com.maritz.culturenext.sitecontent.services.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Translatable;
import com.maritz.core.jpa.entity.TranslatablePhrase;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.SiteContentTypeEnum;
import com.maritz.culturenext.recognition.awardcode.services.impl.AwardCodeServiceImpl;
import com.maritz.culturenext.sitecontent.constants.SiteContentConstants;
import com.maritz.culturenext.sitecontent.dto.SiteContentDTO;
import com.maritz.culturenext.sitecontent.services.SiteContentService;
import com.maritz.culturenext.util.TranslationUtil;


@Component
public class SiteContentServiceImpl implements SiteContentService{

    final Logger logger = LoggerFactory.getLogger(AwardCodeServiceImpl.class.getName());

    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<Translatable> translatableDao;
    @Inject private ConcentrixDao<TranslatablePhrase> translatablePhraseDao;
    @Inject private Security security;
    @Inject private TranslationUtil translationUtil;


    @Override
    public SiteContentDTO getSiteContent(String type, String languageCode) throws ErrorMessageException {
        if (!SiteContentTypeEnum.TERMS_AND_CONDITIONS.name().equals(type)) {
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_TYPE_MESSAGE);
        }

        SiteContentDTO siteContenDto = new SiteContentDTO();

        // Translate event's name and description.
        Files file = filesDao.findBy()
            .where(ProjectConstants.FILE_TYPE_CODE)
            .eq(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name())
            .findOne()
        ;

        if (file == null) {
            logger.error(SiteContentConstants.ERROR_SITE_CONTENT_FILE_DOESNOT_EXIST +" [" + SiteContentTypeEnum.TERMS_AND_CONDITIONS.name() + "] " );
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_MESSAGE);
        }

        Long translatableId = translatableDao.findBy()
            .where(SiteContentConstants.DISPLAY_NAME)
            .eq(SiteContentConstants.COLUMN_FILES_DATA)
            .findOne(ProjectConstants.ID, Long.class)
        ;
        if (translatableId == null) {
            logger.error(SiteContentConstants.ERROR_SITE_CONTENT_TRANSLATABLE_ID_DOESNOT_EXIST  +" [" + SiteContentConstants.COLUMN_FILES_DATA + "] " );
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_MESSAGE);
        }

        TranslatablePhrase translatablePhrase = translatablePhraseDao.findBy()
            .where(ProjectConstants.TRANSLATABLE_ID)
            .eq(translatableId)
            .and(ProjectConstants.TARGET_ID).eq(file.getId())
            .findOne()
        ;

        if (translatablePhrase == null) {
            logger.error(SiteContentConstants.ERROR_SITE_CONTENT_TRANSLATABLE_PHRASE_DOESNOT_EXIST+" [" + file.getId() + "] " );
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_MESSAGE);
        }

        String fileText = StreamUtils.toString(filesDao.findBy().where("id").eq(file.getId()), "data");
        if (languageCode != null && !ProjectConstants.DEFAULT_LOCALE_CODE.equals(languageCode)) {

            List<String> columnList = Arrays.asList(SiteContentConstants.COLUMN_DATA);
            Map<String, String> mapTrans = translationUtil.getTranslationsForListOfColumns(languageCode, SiteContentConstants.COLUMN_FILES, columnList, file.getId(), null);

            if (mapTrans == null || mapTrans.isEmpty()) {// return default value
                siteContenDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());
                siteContenDto.setStatus(translatablePhrase.getStatusTypeCode());
                siteContenDto.setContent(fileText);
            }
            else {
                siteContenDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());
                siteContenDto.setStatus(StatusTypeCode.ACTIVE.name());
                siteContenDto.setContent(mapTrans.get(SiteContentConstants.COLUMN_DATA));
            }
        }
        else {//return default value
            siteContenDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());
            siteContenDto.setStatus(translatablePhrase.getStatusTypeCode());
            siteContenDto.setContent(fileText);
        }

        return siteContenDto;
    }

    @Override
    @Transactional
    public SiteContentDTO updateSiteContent(SiteContentDTO siteContentDTO) throws ErrorMessageException {

        //Check if logged in user is Admin
        if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.SITE_CONTENT_ERROR_NOT_ADMIN_MESSAGE);
        }

        if (siteContentDTO == null) {
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_MISSING_REQUEST_BODY_MESSAGE);
        }

        if (siteContentDTO.getType() == null || !SiteContentTypeEnum.getSiteContentTypeList().contains(siteContentDTO.getType())) {
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_TYPE_MESSAGE);
        }

        if (siteContentDTO.getStatus() == null || !SiteContentConstants.validStatuses.contains(siteContentDTO.getStatus())){
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_STATUS_MESSAGE);
        }

        if (siteContentDTO.getContent() == null || "".equals(siteContentDTO.getContent().trim())) {
            throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_CONTENT_MESSAGE);
        }

        if (SiteContentTypeEnum.TERMS_AND_CONDITIONS.name().equals(siteContentDTO.getType())) {
            Files file = filesDao.findBy()
                .where(ProjectConstants.FILE_TYPE_CODE).eq(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name())
                .findOne()
            ;

            StreamUtils.fromString(filesDao.findBy().where("id").eq(file.getId()), "data", siteContentDTO.getContent());

            Long translatableId = translatableDao.findBy()
                .where(SiteContentConstants.DISPLAY_NAME).eq(SiteContentConstants.COLUMN_FILES_DATA)
                .findOne(ProjectConstants.ID, Long.class)
            ;

            if (translatableId == null) {
                logger.error(SiteContentConstants.ERROR_SITE_CONTENT_TRANSLATABLE_ID_DOESNOT_EXIST  +" [" + SiteContentConstants.COLUMN_FILES_DATA + "] " );
                throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_MESSAGE);
            }

            TranslatablePhrase translatablePhrase = translatablePhraseDao.findBy()
                .where(ProjectConstants.TRANSLATABLE_ID).eq(translatableId)
                .and(ProjectConstants.TARGET_ID).eq(file.getId())
                .findOne()
            ;
            if (translatablePhrase == null) {
                logger.error(SiteContentConstants.ERROR_SITE_CONTENT_TRANSLATABLE_PHRASE_DOESNOT_EXIST+" [" + file.getId() + "] " );
                throw new ErrorMessageException().addErrorMessage(SiteContentConstants.ERROR_SITE_CONTENT_MESSAGE);

            }

            translatablePhrase.setStatusTypeCode(siteContentDTO.getStatus());
            translatablePhraseDao.update(translatablePhrase);
        }
        else {
            siteContentDTO = null;
        }

        return siteContentDTO;
    }

}
