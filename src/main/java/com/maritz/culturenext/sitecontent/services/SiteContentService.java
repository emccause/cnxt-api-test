package com.maritz.culturenext.sitecontent.services;


import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.sitecontent.dto.SiteContentDTO;

public interface SiteContentService {

    /**
     * Return site content by type
     * 
     * @param type - TERMS_AND_CONDITIONS
     * @param languageCode
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/30572556/GET+Site+Content
     */
    SiteContentDTO getSiteContent(String type, String languageCode) throws ErrorMessageException;
    
    /**
     * Update existing site content
     * 
     * @param siteContentDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/30572565/Update+Site+Content
     */
    SiteContentDTO updateSiteContent(SiteContentDTO siteContentDTO) throws ErrorMessageException;
}
