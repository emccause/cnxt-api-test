package com.maritz.culturenext.theme.dto;

public class ImageUrlsDto {
    private String logo;
    private String background;

    public String getLogo() {
        return logo;
    }

    public ImageUrlsDto setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    public String getBackground() {
        return background;
    }

    public ImageUrlsDto setBackground(String background) {
        this.background = background;
        return this;
    }
}
