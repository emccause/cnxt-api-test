package com.maritz.culturenext.theme.dto;

public class BrandingDto {
    private ImageUrlsDto imageUrls;
    private ColorsDto colors;

    public ImageUrlsDto getImageUrls() {
        return imageUrls;
    }

    public BrandingDto setImageUrls(ImageUrlsDto imageUrls) {
        this.imageUrls = imageUrls;
        return this;
    }

    public ColorsDto getColors() {
        return colors;
    }

    public BrandingDto setColors(ColorsDto colors) {
        this.colors = colors;
        return this;
    }
}
