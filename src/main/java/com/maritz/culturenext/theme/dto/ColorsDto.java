package com.maritz.culturenext.theme.dto;

public class ColorsDto {
    private String primary;
    private String secondary;
    private String text;


    public String getPrimary() {
        return primary;
    }

    public ColorsDto setPrimary(String primary) {
        this.primary = primary;
        return this;
    }

    public String getSecondary() {
        return secondary;
    }

    public ColorsDto setSecondary(String secondary) {
        this.secondary = secondary;
        return this;
    }

    public String getText() {
        return text;
    }

    public ColorsDto setText(String text) {
        this.text = text;
        return this;
    }
}
