package com.maritz.culturenext.theme.constants;

import com.maritz.core.rest.ErrorMessage;

public class ThemeConstants {

    public static final int LOGO_WIDTH = 400;
    public static final int LOGO_HEIGHT = 150;

    public static final int LOGO_LIGHT_WIDTH = 400;
    public static final int LOGO_LIGHT_HEIGHT = 150;

    public static final int BACKGROUND_WIDTH = 2000;
    public static final int BACKGROUND_HEIGHT = 1500;

    public static final String LOGO = "Logo";

    public static final String FILE_TYPE_CODE = "fileTypeCode";
    
    public static final String ERROR_DELETE_LOGO = "ERROR_DELETE_LOGO";
    public static final String ERROR_DELETE_LOGO_MSG = "Site doesn't have a logo to delete.";

    public static final ErrorMessage ERROR_DELETE_LOGO_MESSAGE = new ErrorMessage()
            .setCode(ERROR_DELETE_LOGO)
            .setField(LOGO)
            .setMessage(ERROR_DELETE_LOGO_MSG);
}
