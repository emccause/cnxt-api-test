package com.maritz.culturenext.theme.rest;


import static com.maritz.culturenext.constants.RestParameterConstants.IMAGE_REST_PARAM;

import javax.inject.Inject;

import com.maritz.culturenext.theme.dto.BrandingDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.annotation.Permission;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.theme.services.ThemeService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "/theme", description = "all calls dealing with recognitions")
public class ThemeRestService {
    @Inject private ThemeService themeService;

    //rest/images/logo
    @GetMapping("images/logo")
    @SecurityPolicy(authenticated=false)
    @ApiOperation(value = "Returns theme logo image info for the client passed in.")
    public ResponseEntity<Object> getLogo() {
        return themeService.getResponseLogo();
    }

    //rest/images/logo
    @PostMapping("images/logo")
    @PreAuthorize("!@security.isImpersonated()")
    @Permission("PUBLIC")
    @ApiOperation(value = "Uploads the theme logo image the client passed in.")
    public void uploadLogo(
        @ApiParam(value = "the Image you want to upload") @RequestParam(IMAGE_REST_PARAM) MultipartFile[] images
    ) throws Exception{
        themeService.uploadThemeImage(images, FileTypes.PROJECT_LOGO.name());
    }

    //rest/images/background
    @GetMapping("images/background")
    @SecurityPolicy(authenticated=false)
    @ApiOperation(value = "Returns theme background image for the client passed in.")
    public ResponseEntity<Object> getBackground() {
        return themeService.getBackgroundResponse();
    }

    //rest/images/background
    @PreAuthorize("!@security.isImpersonated()")
    @PostMapping("images/background")
    @ApiOperation(value = "Uploads the theme background image the client passed in.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> uploadBackground(
            @ApiParam(value = "the Image you want to upload") @RequestParam(IMAGE_REST_PARAM) MultipartFile[] images) throws Exception{
        themeService.uploadThemeImage(images, FileTypes.PROJECT_BACKGROUND.name());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    //rest/images/logo-light
    @GetMapping("images/logo-light")
    @ApiOperation(value = "returns the logo light image that the client has passed in.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> getLogoLight() {
        return themeService.getResponseLogoLight();
    }

    //rest/images/logo-light
    @PreAuthorize("!@security.isImpersonated()")
    @PostMapping("images/logo-light")
    @ApiOperation(value = "Uploads the logo image the client passed in.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> uploadLogoLight(
            @ApiParam(value = "the image you want to upload") @RequestParam(IMAGE_REST_PARAM) MultipartFile[] images) throws Exception{

        themeService.uploadThemeImage(images, FileTypes.PROJECT_LOGO_LIGHT.name());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //rest/images/logo
    @PreAuthorize("!@security.isImpersonated()")
    @DeleteMapping("images/logo")
    @ApiOperation(value = "Removes the logo set up at the global/site level.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> deleteLogo() throws Exception{

        themeService.deleteLogo();

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("branding")
    @SecurityPolicy(authenticated=false)
    public BrandingDto getBranding() {
        return themeService.getBranding();
    }
}