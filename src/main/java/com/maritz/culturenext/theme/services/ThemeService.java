package com.maritz.culturenext.theme.services;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.theme.dto.BrandingDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;


public interface ThemeService {

    /**
     * Return the site logo image
     */
    Files getLogo();

    /**
     * Return the background image
     */
    Files getBackground();

    /**
     * Return the logo light image
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773815/Get+Logo+Light
     */
    Files getLogoLight();

    /**
     * Upload a theme image
     * 
     * @param images - image to upload
     * @param type - PROJECT_LOGO_LIGHT, PROJECT_LOGO, or PROJECT_BACKGROUND
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773804/Create+Logo+Light
     */
    void uploadThemeImage(MultipartFile[] images, String type) throws Exception;
    
    /**
     * Removes the logo set up at the global/site level. 
     */
    void deleteLogo() throws Exception;

    ResponseEntity<Object> getResponseLogo();

    ResponseEntity<Object> getResponseLogoLight();

    ResponseEntity<Object> getBackgroundResponse();

    BrandingDto getBranding();
}
