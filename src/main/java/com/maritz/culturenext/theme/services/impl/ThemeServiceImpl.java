package com.maritz.culturenext.theme.services.impl;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.theme.constants.ThemeConstants;
import com.maritz.culturenext.theme.dto.BrandingDto;
import com.maritz.culturenext.theme.dto.ColorsDto;
import com.maritz.culturenext.theme.dto.ImageUrlsDto;
import com.maritz.culturenext.theme.services.ThemeService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static com.maritz.culturenext.images.constants.FileImageTypes.PROJECT_BACKGROUND;
import static com.maritz.culturenext.images.constants.FileImageTypes.PROJECT_LOGO;

import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR;
import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR;
import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR;

@Component
public class ThemeServiceImpl implements ThemeService {
    private static String APPLICATION_LOGO_URL = "/rest/images/logo";
    private static String APPLICATION_BACKGROUND_URL = "/rest/images/background";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private FileImageService fileImageService;
    @Inject
    private Security security;
    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;


    @Inject
    public ThemeServiceImpl setFileImageService(FileImageService fileImageService) {
        this.fileImageService = fileImageService;
        return this;
    }

    @Inject
    public ThemeServiceImpl setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public ThemeServiceImpl setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    @Override
    public Files getLogo() {
        return fileImageService.searchForImage(FileImageTypes.PROJECT_LOGO);
    }

    @Override
    public Files getLogoLight() {
        return fileImageService.searchForImage(FileImageTypes.PROJECT_LOGO_LIGHT);
    }

    @Override
    public ResponseEntity<Object> getResponseLogo() {
        return fileImageService.prepareLogoForResponse(this.getLogo());
    }

    @Override
    public ResponseEntity<Object> getResponseLogoLight() {
        return fileImageService.prepareLogoLightForResponse(this.getLogoLight());
    }

    @Override
    public ResponseEntity<Object> getBackgroundResponse() {
        return fileImageService.prepareBackgroundForResponse(this.getBackground());
    }

    @Override
    public BrandingDto getBranding() {
        String logoUrl;
        String backgroundImageUrl;
        if (environmentUtil.readFromCdn()) {
            logoUrl = getCdnUrl(PROJECT_LOGO, APPLICATION_LOGO_URL);
            backgroundImageUrl = getCdnUrl(PROJECT_BACKGROUND, APPLICATION_BACKGROUND_URL);
        } else {
            logoUrl = APPLICATION_LOGO_URL;
            backgroundImageUrl = APPLICATION_BACKGROUND_URL;
        }

        return new BrandingDto()
                .setImageUrls(new ImageUrlsDto()
                        .setLogo(logoUrl)
                        .setBackground(backgroundImageUrl))
                .setColors(new ColorsDto()
                        .setPrimary(environmentUtil.getProperty(KEY_NAME_PRIMARY_COLOR, false))
                        .setSecondary(environmentUtil.getProperty(KEY_NAME_SECONDARY_COLOR, false))
                        .setText(environmentUtil.getProperty(KEY_NAME_TEXT_ON_COLOR, false)));
    }

    private String getCdnUrl(FileImageTypes fileImageType, String defaultUrl) {
        try {
            return gcpUtil.signUrl(fileImageService.searchForImage(fileImageType));
        } catch (Exception e) {
            logger.error("Failed to obtain " + fileImageType + " CDN URL", e);
            return defaultUrl;
        }
    }

    @Override
    public Files getBackground() {
        return fileImageService.searchForImage(FileImageTypes.PROJECT_BACKGROUND);
    }

    @Override
    public void uploadThemeImage(MultipartFile[] images, String type) throws Exception {
        List<ErrorMessage> errors = new ArrayList<>();

        if (images == null || images.length == 0) {
            errors.add(ImageConstants.IMAGE_MISSING_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else if (images.length > 1) {
            errors.add(ImageConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        MultipartFile image = images[0];

        if (image == null || image.isEmpty()) {
            errors.add(ImageConstants.IMAGE_MISSING_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            logger.info("a false was returned!");
            errors.add(ImageConstants.NOT_ADMIN_CODE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors).setStatus(HttpStatus.FORBIDDEN);
        }
        fileImageService.saveImageByType(image, type, null);
    }


    @Override
    public void deleteLogo() {
        try {
            fileImageService.deleteImage(getLogo().getId());
        }catch (Exception e){
            throw new ErrorMessageException(ThemeConstants.ERROR_DELETE_LOGO_MESSAGE);
        }
    }
}
