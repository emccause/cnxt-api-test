package com.maritz.culturenext.program.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the most common program information. Information includes ID, name,"
        + "description, type, category, fromDate and thruDate (program usage date range), creation date, status, "
        + "and creator information.")
public class ProgramSummaryDTO {
    private long programId;
    private String programName;
    private String programDescriptionShort;
    private String programDescriptionLong;
    private String programType;
    private String programCategory;
    private String createDate;
    private String fromDate;
    private String thruDate;        
    private String status;
    private EmployeeDTO creatorPax;
    private long likeCount;
    private Long imageId;
    private EmployeeDTO likePax;
    private AwardCodeProgramDetailsDTO awardCode;
    
    @ApiModelProperty(position = 12, required = false, value = "Count of pax that liked the program")
    public long getLikeCount() {
        return likeCount;
    }
    public void setLikeCount(long likeCount) {
        this.likeCount = likeCount;
    }

    @ApiModelProperty(position = 14, required = false, value = "Image ID of the corresponding Image to the Program")
    public Long getImageId() {
        return imageId;
    }
    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    @ApiModelProperty(position = 13, required = false, value = "logged in pax or latest Pax that liked the program")
    public EmployeeDTO getLikePax() {
        return likePax;
    }
    public void setLikePax(EmployeeDTO likePax) {
        this.likePax = likePax;
    }

    @ApiModelProperty(position = 1, required = false, value = "ID of the program")
    public long getProgramId() {
        return programId;
    }
    public void setProgramId(long programId) {
        this.programId = programId;
    }

    @ApiModelProperty(position = 2, required = true, value = "name of the program")
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @ApiModelProperty(position = 3, required = true, value = "short description of the program")
    public String getProgramDescriptionShort() {
        return programDescriptionShort;
    }
    public void setProgramDescriptionShort(String programDescriptionShort) {
        this.programDescriptionShort = programDescriptionShort;
    }

    @ApiModelProperty(position = 4, required = true, value = "type of the program")
    public String getProgramType() {
        return programType;
    }
    public void setProgramType(String programType) {
        this.programType = programType;
    }

    @ApiModelProperty(position = 5, required = true, value = "category of the program")
    public String getProgramCategory() {
        return programCategory;
    }
    public void setProgramCategory(String programCategory) {
        this.programCategory = programCategory;
    }

    @ApiModelProperty(position = 6, required = false, value = "date that program was created")
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @ApiModelProperty(position = 7, required = true, value = "date that program usage starts")
    public String getFromDate() {
        return fromDate;
    }
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    @ApiModelProperty(position = 8, required = false, value = "date that program usage ends")
    public String getThruDate() {
        return thruDate;
    }
    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }

    @ApiModelProperty(position = 9, required = true, value = "status of the program")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @ApiModelProperty(position = 10, required = false, 
            value = "information of the participant that created the program")
    public EmployeeDTO getCreatorPax() {
        return creatorPax;
    }
    public void setCreatorPax(EmployeeDTO creatorPax) {
        this.creatorPax = creatorPax;
    }

    @ApiModelProperty(position = 11, required = true, value = "long description of the program")
    public String getProgramDescriptionLong() {
        return programDescriptionLong;
    }
    public void setProgramDescriptionLong(String programDescriptionLong) {
        this.programDescriptionLong = programDescriptionLong;
    }
    
    @JsonInclude(Include.NON_NULL) 
    public AwardCodeProgramDetailsDTO getAwardCode() {
        return awardCode;
    }
    public void setAwardCode(AwardCodeProgramDetailsDTO awardCode) {
        this.awardCode = awardCode;
    }

}