package com.maritz.culturenext.program.dto;

public class ProgramEligibilityDTO {
    
    private RecognitionEligibilityDTO give;
    private RecognitionEligibilityDTO receive;
    
    public RecognitionEligibilityDTO getGive() {
        return give;
    }
    public void setGive(RecognitionEligibilityDTO give) {
        this.give = give;
    }
    public RecognitionEligibilityDTO getReceive() {
        return receive;
    }
    public void setReceive(RecognitionEligibilityDTO receive) {
        this.receive = receive;
    }
    
    

}
