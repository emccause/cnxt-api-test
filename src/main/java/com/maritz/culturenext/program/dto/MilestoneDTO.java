package com.maritz.culturenext.program.dto;

import org.springframework.stereotype.Component;

@Component
public class MilestoneDTO {
    private Long id;
    private String type;
    private Boolean repeat;
    private Double awardAmount;
    private Integer yearsOfService;
    private Long ecardId;
    private String publicHeadline;
    private String privateMessage;
    
    public Long getId() {
        return id;
    }
    public MilestoneDTO setId(Long id) {
        this.id = id;
        return this;
    }
    public Boolean isRepeat() {
        return repeat;
    }
    public MilestoneDTO setRepeat(Boolean repeat) {
        this.repeat = repeat;
        return this;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public MilestoneDTO setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
        return this;
    }
    public Integer getYearsOfService() {
        return yearsOfService;
    }
    public MilestoneDTO setYearsOfService(Integer yearsOfService) {
        this.yearsOfService = yearsOfService;
        return this;
    }
    public Long getEcardId() {
        return ecardId;
    }
    public MilestoneDTO setEcardId(Long ecardId) {
        this.ecardId = ecardId;
        return this;
    }
    public String getPublicHeadline() {
        return publicHeadline;
    }
    public MilestoneDTO setPublicHeadline(String publicHeadline) {
        this.publicHeadline = publicHeadline;
        return this;
    }
    public String getType() {
        return type;
    }
    public MilestoneDTO setType(String type) {
        this.type = type;
        return this;
    }
    public String getPrivateMessage() {
        return privateMessage;
    }
    public MilestoneDTO setPrivateMessage(String privateMessage) {
        this.privateMessage = privateMessage;
        return this;
    }
}
