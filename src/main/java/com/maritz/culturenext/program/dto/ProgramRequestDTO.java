package com.maritz.culturenext.program.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing a request for a Program. Contains program summary information, "
        + "basic program information, and basic information for defining eligibility and "
        + "budget assignments for the program.")
public class ProgramRequestDTO extends ProgramBasicDTO {
    private EligibilityStubDTO eligibility;
    private List<BudgetAssignmentStubDTO> budgetAssignments;
    private int reminderDays;

    @ApiModelProperty(position = 15, required = false, value = "Number of days that a reminder for pending approvals will be send out")
    public int getReminderDays(){ return  reminderDays; }
    public void setReminderDays(int reminderDays){ this.reminderDays = reminderDays; }

    @ApiModelProperty(position = 16, required = false, value = "eligibility definitions to set for the program")
    public EligibilityStubDTO getEligibility() {
        return eligibility;
    }
    public void setEligibility(EligibilityStubDTO eligibility) {
        this.eligibility = eligibility;
    }

    @ApiModelProperty(position = 17, required = false, value = "budget assignment definitions to set for the program")
    public List<BudgetAssignmentStubDTO> getBudgetAssignments() {
        return budgetAssignments;
    }
    public void setBudgetAssignments(List<BudgetAssignmentStubDTO> budgetAssignments) {
        this.budgetAssignments = budgetAssignments;
    }
}
