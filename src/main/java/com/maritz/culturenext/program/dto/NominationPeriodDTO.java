package com.maritz.culturenext.program.dto;

import java.time.LocalDate;

public class NominationPeriodDTO {
    private LocalDate fromDate;
    private LocalDate thruDate;

    public LocalDate getFromDateLocalDate() {
        return fromDate;
    }

    public String getFromDate() {
        return fromDate.toString();
    }

    public void setFromDate(String fromDate) { this.fromDate = LocalDate.parse(fromDate);
    }

    public LocalDate getThruDateLocalDate() {
        return thruDate;
    }

    public String getThruDate() {
        if (thruDate != null) {
            return thruDate.toString();
        }
        else {
            return null;
        }
    }

    public void setThruDate(String thruDate) {
        if (thruDate != null) { this.thruDate = LocalDate.parse(thruDate); }
    }

}
