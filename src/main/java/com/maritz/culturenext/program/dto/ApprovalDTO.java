package com.maritz.culturenext.program.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class ApprovalDTO {
    private String type;
    private EmployeeDTO pax;
    private Long level;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EmployeeDTO getPax() { return pax; }

    public void setPax(EmployeeDTO pax) { this.pax = pax; }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }
}
