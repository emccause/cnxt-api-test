package com.maritz.culturenext.program.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

import com.maritz.culturenext.budget.dto.AwardTierDTO;
import java.util.UUID;

@ApiModel(description = "DTO representing common basic program information (which is information regarding" +
        "ecards, recognition criteria, miscellaneous program information, and award tier amounts).")
public abstract class ProgramBasicDTO extends ProgramSummaryDTO {
    private List <Long> ecardIds;
    private List <Long> recognitionCriteriaIds;    
    private List <AwardTierDTO> awardTiers;
    private String programVisibility; //stored as ACL
    private String newsfeedVisibility; //stored as PROGRAM_misc
    private List <String> notification; //stored as PROGRAM_misc    
    private Boolean allowTemplateUpload;
    private String payoutType; //Stored as PROGRAM_MISC
    private String payoutDescription; // long description for the selected awardType
    private String payoutDisplayName; // displayName for the selected awardType
    private boolean pppIndexEnabled; //Stored as PROGRAM_MISC    
    private List<MilestoneDTO> milestones;
    private List<Integer> milestoneReminders;
    private PinnacleDTO pinnacle;
    private String programIdentifierByNomination;
    
    protected ProgramBasicDTO() { 	
    	this.programIdentifierByNomination = String.valueOf(UUID.randomUUID());
    }
    @ApiModelProperty(position = 13, required = false, 
            value = "IDs of recognition criterias associated with the program")
    public List<Long> getRecognitionCriteriaIds() {
        return recognitionCriteriaIds;
    }
    public void setRecognitionCriteriaIds(List<Long> recognitionCriteriaIds) {
        this.recognitionCriteriaIds = recognitionCriteriaIds;
    }

    @ApiModelProperty(position = 14, required = false, value = "ID of the ecards associated with the program")
    public List<Long> getEcardIds() {
        return ecardIds;
    }
    public void setEcardIds(List<Long> ecardIds) {
        this.ecardIds = ecardIds;
    }

    @ApiModelProperty(position = 15, required = false, value = "award tiers defined for this program")
    public List<AwardTierDTO> getAwardTiers() {
        return awardTiers;
    }
    
    public void setAwardTiers(List<AwardTierDTO> awardTiers) {
        this.awardTiers = awardTiers;
    }
    
    public String getProgramVisibility() {
        return programVisibility;
    }
    
    public void setProgramVisibility(String programVisibility) {
        this.programVisibility = programVisibility;
    }
    
    public String getNewsfeedVisibility() {
        return newsfeedVisibility;
    }
    
    public void setNewsfeedVisibility(String newsfeedVisibility) {
        this.newsfeedVisibility = newsfeedVisibility;
    }
    
    public List<String> getNotification() {
        return notification;
    }
    
    public void setNotification(List<String> notification) {
        this.notification = notification;
    }
    
    public Boolean getAllowTemplateUpload() {
        return allowTemplateUpload;
    }
    
    public void setAllowTemplateUpload(Boolean allowTemplateUpload) {
        this.allowTemplateUpload = allowTemplateUpload;
    }
    
    public String getPayoutType() {
        return payoutType;
    }
    
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getPayoutDescription() {
        return payoutDescription;
    }

    public void setPayoutDescription(String payoutDescription) {
        this.payoutDescription = payoutDescription;
    }
    
    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }
    
    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
    
    public boolean isPppIndexEnabled() {
        return pppIndexEnabled;
    }

    public void setPppIndexEnabled(boolean pppIndexEnabled) {
        this.pppIndexEnabled = pppIndexEnabled;
    }
	
	public List<MilestoneDTO> getMilestones() {
        return milestones;
    }
    public void setMilestones(List<MilestoneDTO> milestones) {
        this.milestones = milestones;
    }

    public List<Integer> getMilestoneReminders() {
        return milestoneReminders;
    }
    public void setMilestoneReminders(List<Integer> milestoneReminders) {
        this.milestoneReminders = milestoneReminders;
    }

    public PinnacleDTO getPinnacle() { return pinnacle; }

    public void setPinnacle(PinnacleDTO pinnacle) { this.pinnacle = pinnacle; }
	
    public String getProgramIdentifierByNomination() {
		return programIdentifierByNomination;
	}
    
	public void setProgramIdentifierByNomination(String programIdentifierByNomination) {
		this.programIdentifierByNomination = programIdentifierByNomination;
	}
	
}