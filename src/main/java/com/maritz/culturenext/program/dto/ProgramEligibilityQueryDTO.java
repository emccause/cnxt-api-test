package com.maritz.culturenext.program.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

public class ProgramEligibilityQueryDTO {
    
    List<String> programCategory;
    List<GroupMemberStubDTO> receivers;

    public List<String> getProgramCategory() {
        return programCategory;
    }

    public ProgramEligibilityQueryDTO setProgramCategory(List<String> programCategory) {
        this.programCategory = programCategory;
        return this;
    }

    public List<GroupMemberStubDTO> getReceivers() {
        return receivers;
    }

    public ProgramEligibilityQueryDTO setReceivers(List<GroupMemberStubDTO> receivers) {
        this.receivers = receivers;
        return this;
    }

}
