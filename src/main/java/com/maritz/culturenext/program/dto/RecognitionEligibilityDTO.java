package com.maritz.culturenext.program.dto;

public class RecognitionEligibilityDTO {
    
    private Boolean recognition;
    private Boolean awardCodeCert;
    private Boolean awardCodeDownload;
    private Boolean pointLoad;
    
    public Boolean getRecognition() {
        return recognition;
    }
    
    public RecognitionEligibilityDTO setRecognition(Boolean recognition) {
        this.recognition = recognition;
        return this;
    }
    
    public Boolean getAwardCodeCert() {
        return awardCodeCert;
    }
    
    public RecognitionEligibilityDTO setAwardCodeCert(Boolean awardCodeCert) {
        this.awardCodeCert = awardCodeCert;
        return this;
    }
    
    public Boolean getAwardCodeDownload() {
        return awardCodeDownload;
    }
    
    public RecognitionEligibilityDTO setAwardCodeDownload(Boolean awardCodeDownload) {
        this.awardCodeDownload = awardCodeDownload;
        return this;
    }

    public Boolean getPointLoad() {
        return pointLoad;
    }

    public void setPointLoad(Boolean pointLoad) {
        this.pointLoad = pointLoad;
    }

}
