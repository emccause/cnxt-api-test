package com.maritz.culturenext.program.dto;

import java.util.List;

import com.maritz.core.jpa.entity.FileItem;


public class ProgramFilesDistinctTypesDTO {
    private FileItem programFileImage;
    private FileItem programFileIcon;
    private List<FileItem> programFileCertificates;
    public FileItem getProgramFileImage() {
        return programFileImage;
    }
    public void setProgramFileImage(FileItem programFileImage) {
        this.programFileImage = programFileImage;
    }
    public FileItem getProgramFileIcon() {
        return programFileIcon;
    }
    public void setProgramFileIcon(FileItem programFileIcon) {
        this.programFileIcon = programFileIcon;
    }
    public List<FileItem> getProgramFileCertificates() {
        return programFileCertificates;
    }
    public void setProgramFileCertificates(List<FileItem> programFileCertificates) {
        this.programFileCertificates = programFileCertificates;
    }
}
