package com.maritz.culturenext.program.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.EntityDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the eligibility information for a program (with both receive and give " +
        "eligibility information) which includes either full participant or group information.")
public class EligibilityDTO {
    private List<EntityDTO> receive = new ArrayList<EntityDTO>();
    private List<EntityDTO> give = new ArrayList<EntityDTO>();

    @ApiModelProperty(position = 1, required = false, value = "receiving eligibility definition")
    public List<EntityDTO> getReceive() {
        return receive;
    }
    public void setReceive(List<EntityDTO> receive) {
        this.receive = receive;
    }

    @ApiModelProperty(position = 1, required = false, value = "giving eligibility definition")
    public List<EntityDTO> getGive() {
        return give;
    }
    public void setGive(List<EntityDTO> give) {
        this.give = give;
    }
}