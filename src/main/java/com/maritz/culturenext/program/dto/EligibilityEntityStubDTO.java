package com.maritz.culturenext.program.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.EntityDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the ID of the entity (group or participant) when defining eligibility for "
        + "a program (either for receive or give eligibility) for a request.")
public class EligibilityEntityStubDTO extends EntityDTO {

    private Long groupId;
    private Long paxId;

    @ApiModelProperty(position = 1, required = false, value = "ID of the group")
    public Long getGroupId() {
        return groupId;
    }
    public EligibilityEntityStubDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }

    @ApiModelProperty(position = 2, required = false, value = "ID of the participant")
    public Long getPaxId() {
        return paxId;
    }
    public EligibilityEntityStubDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }
}