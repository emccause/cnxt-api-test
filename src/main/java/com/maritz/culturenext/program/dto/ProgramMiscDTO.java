package com.maritz.culturenext.program.dto;

public class ProgramMiscDTO {

    private Long programId;
    private Long programMiscId;
    private String vfName;
    private String miscData;
    
    public Long getProgramId() {
        return programId;
    }
    public void setProgramId(Long programId) {
        this.programId = programId;
    }
    public Long getProgramMiscId() {
        return programMiscId;
    }
    public void setProgramMiscId(Long programMiscId) {
        this.programMiscId = programMiscId;
    }
    public String getVfName() {
        return vfName;
    }
    public void setVfName(String vfName) {
        this.vfName = vfName;
    }
    public String getMiscData() {
        return miscData;
    }
    public void setMiscData(String miscData) {
        this.miscData = miscData;
    }
    
}
