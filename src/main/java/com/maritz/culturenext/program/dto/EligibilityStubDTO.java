package com.maritz.culturenext.program.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the eligibility definition for a program (with both receive and give " +
        "eligibility user information) when making a request.")
public class EligibilityStubDTO {
    private List<EligibilityEntityStubDTO> receive = new ArrayList<EligibilityEntityStubDTO>();
    private List<EligibilityEntityStubDTO> give = new ArrayList<EligibilityEntityStubDTO>();

    @ApiModelProperty(position = 1, required = false, value = "receiving eligibility definition")
    public List<EligibilityEntityStubDTO> getReceive() {
        return receive;
    }
    public void setReceive(List<EligibilityEntityStubDTO> receive) {
        this.receive = receive;
    }

    @ApiModelProperty(position = 2, required = false, value = "giving eligibility definition")
    public List<EligibilityEntityStubDTO> getGive() {
        return give;
    }
    public void setGive(List<EligibilityEntityStubDTO> give) {
        this.give = give;
    }
}
