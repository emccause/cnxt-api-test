package com.maritz.culturenext.program.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO containing the entire information for a particular program. "
        + "Includes summary information, basic program information, eligibility information, "
        + "budget assignment information, and program usage indicator.")
public class ProgramFullDTO extends ProgramBasicDTO {
    private EligibilityDTO eligibility;
    private List<BudgetAssignmentDTO> budgetAssignments;
    private Boolean programUsed;

    public ProgramFullDTO(){super();}
    @ApiModelProperty(position = 16, required = false, value = "eligibility information defined for the program")
    public EligibilityDTO getEligibility() {
        return eligibility;
    }
    public void setEligibility(EligibilityDTO eligibility) {
        this.eligibility = eligibility;
    }

    @ApiModelProperty(position = 17, required = false, value = "budget assignment information defined for the program")
    public List<BudgetAssignmentDTO> getBudgetAssignments() {
        return budgetAssignments;
    }
    public void setBudgetAssignments(List<BudgetAssignmentDTO> budgetAssignments) {
        this.budgetAssignments = budgetAssignments;
    }

    public boolean isProgramUsed() {
        return programUsed;
    }
    public void setProgramUsed(boolean programUsed) {
        this.programUsed = programUsed;
    }
}
