package com.maritz.culturenext.program.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class TransactionDTO {
    
    private Long nominationId;
    private Long transactionId;
    private String createDate;
    private Double awardAmount;
    private String payoutType;
    private String programName;
    private String status;
    private String headline;
    private String comment;
    private String reversalComment;
    private EmployeeDTO fromPax;
    private EmployeeDTO toPax;
    private EmployeeDTO approverPax;
    
    public Long getNominationId() {
        return nominationId;
    }
    public void setNominationId(Long nominationId) {
        this.nominationId = nominationId;
    }
    public Long getTransactionId() {
        return transactionId;
    }
    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getHeadline() {
        return headline;
    }
    public void setHeadline(String headline) {
        this.headline = headline;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getReversalComment() {
        return reversalComment;
    }
    public void setReversalComment(String reversalComment) {
        this.reversalComment = reversalComment;
    }
    public EmployeeDTO getFromPax() {
        return fromPax;
    }
    public void setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
    }
    public EmployeeDTO getToPax() {
        return toPax;
    }
    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }
    public EmployeeDTO getApproverPax() {
        return approverPax;
    }
    public void setApproverPax(EmployeeDTO approverPax) {
        this.approverPax = approverPax;
    }

}
