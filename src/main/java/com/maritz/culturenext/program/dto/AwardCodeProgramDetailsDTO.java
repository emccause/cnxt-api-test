package com.maritz.culturenext.program.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AwardCodeProgramDetailsDTO {
    
    private String claimingInstructions;//stored as PROGRAM_misc
    private Boolean printable;             //stored as PROGRAM_misc
    private Boolean downloadable;        //stored as PROGRAM_misc
    private Long iconId;
    private List<Certificates> certificates;
    
    public List<Certificates> getCertificates() {
        return certificates;
    }
    public void setCertificates(List<Certificates> certificates) {
        this.certificates = certificates;
    }
    @JsonProperty("isPrintable")
    public Boolean isPrintable() {
        return printable != null ? printable : Boolean.FALSE ;
    }
    public void setPrintable(Boolean printable) {
        this.printable = printable;
    }
    
    @JsonProperty("isDownloadable")
    public Boolean isDownloadable() {
        return downloadable != null ? downloadable : Boolean.FALSE ;
    }
    public void setDownloadable(Boolean downloadable) {
        this.downloadable = downloadable;
    }

    public String getClaimingInstructions() {
        return claimingInstructions;
    }
    public void setClaimingInstructions(String claimingInstructions) {
        this.claimingInstructions = claimingInstructions;
    }
    public Long getIconId() {
        return iconId;
    }
    public void setIconId(Long iconId) {
        this.iconId = iconId;
    }

}
