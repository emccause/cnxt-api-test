package com.maritz.culturenext.program.dto;

public class AwardsDTO {
    private Double individualAmount;
    private Double teamAmountPerPerson;
    private Double teamAmountPerTeam;

    public Double getIndividualAmount() {
        return individualAmount;
    }

    public void setIndividualAmount(Double individualAmount) {
        this.individualAmount = individualAmount;
    }

    public Double getTeamAmountPerPerson() {
        return teamAmountPerPerson;
    }

    public void setTeamAmountPerPerson(Double teamAmountPerPerson) {
        this.teamAmountPerPerson = teamAmountPerPerson;
    }

    public Double getTeamAmountPerTeam() {
        return teamAmountPerTeam;
    }

    public void setTeamAmountPerTeam(Double teamAmountPerTeam) {
        this.teamAmountPerTeam = teamAmountPerTeam;
    }
}
