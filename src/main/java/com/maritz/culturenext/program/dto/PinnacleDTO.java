package com.maritz.culturenext.program.dto;

import com.maritz.survey.dto.QuestionDTO;

import java.util.List;

public class PinnacleDTO {
    private String audience;
    private String nominationInstructions;
    private List<QuestionDTO> questions;
    private AwardsDTO awards;
    private List<ApprovalDTO> approvals;
    private List<String> notifications;
    private String newsfeedVisibilityNominated;
    private String newsfeedVisibilityApproved;
    private List<NominationPeriodDTO> nominationPeriods;
    private Long testId;

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getNominationInstructions() {
        return nominationInstructions;
    }

    public void setNominationInstructions(String nominationInstructions) {
        this.nominationInstructions = nominationInstructions;
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public AwardsDTO getAwards() {
        return awards;
    }

    public void setAwards(AwardsDTO awards) {
        this.awards = awards;
    }

    public List<ApprovalDTO> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<ApprovalDTO> approvals) {
        this.approvals = approvals;
    }

    public List<String> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<String> notifications) {
        this.notifications = notifications;
    }

    public String getNewsfeedVisibilityNominated() {
        return newsfeedVisibilityNominated;
    }

    public void setNewsfeedVisibilityNominated(String newsfeedVisibilityNominated) {
        this.newsfeedVisibilityNominated = newsfeedVisibilityNominated;
    }

    public String getNewsfeedVisibilityApproved() {
        return newsfeedVisibilityApproved;
    }

    public void setNewsfeedVisibilityApproved(String newsfeedVisibilityApproved) {
        this.newsfeedVisibilityApproved = newsfeedVisibilityApproved;
    }

    public List<NominationPeriodDTO> getNominationPeriods() {
        return nominationPeriods;
    }

    public void setNominationPeriods(List<NominationPeriodDTO> nominationPeriods) {
        this.nominationPeriods = nominationPeriods;
    }

    public Long getTestId() {
        return testId;
    }

    public void setTestId(Long testId) {
        this.testId = testId;
    }
}
