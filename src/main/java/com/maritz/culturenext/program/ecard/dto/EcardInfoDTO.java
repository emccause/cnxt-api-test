package com.maritz.culturenext.program.ecard.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EcardInfoDTO {

    private Long id;
    private String displayName;
    private String description;
    private String displayType;
    
    public Long getId() {
        return id;
    }
    
    public EcardInfoDTO setId(Long id) {
        this.id = id;
        return this;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public EcardInfoDTO setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public EcardInfoDTO setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public String getDisplayType() {
        return displayType;
    }
    
    public EcardInfoDTO setDisplayType(String displayType) {
        this.displayType = displayType;
        return this;
    }
    
}
