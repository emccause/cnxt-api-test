package com.maritz.culturenext.program.ecard.services;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.program.ecard.dto.EcardInfoDTO;

public interface EcardService {
    
    /**
     * Return all ecards in the system that are ACTIVE.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9797653/Ecards+Example
     */
    List<Map<String,Object>> getInfoForAllActiveEcards();

    /**
     * Upload a new ecard image
     * 
     * @param images
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9797653/Ecards+Example
     */
    Map<String, Object> uploadEcard(MultipartFile[] images) throws Exception;

    /**
     * Return an ecard image.
     * 
     * @param ecardId - ID of ecard to return
     * @param size - Size of image to return. Can be 'full' or 'thumbnail'. Default is 'full'
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9797653/Ecards+Example
     */
    ResponseEntity<Object> getImage(Long ecardId, String size);

    Files getECardImage(long eCardId, String size);

    Files getECardImage(long eCardId);

    /**
     * Update an ecard. You can update name, description, and displayType (STOCK or CUSTOM)
     * 
     * @param ecardId
     * @param ecardInfo
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9797653/Ecards+Example
     */
    Map<String, Object> updateEcardInfo(Long ecardId, EcardInfoDTO ecardInfo);

    /**
     * Inactivate an existing ecard
     * 
     * @param ecardId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9797653/Ecards+Example
     */
    void inactiveCustomEcard(Long ecardId);

    /**
     * Updates ecards' images
     * @param ecardInfoLst
     * @param images
     * @return
     */

    Map<String, Object> updateEcardsImages(EcardImageDTO[] ecardInfoLst, MultipartFile[] images) throws Exception;
}
