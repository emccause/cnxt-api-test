package com.maritz.culturenext.program.ecard.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.program.ecard.dao.EcardDao;

@Repository
public class EcardDaoImpl extends AbstractDaoImpl implements EcardDao {

    private static final String ECARD_ID_SQL_PARAM = "ECARD_ID";
    private static final String ECARD_ASSIGNED_TO_PROGRAM = "assignedToProgram";
    private static final String GET_ACTIVE_INFO_QUERY = 
            "SELECT ECARD.ID, ECARD_NAME AS DISPLAY_NAME, DESCRIPTION, ECARD_TYPE_CODE AS DISPLAY_TYPE, ISNULL(("
                    + "SELECT TOP 1 'TRUE' "
                    + "FROM PROGRAM_ECARD "
                    + "WHERE ECARD_ID = ECARD.ID),'FALSE')"
            + "AS ASSIGNED_TO_PROGRAM, STATUS_TYPE_CODE AS STATUS "
            + "FROM ECARD WHERE STATUS_TYPE_CODE = 'ACTIVE'"; // after UDM changes
    private static final String GET_INFO_BY_ID_QUERY = 
            "SELECT ID, ECARD_NAME AS DISPLAY_NAME, DESCRIPTION, ECARD_TYPE_CODE AS DISPLAY_TYPE, ISNULL(("
                    + "SELECT TOP 1 'TRUE' "
                    + "FROM PROGRAM_ECARD "
                    + "WHERE ECARD_ID = ECARD.ID),'FALSE') "
            + "AS ASSIGNED_TO_PROGRAM" 
            + " FROM component.ECARD WHERE ID = :" + ECARD_ID_SQL_PARAM;
    
    private static final String ECARD_FILE_TYPE_SQL_PARAM = "ECARD_FILE_TYPE";
    private static final String GET_ECARD_FILE_QUERY = "SELECT TOP 1 F.*"
            + " FROM FILES F" 
            + " INNER JOIN FILE_TYPE FT" 
            + " ON FT.FILE_TYPE_CODE = F.FILE_TYPE_CODE" 
            + " INNER JOIN ECARD_FILES EF" 
            + " ON EF.FILES_ID = F.ID" 
            + " WHERE FT.FILE_TYPE_NAME = :" + ECARD_FILE_TYPE_SQL_PARAM
            + " AND EF.ECARD_ID = :" + ECARD_ID_SQL_PARAM;
    
    private static final String TYPE_SQL_PARAM = "TYPE";
    private static final String GET_FILE_TYPE_QUERY = 
            "SELECT FT.FILE_TYPE_CODE "
            + "FROM FILE_TYPE FT "
            + "WHERE FT.FILE_TYPE_NAME = ('Ecard Image ' + :" + TYPE_SQL_PARAM + ")";

    @Override
    public List<Map<String, Object>> getInfoForAllActive() {
        
        MapSqlParameterSource params = new MapSqlParameterSource();

        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(GET_ACTIVE_INFO_QUERY, params, new CamelCaseMapRowMapper());
            
        for (Map<String,Object> node: nodes) {
            if (node.get(ECARD_ASSIGNED_TO_PROGRAM).equals("TRUE")) {
                node.put(ECARD_ASSIGNED_TO_PROGRAM, true);
            } else {
                node.put(ECARD_ASSIGNED_TO_PROGRAM, false);
            }
        }

        return nodes;
    }
    
    @Override
    public Map<String, Object> getInfoForEcardId(Long ecardId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ECARD_ID_SQL_PARAM, ecardId);
        
        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(GET_INFO_BY_ID_QUERY, params, new CamelCaseMapRowMapper());
        if (nodes != null && !nodes.isEmpty()) {

            if (nodes.get(0).get(ECARD_ASSIGNED_TO_PROGRAM).equals("TRUE")) {
                nodes.get(0).put(ECARD_ASSIGNED_TO_PROGRAM, true);
            } else {
                nodes.get(0).put(ECARD_ASSIGNED_TO_PROGRAM, false);
            }
            return nodes.get(0);
        }
        
        return null;
    }

    @Override
    public Files getEcard(Long ecardId, String size) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ECARD_ID_SQL_PARAM, ecardId);
        params.addValue(ECARD_FILE_TYPE_SQL_PARAM, "Ecard Image " + size);
        
        List<Files> nodes =
                namedParameterJdbcTemplate.query(GET_ECARD_FILE_QUERY, params, new ConcentrixBeanRowMapper<Files>().setBeanClass(Files.class)
                        .setTable(jitBuilder.getTable(Files.class))
                        .build());
            
        if (nodes != null && !nodes.isEmpty()) {
            return nodes.get(0);
        }
        
        return null;
    }

    @Override
    public String getImageTypeCodeByType(String type) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(TYPE_SQL_PARAM, type);

        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(GET_FILE_TYPE_QUERY, params, new ColumnMapRowMapper());
        return (String) nodes.get(0).get("FILE_TYPE_CODE");

    }

}
