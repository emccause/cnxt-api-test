package com.maritz.culturenext.program.ecard.dao;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Files;

public interface EcardDao {
    
    List<Map<String,Object>> getInfoForAllActive();
    Map<String, Object> getInfoForEcardId(Long ecardId);
    Files getEcard(Long ecardId, String size);
    String getImageTypeCodeByType(String type);

}
