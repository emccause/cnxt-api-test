package com.maritz.culturenext.program.ecard.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

public class EcardImagesDictDTO {
    private Map<String, EcardImageDTO> metadataDict;
    private Map<String, MultipartFile> imagesDict;

    public void setMetadataDict(Map<String, EcardImageDTO> metadataDict) {
        this.metadataDict = metadataDict;
    }

    public Map<String, EcardImageDTO> getMetadataDict() {
        return metadataDict;
    }

    public void setImagesDict(Map<String, MultipartFile> imagesDict) {
        this.imagesDict = imagesDict;
    }

    public Map<String, MultipartFile> getImagesDict() {
        return imagesDict;
    }
}
