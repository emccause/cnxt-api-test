package com.maritz.culturenext.program.ecard.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.ECARD_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.IMAGE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.SIZE_REST_PARAM;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.rest.NotFoundException;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.program.ecard.dto.EcardInfoDTO;
import com.maritz.culturenext.program.ecard.services.EcardService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("ecards")
@Api(value="/ecards", description="All end points dealing with user profiles")
public class EcardRestService {
    private final Logger log = LoggerFactory.getLogger(EcardRestService.class);

    @Inject private EcardService ecardService;
    @Inject private Security security;

    //rest/ecards
    @RequestMapping(method = RequestMethod.GET)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC)
    @ApiOperation(value="Returns list of all Ecards configured for a project that are active.")
    public List<Map<String, Object>> getInfoForAllActiveEcards() {
        return ecardService.getInfoForAllActiveEcards();
    }

    //rest/ecards/~
    @GetMapping("{ecardId}")
    // Purposefully left as public access, since this is used for images in e-mails by clients who do not have their images migrated to the CDN.
    @SecurityPolicy(authenticated=false)
    @ApiOperation(value = "Fetch an ecard image.",
        notes="values for 'size' are 'thumbnail', 'full', default is full size<br><br>"
            + "thumbnail is 480x320 and full is 1000x667<br><br>"
            + "Auth-Token is not required as this is a public URL")
    public ResponseEntity<Object> getEcard(
        @ApiParam(value = "Id of the ecard you want to retrieve")
        @PathVariable(ECARD_ID_REST_PARAM) String ecardIdString,
        @ApiParam(value = "Requested size, values are:<br><br>Full(default)<br>tiny")
        @RequestParam(value=SIZE_REST_PARAM, required = false) String size
    ) throws Exception {
        Long ecardId = security.getId(ecardIdString, null);
        if (ecardId == null) {
            throw new NotFoundException(ECARD_ID_REST_PARAM);
        }
        return ecardService.getImage(ecardId, size);
    }

    //rest/ecards
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @RequestMapping( method = RequestMethod.POST)
    @ApiOperation(value="Ecard upload for CUSTOM ecard types")
    public Map<String,Object> uploadEcard(
        @ApiParam(value="Image you want to upload for the ecard")
        @RequestParam(IMAGE_REST_PARAM) MultipartFile[] images
    ) throws Exception{
        return ecardService.uploadEcard(images);
    }

    //rest/ecards/upload
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false,roles="MTZ:ADM,CADM")
    @RequestMapping(value = "/upload",  method = RequestMethod.PUT)
    @ApiOperation(value="update Ecards with Images - This is only for Maritz admin")
    public Map<String, Object> updateEcardsImages(
            @ApiParam(value="vector of images that are going to be updated.")
            @RequestPart("request") EcardImageDTO[] ecardInfoLst,
            @RequestPart(IMAGE_REST_PARAM) MultipartFile[] images
    ) throws Exception {
        log.info("data found:"+ ReflectionToStringBuilder.toString(ecardInfoLst));
        log.info("images found: "+ images.length);

        return ecardService.updateEcardsImages(ecardInfoLst,images);
    }

    //rest/ecards/~/info
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @RequestMapping(value = "{ecardId}/info",  method = RequestMethod.PUT)
    @ApiOperation(value="Ecard update - This is only for Maritz admin")
    public Map<String, Object> updateEcard(
        @ApiParam(value="Id of the ecard you want to update")
        @PathVariable(ECARD_ID_REST_PARAM) String ecardIdString,
        @ApiParam(value="the body containing the changes you want to make for the specified ecard")
        @RequestBody EcardInfoDTO ecardInfo
    ) throws Exception {
        Long ecardId = security.getId(ecardIdString, null);
        if(ecardId == null) throw new NoHandlerFoundException(null, null, null);
        return ecardService.updateEcardInfo(ecardId, ecardInfo);
    }

    //rest/ecards/~
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false, roles="MTZ:ADM,CADM")
    @RequestMapping(value = "{ecardId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete (set to INACTIVE) an ecard image.",
        notes = "cannot delete ecard if assignedToProgram = true<br><br>"
            + "Must be admin to delete<br><br>Must be CUSTOM type")
    public void inactiveEcard(
        @ApiParam(value = "id of the ecard you want to delete")
        @PathVariable(ECARD_ID_REST_PARAM) String ecardIdString
    ) throws Exception {
        Long ecardId = security.getId(ecardIdString, null);
        if(ecardId == null) throw new NoHandlerFoundException(null, null, null);
        ecardService.inactiveCustomEcard(ecardId);
    }

}
