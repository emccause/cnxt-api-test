package com.maritz.culturenext.program.ecard.services.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Ecard;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.ProgramEcard;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.images.validators.EcardFileImageValidator;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.ecard.businessobject.EcardBO;
import com.maritz.culturenext.program.ecard.dao.EcardDao;
import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import com.maritz.culturenext.program.ecard.dto.EcardImagesDictDTO;
import com.maritz.culturenext.program.ecard.dto.EcardInfoDTO;
import com.maritz.culturenext.program.ecard.services.EcardService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.*;

import static com.maritz.culturenext.images.constants.FileImageConstants.ERROR_ECARD_NAME_LENGTH;
import static com.maritz.culturenext.images.constants.FileImageConstants.ERROR_ECARD_NAME_LENGTH_MSG;

@Component
public class EcardServiceImpl implements EcardService {
    private final Logger log = LoggerFactory.getLogger(getClass());

    @Inject @Named("ecardDao") private ConcentrixDao<Ecard> concentrixEcardDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<ProgramEcard> programEcardDao;
    @Inject private EcardDao ecardDao;
    @Inject private FileImageService fileImageService;
    @Inject private EcardBO  ecardBO;
    @Inject
    private EcardFileImageValidator ecardFileImageValidator;

    public static final int MAX_ECARD_NAME_LENGTH = 50;
    private static final int MAX_ECARD_DESCRIPTION_LENGTH = 100;

    public static final int ECARD_DEFAULT_WIDTH = 735;
    public static final int ECARD_DEFAULT_HEIGHT = 490;

    public static final int ECARD_THUMBNAIL_WIDTH = 480;
    public static final int ECARD_THUMBNAIL_HEIGHT = 320;

    private static final String DEFAULT_IMAGE_SIZE = "default";
    private static final Set<String> SUPPORTED_IMAGE_SIZES = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(DEFAULT_IMAGE_SIZE, "thumbnail")));
    private Map<String, String> fileTypesMappedByImageSize;

    public static final String ECARD_STOCK_TYPE = "STOCK";
    public static final String ECARD_CUSTOM_TYPE = "CUSTOM";

    // error codes
    private static final String ERROR_ECARD_MISSING = "ECARD_MISSING";
    private static final String ERROR_ECARD_MISSING_MSG = "Must upload an ecard.";

    private static final String ERROR_ECARD_IMAGES_WRONG_DATA = "ECARD_IMAGES_WRONG_DATA";

    public static final String ERROR_ECARD_TOO_SMALL = "ECARD_TOO_SMALL";
    public static final String ERROR_ECARD_TOO_SMALL_MSG =
            "Minimum ecard size is " + ECARD_DEFAULT_WIDTH + " by " + ECARD_DEFAULT_HEIGHT + ".";
    private static final String ERROR_ECARD_DESCRIPTION_LENGTH = "ECARD_DESCRIPTION_LENGTH";
    private static final String ERROR_ECARD_DESCRIPTION_LENGTH_MSG =
            "Description is too long.  Max length is " + MAX_ECARD_DESCRIPTION_LENGTH;

    private static final String ERROR_ECARD_NOT_EXIST = "ERROR_ECARD_NOT_EXIST";
    private static final String ERROR_ECARD_NOT_EXIST_MSG = "Ecard Id not exist.";
    private static final String ERROR_ECARD_TYPE = "ECARD_TYPE";
    private static final String ERROR_ECARD_TYPE_MSG = "Ecard must be CUSTOM type.";
    private static final String ERROR_ECARD_ASSIGNED_TO_PROGRAM = "ECARD_ASSIGNED_TO_PROGRAM";
    private static final String ERROR_ECARD_ASSIGNED_TO_PROGRAM_MSG = "Ecard is assigned to a program.";


    @PostConstruct
    public void postConstruct() {
        fileTypesMappedByImageSize = new HashMap<>(SUPPORTED_IMAGE_SIZES.size());
        for (String supportedImageSize : SUPPORTED_IMAGE_SIZES) {
            fileTypesMappedByImageSize.put(supportedImageSize.toLowerCase(), ecardDao.getImageTypeCodeByType(supportedImageSize));
        }
        fileTypesMappedByImageSize = Collections.unmodifiableMap(fileTypesMappedByImageSize);
    }

    @Override
    public List<Map<String, Object>> getInfoForAllActiveEcards() {
        return ecardDao.getInfoForAllActive();
    }

    @Override
    public ResponseEntity<Object> getImage(Long eCardId, String size) {
        Files eCardImage = getECardImage(eCardId, size);
        if (eCardImage == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Ecard eCard = concentrixEcardDao.findById(eCardId);
        return fileImageService.prepareProfileImageForResponse (eCard.getEcardName(), eCardImage);
    }

    @Override
    public Files getECardImage(long eCardId, String size) {
        if (size == null || !SUPPORTED_IMAGE_SIZES.contains(size.toLowerCase())){
            size = DEFAULT_IMAGE_SIZE;
        }
        size = size.toLowerCase();

        List<Files> filesList = fileImageService.getFilesByTargetId(TableName.ECARD.name(), eCardId);
        if (CollectionUtils.isEmpty(filesList)) {
            return null;
        }

        String fileTypeCode = fileTypesMappedByImageSize.get(size);
        Optional<Files> eCardImage = filesList.stream().filter(item -> fileTypeCode.equalsIgnoreCase(item.getFileTypeCode())).findFirst();
        return (eCardImage.isPresent())? eCardImage.get():null;
    }

    @Override
    public Files getECardImage(long eCardId) {
        return getECardImage(eCardId, null);
    }

    /**
     * always create a new image, since the name is created with the ecard name plus the current server time.
     * @param images
     *
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> uploadEcard(MultipartFile[] images) throws Exception {
        ArrayList<ErrorMessage> errors = new ArrayList<>();

        if (images == null || images.length == 0) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_ECARD_MISSING)
                    .setField(ProjectConstants.UPLOAD)
                    .setMessage(ERROR_ECARD_MISSING_MSG));
        } else if (images.length > 1) {
            errors.add(ProgramActivityConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        if(!errors.isEmpty()){
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        MultipartFile image = images[0];
        Ecard ecard = new Ecard();

        // ecard name validation
        String ecardName = image.getOriginalFilename();
        if(ecardName == null || ecardName.trim().equals(ProjectConstants.EMPTY_STRING)){
            errors.add(ProgramActivityConstants.ECARD_NAME_MISSING_MESSAGE);
        }
        else if(ecardName.length() > EcardServiceImpl.MAX_ECARD_NAME_LENGTH){
            errors.add(new ErrorMessage()
                    .setCode(ERROR_ECARD_NAME_LENGTH)
                    .setField(ProjectConstants.DISPLAY_NAME)
                    .setMessage(ERROR_ECARD_NAME_LENGTH_MSG));
        }
        else{
            ecardName = ecardName + "-" + new Date().getTime();
            // check if the ecard info has already been entered - shouldn't happen but just to be safe
            Ecard existingEcard = concentrixEcardDao.findBy()
                    .where(ProjectConstants.ECARD_NAME).eq(ecardName)
                    .findOne();
            if(existingEcard != null) {
                errors.add(ProgramActivityConstants.DUPLICATE_ECARD_NAME_MESSAGE);
            } else {
                ecard.setEcardName(ecardName);
            }
        }

        try{
            ecardFileImageValidator.validateImageForUpload(image, FileImageTypes.ECARD_DEFAULT.name(), null);
        }catch (ErrorMessageException error) {
            throw error;
        }
        ecard.setEcardTypeCode(EcardServiceImpl.ECARD_CUSTOM_TYPE);
        ecard.setImageUrl("TODO");
        ecard.setThumbnailUrl("TODO");
        ecard.setStatusTypeCode(StatusTypeCode.ACTIVE.name());

        concentrixEcardDao.save(ecard);
        fileImageService.saveImageByType(image, FileImageTypes.ECARD_DEFAULT.name(), ecard.getId());

        return  ecardDao.getInfoForEcardId(ecard.getId());
    }

    @Override
    public Map<String, Object> updateEcardInfo(Long ecardId, EcardInfoDTO ecardInfo){
        ArrayList<ErrorMessage> errors = new ArrayList<>();

        if(ecardInfo == null) {
            throw new ErrorMessageException(ProgramActivityConstants.ECARD_INFO_MISSING_MESSAGE);
        }
        Ecard ecard = new Ecard();

        // ecard name validation
        String ecardName = ecardInfo.getDisplayName();
        if(ecardName == null || ecardName.trim().equals(ProjectConstants.EMPTY_STRING)){
            errors.add(ProgramActivityConstants.ECARD_NAME_MISSING_MESSAGE);
        }
        else if(ecardName.length() > MAX_ECARD_NAME_LENGTH){
            errors.add(new ErrorMessage()
                    .setCode(FileImageConstants.ERROR_ECARD_NAME_LENGTH)
                    .setField(ProjectConstants.DISPLAY_NAME)
                    .setMessage(FileImageConstants.ERROR_ECARD_NAME_LENGTH_MSG));
        }
        else{
            // check if the ecard info has already been entered
            Ecard existingEcard = concentrixEcardDao.findBy()
                    .where(ProjectConstants.ECARD_NAME).eq(ecardName)
                    .findOne();
            if(existingEcard != null) {
                errors.add(ProgramActivityConstants.DUPLICATE_ECARD_NAME_MESSAGE);
            } else {
                ecard.setEcardName(ecardName);
            }
        }

        //description validation
        String description = ecardInfo.getDescription();
        if(description == null || description.trim().equals(ProjectConstants.EMPTY_STRING)){
            errors.add(ProgramActivityConstants.ECARD_DESCRIPTION_MISSING_ERROR_MESSAGE);
        }
        else if(description.length() > MAX_ECARD_DESCRIPTION_LENGTH){
            errors.add(new ErrorMessage()
                    .setCode(ERROR_ECARD_DESCRIPTION_LENGTH)
                    .setField(ProjectConstants.DESCRIPTION)
                    .setMessage(ERROR_ECARD_DESCRIPTION_LENGTH_MSG));
        }
        else{
            ecard.setDescription(description);
        }

        String displayType = ecardInfo.getDisplayType();

        if(displayType == null || displayType.trim().equals(ProjectConstants.EMPTY_STRING)){
            errors.add(ProgramActivityConstants.ECARD_DISPLAY_TYPE_MISSING_MESSAGE);
        }
        else if(!displayType.equalsIgnoreCase(ECARD_STOCK_TYPE) && !displayType.equalsIgnoreCase(ECARD_CUSTOM_TYPE)){
            errors.add(ProgramActivityConstants.ECARD_INVALID_DISPLAY_TYPE_MESSAGE);
        }
        else{
            ecard.setEcardTypeCode(displayType.toUpperCase());
        }

        if(!errors.isEmpty()){
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        ecard.setThumbnailUrl("TODO");
        ecard.setImageUrl("TODO");
        ecard.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        concentrixEcardDao.save(ecard);

        return ecardDao.getInfoForEcardId(ecard.getId());
    }


    @Override
    public void inactiveCustomEcard(Long ecardId) {
        Ecard ecard = concentrixEcardDao.findById(ecardId);
        //validate ecard exist.
        if(ecard != null){
            //It is CUSTOM type
            if(ecard.getEcardTypeCode().equals(ECARD_CUSTOM_TYPE)){
                //check if it has been asigned to program
                ProgramEcard programEcard = programEcardDao.findBy()
                        .where(ProjectConstants.ECARD_ID).eq(ecardId)
                        .findOne();
                //ecard not assigned to program
                if(programEcard == null){
                    if(!ecard.getStatusTypeCode().equals(StatusTypeCode.INACTIVE.name())){
                        ecard.setStatusTypeCode(StatusTypeCode.INACTIVE.name());
                        concentrixEcardDao.save(ecard);
                    }
                }else{
                    throw new ErrorMessageException(new ErrorMessage()
                            .setCode(ERROR_ECARD_ASSIGNED_TO_PROGRAM)
                            .setField(ProjectConstants.ECARD_ID)
                            .setMessage(ERROR_ECARD_ASSIGNED_TO_PROGRAM_MSG));
                }
            }else{
                throw new ErrorMessageException(new ErrorMessage()
                        .setCode(ERROR_ECARD_TYPE)
                        .setField(ProjectConstants.ECARD_ID)
                        .setMessage(ERROR_ECARD_TYPE_MSG));
            }
        }else{
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_ECARD_NOT_EXIST)
                    .setField(ProjectConstants.ECARD_ID)
                    .setMessage(ERROR_ECARD_NOT_EXIST_MSG))
                .setStatus(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public Map<String, Object> updateEcardsImages(EcardImageDTO[] ecardInfoLst, MultipartFile[] images) throws Exception {
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        EcardImagesDictDTO imagesDictionary = null;
        try {
            imagesDictionary = ecardBO.validateMetadataAndImages(ecardInfoLst, images, errors);
            if (errors.isEmpty()) {
                ecardBO.validateImages(imagesDictionary, errors);
                ecardBO.updateEcardImages(imagesDictionary, errors);
                errors.forEach(error -> log.error(error.getMessage()));
            } else {
                errors.forEach(error -> log.error(error.getMessage()));
            }

        } catch (RuntimeException e){
            log.error ("updateEcardsImage: ",e);
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_ECARD_IMAGES_WRONG_DATA)
                    .setField(ProjectConstants.UPDATE_ECARDS_IMAGES)
                    .setMessage(e.getMessage()));
        }

        return null;
    }

}
