package com.maritz.culturenext.program.ecard.businessobject;

import com.google.common.base.Preconditions;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Ecard;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StreamUtils;
import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.ecard.dao.EcardDao;
import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import com.maritz.culturenext.program.ecard.dto.EcardImagesDictDTO;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import com.maritz.culturenext.util.ImageManipulationUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.inject.Inject;
import javax.inject.Named;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


@Component
public class EcardBO {
    private static final int ECARD_DEFAULT_WIDTH = 735;
    private static final int ECARD_DEFAULT_HEIGHT = 490;
    private static final int ECARD_THUMBNAIL_WIDTH = 480;
    private static final int ECARD_THUMBNAIL_HEIGHT = 320;

    private static final String ERROR_ECARD_TOO_SMALL = "ECARD_TOO_SMALL";
    private static final String ERROR_ECARD_TOO_SMALL_MSG =
            "Minimum ecard size is " + ECARD_DEFAULT_WIDTH + " by " + ECARD_DEFAULT_HEIGHT + ".";


    private static final ArrayList<String> SUPPORTED_IMAGE_TYPES =
            new ArrayList<>(Arrays.asList("image/jpeg", "image/png"));

    public static final ErrorMessage CONTENT_TYPE_MESSAGE = new ErrorMessage()
            .setCode(ImageConstants.ERROR_CONTENT_TYPE_CODE)
            .setField(ProjectConstants.UPLOAD)
            .setMessage(ImageConstants.ERROR_CONTENT_TYPE_MSG);

    private static final String ERROR_NOT_MATCHING_NAMES = "ERROR_NOT_MATCHING_NAMES";


    @Inject
    @Named("ecardDao") private ConcentrixDao<Ecard> concentrixEcardDao;
    @Inject private EcardDao ecardDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private FilesRepository filesRepository;
    @Inject private FilesDao filesItemDao;
    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;


    @Inject
    public EcardBO setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public EcardBO setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    /**
     * Validates that for each element in ecardInfoLst there's an element in images. And the name of the files
     * should match each other.
     *
     * @param ecardInfoLst
     * @param images
     * @param errors
     * @return
     * @throws Exception
     */
    public EcardImagesDictDTO validateMetadataAndImages(EcardImageDTO[] ecardInfoLst, MultipartFile[] images, ArrayList<ErrorMessage> errors){
        Preconditions.checkNotNull(ecardInfoLst,"Metadata should not be null!");
        Preconditions.checkNotNull(images, "images should not be null");
        Preconditions.checkArgument(ecardInfoLst.length > 0,  "Metadata should not be empty!");
        Preconditions.checkArgument(images.length > 0, "images should not be empty!");
        Preconditions.checkArgument(ecardInfoLst.length == images.length, "metadata and images should be same size!");

        boolean matchingNames = false;
        Map<String, MultipartFile> imageDict = new HashMap<>();
        Map<String, EcardImageDTO > ecardInfoDTODict = new HashMap<>();
        for (int i = 0; i < images.length; i++){
            imageDict.put(images[i].getOriginalFilename(), images[i]);
        }
        for (int i=0; i< ecardInfoLst.length; i++){
            ecardInfoDTODict.put(ecardInfoLst[i].getFileName(),ecardInfoLst[i]);
        }

        matchingNames = imageDict.keySet().containsAll(ecardInfoDTODict.keySet());
        EcardImagesDictDTO imagesDictDTO = new EcardImagesDictDTO();
        if (matchingNames) {
            imagesDictDTO.setImagesDict(imageDict);
            imagesDictDTO.setMetadataDict(ecardInfoDTODict);
        } else{
            errors.add(new ErrorMessage()
                    .setCode(ERROR_NOT_MATCHING_NAMES)
                    .setField(ProjectConstants.UPDATE_ECARDS_IMAGES)
                    .setMessage("no matching names between images and metadata"));
        }
        return imagesDictDTO;
    }


    public void validateImages(EcardImagesDictDTO imagesDictionary, ArrayList<ErrorMessage> errors) throws IOException {
        Preconditions.checkNotNull(imagesDictionary,"images dictionary shouldn't be null and is");
        Preconditions.checkNotNull(imagesDictionary.getImagesDict(), "images are null and shouldn't be");
        for (MultipartFile image : imagesDictionary.getImagesDict().values()){
            if (image.isEmpty()){
                errors.add(new ErrorMessage(ImageConstants.ERROR_CONTENT_TYPE_CODE,
                        ProjectConstants.UPDATE_ECARDS_IMAGES, String.format(" %s Image is empty", image.getName())));
            } else {
                BufferedImage bufferedImage =  processImageFile(image, errors);
                if (areEcardImageDimensionsValid(image, bufferedImage, errors )){
                    imagesDictionary.getMetadataDict().get(image.getOriginalFilename()).setBufferedImage(bufferedImage);
                }
            }
        }
    }

    protected boolean areEcardImageDimensionsValid(MultipartFile image, BufferedImage bufferedImage, ArrayList<ErrorMessage> errors) {
        boolean isValid = false;
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        if(width < ECARD_DEFAULT_WIDTH || height < ECARD_DEFAULT_HEIGHT){
            errors.add(new ErrorMessage()
                    .setCode(ERROR_ECARD_TOO_SMALL)
                    .setField(ProjectConstants.UPLOAD)
                    .setMessage(String.format("%s image: %s",image.getName(), ERROR_ECARD_TOO_SMALL_MSG)));
        } else {
            isValid = true;
        }
        return isValid;
    }

    /**
     * reads the image file into a bufferedImage
     * @param image
     * @param errors
     * @throws IOException
     */
    protected BufferedImage processImageFile(MultipartFile image, ArrayList<ErrorMessage> errors) throws IOException {
        BufferedImage bufferedImage = null;
        ImageReader imageReader = null;
        MemoryCacheImageInputStream mciis = null;
        String contentType = image.getContentType();
        if(contentType == null || !SUPPORTED_IMAGE_TYPES.contains(contentType.toLowerCase())){
            errors.add(new ErrorMessage( ImageConstants.ERROR_CONTENT_TYPE_CODE,
                    ProjectConstants.UPDATE_ECARDS_IMAGES,
                    String.format("% image is not supported type.",image.getName())));
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByMIMEType(contentType);
        if(imageReaders != null && imageReaders.hasNext()){
            imageReader = imageReaders.next();
        }
        else{
            errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        try {
            if(image.getBytes() != null){
                mciis = new MemoryCacheImageInputStream(new ByteArrayInputStream(image.getBytes()));
            }
            else{
                errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
            if(imageReader != null && mciis != null){
                imageReader.setInput(mciis);
                if(imageReader.getNumImages(true) > 1){
                    errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }
                bufferedImage = imageReader.read(0);
            }
        } catch (IOException e) {
            errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }finally{
            if(mciis != null){
                mciis.close();
            }
        }
        return bufferedImage;
    }

    public void updateEcardImages(EcardImagesDictDTO imagesDictionary, ArrayList<ErrorMessage> errors) throws IOException {
        for (EcardImageDTO item : imagesDictionary.getMetadataDict().values()){
            if (item.getBufferedImage() != null) {
                Ecard existingEcard = concentrixEcardDao.findBy()
                        .where(ProjectConstants.ID).eq(item.getId())
                        .findOne();
                if (existingEcard != null) {
                    BufferedImage bufferedImage = ImageManipulationUtil.resize(item.getBufferedImage(), ECARD_DEFAULT_WIDTH, ECARD_DEFAULT_HEIGHT);
                    saveImage(bufferedImage, "ECARD_DEFAULT", existingEcard.getId());

                    bufferedImage = ImageManipulationUtil.resize(bufferedImage, ECARD_THUMBNAIL_WIDTH, ECARD_THUMBNAIL_HEIGHT);

                    saveImage(bufferedImage, "ECARD_THUMBNAIL", existingEcard.getId());
                }
            }
        }
    }
    public void saveImage(BufferedImage bufferedImage, String type, Long ecardId){
        List<Files> filesEntry = filesRepository.findByEcardIdFileTypeCode(ecardId, type);
        // NOTE - images can be associated to ecards and programs. When linked to programs, no record is stored at
        //        ecards_files table.
        Files file = null;
        if (filesEntry == null || filesEntry.size() == 0) {
            file = filesItemDao.findOneByFileImageTypeAndTargetId(FileImageTypes.valueOf(type),ecardId);
        } else {
            file = filesEntry.get(0);
        }
        StreamingOutput streamingOutput = new PNGStreamingOutput(bufferedImage);
        try {
            try (
                    InputStream inputStream = StreamUtils.getInputStream(streamingOutput);
            ) {
                filesDao.findBy()
                        .where("id").eq(file.getId())
                        .write("data", inputStream, inputStream.available());
            }

            if (environmentUtil.writeToCdn()) {
                gcpUtil.uploadFile(file.getId(), streamingOutput);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
