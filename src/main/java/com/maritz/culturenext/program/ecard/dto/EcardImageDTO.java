package com.maritz.culturenext.program.ecard.dto;

import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;

public class EcardImageDTO {
    private Long id;
    private String fileName;
    private BufferedImage bufferedImage = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    @Override
    public String toString() {
        return "EcardImageDTO{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                '}';
    }
}
