package com.maritz.culturenext.program.util;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.program.dto.ProgramBasicDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class ProgramTranslationUtil {
    
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;
    @Inject private TranslationUtil translationUtil;

    /**
     * Populate translatable content for basic program data.
     * @param programSummaryDTO
     * @param languageCode
     */
    public void translateProgramFieldsOnProgramSummaryDTO(ProgramSummaryDTO programSummaryDTO, String languageCode) {
        Map<String, String> defaultMap = new HashMap<>();
        defaultMap.put(TranslationConstants.COLUMN_NAME_PROGRAM_NAME, programSummaryDTO.getProgramName());
        defaultMap.put(TranslationConstants.COLUMN_NAME_PROGRAM_DESC, programSummaryDTO.getProgramDescriptionShort());
        defaultMap.put(TranslationConstants.COLUMN_NAME_PROGRAM_LONG_DESC, 
                programSummaryDTO.getProgramDescriptionLong());

        Map<String, String> translations = translationUtil.getTranslationsForListOfColumns(languageCode,
                TableName.PROGRAM.name(), TranslationConstants.PROGRAM_COLUMNS,
                programSummaryDTO.getProgramId(), defaultMap);

        programSummaryDTO.setProgramName(translations.get(TranslationConstants.COLUMN_NAME_PROGRAM_NAME));
        programSummaryDTO.setProgramDescriptionShort(translations.get(TranslationConstants.COLUMN_NAME_PROGRAM_DESC));
        programSummaryDTO.setProgramDescriptionLong(translations.get(
                TranslationConstants.COLUMN_NAME_PROGRAM_LONG_DESC));
    }
    
    /**
     * Populate translatable content for ProgramMisc data
     * @param programBasicDTO
     * @param languageCode
     */
    public void translateProgramMiscFields(ProgramBasicDTO programBasicDTO, String languageCode) {
        
        // Award code translatable content
        if (ProgramTypeEnum.AWARD_CODE.name().equals(programBasicDTO.getProgramType())) {        
            // Get translatable content for program misc data
            Map<String, String> defaultMap = new HashMap<>();
            defaultMap.put(TranslationConstants.COLUMN_NAME_PROGRAM_MISC_DATA, 
                    programBasicDTO.getAwardCode().getClaimingInstructions());
    
            Long claimingInstructionsMiscId = programMiscDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
                    .and(ProjectConstants.VF_NAME).eq(VfName.CLAIM_INSTRUCTIONS.name())
                    .findOne(ProjectConstants.PROGRAM_MISC_ID, Long.class);
            
            Map<String, String> translations = translationUtil.getTranslationsForListOfColumns(languageCode,
                    TableName.PROGRAM_MISC.name(), TranslationConstants.PROGRAM_MISC_COLUMNS,
                    claimingInstructionsMiscId, defaultMap);
    
            programBasicDTO.getAwardCode().setClaimingInstructions(translations.get(
                    TranslationConstants.COLUMN_NAME_PROGRAM_MISC_DATA));
        }
    }
    
    /**
     * Create program translatable data. 
     * @param programId
     */
    public void setTranslationProgramFields(Long programId) {
        translationUtil.saveTranslationData(TableName.PROGRAM.name(), 
                TranslationConstants.COLUMN_NAME_PROGRAM_NAME, programId, StatusTypeCode.ACTIVE.name());
        translationUtil.saveTranslationData(TableName.PROGRAM.name(), 
                TranslationConstants.COLUMN_NAME_PROGRAM_DESC, programId, StatusTypeCode.ACTIVE.name());
        translationUtil.saveTranslationData(TableName.PROGRAM.name(), 
                TranslationConstants.COLUMN_NAME_PROGRAM_LONG_DESC, programId, StatusTypeCode.ACTIVE.name());

        Long claimingInstructionsMiscId = programMiscDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .and(ProjectConstants.VF_NAME).eq(VfName.CLAIM_INSTRUCTIONS.name())
                .findOne(ProjectConstants.PROGRAM_MISC_ID, Long.class);
        
        if (claimingInstructionsMiscId != null) {
            translationUtil.saveTranslationData(TableName.PROGRAM_MISC.name(), 
                    TranslationConstants.COLUMN_NAME_PROGRAM_MISC_DATA, claimingInstructionsMiscId,
                    StatusTypeCode.ACTIVE.name());
        }
        
    }

}
