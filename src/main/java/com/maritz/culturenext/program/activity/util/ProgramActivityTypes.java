package com.maritz.culturenext.program.activity.util;

public enum ProgramActivityTypes {
    JUMBOTRON,
    CAROUSEL
}
