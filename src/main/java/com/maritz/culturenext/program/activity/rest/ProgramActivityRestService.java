package com.maritz.culturenext.program.activity.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.program.activity.dto.ProgramActivityDTO;
import com.maritz.culturenext.program.activity.service.ProgramActivityService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/program-activities", description = "All end points dealing with program activities")

public class ProgramActivityRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private ProgramActivityService programActivityService;
    @Inject private Security security;

    //rest/program-activities
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "program-activities", method = RequestMethod.POST)
    @ApiOperation(value = "Creates multiple program activities", 
        notes = "This method allows to create multiple program activities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the program activity details.", 
                    response = ProgramActivityDTO.class),
            @ApiResponse(code = 400, message = "Bad request body.", response = ProgramActivityDTO.class),
            @ApiResponse(code = 403, message = "Requesting user is not an admin.")})
    @Permission("PUBLIC")
    public List<ProgramActivityDTO> createProgramActivityList(
            @RequestBody List<ProgramActivityDTO> programActivityList)
                    throws Throwable {

        return programActivityService.createProgramActivityList(programActivityList);
    }
    
    //rest/program-activities/~
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "program-activities/{programActivityId}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates a program activity", notes = "This method allows to update a program activity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the program activity detail.", 
                    response = ProgramActivityDTO.class),
            @ApiResponse(code = 400, message = "Bad request body.", response = ProgramActivityDTO.class),
            @ApiResponse(code = 403, message = "Requesting user is not an admin."),
            @ApiResponse(code = 404, message = "Invalid program activity Id."),})
    @Permission("PUBLIC")
    public ProgramActivityDTO updateProgramActivity(
            @RequestBody ProgramActivityDTO programActivity,
            @ApiParam(value = "Program Activity Id", required = true) 
            @PathVariable(PROGRAM_ACTIVITY_ID_REST_PARAM) String programActivityIdString
            ) throws Throwable {

        Long programActivityId = convertPathVariablesUtil.getId(programActivityIdString);
        
        return programActivityService.updateProgramActivity(programActivity, programActivityId);
    }
    
    //rest/program-activities
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "program-activities", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates a list of program activities", 
    notes = "This method allows to update one or more program activities")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the program activity detail.", 
                    response = ProgramActivityDTO.class),
            @ApiResponse(code = 400, message = "Bad request body.", response = ProgramActivityDTO.class),
            @ApiResponse(code = 403, message = "Requesting user is not an admin."),
            @ApiResponse(code = 404, message = "Invalid program activity Id."),})
    @Permission("PUBLIC")
    public List<ProgramActivityDTO> updateProgramActivityList(
            @RequestBody List<ProgramActivityDTO> programActivityList) throws Throwable {

        return programActivityService.updateProgramActivityList(programActivityList);
    }
    
    //rest/program-activities/~
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "program-activities/{programActivityId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Removes a program activity", notes = "This method allows to delete a program activity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully removed the program activity."),
            @ApiResponse(code = 403, message = "Requesting user is not an admin."),
            @ApiResponse(code = 404, message = "Invalid program activity Id."),})
    @Permission("PUBLIC")
    public void deleteProgramActivity(
            @ApiParam(value = "Program Activity Id", required = true) 
            @PathVariable(PROGRAM_ACTIVITY_ID_REST_PARAM) String programActivityIdString)
                    throws Throwable {

        Long programActivityId = convertPathVariablesUtil.getId(programActivityIdString);
        
        programActivityService.deleteProgramActivity(programActivityId);
    }

    //rest/program-activities
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "program-activities", method = RequestMethod.GET)
    @ApiOperation(value = "Get all program activities", 
        notes = "Return all program activities, it includes support to paging.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the program activities")})
    @Permission("PUBLIC")
    public List<ProgramActivityDTO> getProgramActivities(
            @ApiParam(value = "ACTIVE: returns only program activities in which the current "
                    + "date falls between the start and end date (inclusive)", required = true)        
            @RequestParam(value = STATUS_REST_PARAM, required = true) String statusListString,
            @ApiParam(value = "values are JUMBOTRON or CAROUSEL", required = true)
            @RequestParam(value = TYPE_REST_PARAM, required = true) String type,
            // TODO Pagination Techdebt: MP-7512 & MP-7529
            @ApiParam(value = "Page of progam activities used to paginate the request", required = false) 
            @RequestParam( value = PAGE_REST_PARAM, required = false ) Integer page,
            @ApiParam(value = "The number of records per page.", required = false) 
            @RequestParam( value = SIZE_REST_PARAM, required = false ) Integer size)
                    throws Throwable {

        return programActivityService.getAllProgramActivities(statusListString, type, page, size);
    }
    
    
    //rest/participants/~/program-activities
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "participants/{paxId}/program-activities", method = RequestMethod.GET)
    @ApiOperation(value = "Get all program activities for the specified pax", 
    notes = "Return all program activities the logged in pax is configured to see.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retreived program activities for pax.")})
    @Permission("PUBLIC")
    public List<ProgramActivityDTO> getProgramActivitiesForPax(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "values are JUMBOTRON or CAROUSEL", required = false)
            @RequestParam(value = TYPE_REST_PARAM, required = false) String type)
            throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return programActivityService.getProgramActivitiesForPax(paxId, type);
    }


}
