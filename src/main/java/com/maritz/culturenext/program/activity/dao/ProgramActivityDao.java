package com.maritz.culturenext.program.activity.dao;

import java.util.List;
import java.util.Map;

public interface ProgramActivityDao {
    
    /**
     * Gets a list of currently active program activities that the logged in pax can see
     * (They can either give/receive out of the program the activity is tied to)
     * @param paxId -  person to pull program activities for
     * @param type - program activity type. Either JUMBOTRON or CAROUSEL
     * @return
     */
    List<Map<String,Object>> getProgramActivitiesForPax(Long paxId, String type);
}