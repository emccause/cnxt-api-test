package com.maritz.culturenext.program.activity.constants;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.ImageConstants;

import java.util.Arrays;
import java.util.List;

public class ProgramActivityConstants {
    
    public static final int MAX_DESCRIPTION_LENGTH = 255;
    
    public static final List<String> VALID_STATUS = 
            Arrays.asList(StatusTypeCode.ACTIVE.name(),
                    StatusTypeCode.SCHEDULED.toString(),
                    StatusTypeCode.ENDED.toString());
            
    //General Strings
     private static final String UPLOAD_FIELD = "upload";
     private static final String INFO_FIELD = "info";
     private static final String DISPLAY_NAME_FIELD = "displayName";   
     private static final String DISPLAY_TYPE_FIELD = "displayType";
     private static final String ECARD_STOCK_TYPE = "STOCK";
     private static final String ECARD_CUSTOM_TYPE = "CUSTOM";
     private static final String DESCRIPTION_FIELD = "description";
        
    // Error codes and messages 
    public static final String ERROR_MISSING_STATUS = "MISSING_STATUS";
    public static final String ERROR_MISSING_STATUS_MSG = "A status is required";
    public static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    public static final String ERROR_INVALID_STATUS_MSG = "A valid status is required";
    public static final String ERROR_MISSING_TYPE = "MISSING_TYPE";
    public static final String ERROR_MISSING_TYPE_MSG = "A type is required";
    public static final String ERROR_INVALID_TYPE = "INVALID_TYPE";
    public static final String ERROR_INVALID_TYPE_MSG = "A valid type is required";
    public static final String ERROR_MISSING_PROGRAM_ID = "MISSING_PROGRAM_ID";
    public static final String ERROR_MISSING_PROGRAM_MSG = "A program id is required";
    public static final String ERROR_INVALID_PROGRAM_ID = "INVALID_PROGRAM_ID";
    public static final String ERROR_INVALID_PROGRAM_MSG = "A valid program Id is required";
    public static final String ERROR_MISSING_START_DATE = "MISSING_START_DATE";
    public static final String ERROR_MISSING_START_DATE_MSG = "A start date is required";
    public static final String ERROR_START_DATE_INVALID = "INVALID_START_DATE";
    public static final String ERROR_START_DATE_INVALID_MSG = "A valid start date is required (YYYY-MM-DD)";
    public static final String ERROR_MISSING_END_DATE = "MISSING_END_DATE";
    public static final String ERROR_MISSING_END_DATE_MSG = "A end date is required"; 
    public static final String ERROR_INVALID_END_DATE = "INVALID_END_DATE";
    public static final String ERROR_INVALID_END_DATE_MSG = "A valid end date is required (YYYY-MM-DD)";
    public static final String ERROR_START_DATE_IS_AFTER_END_DATE = "START_DATE_IS_AFTER_TO_DATE";
    public static final String ERROR_START_DATE_IS_AFTER_END_DATE_MSG =  "Start date must be before end date";
    public static final String ERROR_MISSING_DESCRIPTION = "MISSING_DESCRIPTION";
    public static final String ERROR_MISSING_DESCRIPTION_MSG ="A description is required";
    public static final String ERROR_DESCRIPTION_TOO_LONG = "DESCRIPTION_TOO_LONG";
    public static final String ERROR_DESCRIPTION_TOO_MSG = "Description value is too long";
    public static final String ERROR_MISSING_PROGRAM_ACTIVITY_ID = "MISSING_PROGRAM_ACTIVITY_ID";
    public static final String ERROR_MISSING_PROGRAM_ACTIVITY_ID_MSG = "A program activity id is required";
    public static final String ERROR_INVALID_PROGRAM_ACTIVITY_ID = "INVALID_PROGRAM_ACTIVITY_ID";
    public static final String ERROR_INVALID_PROGRAM_ACTIVITY_ID_MSG = "A valid program activity id is required";
    public static final String ERROR_INVALID_TIMEFRAME = "INVALID_TIMEFRAME";
    public static final String ERROR_INVALID_TIMEFRAME_MSG = "Timeframe is overlapping an existing program activity";
    public static final String ERROR_INVALID_IMAGE_ID = "INVALID_IMAGE_ID";
    public static final String ERROR_INVALID_IMAGE_ID_MSG = "An invalid imageId was provided";
    public static final String ERROR_INACTIVE_PROGRAM = "INACTIVE_PROGRAM";
    public static final String ERROR_INACTIVE_PROGRAM_MSG = "The program you are using must be active";
    public static final String ERROR_INVALID_IMAGE_TYPE_CODE = "INVALID_IMAGE_TYPE_CODE";
    public static final String ERROR_INCOMPATIBLE_IMAGE_AND_PROGRAM = 
            "The selected image does not belong to the selected program";
    public static final String ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED = "GIVING_ELIGIBILITY_NOT_ALLOWED";
    public static final String ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED_MSG =
            "Giving Eligibility is not allowed for a MILESTONE program";
    public static final String ERROR_RECEIVING_ELIGIBILITY_REQUIRED = "RECEIVING_ELIGIBILITY_REQUIRED";
    public static final String ERROR_RECEIVING_ELIGIBILITY_REQUIRED_MSG =
            "Receiving Eligibility is required for this program";

    private static final String ERROR_ECARD_MISSING = "ECARD_MISSING";
    private static final String ERROR_ECARD_MISSING_MSG = "Must upload an ecard.";

    private static final String ERROR_ECARD_INFO_MISSING = "ECARD_INFO_MISSING";
    private static final String ERROR_ECARD_INFO_MISSING_MSG = "Ecard information is missing or malformed.";
    private static final String ERROR_ECARD_NAME_MISSING = "ECARD_NAME_MISSING";
    private static final String ERROR_ECARD_NAME_MISSING_MSG = "Display name must be provided.";
    private static final String ERROR_ECARD_DESCRIPTION_MISSING = "ECARD_DESCRIPTION_MISSING";
    private static final String ERROR_ECARD_DESCRIPTION_MISSING_MSG = "Description name must be provided.";

    private static final String ERROR_DUPLICATE_ECARD_NAME = "DUPLICATE_ECARD_NAME";
    private static final String ERROR_DUPLICATE_ECARD_NAME_MSG = "Ecard with specified ecard name already exists.";

    private static final String ERROR_ECARD_DISPLAY_TYPE_MISSING = "ECARD_DISPLAY_TYPE_MISSING";
    private static final String ERROR_ECARD_DISPLAY_TYPE_MISSING_MSG = "Display type name must be provided.";
    private static final String ERROR_ECARD_INVALID_DISPLAY_TYPE = "ECARD_DISPLAY_TYPE_INVALID";
    private static final String ERROR_ECARD_INVALID_DISPLAY_TYPE_MSG = 
            "Ecard display type is invalid. Valid display types are " + ECARD_STOCK_TYPE +" and " + ECARD_CUSTOM_TYPE + ".";
    
    private static final String ERROR_MISSING_BUDGET = "MISSING_BUDGET";
    private static final String ERROR_MISSING_BUDGET_MSG = "Budget assignment is required for POINT_LOAD program";
    private static final String ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED = "BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED";
    private static final String ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED_MSG = 
            "Budget Giving Eligibility is not allowed for POINT_LOAD program";

    //Error Messages
    public static final ErrorMessage MISSING_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_STATUS)
        .setField(ProjectConstants.STATUS)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_STATUS_MSG);
    
    public static final ErrorMessage INVALID_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_STATUS)
        .setField(ProjectConstants.STATUS)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_STATUS_MSG);
    
    public static final ErrorMessage MISSING_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_TYPE_MSG);
    
    public static final ErrorMessage INVALID_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_TYPE_MSG);
    
    public static final ErrorMessage MISSING_PROGRAM_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_PROGRAM_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_PROGRAM_MSG);
    
    public static final ErrorMessage INACTIVE_PROGRAM_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INACTIVE_PROGRAM)
        .setField(ProjectConstants.PROGRAM_STATUS)
        .setMessage(ProgramActivityConstants.ERROR_INACTIVE_PROGRAM_MSG);
    
    public static final ErrorMessage MISSING_START_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_START_DATE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_START_DATE_MSG);
    
    public static final ErrorMessage START_DATE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_START_DATE_INVALID)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramActivityConstants.ERROR_START_DATE_INVALID_MSG);
    
    public static final ErrorMessage MISSING_END_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_END_DATE)
        .setField(ProjectConstants.END_DATE)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_END_DATE_MSG);
    
    public static final ErrorMessage INVALID_END_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_END_DATE)
        .setField(ProjectConstants.END_DATE)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_END_DATE_MSG);
    
    public static final ErrorMessage START_DATE_IS_AFTER_END_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_START_DATE_IS_AFTER_END_DATE)
        .setField(ProjectConstants.END_DATE)
        .setMessage(ProgramActivityConstants.ERROR_START_DATE_IS_AFTER_END_DATE_MSG);
    
    public static final ErrorMessage INVALID_TIMEFRAME_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_TIMEFRAME)
        .setField(ProjectConstants.START_DATE)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_TIMEFRAME_MSG);
    
    public static final ErrorMessage MISSING_DESCRIPTION_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_DESCRIPTION)
        .setField(ProjectConstants.DESCRIPTION)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_DESCRIPTION_MSG);
    
    public static final ErrorMessage DESCRIPTION_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_DESCRIPTION_TOO_LONG)
        .setField(ProjectConstants.DESCRIPTION)
        .setMessage(ProgramActivityConstants.ERROR_DESCRIPTION_TOO_MSG);
    
    public static final ErrorMessage INVALID_IMAGE_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_IMAGE_ID)
        .setField(ProjectConstants.IMAGE_ID)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_IMAGE_ID_MSG);
    
    public static final ErrorMessage INCOMPATIBLE_IMAGE_AND_PROGRAM_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_IMAGE_TYPE_CODE)
        .setField(ProjectConstants.IMAGE_ID)
        .setMessage(ProgramActivityConstants.ERROR_INCOMPATIBLE_IMAGE_AND_PROGRAM);
    
    public static final ErrorMessage MISSING_PROGRAM_ACTIVITY_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_MISSING_PROGRAM_ACTIVITY_ID)
        .setField(ProjectConstants.ID)
        .setMessage(ProgramActivityConstants.ERROR_MISSING_PROGRAM_ACTIVITY_ID_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_ACTIVITY_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramActivityConstants.ERROR_INVALID_PROGRAM_ACTIVITY_ID)
        .setField(ProjectConstants.ID)
        .setMessage(ProgramActivityConstants.ERROR_INVALID_PROGRAM_ACTIVITY_ID_MSG);
    
    public static final ErrorMessage IMAGE_UPLOAD_COUNT_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_UPLOAD_COUNT_CODE)
        .setField(ProjectConstants.UPLOAD)
        .setMessage(ImageConstants.ERROR_IMAGE_UPLOAD_COUNT_MESSAGE);
    
    public static final ErrorMessage ECARD_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_MISSING)
        .setField(UPLOAD_FIELD)
        .setMessage(ERROR_ECARD_MISSING_MSG);
    
    public static final ErrorMessage ECARD_DESCRIPTION_MISSING_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_DESCRIPTION_MISSING)
        .setField(DESCRIPTION_FIELD)
        .setMessage(ERROR_ECARD_DESCRIPTION_MISSING_MSG);
    
    public static final ErrorMessage CONTENT_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_CONTENT_TYPE_CODE)
        .setField(ProjectConstants.UPLOAD)
        .setMessage(ImageConstants.ERROR_CONTENT_TYPE_MSG);
    
    public static final ErrorMessage IMAGE_PROCESS_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_PROCESS_CODE)
        .setField(UPLOAD_FIELD)
        .setMessage(ImageConstants.ERROR_IMAGE_PROCESS_MESSAGE);
    
    public static final ErrorMessage ECARD_NAME_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_NAME_MISSING)
        .setField(DISPLAY_NAME_FIELD)
        .setMessage(ERROR_ECARD_NAME_MISSING_MSG);
    
    public static final ErrorMessage DUPLICATE_ECARD_NAME_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DUPLICATE_ECARD_NAME)
        .setField(DISPLAY_NAME_FIELD)
        .setMessage(ERROR_DUPLICATE_ECARD_NAME_MSG);
    
    public static final ErrorMessage ECARD_INFO_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_INFO_MISSING)
        .setField(INFO_FIELD)
        .setMessage(ERROR_ECARD_INFO_MISSING_MSG);
    
    public static final ErrorMessage ECARD_DISPLAY_TYPE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_DISPLAY_TYPE_MISSING)
        .setField(DISPLAY_TYPE_FIELD)
        .setMessage(ERROR_ECARD_DISPLAY_TYPE_MISSING_MSG);
    
    public static final ErrorMessage ECARD_INVALID_DISPLAY_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ECARD_INVALID_DISPLAY_TYPE)
        .setField(DISPLAY_TYPE_FIELD)
        .setMessage(ERROR_ECARD_INVALID_DISPLAY_TYPE_MSG);
    
    public static final ErrorMessage RECEIVING_ELIGIBILITY_REQUIRED_MESSAGE = new ErrorMessage()
        .setCode(ERROR_RECEIVING_ELIGIBILITY_REQUIRED)
        .setField(ProjectConstants.ELIGIBILITY)
        .setMessage(ERROR_RECEIVING_ELIGIBILITY_REQUIRED_MSG);

    public static final ErrorMessage GIVING_ELIGIBILITY_NOT_ALLOWED = new ErrorMessage()
        .setCode(ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED)
        .setField(ProjectConstants.ELIGIBILITY)
        .setMessage(ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED_MSG);
    
    public static final ErrorMessage MISSING_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_BUDGET)
        .setField(ProjectConstants.BUDGET_ASSIGNMENTS)
        .setMessage(ERROR_MISSING_BUDGET_MSG);
    
    public static final ErrorMessage BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED = new ErrorMessage()
        .setCode(ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED)
        .setField(ProjectConstants.GIVING_MEMBERS)
        .setMessage(ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED_MSG);
}