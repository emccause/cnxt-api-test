package com.maritz.culturenext.program.activity.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.activity.dao.ProgramActivityDao;

@Repository
public class ProgramActivityDaoImpl extends AbstractDaoImpl implements ProgramActivityDao {
    
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String TYPE_SQL_PARAM = "TYPE";
        
    private static final String PROGRAM_ACTIVITIES_FOR_PAX_QUERY = 
            "SELECT DISTINCT p.PROGRAM_ID, pa.PROGRAM_ACTIVITY_ID, pa.PROGRAM_ACTIVITY_TYPE_CODE, " +
            "pa.STATUS_TYPE_CODE, pa.FROM_DATE, pa.THRU_DATE, pa.PROGRAM_ACTIVITY_DESC AS 'DESCRIPTION', paf.FILES_ID " +
            "FROM component.ROLE r " +
            "JOIN component.ACL a ON a.ROLE_ID = r.ROLE_ID AND a.TARGET_TABLE = 'PROGRAM' " +
            "JOIN component.PROGRAM p ON p.PROGRAM_ID = a.TARGET_ID AND p.STATUS_TYPE_CODE = 'ACTIVE' " +
            "JOIN component.VW_PROGRAM_VISIBILITY pv ON pv.PROGRAM_ID = p.PROGRAM_ID AND (pv.VISIBILITY IS NULL OR pv.VISIBILITY = 'PUBLIC') " +
            "JOIN component.PROGRAM_ACTIVITY pa ON pa.PROGRAM_ID = p.PROGRAM_ID " +
            "JOIN component.PROGRAM_ACTIVITY_FILES paf ON paf.PROGRAM_ACTIVITY_ID = pa.PROGRAM_ACTIVITY_ID " +
            "WHERE r.ROLE_CODE IN ('PGIV', 'PREC')  " +
            "AND ( " +
                "(a.SUBJECT_TABLE = 'PAX' AND a.SUBJECT_ID = :" + PAX_ID_SQL_PARAM + ") " +
                "OR (a.SUBJECT_TABLE = 'GROUPS' AND a.SUBJECT_ID IN (SELECT GROUP_ID FROM component.GROUPS_PAX WHERE PAX_ID = :" + PAX_ID_SQL_PARAM + ")) " +
            ") " +
            "AND p.FROM_DATE < GETDATE() " +
            "AND (p.THRU_DATE IS NULL OR p.THRU_DATE > GETDATE()) " +
            "AND (:" + TYPE_SQL_PARAM + " IS NULL OR pa.PROGRAM_ACTIVITY_TYPE_CODE = :" + TYPE_SQL_PARAM + ") " +
            "AND pa.FROM_DATE < GETDATE() " +
            "AND (pa.THRU_DATE IS NULL OR pa.THRU_DATE > GETDATE())";
    

    @Override
    public List<Map<String, Object>> getProgramActivitiesForPax(Long paxId, String type) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        params.addValue(TYPE_SQL_PARAM, type);
        
            List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROGRAM_ACTIVITIES_FOR_PAX_QUERY, params,
                    new CamelCaseMapRowMapper());
            // need to convert all Date objects to String.
            for (Map<String, Object> node : nodes) {
                if (node.get(ProjectConstants.FROM_DATE) != null)
                    node.put(ProjectConstants.FROM_DATE, DateUtil.convertToUTCDate((Date) node.get(ProjectConstants.FROM_DATE)));
                if (node.get(ProjectConstants.THRU_DATE) != null)
                    node.put(ProjectConstants.THRU_DATE, DateUtil.convertToUTCDate((Date) node.get(ProjectConstants.THRU_DATE)));
            }
            return nodes;
    }
    
}
