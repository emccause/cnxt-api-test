package com.maritz.culturenext.program.activity.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityFiles;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.activity.dao.ProgramActivityDao;
import com.maritz.culturenext.program.activity.dto.ProgramActivityDTO;
import com.maritz.culturenext.program.activity.service.ProgramActivityService;
import com.maritz.culturenext.program.activity.util.ProgramActivityTypes;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;

@Service
public class ProgramActivityServiceImpl implements ProgramActivityService {

    @Inject private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    @Inject private ConcentrixDao<ProgramActivity> programActivityDao;
    @Inject private ConcentrixDao<ProgramActivityFiles> programActivityFilesDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private FileImageService fileImageService;
    @Inject private ProgramRepository programRepository;
    @Inject private ProgramActivityDao programActivityDaoImpl;

    @Override
    @Transactional
    public List<ProgramActivityDTO> createProgramActivityList(List<ProgramActivityDTO> programActivityList) {
        
        List<ProgramActivityDTO> responseList = new ArrayList<>();
        
        for (ProgramActivityDTO programActivityDto : programActivityList) {
            // Validate input params
            validateProgramActivityAttributes(programActivityDto);
            
            // create program activity
            ProgramActivity programActivity = populateEntity(null,programActivityDto);
            programActivityDao.save(programActivity);
            
            // populate response
            ProgramActivityDTO responseDto = populateDto(programActivity);
                        
            // assign jumbotron to program activity
            fileImageService.saveFileItemsEntry(programActivityDto.getImageId(), 
                    TableName.PROGRAM_ACTIVITY.name(), programActivity.getProgramActivityId());
            addRemoveFiles(responseDto,  programActivityDto.getImageId());
            responseList.add(responseDto);
        }
        
        return responseList;
    }

    @Override
    public ProgramActivityDTO updateProgramActivity(ProgramActivityDTO programActivity, Long programActivityId) {
        programActivity.setId(programActivityId); // override body id with path id
        List<ProgramActivityDTO> responseList = updateProgramActivityList(Arrays.asList(programActivity));
        return responseList.get(0);
    }

    @Override
    public List<ProgramActivityDTO> updateProgramActivityList(List<ProgramActivityDTO> programActivityList) {
        
        List<ProgramActivityDTO> responseList = new ArrayList<>();
        
        for (ProgramActivityDTO programActivityDto : programActivityList) {
            Long programActivityId = programActivityDto.getId();
            // Validate input params
            validateQueryIdParam(programActivityId);
            validateProgramActivityAttributes(programActivityDto);
            
            // getting program activity entity by id.
            ProgramActivity programActivity = programActivityDao.findById(programActivityId);
            populateEntity(programActivity,programActivityDto);
            
            // dao update/save was updating thruDate with current date value.  
            // So went with programActivityRepository (similar to budget service).
            cnxtProgramActivityRepository.save(programActivity);

            // populate response
            ProgramActivityDTO responseDto = populateDto(programActivity);
            addRemoveFiles(responseDto,  programActivityDto.getImageId());
            responseList.add(responseDto);
        }
        
        return responseList;
    }

    @Override
    public void deleteProgramActivity(Long programActivityId) {
        // Validate input params
        validateQueryIdParam(programActivityId);
        
        // delete program activity files 
        programActivityFilesDao.findBy()
            .where(ProjectConstants.PROGRAM_ACTIVITY_ID).eq(programActivityId)
            .delete();
        
        // using repository because dao is only updating the thru_date column with current date value.
        cnxtProgramActivityRepository.delete(programActivityId);
    }

    @Override
    public List<ProgramActivityDTO> getAllProgramActivities(String statusListString, String type,
            Integer page, Integer size) {
        
        List<String> statusList = null;
        if(statusListString != null && !statusListString.trim().equals("")){
            statusList = Arrays.asList(statusListString.trim().toUpperCase().split(","));
        }
        // Validate input params        
        validateGetParams(statusList, type);
        
        String currentDate = DateUtil.convertToUTCDate(new Date());
        List<ProgramActivity> programActivities = new ArrayList<ProgramActivity>();

        FindBy<ProgramActivity> programActivitiesQuery = programActivityDao.findBy()
                .where(ProjectConstants.PROGRAM_ACTIVITY_TYPE_CODE).eq(type)
                .and();
        
        if (statusList.contains(StatusTypeCode.ACTIVE.name())) {
            // Current date is between start and end date. (inclusive)
            programActivitiesQuery = programActivitiesQuery.or()
                    .and(ProjectConstants.FROM_DATE).le(currentDate)
                    .and(ProjectConstants.THRU_DATE).ge(currentDate).end();
        }
        
        if (statusList.contains(StatusTypeCode.SCHEDULED.name())) {
            // Current date is before the start date
            programActivitiesQuery = programActivitiesQuery.or()
                    .and(ProjectConstants.FROM_DATE).gt(currentDate).end();
        }
        
        if (statusList.contains(StatusTypeCode.ENDED.name())) {
            // Current date is after the end date.
            programActivitiesQuery = programActivitiesQuery.or()
                    .and(ProjectConstants.THRU_DATE).lt(currentDate).end();
        }
        programActivities = programActivitiesQuery.end().find();
        
        List<ProgramActivityDTO> programActivitiesDto = new ArrayList<ProgramActivityDTO>();
        for (ProgramActivity entity : programActivities) {
            ProgramActivityDTO dto = populateDto(entity);
            programActivitiesDto.add(dto);
        }

        return PaginationUtil.getPaginatedList(programActivitiesDto, page, size);
    }

    private void validateGetParams(List<String> statusList, String type){
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        if (CollectionUtils.isEmpty(statusList)) {
            errors.add(ProgramActivityConstants.MISSING_STATUS_MESSAGE);
        }
        else {
            for (String status : statusList) {
                if (!ProgramActivityConstants.VALID_STATUS.contains(status)) {
                    errors.add(ProgramActivityConstants.INVALID_STATUS_MESSAGE);
                }
            }
        }

        if (type == null || type.isEmpty() ) {
            errors.add(ProgramActivityConstants.MISSING_TYPE_MESSAGE);
        } else if(!EnumUtils.isValidEnum(ProgramActivityTypes.class, type.toUpperCase())) {
            errors.add(ProgramActivityConstants.INVALID_TYPE_MESSAGE);
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
    }
    
    private void validateProgramActivityAttributes(ProgramActivityDTO programActivityDto){
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        // validate type.
        if(programActivityDto.getType() == null || programActivityDto.getType().isEmpty()){
            errors.add(ProgramActivityConstants.MISSING_TYPE_MESSAGE);
        }
        else if(!EnumUtils.isValidEnum(ProgramActivityTypes.class, programActivityDto.getType())){
            errors.add(ProgramActivityConstants.INVALID_TYPE_MESSAGE);
        }
        
        
        // validate program id
        if(programActivityDto.getProgramId() == null ){
            errors.add(ProgramActivityConstants.MISSING_PROGRAM_MESSAGE);
        }else {
            Program program;
            program  = programRepository.findOne(programActivityDto.getProgramId());
            if(program == null){
                errors.add(ProgramActivityConstants.INVALID_PROGRAM_MESSAGE);
            } else if (!program.getStatusTypeCode().equals(StatusTypeCode.ACTIVE.name())) {
                errors.add(ProgramActivityConstants.INACTIVE_PROGRAM_MESSAGE);
                }
        }    
        // Validate dates
        Date startDate =null;
        Date endDate = null;
        
        // validate from date
        if (programActivityDto.getStartDate() == null || programActivityDto.getStartDate().isEmpty()) {
            errors.add(ProgramActivityConstants.MISSING_START_DATE_MESSAGE);
        } else if (!programActivityDto.getStartDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            errors.add(ProgramActivityConstants.START_DATE_INVALID_MESSAGE);
        } else {
             startDate = DateUtil.convertToStartOfDayString(programActivityDto.getStartDate());
        }
        
        // validate end date
        if (programActivityDto.getEndDate() == null || programActivityDto.getEndDate().isEmpty()) {
            errors.add(ProgramActivityConstants.MISSING_END_DATE_MESSAGE);
            
        } else if (!programActivityDto.getEndDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            errors.add(ProgramActivityConstants.INVALID_END_DATE_MESSAGE);
        }
        else{
            endDate = DateUtil.convertToEndOfDayString(programActivityDto.getEndDate());
        }
        

        if (startDate != null && endDate != null){ 

            // validate fromDate before endDate only if both has value.
            if(!startDate.before(endDate) && !startDate.equals(endDate)) {
                errors.add(ProgramActivityConstants.START_DATE_IS_AFTER_END_DATE_MESSAGE);
            }
        
            // validate only one activity per type per program for a given timeframe.
            else if(programActivityDto.getProgramId() != null && programActivityDto.getType()!= null
                    && cnxtProgramActivityRepository.isOverlappedTime(programActivityDto.getId(),
                            programActivityDto.getProgramId(), programActivityDto.getType(), 
                            programActivityDto.getStartDate(), programActivityDto.getEndDate()) != null){
                    errors.add(ProgramActivityConstants.INVALID_TIMEFRAME_MESSAGE);
            }        
        }
        
        // Validate description (Carousel type does not requires description)
        if( !ProgramActivityTypes.CAROUSEL.name().equals(programActivityDto.getType())
                && (programActivityDto.getDescription() == null || programActivityDto.getDescription().isEmpty())){
            errors.add(ProgramActivityConstants.MISSING_DESCRIPTION_MESSAGE);
        }
        else if(programActivityDto.getDescription() != null && 
                programActivityDto.getDescription().length() > ProgramActivityConstants.MAX_DESCRIPTION_LENGTH){
            errors.add(ProgramActivityConstants.DESCRIPTION_TOO_LONG_MESSAGE);
        }
        
        // Validate file (image) id
        Files filesDAO = filesDao.findById(programActivityDto.getImageId());
        if (programActivityDto.getImageId() != null && filesDAO == null) {
            errors.add(ProgramActivityConstants.INVALID_IMAGE_ID_MESSAGE);
        } else {
            if (filesDAO != null && filesDAO.getId() != null) {
                if (ProgramActivityTypes.CAROUSEL.name().equals(programActivityDto.getType())) {
                    if (!ProgramActivityTypes.CAROUSEL.name().equals(filesDAO.getFileTypeCode())) {
                        errors.add(ProgramActivityConstants.INCOMPATIBLE_IMAGE_AND_PROGRAM_MESSAGE);
                    }
                } else {
                    if (!ProgramActivityTypes.JUMBOTRON.name().equals(programActivityDto.getType())) {
                        errors.add(ProgramActivityConstants.INCOMPATIBLE_IMAGE_AND_PROGRAM_MESSAGE);
                    }
                }
            }
        }

        ErrorMessageException.throwIfHasErrors(errors);
    }
    
    private void validateQueryIdParam(Long programActivityId){
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        if(programActivityId == null ){
            errors.add(ProgramActivityConstants.MISSING_PROGRAM_ACTIVITY_ID_MESSAGE);
        }
        else if(programActivityDao.findById(programActivityId) == null){
            errors.add(ProgramActivityConstants.INVALID_PROGRAM_ACTIVITY_ID_MESSAGE);
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
    }
    
    /**
     * Adding or deleting program activity files
     * @param responseDto
     * @param fileId
     */
    private void addRemoveFiles(ProgramActivityDTO responseDto, Long fileId){
        
        // Adding file id
        if(fileId != null){
            saveProgramActivityFilesEntry(responseDto.getId(), fileId);
            responseDto.setImageId(fileId);
        }
        else{
            programActivityFilesDao.findBy()
            .where(ProjectConstants.PROGRAM_ACTIVITY_ID).eq(responseDto.getId())
            .delete();
            responseDto.setImageId(null);
        }
    
    }
    
    /**
     * Getting program activity files Id
     * @param programActivityId
     */
    private Long getProgramActivityFilesId(Long programActivityId){
        if(programActivityId != null){
            return programActivityFilesDao.findBy()
                    .where(ProjectConstants.PROGRAM_ACTIVITY_ID).eq(programActivityId)
                    .findOne(ProjectConstants.FILES_ID, Long.class);
        }
        
        return null;
    }
    
    /**
     * Creating or updating Program Activity Files records.
     * @param programActivityId
     * @param fileId
     */
    private void saveProgramActivityFilesEntry(Long programActivityId, Long fileId){
        
        ProgramActivityFiles programActivityFile = programActivityFilesDao.findBy()
                .where(ProjectConstants.PROGRAM_ACTIVITY_ID).eq(programActivityId)
                .findOne();
        
        if(programActivityFile == null){
            programActivityFile = new ProgramActivityFiles();
            programActivityFile.setProgramActivityId(programActivityId);
        }
        
        programActivityFile.setFilesId(fileId);
        
        programActivityFilesDao.save(programActivityFile);
    }

    private ProgramActivityDTO populateDto(ProgramActivity entity) {
        ProgramActivityDTO dto = new ProgramActivityDTO();

        dto.setId(entity.getProgramActivityId());
        dto.setType(entity.getProgramActivityTypeCode());
        dto.setProgramId(entity.getProgramId());
        dto.setStartDate(DateUtil.convertToUTCDate(entity.getFromDate()));
        dto.setEndDate(DateUtil.convertToUTCDate(entity.getThruDate()));
        dto.setDescription(entity.getProgramActivityDesc());
        
        // set status according to start and end date.
        Date currentDate = new Date();
        if(DateUtils.isBetween(currentDate, entity.getFromDate(), entity.getThruDate())){
            dto.setStatus(StatusTypeCode.ACTIVE.name());
        }
        else if(currentDate.before(entity.getFromDate())){
            dto.setStatus(StatusTypeCode.SCHEDULED.name());
        }
        else{
            dto.setStatus(StatusTypeCode.ENDED.name());
        }
        
        // populate image ID
        dto.setImageId(getProgramActivityFilesId(dto.getId()));

        return dto;
    }
    
    private ProgramActivity populateEntity(ProgramActivity entity, ProgramActivityDTO dto){
        if(entity == null){
            entity = new ProgramActivity();
        }
        
        entity.setProgramActivityId(dto.getId());
        entity.setProgramActivityTypeCode(dto.getType());
        entity.setProgramId(dto.getProgramId());
        entity.setFromDate(DateUtil.convertToStartOfDayString(dto.getStartDate()));
        entity.setThruDate(DateUtil.convertToEndOfDayString(dto.getEndDate()));
        entity.setProgramActivityDesc(dto.getDescription());
        
        // default ACTIVE
        entity.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        
        return entity;
    }

    @Override
    public List<ProgramActivityDTO> getProgramActivitiesForPax(Long paxId, String type) {
                
        List<Map<String, Object>> nodes = programActivityDaoImpl.getProgramActivitiesForPax(paxId, type);
        
        List<ProgramActivityDTO> programActivityList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(nodes)) {
            for (Map<String, Object> node : nodes) {
                ProgramActivityDTO programActivity = new ProgramActivityDTO();
                programActivity.setProgramId((Long) node.get(ProjectConstants.PROGRAM_ID));
                programActivity.setId((Long) node.get(ProjectConstants.PROGRAM_ACTIVITY_ID));
                programActivity.setType((String) node.get(ProjectConstants.PROGRAM_ACTIVITY_TYPE_CODE));
                programActivity.setStatus((String) node.get(ProjectConstants.STATUS_TYPE_CODE));
                programActivity.setStartDate((String) node.get(ProjectConstants.FROM_DATE));
                programActivity.setEndDate((String) node.get(ProjectConstants.THRU_DATE));
                programActivity.setDescription((String) node.get(ProjectConstants.DESCRIPTION));
                programActivity.setImageId((Long) node.get(ProjectConstants.FILES_ID));
                programActivityList.add(programActivity);
            }
        }
        
        return programActivityList;
    }
}
