package com.maritz.culturenext.program.activity.service;

import java.util.List;

import com.maritz.culturenext.program.activity.dto.ProgramActivityDTO;

public interface ProgramActivityService {
    
    /**
     * Creates multiple PROGRAM_ACTIVITY records
     * @param programActivityList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22184179/Create+Program+Activity
     */
    List<ProgramActivityDTO> createProgramActivityList(List<ProgramActivityDTO> programActivityList);
    
    /**
     * Update one PROGRAM_ACTIVITY record
     * @param programActivity
     * @param programActivityId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22184181/Update+Program+Activity+by+Id
     */
    ProgramActivityDTO updateProgramActivity(ProgramActivityDTO programActivity, Long programActivityId);
    
    /**
     * Update multiple PROGRAM_ACTIVITY records
     * @param programActivityList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/26247214/Update+Program+Activity
     */
    List<ProgramActivityDTO> updateProgramActivityList(List<ProgramActivityDTO> programActivityList);
    
    /**
     * Delete a PROGRAM_ACTIVITY record
     * @param programActivityId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/23167017/Delete+Program+Activity
     */
    void deleteProgramActivity(Long programActivityId);
    
    /**
     * Return a paginated list of program activities. 
     * @param statusListString - Valid statuses are ACTIVE, SCHEDULED, ENDED
     * @param type - Valid types are JUMBOTRON and CAROUSEL
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22184177/Get+all+Program+Activities
     */
    List<ProgramActivityDTO> getAllProgramActivities(String statusListString, String type, Integer page, Integer size);
    
    /**
     * Gets a list of currently active program activities that the logged in pax can see
     * (They can either give/receive out of the program the activity is tied to)
     * @param paxId -  person to pull program activities for
     * @param type - Valid types are JUMBOTRON and CAROUSEL
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/96145701/Get+all+Program+Activities+for+a+Participant
     */
    List<ProgramActivityDTO> getProgramActivitiesForPax(Long paxId, String type);
}
