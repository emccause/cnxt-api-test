package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

@Component
public class PeerToPeerProgramValidator extends EntityProgramValidator {
    @Nonnull
    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID){
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // utilize default validation

        // eligibility validation
        EligibilityStubDTO eligibility = programRequestDTO.getEligibility();
        validateEligibilityDefault(eligibility, requestType, errors);

        // budget assignments validation
        List<BudgetAssignmentStubDTO> budgetAssignments =
                programRequestDTO.getBudgetAssignments();
        validateBudgetAssignmentsDefault(budgetAssignments, errors);

        // budget assignments and award tiers validation
        List<AwardTierDTO> programAwardTiers = programRequestDTO.getAwardTiers();
        validateBudgetAssignmentsAndAwardTiersDefault(budgetAssignments, programAwardTiers, errors);

        return errors;
    }
}
