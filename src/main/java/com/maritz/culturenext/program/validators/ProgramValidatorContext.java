package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

/**
 * Context class for Program validation. Handles logic associated with
 * determining which type of validator to use (neat place to remove hard
 * dependencies from impl and allow for easier modification).
 */
@Component
public class ProgramValidatorContext {
    private static final Logger logger = LoggerFactory.getLogger(ProgramValidatorContext.class);

    // validators
    @Inject private AwardCodeProgramValidator awardCodeProgramValidator;
    @Inject private DefaultProgramValidator defaultProgramValidator;
    @Inject private EcardOnlyProgramValidator ecardOnlyProgramValidator;
    @Inject private ManagerDiscretionaryProgramValidator managerDiscretionaryProgramValidator;
    @Inject private PeerToPeerProgramValidator peerToPeerProgramValidator;
    @Inject private PointLoadProgramValidator pointLoadProgramValidator;
    @Inject private MilestoneProgramValidator milestoneProgramValidator;
    @Inject private PinnacleProgramValidator pinnacleProgramValidator;

    /**
     * Initial method to determine which validation to perform on the specified
     * Program.
     *
     * @return errors
     */
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        switch (ProgramTypeEnum.valueOf(programRequestDTO.getProgramType())) {
            case POINT_LOAD:
                errors.addAll(pointLoadProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case MANAGER_DISCRETIONARY:
                errors.addAll(managerDiscretionaryProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case PEER_TO_PEER:
                errors.addAll(peerToPeerProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case ECARD_ONLY:
                errors.addAll(ecardOnlyProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case AWARD_CODE:
                errors.addAll(awardCodeProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case MILESTONE:
                errors.addAll(milestoneProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            case PINNACLE:
                errors.addAll(pinnacleProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
            default:
                logger.warn("Specified program type was not expected. Validating with default validation...");
                errors.addAll(defaultProgramValidator.validateProgram(programRequestDTO, requestType, programID));
                break;
        }

        return errors;
    }
}
