package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.ImmutableMap;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.enums.ApprovalTypeEnum;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.EligibilityDao;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.util.EntityUtil;
import com.maritz.culturenext.budget.dto.ApprovalDTO;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.budget.util.ApprovalTypeComparator;
import com.maritz.culturenext.constants.ProjectConstants;

/**
 * Validator that utilizes entities for program validation.
 */
@Component
public abstract class EntityProgramValidator implements ProgramValidator {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    protected static final List<String> VALID_GROUP_TYPE_DESCS = 
            Arrays.asList(GroupType.CUSTOM.name(), GroupType.ENROLLMENT.name(), GroupType.ROLE_BASED.name());
    
    // eligibility entity error mapping
    private Map<String, String> ELIGIBILITY_ERRORS = ImmutableMap.<String, String>builder()
            .put(EntityUtil.getErrorEntityNotFound(), ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND)
            .put(EntityUtil.getErrorEntityNotFoundMsg(), ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND_MSG)
            .put(EntityUtil.getErrorPaxEntityNotActive(), ProgramConstants.ERROR_PAX_ELIGIBILITY_NOT_ACTIVE)
            .put(EntityUtil.getErrorPaxEntityNotActiveMsg(), ProgramConstants.ERROR_PAX_ELIGIBILITY_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityNotActive(), ProgramConstants.ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE)
            .put(EntityUtil.getErrorGroupEntityNotActiveMsg(), ProgramConstants.ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupType(),
                    ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupTypeMsg(), 
                    ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE_MSG)
            .build();

    // budget entity error mapping
    private Map<String, String> BUDGET_ASSIGNMENT_ERRORS = ImmutableMap.<String, String>builder()
            .put(EntityUtil.getErrorEntityNotFound(), ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND)
            .put(EntityUtil.getErrorEntityNotFoundMsg(), ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND_MSG)
            .put(EntityUtil.getErrorPaxEntityNotActive(), ProgramConstants.ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE)
            .put(EntityUtil.getErrorPaxEntityNotActiveMsg(), ProgramConstants.ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityNotActive(), ProgramConstants.ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE)
            .put(EntityUtil.getErrorGroupEntityNotActiveMsg(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupType(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupTypeMsg(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE_MSG)
            .build();

    // for entity state validation
    protected EligibilityDao eligibilityDao;
    private ConcentrixDao<Budget> budgetDao;
    
    @Inject
    public EntityProgramValidator setEligibilityDao(EligibilityDao eligibilityDao) {
        this.eligibilityDao = eligibilityDao;
        return this;
    }
    
    @Inject
    public EntityProgramValidator setBudgetDao(ConcentrixDao<Budget> budgetDao) {
        this.budgetDao = budgetDao;
        return this;
    }

    // helper methods

    /**
     * Validates the given budget specified within the budget assignment.
     *
     * @param budget budget specified within a budget assignment to validate
     * @param errors errors
     */
    protected void validateBudgetAssignmentBudget(BudgetEntityStubDTO budget,
                                                @Nonnull List<ErrorMessage> errors) {
        // validate provided budget information
        if(budget == null) {   // check if budget is specified
            errors.add(ProgramConstants.MISSING_BUDGET_ID_MESSAGE);
            return;
        }

        // validate if the budget specifies a valid budget
        Long budgetId = budget.getBudgetId();
        if(budgetId == null) {
            errors.add(ProgramConstants.MISSING_BUDGET_ID_MESSAGE);
            return;
        }

        // get the specified budget
        Budget budgetEntity = null;

        try {
            budgetEntity = budgetDao.findById(budgetId);
        } catch (Exception e) {
            logger.error("Error occurred while finding budget entity!!");
        }

        // check if budget entity exists
        if(budgetEntity == null) {
            errors.add(ProgramConstants.INVALID_BUDGET_ID_MESSAGE);
        } else {
            // determine if the budget is active
            if(!StatusTypeCode.ACTIVE.name().equals(budgetEntity.getStatusTypeCode())) {
                errors.add(ProgramConstants.BUDGET_NOT_ACTIVE_MESSAGE);
            }
        }
    }

    /**
     * Validates the given giving members stub DTOs to ensure that they contain either a groupId or a paxId.
     *
     * @param givingMembers Giving Members DTOs to validate
     * @param errors errors
     */
    protected void validateBudgetAssignmentGivingMembersStubs(@Nonnull List<BudgetEntityStubDTO> givingMembers,
                                                             @Nonnull List<ErrorMessage> errors) {
        for(BudgetEntityStubDTO givingMember : givingMembers) {
            Long groupId = givingMember.getGroupId();
            Long paxId = givingMember.getPaxId();
            if (groupId == null && paxId == null) {
                errors.add(ProgramConstants.MISSING_GIVING_MEMBER_ID_MESSAGE);
            } else if (groupId != null && paxId != null) {
                errors.add(ProgramConstants.MULTIPLE_GIVING_MEMBER_ID_MESSAGE);
            }
        }
    }

    /**
     * Validates the given giving members specified within a budget assignment.
     *
     * @param givingMembers giving members specified within a budget assignment to validate
     * @param errors errors
     */
    protected void validateBudgetAssignmentGivingMembers(@Nonnull List<BudgetEntityStubDTO> givingMembers,
                                                         @Nonnull List<ErrorMessage> errors) {
        // no need to validate if giving members is empty
        if(givingMembers.isEmpty()) {
            return;
        }

        List<Long> groupIds = new ArrayList<Long>();
        List<Long> paxIds = new ArrayList<Long>();

        // filter group and pax ids by what is specified by the stub DTOs
        for(BudgetEntityStubDTO givingMember : givingMembers) {
            Long groupId = givingMember.getGroupId();
            Long paxId = givingMember.getPaxId();
            if(groupId != null && paxId == null) {
                groupIds.add(groupId);
            } else if(paxId != null && groupId == null) {
                paxIds.add(paxId);
            }
            // if both paxId and groupId, then just continue 
            //(other validation methods handle for invalid entity stub DTOs)
        }

        // get entity DTO objects and validate (only if there were
        List<Map<String, Object>> entities = eligibilityDao.getEligibilityEntitiesAndStatus(paxIds, groupIds);
        errors.addAll(EntityUtil.validateEntityState(entities, BUDGET_ASSIGNMENT_ERRORS, 
                ProjectConstants.GIVING_MEMBERS));
    }

    /**
     * Validates the given eligibility stub DTOs to ensure that they contain either a groupId or a paxId.
     *
     * @param eligibilityEntityStubDTOs Eligibility Stub DTOs to validate
     * @param errors error collection
     */
    protected void validateEligibilityEntityStubs(List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs, 
            List<ErrorMessage> errors) {
        for(EligibilityEntityStubDTO entity : eligibilityEntityStubDTOs) {
            Long groupId = entity.getGroupId();
            Long paxId = entity.getPaxId();
            if (groupId == null && paxId == null) {
                errors.add(ProgramConstants.MISSING_ELIGIBILITY_ID_MESSAGE);
            } else if (groupId != null && paxId != null) {
                errors.add(ProgramConstants.MULTIPLE_ELIGIBILITY_ID_MESSAGE);
            }
        }
    }

    /**
     * Validates eligibility entities specified within the request.
     *
     * @param eligibilityStubDTOs eligibilility entities
     * @param errors errors
     */
    protected void validateEligibilityEntityState(List<EligibilityEntityStubDTO> eligibilityStubDTOs,
                                                  @Nonnull List<String> validGroupTypeNames,
                                                  @Nonnull List<ErrorMessage> errors) {
        List<Long> groupIds = new ArrayList<Long>();
        List<Long> paxIds = new ArrayList<Long>();

        // filter group and pax ids by what is specified by the stub DTOs
        for(EligibilityEntityStubDTO eligibilityEntityStubDTO : eligibilityStubDTOs) {
            Long groupId = eligibilityEntityStubDTO.getGroupId();
            Long paxId = eligibilityEntityStubDTO.getPaxId();
            if(groupId != null && paxId == null) {
                groupIds.add(groupId);
            } else if(paxId != null && groupId == null) {
                paxIds.add(paxId);
            }
            // if both paxId and groupId, then just continue 
            // (other validation methods handle for invalid entity stub DTOs)
        }

        // get eligibility DTO objects and validate
        List<Map<String, Object>> entities = eligibilityDao.getEligibilityEntitiesAndStatus(paxIds,
                groupIds, validGroupTypeNames);

        errors.addAll(EntityUtil.validateEntityState(entities, ELIGIBILITY_ERRORS,
                ProjectConstants.ELIGIBILITY, validGroupTypeNames));
    }

    // default validation methods


    /**
     * Validates given eligibility definition.
     *
     * @param eligibility eligibility
     * @param requestType request type
     * @param errors errors
     */
    protected void validateEligibilityDefault(EligibilityStubDTO eligibility,
                                            @Nonnull String requestType,
                                            @Nonnull List<ErrorMessage> errors) {
        if(eligibility == null) {
             errors.add(ProgramConstants.MISSING_ELIGIBILITY_ERROR_MESSAGE);
        }

        if (!RequestMethod.GET.name().equals(requestType)) {
            // get giving and receiving eligibility definition
            List<EligibilityEntityStubDTO> giverEligibilityStubDTOs = eligibility != null ?
                    eligibility.getGive() :
                    new ArrayList<EligibilityEntityStubDTO>();
            List<EligibilityEntityStubDTO> receiverEligibilityStubDTOs = eligibility != null ?
                    eligibility.getReceive() :
                    new ArrayList<EligibilityEntityStubDTO>();

            if(giverEligibilityStubDTOs == null) {
                giverEligibilityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();
            }

            if(receiverEligibilityStubDTOs == null) {
                receiverEligibilityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();
            }

            // check the given eligibility stub definitions if it has either a groupId and paxId defined (but not both)
            validateEligibilityEntityStubs(giverEligibilityStubDTOs, errors);
            validateEligibilityEntityState(giverEligibilityStubDTOs, VALID_GROUP_TYPE_DESCS, errors);

            validateEligibilityEntityStubs(receiverEligibilityStubDTOs, errors);
            validateEligibilityEntityState(receiverEligibilityStubDTOs, VALID_GROUP_TYPE_DESCS, errors);
        }
    }

    /**
     * Validates the given budget assignments.
     *
     * @param budgetAssignments budget assignments to validate
     */
    protected void validateBudgetAssignmentsDefault(List<BudgetAssignmentStubDTO> budgetAssignments,
                                                  @Nonnull List<ErrorMessage> errors) {
        if(budgetAssignments != null) {
            // validate each budget assignment definition
            for(BudgetAssignmentStubDTO budgetAssignment : budgetAssignments) {
                validateBudgetAssignmentBudget(budgetAssignment.getBudget(), errors);

                List<BudgetEntityStubDTO> givingMembers = budgetAssignment.getGivingMembers();
                if(givingMembers == null) {
                    errors.add(ProgramConstants.MISSING_GIVING_MEMBERS_MESSAGE);
                } else if(givingMembers.isEmpty()) {
                    errors.add(ProgramConstants.GIVING_MEMBERS_EMPTY_MESSAGE);
                } else {
                    validateBudgetAssignmentGivingMembersStubs(budgetAssignment.getGivingMembers(), errors);
                    validateBudgetAssignmentGivingMembers(budgetAssignment.getGivingMembers(), errors);
                }
            }
        }
    }

    /**
     * Validates the given budget assignments in tandem with the award tiers.
     *
     * @param budgetAssignments budget assignments to validate
     * @param programAwardTiers program award tiers to validate with the budget assignments
     */
    protected void validateBudgetAssignmentsAndAwardTiersDefault(List<BudgetAssignmentStubDTO> budgetAssignments,
                                                               List<AwardTierDTO> programAwardTiers,
                                                               @Nonnull List<ErrorMessage> errors) {
        // handling of missing objects
        List<BudgetAssignmentStubDTO> budgetAssignmentsCopy = (budgetAssignments != null) ?
                new ArrayList<BudgetAssignmentStubDTO>(budgetAssignments) :
                new ArrayList<BudgetAssignmentStubDTO>();

        List<AwardTierDTO> awardTiersCopy = (programAwardTiers != null) ?
                new ArrayList<AwardTierDTO>(programAwardTiers) :
                new ArrayList<AwardTierDTO>();

        // validate that program award tiers and budget assignments are either both present or not present
        if((budgetAssignmentsCopy.isEmpty() && !awardTiersCopy.isEmpty()) ||
                !budgetAssignmentsCopy.isEmpty() && awardTiersCopy.isEmpty()) {
            errors.add(ProgramConstants.BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY_MESSAGE);
        }
        else if(!awardTiersCopy.isEmpty()){
            //award tiers no duplicated
            Set<AwardTierDTO> uniqueAwardTiers = new HashSet<AwardTierDTO>(programAwardTiers);
            if(uniqueAwardTiers.size() != programAwardTiers.size() ){
                errors.add(ProgramConstants.AWARD_AMOUNT_DUPLICATED_MESSAGE);
            }

            for(AwardTierDTO awardTier : programAwardTiers){

                //validate Approvals list
                if(awardTier.getApprovals() != null && !awardTier.getApprovals().isEmpty()){
                    List<ApprovalDTO> approvalLevels = awardTier.getApprovals();
                    
                    //approval level duplicated
                    SortedSet<ApprovalDTO> uniqueSortApprovals = new TreeSet<ApprovalDTO>(approvalLevels);
                    if(uniqueSortApprovals.size() != approvalLevels.size() ){
                        errors.add(ProgramConstants.APPROVAL_LEVEL_DUPLICATED_MESSAGE);
                    }
                    //Invalid approval levels
                    else if (uniqueSortApprovals.last().getLevel() != approvalLevels.size()){
                        errors.add(ProgramConstants.INVALID_APPROVAL_LEVEL_MESSAGE);
                    }
                    
                    for(ApprovalDTO approval : approvalLevels){
                        //valid approval type
                        try {
                            ApprovalTypeEnum.valueOf(approval.getType().toUpperCase());
                        }
                        catch (IllegalArgumentException e) {
                            errors.add(ProgramConstants.INVALID_APPROVAL_TYPE_MESSAGE);
                        }
                        //approval type PERSON requires PAX.
                        if(ApprovalTypeEnum.PERSON.getType().equals(approval.getType()) && 
                                (approval.getPax() == null || approval.getPax().getPaxId() == null)){
                            errors.add(ProgramConstants.MISSING_APPROVER_PAX_MESSAGE);
                        }
                        
                    }
                    //approval type duplicated
                    uniqueSortApprovals = new TreeSet<ApprovalDTO>(new ApprovalTypeComparator());
                    uniqueSortApprovals.addAll(approvalLevels);
                    if(uniqueSortApprovals.size() != approvalLevels.size() ){
                        errors.add(ProgramConstants.APPROVAL_TYPE_DUPLICATED_MESSAGE);
                    }
                }
            }
        }
    }
}
