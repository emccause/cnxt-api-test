package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.constants.*;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

@Component
public class EcardOnlyProgramValidator extends EntityProgramValidator{


    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // utilize default validation

        // eligibility validation
        EligibilityStubDTO eligibility = programRequestDTO.getEligibility();
        validateEligibilityDefault(eligibility, requestType, errors);

        if (programRequestDTO.getEcardIds() == null || programRequestDTO.getEcardIds().size() == 0) {
                errors.add(new ErrorMessage()
             .setCode(ProgramConstants.ERROR_ECARD_REQUIRED)
             .setField(ProjectConstants.ECARD_ID)
             .setMessage(ProgramConstants.ERROR_ECARD_REQUIRED_MSG));
        }
        // budget assignments validation
        if (programRequestDTO.getAwardTiers() != null && programRequestDTO.getAwardTiers().size() > 0){
             errors.add(new ErrorMessage()
             .setCode(ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED)
             .setField(ProjectConstants.AWARD_TIER_ASSIGNMENTS)
             .setMessage(ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED_MSG));
        }

        if(programRequestDTO.getBudgetAssignments() != null && programRequestDTO.getBudgetAssignments().size() > 0){
               errors.add(new ErrorMessage()
             .setCode(ProgramConstants.ERROR_BUDGET_NOT_ALLOWED)
             .setField(ProjectConstants.BUDGET_ASSIGNMENTS)
             .setMessage(ProgramConstants.ERROR_BUDGET_NOT_ALLOWED_MSG));
        }

        return errors;
    }

}
