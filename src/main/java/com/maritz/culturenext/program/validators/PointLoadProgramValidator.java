package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

@Component
public class PointLoadProgramValidator extends EntityProgramValidator {

    // valid group type names
    private static final String CUSTOM = "CUSTOM";
    private static final String ENROLLMENT = "ENROLLMENT";
    private static final String ROLE_BASED = "ROLE_BASED";
    private static final List<String> VALID_GROUP_TYPE_DESCS = Arrays.asList(CUSTOM, ENROLLMENT, ROLE_BASED);

    private static final String ERROR_MISSING_BUDGET = "MISSING_BUDGET";
    private static final String ERROR_MISSING_BUDGET_MSG = "Budget assignment is required for POINT_LOAD program";
    private static final String ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED = "BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED";
    private static final String ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED_MSG = 
            "Budget Giving Eligibility is not allowed for POINT_LOAD program";
    private static final String ERROR_RECEIVING_ELIGIBILITY_REQUIRED = "RECEIVING_ELIGIBILITY_REQUIRED";
    private static final String ERROR_RECEIVING_ELIGIBILITY_REQUIRED_MSG = 
            "Receiving Eligibility is required for POINT_LOAD program";

    @Nonnull
    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID){
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // eligibility validation
        EligibilityStubDTO eligibility = programRequestDTO.getEligibility();
        validateEligibility(eligibility, requestType, errors);

        // budget assignments validation
        List<BudgetAssignmentStubDTO> budgetAssignments =
                programRequestDTO.getBudgetAssignments();
        validateBudgetAssignments(budgetAssignments, errors);

        // validate award tiers
        List<AwardTierDTO> awardTiers = programRequestDTO.getAwardTiers();
        validateAwardTiers(awardTiers, errors);

        return errors;
    }

    /**
     * Validates award tiers.
     *
     * @param awardTiers award tiers defined for a program
     * @param errors errors
     */
    private void validateAwardTiers(List<AwardTierDTO> awardTiers, @Nonnull List<ErrorMessage> errors) {
        if(awardTiers != null && !awardTiers.isEmpty()) {
            errors.add(ProgramConstants.AWARD_TIER_NOT_ALLOWED_MESSAGE);
        }
    }

    /**
     * Validates given eligibility definition.
     *
     * @param eligibility eligibility
     * @param requestType request type
     * @param errors errors
     */
    private void validateEligibility(EligibilityStubDTO eligibility, @Nonnull String requestType,
            @Nonnull List<ErrorMessage> errors) {
        if(eligibility == null) {
             errors.add(ProgramConstants.MISSING_ELIGIBILITY_ERROR_MESSAGE);
        }

        if (!RequestMethod.GET.name().equals(requestType)) {
            // get giving and receiving eligibility definition
            List<EligibilityEntityStubDTO> giverEligibilityStubDTOs = eligibility != null ?
                            eligibility.getGive() :
                            new ArrayList<EligibilityEntityStubDTO>();
            List<EligibilityEntityStubDTO> receiverEligibilityStubDTOs = eligibility != null ?
                    eligibility.getReceive() :
                    new ArrayList<EligibilityEntityStubDTO>();

            if(giverEligibilityStubDTOs == null) {
                giverEligibilityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();
            }

            if(receiverEligibilityStubDTOs == null) {
                receiverEligibilityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();
            }

            // validate giving eligibility definition
            validateEligibilityEntityStubs(giverEligibilityStubDTOs, errors);
            validateEligibilityEntityState(giverEligibilityStubDTOs, VALID_GROUP_TYPE_DESCS, errors);

            // verify that receiver eligibility definition is provided
            if(receiverEligibilityStubDTOs.isEmpty()) {
                errors.add(ProgramActivityConstants.RECEIVING_ELIGIBILITY_REQUIRED_MESSAGE);
            } else {
                // validate receiving eligibility definition
                validateEligibilityEntityStubs(receiverEligibilityStubDTOs, errors);
                validateEligibilityEntityState(receiverEligibilityStubDTOs, VALID_GROUP_TYPE_DESCS, errors);
            }
        }
    }

    /**
     * Validates the given budget assignments.
     *
     * @param budgetAssignments budget assignments to validate
     */
    private void validateBudgetAssignments(List<BudgetAssignmentStubDTO> budgetAssignments,
                                           @Nonnull List<ErrorMessage> errors) {
        if(budgetAssignments == null || budgetAssignments.isEmpty()) {
            // verify that budgets is at least defined
            errors.add(ProgramActivityConstants.MISSING_BUDGET_MESSAGE);
        } else {
            // validate each budget assignment definition
            for(BudgetAssignmentStubDTO budgetAssignment : budgetAssignments) {
                // validate the budget for each budget assignment definition
                validateBudgetAssignmentBudget(budgetAssignment.getBudget(), errors);

                // verify giving members (don't need giving definition but needs to be correct if defined)
                List<BudgetEntityStubDTO> givingMembers = budgetAssignment.getGivingMembers();
                if(givingMembers != null && !givingMembers.isEmpty()) {
                    errors.add(ProgramActivityConstants.BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED);
                }
            }
        }
    }
}
