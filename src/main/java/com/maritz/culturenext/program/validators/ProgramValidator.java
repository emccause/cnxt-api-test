package com.maritz.culturenext.program.validators;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Validator for Programs.
 */
public interface ProgramValidator {
    /**
     * Validates the specified program information that was given in the request with the specified type and ID.
     *
     * @param programRequestDTO program information to validate
     * @param requestType type of request
     * @param programID ID of program
     * @return validation errors with the program information (if not, returns empty list of errors)
     */
    @Nonnull
    List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID);
}
