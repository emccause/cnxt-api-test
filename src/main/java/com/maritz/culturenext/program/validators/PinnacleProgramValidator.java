package com.maritz.culturenext.program.validators;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.program.dto.AwardsDTO;
import com.maritz.culturenext.program.dto.NominationPeriodDTO;
import com.maritz.culturenext.program.dto.PinnacleDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class PinnacleProgramValidator implements ProgramValidator {
    @Nonnull
    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID) {
        PinnacleDTO pinnacleDTO = programRequestDTO.getPinnacle();

        List<ErrorMessage> errors = new ArrayList<>();

        Boolean programHasBudget = !(programRequestDTO.getBudgetAssignments() == null || programRequestDTO.getBudgetAssignments().isEmpty());

        Boolean missingApprover = pinnacleDTO.getApprovals() == null || pinnacleDTO.getApprovals().isEmpty();
        if (missingApprover) {
            errors.add(PinnacleConstants.MISSING_APPROVAL_MESSAGE);
        }

        Boolean noQuestions = pinnacleDTO.getQuestions() == null ||
                pinnacleDTO.getQuestions().size() == 0 ||
                pinnacleDTO.getQuestions().get(0).getText().isEmpty() ||
                pinnacleDTO.getQuestions().get(0).getText() == null;
        if (noQuestions) {
            errors.add(PinnacleConstants.NO_QUESTIONS_MESSAGE);
        }

        Boolean noInstructions = pinnacleDTO.getNominationInstructions() == null ||
                pinnacleDTO.getNominationInstructions().isEmpty();
        if (noInstructions) {
            errors.add(PinnacleConstants.MISSING_INSTRUCTIONS_MESSAGE);
        }

        Boolean instructionsTooLong = pinnacleDTO.getNominationInstructions() != null &&
                pinnacleDTO.getNominationInstructions().length() > 255;
        if (instructionsTooLong) {
            errors.add(PinnacleConstants.INSTRUCTIONS_TOO_LONG_MESSAGE);
        }

        // should only validate awards if the program has a budget
        List<ErrorMessage> awardAndAudienceErrors = awardAndAudienceErrors(pinnacleDTO.getAudience(), pinnacleDTO.getAwards(), programHasBudget);
        if (!awardAndAudienceErrors.isEmpty()) {
            errors.addAll(awardAndAudienceErrors);
        }

        List<ErrorMessage> nominationPeriodErrors = nominationPeriodErrors(pinnacleDTO.getNominationPeriods());
        if (!nominationPeriodErrors.isEmpty()) {
            errors.addAll(nominationPeriodErrors);
        }

        return errors;
    }

    private List<ErrorMessage> awardAndAudienceErrors(String audience, AwardsDTO awards, Boolean validateAwards) {
        List<ErrorMessage> errors = new ArrayList<>();

        if (audience == null || audience.isEmpty()) {
            errors.add(PinnacleConstants.MISSING_AUDIENCE_MESSAGE);
        }
        if (validateAwards && awards == null) {
            errors.add(PinnacleConstants.AWARDS_REQUIRED_MESSAGE);
        }
        if (!errors.isEmpty()) {
            return errors;
        }

        if (validateAwards) {
            Boolean hasIndividualAwardAmount = awards.getIndividualAmount() != null && awards.getIndividualAmount() != 0D;
            Boolean hasIndividualPerTeamAwardAmount = awards.getTeamAmountPerPerson() != null && awards.getTeamAmountPerPerson() != 0D;
            Boolean hasTeamPerTeamAwardAmount = awards.getTeamAmountPerTeam() != null && awards.getTeamAmountPerTeam() != 0D;

            if (audience.equalsIgnoreCase(PinnacleConstants.INDIVIDUAL_AUDIENCE)) {
                if (!hasIndividualAwardAmount) {
                    errors.add(PinnacleConstants.INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MESSAGE);
                }
                if (hasIndividualPerTeamAwardAmount || hasTeamPerTeamAwardAmount) {
                    errors.add(PinnacleConstants.TEAM_AWARD_AMOUNT_NOT_ALLOWED_MESSAGE);
                }
            } else if (audience.equalsIgnoreCase(PinnacleConstants.TEAM_AUDIENCE)) {
                if (hasIndividualAwardAmount) {
                    errors.add(PinnacleConstants.INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED_MESSAGE);
                }
                if (hasIndividualPerTeamAwardAmount && hasTeamPerTeamAwardAmount) {
                    errors.add(PinnacleConstants.TWO_TEAM_AWARD_AMOUNTS_MESSAGE);
                }
                if (!hasIndividualPerTeamAwardAmount && !hasTeamPerTeamAwardAmount) {
                    errors.add(PinnacleConstants.TEAM_AWARD_AMOUNT_REQUIRED_MESSAGE);
                }
            } else if (audience.equalsIgnoreCase(PinnacleConstants.BOTH_AUDIENCE)) {
                if (!hasIndividualAwardAmount) {
                    errors.add(PinnacleConstants.INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MESSAGE);
                }
                if (hasIndividualPerTeamAwardAmount && hasTeamPerTeamAwardAmount) {
                    errors.add(PinnacleConstants.TWO_TEAM_AWARD_AMOUNTS_MESSAGE);
                }
                if (!hasIndividualPerTeamAwardAmount && !hasTeamPerTeamAwardAmount) {
                    errors.add(PinnacleConstants.TEAM_AWARD_AMOUNT_REQUIRED_MESSAGE);
                }
            } else {
                errors.add(PinnacleConstants.INVALID_AUDIENCE_MESSAGE);
            }
        }

        return errors;
    }

    private List<ErrorMessage> nominationPeriodErrors(List<NominationPeriodDTO> nominationPeriods) {
        List<ErrorMessage> errors = new ArrayList<>();

        if (nominationPeriods == null || nominationPeriods.isEmpty()) {
            errors.add(PinnacleConstants.NOMINATION_PERIOD_REQUIRED_MESSAGE);
            return errors;
        }

        Boolean missingFromDate = false;
        for (NominationPeriodDTO nominationPeriod : nominationPeriods) {
            if (nominationPeriod.getFromDateLocalDate() == null) {
                missingFromDate = true;
            }
        }
        if (missingFromDate) {
            errors.add(PinnacleConstants.NOMINATION_PERIODS_FROM_DATES_REQUIRED_MESSAGE);
        }

        if (nominationPeriods.size() > 1) {
            Boolean missingThruDate = false;
            int nominationIndex = 0;
            for (NominationPeriodDTO nominationPeriod : nominationPeriods) {
                if (nominationPeriod.getThruDateLocalDate() == null && nominationIndex != nominationPeriods.size() - 1) {
                    missingThruDate = true;
                }
                nominationIndex++;
            }
            if (missingThruDate) {
                errors.add(PinnacleConstants.NOMINATION_PERIODS_THRU_DATES_REQUIRED_MESSAGE);
            }
            if (!missingThruDate && nominationPeriodsOverlap(nominationPeriods)) {
                errors.add(PinnacleConstants.NOMINATION_PERIODS_MUST_NOT_OVERLAP_MESSAGE);
            }
        }

        return errors;
    }

    private Boolean nominationPeriodsOverlap(List<NominationPeriodDTO> nominationPeriods) {
        Boolean nominationPeriodsOverlap = true;
        for (int i = 0; i < nominationPeriods.size() - 1; i++) {
            NominationPeriodDTO focalNominationPeriod = nominationPeriods.get(i);
            List<NominationPeriodDTO> sublist = nominationPeriods.subList(i + 1, nominationPeriods.size());
            for (NominationPeriodDTO nominationPeriodToCompare : sublist) {
                LocalDate focalFromDate = focalNominationPeriod.getFromDateLocalDate();
                LocalDate focalThruDate = focalNominationPeriod.getThruDateLocalDate();
                LocalDate fromDateToCompare = nominationPeriodToCompare.getFromDateLocalDate();
                LocalDate thruDateToCompare = nominationPeriodToCompare.getThruDateLocalDate();
                nominationPeriodsOverlap = thruDateToCompare == null ? false : !((focalFromDate.isBefore(focalThruDate)
                        && focalThruDate.isBefore(fromDateToCompare)
                        && fromDateToCompare.isBefore(thruDateToCompare)) ||
                        (fromDateToCompare.isBefore(thruDateToCompare) &&
                                thruDateToCompare.isBefore(focalFromDate) &&
                                focalFromDate.isBefore(focalThruDate)));
            }
        }
        return nominationPeriodsOverlap;
    }
}
