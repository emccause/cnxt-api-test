package com.maritz.culturenext.program.validators;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.Certificates;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.StatusTypeUtil;

@Component
public class AwardCodeProgramValidator extends EntityProgramValidator {

    private ConcentrixDao<Files> filesDao;
    
    @Inject
    public AwardCodeProgramValidator setFilesDao(ConcentrixDao<Files> filesDao) {
        this.filesDao = filesDao;
        return this;
    }

    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID) {
        List<ErrorMessage> errors = new ArrayList<>();

        // utilize default validation

        // eligibility validation
        EligibilityStubDTO eligibility = programRequestDTO.getEligibility();
        validateEligibilityDefault(eligibility, requestType, errors);

        // get implied status
         String programStatus = StatusTypeUtil.determineRequestStatus(programRequestDTO.getStatus());

        if ((programRequestDTO.getAwardCode() != null)) {
            if ((programRequestDTO.getAwardCode().getClaimingInstructions() == null
                    ||programRequestDTO.getAwardCode().getClaimingInstructions().isEmpty())
                    && StatusTypeCode.ACTIVE.name().equals(programStatus)) {
                errors.add(ProgramConstants.MISSING_CLAIMING_INSTRUCTIONS_CODE_MESSAGE);
            }

            if (programRequestDTO.getAwardCode().getIconId() == null
                    && StatusTypeCode.ACTIVE.name().equals(programStatus)) {
                errors.add(ProgramConstants.MISSING_ICON_ID_CODE_MESSAGE);
            }

            // Validate program file icon type
            else if (programRequestDTO.getAwardCode().getIconId() != null && filesDao.findBy()
                        .where(ProjectConstants.ID).eq(programRequestDTO.getAwardCode().getIconId())
                        .and(ProjectConstants.FILE_TYPE_CODE).eq(FileTypes.AWARD_CODE_ICON)
                        .findOne(ProjectConstants.ID, Long.class) == null ) {
                errors.add(ProgramConstants.INVALID_ICON_ID_CODE_MESSAGE);
            }

            // At least one generation mode (Printable or Downloadable) must be selected.
            if (!programRequestDTO.getAwardCode().isDownloadable()
                    && !programRequestDTO.getAwardCode().isPrintable()
                    && StatusTypeCode.ACTIVE.name().equals(programStatus)) {
                errors.add(ProgramConstants.AWARD_CODE_INVALID_MODE_CODE_MESSAGE);
            }

            // Certificates are required only for printable programs.
            else if (programRequestDTO.getAwardCode().isPrintable()
                    && CollectionUtils.isEmpty(programRequestDTO.getAwardCode().getCertificates())
                    && StatusTypeCode.ACTIVE.name().equals(programStatus)) {
                errors.add(ProgramConstants.MISSING_CERTIFICATES_CODE_MESSAGE);
            }
            else if (!programRequestDTO.getAwardCode().isPrintable()
                    && !CollectionUtils.isEmpty(programRequestDTO.getAwardCode().getCertificates())) {
                errors.add(ProgramConstants.CERTIFICATES_NOT_REQUIRED_CODE_MESSAGE);
            }

            // validate cert ids are AWARD_CODE_CERT file type.
            else if (programRequestDTO.getAwardCode().isPrintable()
                    && StatusTypeCode.ACTIVE.name().equals(programStatus)) {

                if (!CollectionUtils.isEmpty(programRequestDTO.getAwardCode().getCertificates())) {
                    List<Long> certIds = (List<Long>) CollectionUtils.<Certificates,Long>collect(
                        programRequestDTO.getAwardCode().getCertificates()
                        ,TransformerUtils.invokerTransformer(ProjectConstants.GET_ID)
                    );

                    List<Long> certFileIds = filesDao.findBy().where(ProjectConstants.ID).in(certIds)
                            .and(ProjectConstants.FILE_TYPE_CODE).eq(FileTypes.AWARD_CODE_CERT)
                            .find(ProjectConstants.ID, Long.class);

                    if (CollectionUtils.isEmpty(certFileIds)
                            || certFileIds.size() < programRequestDTO.getAwardCode().getCertificates().size()) {
                        errors.add(ProgramConstants.INVALID_CERTIFICATES_CODE_MESSAGE);
                    }
                }
            }
        }

        // Validate that none of these fields are required unless the program is set to ACTIVE
        else if (StatusTypeCode.ACTIVE.name().equals(programStatus)) {
            errors.add(ProgramConstants.MISSING_AWARD_CODE_OBJECT_CODE_MESSAGE);
        }

        // Only NOTIFICATION_RECIPIENT_MGR are allowed for awardCode program.
        if ( !CollectionUtils.isEmpty(programRequestDTO.getNotification())
                && programRequestDTO.getNotification()
                        .contains(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME)) {
            errors.add(ProgramConstants.NOTIFICATION_INVALID_MESSAGE);
        }

        return errors;
    }

}
