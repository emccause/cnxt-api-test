package com.maritz.culturenext.program.validators;

import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.budget.service.BudgetService;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import org.springframework.stereotype.Component;
import org.thymeleaf.util.ListUtils;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class MilestoneProgramValidator extends  EntityProgramValidator {

    private BudgetService budgetService;
    private PaxRepository paxRepository;
    
    @Inject
    public MilestoneProgramValidator setBudgetService(BudgetService budgetService) {
        this.budgetService = budgetService;
        return this;
    }
    
    @Inject
    public MilestoneProgramValidator setPaxRepository(PaxRepository paxRepository) {
        this.paxRepository = paxRepository;
        return this;
    }

    @Nonnull
    @Override
    public List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType, Long programID){
        List<ErrorMessage> errors = new ArrayList<>();

        // eligibility validation
        validateEligibility(programRequestDTO.getEligibility(), requestType, errors);

        // budget assignments validation
        validateBudgetAssignments(programRequestDTO.getBudgetAssignments(), errors);

        // validate award tiers
        validateAwardTiers(programRequestDTO.getAwardTiers(), errors);

        return errors;
    }

    /**
     * Validate award tiers.
     *
     * @param awardTiers award tiers defined for a program
     * @param errors errors
     */
    private void validateAwardTiers(List<AwardTierDTO> awardTiers, @Nonnull List<ErrorMessage> errors) {
        if (!ListUtils.isEmpty(awardTiers)) {
            errors.add(ProgramConstants.AWARD_TIER_NOT_ALLOWED_MESSAGE);
        }
    }

    /**
     * Validates given eligibility definition.
     *
     * @param eligibility eligibility
     * @param requestType request type
     * @param errors errors
     */
    private void validateEligibility(EligibilityStubDTO eligibility, @Nonnull String requestType,
                                     @Nonnull List<ErrorMessage> errors) {
        if (eligibility == null) {
            errors.add(ProgramConstants.MISSING_ELIGIBILITY_ERROR_MESSAGE);
            return;
        }
        
        if (!ListUtils.isEmpty(eligibility.getGive())) {
            if (eligibility.getGive().size() != 1
                    || !paxRepository.findPaxIdByControlNum("admin").equals(eligibility.getGive().get(0).getPaxId())
                    ) {
                // Only admin can give from program
                errors.add(ProgramActivityConstants.GIVING_ELIGIBILITY_NOT_ALLOWED);    
            }
        }

        if (!ListUtils.isEmpty(eligibility.getReceive())) {
            validateEligibilityEntityStubs(eligibility.getReceive(), errors);
            validateEligibilityEntityState(eligibility.getReceive(), VALID_GROUP_TYPE_DESCS, errors);
        }
        else {
            errors.add(ProgramActivityConstants.RECEIVING_ELIGIBILITY_REQUIRED_MESSAGE);
        }
    }

    /**
     * Validates the given budget assignments.
     *
     * @param budgetAssignments budget assignments to validate
     * @param errors errors
     */
    private void validateBudgetAssignments(List<BudgetAssignmentStubDTO> budgetAssignments,
                                           @Nonnull List<ErrorMessage> errors) {

        if (budgetAssignments == null) {
            return;
        }
        else if (budgetAssignments.size() == 1) {
            // milestone programs should have at most 1 simple budget
            BudgetDTO budgetDTO = budgetService.getBudgetById(budgetAssignments.get(0).getBudget().getBudgetId());

            validateBudgetAssignmentBudget(budgetAssignments.get(0).getBudget(), errors);

            if (!BudgetTypeEnum.SIMPLE.name().equalsIgnoreCase(budgetDTO.getType())) {
                errors.add(ProgramConstants.PROGRAM_NON_SIMPLE_BUDGET_MESSAGE);
            }

            List<BudgetEntityStubDTO> givingMembers = budgetAssignments.get(0).getGivingMembers();
            if(!ListUtils.isEmpty(givingMembers)) {
                if (
                    givingMembers.size() != 1
                    || !paxRepository.findPaxIdByControlNum("admin").equals(givingMembers.get(0).getPaxId())
                ) {
                    // only admin can give from the program budget
                    errors.add(ProgramActivityConstants.BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED);
                }
            }
        }
        else {
            errors.add(ProgramConstants.PROGRAM_MORE_THAN_ONE_BUDGET_MESSAGE);
        }
    }
}
