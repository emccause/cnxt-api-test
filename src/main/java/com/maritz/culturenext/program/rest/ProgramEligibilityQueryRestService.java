package com.maritz.culturenext.program.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.program.dto.ProgramEligibilityDTO;
import com.maritz.culturenext.program.dto.ProgramEligibilityQueryDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.dto.RecognitionEligibilityDTO;
import com.maritz.culturenext.program.services.ProgramEligibilityQueryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("participants")
@Api(value = "programs",description = "Endpoint for programs (program activities)")
public class ProgramEligibilityQueryRestService {
    
    @Inject private Security security;
    @Inject private ProgramEligibilityQueryService programEligibilityQueryService;

    //rest/participants/~/programs/query
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value= "{paxId}/programs/query", method = RequestMethod.POST)
    @ApiOperation(value="Returns a list of all programs for giving recognition that a giver (logged in user) and "
            + "receivers are eligible for in a client-project", 
            notes= "'query' in the URI indicates that the API consumer is sending a list of users/groups, "
                    + "type, etc. that they are wanting the details for.<br><br>Only return the programs that "
                    + "are current & 'ACTIVE' status. i.e programs that are 'ACTIVE' and from-date is not in "
                    + "future.<br><br>Added programCategory for categorizing subtyping/categorizing the "
                    + "individual types (p2p, manager discretionary, uploads etc)")
    @ApiResponse(code = 200, message = "Successful retrieval of programs", 
        response = ProgramSummaryDTO.class, responseContainer = "List")
    @Permission("PUBLIC")
    public List<ProgramSummaryDTO> getEligibleProgramsForGiverAndReceiver(
            @ApiParam(value = "participant Id of the logged in user")@PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @RequestBody ProgramEligibilityQueryDTO query,
            @ApiParam(value = "language code for program translations") 
                @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode) {
        return programEligibilityQueryService.getEligiblePrograms(security.getPaxId(paxIdString), query, languageCode);
    }
    
    //rest/participants/~/programs
    @RequestMapping(value= "{paxId}/programs", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a list of all active programs that the participant is "
            + "eligible for (to either give or receive)", 
            notes ="Only return the programs that are current & 'ACTIVE' status. i.e programs that are 'ACTIVE' and "
                    + "from-date is not in future.<br><br>Only retrieve programs that have "
                    + "'programVisibility'= PUBLIC (added 7/17/2015)<br><br>Added 'likeCount' &"
                    + " 'likePax' 7/29/2015 (for sprint 12)<br><br>'likeCount' is the total number of "
                    + "people who have liked the program (including self)<br><br>'likePax' is an object of"
                    + " 1 PAX that has liked this program. if it's the requestor, then that person is "
                    + "populated here as the pax otherwise its populated with one of the other paxes who "
                    + "liked this program (same logic as activity toPax)<br><br>")
    @ApiResponse(code = 200, message = "Successful retrieval of programs", 
        response = ProgramSummaryDTO.class, responseContainer = "List")
    @Permission("PUBLIC")
    public List<ProgramSummaryDTO> getAllEligibleProgramsForPax(
            @ApiParam(value="participant id of the logged in user")@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "language code for program translations", required = false) 
                @RequestParam (value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode,
            @ApiParam(value = "comma separated list of program types to return", required = false) 
                @RequestParam (value = PROGRAM_TYPE_REST_PARAM, required = false) String programType,
            @ApiParam(value = "specify to get programs the pax is eligible to GIVE or RECEIVE ", required = false) 
                @RequestParam (value = ELIGIBILITY_REST_PARAM, required = false) String eligibility){
        return programEligibilityQueryService.getAllEligibleProgramsForPax(
                security.getPaxId(paxIdString), languageCode, programType, eligibility);
    }

    //rest/participants/~/programs/eligibility
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value= "{paxId}/programs/eligibility", method = RequestMethod.GET)
    @ApiOperation(value="Returns logged in user's eligibility for giving / receiving recognitions of specific types", 
        notes= "Only evaluates programs that are current & 'ACTIVE' status. i.e programs that are "
                + "'ACTIVE' and from-date is not in future.")
    @ApiResponse(code = 200, message = "Successful retrieval of eligibility", 
        response = RecognitionEligibilityDTO.class)
    @Permission("PUBLIC")
    public ProgramEligibilityDTO getRecognitionEligibility(
            @ApiParam(value = "participant Id of the logged in user")@PathVariable(PAX_ID_REST_PARAM) String paxIdString) {
        return programEligibilityQueryService.getRecognitionEligibility(security.getPaxId(paxIdString));
    }
}
