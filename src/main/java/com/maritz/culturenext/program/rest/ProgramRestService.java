package com.maritz.culturenext.program.rest;

import java.util.List;
import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.dto.TransactionDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(value = "programs",description = "Endpoint for programs (program activities)")
public class ProgramRestService {
    
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private ProgramService programService;
    @Inject private RecognitionService recognitionService;
    
    private static final String GET_NOMINATION_OR_TRANSACTION_BY_ID =
            "programs/{" + PROGRAM_TYPE_REST_PARAM + "}/transactions/{" + TRANSACTION_ID_REST_PARAM + "}";

    //rest/programs
    @RequestMapping(value = "programs", method = RequestMethod.GET)
    @ApiOperation(value="Retrieves programs", notes="This method retrieves all programs with the specified statuses "
            + "(comma-separated list) or with default set of statuses if none is specified."
            + "It is posible to filter out the programs by program type. By default, Application program types are filtered out.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of programs", 
                    response = ProgramSummaryDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<ProgramSummaryDTO> getProgramList(
            @ApiParam(value = "comma-delimited list of statuses", required = false) 
                @RequestParam (value = STATUS_REST_PARAM, required = false) String status,
            @ApiParam(value = "language to translate results into", required = false) 
                @RequestParam (value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode,
            @ApiParam(value = "filters the list to only specific program types.")
                @RequestParam (value = PROGRAM_TYPE_REST_PARAM, required = false) String programType) throws Throwable {
        return programService.getProgramListByStatus(status, languageCode, programType);
    }
    
    //rest/program/~
    @GetMapping(path = "program/{programId}")
    @ApiOperation(value="Retrieves the program specified by ID", 
        notes="This method retrieves the program with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of specified program", 
                    response = ProgramFullDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProgramFullDTO getProgramByID(
            @ApiParam(value = "ID of program", required = true) 
                @PathVariable (PROGRAM_ID_REST_PARAM) String programIDString,
            @ApiParam(value = "language to translate results into", required = false) 
                @RequestParam (value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode) throws Throwable {
        
        Long programID = convertPathVariablesUtil.getId(programIDString);

        return programService.getProgramByID(programID, languageCode);

    }

    //rest/programs/~/budgets
    @RequestMapping(value = "programs/{programId}/budgets", method = RequestMethod.GET)
    @ApiOperation(
            value = "Return a list of all budgets in the specified program."
    )
    @ApiResponses(
            value = { @ApiResponse(code = 200, message = "successfully returned the list of budgets for the program")}
    )
    @Permission("PUBLIC")
    public List<BudgetAssignmentDTO> getProgramBudgetsByProgramID(
            @ApiParam(value = "ID of the program you want to return budgets for") @PathVariable(PROGRAM_ID_REST_PARAM) Long programId) throws Throwable {
        return programService.getProgramBudgetsByProgramID(programId);
    }

    //rest/program
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value= "program", method = RequestMethod.POST)
    @ApiOperation(value="Creates a new program", 
        notes="This method creates the program with the specified information.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful creation of specified program", 
                    response = ProgramFullDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProgramFullDTO insertProgram(@RequestBody ProgramRequestDTO programRequestDTO
            ) throws Throwable {
        return programService.insertProgram(programRequestDTO);

    }

    //rest/program/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value= "program/{programId}", method = RequestMethod.PUT)
    @ApiOperation(value="Updates the program specified by ID", 
        notes="This method updates the program with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful update of specified program", 
                    response = ProgramFullDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProgramFullDTO updateProgram(
            @ApiParam(value = "ID of program", required = true) 
            @PathVariable (PROGRAM_ID_REST_PARAM) String programIdString, 
                @RequestBody ProgramRequestDTO programRequestDTO
            ) throws Throwable {

        Long programId = convertPathVariablesUtil.getId(programIdString);
        
        return programService.updateProgram(programRequestDTO, programId);
    }
    
    //rest/program/eligibility/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value= "program/eligibility/{programId}", method = RequestMethod.PUT)
    @ApiOperation(value="Updates the program specified by ID", 
        notes="This method updates the program eligibity to give/receive with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful update of specified program eligibility", 
                    response = EligibilityStubDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public EligibilityStubDTO updateProgramEligibility(
            @ApiParam(value = "ID of program", required = true) 
            @PathVariable (PROGRAM_ID_REST_PARAM) String programIdString, 
                @RequestBody EligibilityStubDTO eligibilityStubDTO
            ) throws Throwable {

        Long programId = convertPathVariablesUtil.getId(programIdString);
        
        return programService.updateProgramEligibility(eligibilityStubDTO, programId);
    }
    
    //rest/programs/{programType}/transactions/{transactionId}
    //rest/programs/~/transactions/~
    @RequestMapping(value =  GET_NOMINATION_OR_TRANSACTION_BY_ID , method = RequestMethod.GET)
        @ApiOperation(value="Retrieves the nomination or transaction by ID", 
            notes="This method retrieves the nomination or transaction with the specified ID.")
        @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful retrieval of specified nomination or transaction", 
                        response = RecognitionDetailsDTO.class),
        @ApiResponse(code = 400, message = "Request error")
        })
        @Permission("PUBLIC")
        public TransactionDTO getNominationOrTransactionByID(
            @ApiParam(value = "program type", required = true) 
            @PathVariable (PROGRAM_TYPE_REST_PARAM) String programType,
            @ApiParam(value = "transaction or nomination id", required = true) 
            @PathVariable (TRANSACTION_ID_REST_PARAM) String transactionIdString) 
            throws Throwable {
        
            Long id = convertPathVariablesUtil.getId(transactionIdString);
        
            return recognitionService.getNominationOrTransactionByID(programType, id);
    }

    //rest/programs/{programId}/milestones/{milestoneId}
    //rest/programs/~/milestones/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value= "programs/{programId}/milestones/{milestoneId}", method = RequestMethod.DELETE)
    @ApiOperation(value="Deletes the milestone by ID",
            notes="This method deletes the milestone with the specified ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful deletion of the milestone",
                    response = RecognitionDetailsDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public void deleteMilestoneById(
            @ApiParam(value = "programId", required = true)
            @PathVariable (PROGRAM_ID_REST_PARAM) String programIdString,
            @ApiParam(value = "milestoneId", required = true)
            @PathVariable (MILESTONE_ID_REST_PARAM) String milestoneIdString
    ) throws Throwable {
        Long programId = convertPathVariablesUtil.getId(programIdString);
        Long milestoneId = convertPathVariablesUtil.getId(milestoneIdString);

        programService.deleteMilestoneById(programId, milestoneId);
    }

}
