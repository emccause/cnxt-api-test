package com.maritz.culturenext.program.pinnacle.constants;

import com.maritz.core.rest.ErrorMessage;

public class PinnacleConstants {
    public static final String INDIVIDUAL_AUDIENCE = "INDIVIDUAL";
    public static final String TEAM_AUDIENCE = "TEAM";
    public static final String BOTH_AUDIENCE = "BOTH";
    public static final String APPROVAL_TYPE_CODE_APPROVAL = "APPR";
    public static final String APPROVAL_PROCESS_TYPE_CODE = "approvalProcessTypeCode";
    public static final String PROGRAM = "PROGRAM";
    public static final String PROGRAM_ID = "PROGRAM_ID";
    public static final String PINNACLE_TEST_DESCRIPTION = "Pinnacle Nomination Instructions";
    public static final String PINNACLE = "PINNACLE";

    public static final Integer MAX_ANSWERS = 1;
    public static final Integer REQ_ANSWERS = 1;

    public static final String PROGRAM_TABLE = "PROGRAM";
    public static final String TEST_TABLE = "TEST";

    public static final String NOMINATION_PERIOD = "NOMINATION_PERIOD";

    //errors
    public static final String APPROVAL = "APPROVAL";
    public static final String ERROR_MISSING_APPROVAL = "MISSING_APPROVAL";
    public static final String ERROR_MISSING_APPROVAL_MSG = "Approval is required.";

    public static final String QUESTIONS = "QUESTIONS";
    public static final String ERROR_NO_QUESTIONS = "NO_QUESTIONS";
    public static final String ERROR_NO_QUESTIONS_MSG = "At least one question is required.";

    public static final String INSTRUCTIONS = "INSTRUCTIONS";
    public static final String ERROR_MISSING_INSTRUCTIONS = "MISSING_INSTRUCTIONS";
    public static final String ERROR_MISSING_INSTRUCTIONS_MSG = "Nomination instructions are required.";
    public static final String ERROR_INSTRUCTIONS_TOO_LONG = "INSTRUCTIONS_TOO_LONG";
    public static final String ERROR_INSTRUCTIONS_TOO_LONG_MSG = "Instructions must be 255 characters or less.";

    public static final String AUDIENCE = "AUDIENCE";
    public static final String ERROR_MISSING_AUDIENCE = "MISSING_AUDIENCE";
    public static final String ERROR_MISSING_AUDIENCE_MSG = "Audience is required.";
    public static final String ERROR_INVALID_AUDIENCE = "ERROR_INVALID_AUDIENCE";
    public static final String ERROR_INVALID_AUDIENCE_MSG = "Audience specified is invalid.";

    public static final String AWARDS = "AWARDS";
    public static final String ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED = "ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED";
    public static final String ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG = "If individuals can be recipients in the program, " +
            "an individual award amount must be specified.";
    public static final String ERROR_TEAM_AWARD_AMOUNT_REQUIRED = "ERROR_TEAM_AWARD_AMOUNT_REQUIRED";
    public static final String ERROR_TEAM_AWARD_AMOUNT_REQUIRED_MSG = "If teams can be recipients in the program, " +
            "a team award amount must be specified.";
    public static final String ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED = "ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED";
    public static final String ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED_MSG = "If individuals cannot be recipients in the program, " +
            "an individual award amount may not be specified.";
    public static final String ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED = "ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED";
    public static final String ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED_MSG = "If teams cannot be recipients in the program, " +
            "a team award amount may not be specified.";
    public static final String ERROR_TWO_TEAM_AWARD_AMOUNTS = "ERROR_TWO_TEAM_AWARD_AMOUNTS";
    public static final String ERROR_TWO_TEAM_AWARD_AMOUNTS_MSG = "Team awards must be per participant or per team, not both.";
    public static final String ERROR_AWARDS_REQUIRED = "ERROR_AWARDS_REQUIRED";
    public static final String ERROR_AWARDS_REQUIRED_MSG = "Award amount(s) are required.";

    public static final String NOMINATION_PERIODS = "NOMINATION_PERIODS";
    public static final String NOMINATION_PERIOD_REQUIRED = "NOMINATION_PERIOD_REQUIRED";
    public static final String NOMINATION_PERIOD_REQUIRED_MSG = "At least one nomination period is required.";
    public static final String NOMINATION_PERIODS_MUST_NOT_OVERLAP = "NOMINATION_PERIODS_MUST_NOT_OVERLAP";
    public static final String NOMINATION_PERIODS_MUST_NOT_OVERLAP_MSG = "Nomination periods must not overlap.";
    public static final String NOMINATION_PERIOD_THRU_DATE_REQUIRED = "NOMINATION_PERIOD_THRU_DATE_REQUIRED";
    public static final String NOMINATION_PERIOD_THRU_DATE_REQUIRED_MSG = "If there are multiple nomination periods, they must have through dates.";
    public static final String NOMINATION_PERIOD_FROM_DATE_REQUIRED = "NOMINATION_PERIOD_FROM_DATE_REQUIRED";
    public static final String NOMINATION_PERIOD_FROM_DATE_REQUIRED_MSG = "All nominations must have from dates.";

    //error messages
    public static final ErrorMessage MISSING_APPROVAL_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_APPROVAL)
            .setField(APPROVAL)
            .setMessage(ERROR_MISSING_APPROVAL_MSG);

    public static final ErrorMessage NO_QUESTIONS_MESSAGE = new ErrorMessage()
            .setCode(ERROR_NO_QUESTIONS)
            .setField(QUESTIONS)
            .setMessage(ERROR_NO_QUESTIONS_MSG);

    public static final ErrorMessage MISSING_INSTRUCTIONS_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_INSTRUCTIONS)
            .setField(INSTRUCTIONS)
            .setMessage(ERROR_MISSING_INSTRUCTIONS_MSG);

    public static final ErrorMessage INSTRUCTIONS_TOO_LONG_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INSTRUCTIONS_TOO_LONG)
            .setField(INSTRUCTIONS)
            .setMessage(ERROR_INSTRUCTIONS_TOO_LONG_MSG);

    public static final ErrorMessage MISSING_AUDIENCE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_AUDIENCE)
            .setField(AUDIENCE)
            .setMessage(ERROR_MISSING_AUDIENCE_MSG);

    public static final ErrorMessage INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED)
            .setField(AWARDS)
            .setMessage(ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG);

    public static final ErrorMessage TEAM_AWARD_AMOUNT_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_TEAM_AWARD_AMOUNT_REQUIRED)
            .setField(AWARDS)
            .setMessage(ERROR_TEAM_AWARD_AMOUNT_REQUIRED_MSG);

    public static final ErrorMessage INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED)
            .setField(AWARDS)
            .setMessage(ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED_MSG);

    public static final ErrorMessage TEAM_AWARD_AMOUNT_NOT_ALLOWED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED)
            .setField(AWARDS)
            .setMessage(ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED_MSG);

    public static final ErrorMessage TWO_TEAM_AWARD_AMOUNTS_MESSAGE = new ErrorMessage()
            .setCode(ERROR_TWO_TEAM_AWARD_AMOUNTS)
            .setField(AWARDS)
            .setMessage(ERROR_TWO_TEAM_AWARD_AMOUNTS_MSG);

    public static final ErrorMessage INVALID_AUDIENCE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_AUDIENCE)
            .setField(AUDIENCE)
            .setMessage(ERROR_INVALID_AUDIENCE_MSG);

    public static final ErrorMessage AWARDS_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_AWARDS_REQUIRED)
            .setField(AWARDS)
            .setMessage(ERROR_AWARDS_REQUIRED_MSG);

    public static final ErrorMessage NOMINATION_PERIOD_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(NOMINATION_PERIOD_REQUIRED)
            .setField(NOMINATION_PERIODS)
            .setMessage(NOMINATION_PERIOD_REQUIRED_MSG);

    public static final ErrorMessage NOMINATION_PERIODS_MUST_NOT_OVERLAP_MESSAGE = new ErrorMessage()
            .setCode(NOMINATION_PERIODS_MUST_NOT_OVERLAP)
            .setField(NOMINATION_PERIODS)
            .setMessage(NOMINATION_PERIODS_MUST_NOT_OVERLAP_MSG);

    public static final ErrorMessage NOMINATION_PERIODS_THRU_DATES_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(NOMINATION_PERIOD_THRU_DATE_REQUIRED)
            .setField(NOMINATION_PERIODS)
            .setMessage(NOMINATION_PERIOD_THRU_DATE_REQUIRED_MSG);

    public static final ErrorMessage NOMINATION_PERIODS_FROM_DATES_REQUIRED_MESSAGE = new ErrorMessage()
            .setCode(NOMINATION_PERIOD_FROM_DATE_REQUIRED)
            .setField(NOMINATION_PERIODS)
            .setMessage(NOMINATION_PERIOD_FROM_DATE_REQUIRED_MSG);
}
