package com.maritz.culturenext.program.pinnacle.service.impl;

import com.maritz.core.jpa.entity.*;
import com.maritz.core.jpa.repository.*;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.CnxtTestTypeCode;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.*;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import com.maritz.culturenext.program.pinnacle.service.PinnacleService;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.survey.dto.AnswerDTO;
import com.maritz.survey.dto.QuestionDTO;
import com.maritz.survey.dto.SectionDTO;
import com.maritz.survey.dto.TestDTO;
import com.maritz.survey.services.TestAdminService;
import com.maritz.survey.services.TestService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class PinnacleServiceImpl implements PinnacleService {
    private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    private ApprovalProcessConfigRepository approvalProcessConfigRepository;
    private TestAdminService testAdminService;
    private TestService testService;
    private AclRepository aclRepository;
    private PaxRepository paxRepository;


    @Inject
    public PinnacleServiceImpl setCnxtProgramMiscRepository(CnxtProgramMiscRepository cnxtProgramMiscRepository) {
        this.cnxtProgramMiscRepository = cnxtProgramMiscRepository;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setCnxtProgramActivityRepository(CnxtProgramActivityRepository cnxtProgramActivityRepository) {
        this.cnxtProgramActivityRepository = cnxtProgramActivityRepository;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setApprovalProcessConfigRepository(ApprovalProcessConfigRepository approvalProcessConfigRepository) {
        this.approvalProcessConfigRepository = approvalProcessConfigRepository;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setTestAdminService(TestAdminService testAdminService) {
        this.testAdminService = testAdminService;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setAclRepository(AclRepository aclRepository) {
        this.aclRepository = aclRepository;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setTestService(TestService testService) {
        this.testService = testService;
        return this;
    }

    @Inject
    public PinnacleServiceImpl setPaxRepository(PaxRepository paxRepository) {
        this.paxRepository = paxRepository;
        return this;
    }

    @Override
    @Transactional
    public void insertUpdatePinnacle(Long programId, ProgramRequestDTO programRequestDTO, String requestType) {
        PinnacleDTO pinnacleDTO = programRequestDTO.getPinnacle();

        for (String notification : pinnacleDTO.getNotifications()) {
            saveProgramMisc(programId, requestType, notification, ProjectConstants.TRUE);
        }
        saveProgramMisc(programId, requestType, VfName.AUDIENCE.name(), pinnacleDTO.getAudience());
        saveNominationPeriods(programId, requestType, pinnacleDTO.getNominationPeriods());
        saveNominationInstructions(programId, pinnacleDTO, requestType);
        saveApprover(programId, pinnacleDTO.getApprovals(), requestType);
        saveProgramMisc(programId, requestType, VfName.NEWSFEED_VISIBILITY_NOMINATED.name(),
                pinnacleDTO.getNewsfeedVisibilityNominated().toString());
        saveProgramMisc(programId, requestType, VfName.NEWSFEED_VISIBILITY_APPROVED.name(),
                pinnacleDTO.getNewsfeedVisibilityApproved().toString());

        if (pinnacleDTO.getAwards().getIndividualAmount() != null && pinnacleDTO.getAwards().getIndividualAmount() != 0D) {
            saveProgramMisc(programId, requestType, VfName.PINNACLE_INDIVIDUAL_AWARD_AMOUNT.name(),
                    pinnacleDTO.getAwards().getIndividualAmount().toString());
        }
        if (pinnacleDTO.getAwards().getTeamAmountPerPerson() != null && pinnacleDTO.getAwards().getTeamAmountPerPerson() != 0D) {
            saveProgramMisc(programId, requestType, VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON.name(),
                    pinnacleDTO.getAwards().getTeamAmountPerPerson().toString());
        }
        if (pinnacleDTO.getAwards().getTeamAmountPerTeam() != null && pinnacleDTO.getAwards().getTeamAmountPerTeam() != 0D) {
            saveProgramMisc(programId, requestType, VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM.name(),
                    pinnacleDTO.getAwards().getTeamAmountPerTeam().toString());
        }
    }

    @Override
    @Transactional
    public PinnacleDTO getPinnacle(Long programId) {
        PinnacleDTO pinnacle = new PinnacleDTO();

        List<ProgramMisc> programMiscs = cnxtProgramMiscRepository.findByProgramId(programId);
        pinnacle = mapProgramMiscsToPinnacle(programMiscs, pinnacle);
        List<ProgramActivity> programActivities = cnxtProgramActivityRepository.findByProgramId(programId);
        pinnacle = mapProgramActivitiesToPinnacle(programActivities, pinnacle);

        Acl testAcl = aclRepository.findBy().where(ProjectConstants.SUBJECT_TABLE).eq(PinnacleConstants.TEST_TABLE)
                .and(ProjectConstants.TARGET_TABLE).eq(PinnacleConstants.PROGRAM_TABLE)
                .and(ProjectConstants.TARGET_ID).eq(programId).findOne();
        TestDTO test = testService.getTest(testAcl.getSubjectId(), false);
        pinnacle = mapNominationInstructionsToPinnacle(test, pinnacle);

        List<ApprovalProcessConfig> approvals = approvalProcessConfigRepository.findBy()
                .where(PinnacleConstants.APPROVAL_PROCESS_TYPE_CODE).eq(PinnacleConstants.APPROVAL_TYPE_CODE_APPROVAL)
                .and(ProjectConstants.TARGET_ID).eq(programId).findAll();
        pinnacle = mapApprovalsToPinnacle(approvals, pinnacle);
        return pinnacle;
    }

    private void saveProgramMisc(Long programId, String requestType, String vfName, String miscData) {
        ProgramMisc programMiscEntity = new ProgramMisc();

        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
            programMiscEntity = cnxtProgramMiscRepository.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.VF_NAME).eq(vfName)
                    .findOne();
            if (programMiscEntity == null) {
                programMiscEntity = new ProgramMisc();
            }
        }

        programMiscEntity.setProgramId(programId);
        programMiscEntity.setVfName(vfName);
        programMiscEntity.setMiscData(miscData);
        programMiscEntity.setMiscDate(DateUtil.convertFromString(DateUtil.convertToUTCDate(new Date())));

        cnxtProgramMiscRepository.save(programMiscEntity);
    }

    private void saveNominationPeriods(Long programId, String requestType, List<NominationPeriodDTO> nominationPeriods) {
        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
            cnxtProgramActivityRepository.deleteByProgramId(programId);
        }

        for (NominationPeriodDTO nominationPeriod : nominationPeriods) {
            ProgramActivity programActivity = new ProgramActivity();
            programActivity.setProgramId(programId);
            programActivity.setProgramActivityName(PinnacleConstants.NOMINATION_PERIOD);
            programActivity.setProgramActivityDesc(PinnacleConstants.NOMINATION_PERIOD);
            programActivity.setFromDate(convertLocalDateToDate(nominationPeriod.getFromDateLocalDate()));
            programActivity.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
            programActivity.setProgramActivityTypeCode(PinnacleConstants.PINNACLE);
            if (nominationPeriod.getThruDateLocalDate() != null) {
                programActivity.setThruDate(convertLocalDateToDate(nominationPeriod.getThruDateLocalDate()));
            }

            cnxtProgramActivityRepository.save(programActivity);
        }
    }

    private Date convertLocalDateToDate(LocalDate inputDate) {
        return java.sql.Date.valueOf(inputDate.toString());
    }

    private void saveApprover(Long programId, List<ApprovalDTO> approvers, String requestType) {
        // for now there should only be one approver
        for (ApprovalDTO approver : approvers) {
            ApprovalProcessConfig approvalProcessConfig = new ApprovalProcessConfig();

            if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
                approvalProcessConfig = approvalProcessConfigRepository.findBy()
                        .where(ProjectConstants.TARGET_ID).eq(programId).findOne();
            }

            approvalProcessConfig.setTargetId(programId);
            approvalProcessConfig.setTargetTable(PinnacleConstants.PROGRAM_TABLE);
            approvalProcessConfig.setApproverPaxId(approver.getPax().getPaxId());
            approvalProcessConfig.setApprovalLevel(approver.getLevel());
            approvalProcessConfig.setApprovalTypeCode(approver.getType());
            approvalProcessConfig.setApprovalProcessTypeCode(PinnacleConstants.APPROVAL_TYPE_CODE_APPROVAL);

            approvalProcessConfigRepository.save(approvalProcessConfig);
        }
    }

    private void saveNominationInstructions(Long programId, PinnacleDTO pinnacle, String requestType) {
        TestDTO test;
        Long testId;
        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
            test = createTest(pinnacle.getNominationInstructions(), pinnacle.getQuestions());
            test.setId(pinnacle.getTestId());
            testAdminService.updateTest(test);
        }
        else {
            test = testAdminService.createTest(createTest(pinnacle.getNominationInstructions(), pinnacle.getQuestions()));
            testId = test.getId();

            Acl testAcl = new Acl();
            testAcl.setTargetId(programId);
            testAcl.setTargetTable(PinnacleConstants.PROGRAM_TABLE);
            testAcl.setSubjectId(testId);
            testAcl.setSubjectTable(PinnacleConstants.TEST_TABLE);
            aclRepository.save(testAcl);
        }
    }

    private TestDTO createTest(String nominationInstructions, List<QuestionDTO> questions) {
        // question fields
        for (QuestionDTO question : questions) {
            List<AnswerDTO> answers = new ArrayList<>();
            AnswerDTO answer = new AnswerDTO();
            answer.setUserProvided(true);
            answers.add(answer);

            question.setQuestionTypeCode(PinnacleConstants.PINNACLE);
            question.setMaxAnswers(PinnacleConstants.MAX_ANSWERS);
            question.setRequiredAnswers(PinnacleConstants.REQ_ANSWERS);
            question.setAnswers(answers);
        }

        // section fields
        SectionDTO section = new SectionDTO();
        section.setQuestions(questions);
        section.setNumberOfQuestions(questions.size());
        List<SectionDTO> sections = new ArrayList<>();
        sections.add(section);

        // test fields
        TestDTO test = new TestDTO();
        test.setDescription(nominationInstructions);
        test.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        test.setTestTypeCode(CnxtTestTypeCode.PINNACLE.name());
        test.setSections(sections);
        test.setName(PinnacleConstants.PINNACLE);
        test.setCanResume(true);

        return test;
    }

    private PinnacleDTO mapProgramMiscsToPinnacle(List<ProgramMisc> programMiscs, PinnacleDTO pinnacle) {
        List<String> notifications = new ArrayList<>();
        AwardsDTO awards = new AwardsDTO();
        for (ProgramMisc programMisc:programMiscs) {
            switch(programMisc.getVfName()) {
                case "AUDIENCE": pinnacle.setAudience(programMisc.getMiscData());
                    break;
                case "NEWSFEED_VISIBILITY_NOMINATED": pinnacle.setNewsfeedVisibilityNominated(programMisc.getMiscData());
                    break;
                case "NEWSFEED_VISIBILITY_APPROVED": pinnacle.setNewsfeedVisibilityApproved(programMisc.getMiscData());
                    break;
                case "PINNACLE_NOTIFICATION_RECIPIENT": notifications.add(programMisc.getVfName());
                    break;
                case "PINNACLE_NOTIFICATION_RECIPIENT_MGR": notifications.add(programMisc.getVfName());
                    break;
                case "PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR": notifications.add(programMisc.getVfName());
                    break;
                case "PINNACLE_INDIVIDUAL_AWARD_AMOUNT": awards.setIndividualAmount(Double.parseDouble(programMisc.getMiscData()));
                    break;
                case "PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON": awards.setTeamAmountPerPerson(Double.parseDouble(programMisc.getMiscData()));
                    break;
                case "PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM": awards.setTeamAmountPerTeam(Double.parseDouble(programMisc.getMiscData()));
                    break;
            }
        }
        pinnacle.setNotifications(notifications);
        pinnacle.setAwards(awards);
        return pinnacle;
    }

    private PinnacleDTO mapProgramActivitiesToPinnacle(List<ProgramActivity> programActivities, PinnacleDTO pinnacle) {
        List<NominationPeriodDTO> nominationPeriods = new ArrayList<>();
        for (ProgramActivity programActivity:programActivities) {
            NominationPeriodDTO nominationPeriod = new NominationPeriodDTO();

            String fromDate = DateUtil.convertToUTCDate(programActivity.getFromDate());
            nominationPeriod.setFromDate(fromDate);

            if (programActivity.getThruDate() != null) {
                String thruDate = DateUtil.convertToUTCDate(programActivity.getThruDate());
                nominationPeriod.setThruDate(thruDate);
            }

            nominationPeriods.add(nominationPeriod);
        }
        pinnacle.setNominationPeriods(nominationPeriods);
        return pinnacle;
    }

    private PinnacleDTO mapNominationInstructionsToPinnacle(TestDTO test, PinnacleDTO pinnacle) {
        pinnacle.setNominationInstructions(test.getDescription());
        pinnacle.setQuestions(test.getSections().get(0).getQuestions());
        pinnacle.setTestId(test.getId());
        return pinnacle;
    }

    private PinnacleDTO mapApprovalsToPinnacle(List<ApprovalProcessConfig> approvals, PinnacleDTO pinnacle) {
        List<ApprovalDTO> approvalEntities = new ArrayList<>();
        for (ApprovalProcessConfig approval:approvals) {
            Pax pax = paxRepository.findBy().where(ProjectConstants.PAX_ID).eq(approval.getApproverPaxId()).findOne();

            EmployeeDTO employee = setupEmployee(pax);

            ApprovalDTO approvalEntity = new ApprovalDTO();
            approvalEntity.setPax(employee);
            approvalEntity.setLevel(approval.getApprovalLevel());
            approvalEntity.setType(approval.getApprovalTypeCode());
            approvalEntities.add(approvalEntity);
        }
        pinnacle.setApprovals(approvalEntities);
        return pinnacle;
    }

    private EmployeeDTO setupEmployee(Pax pax) {
        EmployeeDTO employee = new EmployeeDTO();

        employee.setPaxId(pax.getPaxId());
        if (pax.getLanguageCode() != null) employee.setLanguageCode(pax.getLanguageCode());
        if (pax.getPreferredLocale() != null) employee.setPreferredLocale(pax.getPreferredLocale());
        if (pax.getControlNum() != null) employee.setControlNum(pax.getControlNum());
        if (pax.getFirstName() != null) employee.setFirstName(pax.getFirstName());
        if (pax.getMiddleName() != null) employee.setMiddleName(pax.getMiddleName());
        if (pax.getLastName() != null) employee.setLastName(pax.getLastName());
        if (pax.getNamePrefix() != null) employee.setNamePrefix(pax.getNamePrefix());
        if (pax.getNameSuffix() != null) employee.setNameSuffix(pax.getNameSuffix());
        if (pax.getCompanyName() != null) employee.setCompanyName(pax.getCompanyName());

        return employee;
    }
}
