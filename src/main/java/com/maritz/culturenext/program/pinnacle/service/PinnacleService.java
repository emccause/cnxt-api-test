package com.maritz.culturenext.program.pinnacle.service;

import com.maritz.culturenext.program.dto.PinnacleDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;

public interface PinnacleService {
    /**
     * @param programId - the programId of the pinnacle program to be saved
     * @param programRequestDTO - the pinnacle program request
     * @param requestType - POST or PUT
     * @return the pinnacle program that was saved
     */
    void insertUpdatePinnacle(Long programId, ProgramRequestDTO programRequestDTO, String requestType);

    /**
     * @param programId - the programId of the pinnacle program
     * @return pinnacle program
     */
    PinnacleDTO getPinnacle(Long programId);
}
