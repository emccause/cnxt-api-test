package com.maritz.culturenext.program.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.enums.ProgramEligibilityEnum;
import com.maritz.culturenext.constants.ProjectConstants;

public class ProgramConstants {

    public static final int PROGRAM_NAME_MAX_LENGTH = 100;
    public static final int SHORT_PROGRAM_DESCRIPTION_MAX_LENGTH = 255;
    public static final int LONG_PROGRAM_DESCRIPTION_MAX_LENGTH = 250000;
    public static final Long RANGE_MIN_MINUMN_VALUE = 1L;

    public static final List<String> DEFAULT_PROGRAM_ELIGIBILITY_TYPE_LIST =
            Arrays.asList(
                    ProgramEligibilityEnum.GIVE.getEligibilityRoleName(),
                    ProgramEligibilityEnum.RECEIVE.getEligibilityRoleName()
            );

    //Constant created to avoid Time Zone Issues since all dates are converted to UTC
    public static final int UTC_OFFSET = 2;

    // SQL Constants
    public static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST";

    //errors
    public static final String ERROR_BUDGET_NOT_ALLOWED = "BUDGET_NOT_ALLOWED";
    public static final String ERROR_BUDGET_NOT_ALLOWED_MSG = "Budget assignment not allowed on ECARD_ONLY programs";
    public static final String ERROR_ECARD_REQUIRED = "ECARD_REQUIRED";
    public static final String ERROR_ECARD_REQUIRED_MSG = "Ecards are required in ECARD_ONLY programs";
    public static final String ERROR_AWARD_TIER_NOT_ALLOWED = "AWARD_TIER_NOT_ALLOWED";
    public static final String ERROR_AWARD_TIER_NOT_ALLOWED_MSG = "Award tiers are not allowed on this program";
    public static final String ERROR_IMAGE_TYPE_CODE = "PROGRAM_IMAGE_WRONG_TYPE";
    public static final String ERROR_IMAGE_TYPE_MESSAGE = "Only .jpg, .png, and .jpeg are supported";
    public static final String ERROR_NOT_ADMIN_CODE = "PROGRAM_IMAGE_ADMIN_ONLY";
    public static final String ERROR_NOT_ADMIN_CODE_MESSAGE = "Must be an admin to upload image";
    public static final String ERROR_IMAGE_TOO_BIG_CODE = "PROGRAM_IMAGE_TOO_BIG";
    public static final String ERROR_IMAGE_TOO_BIG_MESSAGE = "The image must be less than 6 MB.";
    public static final String ERROR_DUPLICATE_PROGRAM_NAME = "DUPLICATE_PROGRAM_NAME";
    public static final String ERROR_DUPLICATE_PROGRAM_NAME_MSG = "The program name already exists";
    public static final String ERROR_PROGRAM_TYPE_NULL = "PROGRAM_TYPE_NULL";
    public static final String ERROR_PROGRAM_TYPE_NULL_MSG = "Program type not provided";
    public static final String ERROR_PROGRAM_NAME_NULL = "PROGRAM_NAME_NULL";
    public static final String ERROR_PROGRAM_NAME_NULL_MSG = "Program name not provided";
    public static final String ERROR_PROGRAM_NAME_TOO_LONG = "PROGRAM_NAME_TOO_LONG";
    public static final String ERROR_PROGRAM_NAME_TOO_LONG_MSG = "Program name too long";
    public static final String ERROR_SHORT_DESCRIPTION_NULL = "SHORT_DESCRIPTION_NULL";
    public static final String ERROR_SHORT_DESCRIPTION_NULL_MSG = "Short description not provided";
    public static final String ERROR_SHORT_PROGRAM_DESCRIPTION_TOO_LONG = "SHORT_PROGRAM_DESCRIPTION_TOO_LONG";
    public static final String ERROR_SHORT_PROGRAM_DESCRIPTION_TOO_LONG_MSG = "Short description too long";
    public static final String ERROR_LONG_DESCRIPTION_NULL = "LONG_DESCRIPTION_NULL";
    public static final String ERROR_LONG_DESCRIPTION_NULL_MSG = "Long description not provided";
    public static final String ERROR_LONG_PROGRAM_DESCRIPTION_TOO_LONG = "LONG_PROGRAM_DESCRIPTION_TOO_LONG";
    public static final String ERROR_LONG_PROGRAM_DESCRIPTION_TOO_LONG_MSG = "Long description too long";
    public static final String ERROR_PROGRAM_MORE_THAN_ONE_BUDGET = "PROGRAM_MORE_THAN_ONE_BUDGET";
    public static final String ERROR_PROGRAM_MORE_THAN_ONE_BUDGET_MSG = "A Milestone program cannot have more than one budget";
    public static final String ERROR_PROGRAM_NON_SIMPLE_BUDGET = "PROGRAM_NON_SIMPLE_BUDGET";
    public static final String ERROR_PROGRAM_NON_SIMPLE_BUDGET_MSG = "A Milestone program budget must be simple";
    public static final String ERROR_FROM_DATE_NULL = "FROM_DATE_NULL";
    public static final String ERROR_FROM_DATE_NULL_MSG = "From date not provided";
    public static final String ERROR_FROM_DATE_NOT_FUTURE = "FROM_DATE_NOT_FUTURE";
    public static final String ERROR_FROM_DATE_NOT_FUTURE_MSG = "From date not future date";
    public static final String ERROR_FROM_DATE_BAD_FORMAT = "FROM_DATE_BAD_FORMAT";
    public static final String ERROR_FROM_DATE_BAD_FORMAT_MSG = "From date is in incorrect format";
    public static final String ERROR_THRU_DATE_NOT_FUTURE = "THRU_DATE_NOT_FUTURE";
    public static final String ERROR_THRU_DATE_NOT_FUTURE_MSG = "Thru date not future date";
    public static final String ERROR_FROM_DATE_AFTER_THRU_DATE = "FROM_DATE_AFTER_THRU_DATE";
    public static final String ERROR_FROM_DATE_AFTER_THRU_DATE_MSG = "From date is after thru date";
    public static final String ERROR_THRU_DATE_BAD_FORMAT = "THRU_DATE_BAD_FORMAT";
    public static final String ERROR_THRU_DATE_BAD_FORMAT_MSG = "Thru date is in incorrect format";
    public static final String ERROR_PROGRAM_TYPE_INVALD = "PROGRAM_TYPE_INVALID";
    public static final String ERROR_PROGRAM_TYPE_INVALD_MSG = "Program type is not valid";
    public static final String ERROR_MISMATCHED_PROGRAM_IDS = "MISMATCHED_PROGRAM_IDS";
    public static final String ERROR_MISMATCHED_PROGRAM_IDS_MSG =
            "programId on URI path does not match programId in request";
    public static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    public static final String ERROR_INVALID_STATUS_MSG = " is not a valid status type code";
    public static final String ERROR_PROGRAM_NOT_FOUND = "PROGRAM_NOT_FOUND";
    public static final String ERROR_PROGRAM_NOT_FOUND_MSG = "Program was not found in the database";
    public static final String ERROR_INVALID_RECOGNITION_CRITERIA_ID = "CRITERIA_VALUE_INVALID";
    public static final String ERROR_INVALID_RECOGNITION_CRITERIA_ID_MSG = "Criteria values invalid";
    public static final String ERROR_INVALID_ECARD_ID = "ECARD_VALUE_INVALID";
    public static final String ERROR_INVALID_ECARD_ID_MSG = "Ecard values invalid";
    public static final String ERROR_MISSING_PROGRAM_ID = "MISSING_PROGRAM_ID";
    public static final String ERROR_MISSING_PROGRAM_ID_MSG = "Program ID is missing!";
    public static final String ERROR_PROGRAM_START_DATE_ALREADY_IN_USE = "PROGRAM_START_DATE_ALREADY_IN_USE";
    public static final String ERROR_PROGRAM_START_DATE_ALREADY_IN_USE_MSG =
            "Program start date cannot be changed after in use";
    public static final String ERROR_NOTIFICATION_INVALID = "NOTIFICATION_INVALID";
    public static final String ERROR_NOTIFICATION_INVALID_MSG = "Invalid notification type found.";
    public static final String ERROR_NOTIFICATION_NULL = "NOTIFICATION_NULL";
    public static final String ERROR_NOTIFICATION_NULL_MSG = "notification list not found.";
    public static final String ERROR_NEWSFEED_NULL = "NEWSFEED_VISIBLITY_NULL";
    public static final String ERROR_NEWSFEED_NULL_MSG = "newsfeed visibility not found.";
    public static final String ERROR_NEWSFEED_INVALID = "NEWSFEED_VISIBLITY_INVALID";
    public static final String ERROR_NEWSFEED_INVALID_MSG = "Invalid newsfeed visibility found.";
    public static final String ERROR_MISSING_ELIGIBILITY_ID = "MISSING_ELIGIBILITY_ID";
    public static final String ERROR_MISSING_ELIGIBILITY_ID_MSG = "Group or Pax ID is not defined for eligibility!!";
    public static final String ERROR_MULTIPLE_ELIGIBILITY_ID = "MULTIPLE_ELIGIBILITY_ID";
    public static final String ERROR_MULTIPLE_ELIGIBILITY_ID_MSG = "Eligibility has both Group and Pax ID defined!!";
    public static final String ERROR_ELIGIBILITY_ENTITY_NOT_FOUND = "ELIGIBILITY_ENTITY_NOT_FOUND";
    public static final String ERROR_ELIGIBILITY_ENTITY_NOT_FOUND_MSG = "Eligibility entity does not exist: ";
    public static final String ERROR_PAX_ELIGIBILITY_NOT_ACTIVE = "PAX_ELIGIBILITY_NOT_ACTIVE";
    public static final String ERROR_PAX_ELIGIBILITY_NOT_ACTIVE_MSG =
            "Specified eligibility participant is not active: ";
    public static final String ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE = "GROUP_ELIGIBILITY_NOT_ACTIVE";
    public static final String ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE_MSG = "Specified eligibility group is not active: ";
    public static final String ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE = "GROUP_ELIGIBILITY_INVALID_GROUP_TYPE";
    public static final String ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE_MSG =
            "Specified eligibility group type is not valid: ";
    public static final String ERROR_GIVING_MEMBER_NOT_FOUND = "ENTITY_GIVING_MEMBER_FOUND";
    public static final String ERROR_GIVING_MEMBER_NOT_FOUND_MSG = "Specified giving member does not exist: ";
    public static final String ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE = "PAX_GIVING_MEMBER_NOT_ACTIVE";
    public static final String ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE_MSG =
            "Specified giving member participant is not active: ";
    public static final String ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE = "GROUP_GIVING_MEMBER_NOT_ACTIVE";
    public static final String ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE_MSG =
            "Specified giving member group is not active: ";
    public static final String ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE = "GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE";
    public static final String ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE_MSG =
            "Specified giving member group type is not valid: ";
    public static final String ERROR_MISSING_BUDGET_ID = "MISSING_BUDGET_ID";
    public static final String ERROR_MISSING_BUDGET_ID_MSG = "Missing budget ID";
    public static final String ERROR_INVALID_BUDGET_ID = "INVALID_BUDGET_ID";
    public static final String ERROR_INVALID_BUDGET_ID_MSG = "Invalid budget ID";
    public static final String ERROR_BUDGET_NOT_ACTIVE = "BUDGET_NOT_ACTIVE";
    public static final String ERROR_BUDGET_NOT_ACTIVE_MSG = "Budget is not active";
    public static final String ERROR_AWARD_AMOUNT_DUPLICATED= "AWARD_AMOUNT_DUPLICATED";
    public static final String ERROR_AWARD_AMOUNT_DUPLICATED_MSG= "Award amount duplicated";
    public static final String ERROR_INVALID_APPROVAL_TYPE = "INVALID_APPROVAL_TYPE";
    public static final String ERROR_INVALID_APPROVAL_TYPE_MSG = "Invalid approval type";
    public static final String ERROR_MISSING_APPROVER_PAX = "MISSING_APPROVER_PAX";
    public static final String ERROR_MISSING_APPROVER_PAX_MSG = "Missing approver pax object for approval PERSON type";
    public static final String ERROR_INVALID_APPROVAL_LEVEL = "INVALID_APPROVAL_LEVEL";
    public static final String ERROR_INVALID_APPROVAL_LEVEL_MSG = "Invalid approval level";
    public static final String ERROR_APPROVAL_LEVEL_DUPLICATED = "APPROVAL_LEVEL_DUPLICATED";
    public static final String ERROR_APPROVAL_LEVEL_DUPLICATED_MSG = "Approval level are duplicated";
    public static final String ERROR_APPROVAL_TYPE_DUPLICATED = "APPROVAL_TYPE_DUPLICATED";
    public static final String ERROR_APPROVAL_TYPE_DUPLICATED_MSG = "Approval type duplicated";
    public static final String ERROR_MISSING_GIVING_MEMBER_ID = "MISSING_GIVING_MEMBER_ID";
    public static final String ERROR_MISSING_GIVING_MEMBER_ID_MSG = "Giving member ID is missing";
    public static final String ERROR_MULTIPLE_GIVING_MEMBER_ID = "MULTIPLE_GIVING_MEMBER_ID";
    public static final String ERROR_MULTIPLE_GIVING_MEMBER_ID_MSG = "Giving member should only have 1 ID";

    public static final String ERROR_MISSING_GIVING_MEMBERS = "MISSING_GIVING_MEMBERS";
    public static final String ERROR_MISSING_GIVING_MEMBERS_MSG = "Giving members are missing";
    public static final String ERROR_GIVING_MEMBERS_EMPTY = "GIVING_MEMBERS_EMPTY";
    public static final String ERROR_GIVING_MEMBERS_EMPTY_MSG = "At least one giving member must be provided";

    public static final String ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY =
            "BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY_ERROR";
    public static final String ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY_MSG =
            "Award Tier and Budgets should both be present together";

    public static final String ERROR_MISSING_AWARD_TIER_TYPE_CODE = "MISSING_AWARD_TIER_TYPE_CODE";
    public static final String ERROR_MISSING_AWARD_TIER_TYPE_MSG = "Award tier type is required";
    public static final String ERROR_INVALID_AWARD_TIER_TYPE_CODE = "INVALID_AWARD_TIER_TYPE_CODE";
    public static final String ERROR_INVALID_AWARD_TIER_TYPE_MSG = "Award tier type is invalid";
    public static final String ERROR_MISSING_AWARD_MIN_CODE = "MISSING_AWARD_MIN_CODE";
    public static final String ERROR_MISSING_AWARD_MIN_MSG = "Award min is required";
    public static final String ERROR_MISSING_AWARD_MAX_CODE = "MISSING_AWARD_MAX_CODE";
    public static final String ERROR_MISSING_AWARD_MAX_MSG = "Award max is required";
    public static final String ERROR_AWARD_MIN_MINIMUM_VALUE_CODE = "ERROR_AWARD_MIN_MINIMUM_VALUE";
    public static final String ERROR_AWARD_MIN_MINIMUM_VALUE_MSG = "Award min is below the minimum allowed value";
    public static final String ERROR_AWARD_MIN_MAX_CAN_NOT_EQUAL_CODE = "ERROR_AWARD_MIN_MAX_CAN_NOT_EQUAL_CODE";
    public static final String ERROR_AWARD_MIN_MAX_CAN_NOT_EQUAL_MSG = "Award min and max values can not equal";
    public static final String ERROR_AWARD_MIN_GREATER_THAN_MAX_CODE = "ERROR_AWARD_MIN_GREATER_THAN_MAX_CODE";
    public static final String ERROR_AWARD_MIN_GREATER_THAN_MAX_MSG = "Award min can not be greater than max";
    public static final String ERROR_MISSING_AWARD_AMOUNT_CODE = "ERROR_MISSING_AWARD_AMOUNT_CODE";
    public static final String ERROR_MISSING_AWARD_AMOUNT_MSG = "Award amount is required";
    public static final String ERROR_AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_CODE =
            "ERROR_AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET";
    public static final String ERROR_AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_MSG =
            "Award amount is larger than allowed budget";
    public static final String ERROR_AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_CODE =
            "ERROR_AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET";
    public static final String ERROR_AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_MSG =
            "Award max is larger than allowed budget";

    public static final String ERROR_MISSING_AWARD_CODE_OBJECT_CODE = "MISSING_AWARD_CODE_OBJECT_CODE";
    public static final String ERROR_MISSING_AWARD_CODE_OBJECT_MSG = "Award Code object is required";
    public static final String ERROR_MISSING_CLAIMING_INSTRUCTIONS_CODE = "MISSING_CLAIMING_INSTRUCTIONS_CODE";
    public static final String ERROR_MISSING_CLAIMING_INSTRUCTIONS_MSG = "Claiming instructions are required";
    public static final String ERROR_MISSING_ICON_ID_CODE = "MISSING_ICON_ID_CODE";
    public static final String ERROR_MISSING_ICON_ID_MSG = "Icon Id required";
    public static final String ERROR_INVALID_ICON_ID_CODE = "INVALID_ICON_ID_CODE";
    public static final String ERROR_INVALID_ICON_ID_MSG = "Invalid icon Id";
    public static final String ERROR_AWARD_CODE_INVALID_MODE_CODE = "AWARD_CODE_INVALID_MODE_CODE";
    public static final String ERROR_AWARD_CODE_INVALID_MODE_MSG = "At least one mode must be selected";
    public static final String ERROR_MISSING_CERTIFICATES_CODE = "MISSING_CERTIFICATES_CODE";
    public static final String ERROR_MISSING_CERTIFICATES_MSG = "Certificate Ids are required";
    public static final String ERROR_CERTIFICATES_NOT_REQUIRED_CODE = "CERTIFICATES_NOT_REQUIRED_CODE";
    public static final String ERROR_CERTIFICATES_NOT_REQUIRED_MSG =
            "Certificates are not required if program is not printable";
    public static final String ERROR_INVALID_CERTIFICATES_CODE = "INVALID_CERTIFICATES_CODE";
    public static final String ERROR_INVALID_CERTIFICATES_MSG = "Certificates not found";
    public static final String ERROR_AWARD_CODE_AWARD_TIERS_ALLOW_RAISING = "ALLOW_RAISING_NOT_ALLOWED";
    public static final String ERROR_AWARD_CODE_AWARD_TIERS_ALLOW_RAISING_MESSAGE =
            "allowRaising is not valid for award code programs.";
    public static final String ERROR_AWARD_CODE_AWARD_TIERS_APPROVALS = "APPROVALS_NOT_ALLOWED";
    public static final String ERROR_AWARD_CODE_AWARD_TIERS_APPROVALS_MESSAGE =
            "approvals are not valid for award code programs.";

    public static final String ERROR_MISSING_ELIGIBILITY_CODE = "MISSING_ELIGIBILITY_CODE";
    public static final String ELIGIBILITY_FIELD = "eligibility";
    public static final String ERROR_MISSING_ELIGIBILITY_MESSAGE = "Eligibility is required";
    public static final String ERROR_PAYOUT_TYPE_INVALID = "PAYOUT_TYPE_INVALID";
    public static final String ERROR_PAYOUT_TYPE_INVALID_MSG = "Invalid payoutType found.";
    public static final String ERROR_PAYOUT_TYPE_MISSING = "PAYOUT_TYPE_MISSING";
    public static final String ERROR_PAYOUT_TYPE_MISSING_MSG = "payoutType missing or Empty. Required when a budget is chosen";
    public static final String PROGRAM_VISIBILITY_ROLE_NAME = "Program Visibility";
    public static final String ERROR_PROGRAM_VISIBILITY_INVALID = "PROGRAM_VISIBILITY_INVALID";
    public static final String ERROR_PROGRAM_VISIBILITY_INVALID_MSG = "Invalid program visibility found.";
    public static final String ERROR_PROGRAM_VISIBILITY_NULL = "PROGRAM_VISIBILITY_NULL";
    public static final String ERROR_PROGRAM_VISIBILITY_NULL_MSG = "Program visibility not found.";
    public static final String ERROR_MILESTONE_REMINDERS_NULL = "MILESTONE_REMINDERS_NULL";
    public static final String ERROR_MILESTONE_REMINDERS_NULL_MSG = "One or more milestone reminders were not provided.";
    public static final String ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT = "ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT";
    public static final String ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MSG = "If the nomination has an award amount, a budgetId is required.";

    public static final String ALLOW_TEMPLATE_UPLOAD_FIELD = "allowTemplateUpload";
    public static final String ERROR_ALLOW_TEMPLATE_UPLOAD_MISSING = "ALLOW_TEMPLATE_UPLOAD_MISSING";
    public static final String ERROR_ALLOW_TEMPLATE_UPLOAD_MISSING_MESSAGE =
            "allowTemplateUpload must be provided for this program type.";
    public static final String ERROR_ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED = "ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED";
    public static final String ERROR_ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED_MESSAGE =
            "allowTemplateUpload is not allowed for this program type.";
    public static final String ERROR_HEADLINE_MISSING = "ERROR_HEADLINE_MISSING";
    public static final String ERROR_HEADLINE_MISSING_MSG = "Public headline is required.";
    public static final String ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT = "ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT";
    public static final String ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MSG = "If awardAmount or budgetId is supplied, payoutType is required.";

    public static final String ERROR_MISSING_AWARD_TIER_REMINDER_EMAIL = "EMAIL_REMINDER_ENABLED_MISSING";
    public static final String ERROR_MISSING_AWARD_TIER_REMINDER_EMAIL_MSG = "Email reminder ENABLED field missing or empty. It is required in award tier values.";
    public static final String AWARD_TIER_REMINDER_ENABLED_FIELD = "ENABLED";
    public static final String PROGRAM_AwARD_TIER_ID_FIELD = "PROGRAM_AWARD_TIER_ID";

    //Error Messages
    public static final ErrorMessage MISMATCHED_PROGRAM_IDS_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISMATCHED_PROGRAM_IDS)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ProgramConstants.ERROR_MISMATCHED_PROGRAM_IDS_MSG);

    public static final ErrorMessage PROGRAM_NAME_NULL_NAME = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_NAME_NULL)
        .setField(ProjectConstants.PROGRAM_NAME)
        .setMessage(ProgramConstants.ERROR_PROGRAM_NAME_NULL_MSG);

    public static final ErrorMessage DUPLICATE_PROGRAM_NAME_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_DUPLICATE_PROGRAM_NAME)
        .setField(ProjectConstants.PROGRAM_NAME)
        .setMessage(ProgramConstants.ERROR_DUPLICATE_PROGRAM_NAME_MSG);

    public static final ErrorMessage PROGRAM_START_DATE_ALREADY_IN_USE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_START_DATE_ALREADY_IN_USE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramConstants.ERROR_PROGRAM_START_DATE_ALREADY_IN_USE_MSG);

    public static final ErrorMessage FROM_DATE_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_FROM_DATE_NULL)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramConstants.ERROR_FROM_DATE_NULL_MSG);

    public static final ErrorMessage FROM_DATE_BAD_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_FROM_DATE_BAD_FORMAT)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramConstants.ERROR_FROM_DATE_BAD_FORMAT_MSG);

    public static final ErrorMessage FROM_DATE_NOT_FUTURE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_FROM_DATE_NOT_FUTURE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramConstants.ERROR_FROM_DATE_NOT_FUTURE_MSG);

    public static final ErrorMessage FROM_DATE_AFTER_THRU_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_FROM_DATE_AFTER_THRU_DATE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ProgramConstants.ERROR_FROM_DATE_AFTER_THRU_DATE_MSG);

    public static final ErrorMessage THRU_DATE_BAD_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_THRU_DATE_BAD_FORMAT)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ProgramConstants.ERROR_THRU_DATE_BAD_FORMAT_MSG);

    public static final ErrorMessage THRU_DATE_NOT_FUTURE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_THRU_DATE_NOT_FUTURE)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ProgramConstants.ERROR_THRU_DATE_NOT_FUTURE_MSG);

    public static final ErrorMessage SHORT_DESCRIPTION_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_SHORT_DESCRIPTION_NULL)
        .setField(ProjectConstants.PROGRAM_DESCRIPTION_SHORT)
        .setMessage(ProgramConstants.ERROR_SHORT_DESCRIPTION_NULL_MSG);

    public static final ErrorMessage SHORT_PROGRAM_DESCRIPTION_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_SHORT_PROGRAM_DESCRIPTION_TOO_LONG)
        .setField(ProjectConstants.PROGRAM_DESCRIPTION_SHORT)
        .setMessage(ProgramConstants.ERROR_SHORT_PROGRAM_DESCRIPTION_TOO_LONG_MSG);

    public static final ErrorMessage LONG_DESCRIPTION_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_LONG_DESCRIPTION_NULL)
        .setField(ProjectConstants.PROGRAM_DESCRIPTION_LONG)
        .setMessage(ProgramConstants.ERROR_LONG_DESCRIPTION_NULL_MSG);

    public static final ErrorMessage LONG_PROGRAM_DESCRIPTION_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_LONG_PROGRAM_DESCRIPTION_TOO_LONG)
        .setField(ProjectConstants.PROGRAM_DESCRIPTION_LONG)
        .setMessage(ProgramConstants.ERROR_LONG_PROGRAM_DESCRIPTION_TOO_LONG_MSG);

    public static final ErrorMessage PROGRAM_NAME_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_NAME_TOO_LONG)
        .setField(ProjectConstants.PROGRAM_NAME)
        .setMessage(ProgramConstants.ERROR_PROGRAM_NAME_TOO_LONG_MSG);

    public static final ErrorMessage INVALID_RECOGNTITION_CRITERIA_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_RECOGNITION_CRITERIA_ID)
        .setField(ProjectConstants.RECOGNITION_CRITERIA)
        .setMessage(ProgramConstants.ERROR_INVALID_RECOGNITION_CRITERIA_ID_MSG);

    public static final ErrorMessage INVALID_ECARD_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_ECARD_ID)
        .setField(ProjectConstants.ECARD_ID)
        .setMessage(ProgramConstants.ERROR_INVALID_ECARD_ID_MSG);

    public static final ErrorMessage PROGRAM_VISIBILITY_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_VISIBILITY_NULL)
        .setField(ProjectConstants.PROGRAM_VISIBILITY)
        .setMessage(ProgramConstants.ERROR_PROGRAM_VISIBILITY_NULL_MSG);

    public static final ErrorMessage PROGRAM_VISIBILITY_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_VISIBILITY_INVALID)
        .setField(ProjectConstants.PROGRAM_VISIBILITY)
        .setMessage(ProgramConstants.ERROR_PROGRAM_VISIBILITY_INVALID_MSG);

    public static final ErrorMessage PROGRAM_MORE_THAN_ONE_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_MORE_THAN_ONE_BUDGET)
        .setField(ProjectConstants.PROGRAM_BUDGET)
        .setMessage(ProgramConstants.ERROR_PROGRAM_MORE_THAN_ONE_BUDGET_MSG);

    public static final ErrorMessage PROGRAM_NON_SIMPLE_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_NON_SIMPLE_BUDGET)
        .setField(ProjectConstants.PROGRAM_BUDGET)
        .setMessage(ProgramConstants.ERROR_PROGRAM_NON_SIMPLE_BUDGET_MSG);

    public static final ErrorMessage NOTIFICATION_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NOTIFICATION_NULL)
        .setField(ProjectConstants.NOTIFICATION)
        .setMessage(ProgramConstants.ERROR_NOTIFICATION_NULL_MSG);

    public static final ErrorMessage NOTIFICATION_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NOTIFICATION_INVALID)
        .setField(ProjectConstants.NOTIFICATION)
        .setMessage(ProgramConstants.ERROR_NOTIFICATION_INVALID_MSG);

    public static final ErrorMessage NEWSFEED_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NEWSFEED_NULL)
        .setField(ProjectConstants.NEWSFEED_VISIBILITY)
        .setMessage(ProgramConstants.ERROR_NEWSFEED_NULL_MSG);

    public static final ErrorMessage NEWSFEED_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NEWSFEED_INVALID)
        .setField(ProjectConstants.NEWSFEED_VISIBILITY)
        .setMessage(ProgramConstants.ERROR_NEWSFEED_INVALID_MSG);

    public static final ErrorMessage ALLOW_TEMPLATE_UPLOAD_MISSING_MESSAGE = new ErrorMessage()
        .setField(ProgramConstants.ALLOW_TEMPLATE_UPLOAD_FIELD)
        .setCode(ProgramConstants.ERROR_ALLOW_TEMPLATE_UPLOAD_MISSING)
        .setMessage(ProgramConstants.ERROR_ALLOW_TEMPLATE_UPLOAD_MISSING_MESSAGE);

    public static final ErrorMessage ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setField(ProgramConstants.ALLOW_TEMPLATE_UPLOAD_FIELD)
        .setCode(ProgramConstants.ERROR_ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED)
        .setMessage(ProgramConstants.ERROR_ALLOW_TEMPLATE_UPLOAD_NOT_ALLOWED_MESSAGE);

    public static final ErrorMessage MISSING_PROGRAM_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ProgramConstants.ERROR_MISSING_PROGRAM_ID_MSG);

    public static final ErrorMessage PROGRAM_TYPE_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_TYPE_NULL)
        .setField(ProjectConstants.PROGRAM_TYPE)
        .setMessage(ProgramConstants.ERROR_PROGRAM_TYPE_NULL_MSG);

    public static final ErrorMessage PROGRAM_TYPE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PROGRAM_TYPE_INVALD)
        .setField(ProjectConstants.PROGRAM_TYPE)
        .setMessage(ProgramConstants.ERROR_PROGRAM_TYPE_INVALD_MSG);

    public static final ErrorMessage MISSING_AWARD_TIER_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_TIER_TYPE_CODE)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_TIER_TYPE_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage INVALID_AWARD_TIER_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_AWARD_TIER_TYPE_CODE)
        .setMessage(ProgramConstants.ERROR_INVALID_AWARD_TIER_TYPE_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage MISSING_AWARD_MIN_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_MIN_CODE)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_MIN_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage MISSING_AWARD_MAX_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_MAX_CODE)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_MAX_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_MIN_MINIMUM_VALUE_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_MIN_MINIMUM_VALUE_CODE)
        .setMessage(ProgramConstants.ERROR_AWARD_MIN_MINIMUM_VALUE_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_CODE)
        .setMessage(ProgramConstants.ERROR_AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_MIN_GREATER_THAN_MAX_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_MIN_GREATER_THAN_MAX_CODE)
        .setMessage(ProgramConstants.ERROR_AWARD_MIN_GREATER_THAN_MAX_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_MIN_MAX_CAN_NOT_EQUAL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_MIN_MAX_CAN_NOT_EQUAL_CODE)
        .setMessage(ProgramConstants.ERROR_AWARD_MIN_MAX_CAN_NOT_EQUAL_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage MISSING_AWARD_AMOUNT_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_AMOUNT_CODE)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_AMOUNT_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_CODE)
        .setMessage(ProgramConstants.ERROR_AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_MSG)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_CODE_AWARD_TIERS_ALLOW_RAISING_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_CODE_AWARD_TIERS_ALLOW_RAISING)
        .setMessage(ProgramConstants.ERROR_AWARD_CODE_AWARD_TIERS_ALLOW_RAISING_MESSAGE)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage AWARD_CODE_AWARD_TIERS_APPROVALS_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_CODE_AWARD_TIERS_APPROVALS)
        .setMessage(ProgramConstants.ERROR_AWARD_CODE_AWARD_TIERS_APPROVALS_MESSAGE)
        .setField(ProjectConstants.AWARD_TIER);

    public static final ErrorMessage MISSING_CLAIMING_INSTRUCTIONS_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_CLAIMING_INSTRUCTIONS_CODE)
        .setField(ProjectConstants.CLAIMING_INSTRUCTIONS)
        .setMessage(ProgramConstants.ERROR_MISSING_CLAIMING_INSTRUCTIONS_MSG);

    public static final ErrorMessage MISSING_ICON_ID_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_ICON_ID_CODE)
        .setField(ProjectConstants.ICON_ID)
        .setMessage(ProgramConstants.ERROR_MISSING_ICON_ID_MSG);

    public static final ErrorMessage INVALID_ICON_ID_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_ICON_ID_CODE)
        .setField(ProjectConstants.ICON_ID)
        .setMessage(ProgramConstants.ERROR_INVALID_ICON_ID_MSG);

    public static final ErrorMessage AWARD_CODE_INVALID_MODE_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_CODE_INVALID_MODE_CODE)
        .setField(ProjectConstants.IS_AWARD_CODE_PRINTABLE + " or " + ProjectConstants.IS_AWARD_CODE_DOWNLOADABLE)
        .setMessage(ProgramConstants.ERROR_AWARD_CODE_INVALID_MODE_MSG);

    public static final ErrorMessage MISSING_CERTIFICATES_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_CERTIFICATES_CODE)
        .setField(ProjectConstants.CERTIFICATE_IDS)
        .setMessage(ProgramConstants.ERROR_MISSING_CERTIFICATES_MSG);

    public static final ErrorMessage CERTIFICATES_NOT_REQUIRED_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_CERTIFICATES_NOT_REQUIRED_CODE)
        .setField(ProjectConstants.CERTIFICATE_IDS)
        .setMessage(ProgramConstants.ERROR_CERTIFICATES_NOT_REQUIRED_MSG);

    public static final ErrorMessage INVALID_CERTIFICATES_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_CERTIFICATES_CODE)
        .setField(ProjectConstants.CERTIFICATE_IDS)
        .setMessage(ProgramConstants.ERROR_INVALID_CERTIFICATES_MSG);

    public static final ErrorMessage MISSING_AWARD_CODE_OBJECT_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_CODE_OBJECT_CODE)
        .setField(ProjectConstants.AWARD_CODE)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_CODE_OBJECT_MSG);

    public static final ErrorMessage AWARD_TIER_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED)
        .setField(ProjectConstants.AWARD_TIERS)
        .setMessage(ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED_MSG);

    public static final ErrorMessage MISSING_BUDGET_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_BUDGET_ID)
        .setField(ProjectConstants.BUDGET_ASSIGNMENTS)
        .setMessage(ProgramConstants.ERROR_MISSING_BUDGET_ID_MSG);

    public static final ErrorMessage INVALID_BUDGET_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_BUDGET_ID)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(ProgramConstants.ERROR_INVALID_BUDGET_ID_MSG);

    public static final ErrorMessage BUDGET_NOT_ACTIVE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_BUDGET_NOT_ACTIVE)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(ProgramConstants.ERROR_BUDGET_NOT_ACTIVE_MSG);

    public static final ErrorMessage MISSING_GIVING_MEMBER_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_GIVING_MEMBER_ID)
        .setField(ProjectConstants.GIVING_MEMBERS)
        .setMessage(ProgramConstants.ERROR_MISSING_GIVING_MEMBER_ID_MSG);

    public static final ErrorMessage MULTIPLE_GIVING_MEMBER_ID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MULTIPLE_GIVING_MEMBER_ID)
        .setField(ProjectConstants.GIVING_MEMBERS)
        .setMessage(ProgramConstants.ERROR_MULTIPLE_GIVING_MEMBER_ID_MSG);

    public static final ErrorMessage MISSING_ELIGIBILITY_ID_MESSAGE = new ErrorMessage()
           .setCode(ProgramConstants.ERROR_MISSING_ELIGIBILITY_ID)
           .setField(ProjectConstants.ELIGIBILITY)
           .setMessage(ProgramConstants.ERROR_MISSING_ELIGIBILITY_ID_MSG);

    public static final ErrorMessage MULTIPLE_ELIGIBILITY_ID_MESSAGE = new ErrorMessage()
           .setCode(ProgramConstants.ERROR_MULTIPLE_ELIGIBILITY_ID)
           .setField(ProjectConstants.ELIGIBILITY)
           .setMessage(ProgramConstants.ERROR_MULTIPLE_ELIGIBILITY_ID_MSG);

    public static final ErrorMessage MISSING_GIVING_MEMBERS_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_GIVING_MEMBERS)
        .setField(ProjectConstants.GIVING_MEMBERS)
        .setMessage(ProgramConstants.ERROR_MISSING_GIVING_MEMBERS_MSG);

  public static final ErrorMessage GIVING_MEMBERS_EMPTY_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_GIVING_MEMBERS_EMPTY)
        .setField(ProjectConstants.GIVING_MEMBERS)
        .setMessage(ProgramConstants.ERROR_GIVING_MEMBERS_EMPTY_MSG);

    public static final ErrorMessage BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY)
        .setField(ProjectConstants.BUDGET_ASSIGNMENTS)
        .setMessage(ProgramConstants.ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY_MSG);

    public static final ErrorMessage AWARD_AMOUNT_DUPLICATED_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_AWARD_AMOUNT_DUPLICATED)
        .setField(ProjectConstants.AMOUNT)
        .setMessage(ProgramConstants.ERROR_AWARD_AMOUNT_DUPLICATED_MSG);

    public static final ErrorMessage MISSING_AWARD_AMOUNT_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_AWARD_AMOUNT_CODE)
        .setField(ProjectConstants.AMOUNT)
        .setMessage(ProgramConstants.ERROR_MISSING_AWARD_AMOUNT_MSG);

    public static final ErrorMessage APPROVAL_LEVEL_DUPLICATED_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_APPROVAL_LEVEL_DUPLICATED)
        .setField(ProjectConstants.LEVEL)
        .setMessage(ProgramConstants.ERROR_APPROVAL_LEVEL_DUPLICATED_MSG);

    public static final ErrorMessage INVALID_APPROVAL_LEVEL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_APPROVAL_LEVEL)
        .setField(ProjectConstants.LEVEL)
        .setMessage(ProgramConstants.ERROR_INVALID_APPROVAL_LEVEL_MSG);

    public static final ErrorMessage INVALID_APPROVAL_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_INVALID_APPROVAL_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ProgramConstants.ERROR_INVALID_APPROVAL_TYPE_MSG);

    public static final ErrorMessage MISSING_APPROVER_PAX_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_APPROVER_PAX)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ProgramConstants.ERROR_MISSING_APPROVER_PAX_MSG);

    public static final ErrorMessage APPROVAL_TYPE_DUPLICATED_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_APPROVAL_TYPE_DUPLICATED)
        .setField(ProjectConstants.TYPE)
        .setMessage(ProgramConstants.ERROR_APPROVAL_TYPE_DUPLICATED_MSG);

    public static final ErrorMessage MISSING_ELIGIBILITY_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_ELIGIBILITY_CODE)
        .setField(ELIGIBILITY_FIELD)
        .setMessage(ERROR_MISSING_ELIGIBILITY_MESSAGE);

    public static final ErrorMessage PAYOUT_TYPE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PAYOUT_TYPE_INVALID)
        .setField(ProjectConstants.PAYOUT_TYPE)
        .setMessage(ProgramConstants.ERROR_PAYOUT_TYPE_INVALID_MSG);

    public static final ErrorMessage PAYOUT_TYPE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_PAYOUT_TYPE_MISSING)
        .setField(ProjectConstants.PAYOUT_TYPE)
        .setMessage(ProgramConstants.ERROR_PAYOUT_TYPE_MISSING_MSG);

    public static final ErrorMessage MILESTONE_REMINDERS_NULL_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MILESTONE_REMINDERS_NULL)
        .setField(ProjectConstants.MILESTONE_REMINDERS)
        .setMessage(ProgramConstants.ERROR_MILESTONE_REMINDERS_NULL_MSG);

  public static final ErrorMessage HEADLINE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_HEADLINE_MISSING)
        .setMessage(ProgramConstants.ERROR_HEADLINE_MISSING_MSG)
        .setField(ProjectConstants.HEADLINE);

  public static final ErrorMessage ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT)
        .setMessage(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MSG)
        .setField(ProjectConstants.BUDGET_ID);

  public static final ErrorMessage ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT)
        .setMessage(ProgramConstants.ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MSG)
        .setField(ProjectConstants.PAYOUT_TYPE);

    public static final ErrorMessage ERROR_MISSING_AWARD_TIER_REMINDER_MESSAGE = new ErrorMessage()
            .setCode(ProgramConstants.ERROR_MISSING_AWARD_TIER_REMINDER_EMAIL)
            .setMessage(ProgramConstants.ERROR_MISSING_AWARD_TIER_REMINDER_EMAIL_MSG)
            .setField(ProgramConstants.AWARD_TIER_REMINDER_ENABLED_FIELD);
}
