package com.maritz.culturenext.program.event.listener;

import javax.inject.Inject;

import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.program.event.ProgramCreateUpdateEvent;

/**
 * Listens for ProgramCreateUpdateEvent that is fired after a program is created or updated and lets us do things with it
 * @author muddam
 *
 */
@Component
public class ProgramCreateUpdateEventListener {
    
    @Inject BudgetEligibilityCacheService budgetEligibilityCacheService;

    @Async
    @EventListener
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMPLETION)
    public void handleProgramCreateUpdateEvent(ProgramCreateUpdateEvent event) {
        budgetEligibilityCacheService.buildCache();
    }

}
