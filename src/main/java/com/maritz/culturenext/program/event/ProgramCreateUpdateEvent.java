package com.maritz.culturenext.program.event;

import org.springframework.context.ApplicationEvent;

import com.maritz.core.jpa.entity.Program;

public class ProgramCreateUpdateEvent extends ApplicationEvent {

    public ProgramCreateUpdateEvent(Program program) {
        super(program);
    }
    
    public Program getProgram() {
        return (Program) this.getSource();
    }

}
