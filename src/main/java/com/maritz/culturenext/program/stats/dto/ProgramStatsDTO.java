package com.maritz.culturenext.program.stats.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.util.ConversionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the general stats for the program(s). Information includes number of "
        + "recognitions received and given, percentage of total eligible users defined for the program(s) that "
        + "are giving and receiving, and total points given and received from the accounted recognitions.")
public class ProgramStatsDTO {
    // node column names
    private static final String RECOGNITIONS_GIVEN = "RECOGNITIONS_GIVEN";
    private static final String RECOGNITIONS_RECEIVED = "RECOGNITIONS_RECEIVED";
    private static final String PERCENT_ELIGIBLE_USERS_GIVING = "PERCENT_ELIGIBLE_USERS_GIVING";
    private static final String PERCENT_ELIGIBLE_USERS_RECEIVING = "PERCENT_ELIGIBLE_USERS_RECEIVING";
    private static final String POINTS_GIVEN = "POINTS_GIVEN";
    private static final String POINTS_RECEIVED = "POINTS_RECEIVED";

    private Long recognitionsGiven;
    private Long recognitionsReceived;
    private BigDecimal percentEligibleUsersGiving;
    private BigDecimal percentEligibleUsersReceiving;
    private Long pointsGiven;
    private Long pointsReceived;

    /**
     * Default (empty) constructor.
     */
    public ProgramStatsDTO() {}

    /**
     * Constructor from object mapping (query).
     *
     * @param node object mapping from query
     */
    public ProgramStatsDTO(Map<String, Object> node) {
        if(node == null) {
            return;
        }

        if(node.containsKey(RECOGNITIONS_GIVEN)) {
            this.recognitionsGiven =
                    ConversionUtil.convertLongObjectMapValue(node.get(RECOGNITIONS_GIVEN));
    }

        if(node.containsKey(RECOGNITIONS_RECEIVED)) {
            this.recognitionsReceived =
                    ConversionUtil.convertLongObjectMapValue(node.get(RECOGNITIONS_RECEIVED));
        }

        // Percentage returned is between 0 and 1 (fractional) -- need to multiply by 100
        if(node.containsKey(PERCENT_ELIGIBLE_USERS_GIVING)) {
            BigDecimal percentageGivers =
                    ConversionUtil.convertBigDecimalObjectMapValue(node.get(PERCENT_ELIGIBLE_USERS_GIVING))
                    .multiply(new BigDecimal(100.00));

            // need to round to nearest hundredth (only after multiplying)
            this.percentEligibleUsersGiving =
                    percentageGivers.setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        // Percentage returned is between 0 and 1 (fractional) -- need to multiply by 100
        if(node.containsKey(PERCENT_ELIGIBLE_USERS_RECEIVING)) {
            BigDecimal percentageGivers =
                    ConversionUtil.convertBigDecimalObjectMapValue(node.get(PERCENT_ELIGIBLE_USERS_RECEIVING))
                    .multiply(new BigDecimal(100.00));

            // need to round to nearest hundredth (only after multiplying)
            this.percentEligibleUsersReceiving =
                    percentageGivers.setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        if(node.containsKey(POINTS_GIVEN)) {
            this.pointsGiven =
                    ConversionUtil.convertLongObjectMapValue(node.get(POINTS_GIVEN));
        }

        if(node.containsKey(POINTS_RECEIVED)) {
            this.pointsReceived =
                    ConversionUtil.convertLongObjectMapValue(node.get(POINTS_RECEIVED));
        }
    }

    @ApiModelProperty(position = 1, value = "Total number of recognitions given")
    public Long getRecognitionsGiven() {
        return recognitionsGiven;
    }
    public void setRecognitionsGiven(Long recognitionsGiven) {
        this.recognitionsGiven = recognitionsGiven;
    }

    @ApiModelProperty(position = 2, value = "Total number of recognitions received")
    public Long getRecognitionsReceived() {
        return recognitionsReceived;
    }
    public void setRecognitionsReceived(Long recognitionsReceived) {
        this.recognitionsReceived = recognitionsReceived;
    }

    @ApiModelProperty(position = 3, 
            value = "Percentage of users giving (# of users giving / # of eligible users that can give)")
    public BigDecimal getPercentEligibleUsersGiving() {
        return percentEligibleUsersGiving;
    }
    public void setPercentEligibleUsersGiving(BigDecimal percentEligibleUsersGiving) {
        this.percentEligibleUsersGiving = percentEligibleUsersGiving;
    }

    @ApiModelProperty(position = 4, 
            value = "Percentage of users receiving (# of users receiving / # of eligible users that can receive)")
    public BigDecimal getPercentEligibleUsersReceiving() {
        return percentEligibleUsersReceiving;
    }
    public void setPercentEligibleUsersReceiving(BigDecimal percentEligibleUsersReceiving) {
        this.percentEligibleUsersReceiving = percentEligibleUsersReceiving;
    }

    @ApiModelProperty(position = 5, value = "Total number of points given")
    public Long getPointsGiven() {
        return pointsGiven;
    }
    public void setPointsGiven(Long pointsGiven) {
        this.pointsGiven = pointsGiven;
    }

    @ApiModelProperty(position = 6, value = "Total number of points received")
    public Long getPointsReceived() {
        return pointsReceived;
    }
    public void setPointsReceived(Long pointsReceived) {
        this.pointsReceived = pointsReceived;
    }
}
