package com.maritz.culturenext.program.stats.dao;

import javax.annotation.Nonnull;

import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;

import java.util.Date;
import java.util.List;

public interface StatsDao {
    @Nonnull ProgramStatsDTO getProgramStats(@Nonnull List<Long> programIds,
                                             List<Long> userIds,
                                             @Nonnull Date startDate,
                                             @Nonnull Date endDate);

    @Nonnull ProgramStatsDTO getProgramStatsAllUsers(@Nonnull List<Long> programIds,
                                                     @Nonnull Date startDate,
                                                     @Nonnull Date endDate);

    @Nonnull List<ProgramRecognitionCriteriaStatsDTO> 
                getProgramRecognitionCriteriaStats(@Nonnull List<Long> programIds,
                                                   @Nonnull List<Long> userIds,
                                                   @Nonnull Date startDate,
                                                   @Nonnull Date endDate);

    @Nonnull List<ProgramRecognitionCriteriaStatsDTO> 
                getProgramRecognitionCriteriaStatsAllUsers(@Nonnull List<Long> programIds,
                                                              @Nonnull Date startDate,
                                                              @Nonnull Date endDate);


    @Nonnull LoginCountDTO getAllLogins(@Nonnull List<Long> userIds,
                                        @Nonnull Date startDate,
                                        @Nonnull Date endDate);

    @Nonnull ParticipantActivityStatsDTO getActivityStats(@Nonnull List<Long> programIds,
                                                          @Nonnull List<Long> userIds,
                                                          @Nonnull Date startDate,
                                                          @Nonnull Date endDate);
}
