package com.maritz.culturenext.program.stats.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the engagement score for the particular user(s).")
public class PercentEngagementScoreDTO {

    private Double percentEngagementScore;

    @ApiModelProperty(position = 1, value = "engagement score percentage")
    public Double getPercentEngagementScore() {
        return percentEngagementScore;
    }
    public void setPercentEngagementScore(Double percentEngagementScore) {
        this.percentEngagementScore = percentEngagementScore;
    }
}
