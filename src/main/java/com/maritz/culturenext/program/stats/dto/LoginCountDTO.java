package com.maritz.culturenext.program.stats.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the login count.")
public class LoginCountDTO {
    // node column names
    private static final String LOGIN_COUNT = "LOGIN_COUNT";

    private Long loginCount;

    /**
     * Default constructor.
     */
    public LoginCountDTO() {
    }

    /**
     * Constructor from object mapping (query).
     *
     * @param node object mapping from query
     */
    public LoginCountDTO(Map<String, Object> node) {
        if(node == null) {
            return;
        }

        if(node.containsKey(LOGIN_COUNT)) {
            this.loginCount = (Long) node.get(LOGIN_COUNT);
        }
    }

    @ApiModelProperty(position = 1, required = true, value = "total count of logins")
    public Long getLoginCount() {
        return loginCount;
    }
    public void setLoginCount(Long loginCount) {
        this.loginCount = loginCount;
    }
}
