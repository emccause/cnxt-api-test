package com.maritz.culturenext.program.stats.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;
import com.maritz.culturenext.program.stats.services.StatsService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(description = "Endpoint for stats (programs and recognition criterion)")
public class StatsRestService {

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private Security security;
    @Inject private StatsService statsService;

    //rest/programs/stats
    @RequestMapping(value = "programs/stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves stats",
            notes = "This method allows a user to retrieve stats for programs and "
                    + "users determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of stats", response = ProgramStatsDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProgramStatsDTO getProgramStats(
            @ApiParam(value = "ID of query", required = false) 
                @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Exception
    {
        return statsService.getProgramStats(queryId);
    }

    //rest/programs/~/stats
    @RequestMapping(value = "programs/{programId}/stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves stats for a particular program",
            notes = "This method allows a user to retrieve stats for the specified program and"
                    + " users determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of stats for specified program", 
                    response = ProgramStatsDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProgramStatsDTO getProgramStats(
            @ApiParam(value = "ID of program", required = true) @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString,
            @ApiParam(value = "ID of query", required = false) 
                @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Exception
    {
        Long programId = convertPathVariablesUtil.getId(programIdString);
        
        return statsService.getProgramStats(queryId, programId);
    }

    //rest/programs/values-stats
    @RequestMapping(value = "programs/values-stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves recognition criteria stats",
            notes = "This method allows a user to retrieve recognition criteria stats associated with "
                    + "programs and users determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of recognition criteria stats", 
                    response = ProgramRecognitionCriteriaStatsDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(
            @ApiParam(value = "ID of query", required = false) 
                @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Exception
    {

        return statsService.getProgramRecognitionCriteriaStats(queryId);
    }

    //rest/programs/~/values-stats
    @RequestMapping(value = "programs/{programId}/values-stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves recognition criteria stats for the specified program",
            notes = "This method allows a user to retrieve recognition criteria stats associated with "
                    + "the specified program and users determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of recognition criteria stats", 
                    response = ProgramRecognitionCriteriaStatsDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(
            @ApiParam(value = "ID of program", required = true) @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString,
            @ApiParam(value = "ID of query", required = false) 
                @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Exception
    {
        Long programId = convertPathVariablesUtil.getId(programIdString);
        
        return statsService.getProgramRecognitionCriteriaStats(queryId, programId);
    }
    
    //rest/logins-by-query-id
    @RequestMapping(value = "logins-by-query-id", method = RequestMethod.GET)
    @Permission("PUBLIC")
    public LoginCountDTO getLoginsForQuery(@RequestParam(value = QUERY_ID_REST_PARAM, required=true) String query
            ) throws Throwable {
       
        return statsService.getLoginsForQuery(query);
    }

    //rest/participants/activity-stats
    @RequestMapping(value = "participants/activity-stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves activity stats",
            notes = "This method allows a user to retrieve activity/engagement stats for programs and "
                    + "users determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of activity stats", 
                    response = ParticipantActivityStatsDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ParticipantActivityStatsDTO getActivityStatsAllParticipantsWithProject(
            @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Throwable {
        return statsService.getActivityStats(queryId);
    }

    //rest/participants/~/activity-stats
    @RequestMapping(value = "participants/{paxId}/activity-stats", method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves activity stats for the specified participant",
            notes = "This method allows a user to retrieve activity/engagement stats for the "
                    + "specified user/participant and programs determined by the specified query ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of activity stats", 
                    response = ParticipantActivityStatsDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ParticipantActivityStatsDTO getActivityStatsWithProject(
            @ApiParam(value = "ID of participant", required = true) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "ID of query", required = false)
                @RequestParam(value = QUERY_ID_REST_PARAM, required = false) String queryId) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        return statsService.getActivityStats(queryId, paxId);
    }
}
