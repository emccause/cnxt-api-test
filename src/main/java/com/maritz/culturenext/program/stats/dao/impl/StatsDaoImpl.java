package com.maritz.culturenext.program.stats.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.jpa.support.util.TransactionSubTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.stats.dao.StatsDao;
import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class StatsDaoImpl extends AbstractDaoImpl implements StatsDao {

    private static String LOGIN_COUNT_KEY = "LOGIN_COUNT";
    private static String RECOGNITIONS_GIVEN_KEY = "RECOGNITIONS_GIVEN";
    private static String RECOGNITIONS_RECEIVED_KEY = "RECOGNITIONS_RECEIVED";
    private static String POINTS_GIVEN_KEY = "POINTS_GIVEN";
    private static String POINTS_RECEIVED_KEY = "POINTS_RECEIVED";
    private static final String PROGRAM_ID_LIST_SQL_PARAM = "PROGRAM_ID_LIST";

    // queries
    private static final String PROGRAM_STATS_QUERY = "EXEC component.UP_GET_PROGRAM_STATS :programIds, :userIds, "
            + ":startDate, :endDate";

    private static final String PROGRAM_RECOGNITION_CRITERIA_STATS_QUERY =
            "EXEC component.UP_GET_PROGRAM_RECOGNITION_CRITERIA_STATS :programIds, :userIds, :startDate, :endDate";

    private static final String LOGIN_QUERY = "EXEC component.UP_GET_ALL_LOGINS :userIds, :startDate, :endDate";

    private static final String LOGIN_COUNT_QUERY =
        "SELECT ISNULL(CONVERT(bigint, COUNT(login_history.LOGIN_HISTORY_ID), 0), 0) as LOGIN_COUNT " +
            "FROM component.LOGIN_HISTORY login_history " +
                "LEFT JOIN component.SYS_USER sys_user " +
                    "ON sys_user.SYS_USER_ID = login_history.SYS_USER_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') USER_LIST " +
                    "ON USER_LIST.items = sys_user.PAX_ID " +
            "WHERE USER_LIST.ITEMS IS NOT NULL " +
                "AND login_history.LOGIN_DATE BETWEEN :startDate AND :endDate";

    private static final String RECOGNITIONS_GIVEN_STAT_QUERY =
        "SELECT CONVERT(bigint, COUNT(DISTINCT nomination.ID), 0) AS RECOGNITIONS_GIVEN " +
            "FROM component.NOMINATION nomination " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + PROGRAM_ID_LIST_SQL_PARAM + ", ',') PROGRAM_LIST " +
                    "ON PROGRAM_LIST.items = nomination.PROGRAM_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') USER_LIST " +
                    "ON USER_LIST.ITEMS = nomination.SUBMITTER_PAX_ID " +
            "WHERE nomination.STATUS_TYPE_CODE = '" + StatusTypeCode.APPROVED + "' " +
                "AND (:" + PROGRAM_ID_LIST_SQL_PARAM + " IS NULL OR PROGRAM_LIST.ITEMS IS NOT NULL)" +
                "AND (:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + " IS NULL OR USER_LIST.ITEMS IS NOT NULL) " +
                "AND nomination.SUBMITTAL_DATE BETWEEN :startDate AND :endDate";

    private static final String RECOGNITIONS_RECEIVED_STAT_QUERY =
        "SELECT CONVERT(bigint, COUNT(DISTINCT nomination.ID), 0) as RECOGNITIONS_RECEIVED " +
            "FROM component.NOMINATION nomination " +
                "JOIN component.RECOGNITION recognition " +
                    "ON nomination.ID = recognition.NOMINATION_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + PROGRAM_ID_LIST_SQL_PARAM + ", ',') PROGRAM_LIST " +
                    "ON PROGRAM_LIST.items = nomination.PROGRAM_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') USER_LIST " +
                    "ON USER_LIST.items = recognition.RECEIVER_PAX_ID " +
            "WHERE nomination.STATUS_TYPE_CODE = '" + StatusTypeCode.APPROVED + "' " +
                "AND recognition.STATUS_TYPE_CODE IN ('" + StatusTypeCode.APPROVED + "','" + StatusTypeCode.AUTO_APPROVED + "') " +
                "AND (:" + PROGRAM_ID_LIST_SQL_PARAM + " IS NULL OR PROGRAM_LIST.ITEMS IS NOT NULL)" +
                "AND (:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + " IS NULL OR USER_LIST.ITEMS IS NOT NULL) " +
                "AND nomination.SUBMITTAL_DATE BETWEEN :startDate AND :endDate";

    private static final String POINTS_GIVEN_STAT_QUERY =
        "SELECT ISNULL(CONVERT(bigint, SUM(ISNULL(payout.PAYOUT_AMOUNT,0))), 0) as POINTS_GIVEN " +
            "FROM component.PAYOUT payout " +
                "LEFT JOIN component.EARNINGS_PAYOUT " +
                    "ON payout.PAYOUT_ID = EARNINGS_PAYOUT.PAYOUT_ID " +
                "LEFT JOIN component.TRANSACTION_HEADER_EARNINGS " +
                    "ON earnings_payout.EARNINGS_ID = TRANSACTION_HEADER_EARNINGS.EARNINGS_ID " +
                "LEFT JOIN component.TRANSACTION_HEADER " +
                    "ON TRANSACTION_HEADER_EARNINGS.TRANSACTION_HEADER_ID = TRANSACTION_HEADER.ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + PROGRAM_ID_LIST_SQL_PARAM + ", ',') PROGRAM_LIST " +
                    "ON TRANSACTION_HEADER.PROGRAM_ID = PROGRAM_LIST.items " +
                "LEFT JOIN component.DISCRETIONARY DISCRETIONARY " +
                    "ON TRANSACTION_HEADER.ID = DISCRETIONARY.TRANSACTION_HEADER_ID " +
                "LEFT JOIN component.RECOGNITION recognition " +
                    "ON DISCRETIONARY.TARGET_ID = recognition.ID AND DISCRETIONARY.TARGET_TABLE = 'RECOGNITION' " +
                "LEFT JOIN component.NOMINATION nomination " +
                    "ON nomination.ID = recognition.NOMINATION_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') USER_LIST " +
                    "ON nomination.SUBMITTER_PAX_ID = USER_LIST.items " +
            "WHERE payout.PAYOUT_DATE BETWEEN :startDate AND :endDate " +
                "AND (:" + PROGRAM_ID_LIST_SQL_PARAM + " IS NULL OR PROGRAM_LIST.ITEMS IS NOT NULL)" +
                "AND (:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + " IS NULL OR USER_LIST.ITEMS IS NOT NULL) " +
                "AND payout.STATUS_TYPE_CODE = '" + StatusTypeCode.ISSUED + "' " +
                "AND transaction_header.SUB_TYPE_CODE <> '" + TransactionSubTypeCode.FUND + "'";

    private static final String POINTS_RECEIVED_STAT_QUERY =
        "SELECT ISNULL(CONVERT(bigint, SUM(ISNULL(payout.PAYOUT_AMOUNT,0))), 0) as POINTS_RECEIVED " +
            "FROM component.PAYOUT " +
                "LEFT JOIN component.EARNINGS_PAYOUT " +
                    "ON payout.PAYOUT_ID = EARNINGS_PAYOUT.PAYOUT_ID " +
                "LEFT JOIN component.TRANSACTION_HEADER_EARNINGS transaction_header_earnings " +
                    "ON EARNINGS_PAYOUT.EARNINGS_ID = TRANSACTION_HEADER_EARNINGS.EARNINGS_ID " +
                "LEFT JOIN component.TRANSACTION_HEADER " +
                    "ON TRANSACTION_HEADER.ID = TRANSACTION_HEADER_EARNINGS.TRANSACTION_HEADER_ID " +
                "LEFT JOIN component.DISCRETIONARY " +
                    "ON transaction_header.ID = DISCRETIONARY.TRANSACTION_HEADER_ID " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + PROGRAM_ID_LIST_SQL_PARAM + ", ',') PROGRAM_LIST " +
                    "ON TRANSACTION_HEADER.PROGRAM_ID = PROGRAM_LIST.items " +
                "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') USER_LIST " +
                    "ON PAYOUT.PAX_ID = USER_LIST.items " +
            "WHERE payout.PAYOUT_DATE BETWEEN :startDate AND :endDate " +
                "AND (:" + PROGRAM_ID_LIST_SQL_PARAM + " IS NULL OR PROGRAM_LIST.ITEMS IS NOT NULL)" +
                "AND (:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + " IS NULL OR USER_LIST.ITEMS IS NOT NULL) " +
                "AND payout.STATUS_TYPE_CODE = '" + StatusTypeCode.ISSUED + "' " +
                "AND transaction_header.SUB_TYPE_CODE <> '" + TransactionSubTypeCode.FUND + "'";

    @Nonnull
    @Override
    public ProgramStatsDTO getProgramStats(@Nonnull List<Long> programIds,
                                           List<Long> userIds,
                                           @Nonnull Date startDate,
                                           @Nonnull Date endDate)
    {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String programIdsString = StringUtils.join(programIds, ProjectConstants.COMMA_DELIM);
        String userIdsString = StringUtils.join(userIds, ProjectConstants.COMMA_DELIM);

        params.addValue("programIds", programIdsString);
        params.addValue("userIds", userIdsString);
        params.addValue("startDate", startDate);
        params.addValue("endDate", endDate);

        return initiateProgramStatsQuery(PROGRAM_STATS_QUERY, params, new ColumnMapRowMapper());
    }

    @Nonnull
    @Override
    public ProgramStatsDTO getProgramStatsAllUsers(@Nonnull List<Long> programIds,
                                                   @Nonnull Date startDate,
                                                   @Nonnull Date endDate) {
        return getProgramStats(programIds, null, startDate, endDate);
    }

    @Nonnull
    @Override
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(List<Long> programIds,
            List<Long> userIds, @Nonnull Date startDate, @Nonnull Date endDate) {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String programIdsString = StringUtils.join(programIds, ProjectConstants.COMMA_DELIM);
        String userIdsString = StringUtils.join(userIds, ProjectConstants.COMMA_DELIM);

        params.addValue("programIds", programIdsString);
        params.addValue("userIds", userIdsString);
        params.addValue("startDate", startDate);
        params.addValue("endDate", endDate);

        return initiateProgramRecognitionCriteriaStatsQuery(PROGRAM_RECOGNITION_CRITERIA_STATS_QUERY, 
                params, new ColumnMapRowMapper());
    }

    @Nonnull
    @Override
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStatsAllUsers(
            @Nonnull List<Long> programIds, @Nonnull Date startDate, @Nonnull Date endDate) {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String programIdsString = StringUtils.join(programIds, ProjectConstants.COMMA_DELIM);

        params.addValue("programIds", programIdsString);
        params.addValue("userIds", null);
        params.addValue("startDate", startDate);
        params.addValue("endDate", endDate);

        return initiateProgramRecognitionCriteriaStatsQuery(PROGRAM_RECOGNITION_CRITERIA_STATS_QUERY, 
                params, new ColumnMapRowMapper());
    }

    @Nonnull
    @Override
    public LoginCountDTO getAllLogins(@Nonnull List<Long> userIds, @Nonnull Date startDate, @Nonnull Date endDate) {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String userIdsString = StringUtils.join(userIds, ProjectConstants.COMMA_DELIM);

        params.addValue("userIds", userIdsString);
        params.addValue("startDate", startDate);
        params.addValue("endDate", endDate);

        return initiateLoginQuery(LOGIN_QUERY, params, new ColumnMapRowMapper());
    }

    @Nonnull
    @Override
    public ParticipantActivityStatsDTO getActivityStats(@Nonnull List<Long> programIds, @Nonnull List<Long> userIds,
            @Nonnull Date startDate, @Nonnull Date endDate) {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String programIdsString = StringUtils.join(programIds, ProjectConstants.COMMA_DELIM);
        String userIdsString = StringUtils.join(userIds, ProjectConstants.COMMA_DELIM);

        params.addValue(PROGRAM_ID_LIST_SQL_PARAM, programIdsString);
        params.addValue(ProgramConstants.PAX_ID_LIST_SQL_PARAM, userIdsString);
        params.addValue("startDate", startDate);
        params.addValue("endDate", endDate);

        return initiateActivityStatsQueries(params, new ColumnMapRowMapper());
    }

    /**
     * Initiates the given query with the given parameters to determine the program stats.
     *
     * @param programStatsQuery query to run
     * @param params parameters
     * @return program stats
     */
    @Nonnull
    private ProgramStatsDTO initiateProgramStatsQuery(@Nonnull String programStatsQuery,
                                                      @Nonnull SqlParameterSource params,
                                                      @Nonnull RowMapper<Map<String, Object>> rowMapper) {

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(programStatsQuery, params, rowMapper);

        // ensure correct number of mappings
        if(nodes == null || nodes.size() != 1) {
            throw new RuntimeException("The query for program stats has failed. "
                    + "Could not retrieve correct program stats!");
        }

        // need to convert the result and return
        Map<String, Object> programStatsMapping = nodes.get(0);
        return new ProgramStatsDTO(programStatsMapping);

    }

    /**
     * Initiates the given query with the given parameters to determine the program recognition criteria stats.
     *
     * @param programRecognitionCriterisStatsQuery query to run
     * @param params parameters
     * @return program recognition criteria stats
     */
    @Nonnull
    private List<ProgramRecognitionCriteriaStatsDTO> 
                initiateProgramRecognitionCriteriaStatsQuery(@Nonnull String programRecognitionCriterisStatsQuery,
                                                             @Nonnull SqlParameterSource params,
                                                             @Nonnull RowMapper<Map<String, Object>> rowMapper){

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(programRecognitionCriterisStatsQuery,
                params, rowMapper);

        // ensure that something was returned (even no results returns empty list)
        if(nodes == null) {
            throw new RuntimeException("The query for program recognition criteria stats has failed. "
                    + "Could not retrieve correct program recognition criteria stats!");
        }

        List<ProgramRecognitionCriteriaStatsDTO> programRecognitionCriteriaStatsDTOs =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();
         // need to convert each returned result into the respective DTO and return
        for(Map<String, Object> statsMapping : nodes) {
            programRecognitionCriteriaStatsDTOs.add(new ProgramRecognitionCriteriaStatsDTO(statsMapping));
        }

        return programRecognitionCriteriaStatsDTOs;

    }

    @Nonnull
    private LoginCountDTO initiateLoginQuery(@Nonnull String loginQuery,
                                             @Nonnull SqlParameterSource params,
                                             @Nonnull RowMapper<Map<String, Object>> rowMapper)
    {
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(loginQuery, params, rowMapper);

         // ensure correct number of mappings
        if(nodes == null || nodes.size() != 1) {
            throw new RuntimeException("The query for login count has failed. Could not retrieve correct stats!");
        }
         // need to convert the result and return
        Map<String, Object> programStatsMapping = nodes.get(0);
        return new LoginCountDTO(programStatsMapping);
    }

    @Nonnull
    private ParticipantActivityStatsDTO initiateActivityStatsQueries(@Nonnull SqlParameterSource params,
                                                                     @Nonnull RowMapper<Map<String, Object>> rowMapper)
    {
        ParticipantActivityStatsDTO participantActivityStatsDTO = new ParticipantActivityStatsDTO();

        // Grab the LOGIN COUNT stat
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(LOGIN_COUNT_QUERY, params, rowMapper);

        // need to convert the result and return
        Map<String, Object> programStatsMapping = nodes.get(0);
        participantActivityStatsDTO.setLoginCount((Long) programStatsMapping.get(LOGIN_COUNT_KEY));

        // Grab the RECOGNITIONS GIVEN stat
        nodes = namedParameterJdbcTemplate.query(RECOGNITIONS_GIVEN_STAT_QUERY, params, rowMapper);

        programStatsMapping = nodes.get(0);
        participantActivityStatsDTO.setRecognitionsGiven((Long) programStatsMapping.get(RECOGNITIONS_GIVEN_KEY));

        // Grab the RECOGNITIONS RECEIVED stat
        nodes = namedParameterJdbcTemplate.query(RECOGNITIONS_RECEIVED_STAT_QUERY, params, rowMapper);

        programStatsMapping = nodes.get(0);
        participantActivityStatsDTO.setRecognitionsReceived((Long) programStatsMapping.get(RECOGNITIONS_RECEIVED_KEY));

        // Grab the POINTS GIVEN stat
        nodes = namedParameterJdbcTemplate.query(POINTS_GIVEN_STAT_QUERY, params, rowMapper);

        programStatsMapping = nodes.get(0);
        participantActivityStatsDTO.setPointsGiven((Long) programStatsMapping.get(POINTS_GIVEN_KEY));

        // Grab the POINTS RECEIVED stat
        nodes = namedParameterJdbcTemplate.query(POINTS_RECEIVED_STAT_QUERY, params, rowMapper);

        programStatsMapping = nodes.get(0);
        participantActivityStatsDTO.setPointsReceived((Long) programStatsMapping.get(POINTS_RECEIVED_KEY));

        return participantActivityStatsDTO;
    }
}
