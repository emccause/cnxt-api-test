package com.maritz.culturenext.program.stats.services;

import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;

import java.util.List;

public interface StatsService {
    
    /**
     * Uses the cached queryID to return recognition stats for the cached date range and audience.
     * Stats include recognitions given, recognitions received, percent of eligible users giving,
     * percent of eligible users receiving, points given, and points received. 
     * Important to note that if the audience is "ALL" then the numbers for recognitions given/received
     * and points given/received will be the same.
     * 
     * @param queryId - The queryID that is saved in cache and contains date and audience filters
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468833/Query+Program+Stats+Example
     */
    ProgramStatsDTO getProgramStats(String queryId) throws Exception;
    
    /**
     * Uses the cached queryID and passed in program ID to return recognition stats for the
     * date range, audience, and program. Stats include recognitions given, recognitions received,
     * percent of eligible users giving, percent of eligible users receiving, points given, and points received.
     * 
     * @param queryId - The queryID that is saved in cache and contains date and audience filters
     * @param programId - The program ID to filter the data on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468833/Query+Program+Stats+Example
     */
    ProgramStatsDTO getProgramStats(String queryId, Long programId) throws Exception;
    
    /**
     * Uses the cached queryID to return stats about the values being used in the application.
     * Each object returned will include the value's ID and display name,
     * as well as how many recognitions were received that match that value (for the given
     * date range and audience)
     * 
     * @param queryId - The queryID that is saved in cache and contains date and audience filters
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468833/Query+Program+Stats+Example
     */
    List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(
            String queryId) throws Exception;
    
    /**
     * Uses the cached queryID and passed in program ID to return stats about the values being
     * used in the program. Each object returned will include the value's ID and display name,
     * as well as how many recognitions were received that match that value (for the given 
     * date range and audience)
     * 
     * @param queryId - The queryID that is saved in cache and contains date and audience filters
     * @param programId - The program ID to filter the data on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468833/Query+Program+Stats+Example
     */
    List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(
            String queryId, Long programId) throws Exception;
    
    /**
     * Uses the cached queryID to return stats for the cached date range and audience.
     * Stats include login count, recognitions given, recognitions received, percent engagement score,
     * points received, and points given.
     * 
     * @param queryId - The queryID that is saved in the cache and contains date and audience filters
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12517398/Participant+Activity+Stats
     */
    ParticipantActivityStatsDTO getActivityStats(String queryId) throws Exception;
    
    /**
     * Uses the cached queryID and passed in paxID to return stats for the cached date range and audience,
     * specific to that user. Stats include login count, recognitions given, recognitions recived,
     * percent engagement score, points received, and points given.
     * 
     * @param queryId - The queryID that is saved in the cache and contains date and audience filters
     * @param paxId - The pax ID to filter on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12517398/Participant+Activity+Stats
     */
    ParticipantActivityStatsDTO getActivityStats(String queryId, Long paxId) throws Exception;
    
    
    LoginCountDTO getLoginsForQuery(String queryId) throws Exception;
}
