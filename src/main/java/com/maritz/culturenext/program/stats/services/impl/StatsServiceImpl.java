package com.maritz.culturenext.program.stats.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.program.stats.dao.StatsDao;
import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;
import com.maritz.culturenext.program.stats.services.StatsService;
import com.maritz.culturenext.recognition.services.EngagementScoreService;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheHandler;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheObject;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class StatsServiceImpl implements StatsService {

    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private EngagementScoreService engagementScoreService;
    @Inject private ReportsQueryCacheHandler reportsQueryCacheHandler;
    @Inject private Security security;
    @Inject private StatsDao statsDao;

    // error codes and messages
    private static final String ERROR_REQUESTOR_NOT_MANAGER = "REQUESTOR_NOT_MANAGER";
    private static final String ERROR_REQUESTOR_NOT_MANAGER_MSG = "Requestor is not the manager of the participant";

    @Nonnull
    @Override
    public ProgramStatsDTO getProgramStats(String queryId) throws Exception {
        // get the query object associated with the queryID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // retrieve the programs associated with the query (if any)
        List<Long> queryProgramIds = reportsQueryCacheHandler.getProgramIdList(queryId);

        // retrieve the pax ids associated with the query (if any)
        List<Long> queryPaxIds = reportsQueryCacheHandler.getFinalPaxIdList(queryId);

        // utilize helper method to retrieve program stats
        return retrieveProgramStats(queryCacheObject, queryProgramIds, queryPaxIds);
    }

    @Override
    public ProgramStatsDTO getProgramStats(String queryId, Long programId) throws Exception {
        // get the query object associated with the queryID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // build the list of programs (only contains given program id)
        List<Long> programIds = new ArrayList<Long>(Arrays.asList(programId));

        // retrieve the pax ids associated with the query (if any)
        List<Long> queryPaxIds = reportsQueryCacheHandler.getFinalPaxIdList(queryId);

        // utilize helper method but pass in given program id
        return retrieveProgramStats(queryCacheObject, programIds, queryPaxIds);
    }

    @Nonnull
    @Override
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(String queryId)
            throws Exception {
        // get the query object associated with the queryID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // retrieve the programs associated with the query (if any)
        List<Long> queryProgramIds = reportsQueryCacheHandler.getProgramIdList(queryId);
        
        // Only get the finalPaxList if allMembers is false
        List<Long> queryPaxIds = (queryCacheObject.getAllMembers()) ? null : queryCacheObject.getFinalPaxIdList();
        
        // utilize helper methods to retrieve stats associated with recognition criteria
        return retrieveRecognitionCriteriaStats(queryCacheObject, queryProgramIds, queryPaxIds);
    }

    @Nonnull
    @Override
    public List<ProgramRecognitionCriteriaStatsDTO> getProgramRecognitionCriteriaStats(String queryId,
            Long programId) throws Exception {
        // get the query object associated with the queryID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // build the list of programs (only contains given program id)
        List<Long> programIds = new ArrayList<Long>(Arrays.asList(programId));

        // Only get the finalPaxList if allMembers is false
        List<Long> queryPaxIds = (queryCacheObject.getAllMembers()) ? null : queryCacheObject.getFinalPaxIdList();

        // utilize helper methods to retrieve stats associated with recognition criteria
        return retrieveRecognitionCriteriaStats(queryCacheObject, programIds, queryPaxIds);
    }

    @Nonnull
    @Override
    public LoginCountDTO getLoginsForQuery(String queryId) throws Exception {
        // get the query object associated with the queryID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // retrieve the pax ids associated with the query (if any)
        List<Long> queryPaxIds = reportsQueryCacheHandler.getFinalPaxIdList(queryId);

        // utilize helper methods to retrieve stats associated with recognition criteria
        return retrieveLoginCount(queryCacheObject, queryPaxIds);
    }

    @Nonnull
    @Override
    public ParticipantActivityStatsDTO getActivityStats(String queryId) throws Exception {
        // get the query object associated with the query ID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // retrieve the programs associated with the query (if any)
        List<Long> programIds = reportsQueryCacheHandler.getProgramIdList(queryId);

        // retrieve the pax ids associated with the query (if any)
        List<Long> paxIds = reportsQueryCacheHandler.getFinalPaxIdList(queryId);

        // utilize helper method to retrieve activity stats
        return retrieveActivityStats(queryCacheObject, programIds, paxIds);
    }

    @Nonnull
    @Override
    public ParticipantActivityStatsDTO getActivityStats(String queryId, Long paxId) throws Exception {
        // before handling query and cache objects, validate to see if 
        // specified pax is under user associated with request
        validateUserRelationship(paxId);

        // get the query object associated with the query ID
        ReportsQueryCacheObject queryCacheObject = reportsQueryCacheHandler.getCacheObject(queryId);

        // retrieve the programs associated with the query (if any)
        List<Long> programIds = reportsQueryCacheHandler.getProgramIdList(queryId);

        //  use the provided paxId
        List<Long> paxIds = new ArrayList<Long>(Arrays.asList(paxId));

        // utilize helper method to retrieve activity stats
        return retrieveActivityStats(queryCacheObject, programIds, paxIds);
    }


    // helper methods

    private ParticipantActivityStatsDTO retrieveActivityStats(@Nonnull ReportsQueryCacheObject queryCacheObject,
                                                              @Nonnull List<Long> programIds,
                                                              @Nonnull List<Long> paxIds)
    {

        // good to go with the query so extract the start and end date from the query
        Date fromDate = queryCacheObject.getStartDate();
        Date thruDate = queryCacheObject.getEndDate();

        // utilize the procedure to get the activity stats (retrieves login count,
        // recognitions given/received, and points issued/received)
        ParticipantActivityStatsDTO participantActivityStatsDTO = 
                statsDao.getActivityStats(programIds, paxIds, fromDate, thruDate);

        // now determine the average engagement score for the given users (utilizing logic from another module)
        if(paxIds.size() == 0) {
            participantActivityStatsDTO.setPercentEngagementScore(0);
        } else {
            participantActivityStatsDTO.setPercentEngagementScore(engagementScoreService.bulkFetchEngagementScoreTotal(paxIds));
        }

        return participantActivityStatsDTO;
    }

    // helper methods

    /**
     * Retrieves the program stats with the given query cache object, program ids, and user ids.
     * After validating the given information, the query for program stats is initiated and returned.
     *
     * @param queryCacheObject query cache object with query criterion
     * @param programIds IDs of programs to get stats for
     * @param paxIds IDs of users to get stats for
     * @return program stats for programs that satisfy the query and specified criterion
     */
    @Nonnull
    private ProgramStatsDTO retrieveProgramStats(@Nonnull ReportsQueryCacheObject queryCacheObject,
                                                 @Nonnull List<Long> programIds,
                                                 @Nonnull List<Long> paxIds) {

        // good to go with the query object so extract the start and end date from the query
        Date fromDate = queryCacheObject.getStartDate();
        Date thruDate = queryCacheObject.getEndDate();

        if(queryCacheObject.getAllMembers()) {
            // query for the program stats from the DAO (by feeding 
            // it the specified programs and date range) for all users
            return statsDao.getProgramStatsAllUsers(programIds, fromDate, thruDate);
        } else {
            // query for the program stats from the DAO (by feeding 
            // it the specified programs, users, and date range)
            return statsDao.getProgramStats(programIds, paxIds, fromDate, thruDate);
        }
    }

    /**
     * Retrieves the stats recognition criterias with the given query cache object, program ids, and user ids.
     * After validating the given information, the query for recognition criteria stats is initiated and returned.
     *
     * @param queryCacheObject query cache object with query criterion
     * @param programIds IDs of programs to get stats for. will pull for all programs if this is null
     * @param paxIds IDs of users to get stats for
     * @return program stats for programs that satisfy the query and specified criterion
     */
    @Nonnull
    private List<ProgramRecognitionCriteriaStatsDTO> 
                retrieveRecognitionCriteriaStats(@Nonnull ReportsQueryCacheObject queryCacheObject,
                                                 List<Long> programIds, List<Long> paxIds) {

        // good to go with the query so extract the start and end date from the query
        Date fromDate = queryCacheObject.getStartDate();
        Date thruDate = queryCacheObject.getEndDate();

        // query for the recognition criteria stats from the
        // DAO (by feeding in specified programs, users, and date range)
        return statsDao.getProgramRecognitionCriteriaStats(programIds, paxIds, fromDate, thruDate);
    }

    @Nonnull
    private LoginCountDTO retrieveLoginCount(@Nonnull ReportsQueryCacheObject queryCacheObject,
                                             @Nonnull List<Long> paxIds) {

        // good to go with the query so extract the start and end date from the query
        Date fromDate = queryCacheObject.getStartDate();
        Date thruDate = queryCacheObject.getEndDate();

        // query for the recognition criteria stats from the
        // DAO (by feeding in specified programs, users, and date range)
        return statsDao.getAllLogins(paxIds, fromDate, thruDate);
    }


    /**
     * Generates a program stats object with zeroed stats.
     *
     * @return a program stats object with zeroed stats
     */
    @Nonnull
    private ProgramStatsDTO generateZeroProgramStats() {
        ProgramStatsDTO programStatsDTO = new ProgramStatsDTO();

        programStatsDTO.setRecognitionsGiven(0L);
        programStatsDTO.setRecognitionsReceived(0L);
        programStatsDTO.setPercentEligibleUsersGiving(new BigDecimal(0.00));
        programStatsDTO.setPercentEligibleUsersReceiving(new BigDecimal(0.00));
        programStatsDTO.setPointsGiven(0L);

        return programStatsDTO;
    }

    /**
     * Validates the manager and participant relationship.
     *
     * @param participantPaxId paxID of the participant relationship
     */
    private void validateUserRelationship(Long participantPaxId) {
        //Skip validation if participant requested is same as logged.
        if(security.isMyPax(participantPaxId)){
            return;
        }
        // check if paxId is given in this case
        if(participantPaxId == null) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_REQUESTOR_NOT_MANAGER)
                    .setField(ProjectConstants.PAX_ID)
                    .setMessage(ERROR_REQUESTOR_NOT_MANAGER_MSG));
        }

        // get the user ID associated with this request (requester)
        Long managerPaxId = security.getPaxId();

        // then verify the relationship of the specified user and the 
        // requester by determining if there is a relationship record
        List<Relationship> relationships = null;

        relationships = relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_1).eq(participantPaxId)
                .and().where(ProjectConstants.PAX_ID_2).eq(managerPaxId).end()
                .and().where(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.toString()).end()
                .find();

        if(relationships == null || relationships.isEmpty()) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_REQUESTOR_NOT_MANAGER)
                    .setField(ProjectConstants.PAX_ID)
                    .setMessage(ERROR_REQUESTOR_NOT_MANAGER_MSG));
        }
    }
}
