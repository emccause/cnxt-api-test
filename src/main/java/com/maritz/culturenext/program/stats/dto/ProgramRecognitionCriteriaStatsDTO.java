package com.maritz.culturenext.program.stats.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing general and recognition stats for recognition criterion associated with "
        + "the program(s). Information includes the ID, the name, and total number of recognitions "
        + "received for the respective recognition criteria.")
public class ProgramRecognitionCriteriaStatsDTO {
    // node column names
    private static final String RECOGNITION_CRITERIA_ID = "RECOGNITION_CRITERIA_ID";
    private static final String RECOGNITION_CRITERIA_DISPLAY_NAME = "RECOGNITION_CRITERIA_DISPLAY_NAME";
    private static final String RECOGNITIONS_RECEIVED = "RECOGNITIONS_RECEIVED";

    private Long recognitionCriteriaId;
    private String displayName;
    private Integer recognitionsReceived;

    /**
     * Default constructor.
     */
    public ProgramRecognitionCriteriaStatsDTO() {}

    /**
     * Constructor from object mapping (query).
     *
     * @param node object mapping from query
     */
    public ProgramRecognitionCriteriaStatsDTO(Map<String, Object> node) {
        if(node == null) {
            return;
        }

        if(node.containsKey(RECOGNITION_CRITERIA_ID)) {
            this.recognitionCriteriaId = (Long) node.get(RECOGNITION_CRITERIA_ID);
        }

        if(node.containsKey(RECOGNITION_CRITERIA_DISPLAY_NAME)) {
            this.displayName = (String) node.get(RECOGNITION_CRITERIA_DISPLAY_NAME);
        }

        if(node.containsKey(RECOGNITIONS_RECEIVED)) {
            this.recognitionsReceived = (Integer) node.get(RECOGNITIONS_RECEIVED);
        }
    }

    @ApiModelProperty(position = 1, value = "ID of the recognition criteria")
    public Long getRecognitionCriteriaId() {
        return recognitionCriteriaId;
    }
    public void setRecognitionCriteriaId(Long recognitionCriteriaId) {
        this.recognitionCriteriaId = recognitionCriteriaId;
    }

    @ApiModelProperty(position = 2, value = "display name of the recognition criteria")
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @ApiModelProperty(position = 3, 
            value = "total number of recognitions received for the respective recognition criteria")
    public Integer getRecognitionsReceived() {
        return recognitionsReceived;
    }
    public void setRecognitionsReceived(Integer recognitionsReceived) {
        this.recognitionsReceived = recognitionsReceived;
    }
}
