package com.maritz.culturenext.program.stats.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.util.ConversionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing the activity stats for the participant(s). Information includes total "
        + "login count, total number of recognitions received and given, points received and given from the "
        + "respective recognitions, and the average engagement score associated with the participant(s).")
public class ParticipantActivityStatsDTO {
    // node column names
    private static final String LOGIN_COUNT = "LOGIN_COUNT";
    private static final String RECOGNTIONS_GIVEN = "RECOGNITIONS_GIVEN";
    private static final String RECOGNTIONS_RECEIVED = "RECOGNITIONS_RECEIVED";
    private static final String POINTS_GIVEN = "POINTS_GIVEN";
    private static final String POINTS_RECEIVED = "POINTS_RECEIVED";
    private static final String PERCENT_ENGAGEMENT_SCORE = "PERCENT_ENGAGEMENT_SCORE";

    private Long loginCount;
    private Long recognitionsGiven;
    private Long recognitionsReceived;
    private Integer percentEngagementScore;
    private Long pointsGiven;
    private Long pointsReceived;

    /**
     * Default constructor.
     */
    public ParticipantActivityStatsDTO() {
    }

    /**
     * Constructor from object mapping (query).
     *
     * @param node object mapping from query
     */
    public ParticipantActivityStatsDTO(Map<String, Object> node) {
        if(node == null) {
            return;
        }

        if(node.containsKey(LOGIN_COUNT)) {
            this.loginCount = (Long) node.get(LOGIN_COUNT);
        }

        if(node.containsKey(RECOGNTIONS_GIVEN)) {
            this.recognitionsGiven = (Long) node.get(RECOGNTIONS_GIVEN);
        }

        if(node.containsKey(RECOGNTIONS_RECEIVED)) {
            this.recognitionsReceived = (Long) node.get(RECOGNTIONS_RECEIVED);
        }

        if(node.containsKey(POINTS_GIVEN)) {
            this.pointsGiven = (Long) node.get(POINTS_GIVEN);
        }

        if(node.containsKey(POINTS_RECEIVED)) {
            this.pointsReceived = (Long) node.get(POINTS_RECEIVED);
        }

        // Percentage returned is between 0 and 1 (fractional -- need to multiply by 100)
        if(node.containsKey(PERCENT_ENGAGEMENT_SCORE)) {
            this.percentEngagementScore = 100 *
                    ConversionUtil.convertIntegerObjectMapValue(node.get(PERCENT_ENGAGEMENT_SCORE));
        }
    }

    @ApiModelProperty(position = 1, required = true, value = "total number of times participant(s) has logged in")
    public Long getLoginCount() {
        return loginCount;
    }
    public void setLoginCount(Long loginCount) {
        this.loginCount = loginCount;
    }

    @ApiModelProperty(position = 2, required = true, 
            value = "total number of recognitions that participant(s) has given")
    public Long getRecognitionsGiven() {
        return recognitionsGiven;
    }
    public void setRecognitionsGiven(Long recognitionsGiven) {
        this.recognitionsGiven = recognitionsGiven;
    }

    @ApiModelProperty(position = 3, required = true, 
            value = "total number of recognitions that participant(s) has received")
    public Long getRecognitionsReceived() {
        return recognitionsReceived;
    }
    public void setRecognitionsReceived(Long recognitionsReceived) {
        this.recognitionsReceived = recognitionsReceived;
    }

    @ApiModelProperty(position = 4, required = true, value = "average engagement score percentage for participant(s)")
    public Integer getPercentEngagementScore() {
        return percentEngagementScore;
    }
    public void setPercentEngagementScore(Integer percentEngagementScore) {
        this.percentEngagementScore = percentEngagementScore;
    }

    @ApiModelProperty(position = 5, required = true, value = "total number of points that participant(s) have given")
    public Long getPointsGiven() {
        return pointsGiven;
    }
    public void setPointsGiven(Long pointsGiven) {
        this.pointsGiven = pointsGiven;
    }

    @ApiModelProperty(position = 6, required = true, value ="total number of points that participant(s) have received")
    public Long getPointsReceived() {
        return pointsReceived;
    }
    public void setPointsReceived(Long pointsReceived) {
        this.pointsReceived = pointsReceived;
    }

}
