package com.maritz.culturenext.program.services;

import java.util.List;

import com.maritz.culturenext.program.dto.ProgramEligibilityDTO;
import com.maritz.culturenext.program.dto.ProgramEligibilityQueryDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;

public interface ProgramEligibilityQueryService {

    /**
     * Uses the cached query object to determine which programs the logged in user can give from.
     * The audience inside the query object must be eligible to receive from the programs.
     * 
     * @param submitterPaxId
     * @param query
     * @param languageCode
     * @return List<ProgramSummaryDTO>
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338947/GET+programs+for+giver+receiver
     */
    List<ProgramSummaryDTO> getEligiblePrograms(Long submitterPaxId, ProgramEligibilityQueryDTO query, 
            String languageCode);
    
    /**
     * Returns a list of all active programs the user can either give or receive from.
     * Will only return programs with programVisibility of PUBLIC.
     * 
     * @param paxId
     * @param languageCode
     * @param commaSeparatedProgramTypeList
     * @param eligibility
     * @return List<ProgramSummaryDTO>
     * 
     * https://maritz.atlassian.net/wiki/display/M365/All+Eligible+Programs+for+Pax+Example
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/10518657/All+Eligible+Programs+for+Pax+Example
     */
    List<ProgramSummaryDTO> getAllEligibleProgramsForPax(Long submitterPaxId, String languageCode, 
            String commaSeparatedProgramTypeList, String eligibility);
    
    /**
     * Return true/false values to indicate what types of program the user can GIVE and RECEIVE from
     * 
     * @param paxId
     * @return ProgramEligibilityDTO
     * 
     * https://maritz.atlassian.net/wiki/display/M365/Pax+Program+Eligibility+by+Program+Type
     */
    ProgramEligibilityDTO getRecognitionEligibility(Long paxId);
}
