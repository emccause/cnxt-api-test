package com.maritz.culturenext.program.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramEligibilityDTO;
import com.maritz.culturenext.program.dto.ProgramEligibilityQueryDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.dto.RecognitionEligibilityDTO;
import com.maritz.culturenext.program.services.ProgramEligibilityQueryService;
import com.maritz.culturenext.program.util.ProgramTranslationUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class ProgramEligibilityQueryServiceImpl implements ProgramEligibilityQueryService {
    
    @Inject private ProgramEligibilityQueryDao programEligibilityResolverDao;
    @Inject private ProgramTranslationUtil programTranslationUtil;

    @Override
    public List<ProgramSummaryDTO> getEligiblePrograms(Long submitterPaxId, ProgramEligibilityQueryDTO query, 
            String languageCode) {
        
        ArrayList<Long> paxIds = new ArrayList<Long>();
        ArrayList<Long> groupIds = new ArrayList<Long>();
        for(GroupMemberStubDTO receiver : query.getReceivers()){
            if(receiver.getPaxId() != null){
                paxIds.add(receiver.getPaxId());
            }
            else if(receiver.getGroupId() != null){
                groupIds.add(receiver.getGroupId());
            } 
        }
        
        List<ProgramSummaryDTO> results = programEligibilityResolverDao.getEligiblePrograms(
                query.getProgramCategory(), submitterPaxId, paxIds, groupIds);

        for (ProgramSummaryDTO result : results) {
            programTranslationUtil.translateProgramFieldsOnProgramSummaryDTO(result, languageCode);
        }

        return results;
    }

    @Override
    public List<ProgramSummaryDTO> getAllEligibleProgramsForPax(Long paxId, String languageCode, 
            String commaSeparatedProgramTypeList, String eligibility) {
        
        List<String> programTypeList = ProgramTypeEnum.getAllProgramTypes();
        if (commaSeparatedProgramTypeList != null && !commaSeparatedProgramTypeList.trim().isEmpty()) {
            programTypeList = Arrays.asList(commaSeparatedProgramTypeList.split(ProjectConstants.COMMA_DELIM));
        }
        
        List<ProgramSummaryDTO> results = programEligibilityResolverDao.getAllEligibleProgramsForPax(paxId, 
                programTypeList, eligibility);

        for (ProgramSummaryDTO result : results) {
            programTranslationUtil.translateProgramFieldsOnProgramSummaryDTO(result, languageCode);
        }

        return results;
    }
    
    @Override
    public ProgramEligibilityDTO getRecognitionEligibility(Long paxId) {        
        ProgramEligibilityDTO programEligibility = new ProgramEligibilityDTO();
        RecognitionEligibilityDTO give = new RecognitionEligibilityDTO();
        RecognitionEligibilityDTO receive = new RecognitionEligibilityDTO();
        
        List<Map<String, Object>> nodes = programEligibilityResolverDao.getRecognitionEligibility(paxId);
        
        if (!CollectionUtils.isEmpty(nodes)) {
            for (Map<String, Object> node : nodes) {
                if (node.get(ProjectConstants.ROLE_CODE).equals(RoleCode.PGIV.name())) {
                    //Giving Eligibility
                    give.setRecognition(Boolean.valueOf((String) node.get(ProjectConstants.RECOGNITION)));
                    give.setAwardCodeCert(Boolean.valueOf((String) node.get(ProjectConstants.PRINTABLE)));
                    give.setAwardCodeDownload(Boolean.valueOf((String) node.get(ProjectConstants.DOWNLOAD)));
                    give.setPointLoad(Boolean.valueOf((String) node.get(ProjectConstants.POINT_LOAD)));
                    
                } else if (node.get(ProjectConstants.ROLE_CODE).equals(RoleCode.PREC.name())) {
                    //Receiving Eligibility
                    receive.setRecognition(Boolean.valueOf((String) node.get(ProjectConstants.RECOGNITION)));
                    receive.setAwardCodeCert(Boolean.valueOf((String) node.get(ProjectConstants.PRINTABLE)));
                    receive.setAwardCodeDownload(Boolean.valueOf((String) node.get(ProjectConstants.DOWNLOAD)));
                    receive.setPointLoad(Boolean.valueOf((String) node.get(ProjectConstants.POINT_LOAD)));
                }
            }
        }
        
        programEligibility.setGive(give);
        programEligibility.setReceive(receive);        
        return programEligibility;
    }

}
