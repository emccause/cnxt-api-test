package com.maritz.culturenext.program.services;

import com.maritz.core.jpa.entity.Program;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import com.maritz.culturenext.program.dto.*;

import javax.annotation.Nonnull;
import java.util.List;

public interface ProgramService {
    
    /**
     * Return program info by ID
     * 
     * @param programID
     * @param languageCode
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/37716009/GET+Program+By+ID
     */
    ProgramFullDTO getProgramByID(Long programID, String languageCode);

    /**
     * Return program budgets by program ID
     *
     * @param programID
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/587595874/GET+Program+Budgets+By+Program+ID
     */
    List<BudgetAssignmentDTO> getProgramBudgetsByProgramID(Long programID);

    /**
     * Return a list of programs. Can be filtered by status and program type
     * 
     * @param status - ACTIVE, SCHEDULED, ENDED, INACTIVE, ARCHIVED
     * @param languageCode 
     * @param programType - ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, PEER_TO_PEER, AWARD_CODE
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8257722/GET+Programs+List
     */
    List<ProgramSummaryDTO> getProgramListByStatus(String status, String languageCode, String programType);
    
    /**
     * Create a program
     * 
     * @param programRequestDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/37716013/POST+Program
     */
    ProgramFullDTO insertProgram(ProgramRequestDTO programRequestDTO);
    
    /**
     * Update an existing program
     * 
     * @param programRequestDTO
     * @param programID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/37716024/PUT+Program
     */
    ProgramFullDTO updateProgram(ProgramRequestDTO programRequestDTO, Long programID);
    
    /**
     * Update an existing program eligibility
     * 
     * @param eligibilityStubDTO
     * @param programID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/37716024/PUT+Program
     */
    EligibilityStubDTO updateProgramEligibility(EligibilityStubDTO eligibilityStubDTO, Long programID);
    
    /**
     * Gets the award tiers associated with the specified Program ID.
     *
     * @param programId Program ID
     * @return the program award tiers associated with the specified Program ID
     */
    List<AwardTierDTO> getProgramAwardTiers(@Nonnull Long programId);

    /**
     * Deletes a milestone associated with the specified Program and Milestone IDs.
     *
     * @param programId Program ID
     * @param milestoneId Milestone ID
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338943/Program
     */
    void deleteMilestoneById(Long programId, Long milestoneId);
    
    /**
     * TODO javadocs
     * 
     * @param programDTO
     * @param programFilesTypes
     */
    void populateAwardCodeDetails(ProgramSummaryDTO programDTO, ProgramFilesDistinctTypesDTO programFilesTypes);
}
