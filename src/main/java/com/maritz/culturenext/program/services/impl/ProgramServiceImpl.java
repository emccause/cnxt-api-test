package com.maritz.culturenext.program.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.maritz.culturenext.jpa.entity.ProgramAwardTierReminder;
import com.maritz.culturenext.jpa.repository.*;
import com.maritz.culturenext.nomination.dao.NominationDao;
import com.maritz.culturenext.notifications.constants.NotificationConstants;
import com.maritz.culturenext.notifications.services.impl.NotificationsServiceImpl;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMethod;
import org.thymeleaf.util.ListUtils;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Ecard;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramAwardTierType;
import com.maritz.core.jpa.entity.ProgramBudget;
import com.maritz.core.jpa.entity.ProgramCriteria;
import com.maritz.core.jpa.entity.ProgramEcard;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.ProgramType;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.entity.Translatable;
import com.maritz.core.jpa.entity.TranslatablePhrase;
import com.maritz.core.jpa.repository.AclRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.ProgramTypeRepository;
import com.maritz.core.jpa.support.util.AclTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dto.ApprovalDTO;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentBudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.ApprovalType;
import com.maritz.culturenext.enums.AwardTierType;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.LookupCodeEnum;
import com.maritz.culturenext.enums.LookupTypeEnum;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.ProgramCategoryEnum;
import com.maritz.culturenext.enums.ProgramEligibilityEnum;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.enums.Visibility;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.EligibilityDao;
import com.maritz.culturenext.program.dao.ProgramBudgetsDao;
import com.maritz.culturenext.program.dao.ProgramDao;
import com.maritz.culturenext.program.dto.AwardCodeProgramDetailsDTO;
import com.maritz.culturenext.program.dto.Certificates;
import com.maritz.culturenext.program.dto.EligibilityDTO;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.culturenext.program.dto.ProgramBasicDTO;
import com.maritz.culturenext.program.dto.ProgramFilesDistinctTypesDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.event.ProgramCreateUpdateEvent;
import com.maritz.culturenext.program.milestone.service.MilestoneReminderService;
import com.maritz.culturenext.program.milestone.service.MilestoneService;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import com.maritz.culturenext.program.pinnacle.service.PinnacleService;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.program.util.ProgramTranslationUtil;
import com.maritz.culturenext.program.validators.PinnacleProgramValidator;
import com.maritz.culturenext.program.validators.ProgramValidatorContext;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.AclUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.EntityUtil;
import com.maritz.culturenext.util.StatusTypeUtil;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.profile.services.impl.DetailProfileServiceImpl;

@Component
public class ProgramServiceImpl implements ProgramService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private AclRepository aclRepository;
    @Inject private AclUtil aclUtil;
    @Inject private ApplicationEventPublisher applicationEventPublisher;
    @Inject private BudgetInfoDao budgetInfoDao;
    @Inject private CnxtDiscretionaryRepository cnxtDiscretionaryRepository;
    @Inject private CnxtProgramTypeRepository cnxtProgramTypeRepository;
    @Inject private ConcentrixDao<Acl> aclDao;
    @Inject private ConcentrixDao<ApprovalProcessConfig> approvalProcessConfigDao;
    @Inject private ConcentrixDao<Budget> budgetDao;
    @Inject private ConcentrixDao<Ecard> ecardDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<FileItem> fileItemDao;
    @Inject private ConcentrixDao<Lookup> lookupDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<ProgramAwardTierReminder> programAwardTierReminderDao;
    @Inject private CnxtProgramAwardTierRepository programAwardTierRepository;
    @Inject private ConcentrixDao<ProgramAwardTierType> programAwardTierTypeDao;
    @Inject private ConcentrixDao<ProgramBudget> programBudgetDao;
    @Inject private ConcentrixDao<ProgramCriteria> programCriteriaDao;
    @Inject private ConcentrixDao<ProgramEcard> programEcardDao;
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;
    @Inject private MilestoneReminderService milestoneReminderService;
    @Inject private ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Inject private ConcentrixDao<Role> roleDao;
    @Inject private EligibilityDao eligibilityDao;
    @Inject private FileImageService fileImageService;
    @Inject private MilestoneService milestoneService;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private PaxRepository paxRepository;
    @Inject private PinnacleService pinnacleService;
    @Inject private ProgramDao programDao;
    @Inject private ProgramRepository programRepository;
    @Inject private CnxtTranslatablePhraseRepository translatablePhraseRepository;    
    @Inject private CnxtTranslatableRepository translatableRepository;
    @Inject private ProgramTranslationUtil programTranslationUtil;
    @Inject private ProgramTypeRepository programTypeRepository;
    @Inject private ProgramValidatorContext programValidatorContext;
    @Inject private Security security;
    @Inject private PinnacleProgramValidator pinnacleProgramValidator;
    @Inject private ProgramBudgetsDao programBudgetsDao;
    @Inject private NominationDao nominationDaoInterface;
    @Inject private DetailProfileServiceImpl  detailProfileInfo;
    @Inject private NotificationsServiceImpl notificationsServiceImpl;

    @Override
    public ProgramFullDTO getProgramByID(Long programID, String languageCode) {

        //Get the program
        ProgramFullDTO programFullDTO = new ProgramFullDTO();
        Program program = programRepository.findOne(programID);
        if (program != null) {
            BeanUtils.copyProperties(program, programFullDTO);
            populateProgramFullDTOFromEntity(program, programFullDTO);
            //Pull the information of program name, short description and long description
            //based on the language Locale of the user
            if (languageCode==null) {
            	try{            	
            		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
            	}catch(NullPointerException e){languageCode = TranslationConstants.EN_US;}
            }
            try {
            	notificationsServiceImpl.populatePayoutTypeInformation(languageCode);
            }catch(NullPointerException e) {
            	logger.info("Exception when pulling lookup data", e);	
            }
            try {
	            String payoutDisplayNameTranslation = notificationsServiceImpl.getTranslations().get(
	            		notificationsServiceImpl.getLookupCategoryCodeMap().get(programFullDTO.getPayoutType()));
	            if (payoutDisplayNameTranslation != null) {
	            	programFullDTO.setPayoutDisplayName(payoutDisplayNameTranslation);
	            }
            }catch(NullPointerException e) {
            	logger.info("Exception trying to get translations", e);	
            }
            translateProgramSummaryDTOFields(programFullDTO, languageCode);
            translateProgramMiscFields(programFullDTO, languageCode);
            if (program.getProgramTypeCode().equalsIgnoreCase(PinnacleConstants.PINNACLE)) {
                programFullDTO.setPinnacle(pinnacleService.getPinnacle(programID));
            }
        } else {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                .setCode(ProgramConstants.ERROR_PROGRAM_NOT_FOUND)
                .setField(ProjectConstants.PROGRAM_ID)
                .setMessage(ProgramConstants.ERROR_PROGRAM_NOT_FOUND_MSG));
        }

        return programFullDTO;
    }

    @Override
    public List<BudgetAssignmentDTO> getProgramBudgetsByProgramID(Long programID) {

        // get the program
        List<BudgetAssignmentDTO> budgetAssignmentDTOList;
        Program program = programRepository.findOne(programID);
        if (program != null) {
            budgetAssignmentDTOList = getBudgetAssignments(program.getProgramId());
        }
        else {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(ProgramConstants.ERROR_PROGRAM_NOT_FOUND)
                        .setField(ProjectConstants.PROGRAM_ID)
                        .setMessage(ProgramConstants.ERROR_PROGRAM_NOT_FOUND_MSG));
        }

        return budgetAssignmentDTOList;
    }

    @Override
    public List<ProgramSummaryDTO> getProgramListByStatus(String status, String languageCode, String programType) {
        List<String> bulkStatusList = new ArrayList<>();

        boolean haveEndedStatus = false;
        boolean haveScheduledStatus = false;
        boolean haveActiveStatus = false;
        boolean receivedStatusRequest = false;

        if (StringUtils.isBlank(status)) {
            bulkStatusList.add(StatusTypeCode.ACTIVE.name());
            bulkStatusList.add(StatusTypeCode.SCHEDULED.toString());
            bulkStatusList.add(StatusTypeCode.INACTIVE.name());
            bulkStatusList.add(StatusTypeCode.ARCHIVED.toString());
            bulkStatusList.add(StatusTypeCode.SUSPENDED.name());
        }
        else {
            receivedStatusRequest = true;
            StringTokenizer stringTokenizer = new StringTokenizer(status, ProjectConstants.COMMA_DELIM);
            while (stringTokenizer.hasMoreTokens()) {
                String tokenStatus = stringTokenizer.nextToken();
                if (tokenStatus.equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                    haveActiveStatus = true;
                } else if (tokenStatus.equalsIgnoreCase(StatusTypeCode.SCHEDULED.toString())) {
                    haveScheduledStatus = true;
                } else if (tokenStatus.equalsIgnoreCase(StatusTypeCode.ENDED.toString())) {
                    haveEndedStatus = true;
                } else if (tokenStatus.equalsIgnoreCase(StatusTypeCode.ARCHIVED.toString())) {
                    bulkStatusList.add(StatusTypeCode.ARCHIVED.toString());
                } else if (tokenStatus.equalsIgnoreCase(StatusTypeCode.SUSPENDED.name())) {
                    bulkStatusList.add(StatusTypeCode.SUSPENDED.name());
                }
            }
        }
        List<ProgramSummaryDTO> programList = programDao.getProgramByStatus(receivedStatusRequest, bulkStatusList,
                haveActiveStatus, haveScheduledStatus, haveEndedStatus, programType);

        for (ProgramSummaryDTO node : programList) {
            translateProgramSummaryDTOFields(node, languageCode);
        }

        return programList;
    }

    /**
     * This will fetch and replace the translations for the translatable fields on ProgramSummaryDTO
     * Note: Works for ProgramFullDTO as well, due to subclasses
     * @param programSummaryDTO
     * @param languageCode
     * @return
     */
    private ProgramSummaryDTO translateProgramSummaryDTOFields(ProgramSummaryDTO programSummaryDTO,
            String languageCode) {
        programTranslationUtil.translateProgramFieldsOnProgramSummaryDTO(programSummaryDTO, languageCode);
        return programSummaryDTO;
    }

    /**
     * This will fetch and replace the translations for the translatable fields on ProgramMisc table
     * Note: Works for ProgramFullDTO as well, due to subclasses
     * @param programBasicDTO
     * @param languageCode
     * @return
     */
    private ProgramBasicDTO translateProgramMiscFields(ProgramBasicDTO programBasicDTO, String languageCode) {
        programTranslationUtil.translateProgramMiscFields(programBasicDTO, languageCode);
        return programBasicDTO;
    }

    /**
     * This will copy over and translate any data (status) which doesn't come over from the
     * BeanUtils.copyProperties method call
     * @param program
     * @param programFullDTO
     * @return
     */
    private ProgramFullDTO populateProgramFullDTOFromEntity(Program program, ProgramFullDTO programFullDTO) {

        if (program.getProgramTypeCode() != null && program.getProgramTypeCode().length() > 0) {
            ProgramType programType = programTypeRepository.findByProgramTypeCode(program.getProgramTypeCode());
            programFullDTO.setProgramType(programType.getProgramTypeName());
        }
        if (program.getProgramCategoryCode() != null && program.getProgramCategoryCode().length() > 0) {
            programFullDTO.setProgramCategory(program.getProgramCategoryCode());
        }

        TimeZone tz = TimeZone.getTimeZone("UTC");

        if (program.getFromDate() != null) {
            programFullDTO.setFromDate(DateUtil.convertToDateOnlyFormat(program.getFromDate(), tz));
        }

        if (program.getThruDate() != null) {
            programFullDTO.setThruDate(DateUtil.convertToDateOnlyFormat(program.getThruDate(), tz));
        }

        if (program.getCreatorPaxId() != null)  {
            programFullDTO.setCreatorPax(participantInfoDao.getEmployeeDTO(program.getCreatorPaxId()));
        }
        programFullDTO.setProgramId(program.getProgramId());
        programFullDTO.setProgramName(program.getProgramName());
        programFullDTO.setProgramDescriptionShort(program.getProgramDesc());
        programFullDTO.setProgramDescriptionLong(program.getProgramLongDesc());
        programFullDTO.setProgramCategory(program.getProgramCategoryCode());
        programFullDTO.setStatus(StatusTypeUtil.getImpliedStatus(
                program.getStatusTypeCode(), program.getFromDate(), program.getThruDate()));

        programFullDTO.setEcardIds(setProgramEcards(programFullDTO.getProgramId()));
        programFullDTO.setRecognitionCriteriaIds(setProgramCriteria(programFullDTO.getProgramId()));

        programDao.setLikeInfo(security.getPaxId(), programFullDTO);

        //set the PROGRAM_misc values
        Map<String, String> programDtoProperies = programDao.getProgramDtoData(program.getProgramId());

        programFullDTO.setNewsfeedVisibility(programDtoProperies.get(VfName.NEWSFEED_VISIBILITY.toString()));
        String notificationRecipient = programDtoProperies.get(VfName.NOTIFICATION_RECIPIENT.toString());
        String notificationRecipientMgr = programDtoProperies.get(VfName.NOTIFICATION_RECIPIENT_MGR.toString());
        String notificationRecipient2ndMgr = programDtoProperies.get(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString());
        String notificationSubmitter = programDtoProperies.get(VfName.NOTIFICATION_SUBMITTER.toString());


        List <String> notificationList = new ArrayList<>();
        if (notificationRecipient != null && Boolean.parseBoolean(notificationRecipient)) {
            notificationList.add(VfName.NOTIFICATION_RECIPIENT.toString());
        }
        if (notificationRecipientMgr != null && Boolean.parseBoolean(notificationRecipientMgr)) {
            notificationList.add(VfName.NOTIFICATION_RECIPIENT_MGR.toString());
        }
        if (notificationRecipient2ndMgr != null && Boolean.parseBoolean(notificationRecipient2ndMgr)) {
            notificationList.add(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString());
        }
        if (notificationSubmitter != null && Boolean.parseBoolean(notificationSubmitter)) {
            notificationList.add(VfName.NOTIFICATION_SUBMITTER.toString());
        }

        programFullDTO.setNotification(notificationList);

        if (ProgramTypeEnum.getRecognitionCategoryTypes().contains(programFullDTO.getProgramType())) {

            programFullDTO.setAllowTemplateUpload(
                    Boolean.parseBoolean(programDtoProperies.get(VfName.ALLOW_TEMPLATE_UPLOAD.toString())));
        }

        String payoutType = programDtoProperies.get(VfName.SELECTED_AWARD_TYPE.name());

        programFullDTO.setPayoutDescription(getPayoutDescription(payoutType));
        programFullDTO.setPayoutDisplayName(getPayoutDisplayName(payoutType));
        programFullDTO.setPayoutType(payoutType);

        String pppIndexEnabled =  programDtoProperies.get(VfName.PPP_INDEX_ENABLED.name());
        programFullDTO.setPppIndexEnabled(Boolean.parseBoolean(pppIndexEnabled));

        programFullDTO.setProgramVisibility(programDao.getProgramVisibility(programFullDTO.getProgramId()));
        programFullDTO.setEligibility(getProgramEligibility(programFullDTO.getProgramId()));

        programFullDTO.setAwardTiers(getProgramAwardTiers(programFullDTO.getProgramId()));
        
        programFullDTO.setProgramUsed(isProgramUsed(programFullDTO.getProgramId()));

        programFullDTO.setMilestones(milestoneService.getMilestones(programFullDTO.getProgramId()));
        programFullDTO.setMilestoneReminders(milestoneReminderService.getMilestoneReminders(programFullDTO.getProgramId()));

        ProgramFilesDistinctTypesDTO programFilesTypes = getCurrentProgramFilesTypes(programFullDTO.getProgramId());
        if (programFilesTypes != null && programFilesTypes.getProgramFileImage() != null) {
            programFullDTO.setImageId(programFilesTypes.getProgramFileImage().getFilesId());
        }

        if (ProgramTypeEnum.AWARD_CODE.getCode().equals(program.getProgramTypeCode())) {
            populateAwardCodeDetails(programFullDTO, programFilesTypes);
        }

        return programFullDTO;
    }

    @Override
    public void populateAwardCodeDetails(ProgramSummaryDTO programDTO, ProgramFilesDistinctTypesDTO programFilesTypes) {

        if (programDTO != null) {

            if (programFilesTypes == null) {
                programFilesTypes = getCurrentProgramFilesTypes(programDTO.getProgramId());
            }

            AwardCodeProgramDetailsDTO awardCodeDetails = new AwardCodeProgramDetailsDTO();
            awardCodeDetails.setClaimingInstructions(
                    getProgramMisc(programDTO, VfName.CLAIM_INSTRUCTIONS.name()));
            awardCodeDetails.setDownloadable(
                    Boolean.valueOf(getProgramMisc(programDTO, VfName.IS_DOWNLOADABLE.name())));
            awardCodeDetails.setPrintable(
                    Boolean.valueOf(getProgramMisc(programDTO, VfName.IS_PRINTABLE.name())));

            if (programFilesTypes != null && programFilesTypes.getProgramFileIcon() != null) {
                awardCodeDetails.setIconId(programFilesTypes.getProgramFileIcon().getFilesId());
            }

            if(programFilesTypes  != null && programFilesTypes.getProgramFileCertificates() != null){
                List<Long> certIds = (List<Long>) CollectionUtils.<FileItem, Long>collect(programFilesTypes.getProgramFileCertificates(),
                        TransformerUtils.invokerTransformer(ProjectConstants.PROGRAM_FILES_ID_GETTER));
                List<Certificates> certificatesList = new ArrayList<>();
                if (!CollectionUtils.isEmpty(certIds)){
                for(Long cert: certIds){
                    Files file = filesDao.findById(cert);
                    if(file !=null){
                        Certificates certificate = new Certificates();
                        certificate.setId(file.getId());
                        certificate.setFileName(file.getName());
                        certificatesList.add(certificate);
                    }

                }
                awardCodeDetails.setCertificates(certificatesList);
                }
            }
            programDTO.setAwardCode(awardCodeDetails);
        }
    }

    private List<Long> setProgramCriteria(Long programId ) {
        //Set recognitionCriteriaIds
        List<Long> recCriteriaIdList = new ArrayList<>();
        List<ProgramCriteria> programCriteriaList =
                programCriteriaDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .find();
        if (programCriteriaList != null) {
            for (ProgramCriteria programCriteria : programCriteriaList) {
                recCriteriaIdList.add(programCriteria.getRecognitionCriteriaId());
            }
        }
        return recCriteriaIdList;
    }

    private List<Long> setProgramEcards(Long programId) {
        //Set eCard Ids
        List<Long> eCardIdList = new ArrayList<>();
        List<ProgramEcard> programEcardList =
                programEcardDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .find();
        if (programEcardList != null) {
            for (ProgramEcard programEcard : programEcardList) {
                eCardIdList.add(programEcard.getEcardId());
            }
        }
        return eCardIdList;
    }

    @Override
    @Transactional
    public ProgramFullDTO insertProgram(ProgramRequestDTO programRequestDTO) {
        return populateProgramEntityFromDTO(programRequestDTO, RequestMethod.POST.toString(), null);
    }

    /**
     * Handles validation of a ProgramFullDTO
     * Saves are done in active status only, must pass all business rules for required fields.
     * Current required fields for non-DRAFT status are... Program type,Program name,Short description,Long description
     * @param programRequestDTO
     * @param requestType
     * @return list of errors
     */
    private List<ErrorMessage> validateProgram(ProgramRequestDTO programRequestDTO, String requestType,
            Long programID) {
        List<ErrorMessage> errors = new ArrayList<>();

        if (!requestType.equals(RequestMethod.POST.toString()) && programRequestDTO.getProgramId() != programID) {
            errors.add(ProgramConstants.MISMATCHED_PROGRAM_IDS_MESSAGE);
        }

        if (programRequestDTO.getProgramName() == null || programRequestDTO.getProgramName().length() == 0) {
            errors.add(ProgramConstants.PROGRAM_NAME_NULL_NAME);
        }
        else if (programRequestDTO.getProgramName().length() > ProgramConstants.PROGRAM_NAME_MAX_LENGTH) {
            errors.add(ProgramConstants.PROGRAM_NAME_TOO_LONG_MESSAGE);
        }

        //check for duplicate name
        List<Long> programIds =  programRepository.findBy()
            .where(ProjectConstants.PROGRAM_NAME).eq(programRequestDTO.getProgramName())
            .findAll(ProjectConstants.PROGRAM_ID, Long.class);

        if (requestType.equals(RequestMethod.PUT.toString())) {
            if (!programIds.isEmpty() && !programIds.get(0).equals(programID)) {
                errors.add(ProgramConstants.DUPLICATE_PROGRAM_NAME_MESSAGE);
            }
        }
        else if (requestType.equals(RequestMethod.POST.toString())) {
            if (!programIds.isEmpty()) {
                errors.add(ProgramConstants.DUPLICATE_PROGRAM_NAME_MESSAGE);
            }
        }

        if (RequestMethod.PUT.toString().equals(requestType) && programRequestDTO.getFromDate() != null) {
            // retrieve the previous fromDate
            Date fromDate = DateUtil.convertFromString(programRequestDTO.getFromDate());
            Program previousProgram = programRepository.findOne(programID);

            Date previousFromDate = previousProgram != null ? previousFromDate =
                    DateUtil.convertFromString(DateUtil.convertToUTCDate(previousProgram.getFromDate())) : null;

            // validate if program is already being used (can only update from
            //		date if program is not used and provided fromDate is different than before)
            if((fromDate != null ? !fromDate.equals(previousFromDate) : previousFromDate != null)
                    && isProgramUsed(programID)) {
                errors.add(ProgramConstants.PROGRAM_START_DATE_ALREADY_IN_USE_MESSAGE);

            }
        }

        boolean haveValidFromDate = false;
        boolean haveValidThruDate = false;
        if (programRequestDTO.getFromDate() == null ||
                programRequestDTO.getFromDate().equals(ProjectConstants.EMPTY_STRING)) {
            errors.add(ProgramConstants.FROM_DATE_NULL_MESSAGE);
        } else {
            if (DateUtil.convertFromString(programRequestDTO.getFromDate()) == null) {
                errors.add(ProgramConstants.FROM_DATE_BAD_FORMAT_MESSAGE);
            } else if (requestType.equals(RequestMethod.POST.toString())) {
                if (DateUtil.verifyDateBeforeDays(DateUtil.convertFromString(programRequestDTO.getFromDate())
                    ,ProgramConstants.UTC_OFFSET)) {
                    errors.add(ProgramConstants.FROM_DATE_NOT_FUTURE_MESSAGE);
                } else {
                    haveValidFromDate = true;
                }
            } else {
                haveValidFromDate = true;
            }
        }

        if (programRequestDTO.getThruDate() != null
                && !programRequestDTO.getThruDate().equals(ProjectConstants.EMPTY_STRING)
                && DateUtil.convertFromString(programRequestDTO.getThruDate()) == null) {
            errors.add(ProgramConstants.THRU_DATE_BAD_FORMAT_MESSAGE);
        } else if (programRequestDTO.getThruDate() != null
                && !programRequestDTO.getThruDate().equals(ProjectConstants.EMPTY_STRING)
                && requestType.equals(RequestMethod.POST.toString())
                && DateUtil.convertToEndOfDayString(programRequestDTO.getThruDate()).before(new Date())){
            errors.add(ProgramConstants.THRU_DATE_NOT_FUTURE_MESSAGE);
            haveValidThruDate = true;
        } else if (programRequestDTO.getThruDate() != null
                && !programRequestDTO.getThruDate().equals(ProjectConstants.EMPTY_STRING)){
            haveValidThruDate = true;
        }

        if (haveValidFromDate && haveValidThruDate) {
            if (DateUtil.convertFromString(programRequestDTO.getFromDate())
                    .after(DateUtil.convertFromString(programRequestDTO.getThruDate()))) {
                errors.add(ProgramConstants.FROM_DATE_AFTER_THRU_DATE_MESSAGE);
            }
        }

        if (programRequestDTO.getProgramDescriptionShort() == null
                || programRequestDTO.getProgramDescriptionShort().equals(ProjectConstants.EMPTY_STRING)) {
            if ((requestType.equals(RequestMethod.PUT.toString())
                    || requestType.equals(RequestMethod.GET.toString())) && programRequestDTO.getStatus() != null) {
                errors.add(ProgramConstants.SHORT_DESCRIPTION_NULL_MESSAGE);
            }
        } else {
            if (programRequestDTO.getProgramDescriptionShort().length() >
                    ProgramConstants.SHORT_PROGRAM_DESCRIPTION_MAX_LENGTH) {
                errors.add(ProgramConstants.SHORT_PROGRAM_DESCRIPTION_TOO_LONG_MESSAGE);
            }
        }

        if (programRequestDTO.getProgramDescriptionLong() == null
                || programRequestDTO.getProgramDescriptionLong().equals(ProjectConstants.EMPTY_STRING)) {
            if ((requestType.equals(RequestMethod.PUT.toString())
                    || requestType.equals(RequestMethod.GET.toString())) && programRequestDTO.getStatus() != null) {
                errors.add(ProgramConstants.LONG_DESCRIPTION_NULL_MESSAGE);
            }
        } else {
            if (programRequestDTO.getProgramDescriptionLong().length() >
                    ProgramConstants.LONG_PROGRAM_DESCRIPTION_MAX_LENGTH) {
                errors.add(ProgramConstants.LONG_PROGRAM_DESCRIPTION_TOO_LONG_MESSAGE);
            }
        }

        // status validation
        String status = programRequestDTO.getStatus();
        errors.addAll(validateStatus(requestType, status));

        if (programRequestDTO.getRecognitionCriteriaIds() != null &&
                !requestType.equals(RequestMethod.GET.toString())) {
            //Query to validate that the ids exist in the database before attempting update/insert
            List <RecognitionCriteria> recognitionCriteriaList =
                    recognitionCriteriaDao.findBy()
                    .where(ProjectConstants.ID).in(programRequestDTO.getRecognitionCriteriaIds())
                    .find();

            if (recognitionCriteriaList == null || (recognitionCriteriaList.size() !=
                    programRequestDTO.getRecognitionCriteriaIds().size())) {
                errors.add(ProgramConstants.INVALID_RECOGNTITION_CRITERIA_ID_MESSAGE);
            }
        }

        if (programRequestDTO.getEcardIds() != null && !requestType.equals(RequestMethod.GET.toString())) {
            //Query to validate that the ids exist in the database and it is active before attempting update/insert
            List <Ecard> eCardList = ecardDao.findBy()
                    .where(ProjectConstants.ID).in(programRequestDTO.getEcardIds())
                    .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                    .find();

            if (eCardList == null || (eCardList.size() != programRequestDTO.getEcardIds().size())) {
                errors.add(ProgramConstants.INVALID_ECARD_ID_MESSAGE);
            }
        }

        //program miscs validation
        errors.addAll(validateProgramMiscs(programRequestDTO));

        //validate AwardTier
        if (!programRequestDTO.getProgramType().equals(ProgramTypeEnum.PINNACLE.name())) {
            errors.addAll(validateAwardTier(programRequestDTO));
        }

        if (programRequestDTO.getProgramVisibility() == null
                || programRequestDTO.getProgramVisibility().equals(ProjectConstants.EMPTY_STRING)) {
            errors.add(ProgramConstants.PROGRAM_VISIBILITY_NULL_MESSAGE);
        } else {
            if (programRequestDTO.getProgramVisibility().equalsIgnoreCase(Visibility.ADMIN.toString())
                    || programRequestDTO.getProgramVisibility().equalsIgnoreCase(Visibility.PUBLIC.toString())) {
                //pass
            } else {
                errors.add(ProgramConstants.PROGRAM_VISIBILITY_INVALID_MESSAGE);
            }
        }

        // validate programType
        if (programRequestDTO.getProgramType() == null
                || programRequestDTO.getProgramType().equals(ProjectConstants.EMPTY_STRING)) {
            errors.add(ProgramConstants.PROGRAM_TYPE_NULL_MESSAGE);
        }
        else {
            ProgramType programType = cnxtProgramTypeRepository.findByProgramTypeName(programRequestDTO.getProgramType());
            if (programType == null) {
                errors.add(ProgramConstants.PROGRAM_TYPE_INVALID_MESSAGE);
            }
        }

        return errors;
    }

    private List <ErrorMessage> validateProgramMiscs(ProgramRequestDTO programRequestDTO) {
        List <ErrorMessage> errors = new ArrayList<>();

        //notification validation begin
        List <String> tmpNotificationValidationList = new ArrayList<>();
        if(programRequestDTO.getNotification() == null){
            errors.add(ProgramConstants.NOTIFICATION_NULL_MESSAGE);
        }
        else{
            tmpNotificationValidationList = new ArrayList<>(programRequestDTO.getNotification());
        }
        //Creating this list from the notification list so we can validate against it without
        //changing the original list.  Will remove all valid objects from the List, if anything
        //is left we have an invalid object in the list and will throw an error

        if (tmpNotificationValidationList != null
                && tmpNotificationValidationList.contains(VfName.NOTIFICATION_RECIPIENT.toString())) {
            tmpNotificationValidationList.remove(VfName.NOTIFICATION_RECIPIENT.toString());
        }

        if (tmpNotificationValidationList != null
                && tmpNotificationValidationList.contains(VfName.NOTIFICATION_RECIPIENT_MGR.toString())) {
            tmpNotificationValidationList.remove(VfName.NOTIFICATION_RECIPIENT_MGR.toString());
        }

        if (tmpNotificationValidationList != null
                && tmpNotificationValidationList.contains(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString())) {
            tmpNotificationValidationList.remove(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString());
        }

        if (tmpNotificationValidationList != null
                && tmpNotificationValidationList.contains(VfName.NOTIFICATION_SUBMITTER.toString())) {
            tmpNotificationValidationList.remove(VfName.NOTIFICATION_SUBMITTER.toString());
        }

        if (tmpNotificationValidationList != null
                && tmpNotificationValidationList.size() > 0) {
            //If there are still values in the list, we have some invalid data.  Add an error
            errors.add(ProgramConstants.NOTIFICATION_INVALID_MESSAGE);
        }
        //notification validation end

        //newsfeedVisibility validation begin

        if (programRequestDTO.getNewsfeedVisibility() == null
                || programRequestDTO.getNewsfeedVisibility().equals(ProjectConstants.EMPTY_STRING) ) {
            errors.add(ProgramConstants.NEWSFEED_NULL_MESSAGE);

        } else {
            if (programRequestDTO.getNewsfeedVisibility().equals(NewsfeedVisibility.NONE.toString())
                    || programRequestDTO.getNewsfeedVisibility().equals(NewsfeedVisibility.RECIPIENT_PREF.toString())
                    || programRequestDTO.getNewsfeedVisibility().equals(NewsfeedVisibility.PRIVATE.toString())
                    || programRequestDTO.getNewsfeedVisibility().equals(NewsfeedVisibility.PUBLIC.toString())) {
                //These are the expected values, so just continue
            } else {
                errors.add(ProgramConstants.NEWSFEED_INVALID_MESSAGE);
            }
        }
        //newsfeedVisibility validation end

        if(programRequestDTO.getMilestoneReminders() != null && programRequestDTO.getMilestoneReminders().contains(null)) {
            errors.add(ProgramConstants.MILESTONE_REMINDERS_NULL_MESSAGE);
        }

        //payoutType validation against Lookup table
        if (!StringUtils.isEmpty(programRequestDTO.getPayoutType())) {
            if (!isValidPayout(programRequestDTO.getPayoutType())) {
                errors.add(ProgramConstants.PAYOUT_TYPE_INVALID_MESSAGE);
            }
        }

        /*
         * Validation to ensure that a payoutType is selected when a budget is selected
         *  1. Validate that payoutType and budgetAssignment are not null
         *  2. Go through the list of buddgetAssignments
         *  3. Check if there is a budget assigned
         *  4. If there is a budgetId attached to the assignment and
         *		the list of payoutTypes is empty, add an error
         */
        if (!CollectionUtils.isEmpty(programRequestDTO.getBudgetAssignments())
                && StringUtils.isEmpty(programRequestDTO.getPayoutType())) {
            errors.add(ProgramConstants.PAYOUT_TYPE_MISSING_MESSAGE);
        }
        //payoutType validation end

        return errors;
    }

    /**
     * Validates the status specified within the given request.
     *
     * @param requestType type of request (HTTP verb)
     * @param status status specified within the request
     * @return validation errors associated with the specified status
     */
    private List<ErrorMessage> validateStatus(@Nonnull String requestType, String status) {
        List<ErrorMessage> errors = new ArrayList<>();
        boolean isValidStatus = true;

        // determine if status is valid depending on the specified request type (HTTP verb)
        if (RequestMethod.PUT.toString().equals(requestType)){
            // determine if status is an actual status or an implied status
            isValidStatus = StatusTypeUtil.isValidActualOrImpliedStatus(status);
        }
        else if (RequestMethod.POST.toString().equals(requestType)) {
            // determine if status is an actual status (implied statuses are not allowed on create)
            isValidStatus = StatusTypeUtil.isValidStatus(status);
        }
        else {
            logger.warn("Specified request type was not an anticipated HTTP verb!");
        }

        // error message check
        if (!isValidStatus) {
            errors.add(new ErrorMessage()
                    .setCode(ProgramConstants.ERROR_INVALID_STATUS)
                    .setField("status")
                    .setMessage(status + ProgramConstants.ERROR_INVALID_STATUS_MSG));
        }

        return errors;
    }


    /**
     * Adds the specified eligibility entities corresponding to the specified Program.
     * @param entities eligibility entities to add
     * @param programID ID of Program
     */
    private void addEligibilityEntityStubEntries(List<EligibilityEntityStubDTO> entities,
                                                 @Nonnull Long programID,
                                                 @Nonnull String eligibilityRole) {
        // if there are no entities to add, then we're done
        if(entities == null || entities.isEmpty()) {
            return;
        }

        // iterate through each of the entities and handle addition accordingly
        for(EligibilityEntityStubDTO eligibilityEntityStubDTO : entities) {
            if(eligibilityEntityStubDTO == null ||
                    (eligibilityEntityStubDTO.getPaxId() == null && eligibilityEntityStubDTO.getGroupId() == null)) {
                throw new ErrorMessageException(new ErrorMessage()
                            .setCode(ProgramConstants.ERROR_MISSING_ELIGIBILITY_ID)
                            .setField(ProjectConstants.ELIGIBILITY)
                            .setMessage(ProgramConstants.ERROR_MISSING_ELIGIBILITY_ID_MSG));
            }

            // determine what type of entity it is and handle accordingly
            if(eligibilityEntityStubDTO.getGroupId() != null) { // Groups ID
                aclUtil.addAcl(eligibilityRole, programID, TableName.PROGRAM.toString(),
                        TableName.GROUPS.toString(), eligibilityEntityStubDTO.getGroupId());
            } else { // PAX ID
                aclUtil.addAcl(eligibilityRole, programID, TableName.PROGRAM.toString(),
                        TableName.PAX.toString(), eligibilityEntityStubDTO.getPaxId());
            }
        }
    }

    @Override
    @Transactional
    public ProgramFullDTO updateProgram( ProgramRequestDTO programRequestDTO, Long programID) {
        return populateProgramEntityFromDTO(programRequestDTO, RequestMethod.PUT.toString(), programID);
    }
    
    @Override
    @Transactional
    public EligibilityStubDTO updateProgramEligibility( EligibilityStubDTO eligibilityStubDTO, Long programID) {
        return populateProgramEligibilityFromDTO(eligibilityStubDTO, RequestMethod.PUT.toString(), programID);
    }
    
    /**
     * This will populate a Program entity based on the ProgramFullDTO that is passed in.
     * It is used by update/insert to perform saves
     * @param programRequestDTO
     * @param responseType
     * @param programID
     * @return
     */
    private ProgramFullDTO populateProgramEntityFromDTO(ProgramRequestDTO programRequestDTO,
            String responseType, Long programID) {
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        Program programEntity = new Program();
        List <Translatable> translatableList = new ArrayList<Translatable>();
        List <TranslatablePhrase> translatablePhrase = new ArrayList<TranslatablePhrase>();

        // check if program id is provided within update
        if(RequestMethod.PUT.toString().equals(responseType) && programID == null) {
            errors.add(ProgramConstants.MISSING_PROGRAM_ID_MESSAGE);
        }
        //If any errors, This will catch/throw them prior to any other work

        ErrorMessageException.throwIfHasErrors(errors);

        if (programRequestDTO.getProgramType().equals(ProgramTypeEnum.MILESTONE.name())) {
            List<Long> milestoneECardIds = new ArrayList<>();

            for(MilestoneDTO milestone : programRequestDTO.getMilestones()) {
                if (milestone.getEcardId() != null && !milestoneECardIds.contains(milestone.getEcardId())) {
                    milestoneECardIds.add(milestone.getEcardId());
                }
            }
            programRequestDTO.setEcardIds(milestoneECardIds);
        }

        //Validate the DTO object data and create any errors we find
        errors.addAll(validateProgram(programRequestDTO, responseType, programID));

        //If any errors, This will catch/throw them prior to any other work
        ErrorMessageException.throwIfHasErrors(errors);

        // Bonus validation
        errors.addAll(programValidatorContext.validateProgram(programRequestDTO, responseType, programID));

        //If any errors, This will catch/throw them prior to any other work
        ErrorMessageException.throwIfHasErrors(errors);

        if (ProgramTypeEnum.MILESTONE.name().equalsIgnoreCase(programRequestDTO.getProgramType())) {
            errors.addAll(milestoneService.validateMilestones(programRequestDTO));

            // need to tie admin to the program as giver
            Long adminPaxId = paxRepository.findPaxIdByControlNum("admin");
            if (programRequestDTO.getEligibility() != null) {
                programRequestDTO.getEligibility().setGive(
                        Arrays.asList(new EligibilityEntityStubDTO().setPaxId(adminPaxId))
                    );
            }

            if (!ListUtils.isEmpty(programRequestDTO.getBudgetAssignments())) {
                programRequestDTO.getBudgetAssignments().get(0)
                    .setGivingMembers(Arrays.asList(new BudgetEntityStubDTO().setPaxId(adminPaxId)));
            }
        }

        //If any errors, This will catch/throw them prior to any other work
        ErrorMessageException.throwIfHasErrors(errors);

        //Set up entity to be saved
        programEntity = setProgramEntityForSave(programRequestDTO, responseType, programID);

        programRepository.save(programEntity);
        
        String languageCode = null;
        try{            	
    		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
    	}catch(NullPointerException e){languageCode = TranslationConstants.EN_US;}    
        //If is not a new Program, or is not the default languageCode "en_US"
        //Get the list of id's of the records from TRANSLATABLE table where TABLE_NAME column values equals to 'PROGRAM'
        if (languageCode!=null && !languageCode.equals(TranslationConstants.EN_US)){
	        translatableList = getTranslatableEntity(TranslationConstants.TABLE_NAME_PROGRAM);
	        //Set up entity to be saved when language is different than US English 
	        translatablePhrase = setTranslatablePhraseForSave(programRequestDTO, responseType, programID,translatableList);
	        translatablePhraseRepository.save(translatablePhrase);
        }
                
        /*
         * Now that we've saved the ProgramEntity we need to repopulate
         * the programRequestDTO with all the correct values to for the response
         */
        programRequestDTO.setProgramId(programEntity.getProgramId());

        //Set up ProgramMisc entity and save.
        //Have to do this after saving Program so that we know we have a ProgramID (for POST)
        saveProgramMisc(programRequestDTO, responseType,
                VfName.NEWSFEED_VISIBILITY.toString(), programRequestDTO.getNewsfeedVisibility());

        // If notification is missing, we're throwing an error.
        //Thus, we should always do this logic since it works for both empty and populated array
        boolean hasNotificationRecipient = false;
        boolean hasNotificationRecipientMgr = false;
        boolean hasNotificationRecipient2ndMgr = false;
        boolean hasNotificationSubmitter = false;
        for (String notificationType : programRequestDTO.getNotification()) {
            if (notificationType.equalsIgnoreCase(VfName.NOTIFICATION_RECIPIENT.toString())) {
                hasNotificationRecipient = true;
            } else if (notificationType.equalsIgnoreCase(VfName.NOTIFICATION_RECIPIENT_MGR.toString())) {
                hasNotificationRecipientMgr = true;
            } else if (notificationType.equalsIgnoreCase(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString())) {
                hasNotificationRecipient2ndMgr = true;
            } else if (notificationType.equalsIgnoreCase(VfName.NOTIFICATION_SUBMITTER.toString())) {
                hasNotificationSubmitter = true;
            }
        }

        saveProgramMisc(programRequestDTO, responseType, VfName.NOTIFICATION_RECIPIENT.toString(),
                Boolean.toString(hasNotificationRecipient).toUpperCase());

        saveProgramMisc(programRequestDTO, responseType, VfName.NOTIFICATION_RECIPIENT_MGR.toString(),
                Boolean.toString(hasNotificationRecipientMgr).toUpperCase());

        saveProgramMisc(programRequestDTO, responseType, VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString(),
                Boolean.toString(hasNotificationRecipient2ndMgr).toUpperCase());

        saveProgramMisc(programRequestDTO, responseType, VfName.NOTIFICATION_SUBMITTER.toString(),
                Boolean.toString(hasNotificationSubmitter).toUpperCase());

        if (programRequestDTO.getAllowTemplateUpload() != null) {
            saveProgramMisc(programRequestDTO, responseType, VfName.ALLOW_TEMPLATE_UPLOAD.toString(),
                    Boolean.toString(programRequestDTO.getAllowTemplateUpload()).toUpperCase());
        }

        if (!StringUtils.isEmpty(programRequestDTO.getPayoutType())) {
            saveProgramMisc(programRequestDTO, responseType, VfName.SELECTED_AWARD_TYPE.toString(),
                    programRequestDTO.getPayoutType());
        }

        saveProgramMisc(programRequestDTO, responseType, VfName.PPP_INDEX_ENABLED.toString(),
                Boolean.toString(programRequestDTO.isPppIndexEnabled()).toUpperCase());
        
        List<AwardTierDTO> awardTiersList = programRequestDTO.getAwardTiers();
        
        if (awardTiersList != null) {
	        for (AwardTierDTO awardTier : awardTiersList ){
	        	//If getRouteNextLevelManager flag is and status of of AwardTier is Active 
        		//then we update the record in ProgramMisc table
	        	if (awardTier.getRouteNextLevelManager()!=null && awardTier.getRouteNextLevelManager()==true
	        			&& awardTier.getStatus().equals(StatusTypeCode.ACTIVE.name())) {
	        		
	        		saveProgramMisc(programRequestDTO, responseType, VfName.ROUTE_NEXT_LEVEL_MANAGER.toString(),	        	
	        		String.valueOf(awardTier.getAwardTierId()));
	        		
	        	}else{	        		
	        		//If getRouteNextLevelManager flag is not true then we remove the record from the ProgramMisc table,
	        		//we just want in the ProgramMisc table those records which flag is true
	        		if (awardTier.getRouteNextLevelManager()!=null) {
	        			try {
		        			deleteProgramMisc(programRequestDTO.getProgramId(), responseType, VfName.ROUTE_NEXT_LEVEL_MANAGER.toString(),	        	
		        					String.valueOf(awardTier.getAwardTierId()));
	        			}catch(Exception e) {
	        				logger.error(e.toString());
	        			}
	        		}
	        	}
	        }
        }

        if (!ListUtils.isEmpty(programRequestDTO.getMilestoneReminders())) {
            milestoneReminderService.insertUpdateMilestoneReminders(programRequestDTO.getProgramId(), programRequestDTO.getMilestoneReminders());
        }

        if (ProgramTypeEnum.AWARD_CODE.name().equals(programRequestDTO.getProgramType())) {
            saveAwardCodeProgramMisc(programRequestDTO, responseType);
        }

        saveVisibilityAcl(programRequestDTO, responseType);

        saveEcardIds(programRequestDTO, responseType);
        saveProgramCriteria(programRequestDTO, responseType);

        Long programId = programRequestDTO.getProgramId();

        //Save eligibility
        EligibilityStubDTO eligibilityStubDTO = programRequestDTO.getEligibility();
        saveProgramEligibility(eligibilityStubDTO, programId, responseType);

        //save budget assignments
        saveBudgetAssignments(programRequestDTO.getBudgetAssignments(), programId, responseType);

        // save program award tiers
        saveProgramAwardTier(programRequestDTO.getAwardTiers(), programId, responseType);

        if (responseType.equals(RequestMethod.POST.toString())) {
            createProgramFilesEntries(programRequestDTO);
        }
        else if (responseType.equals(RequestMethod.PUT.toString())) {
            updateProgramFilesEntries(programRequestDTO);
        }
        milestoneService.insertUpdateMilestones(programRequestDTO);

        if (programRequestDTO.getProgramType().equalsIgnoreCase(PinnacleConstants.PINNACLE)) {
            pinnacleService.insertUpdatePinnacle(programRequestDTO.getProgramId(), programRequestDTO, responseType);
        }

        // create translation data
        programTranslationUtil.setTranslationProgramFields(programEntity.getProgramId());

        // Fire off an event indicating we're done
        applicationEventPublisher.publishEvent(new ProgramCreateUpdateEvent(programEntity));

        return getProgramByID(programEntity.getProgramId(), null);
    }
    
    /**
     * This will populate a Program entity based on the ProgramFullDTO that is passed in.
     * It is used by update/insert to perform saves
     * @param eligibilityStubDTO
     * @param responseType
     * @param programID
     * @return
     */
    private EligibilityStubDTO populateProgramEligibilityFromDTO(EligibilityStubDTO eligibilityStubDTO,
    		String responseType, Long programID) {
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        
        // check if program id is provided within update
        if(RequestMethod.PUT.toString().equals(responseType) && programID == null) {
            errors.add(ProgramConstants.MISSING_PROGRAM_ID_MESSAGE);
        }
        
        //If any errors, This will catch/throw them prior to any other work
        ErrorMessageException.throwIfHasErrors(errors);

        //Save eligibility
        saveProgramEligibility(eligibilityStubDTO, programID, responseType);
        
        return eligibilityStubDTO; //getProgramByID(programID, null);
    }
    
    /**
     * Save Program misc data related to Award Code program.
     * @param programRequestDTO
     * @param requestType
     */
    private void saveAwardCodeProgramMisc(ProgramBasicDTO programRequestDTO,
            String requestType) {
        if (programRequestDTO.getAwardCode() != null) {
            if (programRequestDTO.getAwardCode().getClaimingInstructions() != null) {
                saveProgramMisc(programRequestDTO, requestType, VfName.CLAIM_INSTRUCTIONS.name(),
                        programRequestDTO.getAwardCode().getClaimingInstructions());
            }
            saveProgramMisc(programRequestDTO, requestType, VfName.IS_DOWNLOADABLE.name(),
                    Boolean.toString(programRequestDTO.getAwardCode().isDownloadable()).toUpperCase());

            saveProgramMisc(programRequestDTO, requestType, VfName.IS_PRINTABLE.name(),
                    Boolean.toString(programRequestDTO.getAwardCode().isPrintable()).toUpperCase());
        }

    }

    /**
     * Set up Program entity.  Will save in populateProgramEntityFromDTO() method after doing validation
     * @param programBasicDTO
     * @param responseType
     * @param programID
     * @return
     */
    private Program setProgramEntityForSave(ProgramBasicDTO programBasicDTO, String responseType, Long programID) {
        Program programEntity = null;

        if (responseType.equals(RequestMethod.PUT.toString())) {
            programEntity = programRepository.findOne(programID);
        }

        if (programEntity == null) {
            programEntity = new Program();
        }

        String languageCode = null;
        try{            	
    		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
    	}catch(NullPointerException e){languageCode = TranslationConstants.EN_US;}      
        //Updates on Program table about name, short description and long description will be available
        //just for English language, all other languages will update on TranslatablePhrase table. 
        if (languageCode != null && languageCode.equals(TranslationConstants.EN_US)
        		||!responseType.equals(RequestMethod.PUT.toString())) {//If is not PUT (Update)then we are creating new program with POST
        	//this means we need to populate the program name and their descriptions.
        	if (programBasicDTO.getProgramName()!=null) {
                programEntity.setProgramName(programBasicDTO.getProgramName().trim());
            }
            programEntity.setProgramDesc(programBasicDTO.getProgramDescriptionShort());
            programEntity.setProgramLongDesc(programBasicDTO.getProgramDescriptionLong());        
        }        
        
        if (programBasicDTO.getFromDate() == null
                || programBasicDTO.getFromDate().equals(ProjectConstants.EMPTY_STRING)) {
            programEntity.setFromDate(null);
        }
        else if (DateUtil.convertFromString(programBasicDTO.getFromDate()) != null) {
            Date fromDate = DateUtil.convertFromString(programBasicDTO.getFromDate());
            programEntity.setFromDate(fromDate);
        }

        if (programBasicDTO.getThruDate() == null
                || programBasicDTO.getThruDate().equals(ProjectConstants.EMPTY_STRING)) {
            programEntity.setThruDate(null);
        }
        else if (DateUtil.convertFromString(programBasicDTO.getThruDate()) != null) {
            Date thruDate = DateUtil.convertToEndOfDayString(programBasicDTO.getThruDate());
            programEntity.setThruDate(thruDate);
        }

        if (responseType.equals(RequestMethod.POST.toString())) {
            programEntity.setCreatorPaxId(security.getPaxId());
        }

        // determine status (with handling for implied statuses)
        String status = StatusTypeUtil.determineRequestStatus(programBasicDTO.getStatus());
        programEntity.setStatusTypeCode(status);

        if (programBasicDTO.getProgramType() != null && programBasicDTO.getProgramType().length() != 0) {

            ProgramType programType = cnxtProgramTypeRepository.findByProgramTypeName(programBasicDTO.getProgramType());
            if (programType != null) {
                programEntity.setProgramTypeCode(programType.getProgramTypeCode());
                String programCategory = null;
                if(ProgramTypeEnum.getPointLoadCategoryTypes().contains(programType.getProgramTypeName())){
                    programCategory = ProgramCategoryEnum.POINT_LOAD.toString();
                }
                else if (ProgramTypeEnum.getRecognitionCategoryTypes().contains(programType.getProgramTypeName())){
                    programCategory = ProgramCategoryEnum.RECOGNITION.toString();
                }
                programEntity.setProgramCategoryCode(programCategory);
            }

        }
        return programEntity;
    }
    
    /**
     * Get Translatable entity.
     * @param table_name
     * @return
     */
    private List<Translatable> getTranslatableEntity(String table_name) {
        
    	List<Translatable> translatableEntityList = null; 
       	translatableEntityList = translatableRepository.findTranslatableList(table_name);
        return translatableEntityList;
    }
    
    /**
     * Set up TranslatablePhrase entity. Will save in populateProgramEntityFromDTO() method after populate object translatablePhraseEntityListUpdated
     * @param programBasicDTO
     * @param responseType
     * @param programID
     * @param translatableList     
     * @return
     */
    private List<TranslatablePhrase> setTranslatablePhraseForSave(ProgramBasicDTO programBasicDTO, String responseType, Long programID, List<Translatable> translatableList){
        
    	List<TranslatablePhrase> translatablePhraseEntityList = null;    	
    	List<TranslatablePhrase> translatablePhraseEntityListUpdated = new ArrayList<TranslatablePhrase>();
    	ArrayList<String> translatableIdList = new ArrayList<String>();
    	Map<Long,String> translatableMap = new HashMap<Long,String>();
    
    	String languageCode = null;
    	try{            	
    		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
    	}catch(NullPointerException e){languageCode = TranslationConstants.EN_US;} 
    	for (Translatable translatable :translatableList) {
    		translatableIdList.add(String.valueOf(translatable.getId()));
    		translatableMap.put(translatable.getId(),translatable.getColumnName());
    	}
        if (responseType.equals(RequestMethod.PUT.toString())) {
        	translatablePhraseEntityList = translatablePhraseRepository.findTranslatablePhraseObject(programID,translatableIdList,languageCode);
        
        	for (TranslatablePhrase translatablePhrase : translatablePhraseEntityList) {
        		
        		String column_name = translatableMap.get(translatablePhrase.getTranslatableId());
        		switch(column_name) 
                { 
                    case TranslationConstants.COLUMN_NAME_PROGRAM_NAME: 
                    	translatablePhrase.setTranslation(programBasicDTO.getProgramName());
                    	translatablePhraseEntityListUpdated.add(translatablePhrase);
                        break; 
                    case TranslationConstants.COLUMN_NAME_PROGRAM_DESC: 
                    	translatablePhrase.setTranslation(programBasicDTO.getProgramDescriptionShort());
                    	translatablePhraseEntityListUpdated.add(translatablePhrase);
                        break; 
                    case TranslationConstants.COLUMN_NAME_PROGRAM_LONG_DESC: 
                    	translatablePhrase.setTranslation(programBasicDTO.getProgramDescriptionLong());
                    	translatablePhraseEntityListUpdated.add(translatablePhrase);
                        break;
                } 
        	}
        }        
        return translatablePhraseEntityListUpdated;
    }

    /**
     * Set up ProgramMisc entity and save
     * @param programBasicDTO
     * @param requestType
     */
    private void saveProgramMisc(ProgramBasicDTO programBasicDTO, String requestType,
            String vfName, String miscData) {
        ProgramMisc programMiscEntity = new ProgramMisc();

        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)&& vfName.equals(VfName.ROUTE_NEXT_LEVEL_MANAGER.toString())) {
            programMiscEntity = programMiscDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
                    .and(ProjectConstants.VF_NAME).eq(vfName)
                    .and(ProjectConstants.MISC_DATA).eq(miscData)
                    .findOne();
            if (programMiscEntity == null) {
                programMiscEntity = new ProgramMisc();
            }
        }else {
        
	        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
	            programMiscEntity = programMiscDao.findBy()
	                    .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
	                    .and(ProjectConstants.VF_NAME).eq(vfName)
	                    .findOne();
	            if (programMiscEntity == null) {
	                programMiscEntity = new ProgramMisc();
	            }
	        }
        }

        programMiscEntity.setProgramId(programBasicDTO.getProgramId());
        programMiscEntity.setVfName(vfName);
        programMiscEntity.setMiscData(miscData);
        programMiscEntity.setMiscDate(DateUtil.convertFromString(DateUtil.convertToUTCDate(new Date())));

        programMiscDao.save(programMiscEntity);
    }
    
    /**
     * Delete ProgramMisc entity
     * @param programBasicDTO
     * @param requestType
     */
    private void deleteProgramMisc(Long programId, String requestType,
            String vfName, String miscData) {
        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)&& vfName.equals(VfName.ROUTE_NEXT_LEVEL_MANAGER.toString())) {
            programMiscDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.VF_NAME).eq(vfName)
                    .and(ProjectConstants.MISC_DATA).eq(miscData)
                    .delete();           
        }
    }
    
    /**
     * This will save the appropriate ACLs
     * @param programBasicDTO
     */
    private void saveVisibilityAcl(ProgramBasicDTO programBasicDTO, String requestType) {
        Role role;

        if (RequestMethod.PUT.toString().equals(requestType)) {
            //On update, if new values differ from db then delete and re-insert
            String currentProgramVisibility = programDao.getProgramVisibility(programBasicDTO.getProgramId());

            if (currentProgramVisibility != null) {
                if (!currentProgramVisibility.equals(programBasicDTO.getProgramVisibility())) {

                    // Program visibility has changed, remove existing configuration and add a new one.
                    aclUtil.deleteAcl(ProgramConstants.PROGRAM_VISIBILITY_ROLE_NAME,
                            programBasicDTO.getProgramId(), TableName.PROGRAM.toString(), null, null);

                    if (programBasicDTO.getProgramVisibility() != null
                            && programBasicDTO.getProgramVisibility().equalsIgnoreCase(Visibility.ADMIN.toString())) {
                        //ADMIN applies to both maritz and client admins
                        role = roleDao.findBy()
                                .where(ProjectConstants.ROLE_CODE).eq(ProjectConstants.ADMIN_ROLE_CODE)
                                .findOne();
                        if (role != null) {
                            aclUtil.addAcl(ProgramConstants.PROGRAM_VISIBILITY_ROLE_NAME, programBasicDTO.getProgramId(),
                                    TableName.PROGRAM.toString(),TableName.ROLE.toString(), role.getRoleId());
                        }

                        role = roleDao.findBy()
                                .where(ProjectConstants.ROLE_CODE).eq(ProjectConstants.CLIENT_ADMIN_ROLE)
                                .findOne();
                        if  (role != null) {
                            aclUtil.addAcl(ProgramConstants.PROGRAM_VISIBILITY_ROLE_NAME, programBasicDTO.getProgramId(),
                                    TableName.PROGRAM.toString(),TableName.ROLE.toString(), role.getRoleId());
                        }
                    } else {
                        role = roleDao.findBy()
                                .where(ProjectConstants.ROLE_CODE).eq(ProjectConstants.PARTICIPANT_ROLE)
                                .findOne();
                        if (role != null) {
                            aclUtil.addAcl(ProgramConstants.PROGRAM_VISIBILITY_ROLE_NAME, programBasicDTO.getProgramId(),
                                    TableName.PROGRAM.toString(), TableName.ROLE.toString(), role.getRoleId());
                        }
                    }
                }
            }
        }
    }

    /**
     * Gets Payout Long Description from Lookup table
     * @param payoutType
     * @return
     */

    private String getPayoutDescription(String payoutType) {

        String payoutDescription = ProjectConstants.EMPTY_STRING;

        Lookup lookupEntity = lookupDao.findBy()
                .where(ProjectConstants.LOOKUP_TYPE_CODE).eq(LookupTypeEnum.AWARD_TYPE.name())
                .and(ProjectConstants.LOOKUP_CATEGORY_CODE).eq(payoutType)
                .and(ProjectConstants.LOOKUP_CODE).eq(LookupCodeEnum.LONG_DESC.name())
                .findOne();

        if (lookupEntity != null) {
            payoutDescription = lookupEntity.getLookupDesc();
        }

        return payoutDescription;
    }

    /**
     * Gets Payout Display Name from Lookup table
     * @param payoutType
     * @return
     */
    private String getPayoutDisplayName(String payoutType) {

        String payoutDisplayName = ProjectConstants.EMPTY_STRING;

        Lookup lookupEntity = lookupDao.findBy()
                .where(ProjectConstants.LOOKUP_TYPE_CODE).eq(LookupTypeEnum.AWARD_TYPE.name())
                .and(ProjectConstants.LOOKUP_CATEGORY_CODE).eq(payoutType)
                .and(ProjectConstants.LOOKUP_CODE).eq(LookupCodeEnum.DISPLAY_NAME.name())
                .findOne();

        if (lookupEntity != null) {
            payoutDisplayName = lookupEntity.getLookupDesc();
        }

        return payoutDisplayName;
    }

    /**
     * Check if is a valid payout
     * @param payout
     * @return
     */
    private boolean isValidPayout(String payout)  {

        boolean isValid = false;

        Lookup lookupEntity = lookupDao.findBy()
                .where(ProjectConstants.LOOKUP_TYPE_CODE).eq(LookupTypeEnum.AWARD_TYPE.name())
                .and(ProjectConstants.LOOKUP_CODE).eq(LookupCodeEnum.PAYOUT_TYPE.name())
                .and(ProjectConstants.LOOKUP_DESC).eq(payout)
                .findOne();

        if (lookupEntity != null) {
            isValid = true;
        }

        return isValid;
    }

    /**
     * Gets a program misc record by program ID and vfName
     * @param programBasicDTO
     * @param vfName
     * @return
     */
    private String getProgramMisc(ProgramSummaryDTO programBasicDTO, String vfName)  {
        ProgramMisc programMiscEntity = new ProgramMisc();
        String miscData = ProjectConstants.EMPTY_STRING;

        programMiscEntity = programMiscDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
                .and(ProjectConstants.VF_NAME).eq(vfName)
                .findOne();
        if (programMiscEntity != null)
            miscData = programMiscEntity.getMiscData();

        return miscData;
    }

    /**
     * Gets a program List misc records by program ID and vfName
     * @param programBasicDTO
     * @param vfName
     * @return
     */
    private List<ProgramMisc> getProgramMiscRouteNextLevelManagersList(Long programId, String vfName)  {
        List<ProgramMisc> programMiscEntity = new ArrayList<ProgramMisc>();
        
        programMiscEntity = programMiscDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .and(ProjectConstants.VF_NAME).eq(vfName)
                .find();        
        return programMiscEntity;
    }
    /**
     * Set up ProgramCriteria entity and save
     * @param programBasicDTO
     * @param responseType
     */
    private void saveProgramCriteria(ProgramBasicDTO programBasicDTO, String responseType) {
        ProgramCriteria programCriteria = new ProgramCriteria();
        int displaySequence = 0;
        boolean listsAreSame = false;
        if (responseType.equals(RequestMethod.PUT.toString())) {
            //Search for IDs, if the existing IDs are any different, delete/insert
            List<ProgramCriteria> programCriteriaList = programCriteriaDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
                .find();

            List<Long> recCriteriaIdList = new ArrayList<>();
            for (ProgramCriteria programCriteriaInDb : programCriteriaList) {
                recCriteriaIdList.add(programCriteriaInDb.getRecognitionCriteriaId());
            }

            if (programBasicDTO.getRecognitionCriteriaIds() == null
                    || EntityUtil.areListsSame(recCriteriaIdList, programBasicDTO.getRecognitionCriteriaIds())) {
                listsAreSame = true;
            }
            else {
                for (ProgramCriteria programCriteriaToDelete : programCriteriaList) {
                    //Delete these records
                    programCriteriaDao.delete(programCriteriaToDelete);
                }
            }
        }

        if (programBasicDTO.getRecognitionCriteriaIds() != null
                && programBasicDTO.getRecognitionCriteriaIds().size() > 0
                && !listsAreSame) {
            for (Long programCriteriaId : programBasicDTO.getRecognitionCriteriaIds()) {

                programCriteria.setProgramId(programBasicDTO.getProgramId());
                programCriteria.setRecognitionCriteriaId(programCriteriaId);
                programCriteria.setDisplaySequence(displaySequence);
                displaySequence++;
                programCriteriaDao.create(programCriteria);
            }
        }
    }

    /**
     * Saves the given eligibility entities (specified by the stub DTOs) associated with the specified Program.
     *
     * @param eligibilityStubDTO eligibility entities
     * @param programId program associated with eligibility entities
     * @param responseType type of save (insert, update)
     */
    @Transactional
    private void saveProgramEligibility(EligibilityStubDTO eligibilityStubDTO,
                                               @Nonnull Long programId,
                                               @Nonnull String responseType) {
        // extract eligibility entities
        List<EligibilityEntityStubDTO> currentGiverEligibility = (eligibilityStubDTO != null) ?
                eligibilityStubDTO.getGive() :
                new ArrayList<>();

        List<EligibilityEntityStubDTO> currentReceiverEligibility = (eligibilityStubDTO != null) ?
                eligibilityStubDTO.getReceive() :
                new ArrayList<>();

        // filter duplicate entities
        currentGiverEligibility = EntityUtil.filterDuplicateEligibilityEntities(currentGiverEligibility);
        currentReceiverEligibility = EntityUtil.filterDuplicateEligibilityEntities(currentReceiverEligibility);

        // handle for update (vs insert)
        if (RequestMethod.PUT.toString().equals(responseType)) {
            // Search for existing eligibility definition and if the existing IDs are any different, delete existing
            List<Acl> existingGiverEligibility = aclUtil.getAclEligibilityGivers(programId);
            List<Acl> existingReceiverEligibility = aclUtil.getAclEligibilityReceivers(programId);

            if(!EntityUtil.isEligibilityEntitiesSame(existingGiverEligibility, currentGiverEligibility) ||
                    !EntityUtil.isEligibilityEntitiesSame(existingReceiverEligibility, currentReceiverEligibility)) {
                // entities are not the same so just delete programs Giver and Receiver roles.
                aclUtil.deleteAcl(ProgramEligibilityEnum.GIVE.getEligibilityRoleName(),programId, TableName.PROGRAM.toString(),
                        null, null);
                aclUtil.deleteAcl(ProgramEligibilityEnum.RECEIVE.getEligibilityRoleName(),programId, TableName.PROGRAM.toString(),
                        null, null);
            } else {
                // given entities are the same so no need to do any other action
                logger.warn("Eligibility entities are exactly the same as existing eligibility entities. "
                        + "Skipping update...");
                return;
            }
        }

        // insert the eligibility objects
        addEligibilityEntityStubEntries(currentGiverEligibility, programId,
                ProgramEligibilityEnum.GIVE.getEligibilityRoleName());
        addEligibilityEntityStubEntries(currentReceiverEligibility, programId,
                ProgramEligibilityEnum.RECEIVE.getEligibilityRoleName());
    }

    /**
     * Set up program_budget and program budget eligibility acl and save
     * @param budgetAssignmentStubDTOList
     * @param programId
     * @param responseType
     */
    private void saveBudgetAssignments(List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOList,
                                       @Nonnull Long programId,
                                       @Nonnull String responseType) {

        //Get complete list of budget ids
        List <Long> budgetIdList = new ArrayList<>();
        if (budgetAssignmentStubDTOList != null) {
            for (BudgetAssignmentStubDTO budgetAssignmentStubDTO : budgetAssignmentStubDTOList) {
                BudgetEntityStubDTO budget = (budgetAssignmentStubDTO != null) ?
                        budgetAssignmentStubDTO.getBudget() : new BudgetEntityStubDTO();
                budgetIdList.add(budget.getBudgetId());
            }
            budgetIdList = EntityUtil.filterDuplicateIds(budgetIdList);
        }

        Role programBudgetGiverRole = roleDao.findBy().where(ProjectConstants.ROLE_CODE)
                .eq(RoleCode.PROGRAM_BUDGET_GIVER).findOne();

        boolean budgetListsAreSame = false;
        if (responseType.equals(RequestMethod.PUT.toString())) {

            //Search for budget IDs, if the existing IDs are any different, delete/insert
            List <ProgramBudget> programBudgetList = programBudgetDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .find();

            List <Long> budgetIdFromDbList = new ArrayList<>();
            List <Long> programBudgetIdsFromDbList = new ArrayList<>();
            for (ProgramBudget programBudgetInDb : programBudgetList) {
                budgetIdFromDbList.add(programBudgetInDb.getBudgetId());
                programBudgetIdsFromDbList.add(programBudgetInDb.getId());
            }

            // We always need to delete ACL entries, we'll rebuild later
            aclDao.findBy().where(ProjectConstants.ROLE_ID).eq(programBudgetGiverRole.getRoleId())
                .and(ProjectConstants.TARGET_TABLE).eq(AclTypeCode.PROGRAM_BUDGET.name())
                .and(ProjectConstants.TARGET_ID).in(programBudgetIdsFromDbList)
                .delete();

            if (budgetIdList == null || EntityUtil.areListsSame(budgetIdList, budgetIdFromDbList)) {
                budgetListsAreSame = true;
            } else {

                //Delete from program_budget
                programBudgetDao.findBy()
                    .where(ProjectConstants.ID).in(programBudgetIdsFromDbList)
                    .delete();
            }
        }

        boolean programBudgetCreated = false;
        if (budgetAssignmentStubDTOList != null) {
            //Loop thru each budgetAssignment and insert into program_budget and acl as needed

            List<Acl> programBudgetEligibilityAclList = new ArrayList<>();
            for (BudgetAssignmentStubDTO budgetAssignmentStubDTO : budgetAssignmentStubDTOList) {
                //Extract budget
                BudgetEntityStubDTO budget = (budgetAssignmentStubDTO != null) ?
                        budgetAssignmentStubDTO.getBudget() : new BudgetEntityStubDTO();

                //Extract giving members
                List<BudgetEntityStubDTO> givingMembers = (budgetAssignmentStubDTO != null
                        && budgetAssignmentStubDTO.getGivingMembers() != null) ?
                                budgetAssignmentStubDTO.getGivingMembers() : new ArrayList<>();

                //Filter duplicate entries
                givingMembers = EntityUtil.filterDuplicateGivingMemberEntities(givingMembers);

                //Save program_budget.
                if (!budgetListsAreSame
                        && !programBudgetCreated
                        && budgetIdList != null
                        && budgetIdList.size() > 0) {
                    if (budget != null) {
                        ProgramBudget programBudgetEntity = new ProgramBudget();
                        programBudgetEntity.setBudgetId(budget.getBudgetId());
                        programBudgetEntity.setProgramId(programId);
                        programBudgetDao.create(programBudgetEntity);
                    }
                }

                // determine program_budget entity id (at this point, it either exists or we just created)
                ProgramBudget programBudget = programBudgetDao.findBy()
                        .where(ProjectConstants.PROGRAM_ID).eq(programId)
                        .and().where(ProjectConstants.BUDGET_ID).eq(budget.getBudgetId()).end()
                        .findOne();

                //Save program budget eligibility.
                for (BudgetEntityStubDTO givingMember : givingMembers) {
                    Acl acl = new Acl()
                            .setRoleId(programBudgetGiverRole.getRoleId())
                            .setTargetTable(AclTypeCode.PROGRAM_BUDGET.name())
                            .setTargetId(programBudget.getId());

                    if (givingMember.getGroupId() != null && givingMember.getGroupId() > 0) {
                        acl.setSubjectTable(AclTypeCode.GROUPS.name()).setSubjectId(givingMember.getGroupId());
                    }
                    else if (givingMember.getPaxId() != null && givingMember.getPaxId() > 0) {
                        acl.setSubjectTable(AclTypeCode.PAX.name()).setSubjectId(givingMember.getPaxId());
                    }
                    programBudgetEligibilityAclList.add(acl);
                }
            }
            aclRepository.save(programBudgetEligibilityAclList);
        }
    }

    /**
    * Saves the given award tiers associated with the specified Program.
    *
    * @param awardTierToSave award tiers to save
    * @param programId program ID
    * @param responseType response type
    */
    private void saveProgramAwardTier(List<AwardTierDTO> awardTierToSave,
                                              @Nonnull Long programId,
                                              @Nonnull String responseType) {
        // copy provided award tier amounts (to prevent modification of provided list)
        List<AwardTierDTO> programAwardTier = (awardTierToSave != null) ? new ArrayList<>(
                awardTierToSave) : new ArrayList<>();

        //if insert just save
        if (RequestMethod.POST.toString().equals(responseType)) {

            for (AwardTierDTO awardTier : programAwardTier) {
                saveAwardTier(awardTier, programId, responseType);
            }
        } else if (RequestMethod.PUT.toString().equals(responseType)) {

            // if update, get existing award tiers and iterate through,
            //either removing from specified award tiers or deleting DB record
            ProgramAwardTier programAwardTierGen = null;

            //get awardMin and awardMax to save, they are unique values
            //that can be used to verify if award tier already exists.
            Table<Long, Long, AwardTierDTO> awardTierRangeTable = HashBasedTable.create();
            for (AwardTierDTO awardTier : programAwardTier) {
                if (awardTier.getType().equalsIgnoreCase(AwardTierType.DISCRETE.toString())) {
                    awardTierRangeTable.put(awardTier.getAwardAmount(), awardTier.getAwardAmount(), awardTier);
                }
                if (awardTier.getType().equalsIgnoreCase(AwardTierType.RANGE.toString())) {
                    awardTierRangeTable.put(awardTier.getAwardMin(), awardTier.getAwardMax(), awardTier);
                }
            }

            //If no award tiers were passed in but some exist in db, they will be deleted
            // get existing award tiers
            List<ProgramAwardTier> existingProgramAwardTierEntities = getProgramAwardTierEntities(programId);

            // now check if award tier is to be removed (not specified in new award tiers) or not
            int indexAwardTierDto = 0;
            for (ProgramAwardTier existingProgramAwardTier : existingProgramAwardTierEntities) {

                Long existingAwardTierMin = existingProgramAwardTier.getMinAmount().longValue();
                Long existingAwardTierMax = existingProgramAwardTier.getMaxAmount().longValue();

                if (existingAwardTierMin == null || existingAwardTierMax == null) {
                    logger.warn("Existing Point Award Tier max or min is missing. Continuing award tier saving...");
                    continue;
                }

                // if new award tier min and max does not contain existing award tier
                //min and max, then it will be removed
                if (awardTierRangeTable.isEmpty() || !awardTierRangeTable.contains(
                        existingAwardTierMin, existingAwardTierMax)) {
                    try{
                        programAwardTierDao.delete(existingProgramAwardTier);

                        //delete the program_award_tier_reminder row.
                        programAwardTierRepository.deleteApprovalReminderEmaildById(existingProgramAwardTier.getId());

                        //delete all approval levels related to program award tier.
                        approvalProcessConfigDao.findBy()
                            .where(ProjectConstants.TARGET_ID).eq(existingProgramAwardTier.getId())
                            .delete();                        
                    }
                    catch (DataIntegrityViolationException e) {
                        // in use, inactivate
                        existingProgramAwardTier.setStatusTypeCode(StatusTypeCode.INACTIVE.name());
                        programAwardTierDao.update(existingProgramAwardTier);

                        // no need to delete
                    }
                    //If an AwardTier is removed or set as Inactive from PROGRAM_AWARD_TIER table then the association 
                    //with PROGRAM_MISC about ROUTE_NEXT_LEVEL_MANAGER record should be removed as well 
                    try {
	                    deleteProgramMisc(existingProgramAwardTier.getProgramId(), responseType, VfName.ROUTE_NEXT_LEVEL_MANAGER.toString(),	        	
	    		        String.valueOf(existingProgramAwardTier.getId().toString()));
                    	throw new Exception();
                    }catch(Exception e){
                    	logger.error(e.toString());
                    }
                } else {
                    //if award tier already exist, update record.
                    AwardTierDTO awardTierDto = awardTierRangeTable.get(existingAwardTierMin, existingAwardTierMax);
                    programAwardTierGen = generateProgramAwardTier(programId, awardTierDto, existingProgramAwardTier);
                    programAwardTierDao.update(programAwardTierGen);

                    //update the programAwardTierReminder related row.
                    programAwardTierRepository.updateApprovalReminderEmaildById(programAwardTierGen.getId(),awardTierDto.getApprovalReminderEmail().booleanValue());

                    //update approval process config.
                    saveApprovalProcessLevels(awardTierDto.getApprovals(), programAwardTierGen.getId(), responseType,programId);
                    //so remove from award tier amounts to add
                    awardTierRangeTable.remove(existingAwardTierMin, existingAwardTierMax);
                }
                indexAwardTierDto++;
            }
            // now iterate through remaining award tiers and insert program award tiers records
            if (!awardTierRangeTable.isEmpty()) {
                for (AwardTierDTO awardTier : awardTierRangeTable.values()) {
                    saveAwardTier(awardTier, programId, responseType);
                }
            }
        }
    }

    /**
    * Saves the given aprovals associated with the specified Program Award Tiers.
    *
    * @param approvalsToSave levels to save
    * @param programAwardTierId program award tier ID
    * @param responseType response type
    * @param programId
    */
    private void saveApprovalProcessLevels(List<ApprovalDTO> approvalsToSave,
                                           @Nonnull Long programAwardTierId,
                                           @Nonnull String responseType,
                                           Long programId) {
        ApprovalProcessConfig approvalProcessConfigEntity = null;

        //get approval levels to save, they are unique values that can be used
        //to verify if approval process already exists.
        Map<Integer,ApprovalDTO> approvalLevelsMap = new Hashtable<>();
        if(approvalsToSave != null){
            for(ApprovalDTO approval: approvalsToSave) {
                approvalLevelsMap.put(approval.getLevel(), approval);
            }
        }
        // if update, get existing approval levels and iterate through, either removing
        // from specified aproval processes or updating DB record
        if(RequestMethod.PUT.toString().equals(responseType)) {
           //If no approval levels were passed in but some exist in db, they will be deleted
            // get existing approval levels
            List<ApprovalProcessConfig> existingApprovalProcessEntities =
                    getApprovalProcessConfigEntities(programAwardTierId);

            // now check if approval level is to be removed or updated
            for(ApprovalProcessConfig existingApprovalProcess : existingApprovalProcessEntities) {
                Integer existingApprovalLevel = null;
                if(existingApprovalProcess.getApprovalLevel() != null){
                    existingApprovalLevel= existingApprovalProcess.getApprovalLevel().intValue();
                }

                if(existingApprovalLevel == null) {
                    logger.warn("Existing Approval level is missing. Continuing aproval process config saving...");
                    continue;
                }
                // if new approval levels does not contain existing approval level, then it will be removed
                if(approvalLevelsMap.isEmpty()|| !approvalLevelsMap.containsKey(existingApprovalLevel)){
                     approvalProcessConfigDao.delete(existingApprovalProcess);
                     
 	        		//If approvalProcessConfigDao remove the record about existing approval level (RECEIVER_FIRST or SUBMITTER_FIRST),
                     //then we need to remove as well the association that contains the info about reroutNextLevelManager from the ProgramMisc table
 	        			
                     	if (existingApprovalProcess.getApprovalTypeCode().equals(ApprovalType.RECEIVER_FIRST.toString())
                     			||existingApprovalProcess.getApprovalTypeCode().equals(ApprovalType.SUBMITTER_FIRST.toString()) ) {
	                     	try {
	 		        			deleteProgramMisc(programId, responseType, VfName.ROUTE_NEXT_LEVEL_MANAGER.toString(),	        	
	 		        					String.valueOf(existingApprovalProcess.getTargetId()));
	 	        			}catch(Exception e) {
	 	        				logger.error(e.toString());
	 	        			} 
                     	}
                } else {
                    //if approval level already exist, update record.
                    ApprovalDTO approvalDto = approvalLevelsMap.get(existingApprovalLevel);
                    approvalProcessConfigEntity = generateApprovalProcessConfig(programAwardTierId,
                            approvalDto, existingApprovalProcess);
                    approvalProcessConfigDao.update(approvalProcessConfigEntity);
                    //so remove from award tier amounts to add
                    approvalLevelsMap.remove(existingApprovalLevel);
                }
            }
        }
        if(!approvalLevelsMap.isEmpty()) {
            // now iterate through remaining approval levels and insert Approval Process Config records

            for(ApprovalDTO approvalLevel : approvalLevelsMap.values()){
                approvalProcessConfigEntity = generateApprovalProcessConfig(programAwardTierId,
                        approvalLevel, new ApprovalProcessConfig());
                approvalProcessConfigDao.save(approvalProcessConfigEntity);
            }
        }
    }


    /**
     * Set up EcardIds to save
     * @param programBasicDTO
     * @param responseType
     */
    private void saveEcardIds(ProgramBasicDTO programBasicDTO, String responseType) {
        ProgramEcard programEcard = new ProgramEcard();
        int displaySequence = 0;
        boolean listsAreSame = false;
        if (responseType.equals(RequestMethod.PUT.toString())) {
            //Search for IDs, if the existing IDs are any different, delete/insert
            List <ProgramEcard> programEcardList = programEcardDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
                .find();

            List<Long> eCardIdList = new ArrayList<>();
            for (ProgramEcard programEcardInDb : programEcardList) {
                eCardIdList.add(programEcardInDb.getEcardId());
            }
            if (programBasicDTO.getEcardIds() == null || EntityUtil.areListsSame(eCardIdList,
                    programBasicDTO.getEcardIds())) {
                listsAreSame = true;
            } else {
                for (ProgramEcard programEcardToDelete : programEcardList) {
                    programEcardDao.delete(programEcardToDelete);
                }
            }
        }

        if (programBasicDTO.getEcardIds() != null
                && programBasicDTO.getEcardIds().size() > 0
                && !listsAreSame) {
            for (Long eCardId : programBasicDTO.getEcardIds()) {
                programEcard.setProgramId(programBasicDTO.getProgramId());
                programEcard.setEcardId(eCardId);
                programEcard.setDisplaySequence(displaySequence);
                displaySequence++;
                programEcardDao.create(programEcard);
            }
        }
    }

    /**
     * Get the budget assignments and giving members for a program ID
     * @param programId
     * @return
     */
    private List<BudgetAssignmentDTO> getBudgetAssignments(@Nonnull Long programId) {
        List<BudgetAssignmentDTO> budgetAssignmentList = new ArrayList<>();

        //Get the budget Id List for the program
        List<ProgramBudget> programBudgetList = programBudgetDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .find();

        List<Long> programBudgetIdList = new ArrayList<>();
        for (ProgramBudget programBudget : programBudgetList) {
            programBudgetIdList.add(programBudget.getBudgetId());
        }

        //Query for each budget Id in List to populate BudgetAssignmentBudgetDTO objects
        List<Map<String, Object>> budgetListNodes = budgetInfoDao.getProgramBudgets(programBudgetIdList);
        List<Map<String, Object>> budgetTotalsList = budgetInfoDao.getBudgetTotals(programBudgetIdList);
        List<BudgetAssignmentDTO> budgetAssignmentDtoList = setBudgetAssignmentBudgetDTOForQueryNodes(budgetListNodes, budgetTotalsList);

        Map<Long, List<EntityDTO>> budgetGivingMembersMap = programBudgetsDao.getEligibileGivingMembers(programId);
        for (BudgetAssignmentDTO budgetAssignmentDTO : budgetAssignmentDtoList) {
            budgetAssignmentDTO.setGivingMembers(budgetGivingMembersMap.get(budgetAssignmentDTO.getBudget().getBudgetId()));
            budgetAssignmentList.add(budgetAssignmentDTO);
        }        

        return budgetAssignmentList;
    }

    /**
     * Populates List<BudgetAssignmentDTO> with BudgetAssignmentBudgetDTO based on the nodes passed in
     * @param budgetListNodes
     * @return
     */
    private List<BudgetAssignmentDTO> setBudgetAssignmentBudgetDTOForQueryNodes(
            List<Map<String, Object>> budgetListNodes, List<Map<String, Object>> budgetTotalsList) {
        List<BudgetAssignmentDTO> budgetAssignmentList = new ArrayList<>();
        for(Map<String, Object> budgetNode: budgetListNodes){
            BudgetAssignmentDTO  budgetAssignmentDTO = new BudgetAssignmentDTO ();
            BudgetAssignmentBudgetDTO budgetAssignmentBudgetDTO = new BudgetAssignmentBudgetDTO();
            budgetAssignmentBudgetDTO.setBudgetId((Long) budgetNode.get(ProjectConstants.BUDGET_ID));
            budgetAssignmentBudgetDTO.setName((String) budgetNode.get(ProjectConstants.BUDGET_NAME));
            budgetAssignmentBudgetDTO.setDisplayName((String) budgetNode.get(ProjectConstants.DISPLAY_NAME));
            budgetAssignmentBudgetDTO.setTotalAmount((Double) budgetNode.get(ProjectConstants.TOTAL_AMOUNT));
            budgetAssignmentBudgetDTO.setUsedAmount((Double) budgetNode.get(ProjectConstants.TOTAL_USED));
            budgetAssignmentBudgetDTO.setType((String) budgetNode.get(ProjectConstants.BUDGET_TYPE_CODE));
            budgetAssignmentBudgetDTO.setIndividualGivingLimit((Double) budgetNode.get(
                    ProjectConstants.INDIVIDUAL_GIVING_LIMIT));
            budgetAssignmentDTO.setBudget(budgetAssignmentBudgetDTO);
            budgetAssignmentList.add(budgetAssignmentDTO);
        }

        for (BudgetAssignmentDTO budgetAssignment : budgetAssignmentList) {
            Long budgetId = budgetAssignment.getBudget().getBudgetId();
            for (Map<String, Object> budgetTotal : budgetTotalsList) {
                Long totalBudgetId = (Long) budgetTotal.get(ProjectConstants.BUDGET_ID);
                if (budgetId.equals(totalBudgetId)) {
                    budgetAssignment.getBudget().setTotalAmount((Double) budgetTotal.get(ProjectConstants.TOTAL_AMOUNT));
                    budgetAssignment.getBudget().setUsedAmount((Double) budgetTotal.get(ProjectConstants.TOTAL_USED));
                }
            }
        }
        return budgetAssignmentList;
    }

    /**
     * Gets and generates the Eligibility information entity for the specified Program.
     *
     * @param programID Program ID
     * @return the eligibility information for the specified Program
     */
    @Transactional
    private EligibilityDTO getProgramEligibility(@Nonnull Long programID) {
        EligibilityDTO eligibilityEntity = new EligibilityDTO();

        // get the group and pax entities that are givers and receivers
        List<Acl> aclGivers = aclUtil.getAclEligibilityGivers(programID);
        List<Acl> aclReceivers = aclUtil.getAclEligibilityReceivers(programID);

        // convert the entities into the respective DTO objects and set them on eligibility
        eligibilityEntity.setGive(convertEligibilityEntitiesToEntityDTOs(aclGivers));
        eligibilityEntity.setReceive(convertEligibilityEntitiesToEntityDTOs(aclReceivers));

        return eligibilityEntity;
    }


    @Override
    public void deleteMilestoneById(Long programId, Long milestoneId) {
        milestoneService.deleteMilestoneById(programId, milestoneId);
    }

    /**
     * Determines if the program is currently being used.
     *
     * Determines usage from the following areas:
     *  -existence of nominations for the given programId for recognitions
     *  -existence of discretionary transcation table records for point load programs
     *
     * @param programId ID of the program
     * @return true if the program is associated with nominations or
     * transactions for point load programs, false otherwise
     */
    @Transactional
    private boolean isProgramUsed(@Nonnull Long programId) {
        // query existence of nominations and budget transactions to determine program usage

        List<Map<String, Object>> programNominations = nominationDaoInterface.getNominationWithoutParentByProgramId(programId);

        List<Discretionary> discretionary = cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(programId);

        return !programNominations.isEmpty() || !discretionary.isEmpty();
    }

    /**
     * Retrieves the Program Award Tiers entities from the DB associated with the specified Program.
     *
     * @param programId Program ID
     * @return Program Award Tiers entities associated with the specified Program
     */
    @Transactional
    @Nonnull
    private List<ProgramAwardTier> getProgramAwardTierEntities(@Nonnull Long programId) {
        // get the award tiers entities associated with the program ID
        List<ProgramAwardTier> awardTierEntities;

        awardTierEntities = programAwardTierDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        // either return retrieved award tiers or empty list if result was null
        return (awardTierEntities != null) ? awardTierEntities : new ArrayList<>();
    }

    /**
     * Retrieves the Approval Process Config entities from the DB associated with the specified Program Award Tier.
     *
     * @param programAwardTierId Program Award Tier ID
     * @return Approval process config entities associated with the specified Program Award Tier.
     */
    @Transactional
    @Nonnull
    private List<ApprovalProcessConfig> getApprovalProcessConfigEntities(@Nonnull Long programAwardTierId) {
        List<ApprovalProcessConfig> approvalProcessConfigEntities;

        approvalProcessConfigEntities = approvalProcessConfigDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(programAwardTierId)
                .find();

        // either return retrieved award tiers or empty list if result was null
        return (approvalProcessConfigEntities != null) ? approvalProcessConfigEntities :
            new ArrayList<>();
    }

    /**
     * Adds the specified ProgramAwardTier entity.
     *
     * @param programAwardTier ProgramAwardTier entity to add
     */
    @Transactional
    private void insertProgramAwardTier(@Nonnull ProgramAwardTier programAwardTier) {
        // attempt to add the specified program award tier record
        try {
            programAwardTierDao.save(programAwardTier);
        } catch (Exception e) {
            logger.error("Error occurred while adding award tiers!!", e);
            throw e;
        }
    }

    /**
     * Deletes the specified Program Award Tier entity from the DB.
     *
     * @param programAwardTierEntityId entity id of the program award tier record
     */
    @Transactional
    private void deleteProgramAwardTier(@Nonnull Long programAwardTierEntityId) {
        // attempt to delete the specified program award tier record (by entity id)
        try {
            programAwardTierDao.findBy()
                    .where(ProjectConstants.ID).eq(programAwardTierEntityId)
                    .delete();
            programAwardTierRepository.deleteApprovalReminderEmaildById(programAwardTierEntityId);
        } catch (Exception e) {
            logger.error("Error occurred while deleting ProgramAwardTier!!", e);
            throw e;
        }
    }

    /**
     * Gets and converts the specified eligibility entites to entity DTOs.
     *
     * @param aclEligibilityEntities eligibility entities
     * @return entity DTOs that represent the specified eligibility entities
     */
    private List<EntityDTO> convertEligibilityEntitiesToEntityDTOs(@Nonnull List<Acl> aclEligibilityEntities) {
        List<EntityDTO> entityDTOs = new ArrayList<>();

        // check if empty and return empty if so
        if(aclEligibilityEntities.isEmpty()) {
            return entityDTOs;
        }

        List<Long> groupsIds = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();

        // iterate through entities and filter ids
        for(Acl eligibilityEntity : aclEligibilityEntities) {
            String type = eligibilityEntity.getSubjectTable();
            Long entityId = eligibilityEntity.getSubjectId();

            // filter ids by type
            if(TableName.GROUPS.toString().equals(type)) {
                groupsIds.add(entityId);
            } else if(TableName.PAX.toString().equals(type)) {
                paxIds.add(entityId);
            } else {
                // log non-expected type and move on
                logger.warn("Found ACL record was not a Group or Pax but was type " + type);
            }
        }

        // get DTO information corresponding to eligibility entities
        entityDTOs.addAll(eligibilityDao.getEligibilityEntities(paxIds, groupsIds));

        return entityDTOs;
    }

    /**
     * Generates a ProgramAwardTier entity with the specified award amount for the specified Program.
     *
     * @param programId Program ID
     * @param awardTier award amount
     * @param programAwardTier
     * @return ProgramAwardTier entity object to populate and return.
     */
    @Nonnull
    private ProgramAwardTier generateProgramAwardTier(@Nonnull Long programId, @Nonnull AwardTierDTO awardTier,
            @Nonnull ProgramAwardTier programAwardTier) {
        programAwardTier.setProgramId(programId);
        programAwardTier.setIncrement(0.0);
        programAwardTier.setProgramAwardTierTypeCode(awardTier.getType());

        if (awardTier.getType().equals(AwardTierType.DISCRETE.toString())) {
            programAwardTier.setMinAmount(awardTier.getAwardAmount().doubleValue());
            programAwardTier.setMaxAmount(awardTier.getAwardAmount().doubleValue());
        } else if (awardTier.getType().equals(AwardTierType.RANGE.toString())) {
            programAwardTier.setMinAmount(awardTier.getAwardMin().doubleValue());
            programAwardTier.setMaxAmount(awardTier.getAwardMax().doubleValue());
        }

        if(Boolean.TRUE == awardTier.getAllowRaising()){
            programAwardTier.setRaisingStatusTypeCode(StatusTypeCode.ACTIVE.name());
        } else{
            programAwardTier.setRaisingStatusTypeCode(StatusTypeCode.INACTIVE.name());
        }
        programAwardTier.setStatusTypeCode(StatusTypeCode.ACTIVE.name());


        return programAwardTier;
    }

    /**
     * Generates a Aproval process config entity with the specified approval level for the specified Program Award Tier
     *
     * @param programAwardTierId Program Award Tier ID
     * @param approval DTO
     * @param approvalProcessConfig
     * @return ApprovalProcessConfig object to populate and return.
     */
    @Nonnull
    private ApprovalProcessConfig generateApprovalProcessConfig(@Nonnull Long programAwardTierId,
            @Nonnull ApprovalDTO approval, @Nonnull ApprovalProcessConfig approvalProcessConfig) {
        approvalProcessConfig.setApprovalLevel(approval.getLevel().longValue());
        approvalProcessConfig.setApprovalProcessTypeCode(NominationConstants.RECOGNITION_GIVEN_TYPE_CODE);
        approvalProcessConfig.setApprovalTypeCode(approval.getType());
        if(approval.getPax() != null)
            approvalProcessConfig.setApproverPaxId(approval.getPax().getPaxId());
        approvalProcessConfig.setTargetId(programAwardTierId);

        return approvalProcessConfig;
    }

    @Override
    public List<AwardTierDTO> getProgramAwardTiers(@Nonnull Long programId) {
        List<AwardTierDTO> awardTier = null;
        List<ApprovalDTO> approvals = null;
        AwardTierDTO awardTierDTO = null;
        ApprovalDTO approvalDTO = null;
        // get the program award tiers entities
        List<ProgramAwardTier> awardTierEntities = getProgramAwardTierEntities(programId);
        List<ProgramMisc> routeNextLevelManagerList = getProgramMiscRouteNextLevelManagersList(programId, VfName.ROUTE_NEXT_LEVEL_MANAGER.name());
        
        Map<String, Boolean> awardTierRouteNextLevelManagerMap = new HashMap<String,Boolean>();
        for (ProgramMisc programMisc :routeNextLevelManagerList) {
        	awardTierRouteNextLevelManagerMap.put(programMisc.getMiscData(), true);        	
        }
        
        // extract program award tier information from the entities and return
        awardTier = new ArrayList<>();
        for(ProgramAwardTier programAwardTier : awardTierEntities) {

            awardTierDTO = new AwardTierDTO();

            awardTierDTO.setAwardTierId(programAwardTier.getId());

            if (programAwardTier.getProgramAwardTierTypeCode() != null) {
                awardTierDTO.setType(programAwardTier.getProgramAwardTierTypeCode());
            }

            if (programAwardTier.getRaisingStatusTypeCode() != null) {
                // UDM4 - ACTIVE = enabled, INACTIVE = disabled
                awardTierDTO.setAllowRaising(programAwardTier.getRaisingStatusTypeCode()
                        .equals(StatusTypeCode.ACTIVE.name()));
            }

            if (programAwardTier.getProgramAwardTierTypeCode() != null &&
                    programAwardTier.getProgramAwardTierTypeCode().equalsIgnoreCase(AwardTierType.RANGE.toString())) {
                awardTierDTO.setAwardMax(programAwardTier.getMaxAmount().longValue());
                awardTierDTO.setAwardMin(programAwardTier.getMinAmount().longValue());
            } else {
                awardTierDTO.setAwardAmount(programAwardTier.getMinAmount().longValue());
            }

            //get approval levels
            List<ApprovalProcessConfig> approvalLevels = approvalProcessConfigDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(programAwardTier.getId())
                    .find();

            approvals = new ArrayList<>();
            for(ApprovalProcessConfig approval : approvalLevels){
                approvalDTO = new ApprovalDTO();
                if (approval.getApprovalLevel() != null) {
                    approvalDTO.setLevel(approval.getApprovalLevel().intValue());
                }
                approvalDTO.setType(approval.getApprovalTypeCode());
                approvalDTO.setPax(participantInfoDao.getEmployeeDTO(approval.getApproverPaxId()));
                approvals.add(approvalDTO);
            }
            awardTierDTO.setApprovals(approvals);
            awardTierDTO.setStatus(programAwardTier.getStatusTypeCode());
            String awardTierId = programAwardTier.getId().toString();
            if (awardTierId!= null) {            
	            if (awardTierRouteNextLevelManagerMap.containsKey(awardTierId)) {
	            	awardTierDTO.setRouteNextLevelManager(awardTierRouteNextLevelManagerMap.get(awardTierId));	            		            	
	            }else {
	            	awardTierDTO.setRouteNextLevelManager(false);
	            }
	            //getting the approvalReminderEmail field
                Boolean reminder = programAwardTierRepository.getApprovalReminderEmaildById(programAwardTier.getId());
                if(reminder != null){
                    awardTierDTO.setApprovalReminderEmail(reminder);
                }else{
                    awardTierDTO.setApprovalReminderEmail(false); //false by default.
                }
            }
            awardTier.add(awardTierDTO);
        }
        return awardTier;
    }

    /**
     * Creating PROGRAM_FILES data for program image, icon and certificates.
     * @param programRequestDTO
     */
    private void createProgramFilesEntries(ProgramRequestDTO programRequestDTO) {
        List<Long> fileIdsToSave = new ArrayList<>();

        // adding program image id
        if(programRequestDTO.getImageId() != null) {
            fileIdsToSave.add(programRequestDTO.getImageId());
        }

        // adding file Ids for Award Code program type
        if (ProgramTypeEnum.AWARD_CODE.name().equals(programRequestDTO.getProgramType())
                && programRequestDTO.getAwardCode() != null) {
            if (programRequestDTO.getAwardCode().getIconId() != null) {
                fileIdsToSave.add(programRequestDTO.getAwardCode().getIconId());
            }
            if (programRequestDTO.getAwardCode().getCertificates() != null) {
                for(Certificates cert: programRequestDTO.getAwardCode().getCertificates()){
                    fileIdsToSave.add(cert.getId());
                }
            }
        }

        for (Long fileId: fileIdsToSave) {
            fileImageService.saveFileItemsEntry(fileId, TableName.PROGRAM.name(), programRequestDTO.getProgramId());
        }
    }

    /**
     * Save a single program file record.
     * @param programId
     * @param fileId
     */
    private void saveProgramFilesEntry(Long programId, Long fileId) {

        FileItem programFiles = new FileItem()
                .setTargetId(programId)
                .setFilesId(fileId)
                .setTargetTable(TableName.PROGRAM.name());
        fileItemDao.save(programFiles);
    }

    /**
     * Get program files and types related to programId.
     * @param programId
     * @return ProgramFilesDistinctTypesDTO
     */
    private ProgramFilesDistinctTypesDTO getCurrentProgramFilesTypes(Long programId) {
        ProgramFilesDistinctTypesDTO programFilesTypes = null;

        List<FileItem> programFileCurrentList = fileItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(programId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.PROGRAM.name())
                .find();

        if (!CollectionUtils.isEmpty(programFileCurrentList)) {
            programFilesTypes = new ProgramFilesDistinctTypesDTO();

            List<FileItem> certIds = new ArrayList<>();
            for (FileItem programFile: programFileCurrentList) {
                String fileType = filesDao.findBy()
                        .where(ProjectConstants.ID).eq(programFile.getFilesId())
                        .findOne(ProjectConstants.FILE_TYPE_CODE, String.class);
                try {
                    if (fileType != null) {
                        switch(FileTypes.valueOf(fileType)){
                            case AWARD_CODE_ICON:
                                programFilesTypes.setProgramFileIcon(programFile);
                                break;
                            case AWARD_CODE_CERT:
                                certIds.add(programFile);
                                break;
                            case PROGRAM_IMAGE:
                                programFilesTypes.setProgramFileImage(programFile);
                                break;
                            default:
                                break;
                        }
                    } else {
                        logger.error("File ID " + programFile.getFilesId() + " is tied to program ID "
                            + programFile.getTargetId() + " but that fileId does not exist.");
                    }
                } catch (IllegalArgumentException e) {
                    logger.error("File type code is not supported :" + fileType);
                }
            }
            if (!CollectionUtils.isEmpty(certIds)) {
                programFilesTypes.setProgramFileCertificates(certIds);
            }
        }
        return programFilesTypes;
    }

    /**
     * Update program files data related to program request.
     * @param programRequestDTO
     */
    private void updateProgramFilesEntries(ProgramRequestDTO programRequestDTO) {
        ProgramFilesDistinctTypesDTO programFilesTypes = getCurrentProgramFilesTypes(programRequestDTO.getProgramId());

        if (programFilesTypes != null) {

            // Updating program image id
            if (programFilesTypes.getProgramFileImage() != null) {
                if (programRequestDTO.getImageId() == null) {
                    fileItemDao.delete(programFilesTypes.getProgramFileImage());
                }
                else if (!programFilesTypes.getProgramFileImage().getFilesId().equals(programRequestDTO.getImageId())) {
                    fileItemDao.save(programFilesTypes.getProgramFileImage()
                            .setFilesId(programRequestDTO.getImageId()));
                }
            }
            else if (programRequestDTO.getImageId() != null) {
                saveProgramFilesEntry(programRequestDTO.getProgramId(), programRequestDTO.getImageId());
            }

            // Award code program
            if (ProgramTypeEnum.AWARD_CODE.name().equals(programRequestDTO.getProgramType())) {
                AwardCodeProgramDetailsDTO awardCodeDetails = programRequestDTO.getAwardCode();

                // Updating award code program icon id
                if (programFilesTypes.getProgramFileIcon() != null) {
                    if (awardCodeDetails.getIconId() != null
                            && !programFilesTypes.getProgramFileIcon().getFilesId()
                            .equals(awardCodeDetails.getIconId())) {
                        fileItemDao.save(
                                programFilesTypes.getProgramFileIcon().setFilesId(awardCodeDetails.getIconId()));
                    }

                    // Delete award code program icon (only if program status is INACTIVE)
                    else if (awardCodeDetails.getIconId() == null)  {
                        fileItemDao.delete(programFilesTypes.getProgramFileIcon());
                    }
                }

                // Create new program file for award code icon id
                else if (awardCodeDetails.getIconId() != null) {
                    saveProgramFilesEntry(programRequestDTO.getProgramId(), awardCodeDetails.getIconId());
                }

                // Updating award code program certificate ids
                List<Long> existedCertIdsList = new ArrayList<>();
                if (programFilesTypes.getProgramFileCertificates() != null) {
                    for (FileItem programFile : programFilesTypes.getProgramFileCertificates()) {
                        if (awardCodeDetails.getCertificates() != null) {
                            List<Long> certIds = (List<Long>) CollectionUtils.<Certificates,Long>collect(
                                awardCodeDetails.getCertificates()
                                ,TransformerUtils.invokerTransformer(ProjectConstants.GET_ID)
                            );
                            if (certIds == null || !certIds.contains(programFile.getFilesId())) {
                                fileItemDao.delete(programFile);
                            } else {
                                existedCertIdsList.add(programFile.getFilesId());
                            }
                        } else {
                            existedCertIdsList.add(programFile.getFilesId());
                        }
                    }
                }

                // Add new certificates
                if (programRequestDTO.getAwardCode().getCertificates() != null) {
                    for (Certificates cert : programRequestDTO.getAwardCode().getCertificates()) {
                        if (!existedCertIdsList.contains(cert.getId())) {
                            saveProgramFilesEntry(programRequestDTO.getProgramId(), cert.getId());
                        }
                    }
                }

            }
        }
        else {
            createProgramFilesEntries(programRequestDTO);
        }
    }

    /**
     *
     * @param awardTier
     * @param programActivityId
     * @param responseType
     *
     * Saves Award Tier
     */
    private void saveAwardTier (AwardTierDTO awardTier, Long programActivityId, String responseType) {
        ProgramAwardTier programAwardTierGen = generateProgramAwardTier(programActivityId,
                awardTier, new ProgramAwardTier());
        programAwardTierDao.save(programAwardTierGen);
        saveApprovalProcessLevels(awardTier.getApprovals(), programAwardTierGen.getId(), responseType,programActivityId);
        ProgramAwardTierReminder reminder = new ProgramAwardTierReminder(programAwardTierGen.getId(),awardTier.getApprovalReminderEmail());
        programAwardTierReminderDao.save(reminder);
    }

    /**
     *
     * @param programRequestDTO
     * @return list of ErrorMessage
     *
     * Validate AwardTier
     */
    private List<ErrorMessage> validateAwardTier (ProgramRequestDTO programRequestDTO) {
        List<ErrorMessage> errors = new ArrayList<>();

        if (!CollectionUtils.isEmpty(programRequestDTO.getAwardTiers())) {

            // Get Validate program award types
            List<ProgramAwardTierType> programAwardTierTypeList = programAwardTierTypeDao.findBy().find();
            List<String> validAwardTierTypes = new ArrayList<>();
            for (ProgramAwardTierType type : programAwardTierTypeList) {
                validAwardTierTypes.add(type.getProgramAwardTierTypeCode());
            }

            //Get the max allowed budget
            Long maxBudgetLimit = null;
            List<Long> budgetIdList = new ArrayList<>();
            if (!CollectionUtils.isEmpty(programRequestDTO.getBudgetAssignments())) {

                for (BudgetAssignmentStubDTO budgetAssignmentStubDTO : programRequestDTO.getBudgetAssignments()) {
                    if (budgetAssignmentStubDTO.getBudget() != null &&
                            budgetAssignmentStubDTO.getBudget().getBudgetId() != null) {
                        budgetIdList.add(budgetAssignmentStubDTO.getBudget().getBudgetId());
                    }
                }
                budgetIdList = EntityUtil.filterDuplicateIds(budgetIdList);

                List<Budget> budgetList = budgetDao.findById(budgetIdList);
                // get child budgets
                budgetList.addAll(budgetDao.findBy()
                        .where(ProjectConstants.PARENT_BUDGET_ID).in(budgetIdList)
                        .find());

                if (!CollectionUtils.isEmpty(budgetList)) {
                    maxBudgetLimit = 0L;
                    for (Budget budget : budgetList) {
                        if (budget.getBudgetTypeCode().equalsIgnoreCase(BudgetTypeEnum.DISTRIBUTED.name())
                                && budget.getParentBudgetId() == null) {
                            // distributed parent budget, skip
                            continue;
                        }
                        if (budget.getIndividualGivingLimit() == null) {
                            maxBudgetLimit = null;// simple or child budget with no giving limit, no cap on award tiers
                            break;
                        }
                        else if (budget.getIndividualGivingLimit().longValue() > maxBudgetLimit) {
                            maxBudgetLimit = budget.getIndividualGivingLimit().longValue();
                        }
                    }
                }
            }

            for (AwardTierDTO awardTier : programRequestDTO.getAwardTiers()) {
                //validate validate type
                if (awardTier.getType() == null) {
                    errors.add(ProgramConstants.MISSING_AWARD_TIER_TYPE_MESSAGE);
                } else if (!validAwardTierTypes.contains(awardTier.getType())) {
                    errors.add(ProgramConstants.INVALID_AWARD_TIER_TYPE_MESSAGE);
                } else {

                    //Validate award tiers amounts
                    if (awardTier.getType().equalsIgnoreCase(AwardTierType.RANGE.toString())) {

                        if (awardTier.getAwardMin() == null) {
                            errors.add(ProgramConstants.MISSING_AWARD_MIN_MESSAGE);

                        } else if (awardTier.getAwardMax() == null) {
                            errors.add(ProgramConstants.MISSING_AWARD_MAX_MESSAGE);

                        } else {

                            if (awardTier.getAwardMin() < ProgramConstants.RANGE_MIN_MINUMN_VALUE) {
                                errors.add(ProgramConstants.AWARD_MIN_MINIMUM_VALUE_CODE_MESSAGE);
                            }

                            if (maxBudgetLimit != null && (awardTier.getAwardMax() > maxBudgetLimit)) {
                                errors.add(ProgramConstants.AWARD_MAX_LARGER_THAN_ALLOWED_BUDGET_MESSAGE);
                            }

                            if (awardTier.getAwardMin().equals(awardTier.getAwardMax())) {
                                errors.add(ProgramConstants.AWARD_MIN_MAX_CAN_NOT_EQUAL_MESSAGE);
                            } else if (awardTier.getAwardMin() > awardTier.getAwardMax()) {
                                errors.add(ProgramConstants.AWARD_MIN_GREATER_THAN_MAX_MESSAGE);
                            }

                        }

                    } else if (awardTier.getType().equalsIgnoreCase(AwardTierType.DISCRETE.toString())) {

                        if (awardTier.getAwardAmount() == null) {
                            errors.add(ProgramConstants.MISSING_AWARD_AMOUNT_CODE_MESSAGE);

                        } else if (maxBudgetLimit != null && (awardTier.getAwardAmount() > maxBudgetLimit)) {
                            errors.add(ProgramConstants.AWARD_AMOUNT_LARGER_THAN_ALLOWED_BUDGET_CODE_MESSAGE);
                        }
                    }
                }
                if (ProgramTypeEnum.AWARD_CODE.name().equalsIgnoreCase(programRequestDTO.getProgramType())) {
                    if (awardTier.getAllowRaising() != null) {
                        errors.add(ProgramConstants.AWARD_CODE_AWARD_TIERS_ALLOW_RAISING_MESSAGE);
                    }
                    if (!CollectionUtils.isEmpty(awardTier.getApprovals())) {
                        errors.add(ProgramConstants.AWARD_CODE_AWARD_TIERS_APPROVALS_MESSAGE);
                    }
                }
                if(awardTier.getApprovalReminderEmail() == null){
                    errors.add(ProgramConstants.ERROR_MISSING_AWARD_TIER_REMINDER_MESSAGE);
                }
            }
        }
        return errors;
    }
}
