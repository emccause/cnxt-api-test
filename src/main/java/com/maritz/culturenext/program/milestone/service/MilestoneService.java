package com.maritz.culturenext.program.milestone.service;

import java.util.List;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.culturenext.program.dto.ProgramBasicDTO;

public interface MilestoneService {

    /**
     * gets the milestones associated with the program id provided
     *
     * @param programId - id of program to fetch milestones for
     * @return List of milestones (empty if none are related)
     */
    public List<MilestoneDTO> getMilestones(Long programId);

    /**
     * validates that:
     *  - All required fields are populated and meet requirements
     *  - No duplication per type / repetition / years of service combination
     *
     * @param program request program with milestones to be validated
     * @return List of errors, if any
     */
    public List<ErrorMessage> validateMilestones(ProgramBasicDTO program);

    /**
     * Deletes milestones for the program if they exist and then inserts provided milestone configuration
     *
     * @param program - program request program with milestones to be inserted / updated
     */
    public void insertUpdateMilestones(ProgramBasicDTO program);


  /**
     * Deletes a milestone as long as it belongs to the program provided.
     *
     * @param programId - id of the program to match against
     * @param milestoneId - milestone id (program activity id) to delete
     */
    void deleteMilestoneById(Long programId, Long milestoneId);

  /**
   * Triggered task that publishes birthday milestones
   */
    void findBirthdayMilestones();

  /**
   * Triggered task that publishes service anniversary milestones
   */
  void findServiceAnniversaryMilestones();

  /**
   * Triggered task that publishes birthday milestone reminders
   */
  void findUpcomingBirthdayMilestoneReminders();

  /**
   * Triggered task that publishes service anniversary milestone reminders
   */
  void findUpcomingServiceAnniversaryMilestoneReminders();
  /**
   * Triggered task that check for errors about access denied on Service Anniversary milestones
   */
  void findAccessDeniedErrorsForServiceAnniversaryMilestones();
}
