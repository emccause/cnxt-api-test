package com.maritz.culturenext.program.milestone.event;

public class PaxMilestoneDTO {
    private Long paxId;
    private String eventType;
    private Long yearsOfService;
    private Long programActivityId;
    private Long programId;

    public PaxMilestoneDTO(Long paxId, String eventType, Long yearsOfService, Long programActivityId, Long programId) {
      this.paxId = paxId;
      this.eventType = eventType;
      this.yearsOfService = yearsOfService;
      this.programId = programId;
    }

    public Long getPaxId() {
      return paxId;
    }

    public void setPaxId(Long paxId) {
      this.paxId = paxId;
    }

    public String getEventType() {
      return eventType;
    }

    public void setEventType(String eventType) {
      this.eventType = eventType;
    }

    public Long getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(Long yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public Long getProgramActivityId() {
        return programActivityId;
    }

    public void setProgramActivityId(Long programId) {
        this.programActivityId = programId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

}
