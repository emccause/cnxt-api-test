package com.maritz.culturenext.program.milestone.event.listener;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.dto.TargetDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.rest.NotFoundException;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.MilestoneDao;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class MilestoneEventListener {
    private MilestoneDao milestoneDao;
    private SysUserRepository sysUserRepository;
    private NominationService nominationService;
    private BatchService batchService;
    private static final String SYS_USER = "admin";

    @Inject
    MilestoneEventListener setMilestoneDao(MilestoneDao milestoneDao) {
        this.milestoneDao = milestoneDao;
        return this;
    }

    @Inject
    MilestoneEventListener setSysUserRepository(SysUserRepository sysUserRepository) {
        this.sysUserRepository = sysUserRepository;
        return this;
    }

    @Inject
    MilestoneEventListener setNominationService(NominationService nominationService) {
        this.nominationService = nominationService;
        return this;
    }

    @Inject
    MilestoneEventListener setBatchService(BatchService batchService) {
        this.batchService = batchService;
        return this;
    }

    @EventListener
    public void handleMilestones(MilestoneEvent milestoneEvent) {
        Batch batch;
        if (milestoneEvent.getMilestoneType().equalsIgnoreCase(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE)) {
            batch = batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_EVENT.name());
        }
        else {
            batch = batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_EVENT.name());
        }

        int totalRecords = 0;
        int totalErrorRecords = 0;
        double totalAmount = 0;

        for (PaxMilestoneDTO paxMilestoneDTO : milestoneEvent.getPaxMilestoneList()) {
            totalRecords++;
            try {
                Map<String, Object> programData = getProgramData(paxMilestoneDTO);
                Long systemUser = sysUserRepository.findBySysUserName(SYS_USER).getSysUserId();
                NominationRequestDTO nominationRequestDTO = createNominationRequest(programData, paxMilestoneDTO, batch.getBatchId());
                totalAmount = totalAmount + getReceiverAwardAmount(nominationRequestDTO);
                nominationService.createStandardNomination(systemUser, nominationRequestDTO, true);
            } catch (Exception e) {
                logBatchEvent(batch, BatchEventTypeCode.ERR.name(), e.getMessage(), "PAX", paxMilestoneDTO.getPaxId());
                totalErrorRecords++;
            }
        }
        logBatchEvent(batch, BatchEventTypeCode.RECS.name(), String.valueOf(totalRecords));
        logBatchEvent(batch, BatchEventTypeCode.ERRS.name(), String.valueOf(totalErrorRecords));
        logBatchEvent(batch, BatchEventTypeCode.TOTAL_POINTS.name(), String.valueOf(totalAmount));
        batchService.endBatch(batch);
    }

    private Map<String, Object> getProgramData(PaxMilestoneDTO paxMilestoneDTO) {
        Map<String, Object> programData;
        if (isServiceAnniversary(paxMilestoneDTO.getEventType())) {
            programData = milestoneDao.getMilestoneProgramData(paxMilestoneDTO.getPaxId(),
                    paxMilestoneDTO.getProgramId(),
                    paxMilestoneDTO.getEventType(),
                    paxMilestoneDTO.getYearsOfService());
        } else { // is Birthday
            programData = milestoneDao.getMilestoneProgramData(paxMilestoneDTO.getPaxId(), paxMilestoneDTO.getProgramId(), MilestoneConstants.BIRTHDAY_TYPE, null);
        }

        // TODO: MP-12017 - Refactor to use a JDBC to map to a DTO
        if (programData == null || programData.isEmpty()) {
            throw new NotFoundException("programId");
        }

        return programData;
    }

    private boolean isServiceAnniversary(String eventType) {
        return eventType.equals(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
    }

    private void logBatchEvent(Batch batch, String typeCode, String message) {
        logBatchEvent(batch, typeCode, message, null, null);
    }

    private void logBatchEvent(Batch batch, String typeCode, String message, String targetTable, Long targetId) {
        BatchEventDTO batchEvent = new BatchEventDTO();
        batchEvent.setBatchId(batch.getBatchId());
        batchEvent.setType(typeCode);
        batchEvent.setMessage(message);
        batchEvent.setTarget(new TargetDTO(targetTable, targetId));
        batchService.createBatchEvent(batch, batchEvent);
    }

    private int getReceiverAwardAmount(NominationRequestDTO nominationRequestDTO) {
        if (!nominationRequestDTO.getReceivers().isEmpty()) {
            //there will only be one for milestones
            //TODO: Change to original award amount when appropriate
            if (nominationRequestDTO.getReceivers().get(0).getAwardAmount() != null) {
                return nominationRequestDTO.getReceivers().get(0).getAwardAmount().intValue();
            }
        }
        return 0;
    }

  private NominationRequestDTO createNominationRequest(Map<String, Object> programData, PaxMilestoneDTO paxMilestoneDTO, Long batchId) {
      ErrorMessageException.throwIfHasErrors(validateProgramDataForNomination(programData));

      NominationRequestDTO nominationRequestDTO = new NominationRequestDTO();
      GroupMemberAwardDTO groupMemberAwardDTO = new GroupMemberAwardDTO();

      // required fields
      String publicHeadline = programData.get("publicHeadline").toString();
      nominationRequestDTO.setHeadline(publicHeadline);
      groupMemberAwardDTO.setPaxId(paxMilestoneDTO.getPaxId());
      nominationRequestDTO.setProgramId(Long.parseLong(programData.get("programId").toString()));

      if (programData.get(ProjectConstants.PRIVATE_MESSAGE) != null) {
          String privateMessage = programData.get(ProjectConstants.PRIVATE_MESSAGE).toString();
          nominationRequestDTO.setComment(privateMessage);
      }

      if (programData.get("budgetId") != null) {
          Long budgetId = Long.parseLong(programData.get("budgetId").toString());
          nominationRequestDTO.setBudgetId(budgetId);
      }

      if (programData.get("ecardId") != null) {
          Long eCardId = Long.parseLong(programData.get("ecardId").toString());
          nominationRequestDTO.setImageId(eCardId);
      }

      if (programData.get("awardAmount") != null) {
          double awardAmount = Double.parseDouble(programData.get("awardAmount").toString());
          int awardAmountInt = (int) awardAmount;
          if (awardAmount != 0) {
              groupMemberAwardDTO.setAwardAmount(awardAmountInt);
              groupMemberAwardDTO.setOriginalAmount(awardAmountInt);
          }
      }

      if (programData.get("payoutType") != null) {
          String payoutType = programData.get("payoutType").toString();
          nominationRequestDTO.setPayoutType(payoutType);
      }

      nominationRequestDTO.setGiveAnonymous(true);

      List<GroupMemberAwardDTO> receivers = new ArrayList<>();
      receivers.add(groupMemberAwardDTO);

      nominationRequestDTO.setReceivers(receivers);
      nominationRequestDTO.setBatchId(batchId);
      return nominationRequestDTO;
  }

  private List<ErrorMessage> validateProgramDataForNomination(Map<String, Object> programData) {
        List<ErrorMessage> errors = new ArrayList<>();

        Boolean missingHeadline =
                !programData.containsKey("publicHeadline") ||
                        programData.get("publicHeadline") == null ||
                        programData.get("publicHeadline").toString().isEmpty();

        Boolean budgetIdIsRequired =
                (!programData.containsKey("budgetId") && programData.containsKey("awardAmount")) ||
                        (programData.get("budgetId") == null && programData.get("awardAmount") != null);

        Boolean payoutTypeIsRequired =
                ((programData.containsKey("budgetId") || programData.containsKey("awardAmount")) && !programData.containsKey("payoutType")) ||
                        ((programData.get("budgetId") != null || programData.get("awardAmount") != null) && programData.get("payoutType") == null);

        if (missingHeadline) {
            errors.add(ProgramConstants.HEADLINE_MISSING_MESSAGE);
        }

        if (budgetIdIsRequired) {
            errors.add(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE);
        }

        if (payoutTypeIsRequired) {
            errors.add(ProgramConstants.ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MESSAGE);
        }

        return errors;
    }
}
