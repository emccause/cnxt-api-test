package com.maritz.culturenext.program.milestone.service.impl;

import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.culturenext.email.event.RecognitionEvent;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.jpa.repository.CnxtProgramAwardTierRepository;
import com.maritz.culturenext.program.milestone.service.ProgramReminderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Component
public class ProgramReminderServiceImpl implements ProgramReminderService {

    final Logger logger = LoggerFactory.getLogger(getClass());
    @Inject private ApplicationEventPublisher applicationEventPublisher;
    @Inject private CnxtProgramAwardTierRepository cnxtProgramAwardTierRepository;
    @Inject private Environment environment;

    /**
     * Notify managers thru email for all pending recognitions every X days according to the program configuration.
     *
     */
    @Override
    @TriggeredTask(name = "sendPendingApprovalReminder")
    public void sendPendingApprovalRecognitionEmail() {
        logger.info("Started sendPendingApprovalReminder job.");

        // get pending approval recognitions that are part of a program with a reminder days different than 0 and projectProfile.reminderDaysEnabled is TRUE.
        List<Map<String, Object>>nominations = cnxtProgramAwardTierRepository.getPendingApprovalProgramReminderDays();

        // send emails
            for (Map<String, Object> nomination : nominations){
                Integer reminderDays = 10;
                LocalDate truncatedCreateDate = LocalDate.parse(nomination.get("CREATE_DATE").toString());
                LocalDate truncatedToday = LocalDate.now();
                if((truncatedToday.toEpochDay() > truncatedCreateDate.toEpochDay()) &&
                        ((truncatedToday.toEpochDay()-truncatedCreateDate.toEpochDay())%reminderDays)==0){
                    applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.APPROVAL_REMINDER.getCode(), Long.parseLong(nomination.get("ID").toString())));
                    logger.info("nomination id {} , create_date {} reminder has been sent", nomination.get("ID"), truncatedCreateDate.toString(), reminderDays);
                }
            }
            logger.info("Finished sendPendingApprovalReminder job.");
    }
}
