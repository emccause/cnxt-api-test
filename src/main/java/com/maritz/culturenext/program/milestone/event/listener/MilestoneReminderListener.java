package com.maritz.culturenext.program.milestone.event.listener;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.dto.TargetDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class MilestoneReminderListener {
    private BatchService batchService;

    @Inject
    private AlertService alertService;

    @Inject
    public MilestoneReminderListener setBatchService(BatchService batchService) {
        this.batchService = batchService;
        return this;
    }

    @Async
    @EventListener
    public void handleMilestoneReminders(MilestoneReminderEvent milestoneReminderEvent) {
        Batch batch;
        if (milestoneReminderEvent.getEventType().equals(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode())) {
            batch = batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_REMINDER_ALERT.name());
        }
        else {
            batch = batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_REMINDER_ALERT.name());
        }

        int totalRecords = 0;
        int totalErrorRecords = 0;

        for (PaxMilestoneReminderDTO paxMilestoneReminderDTO : milestoneReminderEvent.getPaxMilestoneReminderList()) {
            totalRecords++;
            try {
                alertService.addMilestoneReminderAlert(paxMilestoneReminderDTO);
            } catch (Exception e) {
                logBatchEvent(batch, BatchEventTypeCode.ERR.name(), e.getMessage(), "PAX", paxMilestoneReminderDTO.getPaxId());
                totalErrorRecords++;
            }
        }

        logBatchEvent(batch, BatchEventTypeCode.RECS.name(), String.valueOf(totalRecords));
        logBatchEvent(batch, BatchEventTypeCode.ERRS.name(), String.valueOf(totalErrorRecords));
        batchService.endBatch(batch);
    }

    private void logBatchEvent(Batch batch, String typeCode, String message) {
        logBatchEvent(batch, typeCode, message, null, null);
    }

    private void logBatchEvent(Batch batch, String typeCode, String message, String targetTable, Long targetId) {
        BatchEventDTO batchEvent = new BatchEventDTO();
        batchEvent.setBatchId(batch.getBatchId());
        batchEvent.setType(typeCode);
        batchEvent.setMessage(message);
        batchEvent.setTarget(new TargetDTO(targetTable, targetId));
        batchService.createBatchEvent(batch, batchEvent);
    }
}
