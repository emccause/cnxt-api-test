package com.maritz.culturenext.program.milestone.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.thymeleaf.util.ListUtils;

import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.program.milestone.service.MilestoneReminderService;
import com.maritz.culturenext.util.DateUtil;

@Component
public class MilestoneReminderServiceImpl implements MilestoneReminderService {
    
    private ProgramMiscRepository programMiscRepository;
    
    @Inject
    public MilestoneReminderServiceImpl setProgramMiscRepository(ProgramMiscRepository programMiscRepository) {
        this.programMiscRepository = programMiscRepository;
        return this;
    }

    @Override
    public List<Integer> getMilestoneReminders(Long programId) {
        List<Integer> milestoneRemindersList = new ArrayList<Integer>();
        
        List<ProgramMisc> programMiscs = getMilestoneRemindersListBy(programId);

        if (!ListUtils.isEmpty(programMiscs)) {
            for (ProgramMisc programMisc : programMiscs) {
                milestoneRemindersList.add(Integer.parseInt(programMisc.getMiscData()));
            }
        }

        return milestoneRemindersList;
    }

    @Override
    public void insertUpdateMilestoneReminders(Long programId, List<Integer> milestoneReminders) {
        List<ProgramMisc> programMiscs = getMilestoneRemindersListBy(programId);
        programMiscRepository.delete(programMiscs);

        if (!ListUtils.isEmpty(milestoneReminders)) {
            for (Integer milestoneReminder : milestoneReminders) {
                ProgramMisc programMisc = new ProgramMisc();
                programMisc.setProgramId(programId);
                programMisc.setVfName(VfName.MILESTONE_REMINDER.toString());
                programMisc.setMiscData(milestoneReminder.toString());
                programMisc.setMiscDate(DateUtil.convertFromString(DateUtil.convertToUTCDate(new Date())));
                programMiscRepository.save(programMisc);
            }
        }
    }

    private List<ProgramMisc> getMilestoneRemindersListBy(Long programId) {
        List<String> vfNames = new ArrayList<String>();
        vfNames.add(VfName.MILESTONE_REMINDER.toString());
        List<ProgramMisc> programMiscs = programMiscRepository.findAll(programId, vfNames);
        return programMiscs;
    }

}
