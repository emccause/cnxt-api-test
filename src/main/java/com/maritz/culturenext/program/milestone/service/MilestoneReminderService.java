package com.maritz.culturenext.program.milestone.service;

import java.util.List;

public interface MilestoneReminderService {

    /**
     * gets the milestone reminders associated with the program id provided
     * 
     * @param programId - id of program to fetch milestone reminders for
     * @return List of milestone reminder values (empty if none are related)
     */
    public List<Integer> getMilestoneReminders(Long programId);
    
    /**
     * Deletes milestone reminders for the program if they exist and then inserts provided milestone reminders
     * 
     * @param programId - id of program to insert/update milestone reminders for
     * @param List of milestone reminder values
     */
    public void insertUpdateMilestoneReminders(Long programId, List<Integer> milestoneReminders);


}
