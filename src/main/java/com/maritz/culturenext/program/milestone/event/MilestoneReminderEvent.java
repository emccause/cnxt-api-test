package com.maritz.culturenext.program.milestone.event;

import org.springframework.context.ApplicationEvent;

import java.util.List;

public class MilestoneReminderEvent extends ApplicationEvent {

    private String eventType;

    public MilestoneReminderEvent(List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList, String eventType) {
        super(paxMilestoneReminderDTOList);
        this.eventType = eventType;
    }

    public String getEventType() { return eventType; }

    public void setEventType(String eventType) { this.eventType = eventType; }

    public List<PaxMilestoneReminderDTO> getPaxMilestoneReminderList() {
        return (List<PaxMilestoneReminderDTO>) this.getSource();
    }
}