package com.maritz.culturenext.program.milestone.service.impl;

import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityMisc;
import com.maritz.core.jpa.repository.ProgramActivityMiscRepository;
import com.maritz.core.jpa.repository.ProgramActivityRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.NotFoundException;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.culturenext.enums.ProgramActivityTypeCode;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtBatchEventRepository;
import com.maritz.culturenext.jpa.repository.CnxtBatchRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityMiscRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.program.dao.MilestoneDao;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.culturenext.program.dto.ProgramBasicDTO;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneEvent;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneDTO;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.program.milestone.service.MilestoneService;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.DateUtil;
import com.microsoft.sqlserver.jdbc.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.ListUtils;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchType;

@Component
public class MilestoneServiceImpl implements MilestoneService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    private CnxtProgramActivityMiscRepository cnxtProgramActivityMiscRepository;
    private ProgramActivityRepository programActivityRepository;
    private ProgramActivityMiscRepository programActivityMiscRepository;
    private MilestoneDao milestoneDao;
    private ApplicationEventPublisher applicationEventPublisher;
    private CnxtBatchRepository cnxtBatchRepository;
    private CnxtBatchEventRepository cnxtBatchEventRepository;
    
    @Inject
    public MilestoneServiceImpl setCnxtBatchRepository(CnxtBatchRepository cnxtBatchRepository) {
        this.cnxtBatchRepository = cnxtBatchRepository;
        return this;
    }
    
    @Inject
    public MilestoneServiceImpl setCnxtBatchEventRepository(CnxtBatchEventRepository cnxtBatchEventRepository) {
        this.cnxtBatchEventRepository = cnxtBatchEventRepository;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setCnxtProgramActivityRepository(CnxtProgramActivityRepository cnxtProgramActivityRepository) {
        this.cnxtProgramActivityRepository = cnxtProgramActivityRepository;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setCnxtProgramActivityMiscRepository(CnxtProgramActivityMiscRepository cnxtProgramActivityMiscRepository) {
        this.cnxtProgramActivityMiscRepository = cnxtProgramActivityMiscRepository;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setProgramActivityMiscRepository(ProgramActivityMiscRepository programActivityMiscRepository) {
        this.programActivityMiscRepository = programActivityMiscRepository;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setProgramActivityRepository(ProgramActivityRepository programActivityRepository) {
        this.programActivityRepository = programActivityRepository;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setMilestoneDao(MilestoneDao milestoneDao) {
        this.milestoneDao = milestoneDao;
        return this;
    }

    @Inject
    public MilestoneServiceImpl setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
        return this;
    }

    @Override
    public List<MilestoneDTO> getMilestones(Long programId) {
        List<MilestoneDTO> resultList = new ArrayList<MilestoneDTO>();
        List<ProgramActivity> programActivityList = programActivityRepository.findBy()
                .where("programId").eq(programId)
                .and("programActivityName").in(ProgramActivityTypeCode.getNames())
                .findAll();

        if (!ListUtils.isEmpty(programActivityList)) {
            // Collect all miscs, group by program activity id
            Map<Long, List<ProgramActivityMisc>> programActivityIdMiscMap = programActivityMiscRepository.findBy()
                    .where("programActivityId").in(
                            programActivityList.stream() // Parse programActivityList to collect ID as a list
                                    .map(pa -> {return pa.getProgramActivityId();})
                                    .collect(Collectors.toList())
                    )
                    .findAll().stream()
                    .collect( // This is just collecting the results grouped by program activity id
                            Collectors.groupingBy(pam -> pam.getProgramActivityId(), Collectors.toList())
                    );

            // convert list of program activities and map of lists of miscs into DTOs
            for (ProgramActivity programActivity : programActivityList) {
                MilestoneDTO milestone = new MilestoneDTO();
                milestone.setType(programActivity.getProgramActivityName());
                milestone.setId(programActivity.getProgramActivityId());

                for (ProgramActivityMisc programActivityMisc : programActivityIdMiscMap.get(programActivity.getProgramActivityId())) {
                    if (VfName.REPEATING.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setRepeat(Boolean.valueOf(programActivityMisc.getMiscData()));
                    }
                    else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setYearsOfService(Integer.valueOf(programActivityMisc.getMiscData()));
                    }
                    else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setPublicHeadline(programActivityMisc.getMiscData());
                    }
                    else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setAwardAmount(Double.valueOf(programActivityMisc.getMiscData()));
                    }
                    else if (VfName.ECARD_ID.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setEcardId(Long.valueOf(programActivityMisc.getMiscData()));
                    }
                    else if (VfName.PRIVATE_MESSAGE.name().equalsIgnoreCase(programActivityMisc.getVfName())) {
                        milestone.setPrivateMessage(programActivityMisc.getMiscData());
                    }
                }
                resultList.add(milestone);
            }
        }
        return resultList;
    }

    @Override
    public List<ErrorMessage> validateMilestones(ProgramBasicDTO program) {
        List<ErrorMessage> errorList = new ArrayList<>();

        if (!ListUtils.isEmpty(program.getMilestones())) {
            program.getMilestones().forEach(milestone -> {
                // Type validation - required, must be valid type
                if (!ProgramActivityTypeCode.contains(milestone.getType())) {
                    errorList.add(MilestoneConstants.INVALID_MILESTONE_TYPE_ERROR);
                }

                // Years of service validation - required, must be non-negative (zero is okay)
                if (milestone.getYearsOfService() == null) {
                    errorList.add(MilestoneConstants.YEARS_OF_SERVICE_MISSING_ERROR);
                }
                else if (milestone.getYearsOfService() < 0) {
                    errorList.add(MilestoneConstants.YEARS_OF_SERVICE_NEGATIVE_ERROR);
                }

                // repeating validation - required
                if (milestone.isRepeat() == null) {
                    errorList.add(MilestoneConstants.REPEAT_MISSING_ERROR);
                }

                // public headline validation - required, can't be too long
                if (StringUtils.isEmpty(milestone.getPublicHeadline())) {
                    errorList.add(MilestoneConstants.PUBLIC_HEADLINE_MISSING_ERROR);
                }
                else if (milestone.getPublicHeadline().length() > NominationConstants.MAX_HEADLINE_CHAR_LENGTH) {
                    errorList.add(MilestoneConstants.PUBLIC_HEADLINE_TOO_LONG_ERROR);
                }

                // private message validation - optional, can't be too long
                if (!StringUtils.isEmpty(milestone.getPrivateMessage()) && milestone.getPrivateMessage().length() > NominationConstants.MAX_COMMENT_CHAR_LENGTH) {
                    errorList.add(MilestoneConstants.PRIVATE_MESSAGE_TOO_LONG_ERROR);
                }
            });

            // determine duplicates
            List<MilestoneDTO> valuesChecked = new ArrayList<>();
            program.getMilestones()
                    .forEach(element -> {
                        // I'd love to do this sort of validation with sets, but, since we don't currently implement a .equals
                        // or .hashCode method on our DTOs, each object is seen as unique
                        valuesChecked.forEach(record -> {
                            if (record.getType().equals(element.getType())
                                    && record.getYearsOfService().equals(element.getYearsOfService())
                                    && record.isRepeat().equals(element.isRepeat())
                                    ) {
                                if (Boolean.TRUE.equals(element.isRepeat())) {
                                    // For now, UI only supports sends one value for repeating years of service
                                    // In the future, we can reduce this to only the else case
                                    errorList.add(MilestoneConstants.MULTIPLE_REPEATING_NOT_SUPPORTED_ERROR);
                                }
                                else {
                                    errorList.add(MilestoneConstants.DUPLICATE_MILESTONE_ERROR);
                                }
                                return;
                            }
                        });
                        valuesChecked.add(element);
                    });
        }
        return errorList;
    }

    @Override
    @Transactional
    public void insertUpdateMilestones(ProgramBasicDTO program) {
        if (!ListUtils.isEmpty(program.getMilestones())) {
            // clean up old milestones
            deleteMilestoneByIdList(program.getProgramId(), programActivityRepository.findBy()
                    .where("programId").eq(program.getProgramId())
                    .and("programActivityName").in(ProgramActivityTypeCode.getNames())
                    .findAll("programActivityId", Long.class)
            );

            List<ProgramActivityMisc> programActivityMiscList = new ArrayList<>();
            List<Long> ecardIdList = new ArrayList<>();
            for (MilestoneDTO milestone : program.getMilestones()) {
                ProgramActivity programActivity = new ProgramActivity();
                programActivity.setProgramId(program.getProgramId());
                programActivity.setProgramActivityName(milestone.getType());
                programActivity.setFromDate(DateUtil.convertFromString(program.getFromDate()));
                programActivity.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
                programActivity.setProgramActivityTypeCode(MilestoneConstants.MILESTONE);
                programActivityRepository.save(programActivity);

                ProgramActivityMisc repeating = new ProgramActivityMisc();
                repeating.setProgramActivityId(programActivity.getProgramActivityId());
                repeating.setVfName(VfName.REPEATING.name());
                repeating.setMiscData(Boolean.toString(milestone.isRepeat()));
                repeating.setMiscDate(new Date());
                programActivityMiscList.add(repeating);

                ProgramActivityMisc yearsOfService = new ProgramActivityMisc();
                yearsOfService.setVfName(VfName.YEARS_OF_SERVICE.name());
                yearsOfService.setProgramActivityId(programActivity.getProgramActivityId());
                yearsOfService.setMiscData(Integer.toString(milestone.getYearsOfService()));
                yearsOfService.setMiscDate(new Date());
                programActivityMiscList.add(yearsOfService);

                ProgramActivityMisc publicHeadline = new ProgramActivityMisc();
                publicHeadline.setProgramActivityId(programActivity.getProgramActivityId());
                publicHeadline.setVfName(VfName.PUBLIC_HEADLINE.name());
                publicHeadline.setMiscData(milestone.getPublicHeadline());
                publicHeadline.setMiscDate(new Date());
                programActivityMiscList.add(publicHeadline);

                // optional fields
                if (milestone.getAwardAmount() != null) {
                    ProgramActivityMisc awardAmount = new ProgramActivityMisc();
                    awardAmount.setProgramActivityId(programActivity.getProgramActivityId());
                    awardAmount.setVfName(VfName.AWARD_AMOUNT.name());
                    awardAmount.setMiscData(Double.toString(milestone.getAwardAmount()));
                    awardAmount.setMiscDate(new Date());
                    programActivityMiscList.add(awardAmount);
                }

                if (milestone.getEcardId() != null) {
                    ProgramActivityMisc ecardId = new ProgramActivityMisc();
                    ecardId.setProgramActivityId(programActivity.getProgramActivityId());
                    ecardId.setVfName(VfName.ECARD_ID.name());
                    ecardId.setMiscData(Long.toString(milestone.getEcardId()));
                    ecardId.setMiscDate(new Date());
                    programActivityMiscList.add(ecardId);

                    // we will need to create PROGRAM_ECARD records to tie the selected ecard to the program
                    if (!ecardIdList.contains(milestone.getEcardId())) {
                        ecardIdList.add(milestone.getEcardId());
                    }
                }

                if (milestone.getPrivateMessage() != null) {
                    ProgramActivityMisc privateMessage = new ProgramActivityMisc();
                    privateMessage.setProgramActivityId(programActivity.getProgramActivityId());
                    privateMessage.setVfName(VfName.PRIVATE_MESSAGE.name());
                    privateMessage.setMiscData(milestone.getPrivateMessage());
                    privateMessage.setMiscDate(new Date());
                    programActivityMiscList.add(privateMessage);
                }
            }
            // save all miscs
            programActivityMiscRepository.save(programActivityMiscList);
        }
    }

    @Transactional
    @Override
    public void deleteMilestoneById(Long programId, Long milestoneId) {
        ProgramActivity programActivity = programActivityRepository.findOne(milestoneId);
        // Return 404 if either the milestone doesn't exist or it isn't tied to the program provided
        if (programActivity == null) {
            // no program
            throw new NotFoundException("milestoneId");
        }

        if (!programActivity.getProgramId().equals(programId)) {
            // milestone not tied to the program
            // Yes, this could be part of the above if statement, but I broke it out for branch coverage.
            throw new NotFoundException("milestoneId");
        }

        deleteMilestoneByIdList(programId, Arrays.asList(milestoneId));
    }

    /**
     * deletes milestones with the ids provided. Cleans up ecards tied to the program provided afterwards.
     *
     * @param programId - id of program to clean up ecards for
     * @param milestoneIdList - list of ids of milestones to delete
     */
    private void deleteMilestoneByIdList(Long programId, List<Long> milestoneIdList) {
        cnxtProgramActivityMiscRepository.deleteByProgramActivityIdIn(milestoneIdList);
        cnxtProgramActivityRepository.deleteByProgramActivityIdIn(milestoneIdList);
    }

    @Override
    @TriggeredTask(name = "findBirthdayMilestoneReminders")
    public void findUpcomingBirthdayMilestoneReminders() {
        List<Map<String, Object>> upcomingBirthdays = milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.BIRTHDAY_TYPE);

        logger.info("Started findBirthdayMilestoneReminders job.");
        if (!ListUtils.isEmpty(upcomingBirthdays)) {

            List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList = new ArrayList<PaxMilestoneReminderDTO>();

            for (Map<String, Object> upcomingBirthday : upcomingBirthdays) {
                MonthDay monthDay = getUpcomingMilestoneMonthDay(upcomingBirthday);
                List<Map<String, Object>> participants = milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, monthDay);

                if (!ListUtils.isEmpty(participants)) {

                    for (Map<String, Object> participant : participants) {
                        Long paxId = (Long) participant.get("paxId");
                        String milestoneDate = participant.get("milestoneDate").toString();
                        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(paxId, MilestoneConstants.BIRTHDAY_TYPE, null, monthDay);
                        addEnrolledMilestoneRemindersToList(enrolledPrograms, paxId, monthDay, null, paxMilestoneReminderDTOList, MilestoneConstants.BIRTHDAY_REMINDER_TYPE, milestoneDate);
                    }
                }
            }
            logger.info("Processing findBirthdayMilestoneReminders job. upcomingBirthdays info: {} ", String.valueOf(upcomingBirthdays));
            publishMilestoneReminderEvent(paxMilestoneReminderDTOList, MilestoneConstants.BIRTHDAY_REMINDER_TYPE);
        }
        logger.info("Finished findBirthdayMilestoneReminders job.");
    }

    @Override
    @TriggeredTask(name = "findServiceAnniversaryMilestoneReminders")
    public void findUpcomingServiceAnniversaryMilestoneReminders() {
        List<Map<String, Object>> upcomingServiceAnniversaries = milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);

        logger.info("Started findServiceAnniversaryMilestoneReminders job.");
        if (!ListUtils.isEmpty(upcomingServiceAnniversaries)) {

            List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList = new ArrayList<PaxMilestoneReminderDTO>();

            for (Map<String, Object> upcomingServiceAnniversary : upcomingServiceAnniversaries) {
                MonthDay monthDay = getUpcomingMilestoneMonthDay(upcomingServiceAnniversary);
                List<Map<String, Object>> participants =  milestoneDao.findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, monthDay);

                if (!ListUtils.isEmpty(participants)) {

                    for (Map<String, Object> participant : participants) {
                        Long paxId = (Long) participant.get("paxId");
                        Year hireYear = Year.of(LocalDate.parse(participant.get("milestoneDate").toString()).getYear());
                        String milestoneDate = participant.get("milestoneDate").toString();
                        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(paxId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, getServiceAnniversaryYear(participant), monthDay);
                        addEnrolledMilestoneRemindersToList(enrolledPrograms, paxId, monthDay, hireYear, paxMilestoneReminderDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE, milestoneDate);
                    }
                }
            }
            logger.info("Processing findServiceAnniversaryMilestoneReminders job. upcomingServiceAnniversaries info: {} ", String.valueOf(upcomingServiceAnniversaries));
            publishMilestoneReminderEvent(paxMilestoneReminderDTOList,MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE);
        }
        logger.info("Finished findServiceAnniversaryMilestoneReminders job.");
    }

    @Override
    @TriggeredTask(name = "findBirthdayMilestones")
    public void findBirthdayMilestones() {
        List<Map<String, Object>> participants = getParticipantsWithMilestonesToday(MilestoneConstants.BIRTHDAY_TYPE);

        logger.info("Started findBirthdayMilestones job.");
        if (!ListUtils.isEmpty(participants)) {
            List<PaxMilestoneDTO> paxMilestoneList = new ArrayList<PaxMilestoneDTO>();

            for (Map<String, Object> participant : participants) {
                Long paxId = (Long) participant.get("paxId");
                List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(paxId, MilestoneConstants.BIRTHDAY_TYPE, null, null);
                addEnrolledMilestonesToList(enrolledPrograms, paxId, paxMilestoneList, MilestoneConstants.BIRTHDAY_TYPE);
            }
            logger.info("Processing findBirthdayMilestones job. participants info: {} ", String.valueOf(participants));
            publishMilestoneEvent(paxMilestoneList, MilestoneConstants.BIRTHDAY_TYPE);
        }
        logger.info("Finished findBirthdayMilestones job.");
    }

    @Override
    @TriggeredTask(name = "findServiceAnniversaryMilestones")
    public void findServiceAnniversaryMilestones() {
        List<Map<String, Object>> participants = getParticipantsWithMilestonesToday(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);

        logger.info("Started findServiceAnniversaryMilestones job.");
        if (!ListUtils.isEmpty(participants)) {
            List<PaxMilestoneDTO> paxMilestoneDTOList = new ArrayList<PaxMilestoneDTO>();

            for (Map<String, Object> participant : participants) {
                Long paxId = (Long) participant.get("paxId");
                List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(paxId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, getServiceAnniversaryYear(participant), null);
                addEnrolledMilestonesToList(enrolledPrograms, paxId, paxMilestoneDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
            }
            logger.info("Processing findServiceAnniversaryMilestones job. participants info: {}", String.valueOf(participants));
            publishMilestoneEvent(paxMilestoneDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
        }
        logger.info("Finished findServiceAnniversaryMilestones job.");
    }

    private Map<String, Object> getProgramData(PaxMilestoneReminderDTO paxMilestoneReminderDTO) {
        Map<String, Object> programData;
        if (isServiceAnniversaryReminder(paxMilestoneReminderDTO.getEventType())) {
            programData = milestoneDao.getMilestoneProgramData(paxMilestoneReminderDTO.getPaxId(),
                    paxMilestoneReminderDTO.getProgramId(),
                    paxMilestoneReminderDTO.getEventType(),
                    paxMilestoneReminderDTO.getYearsOfService());
        } else { // is Birthday
            programData = milestoneDao.getMilestoneProgramData(paxMilestoneReminderDTO.getPaxId(), paxMilestoneReminderDTO.getProgramId(), MilestoneConstants.BIRTHDAY_REMINDER_TYPE, null);
        }

        if (programData == null || programData.isEmpty()) {
            throw new NotFoundException("programId");
        }

        return programData;
    }

    private boolean isServiceAnniversaryReminder(String eventType) {
        return eventType.equals(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE);
    }

    private Long getServiceAnniversaryYear(Map<String, Object> participant) {
        Year currentYear = Year.now();
        Year hireYear = Year.of(LocalDate.parse(participant.get("milestoneDate").toString()).getYear());
        Long anniversaryYear = ChronoUnit.YEARS.between(hireYear, currentYear);
        return anniversaryYear;
    }

    private void setProgramDataValuesOnMilestoneReminder(PaxMilestoneReminderDTO paxMilestoneReminderDTO) {
        Map<String, Object> programData = getProgramData(paxMilestoneReminderDTO);
        paxMilestoneReminderDTO.setPayoutType(programData.get("payoutType").toString());
        paxMilestoneReminderDTO.setProgramName(programData.get("programName").toString());

        setManagerIdOnMilestoneReminder(programData, paxMilestoneReminderDTO);
        setAwardAmountOnMilestoneReminder(programData, paxMilestoneReminderDTO);
    }

    private void setManagerIdOnMilestoneReminder (Map<String, Object> programData, PaxMilestoneReminderDTO paxMilestoneReminderDTO) {
        if(programData.get("directManager")!= null) {
            paxMilestoneReminderDTO.setManagerPaxId(Long.parseLong(programData.get("directManager").toString()));
        }
    }

    private void setAwardAmountOnMilestoneReminder(Map<String, Object> programData, PaxMilestoneReminderDTO paxMilestoneReminderDTO) {
        if (programData.get("awardAmount") != null) {
            paxMilestoneReminderDTO.setAwardAmount(Double.parseDouble(programData.get("awardAmount").toString()));
        }
    }

    private MonthDay getUpcomingMilestoneMonthDay(Map<String, Object> upcomingMilestone) {
        LocalDate milestone = LocalDate.parse(DateUtil.convertToUTCDate((Date) upcomingMilestone.get("upcomingMilestone")));
        MonthDay monthDay = MonthDay.of(milestone.getMonthValue(), milestone.getDayOfMonth());

        return monthDay;
    }

    private void publishMilestoneReminderEvent(List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList, String eventType) {
        if (!ListUtils.isEmpty(paxMilestoneReminderDTOList)) {
            applicationEventPublisher.publishEvent(new MilestoneReminderEvent(paxMilestoneReminderDTOList, eventType));
        }
    }

    private void publishMilestoneEvent(List<PaxMilestoneDTO> paxMilestoneDTOList, String eventType) {
        if (!ListUtils.isEmpty(paxMilestoneDTOList)) {
            applicationEventPublisher.publishEvent(new MilestoneEvent(paxMilestoneDTOList, eventType));
        }
    }

    private PaxMilestoneDTO createMilestoneDTOWithEnrolledProgramValues(Map<String, Object> enrolledProgram, Long paxId, String eventType) {
        Long yearsOfService = Long.parseLong(enrolledProgram.get("yearsOfService").toString());
        Long programActivityId = (Long) enrolledProgram.get("programActivityId");
        Long programId = (Long) enrolledProgram.get("programId");
        PaxMilestoneDTO paxMilestoneDTO = new PaxMilestoneDTO(paxId, eventType, yearsOfService, programActivityId, programId);
        return paxMilestoneDTO;
    }

    private PaxMilestoneReminderDTO createMilestoneReminderWithEnrolledProgramValues(Long paxId, MonthDay monthDay, Map<String, Object> enrolledProgram, Year hireYear, String eventType) {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(eventType, paxId, monthDay, Long.parseLong(enrolledProgram.get("yearsOfService").toString()),
                Long.parseLong(enrolledProgram.get("programId").toString()), hireYear, null, null, null, null, null);
        return paxMilestoneReminderDTO;
    }

    private void addEnrolledMilestoneRemindersToList(List<Map<String, Object>> enrolledPrograms, Long paxId, MonthDay monthDay, Year hireYear, List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList, String eventType, String milestoneDate) {
        if(!ListUtils.isEmpty(enrolledPrograms)) {
            for(Map<String, Object> enrolledProgram : enrolledPrograms) {
                PaxMilestoneReminderDTO paxMilestoneReminderDTO = createMilestoneReminderWithEnrolledProgramValues(paxId, monthDay, enrolledProgram, hireYear, eventType);
                paxMilestoneReminderDTO.setMilestoneDate(milestoneDate);
                setProgramDataValuesOnMilestoneReminder(paxMilestoneReminderDTO);
                paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);
            }
        }
    }

    private void addEnrolledMilestonesToList(List<Map<String, Object>> enrolledPrograms, Long paxId, List<PaxMilestoneDTO> paxMilestoneDTOList, String eventType) {
        if (!ListUtils.isEmpty(enrolledPrograms)) {
            for (Map<String, Object> enrolledProgram : enrolledPrograms) {
                PaxMilestoneDTO paxMilestoneDTO = createMilestoneDTOWithEnrolledProgramValues(enrolledProgram, paxId, eventType);
                paxMilestoneDTOList.add(paxMilestoneDTO);
            }
        }
    }
    private List<Map<String, Object>> getParticipantsWithMilestonesToday(String eventType) {
        MonthDay today = MonthDay.now();
        List<Map<String, Object>> participants = milestoneDao.findParticipantsWithMilestones(eventType, today);
        return participants;
    }

	@Override
	@TriggeredTask(name = "findServiceAnniversaryMilestonesErrors")
 	public void findAccessDeniedErrorsForServiceAnniversaryMilestones(){
 		
 		logger.info("Started triggered task findServiceAnniversaryMilestonesErrors.");	
 		
 		List<Batch> batchList = cnxtBatchRepository.findBatchInformationByStatusTypeCode(Long.valueOf(ProjectConstants.ZERO_STRING),
 				String.valueOf(BatchType.MILESTONE_SERVICE_ANNIVERSARY_EVENT));
 			try {	
 				List<Long> batchIds = new ArrayList<>();
 				for (Batch batch: batchList) {
 					batchIds.add(batch.getBatchId());
 				}
 				Long lastBatchId = Collections.max(batchIds);
 				long errorCount = cnxtBatchEventRepository.findBatchEventErrorCount(lastBatchId);
 				long serviceAnniversariesCount = cnxtBatchEventRepository.findMissingServiceAnniversariesCount(Long.valueOf(ProjectConstants.ZERO_STRING));
 				if (errorCount > 0 && errorCount == serviceAnniversariesCount ){
 	 				logger.info("Started to run the findServiceAnniversaryMilestones process due 'Access is denied' error. Total of errors: {}", errorCount);
 	 				findServiceAnniversaryMilestones();
 	 			}
 			}
 			catch (NoSuchElementException e) {
 				logger.info("No batch for MILESTONE_SERVICE_ANNIVERSARY_EVENT was found.");
 	        }
 			logger.info("Finished triggered task findServiceAnniversaryMilestonesErrors.");
     }
}
