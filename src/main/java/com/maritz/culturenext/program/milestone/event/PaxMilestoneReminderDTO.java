package com.maritz.culturenext.program.milestone.event;

import java.time.MonthDay;
import java.time.Year;

public class PaxMilestoneReminderDTO {
    private Long paxId;
    private MonthDay monthDay;
    private Long yearsOfService;
    private Long programId;
    private Year hireYear;
    private String programName;
    private Long managerPaxId;
    private String milestoneDate;
    private Double awardAmount;
    private String payoutType;
    private String eventType;

    public PaxMilestoneReminderDTO(String eventType, Long paxId, MonthDay monthDay, Long yearsOfService, Long programId, Year hireYear, String programName, Long managerPaxId, String milestoneDate, Double awardAmount, String payoutType) {

        this.paxId = paxId;
        this.monthDay = monthDay;
        this.yearsOfService = yearsOfService;
        this.programId = programId;
        this.hireYear = hireYear;
        this.programName = programName;
        this.managerPaxId = managerPaxId;
        this.milestoneDate = milestoneDate;
        this.awardAmount = awardAmount;
        this.payoutType = payoutType;
        this.eventType = eventType;
    }

    public Long getPaxId() {
        return paxId;
    }

    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }

    public MonthDay getMonthDay() {
        return monthDay;
    }

    public void setMonthDay(MonthDay monthDay) {
        this.monthDay = monthDay;
    }

    public Long getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(Long yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Year getHireYear() {
        return hireYear;
    }

    public void setHireYear(Year hireYear) {
        this.hireYear = hireYear;
    }

    public String getProgramName() { return programName; }

    public void setProgramName(String programName) { this.programName = programName; }

    public Long getManagerPaxId() { return managerPaxId; }

    public void setManagerPaxId(Long managerPaxId) {
        this.managerPaxId = managerPaxId;
    }

    public String getMilestoneDate() {
        return milestoneDate;
    }

    public void setMilestoneDate(String milestoneDate) {
        this.milestoneDate = milestoneDate;
    }

    public Double getAwardAmount() {
        return awardAmount;
    }

    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }

    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
