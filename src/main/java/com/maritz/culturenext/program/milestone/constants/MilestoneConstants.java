package com.maritz.culturenext.program.milestone.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.recognition.constants.NominationConstants;

public class MilestoneConstants {

    // fields
    public static final String MILESTONES_FIELD = "milestones";
    public static final String YEARS_OF_SERVICE_FIELD = "yearsOfService";
    public static final String REPEAT_FIELD = "repeat";
    public static final String PUBLIC_HEADLINE_FIELD = "publicHeadline";
    public static final String PRIVATE_MESSAGE_FIELD = "privateMessage";
    public static final String MILESTONE = "MILESTONE";

    // milestone event types
    public static final String SERVICE_ANNIVERSARY_TYPE = "SERVICE_ANNIVERSARY";
    public static final String BIRTHDAY_TYPE = "BIRTH_DAY";

    // milestone reminder types
    public static final String SERVICE_ANNIVERSARY_REMINDER_TYPE = "SERVICE_ANNIVERSARY_REMINDER";
    public static final String BIRTHDAY_REMINDER_TYPE = "BIRTHDAY_REMINDER";

    // milestone type errors
    public static final String INVALID_MILESTONE_TYPE_CODE = "INVALID_MILESTONE_TYPE";
    public static final String INVALID_MILESTONE_TYPE_MESSAGE = "Not a valid type of milestone.";
    
    // milestone years of service errors
    public static final String YEARS_OF_SERVICE_MISSING_CODE = "YEARS_OF_SERVICE_MISSING";
    public static final String YEARS_OF_SERVICE_MISSING_MESSAGE = "Years of service is required.";
    
    public static final String YEARS_OF_SERVICE_NEGATIVE_CODE = "YEARS_OF_SERVICE_NEGATIVE";
    public static final String YEARS_OF_SERVICE_NEGATIVE_MESSAGE = "Years of service cannot be negative.";
    
    // milestone public headline errors
    public static final String PUBLIC_HEADLINE_MISSING_CODE = "PUBLIC_HEADLINE_MISSING";
    public static final String PUBLIC_HEADLINE_MISSING_MESSAGE = "Public headline is required.";
    
    public static final String PUBLIC_HEADLINE_TOO_LONG_CODE = "PUBLIC_HEADLINE_TOO_LONG";
    public static final String PUBLIC_HEADLINE_TOO_LONG_MESSAGE = "Public headline is too long. Max length is " + NominationConstants.MAX_HEADLINE_CHAR_LENGTH;

    public static final String PRIVATE_MESSAGE_TOO_LONG_CODE = "PRIVATE_MESSAGE_TOO_LONG";
    public static final String PRIVATE_MESSAGE_TOO_LONG_MESSAGE = "Private message is too long. Max length is " + NominationConstants.MAX_COMMENT_CHAR_LENGTH;
    
    // milestone repeating errors
    public static final String REPEATING_MISSING_CODE = "REPEAT_MISSING";
    public static final String REPEATING_MISSING_MESSAGE = "Repeat is required.";
    
    // duplicate errors
    public static final String DUPLICATE_MILESTONE_CODE = "DUPLICATE_MILESTONE";
    public static final String DUPLICATE_MILESTONE_MESSAGE = "A milestone already exists for that type / repetition / years of service combination.";
    
    public static final String MULTIPLE_REPEATING_NOT_SUPPORTED_CODE = "MULTIPLE_REPEATING_NOT_SUPPORTED";
    public static final String MULTIPLE_REPEATING_NOT_SUPPORTED_MESSAGE = "Only one repeating milestone per program is allowed at this time.";

    // error objects
    
    // type error
    public static final ErrorMessage INVALID_MILESTONE_TYPE_ERROR = new ErrorMessage()
            .setField(ProjectConstants.TYPE)
            .setCode(INVALID_MILESTONE_TYPE_CODE)
            .setMessage(INVALID_MILESTONE_TYPE_MESSAGE);
    
    // years of service error
    public static final ErrorMessage YEARS_OF_SERVICE_MISSING_ERROR = new ErrorMessage()
            .setField(YEARS_OF_SERVICE_FIELD)
            .setCode(YEARS_OF_SERVICE_MISSING_CODE)
            .setMessage(YEARS_OF_SERVICE_MISSING_MESSAGE);

    public static final ErrorMessage YEARS_OF_SERVICE_NEGATIVE_ERROR = new ErrorMessage()
            .setField(YEARS_OF_SERVICE_FIELD)
            .setCode(YEARS_OF_SERVICE_NEGATIVE_CODE)
            .setMessage(YEARS_OF_SERVICE_NEGATIVE_MESSAGE);
    
    // public headline error
    public static final ErrorMessage PUBLIC_HEADLINE_MISSING_ERROR = new ErrorMessage()
            .setField(PUBLIC_HEADLINE_FIELD)
            .setCode(PUBLIC_HEADLINE_MISSING_CODE)
            .setMessage(PUBLIC_HEADLINE_MISSING_MESSAGE);
    
    public static final ErrorMessage PUBLIC_HEADLINE_TOO_LONG_ERROR = new ErrorMessage()
            .setField(PUBLIC_HEADLINE_FIELD)
            .setCode(PUBLIC_HEADLINE_TOO_LONG_CODE)
            .setMessage(PUBLIC_HEADLINE_TOO_LONG_MESSAGE);

    // private message error
    public static final ErrorMessage PRIVATE_MESSAGE_TOO_LONG_ERROR = new ErrorMessage()
            .setField(PRIVATE_MESSAGE_FIELD)
            .setCode(PRIVATE_MESSAGE_TOO_LONG_CODE)
            .setMessage(PRIVATE_MESSAGE_TOO_LONG_MESSAGE);
    
    // repeating error
    public static final ErrorMessage REPEAT_MISSING_ERROR = new ErrorMessage()
            .setField(REPEAT_FIELD)
            .setCode(REPEATING_MISSING_CODE)
            .setMessage(REPEATING_MISSING_MESSAGE);
    
    // duplicate error
    public static final ErrorMessage DUPLICATE_MILESTONE_ERROR = new ErrorMessage()
            .setField(MILESTONES_FIELD)
            .setCode(DUPLICATE_MILESTONE_CODE)
            .setMessage(DUPLICATE_MILESTONE_MESSAGE);
    
    public static final ErrorMessage MULTIPLE_REPEATING_NOT_SUPPORTED_ERROR = new ErrorMessage()
            .setField(MILESTONES_FIELD)
            .setCode(MULTIPLE_REPEATING_NOT_SUPPORTED_CODE)
            .setMessage(MULTIPLE_REPEATING_NOT_SUPPORTED_MESSAGE);
}
