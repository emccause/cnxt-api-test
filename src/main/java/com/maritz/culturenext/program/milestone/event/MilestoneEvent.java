package com.maritz.culturenext.program.milestone.event;

import java.util.List;

import org.springframework.context.ApplicationEvent;

public class MilestoneEvent extends ApplicationEvent {
    
  private String milestoneType;
  
  public MilestoneEvent(List<PaxMilestoneDTO> paxMilestoneList, String milestoneType) {
    super(paxMilestoneList);
    this.milestoneType = milestoneType;
  }

  @SuppressWarnings("unchecked")
  public List<PaxMilestoneDTO> getPaxMilestoneList() {
    return (List<PaxMilestoneDTO>) this.getSource();
  }
  
  public String getMilestoneType() {
    return milestoneType;
  }

  public void setMilestoneType(String milestoneType) {
    this.milestoneType = milestoneType;
  }

}
