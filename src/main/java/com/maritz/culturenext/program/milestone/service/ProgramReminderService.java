package com.maritz.culturenext.program.milestone.service;

public interface ProgramReminderService {

    void sendPendingApprovalRecognitionEmail();
}
