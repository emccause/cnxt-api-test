package com.maritz.culturenext.program.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dao.EligibilityDao;

@Repository
public class EligibilityDaoImpl extends AbstractDaoImpl implements EligibilityDao {
    private static final Logger logger = LoggerFactory.getLogger(EligibilityDaoImpl.class);

    private static final String PAX_SEARCH_TYPE = "P";
    private static final String GROUPS_SEARCH_TYPE = "G";
    private static final String PAX_TYPE = "PAX";
    private static final String GROUP_TYPE = "GROUPS";

    //Result Set Columns (mapped to camelCase)
      private static final String RS_SEARCH_TYPE = "searchType";
      
    // query parameters
    private static final String PAX_IDS_PARAM = "PAX_IDS";
    private static final String GROUPS_IDS_PARAM = "GROUPS_IDS";
    private static final String MEMBER_LIST_PLACEHOLDER = "{MEMBER_LIST_PLACEHOLDER}";

    // query components
    private static final String OR_CONNECTOR = " or ";
    private static final String AND_CONNECTOR = " and ";
    private static final String PAX_IDS_WHERE_CLAUSE = "pax_id in ( :"+PAX_IDS_PARAM+" ) ";
    private static final String GROUP_IDS_WHERE_CLAUSE = "group_id in ( :"+GROUPS_IDS_PARAM+" ) ";
    private static final String VALID_GROUP_TYPE_DESC_LIST = "VALID_GROUP_TYPE_DESC_LIST";
    private static final String VALID_GROUPS_PLACEHOLDER =
            "WHEN (ELIGIBILITY_ENTITIES.TYPE = '"+GROUP_TYPE+"' " +
                    "AND G.GROUP_ID IS NOT NULL " +
                    "AND GT.GROUP_TYPE_DESC NOT IN (:" + VALID_GROUP_TYPE_DESC_LIST + ") ) " +
            "THEN 'INVALID_GROUP_TYPE' ";

    // queries
    private static final String ENTITY_DTO_QUERY = "SELECT * FROM component.VW_USER_GROUPS_SEARCH_DATA WHERE STATUS_TYPE_CODE <> 'INACTIVE'";
    private static final String ENTITY_DTO_AND_STATUS_QUERY_FIRST_HALF = 
            "SELECT DISTINCT ELIGIBILITY_ENTITIES.ID AS INPUT_ID, " +
            "ELIGIBILITY_ENTITIES.TYPE AS INPUT_TYPE, " +
            "USER_GROUPS.*,  " +
            "CASE  " +
            "WHEN " +
            "(ELIGIBILITY_ENTITIES.TYPE = '"+PAX_TYPE+"' AND P.PAX_ID IS NULL)  " +
            "OR (ELIGIBILITY_ENTITIES.TYPE = '"+GROUP_TYPE+"' AND G.GROUP_ID IS NULL)  " +
            "THEN 'DOES_NOT_EXIST' ";
    private static final String ENTITY_DTO_AND_STATUS_QUERY_SECOND_HALF =
            "END AS STATUS " +
            "FROM ( " +
            MEMBER_LIST_PLACEHOLDER +
            ") AS ELIGIBILITY_ENTITIES " +
            "LEFT JOIN component.PAX P " +
            "ON P.PAX_ID = ELIGIBILITY_ENTITIES.ID " +
            "AND ELIGIBILITY_ENTITIES.TYPE = '"+PAX_TYPE+"' " +
            "LEFT JOIN component.GROUPS G " +
            "ON G.GROUP_ID = ELIGIBILITY_ENTITIES.ID " +
            "AND ELIGIBILITY_ENTITIES.TYPE = '"+GROUP_TYPE+"' " +
            "LEFT JOIN component.GROUP_TYPE GT " +
            "ON G.GROUP_TYPE_CODE = GT.GROUP_TYPE_CODE " +
            "LEFT JOIN component.VW_USER_GROUPS_SEARCH_DATA USER_GROUPS " +
                "ON (USER_GROUPS.SEARCH_TYPE = 'P' AND ELIGIBILITY_ENTITIES.TYPE = '"+PAX_TYPE+
                    "' AND USER_GROUPS.PAX_ID = ELIGIBILITY_ENTITIES.ID) " +
                "OR (USER_GROUPS.SEARCH_TYPE = 'G' AND ELIGIBILITY_ENTITIES.TYPE = '"+GROUP_TYPE+
                    "' AND USER_GROUPS.GROUP_ID = ELIGIBILITY_ENTITIES.ID) " +
            "ORDER BY USER_GROUPS.SEARCH_TYPE DESC";


    @Override
    @Nonnull
    public List<EntityDTO> getEligibilityEntities(@Nonnull List<Long> paxIds, @Nonnull List<Long> groupIds) {
        if (paxIds.isEmpty() && groupIds.isEmpty()) {
            // if no ids to search for, then return empty set
            return new ArrayList<EntityDTO>();
        }

        // prepare the query
        MapSqlParameterSource params = new MapSqlParameterSource();

        String query = ENTITY_DTO_QUERY + AND_CONNECTOR;

        if (!paxIds.isEmpty()) {
            query += PAX_IDS_WHERE_CLAUSE;
            params.addValue(PAX_IDS_PARAM, paxIds);

            if (!groupIds.isEmpty()) {
                query += OR_CONNECTOR;
            }
        }

        if (!groupIds.isEmpty()) {
            query += GROUP_IDS_WHERE_CLAUSE;
            params.addValue(GROUPS_IDS_PARAM, groupIds);
        }


        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params,
                new CamelCaseMapRowMapper());

        List<EntityDTO> resultsList = new ArrayList<EntityDTO>();

        // iterate through the results (given as a list of maps and translate into the respective entities)
        for (Map<String, Object> node: nodes) {
            if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase(PAX_SEARCH_TYPE)) {
                resultsList.add(new EmployeeDTO(node, null));
            } else if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase(GROUPS_SEARCH_TYPE)) {
                resultsList.add(new GroupDTO(node));
            } else {
                // log no matching and move on
                logger.warn("No matching search type found for node " + node);
            }
        }
        return resultsList;
    }

    @Nonnull
    @Override
    public List<Map<String, Object>> getEligibilityEntitiesAndStatus(@Nonnull List<Long> paxIds,
            @Nonnull List<Long> groupIds) {
        // utilize the other getEligibilityEntitiesAndStatus method but with empty groups
        return getEligibilityEntitiesAndStatus(paxIds, groupIds, new ArrayList<String>());
    }

    @Override
    @Nonnull
    public List<Map<String, Object>> getEligibilityEntitiesAndStatus(@Nonnull List<Long> paxIds, 
            @Nonnull List<Long> groupIds, @Nonnull List<String> validGroupTypeNames) {
        if(paxIds.isEmpty() && groupIds.isEmpty()) {
            // if no ids to search for, then return empty set
            return new ArrayList<Map<String, Object>>();
        }

        // set up parameters within query
        MapSqlParameterSource params = new MapSqlParameterSource();
        String query = ENTITY_DTO_AND_STATUS_QUERY_FIRST_HALF;

        // account for valid groups
        if (!validGroupTypeNames.isEmpty()) {
            query += VALID_GROUPS_PLACEHOLDER;
            params.addValue(VALID_GROUP_TYPE_DESC_LIST, validGroupTypeNames);
        } else {
            logger.warn("No groups specified as valid. Running query without accounting for group validity...");
        }

        query += ENTITY_DTO_AND_STATUS_QUERY_SECOND_HALF;

        boolean unionPlaceholder = false;
        StringBuilder memberBuilder = new StringBuilder();

        // handle for paxIds and groupIds
        for (Long paxId : paxIds) {
            if(unionPlaceholder) {
                memberBuilder.append("UNION SELECT " + paxId + ", '" + PAX_TYPE + "' ");
            } else {
                memberBuilder.append("SELECT " + paxId + " AS ID, '" + PAX_TYPE + "' AS TYPE ");
                unionPlaceholder = true;
            }
        }

        for (Long groupId : groupIds) {
            if (unionPlaceholder) {
                memberBuilder.append("UNION SELECT " + groupId + ", '" + GROUP_TYPE + "' ");
            } else {
                memberBuilder.append("SELECT " + groupId + " AS ID, '" + GROUP_TYPE + "' AS TYPE ");
                unionPlaceholder = true;
            }
        }

        query = query.replace(MEMBER_LIST_PLACEHOLDER, memberBuilder.toString());

        // query and return results

        return namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());

    }
}
