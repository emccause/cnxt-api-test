package com.maritz.culturenext.program.dao;

import java.time.MonthDay;
import java.util.List;
import java.util.Map;

public interface MilestoneDao {
  /**
   * retrieves milestone program data associated with a program that pax is enrolled in
   * @param paxId - pax enrolled in program (included to validate that the pax is actually enrolled in the program)
   * @param programId - program to retrieve
   * @return
   */
  Map<String, Object> getMilestoneProgramData(Long paxId, Long programId, String milestoneEventType, Long yearsOfService);

  /**
   * given a date for a participant with a milestone of the specified type
   * @param milestoneEventType - service anniversary or birthday
   * @param monthDay - milestone date to look for
   * @return
   */
  List<Map<String, Object>> findParticipantsWithMilestones(String milestoneEventType, MonthDay monthDay);

  /**
   * retrieves a list of unique dates with upcoming milestones that have notifications that should be sent today
   * @param milestoneEventType - service anniversary or birthday
   * @return
   */
  List<Map<String, Object>> findUpcomingMilestoneDates(String milestoneEventType);

  /**
   * finds any programs a pax is enrolled in
   * @param paxId - participant to search for program eligibility
   * @param milestoneEventType - service anniversary or birthday
   * @param anniversaryYear - in the case of a service anniversary milestone, the year the pax was hired
   * @param monthDay - the monthDay of the event
   * @return
   */
  List<Map<String, Object>> getEnrolledProgramsForPax(Long paxId, String milestoneEventType, Long anniversaryYear, MonthDay monthDay);
}
