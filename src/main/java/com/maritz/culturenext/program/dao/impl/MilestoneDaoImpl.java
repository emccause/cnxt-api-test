package com.maritz.culturenext.program.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.program.dao.MilestoneDao;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.time.MonthDay;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static com.maritz.culturenext.constants.ProjectConstants.PROGRAM_ID_SQL_PARAM;

@Repository
public class MilestoneDaoImpl extends AbstractDaoImpl implements MilestoneDao {
  public static final String PAX_ID_SQL_PARAM = "PAX_ID_SQL_PARAM";
  public static final String ANNIVERSARY_YEAR_SQL_PARAM = "ANNIVERSARY_YEAR_SQL_PARAM";
  public static final String DATE_SQL_PARAM = "DATE_SQL_PARAM";
  public static final String PROGRAM_ACTIVITY_NAME_SQL_PARAM = "PROGRAM_ACTIVITY_NAME_SQL_PARAM";
  public static final String BIRTHDAY_PROGRAM_ACTIVITY_NAME = "BIRTHDAY";
  public static final String SERVICE_ANNIVERSARY_PROGRAM_ACTIVITY_NAME = "SERVICE_ANNIVERSARY";
  public static final String VF_NAME_SQL_PARAM = "VF_NAME_SQL_PARAM";
  public static final String BIRTH_DAY_VF_NAME = "BIRTH_DAY";
  public static final String HIRE_DATE_VF_NAME = "HIRE_DATE";

  public static final String GET_MILESTONE_PROGRAM_DATA =
          "p.PROGRAM_ID, " +
                  "p.[PROGRAM_NAME], " +
                  "pb.BUDGET_ID, " +
                  "rel.PAX_ID_2 AS DIRECT_MANAGER, " +
                  "CASE WHEN pm_sat.MISC_DATA IS NOT NULL THEN pm_sat.MISC_DATA END AS PAYOUT_TYPE, " +
                  "CASE WHEN pam_rep.MISC_DATA IS NOT NULL THEN pam_rep.MISC_DATA END AS REPEATING, " +
                  "CASE WHEN pam_aa.MISC_DATA IS NOT NULL THEN pam_aa.MISC_DATA END AS AWARD_AMOUNT, " +
                  "CASE WHEN pam_yrs.MISC_DATA IS NOT NULL THEN pam_yrs.MISC_DATA END AS YEARS_OF_SERVICE, " +
                  "CASE WHEN pam_ec.MISC_DATA IS NOT NULL THEN pam_ec.MISC_DATA END AS ECARD_ID, " +
                  "CASE WHEN pam_ph.MISC_DATA IS NOT NULL THEN pam_ph.MISC_DATA END AS PUBLIC_HEADLINE, " +
                  "CASE WHEN pam_pm.MISC_DATA IS NOT NULL THEN pam_pm.MISC_DATA END AS PRIVATE_MESSAGE " +
                  "FROM component.PROGRAM p WITH (NOLOCK) " +
                  "INNER JOIN component.PROGRAM_ACTIVITY pa WITH (NOLOCK) ON p.PROGRAM_ID = pa.PROGRAM_ID AND pa.PROGRAM_ACTIVITY_NAME IN ('SERVICE_ANNIVERSARY', 'BIRTHDAY') " +
                  "AND pa.PROGRAM_ACTIVITY_TYPE_CODE = 'MILESTONE' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_rep WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_rep.PROGRAM_ACTIVITY_ID AND pam_rep.VF_NAME = 'REPEATING' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_aa WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_aa.PROGRAM_ACTIVITY_ID AND pam_aa.VF_NAME = 'AWARD_AMOUNT' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_yrs WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_yrs.PROGRAM_ACTIVITY_ID AND pam_yrs.VF_NAME = 'YEARS_OF_SERVICE' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_ec WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_ec.PROGRAM_ACTIVITY_ID AND pam_ec.VF_NAME = 'ECARD_ID' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_ph WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_ph.PROGRAM_ACTIVITY_ID AND pam_ph.VF_NAME = 'PUBLIC_HEADLINE' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_pm WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_pm.PROGRAM_ACTIVITY_ID AND pam_pm.VF_NAME = 'PRIVATE_MESSAGE' " +
                  "LEFT JOIN component.PROGRAM_MISC pm_sat WITH (NOLOCK) ON p.PROGRAM_ID = pm_sat.PROGRAM_ID AND pm_sat.VF_NAME = 'SELECTED_AWARD_TYPE' " +
                  "LEFT JOIN component.PROGRAM_MISC pm_nv WITH (NOLOCK) ON p.PROGRAM_ID = pm_nv.PROGRAM_ID AND pm_nv.VF_NAME = 'NEWSFEED_VISIBILITY' " +
                  "LEFT JOIN component.PROGRAM_BUDGET pb WITH (NOLOCK) ON pb.PROGRAM_ID = p.PROGRAM_ID " +
                  "LEFT JOIN component.RELATIONSHIP rel WITH (NOLOCK) ON rel.PAX_ID_1 = :" + PAX_ID_SQL_PARAM + " AND rel.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
                  "WHERE p.PROGRAM_TYPE_CODE = 'MILESTONE' " +
                  "AND p.PROGRAM_ID = :" + PROGRAM_ID_SQL_PARAM + " ";

  public static final String GET_BIRTHDAY_MILESTONE_PROGRAM_DATA_PREFIX =
          "SELECT TOP 1 ";

  public static final String GET_SERVICE_ANNIVERSARY_MILESTONE_PROGRAM_DATA_PREFIX_FRAGMENT =
          "WITH RANKED_PROGRAMS AS " +
                  "( " +
                  "SELECT ";

  public static final String GET_SERVICE_ANNIVERSARY_MILESTONE_PROGRAM_DATA_SUFFIX_FRAGMENT =
          " ) " +
                  "SELECT TOP 1 * FROM RANKED_PROGRAMS " +
                  "WHERE (REPEATING = 'FALSE' AND YEARS_OF_SERVICE = :" + ANNIVERSARY_YEAR_SQL_PARAM + ") " +
                  // modulo check in case we add repeating service anniversaries at intervals > 1 year.
                  "OR (REPEATING = 'TRUE' AND :" + ANNIVERSARY_YEAR_SQL_PARAM + " % YEARS_OF_SERVICE = 0) " +
                  "ORDER BY YEARS_OF_SERVICE DESC, REPEATING ";

  public static final String FIND_PARTICIPANTS_WITH_MILESTONES =
          "SELECT PAX_ID, MISC_DATA AS MILESTONE_DATE" +
                  " FROM component.PAX_MISC  WITH (NOLOCK) " +
                  " WHERE VF_NAME = :" + VF_NAME_SQL_PARAM +
                  " AND MISC_DATA LIKE :" + DATE_SQL_PARAM;

  public static final String GET_UPCOMING_MILESTONE_DATES_QUERY =
          "SELECT " +
                  "DISTINCT(DATEADD(DAY, CONVERT(INT, pm.MISC_DATA), GETDATE())) AS UPCOMING_MILESTONE " +
                  "FROM component.PROGRAM_MISC pm WITH (NOLOCK) " +
                  "INNER JOIN component.PROGRAM_ACTIVITY pa WITH (NOLOCK) ON pm.PROGRAM_ID = pa.PROGRAM_ID " +
                  "INNER JOIN component.PROGRAM p WITH (NOLOCK) ON p.PROGRAM_ID = pm.PROGRAM_ID " +
                  "WHERE pm.VF_NAME = 'MILESTONE_REMINDER' " +
                  "AND pa.PROGRAM_ACTIVITY_TYPE_CODE = 'MILESTONE' " +
                  "AND p.FROM_DATE < GETDATE() " +
                  "AND (p.THRU_DATE > GETDATE() OR p.THRU_DATE IS NULL) " +
                  "AND pa.PROGRAM_ACTIVITY_NAME = :" + PROGRAM_ACTIVITY_NAME_SQL_PARAM;

  public static final String GET_ENROLLED_PROGRAMS_FOR_PAX =
          "WITH RANKED_PROGRAMS AS " +
                  "( " +
                  "SELECT " +
                  "p.PROGRAM_ID, " +
                  "pa.PROGRAM_ACTIVITY_ID, " +
                  "CASE WHEN pam_yrs.MISC_DATA IS NOT NULL THEN pam_yrs.MISC_DATA END AS YEARS_OF_SERVICE, " +
                  "CASE WHEN pam_rep.MISC_DATA IS NOT NULL THEN pam_rep.MISC_DATA END AS REPEATING, " +
                  "ROW_NUMBER() OVER (PARTITION BY p.PROGRAM_ID ORDER BY pam_yrs.MISC_DATA DESC, pam_rep.MISC_DATA ASC) AS PROGRAM_RANK " +
                  "FROM component.PROGRAM p WITH (NOLOCK) " +
                  "INNER JOIN component.ACL acl WITH (NOLOCK) ON acl.TARGET_ID = p.PROGRAM_ID AND TARGET_TABLE = 'PROGRAM' " +
                  "INNER JOIN component.SYS_USER su WITH (NOLOCK) ON su.PAX_ID = :" + PAX_ID_SQL_PARAM + " AND su.STATUS_TYPE_CODE NOT IN ('INACTIVE', 'RESTRICTED') " +
                  "INNER JOIN component.PROGRAM_ACTIVITY pa WITH (NOLOCK) ON p.PROGRAM_ID = pa.PROGRAM_ID AND pa.PROGRAM_ACTIVITY_NAME IN ('SERVICE_ANNIVERSARY', 'BIRTHDAY') " +
                  "AND pa.PROGRAM_ACTIVITY_TYPE_CODE = 'MILESTONE' " +
                  "INNER JOIN component.[ROLE] r WITH (NOLOCK) ON r.ROLE_ID = acl.ROLE_ID AND acl.TARGET_TABLE = 'PROGRAM' AND r.ROLE_CODE = 'PREC' " +
                  "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP gtm WITH (NOLOCK) ON gtm.group_id = acl.SUBJECT_ID AND acl.SUBJECT_TABLE = 'GROUPS' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_rep WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_rep.PROGRAM_ACTIVITY_ID AND pam_rep.VF_NAME = 'REPEATING' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_yrs WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_yrs.PROGRAM_ACTIVITY_ID AND pam_yrs.VF_NAME = 'YEARS_OF_SERVICE' " +
                  "WHERE p.FROM_DATE < GETDATE() " +
                  "AND (p.THRU_DATE > GETDATE() OR p.THRU_DATE IS NULL) " +
                  "AND ((acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = :" + PAX_ID_SQL_PARAM + ") " +
                  "OR gtm.PAX_ID = :" + PAX_ID_SQL_PARAM + ") " +
                  "AND pa.PROGRAM_ACTIVITY_NAME = :" + PROGRAM_ACTIVITY_NAME_SQL_PARAM + " ";

  public static final String GET_ENROLLED_PROGRAMS_FOR_PAX_REMINDER =
          "WITH RANKED_PROGRAMS AS " +
                  "( " +
                  "SELECT " +
                  "p.PROGRAM_ID, " +
                  "pa.PROGRAM_ACTIVITY_ID, " +
                  "CASE WHEN pam_yrs.MISC_DATA IS NOT NULL THEN pam_yrs.MISC_DATA END AS YEARS_OF_SERVICE, " +
                  "CASE WHEN pam_rep.MISC_DATA IS NOT NULL THEN pam_rep.MISC_DATA END AS REPEATING, " +
                  "ROW_NUMBER() OVER (PARTITION BY p.PROGRAM_ID ORDER BY pam_yrs.MISC_DATA DESC, pam_rep.MISC_DATA ASC) AS PROGRAM_RANK " +
                  "FROM component.PROGRAM p WITH (NOLOCK) " +
                  "INNER JOIN component.ACL acl WITH (NOLOCK) ON acl.TARGET_ID = p.PROGRAM_ID AND TARGET_TABLE = 'PROGRAM' " +
                  "INNER JOIN component.SYS_USER su WITH (NOLOCK) ON su.PAX_ID = :" + PAX_ID_SQL_PARAM + " AND su.STATUS_TYPE_CODE NOT IN ('INACTIVE', 'RESTRICTED') " +
                  "INNER JOIN component.PROGRAM_ACTIVITY pa WITH (NOLOCK) ON p.PROGRAM_ID = pa.PROGRAM_ID AND pa.PROGRAM_ACTIVITY_NAME IN ('SERVICE_ANNIVERSARY', 'BIRTHDAY') " +
                  "AND pa.PROGRAM_ACTIVITY_TYPE_CODE = 'MILESTONE' " +
                  "INNER JOIN component.[ROLE] r WITH (NOLOCK) ON r.ROLE_ID = acl.ROLE_ID AND acl.TARGET_TABLE = 'PROGRAM' AND r.ROLE_CODE = 'PREC' " +
                  "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP gtm WITH (NOLOCK) ON gtm.group_id = acl.SUBJECT_ID AND acl.SUBJECT_TABLE = 'GROUPS' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_rep WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_rep.PROGRAM_ACTIVITY_ID AND pam_rep.VF_NAME = 'REPEATING' " +
                  "LEFT JOIN component.PROGRAM_ACTIVITY_MISC pam_yrs WITH (NOLOCK) ON pa.PROGRAM_ACTIVITY_ID = pam_yrs.PROGRAM_ACTIVITY_ID AND pam_yrs.VF_NAME = 'YEARS_OF_SERVICE' " +
                  "INNER JOIN component.PROGRAM_MISC pm WITH (NOLOCK) ON pm.PROGRAM_ID = p.PROGRAM_ID AND pm.VF_NAME = 'MILESTONE_REMINDER'" +
                  " AND pm.MISC_DATA = DATEDIFF(d, GETDATE(),  convert(datetime, :" + DATE_SQL_PARAM + ")) " +
                  "WHERE p.FROM_DATE < GETDATE() " +
                  "AND (p.THRU_DATE > GETDATE() OR p.THRU_DATE IS NULL) " +
                  "AND ((acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = :" + PAX_ID_SQL_PARAM + ") " +
                  "OR gtm.PAX_ID = :" + PAX_ID_SQL_PARAM + ") " +
                  "AND pa.PROGRAM_ACTIVITY_NAME = :" + PROGRAM_ACTIVITY_NAME_SQL_PARAM + " ";

  public static final String GET_ENROLLED_PROGRAMS_BIRTHDAY_FRAGMENT =
          ") " +
                  "SELECT * FROM RANKED_PROGRAMS " +
                  "WHERE PROGRAM_RANK = 1";

  public static final String GET_ENROLLED_PROGRAMS_SERVICE_ANNIVERSARY_FRAGMENT =
          "AND ((pam_rep.MISC_DATA = 'true' AND :" + ANNIVERSARY_YEAR_SQL_PARAM + " % pam_yrs.MISC_DATA = 0) " +
                  "OR (pam_rep.MISC_DATA = 'false' AND pam_yrs.MISC_DATA = :" + ANNIVERSARY_YEAR_SQL_PARAM + " )) " +
                  ") " +
                  "SELECT * FROM RANKED_PROGRAMS " +
                  "WHERE PROGRAM_RANK = 1";

  @Override
  public List<Map<String, Object>> getEnrolledProgramsForPax(Long paxId, String milestoneEventType, Long anniversaryYear, MonthDay monthDay) {
    String query, dateString = null;

    if (monthDay != null) {
      query = milestoneEventType.equals(MilestoneConstants.BIRTHDAY_TYPE) ?
              GET_ENROLLED_PROGRAMS_FOR_PAX_REMINDER + GET_ENROLLED_PROGRAMS_BIRTHDAY_FRAGMENT :
              GET_ENROLLED_PROGRAMS_FOR_PAX_REMINDER + GET_ENROLLED_PROGRAMS_SERVICE_ANNIVERSARY_FRAGMENT;
      //
    } else {
      query = milestoneEventType.equals(MilestoneConstants.BIRTHDAY_TYPE) ?
              GET_ENROLLED_PROGRAMS_FOR_PAX + GET_ENROLLED_PROGRAMS_BIRTHDAY_FRAGMENT :
              GET_ENROLLED_PROGRAMS_FOR_PAX + GET_ENROLLED_PROGRAMS_SERVICE_ANNIVERSARY_FRAGMENT;
    }

    String programActivityName = milestoneEventType.equals(MilestoneConstants.BIRTHDAY_TYPE) ? BIRTHDAY_PROGRAM_ACTIVITY_NAME :
            SERVICE_ANNIVERSARY_PROGRAM_ACTIVITY_NAME;

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(PROGRAM_ACTIVITY_NAME_SQL_PARAM, programActivityName);
    params.addValue(PAX_ID_SQL_PARAM, paxId);
    if (monthDay != null) {
      int year = Calendar.getInstance().get(Calendar.YEAR);

      if (Calendar.getInstance().get(Calendar.MONTH) > monthDay.getMonthValue()) {
        year++;
      }
      dateString = String.valueOf(year) + getDateString(monthDay);
      params.addValue(DATE_SQL_PARAM, dateString);
    }
    if (milestoneEventType.equals(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE)) {
      params.addValue(ANNIVERSARY_YEAR_SQL_PARAM, anniversaryYear);
    }

    return namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());
  }

  @Override
  public Map<String, Object> getMilestoneProgramData(Long paxId, Long programId, String milestoneEventType, Long yearsOfService) {
    String query;

    MapSqlParameterSource params = new MapSqlParameterSource();

    params.addValue(PAX_ID_SQL_PARAM, paxId);
    params.addValue(PROGRAM_ID_SQL_PARAM, programId);

    if (milestoneEventType.equals(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE) ||
            milestoneEventType.equals(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE)) {
      query = GET_SERVICE_ANNIVERSARY_MILESTONE_PROGRAM_DATA_PREFIX_FRAGMENT + GET_MILESTONE_PROGRAM_DATA + GET_SERVICE_ANNIVERSARY_MILESTONE_PROGRAM_DATA_SUFFIX_FRAGMENT;
      params.addValue(ANNIVERSARY_YEAR_SQL_PARAM, yearsOfService);
    }
    else {
      query = GET_BIRTHDAY_MILESTONE_PROGRAM_DATA_PREFIX + GET_MILESTONE_PROGRAM_DATA;
    }

    return namedParameterJdbcTemplate.queryForObject(query, params, new CamelCaseMapRowMapper());
  }

  @Override
  public List<Map<String, Object>> findParticipantsWithMilestones(String milestoneEventType, MonthDay monthDay) {
    String vfName = milestoneEventType.equals(MilestoneConstants.BIRTHDAY_TYPE) ?
            BIRTH_DAY_VF_NAME : HIRE_DATE_VF_NAME;

    String monthString = monthDay.getMonthValue() <= 9 ? "0" + String.valueOf(monthDay.getMonthValue()) : String.valueOf(monthDay.getMonthValue());
    String dayString = monthDay.getDayOfMonth() <= 9 ? "0" + String.valueOf(monthDay.getDayOfMonth()) : String.valueOf(monthDay.getDayOfMonth());
    String dateString = monthString + "-" + dayString;

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(DATE_SQL_PARAM, "%" + dateString);
    params.addValue(VF_NAME_SQL_PARAM, vfName);

    return namedParameterJdbcTemplate.query(FIND_PARTICIPANTS_WITH_MILESTONES, params, new CamelCaseMapRowMapper());
  }

  @Override
  public List<Map<String, Object>> findUpcomingMilestoneDates(String milestoneEventType) {
    String programActivityName = milestoneEventType.equals(MilestoneConstants.BIRTHDAY_TYPE) ?
            BIRTHDAY_PROGRAM_ACTIVITY_NAME : SERVICE_ANNIVERSARY_PROGRAM_ACTIVITY_NAME;

    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(PROGRAM_ACTIVITY_NAME_SQL_PARAM, programActivityName);

    return namedParameterJdbcTemplate.query(GET_UPCOMING_MILESTONE_DATES_QUERY, params, new CamelCaseMapRowMapper());
  }

  private String getDateString (MonthDay monthDay) {
    String monthString = monthDay.getMonthValue() <= 9 ? "0" + String.valueOf(monthDay.getMonthValue()) : String.valueOf(monthDay.getMonthValue());
    String dayString = monthDay.getDayOfMonth() <= 9 ? "0" + String.valueOf(monthDay.getDayOfMonth()) : String.valueOf(monthDay.getDayOfMonth());
    return "-" + monthString + "-" + dayString;
  }
}