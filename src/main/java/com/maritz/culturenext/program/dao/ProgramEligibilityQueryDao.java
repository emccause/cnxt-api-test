package com.maritz.culturenext.program.dao;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.program.dto.ProgramSummaryDTO;

public interface ProgramEligibilityQueryDao {
    
    List<ProgramSummaryDTO> getEligiblePrograms(List<String> programCategory, Long submitterId, 
            List<Long> paxIds, List<Long> groupIds);
    
    Boolean validateProgram(Long programId, Long submitterId, List<Long> paxIds, List<Long> groupIds);
    
    List<ProgramSummaryDTO> getAllEligibleProgramsForPax(Long paxId, List<String> programTypeList, String eligibility);
    
    List<Long> getProgramsWithAdminVisibility();
    
    Boolean checkIfEligibleProgramsForPax(Long paxId, String eligibility);
    
    List<Long> getEligibleReceiversForProgram(Long programId);
    
    List<Map<String, Object>> getEligibleReceiversForProgram(Long programId, List<Long> paxIds, List<Long> groupIds);
    
    List<Map<String, Object>> getRecognitionEligibility(Long paxId);
    
    /**
     * Checks ACL to determine if the specified pax can give from the specified program
     * 
     * @param paxId
     * @param programId
     * @return TRUE/FALSE
     */
    Boolean canPaxGiveFromProgram(Long paxId, Long programId);
}