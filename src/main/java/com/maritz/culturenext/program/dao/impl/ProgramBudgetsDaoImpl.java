package com.maritz.culturenext.program.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dao.ProgramBudgetsDao;

@Repository
public class ProgramBudgetsDaoImpl extends AbstractDaoImpl implements ProgramBudgetsDao {
    private static final Logger logger = LoggerFactory.getLogger(ProgramBudgetsDaoImpl.class);
    
    private static final String PAX_SEARCH_TYPE = "P";
    private static final String GROUPS_SEARCH_TYPE = "G";
    private static final String RS_SEARCH_TYPE = "searchType";
    
    private static final String PROGRAM_ID_PARAM = "PROGRAM_ID";
    
    private static final String BUDGET_ID_KEY = "budgetId";
    
    private static final String ELIGIBLE_GIVING_MEMBERS_SPROC = "EXEC component.UP_GET_BUDGET_GIVING_MEMBERS :" + PROGRAM_ID_PARAM;

    @Override
    @Nonnull
    public Map<Long, List<EntityDTO>> getEligibileGivingMembers(@Nonnull Long programId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID_PARAM, programId);

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(ELIGIBLE_GIVING_MEMBERS_SPROC, params, new CamelCaseMapRowMapper());

        Map<Long, List<EntityDTO>> resultMap = new HashMap<Long, List<EntityDTO>>();
        for (Map<String, Object> node : nodes) {
            List<EntityDTO> giversList = resultMap.get((Long)node.get(BUDGET_ID_KEY));
            if (giversList == null) {
                giversList = new ArrayList<EntityDTO>();
            }

            if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase(PAX_SEARCH_TYPE)) {
                giversList.add(new EmployeeDTO(node, null));
            } else if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase(GROUPS_SEARCH_TYPE)) {
                giversList.add(new GroupDTO(node));
            } else {
                // log no matching and move on
                logger.warn("No matching search type found for node " + node);
            }
            resultMap.put((Long)node.get(BUDGET_ID_KEY), giversList);
        }
        return resultMap;
    }

}
