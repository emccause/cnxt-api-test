package com.maritz.culturenext.program.dao;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.program.dto.ProgramSummaryDTO;

public interface ProgramDao {
    
    List<ProgramSummaryDTO> getProgramByStatus(boolean receivedStatusRequest, List<String> statusList, 
            boolean haveActiveStatus, boolean haveScheduledStatus, boolean haveEndedStatus, String programTypeList);
    String getProgramVisibility(Long programId);
    void setLikeInfo(Long paxId, ProgramSummaryDTO programDto);
    Long getProgramImageId(Long programId);
    Map<String, String> getProgramDtoData(Long programId);
}
