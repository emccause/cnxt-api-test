package com.maritz.culturenext.program.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import com.maritz.culturenext.profile.dto.EntityDTO;

public interface ProgramBudgetsDao {
    @Nonnull Map<Long, List<EntityDTO>> getEligibileGivingMembers(@Nonnull Long programId);
}
