package com.maritz.culturenext.program.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dao.ProgramDao;
import com.maritz.culturenext.program.dto.ProgramMiscDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.StatusTypeUtil;

@Component
public class ProgramDaoImpl extends AbstractDaoImpl implements ProgramDao {
    private static final String STATUSES = "STATUSES";

    private static final String PROGRAM_TYPE_LIST = "PROGRAM_TYPE_LIST";

    final Logger logger = LoggerFactory.getLogger(getClass());
    
    //program visibility
    private static final String VISIBILITY = "VISIBILITY";
    private static final String PUBLIC_VISIBILITY = "PUBLIC";

    private static final String WHERE_CLAUSE_PLACEHOLDER = "WHERE_CLAUSE_PLACEHOLDER";

    /**
     * QUERY for ALL STATUS TYPES
     */
    private static final String ACTIVE_QUERY = "select DISTINCT PRGM.PROGRAM_ID, PRGM.PROGRAM_NAME, PRGM.PROGRAM_DESC, PRGM.FROM_DATE, PRGM.THRU_DATE, " +
            "PRGM.PROGRAM_TYPE_CODE, PRGM.CREATE_DATE, PRGM.CREATOR_PAX_ID, PRGM.PROGRAM_LONG_DESC, PRGM.STATUS_TYPE_CODE, PT.PROGRAM_TYPE_NAME, " +
            "PRGM.PROGRAM_CATEGORY_CODE, p.PAX_ID, p.LANGUAGE_CODE, p.PREFERRED_LOCALE, p.CONTROL_NUM, p.FIRST_NAME, p.MIDDLE_NAME, p.LAST_NAME, p.NAME_PREFIX, p.NAME_SUFFIX, p.COMPANY_NAME, " +
            "p.ALTERNATE_NAME, program_images.files_id as 'PROGRAM_IMAGE_ID', pax_file.PAX_ID as 'PAX_IMAGE_ID', su.STATUS_TYPE_CODE AS 'PAX_STATUS', pgm.MISC_DATA AS 'JOB_TITLE', rl.PAX_ID_2 as 'MANAGER_PAX_ID' " +
            "from component.PROGRAM PRGM " +
            "RIGHT OUTER JOIN component.PROGRAM_type PT " +
            "ON PT.PROGRAM_TYPE_CODE = PRGM.PROGRAM_type_code " +
            "LEFT OUTER JOIN component.pax p " +
            "ON PRGM.creator_pax_id = p.pax_id " +
            "LEFT OUTER JOIN component.pax_files pax_file " +
            "ON p.pax_id = pax_file.pax_id " +
            "LEFT OUTER JOIN component.sys_user su " +
            "ON su.pax_id = p.pax_id " +
            "LEFT OUTER JOIN component.pax_group pg " +
            "ON pg.pax_id = p.pax_id and pg.thru_date is null " +
            "LEFT OUTER JOIN component.pax_group_misc pgm " +
            "on pgm.pax_group_id = pg.pax_group_id and pgm.VF_NAME = 'JOB_TITLE' " +
            "LEFT OUTER JOIN (" +
            "select target_id as program_id, files_id " +
            "from component.FILE_ITEM file_item " +
            "inner join component.FILES files " +
            "on file_item.FILES_ID = files.id " +
            "where files.FILE_TYPE_CODE = '" + FileTypes.PROGRAM_IMAGE.name() + "'" +
            "and file_item.TARGET_TABLE = '" + TableName.PROGRAM.name() + "'" +
            ") program_images " +
            "on PRGM.PROGRAM_ID = program_images.PROGRAM_ID " +
            "LEFT OUTER JOIN component.RELATIONSHIP rl " +
            "on rl.PAX_ID_1 = p.PAX_ID and rl.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
            WHERE_CLAUSE_PLACEHOLDER;

    /**
     * Wrap union queries together to get distinct values
     */
    private static final String QUERY_WRAPPER = "select DISTINCT a.PROGRAM_ID, a.PROGRAM_NAME, a.PROGRAM_DESC, a.FROM_DATE, a.THRU_DATE, " +
               "a.PROGRAM_TYPE_CODE, a.CREATE_DATE, a.CREATOR_PAX_ID, a.PROGRAM_LONG_DESC, a.STATUS_TYPE_CODE, a.PROGRAM_TYPE_NAME, " +
               "a.PROGRAM_CATEGORY_CODE, a.PAX_ID, a.LANGUAGE_CODE, a.PREFERRED_LOCALE, a.CONTROL_NUM, a.FIRST_NAME, a.MIDDLE_NAME, " +
               "a.LAST_NAME, a.NAME_PREFIX, a.NAME_SUFFIX, a.COMPANY_NAME, a.ALTERNATE_NAME, a.PAX_IMAGE_ID, a.PAX_STATUS, " +
               "a.JOB_TITLE, a.PROGRAM_IMAGE_ID, a.MANAGER_PAX_ID from ( ";
    /**
     * Query for program_misc by program_id
     */
    private static final String PROGRAM_MISC_QUERY = "select * from component.PROGRAM_misc where PROGRAM_id = :PROGRAM_ID";
    private static final String PROGRAM_MISC_POPULATE_DTO_QUERY = "select COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'NEWSFEED_VISIBILITY'),'') as 'newsfeed_visibility', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'NOTIFICATION_RECIPIENT'),'') as 'notification_recipient', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'NOTIFICATION_RECIPIENT_MGR'),'')  as 'notification_recipient_mgr', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'NOTIFICATION_RECIPIENT_2ND_MGR'),'')  as 'notification_recipient_2nd_mgr', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'NOTIFICATION_SUBMITTER'),'')  as 'notification_submitter', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'ALLOW_TEMPLATE_UPLOAD'),'')  as 'allow_template_upload', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'SELECTED_AWARD_TYPE'),'')  as 'selected_award_type', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'PPP_INDEX_ENABLED'),'')  as 'ppp_index_enabled', " +
            "COALESCE ((select MISC_DATA from PROGRAM_MISC where PROGRAM_ID = :PROGRAM_ID and VF_NAME = 'REMINDER_DAYS'),'')  as 'reminder_days'";
    private static final String PROGRAM_IMAGE_ID_QUERY = "select top 1 FILES_ID from component.FILE_ITEM WHERE TARGET_TABLE = '' AND TARGET_ID = :PROGRAM_ID";
    private static final String PROGRAM_VISIBILITY_QUERY = "select * from component.VW_PROGRAM_VISIBILITY where program_id = :PROGRAM_ID";

    private static final String ACTIVE_WHERE_CLAUSE =
            "WHERE PRGM.status_type_code = 'ACTIVE' " +
            "and PRGM.from_date <= GETDATE() " +
            "and (PRGM.thru_date IS NULL or PRGM.thru_date > GETDATE()) ";
    private static final String SCHEDULED_WHERE_CLAUSE =
            "WHERE PRGM.status_type_code = 'ACTIVE' " +
            "and PRGM.from_date > GETDATE() ";
    private static final String ENDED_WHERE_CLAUSE =
            "WHERE PRGM.status_type_code = 'INACTIVE' " +
            " or (PRGM.status_type_code = 'ACTIVE' " +
            "and PRGM.thru_date <= GETDATE()) ";
    private static final String ALL_REMAINING_WHERE_CLAUSE =
            "WHERE PRGM.status_type_code IN (:STATUSES) ";
    private static final String UNION_ALL_STATEMENT =
            " UNION ALL ";
    
    private static final String FILTER_BY_PROGRAM_TYPE =
            " ) a WHERE a.PROGRAM_TYPE_CODE <> 'APPLICATION'"
            + " AND (:PROGRAM_TYPE_LIST IS NULL OR PROGRAM_TYPE_NAME IN "
            + "(SELECT * FROM component.UF_LIST_TO_TABLE(:PROGRAM_TYPE_LIST, ','))) "
            + "ORDER BY a.PROGRAM_NAME ";
    
    //PROGRAM columns
    private static final String PROGRAM_ID = "programId";
    private static final String PROGRAM_NAME = "programName";
    private static final String PROGRAM_DESC = "programDesc";
    private static final String FROM_DATE = "fromDate";
    private static final String THRU_DATE = "thruDate";
    private static final String CREATE_DATE = "createDate";
    private static final String CREATOR_PAX_ID = "creatorPaxId";
    private static final String PROGRAM_LONG_DESC = "programLongDesc";
    private static final String STATUS_TYPE_CODE = "statusTypeCode";
    private static final String PROGRAM_TYPE_NAME = "programTypeName";
    private static final String PROGRAM_CATEGORY_CODE = "programCategoryCode";
    private static final String PROGRAM_IMAGE_ID = "programImageId";

    //PROGRAM_misc columns
    private static final String PROGRAM_MISC_ID = "PROGRAM_MISC_ID";
    private static final String VF_NAME = "VF_NAME";
    private static final String MISC_DATA = "MISC_DATA";

    private static final String LIKE_COUNT = "LIKE_COUNT";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID_SQL_PARAM";
    private static final String RS_LIKE_COUNT = "likeCount";



    private static final String LIKE_DATA_QUERY =
            "SELECT " +
                    "PRGM.PROGRAM_ID, " +
                    "(SELECT COUNT(*) FROM component.LIKES WHERE TARGET_TABLE = 'PROGRAM' AND TARGET_ID = PRGM.PROGRAM_ID) as " + LIKE_COUNT + ", " +
                    "LIKE_PAX.STATUS_TYPE_CODE, " +
                    "LIKE_PAX.PAX_ID, " +
                    "LIKE_PAX.LANGUAGE_CODE, " +
                    "LIKE_PAX.PREFERRED_LOCALE, " +
                    "LIKE_PAX.CONTROL_NUM, " +
                    "LIKE_PAX.FIRST_NAME, " +
                    "LIKE_PAX.MIDDLE_NAME, " +
                    "LIKE_PAX.LAST_NAME, " +
                    "LIKE_PAX.NAME_PREFIX, " +
                    "LIKE_PAX.NAME_SUFFIX, " +
                    "LIKE_PAX.COMPANY_NAME, " +
                    "LIKE_PAX.VERSION, " +
                    "LIKE_PAX.JOB_TITLE, " +
                    "LIKE_PAX.MANAGER_PAX_ID  " +
                "FROM component.PROGRAM PRGM " +
                "LEFT JOIN ( " +
                "SELECT " +
                    "L1.TARGET_ID, " +
                    "L1.PAX_ID FROM component.LIKES L1 " +
                    "LEFT OUTER JOIN component.LIKES L2 " +
                        "ON L2.TARGET_TABLE = L1.TARGET_TABLE " +
                        "AND L2.TARGET_ID = L1.TARGET_ID " +
                        "AND L2.LIKE_ID > L1.LIKE_ID " +
                    "WHERE L2.LIKE_ID IS NULL " +
                        "AND L1.TARGET_TABLE = 'PROGRAM' " +
                ") AS LATEST_LIKE_PAX " +
                    "ON PRGM.PROGRAM_ID = LATEST_LIKE_PAX.TARGET_ID " +
                "LEFT JOIN ( " +
                "SELECT TARGET_ID, PAX_ID FROM component.LIKES WHERE PAX_ID = :" + PAX_ID_SQL_PARAM + " AND TARGET_TABLE = 'PROGRAM' " +
                ") AS USER_LIKE_PAX " +
                    "ON PRGM.PROGRAM_ID = USER_LIKE_PAX.TARGET_ID " +
                "LEFT JOIN component.VW_USER_GROUPS_SEARCH_DATA LIKE_PAX " +
                    "ON CASE " +
                        "WHEN USER_LIKE_PAX.PAX_ID IS NOT NULL AND LIKE_PAX.PAX_ID = USER_LIKE_PAX.PAX_ID THEN 1 " +
                        "WHEN USER_LIKE_PAX.PAX_ID IS NULL AND LIKE_PAX.PAX_ID = LATEST_LIKE_PAX.PAX_ID THEN 1 " +
                        "ELSE 0 " +
                    "END = 1 " +
                "LEFT JOIN component.RELATIONSHIP RL ON RL.PAX_ID_1 = LIKE_PAX.PAX_ID AND RL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
                "WHERE PRGM.PROGRAM_ID = :" + ProjectConstants.PROGRAM_ID_SQL_PARAM;

    private static final boolean isShowQueryToExecute = false; // flag for enabling/disabling the logging of the query to execute.
                                                               // disabled by default.
    
    @Override
    public List<ProgramSummaryDTO> getProgramByStatus(boolean receivedStatusRequest, List<String> statusList, 
            boolean haveActiveStatus, boolean haveScheduledStatus, boolean haveEndedStatus, String programTypeList) {
        List<ProgramSummaryDTO> totalProgramList = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();
        if (receivedStatusRequest) {
            if (haveActiveStatus) {
                queryString.append( ACTIVE_QUERY ); // Where ACTIVE QUERY existed: Used for when status_type_code is active
                queryString = new StringBuilder(Pattern.compile(WHERE_CLAUSE_PLACEHOLDER).matcher(queryString).replaceAll(ACTIVE_WHERE_CLAUSE)); 
            }

            if (haveScheduledStatus) {
                if ( queryString.length() > 0 ) {
                    // Where SCHEDULED_QUERY existed: Used for when status_type_code is scheduled. Union all statement
                    queryString.append(UNION_ALL_STATEMENT);
                }
                // Where SCHEDULED_QUERY existed: Used for when status_type_code is scheduled
                queryString.append(ACTIVE_QUERY);
                queryString = new StringBuilder(Pattern.compile(WHERE_CLAUSE_PLACEHOLDER).matcher(queryString).replaceAll(SCHEDULED_WHERE_CLAUSE));
            }

            if (haveEndedStatus) {
                if ( queryString.length() > 0 ) {
                    // Where ENDED_QUERY existed: Used for when status_type_code is scheduled. Union all statement
                    queryString.append(UNION_ALL_STATEMENT);
                } 
                // Where ENDED_QUERY existed: Used for when status_type_code is scheduled.
                queryString.append(ACTIVE_QUERY);
                queryString = new StringBuilder(Pattern.compile(WHERE_CLAUSE_PLACEHOLDER).matcher(queryString).replaceAll(ENDED_WHERE_CLAUSE));                    
            }
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(STATUSES, statusList);
        if (statusList != null && statusList.size() > 0) {            
            if (queryString.length() > 0 ){
                // Where ALL_REMAINING_QUERY existed: Used for when status_type_code is in the list of all remaining statuses. Union all statement
                queryString.append(UNION_ALL_STATEMENT);
            } 
            // Where ALL_REMAINING_QUERY existed: Used for when status_type_code is in the list of all remaining statuses
            queryString.append(ACTIVE_QUERY);
            queryString = new StringBuilder(Pattern.compile(WHERE_CLAUSE_PLACEHOLDER).matcher(queryString).replaceAll(ALL_REMAINING_WHERE_CLAUSE));
        }
        
        queryString.insert(0,QUERY_WRAPPER);
        
        params.addValue(PROGRAM_TYPE_LIST, programTypeList);
        queryString.append(FILTER_BY_PROGRAM_TYPE);
        
        List<Map<String, Object>> nodes = null;
        if (isShowQueryToExecute){
            logger.warn("About to execute query"+ queryString.toString());
            logger.warn("with parameteres:"+ params.getValues());
        }
        if (params.getValues().size() > 0){
            nodes = namedParameterJdbcTemplate.query(queryString.toString(), params, new CamelCaseMapRowMapper());
        } else {
            nodes = namedParameterJdbcTemplate.query(queryString.toString(), new CamelCaseMapRowMapper());
        }
        
        totalProgramList.addAll(setProgramSummaryDTOFromQueryNodes(nodes));

        return totalProgramList;
    }

    @Override
    public Map<String, String> getProgramDtoData(Long programId){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.PROGRAM_ID_SQL_PARAM, programId);
        return (Map) namedParameterJdbcTemplate.queryForMap(PROGRAM_MISC_POPULATE_DTO_QUERY,params);
    }


    private List<ProgramSummaryDTO> setProgramSummaryDTOFromQueryNodes(List<Map<String, Object>> nodes) {
        List<ProgramSummaryDTO> programSummaryDTOList = new ArrayList<>();
        for (Map<String, Object> node: nodes) {
            ProgramSummaryDTO result = new ProgramSummaryDTO();

            result.setProgramId((Long) node.get(PROGRAM_ID));
            result.setProgramName((String) node.get(PROGRAM_NAME));
            result.setProgramDescriptionShort((String) node.get(PROGRAM_DESC));
            result.setImageId((Long) node.get(PROGRAM_IMAGE_ID));

            Date fromTime = (Date) node.get(FROM_DATE);
            if (fromTime != null) {
                result.setFromDate(DateUtil.convertToUTCDate(fromTime));
            }

            Date thruTime = (Date) node.get(THRU_DATE);
            if (thruTime != null) {
                result.setThruDate(DateUtil.convertToUTCDate(thruTime));
            }

            result.setProgramType((String) node.get(PROGRAM_TYPE_NAME));
            result.setProgramCategory((String) node.get(PROGRAM_CATEGORY_CODE));

            Date createTime = (Date) node.get(CREATE_DATE);
            if (createTime != null) {
                result.setCreateDate(DateUtil.convertToUTCDate(createTime));
            }

            result.setProgramDescriptionLong((String) node.get(PROGRAM_LONG_DESC));
            result.setStatus(StatusTypeUtil.getImpliedStatus((String) node.get(STATUS_TYPE_CODE), 
                    DateUtil.convertToStartOfDayString(result.getFromDate()), DateUtil.convertToEndOfDayString(result.getThruDate()))); 
                
            //Set the program Miscs            
            List <ProgramMiscDTO> programMiscDTOList = new ArrayList<>();

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue(ProjectConstants.PROGRAM_ID_SQL_PARAM, result.getProgramId());
            List<Map<String, Object>> programMiscnodes = namedParameterJdbcTemplate.query(PROGRAM_MISC_QUERY, params, new ColumnMapRowMapper());

            for (Map<String, Object> programMiscNode: programMiscnodes) {

                ProgramMiscDTO programMiscDTO = new ProgramMiscDTO();
                programMiscDTO.setProgramId((Long) programMiscNode.get(PROGRAM_ID));
                programMiscDTO.setProgramMiscId((Long) programMiscNode.get(PROGRAM_MISC_ID));
                programMiscDTO.setVfName((String) programMiscNode.get(VF_NAME));
                programMiscDTO.setMiscData((String) programMiscNode.get(MISC_DATA));

                programMiscDTOList.add(programMiscDTO);
            }

            Long creatorPaxId = (Long) node.get(CREATOR_PAX_ID);

            //Set the creatorPax
            if (creatorPaxId != null)  {
                result.setCreatorPax(new EmployeeDTO(node, null));
            }
            
            programSummaryDTOList.add(result);
        }
        return programSummaryDTOList;
    }

    @Override
    public void setLikeInfo(Long paxId, ProgramSummaryDTO programDto) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        params.addValue(ProjectConstants.PROGRAM_ID_SQL_PARAM, programDto.getProgramId());

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(LIKE_DATA_QUERY, params, new CamelCaseMapRowMapper());
        if (nodes.isEmpty()) {
            return;
        }
        Map<String, Object> node = nodes.get(0);

        programDto.setLikeCount((Integer) node.get(RS_LIKE_COUNT));

        Long likePaxId = (Long) node.get(ProjectConstants.PAX_ID);
        if (likePaxId != null) {
            programDto.setLikePax(new EmployeeDTO(node, null));
        }

    }

    /**
     * Gets the program visibility from VW_PROGRAM_VISIBILITY.  Should either be ADMIN or PUBLIC
     */
    @Override
    public String getProgramVisibility(Long programId) {
        String visibility = "";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.PROGRAM_ID_SQL_PARAM, programId);
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROGRAM_VISIBILITY_QUERY, params, new ColumnMapRowMapper());

        if (CollectionUtils.isEmpty(nodes)) {
            visibility = PUBLIC_VISIBILITY;
        } else {
            for (Map<String, Object> node: nodes) {
                visibility = node.get(VISIBILITY) != null ? (String) node.get(VISIBILITY) : PUBLIC_VISIBILITY;
                //will only have 1 value so break
                break;
            }
        }
        return visibility;
    }

    // For use until Program Images jpa entity is created
    public Long getProgramImageId(Long programId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.PROGRAM_ID_SQL_PARAM, programId);
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROGRAM_IMAGE_ID_QUERY, params, new ColumnMapRowMapper());

        if (nodes == null || nodes.size() == 0) {
            return null;
        }

        return (Long) nodes.get(0).get("FILES_ID");
    }
}
