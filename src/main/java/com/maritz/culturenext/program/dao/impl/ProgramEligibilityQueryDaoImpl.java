package com.maritz.culturenext.program.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.services.ProgramService;

import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.crypto.signers.ISOTrailers;
import org.joda.time.format.ISOPeriodFormat;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.ProgramEligibilityEnum;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.BulkDataUtil;

import org.springframework.util.ObjectUtils;

@Repository
public class ProgramEligibilityQueryDaoImpl extends AbstractDaoImpl implements ProgramEligibilityQueryDao {
    
    @Inject private ProgramService programService;
    
    private static final String SUBMITTER_PAX_ID_SQL_PARAM = "SUBMITTER_PAX_ID";
    private static final String STATUS_TYPE_CODE_LIST_SQL_PARAM = "STATUS_TYPE_CODE_LIST";
    private static final String ROLE_NAMES_LIST_SQL_PARAM = "ROLE_NAMES_LIST";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST";
    private static final String PROGRAM_TYPE_NAME_SQL_PARAM = "PROGRAM_TYPE_LIST";
    private static final String IS_GIVE_REC_ELEGIBILITY_PARAM = "IS_GIVE_REC_ELEGIBILITY";
    
    private static final String ID = "ID";
    private static final String TYPE = "TYPE";
    
    private static final List<String> UNSUPPORTED_STATUS_TYPE_CODE_LIST = Arrays.asList(StatusTypeCode.INACTIVE.name(), StatusTypeCode.RESTRICTED.name());
    
    private static final String RESOLVE_FINAL_GROUPS_PAX_QUERY = 
            "SELECT DISTINCT P.PAX_ID AS " + ID + ", 'PAX' AS " + TYPE +" " +
            "FROM component.PAX P " +
            "LEFT JOIN component.GROUPS_PAX GP " +
                "ON P.PAX_ID = GP.PAX_ID " +
            "LEFT JOIN component.GROUPS G " +
                "ON GP.GROUP_ID = G.GROUP_ID " +
            "LEFT JOIN component.SYS_USER SU " +
                "ON P.PAX_ID = SU.PAX_ID " +
            "LEFT JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL " +
                "ON PL." + BulkDataUtil.PAX_ID_COLUMN + " = P.PAX_ID " +
            "LEFT JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL " +
                "ON GL." + BulkDataUtil.GROUP_ID_COLUMN + " = G.GROUP_ID " +
            "WHERE ( " +
                "PL." + BulkDataUtil.PAX_ID_COLUMN + " IS NOT NULL " +
                "OR (GL." + BulkDataUtil.GROUP_ID_COLUMN + " IS NOT NULL " +
                        "AND G.PAX_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM + ")" +
            ") " +
                "AND SU.STATUS_TYPE_CODE NOT IN (:" + STATUS_TYPE_CODE_LIST_SQL_PARAM +  ") " +
            "UNION ALL " +
            "SELECT DISTINCT G.GROUP_ID, 'GROUP' " +
            "FROM component.GROUPS G " +
            "INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL " +
                "ON GL." + BulkDataUtil.GROUP_ID_COLUMN +" = G.GROUP_ID " +
            "LEFT JOIN COMPONENT.VW_GROUPS_GROUPS VGG " +
                "ON VGG.GROUP_ID_2 = G.GROUP_ID " +
            "LEFT JOIN component.GROUPS CG " +
                "ON VGG.GROUP_ID_1 = CG.GROUP_ID " + 
            "WHERE G.STATUS_TYPE_CODE NOT IN (:" + STATUS_TYPE_CODE_LIST_SQL_PARAM +  ")" +
            "AND (G.PAX_ID IS NULL OR CG.PAX_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM +")";
    
    private static final String PROGRAM_ID = "PROGRAM_ID";
    private static final String PROGRAM_NAME = "PROGRAM_NAME";
    private static final String PROGRAM_DESCRIPTION_SHORT = "PROGRAM_DESCRIPTION_SHORT";
    private static final String PROGRAM_DESCRIPTION_LONG = "PROGRAM_DESCRIPTION_LONG";
    private static final String PROGRAM_TYPE = "PROGRAM_TYPE";
    private static final String PROGRAM_CATEGORY_CODE = "PROGRAM_CATEGORY";
    private static final String CREATE_DATE = "CREATE_DATE";
    private static final String FROM_DATE = "FROM_DATE";
    private static final String THRU_DATE = "THRU_DATE";
    private static final String STATUS = "STATUS";
    private static final String PROGRAM_IMAGE_ID = "PROGRAM_IMAGE_ID";
    
    private static final String RESOLVE_PROGRAMS_BASE_QUERY = 
            "SELECT SUBMITTER_PROGRAMS.* " +
            "FROM ( " +
                "SELECT DISTINCT " +
                    "PRGM.PROGRAM_ID AS " + PROGRAM_ID + ", " +
                    "PRGM.PROGRAM_NAME AS " + PROGRAM_NAME + ", " +
                    "PRGM.PROGRAM_DESC AS " + PROGRAM_DESCRIPTION_SHORT + ", " +
                    "PRGM.PROGRAM_LONG_DESC AS " + PROGRAM_DESCRIPTION_LONG + ", " +
                    "PT.PROGRAM_TYPE_NAME AS " + PROGRAM_TYPE + ", " +
                    "PRGM.PROGRAM_CATEGORY_CODE AS " + PROGRAM_CATEGORY_CODE + ", " +
                    "PRGM.CREATE_DATE AS " + CREATE_DATE + ", " +
                    "PRGM.FROM_DATE AS " + FROM_DATE + ", " +
                    "PRGM.THRU_DATE AS " + THRU_DATE + ", " +
                    "PRGM.STATUS_TYPE_CODE AS " + STATUS + ", " +
                    "PROGRAM_IMAGE.FILES_ID AS " + PROGRAM_IMAGE_ID + " " +
                "FROM component.ACL " +
                    "INNER JOIN component.ROLE " +
                        "ON ROLE.ROLE_ID = ACL.ROLE_ID " +
                    "INNER JOIN component.PROGRAM PRGM " +
                        "ON PRGM.PROGRAM_ID = ACL.TARGET_ID " +
                    "INNER JOIN component.PROGRAM_TYPE PT " +
                        "ON PRGM.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE " +
                    "LEFT JOIN (" +
                        "SELECT " +
                            "FILE_ITEM.TARGET_ID AS PROGRAM_ID, " +
                            "FILES_ID " +
                        "FROM component.FILE_ITEM " +
                        "INNER JOIN component.FILES " +
                            "ON FILE_ITEM.FILES_ID = FILES.id " +
                        "WHERE FILES.FILE_TYPE_CODE = '" + FileTypes.PROGRAM_IMAGE.name() + "' " +
                            "AND FILE_ITEM.TARGET_TABLE = '" + TableName.PROGRAM.name() + "'" +
                    ") PROGRAM_IMAGE " +
                        "ON PROGRAM_IMAGE.PROGRAM_ID = ACL.TARGET_ID " +
                    "LEFT JOIN PROGRAM_CATEGORY_LIST PGL " +
                        "ON PRGM.PROGRAM_CATEGORY_CODE = PGL." +
                            BulkDataUtil.PROGRAM_CATEGORY_CODE_COLUMN + " " +
                    "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
                        "ON ACL.SUBJECT_ID = GTM.group_id AND ACL.SUBJECT_TABLE = '" + TableName.GROUPS.name() + "' " +
                "WHERE ROLE.ROLE_NAME = 'PROGRAM GIVER' " +
                    "AND ACL.TARGET_TABLE = '" + TableName.PROGRAM.name() + "' " +
                    "AND PRGM.STATUS_TYPE_CODE = '" + StatusTypeCode.ACTIVE.name() + "' " +
                    "AND PGL." + BulkDataUtil.PROGRAM_CATEGORY_CODE_COLUMN + " IS NOT NULL " +
                    "AND PRGM.FROM_DATE <= GETDATE() " +
                    "AND ( " +
                        "PRGM.THRU_DATE IS NULL " +
                        "OR " +
                        "PRGM.THRU_DATE >= GETDATE() " +
                    ") " +
                    "AND ( " +
                        "GTM.PAX_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM + " " +
                        "OR " +
                        "( " +
                            "ACL.SUBJECT_TABLE = 'PAX' " +
                            "AND " +
                            "ACL.SUBJECT_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM + " " +
                        ") " +
                    ") " +
            ") AS SUBMITTER_PROGRAMS";
    
    private static final String RESOLVE_PROGRAMS_PAX_FRAGMENT = 
            "INNER JOIN ( " +
                "SELECT DISTINCT TARGET_ID " +
                "FROM ( " +
                    "SELECT ACL.TARGET_ID, P.PAX_ID " +
                    "FROM component.PAX P " +
                    "INNER JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
                        "ON GTM.PAX_ID = P.PAX_ID " +
                    "INNER JOIN component.ACL " +
                        "ON GTM.GROUP_ID = ACL.SUBJECT_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "INNER JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL " +
                        "ON PL." + BulkDataUtil.PAX_ID_COLUMN +" = P.PAX_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'GROUPS' " +
                        "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                        "AND ROLE.ROLE_NAME = 'PROGRAM RECEIVER' " +
                    "UNION ALL " +
                    "SELECT ACL.TARGET_ID, P.PAX_ID " +
                    "FROM component.PAX P " +
                    "INNER JOIN component.ACL " +
                        "ON P.PAX_ID = ACL.SUBJECT_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "INNER JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL " +
                        "ON PL." + BulkDataUtil.PAX_ID_COLUMN + " = P.PAX_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'PAX' " +
                        "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                        "AND ROLE.ROLE_NAME = 'PROGRAM RECEIVER' " +
                    ") AS DATA " +
                "GROUP BY TARGET_ID " +
                    "HAVING COUNT(DISTINCT DATA.PAX_ID) = "
                    + "(SELECT COUNT(DISTINCT P.PAX_ID) "
                    + " FROM component.PAX P"
                    + " INNER JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL "
                        + " ON PL." + BulkDataUtil.PAX_ID_COLUMN + " = P.PAX_ID) " +
            ") AS PAX_PROGRAMS " +
                "ON SUBMITTER_PROGRAMS." + PROGRAM_ID + " = PAX_PROGRAMS.TARGET_ID";
    
    private static final String RESOLVE_PROGRAMS_GROUPS_FRAGMENT = 
            "INNER JOIN ( " +
                "SELECT DISTINCT TARGET_ID " +
                "FROM ( " +
                    "SELECT DISTINCT ACL.TARGET_ID, G.GROUP_ID " +
                    "FROM component.GROUPS G " +
                    "INNER JOIN component.ACL " +
                        "ON G.GROUP_ID = ACL.SUBJECT_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL " +
                        "ON GL." + BulkDataUtil.GROUP_ID_COLUMN + " = G.GROUP_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'GROUPS' " +
                        "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                        "AND ROLE.ROLE_NAME = 'PROGRAM RECEIVER' " +
                    "UNION ALL " +
                    "SELECT ACL.TARGET_ID, G.GROUP_ID " +
                    "FROM component.GROUPS G " +
                    "INNER JOIN component.VW_GROUPS_GROUPS VGG " +
                        "ON VGG.GROUP_ID_2 = G.GROUP_ID " +
                    "INNER JOIN component.ACL " +
                        "ON VGG.GROUP_ID_1 = ACL.SUBJECT_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL " +
                        "ON GL." + BulkDataUtil.GROUP_ID_COLUMN + " = G.GROUP_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'GROUPS' " +
                        "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                        "AND ROLE.ROLE_NAME = 'PROGRAM RECEIVER' " +
                ") AS DATA " +
                "GROUP BY TARGET_ID " +
                    "HAVING COUNT(DISTINCT DATA.GROUP_ID) = " +
                        "(SELECT COUNT(DISTINCT G.GROUP_ID) " +
                        " FROM component.GROUPS G " +
                        " INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL " +
                            " ON GL." + BulkDataUtil.GROUP_ID_COLUMN + " = G.GROUP_ID)" +
            ") AS GROUP_PROGRAMS " +
                "ON SUBMITTER_PROGRAMS." + PROGRAM_ID + " = GROUP_PROGRAMS.TARGET_ID";
    
    private static final String PROGRAM_ID_SQL_PARAM = "PROGRAM_ID";
    
    private static final String VALIDATE_PROGRAM_BASE_QUERY = 
            "SELECT SUBMITTER_PROGRAMS." + PROGRAM_ID + " " +
            "FROM ( " +
                "SELECT DISTINCT " +
                "PRGM.PROGRAM_ID AS " + PROGRAM_ID + ", " +
                "PRGM.CREATE_DATE AS " + CREATE_DATE + ", " + 
                "PRGM.FROM_DATE AS " + FROM_DATE + ", " + 
                "PRGM.THRU_DATE AS " + THRU_DATE + ", " + 
                "PRGM.STATUS_TYPE_CODE AS " + STATUS + " " + 
                "FROM component.ACL " +
                "INNER JOIN component.ROLE " +
                    "ON ROLE.ROLE_ID = ACL.ROLE_ID " +
                "INNER JOIN component.PROGRAM PRGM " +
                    "ON PRGM.PROGRAM_ID = ACL.TARGET_ID " +

                "INNER JOIN component.PROGRAM_TYPE PT " +
                    "ON PRGM.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE " +
                "WHERE ROLE.ROLE_NAME = 'PROGRAM GIVER' " +
                    "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                    "AND PRGM.STATUS_TYPE_CODE = 'ACTIVE' " +
                    "AND PRGM.PROGRAM_ID = :" + PROGRAM_ID_SQL_PARAM +  " " +
                    "AND PRGM.FROM_DATE <= GETDATE() " +
                    "AND ( " +
                        "PRGM.THRU_DATE IS NULL " +
                        "OR " +
                        "PRGM.THRU_DATE >= GETDATE() " +
                    ") " +
                    "AND ( " +
                        "( " +
                            "ACL.SUBJECT_TABLE = 'GROUPS' " +
                            "AND " +
                            "ACL.SUBJECT_ID IN (SELECT GROUP_ID FROM component.GROUPS_PAX WHERE PAX_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM + ") " +
                        ") " +
                        "OR " +
                        "( " +
                            "ACL.SUBJECT_TABLE = 'PAX' " +
                            "AND " +
                            "ACL.SUBJECT_ID = :" + SUBMITTER_PAX_ID_SQL_PARAM + " " +
                        ") " +
                    ") " +
            ") AS SUBMITTER_PROGRAMS";

    private static final String LIKE_COUNT = "LIKE_COUNT";
    private static final String STATUS_TYPE_CODE = "STATUS_TYPE_CODE";
    private static final String PAX_ID = "PAX_ID";
    private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    private static final String PREFERRED_LOCALE = "PREFERRED_LOCALE";
    private static final String CONTROL_NUM = "CONTROL_NUM";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String MIDDLE_NAME = "MIDDLE_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String NAME_PREFIX = "NAME_PREFIX";
    private static final String NAME_SUFFIX = "NAME_SUFFIX";
    private static final String COMPANY_NAME = "COMPANY_NAME";
    private static final String PROFILE_PICTURE_VERSION = "VERSION"; 
    private static final String JOB_TITLE = "JOB_TITLE";
    private static final String MANAGER_PAX_ID = "MANAGER_PAX_ID";
    
    private static final String CREATOR = "creator";
    private static final String CREATOR_ID = "CREATOR_ID";
    private static final String CREATOR_LANGUAGE_CODE = "CREATOR_LANGUAGE_CODE";
    private static final String CREATOR_PREFERRED_LOCALE = "CREATOR_PREFERRED_LOCALE";
    private static final String CREATOR_CONTROL_NUM = "CREATOR_CONTROL_NUM";
    private static final String CREATOR_FIRST_NAME = "CREATOR_FIRST_NAME";
    private static final String CREATOR_MIDDLE_NAME = "CREATOR_MIDDLE_NAME";
    private static final String CREATOR_LAST_NAME = "CREATOR_LAST_NAME";
    private static final String CREATOR_NAME_PREFIX = "CREATOR_NAME_PREFIX";
    private static final String CREATOR_NAME_SUFFIX = "CREATOR_NAME_SUFFIX";
    private static final String CREATOR_COMPANY_NAME = "CREATOR_COMPANY_NAME";
    private static final String CREATOR_STATUS = "CREATOR_STATUS_TYPE_CODE";
    private static final String CREATOR_JOB_TITLE = "CREATOR_JOB_TITLE";
    private static final String CREATOR_MANAGER_ID = "CREATOR_MANAGER_PAX_ID";
    
    private static final String PROGRAMS_FOR_PAX_QUERY = "EXEC component.UP_GET_PROGRAMS_FOR_PAX " +
             " :" + PAX_ID_SQL_PARAM +
            ", :" + ROLE_NAMES_LIST_SQL_PARAM +
            ", :" + PROGRAM_TYPE_NAME_SQL_PARAM +
            ", :" + IS_GIVE_REC_ELEGIBILITY_PARAM;

    private static final String QUERY_PROGRAMS_WITH_ADMIN_VISIBILITY = "SELECT DISTINCT " +
            "PROGRAM_ID FROM component.VW_PROGRAM_VISIBILITY " +
            "WHERE VISIBILITY = 'ADMIN' ";
    
    private static final String QUERY_PAX_ELIGIBLE_RECEIVER_FROM_PROGRAM =
            "SELECT S.PAX_ID "
            + "FROM component.SYS_USER S "
            + "INNER JOIN ("
            + "        SELECT DISTINCT "
            + "            CASE    "
            + "                WHEN EL.SUBJECT_TABLE ='PAX' THEN EL.SUBJECT_ID"
            + "                WHEN EL.SUBJECT_TABLE ='GROUPS' THEN GP.PAX_ID"
            + "                ELSE NULL"
            + "            END AS PAX_ID"
            + "        FROM "
            + "        (SELECT SUBJECT_TABLE, SUBJECT_ID "
            + "         FROM component.ACL "
            + "         INNER JOIN component.ROLE "
            + "            ON ROLE.ROLE_ID = ACL.ROLE_ID  "
            + "                AND ROLE.ROLE_NAME IN ('PROGRAM RECEIVER') "
            + "         WHERE "
            + "            ACL.TARGET_TABLE = 'PROGRAM' "
            + "            AND ACL.TARGET_ID  = :" + PROGRAM_ID_SQL_PARAM 
            + "        ) AS EL "
            + "        LEFT JOIN component.GROUPS_PAX GP "
            + "            ON GP.GROUP_ID = EL.SUBJECT_ID "
            + "            AND EL.SUBJECT_TABLE ='GROUPS'"
            + ") P ON S.PAX_ID = P.PAX_ID "
            + "WHERE "
            + "    S.STATUS_TYPE_CODE NOT IN ('INACTIVE', 'RESTRICTED')";

    private static final String QUERY_ELIGIBLE_RECEIVERS_BY_PAX_IDS_AND_GROUP_IDS =
            "WITH AUDIENCE AS ( " + 
                "SELECT 'PAX' AS type, items AS id FROM component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ",',') " +
                "UNION ALL " +
                "SELECT 'GROUPS' AS type, items AS id FROM component.UF_LIST_TO_TABLE_ID(:" + GROUP_ID_LIST_SQL_PARAM + ",',') " +
            ") SELECT " +
                "recipients.TYPE as SUBJECT_TABLE, " + 
                "recipients.id as SUBJECT_ID " + 
            "FROM component.ACL " +
            "INNER JOIN component.ROLE R ON R.ROLE_ID = ACL.ROLE_ID " +
            "INNER JOIN (" +
                "SELECT * FROM AUDIENCE " +
                "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP VW_GR ON VW_GR.pax_id = AUDIENCE.id AND AUDIENCE.type = 'PAX' " +
                "LEFT JOIN component.VW_GROUPS_GROUPS VW_GRGR ON VW_GRGR.GROUP_ID_2 = AUDIENCE.id AND AUDIENCE.type = 'GROUPS' " +
            ") recipients " +
                "ON ( " +
                    "recipients.id = ACL.SUBJECT_ID " +
                    "AND recipients.type = ACL.SUBJECT_TABLE " +
                ") " +
                "OR ( " +
                    "ACL.SUBJECT_TABLE = 'GROUPS' " +
                    "AND ( " +
                        "ACL.SUBJECT_ID = recipients.group_id " +
                        "OR ACL.SUBJECT_ID = recipients.GROUP_ID_1 " +
                    ") " +
                ") " +
            "WHERE R.ROLE_CODE = 'PREC' " +
                "AND ACL.TARGET_TABLE = 'PROGRAM' " +
                "AND ACL.TARGET_ID = :" + PROGRAM_ID_SQL_PARAM;
    
    private static final String PROGRAM_ELIGIBILITY_QUERY = "EXEC component.UP_PROGRAM_ELIGIBILITY :" + PAX_ID_SQL_PARAM;
    
    private static final String PROGRAM_GIVER_QUERY = 
            "SELECT * " + 
            "FROM component.ROLE r " + 
            "JOIN component.ACL acl " + 
                "ON acl.ROLE_ID = r.ROLE_ID " +  
                "AND acl.TARGET_TABLE = 'PROGRAM' " + 
                "AND acl.TARGET_ID = :" + PROGRAM_ID_SQL_PARAM + " " + 
            "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP gtm " + 
                "ON gtm.group_id = acl.SUBJECT_ID " + 
                "AND acl.SUBJECT_TABLE = 'GROUPS' " + 
            "WHERE r.ROLE_CODE = 'PGIV' " + 
            "AND ( " + 
                "(acl.SUBJECT_TABLE = 'PAX' and acl.SUBJECT_ID = :" + PAX_ID_SQL_PARAM + ") " + 
                "OR gtm.PAX_ID = :" + PAX_ID_SQL_PARAM + " " + 
            ")"; 
    
    @Override
    public List<ProgramSummaryDTO> getEligiblePrograms(List<String> programCategory, Long submitterId, 
            List<Long> paxIds, List<Long> groupIds) {
        
        ArrayList<Long> finalPaxIds = new ArrayList<Long>();
        ArrayList<Long> finalGroupIds = new ArrayList<Long>();
        
        resolvePaxAndGroups(submitterId, paxIds, groupIds, finalPaxIds, finalGroupIds);

        return resolvePrograms(programCategory, submitterId, finalPaxIds, finalGroupIds);
    }
    
    @Override
    public Boolean validateProgram(Long programId, Long submitterId, List<Long> paxIds, List<Long> groupIds) {
        
        ArrayList<Long> finalPaxIds = new ArrayList<Long>();
        ArrayList<Long> finalGroupIds = new ArrayList<Long>();
        
        resolvePaxAndGroups(submitterId, paxIds, groupIds, finalPaxIds, finalGroupIds);

        return validateProgramFinal(programId, submitterId, finalPaxIds, finalGroupIds);
    }
    
    private void resolvePaxAndGroups(Long submitterId, List<Long> paxIds, List<Long> groupIds,
            ArrayList<Long> finalPaxIds, ArrayList<Long> finalGroupIds) {
        if (paxIds.isEmpty()) {
            paxIds.add(0L);
        }
        
        if (groupIds.isEmpty()) {
            groupIds.add(0L);
        }
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SUBMITTER_PAX_ID_SQL_PARAM, submitterId);
        params.addValue(STATUS_TYPE_CODE_LIST_SQL_PARAM, UNSUPPORTED_STATUS_TYPE_CODE_LIST);
        
        String query = BulkDataUtil.buildCommonTablesQuery(RESOLVE_FINAL_GROUPS_PAX_QUERY,
                                                           params,
                                                           paxIds,
                                                           groupIds,
                                                           null);
        

        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
        for (Map<String,Object> node: nodes) {
            Long id = (Long) node.get(ID);
            String type = (String) node.get(TYPE);
            if (type.equalsIgnoreCase("PAX")) {
                finalPaxIds.add(id);
            } else {
                finalGroupIds.add(id);
            }
        }
    }

    
    private List<ProgramSummaryDTO> resolvePrograms(List<String> programCategories,
                                                    Long submitterId,
                                                    List<Long> paxIds,
                                                    List<Long> groupIds){
        
        String query = RESOLVE_PROGRAMS_BASE_QUERY;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SUBMITTER_PAX_ID_SQL_PARAM, submitterId);
        
        if (!ObjectUtils.isEmpty(paxIds)) {
            query = query + " " + RESOLVE_PROGRAMS_PAX_FRAGMENT;
        }
        
        if (!ObjectUtils.isEmpty(groupIds)) {
            query = query + " " + RESOLVE_PROGRAMS_GROUPS_FRAGMENT;
        }
        
        query = BulkDataUtil.buildCommonTablesQuery(query,
                                                    params,
                                                    paxIds,
                                                    groupIds,
                                                    programCategories);
        

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
            
        ArrayList<ProgramSummaryDTO> programs = new ArrayList<ProgramSummaryDTO>();
        for (Map<String,Object> node: nodes) {
            ProgramSummaryDTO program = new ProgramSummaryDTO();
            program.setProgramId((Long) node.get(PROGRAM_ID));
            program.setProgramName((String) node.get(PROGRAM_NAME));
            program.setProgramDescriptionShort((String) node.get(PROGRAM_DESCRIPTION_SHORT));
            program.setProgramDescriptionLong((String) node.get(PROGRAM_DESCRIPTION_LONG));
            program.setProgramType((String) node.get(PROGRAM_TYPE));
            program.setProgramCategory((String) node.get(PROGRAM_CATEGORY_CODE));
            program.setCreateDate(DateUtil.convertToUTCDate((Date) node.get(CREATE_DATE)));
            program.setFromDate(DateUtil.convertToUTCDate((Date) node.get(FROM_DATE)));
            Date thruDate = (Date) node.get(THRU_DATE);
            if (thruDate != null) {
                program.setThruDate(DateUtil.convertToUTCDate(thruDate));
            }
            program.setStatus((String) node.get(STATUS));
                program.setImageId((Long) node.get(PROGRAM_IMAGE_ID));
                
            programs.add(program);
        }
        return programs;

    }
    
    private Boolean validateProgramFinal(Long programId, Long submitterId, List<Long> paxIds, List<Long> groupIds) {
        String query = VALIDATE_PROGRAM_BASE_QUERY;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SUBMITTER_PAX_ID_SQL_PARAM, submitterId);
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);
        
        if (!ObjectUtils.isEmpty(paxIds)) {
            query = query + " " + RESOLVE_PROGRAMS_PAX_FRAGMENT;
        }
        
        if (!ObjectUtils.isEmpty(groupIds)) {
            query = query + " " + RESOLVE_PROGRAMS_GROUPS_FRAGMENT;
        }
        
        query = BulkDataUtil.buildCommonTablesQuery(query, params, paxIds, groupIds, null);
        

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
            
        if (nodes != null && !nodes.isEmpty()) {
            return true;
        }

        return false;
    }

    @Override
    public Boolean checkIfEligibleProgramsForPax(Long paxId, String eligibility) {
        List<Map<String, Object>> eligibileProgramList =
                getAllEligibleProgramsForPaxList(paxId, ProgramTypeEnum.getRecognitionCategoryTypes(), eligibility);
        return eligibileProgramList.size() > 0;
    }

    @Override
    public List<ProgramSummaryDTO> getAllEligibleProgramsForPax(Long paxId, List<String> programTypeList, String eligibility) {
        
        List<Map<String, Object>> nodes = getAllEligibleProgramsForPaxList(paxId, programTypeList, eligibility);
            
        List<ProgramSummaryDTO> programSummaryDtoList = new ArrayList<ProgramSummaryDTO>();

        for (Map<String,Object> node: nodes) {
            ProgramSummaryDTO program = new ProgramSummaryDTO();
            program.setProgramId((Long) node.get(ProjectConstants.PROGRAM_ID));
            program.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
            program.setProgramDescriptionShort((String) node.get(ProjectConstants.PROGRAM_DESCRIPTION_SHORT));
            program.setProgramDescriptionLong((String) node.get(ProjectConstants.PROGRAM_DESCRIPTION_LONG));
            program.setProgramType((String) node.get(ProjectConstants.PROGRAM_TYPE));
            program.setProgramCategory((String) node.get(ProjectConstants.PROGRAM_CATEGORY));
            program.setCreateDate(DateUtil.convertToUTCDate((Date) node.get(ProjectConstants.CREATE_DATE)));
            program.setFromDate(DateUtil.convertToUTCDate((Date) node.get(ProjectConstants.FROM_DATE)));
            program.setCreatorPax(new EmployeeDTO(node,CREATOR));
            Date thruDate = (Date) node.get(ProjectConstants.THRU_DATE);
            if (thruDate != null) {
                program.setThruDate(DateUtil.convertToUTCDate(thruDate));
            }
            program.setStatus((String) node.get(ProjectConstants.STATUS));
                
            program.setLikeCount((Integer) node.get(ProjectConstants.LIKE_COUNT));
                
            Long likePaxId = (Long) node.get(ProjectConstants.PAX_ID);
            if (likePaxId != null) {
                program.setLikePax(new EmployeeDTO(node, null));
            }

            program.setImageId((Long) node.get(ProjectConstants.PROGRAM_IMAGE_ID));
                
            if (ProgramTypeEnum.AWARD_CODE.getCode().equals(program.getProgramType())) {
                programService.populateAwardCodeDetails(program, null);
            }
                
            programSummaryDtoList.add(program);
        }
            
        return programSummaryDtoList;
    }
    
    private List<Map<String, Object>> getAllEligibleProgramsForPaxList(Long paxId, 
            List<String> programTypeList, String eligibility) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        List<String> roleNamesList = new ArrayList<String>();
        
        if (eligibility != null) {
            if (eligibility.toUpperCase().contains(ProgramEligibilityEnum.GIVE.name())) {
                roleNamesList.add(ProgramEligibilityEnum.GIVE.getEligibilityRoleName());
            }
            if (eligibility.toUpperCase().contains(ProgramEligibilityEnum.RECEIVE.name())) {
                roleNamesList.add(ProgramEligibilityEnum.RECEIVE.getEligibilityRoleName());
            }
            // Display program even if program visibility is not public
            params.addValue(IS_GIVE_REC_ELEGIBILITY_PARAM, Boolean.TRUE);
        } else {
            // Display program only if program visibility is public
            params.addValue(IS_GIVE_REC_ELEGIBILITY_PARAM, Boolean.FALSE);
        }
        
        if (roleNamesList.isEmpty()) {
            roleNamesList = ProgramConstants.DEFAULT_PROGRAM_ELIGIBILITY_TYPE_LIST;
        }

        params.addValue(PAX_ID_SQL_PARAM, paxId);
        params.addValue(ROLE_NAMES_LIST_SQL_PARAM, String.join(",",roleNamesList));
        params.addValue(PROGRAM_TYPE_NAME_SQL_PARAM, String.join(",", programTypeList));

        return namedParameterJdbcTemplate.query(PROGRAMS_FOR_PAX_QUERY, params, new CamelCaseMapRowMapper());
    }
    
    public List<Long> getProgramsWithAdminVisibility() {
        
        String query = QUERY_PROGRAMS_WITH_ADMIN_VISIBILITY;
            
        MapSqlParameterSource params = new MapSqlParameterSource();
    

        return namedParameterJdbcTemplate.queryForList(query, params, Long.class);        
    }
    
    /**
     * Get eligible recipients PAX ids by program 
     * @param programId
     * @return
     */
    @Override
    public List<Long> getEligibleReceiversForProgram(Long programId) {
        
        String query = QUERY_PAX_ELIGIBLE_RECEIVER_FROM_PROGRAM;
            
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);

        return namedParameterJdbcTemplate.queryForList(query, params, Long.class);        
    }

    /**
     * Get eligible recipients PAX ids by program from the given pax IDs and group IDs.
     * @param programId    The program being checked for eligible receivers
     * @param paxIds    List of pax IDs for receivers of the program
     * @param groupIds    List of group Ids for receivers of the program
     * @return List of maps containing the rows of receivers
     */
    @Override
    public List<Map<String, Object>> getEligibleReceiversForProgram(Long programId,
                                                                    List<Long> paxIds,
                                                                    List<Long> groupIds) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);
        params.addValue(ProgramConstants.PAX_ID_LIST_SQL_PARAM, StringUtils.join(paxIds, ProjectConstants.COMMA_DELIM));
        params.addValue(GROUP_ID_LIST_SQL_PARAM, StringUtils.join(groupIds, ProjectConstants.COMMA_DELIM));

        return namedParameterJdbcTemplate.query(QUERY_ELIGIBLE_RECEIVERS_BY_PAX_IDS_AND_GROUP_IDS,
                params,
                new CamelCaseMapRowMapper());

    }
    
    @Override
    public List<Map<String, Object>> getRecognitionEligibility(Long paxId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        
        return namedParameterJdbcTemplate.query(PROGRAM_ELIGIBILITY_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public Boolean canPaxGiveFromProgram(Long paxId, Long programId) {    
        return Boolean.FALSE.equals(
                namedParameterJdbcTemplate.query(
                        PROGRAM_GIVER_QUERY,
                        new MapSqlParameterSource()
                            .addValue(PAX_ID_SQL_PARAM, paxId)
                            .addValue(PROGRAM_ID_SQL_PARAM, programId),
                        new CamelCaseMapRowMapper()
                ).isEmpty()
        );
    }
}