package com.maritz.culturenext.program.dao;

import javax.annotation.Nonnull;

import com.maritz.culturenext.profile.dto.EntityDTO;

import java.util.List;
import java.util.Map;

public interface EligibilityDao {
    @Nonnull List<EntityDTO> getEligibilityEntities(@Nonnull List<Long> paxIds, @Nonnull List<Long> groupIds);
    @Nonnull List<Map<String, Object>> getEligibilityEntitiesAndStatus(@Nonnull List<Long> paxIds, 
            @Nonnull List<Long> groupIds);
    @Nonnull List<Map<String, Object>> getEligibilityEntitiesAndStatus(@Nonnull List<Long> paxIds, 
            @Nonnull List<Long> groupIds, @Nonnull List<String> validGroupTypeNames);
}
