package com.maritz.culturenext.project.dto;

public class SeoDTO {
    private String clientTitle;
    private String clientDesc;
    
    public String getClientTitle() {
        return clientTitle;
    }
    public void setClientTitle(String clientTitle) {
        this.clientTitle = clientTitle;
    }
    
    public String getClientDesc() {
        return clientDesc;
    }
    public void setClientDesc(String clientDesc) {
        this.clientDesc = clientDesc;
    }
}
