package com.maritz.culturenext.project.dto;

public class TermsAndConditionsDTO {
    private Boolean enforceTCAtLogin;
    private String majorChangeDate;
    private String editDate;
    private Boolean showInFooter;
    
    public Boolean getEnforceTCAtLogin() {
        return enforceTCAtLogin;
    }
    public void setEnforceTCAtLogin(Boolean enforceTCAtLogin) {
        this.enforceTCAtLogin = enforceTCAtLogin;
    }
    public String getMajorChangeDate() {
        return majorChangeDate;
    }
    public void setMajorChangeDate(String majorChangeDate) {
        this.majorChangeDate = majorChangeDate;
    }
    public String getEditDate() {
        return editDate;
    }
    public void setEditDate(String editDate) {
        this.editDate = editDate;
    }
    public Boolean getShowInFooter() {
        return showInFooter;
    }
    public void setShowInFooter(Boolean showInFooter) {
        this.showInFooter = showInFooter;
    } 
}
