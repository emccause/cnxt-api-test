package com.maritz.culturenext.project.dto;

public class ServiceAnniversaryDTO {
    private String provider;
    private String baseUrl;
    private String redirectLink;
    private String productCode;
    private String subClient;
    
    public String getProvider() {
        return provider;
    }
    
    public void setProvider(String provider) {
        this.provider = provider;
    }
    
    public String getBaseUrl() {
        return baseUrl;
    }
    
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
    
    public String getRedirectLink() {
        return redirectLink;
    }
    
    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }
    
    public String getProductCode() {
        return productCode;
    }
    
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    
    public String getSubClient() {
        return subClient;
    }
    
    public void setSubClient(String subClient) {
        this.subClient = subClient;
    }
}