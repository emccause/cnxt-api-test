package com.maritz.culturenext.project.dto;

public class ProjectMessagesDTO {
    private String adminOverviewHelpText;
    private String loginWelcomeMessage;
    private String loginUsernamePlaceholder;
    private String loginPasswordPlaceholder;
    private String loginSSOWelcomeMessage;
    private String logoutSSOMessage;
    private String ssoAccessErrorMessage;
    private String partnerSSOLinkText;
    private String employeeId;

    public String getAdminOverviewHelpText() {
        return adminOverviewHelpText;
    }
    public void setAdminOverviewHelpText(String adminOverviewHelpText) {
        this.adminOverviewHelpText = adminOverviewHelpText;
    }
    public String getLoginWelcomeMessage() {
        return loginWelcomeMessage;
    }
    public void setLoginWelcomeMessage(String loginWelcomeMessage) {
        this.loginWelcomeMessage = loginWelcomeMessage;
    }
    public String getLoginUsernamePlaceholder() {
        return loginUsernamePlaceholder;
    }
    public void setLoginUsernamePlaceholder(String loginUsernamePlaceholder) {
        this.loginUsernamePlaceholder = loginUsernamePlaceholder;
    }
    public String getLoginPasswordPlaceholder() {
        return loginPasswordPlaceholder;
    }
    public void setLoginPasswordPlaceholder(String loginPasswordPlaceholder) {
        this.loginPasswordPlaceholder = loginPasswordPlaceholder;
    }
    public String getLoginSSOWelcomeMessage() {
        return loginSSOWelcomeMessage;
    }
    public void setLoginSSOWelcomeMessage(String loginSSOWelcomeMessage) {
        this.loginSSOWelcomeMessage = loginSSOWelcomeMessage;
    }
    public String getLogoutSSOMessage() {
        return logoutSSOMessage;
    }
    public void setLogoutSSOMessage(String logoutSSOMessage) {
        this.logoutSSOMessage = logoutSSOMessage;
    }
    public String getSsoAccessErrorMessage() {
        return ssoAccessErrorMessage;
    }
    public void setSsoAccessErrorMessage(String ssoAccessErrorMessage) {
        this.ssoAccessErrorMessage = ssoAccessErrorMessage;
    }
    public String getPartnerSSOLinkText() {
        return partnerSSOLinkText;
    }
    public void setPartnerSSOLinkText(String partnerSSOLinkText) {
        this.partnerSSOLinkText = partnerSSOLinkText;
    }
    public String getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}
