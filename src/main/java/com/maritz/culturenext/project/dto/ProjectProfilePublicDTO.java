package com.maritz.culturenext.project.dto;

import java.util.List;

import com.maritz.culturenext.dto.ActivityFeedFilterDTO;
import com.maritz.culturenext.profile.constants.ProfileConstants;

import org.hibernate.validator.constraints.Email;

public class ProjectProfilePublicDTO {
    private ActivityFeedFilterDTO activityFeedFilter;
    private String clientName;
    private Long reportAbuseAdmin;
    @Email (message=ProfileConstants.INVALID_EMAIL_FORMAT_MSG)
    private String supportEmail;
    private Boolean ssoEnabled;
    private String partnerSsoUrl;
    private Boolean likingEnabled;
    private Boolean loginHelpMessageNoTpl;
    private Boolean reportingEnabled;
    private Boolean commentingEnabled;
    private Boolean engagementScoreEnabled;
    private Boolean networkConnectionsEnabled;
    private String pointBank;
    private ColorsProfileDTO colors;
    private List<String> siteLanguages;
    private Long unassignedApproverPaxId;
    private Boolean calendarEnabled;
    private EmailPreferencesDTO emailPreferences;
    private TermsAndConditionsDTO termsAndConditions;
    private Boolean serviceAnniversaryEnabled;
    private ServiceAnniversaryDTO serviceAnniversary;
    private SeoDTO seo;
    private String fromEmailAddress;
    private String privacyBaseUrl;
    private List<Element> view;
    private String newRelicAppId;
    private String cdnUrl;

    public String getCdnUrl() {
        return cdnUrl;
    }

    public void setCdnUrl(String cdnUrl) {
        this.cdnUrl = cdnUrl;
    }

    public String getNewRelicAppId() {
        return newRelicAppId;
    }

    public void setNewRelicAppId(String newRelicAppId) {
        this.newRelicAppId = newRelicAppId;
    }

    public ActivityFeedFilterDTO getActivityFeedFilter() {
        return activityFeedFilter;
    }

    public void setActivityFeedFilter(ActivityFeedFilterDTO activityFeedFilter) {
        this.activityFeedFilter = activityFeedFilter;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Boolean getSsoEnabled() {
        return ssoEnabled;
    }

    public void setSsoEnabled(Boolean ssoEnabled) {
        this.ssoEnabled = ssoEnabled;
    }

    public Long getReportAbuseAdmin() {
        return reportAbuseAdmin;
    }

    public void setReportAbuseAdmin(Long reportAbuseAdmin) {
        this.reportAbuseAdmin = reportAbuseAdmin;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getPartnerSsoUrl() {
        return partnerSsoUrl;
    }

    public void setPartnerSsoUrl(String partnerSsoUrl) {
        this.partnerSsoUrl = partnerSsoUrl;
    }

    public Boolean getLikingEnabled() {
        return likingEnabled;
    }

    public void setLikingEnabled(Boolean likingEnabled) {
        this.likingEnabled = likingEnabled;
    }

    public Boolean getLoginHelpMessageNoTpl() {
        return loginHelpMessageNoTpl;
    }

    public void setLoginHelpMessageNoTpl(Boolean loginHelpMessageNoTpl) {
        this.loginHelpMessageNoTpl = loginHelpMessageNoTpl;
    }

    public Boolean getReportingEnabled() {
        return reportingEnabled;
    }

    public void setReportingEnabled(Boolean reportingEnabled) {
        this.reportingEnabled = reportingEnabled;
    }

    public Boolean getCommentingEnabled() {
        return commentingEnabled;
    }

    public void setCommentingEnabled(Boolean commentingEnabled) {
        this.commentingEnabled = commentingEnabled;
    }

    public Boolean getEngagementScoreEnabled() {
        return engagementScoreEnabled;
    }

    public void setEngagementScoreEnabled(Boolean engagementScoreEnabled) {
        this.engagementScoreEnabled = engagementScoreEnabled;
    }

    public String getPointBank() {
        return pointBank;
    }

    public void setPointBank(String pointBank) {
        this.pointBank = pointBank;
    }    
    
    public ColorsProfileDTO getColors() {
        return colors;
    }
    public void setColors(ColorsProfileDTO colors) {
        this.colors = colors;
    }

    public List<String> getSiteLanguages() {
        return siteLanguages;
    }

    public void setSiteLanguages(List<String> siteLanguages) {
        this.siteLanguages = siteLanguages;
    }

    public Boolean getNetworkConnectionsEnabled() {
        return networkConnectionsEnabled;
    }

    public void setNetworkConnectionsEnabled(Boolean networkConnectionsEnabled) {
        this.networkConnectionsEnabled = networkConnectionsEnabled;
    }

    public Long getUnassignedApproverPaxId() {
        return unassignedApproverPaxId;
    }

    public void setUnassignedApproverPaxId(Long unassignedApproverPaxId) {
        this.unassignedApproverPaxId = unassignedApproverPaxId;
    }

    public Boolean getCalendarEnabled() {
        return calendarEnabled;
    }

    public void setCalendarEnabled(Boolean calendarEnabled) {
        this.calendarEnabled = calendarEnabled;
    }

    public EmailPreferencesDTO getEmailPreferences() {
        return emailPreferences;
    }

    public void setEmailPreferences(EmailPreferencesDTO emailPreferences) {
        this.emailPreferences = emailPreferences;
    }

    public TermsAndConditionsDTO getTermsAndConditions() {
        return termsAndConditions;
    }

    public void setTermsAndConditions(TermsAndConditionsDTO termsAndConditions) {
        this.termsAndConditions = termsAndConditions;
    }

    public Boolean getServiceAnniversaryEnabled() {
        return serviceAnniversaryEnabled;
    }

    public void setServiceAnniversaryEnabled(Boolean serviceAnniversaryEnabled) {
        this.serviceAnniversaryEnabled = serviceAnniversaryEnabled;
    }

    public ServiceAnniversaryDTO getServiceAnniversary() {
        return serviceAnniversary;
    }

    public void setServiceAnniversary(ServiceAnniversaryDTO serviceAnniversary) {
        this.serviceAnniversary = serviceAnniversary;
    }

    public SeoDTO getSeo() {
        return seo;
    }

    public void setSeo(SeoDTO seo) {
        this.seo = seo;
    }

    public String getFromEmailAddress() {
        return fromEmailAddress;
    }

    public void setFromEmailAddress(String fromEmailAddress) {
        this.fromEmailAddress = fromEmailAddress;
    }

    public String getPrivacyBaseUrl() { return privacyBaseUrl; };

    public void setPrivacyBaseUrl(String privacyBaseUrl) { this.privacyBaseUrl = privacyBaseUrl; }

    public List<Element> getView() {
        return view;
    }

    public void setView(List<Element> view) {
        this.view = view;
    }
}