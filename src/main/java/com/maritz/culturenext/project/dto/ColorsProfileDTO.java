package com.maritz.culturenext.project.dto;

import java.util.Map;

import com.maritz.culturenext.groups.config.constants.GroupConfigConstants;

public class ColorsProfileDTO {
    
    private String primaryColor;
    private String secondaryColor;
    private String textOnPrimaryColor;
    
    public ColorsProfileDTO() {
        // TODO Auto-generated constructor stub
    }
    
    public ColorsProfileDTO(Map<String, Object> node) {
        this.primaryColor = (String) node.get(GroupConfigConstants.PRIMARY_COLOR);
        this.secondaryColor = (String) node.get(GroupConfigConstants.SECONDARY_COLOR);
        this.textOnPrimaryColor = (String) node.get(GroupConfigConstants.TEXT_ON_PRIMARY_COLOR);
    }
    
    public String getPrimaryColor() {
        return primaryColor;
    }
    public ColorsProfileDTO setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
        return this;
    }
    public String getSecondaryColor() {
        return secondaryColor;
    }
    public ColorsProfileDTO setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
        return this;
    }
    public String getTextOnPrimaryColor() {
        return textOnPrimaryColor;
    }
    public ColorsProfileDTO setTextOnPrimaryColor(String textOnPrimaryColor) {
        this.textOnPrimaryColor = textOnPrimaryColor;
        return this;
    }
}