package com.maritz.culturenext.project.dto;

public class EmailPreferencesDTO {
    
    private Boolean whenRecognized;
    private Boolean directRptRecognized;
    private Boolean approvalNeeded;
    private Boolean weeklyDigest;
    private Boolean notifyOthers;
    private Boolean milestoneReminder;

    public Boolean getWhenRecognized() {
        return whenRecognized;
    }
    public void setWhenRecognized(Boolean whenRecognized) {
        this.whenRecognized = whenRecognized;
    }
    public Boolean getDirectRptRecognized() {
        return directRptRecognized;
    }
    public void setDirectRptRecognized(Boolean directRptRecognized) {
        this.directRptRecognized = directRptRecognized;
    }
    public Boolean getApprovalNeeded() {
        return approvalNeeded;
    }
    public void setApprovalNeeded(Boolean approvalNeeded) {
        this.approvalNeeded = approvalNeeded;
    }
    public Boolean getWeeklyDigest() {
        return weeklyDigest;
    }
    public void setWeeklyDigest(Boolean weeklyDigest) {
        this.weeklyDigest = weeklyDigest;
    }
    public Boolean getNotifyOthers() {
        return notifyOthers;
    }
    public void setNotifyOthers(Boolean notifyOthers) {
        this.notifyOthers = notifyOthers;
    }
    public Boolean getMilestoneReminder() {
        return milestoneReminder;
    }
    public void setMilestoneReminder(Boolean milestoneReminder) {
        this.milestoneReminder = milestoneReminder;
    }
}