package com.maritz.culturenext.project.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.project.dao.ProjectsDao;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectsDaoImpl extends AbstractDaoImpl implements ProjectsDao {

    private static final String PROJECT_NUMBER = "projectNumber";

    private static final String ALL_PROJECT_NUMBERS_QUERY = "SELECT P.PROJECT_NUMBER FROM component.PROJECT P";

    @Override
    public List<String> getProjectNumbers() {
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(ALL_PROJECT_NUMBERS_QUERY,
                new CamelCaseMapRowMapper());

        List<String> projectNumbers = new ArrayList<>();
        for (Map<String, Object> node : nodes) {
            projectNumbers.add((String) node.get(PROJECT_NUMBER));
        }

        return projectNumbers;
    }
}
