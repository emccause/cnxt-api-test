package com.maritz.culturenext.project.dao;

import java.util.List;

public interface ProjectsDao {
    List<String> getProjectNumbers();
}
