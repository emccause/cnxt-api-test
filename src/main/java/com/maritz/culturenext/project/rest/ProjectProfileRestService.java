package com.maritz.culturenext.project.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.project.constants.ProjectProfileConstants;
import com.maritz.culturenext.project.dto.ProjectMessagesDTO;
import com.maritz.culturenext.project.dto.ProjectProfilePublicDTO;
import com.maritz.culturenext.project.services.ProjectProfileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "project",description = "Endpoint for projects")
public class ProjectProfileRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ProjectProfileService projectProfileService;

    //rest/project-profile
    @RequestMapping(value = "project-profile", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false)
    @ApiOperation(value = "Returns theme color info for the client passed in.")
    @ApiResponse(code = 200, message = "Successful retrieval of stats", response = ProjectProfilePublicDTO.class)
    public ProjectProfilePublicDTO getPublicProjectProfile() throws Exception {
        return projectProfileService.getProjectPublicInfoDTO();
    }

    //rest/project-profile
    @RequestMapping(value = "project-profile", method = RequestMethod.PUT)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, roles="ADMIN", impersonated=false)
    @ApiOperation(value = "Update project profile.")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful retrieval of stats", response = ProjectProfilePublicDTO.class),
        @ApiResponse(code = 400, message = "Request error")
    })
    public ProjectProfilePublicDTO putPublicProjectProfile(
        @ApiParam(value = "Project profile request body to update properties.")
        @RequestBody @Valid ProjectProfilePublicDTO projectProfileDTO
    ) {
        return projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), projectProfileDTO);
    }

    //rest/project-profile
    @RequestMapping(value = "project-profile", method = RequestMethod.PATCH)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, roles="ADMIN", impersonated=false)
    @ApiOperation(value = "Update project profile.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of stats", response = ProjectProfilePublicDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    public ProjectProfilePublicDTO patchPublicProjectProfile(
        @ApiParam(value = "Project profile request body to update properties.")
        @RequestBody @Valid ProjectProfilePublicDTO projectProfileDTO
    ) {
        return projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDTO);
    }

    //rest/messages
    @RequestMapping(value = "messages", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false)
    @ApiOperation(value = "Retrieve project-specific messages.",
        notes = "This is a publicly available endpoint that can be used to retrieve project/client messages for pages like login, "
        + "contact details etc.<br><br>Messages can contain some very basic html tags too.")
    @ApiResponse(code = 200, message = "Successful retrieval of messages", response = ProjectMessagesDTO.class)
    public ProjectMessagesDTO getPublicProjectMessages(
        @ApiParam(value = "Language Code for translation of Project-specific messages")
        @RequestParam(value = "languageCode", required = false) String languageCode
    ) {
    	languageCode = StringEscapeUtils.escapeHtml4(languageCode);
        return projectProfileService.getProjectMessages(languageCode);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorMessage>> handleEmailConstraintViolationError(MethodArgumentNotValidException manve) {
        List<ErrorMessage> errorList = new ArrayList<>();
        errorList.add(ProjectProfileConstants.INVALID_SUPPORT_EMAIL_FORMAT_ERROR);
        return new ResponseEntity<>(errorList, HttpStatus.BAD_REQUEST);
    }

}
