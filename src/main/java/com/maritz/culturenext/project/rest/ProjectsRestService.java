package com.maritz.culturenext.project.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.project.services.ProjectsService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class ProjectsRestService {

    @Inject private ProjectsService projectsService;

    //rest/award-project-numbers
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "award-project-numbers", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a list of award/abs project numbers setup for a client/project.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully returned the list of awawrds based on the current project.") })
    @Permission("PUBLIC")
    public List<String> getGroupInfo() throws Exception {

        return projectsService.getProjectNumbers();
    }
}