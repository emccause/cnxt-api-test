package com.maritz.culturenext.project.services.impl;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.project.dao.ProjectsDao;
import com.maritz.culturenext.project.services.ProjectsService;

import javax.inject.Inject;

import java.util.List;

@Component
public class ProjectsServiceImpl implements ProjectsService {

    @Inject private ProjectsDao projectsDao;

    @Override
    public List<String> getProjectNumbers() {
        return projectsDao.getProjectNumbers();
    }
}
