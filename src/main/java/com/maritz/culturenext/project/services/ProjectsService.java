package com.maritz.culturenext.project.services;

import java.util.List;

public interface ProjectsService {
    
    /**
     * Returns a list of project numbers that have been set up for the client.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/10190893/Award+Project+Numbers+Example
     */
    List<String> getProjectNumbers();
}
