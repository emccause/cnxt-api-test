package com.maritz.culturenext.project.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.validation.Valid;

import com.maritz.culturenext.project.dto.*;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.Country;
import com.maritz.core.jpa.entity.Language;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.dto.ActivityFeedFilterDTO;
import com.maritz.culturenext.dto.FilterDetailsDTO;
import com.maritz.culturenext.enums.ActivityFeedFilterEnum;
import com.maritz.culturenext.enums.PointBankEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.project.constants.ProjectProfileConstants;
import com.maritz.culturenext.project.services.ProjectProfileService;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.TranslationUtil;

@Component
public class ProjectProfileServiceImpl implements ProjectProfileService {

    public static final String TRUE = "true";
    private final Logger logger = LoggerFactory.getLogger(ProjectProfileServiceImpl.class.getName());

    @Inject private ApplicationDataService applicationDataService;
    @Inject private ConcentrixDao<ApplicationData> applicationDataDao;
    @Inject private ConcentrixDao<Country> countryDao;
    @Inject private ConcentrixDao<Language> languageDao;
    @Inject private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private Environment environment;
    @Inject private GroupConfigServiceDao groupConfigServiceDao;
    @Inject private Security security;
    @Inject private TranslationUtil translationUtil;
    
    @Override
    public ProjectProfilePublicDTO getProjectPublicInfoDTO() {

        ProjectProfilePublicDTO projectProfile = getProjectProfileProps();

        return projectProfile;
    }

    @Override
    public ProjectProfilePublicDTO updateProjectPublicInfo(String requestType, @Valid ProjectProfilePublicDTO projectProfile) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        // verify input values
        errors.addAll(verifyProjectProfileDetails(requestType, projectProfile));
        // handle any errors before updating
        ErrorMessageException.throwIfHasErrors(errors);

        // Update project profile properties
        saveApplicationDataProps(projectProfile);
        
        //Kick off the updating of the RIDEAU accounts (need to update status based on permissions)
        logger.info("Started - updateRideauAccountStatus");
        groupConfigServiceDao.updateRideauAccountStatus();
        logger.info("Finished - updateRideauAccountStatus");

        // retrieve ProjectProfilePublicDTO values
        return getProjectProfileProps();
    }

    @Override
    @Nonnull
    public ProjectMessagesDTO getProjectMessages(String languageCode) {
        List<ErrorMessage> errors = new ArrayList<>();

        // verify that the language code is well-formatted and project-enabled
        errors.addAll(verifyLanguageTranslation(languageCode));

        // handle any errors before proceeding
        ErrorMessageException.throwIfHasErrors(errors);

        // retrieve ProjectMessage values
        ProjectMessagesDTO projectMessages = retrieveProjectMessageDTO(languageCode);

        return projectMessages;
    }

    @Nonnull
    private ProjectMessagesDTO retrieveProjectMessageDTO(String languageCode) {
        ProjectMessagesDTO projectMessagesDTO = new ProjectMessagesDTO();

        List<String> applicationDataKeyNames = Arrays.asList(
                ApplicationDataConstants.KEY_NAME_ADMIN_HELP_TEXT,
                ApplicationDataConstants.KEY_NAME_LOGIN_WELCOME_MESSAGE, 
                ApplicationDataConstants.KEY_NAME_USERNAME_PLACEHOLDER,
                ApplicationDataConstants.KEY_NAME_PASSWORD_PLACEHOLDER, 
                ApplicationDataConstants.KEY_NAME_SSO_WELCOME_MESSAGE,
                ApplicationDataConstants.KEY_NAME_SSO_LOGOUT_MESSAGE, 
                ApplicationDataConstants.KEY_NAME_SSO_ERROR_MESSAGE,
                ApplicationDataConstants.KEY_NAME_SSO_LINK_TEXT,
                ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID);

        // retrieve the ApplicationData entries corresponding to ProjectMessage
        // values
        List<ApplicationData> entries = null;
        try {
            entries = applicationDataDao.findBy()
                    .where(ProjectConstants.KEY_NAME).in(applicationDataKeyNames)
                    .find();
        } catch (Exception e) {
            logger.error("Error occurred while retrieving ProjectMessage values.", e);
        }

        Map<Long, String> translations = fetchTranslations(languageCode, entries);

        // populate the DTO from the entries respectively
        if (entries != null && !MapUtils.isEmpty(translations)) {
            for (ApplicationData applicationData : entries) {
                switch (applicationData.getKeyName()) {
                    case ApplicationDataConstants.KEY_NAME_ADMIN_HELP_TEXT:
                        projectMessagesDTO.setAdminOverviewHelpText(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_LOGIN_WELCOME_MESSAGE:
                        projectMessagesDTO.setLoginWelcomeMessage(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_USERNAME_PLACEHOLDER:
                        projectMessagesDTO.setLoginUsernamePlaceholder(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_PASSWORD_PLACEHOLDER:
                        projectMessagesDTO.setLoginPasswordPlaceholder(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_SSO_WELCOME_MESSAGE:
                        projectMessagesDTO.setLoginSSOWelcomeMessage(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_SSO_LOGOUT_MESSAGE:
                        projectMessagesDTO.setLogoutSSOMessage(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_SSO_ERROR_MESSAGE:
                        projectMessagesDTO.setSsoAccessErrorMessage(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_SSO_LINK_TEXT:
                        projectMessagesDTO.setPartnerSSOLinkText(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    case ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID:
                        projectMessagesDTO.setEmployeeId(translations
                                .get(applicationData.getApplicationDataId()));
                        break;
                    default:
                        logger.warn("Retrieved ApplicationData entry not related to ProjectMessages.");
                }
            }
        }

        return projectMessagesDTO;
    }

    private Map<Long, String> fetchTranslations(String languageCode, List<ApplicationData> entries) {
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);

        Map<Long, String> translations = null;
        if (entries != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (ApplicationData entry : entries) {
                rowIds.add(entry.getApplicationDataId());
                defaultMap.put(entry.getApplicationDataId(), entry.getValue());
            }

            if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                return defaultMap;
            }

            translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode, 
                    TableName.APPLICATION_DATA.name(), TranslationConstants.COLUMN_NAME_VALUE, 
                    rowIds, defaultMap);
        }

        return translations;
    }

    /**
     * Verify project details properties.
     *
     * @param projectProfile
     * @return
     */
    private List<ErrorMessage> verifyProjectProfileDetails(@Nonnull String requestType, @Nonnull @Valid ProjectProfilePublicDTO projectProfile) {
        
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        //Do all NULL checks for PUT
        if (requestType.equals(ProjectConstants.PUT)) {
            if (projectProfile.getSupportEmail() == null) {
                errors.add(ProjectProfileConstants.MISSING_SUPPORT_EMAIL_ERROR_MESSAGE);
            }

            if (projectProfile.getSsoEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_SSO_ENABLED_MESSAGE);
            }

            if (projectProfile.getPartnerSsoUrl() == null) {
                errors.add(ProjectProfileConstants.MISSING_PARTNER_SSO_URL_MESSAGE);
            }

            if (projectProfile.getClientName() == null) {
                errors.add(ProjectProfileConstants.MISSING_CLIENT_NAME_MESSAGE);
            }

            if (projectProfile.getColors() != null) { 
                if (projectProfile.getColors().getPrimaryColor() == null) {
                    errors.add(ProjectProfileConstants.MISSING_PRIMARY_COLOR_MESSAGE);
                }

                if (projectProfile.getColors().getSecondaryColor() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SECONDARY_COLOR_MESSAGE);
                }

                if (projectProfile.getColors().getTextOnPrimaryColor() == null) {
                    errors.add(ProjectProfileConstants.MISSING_TEXT_PRIMARY_COLOR_MESSAGE);
                }
            }
            
            if (projectProfile.getServiceAnniversaryEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_SERVICE_ANNIVERSARY_ENABLED_MESSAGE);
            }
            
            if (Boolean.TRUE.equals(projectProfile.getServiceAnniversaryEnabled())
                    && projectProfile.getServiceAnniversary() == null
                ) {
                errors.add(ProjectProfileConstants.MISSING_SERVICE_ANNIVERSARY_PROVIDER_MESSAGE);
                errors.add(ProjectProfileConstants.MISSING_SERVICE_ANNIVERSARY_BASE_URL_MESSAGE);
            }
            else if (projectProfile.getServiceAnniversary() != null) {
                if (projectProfile.getServiceAnniversary().getProvider() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SERVICE_ANNIVERSARY_PROVIDER_MESSAGE);
                }

                if (projectProfile.getServiceAnniversary().getBaseUrl() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SERVICE_ANNIVERSARY_BASE_URL_MESSAGE);
                }
            }

            if (projectProfile.getLikingEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_LIKING_ENABLED_MESSAGE);
            }

            if (projectProfile.getCommentingEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_COMMENTING_ENABLED_MESSAGE);
            }

            if (projectProfile.getEngagementScoreEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_ENGAGEMENT_SCORE_ENABLED_MESSAGE);
            }

            if (projectProfile.getNetworkConnectionsEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_NETWORK_CONNECTIONS_ENABLED_MESSAGE);
            }

            if (projectProfile.getPointBank() == null) {
                errors.add(ProjectProfileConstants.MISSING_POINT_BANK_MESSAGE);
            }

            if (projectProfile.getReportAbuseAdmin() == null) {
                errors.add(ProjectProfileConstants.MISSING_REPORT_ABUSE_ADMIN_MESSAGE);
            }

            if (projectProfile.getCalendarEnabled() == null) {
                errors.add(ProjectProfileConstants.MISSING_CALENDER_ENABLED_MESSAGE);
            }

            if (CollectionUtils.isEmpty(projectProfile.getSiteLanguages())) {
                errors.add(ProjectProfileConstants.MISSING_SITE_LANGUAGES_MESSAGE);
            }
                        
            if (projectProfile.getEmailPreferences() != null) { 
                if (projectProfile.getEmailPreferences().getWhenRecognized() == null) {
                    errors.add(ProjectProfileConstants.MISSING_EMAIL_WHEN_RECOGNIZED_MESSAGE);
                }

                if (projectProfile.getEmailPreferences().getDirectRptRecognized() == null) {
                    errors.add(ProjectProfileConstants.MISSING_EMAIL_DIRECT_RPT_RECOGNIZED_MESSAGE);
                }

                if (projectProfile.getEmailPreferences().getApprovalNeeded() == null) {
                    errors.add(ProjectProfileConstants.MISSING_EMAIL_APPROVAL_NEEDED_MESSAGE);
                }
                
                if (projectProfile.getEmailPreferences().getWeeklyDigest() == null) {
                    errors.add(ProjectProfileConstants.MISSING_EMAIL_WEEKLY_DIGEST_MESSAGE);
                }

                if (projectProfile.getEmailPreferences().getNotifyOthers() == null) {
                    errors.add(ProjectProfileConstants.MISSING_NOTIFY_OTHERS);
                }
                
                if (projectProfile.getEmailPreferences().getMilestoneReminder() == null){
                    errors.add(ProjectProfileConstants.MISSING_MILESTONE_REMINDER);
                }
            }
            
            if (projectProfile.getTermsAndConditions() != null) { 
                if (projectProfile.getTermsAndConditions().getEnforceTCAtLogin() == null) {
                    errors.add(ProjectProfileConstants.MISSING_TC_ENFORCE_LOGIN_MESSAGE);
                }

                if (projectProfile.getTermsAndConditions().getShowInFooter() == null) {
                    errors.add(ProjectProfileConstants.MISSING_TC_SHOW_IN_FOOTER_MESSAGE);
                }

                if (projectProfile.getTermsAndConditions().getEditDate() == null
                        || projectProfile.getTermsAndConditions().getEditDate().isEmpty()) {
                    errors.add(ProjectProfileConstants.MISSING_TC_EDIT_DATE_MESSAGE);
                }
                if (projectProfile.getTermsAndConditions().getMajorChangeDate() == null
                        || projectProfile.getTermsAndConditions().getMajorChangeDate().isEmpty()) {
                    errors.add(ProjectProfileConstants.MISSING_TC_MAJOR_CHANGE_DATE_MESSAGE);
                }
            }

            if (projectProfile.getActivityFeedFilter() == null) {
                errors.add(ProjectProfileConstants.MISSING_ACTIVITY_FEED_FILTER_ERROR_MESSAGE);
            } else {
                if (projectProfile.getActivityFeedFilter().getDefaultFilter() == null) {
                    errors.add(ProjectProfileConstants.MISSING_DEFAULT_FILTER_ERROR_MESSAGE);
                }
                if (projectProfile.getActivityFeedFilter().getFilterOptions() == null) {
                    errors.add(ProjectProfileConstants.MISSING_FILTER_OPTIONS_ERROR_MESSAGE);
                }
                if (projectProfile.getActivityFeedFilter().getSearchEnabled() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SEARCH_ENABLED_ERROR_MESSAGE);
                }
            }
            
            if (projectProfile.getSeo() != null) {
                if (projectProfile.getSeo().getClientTitle() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SEO_CLIENT_TITLE_ERROR_MESSAGE);
                }
                if (projectProfile.getSeo().getClientDesc() == null) {
                    errors.add(ProjectProfileConstants.MISSING_SEO_CLIENT_DESC_ERROR_MESSAGE);
                }
            }
            
            if (projectProfile.getFromEmailAddress() == null) {
                errors.add(ProjectProfileConstants.MISSING_FROM_EMAIL_ADDRESS_ERROR_MESSAGE);
            }
        }
        
        //Throw any errors before continuing - no need to validate things that are NULL
        ErrorMessageException.throwIfHasErrors(errors);
        
        //Validations for both PUT and PATCH
        if (projectProfile.getSupportEmail() != null) {
            if (projectProfile.getSupportEmail().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_SUPPORT_EMAIL_MESSAGE);
            }
        }

        if (projectProfile.getPartnerSsoUrl() != null &&
                projectProfile.getPartnerSsoUrl().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
            errors.add(ProjectProfileConstants.LENGTH_PARTNER_SSO_URL_MESSAGE);
        }
        
        if (projectProfile.getClientName() != null && 
                projectProfile.getClientName().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
            errors.add(ProjectProfileConstants.LENGTH_CLIENT_NAME_MESSAGE);
        }

        if (projectProfile.getColors() != null) {
            if (projectProfile.getColors().getPrimaryColor() != null &&
                    projectProfile.getColors().getPrimaryColor().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_PRIMARY_COLOR_MESSAGE);
            }

            if (projectProfile.getColors().getSecondaryColor() != null &&
                    projectProfile.getColors().getSecondaryColor().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_SECONDARY_COLOR_MESSAGE);
            }

            if (projectProfile.getColors().getTextOnPrimaryColor() != null &&
                    projectProfile.getColors().getTextOnPrimaryColor().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_TEXT_PRIMARY_COLOR_MESSAGE);
            }
        }

        if (projectProfile.getPointBank() != null) {
            if (projectProfile.getPointBank().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_POINT_BLANK_ERROR_MESSAGE);
            } else if (PointBankEnum.getTypeByBank(projectProfile.getPointBank()) == null) {
                errors.add(ProjectProfileConstants.INVALID_POINT_BLANK_ERROR_MESSAGE);
            }
        }
        
        if (projectProfile.getReportAbuseAdmin() != null) { 
            Pax pax = paxDao.findById(projectProfile.getReportAbuseAdmin());
            if(pax == null) {
                errors.add(ProjectProfileConstants.NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID_ERROR_MESSAGE);
            }
        }
        
        if (projectProfile.getUnassignedApproverPaxId() != null) {
            Pax pax = paxDao.findById(projectProfile.getUnassignedApproverPaxId());
            if (pax == null) {
                errors.add(ProjectProfileConstants.NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID_ERROR_MESSAGE);
            }
        }

        if (projectProfile.getSiteLanguages() != null) {
            for (String siteLanguage : projectProfile.getSiteLanguages()) {
                String formattedLocale = LocaleUtil.formatLocaleCode(siteLanguage);
                if (formattedLocale == null) {
                    errors.add(new ErrorMessage()
                            .setCode(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE)
                            .setMessage(String.format(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE_MSG, siteLanguage))
                            .setField(ProjectConstants.SITE_LANGUAGES));
                } else if (Boolean.FALSE.equals(localeInfoDao.findBy().where(ProjectConstants.LOCALE_CODE).eq(formattedLocale).exists())) {
                    errors.add(new ErrorMessage()
                            .setCode(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE)
                            .setMessage(String.format(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE_MSG, siteLanguage))
                            .setField(ProjectConstants.SITE_LANGUAGES));
                }
            }
        }

        // Validate that the TC dates can't be saved with a value older than the previously saved date
        if (projectProfile.getTermsAndConditions() != null) {
            if (projectProfile.getTermsAndConditions().getEditDate() != null) {        
                Date editDateCurrent = DateUtil.convertFromString(
                        applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_EDIT_DATE).getValue());
                Date editDateNew = DateUtil.convertFromString(
                        projectProfile.getTermsAndConditions().getEditDate());
                
                if (editDateNew.before(editDateCurrent)) {
                    errors.add(ProjectProfileConstants.ERROR_TC_EDIT_DATE_BEFORE_CURRENT_VALUE_MESSAGE);
                }
            }
            
            if (projectProfile.getTermsAndConditions().getMajorChangeDate() != null
                    && !Boolean.FALSE.equals(projectProfile.getTermsAndConditions().getEnforceTCAtLogin())) { // enclose null and TRUE values.
                Date majorChangeDateCurrent = DateUtil.convertFromString(
                        applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_MAJOR_CHANGE_DATE).getValue());
                Date majorChangeDateNew = DateUtil.convertFromString(
                        projectProfile.getTermsAndConditions().getMajorChangeDate());
                
                if (majorChangeDateNew.before(majorChangeDateCurrent)) {
                    errors.add(ProjectProfileConstants.ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_VALUE_MESSAGE);
                }
            }
        }

        ActivityFeedFilterDTO activityFeedFilter = projectProfile.getActivityFeedFilter();
        if (activityFeedFilter != null) {
            List<String> filterNames = new ArrayList<>();
            if (activityFeedFilter.getFilterOptions() != null) {
                for (FilterDetailsDTO filterOption : activityFeedFilter.getFilterOptions()) {
                    String filterName = filterOption.getName();

                    // Prevent static filters from being disabled
                    if (ActivityFeedFilterEnum.getStaticFilterList().contains(filterName) &&
                        !filterOption.getEnabled()) {
                        errors.add(ProjectProfileConstants.CANNOT_DISABLE_STATIC_FILTER_OPTIONS_ERROR_MESSAGE);
                        break;
                    }

                    // Make sure the filter is active by making sure a row exists in APPLICATION_DATA for it
                    String filterOptionRow = environment.getProperty(
                        ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
                            ProjectConstants.DOT_DELIM +
                            filterName);

                    // Make sure the filter is a valid choice
                    if (!ActivityFeedFilterEnum.contains(filterName)) {
                        errors.add(ProjectProfileConstants.INVALID_FILTER_OPTIONS_ERROR_MESSAGE);
                        break;
                    // Make sure the filter is active
                    } else if (filterOptionRow == null) {
                        errors.add(ProjectProfileConstants.INACTIVE_FILTER_OPTIONS_ERROR_MESSAGE);
                        break;
                    }

                    // Keep a list of names in case the default filter is being set to one being enabled
                    filterNames.add(filterName);
                }
            }

            String defaultFilter = activityFeedFilter.getDefaultFilter();
            if (!StringUtils.isBlank(defaultFilter)) {
                String filterOptionValue = environment.getProperty(
                    ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
                        ProjectConstants.DOT_DELIM +
                        defaultFilter);

                // If the filter already exists, make sure it is enabled
                boolean filterEnabled = filterOptionValue == null ? false : Boolean.valueOf(filterOptionValue);

                // If the user is setting this filter to be enabled and the default in the same call
                if (filterNames.contains(defaultFilter)) {
                    for (FilterDetailsDTO filterOption : activityFeedFilter.getFilterOptions()) {
                        if (defaultFilter.equals(filterOption.getName())) {
                            filterEnabled = filterOption.getEnabled();
                        }
                    }
                }

                // If the filter chosen isn't active or enabled or a valid choice
                if (!filterEnabled || (!ActivityFeedFilterEnum.contains(defaultFilter))) {
                    errors.add(ProjectProfileConstants.INVALID_DEFAULT_FILTER_ERROR_MESSAGE);
                }
            }
        }

        if (projectProfile.getSeo() != null) {
            if (projectProfile.getSeo().getClientTitle() != null &&
                    projectProfile.getSeo().getClientTitle().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_SEO_CLIENT_TITLE_MESSAGE);
            }

            if (projectProfile.getSeo().getClientDesc() != null &&
                    projectProfile.getSeo().getClientDesc().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_SEO_CLIENT_DESC_MESSAGE);
            }
        }
        
        if (projectProfile.getFromEmailAddress() != null) {
            if (projectProfile.getFromEmailAddress().length() > ProjectProfileConstants.MAX_VALUE_1_SIZE) {
                errors.add(ProjectProfileConstants.LENGTH_FROM_EMAIL_ADDRESS_MESSAGE);
            }
        }

        return errors;
    }

    private List<ErrorMessage> verifyLanguageTranslation(String languageCode) {
        List<ErrorMessage> errors = new ArrayList<>();

        // Do not want error for no languageCode provide, will default
        if (languageCode == null) {
            return errors;
        }

        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);

        if (formattedLanguageCode == null) {
            errors.add(new ErrorMessage()
                    .setCode(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE)
                    .setMessage(String.format(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE_MSG, languageCode))
                    .setField(ProjectConstants.SITE_LANGUAGES));

            // Return the list here, as a null languageCode is no use to us in future validations
            return errors;
        }

        List<String> enabledLocales = localeInfoDao.findBy()
                .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find(ProjectConstants.LOCALE_CODE, String.class);

        if (!enabledLocales.contains(formattedLanguageCode)) {
            errors.add(new ErrorMessage()
                    .setCode(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE)
                    .setMessage(String.format(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE_MSG, languageCode))
                    .setField(ProjectConstants.SITE_LANGUAGES));
        }

        return errors;
    }

    /**
     * Retrieves the project URL.
     *
     * @return project URL
     */
    @CheckForNull
    private String getProjectUrl() {
        String projectUrl = null;

        try {
            projectUrl = environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL);
        } catch (Exception e) {
            logger.error("Error occurred while retrieving project URL.", e);
        }

        return projectUrl;
    }

    /**
     * Save project profile properties
     *
     * @param projectProfile
     */
    private void saveApplicationDataProps(ProjectProfilePublicDTO projectProfile) {
        
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME,
                                projectProfile.getClientName());
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN,
                                String.valueOf(projectProfile.getReportAbuseAdmin()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SUPPORT_EMAIL,
                                projectProfile.getSupportEmail());
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SSO_ENABLED,
                                String.valueOf(projectProfile.getSsoEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SSO_URL,
                                projectProfile.getPartnerSsoUrl());
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED,
                                String.valueOf(projectProfile.getLikingEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_LOGIN_HELP_MESSAGE_NO_TPL,
                                String.valueOf(projectProfile.getLoginHelpMessageNoTpl()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_REPORTING_ENABLED,
                                String.valueOf(projectProfile.getReportingEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED,
                                String.valueOf(projectProfile.getCommentingEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED,
                                String.valueOf(projectProfile.getEngagementScoreEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED,
                                String.valueOf(projectProfile.getNetworkConnectionsEnabled()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_POINT_BANK,
                                projectProfile.getPointBank());
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_NEW_RELIC_APP_ID,
                projectProfile.getNewRelicAppId());
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_CDN_URL,
                projectProfile.getCdnUrl());

        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_NEW_RELIC_APP_ID,
                projectProfile.getNewRelicAppId());

        if (projectProfile.getColors() != null) {
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR,
                                    projectProfile.getColors().getPrimaryColor());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR,
                                    projectProfile.getColors().getSecondaryColor());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR,
                                    projectProfile.getColors().getTextOnPrimaryColor());
        }
        
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_ENABLED,
                                String.valueOf(projectProfile.getServiceAnniversaryEnabled()));
        
        if (projectProfile.getServiceAnniversary() != null) {
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_PROVIDER,
                                    projectProfile.getServiceAnniversary().getProvider());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL,
                                    projectProfile.getServiceAnniversary().getBaseUrl());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_REDIRECT_LINK,
                                    projectProfile.getServiceAnniversary().getRedirectLink());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_PRODUCT_CODE,
                                    projectProfile.getServiceAnniversary().getProductCode());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_SUBCLIENT,
                                    projectProfile.getServiceAnniversary().getSubClient());
        }

        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER,
                                String.valueOf(projectProfile.getUnassignedApproverPaxId()));
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED,
                                String.valueOf(projectProfile.getCalendarEnabled()));
        
        if (projectProfile.getSiteLanguages() != null) {
            saveSiteLanguages(projectProfile.getSiteLanguages());
        }
        
        if (projectProfile.getEmailPreferences() != null) {
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_EMAIL_WHEN_RECOGNIZED,
                                    String.valueOf(projectProfile.getEmailPreferences().getWhenRecognized()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_EMAIL_DIRECT_RPT_RECOGNIZED,
                                    String.valueOf(projectProfile.getEmailPreferences().getDirectRptRecognized()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_EMAIL_APPROVAL_NEEDED,
                                    String.valueOf(projectProfile.getEmailPreferences().getApprovalNeeded()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_EMAIL_WEEKLY_DIGEST,
                                    String.valueOf(projectProfile.getEmailPreferences().getWeeklyDigest()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_NOTIFY_OTHERS,
                                    String.valueOf(projectProfile.getEmailPreferences().getNotifyOthers()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_MILESTONE_REMINDER, 
                                    String.valueOf(projectProfile.getEmailPreferences().getMilestoneReminder()));
        }
        
        if (projectProfile.getTermsAndConditions() != null) {
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_TC_ENFORCE_TC_AT_LOGIN,
                                    String.valueOf(projectProfile.getTermsAndConditions().getEnforceTCAtLogin()));
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_TC_SHOW_IN_FOOTER,
                                    String.valueOf(projectProfile.getTermsAndConditions().getShowInFooter()));
            
            // Converting and saving dates format as "yyyy-MM-dd HH:mm:ss" 
            if (projectProfile.getTermsAndConditions().getEditDate() != null) {
                Date tcEditDate = DateUtil.convertFromString(projectProfile.getTermsAndConditions().getEditDate());
                setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_TC_EDIT_DATE,
                                        DateUtil.convertToUTCDateComponentsFormat(tcEditDate));
            }
            
            if (projectProfile.getTermsAndConditions().getMajorChangeDate() != null
                    && !Boolean.FALSE.equals(projectProfile.getTermsAndConditions().getEnforceTCAtLogin())) { // enclose null and TRUE values.
                Date tcMajorChangeDate = DateUtil.convertFromString(projectProfile.getTermsAndConditions().getMajorChangeDate());
                setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_TC_MAJOR_CHANGE_DATE,
                                        DateUtil.convertToUTCDateComponentsFormat(tcMajorChangeDate));
            }
        }

        // Loop through the filters and apply the key to each name before saving
        ActivityFeedFilterDTO activityFeedFilter = projectProfile.getActivityFeedFilter();
        if (activityFeedFilter != null) {
            if (activityFeedFilter.getFilterOptions() != null) {
                for (FilterDetailsDTO filterOption : activityFeedFilter.getFilterOptions()) {
                    setApplicationDataByKey(
                        ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER + ProjectConstants.DOT_DELIM +
                            filterOption.getName(),
                        filterOption.getEnabled().toString());
                }
            }

            String defaultFilter = activityFeedFilter.getDefaultFilter();
            if (!StringUtils.isBlank(defaultFilter)) {
                setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT, defaultFilter);
            }

            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SEARCH_ENABLED, 
                    String.valueOf(projectProfile.getActivityFeedFilter().getSearchEnabled()));
        }
        
        if (projectProfile.getSeo() != null) {
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_TITLE,
                                    projectProfile.getSeo().getClientTitle());
            setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_DESC,
                                    projectProfile.getSeo().getClientDesc());
        }
        
        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_NOTIFICATION_FROM_EMAIL_ADDRESS,
                projectProfile.getFromEmailAddress());

        setApplicationDataByKey(ApplicationDataConstants.KEY_NAME_PRIVACY_BASE_URL,
                projectProfile.getPrivacyBaseUrl());
    }

    /**
     * Get all project profile properties
     *
     * @return
     */
    private ProjectProfilePublicDTO getProjectProfileProps() {
        ProjectProfilePublicDTO projectProfile = new ProjectProfilePublicDTO();

        projectProfile.setSupportEmail(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SUPPORT_EMAIL));
        projectProfile.setSsoEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED)));
        projectProfile.setPartnerSsoUrl(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_URL));
        
        projectProfile.setColors(new ColorsProfileDTO());
        projectProfile.getColors().setPrimaryColor(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR));
        projectProfile.getColors().setSecondaryColor(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR));
        projectProfile.getColors().setTextOnPrimaryColor(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR));
        
        projectProfile.setClientName(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME));
        projectProfile.setLikingEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED)));
        projectProfile.setLoginHelpMessageNoTpl(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_LOGIN_HELP_MESSAGE_NO_TPL)));
        projectProfile.setReportingEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_REPORTING_ENABLED)));
        projectProfile.setCommentingEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED)));
        projectProfile.setEngagementScoreEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED)));
        projectProfile.setNetworkConnectionsEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED)));
        projectProfile.setServiceAnniversaryEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_ENABLED)));
        projectProfile.setNewRelicAppId(environment.getProperty(ApplicationDataConstants.KEY_NAME_NEW_RELIC_APP_ID));
        projectProfile.setCdnUrl(environment.getProperty(ApplicationDataConstants.KEY_NAME_CDN_URL));
        projectProfile.setServiceAnniversary(new ServiceAnniversaryDTO());
        projectProfile.getServiceAnniversary().setProvider(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_PROVIDER));
        projectProfile.getServiceAnniversary().setBaseUrl(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL));
        projectProfile.getServiceAnniversary().setRedirectLink(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_REDIRECT_LINK));
        projectProfile.getServiceAnniversary().setProductCode(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_PRODUCT_CODE));
        projectProfile.getServiceAnniversary().setSubClient(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_SUBCLIENT));    
        
        projectProfile.setPointBank(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_POINT_BANK));

        if (security.getAuthToken() != null 
                && security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            
            try {
                projectProfile.setReportAbuseAdmin(Long.valueOf(environment.getProperty(
                        ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN)));
            } catch (NumberFormatException nfe) {
                projectProfile.setReportAbuseAdmin(null);
            }

            try{
                projectProfile.setUnassignedApproverPaxId(Long.valueOf(environment.getProperty(
                        ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER)));    
            } catch (NumberFormatException nfe) {
                projectProfile.setUnassignedApproverPaxId(null);
            }
            
        }
        projectProfile.setCalendarEnabled(Boolean.valueOf(environment.getProperty(
                ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED)));

        projectProfile.setSiteLanguages(
                localeInfoDao.findBy()
                    .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                    .find(ProjectConstants.LOCALE_CODE, String.class)
        );
        
        projectProfile.setEmailPreferences(new EmailPreferencesDTO());
        projectProfile.getEmailPreferences().setWhenRecognized(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_WHEN_RECOGNIZED)));
        projectProfile.getEmailPreferences().setDirectRptRecognized(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_DIRECT_RPT_RECOGNIZED)));
        projectProfile.getEmailPreferences().setApprovalNeeded(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_APPROVAL_NEEDED)));
        projectProfile.getEmailPreferences().setWeeklyDigest(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_WEEKLY_DIGEST)));
        projectProfile.getEmailPreferences().setNotifyOthers(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_NOTIFY_OTHERS)));
        projectProfile.getEmailPreferences().setMilestoneReminder(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_MILESTONE_REMINDER)));

        // Terms and conditions
        projectProfile.setTermsAndConditions(new TermsAndConditionsDTO());
        projectProfile.getTermsAndConditions().setEnforceTCAtLogin(
                Boolean.valueOf(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_ENFORCE_TC_AT_LOGIN).getValue()));
        projectProfile.getTermsAndConditions().setShowInFooter(
                Boolean.valueOf(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_SHOW_IN_FOOTER).getValue()));
        
        // Converting dates format to "yyyy-MM-dd'T'HH:mm'Z'"
        Date tcEditDate = DateUtil.convertFromString(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_EDIT_DATE).getValue());
        projectProfile.getTermsAndConditions().setEditDate(DateUtil.convertToUTCDateTime(tcEditDate));
        
        Date tcMajorChangeDate = DateUtil.convertFromString(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_MAJOR_CHANGE_DATE).getValue());
        projectProfile.getTermsAndConditions().setMajorChangeDate(DateUtil.convertToUTCDateTime(tcMajorChangeDate));

        List<FilterDetailsDTO> filterOptions = new ArrayList<>();
        for (String keyName : ActivityFeedFilterEnum.getKeyNameList()) {
            // Grab the filter value if found in APPLICATION_DATA
            String filterValue = environment.getProperty(keyName);

            if (filterValue != null) {
                FilterDetailsDTO filterOption = new FilterDetailsDTO();

                // Strip everything before the key name to get the filter name
                filterOption.setName(keyName.replace(
                    ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER + ProjectConstants.DOT_DELIM,
                    ProjectConstants.EMPTY_STRING));
                filterOption.setEnabled(Boolean.valueOf(filterValue));

                filterOptions.add(filterOption);
            }
        }

        ActivityFeedFilterDTO activityFeedFilterDTO = new ActivityFeedFilterDTO();
        activityFeedFilterDTO.setFilterOptions(filterOptions);
        activityFeedFilterDTO.setSearchEnabled(
                Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SEARCH_ENABLED)));
        activityFeedFilterDTO.setDefaultFilter(
            environment.getProperty(ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT));

        projectProfile.setActivityFeedFilter(activityFeedFilterDTO);
        
        projectProfile.setSeo(new SeoDTO());
        projectProfile.getSeo().setClientTitle(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_TITLE));
        projectProfile.getSeo().setClientDesc(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_DESC));
        
        projectProfile.setFromEmailAddress(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_NOTIFICATION_FROM_EMAIL_ADDRESS));

        projectProfile.setPrivacyBaseUrl(
                environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIVACY_BASE_URL));

        getViewElements(projectProfile);
        return projectProfile;
    }

    public ProjectProfilePublicDTO getViewElements(ProjectProfilePublicDTO projectProfile) {
        List<ApplicationData> viewElements = applicationDataDao.findBy()
                .where(ProjectConstants.KEY_NAME).startsWith(ApplicationDataConstants.PROJECT_PROFILE_VIEW)
                .find();

        if (viewElements != null && viewElements.size() > 0){
            List<Element> viewElementsLst = new ArrayList<>();
            for (ApplicationData viewElement : viewElements)  {
                Element element = new Element();
                element.setKey(
                        StringUtils.removeStartIgnoreCase(viewElement.getKeyName(),
                                ApplicationDataConstants.PROJECT_PROFILE_VIEW + ProjectConstants.DOT_DELIM));
                element.setEnabled(viewElement.getValue().equalsIgnoreCase(TRUE));
                viewElementsLst.add(element);
            }
            projectProfile.setView(viewElementsLst);
        }
        return projectProfile;
    }

    /**
     * Save or Update Application Data by key name
     *
     * @param key
     * @param newValue
     */
    private void setApplicationDataByKey(String key, String newValue) {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(key);

        // Add a new key
        if (appData == null || appData.getId() == null) {
            if (newValue != null && (newValue.equals("null") || newValue.isEmpty())) {
                newValue = null;
            }

            appData = new ApplicationDataDTO();
            appData.setKey(key);
            appData.setValue(newValue);
            applicationDataService.setApplicationData(appData);
        // Update the key
        } else if (newValue != null && !newValue.equals("null")) {
            if (newValue.isEmpty()) {
                newValue = null;
            }
            appData.setValue(newValue);
            applicationDataService.setApplicationData(appData);
        }
    }

    private void saveSiteLanguages(List<String> siteLanguages) {
        // Populate the language and country codes for future locales
        List<String> newLanguageCodes = new ArrayList<>();
        List<String> newCountryCodes = new ArrayList<>();

        for (String localeCode : siteLanguages) {
            newLanguageCodes.add(LocaleUtil.extractLanguageCodeFromLocaleCode(localeCode));
            newCountryCodes.add(LocaleUtil.extractCountryCodeFromLocaleCode(localeCode));
        }

        // Activate/deactivate languages
        List<Language> enabledLanguages = languageDao.findBy()
                .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();
        
        for (Language language : enabledLanguages) {
            language.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
            languageDao.save(language);
        }

        List<Language> newLanguages = languageDao.findBy()
                .where(ProjectConstants.LANGUAGE_CODE).in(newLanguageCodes)
                .find();
        
        for (Language language : newLanguages) {
            language.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
            languageDao.save(language);
        }

        // Activate/deactivate countries
        List<Country> enabledCountries = countryDao.findBy()
                .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();
        
        for (Country country : enabledCountries) {
            country.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
            countryDao.save(country);
        }

        List<Country> newCountries = countryDao.findBy()
                .where(ProjectConstants.COUNTRY_CODE).in(newCountryCodes)
                .find();
        
        for (Country country : newCountries) {
            country.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
            countryDao.save(country);
        }

        // Activate/deactivate localeInfo data
        List<LocaleInfo> enabledLocales = localeInfoDao.findBy()
                .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();
        
        for (LocaleInfo localeInfo : enabledLocales) {
            localeInfo.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
            localeInfoDao.save(localeInfo);
        }

        List<LocaleInfo> newLocales = localeInfoDao.findBy()
                .where(ProjectConstants.LOCALE_CODE).in(siteLanguages)
                .find();
        
        for (LocaleInfo localeInfo : newLocales) {
            localeInfo.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
            localeInfoDao.save(localeInfo);
        }
    }

}
