package com.maritz.culturenext.project.services;

import com.maritz.culturenext.project.dto.ProjectMessagesDTO;
import com.maritz.culturenext.project.dto.ProjectProfilePublicDTO;

public interface ProjectProfileService {
    
    /**
     * Return project profile details
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/21331987/Get+Project+Profile
     */
    ProjectProfilePublicDTO getProjectPublicInfoDTO();
    
    /**
     * Return project specific messages
     * 
     * @param languageCode
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12156993/Get+Project+Specific+Messages
     */
    ProjectMessagesDTO getProjectMessages(String languageCode);
    
    /**
     * Update project profile details
     * 
     * @param requestType
     * @param projectProfile
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/21331985/Update+Project+Profile
     */
    ProjectProfilePublicDTO updateProjectPublicInfo(String requestType, ProjectProfilePublicDTO projectProfile);

    ProjectProfilePublicDTO getViewElements(ProjectProfilePublicDTO projectProfile);
}

