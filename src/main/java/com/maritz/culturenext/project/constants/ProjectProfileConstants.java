package com.maritz.culturenext.project.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class ProjectProfileConstants {    

    public static final int MAX_VALUE_1_SIZE = 255;

    //ERROR MESSAGES
    public static final String ERROR_INVALID_DEFAULT_FILTER = "INVALID_DEFAULT_FILTER";
    public static final String ERROR_INVALID_DEFAULT_FILTER_MSG = "Default filter is not valid";
    public static final String ERROR_MISSING_DEFAULT_FILTER = "MISSING_DEFAULT_FILTER";
    public static final String ERROR_MISSING_DEFAULT_FILTER_MSG = "Default filter is missing";
    public static final String ERROR_INACTIVE_FILTER_OPTIONS = "INACTIVE_FILTER_OPTIONS";
    public static final String ERROR_INACTIVE_FILTER_OPTIONS_MSG = "Filter options contain an inactive group config";
    public static final String ERROR_INVALID_FILTER_OPTIONS = "INVALID_FILTER_OPTIONS";
    public static final String ERROR_INVALID_FILTER_OPTIONS_MSG = "Filter options are not valid";
    public static final String ERROR_MISSING_FILTER_OPTIONS = "MISSING_FILTER_OPTIONS";
    public static final String ERROR_MISSING_FILTER_OPTIONS_MSG = "Filter options are missing";
    public static final String ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS = "CANNOT_DISABLE_STATIC_FILTER_OPTIONS";
    public static final String ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS_MSG = "Static filter options cannot be disabled";
    public static final String ERROR_MISSING_ACTIVITY_FEED_FILTER = "MISSING_ACTIVITY_FEED_FILTER";
    public static final String ERROR_MISSING_ACTIVITY_FEED_FILTER_MSG = "Activity Feed filters are not set up";
    public static final String ERROR_MISSING_POINT_BANK = "MISSING_POINT_BANK";
    public static final String ERROR_MISSING_POINT_BANK_MSG = "Point bank is not set up";
    public static final String ERROR_INVALID_POINT_BANK = "INVALID_POINT_BANK";
    public static final String ERROR_INVALID_POINT_BANK_MSG = "Point bank is not valid";
    public static final String ERROR_LENGTH_POINT_BANK = "POINT_BANK_LENGTH";
    public static final String ERROR_LENGTH_POINT_BANK_MSG = "Point bank is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_SUPPORT_EMAIL = "MISSING_SUPPORT_EMAIL";
    public static final String ERROR_MISSING_SUPPORT_EMAIL_MSG = "Support email is not set up";
    public static final String ERROR_LENGTH_SUPPORT_EMAIL = "SUPPORT_EMAIL_LENGTH";
    public static final String ERROR_LENGTH_SUPPORT_EMAIL_MSG = "Support email is too long. Max length is " + MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_SSO_ENABLED = "MISSING_SSO_ENABLED";
    public static final String ERROR_MISSING_SSO_ENABLED_MSG = "SSO enabled flag is not set up";
    public static final String ERROR_MISSING_PARTNER_SSO_URL = "MISSING_PARTNER_SSO_URL";
    public static final String ERROR_MISSING_PARTNER_SSO_URL_MSG = "Partner SSO URL is not set up";
    public static final String ERROR_LENGTH_PARTNER_SSO_URL = "PARTNER_SSO_URL_LENGTH";
    public static final String ERROR_LENGTH_PARTNER_SSO_URL_MSG = "Partner SSO URL is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_CLIENT_NAME = "MISSING_CLIENT_NAME";
    public static final String ERROR_MISSING_CLIENT_NAME_MSG = "Client display name is not set up";
    public static final String ERROR_LENGTH_CLIENT_NAME = "CLIENT_NAME_LENGTH";
    public static final String ERROR_LENGTH_CLIENT_NAME_MSG = "Client name is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_PRIMARY_COLOR = "MISSING_PRIMARY_COLOR";
    public static final String ERROR_MISSING_PRIMARY_COLOR_MSG = "Primary color is not set up";
    public static final String ERROR_LENGTH_PRIMARY_COLOR = "PRIMARY_COLOR_LENGTH";
    public static final String ERROR_LENGTH_PRIMARY_COLOR_MSG = "Primary color is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_SECONDARY_COLOR = "MISSING_SECONDARY_COLOR";
    public static final String ERROR_MISSING_SECONDARY_COLOR_MSG = "Secondary color is not set up";
    public static final String ERROR_LENGTH_SECONDARY_COLOR = "SECONDARY_COLOR_LENGTH";
    public static final String ERROR_LENGTH_SECONDARY_COLOR_MSG = "Secondary color is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_TEXT_PRIMARY_COLOR = "MISSING_TEXT_ON_PRIMARY_COLOR";
    public static final String ERROR_MISSING_TEXT_PRIMARY_COLOR_MSG = "Text on primary color is not set up";
    public static final String ERROR_LENGTH_TEXT_PRIMARY_COLOR = "TEXT_ON_PRIMARY_COLOR_LENGTH";
    public static final String ERROR_LENGTH_TEXT_PRIMARY_COLOR_MSG = "Text on primary color is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_REPORT_ABUSE_ADMIN = "MISSING_REPORT_ABUSE_ADMIN";
    public static final String ERROR_MISSING_REPORT_ABUSE_ADMIN_MSG = "Report abuse admin is not set up";
    public static final String ERROR_NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID = "NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID";
    public static final String ERROR_NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID_MSG = "Unassigned Approver Pax Id does not exist or is inactive";
    public static final String ERROR_MISSING_LIKING_ENABLED = "MISSING_LIKING_ENABLED";
    public static final String ERROR_MISSING_LIKING_ENABLED_MSG = "Liking enabled flag is not set up";
    public static final String ERROR_MISSING_COMMENTING_ENABLED = "MISSING_COMMENTING_ENABLED";
    public static final String ERROR_MISSING_COMMENTING_ENABLED_MSG = "Commenting enabled flag is not set up";
    public static final String ERROR_MISSING_ENGAGEMENT_SCORE_ENABLED = "MISSING_ENGAGEMENT_SCORE_ENABLED";
    public static final String ERROR_MISSING_ENGAGEMENT_SCORE_ENABLED_MSG = "Engagement score enabled flag is not set up";
    public static final String ERROR_MISSING_SITE_LANGUAGES = "MISSING_SITE_LANGUAGES";
    public static final String ERROR_MISSING_SITE_LANGUAGES_MSG = "Site languages are not set up";
    public static final String ERROR_INVALID_SITE_LANGUAGE = "INVALID_SITE_LANGUAGE";
    public static final String ERROR_INVALID_SITE_LANGUAGE_MSG = "%s site language is invalid";
    public static final String NETWORK_CONNECTIONS_ENABLED = "networkConnectionsEnabled";
    public static final String ERROR_MISSING_NETWORK_CONNECTIONS_ENABLED = "MISSING_NETWORK_CONNECTIONS_ENABLED";
    public static final String ERROR_MISSING_NETWORK_CONNECTIONS_ENABLED_MSG = "Network connections flag is missing";
    public static final String ERROR_MISSING_CALENDAR_ENABLED = "MISSING_CALENDAR_ENABLED";
    public static final String ERROR_MISSING_CALENDAR_ENABLED_MSG = "Calendar enabled flag is not set up";
    public static final String ERROR_MISSING_EMAIL_WHEN_RECOGNIZED = "MISSING_WHEN_RECOGNIZED";
    public static final String ERROR_MISSING_EMAIL_WHEN_RECONIZED_MSG = "Email preferece for 'when recognized' is not set up";
    public static final String EMAIL_WHEN_RECOGNIZED = "whenRecognized";
    public static final String ERROR_MISSING_EMAIL_DIRECT_RPT_RECOGNIZED = "MISSING_DIRECT_RPT_RECOGNIZED";
    public static final String ERROR_MISSING_EMAIL_DIRECT_RPT_RECOGNIZED_MSG = "Email preferece for 'direct report recognized' is not set up";
    public static final String EMAIL_DIRECT_RPT_RECOGNIZED = "directRptRecognized";
    public static final String ERROR_MISSING_EMAIL_APPROVAL_NEEDED = "MISSING_APPROVAL_NEEDED";
    public static final String ERROR_MISSING_EMAIL_APPROVAL_NEEDED_MSG = "Email preferece for 'approval needed' is not set up";
    public static final String EMAIL_APPROVAL_NEEDED = "approvalNeeded";
    public static final String ERROR_MISSING_EMAIL_WEEKLY_DIGEST = "MISSING_WEEKLY_DIGEST";
    public static final String ERROR_MISSING_EMAIL_WEEKLY_DIGEST_MSG = "Email preference for 'weekly digest' is not set up";
    public static final String EMAIL_WEEKLY_DIGEST = "weeklyDigest";
    public static final String ERROR_MISSING_NOTIFY_OTHERS = "MISSING_NOTIFY_OTHERS";
    public static final String ERROR_MISSING_NOTIFY_OTHERS_MSG = "Email preference for 'notify others' is not set up";
    public static final String NOTIFY_OTHERS = "notifyOthers";
    public static final String ERROR_MISSING_TC_ENFORCE_LOGIN_CODE = "MISSING_TC_ENFORCE_LOGIN_CODE";
    public static final String ERROR_MISSING_TC_ENFORCE_LOGIN_MSG = "Terms and conditions preference for enable T&C is missing";
    public static final String TC_ENFORCE_LOGIN_FIELD = "enable";
    public static final String ERROR_MISSING_TC_SHOW_IN_FOOTER_CODE = "MISSING_TC_SHOW_IN_FOOTER_CODE";
    public static final String ERROR_MISSING_TC_SHOW_IN_FOOTER_MSG = "Terms and conditions preference for showInFooter is missing";
    public static final String TC_SHOW_IN_FOOTER_FIELD = "showInFooter";
    public static final String ERROR_MISSING_TC_EDIT_DATE_CODE = "MISSING_TC_EDIT_DATE_CODE";
    public static final String ERROR_MISSING_TC_EDIT_DATE_MSG = "Terms and conditions preference for editDate is missing";
    public static final String TC_EDIT_DATE_FIELD = "editDate";
    public static final String ERROR_MISSING_TC_MAJOR_CHANGE_DATE_CODE = "MISSING_TC_MAJOR_CHANGE_DATE_CODE";
    public static final String ERROR_MISSING_TC_MAJOR_CHANGE_DATE_MSG = "Terms and conditions preference for majorChangeDate is missing";
    public static final String TC_MAJOR_CHANGE_DATE_FIELD = "majorChangeDate";
    public static final String ERROR_TC_EDIT_DATE_BEFORE_CURRENT_CODE = "ERROR_TC_EDIT_DATE_BEFORE_CURRENT_CODE";
    public static final String ERROR_TC_EDIT_DATE_BEFORE_CURRENT_MSG = 
            "Terms and conditions edit date is older than previously saved date";
    public static final String ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_CODE = "ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_CODE";
    public static final String ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_MSG = 
            "Terms and conditions major change date is older than previously saved date";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_ENABLED = "MISSING_SERVICE_ANNIVERSARY_ENABLED";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_ENABLED_MSG = "Service Anniversary Enabled flag is missing";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_PROVIDER = "MISSING_SERVICE_ANNIVERSARY_PROVIDER";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_PROVIDER_MSG = "Service Anniversary Provider field is null";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_BASE_URL = "MISSING_SERVICE_ANNIVERSARY_BASE_URL";
    public static final String ERROR_MISSING_SERVICE_ANNIVERSARY_BASE_URL_MSG = "Service Anniversary BaseUrl field is null";
    public static final String ERROR_NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID = "NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID";
    public static final String ERROR_NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID_MSG = "Report Abuse Admin Pax Id does not exist or is inactive";
    public static final String ERROR_MISSING_AWARDS_OBJECT = "MISSING_AWARDS_OBJECT";
    public static final String ERROR_MISSING_AWARDS_OBJECT_MSG = "The awards object is missing from the request body";
    public static final String INVALID_SUPPORT_EMAIL_FORMAT_MESSAGE = "Invalid support email format";
    public static final String INVALID_SUPPORT_EMAIL_FORMAT_CODE = "INVALID_SUPPORT_EMAIL_FORMAT";
    public static final String ERROR_MISSING_SEARCH_ENABLED_CODE = "MISSING_SEARCH_ENABLED";
    public static final String ERROR_MISSING_SEARCH_ENABLED_MESSAGE = "Activity feed filter search enabled is missing";
    public static final String ERROR_MISSING_SEO_CLIENT_TITLE_CODE = "MISSING_SEO_CLIENT_TITLE";
    public static final String ERROR_MISSING_SEO_CLIENT_TITLE_MESSAGE = "SEO Client Title is missing";
    public static final String ERROR_LENGTH_SEO_CLIENT_TITLE = "SEO_CLIENT_TITLE_LENGTH";
    public static final String ERROR_LENGTH_SEO_CLIENT_TITLE_MSG = "SEO Client Title is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String SEO_CLIENT_TITLE_FIELD = "clientTitle";
    public static final String ERROR_MISSING_SEO_CLIENT_DESC_CODE = "MISSING_SEO_CLIENT_DESC";
    public static final String ERROR_MISSING_SEO_CLIENT_DESC_MESSAGE = "SEO Client Description is missing";
    public static final String ERROR_LENGTH_SEO_CLIENT_DESC = "SEO_CLIENT_DESC_LENGTH";
    public static final String ERROR_LENGTH_SEO_CLIENT_DESC_MSG = "SEO Client Description is too long. Max length is "+ MAX_VALUE_1_SIZE;
    public static final String SEO_CLIENT_DESC_FIELD = "clientDesc";
    public static final String ERROR_MISSING_FROM_EMAIL_ADDRESS = "MISSING_FROM_EMAIL_ADDRESS";
    public static final String ERROR_MISSING_FROM_EMAIL_ADDRESS_MSG = "Sending email address is not set up";
    public static final String ERROR_LENGTH_FROM_EMAIL_ADDRESS = "FROM_EMAIL_ADDRESS_LENGTH";
    public static final String ERROR_LENGTH_FROM_EMAIL_ADDRESS_MSG = "Sending email address is too long. Max length is " + MAX_VALUE_1_SIZE;
    public static final String ERROR_MISSING_MILESTONE_REMINDER = "MISSING_MILESTONE_REMINDER";

    
    //ErrorMessage Constants
    public static final ErrorMessage MISSING_ACTIVITY_FEED_FILTER_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_ACTIVITY_FEED_FILTER)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_ACTIVITY_FEED_FILTER_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage INVALID_DEFAULT_FILTER_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_INVALID_DEFAULT_FILTER)
        .setMessage(ProjectProfileConstants.ERROR_INVALID_DEFAULT_FILTER_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage MISSING_DEFAULT_FILTER_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_DEFAULT_FILTER)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_DEFAULT_FILTER_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage INACTIVE_FILTER_OPTIONS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_INACTIVE_FILTER_OPTIONS)
        .setMessage(ProjectProfileConstants.ERROR_INACTIVE_FILTER_OPTIONS_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage INVALID_FILTER_OPTIONS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_INVALID_FILTER_OPTIONS)
        .setMessage(ProjectProfileConstants.ERROR_INVALID_FILTER_OPTIONS_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage MISSING_FILTER_OPTIONS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_FILTER_OPTIONS)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_FILTER_OPTIONS_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage CANNOT_DISABLE_STATIC_FILTER_OPTIONS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS)
        .setMessage(ProjectProfileConstants.ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS_MSG)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);

    public static final ErrorMessage MISSING_SUPPORT_EMAIL_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SUPPORT_EMAIL)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SUPPORT_EMAIL_MSG)
        .setField(ProjectConstants.SUPPORT_EMAIL);
    
    public static final ErrorMessage MISSING_SSO_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SSO_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SSO_ENABLED_MSG)
        .setField(ProjectConstants.SSO_ENABLED);
    
    public static final ErrorMessage MISSING_PARTNER_SSO_URL_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_PARTNER_SSO_URL)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_PARTNER_SSO_URL_MSG)
        .setField(ProjectConstants.PARTNER_SSO_URL);
    
    public static final ErrorMessage MISSING_CLIENT_NAME_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_CLIENT_NAME)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_CLIENT_NAME_MSG)
        .setField(ProjectConstants.CLIENT_NAME);
    
    public static final ErrorMessage MISSING_PRIMARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_PRIMARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_PRIMARY_COLOR_MSG)
        .setField(ProjectConstants.PRIMARY_COLOR);
    
    public static final ErrorMessage MISSING_SECONDARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SECONDARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SECONDARY_COLOR_MSG)
        .setField(ProjectConstants.SECONDARY_COLOR);
    
    public static final ErrorMessage MISSING_TEXT_PRIMARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_TEXT_PRIMARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_TEXT_PRIMARY_COLOR_MSG)
        .setField(ProjectConstants.TEXT_PRIMARY_COLOR);
    
    public static final ErrorMessage MISSING_LIKING_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_LIKING_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_LIKING_ENABLED_MSG)
        .setField(ProjectConstants.LIKING_ENABLED);
    
    public static final ErrorMessage MISSING_COMMENTING_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_COMMENTING_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_COMMENTING_ENABLED_MSG)
        .setField(ProjectConstants.COMMENTING_ENABLED);
    
    public static final ErrorMessage MISSING_ENGAGEMENT_SCORE_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_ENGAGEMENT_SCORE_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_ENGAGEMENT_SCORE_ENABLED_MSG)
        .setField(ProjectConstants.ENGAGEMENT_SCORE_ENABLED);
    
    public static final ErrorMessage MISSING_NETWORK_CONNECTIONS_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_NETWORK_CONNECTIONS_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_NETWORK_CONNECTIONS_ENABLED_MSG)
        .setField(ProjectProfileConstants.NETWORK_CONNECTIONS_ENABLED);
    
    public static final ErrorMessage MISSING_POINT_BANK_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_POINT_BANK)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_POINT_BANK_MSG)
        .setField(ProjectConstants.POINT_BANK);
    
    public static final ErrorMessage MISSING_REPORT_ABUSE_ADMIN_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_REPORT_ABUSE_ADMIN)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_REPORT_ABUSE_ADMIN_MSG)
        .setField(ProjectConstants.REPORT_ABUSE_ADMIN);
    
    public static final ErrorMessage MISSING_CALENDER_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_CALENDAR_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_CALENDAR_ENABLED_MSG)
        .setField(ProjectConstants.CALENDAR_ENABLED);
    
    public static final ErrorMessage MISSING_SITE_LANGUAGES_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SITE_LANGUAGES)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SITE_LANGUAGES_MSG)
        .setField(ProjectConstants.SITE_LANGUAGES);
    
    public static final ErrorMessage MISSING_EMAIL_WHEN_RECOGNIZED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_EMAIL_WHEN_RECOGNIZED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_EMAIL_WHEN_RECONIZED_MSG)
        .setField(ProjectProfileConstants.EMAIL_WHEN_RECOGNIZED);
    
    public static final ErrorMessage MISSING_EMAIL_DIRECT_RPT_RECOGNIZED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_EMAIL_DIRECT_RPT_RECOGNIZED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_EMAIL_DIRECT_RPT_RECOGNIZED_MSG)
        .setField(ProjectProfileConstants.EMAIL_DIRECT_RPT_RECOGNIZED);
    
    public static final ErrorMessage MISSING_EMAIL_APPROVAL_NEEDED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_EMAIL_APPROVAL_NEEDED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_EMAIL_APPROVAL_NEEDED_MSG)
        .setField(ProjectProfileConstants.EMAIL_APPROVAL_NEEDED);
    
    public static final ErrorMessage MISSING_EMAIL_WEEKLY_DIGEST_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_EMAIL_WEEKLY_DIGEST)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_EMAIL_WEEKLY_DIGEST_MSG)
        .setField(ProjectProfileConstants.EMAIL_WEEKLY_DIGEST);

    public static final ErrorMessage MISSING_NOTIFY_OTHERS = new ErrorMessage()
            .setCode(ProjectProfileConstants.ERROR_MISSING_NOTIFY_OTHERS)
            .setMessage(ProjectProfileConstants.ERROR_MISSING_NOTIFY_OTHERS_MSG)
            .setField(ProjectProfileConstants.NOTIFY_OTHERS);
    
    public static final ErrorMessage LENGTH_SUPPORT_EMAIL_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_SUPPORT_EMAIL)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_SUPPORT_EMAIL_MSG)
        .setField(ProjectConstants.SUPPORT_EMAIL);
    
    public static final ErrorMessage LENGTH_PARTNER_SSO_URL_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_PARTNER_SSO_URL)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_PARTNER_SSO_URL_MSG)
        .setField(ProjectConstants.SUPPORT_EMAIL);
    
    public static final ErrorMessage LENGTH_CLIENT_NAME_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_CLIENT_NAME)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_CLIENT_NAME_MSG)
        .setField(ProjectConstants.CLIENT_NAME);
    
    public static final ErrorMessage LENGTH_PRIMARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_PRIMARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_PRIMARY_COLOR_MSG)
        .setField(ProjectConstants.PRIMARY_COLOR);
    
    public static final ErrorMessage LENGTH_SECONDARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_SECONDARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_SECONDARY_COLOR_MSG)
        .setField(ProjectConstants.SECONDARY_COLOR);
    
    public static final ErrorMessage LENGTH_TEXT_PRIMARY_COLOR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_TEXT_PRIMARY_COLOR)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_TEXT_PRIMARY_COLOR_MSG)
        .setField(ProjectConstants.TEXT_PRIMARY_COLOR);
    
    public static final ErrorMessage LENGTH_POINT_BLANK_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_POINT_BANK)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_POINT_BANK_MSG)
        .setField(ProjectConstants.POINT_BANK);
    
    public static final ErrorMessage INVALID_POINT_BLANK_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_INVALID_POINT_BANK)
        .setMessage(ProjectProfileConstants.ERROR_INVALID_POINT_BANK_MSG)
        .setField(ProjectConstants.POINT_BANK);
    
    public static final ErrorMessage NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID_MSG)
        .setMessage(ProjectProfileConstants.ERROR_NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID)
        .setField(ProjectConstants.UNASSIGNED_APPROVER_PAX_ID);
    
    public static final ErrorMessage MISSING_TC_ENFORCE_LOGIN_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_TC_ENFORCE_LOGIN_CODE)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_TC_ENFORCE_LOGIN_MSG)
        .setField(ProjectProfileConstants.TC_ENFORCE_LOGIN_FIELD);
    
    public static final ErrorMessage MISSING_TC_SHOW_IN_FOOTER_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_TC_SHOW_IN_FOOTER_CODE)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_TC_SHOW_IN_FOOTER_MSG)
        .setField(ProjectProfileConstants.TC_SHOW_IN_FOOTER_FIELD);

    public static final ErrorMessage MISSING_TC_EDIT_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_TC_EDIT_DATE_CODE)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_TC_EDIT_DATE_MSG)
        .setField(ProjectProfileConstants.TC_EDIT_DATE_FIELD);

    public static final ErrorMessage MISSING_TC_MAJOR_CHANGE_DATE_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_TC_MAJOR_CHANGE_DATE_CODE)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_TC_MAJOR_CHANGE_DATE_MSG)
        .setField(ProjectProfileConstants.TC_MAJOR_CHANGE_DATE_FIELD);
    
    public static final ErrorMessage ERROR_TC_EDIT_DATE_BEFORE_CURRENT_VALUE_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_TC_EDIT_DATE_BEFORE_CURRENT_CODE)
        .setMessage(ProjectProfileConstants.ERROR_TC_EDIT_DATE_BEFORE_CURRENT_MSG)
        .setField(ProjectProfileConstants.TC_EDIT_DATE_FIELD);
    
    public static final ErrorMessage ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_VALUE_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_CODE)
        .setMessage(ProjectProfileConstants.ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_MSG)
        .setField(ProjectProfileConstants.TC_MAJOR_CHANGE_DATE_FIELD);
    
    public static final ErrorMessage MISSING_SERVICE_ANNIVERSARY_ENABLED_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_ENABLED)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_ENABLED_MSG)
        .setField(ProjectConstants.SERVICE_ANNIVERSARY_ENABLED);
    
    public static final ErrorMessage MISSING_SERVICE_ANNIVERSARY_PROVIDER_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_PROVIDER)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_PROVIDER_MSG)
        .setField(ProjectConstants.SERVICE_ANNIVERSARY_ENABLED);
    
    public static final ErrorMessage MISSING_SERVICE_ANNIVERSARY_BASE_URL_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_BASE_URL)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_BASE_URL_MSG)
        .setField(ProjectConstants.SERVICE_ANNIVERSARY_BASE_URL);
    
    public static final ErrorMessage NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID_MSG)
        .setMessage(ProjectProfileConstants.ERROR_NONEXISTENT_REPORT_ABUSE_ADMIN_PAX_ID)
        .setField(ProjectConstants.REPORT_ABUSE_ADMIN);
    
    public static final ErrorMessage MISSING_AWARDS_OBJECT_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_AWARDS_OBJECT)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_AWARDS_OBJECT_MSG)
        .setField(ProjectConstants.AWARDS);
    
    public static final ErrorMessage INVALID_SUPPORT_EMAIL_FORMAT_ERROR = new ErrorMessage()
        .setCode(INVALID_SUPPORT_EMAIL_FORMAT_CODE)
        .setMessage(INVALID_SUPPORT_EMAIL_FORMAT_MESSAGE)
        .setField(ProjectConstants.SUPPORT_EMAIL);
    
    public static final ErrorMessage MISSING_SEARCH_ENABLED_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SEARCH_ENABLED_CODE)
        .setMessage(ERROR_MISSING_SEARCH_ENABLED_MESSAGE)
        .setField(ProjectConstants.ACTIVITY_FEED_FILTERS);
    
    public static final ErrorMessage MISSING_SEO_CLIENT_TITLE_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SEO_CLIENT_TITLE_CODE)
        .setMessage(ERROR_MISSING_SEO_CLIENT_TITLE_MESSAGE)
        .setField(ProjectProfileConstants.SEO_CLIENT_TITLE_FIELD);

    public static final ErrorMessage MISSING_SEO_CLIENT_DESC_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SEO_CLIENT_DESC_CODE)
        .setMessage(ERROR_MISSING_SEO_CLIENT_DESC_MESSAGE)
        .setField(ProjectProfileConstants.SEO_CLIENT_DESC_FIELD);
    
    public static final ErrorMessage LENGTH_SEO_CLIENT_TITLE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_LENGTH_SEO_CLIENT_TITLE)
        .setMessage(ERROR_LENGTH_SEO_CLIENT_TITLE_MSG)
        .setField(ProjectProfileConstants.SEO_CLIENT_TITLE_FIELD);
    
    public static final ErrorMessage LENGTH_SEO_CLIENT_DESC_MESSAGE = new ErrorMessage()
        .setCode(ERROR_LENGTH_SEO_CLIENT_DESC)
        .setMessage(ERROR_LENGTH_SEO_CLIENT_DESC_MSG)
        .setField(ProjectProfileConstants.SEO_CLIENT_DESC_FIELD);

    public static final ErrorMessage MISSING_FROM_EMAIL_ADDRESS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_FROM_EMAIL_ADDRESS)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_FROM_EMAIL_ADDRESS_MSG)
        .setField(ProjectConstants.FROM_EMAIL_ADDRESS);
    
    public static final ErrorMessage LENGTH_FROM_EMAIL_ADDRESS_MESSAGE = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_LENGTH_FROM_EMAIL_ADDRESS)
        .setMessage(ProjectProfileConstants.ERROR_LENGTH_FROM_EMAIL_ADDRESS_MSG)
        .setField(ProjectConstants.FROM_EMAIL_ADDRESS);


    public static final ErrorMessage MISSING_MILESTONE_REMINDER = new ErrorMessage()
        .setCode(ProjectProfileConstants.ERROR_MISSING_MILESTONE_REMINDER)
        .setMessage(ProjectProfileConstants.ERROR_MISSING_MILESTONE_REMINDER)
        .setField(ProjectConstants.MILESTONE_REMINDER);;
}