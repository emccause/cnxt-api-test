package com.maritz.culturenext.activityfeed.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.activityfeed.dao.ActivityFeedInfoDao;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ActivityFeedInfoDaoImpl extends AbstractDaoImpl implements ActivityFeedInfoDao {

    private static final String LOGGED_IN_PAX_ID_PARAM = "LOGGED_IN_PAX_ID";
    private static final String PATH_PAX_ID_PARAM = "PATH_PAX_ID";
    private static final String ACTIVITY_TYPE_LIST_PARAM = "ACTIVITY_TYPE_LIST_ID";
    private static final String FILTER_PARAM = "FILTER";
    private static final String PAGE_NUMBER_PARAM = "PAGE_NUMBER";
    private static final String PAGE_SIZE_PARAM = "PAGE_SIZE";
    private static final String PATH_GROUP_ID_PARAM = "PATH_GROUP_ID";
    private static final String NEWSFEED_ITEM_ID_LIST_PARAM = "NEWSFEED_ITEM_ID_LIST";
    private static final String NEWSFEED_ITEM_ID_PARAM = "NEWSFEED_ITEM_ID";
    private static final String IGNORE_STATUS_PARAM = "IGNORE_STATUS";
    private static final String OVERRIDE_VISIBILITY_PARAM = "OVERRIDE_VISIBILITY";
    
    private static final String NEWSFEED_CORE_DIRECT_REPORTS_QUERY = "EXEC component.UP_ACTIVITY_FEED_CORE_DIRECT_REPORTS" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + ACTIVITY_TYPE_LIST_PARAM +
            ", :" + FILTER_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + NEWSFEED_ITEM_ID_PARAM;
    
    private static final String NEWSFEED_CORE_NETWORK_QUERY = "EXEC component.UP_ACTIVITY_FEED_CORE_NETWORK" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + ACTIVITY_TYPE_LIST_PARAM +
            ", :" + FILTER_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + NEWSFEED_ITEM_ID_PARAM;
    
    private static final String NEWSFEED_CORE_PARTICIPANT_QUERY = "EXEC component.UP_ACTIVITY_FEED_CORE_PARTICIPANT" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + PATH_PAX_ID_PARAM +
            ", :" + ACTIVITY_TYPE_LIST_PARAM +
            ", :" + FILTER_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + NEWSFEED_ITEM_ID_PARAM;
    
    private static final String NEWSFEED_CORE_PUBLIC_QUERY = "EXEC component.UP_ACTIVITY_FEED_CORE_PUBLIC" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + ACTIVITY_TYPE_LIST_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + NEWSFEED_ITEM_ID_PARAM +
            ", :" + IGNORE_STATUS_PARAM +
            ", :" + OVERRIDE_VISIBILITY_PARAM;
    
    private static final String NEWSFEED_GET_DATA_QUERY = "EXEC component.UP_ACTIVITY_FEED_GET_DATA" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + NEWSFEED_ITEM_ID_LIST_PARAM +
            ", :" + IGNORE_STATUS_PARAM;

    private static final String NEWSFEED_CORE_GROUP_QUERY = "EXEC component.UP_ACTIVITY_FEED_CORE_GROUP" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + PATH_GROUP_ID_PARAM +
            ", :" + ACTIVITY_TYPE_LIST_PARAM +
            ", :" + FILTER_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + NEWSFEED_ITEM_ID_PARAM;

    @Override
    public List<Map<String, Object>> getActivityFeedDirectReports(
            Long loggedInPaxId, String activityTypeListString, String filterListString,
            Integer pageNumber, Integer pageSize, Long newsfeedItemId
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(ACTIVITY_TYPE_LIST_PARAM, activityTypeListString);
        params.addValue(FILTER_PARAM, filterListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(NEWSFEED_ITEM_ID_PARAM, newsfeedItemId);
        
        return namedParameterJdbcTemplate.query(NEWSFEED_CORE_DIRECT_REPORTS_QUERY, params, 
                new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getActivityFeedNetwork(
            Long loggedInPaxId, String activityTypeListString, String filterListString,
            Integer pageNumber, Integer pageSize, Long newsfeedItemId
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(ACTIVITY_TYPE_LIST_PARAM, activityTypeListString);
        params.addValue(FILTER_PARAM, filterListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(NEWSFEED_ITEM_ID_PARAM, newsfeedItemId);
        
        return namedParameterJdbcTemplate.query(NEWSFEED_CORE_NETWORK_QUERY, params, 
                new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getActivityFeedParticipant(
            Long loggedInPaxId, Long pathPaxId, String activityTypeListString,
            String filterListString, Integer pageNumber, Integer pageSize, Long newsfeedItemId
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(PATH_PAX_ID_PARAM, pathPaxId);
        params.addValue(ACTIVITY_TYPE_LIST_PARAM, activityTypeListString);
        params.addValue(FILTER_PARAM, filterListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(NEWSFEED_ITEM_ID_PARAM, newsfeedItemId);
        
        return namedParameterJdbcTemplate.query(NEWSFEED_CORE_PARTICIPANT_QUERY, params, 
                new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getActivityFeedPublic(
            Long loggedInPaxId, String activityTypeListString, Integer pageNumber,
            Integer pageSize, Long newsfeedItemId, Boolean ignoreStatus, Boolean overrideVisibility
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(ACTIVITY_TYPE_LIST_PARAM, activityTypeListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(NEWSFEED_ITEM_ID_PARAM, newsfeedItemId);
        params.addValue(IGNORE_STATUS_PARAM, ignoreStatus);
        params.addValue(OVERRIDE_VISIBILITY_PARAM, overrideVisibility);
        
        return namedParameterJdbcTemplate.query(NEWSFEED_CORE_PUBLIC_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getActivityFeedDetails(
            Long loggedInPaxId, String newsfeedItemIdListString, Boolean ignoreStatus
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(NEWSFEED_ITEM_ID_LIST_PARAM, newsfeedItemIdListString);
        params.addValue(IGNORE_STATUS_PARAM, ignoreStatus);
        
        return namedParameterJdbcTemplate.query(NEWSFEED_GET_DATA_QUERY, params, 
                new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getActivityFeedGroup(
            Long loggedInPaxId, Long targetId, String activityTypeListString, 
            String filterListString, Integer pageNumber, Integer pageSize, Long newsfeedItemId
        ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(PATH_GROUP_ID_PARAM, targetId);
        params.addValue(ACTIVITY_TYPE_LIST_PARAM, activityTypeListString);
        params.addValue(FILTER_PARAM, filterListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(NEWSFEED_ITEM_ID_PARAM, newsfeedItemId);

        return namedParameterJdbcTemplate.query(NEWSFEED_CORE_GROUP_QUERY, params,
                new CamelCaseMapRowMapper());
    }

}