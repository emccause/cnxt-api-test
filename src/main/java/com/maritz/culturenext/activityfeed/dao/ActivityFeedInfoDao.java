package com.maritz.culturenext.activityfeed.dao;

import java.util.List;
import java.util.Map;

public interface ActivityFeedInfoDao {

    List<Map<String, Object>> getActivityFeedDirectReports(
            Long loggedInPaxId, String activityTypeListString, String filterListString,
            Integer pageNumber, Integer pageSize, Long newsfeedItemId
        );
    List<Map<String, Object>> getActivityFeedNetwork(
            Long loggedInPaxId, String activityTypeListString, String filterListString,
            Integer pageNumber, Integer pageSize, Long newsfeedItemId
        );
    List<Map<String, Object>> getActivityFeedParticipant(
            Long loggedInPaxId, Long pathPaxId, String activityTypeListString,
            String filterListString, Integer pageNumber, Integer pageSize, Long newsfeedItemId
        );
    List<Map<String, Object>> getActivityFeedPublic(
            Long loggedInPaxId, String activityTypeListString, Integer pageNumber,
            Integer pageSize, Long newsfeedItemId, Boolean ignoreStatus, Boolean overrideVisibility
        );
    List<Map<String, Object>> getActivityFeedDetails(
            Long loggedInPaxId, String newsfeedItemIdListString, Boolean ignoreStatus
        );
    List<Map<String, Object>> getActivityFeedGroup(Long loggedInPaxId, Long targetId, 
            String activityTypeListString, String filterListString, Integer pageNumber, 
            Integer pageSize, Long newsfeedItemId
        );
}
