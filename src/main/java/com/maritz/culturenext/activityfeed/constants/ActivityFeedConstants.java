package com.maritz.culturenext.activityfeed.constants;

public class ActivityFeedConstants {
    public static final String ACTIVITY_FEED_DEFAULT_FILTER_VALUE = "GIVEN,RECEIVED";
    public static final Long ANONYMOUS_SUBMITTER_ID = 0L;
}
