package com.maritz.culturenext.activityfeed.services;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;

public interface ActivityFeedService {

    /**
     * Gets a single activity from the requested feed
     *
     * @param pathPaxId - if looking at PARTICIPANT feed, this will be used
     * @param activityFeedId - id of the activity to fetch data for
     * @param audience - the audience used for the feed - DIRECT_REPORTS, NETWORK, PARTICIPANT, PUBLIC
     * @param commaSeparatedTypeList - filter used for the feed - RECOGNITION or AWARD_CODE
     * @param commaSeparatedFilterList - filter used for the feed - GIVEN, RECEIVED, or GIVEN,RECEIVED
     * @param ignoreStatus - should status be ignored? if true, will return non-approved data
     * @param overrideVisibility - should visibility be override? if true, will return records with visibility = 'NONE'
   * @param languageCode - the user's language setting so the program name and value can be translated
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7798834/GET+Activity+Feed
     */
    NewsfeedItemDTO getActivityById(Long pathPaxId, Long activityFeedId, String audience,
            String commaSeparatedTypeList, String commaSeparatedFilterList, Boolean ignoreStatus,
            Boolean overrideVisibility, String languageCode);

    /**
     * This returns a list of all the activity feed items the logged in user has permission to see
     * Visibility permissions are based upon the recipient's privacy setting
     *
     * If the logged in user is the giver:
     *     - pointsIssued will be total points issued to all recipients
     *
     * If the logged in user is one of the recipients:
     *     - pointsIssued will be only the points that they received
     *     - toPax will be populated with the logged in user's info
     *
     * If the logged in user is neither the giver or one of the recipients:
     *     - pointsIssued will be null
     *
     * @param requestDetails - details from request to be passed through to the PaginatedResponseObject
     * @param pathPaxId - only used for PARTICIPANT feed, fetches activity related to this pax that the logged in pax can see
     * @param audience - PUBLIC: all items the logged in user can see
     *             DIRECT_REPORTS: subset of PUBLIC, shows the logged in user and their direct reports (ignores privacy) (may include
     *             some that aren't in PUBLIC feed)
     *             NETWORK: All participants in participants network including themselves
     *             PARTICIPANT: items related to the path pax.  Path pax has priority over logged in pax for TO_PAX display.
     *                 Takes privacy into account (logged in pax can see everything in their own feed)
     *             GROUP: items related to a group (where a submitter and/or recipient is in the group)
     * @param commaSeparatedActivityTypeList - currently only supports RECOGNITION and AWARD_CODE; future values could be SERVICE_ANNIVERSARY,
     *                         POINT_LOAD, etc.
     * @param commaSeparatedStatusTypeList - not currently supported (will be added in MP-8065)
     * @param commaSeparatedFilterList - determines if we only care about recognitions GIVEN by the audience, RECEIVED, or both (GIVEN,RECEIVED)
     * @param pageNumber - used for pagination
     * @param size - used for pagination
     * @param pageSize - Target participant to get newsfeed for
     * @param activityFeedIdString - id of the activity feed entry to fetch data for (does not fetch more data, just a single object)
     * @param ignoreStatus - should status be ignored? if true, will return non-approved data
     * @param languageCode - the user's language setting so the program name and value can be translated
   *
     * @return a list of NewsfeedItemDTO sorted by CREATE_DATE
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7798834/GET+Activity+Feed
     */
    PaginatedResponseObject<NewsfeedItemDTO> getActivityFeed(
            PaginationRequestDetails requestDetails, Long pathPaxId, String audience,
            String commaSeparatedActivityTypeList, String commaSeparatedFilterList,
            Integer pageNumber, Integer pageSize, String activityFeedIdString, Boolean ignoreStatus, Boolean overrideVisibility,
      String languageCode
        );
}
