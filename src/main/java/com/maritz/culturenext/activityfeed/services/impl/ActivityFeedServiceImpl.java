package com.maritz.culturenext.activityfeed.services.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.ObjectUtils;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.activityfeed.constants.ActivityFeedConstants;
import com.maritz.culturenext.activityfeed.dao.ActivityFeedInfoDao;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.ActivityFeedAudienceEnum;
import com.maritz.culturenext.enums.ActivityFeedRaiseStatusEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.newsfeed.dto.NewsfeedNominationDTO;
import com.maritz.culturenext.newsfeed.dto.PersonalActivityDTO;
import com.maritz.culturenext.newsfeed.dto.RecognitionCriteriaSimpleDTO;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.impl.DetailProfileServiceImpl;
import com.maritz.culturenext.social.comment.dto.CommentDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.TranslationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;

@Component
public class ActivityFeedServiceImpl implements ActivityFeedService {
    private static final String DISPLAY_NAME = "DISPLAY_NAME";
    private static final String LONG_DESC = "LONG_DESC";

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ActivityFeedInfoDao activityFeedInfoDao;
    @Inject private ConcentrixDao<Lookup> lookupDao;
    @Inject private Environment environment;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private Security security;
    @Inject private TranslationUtil translationUtil;
    @Inject private DetailProfileServiceImpl  detailProfileInfo;

    @Override
    public NewsfeedItemDTO getActivityById(Long pathPaxId, Long activityFeedId, String audience,
            String commaSeparatedTypeList, String commaSeparatedFilterList, Boolean ignoreStatus,
            Boolean overrideVisibility, String languageCode) {
        PaginatedResponseObject<NewsfeedItemDTO> object = getActivityFeed(
                null, pathPaxId, audience, commaSeparatedTypeList, commaSeparatedFilterList, ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE, activityFeedId.toString(), ignoreStatus, overrideVisibility, languageCode
            );

        if (!object.getData().isEmpty()) {
            return object.getData().get(0); // should only be one
        }

        return null; // got nothing
    }

    @Override
    public PaginatedResponseObject<NewsfeedItemDTO> getActivityFeed(
            PaginationRequestDetails requestDetails, Long targetId, String audience,
            String commaSeparatedActivityTypeList, String commaSeparatedFilterList,
            Integer pageNumber, Integer pageSize, String activityFeedIdString, Boolean ignoreStatus,
      Boolean overrideVisibility, String languageCode
        ) {

        Long loggedInPaxId = security.getPaxId();

        if (org.apache.commons.lang3.StringUtils.isEmpty(languageCode)) {
	        try{            	
	    		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
	    	}catch(NullPointerException e){
	    		languageCode = ProjectConstants.DEFAULT_LOCALE_CODE;
	    	}
        }
        if (audience == null || audience.trim().isEmpty()) { // fall back to default
            audience = ActivityFeedAudienceEnum.PUBLIC.name();
        }

        if (commaSeparatedFilterList == null || commaSeparatedFilterList.trim().isEmpty()) { // fall back to defaults
            commaSeparatedFilterList = ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE;
        }

        if (commaSeparatedActivityTypeList == null || commaSeparatedActivityTypeList.trim().isEmpty()) { // defaults
            commaSeparatedActivityTypeList = com.maritz.core.util.StringUtils.formatDelimitedData(NewsfeedItemTypeCode.RECOGNITION.name(),
                    NewsfeedItemTypeCode.AWARD_CODE.name());
        }

        Long activityFeedItemId = null;
        if (activityFeedIdString != null && !activityFeedIdString.trim().isEmpty()) {
            try {
                activityFeedItemId = Long.parseLong(activityFeedIdString);
            }
            catch (NumberFormatException nfe) {
                return new PaginatedResponseObject<>(requestDetails, new ArrayList<NewsfeedItemDTO>(), 0); // no data, get out
            }
        }

        // check if network is disabled, if so, fall back to path pax (should be logged in pax)
        if(audience.equalsIgnoreCase(ActivityFeedAudienceEnum.NETWORK.name()) &&
                Boolean.FALSE.equals(
                        Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED))
                    )
            ){
            audience = ActivityFeedAudienceEnum.PARTICIPANT.name(); // fall back to path pax
        }

        List<Map<String, Object>> activityFeedCoreData = null;
        Integer totalResults = 0;

        boolean activityFeedLoggingEnabled = Boolean.valueOf(environment.getProperty("activityFeed.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(activityFeedLoggingEnabled) {
            logger.info("activityFeedInfoDao will be executed with the following params. " +
                            "loggedInPaxId {} " +
                            "audience {} " +
                            "activityTypeList {} " +
                            "filterList {} " +
                            "pageNumber {} " +
                            "pageSize {} " +
                            "activityFeedItemId {} "
                    , loggedInPaxId, audience, commaSeparatedActivityTypeList, commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId);
        }

        if (audience.equalsIgnoreCase(ActivityFeedAudienceEnum.DIRECT_REPORTS.name())) {
            activityFeedCoreData = activityFeedInfoDao.getActivityFeedDirectReports(
                    loggedInPaxId, commaSeparatedActivityTypeList,
                    commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId
                );
        }
        else if (audience.equalsIgnoreCase(ActivityFeedAudienceEnum.NETWORK.name())) {
            activityFeedCoreData = activityFeedInfoDao.getActivityFeedNetwork(
                    loggedInPaxId, commaSeparatedActivityTypeList,
                    commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId
                );
        }
        else if (audience.equalsIgnoreCase(ActivityFeedAudienceEnum.PARTICIPANT.name())) {
            activityFeedCoreData = activityFeedInfoDao.getActivityFeedParticipant(
                    loggedInPaxId, targetId, commaSeparatedActivityTypeList,
                    commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId
                );
        }
        else if (audience.equalsIgnoreCase(ActivityFeedAudienceEnum.PUBLIC.name())) {
            activityFeedCoreData = activityFeedInfoDao.getActivityFeedPublic(
                    loggedInPaxId, commaSeparatedActivityTypeList,
                    pageNumber, pageSize, activityFeedItemId, ignoreStatus, overrideVisibility
                );
        }
        else if (audience.equalsIgnoreCase(ActivityFeedAudienceEnum.GROUP.name())) {
            activityFeedCoreData = activityFeedInfoDao.getActivityFeedGroup(
                    loggedInPaxId, targetId, commaSeparatedActivityTypeList,
                    commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId
                );
        }
        else {
            return new PaginatedResponseObject<>(requestDetails, new ArrayList<NewsfeedItemDTO>(), 0); // no data, get out
        }

        if(activityFeedLoggingEnabled) {
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("activityFeedInfoDao has been executed with the following params. " +
                            "loggedInPaxId {} " +
                            "audience {} " +
                            "activityTypeList {} " +
                            "filterList {} " +
                            "pageNumber {} " +
                            "pageSize {} " +
                            "activityFeedItemId {} " +
                            "durationTimeMilliseconds {}"
                    , loggedInPaxId, audience, commaSeparatedActivityTypeList, commaSeparatedFilterList, pageNumber, pageSize, activityFeedItemId, estimatedTime);
        }

        totalResults = !activityFeedCoreData.isEmpty() ?  Integer.parseInt( activityFeedCoreData.get(0).get(ProjectConstants.TOTAL_RESULTS_KEY).toString()) : 0;

        // we've got data, now let's get the details
        List<Long> activityFeedIdsToGetDataForList = new ArrayList<>();
        List<Long> paxIdsToResolveList = new ArrayList<>();

        for (Map<String, Object> activityFeedItemEntry : activityFeedCoreData) {
            Long activityFeedId = (Long) activityFeedItemEntry.get(ProjectConstants.NEWSFEED_ITEM_ID);
            if (!activityFeedIdsToGetDataForList.contains(activityFeedId)) {
                activityFeedIdsToGetDataForList.add(activityFeedId);
            }
        }

        // take our list of ids and convert them to a comma separated string
        String activityFeedIdsListString = StringUtils.formatDelimitedData(ObjectUtils.toStringCollection(activityFeedIdsToGetDataForList));

        // pass to the details sproc
        List<Map<String, Object>> activityFeedDetailsMapList = activityFeedInfoDao.getActivityFeedDetails(
                loggedInPaxId, activityFeedIdsListString, ignoreStatus
            );

        // build program name and recognition criteria default map for translationUtil
        Map<Long, String> programNameDefaultMap = new HashMap<>();
        Map<Long, String> recognitionCriteriaIdDefaultMap = new HashMap<>();

        // combine maps, convert to DTOs

        List<NewsfeedItemDTO> activityFeedItemDtoList = new ArrayList<>();
        for (Map<String, Object> activityFeedEntry : activityFeedCoreData) {
            Long activityFeedId = (Long) activityFeedEntry.get(ProjectConstants.NEWSFEED_ITEM_ID);

            // we need to find the detail map that corresponds to the id we're working on.
            Boolean found = false;
            for (Map<String, Object> activityFeedDetailsMap : activityFeedDetailsMapList) {
                Long detailactivityFeedId = (Long) activityFeedDetailsMap.get(ProjectConstants.NEWSFEED_ITEM_ID);

                // Put details into core data map
                if (activityFeedId.equals(detailactivityFeedId)) {
                    activityFeedEntry.putAll(activityFeedDetailsMap);
                    found = true;
                    break;
                }
            }

            // THIS SHOULD NEVER HAPPEN
            if (Boolean.FALSE.equals(found)) {
                // We didn't find details for an entry, bail out
                // https://media.giphy.com/media/ZQ0xeMU6cpGla/giphy.gif
                logger.error("Did not find details for activity feed item id " + activityFeedId + ", returning application error.");

                List<ErrorMessage> errors = new ArrayList<>();
                errors.add(ProjectConstants.INTERNAL_ERROR);
                ErrorMessageException.throwIfHasErrors(errors, HttpStatus.UNPROCESSABLE_ENTITY);
            }

            // grab everything
            NewsfeedItemDTO activityFeedItem = new NewsfeedItemDTO();

            activityFeedItem.setId(activityFeedId);
            activityFeedItem.setActivityType((String) activityFeedEntry.get(ProjectConstants.ACTIVITY_TYPE));
            activityFeedItem.setRecipientCount((Integer) activityFeedEntry.get(ProjectConstants.RECIPIENT_COUNT));

            Integer approvedRecipientCount = (Integer) activityFeedEntry.get(ProjectConstants.APPROVED_RECIPIENT_COUNT);
            activityFeedItem.setApprovedRecipientCount(approvedRecipientCount);

            activityFeedItem.setCreateDate(DateUtil.convertToUTCDateTime((Date) activityFeedEntry.get(ProjectConstants.CREATE_DATE)));

            Long submitterPaxId = (Long) activityFeedEntry.get(ProjectConstants.SUBMITTER_PAX_ID);
            if (submitterPaxId != null && !submitterPaxId.equals(ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID)) {
                EmployeeDTO employeeDTO = new EmployeeDTO();
                employeeDTO.setPaxId(submitterPaxId);
                activityFeedItem.setFromPax(employeeDTO); // drop in placeholder

                if (!paxIdsToResolveList.contains(submitterPaxId)) {
                    paxIdsToResolveList.add(submitterPaxId);
                }
            }

            Integer likeCount = (Integer) activityFeedEntry.get(ProjectConstants.LIKE_COUNT);
            if (likeCount != null) {
                activityFeedItem.setLikeCount(new Long(likeCount));
            }

            Integer commenterCount = (Integer) activityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
            if (commenterCount != null) {
                activityFeedItem.setCommenterCount(new Long(commenterCount));
            }

            Integer commentCount = (Integer) activityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
            if (commentCount != null) {
                activityFeedItem.setCommentCount(new Long(commentCount));
            }

            Integer raisedCount = (Integer) activityFeedEntry.get(ProjectConstants.RAISED_COUNT);
            if (raisedCount != null) {
                activityFeedItem.setRaisedCount(new Long(raisedCount));
            }

            Long commentId = (Long) activityFeedEntry.get(ProjectConstants.COMMENT_ID);
            if (commentId != null) {
                CommentDTO commentDTO = new CommentDTO();
                commentDTO.setId(commentId);
                commentDTO.setTime(DateUtil.convertToUTCDateTime((Date) activityFeedEntry.get(ProjectConstants.COMMENT_DATE)));
                commentDTO.setText((String) activityFeedEntry.get(ProjectConstants.COMMENT_TEXT));
                commentDTO.setStatus((String) activityFeedEntry.get(ProjectConstants.COMMENT_STATUS));

                Long commentPaxId = (Long) activityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
                if (commentPaxId != null) {
                    EmployeeDTO employeeDTO = new EmployeeDTO();
                    employeeDTO.setPaxId(commentPaxId);
                    commentDTO.setPax(employeeDTO); // drop in placeholder

                    if (!paxIdsToResolveList.contains(commentPaxId)) {
                        paxIdsToResolveList.add(commentPaxId);
                    }
                }

                activityFeedItem.setComment(commentDTO);
            }

            // Nomination details setup

            NewsfeedNominationDTO nominationDTO = new NewsfeedNominationDTO();
            nominationDTO.setNominationId((Long) activityFeedEntry.get(ProjectConstants.NOMINATION_ID));
            nominationDTO.setProgramId((Long) activityFeedEntry.get(ProjectConstants.PROGRAM_ID));
            nominationDTO.setProgramName((String) activityFeedEntry.get(ProjectConstants.PROGRAM_NAME));
            nominationDTO.setImageId((Long) activityFeedEntry.get(ProjectConstants.IMAGE_ID));
            nominationDTO.setHeadline((String) activityFeedEntry.get(ProjectConstants.HEADLINE));
            nominationDTO.setAwardCode((String) activityFeedEntry.get(ProjectConstants.AWARD_CODE));
      nominationDTO.setComment((String) activityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE));

      if (!ProjectConstants.DEFAULT_LOCALE_CODE.equals(languageCode)) {
        // build out program name/id map to translate
        if (!programNameDefaultMap.containsKey(nominationDTO.getProgramId())) {
          programNameDefaultMap.put(nominationDTO.getProgramId(), nominationDTO.getProgramName());
        }
      }

            String recognitionJSON = (String) activityFeedEntry.get(ProjectConstants.RECOGNITION_CRITERIA_JSON);

            if (recognitionJSON != null && !recognitionJSON.isEmpty()) {
                //Convert the config to a JSON object
                List<RecognitionCriteriaSimpleDTO> recognitionCriteriaList = ObjectUtils.jsonToList(
                        (String) activityFeedEntry.get(ProjectConstants.RECOGNITION_CRITERIA_JSON),
                        RecognitionCriteriaSimpleDTO.class
                    );
                nominationDTO.setRecognitionCriteria(recognitionCriteriaList);

                if (!ProjectConstants.DEFAULT_LOCALE_CODE.equals(languageCode)) {
          // build out recognition criteria/id map to translate
          for (RecognitionCriteriaSimpleDTO criterion : recognitionCriteriaList) {
            if (!recognitionCriteriaIdDefaultMap.containsKey(criterion.getId())) {
              recognitionCriteriaIdDefaultMap.put(criterion.getId(), criterion.getDisplayName());
            }
          }
        }
            }

            // Set payout type (POINTS, CASH, null)
            nominationDTO.setPayoutType((String) activityFeedEntry.get(ProjectConstants.PAYOUT_TYPE));

            populatePayoutTypeInformation (languageCode,nominationDTO);

            activityFeedItem.setNomination(nominationDTO);

            // Personal activity setup
            PersonalActivityDTO personalActivity = new PersonalActivityDTO();

            personalActivity.setDirectReportCount((Integer) activityFeedEntry.get(ProjectConstants.DIRECT_REPORT_COUNT));

            Integer myCommentCount = (Integer) activityFeedEntry.get(ProjectConstants.MY_COMMENT_COUNT);
            if (myCommentCount != null) {
                personalActivity.setMyCommentCount(new Long(myCommentCount));
            }

            Integer myRaisedCount = (Integer) activityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
            if (myRaisedCount != null) {
                personalActivity.setMyRaisedCount(new Long(myRaisedCount));
            }
            personalActivity.setRaisedPointTotal((Double) activityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL));

            Double pointsIssued = (Double) activityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
            personalActivity.setPointsIssued(pointsIssued);

            // resolve a few pax

            Long recipientPaxId = (Long) activityFeedEntry.get(ProjectConstants.RECIPIENT_PAX_ID);
            if (recipientPaxId != null) {
                EmployeeDTO employeeDTO = new EmployeeDTO();
                employeeDTO.setPaxId(recipientPaxId);
                personalActivity.setToPax(employeeDTO); // drop in placeholder

                if (!paxIdsToResolveList.contains(recipientPaxId)) {
                    paxIdsToResolveList.add(recipientPaxId);
                }
            }

            Long raisedPaxId = (Long) activityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
            if (raisedPaxId != null) {
                EmployeeDTO employeeDTO = new EmployeeDTO();
                employeeDTO.setPaxId(raisedPaxId);
                personalActivity.setRaisePax(employeeDTO); // drop in placeholder

                if (!paxIdsToResolveList.contains(raisedPaxId)) {
                    paxIdsToResolveList.add(raisedPaxId);
                }
            }

            Long likePaxId = (Long) activityFeedEntry.get(ProjectConstants.LIKE_PAX_ID);
            if (likePaxId != null) {
                EmployeeDTO employeeDTO = new EmployeeDTO();
                employeeDTO.setPaxId(likePaxId);
                personalActivity.setLikePax(employeeDTO); // drop in placeholder

                if (!paxIdsToResolveList.contains(likePaxId)) {
                    paxIdsToResolveList.add(likePaxId);
                }
            }

            // things related to raise status
            String loggedInUserStatus = (String) activityFeedEntry.get(ProjectConstants.LOGGED_IN_USER_STATUS);
            Boolean programIsActive = (Boolean) activityFeedEntry.get(ProjectConstants.PROGRAM_IS_ACTIVE);
            Boolean canRaiseAnyTier = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_ANY_TIER);
            Boolean canRaiseTier = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_TIER);
            Boolean canRaiseProgramBudget = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_PROGRAM_BUDGET);
            Boolean givenRaise = (Boolean) activityFeedEntry.get(ProjectConstants.GIVEN_RAISE);

            // calculate raise status
            String raiseStatus = calculateRaiseStatus(
                    submitterPaxId, loggedInUserStatus, recipientPaxId, approvedRecipientCount, pointsIssued,
                    programIsActive, canRaiseAnyTier, canRaiseTier, canRaiseProgramBudget, givenRaise
                );

            personalActivity.setRaiseStatus(raiseStatus);

            activityFeedItem.setPersonalActivity(personalActivity);

            activityFeedItemDtoList.add(activityFeedItem);
        }

        // get our pax objects
        List<EntityDTO> paxList = participantInfoDao.getInfo(paxIdsToResolveList, null);

        for (NewsfeedItemDTO activityFeedItem : activityFeedItemDtoList) {
            // replace placeholders

            // activityFeedItem.fromPax
            if (activityFeedItem.getFromPax() != null) {
                activityFeedItem.setFromPax(getPax(activityFeedItem.getFromPax().getPaxId(), paxList));
            }

            // activityFeedItem.comment.commentPax
            CommentDTO comment = activityFeedItem.getComment();
            if (comment != null && comment.getPax() != null) {
                comment.setPax(getPax(comment.getPax().getPaxId(), paxList));
                activityFeedItem.setComment(comment);
            }

            // activityFeedItem.personalActivity.toPax
            PersonalActivityDTO personalActivity = activityFeedItem.getPersonalActivity();
            if (personalActivity.getToPax() != null) {
                personalActivity.setToPax(getPax(personalActivity.getToPax().getPaxId(), paxList));
            }

            // activityFeedItem.personalActivity.raisePax
            if (personalActivity.getRaisePax() != null) {
                personalActivity.setRaisePax(getPax(personalActivity.getRaisePax().getPaxId(), paxList));
            }

            // activityFeedItem.personalActivity.likePax
            if (personalActivity.getLikePax() != null) {
                personalActivity.setLikePax(getPax(personalActivity.getLikePax().getPaxId(), paxList));
            }

            activityFeedItem.setPersonalActivity(personalActivity);
        }

        if (!ProjectConstants.DEFAULT_LOCALE_CODE.equals(languageCode)) {

      Map<Long, String> programNameTranslations = getTranslationOfProgramDetails(
        languageCode, TableName.PROGRAM.name(), TranslationConstants.COLUMN_NAME_PROGRAM_NAME, programNameDefaultMap);

      Map<Long, String> recognitionCriteriaTranslations = getTranslationOfProgramDetails(
        languageCode, TableName.RECOGNITION_CRITERIA.name(), TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_NAME, recognitionCriteriaIdDefaultMap);

      // loop through activityfeeditem list to replace program names with translated value
      for (NewsfeedItemDTO activityFeedItem : activityFeedItemDtoList) {
        NewsfeedNominationDTO nominationDTO = activityFeedItem.getNomination();
        nominationDTO.setProgramName(programNameTranslations.get(nominationDTO.getProgramId()));
        activityFeedItem.setNomination(nominationDTO);
        // loop through recognitioncriteria in each nominationDTO to replace recognitioncriteria with translated value
        if (activityFeedItem.getNomination().getRecognitionCriteria() != null) {
          for (RecognitionCriteriaSimpleDTO recognitionCriteria : activityFeedItem.getNomination().getRecognitionCriteria()) {
            recognitionCriteria.setDisplayName(recognitionCriteriaTranslations.get(recognitionCriteria.getId()));
          }
        }
      }
    }

        return new PaginatedResponseObject<>(requestDetails, activityFeedItemDtoList, totalResults);
    }

    private Map<Long, String> getTranslationOfProgramDetails(String languageCode, String tableName, String columnName, Map<Long, String> defaultMap) {
      return translationUtil.getTranslationsForListOfIds(languageCode, tableName, columnName, new ArrayList<>(defaultMap.keySet()), defaultMap);
    }

    /**
     * sets payout type related information, like description
     * @param nominationDTO
     */
    public void populatePayoutTypeInformation(String languageCode, NewsfeedNominationDTO nominationDTO) {
        if (this.lookupDao != null && nominationDTO != null && nominationDTO.getPayoutType() != null) {
            List<Lookup> lookUpList = this.lookupDao.findBy()
                    .where(ProjectConstants.LOOKUP_CATEGORY_CODE).eq(nominationDTO.getPayoutType()).find();
            if (lookUpList != null) {
            	
            	Map<Long, String> translations = fetchTranslations(languageCode, lookUpList);
            	
                for (Lookup item : lookUpList){
                    if (item.getLookupCode().equals(DISPLAY_NAME)){
                        nominationDTO.setPayoutDisplayName( translations.get(item.getId()));
                    } else if (item.getLookupCode().equals(LONG_DESC)){
                        nominationDTO.setPayoutDescription( translations.get(item.getId()));
                    }
                    if (nominationDTO.getPayoutDisplayName() != null && nominationDTO.getPayoutDescription() != null) {
                        break;
                    }
                }
            }
        }
    }
    
    private Map<Long, String> fetchTranslations(String languageCode, List<Lookup> entries) {
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);

        Map<Long, String> translations = null;
        if (entries != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (Lookup entry : entries) {
                rowIds.add(entry.getId());
                defaultMap.put(entry.getId(), entry.getLookupDesc());
            }

            if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                return defaultMap;
            }

            translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode, 
                    TableName.LOOKUP.name(), TranslationConstants.COLUMN_NAME_LOOKUP_DESC, 
                    rowIds, defaultMap);
        }

        return translations;
    }

    

    /**
     * Finds an EmployeeDTO in the list that has the pax id provided
     * NOTE: only handles EmployeeDTO, skips anything else
     *
     * @param paxId - id of the pax you're looking for
     * @param entityList - list of EmplyeeDTO objects
     *
     * @return EmployeeDTO requested, or null if it's not in the list
     */
    private EmployeeDTO getPax(Long paxId, List<EntityDTO> entityList) {

        for (EntityDTO entity : entityList) {
            if (!(entity instanceof EmployeeDTO)) {
                continue;
            }
            EmployeeDTO pax = (EmployeeDTO) entity;
            if (paxId.equals(pax.getPaxId())) {
                return pax;
            }
        }

        return null;
    }

    /**
     * Calculates raise status based on provided information
     *
     * NOTE: public for testing purposes
     *
     * @param submitterPaxId - who submitted the nom (compared to logged in pax)
     * @param loggedInUserStatus - what status the logged in user is (mainly to see if they're restricted)
     * @param recipientPaxId - who received the nom (used in combination with recipientCount)
     * @param recipientCount - how many recipients on this nom
     * @param pointsIssued - if points were issued (used in combination with submitterPaxId)
     * @param programIsActive - if the program used for the nom is active at this time
     * @param canRaiseAnyTier - if any tier on the program used for the nom supports raising
     * @param canRaiseTier - if the tier used for this nom supports raising
     * @param canRaiseProgramBudget - if the logged in pax can give from a budget for the program used for the nom
     * @param givenRaise - if the logged in pax has given a raise for this nom already
     *
     * @return String, values are contained in ActivityFeedRaiseStatusEnum
     */
    public String calculateRaiseStatus(
            Long submitterPaxId, String loggedInUserStatus, Long recipientPaxId, Integer recipientCount,
            Double pointsIssued, Boolean programIsActive, Boolean canRaiseAnyTier, Boolean canRaiseTier,
            Boolean canRaiseProgramBudget, Boolean givenRaise
        ) {

        // NOT_ALLOWED

        // Logged in user is restricted -> NOT_ALLOWED
        if (StatusTypeCode.RESTRICTED.name().equalsIgnoreCase(loggedInUserStatus)) {
            return ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name();
        }

        // program is not active (Status = active and now is between from / thru dates) -> NOT_ALLOWED
        if (Boolean.FALSE.equals(programIsActive)) {
            return ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name();
        }

        // can't raise any tier for this program -> NOT_ALLOWED
        if (Boolean.FALSE.equals(canRaiseAnyTier)) {
            return ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name();
        }

        // no visible recipients -> NOT_ALLOWED (this should never be returned, but checking just in case)
        if (recipientCount == null || recipientCount.equals(0)) {
            return ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name();
        }

        // INELIGIBLE

        // logged in pax is submitter and gave points -> INEIGIBLE
        if (security.getPaxId().equals(submitterPaxId) && (pointsIssued != null && pointsIssued > 0)) {
            return ActivityFeedRaiseStatusEnum.INELIGIBLE.name();
        }

        // logged in pax is only receiver -> INELIGIBLE
        // NOTE: any case where logged in pax received but isn't receiver means that there was more than one recipient
        if (security.getPaxId().equals(recipientPaxId) && recipientCount.equals(1)) {
            return ActivityFeedRaiseStatusEnum.INELIGIBLE.name();
        }

        // Check if tier allows raising and the user can give from a budget for the program
        if (Boolean.TRUE.equals(canRaiseTier) && Boolean.TRUE.equals(canRaiseProgramBudget)) {
            // If eligible, make sure they haven't already given
            if (Boolean.TRUE.equals(givenRaise)) {
                return ActivityFeedRaiseStatusEnum.GIVEN.name();
            }
            else {
                return ActivityFeedRaiseStatusEnum.ELIGIBLE.name();
            }
        }
        else {
            // Tier doesn't allow raising or can't give from any budget tied to the program -> INELIGIBLE
            return ActivityFeedRaiseStatusEnum.INELIGIBLE.name();
        }
    }
}
