package com.maritz.culturenext.activityfeed.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.ACTIVITY_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.ACTIVITY_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.ACTIVITY_TYPE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.AUDIENCE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.FILTER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_NUMBER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_SIZE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.TARGET_ID_REST_PARAM;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.core.env.Environment;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

@RestController
@Api(value="/newsfeed", description="All end points dealing with the news feed")
public class ActivityFeedRestService {

    private static final String ACTIVITY_FEED_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + TARGET_ID_REST_PARAM + "}/" + ACTIVITY_REST_PARAM;
    private static final String ACTIVITY_BY_ID_PATH = ACTIVITY_FEED_PATH + "/{" + ACTIVITY_ID_REST_PARAM  + "}";
    private static final String ACTIVITY_FEED_BY_GROUP_ID_PATH = ProjectConstants.GROUPS_URI_BASE +"/{" + TARGET_ID_REST_PARAM + "}/" + ACTIVITY_REST_PARAM;
    private static final String ACTIVITY_FEED_BY_GROUP_BY_ID_PATH = ACTIVITY_FEED_BY_GROUP_ID_PATH + "/{" + ACTIVITY_ID_REST_PARAM + "}";

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private ActivityFeedService activityFeedService;
    @Inject private Security security;
    @Inject private Environment environment;

    //rest/participants/~/activity
    @RequestMapping(value = ACTIVITY_FEED_PATH, method = RequestMethod.GET)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @ApiOperation(value="Returns the activity feed information for the desired user.", notes="Returns all activity "
    + "records (public and self) that the requesting participant has the visibility permissions to see per the "
    + "recipients' profile-privacy setting.<br><br>"
    + "pointsIssued is populated only if requester is the giver or receiver. for giver, we need to total up all "
    + "the points for the nomination.<br><br>"
    + "'toPax' will be populated with the the requester-pax if the requester was a recipient on the nomination. "
    + "Otherwise, it will be populated with the first pax (for a multiple-recipient case) based on numeric sort by "
    + "pax ID and the pax has their visibility setting set to public/share-recognition in profile-privacy.<br><br>"
    + "The toPax returned should be the direct report of any manager retrieving a notification or activity record, "
    + "if only one of the recipients is a direct report (otherwise the count is used instead)<br><br>"
    + "'directReportCount' property added 6/11/2015 : 'directReportCount' is the count of the directReports if the "
    + "requester is the manager of the recipient. (this is added to support messages like '3 direct reports and "
    + "10 others' )<br><br>"
    + "If the Activity detail is for the manager, and its a multiple recipient nomination, the recipientCount and "
    + "directReportCount can be greater than one. The private message, points-issued will be populated "
    + "for a manager also.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the activity feed for "
            + "the given user.", response = NewsfeedItemDTO.class)})
    public PaginatedResponseObject<NewsfeedItemDTO> getActivityFeed(
            @ApiParam(value="id of the participant.")
                @PathVariable(TARGET_ID_REST_PARAM) String targetIdString,
            @ApiParam(value="values are DIRECT_REPORTS, NETWORK, PARTICIPANT, PUBLIC",required = false)
                @RequestParam(value = AUDIENCE_REST_PARAM, required = false) String audience,
            @ApiParam(value="The comma-delimited list of activity types, used to get different types of feeds. "
                    + "Values are RECOGNITION and AWARD_CODE.",required = false)
                @RequestParam(value = ACTIVITY_TYPE_REST_PARAM, required = false) String commaSeparatedActivityTypeList,
            @ApiParam(value="Whether to only include recognitions given or received by the audience. "
                    + "Values are GIVEN and / or RECEIVED.",required = false)
                @RequestParam(value = FILTER_REST_PARAM, required = false) String commaSeparatedFilterList,
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value="The number of records per page.",required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
      @ApiParam(value="language code.", required = false)
        @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode
        ) throws Throwable {

        boolean activityFeedEnabled = Boolean.valueOf(environment.getProperty("activityFeed.enabled"));

        if(!activityFeedEnabled){
            return  new PaginatedResponseObject<NewsfeedItemDTO>();
        }

        Long pathPaxId = security.getPaxId(targetIdString);
        if(pathPaxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(TARGET_ID_REST_PARAM, targetIdString);

        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(AUDIENCE_REST_PARAM, audience);
        requestParamMap.put(ACTIVITY_TYPE_REST_PARAM, commaSeparatedActivityTypeList);
        requestParamMap.put(FILTER_REST_PARAM, commaSeparatedFilterList);
        requestParamMap.put(LANGUAGE_CODE_REST_PARAM, languageCode);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(ACTIVITY_FEED_PATH).setPathParamMap(pathParamMap)
                    .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);

        return activityFeedService.getActivityFeed(
                requestDetails, pathPaxId, audience, commaSeparatedActivityTypeList,
                commaSeparatedFilterList, pageNumber, pageSize, null, Boolean.FALSE, Boolean.FALSE,
        languageCode
            );
    }

    //rest/participants/~/activity/~
    @RequestMapping(value = ACTIVITY_BY_ID_PATH, method = RequestMethod.GET)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @ApiOperation(value="Returns a specific detail activity feed information for the user.", notes ="Activity detail "
            + "is only retrieved for the logged in user's activity typically, but it can also be called for activity "
            + "'id' not relevant to the logged in user, in which case the private message, points-issued and other "
            + "sensitive data will not be populated.<br><br>"
    +"Only returns the activity record if the requesting participant has the visibility permissions to see the "
    + "recipients activity per the recipients profile-privacy setting.<br><br>"
    +"pointsIssued & comment (private-message) is populated only if requested user is the giver or receiver. "
    + "for giver, we need to total up all the points for the nomination.<br><br>"
    +"The toPax returned should be the direct report of any manager retrieving a notification or activity record, "
    + "if only one of the recipients is a direct report (otherwise the count is used instead)<br><br>"
    +"'directReportCount' property added 6/11/2015 : 'directReportCount' is the count of the directReports if the "
    + "requester is the manager of the recipient. (this is added to support messages like '3 direct reports and "
    + "10 others')")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the activity from the feed of "
            + "the given user.", response = NewsfeedItemDTO.class)})
    public NewsfeedItemDTO getActivityFeedById(
            @ApiParam(value="Id of the participant.")
                @PathVariable(TARGET_ID_REST_PARAM) String targetIdString,
            @ApiParam(value="Id of the activity feed you want to see.")
                @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,
            @ApiParam(value="Language code.", required = false)
        @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode,
            @ApiParam(value="Audience")
                @RequestParam(value = AUDIENCE_REST_PARAM, required = false) String audience,
            @ApiParam(value="activityType")
                @RequestParam(value = ACTIVITY_TYPE_REST_PARAM, required = false) String commaSeparatedActivityTypeList,
            @ApiParam(value="Whether to only include recognitions given or received by the audience. "
                        + "Values are GIVEN and / or RECEIVED.",required = false)
                @RequestParam(value = FILTER_REST_PARAM, required = false) String commaSeparatedFilterList
        ) throws Throwable {

        Long paxId = security.getPaxId(targetIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        Long activityId = convertPathVariablesUtil.getId(activityIdString);

        return activityFeedService.getActivityById(paxId, activityId, audience, commaSeparatedActivityTypeList, commaSeparatedFilterList, Boolean.FALSE, Boolean.FALSE, languageCode);

    }

    //rest/groups/~/activity
    @RequestMapping(value = ACTIVITY_FEED_BY_GROUP_ID_PATH, method = RequestMethod.GET)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @ApiOperation(value="Returns the activity feed information for the desired group", notes="Returns activity "
    + "records (public and self) that the requested group has the visibility permissions to see per the "
    + "recipients' profile-privacy setting.<br><br>"
    + "pointsIssued is populated only if requester is the giver or receiver. for giver, we need to total up all "
    + "the points for the nomination.<br><br>"
    + "'toPax' will be populated with the the requester-pax if the requester was a recipient on the nomination. "
    + "Otherwise, it will be populated with the first pax (for a multiple-recipient case) based on numeric sort by "
    + "pax ID and the pax has their visibility setting set to public/share-recognition in profile-privacy.<br><br>"
    + "The toPax returned should be the direct report of any manager retrieving a notification or activity record, "
    + "if only one of the recipients is a direct report (otherwise the count is used instead)<br><br>"
    + "'directReportCount' property added 6/11/2015 : 'directReportCount' is the count of the directReports if the "
    + "requester is the manager of the recipient. (this is added to support messages like '3 direct reports and "
    + "10 others' )<br><br>"
    + "If the Activity detail is for the manager, and its a multiple recipient nomination, the recipientCount and "
    + "directReportCount can be greater than one. The private message, points-issued will be populated "
    + "for a manager also.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the activity feed for "
            + "the given group", response = NewsfeedItemDTO.class)})
    public PaginatedResponseObject<NewsfeedItemDTO> getActivityFeedByGroupId(
            @ApiParam(value="Id of the group")
                @PathVariable(TARGET_ID_REST_PARAM) String targetIdString,
            @ApiParam(value="Language code.", required = false)
        @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode,
            @ApiParam(value="values are DIRECT_REPORTS, NETWORK, PARTICIPANT, PUBLIC",required = false)
                @RequestParam(value = AUDIENCE_REST_PARAM, required = false) String audience,
            @ApiParam(value="The comma-delimited list of activity types, used to get different types of feeds. "
                    + "Values are RECOGNITION and AWARD_CODE.",required = false)
                @RequestParam(value = ACTIVITY_TYPE_REST_PARAM, required = false) String commaSeparatedActivityTypeList,
            @ApiParam(value="Whether to only include recognitions given or received by the audience. "
                    + "Values are GIVEN and / or RECEIVED.",required = false)
                @RequestParam(value = FILTER_REST_PARAM, required = false) String commaSeparatedFilterList,
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value="The number of records per page.",required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
        ) throws Throwable {

        Long targetPaxId = security.getId(targetIdString, null);

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(TARGET_ID_REST_PARAM, targetIdString);

        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(AUDIENCE_REST_PARAM, audience);
        requestParamMap.put(ACTIVITY_TYPE_REST_PARAM, commaSeparatedActivityTypeList);
        requestParamMap.put(FILTER_REST_PARAM, commaSeparatedFilterList);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(ACTIVITY_FEED_BY_GROUP_ID_PATH).setPathParamMap(pathParamMap)
                    .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);

        return activityFeedService.getActivityFeed(
                requestDetails, targetPaxId, audience, commaSeparatedActivityTypeList,
                commaSeparatedFilterList, pageNumber, pageSize, null, Boolean.FALSE, Boolean.FALSE, languageCode
            );
    }

    //rest/groups/~/activity/~
    @RequestMapping(value = ACTIVITY_FEED_BY_GROUP_BY_ID_PATH, method = RequestMethod.GET)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, impersonated=false)
    @ApiOperation(value="Returns a specific detail activity feed information for the group", notes ="Activity detail "
            + "is only retrieved for the logged in user's activity typically, but it can also be called for activity "
            + "'id' not relevant to the logged in user, in which case the private message, points-issued and other "
            + "sensitive data will not be populated.<br><br>"
    +"Only returns the activity record if the requested group has the visibility permissions to see the "
    + "recipients activity per the recipients profile-privacy setting.<br><br>"
    +"pointsIssued & comment (private-message) is populated only if requested user is the giver or receiver. "
    + "for giver, we need to total up all the points for the nomination.<br><br>"
    +"The toPax returned should be the direct report of any manager retrieving a notification or activity record, "
    + "if only one of the recipients is a direct report (otherwise the count is used instead)<br><br>"
    +"'directReportCount' property added 6/11/2015 : 'directReportCount' is the count of the directReports if the "
    + "requester is the manager of the recipient. (this is added to support messages like '3 direct reports and "
    + "10 others')")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the activity from the feed of "
            + "the given user.", response = NewsfeedItemDTO.class)})
    public NewsfeedItemDTO getActivityFeedByGroupById(
            @ApiParam(value="Id of the group")
                @PathVariable(TARGET_ID_REST_PARAM) String targetIdString,
            @ApiParam(value="Id of the activity feed you want to see.")
                @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,
            @ApiParam(value="Language code", required=false)
        @RequestParam(value=LANGUAGE_CODE_REST_PARAM, required=false) String languageCode,
            @ApiParam(value="Audience")
                @RequestParam(value = AUDIENCE_REST_PARAM, required = false) String audience,
            @ApiParam(value="activityType")
                @RequestParam(value = ACTIVITY_TYPE_REST_PARAM, required = false) String commaSeparatedActivityTypeList,
            @ApiParam(value="Whether to only include recognitions given or received by the audience. "
                        + "Values are GIVEN and / or RECEIVED.",required = false)
                @RequestParam(value = FILTER_REST_PARAM, required = false) String commaSeparatedFilterList
        ) throws Throwable {

        Long targetPaxId = security.getId(targetIdString, null);

        Long activityId = convertPathVariablesUtil.getId(activityIdString);

        return activityFeedService.getActivityById(targetPaxId, activityId, audience, commaSeparatedActivityTypeList, commaSeparatedFilterList, Boolean.FALSE, Boolean.FALSE, languageCode);
    }
}
