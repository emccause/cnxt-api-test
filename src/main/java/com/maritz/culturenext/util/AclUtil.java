package com.maritz.culturenext.util;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.repository.RoleRepository;
import com.maritz.culturenext.jpa.repository.CnxtAclRepository;

/**
 * Utility class for handling any interaction with the ACL table.
 */
@Component
public class AclUtil {

    @Inject private CnxtAclRepository cnxtAclRepository;
    @Inject private RoleRepository roleRepository;

    private static final Logger logger = LoggerFactory.getLogger(AclUtil.class);

    private static final String PROGRAM = "PROGRAM";
    private static final String RECEIVE_ROLE_NAME = "Program Receiver";
    private static final String GIVER_ROLE_NAME = "Program Giver";

    private static final String ERROR_INVALID_ROLE_MSG = "Specified role is invalid: ";
    private static final String ERROR_ACL_ADDITION_MSG = "Error occurred while adding ACL entry";
    private static final String ERROR_ACL_DELETION_MSG = "Error occurred while removing ACL entry";

    // ACL getters

    @Nonnull
    public List<Acl> getAclEligibilityGivers(Long programId) {
        return getAclEligibilityEntities(GIVER_ROLE_NAME, programId);
    }

    @Nonnull
    public List<Acl> getAclEligibilityReceivers(Long programId) {
        return getAclEligibilityEntities(RECEIVE_ROLE_NAME, programId);
    }

    @Nonnull
    public List<Acl> getAllAclEligibility(Long programId) {
        List<Acl> eligibilityAclEntities = cnxtAclRepository.findAllEligibilityAcl(PROGRAM, programId);
        return eligibilityAclEntities != null ? eligibilityAclEntities : new ArrayList<Acl>();
    }

    // helper methods
    /**
     * Retrieves the ACL entities representing eligibility definitions for a specified Program.
     *
     * @param roleName type of eligibility member to retrieve
     * @param targetId Program ID to retrieve eligibility definitions for
     * @return ACL entities representing eligibility definitions for a specified Program
     */
    @Nonnull
    private List<Acl> getAclEligibilityEntities(String roleName, Long targetId) {
        Long roleId = findRoleId(roleName);
        List<Acl> eligibilityAclEntities = cnxtAclRepository.findEligibilityAclByRole(roleId, PROGRAM, targetId);
        return eligibilityAclEntities != null ? eligibilityAclEntities : new ArrayList<Acl>();
    }

    /**
     * Retrieves the role ID for a specified role name.
     *
     * @param roleName name of the role to retrieve the ID for
     * @return ID of the corresponding role
     */
    private Long findRoleId(String roleName) {
        try {
            Long roleId = roleRepository.findBy()
                .where("roleName").eq(roleName)
                .findOne("roleId", Long.class);
            if (roleId == null) {
                String errorMsg = ERROR_INVALID_ROLE_MSG + roleName;
                logger.error(errorMsg);
                throw new RuntimeException(errorMsg);
            }
            return roleId;
        }
        catch (Exception e) {
            // error handling for lookup failures
            String errorMsg = ERROR_INVALID_ROLE_MSG + roleName;
            logger.error(errorMsg, e);
            throw e;
        }
    }

    /**
     * Adds an ACL record with the specified roleName, target ID, subject Type, and subject ID.
     *
     * @param roleName role name
     * @param targetId target ID
     * @param subjectType subject type
     * @param subjectId subject ID
     */
    public void addAcl(String roleName, Long targetId,String targetType, String subjectType, Long subjectId) {        
        Long roleId = findRoleId(roleName);

        try {
            cnxtAclRepository.addAcl(roleId, targetType, targetId, subjectType, subjectId);
        } catch(Exception e) {
            // log error and rethrow
            logger.error(ERROR_ACL_ADDITION_MSG + " for role " + roleName + " " + e.getMessage(), e);
            throw e;
        }
    }
    
    /**
     * Deletes an ACL record with the specified roleName, targetId, and targetType
     * @param roleName
     * @param targetId
     * @param targetType
     */
    public void deleteAcl(String roleName, Long targetId, String targetType, String subjectType, Long subjectId) {
        Long roleId = null;
        if(roleName != null){
            roleId = findRoleId(roleName);
        }
        try {
            cnxtAclRepository.deleteAcl(targetType, targetId, roleId,subjectType,subjectId);
        }catch(Exception e) {
            // log error and rethrow
            logger.error(ERROR_ACL_DELETION_MSG + " for role " + roleName + " " + e.getMessage(), e);
            throw e;
        }
    }
}