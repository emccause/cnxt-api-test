package com.maritz.culturenext.util;

import com.maritz.core.jpa.support.util.StatusTypeCode;

import java.util.Date;


public class StatusTypeUtil {
    
     /**
     * Checks the passed in status against the StatusTypes enum.
     *
     * @param status Status_type_code to be checked
     */
    public static boolean isValidStatus(String status) {
        try {
            StatusTypeCode.valueOf(status.toUpperCase());
            return true;
        }
        catch (IllegalArgumentException e) {
            return false;
        }
    }

    /**
     * Checks the passed in status to see if it is valid. Determines validity if the
     * status is specified within the listed STATUS_TYPEs (in the DB table) or if
     * it is a valid implied status (e.g. SCHEDULED).
     *
     * @return true if the status is a valid actual or implied status, false otherwise
     */
    public static boolean isValidActualOrImpliedStatus(String status) {
        
        return isValidStatus(status) ||
                StatusTypeCode.SCHEDULED.toString().equalsIgnoreCase(status) || 
                StatusTypeCode.ENDED.toString().equalsIgnoreCase(status);
    }

    /**
     * Determine the status value from the specified request status. Only handles
     * implied status, else just leaves the status as is.
     *
     * @param requestStatus specified status
     * @return determined status type
     */
    public static String determineRequestStatus(String requestStatus) {
        return (StatusTypeCode.SCHEDULED.toString().equals(requestStatus) || 
                StatusTypeCode.ENDED.toString().equals(requestStatus)) ?
                StatusTypeCode.ACTIVE.name() :
                requestStatus;
    }
    
     /**
     * Uses strings to compare from and thru dates of an object to determine implied statuses
     *
     * @param status Status_type_code of the object in the database (ACTIVE or INACTIVE)
     * @param fromDateString fromDate of the object as a string 
     * @param thruDateString thruDate of the object as a string
     * @returns  ACTIVE, INACTIVE, SCHEDULED, or ENDED
     */
    public static String getImpliedStatus(String status, String fromDateString, String thruDateString) {
        
        String impliedStatus = null;
        
        //Only ACTIVE budgets/programs can be flagged as SCHEDULED or ENDED
        if (StatusTypeCode.ACTIVE.name().equalsIgnoreCase(status)) {
            Date fromDate;
            Date thruDate;
            
            if (fromDateString == null)
                fromDate = null;
            else
                fromDate = DateUtil.convertFromUTCStringDate(fromDateString);

            if (thruDateString == null)
                thruDate = null;
            else 
                thruDate = DateUtil.convertToEndOfDayString(thruDateString);
            if (fromDate != null && fromDate.after(new Date())) 
                impliedStatus = StatusTypeCode.SCHEDULED.toString();
            else if (fromDate != null && fromDate.before(new Date())
                    && thruDate != null    && thruDate.before(new Date()))
                impliedStatus = StatusTypeCode.ENDED.toString();
            else 
                impliedStatus = StatusTypeCode.ACTIVE.name();
        }
        else
            impliedStatus = StatusTypeCode.INACTIVE.name();
        
        return impliedStatus;
    }
    
     /**
     * Uses Date objects to compare from and thru dates of an object to determine implied statuses
     *
     * @param status Status_type_code of the object in the database (ACTIVE or INACTIVE)
     * @param fromDate The object's fromDate in the database
     * @param thruDate The object's thruDate in the database
     * @returns  ACTIVE, ENDED, INACTIVE, SCHEDULED, or SUSPENDED
     */
    public static String getImpliedStatus(String status, Date fromDate, Date thruDate) {
        
        String impliedStatus;
        
        //Only ACTIVE budgets/programs can be flagged as SCHEDULED or ENDED
        if (status.equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
            
            if (fromDate != null && fromDate.after(new Date())) 
                impliedStatus = StatusTypeCode.SCHEDULED.toString();
            else if (fromDate != null && fromDate.before(new Date())
                    && thruDate != null    && thruDate.before(new Date()))
                impliedStatus = StatusTypeCode.ENDED.toString();
            else 
                impliedStatus = StatusTypeCode.ACTIVE.name();
        }
        else
            impliedStatus = status;
        
        return impliedStatus;
    }
    
}
