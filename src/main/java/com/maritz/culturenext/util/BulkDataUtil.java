package com.maritz.culturenext.util;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.util.CollectionUtils;

import com.maritz.culturenext.constants.ProjectConstants;

public class BulkDataUtil {
    
    public static final String WITH = "WITH ";
    public static final String COMMA_SPACE = ", ";
    public static final String PAX_ID_LIST_TABLE_NAME = "PAX_ID_LIST";
    public static final String GROUP_ID_LIST_TABLE_NAME = "GROUP_ID_LIST";
    public static final String PROGRAM_CATEGORY_LIST_TABLE_NAME = "PROGRAM_CATEGORY_LIST";
    
    public static final String PAX_ID_COLUMN = "PAX_ID_COL";
    public static final String GROUP_ID_COLUMN = "GROUP_ID_COL";
    public static final String PROGRAM_CATEGORY_CODE_COLUMN = "PG_CODE_COL";
    
    private static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST_PARAM";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST_PARAM";
    private static final String PROGRAM_CATEGORY_LIST_SQL_PARAM = "PROGRAM_CATEGORY_LIST_PARAM";
    
    private static final String WITH_PAX_ID_LIST = PAX_ID_LIST_TABLE_NAME + " AS("
            + " SELECT CAST (items AS BIGINT) AS " + PAX_ID_COLUMN
            + " FROM component.UF_LIST_TO_TABLE_ID(:" + PAX_ID_LIST_SQL_PARAM + ",',')"
            + ") ";
    
    private static final String WITH_GROUP_ID_LIST =  GROUP_ID_LIST_TABLE_NAME + " AS("
            + " SELECT CAST (items AS BIGINT) AS " + GROUP_ID_COLUMN
            + " FROM component.UF_LIST_TO_TABLE_ID(:" + GROUP_ID_LIST_SQL_PARAM + ",',')"
            + ") ";
    
    private static final String WITH_PROGRAM_CATEGORY_ID_LIST =  PROGRAM_CATEGORY_LIST_TABLE_NAME + " AS("
            + " SELECT CAST (items AS VARCHAR) AS " + PROGRAM_CATEGORY_CODE_COLUMN
            + " FROM component.UF_LIST_TO_TABLE(:" + PROGRAM_CATEGORY_LIST_SQL_PARAM + ",',')"
            + ") ";
    
    /**
     * Resolving query structure depending on paxIds and groupIds list.
     * Adding query fragments if any of those parameters is not empty.
     * @param query
     * @param params
     * @param paxIds
     * @param groupIds
     * @param programCategories
     */
    public static String buildCommonTablesQuery(String query, MapSqlParameterSource params
            , List<Long> paxIds, List<Long> groupIds, List<String> programCategories){
        
        if (StringUtils.isBlank(query)) {
            return ProjectConstants.EMPTY_STRING;
        }

        String withQuery = ProjectConstants.EMPTY_STRING;;
        
        if (!CollectionUtils.isEmpty(paxIds)) {
            withQuery += WITH_PAX_ID_LIST;
            if (!params.hasValue(PAX_ID_LIST_SQL_PARAM)) {
                params.addValue(PAX_ID_LIST_SQL_PARAM, StringUtils.join(paxIds, ProjectConstants.COMMA_DELIM));
            }
        }

        if (!CollectionUtils.isEmpty(groupIds)) {
            if (!withQuery.isEmpty()) {
                withQuery += COMMA_SPACE;
            }
            withQuery += WITH_GROUP_ID_LIST;
            if (!params.hasValue(GROUP_ID_LIST_SQL_PARAM)) {
                params.addValue(GROUP_ID_LIST_SQL_PARAM, StringUtils.join(groupIds, ProjectConstants.COMMA_DELIM));
            }
        }
        
        if (!CollectionUtils.isEmpty(programCategories)) {
            if (!withQuery.isEmpty()) {
                withQuery += COMMA_SPACE;
            }
            withQuery += WITH_PROGRAM_CATEGORY_ID_LIST;
            if (!params.hasValue(PROGRAM_CATEGORY_LIST_SQL_PARAM)) {
                    params.addValue(PROGRAM_CATEGORY_LIST_SQL_PARAM, StringUtils.join(programCategories, ProjectConstants.COMMA_DELIM));
            }
        }
        
        if (withQuery.isEmpty()) {
            return query;
        }
        
        return WITH + withQuery + query;
    }
}