package com.maritz.culturenext.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class CsvFileUtil {

    final static Logger logger = LoggerFactory.getLogger(CsvFileUtil.class);

    final static String CONTENT_TYPE = "text/csv;charset=utf-8";
    final static String FILE_EXT = "csv";

    /**
     * Create a CSV file into response object
     * @param response
     * @param fileName 
     * @param dto  file content object
     * @param processors  cell processors that convert Java data types to Strings
     * @param headers  column names
     * @param fieldMapping  array must contain values matching the names of the properties defined in the DTO class. 
     * if it is null, properties name is taken from headers parameter.
     */
    public static void writeDownloadCSVFile(HttpServletResponse response, String fileName, List<?> dto,
            CellProcessor[] processors, String[] headers, String[] fieldMapping) {
        ICsvBeanWriter beanWriter = null;

        response.setContentType(CONTENT_TYPE);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "." + FILE_EXT + "\"");

        try {
            beanWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);
            
            if(fieldMapping == null) {
                fieldMapping = headers;
            }
            
            beanWriter.writeHeader(headers);

            for (Object object : dto) {
                beanWriter.write(object, fieldMapping, processors);
            }

        } catch (IOException e) {
            logger.error("Error while creating file. ", e.getMessage());
        } finally {
            if (beanWriter != null) {
                try {
                    beanWriter.flush();
                    beanWriter.close();
                } catch (IOException e) {
                    logger.error("Error while closing file. ", e.getMessage());
                }
            }
        }
    }

    /**
     * Create a CSV file into response object
     * @param response
     * @param fileName
     * @param dto  file content object
     * @param processors  cell processors that convert Java data types to Strings
     * @param headers  column names
     * @param fieldMapping  array must contain values matching the names of the properties defined in the DTO class.
     *  if it is null, properties name is taken from headers parameter.
     */
    public static void writeDownloadCSVFileFromMap(HttpServletResponse response, String fileName, 
            List<Map<String, Object>> dto, CellProcessor[] processors, String[] headers, String[] fieldMapping) {
        ICsvMapWriter mapWriter = null;

        response.setContentType(CONTENT_TYPE);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "." + FILE_EXT + "\"");

        try {
            mapWriter = new CsvMapWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

            if(fieldMapping == null) {
                fieldMapping = headers;
            }

            mapWriter.writeHeader(headers);

            for (Map<String, Object> objectMap : dto) {
                mapWriter.write(objectMap, fieldMapping, processors);
            }

        } catch (Exception e) {
            logger.error("Error while creating file. ", e.getMessage());
        } finally {
            if (mapWriter != null) {
                try {
                    mapWriter.flush();
                    mapWriter.close();
                } catch (IOException e) {
                    logger.error("Error while closing file. ", e.getMessage());
                }
            }
        }
    }
    
    
    /**
     * Create a CSV file into response object using CsvColumnPropertiesDTO as columns descriptor.
     * @param response
     * @param fileName
     * @param mapContent : file content
     * @param csvColumnProperties : contains CSV column's definition (header, processor, fieldMapping object).
     */
    public static void writeDownloadCSVFileFromMap(HttpServletResponse response, String fileName
            , List<Map<String, Object>> mapContent, List<CsvColumnPropertiesDTO> csvColumnProperties) {
    
        // Column's processor
        CellProcessor[] processors = new CellProcessor[csvColumnProperties.size()];
        for (int i= 0; i < processors.length; i++) {
            processors[i] = csvColumnProperties.get(i).getProcessor();
        }
        
        // Column's headers
        String[] headers = new String[csvColumnProperties.size()];
        for (int i= 0; i < headers.length; i++) {
            headers[i] = csvColumnProperties.get(i).getHeader();
        }
        
        // Column's dto field mapping
        String[] fieldMapping = new String[csvColumnProperties.size()];
        for (int i= 0; i < fieldMapping.length; i++) {
            fieldMapping[i] = csvColumnProperties.get(i).getField();
        }
        
        // Create CSV file.
        writeDownloadCSVFileFromMap(response, fileName, mapContent, processors, headers, fieldMapping);
    }

    /**
     * Read a CSV file
     * @param reader  csv file content
     * @param typeClass  type to instantiate. A new instance will be created using the default no-args constructor.
     * @param processors  an array of CellProcessors used to further process data before it is populated on the bean
     * @param fieldMapping  array must contain values matching the names of the properties defined in the typeClass.
     * @return a list of typeClass objects, populated with the csv file content.
     */
    public static <T> List<T> readCSVFILE(Reader reader, Class<T> typeClass, CellProcessor[] processors, 
            String[] fieldMapping) {
        ICsvBeanReader beanReader = null;
        List<T> content = new ArrayList<T>();
        try {
            beanReader = new CsvBeanReader(reader,
                    CsvPreference.STANDARD_PREFERENCE);

            beanReader.getHeader(true);
            
            T row ;
            while((row = beanReader.read(typeClass, fieldMapping, processors)) != null) {
                content.add(row);
            }
        
            
        } catch(IOException e) {
            logger.error("Error while reading file. ", e.getMessage());
        } finally {
            if (beanReader != null) {
                try {
                    beanReader.close();
                } catch (IOException e) {
                    logger.error("Error while closing the reader. ", e.getMessage());
                }
            }
        }
        return content;
    }

    /**
     * Read a CSV file into a map
     * @param reader  csv file content
     * @param processors  an array of CellProcessors used to further process data before it is populated on the bean
     * @param fieldMapping  array must contain values matching the names of the properties defined in the typeClass.
     * @return a list of typeClass objects, populated with the csv file content.
     */
    public static List<Map<String, Object>> readCSVFILEIntoMap(Reader reader, CellProcessor[] processors,
            String[] fieldMapping) {
        ICsvMapReader mapReader = null;
        List<Map<String, Object>> content = new ArrayList<>();
        try {
            mapReader = new CsvMapReader(reader, CsvPreference.STANDARD_PREFERENCE);

            mapReader.getHeader(true);

            Map<String, Object> row;
            while((row = mapReader.read(fieldMapping ,processors)) != null){
                content.add(row);
            }

        } catch(IOException e) {
            logger.error("Error while reading file. ", e.getMessage());
        } finally {
            if (mapReader != null) {
                try {
                    mapReader.close();
                } catch (IOException e) {
                    logger.error("Error while closing the reader. ", e.getMessage());
                }
            }
        }
        return content;
    }
    
    /**
     * Read a CSV file into a map using CsvColumnPropertiesDTO as columns descriptor.
     * @param reader  : csv file content
     * @param csvColumnProperties : contains CSV column's definition (header, processor, fieldMapping object).
     * @return a list of typeClass objects, populated with the csv file content.
     */
    public static List<Map<String, Object>> readCSVFILEIntoMap(Reader reader, List<CsvColumnPropertiesDTO> csvColumnProperties) {

        // Column's processor
        CellProcessor[] processors = new CellProcessor[csvColumnProperties.size()];
        for (int i= 0; i < processors.length; i++) {
            processors[i] = csvColumnProperties.get(i).getProcessor();
        }
        
        // Column's dto field mapping
        String[] fieldMapping = new String[csvColumnProperties.size()];
        for (int i= 0; i < fieldMapping.length; i++) {
            fieldMapping[i] = csvColumnProperties.get(i).getField();
        }
        
        return readCSVFILEIntoMap(reader, processors, fieldMapping);
    }
    
    /**
     * Create a CSV file into String object using CsvColumnPropertiesDTO as columns descriptor.
     * @param mapContent : file content
     * @param csvColumnProperties : contains CSV column's definition (header, processor, fieldMapping object).
     */
    public static String buildCSVFileFromMap(List<Map<String, Object>> mapContent, 
                                                    List<CsvColumnPropertiesDTO> csvColumnProperties) {
        
        // Column's processor
        CellProcessor[] processors = new CellProcessor[csvColumnProperties.size()];
        for (int i= 0; i < processors.length; i++) {
            processors[i] = csvColumnProperties.get(i).getProcessor();
        }
        
        // Column's headers
        String[] headers = new String[csvColumnProperties.size()];
        for (int i= 0; i < headers.length; i++) {
            headers[i] = csvColumnProperties.get(i).getHeader();
        }
        
        // Column's dto field mapping
        String[] fieldMapping = new String[csvColumnProperties.size()];
        for (int i= 0; i < fieldMapping.length; i++) {
            fieldMapping[i] = csvColumnProperties.get(i).getField();
        }
        
        // Create CSV file.
        return buildCSVFileFromMap(mapContent, processors, headers, fieldMapping);
    }

    /**
     * Create a CSV String File from map.
     * @param csvColumnProperties : contains CSV column's definition (header, processor, fieldMapping object).
     */
    public static String buildCSVFileFromMap( 
        List<Map<String, Object>> dto, CellProcessor[] processors, String[] headers, String[] fieldMapping) {
        
        ICsvMapWriter mapWriter = null;
        StringWriter output = new StringWriter();

        try {
            
            mapWriter = new CsvMapWriter(output, CsvPreference.STANDARD_PREFERENCE);

            if(fieldMapping == null){
                fieldMapping = headers;
            }

            mapWriter.writeHeader(headers);

            for (Map<String, Object> objectMap : dto) {
                mapWriter.write(objectMap, fieldMapping, processors);
            }

        } catch (Exception e) {
            logger.error("Error while creating file. ", e.getMessage());
        } finally {
            if (mapWriter != null) {
                try {
                    mapWriter.flush();
                    mapWriter.close();
                } catch (IOException e) {
                    logger.error("Error while closing file. ", e.getMessage());
                }
            }
        }
        return output.toString();

    }
    
    /**
     * Write a CSV file into response object from batchFile
     * @param response
     * @param batchFile (file and file name)
     */
    public static void writeDownloadCSVFileFromFile(HttpServletResponse response, BatchFile batchFile) {
        Writer writer = null;

        response.setContentType(CONTENT_TYPE);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + batchFile.getFilename() + "." + FILE_EXT + "\"");

        try {
            writer = response.getWriter();
            writer.write(batchFile.getFileUpload());
        } catch (Exception e) {
            logger.error("Error while creating file. ", e.getMessage());
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    logger.error("Error while closing file. ", e.getMessage());
                }
            }
        }

    }

}
