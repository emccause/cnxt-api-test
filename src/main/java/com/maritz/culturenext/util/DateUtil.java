package com.maritz.culturenext.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Locale;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.date.constants.DateConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

public class DateUtil {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);



    /**
     * Converts date to UTC date with timestamp string
     * 
     * @param date
     * @return
     */
    public static String convertToUTCDateTime(Date date) {
        String timeStamp;

        TimeZone tz = TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE);
        DateFormat df = new SimpleDateFormat(DateConstants.ISO_8601_DATE_FORMAT);
        df.setTimeZone(tz);
        timeStamp = df.format(date);

        return timeStamp;
    }

    /**
     * returns UTC date and timestamp string for current time
     * 
     * @return
     */
    public static String getCurrentTimeInUTCDateTime() {
        return convertToUTCDateTime(new Date());
    }

    /**
     * converts date to UTC date string without timestamp
     * 
     * @param date
     * @return
     */
    public static String convertToUTCDate(Date date) {
        String timeStamp;

        TimeZone tz = TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE);
        DateFormat df = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        df.setTimeZone(tz);

        timeStamp = df.format(date);

        return timeStamp;
    }

    public static String convertToUTCDateComponentsFormat(Date date) {
        String timeStamp;

        TimeZone tz = TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE);
        DateFormat df = new SimpleDateFormat(DateConstants.COMPONENTS_DATE_FORMAT_1);
        df.setTimeZone(tz);

        timeStamp = df.format(date);

        return timeStamp;
    }

    @CheckForNull
    public static String convertToDateOnlyFormat(Date date, TimeZone timeZone) {
        if (date == null || timeZone == null) {
            return null;
        }

        return convertDateFormat(date, timeZone, DateConstants.ISO_8601_DATE_ONLY_FORMAT);
    }

    @CheckForNull
    public static String convertToDateComponentsFormat(Date date, TimeZone timeZone) {
        if (date == null || timeZone == null) {
            return null;
        }

        return convertDateFormat(date, timeZone, DateConstants.COMPONENTS_DATE_FORMAT_1);
    }

    private static String convertDateFormat(Date date, TimeZone timeZone, String format) {
        String timeStamp;

        DateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(timeZone);

        timeStamp = df.format(date);

        return timeStamp;
    }

    /**
     * returns UTC date string without timestamp for current time
     * 
     * @return
     */
    public static String getCurrentTimeInUTCDate() {
        return convertToUTCDate(new Date());
    }

    /**
     * converts UTC date and timestamp string to date object
     * 
     * @param timeStamp
     * @return
     */
    public static Date convertFromUTCStringDateTime(String timeStamp) {

        TimeZone tz = TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE);
        DateFormat df = new SimpleDateFormat(DateConstants.ISO_8601_DATE_FORMAT);
        df.setTimeZone(tz);

        Date convertedDate = null;
        try {
            convertedDate = df.parse(timeStamp);
        } catch (ParseException e) {
            logger.debug("Date provided is not in UTC format: " + timeStamp);
        }

        return convertedDate;

    }

    /**
     * converts UTC date string (no timestamp) to date object
     * 
     * @param timeStamp
     * @return
     */
    public static Date convertFromUTCStringDate(String timeStamp) {

        TimeZone tz = TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE);
        DateFormat df = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        df.setTimeZone(tz);

        Date convertedDate = null;
        try {
            convertedDate = df.parse(timeStamp);
        } catch (ParseException e) {
            logger.debug("Date provided is not in UTC format: " + timeStamp);
        }

        return convertedDate;

    }

    /**
     * attemps to convert from UTC. if that fails, tries the old components time
     * formats. if all fail, returns null
     * 
     * @param timeStamp
     * @return
     * @throws ParseException
     */
    public static Date convertFromString(String timeStamp) {
        
        if (timeStamp == null) {
            return null; // Don't even try
        }
        
        Date convertedDate = convertFromUTCStringDateTime(timeStamp);

        if (convertedDate != null) {
            return convertedDate;
        }

        DateFormat df1 = new SimpleDateFormat(DateConstants.COMPONENTS_DATE_FORMAT_1);
        try {
            convertedDate = df1.parse(timeStamp);
        } catch (ParseException e) {
            // not that one
            convertedDate = null;
        }

        if (convertedDate != null) {
            return convertedDate;
        }

        DateFormat df2 = new SimpleDateFormat(DateConstants.COMPONENTS_DATE_FORMAT_2);
        try {
            convertedDate = df2.parse(timeStamp);
        } catch (ParseException e) {
            // not that one
            convertedDate = null;
        }

        if (convertedDate != null) {
            return convertedDate;
        }

        convertedDate = convertFromUTCStringDate(timeStamp);

        if (convertedDate != null) {
            return convertedDate;
        } else {
            logger.debug("error in convertFromString method - unknown date format: " + timeStamp);
        }
        return null;
    }

    /**
     * Validates date range specified on a query object.
     *
     * @param fromDate
     *            fromDate specified on a query object
     * @param thruDate
     *            thruDate specified on a query object
     * @return errors associated with the date range specified on a query object
     */
    @Nonnull
    public static List<ErrorMessage> validateDateRange(Date fromDate, Date thruDate) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        if (fromDate == null) {
            errors.add(new ErrorMessage()
                    .setCode(DateConstants.ERROR_FROM_DATE_INVALID)
                    .setField(DateConstants.FROM_DATE_FIELD)
                    .setMessage(DateConstants.ERROR_FROM_DATE_INVALID_MSG));
        } else if (thruDate == null) {
            errors.add(new ErrorMessage()
                    .setCode(DateConstants.ERROR_THRU_DATE_INVALID)
                    .setField(DateConstants.THRU_DATE_FIELD)
                    .setMessage(DateConstants.ERROR_THRU_DATE_INVALID_MSG));
        } else if (!fromDate.before(thruDate) && !fromDate.equals(thruDate)) {
            errors.add(new ErrorMessage()
                    .setCode(DateConstants.ERROR_FROM_DATE_AFTER_THRU_DATE)
                    .setField(DateConstants.THRU_DATE_FIELD)
                    .setMessage(DateConstants.ERROR_FROM_DATE_AFTER_THRU_DATE_MSG));
        }

        return errors;
    }

    /**
     * Can use this for date comparisons where date should include the current
     * day for from dates
     * 
     * @return
     */
    @Nonnull
    public static Date getCurrentDateAtMidnight() {
        // Save current date for comparisons
        // Today should be yesterday's date at midnight (so today is allowed)
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.roll(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Verify if a date is older than x days
     * 
     * @param dateToValidate
     * @param daysBefore
     * @return
     */
    public static Boolean verifyDateBeforeDays(Date dateToValidate, int daysBefore) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -daysBefore);
        Date dateBefore = calendar.getTime();

        return dateToValidate.before(dateBefore);
    }

    /**
     * @see DateUtil#convertToStartOfDayString(String, TimeZone)
     *
     *      Default conversion is UTC-based.
     */
    @CheckForNull
    public static Date convertToStartOfDayString(String dateString) {
        return convertToStartOfDayString(dateString, TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE));
    }

    /**
     * Converts the specified fromDate into the correct Date object.
     *
     * fromDate is specified as in date-only format. Converts into the specified
     * timezone with the same day/month/year but specifies time at midnight.
     *
     * @param dateString
     *            fromDate to convert
     * @return the correct fromDate date object (with specified day/month/year
     *         but time at midnight)
     */
    @CheckForNull
    public static Date convertToStartOfDayString(String dateString, TimeZone timeZone) {
        if (timeZone == null) {
            throw new RuntimeException("Need to specify timezone for proper date string conversion!!");
        }

        Calendar localCalendar = determineDateRangeStringCalendar(dateString, timeZone);
        if (localCalendar == null) {
            return null;
        }

        localCalendar = setCalendarToBeginningOfDay(localCalendar);

        return localCalendar.getTime();
    }

    /**
     * @see DateUtil#convertToEndOfDayString(String, TimeZone)
     *
     *      Default conversion is UTC-based.
     */
    @CheckForNull
    public static Date convertToEndOfDayString(String dateString) {
        return convertToEndOfDayString(dateString, TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE));
    }

    /**
     * Converts the specified thruDate into the correct Date object.
     *
     * thruDate is specified as in date-only format. Converts into specified
     * timezone with the same day/month/year but specifies time at end of day
     * (23:59:59.999).
     *
     * @param dateString
     *            thruDate to convert
     * @return the correct thruDate date object (with specified day/month/year
     *         but time at 1 second before midnight next day)
     */
    @CheckForNull
    public static Date convertToEndOfDayString(String dateString, TimeZone timeZone) {
        if (timeZone == null) {
            throw new RuntimeException("Need to specify timezone for proper date string conversion!!");
        }

        Calendar localCalendar = determineDateRangeStringCalendar(dateString, timeZone);
        if (localCalendar == null) {
            return null;
        }

        // set with time 1 second before midnight
        localCalendar = setCalendarToEndOfDay(localCalendar);

        return localCalendar.getTime();
    }

    /**
     * Sets the given calendar time to beginning of day (00:00:00).
     *
     * @param calendar
     *            calendar to set
     * @return adjusted calendar (same day/month/year, end of day time), null if
     *         specified calendar is missing
     */
    @CheckForNull
    public static Calendar setCalendarToBeginningOfDay(Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        // set with time 1 second before midnight
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    /**
     * Sets the given calendar time to end of day (23:59:59.999).
     *
     * @param calendar
     *            calendar to set
     * @return adjusted calendar (same day/month/year, end of day time), null if
     *         specified calendar is missing
     */
    @CheckForNull
    public static Calendar setCalendarToEndOfDay(Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        // set with time 1 second before midnight
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return calendar;
    }

    /**
     * @see DateUtil#determineDateRangeStringCalendar(String, TimeZone)
     *
     *      Determiens date range string with default UTC time zone.
     */
    @CheckForNull
    private static Calendar determineDateRangeStringCalendar(String dateString) {
        return determineDateRangeStringCalendar(dateString, TimeZone.getTimeZone(DateConstants.UTC_TIMEZONE));
    }

    /**
     * Determines the calendar for the given date range string.
     *
     * @param dateString
     *            date range string
     * @return local calendar or null otherwise
     */
    @CheckForNull
    private static Calendar determineDateRangeStringCalendar(String dateString, TimeZone timeZone) {
        // basic check (if either date string or timezone is missing, just
        // return null)
        if (dateString == null || timeZone == null) {
            return null;
        }

        Calendar localCalendar = Calendar.getInstance(timeZone);
        SimpleDateFormat sdf = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        try {
            localCalendar.setTime(sdf.parse(dateString));
        } catch (ParseException e) {
            logger.error("Could not parse specified date string " + dateString, e);
            return null;
        }

        return localCalendar;
    }

    /**
     * Format String date to given format
     *
     * @param dateString
     *            String
     * @param oldFormat
     *            String
     * @param newFormat
     *            String
     * @return formatted string
     *
     */
    public static String convertDateFormatString(String dateString, String oldFormat, String newFormat) {
        try {
            Date tempDate = new SimpleDateFormat(oldFormat, Locale.ENGLISH).parse(dateString);
            dateString = new SimpleDateFormat(newFormat, Locale.ENGLISH).format(tempDate);

        } catch (Exception e) {
            logger.error("Could not format data string " + dateString, e);
            return null;
        }
        return dateString;
    }

    /**
     *
     * @param dateString
     *             String in yyyy-mm-dd format
     * @return true if valid calendar date false if not
     *
     * If SimpleDateFormat can not parse dateString it is a invalid calendar date
     */
    public static boolean verifyCalendarDate(String dateString) {
        if (dateString == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        sdf.setLenient(false);

        try {
            sdf.parse(dateString);

        } catch (ParseException e) {
            logger.error("Could not parse date string, invalid date" + dateString, e);
            return false;
        }

        return true;
    }
}
