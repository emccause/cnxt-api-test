package com.maritz.culturenext.util;

import java.math.BigDecimal;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.DoubleCellProcessor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

/**
 * Converts a String to a Double.
 * 
 * @author Kasper B. Graversen
 * 
 * Class modified to avoid Scientific Notation like 2.1E-7
 * line 56: result = new BigDecimal((Double)value).toPlainString();
 * line 64: result = String.valueOf(new Double((String) value));
 */
public class ParseDouble extends CellProcessorAdaptor implements StringCellProcessor {
    
    /**
     * Constructs a new <tt>ParseDouble</tt> processor, which converts a String to a Double.
     */
    public ParseDouble() {
        super();
    }
    
    /**
     * Constructs a new <tt>ParseDouble</tt> processor, which converts a String to a Double, then calls the next
     * processor in the chain.
     * 
     * @param next
     *            the next processor in the chain
     * @throws NullPointerException
     *             if next is null
     */
    public ParseDouble(final DoubleCellProcessor next) {
        super(next);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @throws SuperCsvCellProcessorException
     *             if value is null, isn't a Double or String, or can't be parsed as a Double
     */
    public Object execute(final Object value, final CsvContext context) {
        validateInputNotNull(value, context);

        String result = new String();
        if (value instanceof Double) {
            try {
                result = new BigDecimal((Double) value).toPlainString();
            } catch (final NumberFormatException e) {
                throw new SuperCsvCellProcessorException(String.format("'%s' could not be parsed as a Double", value),
                        context, this, e);
            }
        } else if (value instanceof String) {
            try {
                result = String.valueOf(new Double((String) value));
            } catch (final NumberFormatException e) {
                throw new SuperCsvCellProcessorException(String.format("'%s' could not be parsed as a Double", value),
                        context, this, e);
            }
        } else {
            final String actualClassName = value.getClass().getName();
            throw new SuperCsvCellProcessorException(String
                    .format("the input value should be of type Double or String but is of type %s", actualClassName),
                    context, this);
        }

        return next.execute(result, context);
    }
}