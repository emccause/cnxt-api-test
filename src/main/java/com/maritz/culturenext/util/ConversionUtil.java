package com.maritz.culturenext.util;

import javax.annotation.CheckForNull;
import java.math.BigDecimal;

/**
 * Utility class for handling conversions or any logic regarding transformation of
 * object mappings to types.
 */
public class ConversionUtil {
    /**
     * Transforms and converts the given object map value to the respective integer value.
     *
     * @param objectMapValue value given in object mapping
     * @return respective integer value
     */
    @CheckForNull
    public static Integer convertIntegerObjectMapValue(Object objectMapValue) {
        if(objectMapValue == null) {
            return null;
        }
        if(objectMapValue instanceof BigDecimal) {
            return ((BigDecimal) objectMapValue).intValue();
        } else if(objectMapValue instanceof Number) {
            return ((Number) objectMapValue).intValue();
        } else if(objectMapValue instanceof String){
            return (Integer.valueOf((String) objectMapValue));
        } else {
            // not instance of number so just return null
            return null;
        }
    }

    /**
     * Transforms and converts the given object map value to the respective double value.
     *
     * @param objectMapValue value given in object mapping
     * @return respective double value
     */
    @CheckForNull
    public static Double convertDoubleObjectMapValue(Object objectMapValue) {
        if(objectMapValue == null) {
            return null;
        }

        if(objectMapValue instanceof BigDecimal) {
            return ((BigDecimal) objectMapValue).doubleValue();
        } else if(objectMapValue instanceof Number) {
            return ((Number) objectMapValue).doubleValue();
        } else if(objectMapValue instanceof String){
            return (Double.valueOf((String) objectMapValue));
        } else {
            // not instance of number so just return null
            return null;
        }
    }

    /**
     * Transforms and converts the given object map value to the respective double value.
     *
     * @param objectMapValue value given in object mapping
     * @return respective double value
     */
    @CheckForNull
    public static Long convertLongObjectMapValue(Object objectMapValue) {
        if(objectMapValue == null) {
            return null;
        }

        if(objectMapValue instanceof BigDecimal) {
            return ((BigDecimal) objectMapValue).longValue();
        } else if(objectMapValue instanceof Number) {
            return ((Number) objectMapValue).longValue();
        } else if(objectMapValue instanceof String){
            return (Long.valueOf((String) objectMapValue));
        } else {
            // not instance of number so just return null
            return null;
        }
    }

    /**
     * Transforms and converts the given object map value to the respective BigDecimal value.
     *
     * @param objectMapValue value given in object mapping
     * @return respective BigDecimal value
     */
    @CheckForNull
    public static BigDecimal convertBigDecimalObjectMapValue(Object objectMapValue) {
        if(objectMapValue == null) {
            return null;
        }

        if(objectMapValue instanceof BigDecimal) {
            return ((BigDecimal) objectMapValue);
        } else if(objectMapValue instanceof Number) {
            Number number = (Number) objectMapValue;
            return new BigDecimal(number.toString());
        } else if(objectMapValue instanceof String){
            return new BigDecimal((String) objectMapValue);
        } else {
            // not instance of BigDecimal so just return null
            return null;
        }
    }
}
