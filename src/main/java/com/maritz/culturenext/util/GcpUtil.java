package com.maritz.culturenext.util;

import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.enums.MediaTypesEnum;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.UUID;

@Component
public class GcpUtil {
    private static final String SPRING_GCP_STORAGE_ENABLED_PROPERTY = "spring.cloud.gcp.storage.enabled";

    private static final String ENCRYPTION_ALGORITHM = "HmacSHA1";

    private static final String QUERY_STRING_DELIMITER = "?";
    private static final String QUERY_PARAMETER_DELIMITER = "&";

    private static final String URL_TO_SIGN_TEMPLATE = "%sExpires=%d&KeyName=%s";
    private static final String SIGNED_URL_TEMPLATE = "%s&Signature=%s";

    private static final String IMAGE_URL_TEMPLATE = "%s/%s/%s";

    private static final String SECURE_CDN_URL_PROPERTY = "cdn.secure.url";
    private static final String SECURE_CDN_BUCKET = "cdn.secure.bucket";
    private static final String CLIENT_NAME_PROPERTY = "client.name";

    private static final String URL_SIGNING_KEY_NAME_PROPERTY = "url.signing.key-name";
    private static final String URL_SIGNING_KEY_PROPERTY = "url.signing.key";

    private static final String FILE_NAME_TEMPLATE = "%s.%s";

    private static final String WEB_URL_SIGNING_DURATION_PROPERTY = "url.signing.duration.web";
    private static final int DEFAULT_URL_SIGNING_DURATION = 60;

    private static final String UPLOAD_FILE_ERROR_MESSAGE_TEMPLATE = "Failed to write File ID %d to the CDN";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Value("${" + SPRING_GCP_STORAGE_ENABLED_PROPERTY + ":#{null}}")
    private Boolean springGcpCloudEnabled;

    private Storage storage;
    private EnvironmentUtil environmentUtil;
    private FilesRepository filesRepository;

    @Inject
    public GcpUtil setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public GcpUtil setFilesRepository(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
        return this;
    }

    @Autowired(required = false)
    public GcpUtil setStorage(Storage storage) {
        this.storage = storage;
        return this;
    }

    private Storage getStorage() {
        if (storage == null) {
            throw new IllegalStateException("Google Storage does not exist in this environment; " + SPRING_GCP_STORAGE_ENABLED_PROPERTY + "=" + springGcpCloudEnabled + ".  " +
                    "(This property is used by Spring GCP Cloud to determine whether or not to initialize Google Storage at container startup; default is `true`.)"
            );
        }
        return storage;
    }

    /**
     * Creates a signed URL for a Cloud CDN endpoint with the given key
     * URL must start with http:// or https://, and must contain a forward
     * slash (/) after the hostname.
     *
     * @param url the Cloud CDN endpoint to sign
     * @param base64SigningKey URL signing key uploaded to the backend service/bucket, as a base-64 encoded string
     * @param keyName the name of the signing key added to the back end bucket or service
     * @param expiration the date that the signed URL expires
     * @return a properly formatted signed URL
     * @throws InvalidKeyException when there is an error generating the signature for the input key
     * @throws NoSuchAlgorithmException when the encryption algorithm is not available in the environment
     *
     * Adapted from https://github.com/GoogleCloudPlatform/java-docs-samples/blob/master/cdn/signed-urls/src/main/java/com/google/cdn/SignedUrls.java
     */
    public String signUrl(String url,
                                 String base64SigningKey,
                                 String keyName,
                                 ZonedDateTime expiration)
            throws InvalidKeyException, NoSuchAlgorithmException {
        url = url + (url.contains(QUERY_STRING_DELIMITER) ? QUERY_PARAMETER_DELIMITER : QUERY_STRING_DELIMITER);
        String urlToSign = String.format(URL_TO_SIGN_TEMPLATE, url, expiration.toEpochSecond(), keyName);
        return String.format(SIGNED_URL_TEMPLATE, urlToSign, getSignature(Base64.getUrlDecoder().decode(base64SigningKey), urlToSign));
    }

    public String signUrl(String fileName, long durationInSeconds) throws InvalidKeyException, NoSuchAlgorithmException {
        return signUrl(String.format(IMAGE_URL_TEMPLATE, getProperty(SECURE_CDN_URL_PROPERTY), getClientName(), fileName),
                            getProperty(URL_SIGNING_KEY_PROPERTY), getProperty(URL_SIGNING_KEY_NAME_PROPERTY),
                            ZonedDateTime.now().plusSeconds(durationInSeconds));
    }

    public String signUrl(Files file) throws NoSuchAlgorithmException, InvalidKeyException {
        return signUrl(file, environmentUtil.getIntProperty(WEB_URL_SIGNING_DURATION_PROPERTY, DEFAULT_URL_SIGNING_DURATION));
    }

    public String signUrl(Files file, long durationInSeconds) throws NoSuchAlgorithmException, InvalidKeyException {
        if (file == null) {
            return null;
        }
        if (file.getUrl() == null) {
            throw new IllegalStateException("Encountered a file with no URL: " + file);
        }
        return signUrl(file.getUrl(), durationInSeconds);
    }

    private String getProperty(String key) {
        return environmentUtil.getProperty(key);
    }

    private String getClientName() {
        return getProperty(CLIENT_NAME_PROPERTY);
    }

    private static String getSignature(byte[] privateKey, String input) throws InvalidKeyException, NoSuchAlgorithmException {
        Key key = new SecretKeySpec(privateKey, 0, privateKey.length, ENCRYPTION_ALGORITHM);
        Mac mac = Mac.getInstance(ENCRYPTION_ALGORITHM);
        mac.init(key);
        return  Base64.getUrlEncoder().encodeToString(mac.doFinal(input.getBytes()));
    }

    public String uploadFile(String bucket, String folder, String fileName, byte[] data, String contentType) {
        String objectName = (folder != null) ? folder + "/" + fileName : fileName;
        BlobId blobId = BlobId.of(bucket, objectName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId)
                .setContentType(contentType)
                .build();
        getStorage().create(blobInfo, data);
        return fileName;
    }

    public String uploadFile(byte[] data, MediaTypesEnum mediaType) {
        // We are purposefully ignoring any pre-existing file name and creating a new file name based on a UUID every time,
        //      due to CDN caching issues mentioned in CNXT-777.
        String fileName = String.format(FILE_NAME_TEMPLATE, UUID.randomUUID().toString(), mediaType.getFileExtension());
        return uploadFile(getProperty(SECURE_CDN_BUCKET), getClientName(), fileName, data, mediaType.value());
    }

    public String uploadFile(StreamingOutput streamingOutput, MediaTypesEnum mediaType) throws IOException {
        return uploadFile(toByteArray(streamingOutput), mediaType);
    }

    private static byte[] toByteArray(StreamingOutput streamingOutput) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        streamingOutput.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void uploadFile(long fileId, byte[] data) {
        try {
            Files file = filesRepository.getOne(fileId);
            String fileName = uploadFile(data, MediaTypesEnum.fromValue(file.getMediaType()));
            file.setUrl(fileName);
            filesRepository.save(file);
        } catch (Exception e) {
            logger.error(String.format(UPLOAD_FILE_ERROR_MESSAGE_TEMPLATE, fileId), e);
            // TODO: In the future, when we no longer write to the database and write only to the CDN,
            //          this will become a critical error that must be thrown.
        }
    }

    public void uploadFile(long fileId, StreamingOutput streamingOutput) {
        try {
            uploadFile(fileId, toByteArray(streamingOutput));
        } catch (IOException e) {
            logger.error(String.format(UPLOAD_FILE_ERROR_MESSAGE_TEMPLATE, fileId), e);
            // TODO: In the future, when we no longer write to the database and write only to the CDN,
            //          this will become a critical error that must be thrown.
        }
    }

    public void uploadFile(long fileId, MultipartFile file) {
        try {
            uploadFile(fileId, IOUtils.toByteArray(file.getInputStream()));
        } catch (IOException e) {
            logger.error(String.format(UPLOAD_FILE_ERROR_MESSAGE_TEMPLATE, fileId), e);
            // TODO: In the future, when we no longer write to the database and write only to the CDN,
            //          this will become a critical error that must be thrown.
        }
    }
}
