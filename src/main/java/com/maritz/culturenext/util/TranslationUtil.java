package com.maritz.culturenext.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Translatable;
import com.maritz.core.jpa.entity.TranslatablePhrase;
import com.maritz.core.jpa.repository.TranslatablePhraseRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.content.dao.TranslatableContentDao;
import com.maritz.culturenext.content.dto.TranslatableContentDTO;
import com.maritz.culturenext.content.dto.TranslatableContentExtendedDTO;

@Component
public class TranslationUtil {

    @Inject private ConcentrixDao<Translatable> translatableDao;
    @Inject private ConcentrixDao<TranslatablePhrase> translatablePhraseDao;
    @Inject private TranslatablePhraseRepository translatablePhraseRepository;
    @Inject private TranslatableContentDao translatableContentDao;

    private static final String TABLE_NAME_COLUMN_NAME = "tableName_columnName";
    private static final String INVALID_TABLE_OR_COLUMN = "INVALID_TABLE_OR_COLUMN";
    private static final String INVALID_TABLE_OR_COLUMN_MSG = "Invalid table/column name combination";
    private static final String INVALID_TO_TRANSLATE = "INVALID_TO_TRANSLATE";
    private static final String INVALID_TO_TRANSLATE_MSG = "Invalid toTranslate id";

    /**
     * Creates a map of ids from the table represented by tableName to the translations of that data
     * Null if no translations exist
     * @param languageCode
     * @param tableName
     * @param columnName
     * @param ids
     * @return
     */
    public Map<Long, String> getTranslationsForListOfIds(String languageCode, String tableName,
            String columnName, List<Long> ids, Map<Long, String> defaultMap) {
        if (defaultMap == null) {
            defaultMap = new HashMap<>();
        }

        // Input sanity check
        if (languageCode == null || languageCode.equals(ProjectConstants.DEFAULT_LOCALE_CODE)
                || tableName == null || columnName == null || CollectionUtils.isEmpty(ids)) {
            return defaultMap;
        }

        // Find and validate that the table/column combination is translatable
        Translatable translatable = translatableDao.findBy()
                .where(ProjectConstants.TABLE_NAME).eq(tableName)
                .and(ProjectConstants.COLUMN_NAME).eq(columnName)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .findOne();

        if (translatable == null) {
            return defaultMap;
        }

        // Find any translations that exist
        List<TranslatablePhrase> translatablePhraseList = translatablePhraseDao.findBy()
                .where(ProjectConstants.TRANSLATABLE_ID).eq(translatable.getId())
                .and(ProjectConstants.TARGET_ID).in(ids)
                .and(ProjectConstants.LOCALE_CODE).eq(LocaleUtil.formatLocaleCode(languageCode))
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        if (CollectionUtils.isEmpty(translatablePhraseList)) {
            return defaultMap;
        }
        
        Map<Long, String> idToTranslationMap = new HashMap<>();
        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
            idToTranslationMap.put(translatablePhrase.getTargetId(), translatablePhrase.getTranslation());
        }

        return defaultTranslationMap(idToTranslationMap, defaultMap);
    }

    /**
     * Creates a map of columnNames from the table represented by tableName and the
     * row represented by id to the translations of that data
     * Null if no translations exist
     * @param languageCode
     * @param tableName
     * @param columnNames
     * @param id
     * @return
     */
    public Map<String, String> getTranslationsForListOfColumns(String languageCode, String tableName,
            List<String> columnNames, Long id, Map<String, String> defaultMap) {
        
        if (defaultMap == null) {
            defaultMap = new HashMap<>();
        }

        if (languageCode == null || languageCode.equals(ProjectConstants.DEFAULT_LOCALE_CODE)
                || tableName == null || CollectionUtils.isEmpty(columnNames) || id == null) {
            return defaultMap;
        }

        // Find and validate that the table/column combinations are translatable
        List<Translatable> translatableList = translatableDao.findBy()
                .where(ProjectConstants.TABLE_NAME).eq(tableName)
                .and(ProjectConstants.COLUMN_NAME).in(columnNames)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        if (CollectionUtils.isEmpty(translatableList)) {
            return defaultMap;
        }

        Map<Long, String> translatableIdToColumnNameMap = new HashMap<>();
        for (Translatable translatable : translatableList) {
            translatableIdToColumnNameMap.put(translatable.getId(), translatable.getColumnName());
        }

        // Find and validate that the rows in the table have translations
        List<TranslatablePhrase> translatablePhraseList = translatablePhraseDao.findBy()
                .where(ProjectConstants.TRANSLATABLE_ID).in(translatableIdToColumnNameMap.keySet())
                .and(ProjectConstants.TARGET_ID).eq(id)
                .and(ProjectConstants.LOCALE_CODE).eq(LocaleUtil.formatLocaleCode(languageCode))
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        if (CollectionUtils.isEmpty(translatablePhraseList)) {
            return defaultMap;
        }

        // Map column to translation
        Map<String, String> columnNameToTranslationMap = new HashMap<>();
        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
            String columnName = translatableIdToColumnNameMap.get(translatablePhrase.getTranslatableId());
            columnNameToTranslationMap.put(columnName, translatablePhrase.getTranslation());
        }

        return defaultTranslationMap(columnNameToTranslationMap, defaultMap);
    }

    /**
     * Retrieves only the platform-specific content that is needed for translations.
     * Does not return translated values. It is assumed that this will be called
     *         when new languages are added to the platform.
     *
     * Platform specific content is application_data values used as
     *         dynamic-strings in emails and all of the recognition holidays
     * @return
     */
    public List<Map<String, Object>> getPlatformTranslatableContent() {
        return translatableContentDao.getPlatformTranslatableContent();
    }

    /**
     * Retrieves all translatable content currently in the database, with an optional filter by table name
     * @param tableName (optional) Table name by which to filter results
     * @return
     */
    public List<TranslatableContentDTO> getAllTranslatableContent(String tableName) {
        
        FindBy<Translatable> translatableFindBy = translatableDao.findBy()
                .where(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name());

        if (!StringUtils.isBlank(tableName)) {
            translatableFindBy = translatableFindBy.and(ProjectConstants.TABLE_NAME).eq(tableName);
        }

        List<Translatable> translatableList = translatableFindBy.find();

        if (CollectionUtils.isEmpty(translatableList)) {
            return new ArrayList<>();
        }

        Map<Long, Translatable> idToTranslatableMap = new HashMap<>();
        for (Translatable translatable : translatableList) {
            idToTranslatableMap.put(translatable.getId(), translatable);
        }

        List<TranslatablePhrase> translatablePhraseList = translatablePhraseDao.findBy()
                .where(ProjectConstants.TRANSLATABLE_ID).in(idToTranslatableMap.keySet())
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        if (CollectionUtils.isEmpty(translatablePhraseList)) {
            return new ArrayList<>();
        }

        Map<Long, TranslatablePhrase> idToTranslatablePhraseMap = new HashMap<>();
        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
            idToTranslatablePhraseMap.put(translatablePhrase.getId(), translatablePhrase);
        }
        
        // Mapping phraseTranslation objects with translatablePhrase id.
        Map<Long, List<TranslatablePhrase>> translatablePhraseTranslationsMap = new HashMap<>();
        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
            List<TranslatablePhrase> translatablePhraseSubList = 
                    translatablePhraseTranslationsMap.get(translatablePhrase.getTargetId());
            
            if (CollectionUtils.isEmpty(translatablePhraseSubList)) {
                translatablePhraseSubList = new ArrayList<TranslatablePhrase>();
            }

            translatablePhraseSubList.add(translatablePhrase);                
            translatablePhraseTranslationsMap.put(translatablePhrase.getTargetId(), 
                    translatablePhraseSubList);
        }

        // Creating map content based on translatablePhrase list objects.
        Map<String, TranslatableContentDTO> translatableIdToDTOMap = new HashMap<>();
        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {

            TranslatableContentDTO contentDTO = new TranslatableContentDTO();
            Translatable translatable = idToTranslatableMap.get(translatablePhrase.getTranslatableId());
            contentDTO = new TranslatableContentDTO();
            contentDTO.setTargetType(translatable.getTableName());
            contentDTO.setField(translatable.getColumnName());
            contentDTO.setTargetId(translatablePhrase.getTargetId());

            List<TranslatablePhrase> translatablePhraseSubList =  
                    translatablePhraseTranslationsMap.get(translatablePhrase.getTargetId());
            Map<String, String> translationsMap = new HashMap<String, String>();

            if(!CollectionUtils.isEmpty(translatablePhraseSubList)) {
                for(TranslatablePhrase  phraseTranslation: translatablePhraseSubList) {
                    if (phraseTranslation.getTranslatableId().equals(translatable.getId())) {
                        translationsMap.put(phraseTranslation.getLocaleCode(), phraseTranslation.getTranslation());
                    }
                }
            }
            contentDTO.setTranslations(translationsMap);            
        
            //Make sure we don't already have a mapping for this translatableID / targetID combo
            String key = translatable.getId().toString() + "." + translatablePhrase.getTargetId().toString();
            if (!translatableIdToDTOMap.containsKey(key)) {
                translatableIdToDTOMap.put(key, contentDTO);
            }
            
        }

        return new ArrayList<>(translatableIdToDTOMap.values());
    }

    /**
     * Updates or adds all entries passed in to this method. Does not delete anything.
     * @param content List of DTOs containing the contents to be saved to the database
     */
    public void updateTranslatableContent(List<TranslatableContentDTO> content) {
        if (CollectionUtils.isEmpty(content)) {
            return;
        }

        // Map input DTOs to extended version to hold ids as we go
        List<TranslatableContentExtendedDTO> extendedContent = new ArrayList<>();
        List<String> tableNameList = new ArrayList<>();
        List<String> columnNameList = new ArrayList<>();
        List<Long> targetIdList = new ArrayList<>();

        for (TranslatableContentDTO contentDTO : content) {
            extendedContent.add(transformToExtendedDTO(contentDTO));
            if (!tableNameList.contains(contentDTO.getTargetType())) {
                tableNameList.add(contentDTO.getTargetType());
            }
            if (!columnNameList.contains(contentDTO.getField())) {
                columnNameList.add(contentDTO.getField());
            }
            if (!targetIdList.contains(contentDTO.getTargetId())) {
                targetIdList.add(contentDTO.getTargetId());
            }
        }

        List<Translatable> translatableList = translatableDao.findBy()
                .where(ProjectConstants.TABLE_NAME).in(tableNameList)
                .or(ProjectConstants.COLUMN_NAME).in(columnNameList)
                .find();
        translatableList = translatableList != null ? translatableList : new ArrayList<Translatable>();

        List<Long> translatableIdList = new ArrayList<>();
        // Identify the TRANSLATABLE entry per DTO, or fail
        for (TranslatableContentExtendedDTO extendedDTO : extendedContent) {
            boolean found = false;
            for (Translatable translatable : translatableList) {
                if (translatable.getTableName().equals(extendedDTO.getTargetType())
                        && translatable.getColumnName().equals(extendedDTO.getField())) {
                    extendedDTO.setTranslatableId(translatable.getId());
                    if(!translatableIdList.contains(translatable.getId())) {
                        translatableIdList.add(translatable.getId());
                    }
                    found = true;
                    break;
                }
            }
            if (!found) {
                throw new ErrorMessageException(new ErrorMessage()
                        .setField(TABLE_NAME_COLUMN_NAME)
                        .setMessage(INVALID_TABLE_OR_COLUMN_MSG)
                        .setCode(INVALID_TABLE_OR_COLUMN));
            }
        }

        List<TranslatablePhrase> translatablePhraseList = translatablePhraseDao.findBy()
                .where(ProjectConstants.TARGET_ID).in(targetIdList)
                .find();
        translatablePhraseList = translatablePhraseList != null ? 
                translatablePhraseList : 
            new ArrayList<TranslatablePhrase>();

        // Save all non-default translations to the TRANSLATABLE_PHRASE table
        List<TranslatablePhrase> translatablePhraseSaveList = new ArrayList<>();
        for (TranslatableContentExtendedDTO extendedDTO : extendedContent) {
            for (String localeCode : extendedDTO.getTranslations().keySet()) {
                if (!StringUtils.isBlank(extendedDTO.getTranslations().get(localeCode)) &&
                        !ProjectConstants.DEFAULT_LOCALE_CODE.equals(localeCode)) {
                    TranslatablePhrase foundTranslatablePhrase = null;
                    boolean found = false;
                    for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
                        if (translatablePhrase.getTargetId().equals(extendedDTO.getTargetId())
                                && translatablePhrase.getTranslatableId().equals(extendedDTO.getTranslatableId())) {
                            // Make sure this is a valid translatable record
                            found = true;
                        }
                        if (found == true && translatablePhrase.getLocaleCode() != null
                                && translatablePhrase.getLocaleCode().equals(localeCode)
                                && translatablePhrase.getTranslatableId().equals(extendedDTO.getTranslatableId())) {
                            foundTranslatablePhrase = translatablePhrase;
                            break;
                        }
                    }
                    if (!found) {
                        // If an invalid translatable id / invalid target id combo is found, throw an error
                        throw new ErrorMessageException(new ErrorMessage()
                                .setField(ProjectConstants.TO_TRANSLATE)
                                .setMessage(INVALID_TO_TRANSLATE_MSG)
                                .setCode(INVALID_TO_TRANSLATE));
                    }
                    if (foundTranslatablePhrase == null) {
                        foundTranslatablePhrase = new TranslatablePhrase();
                        foundTranslatablePhrase.setTranslatableId(extendedDTO.getTranslatableId());
                        foundTranslatablePhrase.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
                        foundTranslatablePhrase.setLocaleCode(localeCode);
                        foundTranslatablePhrase.setTargetId(extendedDTO.getTargetId());
                    }
                    // Reduce DB calls -- don't save if nothing changed!
                    if (foundTranslatablePhrase.getTranslation() == null
                            || !extendedDTO.getTranslations().get(localeCode)
                            .equals(foundTranslatablePhrase.getTranslation())) {
                        foundTranslatablePhrase.setTranslation(extendedDTO.getTranslations().get(localeCode));
                        translatablePhraseSaveList.add(foundTranslatablePhrase);
                    }
                }
            }
        }

        // bulk save updates
        if (!CollectionUtils.isEmpty(translatablePhraseSaveList)) {
            translatablePhraseRepository.save(translatablePhraseSaveList);
            
            // last thing - delete placeholders.  Records that needed translations were inserted with null locale code / translation
            translatablePhraseDao.findBy()
                .where(ProjectConstants.TRANSLATABLE_ID).in(translatableIdList)
                .and(ProjectConstants.TARGET_ID).in(targetIdList)
                .and(ProjectConstants.LOCALE_CODE).isNull()
                .delete();    
        }
    }

    private <T> Map<T, String> defaultTranslationMap(Map<T, String> translationMap, Map<T, String> defaultMap) {
        // If the default map is null for some reason, at least return the translations
        if (MapUtils.isEmpty(defaultMap)) {
            return translationMap;
        }

        Map<T, String> translatedMap = new HashMap<>();
        for (T key : defaultMap.keySet()) {
            if (translationMap.get(key) != null) {
                translatedMap.put(key, translationMap.get(key));
            } else {
                translatedMap.put(key, defaultMap.get(key));
            }
        }
        return translatedMap;
    }

    /**
     * Populates TRANSLATABLE and TRANSLATABLE_PHRASE tables with data
     * @param tableName
     * @param columnName
     * @param toTranslate
     * @param active
     */
    public void saveTranslationData(String tableName, String columnName, Long toTranslate, String status) {

        // Validate input params.
        if(tableName == null || tableName.isEmpty()
                || columnName == null || columnName.isEmpty()
                || toTranslate == null || toTranslate == 0){
            return;
        }

        // set default status to ACTIVE
        if(status == null || status.isEmpty()) {
            status = StatusTypeCode.ACTIVE.name();
        }

        String displayName = tableName + ProjectConstants.SLASH + columnName;

        Translatable translatable = translatableDao.findBy()
                .where(ProjectConstants.TABLE_NAME).eq(tableName)
                .and(ProjectConstants.COLUMN_NAME).eq(columnName)
                .findOne();

        if(translatable == null){
            translatable = new Translatable();
            translatable.setDisplayName(displayName);
            translatable.setStatusTypeCode(status);
            translatable.setTableName(tableName);
            translatable.setColumnName(columnName);

            translatableDao.save(translatable);
        }

        if(translatable.getId() != null ){
            TranslatablePhrase translatablePhrase = translatablePhraseDao.findBy()
                    .where(ProjectConstants.TRANSLATABLE_ID).eq(translatable.getId())
                    .and(ProjectConstants.TARGET_ID).eq(toTranslate)
                    .findOne();

            if(translatablePhrase == null){
                translatablePhrase = new TranslatablePhrase();
                translatablePhrase.setTranslatableId(translatable.getId());
                translatablePhrase.setStatusTypeCode(status);
                translatablePhrase.setTargetId(toTranslate);
                // rows that will need translations will have a null locale code / translation
                translatablePhrase.setLocaleCode(null);
                translatablePhrase.setTranslation(null);

                translatablePhraseDao.save(translatablePhrase);
            }
        }
    }

    private TranslatableContentExtendedDTO transformToExtendedDTO(TranslatableContentDTO contentDTO) {
        TranslatableContentExtendedDTO extendedDTO = new TranslatableContentExtendedDTO();
        extendedDTO.setTargetType(contentDTO.getTargetType());
        extendedDTO.setTargetId(contentDTO.getTargetId());
        extendedDTO.setField(contentDTO.getField());

        Map<String, String> translations = new HashMap<>();
        for (String key : contentDTO.getTranslations().keySet()) {
            translations.put(key, contentDTO.getTranslations().get(key));
        }

        extendedDTO.setTranslations(translations);
        return extendedDTO;
    }
}
