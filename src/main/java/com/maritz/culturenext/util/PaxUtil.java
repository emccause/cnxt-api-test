package com.maritz.culturenext.util;


import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PaxUtil {

    @Inject private ParticipantInfoDao participantInfoDao;

    public Map<Long, EmployeeDTO> getPaxIdEmployeeDTOMap (List<Long> paxIds) {
        // Map the EntityDTOs to EmployeeDTOs, as they should all be of that type anyway
        List<EntityDTO>    genericParticipantInfoList = participantInfoDao.getInfo(paxIds, null);

        Map<Long, EmployeeDTO> employeeMap = new HashMap<>();
        for (EntityDTO entityDTO : genericParticipantInfoList) {
            EmployeeDTO employee = (EmployeeDTO) entityDTO;
            if (employee != null && employee.getPaxId() != null) {
                employeeMap.put(employee.getPaxId(), employee);
            }
        }

        return employeeMap;
    }
}
