package com.maritz.culturenext.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class EnvironmentUtil {
    private static final String PROPERTY_DELIMITER = ",";

    private static final String IMAGE_READ_STRATEGY_PROPERTY = "image.read.strategy";
    private static final String IMAGE_WRITE_STRATEGIES_PROPERTY = "image.write.strategies";
    private static final String CDN_STRATEGY_VALUE = "CDN";

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private Environment environment;

    @Inject
    public EnvironmentUtil setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }

    public String getProperty(String key, boolean errorIfNonExistent) {
        String property = environment.getProperty(key);
        if (errorIfNonExistent && (property == null)) {
            throw new IllegalStateException("Property \"" + key + "\" has no value.");
        }
        return property;
    }

    public String getProperty(String key) {
        return getProperty(key, true);
    }

    public int getIntProperty(String key, int defaultValue) {
        String property = getProperty(key, false);
        if (property == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(property);
        } catch (NumberFormatException e) {
            logger.warn("Property \"" + key + "\" contains value \"" + property + "\", which cannot be parsed to an integer; using default value of " + defaultValue);
            return defaultValue;
        }
    }

    public long getLongProperty(String key, long defaultValue) {
        String property = getProperty(key, false);
        if (property == null) {
            return defaultValue;
        }
        try {
            return Long.parseLong(property);
        } catch (NumberFormatException e) {
            logger.warn("Property \"" + key + "\" contains value \"" + property + "\", which cannot be parsed to a long; using default value of " + defaultValue);
            return defaultValue;
        }
    }


    public List<String> getProperties(String key, boolean errorIfNonExistent) {
        String property = getProperty(key, errorIfNonExistent);
        if (property == null) {
            return Collections.emptyList();
        }
        return Arrays.asList(property.split(PROPERTY_DELIMITER));
    }

    public List<String> getProperties(String key) {
        return getProperties(key, true);
    }

    public boolean contains(String key, String value, boolean errorIfNonExistent) {
        for (String property : getProperties(key, errorIfNonExistent)) {
            if (property.equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }

    public boolean contains(String key, String value) {
        return contains(key, value, true);
    }

    public boolean readFromCdn() {
        return CDN_STRATEGY_VALUE.equalsIgnoreCase(getProperty(IMAGE_READ_STRATEGY_PROPERTY, false));
    }

    public boolean writeToCdn() {
        return contains(IMAGE_WRITE_STRATEGIES_PROPERTY, CDN_STRATEGY_VALUE, false);
    }
}
