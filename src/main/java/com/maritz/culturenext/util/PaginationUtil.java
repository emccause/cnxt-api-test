package com.maritz.culturenext.util;

import java.util.ArrayList;
import java.util.List;

import com.maritz.culturenext.constants.ProjectConstants;

public class PaginationUtil {

    /**
     * Completes the actual paginating of the generic list passed in
     * @param list the list to be paginated
     * @param page the page selected; if none selected default page will be used
     * @param size the size of the given page; if none selected default size will be used
     * @param <E> the generic list type
     * @return the paginated list based on the page and size passed in
     */
    public static <E> List<E> getPaginatedList(List<E> list, Integer page, Integer size) {

        if (page == null || page <= 0) {
            page = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (size == null || size <= 0) {
            size = ProjectConstants.DEFAULT_PAGE_SIZE;
        }

        int lower = (page - 1) * size;
        int upper = (page * size);

        if (lower > list.size()) {
            return new ArrayList<>(); // There is nothing to return, the lower bounds is too high
        } else if (upper > list.size()) {
            upper = list.size();
        }
        return list.subList(lower, upper);
    }
}
