package com.maritz.culturenext.util;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileDownloadUtil {

    final static Logger logger = LoggerFactory.getLogger(Object.class);

    final static String CHARSET_UTF_8 = "UTF-8";
    
    public static void downloadFile(HttpServletResponse response, String fileContent, String fileType, String fileName) {
        // Set default file name if one does not exist
        if (fileName == null)
            fileName = ProjectConstants.DOWNLOAD;

        // If response is missing, then we can't send the file so warn and return
        if (response == null) {
            logger.warn("Response is missing!");
            return;
        }

        response.setContentType(fileType + ";charset=utf-8");
        response.setCharacterEncoding(CHARSET_UTF_8);
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

        if (fileContent != null) {
            try {
                // Specify the BOM (Byte order Mask) for UTF-8 so that Excel can
                // identify the encoding and open it in the correct format
                if (fileType.equals(MediaTypesEnum.TEXT_CSV.value())) {
                    response.getOutputStream().write(new byte[] { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF });
                }
                response.getOutputStream().write(fileContent.getBytes(CHARSET_UTF_8));
            } catch (IOException e) {
                logger.error("File Download Error", e);
            }
        }

        try {
            // once all data has been written, close the stream to prevent other data from being written
            // (assumes no additional output after download of CSV)
            response.getOutputStream().close();
        } catch (IOException e) {
            logger.error("File Output Error", e);
        }
    }
}
