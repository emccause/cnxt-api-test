package com.maritz.culturenext.util;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;

@Component
public class ConvertPathVariablesUtil {

    @Inject private Security security;

    /**
     * Transform any id string type to long.
     * @param paxIdString
     * @return paxId as long, or throw an exception with 404 status code.
     * @throws NoHandlerFoundException 
     */
    public Long getId(String value) throws NoHandlerFoundException {
        
        Long id = security.getId(value, null);
        if (id == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return id;
    }
    
}
