package com.maritz.culturenext.util;

public class LocaleUtil {
    /**
     * Formats a 5-character locale code into the form "xx_XX"
     * @param localeCode
     * @return null if malformed localeCode input, formatted localeCode String if valid
     */
    public static String formatLocaleCode(String localeCode) {
        if (localeCode == null || !localeCode.matches("^[A-z][A-z](_|-)[A-z][A-z]$")) {
            return null;
        }

        String language = localeCode.substring(0,2);
        String country = localeCode.substring(3,5);

        return language.toLowerCase() + "_" + country.toUpperCase();
    }

    public static String extractLanguageCodeFromLocaleCode(String localeCode) {
        if (localeCode == null) {
            return null;
        }

        return localeCode.substring(0,2);
    }

    public static String extractCountryCodeFromLocaleCode(String localeCode) {
        if (localeCode == null) {
            return null;
        }

        return localeCode.substring(3,5);
    }
}
