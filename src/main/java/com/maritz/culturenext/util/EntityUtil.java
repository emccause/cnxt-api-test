package com.maritz.culturenext.util;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * General utility class for any operations associated with entities.
 */
public class EntityUtil {
    private static final Logger logger = LoggerFactory.getLogger(EntityUtil.class);

    //Subject Types
    private static final String GROUPS = "GROUPS";
    private static final String PAX = "PAX";
    private static final String INPUT_TYPE = "inputType";

    //pax columns
    private static final String STATUS = "status";

    // eligibility columns
    private static final String INPUT_ID = "inputId";

    // eligibility status
    private static final String DOES_NOT_EXIST_STATUS = "DOES_NOT_EXIST";

    // entity error mapping keys
    private static final String ERROR_ENTITY_NOT_FOUND = "ENTITY_NOT_FOUND";
    private static final String ERROR_ENTITY_NOT_FOUND_MSG = "ERROR_ENTITY_NOT_FOUND_MSG";
    private static final String ERROR_PAX_ENTITY_NOT_ACTIVE = "PAX_ENTITY_NOT_ACTIVE";
    private static final String ERROR_PAX_ENTITY_NOT_ACTIVE_MSG = "ERROR_PAX_ENTITY_NOT_ACTIVE_MSG";
    private static final String ERROR_GROUP_ENTITY_NOT_ACTIVE = "GROUP_ENTITY_NOT_ACTIVE";
    private static final String ERROR_GROUP_ENTITY_NOT_ACTIVE_MSG = "ERROR_GROUP_ENTITY_NOT_ACTIVE_MSG";
    private static final String ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE = "ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE";
    private static final String ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE_MSG = "ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE_MSG";
    
    /**
     * Filters eligibility stub DTOs to remove duplicate entities.
     *
     * @param eligibilityEntityStubDTOs eligibility stub DTOs
     * @return eligibility stub DTOs (non-duplicate)
     */
    @Nonnull
    public static List<EligibilityEntityStubDTO> filterDuplicateEligibilityEntities(
            List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs) {
        List<EligibilityEntityStubDTO> filteredEligibilityStubDTOEntities
                = new ArrayList<EligibilityEntityStubDTO>();

        if(eligibilityEntityStubDTOs == null || eligibilityEntityStubDTOs.isEmpty()) {
            return filteredEligibilityStubDTOEntities;
        }

        Set<Long> groupsIds = new HashSet<Long>();
        Set<Long> paxIds = new HashSet<Long>();

        for(EligibilityEntityStubDTO entity : eligibilityEntityStubDTOs) {
            if(entity.getGroupId() != null && !groupsIds.contains(entity.getGroupId())) {
                filteredEligibilityStubDTOEntities.add(entity);
                groupsIds.add(entity.getGroupId());
            } else if(entity.getPaxId() != null && !paxIds.contains(entity.getPaxId())) {
                filteredEligibilityStubDTOEntities.add(entity);
                paxIds.add(entity.getPaxId());
            }  // basic handling of no group or pax id (stub entity should have an id at this point)
        }

        return filteredEligibilityStubDTOEntities;
    }
    
    /**
     * Filters out any duplicate 'Giving Members' within the specified list of giving members.
     *
     * @param budgetEntityStubDTOList list of giving members to filter
     * @return filtered giving members (no duplicates)
     */
    @Nonnull
    public static List<BudgetEntityStubDTO> filterDuplicateGivingMemberEntities(
            List<BudgetEntityStubDTO> budgetEntityStubDTOList) {
        List<BudgetEntityStubDTO> filteredGivingMemberEntities
                = new ArrayList<BudgetEntityStubDTO>();

        if(budgetEntityStubDTOList == null || budgetEntityStubDTOList.isEmpty()) {
            return filteredGivingMemberEntities;
        }

        Set<Long> groupsIds = new HashSet<Long>();
        Set<Long> paxIds = new HashSet<Long>();

        for(BudgetEntityStubDTO entity : budgetEntityStubDTOList) {
            if(entity.getGroupId() != null && !groupsIds.contains(entity.getGroupId())) {
                filteredGivingMemberEntities.add(entity);
                groupsIds.add(entity.getGroupId());
            } else if(entity.getPaxId() != null && !paxIds.contains(entity.getPaxId())) {
                filteredGivingMemberEntities.add(entity);
                paxIds.add(entity.getPaxId());
            }  // basic handling of no group or pax id (stub entity should have an id at this point)
        }

        return filteredGivingMemberEntities;
    }

    /**
     * Determines if the IDs specified within the given eligibility entities are exactly the same as the IDs
     * already associated with a Program.
     *
     * @param existingEntities eligibility entities already associated with a Program
     * @param entities eligibility entities to check
     * @return true if the eligibility entities are the same, false otherwise
     */
    public static boolean isEligibilityEntitiesSame(@Nonnull List<Acl> existingEntities, 
            List<EligibilityEntityStubDTO> entities) {
        List<Long> groupsIds = new ArrayList<Long>();
        List<Long> paxIds = new ArrayList<Long>();

        if(entities != null) {
            // separate the actual entities by what entity it represents (Groups or Pax)
            for(EligibilityEntityStubDTO entity : entities) {
                if(entity.getGroupId() != null) {
                    groupsIds.add(entity.getGroupId());
                } else if(entity.getPaxId() != null) {
                    paxIds.add(entity.getPaxId());
                } else {
                    // basic handling of no group or pax id (stub entity should have an id at this point)
                    return false;
                }
            }
        }

        // iterate through the existing entities and determine if they are 
        //specified within the tracked groupIds or paxIds
        for(Acl existingEntity : existingEntities) {
            String subjectType = existingEntity.getSubjectTable();
            Long entityId = existingEntity.getSubjectId();

            // check by type of entity (specified by subject type)
            switch(subjectType) {
                case GROUPS:
                    if(!groupsIds.contains(entityId)) {
                        return false;
                    }
                    groupsIds.remove(entityId);
                    break;
                case PAX:
                    if(!paxIds.contains(entityId)) {
                        return false;
                    }
                    paxIds.remove(entityId);
                    break;
                default:
                    // existing entity is not either a group or pax entity so just return false 
                    //(should not hit here at this point)
                    return false;
            }
        }

        // perform one last check if there were any new specified ids (not accounted for within existing)
        return groupsIds.isEmpty() && paxIds.isEmpty();
    }

    /**
     * Filters duplicate IDs from a list of IDs (that are numbers).
     * @param idList ID list
     * @return list of IDs that do not contain duplicates
     */
    @Nonnull
    public static <N extends Number> List<N> filterDuplicateIds(List<N> idList) {
        List<N> filteredIdList = new ArrayList<N>();

        if(idList == null || idList.isEmpty()) {
            return filteredIdList;
        }

        Set<N> ids = new HashSet<N>();

        // iterate through the ids and add to the filtered list if id hasn't already been accounted for
        for(N id : idList) {
            if(!ids.contains(id)) {
                filteredIdList.add(id);
                ids.add(id);
            } // else, don't add to filtered list (duplicate)
        }

        return filteredIdList;
    }

    /**
     * Compares two Lists to see if they contain the same list of objects
     * @param list1
     * @param list2
     * @return
     */
    public static boolean areListsSame( @Nonnull List<?> list1, @Nonnull List<?> list2 ) {
        // make a copy of the list so the original list is not changed, and remove() is supported
        ArrayList<?> cp = new ArrayList<>( list1 );
        for ( Object o : list2 ) {
            if ( !cp.remove(o) ) {
                return false;
            }
        }
        return cp.isEmpty();
    }

    /**
     * @see EntityUtil#validateEntityState(List, Map, String, List)
     *
     * Initiates entity state validation except with no regard to valid groups.
     */
    @Nonnull
    public static List<ErrorMessage> validateEntityState(@Nonnull List<Map<String, Object>> entities,
                                                         @Nonnull Map<String, String> errorInformation,
                                                         @Nonnull String field) {
        return validateEntityState(entities, errorInformation, field, new ArrayList<String>());
    }

    /**
     * Validates if the specified entities are valid for eligibility.
     *
     * @param entities entities to validate
     * @param errorInformation map of error information (codes, messages)
     * @param field field for error information
     * @param validGroupTypeNames valid group type names (if any)
     */
    @Nonnull
    public static List<ErrorMessage> validateEntityState(@Nonnull List<Map<String, Object>> entities,
                                                         @Nonnull Map<String, String> errorInformation,
                                                         @Nonnull String field,
                                                         @Nonnull List<String> validGroupTypeNames)
    {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        for(Map<String, Object> entity : entities) {
            // check for input type
            if(!entity.containsKey(INPUT_TYPE) || !entity.containsKey(STATUS) || !entity.containsKey(INPUT_ID)) {
                String errorMsg = "One or more retrieved eligibility entities is not "
                        + "a valid entity (missing entity information)!";
                logger.error(errorMsg);
                throw new RuntimeException(errorMsg);
            }

            String inputType = (String) entity.get(INPUT_TYPE);
            Long entityId = ConversionUtil.convertLongObjectMapValue(entity.get(INPUT_ID));

            // check if entity exists
            if(DOES_NOT_EXIST_STATUS.equals((String)entity.get(STATUS))) {
                errors.add(new ErrorMessage()
                        .setCode(errorInformation.get(ERROR_ENTITY_NOT_FOUND))
                        .setField(field)
                        .setMessage(errorInformation.get(ERROR_ENTITY_NOT_FOUND_MSG) + inputType + ", " + entityId));
                continue;
            }

            if(GROUPS.equals(entity.get(INPUT_TYPE))) {
                // validate Groups DTO
                GroupDTO groupDTO = new GroupDTO(entity);

                // check if group is active
                if(!StatusTypeCode.ACTIVE.name().equals(groupDTO.getStatus())) {
                    errors.add(new ErrorMessage()
                            .setCode(errorInformation.get(ERROR_GROUP_ENTITY_NOT_ACTIVE))
                            .setField(field)
                            .setMessage(errorInformation.get(ERROR_GROUP_ENTITY_NOT_ACTIVE_MSG) + entityId));
                }

                // check if group is a valid group type (if none specified, then no group type validation)
                if(!validGroupTypeNames.isEmpty() && !validGroupTypeNames.contains(groupDTO.getType())) {
                    errors.add(new ErrorMessage()
                            .setCode(errorInformation.get(ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE))
                            .setField(field)
                            .setMessage(errorInformation.get(ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE_MSG) + 
                                    entityId + ", " + groupDTO.getType()));
                }
            } else if (!PAX.equals(entity.get(INPUT_TYPE))) {
                String errorMsg = "One or more eligibility entities is not a valid entity (not a Group or Pax)!";
                logger.error(errorMsg);
                throw new RuntimeException(errorMsg);
            }
        }

        return errors;
    }

    public static String getErrorEntityNotFound() {
        return ERROR_ENTITY_NOT_FOUND;
    }

    public static String getErrorEntityNotFoundMsg() {
        return ERROR_ENTITY_NOT_FOUND_MSG;
    }

    public static String getErrorPaxEntityNotActive() {
        return ERROR_PAX_ENTITY_NOT_ACTIVE;
    }

    public static String getErrorPaxEntityNotActiveMsg() {
        return ERROR_PAX_ENTITY_NOT_ACTIVE_MSG;
    }

    public static String getErrorGroupEntityNotActive() {
        return ERROR_GROUP_ENTITY_NOT_ACTIVE;
    }

    public static String getErrorGroupEntityNotActiveMsg() {
        return ERROR_GROUP_ENTITY_NOT_ACTIVE_MSG;
    }

    public static String getErrorGroupEntityInvalidGroupType() {
        return ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE;
    }

    public static String getErrorGroupEntityInvalidGroupTypeMsg() {
        return ERROR_GROUP_ENTITY_INVALID_GROUP_TYPE_MSG;
    }
}
