package com.maritz.culturenext.util;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.security.Security;
import com.maritz.culturenext.permission.constants.PermissionConstants;

@Component
public class PermissionUtil {
    
    @Inject private Environment environment;
    @Inject private Security security;
    
    /**
     * Checks if the logged in user has permission to access the specified permission type.
     * The user can have ENABLE_{permissionType} or DISABLE_{permissionType} assigned to them via a group.
     * There is also a global default setting for the passed in {permissionType}.
     * If the user has the DISABLE permission, that will override anything.
     * If the user has the ENABLE permission, they should be allowed access to the calling method.
     * If the user does not have either permission, the global default will be used.
     * @param String permissionType - type of permission to be checked. Currently supported types are:
     *         COMMENTING, LIKING, ENGAGEMENT_SCORE, CALENDAR, NETWORK
     * @return boolean - if the user has permission to access the calling method.
     *         This method will be called via the @PreAuthorize annotation on our endpoints that need permissioned.
     */
    public boolean hasPermission(String permissionType) {
        if (PermissionConstants.VALID_AUDIENCE_PERMISSION_TYPES.contains(permissionType)) {
            if (security.hasPermission(PermissionConstants.DISABLE_PREFIX + permissionType)) {
                //User has the DISABLE permission. They should not be able to access the calling method
                return false;
            } else if (security.hasPermission(PermissionConstants.ENABLE_PREFIX + permissionType)) {
                //User has the ENABLE permission. They should be able to access the calling method
                return true;
            } else {
                //User does not have specific permissions. Use the global default
                return Boolean.valueOf(environment.getProperty(PermissionConstants.AUDIENCE_PERM_MAP.get(permissionType)));
            }
        } else {
            //Invalid permission type
            return false;
        }
    }

}
