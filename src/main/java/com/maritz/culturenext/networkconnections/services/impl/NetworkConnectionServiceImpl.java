package com.maritz.culturenext.networkconnections.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.networkconnections.constants.NetworkConnectionConstants;
import com.maritz.culturenext.networkconnections.services.NetworkConnectionService;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.util.EmployeeSortUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class NetworkConnectionServiceImpl implements NetworkConnectionService {

    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ParticipantInfoDao participantInfoDao;

    @Override
    public List<EmployeeDTO> createNetworkConnections(Long paxId, List<EmployeeDTO> networkConnections) {

        List<EmployeeDTO> connections = new ArrayList<>();

        if (networkConnections != null) {
            for (EmployeeDTO pax : networkConnections) {
                if (pax.getPaxId() == null) {
                    throw new ErrorMessageException(new ErrorMessage()
                            .setCode(NetworkConnectionConstants.ERROR_MISSING_PAX_ID)
                            .setField(ProjectConstants.PAX_ID)
                            .setMessage(NetworkConnectionConstants.ERROR_MISSING_PAX_ID_MSG));
                } else if (paxId.equals(pax.getPaxId())) {
                    throw new ErrorMessageException(new ErrorMessage()
                            .setCode(NetworkConnectionConstants.ERROR_SAME_PAX_RELATIONSHIP)
                            .setField(ProjectConstants.PAX_ID)
                            .setMessage(NetworkConnectionConstants.ERROR_SAME_PAX_RELATIONSHIP_MSG));
                }

                EmployeeDTO connection = participantInfoDao.getEmployeeDTO(pax.getPaxId());

                if (connection != null) {
                    connections.add(connection);
                } else {
                    throw new ErrorMessageException(new ErrorMessage()
                            .setCode(NetworkConnectionConstants.ERROR_INVALID_PAX_ID)
                            .setField(ProjectConstants.PAX_ID)
                            .setMessage(NetworkConnectionConstants.ERROR_INVALID_PAX_ID_MSG));
                }

                if (getSpecificRelationship(paxId, pax.getPaxId(), RelationshipType.FRIENDS.toString()) == null) {
                    Relationship relationship = new Relationship();
                    relationship.setPaxId1(paxId);
                    relationship.setPaxId2(connection.getPaxId());
                    relationship.setRelationshipTypeCode(RelationshipType.FRIENDS.toString());
                    relationship.setFromDate(new Date());
                    relationshipDao.save(relationship);
                }
            }
        }

        return connections;
    }

    @Override
    public boolean deleteNetworkConnection(Long paxId, Long targetPaxId) {

        Relationship relationship = getSpecificRelationship(paxId, targetPaxId, RelationshipType.FRIENDS.toString());
        if (relationship != null) {
            relationshipDao.delete(relationship, false);
        }
        return relationship != null;
    }

    @Override
    public List<EmployeeDTO> getAllNetworkConnections(Long paxId, Integer page, Integer size) {

        List<EmployeeDTO> networkConnections = new ArrayList<>();

        List<Relationship> relationships = relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_1).eq(paxId)
                .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.FRIENDS.toString())
                .find();

        Set<Long> connectionPaxIds = new HashSet<>();
        if (relationships != null) {
            for (Relationship relationship : relationships) {
                connectionPaxIds.add(relationship.getPaxId2());
            }
        }

        for (EntityDTO connectionPax : participantInfoDao.getInfo(new ArrayList<>(connectionPaxIds), null)) {
            networkConnections.add((EmployeeDTO)connectionPax);
        }

        // Sort network connections in ASC order
        Collections.sort(networkConnections, EmployeeSortUtil.firstAndLastNameComparator);

        return PaginationUtil.getPaginatedList(networkConnections, page, size);
    }

    @Override
    public Integer getNetworkConnectionCount(Long paxId) {
        List<EmployeeDTO> networkConnections = new ArrayList<>();

        List<Relationship> relationships = relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_1).eq(paxId)
                .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.FRIENDS.toString())
                .find();

        Set<Long> connectionPaxIds = new HashSet<>();
        if (relationships != null) {
            for (Relationship relationship : relationships) {
                connectionPaxIds.add(relationship.getPaxId2());
            }
        }

        for (EntityDTO connectionPax : participantInfoDao.getInfo(new ArrayList<>(connectionPaxIds), null)) {
            if (!StatusTypeCode.INACTIVE.name().equalsIgnoreCase(((EmployeeDTO)connectionPax).getStatus())) {
                networkConnections.add((EmployeeDTO)connectionPax);
            }
        }

        return networkConnections.size();
    }

    @Override
    public EmployeeDTO checkForNetworkConnection(Long paxId, Long targetPaxId) {

        EmployeeDTO employeeDTO = null;
        if (getSpecificRelationship(paxId, targetPaxId, RelationshipType.FRIENDS.toString()) != null) {
            employeeDTO = participantInfoDao.getEmployeeDTO(targetPaxId);
        }
        return employeeDTO;
    }

    /**
     * Gets specific relationship between the submitter pax id and target connection pax id
     * @param submitterPaxId - Pax id of submitter
     * @param connectionPaxId - Pax id of target connection
     * @param relationshipTypeCode - Relationship type to search for
     * @return Relationship
     */
    private Relationship getSpecificRelationship(Long submitterPaxId, Long connectionPaxId, 
            String relationshipTypeCode) {
        return relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_1).eq(submitterPaxId)
                .and(ProjectConstants.PAX_ID_2).eq(connectionPaxId)
                .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(relationshipTypeCode)
                .findOne();
    }

}
