package com.maritz.culturenext.networkconnections.services;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public interface NetworkConnectionService {
    
    /**
     * Adds the specified pax IDs to the user's network connections
     * 
     * @param paxId
     * @param networkConnections
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20578347/Create+Network+Connection+for+Participant
     */
    List<EmployeeDTO> createNetworkConnections(Long paxId, List<EmployeeDTO> networkConnections);

    /**
     * Removes a network connection
     * 
     * @param paxId
     * @param targetPaxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20578349/Delete+Network+Connection+for+Participant
     */
    boolean deleteNetworkConnection(Long paxId, Long targetPaxId);

    /**
     * Gets a list of all network connections for a user
     * 
     * @param paxId
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20578353/Get+all+Network+Connections+for+Participant
     */
    List<EmployeeDTO> getAllNetworkConnections(Long paxId, Integer page, Integer size);

    /**
     * Gets a count of how many network connections a user has
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20578355/Get+Number+of+Network+Connections+for+Participant
     */
    Integer getNetworkConnectionCount(Long paxId);

    /**
     * Checks to see if a network connection exists. Will return 404 if the network connection is not found
     * 
     * @param paxId- Pax ID of the logged in user
     * @param targetPaxId - Pax ID of the network connection to check for
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20578351/Get+Specific+Network+Connection+for+Participant
     */
    EmployeeDTO checkForNetworkConnection(Long paxId, Long targetPaxId);

}
