package com.maritz.culturenext.networkconnections.constants;

public class NetworkConnectionConstants {
    public static final String ERROR_MISSING_PAX_ID = "MISSING_PAX_ID";
    public static final String ERROR_MISSING_PAX_ID_MSG = "A pax id is required";
    public static final String ERROR_SAME_PAX_RELATIONSHIP = "ERROR_SAME_PAX_RELATIONSHIP";
    public static final String ERROR_SAME_PAX_RELATIONSHIP_MSG = "Submitter pax cannot be same as relationship pax";
    public static final String ERROR_INVALID_PAX_ID = "INVALID_PAX_ID";
    public static final String ERROR_INVALID_PAX_ID_MSG = "Invalid pax id";
}
