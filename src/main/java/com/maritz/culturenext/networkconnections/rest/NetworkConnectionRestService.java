package com.maritz.culturenext.networkconnections.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.networkconnections.services.NetworkConnectionService;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("participants")
@Api(value="/network-connections", description="Endpoints for network connections")
public class NetworkConnectionRestService {

    @Inject private NetworkConnectionService networkConnectionService;
    @Inject private Security security;

    //rest/participants/~/network-connections
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.NETWORK_TYPE + "')")
    @RequestMapping(value = "{paxId}/network-connections", method = RequestMethod.POST)
    @ApiOperation(value = "Creates network connection with user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created network connection", response = List.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @Permission("PUBLIC")
    public List<EmployeeDTO> createNetworkConnection(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @RequestBody List<EmployeeDTO> networkConnections) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return networkConnectionService.createNetworkConnections(paxId, networkConnections);
    }

    //rest/participants/~/network-connections/~
    @PreAuthorize("@security.isMyPax(#loggedInPaxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.NETWORK_TYPE + "')")
    @RequestMapping(value = "{loggedInPaxId}/network-connections/{paxId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes the network connection")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted network connection", response = EmployeeDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @Permission("PUBLIC")
    public void deleteNetworkConnection(
            @PathVariable(LOGGED_IN_PAX_ID_REST_PARAM) String loggedInPaxIdString,
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        Long loggedInPaxId = security.getPaxId(loggedInPaxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        boolean networkConnection = networkConnectionService.deleteNetworkConnection(loggedInPaxId, paxId);
        if (!networkConnection) {
            throw new NoHandlerFoundException(null, null, null);
        }
    }

    //rest/participants/~/network-connections
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.NETWORK_TYPE + "')")
    @RequestMapping(value = "{paxId}/network-connections", method = RequestMethod.GET)
    @ApiOperation(value = "Gets all network connections associated with user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved network connections", response = List.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @Permission("PUBLIC")
    public List<EmployeeDTO> getAllNetworkConnections(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            // TODO Pagination Techdebt: MP-7504 & MP-7522
            @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        return networkConnectionService.getAllNetworkConnections(paxId, page, size);
    }

    //rest/participants/~/network-connections/count
    @PreAuthorize("!@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.NETWORK_TYPE + "')")
    @RequestMapping(value = "{paxId}/network-connections/count", method = RequestMethod.GET)
    @ApiOperation(value = "Gets count of network connections associated with user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved count of network connections", 
                    response = Map.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @Permission("PUBLIC")
    public Map<String, Integer> getNetworkConnectionCount(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        Map<String, Integer> countObject = new HashMap<>();

        countObject.put(ProjectConstants.COUNT, networkConnectionService.getNetworkConnectionCount(paxId));

        return countObject;
    }

    //rest/participants/~/network-connections/~
    @PreAuthorize("@security.isMyPax(#loggedInPaxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.NETWORK_TYPE + "')")
    @RequestMapping(value = "{loggedInPaxId}/network-connections/{paxId}", method = RequestMethod.GET)
    @ApiOperation(value = "Checks if user is in logged in user's network")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created network connection", response = EmployeeDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found"),
            @ApiResponse(code = 403, message = "Forbidden")
    })
    @Permission("PUBLIC")
    public EmployeeDTO checkForNetworkConnection(
            @PathVariable(LOGGED_IN_PAX_ID_REST_PARAM) String loggedInPaxIdString,
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        Long loggedInPaxId = security.getPaxId(loggedInPaxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        EmployeeDTO networkConnection = networkConnectionService.checkForNetworkConnection(loggedInPaxId, paxId);
        if (networkConnection == null) {
            throw new NoHandlerFoundException(null, null, null);
        } else {
            return networkConnection;
        }
    }
}
