package com.maritz.culturenext.images.persister;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FilePersistenceDto;
import com.maritz.culturenext.images.validators.LegacyFileImageValidatorImpl;
import com.maritz.culturenext.util.ImageManipulationUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

@Component
public abstract class LegacyFileImageTypePersister implements FileImageTypePersister {

    @Inject
    private LegacyFileImageValidatorImpl legacyFileImageValidatorImpl;

    @Inject
    private FilesDao filesDao;

    @Override
    public List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {
        BufferedImage bufferedImage = legacyFileImageValidatorImpl.validateImageForUpload(image, type, targetId);
        bufferedImage = ImageManipulationUtil.resize(bufferedImage, getImageWidth(), getImageHeight());
        Files preExistingFile = filesDao.deleteImageRecords(type);
        FileTypes.valueOf(type);  // Although this looks as though it does nothing, it validates the type.
        return Arrays.asList(new FilePersistenceDto(filesDao.saveImage(bufferedImage,type, image.getOriginalFilename(),
                                                                            (preExistingFile != null) ? preExistingFile.getUrl() : null),
                                                        bufferedImage));
    }

    protected abstract int getImageHeight();

    protected abstract int getImageWidth();

}
