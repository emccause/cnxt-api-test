package com.maritz.culturenext.images.persister;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FilePersistenceDto;
import com.maritz.culturenext.images.validators.LegacyFileImageValidatorImpl;
import com.maritz.culturenext.util.ImageManipulationUtil;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

abstract class AbstractExactSizeImageTypePersisterImpl implements FileImageTypePersister {
    @Inject private LegacyFileImageValidatorImpl legacyFileImageValidatorImpl;
    @Inject private FilesDao filesDao;

    @Override
    public final List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {
        BufferedImage bufferedImage = legacyFileImageValidatorImpl.validateImageForUpload(image, type, targetId);
        if (bufferedImage.getHeight() < requiredHeight() ||
                bufferedImage.getWidth() < requiredWidth()) {
            throw new ErrorMessageException().addErrorMessages(Arrays.asList(ImageConstants.IMAGE_TOO_SMALL_MESSAGE));
        }

        // Resize, if necessary
        if (bufferedImage.getHeight() != ImageConstants.PROGRAM_IMAGE_DETAILED_HEIGHT ||
                bufferedImage.getWidth() != ImageConstants.PROGRAM_IMAGE_DETAILED_WIDTH) {
            bufferedImage = ImageManipulationUtil.squareCrop(bufferedImage, requiredWidth(), requiredHeight());
        }

        return Arrays.asList(new FilePersistenceDto(filesDao.saveImage(bufferedImage, type, image.getOriginalFilename()), bufferedImage));
    }

    protected abstract int requiredWidth();
    protected abstract int requiredHeight();
}
