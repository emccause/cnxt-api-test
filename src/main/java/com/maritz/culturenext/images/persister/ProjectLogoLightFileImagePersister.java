package com.maritz.culturenext.images.persister;

import com.maritz.culturenext.theme.constants.ThemeConstants;
import org.springframework.stereotype.Component;

@Component
public class ProjectLogoLightFileImagePersister extends LegacyFileImageTypePersister{
    @Override
    protected int getImageHeight() {
        return ThemeConstants.LOGO_LIGHT_HEIGHT;
    }

    @Override
    protected int getImageWidth() {
        return ThemeConstants.LOGO_LIGHT_WIDTH;
    }
}
