package com.maritz.culturenext.images.persister;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import com.maritz.culturenext.images.dto.FilePersistenceDto;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Ecard;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StreamUtils;
import com.maritz.core.util.stream.JPEGStreamingOutput;
import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileItemTypeEnum;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FileImagesDTO;
import com.maritz.culturenext.images.validators.GenericFileImageValidatorImpl;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.util.ImageManipulationUtil;

@Component
public class GenericFileImageTypePersisterImpl implements FileImageTypePersister {

    @Inject private ConcentrixDao<Ecard> ecardDao;
    @Inject private GenericFileImageValidatorImpl genericFileImageValidator;
    @Inject private FilesDao filesDaoImpl;
    @Inject private ConcentrixDao<Files> filesDao;

    @Override
    public List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {
        return saveImageByType(image, type, targetId, new ArrayList<>());
    }

    private List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId, List<FilePersistenceDto> filePersistenceDtos) throws Exception {
        BufferedImage bufferedImage = genericFileImageValidator.validateImageForUpload(image, type, targetId);

        FileImageTypes fileImageType = FileImageTypes.valueOf(type);
        String ecardName = image.getOriginalFilename();

        if(targetId == null && fileImageType.getTargetTable() != null
            && FileItemTypeEnum.ECARD.name().equals(fileImageType.getTargetTable().name())
        ){
            Ecard ecard = new Ecard();

            ecardName = ecardName + "-" + new Date().getTime();
                // check if the ecard info has already been entered - shouldn't happen but just to be safe
                Ecard existingEcard = ecardDao.findBy()
                    .where(ProjectConstants.ECARD_NAME).eq(ecardName)
                    .findOne();
                if(existingEcard != null) {
                    throw new ErrorMessageException(ProgramActivityConstants.DUPLICATE_ECARD_NAME_MESSAGE);
                } else {
                    ecard.setEcardName(image.getOriginalFilename());
                }
            ecard.setEcardTypeCode(FileImageConstants.ECARD_CUSTOM_TYPE);
            ecard.setImageUrl(""); //eCards are saved in FILES table
            ecard.setThumbnailUrl(""); //eCards Thumbnails are saved in FILES table
            ecard.setStatusTypeCode(StatusTypeCode.ACTIVE.name());

            ecardDao.save(ecard);

            targetId = ecard.getId();
        }

        String mediaType = image.getContentType();
        //  what if fileImageType.getImgHeight is null? or  fileImageType.getImgWidth() ?? then fileImageType height and Width were set incorrectly!

        if (bufferedImage != null && fileImageType.getImgHeight() != null && fileImageType.getImgWidth() != null &&
                (bufferedImage.getHeight() > fileImageType.getImgHeight() || bufferedImage.getWidth() > fileImageType.getImgWidth())) {
            mediaType = MediaType.IMAGE_PNG_VALUE;  // QAF server JDK release doesn't have support for JPEG
            bufferedImage = editImage(bufferedImage, fileImageType);
        }

        // Get existing FILES and FILE_ITEM entries.
        // if there's already a file, remove it and replace it unless it is part of a catalog
        FileImagesDTO preExistingFileDto =  filesDaoImpl.exist(fileImageType,targetId);
        filesDaoImpl.delete(preExistingFileDto);

        // Create new FILES and FILE_ITEM (only if it's required) entries.
        Files fileImage = new Files()
            .setFileTypeCode(fileImageType.getType().name())
            .setMediaType(mediaType)
            .setName(image.getOriginalFilename())
            // This will preserve the URL reference to the CDN object, if one exists.
            .setUrl((preExistingFileDto.getFile() != null) ? preExistingFileDto.getFile().getUrl() : null);
        filesDaoImpl.save(fileImage);
        
        StreamingOutput streamingOutput;
        if (MediaTypesEnum.IMAGE_JPEG.value().equalsIgnoreCase(mediaType) || MediaTypesEnum.IMAGE_JPG.value().equalsIgnoreCase(mediaType)) {
            streamingOutput = new JPEGStreamingOutput(bufferedImage);
        } else {
            streamingOutput = new PNGStreamingOutput(bufferedImage);
        }

        StreamUtils.fromStreamingOutput(filesDao.findBy().where("id").eq(fileImage.getId()), "data", streamingOutput);

        if (fileImageType.getTargetTable() != null && !fileImageType.isCatalog()) {
            FileItem fileItem = new FileItem();
            fileItem.setFilesId(fileImage.getId());
            fileItem.setTargetId(targetId);
            fileItem.setTargetTable(fileImageType.getTargetTable().name());
            filesDaoImpl.saveFileItem(fileItem);
        }

        // Creating sub file images.
        if (!CollectionUtils.isEmpty(fileImageType.getSubFileTypeList())) {
            for (FileImageTypes fileTypeImage: fileImageType.getSubFileTypeList()) {
                saveImageByType(image, fileTypeImage.name(), targetId, filePersistenceDtos);
            }
        }

        filePersistenceDtos.add(0, new FilePersistenceDto(fileImage.getId(), streamingOutput));

        return filePersistenceDtos;
    }
    /**
     * Edit image according to file type parameters.
     * @param bufferedImage
     * @param fileImageType
     * @return
     */
    public BufferedImage editImage(BufferedImage bufferedImage, FileImageTypes fileImageType) {
        BufferedImage editedImage;

    switch (fileImageType.getImageManipulationType()) {

            case CENTER_CROP:
                int x = bufferedImage.getWidth() / 2 - fileImageType.getImgWidth()/ 2;
                int y = bufferedImage.getHeight() / 2 - fileImageType.getImgHeight() / 2;
                editedImage = ImageManipulationUtil.crop(bufferedImage, x, y, fileImageType.getImgWidth(),
                        fileImageType.getImgHeight());
                break;

            case SQUARE_CROP:
                editedImage = ImageManipulationUtil.squareCrop(bufferedImage, fileImageType.getImgWidth(), fileImageType.getImgHeight());
                break;

            case RESIZE:
                editedImage = ImageManipulationUtil.resize(bufferedImage, fileImageType.getImgWidth(), fileImageType.getImgHeight());
                break;

            case SCALED_DOWN_1:
                // scale down image to 1%
                editedImage = ImageManipulationUtil.resize(bufferedImage
                        , bufferedImage.getHeight()/100, bufferedImage.getHeight()/100);
                break;

            case CIRCLE_CROP:
                editedImage = ImageManipulationUtil.circleCrop(bufferedImage);
                break;

            case PROFILE:
                // auto-crop, roll out!
                int height = bufferedImage.getHeight();
                int width = bufferedImage.getWidth();
                int finalHeight = height;
                int finalWidth = width;
                int prfX = 0;
                int prfY = 0;

                if(height > width){
                    finalHeight = width;
                    prfY = (height - finalHeight) / 2;
                }
                else{
                    finalWidth = height;
                    prfX = (width - finalWidth) / 2;
                }

                editedImage = ImageManipulationUtil.crop(bufferedImage, prfX, prfY, finalWidth, finalHeight);
                editedImage = ImageManipulationUtil.resize(editedImage, fileImageType.getImgWidth(), fileImageType.getImgHeight());
                editedImage = ImageManipulationUtil.circleCrop(editedImage);
                break;

        default:
            editedImage = bufferedImage;
            break;
        }
        return editedImage;
    }

}
