package com.maritz.culturenext.images.persister;

import com.maritz.culturenext.theme.constants.ThemeConstants;
import org.springframework.stereotype.Component;

@Component
public class ProjectBackgroundFileImageTypePersister  extends LegacyFileImageTypePersister{

    @Override
    protected int getImageHeight() {
        return ThemeConstants.BACKGROUND_HEIGHT;
    }

    @Override
    protected int getImageWidth() {
        return ThemeConstants.BACKGROUND_WIDTH;
    }
}
