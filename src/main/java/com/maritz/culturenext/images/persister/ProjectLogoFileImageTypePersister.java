package com.maritz.culturenext.images.persister;

import com.maritz.culturenext.theme.constants.ThemeConstants;
import org.springframework.stereotype.Component;

@Component
public class ProjectLogoFileImageTypePersister extends LegacyFileImageTypePersister {

    @Override
    protected int getImageHeight() {
        return ThemeConstants.LOGO_HEIGHT;
    }

    @Override
    protected int getImageWidth() {
        return ThemeConstants.LOGO_WIDTH;
    }
}
