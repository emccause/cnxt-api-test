package com.maritz.culturenext.images.persister;

import java.util.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.maritz.culturenext.images.service.FileImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;

/**
 * This factory returns the persitent algorithm according to the File Image Type. 
 *
 */
@Component
public class FileImageTypePersisterFactory {
    private  final static Logger logger = LoggerFactory.getLogger(FileImageTypePersisterFactory.class);

    @Inject
    private  GenericFileImageTypePersisterImpl genericFileImagePersister;
    @Inject
    private ProgramImageFileImageTypePersisterImpl programImageFileImageTypePersister;
    @Inject
    private CarouselImageFileImageTypePersisterImpl carouselImageFileImageTypePersister;
    @Inject
    private ProgramImageSmallFileImageTypePersisterImpl programImageSmallFileImageTypePersister;
    @Inject
    private ProgramImageDetailedFileImageTypePersisterImpl programImageDetailedFileImageTypePersister;
    @Inject
    private EcardImageTypePersisterImpl ecardImageTypePersister;
    @Inject
    private ProjectLogoFileImageTypePersister projectLogoFileImageTypePersister;
    @Inject
    private ProjectBackgroundFileImageTypePersister projectBackgroundFileImagePersister;
    @Inject
    private ProjectLogoLightFileImagePersister projectLogoLightFileImagePersister;

    private  Map<FileImageTypes, FileImageTypePersister> persisters;

    @PostConstruct
    private void postConstruct(){
        if (persisters == null || persisters.size() ==0){
            persisters = new HashMap<FileImageTypes, FileImageTypePersister>();
            persisters.put(FileImageTypes.PRFD, genericFileImagePersister);
            persisters.put(FileImageTypes.HERO_IMAGE, genericFileImagePersister);
            persisters.put(FileImageTypes.PROGRAM_IMAGE, programImageFileImageTypePersister);
            persisters.put(FileImageTypes.PROGRAM_IMAGE_SMALL, programImageFileImageTypePersister);
            persisters.put(FileImageTypes.PROGRAM_IMAGE_DETAILED, programImageDetailedFileImageTypePersister);
            persisters.put(FileImageTypes.CAROUSEL, carouselImageFileImageTypePersister);
            persisters.put(FileImageTypes.ECARD_DEFAULT, ecardImageTypePersister);
            persisters.put(FileImageTypes.PROJECT_LOGO, projectLogoFileImageTypePersister);
            persisters.put(FileImageTypes.PROJECT_BACKGROUND, projectBackgroundFileImagePersister);
            persisters.put(FileImageTypes.PROJECT_LOGO_LIGHT, projectLogoLightFileImagePersister);
        }
    }
    /**
     * If the file Type Image has not been implemented, it will throw an exception.
     * 
     * @param fileTypeImage name of the file Type Image 
     * @return the implementor of the persisting algorithm
     */
    public FileImageTypePersister getFileImagePersister(String fileTypeImage){
        FileImageTypePersister result = null;

        logger.info("fileTypeImage:"+fileTypeImage);

        FileImageTypes eFileTypeImage = null;
        try {
            eFileTypeImage = FileImageTypes.valueOf(fileTypeImage);
        }catch (NullPointerException | IllegalArgumentException iae){
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.UNHANDLED_FILE_IMAGE_TYPE)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_UNHANDLED_FILE_IMAGE_TYPE)));
        }
        if (eFileTypeImage != null && persisters.containsKey(eFileTypeImage)){
            result = persisters.get(eFileTypeImage);
        } else {
            logger.info("File image type not implemented yet: "+fileTypeImage);
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.UNHANDLED_FILE_IMAGE_TYPE)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_UNHANDLED_FILE_IMAGE_TYPE)));
        }
        return result;
    }
}
