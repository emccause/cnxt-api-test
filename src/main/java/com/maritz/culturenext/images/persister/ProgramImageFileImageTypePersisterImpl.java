package com.maritz.culturenext.images.persister;

import org.springframework.stereotype.Component;
import com.maritz.culturenext.images.constants.ImageConstants;

@Component
public class ProgramImageFileImageTypePersisterImpl extends AbstractExactSizeImageTypePersisterImpl {
    @Override
    protected int requiredWidth() {
        return ImageConstants.PROGRAM_IMAGE_WIDTH;
    }

    @Override
    protected int requiredHeight() {
        return ImageConstants.PROGRAM_IMAGE_HEIGHT;
    }
}
