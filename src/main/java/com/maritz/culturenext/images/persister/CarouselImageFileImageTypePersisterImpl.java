package com.maritz.culturenext.images.persister;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.maritz.culturenext.images.dto.FilePersistenceDto;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.validators.LegacyFileImageValidatorImpl;
import com.maritz.culturenext.util.ImageManipulationUtil;

@Component
public class CarouselImageFileImageTypePersisterImpl implements FileImageTypePersister {

    @Inject private LegacyFileImageValidatorImpl legacyFileImageValidatorImpl;
    @Inject private FilesDao filesDao;

    @Override
    public List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {        String fileName = image.getOriginalFilename();

        //convert the uploaded file into a buffered image
        BufferedImage bufferedImage = legacyFileImageValidatorImpl.validateImageForUpload(image, type, targetId);

        //make sure it isn't too small
        if (bufferedImage.getHeight() < ImageConstants.CAROUSEL_HEIGHT || bufferedImage.getWidth() < ImageConstants.CAROUSEL_WIDTH) {
            throw new ErrorMessageException(ImageConstants.IMAGE_TOO_SMALL_MESSAGE);
        }

        //crop it if it is too large
        if (bufferedImage.getHeight() > ImageConstants.CAROUSEL_HEIGHT || bufferedImage.getWidth() > ImageConstants.CAROUSEL_WIDTH) {

            int x = bufferedImage.getWidth() / 2 - ImageConstants.CAROUSEL_WIDTH / 2;
            int y = bufferedImage.getHeight() / 2 - ImageConstants.CAROUSEL_HEIGHT / 2;
            bufferedImage = ImageManipulationUtil.crop(bufferedImage, x, y, ImageConstants.CAROUSEL_WIDTH, ImageConstants.CAROUSEL_HEIGHT);
        }

        //save it
        return Arrays.asList(new FilePersistenceDto(filesDao.saveImage(bufferedImage, type, fileName), bufferedImage));
    }

}
