package com.maritz.culturenext.images.persister;

import com.maritz.culturenext.images.dto.FilePersistenceDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Contract that any file Image type class that persists to the database should comply with. Any implementing class should also call a validator. 
 *
 */
public interface FileImageTypePersister {
    List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception ;
}
