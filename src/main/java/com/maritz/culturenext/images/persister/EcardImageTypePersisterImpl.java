package com.maritz.culturenext.images.persister;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.util.StreamUtils;
import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dto.FilePersistenceDto;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.images.validators.EcardFileImageValidator;
import com.maritz.culturenext.program.ecard.dao.EcardDao;
import com.maritz.culturenext.program.ecard.services.impl.EcardServiceImpl;
import com.maritz.culturenext.util.ImageManipulationUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class EcardImageTypePersisterImpl implements FileImageTypePersister {

    @Inject
    private EcardFileImageValidator ecardFileImageValidator;

    @Inject
    private EcardDao ecardDao;

    @Inject
    private FileImageService fileImageService;

    @Inject
    private ConcentrixDao<Files> filesDao;

    @Inject
    private FilesRepository filesRepository;

    /**
     * for Ecards we always store two ecard's versions: a normal size and a thumbnail.
     * @param image
     * @param type
     * @param targetId
     * @return
     * @throws Exception
     */
    @Override
    public List<FilePersistenceDto> saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {
        BufferedImage bufferedImage = ecardFileImageValidator.validateImageForUpload(image, type, null);

        bufferedImage = ImageManipulationUtil.resize(bufferedImage, EcardServiceImpl.ECARD_DEFAULT_WIDTH,
                EcardServiceImpl.ECARD_DEFAULT_HEIGHT);
        FilePersistenceDto eCardDefault = saveImage(bufferedImage, FileImageTypes.ECARD_DEFAULT.name(), targetId);

        bufferedImage = ImageManipulationUtil.resize(bufferedImage, EcardServiceImpl.ECARD_THUMBNAIL_WIDTH,
                EcardServiceImpl.ECARD_THUMBNAIL_HEIGHT);
        FilePersistenceDto eCardThumbnail = saveImage(bufferedImage, FileImageTypes.ECARD_THUMBNAIL.name(), targetId);

        return Arrays.asList(eCardDefault, eCardThumbnail);
    }

    private FilePersistenceDto saveImage(BufferedImage bufferedImage, String type, Long ecardId) throws IOException {
        Files file;
        List<Files> filesEntry = filesRepository.findByEcardIdFileTypeCode(ecardId, type);
        if(filesEntry == null || filesEntry.isEmpty()){
            file = new Files();
            file.setFileTypeCode(type);
        } else{
            file = filesEntry.get(0);
        }

        file.setMediaType("image/png");
        filesDao.save(file);

        StreamingOutput streamingOutput = new PNGStreamingOutput(bufferedImage);
        StreamUtils.fromStreamingOutput(file,  "data", streamingOutput);

        fileImageService.saveFileItemsEntry(file.getId(), TableName.ECARD.name(), ecardId);

        return new FilePersistenceDto(file.getId(), streamingOutput);
    }

}
