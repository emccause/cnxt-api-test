package com.maritz.culturenext.images.service;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.images.constants.FileImageTypes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileImageService {

    /**
     * Get files image type
     * @param type (FILE_TYPE_CODE)
     * @param filesId - get image by files id
     * @param targetId - FILE_ITEM target id value (optional)
     * @return image file
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/23167042/Get+Image+by+Id
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/109117452/Get+Image+by+Type+or+TargetId
     */
    ResponseEntity<Object> getImageFile(String type, Long filesId, Long targetId);

    Files searchForImage(FileImageTypes projectBackground);
    
    /**
     * The generic image upload method
     *
     * @param image The image passed in to be stored
     * @param type  The type of the image that dictates cropping size and storage
     * @param targetId - FILE_ITEM target id value (optional)
     * @return The image id in ImageDTO form
     * @throws Exception In case processing of the image does not work
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/23167044/Create+Image
     */
    IdDTO uploadImage( MultipartFile image, String type, Long targetId) throws Exception;

    /**
     * uploadImage calls this method and wraps the answer in IdDTO. This is for used without a REST call.
     * @param image
     * @param type
     * @param targetId
     * @return
     * @throws Exception
     */
    public Long saveImageByType(MultipartFile image, String type, Long targetId) throws Exception;

    /**
     * Create or remove FILE_ITEM entry according to image type.
     * @param filesId
     * @param targetTable
     * @param targetId
     */
    void saveFileItemsEntry(Long filesId, String targetTable , Long targetId);
    
    /**
     * Get File entry by target id.
     * @param targetTable
     * @param targetId
     * @return
     */
    List<Files> getFilesByTargetId(String targetTable , Long targetId);
    
    /**
     * Removes the file associated with imagedId
     * @param imageId id of the image to be deleted
     */
     void deleteImage(Long imageId);
     /**
      * removes the file associated with the type. 
      * <b>only applicable to HERO_IMAGE type </b>
      * @param type
      * 
      * https://maritz.atlassian.net/wiki/spaces/M365/pages/108167184/Delete+Image+by+Type
      */
    void deleteImageByType(String type);

    ResponseEntity<Object> prepareImageForResponse(Long groupId, Files groupHeroImage);

    ResponseEntity<Object> prepareImageForResponse(Files file);

    /**
     * this is a special case where Icon does not have the http header for file image name.
     * @param file
     * @return
     */
    ResponseEntity<Object> prepareIconForResponse(Files file);

    /**
     * this special case is for Profile Image where the http header uses profileImage instead of just id.
     * @param paxId
     * @param imageEntry
     * @return
     */
    ResponseEntity<Object> prepareProfileImageForResponse(Long paxId, Files imageEntry);

    ResponseEntity<Object> prepareProfileImageForResponse(String ecardName, Files imageEntry);

    ResponseEntity<Object> prepareLogoForResponse(Files imageEntry);

    ResponseEntity<Object> prepareLogoLightForResponse(Files imageEntry);

    ResponseEntity<Object> prepareBackgroundForResponse(Files imageEntry);
}
