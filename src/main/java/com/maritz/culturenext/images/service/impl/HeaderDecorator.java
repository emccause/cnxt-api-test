package com.maritz.culturenext.images.service.impl;

import org.springframework.http.HttpHeaders;

/**
 * functional interface for decorating the header depending on the method using it.
 */
public interface HeaderDecorator {
    void decorate(HttpHeaders headers);
}
