package com.maritz.culturenext.images.service.impl;

import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FileImagesDTO;
import com.maritz.culturenext.images.eraser.FileImageTypeEraser;
import com.maritz.culturenext.images.eraser.FileImageTypeEraserFactory;
import com.maritz.culturenext.images.persister.FileImageTypePersisterFactory;
import com.maritz.culturenext.images.dto.FilePersistenceDto;
import com.maritz.culturenext.images.retriever.FileImageResponseBuilderFactory;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileImageServiceImpl implements FileImageService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private FilesDao filesDao;
    @Inject private FileImageTypeEraserFactory fileImageTypeEraserFactory;
    @Inject private FileImageTypePersisterFactory fileImageTypePersisterFactory;
    @Inject private FileImageResponseBuilderFactory fileImageResponseBuilderFactory;
    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;


    @Inject
    public void setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
    }

    @Inject
    public void setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
    }

    @Override
    public IdDTO uploadImage( MultipartFile image, String type, Long targetId) throws Exception {
        IdDTO result = new IdDTO(saveImageByType(image, FileImageTypes.valueOf(type).name(), targetId));
        if (logger.isDebugEnabled()){
            logger.debug("uploadImage:"+result.toString());
        }
        return result;
    }

    @Override
    public ResponseEntity<Object> getImageFile(String type, Long filesId, Long targetId) {
        Files imageEntry = null;
        if (filesId != null) {
            imageEntry = filesDao.findById(filesId);
        }
        else if (!FileImageTypes.containsImageFileType(type)) {
            throw new ErrorMessageException(FileImageConstants.INVALID_FILE_IMAGE_TYPE_MESSAGE);
        }
        else if (FileImageTypes.valueOf(type) != null) {
            FileImageTypes fileType = FileImageTypes.valueOf(type);

            if (targetId != null && fileType.getTargetTable() != null) {
                imageEntry = filesDao.findOneByFileImageTypeAndTargetId (fileType, targetId);
            }
            else {
                imageEntry = searchForImage(fileType);
            }
        }

        return fileImageResponseBuilderFactory.prepareImageForResponse(imageEntry);
    }

    @Override
    public Files searchForImage(FileImageTypes fileType) {
        return filesDao.findOneByTypeName(fileType);
    }

    public ResponseEntity<Object> prepareImageForResponse(Files imageEntry ){
        return prepareImageForResponse(imageEntry.getId() ,imageEntry);
    }

    @Override
    public ResponseEntity<Object> prepareImageForResponse(Long id, Files imageEntry) {

        return fileImageResponseBuilderFactory.prepareImageForResponse(id, imageEntry);
    }

    @Override
    public ResponseEntity<Object> prepareProfileImageForResponse(Long paxId, Files imageEntry) {
        return fileImageResponseBuilderFactory.prepareProfileImageForResponse(paxId, imageEntry);
    }
    @Override
    public ResponseEntity<Object> prepareProfileImageForResponse(String ecardName, Files imageEntry) {
        return fileImageResponseBuilderFactory.prepareProfileImageForResponse(ecardName, imageEntry);
    }

    @Override
    public ResponseEntity<Object> prepareIconForResponse(Files imageEntry) {
        return fileImageResponseBuilderFactory.prepareGenericImageForResponse(imageEntry,null);
    }

    @Override
    public ResponseEntity<Object> prepareLogoForResponse(Files imageEntry){
        return fileImageResponseBuilderFactory.prepareLogoImageForResponse(imageEntry);
    }

    @Override
    public ResponseEntity<Object> prepareLogoLightForResponse(Files imageEntry) {
        return fileImageResponseBuilderFactory.prepareLogoLightImageForResponse(imageEntry);
    }

    @Override
    public ResponseEntity<Object> prepareBackgroundForResponse(Files imageEntry) {
        return fileImageResponseBuilderFactory.prepareBackgroundImageForResponse(imageEntry);
    }

    @Override
    public void deleteImage(Long imageId){
        FileImagesDTO fileImagesTO =  filesDao.exist(null, imageId);
        filesDao.delete(fileImagesTO);
    }

    @Override
    public void deleteImageByType(String type) {
        if (logger.isDebugEnabled()){
            logger.debug("deleteImageByType: type ="+type);
        }
        FileImageTypeEraser fileImageType = fileImageTypeEraserFactory.getFileImageType(type);
        if (fileImageType != null){
            fileImageType.deleteByType();
        }  else {
            logger.info("File image type not found.");
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.FILE_IMAGE_TYPE_NOT_FOUND)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_FILE_IMAGE_TYPE_DOES_NOT_EXIST)));
        }
    }



    /**
     * This method checks the image type and crops the image based on the type
     * @param image
     * @param type
     * @param targetId
     * @return files id Id used when persisted to data storage
     * @throws Exception
     */
    public Long saveImageByType(MultipartFile image, String type, Long targetId) throws Exception {
        List<FilePersistenceDto> filePersistenceDtos = fileImageTypePersisterFactory.getFileImagePersister(type).saveImageByType(image, type, targetId);
        if (environmentUtil.writeToCdn()) {
            for (FilePersistenceDto filePersistenceDto : filePersistenceDtos) {
                gcpUtil.uploadFile(filePersistenceDto.getId(), filePersistenceDto.getData());
            }
        }
        return filePersistenceDtos.get(0).getId();
    }

    @Override
    public void saveFileItemsEntry(Long filesId, String targetTable , Long targetId) {
        FileItem fileItem = null;
        if (targetTable != null && targetId!= null) {

            if (filesId != null) {
                fileItem = new FileItem();
                fileItem.setFilesId(filesId);
                fileItem.setTargetId(targetId);
                fileItem.setTargetTable(targetTable);
                filesDao.saveFileItem(fileItem);
            }
            else {
                filesDao.deleteFilesItemInTargetTableWithTargetId(targetTable, targetId);
            }
        }
    }

    @Override
    public List<Files> getFilesByTargetId(String targetTable, Long targetId) {

        List<Files> filesList = new ArrayList<>();
        if (targetTable != null && targetId != null) {
            filesList = filesDao.findByTargetTableAndTargetId(targetTable, targetId);
        }

        return filesList;
    }
}
