package com.maritz.culturenext.images.dao;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dto.FileImagesDTO;

/**
 * Data Access object used for interacting with Files and FileItem tables. 
 *
 */
public interface FilesDao {

    Files findById(Long filesId);

    Files findOneByTypeName(FileImageTypes fileType);

    Files findOneByFileImageTypeAndTargetId(FileImageTypes fileImageType, Long targetId);

    FileItem findOneFileItemByFileImageTypeAndTargetIdAndFile(FileImageTypes fileImageType, Long targetId,
            Files file);

    List<Files> findByTargetTableAndTargetId(String targetTable, Long targetId);

    FileImagesDTO  exist(FileImageTypes fileImageType, Long targetId);
    
    FileImagesDTO existFileImageType(FileImageTypes fileImageType);
    
    FileImagesDTO existTargetId(Long targetId);


    void deleteFileItem(FileItem fileItem);

    /**
     *  Deletes an image file at FILE_ITEM and FILES if it is not part of a catalog (JUMBOTRON, AWARD_CODE_ICON, AWARD_CODE_CERT).
     * @param fileImagesTO elements to be deleted
     */
    void delete(FileImagesDTO fileImagesTO);

    /**
     * saves the transfer object fileImage into Files Table
     * 
     * @param fileImage
     */
    void save(Files fileImage);
    
    /**
     * saves the image file to the repository.
     * 
     * @param bufferedImage The image to be saved to the db
     * @param type          The type of the image to be stored in the db
     * @param fileName      The file name to be stored in the db
     * @return The Image id to be passed back up to the reqeust
     * @throws IOException In case the processing of the image does not complete
     */
    Long saveImage(BufferedImage bufferedImage, String type, String fileName) throws IOException ;

    /**
     * saves the image file to the repository.
     *
     * @param bufferedImage The image to be saved to the db
     * @param type          The type of the image to be stored in the db
     * @param fileName      The file name to be stored in the db
     * @param url
     * @return The Image id to be passed back up to the reqeust
     * @throws IOException In case the processing of the image does not complete
     */
    Long saveImage(BufferedImage bufferedImage, String type, String fileName, String url) throws IOException ;

    /**
     * saves to FileItem table
     * @param fileItem transport object of the information to be saved.
     */
    void saveFileItem(FileItem fileItem);

    /**
     * deletes a FileItem that target table 
     * @param targetTable
     * @param targetId
     */
    void deleteFilesItemInTargetTableWithTargetId(String targetTable, Long targetId);

    /**
     * Deletes all image records on Files table depending the type param
     * @param type
     */
    Files deleteImageRecords(String type);

}
