
package com.maritz.culturenext.images.dao.impl;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FileItemRepository;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.util.StreamUtils;
import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FileImagesDTO;

@Repository
public class FilesDaoImpl  implements FilesDao {
    final Logger logger = LoggerFactory.getLogger(getClass());

    protected FilesRepository filesRepository;
    protected FileItemRepository fileItemRepository;
    protected ConcentrixDao<Files> filesDao;

    @Inject
    public FilesDaoImpl setFilesRepository(FilesRepository filesRepository) {
        this.filesRepository = filesRepository;
        return this;
    }

    @Inject
    public FilesDaoImpl setFileItemRepository(FileItemRepository fileItemRepository) {
        this.fileItemRepository = fileItemRepository;
        return this;
    }

    @Inject
    public FilesDaoImpl setFilesDao(ConcentrixDao<Files> filesDao) {
        this.filesDao = filesDao;
        return this;
    }

    @Override
    public Files findById(Long filesId) {
        return filesRepository.findOne(filesId);
    }

    @Override
    public Files findOneByFileImageTypeAndTargetId(FileImageTypes fileImageType, Long targetId) {
        List<Long> fileItemIdList = fileItemRepository.findBy()
                .where(ProjectConstants.TARGET_TABLE).eq(fileImageType.getTargetTable().name())
                .and(ProjectConstants.TARGET_ID).eq(targetId)
                .findAll(ProjectConstants.FILES_ID, Long.class);
        Files file = null;
        if (!CollectionUtils.isEmpty(fileItemIdList)) {
            file = filesRepository.findBy()
                    .where(ProjectConstants.FILE_TYPE_CODE).eq(fileImageType.name())
                    .and(ProjectConstants.ID).in(fileItemIdList)
                    .findOne();
        }
        return file;
    }

    @Override
    public Files findOneByTypeName(FileImageTypes fileType) {
        return filesRepository.findBy()
            .where(ProjectConstants.FILE_TYPE_CODE).eq(fileType.name())
            .findOne()
        ;
    }

    /*
     * might be moved to its own DAO?
     */
    @Override
    public FileItem findOneFileItemByFileImageTypeAndTargetIdAndFile(FileImageTypes fileImageType, Long targetId, Files file) {
        return fileItemRepository.findBy()
            .where(ProjectConstants.TARGET_TABLE).eq(fileImageType.getTargetTable().name())
            .and(ProjectConstants.TARGET_ID).eq(targetId)
            .and(ProjectConstants.FILES_ID).eq(file.getId())
            .findOne()
        ;
    }

    @Override
    public List<Files> findByTargetTableAndTargetId(String targetTable, Long targetId) {
        return filesRepository.findBy()
            .where(ProjectConstants.ID).in(
                fileItemRepository.findBy()
                    .where(ProjectConstants.TARGET_TABLE).eq(targetTable)
                    .and(ProjectConstants.TARGET_ID).eq(targetId)
                    .findAll(ProjectConstants.FILES_ID, Long.class)
            )
            .findAll()
        ;
    }

    @Override
    public FileImagesDTO exist(FileImageTypes fileImageType, Long targetId) {
        FileImagesDTO result = new FileImagesDTO();
        result.setFileImageType(fileImageType);
        if (fileImageType.getTargetTable() != null) {
            result.setFile(findOneByFileImageTypeAndTargetId(fileImageType, targetId));

            if (result.getFile() != null) {
                result.setFileItem(findOneFileItemByFileImageTypeAndTargetIdAndFile(fileImageType,targetId,result.getFile()));
            }
        } else {
            result.setFile(findOneByTypeName(fileImageType));
        }
        return result;
    }

    @Override
    public FileImagesDTO existFileImageType(FileImageTypes fileImageType) {
        return this.exist(fileImageType, null);
    }

    @Override
    public FileImagesDTO existTargetId(Long targetId) {
        return this.exist(null, targetId);
    }

    @Override
    public void deleteFileItem(FileItem fileItem) {
        fileItemRepository.delete(fileItem);
    }

    @Override
    public void delete(FileImagesDTO fileImagesDTO) {

        // if there's already a file, remove it and replace it unless it is part of a catalog.
        //deleting the existing FILE_ITEM and FILES if it is not part of a catalog (JUMBOTRON, AWARD_CODE_ICON, AWARD_CODE_CERT).
        if (! fileImagesDTO.getFileImageType() .isCatalog()) {
            if (fileImagesDTO.getFileItem()  != null) {
                deleteFileItem(fileImagesDTO.getFileItem());
            } // should trhow an exception if fileItemImage is not found?

            if ( fileImagesDTO.getFile()  != null) {
                filesDao.delete( fileImagesDTO.getFile());
            }
        }
    }

    @Override
    public void save(Files fileImage) {
        filesRepository.save(fileImage);
        if (logger.isDebugEnabled()){
            logger.debug("fileImage after saving:"+ fileImage.toString());
        }
    }

    /**
     * This method completes the saving of the image to the database
     *
     * @param bufferedImage The image to be saved to the db
     * @param type          The type of the image to be stored in the db
     * @param fileName      The type of the image to be stored in the db
     * @return The Image id to be passed back up to the reqeust
     * @throws IOException In case the processing of the image does not complete
     */
    @Override
    public Long saveImage(BufferedImage bufferedImage, String type, String fileName, String url) throws IOException {
        Files files = filesRepository.saveAndFlush(new Files()
            .setFileTypeCode(type)
            .setMediaType(MediaType.IMAGE_PNG_VALUE)
            .setName(fileName)
            .setUrl(url)
        );

        //save the file data
        StreamingOutput streamingOutput = new PNGStreamingOutput(bufferedImage);
        try (
            InputStream inputStream = StreamUtils.getInputStream(streamingOutput);
        ) {
            filesDao.findBy()
                .where("id").eq(files.getId())
                .write("data", inputStream, inputStream.available())
            ;
            return files.getId();
        }
    }

    @Override
    public Long saveImage(BufferedImage bufferedImage, String type, String fileName) throws IOException {
        return saveImage(bufferedImage, type, fileName, null);
    }

    @Override
    public void saveFileItem(FileItem fileItem) {
          fileItemRepository.save(fileItem);
    }

    @Override
    public void deleteFilesItemInTargetTableWithTargetId(String targetTable, Long targetId) {
        fileItemRepository.delete(fileItemRepository.findBy()
            .where(ProjectConstants.TARGET_TABLE).eq(targetTable)
            .and(ProjectConstants.TARGET_ID).eq(targetId)
            .findAll()
        );
    }

    @Override
    public Files deleteImageRecords(String type) {
        List<Files> preExistingFiles = filesRepository.findBy()
                .where(ProjectConstants.FILES_TABLE)
                .and(ProjectConstants.FILE_TYPE_CODE).eq(type)
                .findAll();
        filesRepository.delete(preExistingFiles);
        if (!preExistingFiles.isEmpty()) {
            return preExistingFiles.get(0);
        } else {
            return null;
        }
    }

}
