package com.maritz.culturenext.images.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.IMAGE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.TARGET_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.TYPE_REST_PARAM;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.images.service.FileImageService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Currently used by FileImageType `CAROUSEL`, `PROGRAM_IMAGE`, `PROGRAM_IMAGE_DETAILED`, `PROGRAM_IMAGE_SMALL and HERO_IMAGE
 * 
 */
@RestController
public class FileImageRestService {
    private static final String ERROR_FILEIMAGE_TYPE = "FILEIMAGE_TYPE";
    private static final String ERROR_FILEIMAGE_TYPE_USER_NOT_FOUND_MSG = "User not found.";
    @Inject private FileImageService fileImageService;
    @Inject private Security security;

    //rest/images?type={type}&targetId={targetId}
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "/images", method = RequestMethod.POST)
    @ApiOperation(value="Upload image", notes="The logged in user needs to be an admin in order to upload an image.\n" +
            "The passed in image cannot be greater than 6MB.\n" +
            "The passed in image should be in one of the following formats: .JPG, .PNG, or .JPEG")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully uploaded image"),
            @ApiResponse(code = 403, message = "User is not admin"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    // !!!! TODO !!!! Review security on this endpoint; see CNXT-742
    public IdDTO uploadImage(
            @ApiParam(value = "multipart file of image as form param (param name is \"image\")") 
            @RequestParam(value = IMAGE_REST_PARAM, required = true) MultipartFile image,
            @RequestParam(value = TYPE_REST_PARAM, required = true) String type,
            @RequestParam(value = TARGET_ID_REST_PARAM, required = false) Long targetId
    ) throws Throwable {    
        return fileImageService.uploadImage(image, type, targetId);
    }

    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "/images", method = RequestMethod.DELETE)
    @ApiOperation(value="Delete image", notes="Endpoint used for deleting an image with the given id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully  image deleted"),
            @ApiResponse(code = 403, message = "User is not admin"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    // !!!! TODO !!!! Review security on this endpoint; see CNXT-742
    public void deleteImageByType(
            @ApiParam(value = "Optional table to filter by")
            @RequestParam(value = TYPE_REST_PARAM, required = true) String type) {
        Long paxId = security.getPaxId();
        if (paxId == null) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ERROR_FILEIMAGE_TYPE)
                    .setField(ProjectConstants.ECARD_ID)
                    .setMessage(ERROR_FILEIMAGE_TYPE_USER_NOT_FOUND_MSG));
        }
        
        fileImageService.deleteImageByType( type);
    }

    //rest/images/{id}
    @RequestMapping(value = "/images/{id}", method = RequestMethod.GET)
    @ApiOperation(value="Get image", notes="Endpoint used to get an image with the given id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully uploaded image"),
            @ApiResponse(code = 403, message = "User is not admin"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getImageById(
            @PathVariable(value = ID_REST_PARAM) Long imageIdLong
    ) {
        Long paxId = security.getPaxId();
        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        if(imageIdLong == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        return  fileImageService.getImageFile(null, imageIdLong, null);
    }

    //rest/images?type={type}&targetId={targetId}
    @RequestMapping(value = "/images", method = RequestMethod.GET)
    @ApiOperation(value="Get image", notes="Endpoint used to get an image with the given id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully uploaded image"),
            @ApiResponse(code = 403, message = "User is not admin"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getImageByTypeAndTarget(
            @RequestParam(value = TYPE_REST_PARAM, required = false) String type,
            @RequestParam(value = TARGET_ID_REST_PARAM, required = false) String targetIdString
    ) {
        Long paxId = security.getPaxId();
        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        Long targetId = security.getId(targetIdString, null);
        
        // both params must be provided.
        if(targetId == null && type == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        return  fileImageService.getImageFile(type, null, targetId);
    }
}
