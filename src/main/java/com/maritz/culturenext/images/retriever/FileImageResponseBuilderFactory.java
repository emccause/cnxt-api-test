package com.maritz.culturenext.images.retriever;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.service.impl.HeaderDecorator;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
public class FileImageResponseBuilderFactory {
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String IMAGE_FOR_RESPONSE = "\"file; filename=\"image%d.%s\"";
    private static final String PROFILE_IMAGE_FOR_RESPONSE = "file; filename=\"profileImage%d.%s\"";
    private static final String PROFILE_IMAGE_WITH_ECARD_FOR_RESPONSE = "file; filename=\"profileImage%s.%s\"";
    private static final String LOGO_FILENAME_FOR_RESPONSE = "file; filename=\"logo.%s\"";
    private static final String LOGO_LIGHT_FOR_RESPONSE = "file; filename=\"logo light.%s\"";
    private static final String BACKGROUND_IMAGE_FOR_RESPONSE = "file; filename=\"background.%s\"";


    public static class UrlWrapper {
        private final String url;

        public UrlWrapper(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;



    @Inject
    public FileImageResponseBuilderFactory setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public FileImageResponseBuilderFactory setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    private String getFileExtension(Files imageEntry) {
        MediaTypesEnum mediaType;
        try {
            mediaType = MediaTypesEnum.fromValue(imageEntry.getMediaType());
        } catch (IllegalArgumentException e) {
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.UNHANDLED_FILE_IMAGE_TYPE)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_UNHANDLED_FILE_IMAGE_TYPE)));
        }
        return mediaType.getFileExtension();
    }

    public ResponseEntity<Object> prepareImageForResponse(Files imageEntry ){
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return prepareImageForResponse(imageEntry.getId() ,imageEntry);
    }

    public ResponseEntity<Object> prepareImageForResponse(Long id, Files imageEntry) {

        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);

        String value = String.format(IMAGE_FOR_RESPONSE,
                id,
                mediaType);
        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION,value);

        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareProfileImageForResponse(Long paxId, Files imageEntry) {
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);
        String value = String.format(
                PROFILE_IMAGE_FOR_RESPONSE,
                paxId,
                mediaType);
        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION, value);

        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareProfileImageForResponse(String ecardName, Files imageEntry) {
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);

        String value = String.format(PROFILE_IMAGE_WITH_ECARD_FOR_RESPONSE,
                ecardName,
                mediaType);
        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION, value);

        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareLogoImageForResponse(Files imageEntry) {
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);

        String value = String.format(LOGO_FILENAME_FOR_RESPONSE,
                mediaType);
        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION, value);
        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareLogoLightImageForResponse(Files imageEntry) {
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);

        String value = String.format(LOGO_LIGHT_FOR_RESPONSE,
                mediaType);

        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION, value);
        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareBackgroundImageForResponse(Files imageEntry) {
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        String mediaType = getFileExtension(imageEntry);
        String value = String.format(BACKGROUND_IMAGE_FOR_RESPONSE,
                mediaType);
        HeaderDecorator headerDecorator = (header) ->
                header.set(CONTENT_DISPOSITION, value);
        return prepareGenericImageForResponse(imageEntry,headerDecorator);
    }

    public ResponseEntity<Object> prepareGenericImageForResponse(Files imageEntry, HeaderDecorator headerDecorator){
        if (imageEntry == null || imageEntry.getMediaType() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (environmentUtil.readFromCdn()) {
            try {
                return ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(new FileImageResponseBuilderFactory.UrlWrapper(gcpUtil.signUrl(imageEntry)));
            } catch (Exception e) {
                logger.error("Error encountered while attempting to return the URL for " + imageEntry +
                        "; attempting to return the image from the database as a backup", e);
            }
        }

        HttpHeaders headers = new HttpHeaders();
        try {
            headers.setContentType(MediaType.parseMediaType(imageEntry.getMediaType()));
        } catch (InvalidMediaTypeException e){
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.UNHANDLED_FILE_IMAGE_TYPE)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_UNHANDLED_FILE_IMAGE_TYPE)));
        }
        if (headerDecorator != null){
            headerDecorator.decorate(headers);
        }
        return new ResponseEntity<>(StreamUtils.getStreamingOutput(imageEntry, ProjectConstants.DATA), headers, HttpStatus.OK);
    }
}
