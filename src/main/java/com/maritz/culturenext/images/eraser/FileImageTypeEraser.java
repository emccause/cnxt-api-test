package com.maritz.culturenext.images.eraser;

import org.springframework.stereotype.Component;

/**
 * This contract should be implemented by File Image Type in case that there is a specific algorithm that should
 * be implemented by File Image Type
 *
 */
@Component
public interface FileImageTypeEraser {

    /**
     * algorithm to implement when erasing a file image type.
     */
    public abstract void deleteByType();
}
