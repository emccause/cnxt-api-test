package com.maritz.culturenext.images.eraser;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;

/**
 * Factory that returns an TypeEraser by fileTypeImage. Only implemented HERO_IMAGE others will throw exceptions.
 *
 */
@Component
public class FileImageTypeEraserFactory {
    private  final static Logger logger = LoggerFactory.getLogger(FileImageTypeEraserFactory.class);
    @Inject private  HeroImageFileImageTypeEraserImpl heroImageFileImageType;
    
    public  FileImageTypeEraser getFileImageType(String fileTypeImage) {
        FileImageTypeEraser result = null;
        if (FileImageTypes.HERO_IMAGE.name().equals(fileTypeImage)){
            result = heroImageFileImageType;
        } else {
            logger.info("File image type not implemented yet.");
            throw new ErrorMessageException((new ErrorMessage()
                    .setCode(FileImageConstants.UNHANDLED_FILE_IMAGE_TYPE)
                    .setField(ProjectConstants.IMAGE)
                    .setMessage(FileImageConstants.ERROR_UNHANDLED_FILE_IMAGE_TYPE)));
        }
        return result;
    }
}
