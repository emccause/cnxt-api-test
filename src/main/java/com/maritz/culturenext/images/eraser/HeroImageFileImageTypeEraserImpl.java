package com.maritz.culturenext.images.eraser;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dto.FileImagesDTO;

@Component
public class HeroImageFileImageTypeEraserImpl implements FileImageTypeEraser {

    @Inject private FilesDao filesDao;

    /**
     * Erases the first HERO_IMAGE found in FILES and FILES_ITEM
     */
    @Override
    public void deleteByType() {
        assert filesDao!=null;
        FileImagesDTO fileImagesTO =  filesDao.exist(FileImageTypes.HERO_IMAGE, null);
        filesDao.delete(fileImagesTO);
    }

}
