package com.maritz.culturenext.images.validators;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.ecard.services.impl.EcardServiceImpl;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import static com.maritz.culturenext.program.ecard.services.impl.EcardServiceImpl.*;

@Component
public class EcardFileImageValidator implements FileImageValidator {
    private static final ArrayList<String> SUPPORTED_IMAGE_TYPES =
            new ArrayList<>(Arrays.asList("image/jpeg", "image/png"));

    @Override
    public BufferedImage validateImageForUpload(MultipartFile image, String type, Long targetId) throws Exception {
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        if(image == null || image.isEmpty()){
            throw new ErrorMessageException(ProgramActivityConstants.ECARD_MISSING_MESSAGE);
        }

        String contentType = image.getContentType();

        BufferedImage bufferedImage = null;
        if(contentType == null || !SUPPORTED_IMAGE_TYPES.contains(contentType.toLowerCase())){
            errors.add(ProgramActivityConstants.CONTENT_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        else{
            ImageReader imageReader = null;
            MemoryCacheImageInputStream mciis = null;
            try{
                Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByMIMEType(contentType);
                if(imageReaders != null && imageReaders.hasNext()){
                    imageReader = imageReaders.next();
                }
                else{
                    errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if(image.getBytes() != null){
                    mciis = new MemoryCacheImageInputStream(new ByteArrayInputStream(image.getBytes()));
                }
                else{
                    errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if(imageReader != null && mciis != null){
                    imageReader.setInput(mciis);
                    if(imageReader.getNumImages(true) > 1){
                        errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                        throw new ErrorMessageException().addErrorMessages(errors);
                    }
                    bufferedImage = imageReader.read(0);
                }


            }
            catch(Exception e){
                errors.add(ProgramActivityConstants.IMAGE_PROCESS_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
            finally{
                if(mciis != null){
                    mciis.close();
                }
            }
        }

        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        if(width < ECARD_DEFAULT_WIDTH || height < ECARD_DEFAULT_HEIGHT){
            errors.add(new ErrorMessage()
                    .setCode(ERROR_ECARD_TOO_SMALL)
                    .setField(ProjectConstants.UPLOAD)
                    .setMessage(ERROR_ECARD_TOO_SMALL_MSG));
        }
        if(!errors.isEmpty()){
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        return bufferedImage;
    }
}
