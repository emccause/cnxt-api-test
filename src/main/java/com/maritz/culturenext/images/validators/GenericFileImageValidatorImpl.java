package com.maritz.culturenext.images.validators;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileItemTypeEnum;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageConstants;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;

@Component
public class GenericFileImageValidatorImpl implements FileImageValidator {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private static final long MAXIMUM_ALLOWED_FILE_SIZE = 6000000; // size in bytes
    
    @Override
    public BufferedImage validateImageForUpload(MultipartFile image, String type, Long targetId) throws Exception {
        if (logger.isDebugEnabled()){
            logger.debug("Entering validateImageForUpload. ");
        }
        List<ErrorMessage> errors = new ArrayList<>();

        String contentType = null;
        if (image == null || image.isEmpty()) {
            errors.add(ImageConstants.IMAGE_MISSING_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } 
        else {
            contentType = image.getContentType();
        }

        if (type == null) {
            errors.add(ImageConstants.MISSING_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        if (!FileImageTypes.containsImageFileType(type)) {
            errors.add(ImageConstants.INVALID_IMAGE_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        
        if (FileImageTypes.valueOf(type).getTargetTable() != null && !FileImageTypes.valueOf(type).isCatalog() && targetId == null) {
            errors.add(FileImageConstants.ERROR_TARGET_ID_MISSING);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        // the maximum allowed size is standard for all type of images. 
        if (image.getSize() > MAXIMUM_ALLOWED_FILE_SIZE){
            errors.add(ImageConstants.IMAGE_TOO_BIG_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        ErrorMessageException.throwIfHasErrors(errors); // Bail here if we've hit errors, do not process

        BufferedImage bufferedImage = null;
        
        if ( contentType == null || !MediaTypesEnum.getMediaTypes().contains(contentType.toLowerCase())) {
            errors.add(ImageConstants.CONTENT_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        
        } else if (!MediaTypesEnum.IMAGE_SVG.value().equalsIgnoreCase(contentType)
                && !MediaTypesEnum.IMAGE_SVG_XML.value().equalsIgnoreCase(contentType)) {
        
            ImageReader imageReader;
            MemoryCacheImageInputStream mciis = null;

            try {
                Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByMIMEType(contentType);
                if (imageReaders != null && imageReaders.hasNext()) {
                    imageReader = imageReaders.next();
                } else {
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if (image.getBytes() != null) {
                    mciis = new MemoryCacheImageInputStream(new ByteArrayInputStream(image.getBytes()));
                } else {
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                }

                if (imageReader != null) {
                    imageReader.setInput(mciis);
                    if (imageReader.getNumImages(true) > 1) {
                        errors.add(ImageConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
                        throw new ErrorMessageException().addErrorMessages(errors);
                    }
                    bufferedImage = imageReader.read(0);
                }
            } catch (Exception e) {
                errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
            } finally {
                if (mciis != null) {
                    mciis.close();
                }
            }
            if (logger.isDebugEnabled()){
                logger.debug("leaving validateImageForUpload. ");
            }
        }
        ErrorMessageException.throwIfHasErrors(errors); // Bail here if we've hit errors, do not process
        
        FileImageTypes fileImageType = FileImageTypes.valueOf(type);
        
        // edit image in case it doesn't fit with the size of the file type. do no check if it is Hero_image
        if ( !FileImageTypes.HERO_IMAGE.equals(fileImageType)  && fileImageType.getImgHeight() != null && fileImageType.getImgWidth() != null &&
                    (bufferedImage.getHeight() < fileImageType.getImgHeight() || bufferedImage.getWidth() < fileImageType.getImgWidth())) {
                errors.add(ImageConstants.IMAGE_TOO_SMALL_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
        }
        // ecard name validation
        String ecardName = image.getOriginalFilename();
        if(fileImageType.getTargetTable() != null && FileItemTypeEnum.ECARD.name().equals(fileImageType.getTargetTable().name())){
            if(ecardName == null || ecardName.trim().equals("")){
                throw new ErrorMessageException(ProgramActivityConstants.ECARD_NAME_MISSING_MESSAGE);
            }
            else if(ecardName.length() > FileImageConstants.MAX_ECARD_NAME_LENGTH){
                throw new ErrorMessageException((new ErrorMessage()
                        .setCode(FileImageConstants.ERROR_ECARD_NAME_LENGTH)
                        .setField(ProjectConstants.DISPLAY_NAME)
                        .setMessage(FileImageConstants.ERROR_ECARD_NAME_LENGTH_MSG)));
            }
        }
        ErrorMessageException.throwIfHasErrors(errors); // Bail here if we've hit errors, do not process
        
        return bufferedImage;

    }
}
