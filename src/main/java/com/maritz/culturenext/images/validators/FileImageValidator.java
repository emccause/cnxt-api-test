package com.maritz.culturenext.images.validators;

import java.awt.image.BufferedImage;

import org.springframework.web.multipart.MultipartFile;

/**
 * Contract that should be Implemented by image file class  that validates characteristics of the file to be persisted.
 *
 */
public interface FileImageValidator {

    /**
     * validations that should take place before the file should be persisted.
     * @param image Information of the file to be persisted.
     * @param type File Image Type name that the file to be persisted will be associated with
     * @param targetId 
     * @return the contents of the file that would be persisted to the database.
     * @throws Exception
     */
    BufferedImage validateImageForUpload(MultipartFile image, String type, Long targetId) throws Exception;
}
