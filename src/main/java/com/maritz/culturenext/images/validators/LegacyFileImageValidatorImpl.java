package com.maritz.culturenext.images.validators;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.ImageConstants;

/**
 * Legacy validation associated with existing files like Carousel, Program Image Detailed, Program Image, and Program Image Small File.
 * Note: Consider merging with GenericFileImageValidatorImpl
 */
@Component
public class LegacyFileImageValidatorImpl implements FileImageValidator {
    
    @Override
    public BufferedImage validateImageForUpload(MultipartFile image, String type, Long targetId) throws Exception {
        Long maxFileSizeBytes = ImageConstants.MAX_IMAGE_SIZE_MB * (1024 * 1024);

        List<ErrorMessage> errors = new ArrayList<>();

        String contentType;
        Long imageSize;

        if (image == null || image.isEmpty()) {
            errors.add(ImageConstants.IMAGE_MISSING_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else {
            contentType = image.getContentType();
            imageSize = image.getSize();
        }
        if (contentType == null || !MediaTypesEnum.containsImageMediaType(contentType.toLowerCase())) {
            errors.add(ImageConstants.IMAGE_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        if (imageSize.compareTo(maxFileSizeBytes) > 0) {
            errors.add(ImageConstants.IMAGE_TOO_BIG_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        if (type == null) {
            errors.add(ImageConstants.MISSING_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        if (!FileTypes.containsImageFileType(type)) {
            errors.add(ImageConstants.INVALID_IMAGE_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        ErrorMessageException.throwIfHasErrors(errors); // Bail here if we've hit errors, do not process

        BufferedImage bufferedImage = null;
        if (!MediaTypesEnum.containsImageMediaType(contentType.toLowerCase())) {
            errors.add(ImageConstants.CONTENT_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else {
            ImageReader imageReader;
            MemoryCacheImageInputStream mciis = null;

            try {
                Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByMIMEType(contentType);
                if (imageReaders != null && imageReaders.hasNext()) {
                    imageReader = imageReaders.next();
                } else {
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if (image.getBytes() != null) {
                    mciis = new MemoryCacheImageInputStream(new ByteArrayInputStream(image.getBytes()));
                } else {
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                }

                if (imageReader != null) {
                    imageReader.setInput(mciis);
                    if (imageReader.getNumImages(true) > 1) {
                        errors.add(ImageConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
                        throw new ErrorMessageException().addErrorMessages(errors);
                    }
                    bufferedImage = imageReader.read(0);
                }
            } catch (Exception e) {
                errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
            } finally {
                if (mciis != null) {
                    mciis.close();
                }
            }
        }

        ErrorMessageException.throwIfHasErrors(errors);

        return bufferedImage;
    }

}
