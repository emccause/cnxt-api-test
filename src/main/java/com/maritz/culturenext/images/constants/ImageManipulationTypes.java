package com.maritz.culturenext.images.constants;

public enum ImageManipulationTypes {
    CENTER_CROP,
    CIRCLE_CROP,
    SQUARE_CROP,
    RESIZE,
    SCALED_DOWN_1,
    PROFILE;
}
