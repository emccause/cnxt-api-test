package com.maritz.culturenext.images.constants;

import static com.maritz.culturenext.images.constants.ImageManipulationTypes.CENTER_CROP;
import static com.maritz.culturenext.images.constants.ImageManipulationTypes.RESIZE;
import static com.maritz.culturenext.images.constants.ImageManipulationTypes.SCALED_DOWN_1;
import static com.maritz.culturenext.images.constants.ImageManipulationTypes.SQUARE_CROP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.TableName;

public enum FileImageTypes {
    
    ECARD_THUMBNAIL (FileTypes.ECARD_THUMBNAIL, TableName.ECARD, Constants.ECARD_THUMBNAIL_HEIGHT, Constants.ECARD_THUMBNAIL_WIDTH, RESIZE, null, false),
    ECARD_DEFAULT (FileTypes.ECARD_DEFAULT, TableName.ECARD, Constants.ECARD_DEFAULT_HEIGHT, Constants.ECARD_DEFAULT_WIDTH, RESIZE, Arrays.asList(ECARD_THUMBNAIL), false),

    GROUP_LOGO (FileTypes.GROUP_LOGO, TableName.GROUPS, null, null, null, null, false),
    GROUP_HERO_IMAGE (FileTypes.GROUP_HERO_IMAGE, TableName.GROUPS, null, null, null, null, false),
    
    PROGRAM_IMAGE_SMALL (FileTypes.PROGRAM_IMAGE_SMALL, TableName.PROGRAM, Constants.PROGRAM_IMAGE_SMALL_HEIGHT, Constants.PROGRAM_IMAGE_SMALL_WIDTH, SQUARE_CROP, null, false),
    PROGRAM_IMAGE_DETAILED (FileTypes.PROGRAM_IMAGE_DETAILED, TableName.PROGRAM, Constants.PROGRAM_IMAGE_DETAILED_HEIGHT, Constants.PROGRAM_IMAGE_DETAILED_WIDTH, SQUARE_CROP, null, false),
    PROGRAM_IMAGE (FileTypes.PROGRAM_IMAGE, TableName.PROGRAM, Constants.PROGRAM_IMAGE_HEIGHT, Constants.PROGRAM_IMAGE_WIDTH, SQUARE_CROP, Arrays.asList(PROGRAM_IMAGE_SMALL, PROGRAM_IMAGE_DETAILED), false),
    CAROUSEL (FileTypes.CAROUSEL, TableName.PROGRAM, Constants.CAROUSEL_HEIGHT, Constants.CAROUSEL_WIDTH, CENTER_CROP, null, false),

    AWARD_CODE_CERT (FileTypes.AWARD_CODE_CERT, TableName.PROGRAM, null, null, null, null, true),
    AWARD_CODE_ICON (FileTypes.AWARD_CODE_ICON,TableName.PROGRAM, null, null, null, null, true),
    JUMBOTRON (FileTypes.JUMBOTRON, TableName.PROGRAM_ACTIVITY, null, null, null, null, true),

    PROJECT_BACKGROUND (FileTypes.PROJECT_BACKGROUND,null,Constants.PROJECT_BACKGROUND_HEIGHT, Constants.PROJECT_BACKGROUND_WIDTH, RESIZE, null, false),
    PROJECT_LOGO_LIGHT (FileTypes.PROJECT_LOGO_LIGHT,null,Constants.PROJECT_LOGO_LIGHT_HEIGHT, Constants.PROJECT_LOGO_LIGHT_WIDTH, RESIZE, null, false),
    PROJECT_LOGO (FileTypes.PROJECT_LOGO,null, Constants.PROJECT_LOGO_HEIGHT, Constants.PROJECT_LOGO_WIDTH, RESIZE, Arrays.asList(PROJECT_LOGO_LIGHT), false),
    
    PRFT (FileTypes.PRFT, TableName.PAX, Constants.IMAGE_TINY_HEIGHT, Constants.IMAGE_TINY_WIDTH, RESIZE, null, false), // Profile Image Tiny
    PRFS (FileTypes.PRFS,TableName.PAX, Constants.IMAGE_SCALED_1_HEIGHT, Constants.IMAGE_SCALED_1_WIDTH, SCALED_DOWN_1, null, false), // Profile Image Scaled
    PRFD (FileTypes.PRFD,TableName.PAX, Constants.PROFILE_IMAGE_HEIGHT, Constants.PROFILE_IMAGE_WIDTH, ImageManipulationTypes.PROFILE, Arrays.asList(PRFT, PRFS), false), // Profile Image Default; 
     
    HERO_IMAGE(FileTypes.HERO_IMAGE, null, Constants.HERO_IMAGE_HEIGHT, Constants.HERO_IMAGE_WIDTH, RESIZE, null, false);
    
    private final FileTypes type;
    private final boolean isCatalog;
    private final Integer imgWidth;
    private final Integer imgHeight;
    private final TableName targetTable;
    private final ImageManipulationTypes imageManipulation;
    private final List<FileImageTypes> subFileTypeList;
    private static List<String> types;


    /**
     * Creates a File Image type Element. 
     * 
     * @param type name of the file type element stored at FileTypes repository.
     * @param targetTable table name where the image will be persisted.
     * @param imgHeight Height of the image in pixels
     * @param imgWidth width of the image in pixels
     * @param imageManipulation allows any image manipulation specified at ImageManipulationTypes enum.
     * @param subFileTypeList  in case there is a combined fileType
     * @param isCatalog defines if  the image whether belongs to a catalog  (JUMBOTRON, AWARD_CODE_ICON, AWARD_CODE_CERT).
     */
    private FileImageTypes(FileTypes type, TableName targetTable, Integer imgHeight, Integer imgWidth,
            ImageManipulationTypes imageManipulation, 
            List<FileImageTypes> subFileTypeList, boolean isCatalog) {
        this.type = type;
        this.targetTable = targetTable;
        this.imgHeight = imgHeight;
        this.imgWidth = imgWidth;
        this.imageManipulation = imageManipulation;
        this.subFileTypeList = subFileTypeList;
        this.isCatalog = isCatalog;
    }
    
    public static boolean containsImageFileType(String s) {
        return getImageFileTypes().contains(s);
    }

    
    public static List<String> getImageFileTypes() {
        if (types == null){ // enforcing a singleton
            types = new ArrayList<>();
            for (FileImageTypes fileType : values()) {
                types.add(fileType.name());
            }
        }
        return types;
    }

    public FileTypes getType() {
        return type;
    }

    public Integer getImgWidth() {
        return imgWidth;
    }

    public Integer getImgHeight() {
        return imgHeight;
    }
    
    public TableName getTargetTable() {
        return targetTable;
    }
    public ImageManipulationTypes getImageManipulationType() {
        return imageManipulation;
    }
    
    public List<FileImageTypes> getSubFileTypeList() {
        return subFileTypeList;
    }

    public boolean isCatalog() {
        return isCatalog;
    }
    
    private static class Constants {
        public static final int ECARD_DEFAULT_WIDTH = 1000;
        public static final int ECARD_DEFAULT_HEIGHT = 667;
        public static final int ECARD_THUMBNAIL_WIDTH = 480;
        public static final int ECARD_THUMBNAIL_HEIGHT = 320;

        public static final int CAROUSEL_HEIGHT = 430;
        public static final int CAROUSEL_WIDTH = 1720;
        public static final int PROGRAM_IMAGE_HEIGHT = 150;
        public static final int PROGRAM_IMAGE_WIDTH = 150;
        public static final int PROGRAM_IMAGE_SMALL_HEIGHT = 50;
        public static final int PROGRAM_IMAGE_SMALL_WIDTH = 50;
        public static final int PROGRAM_IMAGE_DETAILED_HEIGHT = 75;
        public static final int PROGRAM_IMAGE_DETAILED_WIDTH = 75;
        
        public static final int PROJECT_LOGO_WIDTH = 400;
        public static final int PROJECT_LOGO_HEIGHT = 150;

        public static final int HERO_IMAGE_WIDTH = 2000;
        public static final int HERO_IMAGE_HEIGHT = 2000;

        public static final int PROJECT_LOGO_LIGHT_WIDTH = 400;
        public static final int PROJECT_LOGO_LIGHT_HEIGHT = 150;

        public static final int PROJECT_BACKGROUND_WIDTH = 2000;
        public static final int PROJECT_BACKGROUND_HEIGHT = 1500;
        
        public static final int PROFILE_IMAGE_WIDTH = 400;
        public static final int PROFILE_IMAGE_HEIGHT = 400;
        
        public static final int IMAGE_TINY_WIDTH = 60;
        public static final int IMAGE_TINY_HEIGHT = 60;
        
        public static final int IMAGE_SCALED_1_WIDTH = 1;
        public static final int IMAGE_SCALED_1_HEIGHT = 1;
    }
}
