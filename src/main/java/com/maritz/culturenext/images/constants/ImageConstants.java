package com.maritz.culturenext.images.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;

public class ImageConstants {
    
    public static final Long MAX_IMAGE_SIZE_MB = 6L;
    
    public static final String ERROR_IMAGE_MISSING_CODE = "ERROR_IMAGE_MISSING";
    public static final String ERROR_IMAGE_MISSING_MESSAGE = "Must upload an image.";
    public static final String ERROR_IMAGE_PROCESS_CODE = "ERROR_IMAGE_PROCESS";
    public static final String ERROR_IMAGE_PROCESS_MESSAGE = "Error processing image.";
    public static final String ERROR_IMAGE_UPLOAD_COUNT_CODE = "ERROR_IMAGE_UPLOAD_COUNT";
    public static final String ERROR_IMAGE_UPLOAD_COUNT_MESSAGE = "Only one image can be uploaded.";
    public static final String ERROR_INVALID_IMAGE_TYPE = "ERROR_INVALID_IMAGE_TYPE";
    public static final String ERROR_INVALID_IMAGE_TYPE_MSG = "Cannot upload image not in list";
    public static final String ERROR_IMAGE_TOO_SMALL_CODE = "IMAGE_TOO_SMALL";
    public static final String ERROR_IMAGE_TOO_SMALL_MSG = "The image is too small: ";
    public static final String ERROR_MISSING_TYPE = "MISSING_TYPE";
    public static final String ERROR_MISSING_TYPE_MSG = "Type is required";
    public static final String ERROR_CONTENT_TYPE_CODE = "CONTENT_TYPE";
    public static final String ERROR_CONTENT_TYPE_MSG = "Unsupported content type. Supported content types are PNG, JPG, and JPEG.";
    public static final String ERROR_IMAGE_FILE_TYPE_CODE = "ERROR_IMAGE_FILE_TYPE_CODE_WRONG_TYPE";
    public static final String ERROR_IMAGE_FILE_TYPE_CODE_MESSAGE = "Wrong file type code";
    public static final String ERROR_CROP_PARAMETER_COUNT_CODE = "ERROR_CROP_PARAMETER_COUNT";
    public static final String ERROR_CROP_PARAMETER_COUNT_MESSAGE = "Incorrect number of parameters for crop.";
    public static final String ERROR_CROP_X_PARAM_CODE = "ERROR_CROP_X_PARAM";
    public static final String ERROR_CROP_X_PARAM_MESSAGE = "Incorrect X value for crop.";
    public static final String ERROR_CROP_Y_PARAM_CODE = "ERROR_CROP_Y_PARAM";
    public static final String ERROR_CROP_Y_PARAM_MESSAGE = "Incorrect Y value for crop.";
    public static final String ERROR_CROP_WIDTH_PARAM_CODE = "ERROR_CROP_WIDTH_PARAM";
    public static final String ERROR_CROP_WIDTH_PARAM_MESSAGE = "Incorrect width value for crop.";
    public static final String ERROR_CROP_HEIGHT_PARAM_CODE = "ERROR_CROP_HEIGHT_PARAM";
    public static final String ERROR_CROP_HEIGHT_PARAM_MESSAGE = "Incorrect height value for crop.";
    public static final String ERROR_CROP_AREA_CODE = "ERROR_CROP_AREA";
    public static final String ERROR_CROP_AREA_MESSAGE = "Specified area to crop not contained within the image provided.";
    public static final String ERROR_DELETE_IMAGE_CODE = "ERROR_DELETE_IMAGE";
    public static final String ERROR_DELETE_IMAGE_MESSAGE = "User doesn't have an image to delete.";
    
    public static final String CROP = "crop";
    public static final String DELETE = "delete";

    public static final int CAROUSEL_HEIGHT = 430;
    public static final int CAROUSEL_WIDTH = 1720;
    public static final int PROGRAM_IMAGE_HEIGHT = 150;
    public static final int PROGRAM_IMAGE_WIDTH = 150;
    public static final int PROGRAM_IMAGE_SMALL_HEIGHT = 50;
    public static final int PROGRAM_IMAGE_SMALL_WIDTH = 50;
    public static final int PROGRAM_IMAGE_DETAILED_HEIGHT = 75;
    public static final int PROGRAM_IMAGE_DETAILED_WIDTH = 75;

    //Error Messages
    public static final ErrorMessage IMAGE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_MISSING_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ImageConstants.ERROR_IMAGE_MISSING_MESSAGE);
    
    public static final ErrorMessage IMAGE_UPLOAD_COUNT_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_UPLOAD_COUNT_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ImageConstants.ERROR_IMAGE_UPLOAD_COUNT_MESSAGE);
    
    public static final ErrorMessage NOT_ADMIN_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NOT_ADMIN_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ProgramConstants.ERROR_NOT_ADMIN_CODE_MESSAGE);
    
    public static final ErrorMessage IMAGE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_IMAGE_TYPE_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ProgramConstants.ERROR_IMAGE_TYPE_MESSAGE);
    
    public static final ErrorMessage IMAGE_TOO_BIG_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_IMAGE_TOO_BIG_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ProgramConstants.ERROR_IMAGE_TOO_BIG_MESSAGE);
    
    public static final ErrorMessage MISSING_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_MISSING_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ImageConstants.ERROR_MISSING_TYPE_MSG);
    
    public static final ErrorMessage INVALID_IMAGE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_INVALID_IMAGE_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ImageConstants.ERROR_INVALID_IMAGE_TYPE_MSG);
    
    public static final ErrorMessage CONTENT_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_CONTENT_TYPE_CODE)
        .setField(ProjectConstants.UPLOAD)
        .setMessage(ImageConstants.ERROR_CONTENT_TYPE_MSG);
    
    public static final ErrorMessage IMAGE_PROCESS_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_PROCESS_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ImageConstants.ERROR_IMAGE_PROCESS_MESSAGE);
    
    public static final ErrorMessage IMAGE_TOO_SMALL_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_TOO_SMALL_CODE)
        .setField(ProjectConstants.IMAGE)
        .setMessage(ImageConstants.ERROR_IMAGE_TOO_SMALL_MSG);
    
    public static final ErrorMessage CROP_PARAMETER_COUNT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_PARAMETER_COUNT_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_PARAMETER_COUNT_MESSAGE);
    
    public static final ErrorMessage CROP_X_PARAM_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_X_PARAM_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_X_PARAM_MESSAGE);
    
    public static final ErrorMessage CROP_Y_PARAM_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_Y_PARAM_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_Y_PARAM_MESSAGE);
    
    public static final ErrorMessage CROP_WIDTH_PARAM_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_WIDTH_PARAM_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_WIDTH_PARAM_MESSAGE);
    
    public static final ErrorMessage CROP_HEIGHT_PARAM_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_HEIGHT_PARAM_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_HEIGHT_PARAM_MESSAGE);
    
    public static final ErrorMessage CROP_AREA_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CROP_AREA_CODE)
        .setField(CROP)
        .setMessage(ERROR_CROP_AREA_MESSAGE);
    
    public static final ErrorMessage DELETE_IMAGE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DELETE_IMAGE_CODE)
        .setField(DELETE)
        .setMessage(ERROR_DELETE_IMAGE_MESSAGE);

}
