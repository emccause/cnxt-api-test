package com.maritz.culturenext.images.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class FileImageConstants {
    
    
/*    public static final int ECARD_DEFAULT_WIDTH = 1000;
    public static final int ECARD_DEFAULT_HEIGHT = 667;
    public static final int ECARD_THUMBNAIL_WIDTH = 480;
    public static final int ECARD_THUMBNAIL_HEIGHT = 320;

    public static final int CAROUSEL_HEIGHT = 430;
    public static final int CAROUSEL_WIDTH = 1720;
    public static final int PROGRAM_IMAGE_HEIGHT = 150;
    public static final int PROGRAM_IMAGE_WIDTH = 150;
    public static final int PROGRAM_IMAGE_SMALL_HEIGHT = 50;
    public static final int PROGRAM_IMAGE_SMALL_WIDTH = 50;
    public static final int PROGRAM_IMAGE_DETAILED_HEIGHT = 75;
    public static final int PROGRAM_IMAGE_DETAILED_WIDTH = 75;
    
    public static final int PROJECT_LOGO_WIDTH = 400;
    public static final int PROJECT_LOGO_HEIGHT = 150;

    public static final int PROJECT_LOGO_LIGHT_WIDTH = 400;
    public static final int PROJECT_LOGO_LIGHT_HEIGHT = 150;

    public static final int PROJECT_BACKGROUND_WIDTH = 2000;
    public static final int PROJECT_BACKGROUND_HEIGHT = 1500;
    
    public static final int PROFILE_IMAGE_WIDTH = 400;
    public static final int PROFILE_IMAGE_HEIGHT = 400;
    
    public static final int IMAGE_TINY_WIDTH = 60;
    public static final int IMAGE_TINY_HEIGHT = 60;
    
    public static final int IMAGE_SCALED_1_WIDTH = 1;
    public static final int IMAGE_SCALED_1_HEIGHT = 1;*/
    
    public static final String TARGET_ID_MISSING_CODE = "TARGET_ID_MISSING";
    public static final String TARGET_ID_MISSING_CODE_MESSAGE = "Target id has not been defined";
    private static final String ERROR_INVALID_FILE_IMAGE_TYPE = "INVALID_FILE_TYPE";
    private static final String ERROR_INVALID_FILE_IMAGE_TYPE_MSG = "It is not an image file type.";
    
    public static final int MAX_ECARD_NAME_LENGTH = 50;
    public static final String ERROR_ECARD_NAME_LENGTH = "ECARD_NAME_LENGTH";
    public static final String ERROR_ECARD_NAME_LENGTH_MSG = 
            "Display name is too long.  Max length is " + MAX_ECARD_NAME_LENGTH;
    public static final String ECARD_CUSTOM_TYPE = "CUSTOM";
    
    public static final ErrorMessage ERROR_TARGET_ID_MISSING = new ErrorMessage()
            .setCode(TARGET_ID_MISSING_CODE)
            .setField(ProjectConstants.TARGET_ID)
            .setMessage(TARGET_ID_MISSING_CODE_MESSAGE);

    
    public static final ErrorMessage INVALID_FILE_IMAGE_TYPE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_FILE_IMAGE_TYPE)
            .setField(ProjectConstants.TYPE)
            .setMessage(ERROR_INVALID_FILE_IMAGE_TYPE_MSG);
    public static final String UNHANDLED_FILE_IMAGE_TYPE = "Unhandled file image type";
    public static final String ERROR_UNHANDLED_FILE_IMAGE_TYPE = "Operation not implemented yet for specified image type";
    public static final String FILE_IMAGE_TYPE_NOT_FOUND = "File image type not found.";
    public static final String ERROR_FILE_IMAGE_TYPE_DOES_NOT_EXIST = "File image type does not exist";
}
