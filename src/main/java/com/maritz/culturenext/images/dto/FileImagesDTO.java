package com.maritz.culturenext.images.dto;

import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.images.constants.FileImageTypes;

public class FileImagesDTO {
    private Files file = null;
    private FileItem fileItem = null;
    private FileImageTypes fileImageType = null;
    
    public FileItem getFileItem() {
        return fileItem;
    }
    public void setFileItem(FileItem fileItem) {
        this.fileItem = fileItem;
    }
    
    public Files getFile() {
        return file;
    }
    public void setFile(Files file) {
        this.file = file;
    }

    public FileImageTypes getFileImageType() {
        return fileImageType;
    }
    public void setFileImageType(FileImageTypes fileImageType) {
        this.fileImageType = fileImageType;
    }
}
