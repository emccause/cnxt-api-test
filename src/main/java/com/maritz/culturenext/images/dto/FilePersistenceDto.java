package com.maritz.culturenext.images.dto;


import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.core.util.stream.StreamingOutput;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class FilePersistenceDto {
    private final long id;
    private final byte[] data;


    public FilePersistenceDto(long id, byte[] data) {
        this.id = id;
        this.data = data;
    }

    public FilePersistenceDto(long id, StreamingOutput streamingOutput) throws IOException {
        this(id, toByteArray(streamingOutput));
    }

    public FilePersistenceDto(long id, BufferedImage bufferedImage) throws IOException {
        this(id, new PNGStreamingOutput(bufferedImage));
    }

    private static byte[] toByteArray(StreamingOutput streamingOutput) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        streamingOutput.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public long getId() {
        return id;
    }

    public byte[] getData() {
        return data;
    }
}
