package com.maritz.culturenext.images.dto;


public class ImageDTO {
    Long id;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
