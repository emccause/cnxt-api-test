package com.maritz.culturenext.content.dao.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.content.dao.TranslatableContentDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;


@Repository
public class TranslatableContentDaoImpl extends AbstractDaoImpl implements TranslatableContentDao {

    private static final String KEY_NAMES_SQL_PARAM = "keyNames";
    
    private static final List<String> platformContentKeyNames = Arrays.asList(
            ApplicationDataConstants.KEY_NAME_RECGPX_HEADER,
            ApplicationDataConstants.KEY_NAME_RECGMN_HEADER,
            ApplicationDataConstants.KEY_NAME_RECGPX_BUTTON,
            ApplicationDataConstants.KEY_NAME_RECGMN_BUTTON,
            ApplicationDataConstants.KEY_NAME_APR_HEADER,
            ApplicationDataConstants.KEY_NAME_APR_BUTTON,
            ApplicationDataConstants.KEY_NAME_RSPWDN_HEADER,
            ApplicationDataConstants.KEY_NAME_CREATE_PASSWORD_HEADER,
            ApplicationDataConstants.KEY_NAME_WEEKLY_DIGEST_HEADER,
            ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_AND,
            ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHER,
            ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHERS,
            ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS,
            ApplicationDataConstants.KEY_NAME_PASSWORD_CONFIRMATION_HEADER,
            ApplicationDataConstants.KEY_NAME_POINT_LOAD_HEADER,
            ApplicationDataConstants.KEY_NAME_NOTIFY_OTHER_HEADER,
            ApplicationDataConstants.KEY_NAME_MILESTONE_REMINDER_SERVICE_ANNIVERSARY_HEADER,
            ApplicationDataConstants.KEY_NAME_MILESTONE_REMINDER_BIRTHDAY_HEADER);

    private static final String PLATFORM_CONTENT_QUERY =
            "SELECT DISTINCT ids.TABLE_NAME, ids.KEY_NAME AS 'KEY', ids.VALUE AS 'en_US', " +
            "de_DE.TRANSLATION AS 'de_DE', en_GB.TRANSLATION AS 'en_GB', es_ES.TRANSLATION AS 'es_ES', es_MX.TRANSLATION AS 'es_MX', " + 
            "fr_CA.TRANSLATION AS 'fr_CA', fr_FR.TRANSLATION AS 'fr_FR', hi_IN.TRANSLATION AS 'hi_IN', it_IT.TRANSLATION AS 'it_IT', " + 
            "ja_JP.TRANSLATION AS 'ja_JP', nl_NL.TRANSLATION AS 'nl_NL', pt_BR.TRANSLATION AS 'pt_BR', pt_PT.TRANSLATION AS 'pt_PT', " +
            "ru_RU.TRANSLATION AS 'ru_RU', zh_CN.TRANSLATION AS 'zh_CN', zh_TW.TRANSLATION AS 'zh_TW' " +
            "FROM ( " +
                "SELECT 'APPLICATION_DATA' AS TABLE_NAME, APPLICATION_DATA_ID AS 'ID', KEY_NAME, VALUE " +
                "FROM component.APPLICATION_DATA " +
                "WHERE KEY_NAME IN (:" + KEY_NAMES_SQL_PARAM + ") " +
                "UNION ALL " +
                "SELECT 'CALENDAR_ENTRY' AS TABLE_NAME, ce.ID, ce.CALENDAR_ENTRY_NAME, ce.CALENDAR_ENTRY_NAME " +
                "FROM component.CALENDAR_ENTRY ce " +
                "JOIN component.CALENDAR_SUB_TYPE cst ON cst.ID = ce.CALENDAR_SUB_TYPE_ID " +
                "WHERE cst.CALENDAR_SUB_TYPE_CODE = 'US_OBSERVED' " +
                "UNION ALL " +
                "SELECT 'FILES' AS TABLE_NAME, ID, FILE_TYPE_CODE, CONVERT(VARCHAR(MAX), DATA) " +
                "FROM component.FILES " +
                "WHERE FILE_TYPE_CODE = 'TERMS_AND_CONDITIONS' " +
            ") ids " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE de_DE ON de_DE.TARGET_ID = ids.ID AND de_DE.LOCALE_CODE = 'de_DE' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE en_GB ON en_GB.TARGET_ID = ids.ID AND en_GB.LOCALE_CODE = 'en_GB' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE es_ES ON es_ES.TARGET_ID = ids.ID AND es_ES.LOCALE_CODE = 'es_ES' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE es_MX ON es_MX.TARGET_ID = ids.ID AND es_MX.LOCALE_CODE = 'es_MX' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE fr_CA ON fr_CA.TARGET_ID = ids.ID AND fr_CA.LOCALE_CODE = 'fr_CA' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE fr_FR ON fr_FR.TARGET_ID = ids.ID AND fr_FR.LOCALE_CODE = 'fr_FR' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE hi_IN ON hi_IN.TARGET_ID = ids.ID AND hi_IN.LOCALE_CODE = 'hi_IN' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE it_IT ON it_IT.TARGET_ID = ids.ID AND it_IT.LOCALE_CODE = 'it_IT' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE ja_JP ON ja_JP.TARGET_ID = ids.ID AND ja_JP.LOCALE_CODE = 'ja_JP' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE nl_NL ON nl_NL.TARGET_ID = ids.ID AND nl_NL.LOCALE_CODE = 'nl_NL' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE pt_BR ON pt_BR.TARGET_ID = ids.ID AND pt_BR.LOCALE_CODE = 'pt_BR' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE pt_PT ON pt_PT.TARGET_ID = ids.ID AND pt_PT.LOCALE_CODE = 'pt_PT' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE ru_RU ON ru_RU.TARGET_ID = ids.ID AND ru_RU.LOCALE_CODE = 'ru_RU' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE zh_CN ON zh_CN.TARGET_ID = ids.ID AND zh_CN.LOCALE_CODE = 'zh_CN' " +
            "LEFT JOIN component.TRANSLATABLE_PHRASE zh_TW ON zh_TW.TARGET_ID = ids.ID AND zh_TW.LOCALE_CODE = 'zh_TW' ";

    @Override
    public List<Map<String, Object>> getPlatformTranslatableContent() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(KEY_NAMES_SQL_PARAM, platformContentKeyNames);

        return namedParameterJdbcTemplate.query(
                    PLATFORM_CONTENT_QUERY, params, new CamelCaseMapRowMapper());
    }
}
