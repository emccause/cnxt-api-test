package com.maritz.culturenext.content.dao;

import java.util.List;
import java.util.Map;

public interface TranslatableContentDao {
    List<Map<String, Object>> getPlatformTranslatableContent();
}
