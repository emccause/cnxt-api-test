package com.maritz.culturenext.content.services;

import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.content.dto.TranslatableContentDTO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface TranslatableContentService {

    /**
     * TODO javadocs
     * 
     * @param targetType
     * @return
     */
    List<TranslatableContentDTO> getTranslatableContent(String targetType);
    
    /**
     * Return a list of translatable content along with their existing translations
     * 
     * @param response
     * @param targetType - Table where the original data is stored
     * @param platformOnly - True/False to return client entered content or platform-only content.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/25002005/Get+Translatable-Content
     */
    void getTranslatableContentCSV(HttpServletResponse response, String targetType, boolean platformOnly);
    
    /**
     * Accept a CSV of translations and upload them into the database, mapping the
     * translations to the original database values.
     * 
     * @param content
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/25002003/Update+Translatable+Content
     */
    List<TranslatableContentDTO> updateTranslatableContent(List<TranslatableContentDTO> content);
    
    /**
     * TODO javadocs
     * 
     * @param content
     * @return
     */
    Void updateTranslatableContentCSV(MultipartFile content);

}
