package com.maritz.culturenext.content.services.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.CalendarEntry;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.content.dto.TranslatableContentDTO;
import com.maritz.culturenext.content.services.TranslatableContentService;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class TranslatableContentServiceImpl implements TranslatableContentService {
    
    @Inject private ConcentrixDao<ApplicationData> applicationDataDao;
    @Inject private ConcentrixDao<CalendarEntry> calendarEntryDao;
    @Inject private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Inject private ConcentrixDao<Lookup> lookupDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Inject private TranslationUtil translationUtil;
    @Inject private Environment environment;
    @Inject private Security security;

    private static final String TARGET_TYPE = "targetType";
    private static final String INVALID_TARGET_TYPE = "INVALID_TARGET_TYPE";
    private static final String INVALID_TARGET_TYPE_MSG = "The target type does not exist";
    private static final String FIELD = "field";
    private static final String INVALID_FIELD = "INVALID_FIELD";
    private static final String INVALID_FIELD_MSG = "The field does not exist";
    private static final String TARGET_ID = "targetId";
    private static final String INVALID_TARGET_ID = "INVALID_TARGET_ID";
    private static final String INVALID_TARGET_ID_MSG = "The target id does not exist";
    private static final String INPUT_FILE = "inputFile";
    private static final String INVALID_INPUT_FILE = "INVALID_INPUT_FILE";
    private static final String INVALID_INPUT_FILE_MSG = "The input file is invalid";
    
    //Headers for platform-content CSV
    private static final String TABLE_NAME_HEADER = "TABLE_NAME";
    private static final String KEY_HEADER = "KEY";
    private static final String EN_US_VALUE = "enUs";
    private static final String DE_DE_VALUE = "deDe";
    private static final String EN_GB_VALUE = "enGb";
    private static final String ES_ES_VALUE = "esEs";
    private static final String ES_MX_VALUE = "exMx";
    private static final String FR_CA_VALUE = "frCa";
    private static final String FR_FR_VALUE = "frFr";
    private static final String HI_IN_VALUE = "hiIn";
    private static final String IT_IT_VALUE = "itIt";
    private static final String JA_JP_VALUE = "jaJp";
    private static final String NL_NL_VALUE = "nlNl";
    private static final String PT_BR_VALUE = "ptBr";
    private static final String PT_PT_VALUE = "ptPt";
    private static final String RU_RU_VALUE = "ruRu";
    private static final String ZH_CN_VALUE = "zhCn";
    private static final String ZH_TW_VALUE = "zhTw";

    private static final CellProcessor[] PROCESSORS_STUB = new CellProcessor[] { 
            new NotNull(), // Target Type
            new NotNull(), // Target ID
            new NotNull() // Target Name
    };

    private static final String TARGET_TYPE_CSV_HEADER = "Target Type";
    private static final String TARGET_ID_CSV_HEADER = "Target ID";
    private static final String TARGET_NAME_CSV_HEADER = "Target Name";

    private static final String[] HEADERS_STUB = { TARGET_TYPE_CSV_HEADER, TARGET_ID_CSV_HEADER,
            TARGET_NAME_CSV_HEADER };

    private static final String CSV_FILENAME = "translatable-content";
    private static final String CSV_PLATFORM_ONLY_FILENAME = "translatable-platform-content";

    private static final String UTF_8 = "UTF-8";
    private static final String NEWLINE_SEPARATOR_REGEX = "\\r?\\n|\\r";
    private static final String COMMA_SEPARATOR = ",";
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public List<TranslatableContentDTO> getTranslatableContent(String targetType) {
        return translationUtil.getAllTranslatableContent(targetType);
    }

    @Override
    public void getTranslatableContentCSV(HttpServletResponse response, String targetType, boolean platformOnly) {

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("TranslatableContentServiceImpl.getTranslatableContentCSV has been called with the following params. " +
                            "status {} targetType {} platformOnly {} paxId {}"
                    , response.getStatus(),targetType,platformOnly,security.getPaxId());
        }
        //This CSV will be formatted a bit differently
        if (platformOnly) {
            downloadPlatformOnlyCsv(response, translationUtil.getPlatformTranslatableContent());
            return;
        }
        
        // Fetch content
        List<TranslatableContentDTO> content = getTranslatableContent(targetType);
        
        // CSV processing
        List<Map<String, Object>> contentMap = transformDTOListToMap(content);
        List<String> languages = determineTranslationHeaders();
        String[] headers = buildWriterHeaders(HEADERS_STUB, languages);
        CellProcessor[] processors = buildWriterProcessors(PROCESSORS_STUB, languages);
        String[] fieldMapping = buildWriterFieldMapping(languages);
        fillDefaultLanguageContent(contentMap); 

        CsvFileUtil.writeDownloadCSVFileFromMap(response, CSV_FILENAME, contentMap, processors, headers, fieldMapping);

        if(reportsLoggingEnabled) {
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("TranslatableContentServiceImpl.getTranslatableContentCSV has been executed in {} MILLISECONDS with the following params. " +
                            "status {} fileName {} paxId {}"
                    , estimatedTime,response.getStatus(),CSV_FILENAME,security.getPaxId());
        }
    }
    
    /**
     * Create CSV headers, fieldMapping and processor for the platform translatable content result set.
     * Then call the CsvFileUtil to return the result set in CSV format
     * @param response - tells the service to respond with a CSV file
     * @param List<Map<String, Object>> resultSet - data returned that needs to be transformed into CSV 
     */
    private void downloadPlatformOnlyCsv(HttpServletResponse response, List<Map<String, Object>> resultSet) {

        //Set up the headers, fields, and processors
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        csvColumnsProps.add(new CsvColumnPropertiesDTO(TABLE_NAME_HEADER,
                ProjectConstants.TABLE_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(KEY_HEADER,
                ProjectConstants.KEY, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.en_US.name(),
                EN_US_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.de_DE.name(),
                DE_DE_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.en_GB.name(),
                EN_GB_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.es_ES.name(),
                ES_ES_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.es_MX.name(),
                ES_MX_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.fr_CA.name(),
                FR_CA_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.fr_FR.name(),
                FR_FR_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.hi_IN.name(),
                HI_IN_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.it_IT.name(),
                IT_IT_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.ja_JP.name(),
                JA_JP_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.nl_NL.name(),
                NL_NL_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.pt_BR.name(),
                PT_BR_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.pt_PT.name(),
                PT_PT_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.ru_RU.name(),
                RU_RU_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.zh_CN.name(),
                ZH_CN_VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(LocaleCodeEnum.zh_TW.name(),
                ZH_TW_VALUE, null));

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("TranslatableContentServiceImpl.downloadPlatformOnlyCsv has been called with the following params. " +
                            "status {} paxId {}"
                    , response.getStatus(),security.getPaxId());
        }
        // Create CSV file
        CsvFileUtil.writeDownloadCSVFileFromMap(
                response, CSV_PLATFORM_ONLY_FILENAME, resultSet, csvColumnsProps);

        if(reportsLoggingEnabled) {
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("TranslatableContentServiceImpl.downloadPlatformOnlyCsv has been executed in {} MILLISECONDS with the following params. " +
                            "status {} paxId {}"
                    , estimatedTime,response.getStatus(),security.getPaxId());
        }

    }

    @Override
    public List<TranslatableContentDTO> updateTranslatableContent(List<TranslatableContentDTO> content) {
        // Validation
        List<ErrorMessage> errors = validateTranslatableContent(content);
        ErrorMessageException.throwIfHasErrors(errors);

        translationUtil.updateTranslatableContent(content);

        return getTranslatableContent(null);
    }

    @Override
    public Void updateTranslatableContentCSV(MultipartFile content) {
        // Process CSV
        Reader reader;
        String fileContent;

        try {
            ByteArrayInputStream stream = new ByteArrayInputStream(content.getBytes());
            fileContent = IOUtils.toString(stream, UTF_8);
            reader = new StringReader(fileContent);
        } catch (IOException e) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setField(INPUT_FILE)
                    .setCode(INVALID_INPUT_FILE)
                    .setMessage(INVALID_INPUT_FILE_MSG));
        }

        List<String> headerList = extractHeaders(fileContent);
        CellProcessor[] processors = buildReaderProcessors(headerList);
        String[] fieldMapping = buildReaderFieldMapping(headerList);
        List<Map<String, Object>> contentMap = CsvFileUtil.readCSVFILEIntoMap(reader, processors, fieldMapping);
        List<TranslatableContentDTO> contentDTOList = transformMapToDTOList(contentMap);

        // Call update
        updateTranslatableContent(contentDTOList);

        return null;
    }

    private List<ErrorMessage> validateTranslatableContent(List<TranslatableContentDTO> content) {
        List<ErrorMessage> errors = new ArrayList<>();

        for (TranslatableContentDTO contentDTO : content) {
            if (StringUtils.isBlank(contentDTO.getTargetType())) {
                errors.add(new ErrorMessage()
                        .setField(TARGET_TYPE)
                        .setCode(INVALID_TARGET_TYPE)
                        .setMessage(INVALID_TARGET_TYPE_MSG));
            }
            if (StringUtils.isBlank(contentDTO.getField())) {
                errors.add(new ErrorMessage()
                        .setField(FIELD)
                        .setCode(INVALID_FIELD)
                        .setMessage(INVALID_FIELD_MSG));
            }
            if (contentDTO.getTargetId() == null) {
                errors.add(new ErrorMessage()
                        .setField(TARGET_ID)
                        .setCode(INVALID_TARGET_ID)
                        .setMessage(INVALID_TARGET_ID_MSG));
            }
        }

        return errors;
    }

    private List<Map<String, Object>> transformDTOListToMap(List<TranslatableContentDTO> contentList) {
        List<Map<String, Object>> transformedContent = new ArrayList<>();

        for (TranslatableContentDTO content : contentList) {
            Map<String, Object> contentMap = new HashMap<>();
            contentMap.put(TARGET_TYPE, content.getTargetType());
            contentMap.put(TARGET_ID, content.getTargetId());
            contentMap.put(FIELD, content.getField());
            for (String languageCode : content.getTranslations().keySet()) {
                contentMap.put(languageCode, content.getTranslations().get(languageCode));
            }
            transformedContent.add(contentMap);
        }

        return transformedContent;
    }

    private List<TranslatableContentDTO> transformMapToDTOList(List<Map<String, Object>> contentMapList) {
        List<TranslatableContentDTO> transformedContent = new ArrayList<>();

        for (Map<String, Object> contentMap : contentMapList) {
            TranslatableContentDTO content = new TranslatableContentDTO();
            for (String key : contentMap.keySet()) {
                if (TARGET_TYPE.equals(key)) {
                    content.setTargetType((String) contentMap.get(key));
                } else if (TARGET_ID.equals(key)) {
                    content.setTargetId(Long.parseLong((String) contentMap.get(key)));
                } else if (FIELD.equals(key)) {
                    content.setField((String) contentMap.get(key));
                } else if (contentMap.get(key) != null) {
                    content.getTranslations().put(key, (String) contentMap.get(key));
                }
            }
            transformedContent.add(content);
        }

        return transformedContent;
    }

    private List<String> determineTranslationHeaders() {
        return localeInfoDao.findBy()
                .where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find(ProjectConstants.LOCALE_CODE, String.class);
    }

    private String[] buildWriterHeaders(String[] headerStub, List<String> languages) {
        String[] headerArray = new String[headerStub.length + languages.size()];
        for (int i = 0; i < headerStub.length; i++) {
            headerArray[i] = headerStub[i];
        }

        for (int i = 0; i < languages.size(); i++) {
            headerArray[i + headerStub.length] = languages.get(i);
        }

        return headerArray;
    }

    private CellProcessor[] buildWriterProcessors(CellProcessor[] processorStub, List<String> languages) {
        CellProcessor[] processorArray = new CellProcessor[processorStub.length + languages.size()];
        for (int i = 0; i < processorStub.length; i++) {
            processorArray[i] = processorStub[i];
        }

        for (int i = 0; i < languages.size(); i++) {
            processorArray[i + processorStub.length] = null;
        }

        return processorArray;
    }

    private String[] buildWriterFieldMapping(List<String> languages) {
        String[] fieldMapping = new String[HEADERS_STUB.length + languages.size()];
        fieldMapping[0] = TARGET_TYPE;
        fieldMapping[1] = TARGET_ID;
        fieldMapping[2] = FIELD;
        for (int i = 0; i < languages.size(); i++) {
            fieldMapping[HEADERS_STUB.length + i] = languages.get(i);
        }

        return fieldMapping;
    }

    private void fillDefaultLanguageContent(List<Map<String, Object>> contentMap) {
        // Loop each piece, get ids for each table
        Map<String, List<Long>> tableNameToIdListMap = new HashMap<>();
        for (String tableName : TranslationConstants.TABLES) {
            tableNameToIdListMap.put(tableName, new ArrayList<Long>());
        }

        for (Map<String, Object> content : contentMap) {
            tableNameToIdListMap.get(content.get(TARGET_TYPE)).add((Long) content.get(TARGET_ID));
        }

        // Fetch the relevant data
        List<ApplicationData> applicationDataList = applicationDataDao
                .findById(tableNameToIdListMap.get(TableName.APPLICATION_DATA.name()));
        List<RecognitionCriteria> recognitionCriteriaList = recognitionCriteriaDao
                .findById(tableNameToIdListMap.get(TableName.RECOGNITION_CRITERIA.name()));
        List<Program> programList = programDao.findById(tableNameToIdListMap.get(TableName.PROGRAM.name()));
        List<CalendarEntry> calendarEntryList = calendarEntryDao
                .findById(tableNameToIdListMap.get(TableName.CALENDAR_ENTRY.name()));
        List<Lookup> lookupList = lookupDao.findById(tableNameToIdListMap.get(TableName.LOOKUP.name()));

        // Convert to id -> Object maps
        Map<Long, ApplicationData> applicationDataMap = new HashMap<>();
        Map<Long, RecognitionCriteria> recognitionCriteriaMap = new HashMap<>();
        Map<Long, Program> programMap = new HashMap<>();
        Map<Long, CalendarEntry> calendarEntryMap = new HashMap<>();
        Map<Long, Lookup> lookupMap = new HashMap<>();

        for (ApplicationData applicationData : applicationDataList) {
            applicationDataMap.put(applicationData.getApplicationDataId(), applicationData);
        }
        for (RecognitionCriteria recognitionCriteria : recognitionCriteriaList) {
            recognitionCriteriaMap.put(recognitionCriteria.getId(), recognitionCriteria);
        }
        for (Program program : programList) {
            programMap.put(program.getProgramId(), program);
        }
        for (CalendarEntry calendarEntry : calendarEntryList) {
            calendarEntryMap.put(calendarEntry.getId(), calendarEntry);
        }
        for (Lookup lookup : lookupList) {
            lookupMap.put(lookup.getId(), lookup);
        }

        // Insert relevant data into contentMap for default language
        for (Map<String, Object> content : contentMap) {
            if (TableName.APPLICATION_DATA.name().equals(content.get(TARGET_TYPE))) {
                ApplicationData applicationData = applicationDataMap.get(content.get(TARGET_ID));
                if (applicationData != null) {
                    content.put(ProjectConstants.DEFAULT_LOCALE_CODE, applicationData.getValue());
                }
            } else if (TableName.RECOGNITION_CRITERIA.name().equals(content.get(TARGET_TYPE))) {
                RecognitionCriteria recognitionCriteria = recognitionCriteriaMap.get(content.get(TARGET_ID));
                if (recognitionCriteria != null) {
                    if (TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_NAME.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, recognitionCriteria.getRecognitionCriteriaName());
                    } else if (TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_DESC.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, recognitionCriteria.getRecognitionCriteriaDesc());
                    }
                }
            } else if (TableName.PROGRAM.name().equals(content.get(TARGET_TYPE))) {
                Program program = programMap.get(content.get(TARGET_ID));
                if (program != null) {
                    if (TranslationConstants.COLUMN_NAME_PROGRAM_NAME.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, program.getProgramName());
                    } else if (TranslationConstants.COLUMN_NAME_PROGRAM_DESC.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, program.getProgramDesc());
                    } else if (TranslationConstants.COLUMN_NAME_PROGRAM_LONG_DESC.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, program.getProgramLongDesc());
                    }
                }
            } else if (TableName.CALENDAR_ENTRY.name().equals(content.get(TARGET_TYPE))) {
                CalendarEntry calendarEntry = calendarEntryMap.get(content.get(TARGET_ID));
                if (calendarEntry != null) {
                    if (TranslationConstants.COLUMN_NAME_CALENDAR_ENTRY_NAME.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, calendarEntry.getCalendarEntryName());
                    } else if (TranslationConstants.COLUMN_NAME_CALENDAR_ENTRY_DESC.equals(content.get(FIELD))) {
                        content.put(ProjectConstants.DEFAULT_LOCALE_CODE, calendarEntry.getCalendarEntryDesc());
                    }
                }
            } else if (TableName.LOOKUP.name().equals(content.get(TARGET_TYPE))) {
                Lookup lookup = lookupMap.get(content.get(TARGET_ID));
                if (lookup != null) {
                    content.put(ProjectConstants.DEFAULT_LOCALE_CODE, lookup.getLookupDesc());
                }
            }
        }

    }

    private List<String> extractHeaders(String fileContent) {
        // look for \r\n or \n or \r for splitting newlines
        String[] rows = fileContent.split(NEWLINE_SEPARATOR_REGEX);
        String headerRow = rows[0];

        String[] headers = headerRow.split(COMMA_SEPARATOR);
        return Arrays.asList(headers);
    }

    private CellProcessor[] buildReaderProcessors(List<String> headers) {
        CellProcessor[] processors = new CellProcessor[headers.size()];

        for (int i = 0; i < PROCESSORS_STUB.length; i++) {
            processors[i] = PROCESSORS_STUB[i];
        }
        for (int i = PROCESSORS_STUB.length; i < headers.size(); i++) {
            processors[i] = null;
        }

        return processors;
    }

    private String[] buildReaderFieldMapping(List<String> headers) {
        String[] fieldMapping = new String[headers.size()];

        for (int i = 0; i < headers.size(); i++) {
            switch (headers.get(i)) {
                case TARGET_TYPE_CSV_HEADER:
                    fieldMapping[i] = TARGET_TYPE;
                    break;
                case TARGET_ID_CSV_HEADER:
                    fieldMapping[i] = TARGET_ID;
                    break;
                case TARGET_NAME_CSV_HEADER:
                    fieldMapping[i] = FIELD;
                    break;
                default:
                    fieldMapping[i] = headers.get(i);
            }
        }

        return fieldMapping;
    }
}
