package com.maritz.culturenext.content.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.content.services.TranslatableContentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("translatable-content")
@Api(value = "/translatable-content", description = "Endpoints for translatable content processes")
public class TranslatableContentRestService {

    @Inject private TranslatableContentService translatableContentService;

    private static final String SUPPORTED_CONTENT_TYPE = "text/csv";
    private static final String SUPPORTED_CONTENT_TYPE2 = "application/vnd.ms-excel";

    //rest/translatable-content
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.GET, headers = "content-type=application/csv")
    @ApiOperation(
            value = "Returns all configured translations that currently exist in the database project-wide "
                    + "in CSV format")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the translation data") })
    @Permission("PUBLIC")
    public void getTranslatableContentCSV(HttpServletResponse response,
            @ApiParam(value = "Optional table to filter by") 
                @RequestParam(value = TARGET_TYPE_REST_PARAM, required = false) String targetType,
            @ApiParam(value = "Boolean to specify if only platform-specific content is needed")
                @RequestParam(value = PLATFORM_ONLY_REST_PARAM, required = false) boolean platformOnly)
            throws Throwable {

        translatableContentService.getTranslatableContentCSV(response, targetType, platformOnly);
    }

    //rest/translatable-content
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Accepts a CSV file and applies the contained updates to the database")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the translation data") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> updateTranslatableContentCSV(
            @ApiParam(
                    value = "CSV file with translations to add or update")
                @RequestParam("translatable-content") MultipartFile content)
            throws Throwable {

        if (!((SUPPORTED_CONTENT_TYPE.equals(content.getContentType().toLowerCase())
                || (SUPPORTED_CONTENT_TYPE2.equals(content.getContentType().toLowerCase()))))) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<Object>(translatableContentService.updateTranslatableContentCSV(content),
                HttpStatus.OK);
    }

}
