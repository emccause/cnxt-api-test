package com.maritz.culturenext.content.dto;

public class TranslatableContentExtendedDTO extends TranslatableContentDTO {

    private Long translatableId;

    public Long getTranslatableId() {
        return translatableId;
    }

    public void setTranslatableId(Long translatableId) {
        this.translatableId = translatableId;
    }
}
