package com.maritz.culturenext.config;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maritz.batch.annotation.StepBean;
import com.maritz.batch.policy.UnlimitedSkipPolicy;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.culturenext.pointupload.PointUploadJobLauncher;
import com.maritz.culturenext.pointupload.dto.DiscretionaryExtended;
import com.maritz.culturenext.pointupload.processor.PointUploadProcessor;
import com.maritz.culturenext.pointupload.writer.PointUploadWriter;

@Configuration
public class PointUploadJobConfiguration {

    @Inject DataSource dataSource;
    @Inject JobBuilderFactory jobBuilderFactory;
    @Inject StepBuilderFactory stepBuilderFactory;

    private final String SQL_READER =
        "SELECT ID, BATCH_ID, FILE_UPLOAD "
        + "FROM BATCH_FILE "
        + "WHERE BATCH_ID IN ( "
        + "SELECT BATCH_ID "
        + "FROM BATCH "
        + "WHERE "
        + "BATCH_TYPE_CODE = 'CPTD' "
        + "AND "
        + "STATUS_TYPE_CODE = 'NEW');";


    @Bean
    public PointUploadJobLauncher pointUploadJobLauncher() {
        return new PointUploadJobLauncher();
    }

    @Bean
    public Job pointUploadJob() {
        return jobBuilderFactory.get("pointUploadJob")
            .incrementer(new RunIdIncrementer())
            .preventRestart()
            .flow(loadBatchFileStep())
            .end()
            .build();
    }

    @Bean
    protected Step loadBatchFileStep() {
        return stepBuilderFactory.get("loadBatchFileStep")
            .<BatchFile, List<DiscretionaryExtended>>chunk(1)
            .reader(pointUploadReader())
            .processor(pointUploadProcessor())
            .writer(pointUploadWriter())
            .faultTolerant()
            .skipPolicy(new UnlimitedSkipPolicy())
            .build();
    }

    @StepBean
    public JdbcCursorItemReader<BatchFile> pointUploadReader() {
        JdbcCursorItemReader<BatchFile> dbPointUploadItemReader = new JdbcCursorItemReader<>();
        dbPointUploadItemReader.setDataSource(dataSource);
        dbPointUploadItemReader.setSql(SQL_READER);
        dbPointUploadItemReader.setRowMapper(new ConcentrixBeanRowMapper<BatchFile>()
            .setBeanClass(BatchFile.class)
            .build()
        );
        return dbPointUploadItemReader;
    }

    @StepBean
    public PointUploadProcessor pointUploadProcessor() {
        return new PointUploadProcessor();
    }

    @StepBean
    public PointUploadWriter pointUploadWriter() {
        return new PointUploadWriter();
    }

}
