package com.maritz.culturenext.config;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;

import com.maritz.core.bean.Registry;
import com.maritz.core.config.support.UrlAccess;
import com.maritz.core.security.authentication.impersonate.ImpersonateService;
import com.maritz.core.security.authentication.token.IssuerSigningKeyResolver;
import com.maritz.core.security.authentication.token.JsonWebTokenService;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.culturenext.security.authentication.impersonate.ProxyImpersonateServiceImpl;

import io.jsonwebtoken.SigningKeyResolver;

@Configuration
public class CultureNextSecurityConfiguration {

    @Bean(name= {"proxyImpersonateService", "impersonateService"})
    public ImpersonateService proxyImpersonateService() {
        return new ProxyImpersonateServiceImpl();
    }

    @Bean
    public TokenService rideauTokenService() {
        return new JsonWebTokenService("rideau", rideauSigningKeyResolver());
    }

    @Bean
    public SigningKeyResolver rideauSigningKeyResolver() {
        return new IssuerSigningKeyResolver()
            .setKeyType("rideau")
        ;
    }

    @Inject
    public void addSsoSimulatorUrlsToBasicAuthRegistry(@Named("basicAuthUrlAccessRegistry") Registry<UrlAccess> basicAuthUrlAccessRegistry) {
        basicAuthUrlAccessRegistry
            .add(new UrlAccess().setAntPattern("/rest/sso-simulator.html").setMethod(HttpMethod.GET))
            .add(new UrlAccess().setAntPattern("/rest/js/jquery-2.1.4.min.js").setMethod(HttpMethod.GET).permitAll())
        ;
    }

    @Bean
    public SigningKeyResolver surveyApiSigningKeyResolver() {
        return new IssuerSigningKeyResolver()
            .setKeyType("survey-api")
        ;
    }

    @Bean
    public TokenService surveyApiTokenService() {
        return new JsonWebTokenService("survey-api", surveyApiSigningKeyResolver());
    }

}
