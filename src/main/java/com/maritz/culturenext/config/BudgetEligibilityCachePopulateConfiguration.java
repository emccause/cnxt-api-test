package com.maritz.culturenext.config;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;

@Configuration
@DependsOn({"dataSourceConfiguration"})
public class BudgetEligibilityCachePopulateConfiguration {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;

    public BudgetEligibilityCachePopulateConfiguration() {
        logger.info("Start");
    }

    @PostConstruct
    public void populateCache() {
        budgetEligibilityCacheService.buildCache();
    }

}
