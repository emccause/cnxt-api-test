package com.maritz.culturenext.config;

import javax.inject.Inject;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maritz.batch.annotation.StepBean;
import com.maritz.culturenext.enrollment.batch.WorkdayEnrollmentJobLauncher;
import com.maritz.culturenext.enrollment.batch.processor.WorkdayEmployeeToStageEnrollmentProcessor;
import com.maritz.culturenext.enrollment.batch.reader.WorkdayEmployeeServiceReader;
import com.maritz.culturenext.enrollment.batch.writer.StageEnrollmentWriter;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.workday.dto.EmployeeDTO;

@Configuration
public class WorkdayEnrollmentJobConfiguration {

    @Inject JobBuilderFactory jobBuilderFactory;
    @Inject StepBuilderFactory stepBuilderFactory;
    @Inject StageEnrollmentWriter stageEnrollmentWriter;

    @Bean
    public WorkdayEnrollmentJobLauncher workdayEnrollmentJobLauncher() {
        return new WorkdayEnrollmentJobLauncher();
    }

    @Bean
    public Job workdayEnrollmentJob() {
        return jobBuilderFactory.get("workdayEnrollmentJob")
            .flow(workdayEnrollmentStep())
            .end()
            .build()
        ;
     }

    @Bean
    protected Step workdayEnrollmentStep() {
        return stepBuilderFactory.get("workdayEnrollmentStep")
            .<EmployeeDTO, StageEnrollmentDTO>chunk(1000)
            .reader(workdayEmployeeServiceReader())
            .processor(workdayEmployeeToStageEnrollmentProcessor())
            .writer(stageEnrollmentWriter)
            .build()
        ;
    }

    @StepBean
    public WorkdayEmployeeServiceReader workdayEmployeeServiceReader() {
        return new WorkdayEmployeeServiceReader();
    }

    @StepBean
    public WorkdayEmployeeToStageEnrollmentProcessor workdayEmployeeToStageEnrollmentProcessor() {
        return new WorkdayEmployeeToStageEnrollmentProcessor();
    }

}