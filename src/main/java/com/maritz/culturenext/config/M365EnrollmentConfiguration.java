package com.maritz.culturenext.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maritz.culturenext.enrollment.dao.ProcessEnrollmentDao;
import com.maritz.culturenext.enrollment.dao.impl.ProcessEnrollmentDaoImpl;


@Configuration
public class M365EnrollmentConfiguration {

    @Bean
    public ProcessEnrollmentDao processEnrollmentDao() {
        return new ProcessEnrollmentDaoImpl();
    }
}