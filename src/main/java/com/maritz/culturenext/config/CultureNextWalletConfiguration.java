package com.maritz.culturenext.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maritz.wallet.dao.WalletItemsDao;
import com.maritz.wallet.dao.WalletReportDao;
import com.maritz.wallet.dao.impl.MYPTDWalletItemsDao;
import com.maritz.wallet.dao.impl.ProgramWalletReportDao;

@Configuration
public class CultureNextWalletConfiguration {

    @Bean(name = {"myptdWalletItemsDao", "walletItemsDao"})
    public WalletItemsDao myptdWalletItemsDao() {
        return new MYPTDWalletItemsDao();
    }

    @Bean(name = {"cnxtWalletReportDao", "walletReportDao"})
    public WalletReportDao cnxtWalletReportDao() {
        return new ProgramWalletReportDao();
    }

}
