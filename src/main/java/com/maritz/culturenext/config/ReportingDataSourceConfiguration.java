package com.maritz.culturenext.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;

import com.maritz.core.config.support.DataSourceBuilder;

@Configuration
public class ReportingDataSourceConfiguration {

    @Bean(destroyMethod = "close")
    public javax.sql.DataSource dataSourceReporting(ConfigurableEnvironment environment) {
        return new DataSourceBuilder(environment, "datasourceReporting")
            .build();
    }
}