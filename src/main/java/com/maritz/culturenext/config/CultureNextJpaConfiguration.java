package com.maritz.culturenext.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.maritz.core.jpa.support.repository.BaseJpaRepositoryImpl;

@Configuration
@EnableJpaRepositories(
    basePackages = "com.maritz.culturenext.jpa.repository",
    entityManagerFactoryRef="entityManagerFactory",
    transactionManagerRef="transactionManager",
    repositoryBaseClass = BaseJpaRepositoryImpl.class
)
public class CultureNextJpaConfiguration {
}