package com.maritz.culturenext.config;

import java.nio.charset.Charset;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maritz.batch.annotation.StepBean;
import com.maritz.batch.reader.DelimitedFileItemReader;
import com.maritz.batch.support.FileDelimiter;
import com.maritz.core.services.FileService;
import com.maritz.culturenext.enrollment.batch.EnrollmentFileJobLauncher;
import com.maritz.culturenext.enrollment.batch.processor.EncryptingStageEnrollmentProcessor;
import com.maritz.culturenext.enrollment.batch.writer.StageEnrollmentWriter;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;

@Configuration
public class EnrollmentFileJobConfiguration {

    @Inject JobBuilderFactory jobBuilderFactory;
    @Inject StepBuilderFactory stepBuilderFactory;
    @Inject FileService inboxFileService;

    @Value("${enrollment.file.columns:}")
    String readerColumnNames;
    
    @Value("${enrollment.characterSet:UTF-8}")
    String characterSet;

    @Bean
    public EnrollmentFileJobLauncher enrollmentFileJobLauncher() {
        return new EnrollmentFileJobLauncher()
            .setFileService(inboxFileService)
        ;
    }

    @Bean
    public Job enrollmentFileJob() {
        return jobBuilderFactory.get("enrollmentFileJob")
            .flow(enrollmentFileStep())
            .end()
            .build()
        ;
     }

    @Bean
    protected Step enrollmentFileStep() {
        return stepBuilderFactory.get("enrollmentFileStep")
            .<StageEnrollmentDTO, StageEnrollmentDTO>chunk(1000)
            .reader(enrollmentFileReader())
            .processor(encryptingStageEnrollmentProcessor())
            .writer(stageEnrollmentWriter())
            .build()
        ;
    }

    @StepBean
    public DelimitedFileItemReader<StageEnrollmentDTO> enrollmentFileReader() {
       return (DelimitedFileItemReader<StageEnrollmentDTO>)new DelimitedFileItemReader<StageEnrollmentDTO>()
            .setHasHeader(true)
            .setFileDelimiter(FileDelimiter.CSV)
            .setColumnNames(StringUtils.split(readerColumnNames, ','))
            .setResultType(StageEnrollmentDTO.class)
            .setCharset(Charset.forName(characterSet))
            .setFileService(inboxFileService)
        ;
    }

    @StepBean
    public EncryptingStageEnrollmentProcessor encryptingStageEnrollmentProcessor() {
        return new EncryptingStageEnrollmentProcessor();
    }

    @StepBean
    public StageEnrollmentWriter stageEnrollmentWriter() {
        return new StageEnrollmentWriter();
    }

}