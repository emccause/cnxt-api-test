package com.maritz.culturenext.test.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.PAX_ID_REST_PARAM;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.test.services.UserStatusResetService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("testing/participants")
@Api(value = "/testing", description = "all calls dealing with testing")
public class UserStatusResetRestService {

    @Inject private UserStatusResetService testUserStatusResetService;

    @RequestMapping(value = "{paxId}/reset_status_new", method = RequestMethod.PUT)
    @SecurityPolicy(authenticated=false, profiles="!production")
    @ApiOperation(value = "Sets the sys user status to new for the given paxId")
    public ResponseEntity<Void> updateProfilePreferences(
            @ApiParam(value="The paxId of the user you want to set to new")
            @PathVariable(PAX_ID_REST_PARAM) Pax pax
    ) {
        boolean reset = testUserStatusResetService.resetSysUserToNew(pax.getPaxId());
        return new ResponseEntity<>(reset ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

}
