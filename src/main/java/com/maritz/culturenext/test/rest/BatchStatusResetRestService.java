package com.maritz.culturenext.test.rest;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.test.services.BatchStatusResetService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("testing")
@Api(value = "/testing", description = "all calls dealing with testing")
public class BatchStatusResetRestService {

    @Inject private BatchStatusResetService batchStatusResetService;

    @RequestMapping(value = "enrollment-config/reset-status", method = RequestMethod.PUT)
    //@PreAuthorize("!@profileUtil.isProduction() and !@security.isImpersonated()")
    //@Permission("PUBLIC")
    @SecurityPolicy(authenticated=false, profiles="!production")
    @ApiOperation(value = "takes the batch record for enrollment group processing and switches it from IN_PROGRESS to COMPLETE")
    public ResponseEntity<Void> updateGroupProcessingStatus() {
        boolean reset = batchStatusResetService.updateGroupProcessingStatus();
        return new ResponseEntity<>(reset ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

}
