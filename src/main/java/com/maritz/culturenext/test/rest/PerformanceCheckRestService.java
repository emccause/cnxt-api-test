package com.maritz.culturenext.test.rest;


import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.admin.cache.CacheService;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.test.services.PerformanceCheckService;

@RestController
@RequestMapping("testing")
public class PerformanceCheckRestService {

    @Inject private PerformanceCheckService performanceCheckService;
    @Inject private CacheService cacheService;
    @Inject private CacheManager cacheManager;

    @RequestMapping(value = "check-status", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false, ipRestriction="maritz")
    public String checkJavaStatus(HttpServletRequest request) {
        return performanceCheckService.checkPerformance();
    }

    @RequestMapping(value = "clear-caches", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false, ipRestriction="maritz")
    public void clearAllCaches(HttpServletRequest request) {
        cacheService.clearAllCaches();
    }

    @RequestMapping(value = "clear-cache", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false, ipRestriction="maritz")
    public ResponseEntity<Void> clearCache(
        HttpServletRequest request,
        @RequestParam(value="cacheName", required=true) String cacheName
    ) {
        Cache cache = cacheManager.getCache(cacheName);
        if (cache != null) {
            cache.clear();
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "cache-status", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false, ipRestriction="maritz")
    public String cacheStatus(HttpServletRequest request) {
        return performanceCheckService.checkCacheStatus();
    }

}
