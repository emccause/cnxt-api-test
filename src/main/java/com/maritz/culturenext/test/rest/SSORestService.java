package com.maritz.culturenext.test.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.dto.AuthTokenDTO;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.culturenext.enums.TokenKeyTypeEnum;

import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("testing/simplesso")
@Api(value = "/testing", description = "all calls dealing with testing")
public class SSORestService {

    @Inject private TokenService tokenService;
    @Inject private TokenService rideauTokenService;

    @RequestMapping(value = "token", method = RequestMethod.GET)
    @SecurityPolicy(authenticated=false, ipRestriction="maritz")
    @ApiOperation(value = "Generates an auth token with the supplied information")
    public AuthTokenDTO generateToken(
        @RequestParam("username") String username,
        @RequestParam(value = "issuer", required = false) String issuer,
        @RequestParam(value = "keyType", required = true) String keyType,
        HttpServletRequest request
    ) throws Exception {
        TokenService selectedTokenService = null;

        switch (TokenKeyTypeEnum.valueOf(keyType)) {
        case REST : selectedTokenService = tokenService;
                break;
        case RIDEAU : selectedTokenService = rideauTokenService;
                break;
        }

        Claims claims = selectedTokenService.createClaims();
        claims.setIssuer(issuer);
        claims.setSubject(username);
        String token = selectedTokenService.createToken(claims);
        return new AuthTokenDTO().setAuthToken(token);
    }

}
