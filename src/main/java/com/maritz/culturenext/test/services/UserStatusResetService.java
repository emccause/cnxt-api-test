package com.maritz.culturenext.test.services;

public interface UserStatusResetService {

    /** 
     * TODO javadocs
     * 
     * @param paxId
     * @return
     */
    boolean resetSysUserToNew(Long paxId);
}
