package com.maritz.culturenext.test.services;

public interface PerformanceCheckService {
    
    /**
     * TODO javadocs
     * 
     * @return
     */
    String checkPerformance();
    
    /**
     * TODO javadocs
     * 
     * @return
     */
    String checkCacheStatus();

}
