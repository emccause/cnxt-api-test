package com.maritz.culturenext.test.services.impl;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.MemoryUsage;
import java.lang.management.RuntimeMXBean;
import java.text.NumberFormat;
import java.util.Collection;
import java.util.Iterator;

import javax.inject.Inject;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.statistics.StatisticsGateway;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import com.maritz.culturenext.test.services.PerformanceCheckService;
import com.maritz.culturenext.util.DateUtil;

@Component
public class PerformanceCheckServiceImpl implements PerformanceCheckService {

    @Inject private CacheManager cacheManager;

    @Override
    public String checkPerformance() {
        
        Runtime runtime = Runtime.getRuntime();

        NumberFormat format = NumberFormat.getInstance();

        StringBuilder sb = new StringBuilder();        
        sb.append("TIME: " + DateUtil.getCurrentTimeInUTCDateTime() + "\n");
        
        String name = ManagementFactory.getRuntimeMXBean().getName();
        sb.append("PROCESS NAME: " + name + "\n");
        
        // heap space calculations based on http://stackoverflow.com/questions/3571203/
        //what-is-the-exact-meaning-of-runtime-getruntime-totalmemory-and-freememory
        
        long maxMemory = runtime.maxMemory(); // will be at or less than Xmx - difference between 
                                                //Xmx and maxMemory is JVM specific
        long totalMemory = runtime.totalMemory(); // somewhere between Xms and Xmx
        long freeMemory = runtime.freeMemory(); // of total memory, how much is free.
        long usedMemory = totalMemory - freeMemory; // need to calculate
        long totalFreeMemory = maxMemory - usedMemory; // need to calculate
        sb.append("\nHEAP SPACE: \n");

        sb.append("max memory (based on Xmx): " + format.format((maxMemory / 1024) / 1024) + " MB\n");
        sb.append("current total allocated memory (between Xms and Xmx): " + 
        format.format((totalMemory / 1024) / 1024) + " MB\n");
        sb.append("current free allocated memory: " + format.format((freeMemory / 1024) / 1024) + " MB\n");
        sb.append("used allocated memory: " + format.format((usedMemory / 1024) / 1024) + " MB\n");        
        sb.append("total free memory (max minus used): " + format.format(((totalFreeMemory) / 1024) / 1024) + " MB\n");
        
        MemoryUsage nonHeapMemoryUsage = ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage();
        
        sb.append("\nNON-HEAP SPACE: \n");
        sb.append("init: " + format.format((nonHeapMemoryUsage.getInit() / 1024) / 1024) + " MB \n");
        sb.append("committed: " + format.format((nonHeapMemoryUsage.getCommitted() / 1024) / 1024) + " MB \n");
        sb.append("used: " + format.format((nonHeapMemoryUsage.getUsed() / 1024) / 1024) + " MB \n");
        sb.append("max: " + format.format((nonHeapMemoryUsage.getMax() / 1024) / 1024) + " MB \n");
        
        sb.append("\nMEMORY POOLS: \n");
        
        Iterator<MemoryPoolMXBean> iter = ManagementFactory.getMemoryPoolMXBeans().iterator();
        while(iter.hasNext()){
            MemoryPoolMXBean item = iter.next();
            String beanName = item.getName();

            sb.append("\n" + beanName + ": \n");
            MemoryUsage usage = item.getUsage();
            MemoryUsage peak = item.getPeakUsage();
            if(usage != null){
                sb.append("current committed memory: " + format.format((usage.getCommitted() / 1024) / 1024) +
                        " MB\n");
                sb.append("current max memory: " + format.format((usage.getMax() / 1024) / 1024) + " MB\n");
                sb.append("current used memory: " + format.format((usage.getUsed() / 1024) / 1024) + " MB\n");    
            }
            if(peak != null){
                sb.append("peak committed memory: " + format.format((peak.getCommitted() / 1024) / 1024) +
                        " MB\n");
                sb.append("peak max memory: " + format.format((peak.getMax() / 1024) / 1024) + " MB\n");
                sb.append("peak used memory: " + format.format((peak.getUsed() / 1024) / 1024) + " MB\n");    
            }
        }
        
        com.sun.management.OperatingSystemMXBean operatingSystemMXBean =
                (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
        int availableProcessors = operatingSystemMXBean.getAvailableProcessors();
        long prevUpTime = runtimeMXBean.getUptime();
        long prevProcessCpuTime = operatingSystemMXBean.getProcessCpuTime();
        double cpuUsage;
        try
        {
            Thread.sleep(1000);
        }
        catch (Exception ignored) { }

        operatingSystemMXBean = (com.sun.management.OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        long upTime = runtimeMXBean.getUptime();
        long processCpuTime = operatingSystemMXBean.getProcessCpuTime();
        long elapsedCpu = processCpuTime - prevProcessCpuTime;
        long elapsedTime = upTime - prevUpTime;

        cpuUsage = Math.min(99F, elapsedCpu / (elapsedTime * 10000F * availableProcessors));

        sb.append("\nCPU: \n");
        sb.append("usage: " + cpuUsage + "%\n");
            
        int totalThreads =  Thread.getAllStackTraces().keySet().size();
            
        int runningThreads = 0;
        for (Thread t : Thread.getAllStackTraces().keySet()) {
            if (t.getState()==Thread.State.RUNNABLE) runningThreads++;
        }

        sb.append("\nTHREADS: \n");
        sb.append("total: " + totalThreads + "\n");
        sb.append("running: " + runningThreads);
        
        return sb.toString();
    }

    @Override
    public String checkCacheStatus() {
        
        NumberFormat format = NumberFormat.getInstance();

        Collection<String> cacheNames = cacheManager.getCacheNames();
        StringBuilder sb = new StringBuilder();
        for (String cacheName : cacheNames) {
            sb.append(cacheName + " - local heap size: ");
            Cache cache = cacheManager.getCache(cacheName);
            Object cacheObject = cache.getNativeCache();
            if (cacheObject instanceof Ehcache) {
                Ehcache ehCache = (Ehcache) cacheObject;
                StatisticsGateway stats = ehCache.getStatistics();
                sb.append(format.format(stats.getLocalHeapSizeInBytes()) + " bytes");
            }
            else {
                sb.append("N/A");
            }
            sb.append("\n");
        }
        
        return sb.toString();
    }

}
