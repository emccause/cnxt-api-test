package com.maritz.culturenext.test.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.admin.cache.CacheService;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.culturenext.test.services.UserStatusResetService;

@Component
public class UserStatusResetServiceImpl implements UserStatusResetService {
    
    @Inject private CacheService cacheService;
    @Inject private SysUserRepository sysUserRepository;

    private static final String NEW_USER_STATUS = "NEW";

    @Override
    public boolean resetSysUserToNew(Long paxId) {
        List<SysUser> sysUsers = sysUserRepository.findByPaxId(paxId);
        if (sysUsers.isEmpty()) return false;
        
        for (SysUser sysUser : sysUsers) { 
            sysUser.setStatusTypeCode(NEW_USER_STATUS);
            sysUserRepository.save(sysUser);
        }
        
        cacheService.clearCachesForPaxId(paxId);
        
        return true;
    }

}
