package com.maritz.culturenext.test.services.impl;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.culturenext.test.services.BatchStatusResetService;

@Component
public class BatchStatusResetServiceImpl implements BatchStatusResetService {
    
    private static final String SUCCESS = "SUCCESS";
    private static final String PROCESS_GROUPS = "GRPPRC";
    private static final String BATCH_TYPE = "batchTypeCode";
    private static final String CREATE_DATE = "createDate";
    
    @Inject private ConcentrixDao<Batch> batchDao;

    @Override
    public boolean updateGroupProcessingStatus() {
        Batch groupProcessingBatch = batchDao.findBy()
            .where(BATCH_TYPE).eq(PROCESS_GROUPS)
            .desc(CREATE_DATE)
            .findOne();
        
        if (groupProcessingBatch == null) return false;
        
        groupProcessingBatch.setStatusTypeCode(SUCCESS);
        batchDao.save(groupProcessingBatch);
        
        return true;
    }

}
