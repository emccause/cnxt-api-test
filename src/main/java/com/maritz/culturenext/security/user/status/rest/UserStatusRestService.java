package com.maritz.culturenext.security.user.status.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.security.user.status.dto.UserStatusDTO;
import com.maritz.culturenext.security.user.status.services.UserStatusService;

@RestController
@RequestMapping("users")
@Api(value = "/user-statuses", description = "Endpoint for user status related processes")
public class UserStatusRestService {

    @Inject private Security security;
    @Inject private UserStatusService userStatusService;

    //rest/users/~/unlock
    @PreAuthorize("@security.hasRole('MTZ:ADM', 'CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "{sysUserId}/unlock", method = RequestMethod.PUT)
    @ApiOperation(value="Admin call only, Unlocks a locked user for a given SysUserId")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully unlocked the given user.", 
    response = DetailProfileInfo.class) })
    @Permission("PUBLIC")
    public DetailProfileInfo unlockUserBySysUserId ( 
            @ApiParam(value = "System User Id that you wish to unlock") 
            @PathVariable(SYS_USER_ID_REST_PARAM) String sysUserIdString) {
        Long sysUserId = security.getSysUserId(sysUserIdString);        
        
            return userStatusService.unlockUserBySysUserId(sysUserId);
    }
    
    //rest/users/~/status
    @PreAuthorize("(@security.hasRole('MTZ:ADM', 'CADM') or @security.isMySysUser(#sysUserIdString)) "
            + "and !@security.isImpersonated()")
    @RequestMapping(value = "{sysUserId}/status", method = RequestMethod.PUT)
    @ApiOperation(value="Admin call only, Sets a user's status to the inputted status for a given SysUserId")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully changed the given user's status.", 
    response = DetailProfileInfo.class) })
    @Permission("PUBLIC")
    public DetailProfileInfo updateUserStatusBySysUserId ( 
            @ApiParam(value = "System User Id that you wish to unlock") 
            @PathVariable(SYS_USER_ID_REST_PARAM) String sysUserIdString,
            @ApiParam(value = "Body for the new status, accepted vales:<br><br>"
                    + "ACTIVE<br>INACTIVE<br>SUSPENDED<br>RESTRICTED")
            @RequestBody UserStatusDTO userStatusDTO
            ) {
        Long sysUserId = security.getSysUserId(sysUserIdString);        
        
            return userStatusService.updateUserStatusBySysUserId(sysUserId, 
                    userStatusDTO.getStatus());
    }
}
