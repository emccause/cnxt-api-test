package com.maritz.culturenext.security.user.status.dto;


public class UserStatusDTO {
    private String status;
    
    public String getStatus() {
        return status;
    }
    
    public void setToStatus(String status) {
        this.status = status;
    }

}
