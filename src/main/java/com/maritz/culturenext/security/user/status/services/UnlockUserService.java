package com.maritz.culturenext.security.user.status.services;

public interface UnlockUserService {

    /**
     * TODO javadocs
     * 
     * @param sysUserId
     */
    String unlockUserBySysUserId(Long sysUserId);
}