package com.maritz.culturenext.security.user.status.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class UserStatusConstants {

    public static final String ERROR_USER_INVALID = "USER_INVALID";
    public static final String ERROR_USER_INVALID_MSG = "user Id is invalid";
    public static final String ERROR_USER_NOT_LOCKED = "USER_NOT_LOCKED";
    public static final String ERROR_USER_NOT_LOCKED_MSG = "user is not in locked status";
    public static final String STATUS_INVALID = "STATUS_INVALID";
    public static final String STATUS_INVALID_MSG = "status value is invalid for update";
    public static final String EMPTY_STATUS = "EMPTY_STATUS";
    public static final String EMPTY_STATUS_MSG = "Status value was not supplied";
    public static final String ERROR_USER_STATUS_CHANGE_NOT_ALLOWED = "USER_STATUS_CHANGE_NOT_ALLOWED";
    public static final String ERROR_USER_STATUS_CHANGE_NOT_ALLOWED_MSG = "Logged in user is not allowed to change this status.";
    public static final String EMPLOYEE_ID_REQUIRED = "EMPLOYEE_ID_REQUIRED";
    public static final String EMPLOYEE_ID_REQUIRED_MSG = "Employee Id is required.";
    public static final String BIRTH_DATE_REQUIRED = "BIRTH_DATE_REQUIRED";
    public static final String BIRTH_DATE_REQUIRED_MSG = "Birth date field is required.";
    public static final String USER_REGISTER_NOT_ALLOWED = "USER_REGISTER_NOT_ALLOWED";
    public static final String USER_REGISTER_NOT_ALLOWED_MSG = "Not allow to register user without email.";
    public static final String USER_REGISTER_TOO_MANY_BAD_ATTEMPTS= "TOO_MANY_BAD_ATTEMPTS";
    public static final String USER_REGISTER_TOO_MANY_BAD_ATTEMPTS_MSG = "Attempts has exceded the maximum allowed.";
    public static final String USER_REGISTER_BAD_ATTEMPT= "BAD_ATTEMPT";
    public static final String USER_REGISTER_BAD_ATTEMPT_MSG = "Participant's birth date does not match.";
    public static final String USER_EMAIL_REGISTERED= "USER_EMAIL_REGISTERED";
    public static final String USER_EMAIL_REGISTERED_MSG = "There is an email configured for this participant.";
        
    //Error Messages
    public static final ErrorMessage USER_INVALID_MESSAGE = new ErrorMessage()
        .setCode(UserStatusConstants.ERROR_USER_INVALID)
        .setField(ProjectConstants.SYS_USER_ID)
        .setMessage(UserStatusConstants.ERROR_USER_INVALID_MSG);
    
    public static final ErrorMessage USER_NOT_LOCKED_MESSAGE = new ErrorMessage()
        .setCode(UserStatusConstants.ERROR_USER_NOT_LOCKED)
        .setField(ProjectConstants.SYS_USER_ID)
        .setMessage(UserStatusConstants.ERROR_USER_NOT_LOCKED_MSG);
    
    public static final ErrorMessage USER_STATUS_CHANGE_NOT_ALLOWED = new ErrorMessage()
            .setCode(UserStatusConstants.ERROR_USER_STATUS_CHANGE_NOT_ALLOWED)
            .setField(ProjectConstants.SYS_USER_ID)
            .setMessage(UserStatusConstants.ERROR_USER_STATUS_CHANGE_NOT_ALLOWED_MSG);
    
    public static final ErrorMessage ERROR_EMPLOYEE_ID_REQUIRED = new ErrorMessage()
            .setCode(UserStatusConstants.EMPLOYEE_ID_REQUIRED)
            .setField(ProjectConstants.EMPLOYEE_ID)
            .setMessage(UserStatusConstants.EMPLOYEE_ID_REQUIRED_MSG);
    
    public static final ErrorMessage ERROR_BIRTH_DATE_REQUIRED = new ErrorMessage()
            .setCode(UserStatusConstants.BIRTH_DATE_REQUIRED)
            .setField(ProjectConstants.BIRTH_DATE)
            .setMessage(UserStatusConstants.BIRTH_DATE_REQUIRED_MSG);
    
    public static final ErrorMessage ERROR_USER_REGISTER_NOT_ALLOWED = new ErrorMessage()
            .setCode(UserStatusConstants.USER_REGISTER_NOT_ALLOWED)
            .setField(ProjectConstants.EMPLOYEE_ID)
            .setMessage(UserStatusConstants.USER_REGISTER_NOT_ALLOWED_MSG);
    
    public static final ErrorMessage ERROR_USER_REGISTER_BAD_ATTEMPT = new ErrorMessage()
            .setCode(UserStatusConstants.USER_REGISTER_BAD_ATTEMPT)
            .setField(ProjectConstants.EMPLOYEE_ID)
            .setMessage(UserStatusConstants.USER_REGISTER_BAD_ATTEMPT_MSG);

    public static final ErrorMessage ERROR_USER_REGISTER_TOO_MANY_BAD_ATTEMPTS = new ErrorMessage()
            .setCode(UserStatusConstants.USER_REGISTER_TOO_MANY_BAD_ATTEMPTS)
            .setField(ProjectConstants.EMPLOYEE_ID)
            .setMessage(UserStatusConstants.USER_REGISTER_TOO_MANY_BAD_ATTEMPTS_MSG);
    
    public static final ErrorMessage ERROR_USER_EMAIL_REGISTERED = new ErrorMessage()
            .setCode(UserStatusConstants.USER_EMAIL_REGISTERED)
            .setField(ProjectConstants.EMPLOYEE_ID)
            .setMessage(UserStatusConstants.USER_EMAIL_REGISTERED_MSG);
    


}
