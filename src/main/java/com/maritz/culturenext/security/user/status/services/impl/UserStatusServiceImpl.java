package com.maritz.culturenext.security.user.status.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.maritz.core.admin.cache.CacheService;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.security.user.status.constants.UserStatusConstants;
import com.maritz.culturenext.security.user.status.services.UnlockUserService;
import com.maritz.culturenext.security.user.status.services.UserStatusService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.profile.services.DetailProfileService;

@Component
public class UserStatusServiceImpl implements UserStatusService{

    @Inject private CacheService cacheService;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private DetailProfileService detailProfileService;
    @Inject private UnlockUserService unlockUserService;
    @Inject private Security security;
    
    Set<String> ACCEPTED_VALUES = new HashSet<String>(Arrays.asList( new String[] { 
            StatusTypeCode.ACTIVE.name(),
            StatusTypeCode.INACTIVE.name(),
            StatusTypeCode.SUSPENDED.name(),
            StatusTypeCode.RESTRICTED.name()}));

    @Override
    public DetailProfileInfo unlockUserBySysUserId(Long sysUserId) {
        SysUser user = sysUserDao.findBy()
            .where(ProjectConstants.SYS_USER_ID).eq(sysUserId)
            .findOne();
        
        unlockUserService.unlockUserBySysUserId(sysUserId);
        
        return detailProfileService.getDetailProfileInfoByPaxId(user.getPaxId());
    }

    @Override
    public DetailProfileInfo updateUserStatusBySysUserId(Long sysUserId, String status) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        
        SysUser user = sysUserDao.findBy()
                .where(ProjectConstants.SYS_USER_ID).eq(sysUserId)
                .findOne();
        if (user == null) {
            errors.add(new ErrorMessage()
                .setCode(UserStatusConstants.ERROR_USER_INVALID)
                .setField(ProjectConstants.SYS_USER_ID)
                .setMessage(UserStatusConstants.ERROR_USER_INVALID_MSG));
        } else {
            if(status == null ){
                errors.add(new ErrorMessage()
                    .setCode(UserStatusConstants.EMPTY_STATUS)
                    .setField(ProjectConstants.SYS_USER_ID)
                    .setMessage(UserStatusConstants.EMPTY_STATUS_MSG));
            
            } else if(user.getStatusTypeCode().equals(status) || !ACCEPTED_VALUES.contains(status) ) {    
                errors.add(new ErrorMessage()
                    .setCode(UserStatusConstants.STATUS_INVALID)
                    .setField(ProjectConstants.SYS_USER_ID)
                    .setMessage(UserStatusConstants.STATUS_INVALID_MSG));
            
            } else if (!security.hasRole("MTZ:ADM", "CADM") 
                && !(StatusTypeCode.NEW.name().equals(user.getStatusTypeCode()) 
                        && StatusTypeCode.ACTIVE.name().equals(status) )) {    
                errors.add(UserStatusConstants.USER_STATUS_CHANGE_NOT_ALLOWED);
                
                //Throw any errors with 403 status
                ErrorMessageException.throwIfHasErrors(errors, HttpStatus.FORBIDDEN);
            }
        }       
        
        //Throw any errors before we save
        ErrorMessageException.throwIfHasErrors(errors);
        
        user.setStatusTypeCode(status);
        
        sysUserDao.save(user);
        
        // force user cache update
        cacheService.evictUserCache(user.getSysUserName());
        
        return detailProfileService.getDetailProfileInfoByPaxId(user.getPaxId());
    }

}
