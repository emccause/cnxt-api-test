package com.maritz.culturenext.security.user.status.services.impl;

import java.util.ArrayList;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.admin.cache.CacheService;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.HistorySysUser;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.security.user.status.constants.UserStatusConstants;
import com.maritz.culturenext.security.user.status.services.UnlockUserService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class UnlockUserServiceImpl implements UnlockUserService{
    
    @Inject private CacheService cacheService;
    @Inject private ConcentrixDao<HistorySysUser> historySysUserDao;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    
    @Override
    public String unlockUserBySysUserId(Long sysUserId) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        SysUser user = sysUserDao.findBy()
                .where(ProjectConstants.SYS_USER_ID).eq(sysUserId)
                .findOne();
        if (user == null) {
            errors.add(UserStatusConstants.USER_INVALID_MESSAGE);
        } else {
            if(user.getStatusTypeCode().equals(StatusTypeCode.LOCKED.name())) {        
                HistorySysUser historySysUser = historySysUserDao.findBy()
                        .where(ProjectConstants.SYS_USER_ID).eq(sysUserId)
                        .and(ProjectConstants.STATUS_TYPE_CODE).ne(StatusTypeCode.LOCKED.name())
                        .desc(ProjectConstants.HISTORY_ID)
                        .findOne();
                String prevStatus = historySysUser.getStatusTypeCode();
                user.setStatusTypeCode(prevStatus);
            } else {
                errors.add(UserStatusConstants.USER_NOT_LOCKED_MESSAGE);
            }       
        }       
        //Throw any errors before we save
        ErrorMessageException.throwIfHasErrors(errors);
        
        sysUserDao.save(user);

        cacheService.clearCachesForPaxId(user.getPaxId());    
        
        return user.getStatusTypeCode();
    }
}
