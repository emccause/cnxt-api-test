package com.maritz.culturenext.security.user.status.services;

import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;

public interface UserStatusService {

    /**
     * Update a user's status from LOCKED to ACTIVE
     * 
     * @param sysUserId - user ID to unlock
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/14581788/Unlock+User+-+update+status+to+prior+status
     */
    DetailProfileInfo unlockUserBySysUserId(Long sysUserId);
    
    /**
     * Update a user's status in the SYS_USER table
     * 
     * @param sysUserId
     * @param status
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/14581804/Update+user+status
     */
    DetailProfileInfo updateUserStatusBySysUserId(Long sysUserId, String status);
}
