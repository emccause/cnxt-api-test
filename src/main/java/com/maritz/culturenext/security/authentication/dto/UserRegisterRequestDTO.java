package com.maritz.culturenext.security.authentication.dto;

public class UserRegisterRequestDTO {
    private String employeeId;
    private String birthDate;
    
    public String getEmployeeId() {
        return employeeId;
    }
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
    public String getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
