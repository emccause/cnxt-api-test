package com.maritz.culturenext.security.authentication.login.handler;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.security.authentication.login.LoginInfo;
import com.maritz.core.security.authentication.login.handler.LoginHandler;
import com.maritz.culturenext.profile.services.ProfileIncompleteService;

@Component
public class ProfileIncompleteLoginHandler implements LoginHandler {

    private static final String PROFILE_INCOMPLETE_EVENT = "PROFILE_INCOMPLETE";

    @Inject private ProfileIncompleteService profileIncompleteService;

    @Override
    public void handleLogin(LoginInfo loginInfo) {
        //only check if the user is not being impersonated
        if (loginInfo.isImpersonated() == false) {
            if (profileIncompleteService.isProfileIncomplete(loginInfo.getPax().getPaxId()))
                loginInfo.addEvent(PROFILE_INCOMPLETE_EVENT);
        }
    }

    @Override
    public void handleLogout() {
    }

}