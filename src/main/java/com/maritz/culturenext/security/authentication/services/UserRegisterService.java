package com.maritz.culturenext.security.authentication.services;

import com.maritz.culturenext.security.authentication.dto.UserRegisterRequestDTO;
import com.maritz.culturenext.security.authentication.dto.UserRegisterTokenResponseDTO;

public interface UserRegisterService {

    /**
     * Register the user without email. Returning a token to reset password.
     * @param userRegisterRequest
     * @return
     */
    UserRegisterTokenResponseDTO register(UserRegisterRequestDTO userRegisterRequest);


}
