package com.maritz.culturenext.security.authentication.login.handler;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.PaxFiles;
import com.maritz.core.security.authentication.login.LoginInfo;
import com.maritz.core.security.authentication.login.handler.LoginHandler;
import com.maritz.culturenext.constants.ProjectConstants;


@Component
public class CheckProfilePictureLoginHandler implements LoginHandler {

    @Inject private ConcentrixDao<PaxFiles> paxFilesDao;
    
    @Override
    public void handleLogin(LoginInfo loginInfo) {        
        loginInfo.addAttribute("profilePictureVersion",
            paxFilesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(loginInfo.getPax().getPaxId())
                .findOne(ProjectConstants.VERSION, Integer.class)
        );
    }

    @Override
    public void handleLogout() {
    }
}
