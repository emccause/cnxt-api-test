package com.maritz.culturenext.security.authentication.dto;

public class UserRegisterTokenResponseDTO {
    private String userName;
    private String registerToken;

    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getRegisterToken() {
        return registerToken;
    }
    public void setRegisterToken(String registerToken) {
        this.registerToken = registerToken;
    }
}
