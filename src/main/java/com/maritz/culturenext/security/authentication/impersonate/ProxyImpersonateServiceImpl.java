package com.maritz.culturenext.security.authentication.impersonate;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.security.core.AuthenticationException;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.AliasHistory;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.AliasHistoryRepository;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.impersonate.ImpersonateService;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.ProxyDao;
import com.maritz.culturenext.constants.ProjectConstants;

public class ProxyImpersonateServiceImpl implements ImpersonateService {

    @Inject private AliasHistoryRepository aliasHistoryRepository;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private ProxyDao proxyDao;
    @Inject private Security security;

    @Override
    public Long impersonate(Long sysUserId) throws AuthenticationException {
        
        List<String> proxyStatusTypeList = PermissionConstants.DEFAULT_PROXY_STATUS_LIST;
        
        SysUser sysUser = sysUserDao.findBy()
                .where(ProjectConstants.SYS_USER_ID).eq(sysUserId)
                .and(ProjectConstants.STATUS_TYPE_CODE).in(proxyStatusTypeList)
                .findOne();
        
        if (sysUser == null) {
            return null;
        }

        // Admin PROXY_ANYONE permissions
        Boolean proxyGiveRecAnyone = security.hasPermission(
                PermissionManagementTypes.PROXY_GIVE_REC_ANYONE.toString());
        Boolean proxyApproveRecAnyone = security.hasPermission(
                PermissionManagementTypes.PROXY_APPROVE_REC_ANYONE.toString());
        Boolean proxyViewReportsAnyone = security.hasPermission(
                PermissionManagementTypes.PROXY_VIEW_REPORTS_ANYONE.toString());
        
        // Proxy as specific people
        Boolean canProxyAs = proxyDao.doesAclEntryExist(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST, 
                sysUser.getPaxId(), security.getPaxId());
        
        // If any of the above are true then proxy is enabled
        if (proxyGiveRecAnyone == true || proxyApproveRecAnyone == true || 
                proxyViewReportsAnyone == true || canProxyAs == true) {
            AliasHistory aliasHistory = new AliasHistory();
            aliasHistory.setAliasSysUserId(sysUserId);
            aliasHistory.setPaxId(security.getPaxId());
            aliasHistory.setSessionId("PENDING");
            aliasHistory.setStartDate(new Date());
            aliasHistoryRepository.save(aliasHistory);
            return aliasHistory.getAliasHistoryId();
        }
        
        return null;
    }
}
