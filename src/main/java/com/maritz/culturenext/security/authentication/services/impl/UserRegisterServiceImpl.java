package com.maritz.culturenext.security.authentication.services.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.security.authentication.dto.UserRegisterRequestDTO;
import com.maritz.culturenext.security.authentication.dto.UserRegisterTokenResponseDTO;
import com.maritz.culturenext.security.authentication.services.UserRegisterService;
import com.maritz.culturenext.security.user.status.constants.UserStatusConstants;
import com.maritz.profile.dto.PasswordResetDTO;
import com.maritz.profile.services.PasswordService;

@Component
public class UserRegisterServiceImpl implements UserRegisterService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject @Named("passwordResetAttemptsCache") private Cache passwordResetAttemptsCache;
    @Inject @Named("resetPasswordTokenService") private TokenService resetPasswordTokenService;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private EmailRepository emailRepository;
    @Inject private PasswordEncoder passwordEncoder;
    @Inject private PasswordService passwordService;
    @Inject private PaxRepository paxRepository;
    @Inject private PaxMiscRepository paxMiscRepository;
    @Inject private SysUserRepository sysUserRepository;

    @Value("${password.resetAttempts:5}")
    Integer resetAttempts;


    @Override
    public UserRegisterTokenResponseDTO register(UserRegisterRequestDTO userRegisterRequest) {
        validateUserRegisterRequest(userRegisterRequest);

        return createResetToken(userRegisterRequest);

    }

    /**
     * Validate employee id account and birth date fields
     * @param userRegisterRequest
     */
    private void validateUserRegisterRequest(UserRegisterRequestDTO userRegisterRequest) {

        // check the cache for too many attempts
        Integer attempts = passwordResetAttemptsCache.get(userRegisterRequest.getEmployeeId(), Integer.class);
        if (attempts != null && attempts >= resetAttempts) {
            throw new ErrorMessageException(UserStatusConstants.ERROR_USER_REGISTER_TOO_MANY_BAD_ATTEMPTS);
        }

        if (userRegisterRequest.getEmployeeId() == null || userRegisterRequest.getEmployeeId().isEmpty()) {
            throw new ErrorMessageException(UserStatusConstants.ERROR_EMPLOYEE_ID_REQUIRED);
        } else if (userRegisterRequest.getBirthDate() == null || userRegisterRequest.getBirthDate().isEmpty()) {
            throw new ErrorMessageException(UserStatusConstants.ERROR_BIRTH_DATE_REQUIRED);
        }

        Pax pax = paxRepository.findByControlNum(userRegisterRequest.getEmployeeId());
        if (pax == null || pax.getPaxId() == null) {
            logger.error("Participant not exists with an employee id = " + userRegisterRequest.getEmployeeId());
            // update the bad attempts cache count
            attempts = attempts == null ? 1 : ++attempts;
            passwordResetAttemptsCache.put(userRegisterRequest.getEmployeeId(), attempts);
            throw new ErrorMessageException(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED);
        }

        SysUser sysUser = sysUserDao.findBy().where(ProjectConstants.PAX_ID).eq(pax.getPaxId()).findOne();
        if (sysUser != null) {
            // check current status and send error on inactive user
            if (StatusTypeCode.INACTIVE.name().equals(sysUser.getStatusTypeCode())) {
                logger.error("Participant's account is inactive for employee id = " + userRegisterRequest.getEmployeeId());
                // update the bad attempts cache count
                attempts = attempts == null ? 1 : ++attempts;
                passwordResetAttemptsCache.put(userRegisterRequest.getEmployeeId(), attempts);
                throw new ErrorMessageException(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED);
            }
        }

        // Check if there is a birth date value registered.
        PaxMisc paxMiscFullBirthDate = paxMiscRepository.findOne(pax.getPaxId(), VfName.FULL_BIRTH_DATE.name());
        if (paxMiscFullBirthDate == null) {
            logger.error("There isn't any birth date configured for employee id = " + userRegisterRequest.getEmployeeId());
            // update the bad attempts cache count
            attempts = attempts == null ? 1 : ++attempts;
            passwordResetAttemptsCache.put(userRegisterRequest.getEmployeeId(), attempts);
            throw new ErrorMessageException(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED);
        }

        // Validate birth date matches with registered value.
        String formattedDate = DateUtils.parseFullOrPartialDate(userRegisterRequest.getBirthDate()).getFullDate();
        if (!passwordEncoder.matches(formattedDate, paxMiscFullBirthDate.getMiscData())) {
            // update the bad attempts cache count
            attempts = attempts == null ? 1 : ++attempts;
            passwordResetAttemptsCache.put(userRegisterRequest.getEmployeeId(), attempts);

            throw new ErrorMessageException(UserStatusConstants.ERROR_USER_REGISTER_BAD_ATTEMPT);
        }

        // Validate email record doesn't exist.
        if (emailRepository.findPreferredByPaxId(pax.getPaxId()) != null) {
            throw new ErrorMessageException( new ErrorMessage()
                .setCode(UserStatusConstants.USER_EMAIL_REGISTERED)
                .setField(ProjectConstants.EMAIL_ADDRESS)
                .setMessage(emailRepository.findPreferredByPaxId(pax.getPaxId()).getEmailAddress()));
        }
    }

    /**
     * Populate UserRegisterTokenResponseDTO object
     * @param userRegisterRequest
     * @return
     */
    private UserRegisterTokenResponseDTO createResetToken(UserRegisterRequestDTO userRegisterRequest) {

        Long paxId = paxRepository.findPaxIdByControlNum(userRegisterRequest.getEmployeeId());
        String userName = sysUserRepository.findByPaxId(paxId).get(0).getSysUserName();

        UserRegisterTokenResponseDTO registerTokenResponse = new UserRegisterTokenResponseDTO();
        registerTokenResponse.setUserName(userName);
        registerTokenResponse.setRegisterToken(passwordService.createResetPasswordToken(new PasswordResetDTO()
            .setUserName(userName)
        ));

        //clear the attempts cache for this user
        passwordResetAttemptsCache.evict(userRegisterRequest.getEmployeeId());

        return registerTokenResponse;
    }



}
