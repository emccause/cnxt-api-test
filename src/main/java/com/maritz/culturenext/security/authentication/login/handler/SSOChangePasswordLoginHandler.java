package com.maritz.culturenext.security.authentication.login.handler;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.login.LoginInfo;
import com.maritz.core.security.authentication.login.handler.LoginHandler;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.profile.services.PasswordService;


@Component
public class SSOChangePasswordLoginHandler implements LoginHandler {

    @Inject private Environment environment;
    @Inject private PasswordService passwordService;
    @Inject private Security security;
    @Inject private SysUserRepository sysUserRepository;

    @Override
    public void handleLogin(LoginInfo loginInfo) {
        //only check for password change if the user is not being impersonated and the ssoEnabled flag is false
        if (loginInfo.isImpersonated() == false &&
                !Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))) {
            SysUser sysUser = sysUserRepository.findBySysUserName(loginInfo.getUserName());
            if (security.isNewUser() || passwordService.isPasswordExpired(sysUser.getSysUserPasswordDate())) {
                loginInfo.addEvent("CHANGE_PW");
            }
        }
    }

    @Override
    public void handleLogout() {
    }
}
