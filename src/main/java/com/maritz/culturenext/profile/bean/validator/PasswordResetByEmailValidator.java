package com.maritz.culturenext.profile.bean.validator;

import java.util.Collection;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.bean.validator.AbstractValidator;
import com.maritz.core.jpa.support.findby.FindBy;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.jpa.entity.Email;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.profile.dto.PasswordResetDTO;

@Component
public class PasswordResetByEmailValidator extends AbstractValidator<PasswordResetDTO> {

    protected EmailRepository emailRepository;

    public PasswordResetByEmailValidator() {
        super(PasswordResetDTO.class, ProfileConstants.EMAIL);
    }

    /**
     * Set email repository
     *
     * @param emailRepository - set email repository
     *
     */
    @Inject
    public PasswordResetByEmailValidator setEmailRepository(EmailRepository emailRepository) {
        this.emailRepository = emailRepository;
        return this;
    }

    /**
     * Only validates email
     *
     * @param passwordReset - contains the email to be validated
     * @param errorMessages - collection of errorMessages
     * @param hints - collection of hints
     *
     */
    @Override
    public void validate(PasswordResetDTO passwordReset, Collection<ErrorMessage> errorMessages, Collection<String> hints) {
        
        if (passwordReset.getUserName() != null && passwordReset.getEmail() == null) {
            // Happens when this is called during the call to passwordService.createResetPasswordToken
            // in UserRegisterService.createResetToken - user will not have an email at that point
            return;
        }
        
        if (passwordReset.getEmail() == null) {
            errorMessages.add(ProfileConstants.ERROR_EMAIL_MUST_BE_SUPPLIED_MESSAGE);
        }
        
        FindBy<Email> mail= emailRepository.findBy().where(ProjectConstants.EMAIL_ADDRESS).eq(passwordReset.getEmail());        
        if (mail == null) {
            errorMessages.add(ProfileConstants.INVALID_EMAIL_MESSAGE);
        }
    }
}
