package com.maritz.culturenext.profile.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.enums.ActivityFeedFilterEnum;
import org.springframework.stereotype.Component;

import com.maritz.core.dto.AuthorityDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.GroupConfigRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authorization.AuthorizationService;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.jpa.repository.CnxtAclRepository;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.EnrollmentGroupsDao;
import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;
import com.maritz.culturenext.profile.services.EnrollmentGroupsService;
import com.maritz.culturenext.util.StatusTypeUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class EnrollmentGroupsServiceImpl implements EnrollmentGroupsService {    
    private static final String GROUP_CONFIG = "GROUP_CONFIG";
    private static final String ROLE = "ROLE";
    private static final String IN_PROGRESS = "IN_PROGRESS";
    private static final String QUEUED = "QUEUED";
    private static final String VIS_PUBLIC = "PUBLIC";
    private static final String VIS_ADMIN = "ADMIN";
    private static final String ERROR_INVALID_GROUP_CONFIG_ID = "INVALID_GROUP_CONFIG_ID";
    private static final String ERROR_INVALID_GROUP_CONFIG_ID_MSG = "GroupConfigId does not exist: ";
    private static final String ERROR_INVALID_VISIBILITY = "INVALID_VISIBILITY";
    private static final String ERROR_INVALID_VISIBILITY_MSG = " is not a valid option for group visibility";
    private static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    private static final String ERROR_INVALID_STATUS_MSG = " is not a valid status type code";
     
    private Role visibilityRole;
    private Role publicRole;
    private Role maritzAdminRole;
    private Role clientAdminRole;
        
    protected ApplicationDataService applicationDataService;
    protected AuthorizationService authorizationService;
    protected BatchRepository batchRepository;
    protected CnxtAclRepository cnxtAclRepository;
    protected ConcentrixDao<Batch> batchDao;
    protected ConcentrixDao<GroupConfig> groupConfigDao;
    protected ConcentrixDao<Role> roleDao;
    protected EnrollmentGroupsDao enrollmentGroupsDao;
    protected GroupConfigRepository groupConfigRepository;
    protected TriggeredTaskService triggeredTaskService;
    
    @Inject
    public EnrollmentGroupsServiceImpl setApplicationDataService(ApplicationDataService applicationDataService) {
        this.applicationDataService = applicationDataService;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setAuthorizationService(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setBatchRepository(BatchRepository batchRepository) {
        this.batchRepository = batchRepository;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setCnxtAclRepository(CnxtAclRepository cnxtAclRepository) {
        this.cnxtAclRepository = cnxtAclRepository;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setBatchDao(ConcentrixDao<Batch> batchDao) {
        this.batchDao = batchDao;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setGroupConfigDao(ConcentrixDao<GroupConfig> groupConfigDao) {
        this.groupConfigDao = groupConfigDao;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setRoleDao(ConcentrixDao<Role> roleDao) {
        this.roleDao = roleDao;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setEnrollmentGroupsDao(EnrollmentGroupsDao enrollmentGroupsDao) {
        this.enrollmentGroupsDao = enrollmentGroupsDao;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setGroupConfigRepository(GroupConfigRepository groupConfigRepository) {
        this.groupConfigRepository = groupConfigRepository;
        return this;
    }
    
    @Inject
    public EnrollmentGroupsServiceImpl setTriggeredTaskService(TriggeredTaskService triggeredTaskService) {
        this.triggeredTaskService = triggeredTaskService;
        return this;
    }
        
    @Override
    public List<EnrollmentGroupDTO> getEnrollmentGroups() {
        return enrollmentGroupsDao.getEnrollmentGroups();
    }

    @Override
    public List<EnrollmentGroupDTO> updateEnrollmentGroups(List<EnrollmentGroupDTO> groupList) {
         ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
         
         //Can't do the update if there is already an update in progress
         Batch lastGroupProcessing = batchDao.findBy()
                 .where(ProjectConstants.BATCH_TYPE_CODE).eq(BatchType.GROUP_PROCESSING.getCode())
                 .desc(ProjectConstants.CREATE_DATE)
                 .findOne();
         if (lastGroupProcessing != null 
                 && (lastGroupProcessing.getStatusTypeCode().equalsIgnoreCase(IN_PROGRESS) || 
                         lastGroupProcessing.getStatusTypeCode().equalsIgnoreCase(QUEUED))) {
             errors.add(ProfileConstants.UPDATE_IN_PROGRESS_MESSAGE);            
         }
         else {
             //Save the roleIds that we will need to use later (for ACL inserts)    
             List<Role> roles = roleDao.findBy()
                     .where(ProjectConstants.ROLE_CODE).in(RoleCode.GVIS.name(), RoleCode.RECG.name()
                             , RoleCode.ADM.name(), RoleCode.CADM.name())
                     .find();
         
             for(Role role: roles){
                 switch (RoleCode.valueOf(role.getRoleCode())) {
                 case GVIS: 
                     visibilityRole = role;
                     break;
                 case RECG: 
                     publicRole = role;
                     break;
                 case ADM: 
                     maritzAdminRole = role;
                     break;
                 case CADM: 
                     clientAdminRole = role;
                     break;
                 default:
                    break;
                 }
             }
             
             ArrayList<Long> gcIds = new ArrayList<Long>();
             for (EnrollmentGroupDTO groupDTO : groupList) {
                 gcIds.add(groupDTO.getGroupConfigId());
             }
        
             //Revoke all ACL's. The new ones will be added in the for loop
             cnxtAclRepository.revokeAllRolesByTargetAndSubject(visibilityRole.getRoleId(), GROUP_CONFIG, gcIds, ROLE);
        
             for (EnrollmentGroupDTO groupDTO : groupList) {
                       
                 if (groupDTO != null) {
                     String visibility = null;
                     if (groupDTO.getVisibility() == null) {                    
                         //ACTIVE groups need a visibility
                         if (groupDTO.getGroupCreation().equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                             errors.add(ProfileConstants.NULL_VISIBILITY_MESSAGE);
                         }
                     }
                     else {
                         visibility = groupDTO.getVisibility().toUpperCase();
                         //Check for valid visibility - Public or Admin
                         if (!visibility.equalsIgnoreCase(VIS_PUBLIC) && !visibility.equalsIgnoreCase(VIS_ADMIN)) {
                             errors.add(new ErrorMessage()
                                     .setCode(ERROR_INVALID_VISIBILITY)
                                     .setField(ProjectConstants.VISIBILITY)
                                     .setMessage(visibility + ERROR_INVALID_VISIBILITY_MSG));
                         }
                     }
                
                     //Check for a valid status code
                     if (!StatusTypeUtil.isValidStatus(groupDTO.getGroupCreation())) {
                         errors.add(new ErrorMessage()
                                 .setCode(ERROR_INVALID_STATUS)
                                 .setField("groupCreation")
                                 .setMessage(groupDTO.getGroupCreation() + ERROR_INVALID_STATUS_MSG));
                     }
                                                      
                     GroupConfig groupConfigEntity = groupConfigDao.findById(groupDTO.getGroupConfigId());                
                     if (groupConfigEntity != null) {                        
                         //Update existing Group_Config
                         groupConfigEntity.setGroupConfigDesc(groupDTO.getFieldDisplayName());
                         groupConfigEntity.setStatusTypeCode(groupDTO.getGroupCreation());
                                    
                         groupConfigRepository.save(groupConfigEntity);

                         String groupConfigName = groupConfigEntity.getGroupConfigName();
                        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
                            ProjectConstants.DOT_DELIM +
                            groupConfigName;
                        // If the status is INACTIVE, try to delete the applicable Activity Feed Filter row
                        // in APPLICATION_DATA if it exists
                        if (StatusTypeCode.INACTIVE.name().equalsIgnoreCase(groupConfigEntity.getStatusTypeCode())) {
                            ApplicationDataDTO applicationDataDto =
                                applicationDataService.getApplicationData(activityFeedFilterKey);
                            if (applicationDataDto != null && applicationDataDto.getId() != null) {
                                applicationDataService.removeApplicationData(activityFeedFilterKey);

                                // If the defaultFilter is set to this group, set it to PUBLIC instead
                                applicationDataDto = applicationDataService.getApplicationData(
                                    ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT);
                                if (applicationDataDto != null &&
                                    applicationDataDto.getId() != null &&
                                     groupConfigName.equalsIgnoreCase(applicationDataDto.getValue())) {
                                    applicationDataDto.setValue(ActivityFeedFilterEnum.PUBLIC.name());
                                    applicationDataService.setApplicationData(applicationDataDto);
                                }
                            }
                        // If the status is ACTIVE, try to add the applicable Activity Feed Filter row in
                        // APPLICATION_DATA if it isn't already present
                        } else if (StatusTypeCode.ACTIVE.name().equalsIgnoreCase(groupConfigEntity.getStatusTypeCode())) {
                            ApplicationDataDTO applicationDataDto =
                                applicationDataService.getApplicationData(activityFeedFilterKey);
                            if (applicationDataDto == null || applicationDataDto.getId() == null) {
                                applicationDataDto = new ApplicationDataDTO();
                                applicationDataDto.setKey(activityFeedFilterKey);
                                applicationDataDto.setValue(Boolean.FALSE.toString());
                                applicationDataService.setApplicationData(applicationDataDto);
                            }
                        }
                    
                         //Save the new ACL - Only for ACTIVE groups
                         if (groupDTO.getGroupCreation().equalsIgnoreCase(StatusTypeCode.ACTIVE.name()) && visibility != null) {        
                             switch (visibility) {
                                 case (VIS_PUBLIC):
                                     AuthorityDTO publicDTO = new AuthorityDTO();
                                     publicDTO.applyRole(visibilityRole);
                                     publicDTO.applySubject(ROLE, publicRole.getRoleId());
                                     publicDTO.applyTarget(GROUP_CONFIG, groupDTO.getGroupConfigId());
                                     authorizationService.grantRole(publicDTO);
                                     break;
                                 case (VIS_ADMIN):
                                     AuthorityDTO maritzAdminDTO = new AuthorityDTO();
                                     maritzAdminDTO.applyRole(visibilityRole);
                                     maritzAdminDTO.applySubject(ROLE, maritzAdminRole.getRoleId());
                                     maritzAdminDTO.applyTarget(GROUP_CONFIG, groupDTO.getGroupConfigId());
                                     authorizationService.grantRole(maritzAdminDTO);
                                 
                                     AuthorityDTO clientAdminDTO = new AuthorityDTO();
                                     clientAdminDTO.applyRole(visibilityRole);
                                     clientAdminDTO.applySubject(ROLE, clientAdminRole.getRoleId());
                                     clientAdminDTO.applyTarget(GROUP_CONFIG, groupDTO.getGroupConfigId());
                                     authorizationService.grantRole(clientAdminDTO);
                                     break;
                             }
                         }
                     }
                     else {
                         errors.add(new ErrorMessage()
                                 .setCode(ERROR_INVALID_GROUP_CONFIG_ID)
                                 .setField(ProjectConstants.GROUP_CONFIG_ID)
                                 .setMessage(ERROR_INVALID_GROUP_CONFIG_ID_MSG + " (" + groupDTO.getGroupConfigId() + ")"));

                     }
                 }
            }
             
             if (errors.isEmpty()) {
                 //Create a batch record for the group processing
                 Batch currentGroupProcessing = new Batch();
                 currentGroupProcessing.setBatchTypeCode(BatchType.GROUP_PROCESSING.getCode());
                 currentGroupProcessing.setStatusTypeCode(QUEUED);
                 batchRepository.save(currentGroupProcessing);
             }
            
        }
         ErrorMessageException.throwIfHasErrors(errors);
         
         //Kick off the process to update group membership for enrollment groups
         triggeredTaskService.runTriggeredTaskAsync("processGroupMembership");
        
        return getEnrollmentGroups();
    }

}
