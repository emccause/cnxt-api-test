package com.maritz.culturenext.profile.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dto.aggregate.LightweightProfileInfoDTO;
import com.maritz.culturenext.profile.services.LightProfileService;
import com.maritz.culturenext.profile.services.ProfileIncompleteService;

@Component
public class LightProfileServiceImpl implements LightProfileService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ProfileIncompleteService profileIncompleteService;
    @Inject private Security security;

    @Override
    public LightweightProfileInfoDTO getLightProfileInfo() {

        Long paxId = security.getPaxId();
        if (paxId != null) {
            LightweightProfileInfoDTO lightweightProfileInfoDTO = new LightweightProfileInfoDTO();

            lightweightProfileInfoDTO.setUserName(security.getUserName());

            lightweightProfileInfoDTO.setPax(participantInfoDao.getEmployeeDTO(paxId));

            Long impersonatorPaxId = security.getImpersonatorPaxId();
            if (impersonatorPaxId != null) {
                lightweightProfileInfoDTO.setImpersonated(true);
                lightweightProfileInfoDTO.setImpersonatorPax(participantInfoDao.getEmployeeDTO(impersonatorPaxId));
            }
            else {
                lightweightProfileInfoDTO.setImpersonated(false);
            }

            List<String> roles = new ArrayList<>();
            for (GrantedAuthority authority : security.getPaxUserDetails().getAuthorities()) {
                roles.add(authority.getAuthority());
            }
            
            // Work around for UI expecting things that are now permissions to be handled as roles
            for (String permission : security.getPermissions()) {
                String roleCode = permission.substring(ProfileConstants.PERM_PREFIX.length()); // Strip off PERM_ prefix
                if (PermissionManagementTypes.getAllPermissions().contains(roleCode)) { // see if it's one we care about
                    String transformedRoleCode = ProfileConstants.ROLE_PREFIX + roleCode; // Add ROLE_ prefix (like existing roles)
                    if (!roles.contains(transformedRoleCode)) { // Add it if they don't already have it
                        roles.add(transformedRoleCode);
                    }
                }
            }
            
            lightweightProfileInfoDTO.setRoles(roles);

            lightweightProfileInfoDTO.setProfileIncomplete(profileIncompleteService.isProfileIncomplete(paxId));

            return lightweightProfileInfoDTO;
        }

        return null;
    }

}
