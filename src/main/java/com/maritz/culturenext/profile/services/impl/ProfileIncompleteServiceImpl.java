package com.maritz.culturenext.profile.services.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Email;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.services.ProfileIncompleteService;
import com.maritz.profile.services.PasswordService;

@Component
public class ProfileIncompleteServiceImpl implements ProfileIncompleteService {

    @Inject private ConcentrixDao<Email> emailDao;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private Environment environment;
    @Inject private PasswordService passwordService;
    @Inject private Security security;

    @Override
    public boolean isProfileIncomplete(Long paxId) {
         //Get the SSO flag from the project-profile
        boolean ssoEnabled = Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED));
        boolean profileIncomplete = false;

        //Email is required for profile completion
        List<Email> emailList = emailDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .find();
        if (emailList != null && emailList.size() > 0)
            profileIncomplete = false;
        else
            profileIncomplete = true;

        //Only check password if an email is present and the client does not use SSO
        if (profileIncomplete == false && ssoEnabled == false) {
            //The password is invalid if the user is NEW status or the password date is null or expired
            SysUser sysUser = sysUserDao.findBy()
                    .where(ProjectConstants.PAX_ID).eq(paxId)
                    .findOne();
            if (security.isNewUser() || passwordService.isPasswordExpired(sysUser.getSysUserPasswordDate())) {
                profileIncomplete = true;
            }
        }

        return profileIncomplete;
    }

}

