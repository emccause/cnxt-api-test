package com.maritz.culturenext.profile.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.profile.dto.ProfilePictureDTO;

public interface PaxImageService {

    /**
     * Get a user's profile picture
     * 
     * @param paxId
     * @param type
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340108/Get+and+Update+Profile+Picture+Examples
     */
    ResponseEntity<Object> getImage(Long paxId, String type) throws Exception;

    /**
     * Upload a profile picture
     * 
     * @param paxId
     * @param crop
     * @param images
     * @param circleCrop
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340108/Get+and+Update+Profile+Picture+Examples
     */
    ProfilePictureDTO uploadImage(Long paxId, String crop, MultipartFile[] images, Boolean circleCrop) throws Exception;

    /**
     * Delete a user's profile picture
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340108/Get+and+Update+Profile+Picture+Examples
     */
    void deleteImage(Long paxId) throws Exception;

}
