package com.maritz.culturenext.profile.services;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Groups;
import com.maritz.culturenext.profile.dto.*;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentFullDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentWithStubDTO;

public interface GroupsService {

    /**
     * Return information about the group specified
     * 
     * @param groupId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    Map<String, Object> getGroupInfo(Long groupId);
    
    /**
     * Get a list of all members currently in the specified group
     * 
     * @param groupId
     * @param expandGroups
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    List<EntityDTO> getGroupMembers(Long groupId, Boolean expandGroups, Integer page, Integer size);
    
    /** 
     * Create a new CUSTOM group
     * @param groupEnrollmentStub
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    GroupEnrollmentFullDTO createGroup(GroupEnrollmentWithStubDTO groupEnrollmentStub);
    
    /**
     * Update a specific group. Can update name, status, and visibility.
     * 
     * @param groupId
     * @param groupDto
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    Map<String, Object> updateGroup(Long groupId, GroupDTO groupDto);

    /**
     * Add members to a specific group
     * 
     * @param groupId
     * @param members
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    List<EntityDTO> addMembers(Long groupId, List<GroupMemberStubDTO> members);
    
    /**
     * Remove members from a specific group
     * 
     * @param groupId
     * @param members
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338977/Group+Example
     */
    void deleteMembers(Long groupId, List<GroupMemberStubDTO> members);

    /** 
     * Return all active groups
     * 
     * TODO Confluence Documentation
     */
    List<Groups> getActiveGroups();
}
