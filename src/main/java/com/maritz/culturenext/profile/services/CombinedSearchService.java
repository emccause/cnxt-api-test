package com.maritz.culturenext.profile.services;

import java.util.List;

import com.maritz.culturenext.profile.dto.EntityDTO;

public interface CombinedSearchService {

    /**
     * Returns pax or group data that matches the provided params
     * 
     * TODO See if the UI is using the current version anywhere. If not, drop this in favor of
     * the FUTURE version in SearchService
     * 
     * @param searchStr
     * @param page
     * @param size
     * @param visibilityListString
     * @param statusListString
     * @param groupTypeListString
     * @param excludeSelfString
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847663/Combined+Search+ver+2
     */
    List<EntityDTO> getSearchResults(String searchStr, Integer page, Integer size, String visibilityListString,
            String statusListString, String groupTypeListString, String excludeSelfString);
}