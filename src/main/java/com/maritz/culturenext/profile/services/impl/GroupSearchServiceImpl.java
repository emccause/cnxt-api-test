package com.maritz.culturenext.profile.services.impl;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.profile.dao.PaginatedGroupSearchDao;
import com.maritz.culturenext.profile.services.GroupSearchService;
import com.maritz.culturenext.util.PaginationUtil;

import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

@Component
public class GroupSearchServiceImpl implements GroupSearchService {
    
    @Inject private PaginatedGroupSearchDao paginatedGroupSearchDao;

    @Override
    public List<Map<String, Object>> findGroups(String searchString, Integer page, Integer size, 
            String groupConfigIdString) {

        Long groupConfigId = null;
        if(groupConfigIdString != null){
            try{
                groupConfigId = Long.parseLong(groupConfigIdString);
            }
            catch(NumberFormatException nfe){
                throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode("BAD_GROUP_CONFIG_ID")
                        .setField("request")
                        .setMessage("Invalid Group Config Id"));
            }
        }
        
        return PaginationUtil.getPaginatedList(paginatedGroupSearchDao.searchGroupsByNameAndConfigId(searchString, 
                groupConfigId), page, size);
    }

}
