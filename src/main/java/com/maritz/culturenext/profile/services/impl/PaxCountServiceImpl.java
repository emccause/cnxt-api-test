package com.maritz.culturenext.profile.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.PaxCountDTO;
import com.maritz.culturenext.profile.services.PaxCountService;

@Component
public class PaxCountServiceImpl implements PaxCountService {
    
    @Inject private PaxCountDao paxCountDao;

    @Override
    public PaxCountDTO getCountDistinctPax(List<GroupMemberStubDTO> memberStubs, Boolean excludeSelf) {

        ArrayList<Long> groupIds = new ArrayList<>();
        ArrayList<Long> paxIds = new ArrayList<>();
        
        for(GroupMemberStubDTO dto : memberStubs){
            if(dto.getGroupId() != null){
                groupIds.add(dto.getGroupId());
            }
            else if(dto.getPaxId() != null){
                paxIds.add(dto.getPaxId());
            }
        }
        
        // handling empty arrays
        if(groupIds.isEmpty()){
            groupIds.add(0L);
        }
        
        if(paxIds.isEmpty()){
            paxIds.add(0L);
        }
        
        return new PaxCountDTO().setCount(paxCountDao.getCountDistinctPax(groupIds, paxIds, excludeSelf));
    }

}
