package com.maritz.culturenext.profile.services;

import java.util.List;
import java.util.Map;

public interface PersonalGroupsService {

    /**
     * Return a list of all Personal Groups created by the logged in pax. 
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/368443437/Get+Personal+Groups
     */
    List<Map<String, Object>> getPersonalGroups(Long paxId);
    
    /**
     * Delete Personal Groups created by the logged in pax. 
     * 
     * @param groupId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/507412510/Delete+Personal+Groups
     */
    void deletePersonalGroup(Long groupId);
}
