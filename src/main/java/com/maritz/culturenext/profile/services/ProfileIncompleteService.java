package com.maritz.culturenext.profile.services;

public interface ProfileIncompleteService {

    boolean isProfileIncomplete(Long paxId);

}

