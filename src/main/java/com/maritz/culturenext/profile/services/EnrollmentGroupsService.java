package com.maritz.culturenext.profile.services;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;

public interface EnrollmentGroupsService {

    /**
     * Return a list of all enrollment fields available for grouping and their status and visibility. 
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847508/Enrollment+Groups+Setup+and+Processing+Example
     */
    List<EnrollmentGroupDTO> getEnrollmentGroups();
    
    /**
     * Update the configuration for the enrollment groups. Status and visibility can be updated
     * 
     * @param groupList - groups to update
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847508/Enrollment+Groups+Setup+and+Processing+Example
     */
    List<EnrollmentGroupDTO> updateEnrollmentGroups(List<EnrollmentGroupDTO> groupList);

}
