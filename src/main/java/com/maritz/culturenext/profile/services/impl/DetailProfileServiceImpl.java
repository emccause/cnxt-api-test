package com.maritz.culturenext.profile.services.impl;

import static com.maritz.core.util.Collectors.toMapIgnoreDups;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.validation.Valid;

import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.Email;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxEventCommunication;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.Phone;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.jpa.repository.PaxEventCommunicationRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.PhoneRepository;
import com.maritz.core.jpa.repository.RelationshipRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.CommunicationTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.jpa.support.util.Tuple4;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.util.MapBuilder;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.EmailType;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.ManagerDirectReportDao;
import com.maritz.culturenext.profile.dto.AddressDTO;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EmailPasswordWrapperDTO;
import com.maritz.culturenext.profile.dto.LocaleDTO;
import com.maritz.culturenext.profile.dto.PaxMiscDTO;
import com.maritz.culturenext.profile.dto.PhoneDTO;
import com.maritz.culturenext.profile.dto.SysUserDTO;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.dto.aggregate.PaxMiscListDTO;
import com.maritz.culturenext.profile.dto.aggregate.ProfileDemographics;
import com.maritz.culturenext.profile.services.DetailProfileService;
import com.maritz.culturenext.profile.services.ProfileIncompleteService;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.LocaleUtil;

@Component
@EnableCaching(proxyTargetClass= true)
public class DetailProfileServiceImpl implements DetailProfileService {

    @Inject private AddressRepository addressRepository;
    @Inject private AuthenticationService authenticationService;
    @Inject private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private EmailRepository emailRepository;
    @Inject private Environment environment;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private PaxMiscRepository paxMiscRepository;
    @Inject private PhoneRepository phoneRepository;
    @Inject private ProfileIncompleteService profileIncompleteService;
    @Inject private ProxyService proxyService;
    @Inject private RelationshipRepository relationshipRepository;
    @Inject private Security security;
    @Inject private SysUserRepository sysUserRepository;
    @Inject private ManagerDirectReportDao managerDirectReportDao;
    @Inject protected PaxEventCommunicationRepository paxEventCommunicationRepository;

    protected static final Map<String,String> EVENT_TYPE_CODE_TO_VF_NAME_MAP = MapBuilder.<String,String>newHashMap()
        .add("APR",            "RECEIVE_EMAIL_APPR")
        .add("NOTIFY_OTHER",   "RECEIVE_EMAIL_NOTIFY_OTHERS")
        .add("RECGMN",         "RECEIVE_EMAIL_MGR_REC")
        .add("RECGPX",         "RECEIVE_EMAIL_REC")
        .add("POINT_LOAD",         "RECEIVE_EMAIL_REC")
        .add("WEEKLY_DIGEST",  "RECEIVE_EMAIL_DIGEST")
        .add("MILESTONE_REMINDER", "RECEIVE_EMAIL_MILESTONE_REMINDER")
        .build()
    ;
    
    protected static List<String> privacyVfNames = Arrays.asList(
            VfName.SHARE_BDAY.name(), VfName.SHARE_REC.name(), VfName.SHARE_SA.name());

    protected static List<String> profileVfNames = Arrays.asList(
            VfName.VERBAL_REC_1.name(), VfName.VERBAL_REC_2.name(), VfName.VERBAL_REC_3.name(),
            VfName.WRITTEN_REC_1.name(), VfName.WRITTEN_REC_2.name());
    
    protected static List<String> profilePrivacyVfNames = Arrays.asList(
            VfName.SHARE_BDAY.name(), VfName.SHARE_REC.name(), VfName.SHARE_SA.name(),
            VfName.VERBAL_REC_1.name(), VfName.VERBAL_REC_2.name(), VfName.VERBAL_REC_3.name(),
            VfName.WRITTEN_REC_1.name(), VfName.WRITTEN_REC_2.name());
    
    protected static List<String> otherVfNames = Arrays.asList(
            VfName.BIRTH_DAY.name(), VfName.DEFAULT_LOCALE.name(), VfName.HIRE_DATE.name());
    
    @Override
    public DetailProfileInfo getDetailProfileInfoByPaxId(Long paxId) {
        if (paxId == null) return null;

        DetailProfileInfo detailProfile = new DetailProfileInfo();
        ProfileDemographics profileDemographics = getProfileDemographicsByPaxId(paxId);

        //Add all of the demographic information from the profileDemographics object
        detailProfile.setPax(profileDemographics.getPax());

        detailProfile.setAddresses(profileDemographics.getAddresses());
        detailProfile.setEmails(profileDemographics.getEmails());
        detailProfile.setSysUsers(profileDemographics.getSysUsers());
        detailProfile.setPhones(profileDemographics.getPhones());

        List<PaxMiscDTO> paxMiscs = new ArrayList<>();
        paxMiscs.addAll(getPaxMiscData(paxId, otherVfNames));
        paxMiscs.addAll(getProfilePrivacyByPaxId(paxId).getPaxMiscs());
        detailProfile.setPaxMiscs(paxMiscs);

        Relationship  relationship = relationshipRepository.findBy()
            .where(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.toString())
            .and(ProjectConstants.PAX_ID_1).eq(paxId)
            .findOne();
        if (relationship != null && relationship.getPaxId2() != null) {
            EmployeeDTO managerPax = participantInfoDao.getEmployeeDTO(relationship.getPaxId2());
            detailProfile.setManagerPax(managerPax);
        }

        return detailProfile;
    }

    @Override
    public DetailProfileInfo getDetailProfileInfo() {
        return getDetailProfileInfoByPaxId(security.getPaxId());
    }

    @Override
    public ProfileDemographics getProfileDemographicsByPaxId(Long paxId) {
        if (paxId == null) return null;

        ProfileDemographics profileDemographics = new ProfileDemographics();
        profileDemographics.setPax(participantInfoDao.getEmployeeDTO(paxId));

        //show details only if requesting user is an admin or the logged in user or a Speetra/PulseM user
        boolean showDetails = security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE,
                ProjectConstants.PULSEM_ROLE) || security.isMyPax(paxId);

        if(showDetails){
            List<Address> addrList = addressRepository.findByPaxId(paxId);
            if (addrList != null) {
                List<AddressDTO> addressList = new ArrayList<>();
                for (Address addr : addrList) {
                    //Strip out info we don't want
                    AddressDTO addressDTO = new AddressDTO();
                    BeanUtils.copyProperties(addr, addressDTO);
                    addressList.add(addressDTO);
                }
                profileDemographics.setAddresses(addressList);
            }

            List<SysUser> suList = sysUserRepository.findByPaxId(paxId);
            if (suList != null) {
                List<SysUserDTO> sysUserList = new ArrayList<>();
                for (SysUser su : suList) {
                    //Strip out info we don't want
                    SysUserDTO sysUserDTO = new SysUserDTO();
                    BeanUtils.copyProperties(su, sysUserDTO);
                    //Save STATUS_TYPE_CODE in the STATUS field
                    sysUserDTO.setStatus(su.getStatusTypeCode());
                    sysUserList.add(sysUserDTO);
                }
                profileDemographics.setSysUsers(sysUserList);
            }

            List<Phone> phList = phoneRepository.findByPaxId(paxId);
            if (phList != null) {
                List<PhoneDTO> phoneList = new ArrayList<>();
                for (Phone ph : phList) {
                    //Strip out info we don't want
                    PhoneDTO phoneDTO = new PhoneDTO();
                    BeanUtils.copyProperties(ph, phoneDTO);
                    phoneList.add(phoneDTO);
                }
                profileDemographics.setPhones(phoneList);
            }

            //Emails are handled in their own method (has its own endpoint)
            profileDemographics.setEmails(getEmailsByPaxId(paxId));
        }
        return profileDemographics;
    }

    @Override
    public ProfileDemographics getProfileDemographics() {
        return getProfileDemographicsByPaxId(security.getPaxId());
    }

    @Override
    public List<EmailDTO> getEmailsByPaxId(Long paxId) {
        if (paxId == null) return null;

        List<EmailDTO> emails = new ArrayList<>();

        //show details only if requesting user is an admin or the logged in user
        boolean showDetails = security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE,
                ProjectConstants.PULSEM_ROLE) || security.isMyPax(paxId);

        if(showDetails){
            Iterable<Email> emList = emailRepository.findByPaxId(paxId);
            if (emList != null) {
                for (Email em : emList) {
                    //Strip out info we don't want
                    EmailDTO emailDTO = new EmailDTO();
                    BeanUtils.copyProperties(em, emailDTO);
                    //See if email address is editable
                    emailDTO.setEditable(!emailDTO.getEmailTypeCode().equalsIgnoreCase(EmailType.BUSINESS.getCode()));
                    emails.add(emailDTO);
                }
            }
        }
        return emails;
    }

    @Override
    public List<EmailDTO> getEmails() {
        return getEmailsByPaxId(security.getPaxId());
    }

    @Override
    @Transactional
    public List<EmailDTO> updateEmails(Long paxID, EmailPasswordWrapperDTO emailPasswordWrapperDTO) {
        boolean ssoEnabled = Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED));
        if (ssoEnabled || emailPasswordWrapperDTO.getPassword() == null || emailPasswordWrapperDTO.getPassword().length() == 0) {
            //password is null, should be first time login, SSO user or profile is not completed.
            if (ssoEnabled || security.isNewUser() || profileIncompleteService.isProfileIncomplete(paxID)) {
                //first time login, insert without password
                return saveEmailForUpdate(paxID, emailPasswordWrapperDTO);
            }
            
            throw new ErrorMessageException().addErrorMessage(ProfileConstants.INVALID_NOT_FIRST_TIME_MESSAGE);
        }
        
        authenticationService.reAuthenticate(emailPasswordWrapperDTO.getPassword());
        return saveEmailForUpdate(paxID, emailPasswordWrapperDTO);
    }

    protected List<EmailDTO> saveEmailForUpdate(long authenticatedPaxID, EmailPasswordWrapperDTO emailPasswordWrapperDTO) {
        List<ErrorMessage> errors = new ArrayList<>();
        for (EmailDTO emailDTO : emailPasswordWrapperDTO.getEmails()) {

            Long emailDTOPaxId = emailDTO.getPaxId();
            Long emailDTOEmailId = emailDTO.getEmailId();
            String emailDTOTypeCode = emailDTO.getEmailTypeCode();

            // paxId validation
            if (emailDTOPaxId == null) {
                errors.add(ProfileConstants.MISSING_PAX_ID_MESSAGE);
            } else if (authenticatedPaxID != emailDTOPaxId) {
                errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
            }

            // emailId validation
            if(emailDTOEmailId == null) {
                errors.add(ProfileConstants.MISSING_EMAIL_ID_MESSAGE);
            }

            // type code validation
            if(emailDTOTypeCode == null) {
                errors.add(ProfileConstants.MISSING_EMAIL_TYPE_CODE_MESSAGE);
            } else if(!ProfileConstants.VALID_EMAIL_TYPE_CODES.contains(emailDTOTypeCode)) {
                errors.add(ProfileConstants.INVALID_EMAIL_TYPE_CODE_MESSAGE);
            }

            // preferred field validation
            if(!ProfileConstants.VALID_PREFERRED_VALUES.contains(emailDTO.getPreferred())) {
                errors.add(ProfileConstants.INVALID_PREFERRED_VALUE_MESSAGE);
            }

            // editable field validation
            if(emailDTO.isNullEditable()){
                errors.add(ProfileConstants.MISSING_PARAMETER_EDITABLE_MESSAGE);
            }

            // only attempt to initiate update if there are no obvious errors
            if (errors.size() == 0) {
                if (emailDTO != null && emailDTO.isNullEditable()==false && emailDTO.getEditable() == true) {
                    if (emailRepository.exists(emailDTOEmailId)) {
                        //Update existing email
                        Email emailEntity = emailRepository.findOne(emailDTOEmailId);

                        Long existingPaxId = emailEntity.getPaxId();
                        String existingEmailTypeCode = emailEntity.getEmailTypeCode();

                        // verify that correct email is being updated
                        if(!emailDTOPaxId.equals(existingPaxId) || !emailDTOTypeCode.equals(existingEmailTypeCode)) {
                            errors.add(ProfileConstants.INCORRECT_EMAIL_ID_MESSAGE);
                        } else {
                            BeanUtils.copyProperties(emailDTO, emailEntity);
                            emailRepository.save(emailEntity);
                        }
                    } else {
                        errors.add(ProfileConstants.INVALID_EMAIL_ID_MESSAGE);
                    }
                }
            }

            ErrorMessageException.throwIfHasErrors(errors);
        }
        return getEmailsByPaxId(authenticatedPaxID);
    }

    @Override
    @Transactional
    public List<EmailDTO> insertEmail(Long paxID, @Valid EmailPasswordWrapperDTO emailPasswordWrapperDTO) throws Exception {
        boolean ssoEnabled = Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED));
        if (ssoEnabled || emailPasswordWrapperDTO.getPassword() == null || emailPasswordWrapperDTO.getPassword().length() == 0) {
            //password is null, should be first time login, it is a SSO user or profile is incomplete.
            if (ssoEnabled || security.isNewUser()|| profileIncompleteService.isProfileIncomplete(paxID)) {
                //first time login or SSO user, insert without password
                return saveEmailForInsert(paxID, emailPasswordWrapperDTO);
            }

            throw new ErrorMessageException(new ErrorMessage()
                .setCode(ProfileConstants.INVALID_NOT_FIRST_TIME)
                .setField("password")
                .setMessage(ProfileConstants.INVALID_NOT_FIRST_TIME_MSG));
        }
        
        authenticationService.reAuthenticate(emailPasswordWrapperDTO.getPassword());
        return saveEmailForInsert(paxID, emailPasswordWrapperDTO);
    }

    private List<EmailDTO> saveEmailForInsert(long authenticatedPaxID, @Valid EmailPasswordWrapperDTO emailPasswordWrapperDTO) {
        ArrayList<ErrorMessage> errors = new ArrayList<>();

        for (EmailDTO emailDTO : emailPasswordWrapperDTO.getEmails()) {

            if (emailDTO != null) {
                if(emailDTO.getPaxId() == null) {
                    errors.add(ProfileConstants.MISSING_PAX_ID_MESSAGE);
                }
                else {
                    Email email = emailRepository.findByPaxIdAndEmailTypeCode(emailDTO.getPaxId(), EmailType.HOME.getCode());
                    if (email != null) {
                        errors.add(ProfileConstants.DUPLICATE_EMAIL_TYPE_MESSAGE);
                    }

                    if (authenticatedPaxID != emailDTO.getPaxId()) {
                        errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
                    }
                }
            }

            ErrorMessageException.throwIfHasErrors(errors);

            //The HOME email should be PREFERRED = Y if there is no BUSINESS email present
            //(BUSINESS will be PREFERRED = Y if it exists)
            String preferred = ProjectConstants.YES_CHAR;
            Email email = emailRepository.findByPaxIdAndEmailTypeCode(emailDTO.getPaxId(), EmailType.BUSINESS.getCode());
            if (email != null) {
                preferred = ProjectConstants.NO_CHAR;
            }

            //Insert new email
            Email emailEntity = new Email();
            emailEntity.setPaxId(emailDTO.getPaxId());
            emailEntity.setEmailAddress(emailDTO.getEmailAddress());
            emailEntity.setPreferred(preferred);
            emailEntity.setEmailTypeCode(EmailType.HOME.getCode());
            emailRepository.save(emailEntity);
        }

        return getEmailsByPaxId(authenticatedPaxID);
    }

    @Override
    public PaxMiscListDTO getProfilePrivacyByPaxId(Long paxId) {
        if (paxId == null) return null;

        PaxMiscListDTO paxMiscListDTO = new PaxMiscListDTO()
            .setPaxMiscs(getPaxMiscData(paxId, profilePrivacyVfNames))
        ;

        // unsubscribe logic was moved to EVENT_COMMUNICATION, so we need to parse that table for the others
        /*
         * Tuple4 result structure:
         * A = event communication id (primary key)
         * B = event type code (NOTIFY_OTHER etc)
         * C = communication type code (EMAIL or SMS)
         * D = status type code (as expected)
         */
        // Filter results to only email records, convert to pax miscs (look up vf name based on event type code)
        // Status of ACTIVE = true, else false
        paxMiscListDTO.getPaxMiscs().addAll(
            paxEventCommunicationRepository.findParticipantEventCommunicationPreferences(paxId, EVENT_TYPE_CODE_TO_VF_NAME_MAP.keySet()).stream()
                //only look at email communication types
                .filter(t4 -> t4.getC().equalsIgnoreCase(CommunicationTypeCode.EMAIL.name()))
                //turn the data into a PaxMiscDTO
                .map(t4 -> {
                    PaxMiscDTO dto = new PaxMiscDTO();
                    dto.setPaxId(paxId);
                    dto.setVfName(EVENT_TYPE_CODE_TO_VF_NAME_MAP.get(t4.getB()));
                    dto.setMiscData(t4.getD() != null && t4.getD().equalsIgnoreCase(StatusTypeCode.ACTIVE.name()) ? ProjectConstants.TRUE : ProjectConstants.FALSE);
                    return dto;
                })
                //collect them into a map where key = vf_name and value = dto ignoring the dups
                .collect(toMapIgnoreDups(dto -> dto.getVfName(), dto -> dto))
                //extract the unique set of dtos (by vf_name)
                .values()
        );

        return paxMiscListDTO;
    }

    @Override
    public PaxMiscListDTO getProfilePrivacy() {
        return getProfilePrivacyByPaxId(security.getPaxId());
    }

    @Override
    @Transactional
    public List<PaxMiscDTO> updateProfilePrivacy(Long authenticatedPaxID, List<PaxMiscDTO> paxMiscDTOList) {
        ArrayList<ErrorMessage> errors = new ArrayList<>();

        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            if (!authenticatedPaxID.equals(paxMiscDTO.getPaxId())) {
                errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
            }

            // vfName validation
            if (!VfName.getPaxMiscVfNameList().contains(paxMiscDTO.getVfName())) {
                errors.add(ProfileConstants.INVALID_PARAMETER_MESSAGE);
            }

            // null check / data check
            if (!ProjectConstants.TRUE.equalsIgnoreCase(paxMiscDTO.getMiscData())
                    && !ProjectConstants.FALSE.equalsIgnoreCase(paxMiscDTO.getMiscData())) {
                errors.add(ProfileConstants.INVALID_DATA_MESSAGE);
            }
        }

        Long proxyPaxId = null;
        if (security.isImpersonated()) {
            proxyPaxId = checkProxyPermissions().getProxyPaxId();
            if (proxyPaxId == null) {
                errors.add(NominationConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
            }
        }

        // Check for errors and if any exists, an exception will be thrown
        ErrorMessageException.throwIfHasErrors(errors);

        savePaxEventCommunicationPreferences(authenticatedPaxID, proxyPaxId, paxMiscDTOList);

        return getProfilePrivacy().getPaxMiscs();
    }

    @Override
    @Transactional
    public List <PaxMiscDTO> insertProfilePrivacy(Long authenticatedPaxID, List<PaxMiscDTO> paxMiscDTOList) {
        if (paxMiscDTOList != null) {

            ArrayList<ErrorMessage> errors = new ArrayList<>();

            for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
                if (!authenticatedPaxID.equals(paxMiscDTO.getPaxId())) {
                    errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
                }
                if (!EnumUtils.isValidEnum(VfName.class, paxMiscDTO.getVfName())) {
                    errors.add(ProfileConstants.INVALID_PARAMETER_MESSAGE);
                }

                if (!paxMiscDTO.getMiscData().equalsIgnoreCase(ProjectConstants.TRUE)
                        && !paxMiscDTO.getMiscData().equalsIgnoreCase(ProjectConstants.FALSE)) {
                    errors.add(ProfileConstants.INVALID_DATA_MESSAGE);
                }

                PaxMisc paxMiscEntity = paxMiscRepository.findOne(paxMiscDTO.getPaxId(), paxMiscDTO.getVfName());
                if (paxMiscEntity != null) {
                    errors.add(ProfileConstants.DUPLICATE_VFNAME_MESSAGE);
                }
            }

            Long proxyPaxId = null;
             if (security.isImpersonated()) {
                proxyPaxId = checkProxyPermissions().getProxyPaxId();
                if (proxyPaxId == null) {
                    errors.add(NominationConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
                }
            }

            //Check for errors and if any exists, an exception will be thrown
            ErrorMessageException.throwIfHasErrors(errors);

            savePaxEventCommunicationPreferences(authenticatedPaxID, proxyPaxId, paxMiscDTOList);
        }

        return getProfilePrivacy().getPaxMiscs();
    }

    protected void savePaxEventCommunicationPreferences(Long authenticatedPaxID, Long proxyPaxId, List<PaxMiscDTO> paxMiscDTOList) {

        /*
         * create a map built from paxMiscDTOList records that contains 2 lists:
         * one for the actual pax misc records (key=TRUE)
         * one for the pax_event_communication records (key=FALSE)
         *
         */
        Collection<String> vfNames = EVENT_TYPE_CODE_TO_VF_NAME_MAP.values();
        Map<Boolean,List<PaxMiscDTO>> preferencesMap = paxMiscDTOList.stream()
            .collect(groupingBy(pm -> !vfNames.contains(pm.getVfName()), toList()))
        ;

        // process the actual pax misc records (if there are any)
        List<PaxMiscDTO> paxMiscList = preferencesMap.get(Boolean.TRUE);
        if (paxMiscList != null) {
            List<PaxMisc> paxMiscEntityList = new ArrayList<>();
            for (PaxMiscDTO paxMiscDTO : paxMiscList) {

                PaxMisc paxMiscEntity = paxMiscRepository.findOne(paxMiscDTO.getPaxId(), paxMiscDTO.getVfName());
                // Create new entity if one does not previously exist
                if (paxMiscEntity == null) {
                    paxMiscEntity = new PaxMisc();
                    paxMiscEntity.setPaxId(paxMiscDTO.getPaxId());
                    paxMiscEntity.setVfName(paxMiscDTO.getVfName());
                }

                paxMiscEntity.setMiscData(paxMiscDTO.getMiscData());
                paxMiscEntity.setMiscDate(new Date());
                if (proxyPaxId != null) {
                    paxMiscEntity.setProxyPaxId(proxyPaxId);
                }
                paxMiscEntityList.add(paxMiscEntity);
            }
            paxMiscRepository.save(paxMiscEntityList);
        }

        // unsubscribe logic was moved to PAX_EVENT_COMMUNICATION, so we need to parse that table for the others
         /*
          * Tuple4 result structure:
          * A = event communication id (primary key)
          * B = event type code (NOTIFY_OTHER etc)
          * C = communication type code (EMAIL or SMS)
          * D = status type code (as expected)
          */
        //process the pax_event_communication records (if there are any)
        List<PaxMiscDTO> eventCommunicationDTOList = preferencesMap.get(Boolean.FALSE);
        if (eventCommunicationDTOList != null) {

            //build a map of vf_name to the tuple4 objects they map to (each vf_name may map to more than 1 t4 record)
            Map<String, List<Tuple4<Long,String,String,String>>> paxEventCommunicationMap =
                paxEventCommunicationRepository.findParticipantEventCommunicationPreferences(authenticatedPaxID, EVENT_TYPE_CODE_TO_VF_NAME_MAP.keySet()).stream()
                .collect(Collectors.groupingBy(t4 -> EVENT_TYPE_CODE_TO_VF_NAME_MAP.get(t4.getB()), Collectors.toList()))
            ;

            List<PaxEventCommunication> paxEventCommunicationList = new ArrayList<>();
            for (PaxMiscDTO paxMiscDTO : eventCommunicationDTOList) {

                // Update existing if it exists, otherwise create
                List<Tuple4<Long,String,String,String>> t4s = paxEventCommunicationMap.get(paxMiscDTO.getVfName());

                for (Tuple4<Long,String,String,String> t4: t4s) {
                    PaxEventCommunication paxEventCommunication = paxEventCommunicationRepository.findByPaxIdAndEventCommunicationId(authenticatedPaxID, t4.getA());
                    if (paxEventCommunication == null) {
                        paxEventCommunication = new PaxEventCommunication()
                            .setEventCommunicationId(t4.getA())
                            .setPaxId(authenticatedPaxID)
                        ;
                    }

                    // set updated status
                    paxEventCommunicationList.add(paxEventCommunication
                        .setStatusTypeCode(paxMiscDTO.getMiscData().equalsIgnoreCase(ProjectConstants.TRUE) ? StatusTypeCode.ACTIVE.name() : StatusTypeCode.OPT_OUT.name())
                    );
                }
            }

            paxEventCommunicationRepository.save(paxEventCommunicationList);
        }
    }

    @Override
    public PaxMiscListDTO getProfilePreferencesByPaxId(Long paxId) {
        if (paxId == null) return null;

        return new PaxMiscListDTO()
            .setPaxMiscs(getPaxMiscData(paxId, profileVfNames))
        ;
    }

    @Override
    public PaxMiscListDTO getProfilePreferences() {
        return getProfilePreferencesByPaxId(security.getPaxId());
    }

    @Override
    @Transactional
    public List <PaxMiscDTO> updateProfilePreferences(Long authenticatedPaxID,List<PaxMiscDTO> paxMiscDTOList) {
        if (paxMiscDTOList != null) {
            ArrayList<ErrorMessage> errors = new ArrayList<>();
            for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
                if (paxMiscDTO != null) {
                    if (!authenticatedPaxID.equals(paxMiscDTO.getPaxId())) {
                        errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
                    }

                    // vfName validation
                    if (!StringUtils.isIn(paxMiscDTO.getVfName(), profileVfNames)) {
                        errors.add(ProfileConstants.INVALID_PARAMETER_MESSAGE);
                    }

                    // null check / data check
                    if (!ProjectConstants.TRUE.equalsIgnoreCase(paxMiscDTO.getMiscData())
                            && !ProjectConstants.FALSE.equalsIgnoreCase(paxMiscDTO.getMiscData())) {
                        errors.add(ProfileConstants.INVALID_DATA_MESSAGE);
                    }

                    if (errors.isEmpty()) {
                        PaxMisc paxMiscEntity = paxMiscRepository.findOne(paxMiscDTO.getPaxId(),paxMiscDTO.getVfName());
                        paxMiscEntity.setMiscData(paxMiscDTO.getMiscData());
                        paxMiscRepository.save(paxMiscEntity);
                    } else {
                        //Check for errors and if any exists, an exception will be thrown
                        ErrorMessageException.throwIfHasErrors(errors);
                    }
                }
            }
        }
        return getProfilePreferences().getPaxMiscs();
    }

    @Override
    @Transactional
    public List<PaxMiscDTO> insertProfilePreferences(Long authenticatedPaxID, List<PaxMiscDTO> paxMiscDTOList) {
        if(paxMiscDTOList != null){
            ArrayList<ErrorMessage> errors = new ArrayList<>();
            for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
                if(paxMiscDTO != null){
                    if (!authenticatedPaxID.equals(paxMiscDTO.getPaxId())) {
                        errors.add(ProfileConstants.INVALID_PAX_ID_MESSAGE);
                    }

                    if (!StringUtils.isIn(paxMiscDTO.getVfName(), profileVfNames)) {
                        errors.add(ProfileConstants.INVALID_PARAMETER_MESSAGE);
                    }

                    if (!paxMiscDTO.getMiscData().equalsIgnoreCase(ProjectConstants.TRUE)
                            && !paxMiscDTO.getMiscData().equalsIgnoreCase(ProjectConstants.FALSE)) {
                        errors.add(ProfileConstants.INVALID_DATA_MESSAGE);
                    }

                    PaxMisc paxMiscEntity = paxMiscRepository.findOne(paxMiscDTO.getPaxId(), paxMiscDTO.getVfName());
                    if (paxMiscEntity != null) {
                        errors.add(ProfileConstants.DUPLICATE_VFNAME_MESSAGE);
                    }

                    if (errors.isEmpty()) {
                        paxMiscEntity = new PaxMisc();
                        paxMiscEntity.setPaxId(paxMiscDTO.getPaxId());
                        paxMiscEntity.setVfName(paxMiscDTO.getVfName());
                        paxMiscEntity.setMiscData(paxMiscDTO.getMiscData());
                        paxMiscEntity.setMiscDate(new Date());
                        paxMiscRepository.save(paxMiscEntity);
                    } else {
                        //Check for errors and if any exists, an exception will be thrown
                        ErrorMessageException.throwIfHasErrors(errors);
                    }
                }
            }
        }

        return getProfilePreferences().getPaxMiscs();
    }

    @Override
    @Transactional
    public LocaleDTO updatePaxLanguageAndLocale(Long paxId, LocaleDTO localeDTO) {
        if (paxId == null || localeDTO == null) {
            throw new ErrorMessageException().setStatus(HttpStatus.NOT_FOUND);
        }

        List<ErrorMessage> errors = validateLanguageAndLocale(localeDTO);

        ErrorMessageException.throwIfHasErrors(errors);

        String localeCode = localeInfoDao.findBy()
                .where(ProjectConstants.LOCALE_CODE).eq(localeDTO.getLanguageCode())
                .and(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .findOne(ProjectConstants.LOCALE_CODE, String.class);

        if (localeCode == null) {
            throw new ErrorMessageException().setStatus(HttpStatus.NOT_FOUND);
        }

        String languageCode = LocaleUtil.extractLanguageCodeFromLocaleCode(localeCode);

        Pax pax = paxDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .findOne();
        pax.setLanguageCode(languageCode.toLowerCase());
        pax.setPreferredLocale(localeCode);
        paxDao.save(pax);

        // Create new return object off saved pax entity,
        // for the sake of ensuring the data mirrors the database,
        // not just the request
        LocaleDTO response = new LocaleDTO();
        response.setLanguageCode(pax.getPreferredLocale());

        return response;
    }

    /**
     * Method to validate a languageCode and preferredLocale against the database
     * @param localeDTO
     * @return list of validation errors
     */
    private List<ErrorMessage> validateLanguageAndLocale(LocaleDTO localeDTO) {
        List<ErrorMessage> errors = new ArrayList<>();

        if (localeDTO == null || localeDTO.getLanguageCode() == null) {
            errors.add(ProfileConstants.MISSING_LANGUAGE_CODE_MESSAGE);
            return errors;
        }

        String formattedLocaleCode = LocaleUtil.formatLocaleCode(localeDTO.getLanguageCode());

        if (formattedLocaleCode == null) {
            errors.add(ProfileConstants.MALFORMED_LANGUAGE_CODE_MESSAGE);
            return errors;
        }

        LocaleInfo localeInfo = localeInfoDao.findBy()
                .where(ProjectConstants.LOCALE_CODE).eq(formattedLocaleCode)
                .findOne();

        if (localeInfo == null) {
            errors.add(ProfileConstants.INVALID_LANGUAGE_CODE_MESSAGE);
        } else if (!StatusTypeCode.ACTIVE.name().equals(localeInfo.getDisplayStatusTypeCode())) {
            errors.add(ProfileConstants.INACTIVE_LANGAUGE_CODE_MESSAGE);
        }

        return errors;
    }

    /**
     * Common method to return pax misc data
     * @param paxId
     * @param miscDataList : filter for misc data,if it's null value, filter is removed.
     * @return
     */
    private List<PaxMiscDTO> getPaxMiscData(Long paxId, List<String> miscDataList) {
        if (paxId == null) return null;

        // Get all other information needed for profile
        List<PaxMiscDTO> paxMiscDTOs = PaxMiscDTO.fromPaxMiscs(paxMiscRepository.findAll(paxId, miscDataList));
        applyPrivacySettings(paxId, paxMiscDTOs);
        return paxMiscDTOs;
    }

    private void applyPrivacySettings(Long paxId, Collection<PaxMiscDTO> paxMiscs) {

        // show details only if requesting user is an admin or the logged in user
        boolean showDetails = security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE) || security.isMyPax(paxId);
        boolean hideHireDate = false;
        PaxMiscDTO hireDateMisc = null;
        boolean hideBirthday = false;
        PaxMiscDTO birthdayMisc = null;

        for (PaxMiscDTO paxMisc : paxMiscs) {

            // verify if HIRE-DATE data is not shared.
            if (paxMisc.getVfName().equals(VfName.SHARE_SA.name())) {
                hideHireDate = !Boolean.parseBoolean(paxMisc.getMiscData());
            }

            // find index for HIRE_DATE misc.
            if (paxMisc.getVfName().equals(VfName.HIRE_DATE.name())) {
                hireDateMisc = paxMisc;
            }

            // verify if BIRTHDAY data is not shared.
            if (paxMisc.getVfName().equals(VfName.SHARE_BDAY.name())) {
                hideBirthday = !Boolean.parseBoolean(paxMisc.getMiscData());
            }

            // find index for BIRTHDAY misc.
            if (paxMisc.getVfName().equals(VfName.BIRTH_DAY.toString())) {
                birthdayMisc = paxMisc;
            }
        }

        // hide hire-date for other users if hide flag has been set to true.
        if (!showDetails && hideHireDate && hireDateMisc != null) {
            paxMiscs.remove(hireDateMisc);
        }

        // hide birthday for other users if hide flag has been set to true.
        if (!showDetails && hideBirthday && birthdayMisc != null) {
            paxMiscs.remove(birthdayMisc);
        }
    }

    /**
     * Determine if impersonator can update user's privacy settings and set the proxy pax id
     */
    public PaxMiscDTO checkProxyPermissions() {
        PaxMiscDTO paxMiscDTO = new PaxMiscDTO();

        if (proxyService.doesPaxHaveProxyPermission(PermissionConstants.PROXY_SETTINGS, security.getPaxId(),
                security.getImpersonatorPaxId())
                || security.impersonatorHasPermission(PermissionManagementTypes.PROXY_SETTINGS_ANYONE.name())) {
            // impersonator is a proxy of the submitter pax who has the PROXY_SETTINGS permission
            // or the impersonator has the PROXY_SETTINGS_ANYONE permission
            paxMiscDTO.setProxyPaxId(security.getImpersonatorPaxId());
        }

        return paxMiscDTO;
    }

    @Override
    public PaginatedResponseObject<EmployeeDTO> getManagerDirectReports(
            PaginationRequestDetails requestDetails, Long paxId, boolean excludeInactive, Integer pageNumber, Integer pageSize
        ) {
        List<Map<String, Object>> resultList = managerDirectReportDao.getManagerDirectReports(paxId, excludeInactive, pageNumber, pageSize);
        List<EmployeeDTO> employeeList = new ArrayList<>();

        Integer totalResults = 0;
        for (Map<String, Object> result : resultList) {
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            employeeList.add(new EmployeeDTO(result, null));
        }

        return new PaginatedResponseObject<>(requestDetails, employeeList, totalResults);
    }
}
