package com.maritz.culturenext.profile.services;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public interface PaxSearchService {
    
    /**
     * @param searchString
     * @param groupsString
     * @param statusesString
     * @param page
     * @param size
     * @return
     * 
     * TODO see if we can remove CURRENT version
     */
    List<EmployeeDTO> searchPax(String searchString, String groupsString, String statusesString, Integer page,
            Integer size, Integer budgetId);
    
    /**
     * Search for participants matching the parameters
     * 
     * @param searchString
     * @param groupsString
     * @param statusesString
     * @param programId
     * @param excludeSelf
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847563/Participant+Search+Example
     */
    List<EmployeeDTO> searchPax(String searchString, String groupsString, String statusesString, Integer programId, 
             Boolean excludeSelf, Integer page, Integer size, Integer budgetId);
}
