package com.maritz.culturenext.profile.services.impl;

import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.inject.Inject;

import com.maritz.core.util.StringUtils;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.PaxFiles;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StreamUtils;
import com.maritz.core.util.stream.PNGStreamingOutput;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.profile.dao.PaxImageDao;
import com.maritz.culturenext.profile.dto.ProfilePictureDTO;
import com.maritz.culturenext.profile.services.PaxImageService;
import com.maritz.culturenext.util.ImageManipulationUtil;

@Component
public class PaxImageServiceImpl implements PaxImageService {

    @Inject private PaxImageDao paxImageDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<FileItem> fileItemDao;
    @Inject private ConcentrixDao<PaxFiles> paxFilesDao;

    @Inject private FileImageService fileImageService;

    private static final List<String> SUPPORTED_IMAGE_SIZES =
            Arrays.asList("default", "tiny");

    private static final List<String> SUPPORTED_IMAGE_TYPES =
            Arrays.asList("image/jpeg", "image/png", "image/jpg");

    private static final List<String> SUPPORTED_IMAGE_EXTENSIONS =
            Arrays.asList("jpeg", "jpg", "png");


    //TODO MP-7998 clean up this file

    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;

    @Inject
    public PaxImageServiceImpl setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public PaxImageServiceImpl setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    @Override
    public ResponseEntity<Object> getImage(Long paxId, String size ) throws Exception {
        String imageSize = size;
        if(imageSize == null || !SUPPORTED_IMAGE_SIZES.contains(imageSize.toLowerCase())){
            imageSize = "default";
        }

        Files imageEntry = paxImageDao.getProfileImageByIdAndType(paxId, imageSize);
        if (imageEntry == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return fileImageService.prepareProfileImageForResponse(paxId, imageEntry);
    }

    @Override
    public ProfilePictureDTO uploadImage(Long paxId, String crop, MultipartFile[] images, Boolean circleCrop) throws Exception{

        ArrayList<ErrorMessage> errors = new ArrayList<>();

        if (images == null || images.length == 0) {
            errors.add(ImageConstants.IMAGE_MISSING_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        else if (images.length > 1) {
            errors.add(ImageConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        MultipartFile image = images[0];

        //Get filename
        String fImage = image.getOriginalFilename();
        String fileNameExtension = FilenameUtils.getExtension(String.valueOf(fImage));
        if (!StringUtils.isIn(fileNameExtension, SUPPORTED_IMAGE_EXTENSIONS)) {
            errors.add(ImageConstants.INVALID_IMAGE_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        String contentType = image.getContentType();

        BufferedImage bufferedImage = null;
        if(contentType == null || !SUPPORTED_IMAGE_TYPES.contains(contentType.toLowerCase())){
            errors.add(ImageConstants.CONTENT_TYPE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        else{
            ImageReader imageReader = null;
            MemoryCacheImageInputStream mciis = null;
            try{
                Iterator<ImageReader> imageReaders = ImageIO.getImageReadersByMIMEType(contentType);
                if(imageReaders != null && imageReaders.hasNext()){
                    imageReader = imageReaders.next();
                }
                else{
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if(image.getBytes() != null){
                    mciis = new MemoryCacheImageInputStream(new ByteArrayInputStream(image.getBytes()));
                }
                else{
                    errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                    throw new ErrorMessageException().addErrorMessages(errors);
                }

                if(imageReader != null && mciis != null){
                    imageReader.setInput(mciis);
                    if(imageReader.getNumImages(true) > 1){
                        errors.add(ImageConstants.IMAGE_UPLOAD_COUNT_MESSAGE);
                        throw new ErrorMessageException().addErrorMessages(errors);
                    }
                    bufferedImage = imageReader.read(0);
                }


            }
            catch(Exception e){
                errors.add(ImageConstants.IMAGE_PROCESS_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
            finally{
                if(mciis != null){
                    mciis.close();
                }
            }
        }
        if(crop != null){
            String[] cropSplit = crop.split(",");
            if(cropSplit.length != 4){
                errors.add(ImageConstants.CROP_PARAMETER_COUNT_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }

            Integer x = null;
            Integer y = null;
            Integer width = null;
            Integer height = null;
            try{
                x = Integer.parseInt(cropSplit[0]);
            }
            catch(NumberFormatException e){
                errors.add(ImageConstants.CROP_X_PARAM_MESSAGE);
            }
            try{
                y = Integer.parseInt(cropSplit[1]);
            }
            catch(NumberFormatException e){
                errors.add(ImageConstants.CROP_Y_PARAM_MESSAGE);
            }
            try{
                width = Integer.parseInt(cropSplit[2]);
            }
            catch(NumberFormatException e){
                errors.add(ImageConstants.CROP_WIDTH_PARAM_MESSAGE);
            }
            try{
                height = Integer.parseInt(cropSplit[3]);
            }
            catch(NumberFormatException e){
                errors.add(ImageConstants.CROP_HEIGHT_PARAM_MESSAGE);
            }

            if(!errors.isEmpty()){
                throw new ErrorMessageException().addErrorMessages(errors);
            }
            try{
                bufferedImage = ImageManipulationUtil.crop(bufferedImage, x, y, width, height);
            }
            catch(RasterFormatException rfe){
                errors.add(ImageConstants.CROP_AREA_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
        }
        else{
            // auto-crop, roll out!
            Integer height = bufferedImage.getHeight();
            Integer width = bufferedImage.getWidth();
            Integer finalHeight = height;
            Integer finalWidth = width;
            Integer x = 0;
            Integer y = 0;

            if(height > width){
                finalHeight = width;
                y = (height - finalHeight) / 2;
            }
            else{
                finalWidth = height;
                x = (width - finalWidth) / 2;
            }

            bufferedImage = ImageManipulationUtil.crop(bufferedImage, x, y, finalWidth, finalHeight);

        }

        // All manipulations done on the same BufferedImage object to save memory
        bufferedImage = ImageManipulationUtil.resize(bufferedImage, 400, 400);
        if(circleCrop.equals(true)) {
            bufferedImage = ImageManipulationUtil.circleCrop(bufferedImage);
        }

        //Make the version a random positive integer
        //Version will be the same for Default and Tiny images
        Integer version = null;
        Random random = new Random();
        version = Math.abs(random.nextInt());

        ProfilePictureDTO profilePicDefault = saveImage(bufferedImage, "Default", paxId, version);

        return profilePicDefault;
    }

    private ProfilePictureDTO saveImage(BufferedImage bufferedImage, String type, Long paxId, Integer version) throws IOException{
        Files fileEntry = paxImageDao.getProfileImageByIdAndType(paxId, type);

        if(fileEntry == null){
            fileEntry = new Files();
            fileEntry.setFileTypeCode(paxImageDao.getImageTypeCodeByType(type));
        }

        fileEntry.setMediaType("image/png");
        filesDao.save(fileEntry);

        StreamingOutput streamingOutput = new PNGStreamingOutput(bufferedImage);
        StreamUtils.fromStreamingOutput(fileEntry, "data", streamingOutput);

        if (environmentUtil.writeToCdn()) {
            gcpUtil.uploadFile(fileEntry.getId(), streamingOutput);
        }

        PaxFiles paxFile = paxFilesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and("filesId").eq(fileEntry.getId())
                .findOne();
        if(paxFile == null) {
            paxFile = new PaxFiles();
            paxFile.setPaxId(paxId);
            paxFile.setFilesId(fileEntry.getId());
        }

        paxFile.setVersion(version);

        paxFilesDao.save(paxFile);

        ProfilePictureDTO profilePic = new ProfilePictureDTO();
        profilePic.setProfilePictureVersion(version);
        return profilePic;
    }

    @Override
    public void deleteImage(Long paxId) throws Exception {

        ArrayList<ErrorMessage> errors = new ArrayList<>();
        List<PaxFiles> paxFiles = paxFilesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .find();

        List<Long> paxFileIds = new ArrayList<>();
        if(paxFiles == null || paxFiles.iterator() == null || !paxFiles.iterator().hasNext()){
            errors.add(ImageConstants.DELETE_IMAGE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        else{
            for(PaxFiles entry : paxFiles){
                paxFileIds.add(entry.getFilesId());
            }
            paxFilesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .delete();
            
            // need this because we've got some pax images duplicated in FILE_ITEM too
            fileItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(paxId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.PAX.name())
                .and(ProjectConstants.FILES_ID).in(paxFileIds)
                .delete();
            
            filesDao.findBy()
                .where(ProjectConstants.ID).in(paxFileIds)
                .delete();
            // we're good!
        }
    }

}
