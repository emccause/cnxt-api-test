package com.maritz.culturenext.profile.services.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.jpa.repository.CnxtGroupsRepository;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.constants.GroupConstants;
import com.maritz.culturenext.profile.dao.GroupCreationValidationDao;
import com.maritz.culturenext.profile.dao.GroupDataSaveDao;
import com.maritz.culturenext.profile.dao.GroupInfoDao;
import com.maritz.culturenext.profile.dao.PaginatedGroupMembershipDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentFullDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentWithStubDTO;
import com.maritz.culturenext.profile.services.GroupsService;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GroupsServiceImpl implements GroupsService {

    @Inject private CnxtGroupsRepository cnxtGroupsRepository;
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private GroupCreationValidationDao groupCreationValidationDao;
    @Inject private GroupDataSaveDao groupDataSaveDao;
    @Inject private GroupInfoDao groupInfoDao;
    @Inject private PaginatedGroupMembershipDao paginatedGroupMembershipDao;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private Security security;
    
    private static final String GROUP_ID_ARRAY_KEY = "GROUPS";
    private static final String PAX_ID_ARRAY_KEY = "PAX";
    private static final String ERROR_ARRAY_KEY = "ERRORS";
    
    @Override
    public Map<String, Object> getGroupInfo(Long groupId) {
        return groupInfoDao.getGroupInfo(groupId);
    }

    @Override
    public List<EntityDTO> getGroupMembers(Long groupId, Boolean expandGroups, Integer page, Integer size) {

        if (expandGroups == Boolean.TRUE) {
            return PaginationUtil.getPaginatedList(
                    paginatedGroupMembershipDao.getExpandedMembership(groupId), page, size);
        } else {
            return PaginationUtil.getPaginatedList(
                    paginatedGroupMembershipDao.getMembership(groupId), page, size);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public GroupEnrollmentFullDTO createGroup(GroupEnrollmentWithStubDTO groupEnrollmentStub) {
        
        ArrayList<ErrorMessage> errors = validateGroupCreation(groupEnrollmentStub);
        
        if (!errors.isEmpty()) {
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        
        ArrayList<String> invalidGroupTypes = new ArrayList<String>();
        if (GroupType.PERSONAL.name().equals(groupEnrollmentStub.getType())){
            invalidGroupTypes.add(GroupType.HIERARCHY.name());
            invalidGroupTypes.add(GroupType.ROLE_BASED.name());
            invalidGroupTypes.add(GroupType.CUSTOM.name());
            invalidGroupTypes.add(GroupType.PERSONAL.name());
            invalidGroupTypes.add(GroupType.ENROLLMENT.name());
        } else {
            invalidGroupTypes.add(GroupType.CUSTOM.name());
            invalidGroupTypes.add(GroupType.PERSONAL.name());
        }

        Map<String, List<?>> memberResults = resolveMembers(groupEnrollmentStub.getGroupId(), 
                groupEnrollmentStub.getMembers(), invalidGroupTypes, "ADD", groupEnrollmentStub.getType());
        
        List<Long> groupIdsToAdd = (List<Long>)memberResults.get(GROUP_ID_ARRAY_KEY);
        List<Long> paxIdsToAdd = (List<Long>)memberResults.get(PAX_ID_ARRAY_KEY);
        List<ErrorMessage> memberErrors = (List<ErrorMessage>)memberResults.get(ERROR_ARRAY_KEY);

        if (!memberErrors.isEmpty()) {
            // If we want to return the other error messages other than invalid ids, 
            // don't throw but add to the object later.
            throw new ErrorMessageException().addErrorMessages(memberErrors);
        }
        
        if (groupIdsToAdd.isEmpty() && paxIdsToAdd.isEmpty()) {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.NO_VALID_MEMBERS_MESSAGE);
        }
        
        GroupDTO groupInfo = new GroupDTO();
        BeanUtils.copyProperties(groupEnrollmentStub, groupInfo);
        
        Long groupId = saveGroupAndMembers(groupInfo, paxIdsToAdd, groupIdsToAdd);
        
        GroupEnrollmentFullDTO finalGroupInfo = new GroupEnrollmentFullDTO();
        
        Map<String, Object> groupInfoMap = getGroupInfo(groupId);
        finalGroupInfo.setGroupId(groupId);
        finalGroupInfo.setGroupConfigId((Long) groupInfoMap.get(ProjectConstants.GROUP_CONFIG_ID));
        finalGroupInfo.setField((String) groupInfoMap.get(ProjectConstants.FIELD));
        finalGroupInfo.setFieldDisplayName((String) groupInfoMap.get(ProjectConstants.FIELD_DISPLAY_NAME));
        finalGroupInfo.setGroupName((String) groupInfoMap.get(ProjectConstants.GROUP_NAME));
        finalGroupInfo.setType((String) groupInfoMap.get(ProjectConstants.TYPE));
        finalGroupInfo.setVisibility((String) groupInfoMap.get(ProjectConstants.VISIBILITY));
        finalGroupInfo.setStatus((String) groupInfoMap.get(ProjectConstants.STATUS));
        finalGroupInfo.setCreateDate((String) groupInfoMap.get(ProjectConstants.CREATE_DATE));
        finalGroupInfo.setPaxCount((Integer) groupInfoMap.get(ProjectConstants.PAX_COUNT));
        finalGroupInfo.setGroupCount((Integer) groupInfoMap.get(ProjectConstants.GROUP_COUNT));
        finalGroupInfo.setTotalPaxCount((Integer) groupInfoMap.get(ProjectConstants.TOTAL_PAX_COUNT));
        finalGroupInfo.setGroupPaxId((Long) groupInfoMap.get(ProjectConstants.GROUP_PAX_ID));
        
        finalGroupInfo.setMembers(getGroupMembers(groupId, false, 0, 50)); // non-expanded member list
        
        return finalGroupInfo;
    }

    @Override
    public Map<String, Object> updateGroup( Long groupId, GroupDTO groupDto) {
        Map<String, Object> groupInfoMap = getGroupInfo(groupId);
        String groupType = (String) groupInfoMap.get(ProjectConstants.TYPE);
        Long groupPaxId = null;
        
        if (!security.hasRole(PermissionConstants.ADMIN_ROLE_NAME) 
                && !groupType.equalsIgnoreCase(GroupType.CUSTOM.name()) 
                && !groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
        }
        
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            groupPaxId = (Long) groupInfoMap.get(ProjectConstants.GROUP_PAX_ID);
            if (!groupPaxId.equals(security.getPaxId())) {
                throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
            }
        }
        
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        String groupName = (String) groupInfoMap.get(ProjectConstants.GROUP_NAME);
        
        if (!groupName.equalsIgnoreCase(groupDto.getGroupName())) {
            if (groupCreationValidationDao.isGroupNameDuplicate(groupDto.getGroupName(), groupType, groupPaxId)) {
                errors.add(GroupConstants.DUPLICATE_GROUP_NAME_MESSAGE);
            } else {
                groupName = groupDto.getGroupName();
            }
        }
        
        String status = (String) groupInfoMap.get(ProjectConstants.STATUS);
        
        if (!groupDto.getStatus().equalsIgnoreCase(StatusTypeCode.ACTIVE.name()) 
                && !groupDto.getStatus().equalsIgnoreCase(StatusTypeCode.INACTIVE.name())) {
            errors.add(GroupConstants.INVALID_GROUP_STATUS_MESSAGE);
        } else {
            status = groupDto.getStatus();
        }
        
        String visibility = groupDto.getVisibility();
        
        if (groupType.equalsIgnoreCase(GroupType.CUSTOM.name())) {
            if (visibility == null || visibility.trim().equals(ProjectConstants.EMPTY_STRING)) {
                errors.add(GroupConstants.GROUP_VISIBILITY_NULL_MESSAGE);
            } else if (!(visibility.equals("PUBLIC") || visibility.equals(PermissionConstants.ADMIN_ROLE_NAME))) {
                errors.add(GroupConstants.GROUP_VISIBILITY_INVALID_MESSAGE);
            }
        } else if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            if (visibility != null) {
                errors.add(GroupConstants.GROUP_VISIBILITY_INVALID_MESSAGE);
            }
        }
        
        if (!errors.isEmpty()) {
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        
        if (visibility != null && !visibility.equalsIgnoreCase((String) groupInfoMap.get(ProjectConstants.VISIBILITY))) {
            groupDataSaveDao.deleteVisibility(groupId);
            groupDataSaveDao.saveVisibility(groupId, visibility);
        }
        
        Groups group = groupsDao.findById(groupId);
        group.setGroupDesc(groupName);
        group.setStatusTypeCode(status);
        groupsDao.save(group);
        
        return getGroupInfo( groupId);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EntityDTO> addMembers( Long groupId, List<GroupMemberStubDTO> members) {
        Map<String, Object> groupInfoMap = getGroupInfo( groupId);
        String groupType = (String) groupInfoMap.get(ProjectConstants.TYPE);
        
        if (!security.hasRole(PermissionConstants.ADMIN_ROLE_NAME) 
                && !groupType.equalsIgnoreCase(GroupType.CUSTOM.name()) 
                && !groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
        }
        
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            Long groupPaxId = (Long) groupInfoMap.get(ProjectConstants.GROUP_PAX_ID);
            if (!groupPaxId.equals(security.getPaxId())) {
                throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
            }
        }
        
        ArrayList<String> invalidGroupTypes = new ArrayList<String>();
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            invalidGroupTypes.add(GroupType.HIERARCHY.name());
            invalidGroupTypes.add(GroupType.ROLE_BASED.name());
            invalidGroupTypes.add(GroupType.CUSTOM.name());
            invalidGroupTypes.add(GroupType.PERSONAL.name());
            invalidGroupTypes.add(GroupType.ENROLLMENT.name());
        } else {
            invalidGroupTypes.add(GroupType.PERSONAL.name());
        }
        
        if (groupType.equalsIgnoreCase(GroupType.CUSTOM.name())) {
            invalidGroupTypes.add(GroupType.CUSTOM.name());
        }
        
        Map<String, List<?>> memberResults = resolveMembers(groupId, members, invalidGroupTypes, "ADD", groupType);
        
        List<Long> groupIdsToAdd = (List<Long>)memberResults.get(GROUP_ID_ARRAY_KEY);
        List<Long> paxIdsToAdd = (List<Long>)memberResults.get(PAX_ID_ARRAY_KEY);
        List<ErrorMessage> memberErrors = (List<ErrorMessage>)memberResults.get(ERROR_ARRAY_KEY);

        if (!memberErrors.isEmpty()) {
            // If we want to return the other error messages other than invalid ids, 
            // don't throw but add to the object later.
            throw new ErrorMessageException().addErrorMessages(memberErrors);
        }
        
        if (paxIdsToAdd != null && !paxIdsToAdd.isEmpty()) {
            groupDataSaveDao.savePaxMembers(groupId, paxIdsToAdd);
        }
        
        if (groupIdsToAdd != null && !groupIdsToAdd.isEmpty()) {
            groupDataSaveDao.saveGroupMembers(groupId, groupIdsToAdd);
        }
        
        return participantInfoDao.getInfo(paxIdsToAdd, groupIdsToAdd);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void deleteMembers(Long groupId, List<GroupMemberStubDTO> members) {
        Map<String, Object> groupInfoMap = getGroupInfo( groupId);
        String groupType = (String) groupInfoMap.get(ProjectConstants.TYPE);
        
        if (!security.hasRole(PermissionConstants.ADMIN_ROLE_NAME) 
                && !groupType.equalsIgnoreCase(GroupType.CUSTOM.name()) 
                && !groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
        }
        
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            Long groupPaxId = (Long) groupInfoMap.get(ProjectConstants.GROUP_PAX_ID);
            if (!groupPaxId.equals(security.getPaxId())) {
                throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
            }
        }
        
        ArrayList<String> invalidGroupTypes = new ArrayList<String>();
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            invalidGroupTypes.add(GroupType.HIERARCHY.name());
            invalidGroupTypes.add(GroupType.ROLE_BASED.name());
        } else {
            invalidGroupTypes.add(GroupType.PERSONAL.name());
        }
        
        if (groupType.equalsIgnoreCase(GroupType.CUSTOM.name())) {
            invalidGroupTypes.add(GroupType.CUSTOM.name());
        }
        
        Map<String, List<?>> memberResults = resolveMembers(groupId, members, invalidGroupTypes, "DELETE", groupType);
        
        List<Long> groupIdsToAdd = (List<Long>)memberResults.get(GROUP_ID_ARRAY_KEY);
        List<Long> paxIdsToAdd = (List<Long>)memberResults.get(PAX_ID_ARRAY_KEY);
        List<ErrorMessage> memberErrors = (List<ErrorMessage>)memberResults.get(ERROR_ARRAY_KEY);

        if (!memberErrors.isEmpty()) {
            // If we want to return the other error messages other than invalid ids,
            // don't throw but add to the object later.
            throw new ErrorMessageException().addErrorMessages(memberErrors);
        }

        if (paxIdsToAdd != null && !paxIdsToAdd.isEmpty()) {
            groupDataSaveDao.deletePaxMembers(groupId, paxIdsToAdd);
        }
        
        if (groupIdsToAdd != null && !groupIdsToAdd.isEmpty()) {
            groupDataSaveDao.deleteGroupMembers(groupId, groupIdsToAdd);
        }
    }

    @Override
    public List<Groups> getActiveGroups() {
        List<Groups> groups = cnxtGroupsRepository.findByStatusTypeCode(StatusTypeCode.ACTIVE.name());
        return groups;
    }

    private ArrayList<ErrorMessage> validateGroupCreation(GroupEnrollmentWithStubDTO groupEnrollmentStub){
        
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        if (groupEnrollmentStub.getStatus() != null && !(groupEnrollmentStub.getStatus().equalsIgnoreCase(StatusTypeCode.ACTIVE.name()) || 
                groupEnrollmentStub.getStatus().equalsIgnoreCase(StatusTypeCode.INACTIVE.name()))) {
            errors.add(GroupConstants.INVALID_GROUP_STATUS_MESSAGE);
        }
        
        String groupType = groupEnrollmentStub.getType();
        String visibility = groupEnrollmentStub.getVisibility();
        Long groupPaxId = null;
        
        if (groupType == null || groupType.trim().equals(ProjectConstants.EMPTY_STRING)) {
            errors.add(GroupConstants.GROUP_TYPE_NULL_MESSAGE);
        } else {
            if (groupType.equalsIgnoreCase(GroupType.CUSTOM.name())) {
                if (visibility == null || visibility.trim().equals(ProjectConstants.EMPTY_STRING)) {
                    errors.add(GroupConstants.GROUP_VISIBILITY_NULL_MESSAGE);
                } else if (!(visibility.equals("PUBLIC") || visibility.equals(PermissionConstants.ADMIN_ROLE_NAME))) {
                    errors.add(GroupConstants.GROUP_VISIBILITY_INVALID_MESSAGE);
                }
            } else if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
                groupPaxId = security.getPaxId();
                if (visibility != null) {
                    errors.add(GroupConstants.GROUP_VISIBILITY_INVALID_MESSAGE);
                }
            } else {
                errors.add(GroupConstants.GROUP_TYPE_NULL_MESSAGE);
            }
            
            List<GroupMemberStubDTO> members = groupEnrollmentStub.getMembers();
            if (members == null || members.isEmpty()) {
                errors.add(GroupConstants.MEMBERS_LIST_EMPTY_MESSAGE);
            }
            
            if (members.size() > 50) {
                errors.add(GroupConstants.MEMBERS_LIST_TOO_LARGE_MESSAGE);
            }
        }
        
        String groupName = groupEnrollmentStub.getGroupName();
        if (groupName == null || groupName.trim().equals(ProjectConstants.EMPTY_STRING)) {
            errors.add(GroupConstants.GROUP_NAME_NULL_MESSAGE);
        } else {
            if (groupName.length() > 100) {
                errors.add(GroupConstants.GROUP_NAME_TOO_LONG_MESSAGE);
            }
            
            if (groupCreationValidationDao.isGroupNameDuplicate(groupName, groupType, groupPaxId)) {
                errors.add(GroupConstants.DUPLICATE_GROUP_NAME_MESSAGE);
            }
        }
    
        return errors;
    }
    
    private Map<String, List<?>> resolveMembers(Long groupId, List<GroupMemberStubDTO> members, 
            List<String> invalidGroupTypes, String method, String groupType){
        
        List<String> validPaxStatus = new ArrayList<String> ();
        
        if(GroupType.PERSONAL.name().equalsIgnoreCase(groupType)){
            validPaxStatus.add(StatusTypeCode.ACTIVE.name());
            validPaxStatus.add(StatusTypeCode.NEW.name());
            validPaxStatus.add(StatusTypeCode.SUSPENDED.name());
            validPaxStatus.add(StatusTypeCode.LOCKED.name());
        }
        
        List<Map<String, Object>> memberValidationResults = 
                groupCreationValidationDao.getMembershipStatus(groupId, members, validPaxStatus, invalidGroupTypes, method);
        
        List<Long> validPaxIds = new ArrayList<>();
        List<Long> validGroupIds = new ArrayList<>();
        List<ErrorMessage> errors = new ArrayList<>();
        
        StringBuilder invalidGroupsBuilder = new StringBuilder();
        for(int i = 0; i < invalidGroupTypes.size(); i++){
            invalidGroupsBuilder.append(invalidGroupTypes.get(i));
            if (i != invalidGroupTypes.size() - 1) {
                invalidGroupsBuilder.append(", ");
            }
        }
        
        String groupsNotAllowed = invalidGroupsBuilder.toString();
        
        for(Map<String, Object> member : memberValidationResults){
            Integer id = (Integer) member.get("ID");
            String type = (String) member.get("TYPE");
            String status = (String) member.get("STATUS");
            
            switch(status){
                case "DOES_NOT_EXIST":
                    if (type.equalsIgnoreCase("GROUP")) {
                        errors.add(new ErrorMessage()
                            .setCode("INVALID_GROUP")
                            .setField(ProjectConstants.GROUP_ID)
                            .setMessage("Group id " + id + " does not exist."));
                    } else {
                        errors.add(new ErrorMessage()
                            .setCode("INVALID_PAX")
                            .setField(ProjectConstants.PAX_ID)
                            .setMessage("Pax id " + id + " does not exist."));
                    }
                    break;
                case "INVALID_GROUP_TYPE":
                    errors.add(new ErrorMessage()
                        .setCode("GROUP_NOT_ALLOWED")
                        .setField(ProjectConstants.GROUP_ID)
                        .setMessage("Group id " + id + " not allowed to be added to this group type.  "
                                + "Invalid types are " + groupsNotAllowed));
                    break;
                case "INVALID_PAX_STATUS_TYPE":
                    errors.add(new ErrorMessage()
                        .setCode("INVALID_PAX_STATUS_TYPE")
                        .setField(ProjectConstants.PAX_ID)
                        .setMessage("Pax Id " + id + " has an invalid status.  "
                                + "Valid types are " + validPaxStatus));
                    break;
                case "NOT_MEMBER":
                    if (method.equalsIgnoreCase("ADD")) {
                        if (type.equalsIgnoreCase("GROUP")) {
                            validGroupIds.add(new Long(id));
                        } else {
                            validPaxIds.add(new Long(id));
                        }
                    }
                    break;
                case "MEMBER":
                    if (method.equalsIgnoreCase("DELETE")) {
                        if (type.equalsIgnoreCase("GROUP")) {
                            validGroupIds.add(new Long(id));
                        } else {
                            validPaxIds.add(new Long(id));
                        }
                    }
                    break;
            }
        }
        
        if (!validGroupIds.isEmpty() && GroupType.PERSONAL.name().equalsIgnoreCase(groupType)
                && !groupCreationValidationDao.isValidPersonalGroupsCount(security.getPaxId(), validGroupIds)) {
            errors.add(GroupConstants.INVALID_GROUP_OWNER_MESSAGE);
        }
        
        Map<String, List<?>> returnMap = new HashMap<>();
        returnMap.put(GROUP_ID_ARRAY_KEY, validGroupIds);
        returnMap.put(PAX_ID_ARRAY_KEY, validPaxIds);
        returnMap.put(ERROR_ARRAY_KEY, errors);
        
        return returnMap;
    }
    
    @Transactional
    Long saveGroupAndMembers(GroupDTO groupInfo, List<Long> validPaxIdList, List<Long> validGroupIdList){
        
        Groups group = new Groups();
        group.setGroupDesc(groupInfo.getGroupName());
        group.setStatusTypeCode(groupInfo.getStatus());
        group.setGroupCreateDate(new Date());
        
        if (group.getStatusTypeCode() == null || group.getStatusTypeCode().trim().equals(ProjectConstants.EMPTY_STRING)) {
            group.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        }
        
        if (groupInfo.getType().equals(GroupType.PERSONAL.name())) {
            group.setPaxId(security.getPaxId());
            group.setGroupTypeCode(GroupType.PERSONAL.getCode());
        } else {
            group.setGroupTypeCode(GroupType.CUSTOM.getCode());
        }
        
        groupsDao.save(group);
        Long groupId = group.getGroupId();
        
        if (groupInfo.getVisibility() != null) {
            groupDataSaveDao.saveVisibility(groupId, groupInfo.getVisibility());
        }
        
        if (validPaxIdList != null && !validPaxIdList.isEmpty()) {
            groupDataSaveDao.savePaxMembers(groupId, validPaxIdList);
        }
        
        if (validGroupIdList != null && !validGroupIdList.isEmpty()) {
            groupDataSaveDao.saveGroupMembers(groupId, validGroupIdList);
        }
        
        return groupId;
    }

}
