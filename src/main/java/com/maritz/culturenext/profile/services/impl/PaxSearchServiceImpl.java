package com.maritz.culturenext.profile.services.impl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.PaginatedPaxSearchDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.PaxSearchService;
import com.maritz.culturenext.util.PaginationUtil;

@Component
public class PaxSearchServiceImpl implements PaxSearchService {

    @Inject private PaginatedPaxSearchDao paginatedPaxSearchDao;
    @Inject private Security security;

    @Override
    public List<EmployeeDTO> searchPax(String searchString,
            String groupsString, String statusesString, Integer page,
            Integer size,      Integer budgetId) {
        
        return searchPax(searchString, groupsString, statusesString, null, false, page, size,    null);    
    }

    @Override
    public List<EmployeeDTO> searchPax(String searchString, String groupsString, String statusesString, 
            Integer programId, Boolean excludeSelf, Integer page, Integer size, Integer budgetId) {
        
        List<EmployeeDTO> results;
        
        List<String> statuses = null;
        if(statusesString != null){
            statuses = Arrays.asList(statusesString.split(","));
        }

        results = paginatedPaxSearchDao.searchPaxByNameGroupsAndStatusAndProgram(searchString, null, statuses, 
                programId, excludeSelf, security.getPaxId(), budgetId);
        
        if(results == null){
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(ProfileConstants.SEARCH_ERROR_CODE)
                    .setField(ProfileConstants.SEARCH_ERROR_FIELD)
                    .setMessage(ProfileConstants.SEARCH_ERROR_MSG));
        }
        
        return PaginationUtil.getPaginatedList(results, page, size);
    }
    
}
