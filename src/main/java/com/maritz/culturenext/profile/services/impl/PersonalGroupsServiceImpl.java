package com.maritz.culturenext.profile.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.constants.GroupConstants;
import com.maritz.culturenext.profile.dao.PersonalGroupsDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.services.PersonalGroupsService;
import com.maritz.culturenext.profile.services.GroupsService;

@Component
public class PersonalGroupsServiceImpl implements PersonalGroupsService {
    
    @Inject private PersonalGroupsDao personalGroupsDao;
    @Inject private Security security;
    @Inject private GroupsService groupsService;
    @Inject private ConcentrixDao<Groups> groupsDao;
    
    @Override
    public List<Map<String, Object>> getPersonalGroups(Long paxId) {
        if (!paxId.equals(security.getPaxId())) {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_GET_GROUP_MESSAGE);
        }
        return personalGroupsDao.getPersonalGroups(paxId);
    }

    @Transactional
    public void deletePersonalGroup(Long groupId){
        Map<String, Object> groupInfoMap = groupsService.getGroupInfo(groupId);
        
        String groupType = (String) groupInfoMap.get(ProjectConstants.TYPE);
        
        if (groupType.equalsIgnoreCase(GroupType.PERSONAL.name())) {
            Long groupPaxId = (Long) groupInfoMap.get(ProjectConstants.GROUP_PAX_ID);
            if (!groupPaxId.equals(security.getPaxId())) {
                throw new ErrorMessageException().addErrorMessage(GroupConstants.CANNOT_EDIT_GROUP_MESSAGE);
            }
        } else {
            throw new ErrorMessageException().addErrorMessage(GroupConstants.GROUP_TYPE_INVALID_MESSAGE);
        }
        List<EntityDTO> groupMembers = groupsService.getGroupMembers(groupId, Boolean.FALSE, null, null);
        
        List<GroupMemberStubDTO> members = new ArrayList<GroupMemberStubDTO>();
        
        for(EntityDTO groupMember: groupMembers){
            GroupMemberStubDTO member = new GroupMemberStubDTO();
            if(groupMember instanceof EmployeeDTO){
                member.setPaxId(((EmployeeDTO) groupMember).getPaxId());
            } else if (groupMember instanceof GroupDTO){
                member.setGroupId(((GroupDTO) groupMember).getGroupId());
            }
            members.add(member);
        }
        
        groupsService.deleteMembers(groupId, members);
        
        Groups group = groupsDao.findById(groupId);
        groupsDao.delete(group);
    }
}
