package com.maritz.culturenext.profile.services;

import java.util.List;
import java.util.Map;

public interface GroupSearchService {
    
    /**
     * Search for groups
     * 
     * @param searchString - Group name to search for
     * @param page
     * @param size
     * @param groupConfigIdString
     * @return
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847557/Groups+Search+Example
     */
    List<Map<String, Object>> findGroups(String searchString, Integer page, Integer size, String groupConfigIdString);
}
