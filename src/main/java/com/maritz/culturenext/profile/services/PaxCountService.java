package com.maritz.culturenext.profile.services;

import java.util.List;

import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.PaxCountDTO;

public interface PaxCountService {

    /**
     * Grabs the number of pax IDs associated with the given paxIds and groupIds.
     *
     * @param memberStubs Contains the members to use when counting
     * @param excludeSelf Flag to exclude the user from being counted
     * @return Object containing the count of pax IDs
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/10518600/Compute+Number+of+Unique+Participants+Example
     */
    PaxCountDTO getCountDistinctPax(List<GroupMemberStubDTO> memberStubs, Boolean excludeSelf);
    
}
