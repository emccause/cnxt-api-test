package com.maritz.culturenext.profile.services;

import java.util.List;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.profile.dto.*;
import com.maritz.culturenext.profile.dto.aggregate.*;

public interface DetailProfileService {

    /**
     * Return a participant's detail profile
     * 
     * @param paxId - Pax ID to return data for
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    DetailProfileInfo getDetailProfileInfoByPaxId(Long paxId);
    
    /**
     * Get the detail profile for the logged in pax ID.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    DetailProfileInfo getDetailProfileInfo();
    
    /**
     * Get a user's demographic information
     * 
     * @param paxId - Pax ID to return data for
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    ProfileDemographics getProfileDemographicsByPaxId(Long paxId);
    
    /**
     * Get the logged in user's demographic information
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    ProfileDemographics getProfileDemographics();
    
    /**
     * Get a user's email addresses
     * 
     * @param paxId - Pax ID to return data for
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    List<EmailDTO> getEmailsByPaxId(Long paxId);
    
    /**
     * Get the logged in user's email addresses
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    List<EmailDTO> getEmails();
    
    /**
     * Update a user's email address
     * 
     * @param paxID
     * @param emailPasswordWrapperDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743300/Update+Profile+Demographics
     */
    List<EmailDTO> updateEmails(Long paxID, EmailPasswordWrapperDTO emailPasswordWrapperDTO) throws Exception;
    
    /**
     * Insert an email address for a user
     * 
     * @param paxID
     * @param emailPasswordWrapperDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743300/Update+Profile+Demographics
     */
    List<EmailDTO> insertEmail(Long paxID, EmailPasswordWrapperDTO emailPasswordWrapperDTO) throws Exception;
    
    /**
     * Get a user's privacy settings
     * 
     * @param paxId - Pax ID to get data for
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    PaxMiscListDTO getProfilePrivacyByPaxId(Long paxId);
    
    /**
     * Get the logged in user's privacy settings
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    PaxMiscListDTO getProfilePrivacy();
    
    /**
     * Update a user's privacy settings
     * 
     * @param paxID
     * @param paxMiscDTOList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743308/Update+Profile+Privacy
     */
    List <PaxMiscDTO> updateProfilePrivacy(Long paxID, List<PaxMiscDTO> paxMiscDTOList);
    
    /**
     * Insert privacy settings for a user
     * 
     * @param paxID
     * @param paxMiscDTOList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743308/Update+Profile+Privacy
     */
    List <PaxMiscDTO> insertProfilePrivacy(Long paxID, List<PaxMiscDTO> paxMiscDTOList);
    
    /**
     * Get a user's recognition preferences
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    PaxMiscListDTO getProfilePreferencesByPaxId(Long paxId);
    
    /**
     * Get the logged in user's recognition preferences
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7340054/Get+Detail+Profile+Examples
     */
    PaxMiscListDTO getProfilePreferences();
    
    /**
     * Update a user's recognition preferences
     * 
     * @param paxID
     * @param paxMiscDTOList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743368/Update+Recognition+Preferences
     */
    List <PaxMiscDTO> updateProfilePreferences(Long paxID, List<PaxMiscDTO> paxMiscDTOList);
    
    /**
     * Insert recognition preferences for a user
     * 
     * @param paxID
     * @param paxMiscDTOList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743368/Update+Recognition+Preferences
     */
    List <PaxMiscDTO> insertProfilePreferences(Long paxID, List<PaxMiscDTO> paxMiscDTOList);
    
    /**
     * Updates a pax's language. Will change both LANGUAGE_CODE and LANGUAGE_LOCALE on the PAX record.
     * 
     * @param paxId - pax ID to update
     * @param localeDTO - locale code to update to
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22183980/Update+Pax
     */
    LocaleDTO updatePaxLanguageAndLocale(Long paxId, LocaleDTO localeDTO);
    
    /**
     * Return a paginated list of the paxId's direct reports
     * 
     * @param requestDetails
     * @param paxId
     * @param pageNumber
     * @param pageSize
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/74940457/GET+manager+direct+reports
     */
    PaginatedResponseObject<EmployeeDTO> getManagerDirectReports(
            PaginationRequestDetails requestDetails, Long paxId, boolean excludeInactive, Integer pageNumber, Integer pageSize
        );
}
