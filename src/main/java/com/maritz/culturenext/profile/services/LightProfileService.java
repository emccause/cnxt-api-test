package com.maritz.culturenext.profile.services;

import com.maritz.culturenext.profile.dto.aggregate.LightweightProfileInfoDTO;

public interface LightProfileService {

    /**
     * Get a user's lightweight profile information
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/6914106/Lightweight+Profile+Example
     */
    LightweightProfileInfoDTO getLightProfileInfo();
}
