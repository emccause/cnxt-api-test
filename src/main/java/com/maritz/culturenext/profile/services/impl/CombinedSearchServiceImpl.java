package com.maritz.culturenext.profile.services.impl;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.PaginatedCombinedSearchDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.services.CombinedSearchService;
import com.maritz.culturenext.util.PaginationUtil;

@Component
public class CombinedSearchServiceImpl implements CombinedSearchService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private PaginatedCombinedSearchDao paginatedSearchDao;
    @Inject private Security security;
    
    @Override
    public List<EntityDTO> getSearchResults(String searchStr, Integer page, Integer size, String visibilityListString,
            String statusListString, String groupTypeListString, String excludeSelfString) {
        
        List<EntityDTO> results;
        
        Long paxId = security.getPaxId();
        
        if (searchStr == null || searchStr.length() < ProfileConstants.COMBINED_SEARCH_MINIMUM_STRING_LENGTH) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(ProfileConstants.SEARCH_STRING_TOO_SHORT_CODE)
                    .setField(ProfileConstants.SEARCH_STRING_TOO_SHORT_FIELD)
                    .setMessage(ProfileConstants.SEARCH_STRING_TOO_SHORT_MSG));
        }
        
        List<String> visibilityList = null;
        if(visibilityListString != null && !visibilityListString.trim().equals("")){
            visibilityList = Arrays.asList(visibilityListString.trim().split(","));
        }
        List<String> statusList = null;
        if(statusListString != null && !statusListString.trim().equals("")){
            statusList = Arrays.asList(statusListString.trim().split(","));
        }
        List<String> groupTypeList = null;
        if(groupTypeListString != null && !groupTypeListString.trim().equals("")){
            groupTypeList = Arrays.asList(groupTypeListString.trim().split(","));
        }
        
        Boolean excludeSelf = false;
        if(excludeSelfString != null){
            excludeSelf = Boolean.parseBoolean(excludeSelfString);
        }
        
        results = paginatedSearchDao.getSearchResults(searchStr, paxId, visibilityList, statusList, groupTypeList, 
                excludeSelf);
        if(results == null){
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(ProfileConstants.SEARCH_ERROR_CODE)
                    .setField(ProfileConstants.SEARCH_ERROR_FIELD)
                    .setMessage(ProfileConstants.SEARCH_ERROR_MSG));
        }
        
        return PaginationUtil.getPaginatedList(results, page, size);

    }

}
