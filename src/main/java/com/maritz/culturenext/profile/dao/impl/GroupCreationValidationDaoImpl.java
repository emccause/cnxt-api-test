package com.maritz.culturenext.profile.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.dao.GroupCreationValidationDao;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

@Repository
public class GroupCreationValidationDaoImpl extends AbstractDaoImpl implements GroupCreationValidationDao {
    
    private static final String GROUP_NAME_SQL_PARAM = "GROUP_NAME";
    private static final String GROUP_TYPE_DESC_SQL_PARAM = "GROUP_TYPE_DESC";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String GROUPS_LIST_SQL_PARAM = "GROUPS_LIST";
    private static final String PAX_STATUS_TYPE_LIST_SQL_PARAM = "PAX_STATUS_LIST";
    
    private static final String PAX_ID_WHERE_PLACEHOLDER = "{PAX_ID_WHERE_PLACEHOLDER}";
    private static final String PAX_ID_NULL = "IS NULL";
    private static final String PAX_ID_NOT_NULL = "= :" + PAX_ID_SQL_PARAM;
    
    private static final String STATUS_TYPE_CASE_PLACEHOLDER = "{STATUS_TYPE_CASE_PLACEHOLDER}";
    private static final String STATUS_TYPE_NULL = " ";
    private static final String STATUS_TYPE_NOT_NULL = "WHEN " +
            "(MEMBER_LIST.TYPE = 'PAX' " +
            "AND GP.GROUP_ID IS NULL " +
            "AND SU.STATUS_TYPE_CODE NOT IN (:" + PAX_STATUS_TYPE_LIST_SQL_PARAM + ") " +
            "AND MEMBER_LIST.METHOD='ADD') " +
        "THEN 'INVALID_PAX_STATUS_TYPE' ";
    
    private static final String DUPLICATE_GROUP_NAME = 
            "SELECT G.GROUP_DESC "
            + "FROM component.GROUPS G "
                + "INNER JOIN component.GROUP_TYPE GT ON G.GROUP_TYPE_CODE = GT.GROUP_TYPE_CODE "
            + "WHERE GROUP_DESC = :" + GROUP_NAME_SQL_PARAM + " "
                    + "AND GT.GROUP_TYPE_DESC = :" + GROUP_TYPE_DESC_SQL_PARAM + " "
                    + "AND PAX_ID " + PAX_ID_WHERE_PLACEHOLDER;

    private static final String PERSONAL_GROUPS_COUNT = "SELECT COUNT (G.GROUP_ID) AS TOTAL_GROUPS " +
        " FROM component.GROUPS G " +
        " INNER JOIN component.GROUP_TYPE GT ON G.GROUP_TYPE_CODE = GT.GROUP_TYPE_CODE " +
        " WHERE G.GROUP_ID IN ( :" + GROUPS_LIST_SQL_PARAM + ") " +
        " AND GT.GROUP_TYPE_DESC IN ( '" + GroupType.PERSONAL.name() + "' , '" + GroupType.CUSTOM.name() + "' , '" + GroupType.ENROLLMENT.name() + "' )" +
        " AND (G.PAX_ID = :" + PAX_ID_SQL_PARAM +
        " OR G.PAX_ID IS NULL) ";

    private static final String GROUP_ID = "GROUP_ID";
    
    private static final String GROUP_TYPE_DESC_LIST_SQL_PARAM = "GROUP_TYPE_DESC_LIST";
    
    private static final String MEMBER_LIST_PLACEHOLDER = "{MEMBER_LIST_PLACEHOLDER}";
    
    private static final String GROUP_MEMBERSHIP_STATUS_QUERY = "SELECT DISTINCT MEMBER_LIST.*,  " +
            "CASE  " +
            "WHEN " +
            "(MEMBER_LIST.TYPE = 'PAX' AND P.PAX_ID IS NULL)  " +
            "OR (MEMBER_LIST.TYPE = 'GROUP' AND G.GROUP_ID IS NULL)  " +
            "THEN 'DOES_NOT_EXIST' " +
            STATUS_TYPE_CASE_PLACEHOLDER +
            "WHEN " +
            "(MEMBER_LIST.TYPE = 'GROUP' "
                    + "AND G.GROUP_ID IS NOT NULL "
                    + "AND GT.GROUP_TYPE_DESC IN (:" + GROUP_TYPE_DESC_LIST_SQL_PARAM + ") "
                    + "AND MEMBER_LIST.METHOD='ADD') " +
            "THEN 'INVALID_GROUP_TYPE' " +
            "WHEN " +
            "(MEMBER_LIST.TYPE = 'PAX' AND GP.GROUP_ID IS NULL) " +
            "OR (MEMBER_LIST.TYPE = 'GROUP' AND GG.GROUP_ID_1 IS NULL) " +
            "THEN 'NOT_MEMBER' " +
            "WHEN " +
            "(MEMBER_LIST.TYPE = 'PAX' AND GP.GROUP_ID IS NOT NULL) " +
            "OR (MEMBER_LIST.TYPE = 'GROUP' AND GG.GROUP_ID_1 IS NOT NULL) " +
            "THEN 'MEMBER' " +
            "END AS STATUS FROM ( " +
            MEMBER_LIST_PLACEHOLDER +
            ") AS MEMBER_LIST " +
            "LEFT JOIN component.PAX P " +
            "ON P.PAX_ID = MEMBER_LIST.ID " +
            "AND MEMBER_LIST.TYPE = 'PAX' " +
            "LEFT JOIN component.SYS_USER SU " +
            "ON SU.PAX_ID = MEMBER_LIST.ID " +
            "LEFT JOIN component.GROUPS G " +
            "ON G.GROUP_ID = MEMBER_LIST.ID " +
            "AND MEMBER_LIST.TYPE = 'GROUP' " +
            "LEFT JOIN component.GROUP_TYPE GT " +
            "ON G.GROUP_TYPE_CODE = GT.GROUP_TYPE_CODE " +
            "LEFT JOIN component.GROUPS_PAX GP " +
            "ON GP.PAX_ID = MEMBER_LIST.ID " +
            "AND MEMBER_LIST.TYPE = 'PAX' " +
            "AND GP.GROUP_ID = :" + GROUP_ID +" " +
            "LEFT JOIN component.VW_GROUPS_GROUPS GG " +
            "ON GG.GROUP_ID_2 = MEMBER_LIST.ID " +
            "AND MEMBER_LIST.TYPE = 'GROUP' " +
            "AND GG.GROUP_ID_1 = :" + GROUP_ID +" " +
            "ORDER BY MEMBER_LIST.TYPE DESC";

    @Override
    public Boolean isGroupNameDuplicate(String groupName, String type, Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_NAME_SQL_PARAM, groupName);
        params.addValue(GROUP_TYPE_DESC_SQL_PARAM, type);
        String query = DUPLICATE_GROUP_NAME;
        if (paxId == null) {
            query = query.replace(PAX_ID_WHERE_PLACEHOLDER, PAX_ID_NULL);
        } else {
            query = query.replace(PAX_ID_WHERE_PLACEHOLDER, PAX_ID_NOT_NULL);
            params.addValue(PAX_ID_SQL_PARAM, paxId);
        }
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());

        return !nodes.isEmpty(); // 
    }

@Override
public Boolean isValidPersonalGroupsCount(Long paxId, List<Long> groupIds) {
    MapSqlParameterSource params = new MapSqlParameterSource();
    params.addValue(PAX_ID_SQL_PARAM, paxId);
    params.addValue(GROUPS_LIST_SQL_PARAM, groupIds);
    String query = PERSONAL_GROUPS_COUNT;
    
    List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
    
    Integer totalGroups = (Integer)nodes.get(0).get("TOTAL_GROUPS");
        
    return groupIds.size() == totalGroups.intValue();
}

    @Override
    public List<Map<String,Object>> getMembershipStatus(Long groupId, List<GroupMemberStubDTO> members, 
            List<String> validPaxStatus, List<String> invalidGroupTypes, String method) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_TYPE_DESC_LIST_SQL_PARAM, invalidGroupTypes);
        params.addValue(PAX_STATUS_TYPE_LIST_SQL_PARAM, validPaxStatus);
        if (groupId == null) {
            groupId = 0L;
        }
        params.addValue(GROUP_ID, groupId);
        
        String query = GROUP_MEMBERSHIP_STATUS_QUERY;
        
        StringBuilder memberBuilder = new StringBuilder();
        for (int i = 0; i < members.size(); i++) {
            String type = "";
            Long id = null;
            if (members.get(i).getPaxId() != null) {
                type = "PAX";
                id = members.get(i).getPaxId();
            } else if (members.get(i).getGroupId() != null) {
                type = "GROUP";
                id = members.get(i).getGroupId();
            } else {
                continue;
            }
            
            if (i == 0) {
                memberBuilder.append("SELECT " + id + " AS ID, '" + type + "' AS TYPE, '" + method + "' AS METHOD ");
            } else {
                memberBuilder.append("UNION SELECT " + id + ", '" + type + "', '" + method + "' ");
            }
        }
        
        query = query.replace(MEMBER_LIST_PLACEHOLDER, memberBuilder.toString());
        
        if (validPaxStatus == null || validPaxStatus.isEmpty()) {
            query = query.replace(STATUS_TYPE_CASE_PLACEHOLDER, STATUS_TYPE_NULL);
        } else {
            query = query.replace(STATUS_TYPE_CASE_PLACEHOLDER, STATUS_TYPE_NOT_NULL);
        }

        return namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
    }
}
