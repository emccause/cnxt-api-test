package com.maritz.culturenext.profile.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.profile.dao.ManagerDirectReportDao;

@Repository
public class ManagerDirectReportDaoImpl extends PaginatingDaoImpl implements ManagerDirectReportDao {
    
    private static final String PAX_ID_SQL_PARAM = "RL.PAX_ID_2";
    private static final String MANAGER_DIRECT_REPORT_QUERY = "SELECT "
            + "UG.PAX_ID, UG.LANGUAGE_CODE, UG.PREFERRED_LOCALE, UG.LANGUAGE_LOCALE, UG.CONTROL_NUM, "  
            + "UG.FIRST_NAME, UG.MIDDLE_NAME, UG.LAST_NAME, UG.NAME_PREFIX, UG.NAME_SUFFIX, UG.COMPANY_NAME, "
            + "UG.VERSION, UG.JOB_TITLE, UG.SYS_USER_ID, UG.STATUS_TYPE_CODE, UG.MANAGER_PAX_ID "
        + "FROM RELATIONSHIP RL "
        + "LEFT JOIN VW_USER_GROUPS_SEARCH_DATA UG "
        + "ON UG.PAX_ID = RL.PAX_ID_1 "
        + "WHERE RL.RELATIONSHIP_TYPE_CODE = '" + RelationshipType.REPORT_TO + "' "
        + "AND PAX_ID IS NOT NULL AND RL.PAX_ID_2 = :" + PAX_ID_SQL_PARAM;
    private static final String INACTIVE_CONDITION = " AND UG.STATUS_TYPE_CODE<>'INACTIVE' ";
    private static final String ORDER_BY_CLAUSE = "FIRST_NAME ASC, LAST_NAME ASC";
    
    @Override
    public List<Map<String, Object>> getManagerDirectReports(Long paxId, boolean excludeInactive, Integer pageNumber, Integer pageSize) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        if (excludeInactive){
            return getPageWithTotalRowsCTE(MANAGER_DIRECT_REPORT_QUERY + INACTIVE_CONDITION, params, new CamelCaseMapRowMapper(), ORDER_BY_CLAUSE, pageNumber, pageSize);
        }else {
            return getPageWithTotalRowsCTE(MANAGER_DIRECT_REPORT_QUERY, params, new CamelCaseMapRowMapper(), ORDER_BY_CLAUSE, pageNumber, pageSize);
        }
    }
}
