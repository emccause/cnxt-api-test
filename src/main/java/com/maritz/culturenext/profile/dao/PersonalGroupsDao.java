package com.maritz.culturenext.profile.dao;

import java.util.List;
import java.util.Map;

public interface PersonalGroupsDao {

    List<Map<String, Object>> getPersonalGroups(Long paxId);
}
