package com.maritz.culturenext.profile.dao;

import java.util.List;

import com.maritz.culturenext.profile.dto.EntityDTO;

public interface PaginatedGroupMembershipDao {
    
    List<EntityDTO> getMembership(Long groupId);

    List<EntityDTO> getExpandedMembership(Long groupId);
    
}
