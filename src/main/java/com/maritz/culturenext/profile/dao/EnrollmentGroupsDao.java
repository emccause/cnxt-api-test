package com.maritz.culturenext.profile.dao;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;

public interface EnrollmentGroupsDao {

    List<EnrollmentGroupDTO> getEnrollmentGroups();
    
}
