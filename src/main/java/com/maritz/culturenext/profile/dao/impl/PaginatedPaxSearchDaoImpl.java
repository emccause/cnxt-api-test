package com.maritz.culturenext.profile.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.PaginatedPaxSearchDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class PaginatedPaxSearchDaoImpl extends AbstractDaoImpl implements PaginatedPaxSearchDao {
    
    private static final String PROGRAM_ID = "PROGRAM_ID";
    private static final String BUDGET_ID = "BUDGET_ID";
    private static final String PAX_ID_PARAM = "PAX_ID_PARAM";
    private static final String WHERE_CLAUSE_PLACEHOLDER = "{WHERE_CLAUSE_PLACEHOLDER}";
    private static final String PROGRAM_CLAUSE_PLACEHOLDER = "{PROGRAM_CLAUSE_PLACEHOLDER}"; 
    private static final String ACL_CLAUSE_PLACEHOLDER = "{ACL_CLAUSE_PLACEHOLDER}"; 
    private static final String INNER_WHERE_CLAUSE_PLACEHOLDER = "{INNER_WHERE_CLAUSE_PLACEHOLDER}";
    
    private static final String VIEW_USER_GROUPS_SEARCH_QUERY =
            "SELECT * FROM ( " +
                "SELECT ROW_NUMBER() OVER (ORDER BY NAME ASC) AS 'ROW_NUM', * " +
                "FROM (SELECT DISTINCT VW_SEARCH.* " +
                    PROGRAM_CLAUSE_PLACEHOLDER +
                    "FROM COMPONENT.VW_USER_GROUPS_SEARCH_DATA AS VW_SEARCH " +
                    ACL_CLAUSE_PLACEHOLDER +    
                    " WHERE SEARCH_TYPE = 'P' " +
                    INNER_WHERE_CLAUSE_PLACEHOLDER +
                ") AS INNER_QUERY " +
                "WHERE (INNER_QUERY.NAME COLLATE Latin1_general_CI_AI LIKE :" + ProfileConstants.SEARCH_STRING_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
                "OR INNER_QUERY.CONTROL_NUM = :" + ProfileConstants.SEARCH_STRING_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
                "OR INNER_QUERY.EMAIL_ADDRESS = :" + ProfileConstants.SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI) " +
                WHERE_CLAUSE_PLACEHOLDER + 
            ") AS SEARCH_QUERY ";
    
    private static final String STATUS_WHERE_CLAUSE = 
            "AND INNER_QUERY.STATUS_TYPE_CODE IN (:STATUSES) ";
    
    private static final String GROUPS_WHERE_CLAUSE = 
            "AND INNER_QUERY.GROUP_ID IN (:GROUPS#) ";

    private static final String IF_PROGRAM_CLAUSE = 
            ",CASE " +
                "WHEN(ACL.TARGET_ID IS NULL AND ACLG.SUBJECT_ID IS NULL) " +
                    "THEN CAST(0 AS BIT) " + 
                "ELSE CASE " + 
                    "WHEN (ACL.TARGET_ID = :PROGRAM_ID OR ACLG.TARGET_ID = :PROGRAM_ID) " +
                        "THEN CAST(1 AS BIT) " +
                    "ELSE CAST(0 AS BIT) " + 
                "END " +
            "END AS 'ELIGIBLE_RECEIVER_IN_PROGRAM' ";
    
    private static final String ACL_CLAUSE =
            "LEFT JOIN ACL ACL " +
                "ON ACL.SUBJECT_ID = VW_SEARCH.PAX_ID " +
                "AND ACL.SUBJECT_TABLE='PAX' " +
                "AND ACL.TARGET_TABLE='PROGRAM' " +
                "AND ACL.TARGET_ID=:PROGRAM_ID " +
                "AND ACL.ROLE_ID IN (SELECT ROLE_ID FROM ROLE WHERE ROLE_CODE='PREC') " +
            "LEFT JOIN ACL ACLG " +
                "ON ACLG.SUBJECT_ID IN " +
                    "(SELECT group_id FROM VW_GROUP_TOTAL_MEMBERSHIP VGTM WHERE VGTM.pax_id = VW_SEARCH.PAX_ID) " +
                "AND ACLG.SUBJECT_TABLE='GROUPS' " +
                "AND ACLG.TARGET_TABLE='PROGRAM' " +
                "AND ACLG.TARGET_ID =:PROGRAM_ID " +
                "AND ACLG.ROLE_ID IN (SELECT ROLE_ID FROM ROLE WHERE ROLE_CODE='PREC') ";
    
    private static final String BUDGET_PROGRAM_GIVER_PAX_IDS = 
    		"  INNER JOIN ( " +
            " select  gp.PAX_ID	" +
            " from component.program_budget pb " +
            " inner join component.acl acl on acl.target_table = 'program_budget'	and acl.target_ID = pb.ID  and acl.subject_table = 'GROUPS' " + 
            " inner join component.role r on r.role_id = acl.role_id and r.ROLE_CODE = 'PROGRAM_BUDGET_GIVER' " +
            " inner join component.GROUPS_PAX gp on gp.GROUP_ID = acl.SUBJECT_ID " +
            " where pb.BUDGET_ID =:BUDGET_ID " +
            " union  select   gp.PAX_ID " +
            "   from component.program_budget pb " +
            "  inner join component.acl acl on acl.target_table = 'program'	and acl.target_ID = pb.PROGRAM_ID  and acl.subject_table = 'GROUPS' " + 
            "  inner join component.role r on r.role_id = acl.role_id and r.ROLE_CODE = 'PGIV' " +
            "  inner join component.GROUPS_PAX gp on gp.GROUP_ID = acl.SUBJECT_ID " +
            " where pb.BUDGET_ID =:BUDGET_ID " +
            " union  select  acl.SUBJECT_ID 'PAX_ID' " +
            "   from component.program_budget pb " +
            "  inner join component.acl acl on acl.target_table = 'program_budget'	and acl.target_ID = pb.ID  and acl.subject_table = 'pax' " + 
            "  inner join component.role r on r.role_id = acl.role_id and r.ROLE_CODE = 'PROGRAM_BUDGET_GIVER' " +
            " where pb.BUDGET_ID =:BUDGET_ID " +
            " union  select  acl.SUBJECT_ID 'PAX_ID' " +
            "   from component.program_budget pb " +
            "  inner join component.acl acl on acl.target_table = 'program'	and acl.target_ID = pb.PROGRAM_ID  and acl.subject_table = 'pax' " + 
            "  inner join component.role r on r.role_id = acl.role_id and r.ROLE_CODE = 'PGIV' " +
            " where pb.BUDGET_ID =:BUDGET_ID " +
            " ) PROGGVPX ON PROGGVPX.PAX_ID = SEARCH_QUERY.PAX_ID ";
    
    private static final String PAX_ID_FILTER_QUERY = 
            "AND PAX_ID <>:" + PAX_ID_PARAM;
    @Override
    public List<EmployeeDTO> searchPaxByNameGroupsAndStatus(
            String searchString, List<ArrayList<Long>> groups,
            List<String> statuses) {
        return searchPaxByNameGroupsAndStatusAndProgram(searchString, groups, statuses, null, false, null, null);
    }
    
    
    @Override
    public List<EmployeeDTO> searchPaxByNameGroupsAndStatusAndProgram(String searchString, List<ArrayList<Long>> groups,
            List<String> statuses, Integer programId, Boolean excludeSelf, Long paxId, Integer budgetId) {

        // like %% is the equivalent of leaving the where clause off, 
        // which is fine if search string is missing(and makes the dynamic where clause easy to handle)
        String searchStringPrepped = "%" + searchString + "%"; 
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProfileConstants.SEARCH_STRING_SQL_PARAM, searchStringPrepped, Types.NVARCHAR);
        params.addValue(ProfileConstants.SEARCH_STRING_RAW_SQL_PARAM, searchString, Types.NVARCHAR);
        params.addValue(ProfileConstants.SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM, searchString, Types.NVARCHAR);
        //String searchQueryProg = QUERY;
        String searchQuery = VIEW_USER_GROUPS_SEARCH_QUERY;
        if (groups == null && statuses == null && programId == null && (excludeSelf == null || !excludeSelf)) {
            // remove the placeholders
            searchQuery = searchQuery.replace(WHERE_CLAUSE_PLACEHOLDER, ProjectConstants.EMPTY_STRING); 
            searchQuery = searchQuery.replace(PROGRAM_CLAUSE_PLACEHOLDER, ProjectConstants.EMPTY_STRING); 
            searchQuery = searchQuery.replace(ACL_CLAUSE_PLACEHOLDER, ProjectConstants.EMPTY_STRING); 
            searchQuery = searchQuery.replace(INNER_WHERE_CLAUSE_PLACEHOLDER, ProjectConstants.EMPTY_STRING); 
        } else {          
            
            StringBuilder whereBuilder = new StringBuilder();
            
            if (excludeSelf != null && excludeSelf) {
                params.addValue(PAX_ID_PARAM, paxId);
                searchQuery = searchQuery.replace(INNER_WHERE_CLAUSE_PLACEHOLDER, PAX_ID_FILTER_QUERY);
            } else {
                // remove the placeholder
                searchQuery = searchQuery.replace(INNER_WHERE_CLAUSE_PLACEHOLDER, ProjectConstants.EMPTY_STRING); 
            }
                            
            if (programId != null) {
                params.addValue(PROGRAM_ID, programId.toString());
                searchQuery = searchQuery.replace(PROGRAM_CLAUSE_PLACEHOLDER, IF_PROGRAM_CLAUSE);    
                searchQuery = searchQuery.replace(ACL_CLAUSE_PLACEHOLDER, ACL_CLAUSE);    
            } else {
                searchQuery = searchQuery.replace(PROGRAM_CLAUSE_PLACEHOLDER, "");
                searchQuery = searchQuery.replace(ACL_CLAUSE_PLACEHOLDER, "");
            }
                                        
            
            if (statuses != null) {
                whereBuilder.append(STATUS_WHERE_CLAUSE);
                params.addValue("STATUSES", statuses);
            }
            
            if (groups != null) {
                for(int i = 0; i < groups.size(); i++){
                    whereBuilder.append(GROUPS_WHERE_CLAUSE.replace("#", "" + i));
                    params.addValue("GROUPS" + i, groups.get(i));
                }
            }
            searchQuery = searchQuery.replace(WHERE_CLAUSE_PLACEHOLDER, whereBuilder.toString());
            
            
        }
        
        if (budgetId != null) {					//<======<<<
        	params.addValue(BUDGET_ID, budgetId.toString());
        	searchQuery = searchQuery + BUDGET_PROGRAM_GIVER_PAX_IDS;
        }
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(searchQuery, params, new CamelCaseMapRowMapper());

        ArrayList<EmployeeDTO> resultsList = new ArrayList<EmployeeDTO>();
        for (Map<String, Object> node: nodes) {                
            resultsList.add(new EmployeeDTO(node, null));
        }
        return resultsList;
    }    
}
