package com.maritz.culturenext.profile.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.dao.PaginatedGroupMembershipDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

@Repository
public class PaginatedGroupMembershipDaoImpl extends AbstractDaoImpl implements PaginatedGroupMembershipDao {

    @Inject private Security security;
    
    private static final String GROUP_ID_SQL_PARAM = "GROUP_ID";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";

    private static final String SEARCH_TYPE = "SEARCH_TYPE";
    private static final String PAX_SEARCH_TYPE = "P";
    private static final String STATUS_TYPE_CODE = "STATUS_TYPE_CODE";
    
    //Result Set Columns (mapped to camelCase)
    private static final String GROUP_TYPE_CODE = "groupTypeCode";
    private static final String RS_SEARCH_TYPE = "searchType";
    
    private static final String PAX_ID = "PAX_ID";
    private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    private static final String PREFERRED_LOCALE = "PREFERRED_LOCALE";
    private static final String CONTROL_NUM = "CONTROL_NUM";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String MIDDLE_NAME = "MIDDLE_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String NAME_PREFIX = "NAME_PREFIX";
    private static final String NAME_SUFFIX = "NAME_SUFFIX";
    private static final String COMPANY_NAME = "COMPANY_NAME";
    private static final String PROFILE_PICTURE_VERSION = "VERSION";
    private static final String JOB_TITLE = "JOB_TITLE";
    private static final String MANAGER_PAX_ID = "MANAGER_PAX_ID";

    private static final String GROUP_ID = "GROUP_ID";
    private static final String GROUP_CONFIG_ID = "GROUP_CONFIG_ID";
    private static final String FIELD = "FIELD";
    private static final String FIELD_DISPLAY_NAME = "FIELD_DISPLAY_NAME";
    private static final String GROUP_NAME = "GROUP_NAME";
    private static final String GROUP_TYPE = "GROUP_TYPE";
    private static final String GROUP_PAX_ID = "GROUP_PAX_ID";
    private static final String VISIBILITY = "VISIBILITY";
    private static final String CREATE_DATE = "CREATE_DATE";
    private static final String PAX_COUNT = "PAX_COUNT";
    private static final String GROUP_COUNT = "GROUP_COUNT";
    private static final String TOTAL_PAX_COUNT = "TOTAL_PAX_COUNT";
    
    private static final String GROUP_ID_WHERE_PLACEHOLDER = "{GROUP_ID_WHERE_PLACEHOLDER}";
    private static final String GROUP_ID_WHERE_NOT_PERSONAL = "GROUP_ID IN (SELECT GROUP_ID_2 FROM component.VW_GROUPS_GROUPS WHERE GROUP_ID_1 = :" + GROUP_ID_SQL_PARAM + ") ";
    private static final String GROUP_ID_WHERE_PERSONAL = "(GROUP_ID IN (SELECT VGG.GROUP_ID_2 FROM component.VW_GROUPS_GROUPS VGG " +
                "INNER JOIN component.GROUPS G ON G.GROUP_ID = VGG.GROUP_ID_1 " +
                "WHERE VGG.GROUP_ID_1 = :" + GROUP_ID_SQL_PARAM + " AND G.PAX_ID = :" + PAX_ID_SQL_PARAM + ") " +
                "AND GROUP_TYPE IN ('" + GroupType.ENROLLMENT.name() + "', '" + GroupType.CUSTOM.name() + "', "
                + "'" + GroupType.PERSONAL.name() + "')) ";

    private static final String PAX_ID_WHERE_PLACEHOLDER = "{PAX_ID_WHERE_PLACEHOLDER}";
    private static final String PAX_ID_WHERE_NOT_PERSONAL = "PAX_ID IN (SELECT PAX_ID FROM component.GROUPS_PAX WHERE GROUP_ID = :" + GROUP_ID_SQL_PARAM + ") ";
    private static final String PAX_ID_WHERE_PERSONAL = "PAX_ID IN (SELECT GP.PAX_ID FROM component.GROUPS_PAX GP " +
                "INNER JOIN component.GROUPS G ON G.GROUP_ID = GP.GROUP_ID " +
                "WHERE GP.GROUP_ID = :" + GROUP_ID_SQL_PARAM + " AND G.PAX_ID = :" + PAX_ID_SQL_PARAM + ") ";
    
    private static final String SELECT_GROUP_TYPE_BY_GROUP_ID = "SELECT GROUP_TYPE_CODE FROM component.GROUPS WHERE GROUP_ID = :" + GROUP_ID_SQL_PARAM;
    
    private static final String DEFAULT_MEMBERSHIP_QUERY = "SELECT " + 
            SEARCH_TYPE +
            ", " + STATUS_TYPE_CODE +
            ", " + PAX_ID +
            ", " + LANGUAGE_CODE +
            ", " + PREFERRED_LOCALE +
            ", " + CONTROL_NUM +
            ", " + FIRST_NAME +
            ", " + MIDDLE_NAME + 
            ", " + LAST_NAME +
            ", " + NAME_PREFIX +
            ", " + NAME_SUFFIX +
            ", " + COMPANY_NAME +
            ", " + PROFILE_PICTURE_VERSION +
            ", " + JOB_TITLE +
            ", " + MANAGER_PAX_ID +
            ", " + GROUP_ID +
            ", " + GROUP_CONFIG_ID +
            ", " + FIELD +
            ", " + FIELD_DISPLAY_NAME +
            ", " + GROUP_NAME +
            ", " + GROUP_TYPE +
            ", " + VISIBILITY +
            ", " + CREATE_DATE +
            ", " + PAX_COUNT +
            ", " + GROUP_COUNT +
            ", " + TOTAL_PAX_COUNT +
            ", " + GROUP_PAX_ID +
            " FROM ( " +
            "SELECT ROW_NUMBER() OVER (ORDER BY NAME) AS ROW_NUM,* FROM component.VW_USER_GROUPS_SEARCH_DATA " +
            "WHERE " + GROUP_ID_WHERE_PLACEHOLDER +
            "OR " + PAX_ID_WHERE_PLACEHOLDER +
            ") AS INNER_QUERY ";
    
    private static final String EXPANDED_MEMBERSHIP_QUERY = "SELECT " + 
            SEARCH_TYPE +
            ", " + STATUS_TYPE_CODE +
            ", " + PAX_ID +
            ", " + LANGUAGE_CODE +
            ", " + PREFERRED_LOCALE +
            ", " + CONTROL_NUM +
            ", " + FIRST_NAME +
            ", " + MIDDLE_NAME + 
            ", " + LAST_NAME +
            ", " + NAME_PREFIX +
            ", " + NAME_SUFFIX +
            ", " + COMPANY_NAME +
            ", " + PROFILE_PICTURE_VERSION +
            ", " + JOB_TITLE +
            ", " + MANAGER_PAX_ID + 
            " FROM ( " +
            "SELECT ROW_NUMBER() OVER (ORDER BY NAME) AS ROW_NUM, UGSD.* FROM component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
            "INNER JOIN component.VW_USER_GROUPS_SEARCH_DATA UGSD " +
            "ON GTM.PAX_ID = UGSD.PAX_ID WHERE GTM.GROUP_ID = :" + GROUP_ID_SQL_PARAM + " " +
            "AND UGSD." + SEARCH_TYPE +" = '" + PAX_SEARCH_TYPE + "' " +
            ") AS INNER_QUERY ";

    @Override
    public List<EntityDTO> getMembership(Long groupId) {
        
        String query = DEFAULT_MEMBERSHIP_QUERY;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_SQL_PARAM, groupId);
        params.addValue(PAX_ID_SQL_PARAM, security.getPaxId());

        if (isPersonalGroup(groupId)) {
            query = query.replace(GROUP_ID_WHERE_PLACEHOLDER, GROUP_ID_WHERE_PERSONAL);
            query = query.replace(PAX_ID_WHERE_PLACEHOLDER, PAX_ID_WHERE_PERSONAL);
        } else {
            query = query.replace(GROUP_ID_WHERE_PLACEHOLDER, GROUP_ID_WHERE_NOT_PERSONAL);
            query = query.replace(PAX_ID_WHERE_PLACEHOLDER, PAX_ID_WHERE_NOT_PERSONAL);
        }
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());

        ArrayList<EntityDTO> resultsList = new ArrayList<EntityDTO>();
        for (Map<String, Object> node: nodes) {
            if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase(PAX_SEARCH_TYPE)) {                
                resultsList.add(new EmployeeDTO(node, null));
            } else {
                resultsList.add(new GroupDTO(node));
            }
        }
        return resultsList;
    }
    
    @Override
    public List<EntityDTO> getExpandedMembership(Long groupId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_SQL_PARAM, groupId);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(EXPANDED_MEMBERSHIP_QUERY, params, new CamelCaseMapRowMapper());

        ArrayList<EntityDTO> resultsList = new ArrayList<EntityDTO>();
        for (Map<String, Object> node: nodes) {        
            // Query only returns Pax (SEARCH_TYPE = 'P')
            resultsList.add(new EmployeeDTO(node, null));
        }
        return resultsList;
    }
    
    private Boolean isPersonalGroup(Long groupId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_SQL_PARAM, groupId);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(SELECT_GROUP_TYPE_BY_GROUP_ID, params, new CamelCaseMapRowMapper());
        
        for (Map<String, Object> node: nodes) {
            if (GroupType.PERSONAL.getCode().equalsIgnoreCase((String) node.get(GROUP_TYPE_CODE))) {                
                return true;
            }
        }
        return false;
    }
}
