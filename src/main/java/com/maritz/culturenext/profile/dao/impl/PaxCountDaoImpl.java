package com.maritz.culturenext.profile.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.PaxDistinctDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.BulkDataUtil;

@Repository
public class PaxCountDaoImpl extends AbstractDaoImpl implements PaxCountDao {

    // Param names
    private static final String COUNT = "COUNT";
    private static final String SUBMITTER_PAX_ID = "SUBMITTER_PAX_ID";

    private static final String COUNT_DISTINCT_PAX_QUERY = "SELECT " +
            "COUNT(DISTINCT DATA.PAX_ID) AS " + COUNT + "  " +
            "FROM ( " +
            "    SELECT PAX_ID FROM component.VW_GROUP_TOTAL_MEMBERSHIP AS VW" +
            "    INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL" +
            "        ON GL." + BulkDataUtil.GROUP_ID_COLUMN + "= VW.GROUP_ID " +
            "    UNION ALL ( " +
            "        SELECT PAX_ID FROM component.PAX P" +
            "        INNER JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL" +
            "            ON PL." + BulkDataUtil.PAX_ID_COLUMN + "= P.PAX_ID " +
            "    ) " +
            ") AS DATA " +
            "INNER JOIN component.SYS_USER SU " +
            "ON DATA.PAX_ID = SU.PAX_ID " +
            "AND SU.STATUS_TYPE_CODE NOT IN ('INACTIVE', 'RESTRICTED')";
    private static final String COUNT_EXCLUDE_PAX = " WHERE DATA.PAX_ID != :" + SUBMITTER_PAX_ID;
    private static final String DISTINCT_PAX_QUERY = "SELECT " +
            "DISTINCT DATA.*, " +
            "CASE WHEN PM.MISC_DATA IS NULL THEN 'TRUE' ELSE PM.MISC_DATA END AS SHARE_REC, " +
            "R.PAX_ID_2 AS MANAGER_PAX_ID " +
            "FROM ( " +
            "    SELECT PAX_ID, GROUP_ID "
                + "FROM (SELECT PAX_ID, GROUP_ID, ROW_NUMBER() "
                    + "OVER (PARTITION BY PAX_ID ORDER BY GROUP_ID) AS ROW_NUM "
                        + "FROM component.VW_GROUP_TOTAL_MEMBERSHIP VW "
                        + "INNER JOIN " + BulkDataUtil.GROUP_ID_LIST_TABLE_NAME + " AS GL "
                            + "ON GL." + BulkDataUtil.GROUP_ID_COLUMN + "= VW.GROUP_ID "
                        + "WHERE VW.PAX_ID NOT IN ("
                            + "SELECT " + BulkDataUtil.PAX_ID_COLUMN
                            + " FROM " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME
                            + ")"
                + ") "
                + "AS GROUPS WHERE ROW_NUM = 1 " +
            "    UNION ALL ( " +
            "        SELECT PAX_ID, NULL FROM component.PAX P" +
            "        INNER JOIN " + BulkDataUtil.PAX_ID_LIST_TABLE_NAME + " AS PL " +
            "            ON PL." + BulkDataUtil.PAX_ID_COLUMN + "= P.PAX_ID " +
            "    ) " +
            ") AS DATA " +
            "INNER JOIN component.SYS_USER SU " +
            "ON DATA.PAX_ID = SU.PAX_ID " +
            "AND SU.STATUS_TYPE_CODE NOT IN ('INACTIVE', 'RESTRICTED') " +
            "LEFT JOIN component.PAX_MISC PM " +
            "ON DATA.PAX_ID = PM.PAX_ID " +
            "AND PM.VF_NAME = 'SHARE_REC' " +
            "LEFT JOIN component.RELATIONSHIP R " +
            "ON DATA.PAX_ID = R.PAX_ID_1 " +
            "AND R.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
            "WHERE DATA.PAX_ID != :" + SUBMITTER_PAX_ID;

    @Override
    public Integer getCountDistinctPax(List<Long> groupIds, List<Long> paxIds, Boolean excludeSelf) {

        groupIds = validateArray(groupIds);
        paxIds = validateArray(paxIds);

        MapSqlParameterSource params = new MapSqlParameterSource();

        String query = BulkDataUtil.buildCommonTablesQuery(COUNT_DISTINCT_PAX_QUERY,
                                                           params,
                                                           paxIds,
                                                           groupIds,
                                                           null);

        if (Boolean.TRUE.equals(excludeSelf)) {
            params.addValue(SUBMITTER_PAX_ID, security.getPaxId());
            query += COUNT_EXCLUDE_PAX;
        }

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params,
                new ColumnMapRowMapper());
        return (Integer) nodes.get(0).get(COUNT);
    }

    @Override
    public List<Map<String, Object>> getDistinctPax(Long submitterId, List<Long> groupIds, List<Long> paxIds) {

        groupIds = validateArray(groupIds);
        paxIds = validateArray(paxIds);


        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SUBMITTER_PAX_ID, submitterId);

        String query = BulkDataUtil.buildCommonTablesQuery(DISTINCT_PAX_QUERY,
                                                           params,
                                                           paxIds,
                                                           groupIds,
                                                           null);

        return namedParameterJdbcTemplate.query(query, params, new ColumnMapRowMapper());
    }

    @Override
    public List<PaxDistinctDTO> getDistinctPaxObject(Long submitterId, List<Long> groupIds, List<Long> paxIds) {
        List<Map<String, Object>> nodes = getDistinctPax(submitterId, groupIds, paxIds);

        List<PaxDistinctDTO> distinctPaxObjects = new ArrayList<PaxDistinctDTO>();
        for (Map<String, Object> node : nodes) {
            distinctPaxObjects.add(populateDistinctPax(node));
        }

        return distinctPaxObjects;
    }

    private PaxDistinctDTO populateDistinctPax(Map<String, Object> node) {
        PaxDistinctDTO paxDto = new PaxDistinctDTO();

        paxDto.setPaxId((Long) node.get(NominationConstants.PAX_ID_COLUMN_NAME));
        paxDto.setGroupId((Long) node.get(NominationConstants.GROUP_ID_COLUMN_NAME));
        paxDto.setManagerPaxId((Long) node.get(NominationConstants.MANAGER_PAX_ID_COLUMN_NAME));
        paxDto.setShareRec((String) node.get(VfName.SHARE_REC.toString()));

        return paxDto;
    }

    private List<Long> validateArray(List<Long> listToValidate) {
        if (listToValidate == null || listToValidate.isEmpty()) {
            listToValidate = new ArrayList<Long>();
            listToValidate.add(0L);
        }
        return listToValidate;
    }
}
