package com.maritz.culturenext.profile.dao;

import java.util.Map;

public interface GroupInfoDao {

    Map<String, Object> getGroupInfo(Long groupId);
}
