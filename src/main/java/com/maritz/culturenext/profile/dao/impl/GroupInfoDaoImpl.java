package com.maritz.culturenext.profile.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.GroupInfoDao;

@Repository
public class GroupInfoDaoImpl extends AbstractDaoImpl implements GroupInfoDao {
    
    private static final String QUERY = "SELECT DISTINCT " +
            "    G.GROUP_ID " +
            ",    G.GROUP_CONFIG_ID " +
            ",    GC.GROUP_CONFIG_NAME AS FIELD " +
            ",    GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME " +
            ",    G.GROUP_DESC AS GROUP_NAME " +
            ",    GT.GROUP_TYPE_DESC AS TYPE " +
            ",    GV.VISIBILITY " +
            ",    G.STATUS_TYPE_CODE AS STATUS " +
            ",    G.CREATE_DATE " +
            ",    (SELECT COUNT(*) FROM component.GROUPS_PAX WHERE GROUP_ID = G.GROUP_ID) AS PAX_COUNT " +
            ",    (SELECT COUNT(*) FROM component.VW_GROUPS_GROUPS WHERE GROUP_ID_1 = G.GROUP_ID) AS GROUP_COUNT " +
            ",    (SELECT COUNT(*) FROM component.VW_GROUP_TOTAL_MEMBERSHIP WHERE GROUP_ID = G.GROUP_ID) AS TOTAL_PAX_COUNT " +
            ",    G.PAX_ID AS GROUP_PAX_ID " +
            "FROM component.GROUPS G " +
            "LEFT OUTER JOIN component.GROUP_CONFIG GC " +
            "ON G.GROUP_CONFIG_ID = GC.ID " +
            "LEFT OUTER JOIN component.VW_GROUPS_VISIBILITY GV " +
            "ON GV.GROUP_ID = G.GROUP_ID " +
            "JOIN component.GROUP_TYPE GT " +
            "ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE " +
            "WHERE G.GROUP_ID = :GROUP_ID";

    @Override
    public Map<String, Object> getGroupInfo(Long groupId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("GROUP_ID", groupId);
        
            List<Map<String, Object>> nodes = 
                    namedParameterJdbcTemplate.query(QUERY, params, new CamelCaseMapRowMapper());
            // need to convert createDate from Date object to String.
            for (Map<String, Object> node: nodes) {
                node.put("createDate", DateUtil.convertToUTCDate((Date) node.get("createDate")));
            }
            if (!CollectionUtils.isEmpty(nodes)) {
                return nodes.get(0);
            } 
            
            //No data, throw an error message
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ProfileConstants.INVALID_GROUP)
                    .setField(ProjectConstants.GROUP_ID)
                    .setMessage(ProfileConstants.INVALID_GROUP_MSG));
    }

}
