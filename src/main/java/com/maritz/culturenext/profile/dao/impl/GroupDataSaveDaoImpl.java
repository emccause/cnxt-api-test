package com.maritz.culturenext.profile.dao.impl;

import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.profile.dao.GroupDataSaveDao;

@Repository
public class GroupDataSaveDaoImpl extends AbstractDaoImpl implements GroupDataSaveDao {
    
    private static final String ADMIN = "ADMIN";
    private static final String GROUP_ID = "GROUP_ID";
    private static final String VISIBILITY_ROLES_SQL_PARAM = "VISIBILITY_ROLES";
    private static final String VISIBILITY_INSERT_QUERY = 
            "INSERT INTO ACL (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) "
            + "SELECT ("
                + "SELECT ROLE_ID "
                + "FROM component.ROLE "
                + "WHERE ROLE_CODE = 'GVIS'"
            + ")"
            + " AS ROLE_ID, 'GROUPS' AS TARGET_TABLE, :" + GROUP_ID  
            + " AS TARGET_ID, 'ROLE' AS SUBJECT_TABLE, ROLE_ID AS SUBJECT_ID "
            + "FROM component.ROLE WHERE ROLE_CODE IN (:" + VISIBILITY_ROLES_SQL_PARAM + ")";
    private static final List<String> PUBLIC_VISIBILITY = Arrays.asList("RECG");
    private static final List<String> ADMIN_VISIBILITY = Arrays.asList("ADM", "CADM");
    private static final String PAX_INSERT_QUERY = "INSERT INTO GROUPS_PAX (GROUP_ID, PAX_ID) VALUES ";
    private static final String GROUP_INSERT_QUERY = "INSERT INTO ASSOCIATION (ASSOCIATION_TYPE_ID, FK1, FK2) VALUES ";
    private static final Long ASSOCIATION_TYPE_ID = 1000L;

    private static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST";
    private static final String VISIBILITY_DELETE_QUERY = "DELETE FROM ACL "
            + "WHERE TARGET_TABLE = 'GROUPS' "
            + "AND SUBJECT_TABLE = 'ROLE' "
            + "AND TARGET_ID = :" + GROUP_ID;
    private static final String PAX_DELETE_QUERY = "DELETE FROM GROUPS_PAX "
            + "WHERE GROUP_ID = :" + GROUP_ID + 
            " AND PAX_ID IN (:" + PAX_ID_LIST_SQL_PARAM + ")";
    private static final String GROUP_DELETE_QUERY = "DELETE FROM ASSOCIATION "
            + "WHERE ASSOCIATION_TYPE_ID = " + ASSOCIATION_TYPE_ID + " "
            + "AND FK1 = :" + GROUP_ID + " "
            + "AND FK2 IN (:" + GROUP_ID_LIST_SQL_PARAM + ")";

    @Override
    public void saveVisibility(Long groupId, String visibility) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        if (ADMIN.equalsIgnoreCase(visibility)) {
            params.addValue(VISIBILITY_ROLES_SQL_PARAM, ADMIN_VISIBILITY);
        } else {
            params.addValue(VISIBILITY_ROLES_SQL_PARAM, PUBLIC_VISIBILITY);
        }
        
        namedParameterJdbcTemplate.update(VISIBILITY_INSERT_QUERY, params);
    }

    @Override
    public void savePaxMembers(Long groupId, List<Long> paxIds) {
        String query = PAX_INSERT_QUERY;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        StringBuilder insertBuilder = new StringBuilder();
        for (int i = 0; i < paxIds.size(); i++) {
            insertBuilder.append("(:" + GROUP_ID + ", :PAX_ID"+ i + ")");
            params.addValue("PAX_ID" + i, paxIds.get(i));
            if (i != paxIds.size() - 1) {
                insertBuilder.append(",");
            }
        }
        query = query + insertBuilder.toString();
        
        namedParameterJdbcTemplate.update(query, params);
    }

    @Override
    public void saveGroupMembers(Long groupId, List<Long> groupIds) {
        String query = GROUP_INSERT_QUERY;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        StringBuilder insertBuilder = new StringBuilder();
        for (int i = 0; i < groupIds.size(); i++) {
            insertBuilder.append("(" + ASSOCIATION_TYPE_ID + ", :" + GROUP_ID + ", :GROUP_ID" + i + ")");
            params.addValue("GROUP_ID" + i, groupIds.get(i));
            if (i != groupIds.size() - 1) {
                insertBuilder.append(",");
            }
        }
        query = query + insertBuilder.toString();
    
        namedParameterJdbcTemplate.update(query, params);
    }

    @Override
    public void deleteVisibility(Long groupId){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        
        namedParameterJdbcTemplate.update(VISIBILITY_DELETE_QUERY, params);
    }

    @Override
    public void deletePaxMembers(Long groupId, List<Long> paxIds){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        params.addValue(PAX_ID_LIST_SQL_PARAM, paxIds);
        
        namedParameterJdbcTemplate.update(PAX_DELETE_QUERY, params);
    }

    @Override
    public void deleteGroupMembers(Long groupId, List<Long> groupIds){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID, groupId, Types.BIGINT);
        params.addValue(GROUP_ID_LIST_SQL_PARAM, groupIds);

        namedParameterJdbcTemplate.update(GROUP_DELETE_QUERY, params);
    }
}
