package com.maritz.culturenext.profile.dao;

import java.util.List;

import com.maritz.culturenext.profile.dto.EntityDTO;

public interface PaginatedCombinedSearchDao {

    List<EntityDTO> getSearchResults(String searchStr, Long paxId, List<String> visibilityList, 
            List<String> statusList, List<String> groupTypeList, Boolean excludeSelf);
}
