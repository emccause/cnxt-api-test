package com.maritz.culturenext.profile.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.dao.PaginatedGroupSearchDao;
import com.maritz.culturenext.util.DateUtil;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class PaginatedGroupSearchDaoImpl extends AbstractDaoImpl implements PaginatedGroupSearchDao {

    private static final String WHERE_CLAUSE_PLACEHOLDER = "{WHERE_CLAUSE_PLACEHOLDER}";
    
    private static final String GROUP_CONFIG_WHERE_CLAUSE = 
            "AND G.GROUP_CONFIG_ID = (:GROUP_CONFIG_ID) ";
    
    private static final String QUERY = "SELECT GROUP_ID, GROUP_CONFIG_ID, FIELD, FIELD_DISPLAY_NAME, GROUP_NAME, DISPLAY_NAME, "+
            "TYPE, VISIBILITY, STATUS, CREATE_DATE, PAX_COUNT, GROUP_COUNT, TOTAL_PAX_COUNT, GROUP_PAX_ID FROM ( " +
            "SELECT ROW_NUMBER() OVER (ORDER BY GROUP_NAME ASC) AS ROW_NUM, * FROM ( " +
            "SELECT DISTINCT " +
            "G.GROUP_ID AS GROUP_ID, " +
            "GC.ID AS GROUP_CONFIG_ID, " +
            "GC.GROUP_CONFIG_NAME AS FIELD, " +
            "GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME, " +
            "G.GROUP_DESC AS GROUP_NAME, " +
            "CASE " +
                "WHEN GC.GROUP_CONFIG_DESC IS NOT NULL THEN CONCAT(GC.GROUP_CONFIG_DESC, ' - ', G.GROUP_DESC) " +
                "ELSE G.GROUP_DESC " +
            "END AS DISPLAY_NAME, " +
            "GT.GROUP_TYPE_DESC AS TYPE, " +
            "GV.VISIBILITY AS VISIBILITY, " +
            "G.STATUS_TYPE_CODE AS STATUS, " +
            "G.CREATE_DATE AS CREATE_DATE, " +
            "(SELECT COUNT(*) FROM component.GROUPS_PAX WHERE GROUP_ID = G.GROUP_ID) AS PAX_COUNT, " +
            "(SELECT COUNT(*) FROM component.VW_GROUPS_GROUPS WHERE GROUP_ID_1 = G.GROUP_ID) AS GROUP_COUNT, " +
            "(SELECT COUNT(*) FROM component.VW_GROUP_TOTAL_MEMBERSHIP WHERE GROUP_ID = G.GROUP_ID) AS TOTAL_PAX_COUNT, " +
            "G.PAX_ID AS GROUP_PAX_ID " +
            "FROM COMPONENT.GROUPS G " +
            "LEFT JOIN component.GROUP_CONFIG GC " +
            "ON G.GROUP_CONFIG_ID = GC.ID " +
            "INNER JOIN component.GROUP_TYPE GT " +
            "ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE " +
            "LEFT JOIN component.VW_GROUPS_VISIBILITY GV " +
            "ON G.GROUP_ID = GV.GROUP_ID " +
            "WHERE G.PAX_ID IS NULL  " +
            "AND GT.GROUP_TYPE_DESC NOT IN ('HIERARCHY') " +
            "AND G.GROUP_DESC COLLATE Latin1_general_CI_AI LIKE :SEARCH_STRING COLLATE Latin1_general_CI_AI " + 
            WHERE_CLAUSE_PLACEHOLDER +
            ") AS INNER_QUERY " +
            ") AS SEARCH_QUERY " +
            "ORDER BY DISPLAY_NAME";
    
    @Override
    public List<Map<String, Object>> searchGroupsByNameAndConfigId(String searchString, Long groupConfigId) {
        
        // like %% is the equivalent of leaving the where clause off, 
        // which is fine if search string is missing (and makes the dynamic where clause easy to handle)
        String searchStringPrepped = "%" + searchString + "%"; 
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("SEARCH_STRING", searchStringPrepped, Types.NVARCHAR);
        String searchQuery;
        if (groupConfigId == null) { 
            searchQuery = QUERY.replace(WHERE_CLAUSE_PLACEHOLDER, ""); // remove the placeholder
        } else {
            params.addValue("GROUP_CONFIG_ID", groupConfigId);
            searchQuery = QUERY.replace(WHERE_CLAUSE_PLACEHOLDER, GROUP_CONFIG_WHERE_CLAUSE);
        }

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(searchQuery, params, new CamelCaseMapRowMapper());
        // need to convert createDate from Date object to String.
        for (Map<String, Object> node: nodes) {
            node.put("createDate", DateUtil.convertToUTCDate((Date) node.get("createDate")));
        }
        return nodes;
    }
}
