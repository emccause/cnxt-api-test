package com.maritz.culturenext.profile.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dao.PaginatedCombinedSearchDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class PaginatedCombinedSearchDaoImpl extends AbstractDaoImpl implements PaginatedCombinedSearchDao {

	private static final String SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM = "SEARCH_STRING_EMAIL_ADDRESS_RAW";
    private static final String STATUS_LIST_SQL_PARAM = "STATUS_LIST";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String VISIBILITY_LIST_SQL_PARAM = "VISIBILITY_LIST";
    private static final String GROUP_TYPE_LIST_SQL_PARAM = "GROUP_TYPE_LIST";
    private static final String SEARCH_TYPE = "SEARCH_TYPE";
    private static final String STATUS_TYPE_CODE = "STATUS_TYPE_CODE";
    
    //Result Set Columns (mapped to camelCase)
    private static final String RS_SEARCH_TYPE = "searchType";
    
    private static final String PAX_ID = "PAX_ID";
    private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    private static final String PREFERRED_LOCALE = "PREFERRED_LOCALE";
    private static final String CONTROL_NUM = "CONTROL_NUM";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String MIDDLE_NAME = "MIDDLE_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String NAME_PREFIX = "NAME_PREFIX";
    private static final String NAME_SUFFIX = "NAME_SUFFIX";
    private static final String COMPANY_NAME = "COMPANY_NAME";
    private static final String PROFILE_PICTURE_VERSION = "VERSION";
    private static final String JOB_TITLE = "JOB_TITLE";
    private static final String MANAGER_PAX_ID = "MANAGER_PAX_ID";
    private static final String COUNTRY_CODE = "COUNTRY_CODE";

    private static final String GROUP_ID = "GROUP_ID";
    private static final String GROUP_CONFIG_ID = "GROUP_CONFIG_ID";
    private static final String FIELD = "FIELD";
    private static final String FIELD_DISPLAY_NAME = "FIELD_DISPLAY_NAME";
    private static final String GROUP_NAME = "GROUP_NAME";
    private static final String GROUP_TYPE = "GROUP_TYPE";
    private static final String GROUP_PAX_ID = "GROUP_PAX_ID";
    private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    private static final String VISIBILITY = "VISIBILITY";
    private static final String CREATE_DATE = "CREATE_DATE";
    private static final String PAX_COUNT = "PAX_COUNT";
    private static final String GROUP_COUNT = "GROUP_COUNT";
    private static final String TOTAL_PAX_COUNT = "TOTAL_PAX_COUNT";

    private static final String WHERE_CLAUSE_PLACEHOLDER = "{WHERE_CLAUSE_PLACEHOLDER}";
    private static final String STATUS_TYPE_CODE_WHERE_CLAUSE = 
            "AND " + STATUS_TYPE_CODE + " IN (:" + STATUS_LIST_SQL_PARAM + ") ";
    private static final String GROUP_TYPE_WHERE_CLAUSE = 
            "AND ((" + GROUP_TYPE + " IS NULL) OR (" + GROUP_TYPE + " IN (:" + GROUP_TYPE_LIST_SQL_PARAM + ") )) ";
    private static final String EXCLUDE_SELF_WHERE_CLAUSE = 
            "AND (" + PAX_ID + " IS NULL OR " + PAX_ID + " != :" + PAX_ID_SQL_PARAM + ") ";
    
    private static final List<String> DEFAULT_VISIBILITY_LIST = Arrays.asList("PUBLIC","ADMIN");

    private static final String QUERY = "SELECT " +  
            SEARCH_TYPE +
            ", " + STATUS_TYPE_CODE +
            ", " + PAX_ID +
            ", " + LANGUAGE_CODE +
            ", " + PREFERRED_LOCALE +
            ", " + CONTROL_NUM +
            ", " + FIRST_NAME +
            ", " + MIDDLE_NAME + 
            ", " + LAST_NAME +
            ", " + NAME_PREFIX +
            ", " + NAME_SUFFIX +
            ", " + COMPANY_NAME +
            ", " + PROFILE_PICTURE_VERSION +
            ", " + JOB_TITLE +
            ", " + MANAGER_PAX_ID + 
            ", " + COUNTRY_CODE + 
            ", " + GROUP_ID +
            ", " + GROUP_CONFIG_ID +
            ", " + FIELD +
            ", " + FIELD_DISPLAY_NAME +
            ", " + GROUP_NAME +
            ", " + GROUP_TYPE +
            ", " + VISIBILITY +
            ", " + CREATE_DATE +
            ", " + PAX_COUNT +
            ", " + GROUP_COUNT +
            ", " + TOTAL_PAX_COUNT +
            ", " + GROUP_PAX_ID +
            ", " + EMAIL_ADDRESS +
            " FROM ( " +
            "SELECT ROW_NUMBER() OVER (ORDER BY NAME) as ROW_NUM, * FROM COMPONENT.VW_USER_GROUPS_SEARCH_DATA " +
            "WHERE (NAME COLLATE Latin1_general_CI_AI LIKE :" + ProfileConstants.SEARCH_STRING_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
            "OR CONTROL_NUM = :" + ProfileConstants.SEARCH_STRING_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
            "OR EMAIL_ADDRESS = :" + SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI) " +
            "AND ( " +
            "(" + GROUP_ID + " IS NULL) " +
            "OR  " +
            "(" + GROUP_ID + " IS NOT NULL AND " + VISIBILITY + " IN (:" + VISIBILITY_LIST_SQL_PARAM + ")) " +
            "OR (" + GROUP_ID + " IS NOT NULL AND " + VISIBILITY + " IS NULL AND " + GROUP_PAX_ID + " = :" + PAX_ID_SQL_PARAM + ")"  +
            ") " +
            WHERE_CLAUSE_PLACEHOLDER +
            ") AS SEARCH_QUERY ";
    
    @Override
    public List<EntityDTO> getSearchResults(String searchStr, Long paxId, List<String> visibilityList, 
            List<String> statusList, List<String> groupTypeList, Boolean excludeSelf) {

        String searchStringPrepped = "%" + searchStr + "%";
        
        String query = QUERY;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProfileConstants.SEARCH_STRING_SQL_PARAM, searchStringPrepped, Types.NVARCHAR);
        params.addValue(ProfileConstants.SEARCH_STRING_RAW_SQL_PARAM, searchStr, Types.NVARCHAR);
        params.addValue(SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM, searchStr, Types.NVARCHAR);
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        
        if (visibilityList == null) {
            visibilityList = DEFAULT_VISIBILITY_LIST;
        }
        params.addValue(VISIBILITY_LIST_SQL_PARAM, visibilityList);
        
        if (statusList != null || groupTypeList != null || excludeSelf == true) {
            
            StringBuilder whereBuilder = new StringBuilder();
            if (statusList != null) {
                whereBuilder.append(STATUS_TYPE_CODE_WHERE_CLAUSE);
                params.addValue(STATUS_LIST_SQL_PARAM, statusList);
            }
            if (groupTypeList != null) {
                whereBuilder.append(GROUP_TYPE_WHERE_CLAUSE);
                params.addValue(GROUP_TYPE_LIST_SQL_PARAM, groupTypeList);
            }
            if (excludeSelf == true) {
                whereBuilder.append(EXCLUDE_SELF_WHERE_CLAUSE);
            }
            
            query = query.replace(WHERE_CLAUSE_PLACEHOLDER, whereBuilder.toString());
        } else {
            query = query.replace(WHERE_CLAUSE_PLACEHOLDER, "");
        }
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());

        ArrayList<EntityDTO> resultsList = new ArrayList<EntityDTO>();
        for (Map<String, Object> node: nodes) {
            if (((String) node.get(RS_SEARCH_TYPE)).equalsIgnoreCase("P")) {                
                resultsList.add(new EmployeeDTO(node, null));
            } else {
                resultsList.add(new GroupDTO(node));
            }
        }
        return resultsList;
    }
}
