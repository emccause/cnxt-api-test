package com.maritz.culturenext.profile.dao;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

public interface GroupCreationValidationDao {

    Boolean isGroupNameDuplicate(String groupName, String type, Long paxId);

    Boolean isValidPersonalGroupsCount(Long paxId, List<Long> groupIds);
    
//    List<ErrorMessage> checkInvalidGroups(List<Long> groupIds, List<String> groupTypes);
    List<Map<String,Object>> getMembershipStatus(Long groupId, List<GroupMemberStubDTO> members, 
            List<String> validPaxStatus, List<String> invalidGroupTypes, String method);
}
