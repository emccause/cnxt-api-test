package com.maritz.culturenext.profile.dao;

import com.maritz.core.jpa.entity.Files;

public interface PaxImageDao {

    Files getProfileImageByIdAndType(Long paxId, String type);

    String getImageTypeCodeByType(String type);
}
