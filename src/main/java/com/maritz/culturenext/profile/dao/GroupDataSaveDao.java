package com.maritz.culturenext.profile.dao;

import java.util.List;

public interface GroupDataSaveDao {
    
    void saveVisibility(Long groupId, String visibility);
    void savePaxMembers(Long groupId, List<Long> paxIds);
    void saveGroupMembers(Long groupId, List<Long> groupIds);
    void deleteVisibility(Long groupId);
    void deletePaxMembers(Long groupId, List<Long> paxIds);
    void deleteGroupMembers(Long groupId, List<Long> groupIds);

}
