package com.maritz.culturenext.profile.dao;

import java.util.List;
import java.util.Map;

public interface ManagerDirectReportDao {
    List<Map<String, Object>> getManagerDirectReports(Long paxId, boolean excludeInactive, Integer pageNumber, Integer pageSize);
}
