package com.maritz.culturenext.profile.dao;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.profile.dto.PaxDistinctDTO;

public interface PaxCountDao {
    
    Integer getCountDistinctPax(List<Long> groupIds, List<Long> paxIds, Boolean excludeSelf);
    List<Map<String, Object>> getDistinctPax(Long submitterId, List<Long> groupIds, List<Long> paxIds);
    List<PaxDistinctDTO> getDistinctPaxObject(Long submitterId, List<Long> groupIds, List<Long> paxIds);
}
