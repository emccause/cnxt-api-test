package com.maritz.culturenext.profile.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.profile.dao.PaxImageDao;

@Repository
public class PaxImageDaoImpl extends AbstractDaoImpl implements PaxImageDao {
    
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String TYPE_SQL_PARAM = "TYPE";
    
    private static final String GET_PROFILE_IMAGE_QUERY = "SELECT TOP 1 F.* "
            + " FROM FILES F "
            + " INNER JOIN FILE_TYPE FT "
            + " ON FT.FILE_TYPE_CODE = F.FILE_TYPE_CODE "
            + " INNER JOIN PAX_FILES PF "
            + " ON PF.FILES_ID = F.ID "
            + " WHERE FT.FILE_TYPE_NAME = (:" + TYPE_SQL_PARAM + ") "
            + " AND PF.PAX_ID = :" + PAX_ID_SQL_PARAM
            + " ORDER BY VERSION";
    
    private static final String GET_FILE_TYPE_QUERY = 
            "SELECT FT.FILE_TYPE_CODE "
            + "FROM FILE_TYPE FT "
            + "WHERE FT.FILE_TYPE_NAME = ('Profile Image ' + :" + TYPE_SQL_PARAM + ")";

    @Override
    public Files getProfileImageByIdAndType(Long paxId, String type) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        
        params.addValue(TYPE_SQL_PARAM, "Profile Image " + type);
        
        List<Files> nodes = namedParameterJdbcTemplate.query(GET_PROFILE_IMAGE_QUERY, params, 
                new ConcentrixBeanRowMapper<Files>()
                        .setBeanClass(Files.class)
                        .setTable(jitBuilder.getTable(Files.class))
                        .build());
        // need to convert createDate from Date object to String.
            
        if (nodes != null && !nodes.isEmpty()) {
            return nodes.get(0);
        }
        
        return null;
    }
    

    @Override
    public String getImageTypeCodeByType(String type) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(TYPE_SQL_PARAM, type);
    
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(GET_FILE_TYPE_QUERY, params, new ColumnMapRowMapper());
        return (String) nodes.get(0).get("FILE_TYPE_CODE");
    }
}
