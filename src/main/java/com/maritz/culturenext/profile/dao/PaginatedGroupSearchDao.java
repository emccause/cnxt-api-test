package com.maritz.culturenext.profile.dao;

import java.util.List;
import java.util.Map;

public interface PaginatedGroupSearchDao {
    List<Map<String, Object>> searchGroupsByNameAndConfigId(String searchString, Long groupConfigId);
}
