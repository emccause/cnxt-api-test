package com.maritz.culturenext.profile.dao;

import java.util.ArrayList;
import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public interface PaginatedPaxSearchDao {
    
    List<EmployeeDTO> searchPaxByNameGroupsAndStatus(String searchString, List<ArrayList<Long>> groups, 
            List<String> statuses);
    
    List<EmployeeDTO> searchPaxByNameGroupsAndStatusAndProgram(String searchString, List<ArrayList<Long>> groups, 
            List<String> statuses, Integer programID, Boolean excludeSelf, Long paxId, Integer budgetId );

}
