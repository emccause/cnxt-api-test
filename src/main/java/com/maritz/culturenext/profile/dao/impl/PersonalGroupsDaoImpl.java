package com.maritz.culturenext.profile.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.dao.PersonalGroupsDao;
import com.maritz.culturenext.util.DateUtil;

@Repository
public class PersonalGroupsDaoImpl extends AbstractDaoImpl implements PersonalGroupsDao {
    
    private static final String PAX_ID_SQL_PARAM = "paxId";
    
    private static final String GET_PERSONAL_GROUPS_BY_PAX_ID = "SELECT DISTINCT " +
            "    G.GROUP_ID " +
            ",    G.GROUP_CONFIG_ID " +
            ",    GC.GROUP_CONFIG_NAME AS FIELD " +
            ",    GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME " +
            ",    G.GROUP_DESC AS GROUP_NAME " +
            ",    GT.GROUP_TYPE_DESC AS TYPE " +
            ",    GV.VISIBILITY " +
            ",    G.STATUS_TYPE_CODE AS STATUS " +
            ",    G.CREATE_DATE " +
            ",    (SELECT COUNT(*) FROM component.GROUPS_PAX WHERE GROUP_ID = G.GROUP_ID) AS PAX_COUNT " +
            ",    (SELECT COUNT(*) FROM component.VW_GROUPS_GROUPS WHERE GROUP_ID_1 = G.GROUP_ID) AS GROUP_COUNT " +
            ",    (SELECT COUNT(*) FROM component.VW_GROUP_TOTAL_MEMBERSHIP WHERE GROUP_ID = G.GROUP_ID) AS TOTAL_PAX_COUNT " +
            ",    G.PAX_ID AS GROUP_PAX_ID " +
            "FROM component.GROUPS G " +
            "LEFT OUTER JOIN component.GROUP_CONFIG GC " +
            "ON G.GROUP_CONFIG_ID = GC.ID " +
            "LEFT OUTER JOIN component.VW_GROUPS_VISIBILITY GV " +
            "ON GV.GROUP_ID = G.GROUP_ID " +
            "JOIN component.GROUP_TYPE GT " +
            "ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE " +
            "WHERE G.GROUP_TYPE_CODE = '" + GroupType.PERSONAL.getCode() + "' " +
            "AND G.PAX_ID = :" + PAX_ID_SQL_PARAM; 

    @Override
    public List<Map<String, Object>> getPersonalGroups(Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(GET_PERSONAL_GROUPS_BY_PAX_ID, params, new CamelCaseMapRowMapper());
        
        for (Map<String, Object> node: nodes) {
            node.put("createDate", DateUtil.convertToUTCDate((Date) node.get("createDate")));
        }
        return nodes;
    }

    
}
