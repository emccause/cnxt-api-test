package com.maritz.culturenext.profile.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.profile.dao.EnrollmentGroupsDao;
import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;

@Repository
public class EnrollmentGroupsDaoImpl extends AbstractDaoImpl implements EnrollmentGroupsDao {
    
    private static final String GROUP_CONFIG_ID = "GROUP_CONFIG_ID";
    private static final String FIELD = "GROUP_CONFIG_NAME";
    private static final String FIELD_DISPLAY_NAME = "GROUP_CONFIG_DESC";
    private static final String GROUP_CREATION = "STATUS_TYPE_CODE";
    private static final String VISIBILITY = "VISIBILITY";
    private static final String PUBLIC = "PUBLIC";
        
    private static final String QUERY = "select gc.ID as 'GROUP_CONFIG_ID', " +
            "gc.GROUP_CONFIG_NAME, gc.GROUP_CONFIG_DESC, " +
            "gc.STATUS_TYPE_CODE, vw_gc.VISIBILITY " +
            "from component.group_config gc " +
            "left join component.VW_GROUP_CONFIG_VISIBILITY vw_gc on vw_gc.GROUP_CONFIG_ID = gc.ID " +
            "where gc.GROUP_CONFIG_TYPE_CODE = 'ENRL_FIELD' " +
            "and gc.GROUP_TYPE_CODE = 'ENRL' ";

    @Override
    public List<EnrollmentGroupDTO> getEnrollmentGroups() {        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(QUERY, new MapSqlParameterSource(), new ColumnMapRowMapper());

        ArrayList<EnrollmentGroupDTO> resultsList = new ArrayList<EnrollmentGroupDTO>();
        for(Map<String, Object> node: nodes){
            EnrollmentGroupDTO result = new EnrollmentGroupDTO();
            result.setGroupConfigId((Long) node.get(GROUP_CONFIG_ID));
            result.setField((String) node.get(FIELD));
            result.setFieldDisplayName((String) node.get(FIELD_DISPLAY_NAME));
            result.setGroupCreation((String) node.get(GROUP_CREATION));
            if (node.get(VISIBILITY) == null && ((String) node.get(GROUP_CREATION)).equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                result.setVisibility(PUBLIC);
            } else {
                result.setVisibility((String) node.get(VISIBILITY));
            }

            resultsList.add(result);
        }
        return resultsList;
    }
}
