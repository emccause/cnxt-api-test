package com.maritz.culturenext.profile.dto;

public class SearchDTO {
    
    private Long paxId;
    private Long paxGroupId;
    private Long groupId;
    private String name;
    private String firstName;
    private String lastName;
    private Long groupPaxId;
    private String jobTitle;
    private Integer profilePictureVersion;
    
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public Long getPaxGroupId() {
        return paxGroupId;
    }
    public void setPaxGroupId(Long paxGroupId) {
        this.paxGroupId = paxGroupId;
    }
    public Long getGroupId() {
        return groupId;
    }
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Long getGroupPaxId() {
        return groupPaxId;
    }
    public void setGroupPaxId(Long groupPaxId) {
        this.groupPaxId = groupPaxId;
    }
    public String getJobTitle() {
        return jobTitle;
    }
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    public Integer getProfilePictureVersion() {
        return profilePictureVersion;
    }
    public void setProfilePictureVersion(Integer profilePictureVersion) {
        this.profilePictureVersion = profilePictureVersion;
    }
}
