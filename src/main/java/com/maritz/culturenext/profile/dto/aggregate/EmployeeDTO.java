package com.maritz.culturenext.profile.dto.aggregate;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.dto.EntityDTO;

import java.util.Map;

@ApiModel(description = "DTO representing user information (defined as an employee/participant). "
        + "Contains user information including participant ID, " 
        + "language information (language code, preferred locale), control number, "
        + "name information (first, last, middle initial, prefix/suffix), " 
        + "company information (company name, profile pic availablility, status, title), and first level manager.")
public class EmployeeDTO extends EntityDTO {


    //EmployeeDTO field mapping names
    //Note: Saving the field names this way because we might need to modify the constants to add a prefix
    private String PAX_ID = ProjectConstants.PAX_ID;
    private String LANGUAGE_CODE = ProjectConstants.LANGUAGE_CODE;
    private String LANGUAGE_LOCALE = ProjectConstants.LANGUAGE_LOCALE;
    private String PREFERRED_LOCALE = ProjectConstants.PREFERRED_LOCALE;
    private String CONTROL_NUM = ProjectConstants.CONTROL_NUM;
    private String FIRST_NAME = ProjectConstants.FIRST_NAME;
    private String MIDDLE_NAME = ProjectConstants.MIDDLE_NAME;
    private String LAST_NAME = ProjectConstants.LAST_NAME;
    private String NAME_PREFIX = ProjectConstants.NAME_PREFIX;
    private String NAME_SUFFIX = ProjectConstants.NAME_SUFFIX;
    private String COMPANY_NAME = ProjectConstants.COMPANY_NAME;

    private String PROFILE_PICTURE_VERSION = ProjectConstants.PROFILE_PICTURE_VERSION;
    private String STATUS_TYPE_CODE = ProjectConstants.STATUS_TYPE_CODE;
    private String JOB_TITLE = ProjectConstants.JOB_TITLE;
    private String SYS_USER_ID = ProjectConstants.SYS_USER_ID;
    private String MANAGER_PAX_ID = ProjectConstants.MANAGER_PAX_ID;
    private String ELIGIBLE_RECEIVER_IN_PROGRAM = ProjectConstants.ELIGIBLE_RECEIVER_IN_PROGRAM;
    private String COUNTRY_CODE = ProjectConstants.COUNTRY_CODE;
 
    Long paxId;
    String languageCode;
    String preferredLocale;
    String languageLocale;
    String controlNum;
    String firstName;
    String middleName;
    String lastName;
    String namePrefix;
    String nameSuffix;
    String companyName;
    Integer profilePictureVersion;
    String jobTitle;
    Long sysUserId;
    String status;
    Long managerPaxId;
    Boolean eligibleReceiverInProgram;
    String countryCode;

    /**
     * Basic constructor.
     */
    public EmployeeDTO() {
    }    
    
    /**
     * Constructor utilizing given field mapping for default values.
     *
     * @param node field mapping with default values
     */
    public EmployeeDTO(Map<String, Object> node, String prefix) {

        //Add any prefixes if needed
        if (prefix != null && !prefix.equalsIgnoreCase("")) {
            PAX_ID = prefix + 
                    "Id"; //PAX_ID will not become toPaxPaxId
            LANGUAGE_CODE = prefix + 
                    (Character.toUpperCase(LANGUAGE_CODE.charAt(0)) + LANGUAGE_CODE.substring(1));
            LANGUAGE_LOCALE = prefix + 
                    (Character.toUpperCase(LANGUAGE_LOCALE.charAt(0)) + LANGUAGE_LOCALE.substring(1));
            PREFERRED_LOCALE = prefix + 
                    (Character.toUpperCase(PREFERRED_LOCALE.charAt(0)) + PREFERRED_LOCALE.substring(1));
            CONTROL_NUM = prefix + 
                    (Character.toUpperCase(CONTROL_NUM.charAt(0)) + CONTROL_NUM.substring(1));
            FIRST_NAME = prefix + 
                    (Character.toUpperCase(FIRST_NAME.charAt(0)) + FIRST_NAME.substring(1));
            MIDDLE_NAME = prefix + 
                    (Character.toUpperCase(MIDDLE_NAME.charAt(0)) + MIDDLE_NAME.substring(1));
            LAST_NAME = prefix + 
                    (Character.toUpperCase(LAST_NAME.charAt(0)) + LAST_NAME.substring(1));
            NAME_PREFIX = prefix + 
                    (Character.toUpperCase(NAME_PREFIX.charAt(0)) + NAME_PREFIX.substring(1));
            NAME_SUFFIX = prefix + 
                    (Character.toUpperCase(NAME_SUFFIX.charAt(0)) + NAME_SUFFIX.substring(1));
            COMPANY_NAME = prefix + 
                    (Character.toUpperCase(COMPANY_NAME.charAt(0)) + COMPANY_NAME.substring(1));
            PROFILE_PICTURE_VERSION = prefix + 
                    (Character.toUpperCase(PROFILE_PICTURE_VERSION.charAt(0)) + PROFILE_PICTURE_VERSION.substring(1));
            STATUS_TYPE_CODE = prefix + 
                    (Character.toUpperCase(STATUS_TYPE_CODE.charAt(0)) + STATUS_TYPE_CODE.substring(1));
            JOB_TITLE = prefix + 
                    (Character.toUpperCase(JOB_TITLE.charAt(0)) + JOB_TITLE.substring(1));
            SYS_USER_ID = prefix + 
                    (Character.toUpperCase(SYS_USER_ID.charAt(0)) + SYS_USER_ID.substring(1));
            MANAGER_PAX_ID = prefix + 
                    (Character.toUpperCase(MANAGER_PAX_ID.charAt(0)) + MANAGER_PAX_ID.substring(1));
            ELIGIBLE_RECEIVER_IN_PROGRAM = prefix + 
                    (Character.toUpperCase(ELIGIBLE_RECEIVER_IN_PROGRAM.charAt(0)) + 
                            ELIGIBLE_RECEIVER_IN_PROGRAM.substring(1));
            COUNTRY_CODE = prefix + 
                    (Character.toUpperCase(COUNTRY_CODE.charAt(0)) + COUNTRY_CODE.substring(1));
        }

        //Populate the DTO
        this.paxId = (Long) node.get(PAX_ID);
        this.languageCode = (String) node.get(LANGUAGE_CODE);
        this.preferredLocale = (String) node.get(PREFERRED_LOCALE);
        this.languageLocale = (String) node.get(LANGUAGE_LOCALE);
        this.controlNum = (String) node.get(CONTROL_NUM);
        this.firstName = (String) node.get(FIRST_NAME);
        this.middleName = (String) node.get(MIDDLE_NAME);
        this.lastName = (String) node.get(LAST_NAME);
        this.namePrefix = (String) node.get(NAME_PREFIX);
        this.nameSuffix = (String) node.get(NAME_SUFFIX);
        this.companyName = (String) node.get(COMPANY_NAME);
        if (node.get(PROFILE_PICTURE_VERSION) != null) {
            this.profilePictureVersion = (Integer) node.get(PROFILE_PICTURE_VERSION);
        }
        this.status = (String) node.get(STATUS_TYPE_CODE);
        this.jobTitle = (String) node.get(JOB_TITLE);
        if (node.get(SYS_USER_ID) != null) {
            this.sysUserId = (Long) node.get(SYS_USER_ID);
        }
        if (node.get(MANAGER_PAX_ID) != null) {
            this.managerPaxId = (Long) node.get(MANAGER_PAX_ID);
        }
        if (node.get(ELIGIBLE_RECEIVER_IN_PROGRAM) != null) {
            this.eligibleReceiverInProgram = (Boolean) node.get(ELIGIBLE_RECEIVER_IN_PROGRAM);
        }
           this.countryCode = (String) node.get(COUNTRY_CODE);
    }

    @ApiModelProperty(position = 1, required = true, value = "participant ID")
    public Long getPaxId() {
        return paxId;
    }
    public EmployeeDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    @ApiModelProperty(position = 2, required = false, value = "language code")
    public String getLanguageCode() {
        return languageCode;
    }
    public EmployeeDTO setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    @ApiModelProperty(position = 3, required = false, value = "preferred locale")
    public String getPreferredLocale() {
        return preferredLocale;
    }
    public EmployeeDTO setPreferredLocale(String preferredLocale) {
        this.preferredLocale = preferredLocale;
        return this;
    }
    
    @ApiModelProperty(position = 4, required = false, value = "language locale")
    public String getLanguageLocale() {
        return languageLocale;
    }
    public EmployeeDTO setLanguageLocale(String languageLocale) {
        this.languageLocale = languageLocale;
        return this;
    }

    @ApiModelProperty(position = 5, required = false, value = "control num")
    public String getControlNum() {
        return controlNum;
    }
    public EmployeeDTO setControlNum(String controlNum) {
        this.controlNum = controlNum;
        return this;
    }

    @ApiModelProperty(position = 6, required = false, value = "first name")
    public String getFirstName() {
        return firstName;
    }
    public EmployeeDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @ApiModelProperty(position = 7, required = false, value = "initial of middle name")
    public String getMiddleName() {
        return middleName;
    }
    public EmployeeDTO setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @ApiModelProperty(position = 8, required = false, value = "last name")
    public String getLastName() {
        return lastName;
    }
    public EmployeeDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @ApiModelProperty(position = 9, required = false, value = "name prefix")
    public String getNamePrefix() {
        return namePrefix;
    }
    public EmployeeDTO setNamePrefix(String namePrefix) {
        this.namePrefix = namePrefix;
        return this;
    }

    @ApiModelProperty(position = 10, required = false, value = "name suffix")
    public String getNameSuffix() {
        return nameSuffix;
    }
    public EmployeeDTO setNameSuffix(String nameSuffix) {
        this.nameSuffix = nameSuffix;
        return this;
    }

    @ApiModelProperty(position = 11, required = false, value = "name of company")
    public String getCompanyName() {
        return companyName;
    }
    public EmployeeDTO setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    @ApiModelProperty(position = 12, required = false, value = "indicator for profile picture version")
    public Integer getProfilePictureVersion() {
        return profilePictureVersion;
    }

    public EmployeeDTO setProfilePictureVersion(Integer profilePictureVersion) {
        this.profilePictureVersion = profilePictureVersion;
        return this;
    }

    @ApiModelProperty(position = 13, required = true, value = "status within company")
    public String getStatus() {
        return status;
    }
    public EmployeeDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    @ApiModelProperty(position = 14, required = true, value = "title of job within company")
    public String getJobTitle() {
        return jobTitle;
    }
    public EmployeeDTO setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    @ApiModelProperty(position = 15, required = true, value = "Id for the login for this user")
    public Long getSysUserId() {
        return sysUserId;
    }

    public EmployeeDTO setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
        return this;
    }

    @ApiModelProperty(position = 16, required = false, value = "first level manager's paxId")
    public Long getManagerPaxId() {
        return managerPaxId;
    }
    public EmployeeDTO setManagerPaxId(Long managerPaxId) {
        this.managerPaxId = managerPaxId;
        return this;
    }
    
    
    @ApiModelProperty(position = 17, required = false, value = "pax is eligible to receive in the specified program")
    public Boolean getEligibleReceiverInProgram() {
        return eligibleReceiverInProgram;
    }
    public EmployeeDTO setEligibleReceiverInProgram(Boolean eligibleReceiverInProgram) {
        this.eligibleReceiverInProgram = eligibleReceiverInProgram;
        return this;
    }

    @ApiModelProperty(position = 18, required = false, value = "pax id country code")
    public String getCountryCode() {
        return countryCode;
    }
    public EmployeeDTO setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }
}