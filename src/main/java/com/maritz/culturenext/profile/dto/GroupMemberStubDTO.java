package com.maritz.culturenext.profile.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupMemberStubDTO extends EntityDTO {

    protected Long groupId;
    protected Long paxId;
    
    public Long getGroupId() {
        return groupId;
    }
    
    public GroupMemberStubDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }
    
    public Long getPaxId() {
        return paxId;
    }
    
    public GroupMemberStubDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GroupMemberStubDTO other = (GroupMemberStubDTO) obj;
        if (groupId == null) {
            if (other.groupId != null) {
                return false;
            }
        } else if (!groupId.equals(other.groupId)) {
            return false;
        }
        if (paxId == null) {
            if (other.paxId != null) {
                return false;
            }
        } else if (!paxId.equals(other.paxId)) {
            return false;
        }
        return true;
    }

}
