package com.maritz.culturenext.profile.dto;

import java.util.Date;

public class SysUserDTO {
    
    private Long sysUserId;
    private Long paxId;
    private String sysUserName;
    private Date statusDate;
    private String status;
    private Date sysUserPasswordDate;
    
    
    public Long getSysUserId() {
        return sysUserId;
    }
    public void setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
    }
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getSysUserName() {
        return sysUserName;
    }
    public void setSysUserName(String sysUserName) {
        this.sysUserName = sysUserName;
    }
    public Date getStatusDate() {
        return statusDate;
    }
    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Date getSysUserPasswordDate() {
        return sysUserPasswordDate;
    }
    public void setSysUserPasswordDate(Date sysUserPasswordDate) {
        this.sysUserPasswordDate = sysUserPasswordDate;
    }
}
