package com.maritz.culturenext.profile.dto.aggregate;

import java.util.List;

public class LightweightProfileInfoDTO {

    private String userName;
    private List<String> roles;
    private EmployeeDTO pax;
    private Boolean impersonated;
    private EmployeeDTO impersonatorPax;
    private Boolean profileIncomplete;
        
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public List<String> getRoles() {
        return roles;
    }
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }
    public EmployeeDTO getPax() {
        return pax;
    }
    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }
    public boolean getImpersonated() {
        return impersonated;
    }
    public void setImpersonated(boolean impersonated) {
        this.impersonated = impersonated;
    }
    public EmployeeDTO getImpersonatorPax() {
        return impersonatorPax;
    }
    public void setImpersonatorPax(EmployeeDTO impersonatorPax) {
        this.impersonatorPax = impersonatorPax;
    }
    public boolean isProfileIncomplete() {
        return profileIncomplete;
    }
    public void setProfileIncomplete(boolean profileIncomplete) {
        this.profileIncomplete = profileIncomplete;
    }
}
