package com.maritz.culturenext.profile.dto;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class EmailPasswordWrapperDTO {
    String password;
    @NotNull 
    @Valid
    List <EmailDTO> emails;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<EmailDTO> getEmails() {
        return emails;
    }

    public void setEmails(List<EmailDTO> emails) {
        this.emails = emails;
    }

    
    
    
    
}
