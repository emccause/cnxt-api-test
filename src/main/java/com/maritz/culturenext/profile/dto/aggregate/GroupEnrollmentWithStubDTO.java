package com.maritz.culturenext.profile.dto.aggregate;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

// This class is used for put/post 
@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupEnrollmentWithStubDTO extends GroupDTO {
    
    private List<GroupMemberStubDTO> members;

    public List<GroupMemberStubDTO> getMembers() {
        return members;
    }

    public GroupEnrollmentWithStubDTO setMembers(List<GroupMemberStubDTO> members) {
        this.members = members;
        return this;
    }

}
