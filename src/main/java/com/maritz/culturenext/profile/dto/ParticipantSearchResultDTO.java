package com.maritz.culturenext.profile.dto;

import com.maritz.culturenext.search.constants.SearchConstants;

import java.util.Map;

public class ParticipantSearchResultDTO {

    private String controlNumber;
    private String firstName;
    private String lastName;
    private String username;
    private String status;
    private String emailAddress;
    private String managerControlNumber;
    private String jobTitle;
    private String state;
    private String country;

    /**
     * Constructor utilizing given field mapping for default values.
     *
     * @param node field mapping with default values
     */
    public ParticipantSearchResultDTO (Map<String, Object> node) {
        if (node.get(SearchConstants.CONTROL_NUMBER) != null) {
            this.controlNumber = (String) node.get(SearchConstants.CONTROL_NUMBER);
        }
        if (node.get(SearchConstants.FIRST_NAME) != null) {
            this.firstName = (String) node.get(SearchConstants.FIRST_NAME);
        }
        if (node.get(SearchConstants.LAST_NAME) != null) {
            this.lastName = (String) node.get(SearchConstants.LAST_NAME);
        }
        if (node.get(SearchConstants.USER_NAME) != null) {
            this.username = (String) node.get(SearchConstants.USER_NAME);
        }
        if (node.get(SearchConstants.STATUS) != null) {
            this.status = (String) node.get(SearchConstants.STATUS);
        }
        if (node.get(SearchConstants.EMAIL_ADDRESS) != null) {
            this.emailAddress = (String) node.get(SearchConstants.EMAIL_ADDRESS);
        }
        if (node.get(SearchConstants.MANAGER_CONTROL_NUMBER) != null) {
            this.managerControlNumber = (String) node.get(SearchConstants.MANAGER_CONTROL_NUMBER);
        }
        if (node.get(SearchConstants.JOB_TITLE) != null) {
            this.jobTitle = (String) node.get(SearchConstants.JOB_TITLE);
        }
        if (node.get(SearchConstants.STATE) != null) {
            this.state = (String) node.get(SearchConstants.STATE);
        }
        if (node.get(SearchConstants.COUNTRY) != null) {
            this.country = (String) node.get(SearchConstants.COUNTRY);
        }
    }

    public String getControlNumber() {
        return controlNumber;
    }

    public ParticipantSearchResultDTO setControlNumber(String controlNumber) {
        this.controlNumber = controlNumber;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public ParticipantSearchResultDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public ParticipantSearchResultDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public ParticipantSearchResultDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public ParticipantSearchResultDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public ParticipantSearchResultDTO setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public String getManagerControlNumber() {
        return managerControlNumber;
    }

    public ParticipantSearchResultDTO setManagerControlNumber(String managerControlNumber) {
        this.managerControlNumber = managerControlNumber;
        return this;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public ParticipantSearchResultDTO setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public String getState() {
        return state;
    }

    public ParticipantSearchResultDTO setState(String state) {
        this.state = state;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public ParticipantSearchResultDTO setCountry(String country) {
        this.country = country;
        return this;
    }
}
