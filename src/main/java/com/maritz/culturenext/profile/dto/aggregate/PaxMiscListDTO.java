package com.maritz.culturenext.profile.dto.aggregate;

import java.util.List;

import com.maritz.culturenext.profile.dto.PaxMiscDTO;

public class PaxMiscListDTO {

    private List<PaxMiscDTO> paxMiscs;

    public List<PaxMiscDTO> getPaxMiscs() {
        return paxMiscs;
    }
    public PaxMiscListDTO setPaxMiscs(List<PaxMiscDTO> paxMiscs) {
        this.paxMiscs = paxMiscs;
        return this;
    }
}