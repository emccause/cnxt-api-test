package com.maritz.culturenext.profile.dto;

public class PaxCountDTO {

    Integer count;

    public Integer getCount() {
        return count;
    }

    public PaxCountDTO setCount(Integer count) {
        this.count = count;
        return this;
    }
    
}
