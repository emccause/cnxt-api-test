package com.maritz.culturenext.profile.dto;

import java.util.Date;
import java.util.Map;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.constants.GroupConstants;
import com.maritz.culturenext.util.DateUtil;

public class GroupDTO extends EntityDTO {

    private Long groupId;
    private Long groupConfigId;
    private String field;
    private String fieldDisplayName;
    private String groupName;
    private String type;
    private String visibility;
    private String status;
    private String createDate;
    private Integer paxCount;
    private Integer groupCount;
    private Integer totalPaxCount;
    private Long groupPaxId;
    private String displayName;

    /**
     * Basic constructor.
     */
    public GroupDTO() {

    }

    /**
     * Constructor utilizing given field mapping for default values.
     *
     * @param node field mapping with default values
     */
    public GroupDTO (Map<String, Object> node) {
        if(node.get(GroupConstants.GROUP_ID) != null) {
            this.groupId = (Long) node.get(GroupConstants.GROUP_ID);
        }
        if(node.get(GroupConstants.GROUP_CONFIG_ID) != null) {
            this.groupConfigId = (Long) node.get(GroupConstants.GROUP_CONFIG_ID);
        }
        this.field = (String) node.get(GroupConstants.FIELD);
        this.fieldDisplayName = (String) node.get(GroupConstants.FIELD_DISPLAY_NAME);
        this.groupName = (String) node.get(GroupConstants.GROUP_NAME);
        this.type = (String) node.get(GroupConstants.GROUP_TYPE);
        this.visibility = (String) node.get(GroupConstants.VISIBILITY);
        this.status = (String) node.get(GroupConstants.STATUS_TYPE_CODE);
        if(node.get(GroupConstants.CREATE_DATE) != null) {
            this.createDate = DateUtil.convertToUTCDate((Date) node.get(GroupConstants.CREATE_DATE));
        }
        if(node.get(GroupConstants.PAX_COUNT) != null) {
            this.paxCount = (Integer) node.get(GroupConstants.PAX_COUNT);
        }
        if(node.get(GroupConstants.GROUP_COUNT) != null) {
            this.groupCount = (Integer) node.get(GroupConstants.GROUP_COUNT);
        }
        if(node.get(GroupConstants.TOTAL_PAX_COUNT) != null) {
            this.totalPaxCount = (Integer) node.get(GroupConstants.TOTAL_PAX_COUNT);
        }
        if(node.get(GroupConstants.GROUP_PAX_ID) != null) {
            this.groupPaxId = (Long) node.get(GroupConstants.GROUP_PAX_ID);
        }
        this.displayName = (String) node.get(ProjectConstants.DISPLAY_NAME);
    }

    public Long getGroupId() {
        return groupId;
    }
    
    public GroupDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }
    
    public Long getGroupConfigId() {
        return groupConfigId;
    }
    
    public GroupDTO setGroupConfigId(Long groupConfigId) {
        this.groupConfigId = groupConfigId;
        return this;
    }
    
    public String getField() {
        return field;
    }
    
    public GroupDTO setField(String field) {
        this.field = field;
        return this;
    }
    
    public String getFieldDisplayName() {
        return fieldDisplayName;
    }
    
    public GroupDTO setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
        return this;
    }
    
    public String getGroupName() {
        return groupName;
    }

    public GroupDTO setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public String getType() {
        return type;
    }
    
    public GroupDTO setType(String type) {
        this.type = type;
        return this;
    }
    
    public String getVisibility() {
        return visibility;
    }
    
    public GroupDTO setVisibility(String visibility) {
        this.visibility = visibility;
        return this;
    }
    
    public String getStatus() {
        return status;
    }
    
    public GroupDTO setStatus(String status) {
        this.status = status;
        return this;
    }
    
    public String getCreateDate() {
        return createDate;
    }
    
    public GroupDTO setCreateDate(String createDate) {
        this.createDate = createDate;
        return this;
    }
    
    public Integer getPaxCount() {
        return paxCount;
    }
    
    public GroupDTO setPaxCount(Integer paxCount) {
        this.paxCount = paxCount;
        return this;
    }

    public Integer getGroupCount() {
        return groupCount;
    }

    public GroupDTO setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
        return this;
    }

    public Integer getTotalPaxCount() {
        return totalPaxCount;
    }

    public GroupDTO setTotalPaxCount(Integer totalPaxCount) {
        this.totalPaxCount = totalPaxCount;
        return this;
    }

    public Long getGroupPaxId() {
        return groupPaxId;
    }

    public GroupDTO setGroupPaxId(Long groupPaxId) {
        this.groupPaxId = groupPaxId;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public GroupDTO setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
}
