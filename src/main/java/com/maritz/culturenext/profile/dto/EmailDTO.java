package com.maritz.culturenext.profile.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.constants.ProfileConstants;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailDTO {
    
    private Long emailId;
    private Long paxId;
    
    @NotEmpty (message=ProfileConstants.ERROR_EMAIL_MUST_BE_SUPPLIED_MSG)
    @Email (message=ProfileConstants.INVALID_EMAIL_FORMAT_MSG)
    private String emailAddress;
    
    private String preferred;
    private String emailTypeCode;
    private Boolean editable;
    
    
    public Long getEmailId() {
        return emailId;
    }
    public void setEmailId(Long emailId) {
        this.emailId = emailId;
    }
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getPreferred() {
        return preferred;
    }
    public void setPreferred(String preferred) {
        this.preferred = preferred;
    }
    public String getEmailTypeCode() {
        return emailTypeCode;
    }
    public void setEmailTypeCode(String emailTypeCode) {
        this.emailTypeCode = emailTypeCode;
    }
    public boolean getEditable() {
        return editable;
    }
    public void setEditable(boolean editable) {
        this.editable = editable;
    }
    public boolean isNullEditable() {
        if(editable==null){
            return true;
        }else{
            return false;
        }
    }
}