package com.maritz.culturenext.profile.dto;

import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.util.CollectionBuilder;
import com.maritz.core.util.ObjectUtils;

public class PaxMiscDTO {

    private Long paxId;
    private Long paxMiscId;
    private String vfName;
    private String miscData;
    private Long proxyPaxId;

    public PaxMiscDTO() {
    }

    public PaxMiscDTO(PaxMisc paxMisc) {
        if (paxMisc != null) {
            ObjectUtils.objectToObject(paxMisc, this);
        }
    }

    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public Long getPaxMiscId() {
        return paxMiscId;
    }
    public void setPaxMiscId(Long paxMiscId) {
        this.paxMiscId = paxMiscId;
    }
    public String getVfName() {
        return vfName;
    }
    public void setVfName(String vfName) {
        this.vfName = vfName;
    }
    public String getMiscData() {
        return miscData;
    }
    public void setMiscData(String miscData) {
        this.miscData = miscData;
    }
    public Long getProxyPaxId() {
        return this.proxyPaxId;
    }
    public void setProxyPaxId(Long proxyPaxId) {
        this.proxyPaxId = proxyPaxId;
    }


    public static List<PaxMiscDTO> fromPaxMiscs(Iterable<PaxMisc> paxMiscs) {
        return CollectionBuilder.<PaxMiscDTO>newArrayList()
            .map(paxMiscs, fromPaxMiscMapper())
            .buildList()
        ;
    }

    public static Converter<PaxMisc, PaxMiscDTO> fromPaxMiscMapper() {
        return new Converter<PaxMisc, PaxMiscDTO>() {
            @Override
            public PaxMiscDTO convert(PaxMisc source) {
                return new PaxMiscDTO(source);
            }
        };
    }

}