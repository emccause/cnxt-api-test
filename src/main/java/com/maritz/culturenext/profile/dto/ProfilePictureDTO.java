package com.maritz.culturenext.profile.dto;

public class ProfilePictureDTO {
    
    private Integer profilePictureVersion;
    
    
    public void setProfilePictureVersion(Integer profilePictureVersion){
        this.profilePictureVersion = profilePictureVersion;
    }
    
    public Integer getProfilePictureVersion(){
        return profilePictureVersion;
    }

}