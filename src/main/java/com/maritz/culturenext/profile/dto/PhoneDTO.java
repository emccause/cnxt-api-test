package com.maritz.culturenext.profile.dto;

public class PhoneDTO {
    
    private Long phoneId;
    private Long paxId;
    private String phone;
    private String phoneExt;
    private String preferred;
    private String phoneTypeCode;
    
    
    public Long getPhoneId() {
        return phoneId;
    }
    public void setPhoneId(Long phoneId) {
        this.phoneId = phoneId;
    }
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhoneExt() {
        return phoneExt;
    }
    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }
    public String getPreferred() {
        return preferred;
    }
    public void setPreferred(String preferred) {
        this.preferred = preferred;
    }
    public String getPhoneTypeCode() {
        return phoneTypeCode;
    }
    public void setPhoneTypeCode(String phoneTypeCode) {
        this.phoneTypeCode = phoneTypeCode;
    }
}