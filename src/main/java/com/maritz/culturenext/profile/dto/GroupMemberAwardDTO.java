package com.maritz.culturenext.profile.dto;

public class GroupMemberAwardDTO extends GroupMemberStubDTO {
    private Integer awardAmount;
    private Integer originalAmount;
    private Long awardTierId;
    
    public Integer getAwardAmount() {
        return awardAmount;
    }
    public GroupMemberAwardDTO setAwardAmount(Integer awardAmount) {
        this.awardAmount = awardAmount;
        return this;
    }
    public Integer getOriginalAmount() {
        return originalAmount;
    }
    public GroupMemberAwardDTO setOriginalAmount(Integer originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }
    public Long getAwardTierId() {
        return awardTierId;
    }
    public GroupMemberAwardDTO setAwardTierId(Long awardTierId) {
        this.awardTierId = awardTierId;
        return this;
    }
    
    public GroupMemberAwardDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }

    public GroupMemberAwardDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }
    
}
