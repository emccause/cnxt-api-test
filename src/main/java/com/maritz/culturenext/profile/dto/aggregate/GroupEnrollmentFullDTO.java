package com.maritz.culturenext.profile.dto.aggregate;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupEnrollmentFullDTO extends GroupDTO {
    
    private List<EntityDTO> members;

    public List<EntityDTO> getMembers() {
        return members;
    }

    public GroupEnrollmentFullDTO setMembers(List<EntityDTO> members) {
        this.members = members;
        return this;
    }

}
