package com.maritz.culturenext.profile.dto;

public class PaxDistinctDTO {
    Long paxId;
    Long groupId;
    String shareRec;
    Long managerPaxId;
    
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public Long getGroupId() {
        return groupId;
    }
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    public String getShareRec() {
        return shareRec;
    }
    public void setShareRec(String shareRec) {
        this.shareRec = shareRec;
    }
    public Long getManagerPaxId() {
        return managerPaxId;
    }
    public void setManagerPaxId(Long managerPaxId) {
        this.managerPaxId = managerPaxId;
    }
    
    
}
