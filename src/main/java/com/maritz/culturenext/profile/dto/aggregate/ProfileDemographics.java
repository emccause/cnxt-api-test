package com.maritz.culturenext.profile.dto.aggregate;

import java.util.List;

import com.maritz.culturenext.profile.dto.AddressDTO;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.PhoneDTO;
import com.maritz.culturenext.profile.dto.SysUserDTO;

public class ProfileDemographics {
    
    private EmployeeDTO pax;
    private List<SysUserDTO> sysUsers;
    private List<AddressDTO> addresses;
    private List<EmailDTO> emails;
    private List<PhoneDTO> phones;

    public EmployeeDTO getPax() {
        return pax;
    }
    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }
    public List<SysUserDTO> getSysUsers() {
        return sysUsers;
    }
    public void setSysUsers(List<SysUserDTO> sysUsers) {
        this.sysUsers = sysUsers;
    }
    public List<AddressDTO> getAddresses() {
        return addresses;
    }
    public void setAddresses(List<AddressDTO> addresses) {
        this.addresses = addresses;
    }
    public List<EmailDTO> getEmails() {
        return emails;
    }
    public void setEmails(List<EmailDTO> emails) {
        this.emails = emails;
    }
    public List<PhoneDTO> getPhones() {
        return phones;
    }
    public void setPhones(List<PhoneDTO> phones) {
        this.phones = phones;
    }
}