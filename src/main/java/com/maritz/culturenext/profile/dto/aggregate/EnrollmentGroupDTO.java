package com.maritz.culturenext.profile.dto.aggregate;

public class EnrollmentGroupDTO {

    private Long groupConfigId;
    private String field;
    private String fieldDisplayName;
    private String groupCreation;
    private String visibility;
    
    public Long getGroupConfigId() {
        return groupConfigId;
    }
    public void setGroupConfigId(Long groupConfigId) {
        this.groupConfigId = groupConfigId;
    }
    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }
    public String getFieldDisplayName() {
        return fieldDisplayName;
    }
    public void setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
    }
    public String getGroupCreation() {
        return groupCreation;
    }
    public void setGroupCreation(String groupCreation) {
        this.groupCreation = groupCreation;
    }
    public String getVisibility() {
        return visibility;
    }
    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
    
}