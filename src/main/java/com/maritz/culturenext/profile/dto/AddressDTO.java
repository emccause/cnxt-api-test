package com.maritz.culturenext.profile.dto;

public class AddressDTO {
    
    private Long addressId;
    private Long paxId;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String address5;
    private String address6;
    private String city;
    private String state;
    private String zip;
    private String zip4;
    private String zip2;
    private String country;
    private String code1Bypass;
    private String code1Status;
    private String addressTypeCode;
    private String preferred;
    
    
    public Long getAddressId() {
        return addressId;
    }
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getAddress1() {
        return address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }
    public String getAddress2() {
        return address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }
    public String getAddress3() {
        return address3;
    }
    public void setAddress3(String address3) {
        this.address3 = address3;
    }
    public String getAddress4() {
        return address4;
    }
    public void setAddress4(String address4) {
        this.address4 = address4;
    }
    public String getAddress5() {
        return address5;
    }
    public void setAddress5(String address5) {
        this.address5 = address5;
    }
    public String getAddress6() {
        return address6;
    }
    public void setAddress6(String address6) {
        this.address6 = address6;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getZip4() {
        return zip4;
    }
    public void setZip4(String zip4) {
        this.zip4 = zip4;
    }
    public String getZip2() {
        return zip2;
    }
    public void setZip2(String zip2) {
        this.zip2 = zip2;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCode1Bypass() {
        return code1Bypass;
    }
    public void setCode1Bypass(String code1Bypass) {
        this.code1Bypass = code1Bypass;
    }
    public String getCode1Status() {
        return code1Status;
    }
    public void setCode1Status(String code1Status) {
        this.code1Status = code1Status;
    }
    public String getAddressTypeCode() {
        return addressTypeCode;
    }
    public void setAddressTypeCode(String addressTypeCode) {
        this.addressTypeCode = addressTypeCode;
    }
    public String getPreferred() {
        return preferred;
    }
    public void setPreferred(String preferred) {
        this.preferred = preferred;
    }
}
