package com.maritz.culturenext.profile.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.ProfilePictureDTO;
import com.maritz.culturenext.profile.services.PaxImageService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("participants")
@Api(value="/profile", description="All end points dealing with user profiles")
public class PaxProfileImageRestService {

    @Inject private PaxImageService paxImageService;
    @Inject private Security security;

    //rest/participants/~/profile/picture
    @RequestMapping(value = "{paxId}/profile/picture", method = RequestMethod.GET)
    @ApiOperation(value = "gets the logged in user's profile picture")
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPictureForPax(
            @ApiParam(value = "paxId of the user picture you want to see.")@PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value = "Size values:<br><br>Default<br>tiny", required = false)
                @RequestParam(value = SIZE_REST_PARAM, required = false) String size) throws Exception{

        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        return paxImageService.getImage(paxId, size);
    }

    //rest/participants/~/profile/picture
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/picture", method = RequestMethod.POST)
    @ApiOperation(value = "upload a profile picture")
    @Permission("PUBLIC")
    public ResponseEntity<ProfilePictureDTO> uploadPictureForPax(
            @ApiParam(value="PaxId of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="crop=x,y,w,h<br><br> will crop the rectangle started at x, y<br><br>"
                    + "it will have the resulting dimensions of w,h", required = false)
            @RequestParam(value = CROP_REST_PARAM, required = false) String crop, 
            @ApiParam(value = "Picture you want to upload")@RequestParam(IMAGE_REST_PARAM) MultipartFile[] images,
            @ApiParam(value = "Whether or not to circle crop the image") @RequestParam(name = "circleCrop", required = false, defaultValue = "false") Boolean circleCrop
            ) throws Exception{
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            return new ResponseEntity<ProfilePictureDTO>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<ProfilePictureDTO>(paxImageService.uploadImage(paxId, crop, images, circleCrop),HttpStatus.OK);

    }

    //rest/participants/~/profile/picture
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/picture", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes uploaded picture for the user.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> deletePictureForPax(
            @ApiParam(value = "PaxId of the logged in user" )@PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Exception{
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
        }
        
        paxImageService.deleteImage(paxId);
        return new ResponseEntity<Object>(HttpStatus.OK);

    }
}
