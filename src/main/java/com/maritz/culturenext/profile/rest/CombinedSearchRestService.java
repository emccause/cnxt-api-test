package com.maritz.culturenext.profile.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.services.CombinedSearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value="/profile", description="All end points dealing with user profiles")
public class CombinedSearchRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private CombinedSearchService searchService;

    //rest/search
    @RequestMapping(value = "search", method = RequestMethod.GET, headers = {CURRENT_VERSION_HEADER})
    @ApiOperation(value="Returns a list of participants or groups based on the searchStr value.", 
        notes ="If results are not found an empty array will be sent. The parameters for page and size are optional, "
            + "but can be used to find additional results. If those fields are left blank a default of page 1 and "
            + "size 50 will be used.<br><br>"
            +" New object model with separate objects for participants and groups is returned. "
            + "A heterogenous list (list with group objects & pax objects is returned), "
            + "If the record contains a paxId it is a person. If the record contains a groupId it will be a group. "
            + "If they have a groupId and groupPaxId it is a personal-group. "
            + "The groupPaxId should always be the paxId of the user who initiated the search. "
            + "The new Pax object will contain 2 new properties status & jobTitle.<br><br>"
            +" Group 'type' values are - 'ENROLLMENT', 'CUSTOM', 'ADMIN','ROLE_BASED', 'PERSONAL'<br><br>"
            +" status' values are 'ACTIVE', 'INACTIVE','LOCKED','NEW','SUSPENDED','RESTRICTED','RESET'<br><br>"
            +" Group 'visibility' values are 'PUBLIC', 'ADMIN'<br><br>"
            +" 'excludeSelf' param support doc added 6/22/2015 : In order to prevent users from giving recognition to "
            + "themselves, we need to exclude them in the combined search results. it's boolean value with 'true' to "
            + "indicate exclusion of self from results. Default would be that self is included (excludeSelf=false).")
    @Permission("PUBLIC")
    public List<EntityDTO> getSearchResults(
            @ApiParam(value="", required = true)
                @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchStr, 
                //Pagination Techdebt: MP-7508 & MP-7525
            @ApiParam(value="Number of pages you want to return for the search result", required = false) 
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @ApiParam(value="Number of items per page you want to return for the search result", required = false) 
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @ApiParam(value="values are 'PUBLIC', 'ADMIN'", required = false) 
                @RequestParam(value = VISIBILITY_REST_PARAM, required = false) String visibilityListString,
            @ApiParam(value="values are 'ACTIVE', 'INACTIVE','LOCKED','NEW','SUSPENDED','RESTRICTED','RESET'", 
                required = false) @RequestParam(value = STATUS_REST_PARAM, required = false) String statusListString,
            @ApiParam(value="values are - 'ENROLLMENT', 'CUSTOM', 'ADMIN','ROLE_BASED', 'PERSONAL'", required = false)
                @RequestParam(value = GROUP_TYPE_REST_PARAM, required = false) String groupTypeListString,
            @ApiParam(value="See bottom of notes, Defaults to false", required = false) 
                @RequestParam(value = EXCLUDE_SELF_REST_PARAM, required = false) String excludeSelfString) throws Throwable{
        return searchService.getSearchResults( searchStr, page, size, 
                visibilityListString, statusListString, groupTypeListString, excludeSelfString);             
    }
}
