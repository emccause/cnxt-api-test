package com.maritz.culturenext.profile.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.*;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.PaxCountDTO;
import com.maritz.culturenext.profile.services.PaxCountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import static com.maritz.culturenext.constants.RestParameterConstants.EXCLUDE_SELF_REST_PARAM;

@RestController
@RequestMapping("participants")
@Api(value="/participants", description="All end points dealing with participants")
public class PaxCountRestService {
    
    @Inject private PaxCountService paxCountService;
    
    //rest/participants/count
    @RequestMapping(value = "count", method = RequestMethod.POST)
    @ApiOperation(value = "Calculate the number of unique participants from an adhoc list of paxes and groups<br><br>"
            + "ex: 1, [2, 3, 1] -> 3 unique", notes = "may contain these group types (custom, enrollment, "
                    + "role-based, personal)<br><br>Unique pax count will need to be returned.Will need to "
                    + "include inactive & restricted paxes if they are in the groups for any reason")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the count of unique users from the specified groups and particiapnts.", 
        response = PaxCountDTO.class)})
    @Permission("PUBLIC")
    public PaxCountDTO getCountDistinctPax(
            @ApiParam(value="Body containing the paxIds and groupIds you want to check")
            @RequestBody List<GroupMemberStubDTO> members,
            @ApiParam(value = "Flag to exclude the user from being counted")
            @RequestParam(value = EXCLUDE_SELF_REST_PARAM, required = false) Boolean excludeSelf) {
        return paxCountService.getCountDistinctPax(members, excludeSelf);
    }

}