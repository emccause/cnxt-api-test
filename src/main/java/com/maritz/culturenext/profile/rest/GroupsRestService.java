package com.maritz.culturenext.profile.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.jpa.entity.Groups;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentFullDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentWithStubDTO;
import com.maritz.culturenext.profile.services.GroupSearchService;
import com.maritz.culturenext.profile.services.GroupsService;

@RestController
@Api(value="/profile", description="All end points dealing with user profiles")
public class GroupsRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private GroupSearchService groupSearchService;
    @Inject private GroupsService groupsService;
    @Inject private Security security;

    //rest/groups/active
    @PreAuthorize("@security.hasRole('3rdP:PLSM') and !@security.isImpersonated()")
    @RequestMapping(value = "groups/active", method = RequestMethod.GET)
    @ResponseBody
    @Permission("PUBLIC")
    public List<Groups> getActiveGroups() {
        List<Groups> groups = groupsService.getActiveGroups();
        return groups;
    }

    //rest/groups
    @RequestMapping(value = "groups", method = RequestMethod.GET, headers = {CURRENT_VERSION_HEADER})
    @ApiOperation(value = "Get a list of groups by search criteria")
    @Permission("PUBLIC")
    public List<Map<String, Object>> getGroupSearchResults(
            @ApiParam(value = "String of contents you are searching for") 
                @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchStr, 
                //TODO Pagination Techdebt: MP-7509 & MP-7526
            @ApiParam(value = "Number of pages of results you want to return") 
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @ApiParam(value = "Number of results per page you want") 
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size, 
            @ApiParam(value = "group sub-type id") 
                @RequestParam(value = GROUP_CONFIG_ID_REST_PARAM, required = false) String groupConfigIdString
            ) throws Exception{
        
        
        return groupSearchService.findGroups(searchStr, page, size, groupConfigIdString);
    }

    //rest/groups/~
    @RequestMapping(value = "groups/{groupId}", method = RequestMethod.GET)
    @ApiOperation(value = "Get the details of a specified group. It does not return the members of the group.", 
        notes ="members are returned by another endpoint<br><br>'type' values are - "
                + "'ENROLLMENT', 'CUSTOM' ,'ROLE_BASED', 'PERSONAL'(unused for MVP)<br><br>status' values are "
                + "'ACTIVE', 'INACTIVE'<br><br>'visibility' values are 'PUBLIC', 'ADMIN'<br><br>groupCount - "
                + "immediate group members,paxCount - immediate pax members,totalPaxCount - "
                + "paxes including those through groups.")
    @Permission("PUBLIC")
    public Map<String, Object> getGroupInfo(
            @ApiParam(value = "Id of the group you want to get the details of")
                @PathVariable(GROUP_ID_REST_PARAM) String groupIdString
            ) throws Exception {
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return groupsService.getGroupInfo(groupId);
    }
    
    //rest/groups/~/members
    @RequestMapping(value = "groups/{groupId}/members", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a list of participants or groups for the specified group id.", 
        notes ="If results are not found an empty array will be sent. The parameters for page and size are optional, "
                + "but can be used to find additional results. If those fields are left blank a default of page 1 and "
                + "size 50 will be used. Results will be sorted alphabetical, similar to Combined-search, ie., "
                + "on combined-name (group-name/'first-name last-name')<br><br>Group 'type' values are - "
                + "'ENROLLMENT', 'CUSTOM', 'ADMIN','ROLE_BASED', 'PERSONAL'<br><br>status' values for pax are "
                + "'ACTIVE', 'INACTIVE','LOCKED','SUSPENDED','RESTRICTED'<br><br>status values for group are "
                + "'ACTIVE', 'INACTIVE<br><br>Group 'visibility' values are 'PUBLIC', 'ADMIN'")
    @Permission("PUBLIC")
    public List<EntityDTO> getGroupMembers(
            @ApiParam(value = "Id of the group you want to get the details of") 
                @PathVariable(GROUP_ID_REST_PARAM) String groupIdString,
                //TODO Pagination Techdebt: MP-7510 & MP-7527
            @ApiParam(value = "Number of pages of results you want to return") 
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @ApiParam(value = "Number of results per page you want") 
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size, 
            @ApiParam(value = "Flag to indicate if sub-groups should be expanded into list of pax.")
                @RequestParam(value = EXPAND_GROUPS_REST_PARAM, required = false) String expandGroupsString
            ) throws Exception{
        
        
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        Boolean expandGroups = Boolean.parseBoolean(expandGroupsString);
        
        return groupsService.getGroupMembers(groupId, expandGroups, page, size);
    }

    //rest/groups
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups", method = RequestMethod.POST)
    @ApiOperation(value = "Create a new 'Custom' group.", notes="Group name has to be unique for a client-project. "
            + "Provide a list of participants or groups to be members of the specified group. "
            + "By default the group is set to 'ACTIVE' status. This POST can be used to create a new group and "
            + "add members to it.<br><br>Group 'type' values are - 'CUSTOM','PERSONAL'<br><br>"
            + "Group 'visibility' values are 'PUBLIC', 'ADMIN'")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully created the new group.", 
        response = GroupEnrollmentFullDTO.class)})
    @Permission("PUBLIC")
    public GroupEnrollmentFullDTO createGroup( 
            @ApiParam(value = "Body containing the new Group you want to add") 
                @RequestBody GroupEnrollmentWithStubDTO groupEnrollmentStub
            ) throws Exception{    
        
        return groupsService.createGroup(groupEnrollmentStub);
    }

    //rest/groups/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups/{groupId}", method = RequestMethod.PUT)
    @ApiOperation(value="Update the details of a specified group.", 
        notes = "Only Custom & Personal Groups can be updated.<br><br>'visibility' values are 'PUBLIC', 'ADMIN'")
    @Permission("PUBLIC")
    public Map<String, Object> updateGroup(
            @ApiParam(value = "Id of the group you want to get the details of") 
                @PathVariable(GROUP_ID_REST_PARAM) String groupIdString, @RequestBody GroupDTO groupDto
            ) throws Exception{    
        
        
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return groupsService.updateGroup(groupId, groupDto);
    }

    //rest/groups/~/members
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups/{groupId}/members", method = RequestMethod.POST)
    @ApiOperation(value = "Update a new 'Custom' group.", 
        notes = "Provide a list of participants or groups to be added to the members of the specified group.<br><br>"
                + "Group 'type' values are - 'CUSTOM'<br><br>Group 'visibility' values are 'PUBLIC', 'ADMIN'")
    @Permission("PUBLIC")
    public List<EntityDTO> addMembers(
            @ApiParam(value = "Id of the group you want to get the details of") 
                @PathVariable(GROUP_ID_REST_PARAM) String groupIdString, @RequestBody List<GroupMemberStubDTO> members)
                throws Throwable {
        
    
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return groupsService.addMembers(groupId, members);
    }

    //rest/groups/~/members/bulk-delete
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups/{groupId}/members/bulk-delete", method = RequestMethod.POST)
    @ApiOperation(value = "Delete users/groups from a 'Custom' group.", 
        notes ="Provide a list of participants or groups to be removed from the members-"
                + "list of the specified group.<br><br>Group 'type' values are - 'CUSTOM',<br><br"
                + ">Group 'visibility' values are 'PUBLIC', 'ADMIN'" )
    @Permission("PUBLIC")
    public void deleteMembers(
            @ApiParam(value = "Id of the group you want to get the details of") 
                @PathVariable(GROUP_ID_REST_PARAM) String groupIdString,
            @ApiParam(value = "Body containing the group ID and members you wish to remove fronm that group") 
                @RequestBody List<GroupMemberStubDTO> members) throws Throwable {    
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        groupsService.deleteMembers(groupId, members);
    }
}
