package com.maritz.culturenext.profile.rest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EmailPasswordWrapperDTO;
import com.maritz.culturenext.profile.dto.LocaleDTO;
import com.maritz.culturenext.profile.dto.PaxMiscDTO;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.dto.aggregate.PaxMiscListDTO;
import com.maritz.culturenext.profile.dto.aggregate.ProfileDemographics;
import com.maritz.culturenext.profile.services.DetailProfileService;
import com.maritz.culturenext.constants.ProjectConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(ProjectConstants.PARTICIPANTS_URI_BASE)
@Api(value="/profile", description="All end points dealing with user profiles")
public class DetailProfileRestService {
    
    private static final String DIRECT_REPORTS_PATH = "{" + PAX_ID_REST_PARAM + "}/direct-reports";

    @Inject private DetailProfileService detailProfileService;
    @Inject private Security security;


    //rest/participants/~/profile
    @RequestMapping(value = "{paxId}/profile", method = RequestMethod.GET)
    @ApiOperation(value = "Returns profile details for the logged in user", 
        notes ="this includes demographics, and other miscellaneous data like privacy-settings, preferences, hire_date"
                +"<br><br>as of Sprint 5, pax and managerPax are now using the latest participant model")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the detail Profile for the given user.", response = DetailProfileInfo.class)})
    @Permission("PUBLIC")
    public DetailProfileInfo getDetailProfileByPaxId(
            @ApiParam(value="Participant ID of the logged in user") 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return detailProfileService.getDetailProfileInfoByPaxId(paxId);        
    }

    //rest/participants/~/profile/demographics
    @PreAuthorize("!@security.isImpersonated()")
    @ApiOperation(value="Retrieves the demographics of the given paxId.")
    @RequestMapping(value = "{paxId}/profile/demographics", method = RequestMethod.GET)
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the Profile demographics for the given user.", 
        response = ProfileDemographics.class)})
    @Permission("PUBLIC")
    public ProfileDemographics getProfileDemographicsByPaxId(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        
        return detailProfileService.getProfileDemographicsByPaxId(paxId);
    }

    //rest/participants/~/profile/demographics/email
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/demographics/email", method = RequestMethod.GET)
    @ApiOperation(value="Retrieves a users email address info.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully retrieved the email for the given user.",
        response = EmailDTO.class)})
    @Permission("PUBLIC")
    public List<EmailDTO> getEmailsByPaxId(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        
        return detailProfileService.getEmailsByPaxId(paxId);
       
    }

    //rest/participants/~/profile/demographics/email
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")   
    @RequestMapping(value = "{paxId}/profile/demographics/email", method = RequestMethod.PUT)
    @ApiOperation(value="Allows a user to update their email address.", 
        notes="For non SSO - It needs to be secured by passing in the current password in addition to email(s)."
                + " Email is allowed to be added w/o password during first-time login")   
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully updated the email for the given user.", 
        response = EmailDTO.class)})
    @Permission("PUBLIC")
    public List<EmailDTO> updateEmails(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent that has the updated information.")
                @RequestBody @Valid EmailPasswordWrapperDTO emailPasswordWrapperDTO
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.updateEmails(paxId, emailPasswordWrapperDTO);
    }
    
    //rest/participants/~/profile/demographics/email
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")   
    @RequestMapping(value = "{paxId}/profile/demographics/email", method = RequestMethod.POST)
    @ApiOperation(value="Allows a user to create a new email address.", 
        notes="Email is allowed to be added w/o password during first-time login. "
                + "Service will check for first-time login status")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully inserted the new email for the given user.", response = EmailDTO.class)})
    @Permission("PUBLIC")
    public List<EmailDTO> insertEmail(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent that has the new email information.") 
                @RequestBody @Valid EmailPasswordWrapperDTO emailPasswordWrapperDTO
            ) throws Throwable { 
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.insertEmail(paxId, emailPasswordWrapperDTO);
    }

    //rest/participants/~/profile/privacy
    @RequestMapping(value = "{paxId}/profile/privacy", method = RequestMethod.GET)
    @ApiOperation(value="Returns the privacy settings for the given paxId")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the privacy settings for the given user.", response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public PaxMiscListDTO getProfilePrivacyByPaxId(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.getProfilePrivacyByPaxId(paxId);                
    }
    
    //rest/participants/~/profile/privacy
    @RequestMapping(value = "{paxId}/profile/privacy", method = RequestMethod.PATCH)
    @ApiOperation(value = "Allows a user to update parts of their privacy settings")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully updated and returned the new privacy settings for the given user.", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public List<PaxMiscDTO> patchProfilePrivacy (
            @ApiParam(value="Participant ID of the logged in user") 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent with the updated privacy settings") 
                @RequestBody List<PaxMiscDTO> paxMiscDTOList) {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.updateProfilePrivacy(paxId, paxMiscDTOList);
    }

    //rest/participants/~/profile/privacy
    @RequestMapping(value = "{paxId}/profile/privacy", method = RequestMethod.PUT)
    @ApiOperation(value ="Allows a user to update their profile information and privacy settings")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully updated and returned the new privacy settings for the given user.", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public List<PaxMiscDTO> updateProfilePrivacy(
            @ApiParam(value="Participant ID of the logged in user") 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent with the updated privacy settings") 
                @RequestBody List<PaxMiscDTO> paxMiscDTOList) {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.updateProfilePrivacy(paxId, paxMiscDTOList);
    }

    //rest/participants/~/profile/privacy
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/privacy", method = RequestMethod.POST)
    @ApiOperation(value ="Allows a user to insert their profile information and privacy settings")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the new privacy settings for the given user.", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public List<PaxMiscDTO> insertProfilePrivacy(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent with the new privcay settings") @RequestBody List<PaxMiscDTO> paxMiscDTOList
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.insertProfilePrivacy(paxId, paxMiscDTOList);
    }

    //rest/participants/~/profile/recognition_preferences
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/recognition_preferences", method = RequestMethod.GET)
    @ApiOperation(value ="returns a users profile information and recognition preferences")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the profile information and recognition preferences for the given user.", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public PaxMiscListDTO getProfilePreferencesByPaxId(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.getProfilePreferencesByPaxId(paxId);            
    }

    //rest/participants/~/profile/recognition_preferences
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/recognition_preferences", method = RequestMethod.PUT)
    @ApiOperation(value ="Allows a user to update their profile information and recognition preferences")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the updated profile information and "
                + "recognition preferences for the given user.", response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public List<PaxMiscDTO> updateProfilePreferences(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent with the updated information and recognition preferences") 
                @RequestBody List<PaxMiscDTO> paxMiscDTOList
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.updateProfilePreferences(paxId, paxMiscDTOList);
    }

    //rest/participants/~/profile/recognition_preferences
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/recognition_preferences", method = RequestMethod.POST)
    @ApiOperation(value ="Allows a user to update their profile information and recognition preferences")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the new profile information and recognition preferences for the given user.", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public List<PaxMiscDTO> insertProfilePreferences(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="Body sent with the new information and recognition preferences")
                @RequestBody List<PaxMiscDTO> paxMiscDTOList
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        return detailProfileService.insertProfilePreferences(paxId, paxMiscDTOList);
    }
    
    //rest/participants/~/profile/pax
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/profile/pax", method = RequestMethod.PUT)
    @ApiOperation(value ="Allows a user to update their language and locale profile information")
    @ApiResponses(value = {
            @ApiResponse(code = 200, 
                    message = "successfully returned the updated language and locale profile information", 
                    response = LocaleDTO.class),
            @ApiResponse(code = 400, message = "language code or preferred locale are invalid"),
            @ApiResponse(code = 403, message = "pax id is not the logged in user")

    })
    @Permission("PUBLIC")
    public LocaleDTO updatePaxLanguageAndLocale(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="Body sent with the updated language and locale profile information") 
                @RequestBody LocaleDTO localeDTO
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);

        return detailProfileService.updatePaxLanguageAndLocale(paxId, localeDTO);
    }
    
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Collection<ErrorMessage>> handle(ErrorMessageException e) {      
        return new ResponseEntity<>(e.getErrorMessages(), HttpStatus.UNAUTHORIZED);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<List<ErrorMessage>> handleEmailConstraintViolationError(MethodArgumentNotValidException manve) {
        
        List<ErrorMessage> errorList = new ArrayList<>();
        errorList.add(ProjectConstants.INVALID_EMAIL_FORMAT_ERROR);
        return new ResponseEntity<>(errorList, HttpStatus.BAD_REQUEST);
    }
    
    //rest/participants/~/direct-reports
    @RequestMapping(value = DIRECT_REPORTS_PATH, method = RequestMethod.GET)
    @ApiOperation(value ="Return a list of users who directly report to a given manager")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the list of users for the given manager", 
        response = PaxMiscListDTO.class)})
    @Permission("PUBLIC")
    public PaginatedResponseObject<EmployeeDTO> getDirectReportsByManager(
            @ApiParam(value="Participant ID of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="Indicate if it is necessary exclude the inactive participants.",required = false)
            @RequestParam(value = EXCLUDE_INACTIVE_PARAM, required = false) boolean excludeInactive,
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
        @ApiParam(value="The number of records per page.",required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(PAX_ID_REST_PARAM, paxIdString);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(ProjectConstants.PARTICIPANTS_URI_BASE + "/" + DIRECT_REPORTS_PATH).setPathParamMap(pathParamMap)
                    .setPageNumber(pageNumber).setPageSize(pageSize);
        
        return detailProfileService.getManagerDirectReports(requestDetails, paxId, excludeInactive, pageNumber, pageSize);
    }
}
