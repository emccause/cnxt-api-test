package com.maritz.culturenext.profile.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.PaxSearchService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value="/participants", description="All end points dealing with participants")
public class PaxSearchRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private PaxSearchService paxSearchService;

    //rest/participants
    @RequestMapping(value = "participants", method = RequestMethod.GET, headers = CURRENT_VERSION_HEADER)
    @ApiOperation(value="Fetch participants with the specified search criteria query-param.", 
        notes ="Also narrow down the search further by the filter cirteria - groups specified and "
                + "the statuses specified. The parameters for page and size are optional. If those fields are "
                + "left blank a default of page 1 and page-size 50 will be used. It will return the first "
                + "50 participants and alphabetic sort will be used.<br><br>Filter Criteria for groups & "
                + "status will support the 'AND' & the 'OR' logic. i.e for Multiple criteria within "
                + "category use OR criteria; for Multiple criteria between categories use AND criteria.<br><br>"
                + "ex. Return users where users belong to groups: (City-Chicago OR City-New York) AND "
                + "(Cost-Center-1 OR Cost-Center-2 OR Cost-Center-3) AND Status = (Active OR Locked) <br><br>"
                + "the API will determine the enrollment field(GroupConfigId) for the groupIds and "
                + "group them for AND & OR logic")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the list of participants.", response = EmployeeDTO.class)})
    @Permission("PUBLIC")
    public List<EmployeeDTO> getSearchResults(

            @ApiParam(value="Basic string of what you are searching for") 
                @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchStr, 
            @ApiParam(value="Id's of the groups you want to search for ex 2,3,4,8,9",required = false) 
                @RequestParam(value = GROUP_IDS_REST_PARAM, required = false) String groupIds, 
            @ApiParam(value="Status of the group ex. ACTIVE,LOCKED",required = false) 
                @RequestParam(value = STATUS_REST_PARAM, required = false) String status, 
                // TODO Pagination Techdebt: MP-7511 & MP-7528
            @ApiParam(value="Number of pages you want the results to return",required = false)
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @ApiParam(value="Number of items per page",required = false)
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @ApiParam(value="Program Id",required = false) 
                @RequestParam(value = PROGRAM_ID_REST_PARAM, required = false) Integer programId,
            @ApiParam(value="If excludeSelf is true the logged in user is filtered out", required = false) 
                @RequestParam(value = EXCLUDE_SELF_REST_PARAM, required = false) Boolean excludeSelf,
            @ApiParam(value = "Id of the budget id that you want the details of.") 			        
            	@RequestParam(value = BUDGET_ID_REST_PARAM,required = false) Integer budgetId           
        
            ) throws Exception{
        
        return paxSearchService.searchPax(searchStr, groupIds, status, programId, excludeSelf,page, size, budgetId);
    }
}
