package com.maritz.culturenext.profile.rest;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;
import com.maritz.culturenext.profile.services.EnrollmentGroupsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("enrollment-config")
@Api(value="/profile", description="All end points dealing with user profiles")
public class EnrollmentGroupsRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private EnrollmentGroupsService enrollmentGroupsService;

    //rest/enrollment-config/groups
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups", method = RequestMethod.GET)
    @ApiOperation(value="Returns the list of all enrollment fields available for grouping for a client/project setup.",
        notes="Also indicates the fields that have been chosen by admin for creating groups based on enrollment data"
                + "(<br><br>groupCreation indicates this).<br><br>groupConfigStatus values - "
                + "'ACTIVE', 'INACTIVE'<br><br>visibility values - 'PUBLIC', 'ADMIN'<br><br>")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the list of enrollment fields.", 
        response = EnrollmentGroupDTO.class)})
    @Permission("PUBLIC")
    public List<EnrollmentGroupDTO> getEnrollmentGroups() throws Throwable{
        
        
        return enrollmentGroupsService.getEnrollmentGroups();        
    }

    //rest/enrollment-config/groups
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "groups", method = RequestMethod.PUT)
    @ApiOperation(value="Saves the enrollment groups' configuration/setup and "
            + "returns an error if one is already in progress. ", 
            notes="Schedulles/triggers the groups creation for the specified fields by groupingStatus.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully returned the list of enrollment fields.", 
        response = EnrollmentGroupDTO.class)})
    @Permission("PUBLIC")
    public List<EnrollmentGroupDTO> updateEnrollmentGroups(
            @ApiParam(value="Body of the enrollment group configuration/Setup") 
            @RequestBody List<EnrollmentGroupDTO> groupList
            ) throws Throwable {
        return enrollmentGroupsService.updateEnrollmentGroups(groupList);
    }
    
}
