package com.maritz.culturenext.profile.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentFullDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentWithStubDTO;
import com.maritz.culturenext.profile.services.PersonalGroupsService;

@RestController
@RequestMapping("participants")
@Api(value="/personal-groups", description="All end points dealing with personal groups")
public class PersonalGroupsRestService {

    @Inject private PersonalGroupsService personalGroupsService;
    @Inject private Security security;

    //rest/participants/~/personal-groups
    @RequestMapping(value = "{paxId}/personal-groups", method = RequestMethod.GET)
    @ApiOperation(value = "gets the logged in user's personal groups")
    @Permission("PUBLIC")
    public List<Map<String, Object>> getPersonalGroups(
                @ApiParam(value = "PaxId of the logged in user" )
                    @PathVariable(PAX_ID_REST_PARAM) String paxIdString
    ) throws Exception {

        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(RequestMethod.GET.name(), "rest/participants/{paxId}/personal-groups", null);
        }
        
        return personalGroupsService.getPersonalGroups(paxId);
    }
    
    //rest/participants/personal-groups/~/delete
    @RequestMapping(value = "/personal-groups/{groupId}/delete", method = RequestMethod.POST)
    @ApiOperation(value = "Deletes the selected personal group")
    @Permission("PUBLIC")
    public void deletePersonalGroups(
                @ApiParam(value = "Personal Group Id to be deleted." )
                    @PathVariable(GROUP_ID_REST_PARAM) String groupIdString
    ) throws Throwable {
        Long groupId = security.getId(groupIdString, null);
        if (groupId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        personalGroupsService.deletePersonalGroup(groupId);
    }
    
}
