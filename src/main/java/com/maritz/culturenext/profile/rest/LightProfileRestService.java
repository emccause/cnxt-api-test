package com.maritz.culturenext.profile.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.aggregate.LightweightProfileInfoDTO;
import com.maritz.culturenext.profile.services.LightProfileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("participants")
@Api(value="/profile", description="All end points dealing with user profiles")
public class LightProfileRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private LightProfileService lightProfileService;
    @Inject private Security security;

    //rest/participants/~/profile/basic
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = "{paxId}/profile/basic", method = RequestMethod.GET)
    @ApiOperation(value = "Returns basic profile information for the logged in user.", 
        notes ="values for 'status' are ACTIVE, INACTIVE, LOCKED, NEW(for new/first-time-login user), "
                + "SUSPENDED, RESTRICTED.<br><br>Status will move from 'NEW' to 'ACTIVE' once a user logs in and "
                + "changes his password and provides email(for a non-sso client). Status will move from 'NEW' to "
                + "'ACTIVE' once a user logs in and provides email(for a sso client).<br><br>"
                + "Added 'profileIncomplete' flag - this will be 'true' if email & password are not yet "
                + "available for the user for a non-sso client. This will be 'true' if email is not yet "
                + "available for the user for a sso client.")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the light user profile for the logged in user.", 
        response = LightweightProfileInfoDTO.class)})
    @Permission("PUBLIC")
    public LightweightProfileInfoDTO getLightProfile(
            @ApiParam(value = "PaxId of the logged in user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
    ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return lightProfileService.getLightProfileInfo();
    }
}
