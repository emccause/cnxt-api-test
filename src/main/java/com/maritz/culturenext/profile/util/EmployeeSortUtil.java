package com.maritz.culturenext.profile.util;

import java.util.Comparator;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class EmployeeSortUtil {
    /**
     * Provides a comparator for sorting a collection of EmployeeDTOs
     * by First name and Last name
     */
    public static Comparator<EmployeeDTO> firstAndLastNameComparator
            = new Comparator<EmployeeDTO>() {
        @Override
        public int compare(EmployeeDTO e1, EmployeeDTO e2) {
            int firstNameOrder = nullSafeStringCompareTo(e1.getFirstName(), e2.getFirstName()),
                    lastNameOrder = nullSafeStringCompareTo(e1.getLastName(), e2.getLastName());

            return firstNameOrder != 0 ? firstNameOrder : lastNameOrder;
        }
    };

    /**
     * Null safe compareTo for string values
     * @param s1 - First string value to compare
     * @param s2 - Second string value to compare
     * @return int
     */
    private static int nullSafeStringCompareTo(String s1, String s2) {
        if (s1 == null && s2 == null) {
            return 0;
        } else if (s2 == null) {
            return 1;
        } else if (s1 == null) {
            return -1;
        } else {
            return s1.compareToIgnoreCase(s2);
        }
    }
}
