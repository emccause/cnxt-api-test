package com.maritz.culturenext.profile.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class GroupConstants {
    
    //GroupDTO field mapping names
    public static final String GROUP_ID = "groupId";
    public static final String GROUP_CONFIG_ID = "groupConfigId";
    public static final String FIELD = "field";
    public static final String FIELD_DISPLAY_NAME = "fieldDisplayName";
    public static final String GROUP_NAME = "groupName";
    public static final String GROUP_TYPE = "groupType";
    public static final String GROUP_PAX_ID = "groupPaxId";
    public static final String VISIBILITY = "visibility";
    public static final String STATUS_TYPE_CODE = "statusTypeCode";
    public static final String CREATE_DATE = "createDate";
    public static final String PAX_COUNT = "paxCount";
    public static final String GROUP_COUNT = "groupCount";
    public static final String TOTAL_PAX_COUNT = "totalPaxCount";
    public static final String TOTAL_RESULTS = "totalResults";
    public static final String MEMBERS = "members";
    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String REQUEST = "request";
    
    //Error Messages
    public static final String ERROR_BAD_GROUP_CONFIG_ID = "BAD_GROUP_CONFIG_ID";
    public static final String ERROR_NO_VALID_MEMBERS_CODE = "NO_VALID_MEMBERS";
    public static final String ERROR_NO_VALID_MEMBERS_MESSAGE = "At least one valid member must be provided for group creation";
    public static final String ERROR_CANNOT_EDIT_GROUP_CODE = "CANNOT_EDIT_GROUP";
    public static final String ERROR_CANNOT_EDIT_GROUP_MESSAGE = "Can only edit custom groups or personal groups that you own";
    public static final String ERROR_DUPLICATE_GROUP_NAME_CODE = "DUPLICATE_GROUP_NAME";
    public static final String ERROR_DUPLICATE_GROUP_NAME_MESSAGE = "The group name already exists";
    public static final String ERROR_INVALID_GROUP_STATUS_CODE = "INVALID_GROUP_STATUS";
    public static final String ERROR_INVALID_GROUP_STATUS_MESSAGE = "Valid group statuses are ACTIVE, INACTIVE";
    public static final String ERROR_GROUP_VISIBILITY_NULL_CODE = "GROUP_VISIBILITY_NULL";
    public static final String ERROR_GROUP_VISIBILITY_NULL_MESSAGE = "Group Visibility not provided";
    public static final String ERROR_GROUP_VISIBILITY_INVALID_CODE = "GROUP_VISIBILITY_INVALID";
    public static final String ERROR_GROUP_VISIBILITY_INVALID_MESSAGE = "Group Visibility is not valid";
    public static final String ERROR_GROUP_TYPE_NULL_CODE = "GROUP_TYPE_NULL";
    public static final String ERROR_GROUP_TYPE_NULL_MESSAGE = "Group type not provided";
    public static final String ERROR_GROUP_TYPE_INVALID_CODE = "INVALID_GROUP_TYPE";
    public static final String ERROR_GROUP_TYPE_INVALID_MESSAGE = "You can only create/edit/delete PERSONAL groups.";
    public static final String ERROR_INVALID_GROUP_OWNER_CODE = "INVALID_GROUP_OWNER";
    public static final String ERROR_INVALID_GROUP_OWNER_MESSAGE = "You can only add CUSTOM, ENROLLMENT and PERSONAL groups that you own.";
    public static final String ERROR_MEMBERS_LIST_EMPTY_CODE = "MEMBERS_LIST_EMPTY";
    public static final String ERROR_MEMBERS_LIST_EMPTY_MESSAGE = "Group members must be provided";
    public static final String ERROR_MEMBERS_LIST_TOO_LARGE_CODE = "MEMBERS_LIST_TOO_LARGE";
    public static final String ERROR_MEMBERS_LIST_TOO_LARGE_MESSAGE = "Max 50 members for initial group creation";
    public static final String ERROR_GROUP_NAME_NULL_CODE = "GROUP_NAME_NULL";
    public static final String ERROR_GROUP_NAME_NULL_MESSAGE = "Group Name not provided";
    public static final String ERROR_GROUP_NAME_TOO_LONG_CODE = "GROUP_NAME_TOO_LONG";
    public static final String ERROR_GROUP_NAME_TOO_LONG_MESSAGE = "Group name too long, max=100";
    public static final String ERROR_BAD_GROUP_CONFIG_ID_MESSAGE = "Invalid Group Config Id";
    public static final String ERROR_CANNOT_GET_GROUP_CODE = "CANNOT_GET_GROUP";
    public static final String ERROR_CANNOT_GET_GROUP_MESSAGE = "Can only get custom groups or personal groups that you own";
    public static final String ERROR_CANNOT_CREATE_GROUP_CODE = "CANNOT_CREATE_GROUP";
    public static final String ERROR_CANNOT_CREATE_GROUP_MESSAGE = "Can only create personal groups for your user";
    public static final String ERROR_CANNOT_UPDATE_PERSONAL_GROUP_CODE = "CANNOT_EDIT_PERSONAL_GROUP";
    public static final String ERROR_CANNOT_UPDATE_PERSONAL_GROUP_MESSAGE = "Can only edit personal groups that you create.";
    
    //Error Message Constants
    public static final ErrorMessage NO_VALID_MEMBERS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NO_VALID_MEMBERS_CODE)
        .setField(MEMBERS)
        .setMessage(ERROR_NO_VALID_MEMBERS_MESSAGE);
    
    public static final ErrorMessage CANNOT_EDIT_GROUP_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CANNOT_EDIT_GROUP_CODE)
        .setField(ProjectConstants.GROUP)
        .setMessage(ERROR_CANNOT_EDIT_GROUP_MESSAGE);
    
    public static final ErrorMessage DUPLICATE_GROUP_NAME_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DUPLICATE_GROUP_NAME_CODE)
        .setField(GROUP_NAME)
        .setMessage(ERROR_DUPLICATE_GROUP_NAME_MESSAGE);
    
    public static final ErrorMessage INVALID_GROUP_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_GROUP_STATUS_CODE)
        .setField(STATUS)
        .setMessage(ERROR_INVALID_GROUP_STATUS_MESSAGE);
    
    public static final ErrorMessage GROUP_VISIBILITY_NULL_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_VISIBILITY_NULL_CODE)
        .setField(VISIBILITY)
        .setMessage(ERROR_GROUP_VISIBILITY_NULL_MESSAGE);
    
    public static final ErrorMessage GROUP_VISIBILITY_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_VISIBILITY_INVALID_CODE)
        .setField(VISIBILITY)
        .setMessage(ERROR_GROUP_VISIBILITY_INVALID_MESSAGE);

    public static final ErrorMessage GROUP_TYPE_NULL_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_TYPE_NULL_CODE)
        .setField(TYPE)
        .setMessage(ERROR_GROUP_TYPE_NULL_MESSAGE);
    
    public static final ErrorMessage GROUP_TYPE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_TYPE_INVALID_CODE)
        .setField(TYPE)
        .setMessage(ERROR_GROUP_TYPE_INVALID_MESSAGE);

    public static final ErrorMessage INVALID_GROUP_OWNER_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_GROUP_OWNER_CODE)
        .setField(TYPE)
        .setMessage(ERROR_INVALID_GROUP_OWNER_MESSAGE);

    public static final ErrorMessage MEMBERS_LIST_EMPTY_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MEMBERS_LIST_EMPTY_CODE)
        .setField(MEMBERS)
        .setMessage(ERROR_MEMBERS_LIST_EMPTY_MESSAGE);
    
    public static final ErrorMessage MEMBERS_LIST_TOO_LARGE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MEMBERS_LIST_TOO_LARGE_CODE)
        .setField(MEMBERS)
        .setMessage(ERROR_MEMBERS_LIST_TOO_LARGE_MESSAGE);
    
    public static final ErrorMessage GROUP_NAME_NULL_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_NAME_NULL_CODE)
        .setField(GROUP_NAME)
        .setMessage(ERROR_GROUP_NAME_NULL_MESSAGE);
    
    public static final ErrorMessage GROUP_NAME_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_NAME_TOO_LONG_CODE)
        .setField(GROUP_NAME)
        .setMessage(ERROR_GROUP_NAME_TOO_LONG_MESSAGE);
    
    public static final ErrorMessage BAD_GROUP_CONFIG_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_BAD_GROUP_CONFIG_ID)
        .setField(REQUEST)
        .setMessage(ERROR_BAD_GROUP_CONFIG_ID_MESSAGE);
    
    public static final ErrorMessage CANNOT_GET_GROUP_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CANNOT_GET_GROUP_CODE)
        .setField(ProjectConstants.GROUP)
        .setMessage(ERROR_CANNOT_GET_GROUP_MESSAGE);
    
    public static final ErrorMessage CANNOT_CREATE_GROUP_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CANNOT_CREATE_GROUP_CODE)
        .setField(ProjectConstants.GROUP)
        .setMessage(ERROR_CANNOT_CREATE_GROUP_MESSAGE);

    public static final ErrorMessage CANNOT_EDIT_PERSONAL_GROUP_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CANNOT_UPDATE_PERSONAL_GROUP_CODE)
        .setField(ProjectConstants.GROUP)
        .setMessage(ERROR_CANNOT_UPDATE_PERSONAL_GROUP_MESSAGE);
}
