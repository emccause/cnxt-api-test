package com.maritz.culturenext.profile.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.EmailType;

public class ProfileConstants {
    
    public static final Integer COMBINED_SEARCH_MINIMUM_STRING_LENGTH = 2;
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String EMAIL_ID = "emailId";
    public static final String EMAIL_ID_CAPS = "emailID"; 
    public static final String EDITABLE = "editable";
    public static final String PROCESSING_STATUS = "processing status";
    public static final String VISIBILITY ="visibility";
    public static final String SEARCH_STRING_SQL_PARAM = "SEARCH_STRING";
    public static final String SEARCH_STRING_RAW_SQL_PARAM = "SEARCH_STRING_RAW";
    public static final String SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM = "SEARCH_STRING_EMAIL_ADDRESS_RAW";
    
    public static final String ROLE_PREFIX = "ROLE_";
    public static final String PERM_PREFIX = "PERM_";
    
    //Error Messages
    public static final String INVALID_NOT_FIRST_TIME = "INVALID_NOT_FIRST_TIME";
    public static final String INVALID_EMAIL = "INVALID_EMAIL";
    public static final String INVALID_NOT_FIRST_TIME_MSG = "Not a First time login for user";
    public static final String INVALID_DATA = "INVALID_DATA";
    public static final String INVALID_DATA_MSG = "Invalid data passed in";
    public static final String INVALID_PARAMETER = "INVALID_PARAMETER";
    public static final String INVALID_PARAMETER_MSG = "Invalid parameter passed in";
    public static final String INVALID_EMAIL_ID = "INVALID_EMAIL_ID";
    public static final String INVALID_EMAIL_ID_MSG = "EmailID does not exist";
    public static final String MISSING_PAX_ID = "MISSING_PAX_ID";
    public static final String MISSING_PAX_ID_MSG = "PaxId must be provided in request";
    public static final String MISSING_EMAIL_ID = "MISSING_EMAIL_ID";
    public static final String MISSING_EMAIL_ID_MSG = "EmailId must be provided";
    public static final String MISSING_EMAIL_TYPE_CODE = "MISSING_EMAIL_TYPE_CODE";
    public static final String MISSING_EMAIL_TYPE_CODE_MSG = "EmailTypeCode must be provided";
    public static final String INVALID_EMAIL_TYPE_CODE = "INVALID_EMAIL_TYPE_CODE";
    public static final String INVALID_EMAIL_TYPE_CODE_MSG = "EmailTypeCode is invalid";
    public static final String INVALID_PREFERRED_VALUE = "INVALID_PREFERRED_VALUE";
    public static final String INVALID_PREFERRED_VALUE_MSG = "Preferred value is invalid";
    public static final String INCORRECT_EMAIL_ID = "INCORRECT_EMAIL_ID";
    public static final String INCORRECT_EMAIL_ID_MSG = "Specified email does not match with existing email information";
    public static final String INVALID_PAX_ID = "INVALID_PAX_ID";
    public static final String INVALID_PAX_ID_MSG = "PaxID in request does not match authentication";
    public static final String DUPLICATE_VFNAME = "DUPLICATE_VFNAME";
    public static final String DUPLICATE_VFNAME_MSG = "VFName already exists for user";
    public static final String DUPLICATE_EMAIL_TYPE = "DUPLICATE_EMAIL_TYPE";
    public static final String DUPLICATE_EMAIL_TYPE_MSG = "User already has Home email address";
    public static final String MISSING_LANGUAGE_CODE = "MISSING_LANGUAGE_CODE";
    public static final String MISSING_LANGUAGE_CODE_MSG = "Language code must be provided";
    public static final String INVALID_LANGUAGE_CODE = "INVALID_LANGUAGE_CODE";
    public static final String INVALID_LANGUAGE_CODE_MSG = "Language code is invalid";
    public static final String MALFORMED_LANGUAGE_CODE = "MALFORMED_LANGUAGE_CODE";
    public static final String MALFORMED_LANGUAGE_CODE_MSG = "Language Code is malformed";
    public static final String INACTIVE_LANGUAGE_CODE = "INACTIVE_LANGUAGE_CODE";
    public static final String INACTIVE_LANGUAGE_CODE_MSG = "Language Code is inactive";
    public static final String SEARCH_ERROR_CODE = "INTERNAL_ERROR";
    public static final String SEARCH_ERROR_FIELD = "search";
    public static final String SEARCH_ERROR_MSG = "Internal error occured.  Please try again later.";
    public static final String SEARCH_STRING_TOO_SHORT_CODE = "SEARCH_STRING_TOO_SHORT";
    public static final String SEARCH_STRING_TOO_SHORT_FIELD = "search";
    public static final String SEARCH_STRING_TOO_SHORT_MSG = "Must provide at least 2 characters.";
    public static final String MISSING_PARAMETER= "MISSING_PARAMETER";
    public static final String MISSING_PARAMETER_EDITABLE_MSG = "Parameter must be provided";

    public static final String INVALID_EMAIL_FORMAT_MSG = "Invalid email format";
    private static final String ERROR_NULL_VISIBILITY = "NULL_VISIBILITY";
    private static final String ERROR_NULL_VISIBILITY_MSG = "visibility is required when groupCreation is ACTIVE";
    private static final String ERROR_UPDATE_IN_PROGRESS = "GROUP_CREATION_IN_PROGRESS";
    private static final String ERROR_UPDATE_IN_PROGRESS_MSG = 
            "Automated Enrollment group creation is already in progress";
    public static final String INVALID_GROUP = "INVALID_GROUP";
    public static final String INVALID_GROUP_MSG = "Group is invalid";
    
    public static final String ERROR_EMAIL_MUST_BE_SUPPLIED = "MUST_BE_SUPPLIED";
    public static final String ERROR_EMAIL_MUST_BE_SUPPLIED_MSG = "Email must be supplied";
    public static final String ERROR_INVALID_EMAIL = "INVALID_EMAIL";
    public static final String ERROR_INVALID_EMAIL_MSG = "Invalid email";
    
    public static final List<String> VALID_EMAIL_TYPE_CODES =
            Arrays.asList(EmailType.BUSINESS.getCode(), EmailType.HOME.getCode());

    public static final List<String> VALID_PREFERRED_VALUES =
            Arrays.asList(ProjectConstants.YES_CHAR, ProjectConstants.NO_CHAR);
    

    //ErrorMessage Constants
    public static final ErrorMessage INVALID_NOT_FIRST_TIME_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_NOT_FIRST_TIME)
        .setField(PASSWORD)
        .setMessage(ProfileConstants.INVALID_NOT_FIRST_TIME_MSG);
    
    public static final ErrorMessage MISSING_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.MISSING_PAX_ID)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ProfileConstants.MISSING_PAX_ID_MSG);
    
    public static final ErrorMessage INVALID_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_PAX_ID)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ProfileConstants.INVALID_PAX_ID_MSG);
    
    public static final ErrorMessage MISSING_EMAIL_ID_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.MISSING_EMAIL_ID)
        .setField(EMAIL_ID)
        .setMessage(ProfileConstants.MISSING_EMAIL_ID_MSG);
    
    public static final ErrorMessage MISSING_EMAIL_TYPE_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.MISSING_EMAIL_TYPE_CODE)
        .setField(ProjectConstants.EMAIL_TYPE_CODE)
        .setMessage(ProfileConstants.MISSING_EMAIL_TYPE_CODE_MSG);
    
    public static final ErrorMessage INVALID_EMAIL_TYPE_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_EMAIL_TYPE_CODE)
        .setField(ProjectConstants.EMAIL_TYPE_CODE)
        .setMessage(ProfileConstants.INVALID_EMAIL_TYPE_CODE_MSG);
    
    public static final ErrorMessage INVALID_PREFERRED_VALUE_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_PREFERRED_VALUE)
        .setField(ProjectConstants.PREFERRED)
        .setMessage(ProfileConstants.INVALID_PREFERRED_VALUE_MSG);
    
    public static final ErrorMessage MISSING_PARAMETER_EDITABLE_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.MISSING_PARAMETER)
        .setField(EDITABLE)
        .setMessage(ProfileConstants.MISSING_PARAMETER_EDITABLE_MSG);
    
    public static final ErrorMessage INCORRECT_EMAIL_ID_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INCORRECT_EMAIL_ID)
        .setField(EMAIL_ID)
        .setMessage(ProfileConstants.INCORRECT_EMAIL_ID_MSG);
    
    public static final ErrorMessage INVALID_EMAIL_ID_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_EMAIL_ID)
        .setField(EMAIL_ID_CAPS)
        .setMessage(ProfileConstants.INVALID_EMAIL_ID_MSG);
    
    public static final ErrorMessage DUPLICATE_EMAIL_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.DUPLICATE_EMAIL_TYPE)
        .setField(ProjectConstants.EMAIL_ADDRESS)
        .setMessage(ProfileConstants.DUPLICATE_EMAIL_TYPE_MSG);
    
    public static final ErrorMessage INVALID_PARAMETER_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_PARAMETER)
        .setField(ProjectConstants.VF_NAME)
        .setMessage(ProfileConstants.INVALID_PARAMETER_MSG);
    
    public static final ErrorMessage INVALID_DATA_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.INVALID_DATA)
        .setField(ProjectConstants.MISC_DATA)
        .setMessage(ProfileConstants.INVALID_DATA_MSG);
    
    public static final ErrorMessage DUPLICATE_VFNAME_MESSAGE = new ErrorMessage()
        .setCode(ProfileConstants.DUPLICATE_VFNAME)
        .setField(ProjectConstants.VF_NAME)
        .setMessage(ProfileConstants.DUPLICATE_VFNAME_MSG);
    
    public static final ErrorMessage MISSING_LANGUAGE_CODE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.LANGUAGE_CODE)
        .setCode(ProfileConstants.MISSING_LANGUAGE_CODE)
        .setMessage(ProfileConstants.MISSING_LANGUAGE_CODE_MSG);
    
    public static final ErrorMessage MALFORMED_LANGUAGE_CODE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.LANGUAGE_CODE)
        .setCode(ProfileConstants.MALFORMED_LANGUAGE_CODE)
        .setMessage(ProfileConstants.MALFORMED_LANGUAGE_CODE_MSG);
    
    public static final ErrorMessage INVALID_LANGUAGE_CODE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.LANGUAGE_CODE)
        .setCode(ProfileConstants.INVALID_LANGUAGE_CODE)
        .setMessage(ProfileConstants.INVALID_LANGUAGE_CODE_MSG);
    
    public static final ErrorMessage INACTIVE_LANGAUGE_CODE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.LANGUAGE_CODE)
        .setCode(ProfileConstants.INACTIVE_LANGUAGE_CODE)
        .setMessage(ProfileConstants.INACTIVE_LANGUAGE_CODE_MSG);
    
    public static final ErrorMessage UPDATE_IN_PROGRESS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_UPDATE_IN_PROGRESS)
        .setField(PROCESSING_STATUS)
        .setMessage(ERROR_UPDATE_IN_PROGRESS_MSG);
    
    public static final ErrorMessage NULL_VISIBILITY_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_VISIBILITY)
        .setField(VISIBILITY)
        .setMessage(ERROR_NULL_VISIBILITY_MSG);
    
    public static final ErrorMessage ERROR_EMAIL_MUST_BE_SUPPLIED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_EMAIL_MUST_BE_SUPPLIED)
            .setField(EMAIL)
            .setMessage(ERROR_EMAIL_MUST_BE_SUPPLIED_MSG);
    
    public static final ErrorMessage INVALID_EMAIL_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_EMAIL)
            .setField(EMAIL)
            .setMessage(ERROR_INVALID_EMAIL_MSG);
}
