package com.maritz.culturenext.jpa.repository;

import com.maritz.core.jpa.entity.ProgramActivityMisc;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface CnxtProgramAwardTierRepository extends BaseJpaRepository<ProgramAwardTier,Long> {

    //Get a list of nominations that are pending on approval and its program has a reminder_days value.
    @Query(value = "select NOM.ID, CAST(NOM.CREATE_DATE AS DATE) AS CREATE_DATE from component.NOMINATION NOM " +
            "left join component.PROGRAM PROG on PROG.PROGRAM_ID = NOM.PROGRAM_ID " +
            "left join component.RECOGNITION RECG on RECG.NOMINATION_ID = NOM.ID " +
            "left join component.PROGRAM_AWARD_TIER PAT on PROG.PROGRAM_ID = PAT.PROGRAM_ID " +
            "left join component.PROGRAM_AWARD_TIER_REMINDER PATR on PAT.ID = PATR.PROGRAM_AWARD_TIER_ID " +
            "where PROG.STATUS_TYPE_CODE='ACTIVE' and PATR.ENABLED=1 and PAT.STATUS_TYPE_CODE='ACTIVE' " +
            "and NOM.STATUS_TYPE_CODE='PENDING' and RECG.AMOUNT BETWEEN PAT.MIN_AMOUNT and PAT.MAX_AMOUNT GROUP BY CAST(NOM.CREATE_DATE AS DATE),NOM.ID",
            nativeQuery = true)
    List<Map<String, Object>> getPendingApprovalProgramReminderDays();

    @Query( value = "select ISNULL(patr.enabled,0) from component.Program_Award_Tier_Reminder patr " +
                    "inner join component.Program_Award_Tier pat " +
                    "on pat.id = patr.program_Award_Tier_Id " +
                    "where patr.program_Award_Tier_Id = :programAwardTierId",nativeQuery = true)
    Boolean getApprovalReminderEmaildById(@Param("programAwardTierId") Long programAwardTierId);

    @Modifying @Transactional
    @Query( value = "update component.Program_Award_Tier_Reminder set enabled = :enabled " +
            "from component.PROGRAM_AWARD_TIER_REMINDER patr "+
            "inner join component.Program_Award_Tier pat " +
            "on pat.id = patr.program_Award_Tier_Id " +
            "where patr.program_Award_Tier_Id = :programAwardTierId",nativeQuery = true)
    void updateApprovalReminderEmaildById(@Param("programAwardTierId") Long programAwardTierId,@Param("enabled") boolean enabled);

    @Modifying @Transactional
    @Query( value = "delete patr from component.PROGRAM_AWARD_TIER_REMINDER patr "+
            "inner join component.Program_Award_Tier pat " +
            "on pat.id = patr.program_Award_Tier_Id " +
            "where patr.program_Award_Tier_Id = :programAwardTierId",nativeQuery = true)
    void deleteApprovalReminderEmaildById(@Param("programAwardTierId") Long programAwardTierId);
}
