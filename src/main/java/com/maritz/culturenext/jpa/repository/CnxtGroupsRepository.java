package com.maritz.culturenext.jpa.repository;

import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import java.util.List;

public interface CnxtGroupsRepository extends BaseJpaRepository<Groups,Long> {
    
    List<Groups> findByStatusTypeCode(String code);
}
