package com.maritz.culturenext.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtBudgetRepository extends BaseJpaRepository<Budget, Long> {

    List<Budget> findByBudgetCode(String budgetCode);
    
    @Query(value =
            "select " +
            "(CASE " +
                    "when DEBITS.DEBITS is null then 0 " + 
                    "else DEBITS.DEBITS " +
            "end) " +
            "- " +
            "(CASE " +
                "when CREDITS.CREDITS is null then 0 " + 
                "else CREDITS.CREDITS   " +
            "end) " +
            "AS 'TOTAL_USED' " +
            "from component.budget b " +
            "left join ( " +
                "select b.id, SUM(disc_debit.amount) AS 'DEBITS' " +
                "from component.budget b  " +
                "left join component.DISCRETIONARY disc_debit  " +
                "on disc_debit.budget_id_debit = b.id " + 
                "join component.transaction_header th_debit " +
                "on th_debit.id = disc_debit.transaction_header_id " +
                "and th_debit.TYPE_CODE = 'DISC' " +
                "and th_debit.SUB_TYPE_CODE <> 'FUND' " +
                "and th_debit.status_type_code in ('PENDING', 'APPROVED') " +
                "group by b.id " +
            ")DEBITS on b.id = DEBITS.id " +
            "left join ( " +    
                "select b.id,SUM(disc_credit.amount) 'CREDITS' " +
                "from component.budget b  " +
                "left join component.DISCRETIONARY disc_credit " +
                "on disc_credit.budget_id_credit = b.id " +
                "join component.transaction_header th_credit " +
                "on th_credit.id = disc_credit.transaction_header_id " +
                "and th_credit.TYPE_CODE = 'DISC' " +
                "and th_credit.SUB_TYPE_CODE <> 'FUND' " +
                "and th_credit.status_type_code in ('PENDING', 'APPROVED') " +
                "group by b.id " +
            ") CREDITS on b.id = CREDITS.id " +
            "where b.id = :budgetId", nativeQuery = true)
    public Integer getBudgedUsed(@Param("budgetId") Long budgetId);

    @Query(value =
            "select " +
            "(CASE " +
                "when CREDITS.CREDITS is null then 0 " + 
                "else CREDITS.CREDITS " +
            "end) " +
            "- " +
            "(CASE " + 
                "when DEBITS.DEBITS is null then 0 " + 
                "else DEBITS.DEBITS " +
            "end) " +
            "AS 'TOTAL_AMOUNT' " +
            "from component.budget b " +
            "left join ( " +
            "select b.id, SUM(disc_debit.amount) AS 'DEBITS' " +
            "from component.budget b " +
            "left join component.DISCRETIONARY disc_debit " +
            "on disc_debit.budget_id_debit = b.id " +
            "join component.transaction_header th_debit " +
            "on th_debit.id = disc_debit.transaction_header_id " +
            "and th_debit.TYPE_CODE = 'DISC' " +
            "and th_debit.SUB_TYPE_CODE = 'FUND' " + 
            "group by b.id )DEBITS " +
            "on b.id = DEBITS.id " +
            "left join ( " +
            "select b.id, SUM(disc_credit.amount) AS 'CREDITS' " +
            "from component.budget b " +
            "left join component.DISCRETIONARY disc_credit " +
            "on disc_credit.budget_id_credit = b.id " +
            "join component.transaction_header th_credit " +
            "on th_credit.id = disc_credit.transaction_header_id " +
            "and th_credit.TYPE_CODE = 'DISC' " +
            "and th_credit.SUB_TYPE_CODE = 'FUND' " +
            "group by b.id ) CREDITS " +
            "on b.id = CREDITS.id " +
            "where b.id = :budgetId", nativeQuery = true)
    public Integer getTotalBudget(@Param("budgetId") Long budgetId);

}
