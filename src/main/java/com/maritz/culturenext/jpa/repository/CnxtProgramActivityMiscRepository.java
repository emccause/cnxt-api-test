package com.maritz.culturenext.jpa.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.ProgramActivityMisc;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtProgramActivityMiscRepository extends BaseJpaRepository<ProgramActivityMisc, Long> {
    
    @Query(
        "select pam from ProgramActivityMisc pam " +
        "inner join ProgramActivity pa " +
            "on pam.programActivityId = pa.programActivityId " +
        "where pa.programId = :programId " +
            "and pam.vfName = :vfName")
    List<ProgramActivityMisc> findByProgramIdAndVfName(@Param("programId") Long programId, @Param("vfName") String vfName);
    
    void deleteByProgramActivityId(Long programActivityId);
    void deleteByProgramActivityIdIn(Collection<Long> programActivityIdList);

}
