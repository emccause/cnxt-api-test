package com.maritz.culturenext.jpa.repository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.maritz.core.jpa.entity.TranslatablePhrase;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtTranslatablePhraseRepository extends BaseJpaRepository<TranslatablePhrase,Long> {
	

	@Query(nativeQuery = true, value =
			"select tp.*" +
			"  from TRANSLATABLE_PHRASE tp" +
			" where tp.TARGET_ID = :targetId and tp.TRANSLATABLE_ID in (:translatableId) and tp.LOCALE_CODE = :localeCode"
		)
	    List<TranslatablePhrase> findTranslatablePhraseObject(
			@Param("targetId") Long targetId,
			@Param("translatableId") ArrayList<String> translatableId,
			@Param("localeCode") String localeCode
			
		);	

}