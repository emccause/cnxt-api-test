package com.maritz.culturenext.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtAclRepository extends BaseJpaRepository<Acl, Long> {
    
    @Modifying @Transactional
    @Query(value = "delete from component.ACL where ROLE_ID = :roleId and TARGET_TABLE = :targetType and TARGET_ID in :idList and SUBJECT_TABLE = :subjectType", nativeQuery = true)
    public void revokeAllRolesByTargetAndSubject(@Param("roleId") Long roleId, @Param("targetType") String targetType, @Param("idList") List<Long> idList, @Param("subjectType") String subjectType);
    
    @Query(nativeQuery = true, value =
            "select * from component.ACL " +
                    "where ROLE_ID = :roleId " +
                    "and TARGET_TABLE = :targetType " +
                    "and TARGET_ID = :targetId "
    )
    List<Acl> findEligibilityAclByRole(
            @Param("roleId") Long roleId,
            @Param("targetType") String targetType,
            @Param("targetId") Long targetId
    );

    @Query(nativeQuery = true, value =
            "select * from component.ACL " +
                    "where TARGET_TABLE = :targetType " +
                    "and TARGET_ID = :targetId "
    )
    List<Acl> findAllEligibilityAcl(
            @Param("targetType") String targetType,
            @Param("targetId") Long targetId
    );

    @Modifying @Transactional
    @Query(nativeQuery = true, value =
            "insert into component.ACL (role_id, subject_table, subject_id, target_table, target_id)" +
                    " values (:roleId, :subjectType, :subjectId, :targetType, :targetId)"
    )
    int addAcl(
            @Param("roleId") Long roleId,
            @Param("targetType") String targetType,
            @Param("targetId") Long targetId,
            @Param("subjectType") String subjectType,
            @Param("subjectId") Long subjectId
    );

    @Modifying @Transactional
    @Query(nativeQuery = true, value =
            "delete acl" +
                    " from component.acl acl" +
                    " where acl.target_table = :targetType" +
                    "   and acl.target_id = :targetId" +
                    "   and (:roleId is null or acl.role_id = :roleId)" +
                    "   and (:subjectType is null or acl.subject_table = :subjectType)" +
                    "   and (:subjectId is null or acl.subject_id = :subjectId)"
    )
    int deleteAcl(
            @Param("targetType") String targetType,
            @Param("targetId") Long targetId,
            @Param("roleId") Long roleId,
            @Param("subjectType") String subjectType,
            @Param("subjectId") Long subjectId
    );
}
