package com.maritz.culturenext.jpa.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtNominationRepository extends BaseJpaRepository<Nomination,Long> {
    
    @Query(value = "select count(n.id) from Nomination n where n.SUBMITTER_PAX_ID = :paxId and n.status_type_code in ('APPROVED')", nativeQuery = true)
    public int getNominationCountBySubmitter(@Param("paxId") long paxId);

    //Get last nomination given by submitter ID
    Nomination findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(long submitterPaxId, String statusTypeCode);
    
    //Get last nomination received by recipient ID    
    @Query(value = "SELECT TOP 1 AH.APPROVAL_DATE FROM component.NOMINATION N "
            + "JOIN component.RECOGNITION R on R.NOMINATION_ID = N.ID "
            + "JOIN component.APPROVAL_HISTORY AH on R.ID = AH.TARGET_ID "
            + "WHERE R.RECEIVER_PAX_ID = :receiverPaxId "
            + "AND R.STATUS_TYPE_CODE IN ('APPROVED', 'AUTO_APPROVED' ) "
            + "AND AH.TARGET_TABLE = 'RECOGNITION' "
            + "ORDER BY N.SUBMITTAL_DATE DESC",
            nativeQuery = true)
    Date getLastRecReceived(@Param("receiverPaxId") long paxId);
}
