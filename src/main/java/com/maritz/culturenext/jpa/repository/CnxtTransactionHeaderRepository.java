package com.maritz.culturenext.jpa.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import com.maritz.core.jpa.support.util.Tuple4;

public interface CnxtTransactionHeaderRepository extends BaseJpaRepository<TransactionHeader, Long> {
    
    @Query(nativeQuery = true, value =  
            "SELECT * FROM component.TRANSACTION_HEADER " +
                    "WHERE BATCH_ID = ( SELECT TOP 1 BATCH_ID " +
                    "FROM component.BATCH_EVENT " +
                    "WHERE TARGET_ID = :nominationId)")
    List<TransactionHeader> getTransactionHeaderByNominationId(@Param("nominationId") Long nominationId);
    
    @Query(nativeQuery = true, value =  
            "SELECT TOP 1 * FROM component.TRANSACTION_HEADER TH " +
                    "INNER JOIN component.PAX_GROUP PG " +
                    "ON TH.PAX_GROUP_ID = PG.PAX_GROUP_ID " +
                    "WHERE TH.BATCH_ID = ( SELECT TOP 1 BATCH_ID " +
                    "FROM component.BATCH_EVENT " +
                    "WHERE TARGET_ID = :nominationId " +
                    "AND TARGET_TABLE = 'NOMINATION') " +
                    "AND PG.PAX_ID = :paxId"
                    )
    TransactionHeader getTransactionHeaderByNominationIdAndPaxId(@Param("nominationId") Long nominationId, @Param("paxId") Long paxId);
       
    @Query("SELECT new com.maritz.core.jpa.support.util.Tuple4(TRANSACTION_HEADER.id, PAYOUT_TYPE.miscData, " +
            "COALESCE(BUDGET_OVERRIDE_PROJECT_NUMBER.miscData, DEFAULT_PROJECT_NUMBER.miscData), " +
            "COALESCE(CONFIGURABLE_SUB_PROJECT_NUMBER.miscData, BUDGET_OVERRIDE_SUB_PROJECT_NUMBER.miscData, DEFAULT_SUB_PROJECT_NUMBER.miscData)) " +
            "FROM TransactionHeader TRANSACTION_HEADER " + 
            "LEFT JOIN TransactionHeaderMisc PAYOUT_TYPE " + 
                "ON TRANSACTION_HEADER.id = PAYOUT_TYPE.transactionHeaderId " + 
                "AND PAYOUT_TYPE.vfName = 'PAYOUT_TYPE' " + 
            "LEFT JOIN TransactionHeaderMisc DEFAULT_PROJECT_NUMBER " + 
                "ON TRANSACTION_HEADER.id = DEFAULT_PROJECT_NUMBER.transactionHeaderId " + 
                "AND DEFAULT_PROJECT_NUMBER.vfName = 'DEFAULT_PROJECT_NUMBER' " + 
            "LEFT JOIN TransactionHeaderMisc DEFAULT_SUB_PROJECT_NUMBER " + 
                "ON TRANSACTION_HEADER.id = DEFAULT_SUB_PROJECT_NUMBER.transactionHeaderId " + 
                "AND DEFAULT_SUB_PROJECT_NUMBER.vfName = 'DEFAULT_SUB_PROJECT_NUMBER' " +
            "LEFT JOIN TransactionHeaderMisc BUDGET_OVERRIDE_PROJECT_NUMBER " +
                "ON TRANSACTION_HEADER.id = BUDGET_OVERRIDE_PROJECT_NUMBER.transactionHeaderId " +
                "AND BUDGET_OVERRIDE_PROJECT_NUMBER.vfName = 'BUDGET_OVERRIDE_PROJECT_NUMBER' " +
            "LEFT JOIN TransactionHeaderMisc BUDGET_OVERRIDE_SUB_PROJECT_NUMBER " +
                "ON TRANSACTION_HEADER.id = BUDGET_OVERRIDE_SUB_PROJECT_NUMBER.transactionHeaderId " +
                "AND BUDGET_OVERRIDE_SUB_PROJECT_NUMBER.vfName = 'BUDGET_OVERRIDE_SUB_PROJECT_NUMBER' " +
            "LEFT JOIN TransactionHeaderMisc CONFIGURABLE_SUB_PROJECT_NUMBER " +
                "ON TRANSACTION_HEADER.id = CONFIGURABLE_SUB_PROJECT_NUMBER.transactionHeaderId " +
                "AND CONFIGURABLE_SUB_PROJECT_NUMBER.vfName = 'CONFIGURABLE_SUB_PROJECT_NUMBER' " +
            "WHERE TRANSACTION_HEADER.id IN :transactionHeaderIds")
    List<Tuple4<Long,String,String,String>> findPayoutTypeProjectSubProjectNumberByTransactionHeaderId(@Param("transactionHeaderIds") Collection<Long> transactionHeaderIds);
}