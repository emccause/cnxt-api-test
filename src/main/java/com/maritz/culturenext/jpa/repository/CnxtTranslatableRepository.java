package com.maritz.culturenext.jpa.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.maritz.core.jpa.entity.Translatable;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtTranslatableRepository extends BaseJpaRepository<Translatable,Long> { 
	
	@Query(nativeQuery = true, value =
			"select * " +
			" from TRANSLATABLE " +
			" where TABLE_NAME = :program"
		)
	    List<Translatable> findTranslatableList(
			@Param("program") String program
		);	
	


}
