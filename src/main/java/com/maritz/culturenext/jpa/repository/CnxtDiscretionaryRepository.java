package com.maritz.culturenext.jpa.repository;

import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtDiscretionaryRepository extends BaseJpaRepository<Discretionary,Long> {

    @Query(nativeQuery = true, value =
        "SELECT disc.*" +
            " FROM component.discretionary disc" +
            " JOIN component.transaction_header th" +
            " ON  disc.transaction_header_id = th.id" +
            " JOIN component.program PRGM" +
            " ON PRGM.program_id = th.program_id" +
            " WHERE PRGM.program_id = :programId" +
            " AND PRGM.program_type_code = 'PNTU'")
    List<Discretionary> getPointLoadBudgetAllocations(@Nonnull @Param("programId") Long programId);

    @Query(nativeQuery = true, value =
            "SELECT top 1 disc.*" +
                    " FROM component.discretionary disc" +
                    " JOIN component.transaction_header th" +
                    " ON  disc.transaction_header_id = th.id" +
                    " JOIN component.program PRGM" +
                    " ON PRGM.program_id = th.program_id" +
                    " WHERE PRGM.program_id = :programId" +
                    " AND PRGM.program_type_code = 'PNTU'")
    List<Discretionary> getTopPointLoadBudgetAllocations(@Nonnull @Param("programId") Long programId);

}
