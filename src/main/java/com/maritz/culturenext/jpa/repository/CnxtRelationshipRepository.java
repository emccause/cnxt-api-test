package com.maritz.culturenext.jpa.repository;

import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CnxtRelationshipRepository extends BaseJpaRepository<Relationship, Long> {

    @Query(value = "SELECT * " +
            "FROM component.RELATIONSHIP rel " +
            "LEFT JOIN component.SYS_USER su ON rel.PAX_ID_2 = su.PAX_ID " +
            "WHERE rel.PAX_ID_1 IN :paxIds " +
            "AND rel.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
            "AND su.STATUS_TYPE_CODE <> 'INACTIVE'", nativeQuery = true)
    List<Relationship> getActiveManagerRelationships(@Param("paxIds") List<Long> paxIds);
}
