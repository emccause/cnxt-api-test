package com.maritz.culturenext.jpa.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtBatchRepository extends BaseJpaRepository<Batch, Long> {
	
	
	@Query(nativeQuery = true, value =
			"select * from component.BATCH where CREATE_DATE > dateadd(day,datediff(day,0,GETDATE()),0) order by BATCH_ID desc"
		)
	List<Batch> findLatestBatchInformation();
	
	@Query(nativeQuery = true, value =
			"select * from component.BATCH  where BATCH_ID = :batchId"
		)
	List<Batch> findBatchInformationById(	
			@Param("batchId") Long batchId
	);
	
	@Query(nativeQuery = true, value =
			"select BATCH_TYPE_CODE from component.BATCH group by BATCH_TYPE_CODE"
		)
	List<String> findBatchTypeCodes();
	
	@Query(nativeQuery = true, value =
			"select * from component.BATCH  where CREATE_DATE > dateadd(day,datediff(day,:numberOfDays,GETDATE()),0) "
			+ " and BATCH_TYPE_CODE = :statusTypeCode order by BATCH_ID desc"
		)
	List<Batch> findBatchInformationByStatusTypeCode(	
			@Param("numberOfDays") Long numberOfDays,
			@Param("statusTypeCode") String statusTypeCode
	);
}

