package com.maritz.culturenext.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtRecognitionRepository extends BaseJpaRepository<Recognition,Long> {

    @Query(value = "select count(r.id) from Recognition r where r.RECEIVER_PAX_ID = :paxId and r.status_type_code in ('APPROVED', 'AUTO_APPROVED') and r.PARENT_ID IS NULL", nativeQuery = true)
    public int getRecognitionCountByRecipient(@Param("paxId") long paxId);
    

    @Query(value = "SELECT DISTINCT RECG.RECEIVER_PAX_ID FROM RECOGNITION RECG " +
                    "INNER JOIN PAX_MISC PM " +
                    "ON RECG.RECEIVER_PAX_ID = PM.PAX_ID " +
                    "WHERE RECG.NOMINATION_ID = :nominationId " +
                    "AND PM.VF_NAME = 'SHARE_REC' " +
                    "AND PM.MISC_DATA = 'TRUE' " +
                    "AND RECG.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED')", nativeQuery = true)
    List<Long> getApprovedReceiverPaxIdsWithPublicSharePermission(@Param("nominationId") Long nominationId);
    
    @Query(value = "SELECT SUM(AMOUNT) FROM RECOGNITION WHERE NOMINATION_ID = :nominationId", nativeQuery = true)
    Long getTotalPointsForNomination(@Param("nominationId") Long nominationId);
}
