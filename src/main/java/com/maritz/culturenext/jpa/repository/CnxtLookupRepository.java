package com.maritz.culturenext.jpa.repository;

import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import com.maritz.core.jpa.support.util.Tuple3;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

public interface CnxtLookupRepository extends BaseJpaRepository<Lookup,Long> {
	
		@Query("select new com.maritz.core.jpa.support.util.Tuple3(id, lookupCategoryCode,lookupDesc) " +
	            "from Lookup " +
	            "where lookupCode = 'DISPLAY_NAME'"
	            )
	    List<Tuple3<Long, String,String>> getLookupCategoryCodeId();
}
