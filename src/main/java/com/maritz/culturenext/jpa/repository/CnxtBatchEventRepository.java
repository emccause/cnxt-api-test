package com.maritz.culturenext.jpa.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import com.maritz.culturenext.constants.ProjectConstants;

public interface CnxtBatchEventRepository extends BaseJpaRepository<BatchEvent, Long> {
	
	@Query(nativeQuery = true, value =
			"select * from component.BATCH_EVENT where BATCH_ID = :batchId "
		)
	List<BatchEvent> findBatchEventInformationById(	
			@Param("batchId") Long batchId
	);

	@Query(nativeQuery = true, value =
			"select  count(MESSAGE) from component.BATCH_EVENT where BATCH_ID = :batchId "
			+ "and BATCH_EVENT_TYPE_CODE ='"+ProjectConstants.BATCH_EVENT_TYPE_CODE_ERR+"' and message = '"+ProjectConstants.BATCH_EVENT_MESSAGE_ACCESS_IS_DENIED+"' "
		)
	long findBatchEventErrorCount(	
			@Param("batchId") Long batchId
	);
	
	@Query(nativeQuery = true, value =
			"select count(PAX_ID) from component.UF_GET_SERVICE_ANNIVERSARIES(:days) where transaction_id is null "
		)
	long findMissingServiceAnniversariesCount(	
			@Param("days") Long days
	);

	
}