package com.maritz.culturenext.jpa.repository;

import com.maritz.core.jpa.entity.ProgramType;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtProgramTypeRepository extends BaseJpaRepository<ProgramType,Long> {
        
    ProgramType findByProgramTypeName(String programTypeName);
}
