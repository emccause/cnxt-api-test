package com.maritz.culturenext.jpa.repository;

import java.util.List;

import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtGroupConfigRepository extends BaseJpaRepository<GroupConfig,Long> {
    
    List<GroupConfig> findByGroupConfigTypeCode(String groupConfigTypeCode);
}
