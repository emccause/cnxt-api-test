package com.maritz.culturenext.jpa.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;

public interface CnxtProgramActivityRepository extends BaseJpaRepository<ProgramActivity, Long> {

    /**
     * Verify if an activity per type per program for a given timeframe already exists.
     * @param programActivityId
     * @param programId
     * @param programActivityType
     * @param startDate
     * @param endDate
     * @return if time frame is overlapped, return a program activity id. Otherwise null.
     */
    @Query(nativeQuery = true, value =
            "SELECT TOP 1 PROGRAM_ACTIVITY_ID FROM component.PROGRAM_ACTIVITY "
            + "WHERE PROGRAM_ID = :programId "
            + "AND PROGRAM_ACTIVITY_TYPE_CODE = :programActivityType "
            + "AND (:programActivityId IS NULL OR PROGRAM_ACTIVITY_ID <> :programActivityId )"
            + "AND ("
            + "  FROM_DATE BETWEEN :startDate AND :endDate "
            + "  OR THRU_DATE BETWEEN :startDate AND :endDate "
            + "  OR (FROM_DATE < :startDate AND THRU_DATE > :endDate) "
            + ")")
    Long isOverlappedTime(
            @Param("programActivityId") Long programActivityId,
            @Param("programId") Long programId,
            @Param("programActivityType") String programActivityType,
            @Param("startDate") String startDate,
            @Param("endDate") String endDate);

    void deleteByProgramActivityIdIn(Collection<Long> programActivityIdList);
    void deleteByProgramId(Long programId);
    List<ProgramActivity> findByProgramId(Long programId);
}
