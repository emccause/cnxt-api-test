package com.maritz.culturenext.jpa.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.support.repository.BaseJpaRepository;
import com.maritz.core.jpa.support.util.Tuple3;

public interface CnxtProgramMiscRepository extends BaseJpaRepository<ProgramMisc,Long> {

    @Query(nativeQuery = true, value =
        "select *" +
        "  from component.program_misc pm" +
        " inner join component.nomination nom" +
        " on nom.program_id = pm.program_id" +
        " where nom.id = :nominationId" +
        " and pm.vf_name in (:vfNameList)"
    )
    List<ProgramMisc> findByNominationIdAndVfNameList(
        @Param("nominationId") Long nominationId,
        @Param("vfNameList") List<String> vfNameList
    );

    @Query(nativeQuery = true, value =
            "select MISC_DATA " +
                    " from component.program_misc pm" +
                    " inner join component.nomination nom" +
                    " on nom.program_id = pm.program_id" +
                    " where nom.id = :nominationId" +
                    " and pm.vf_name =:vfName"
    )
    String findByNominationIdAndVfName(
            @Param("nominationId") Long nominationId,
            @Param("vfName") String vfName
    );

    @Query(nativeQuery = true, value =
            "SELECT TOP 1 MISC_DATA AS 'NOTIFICATION_RECIPIENT' " +
            "  FROM component.[TRANSACTION_HEADER] TH " +
            "  INNER JOIN component.[PROGRAM_MISC] PM " +
            "  ON PM.PROGRAM_ID=TH.PROGRAM_ID " +
            "  AND VF_NAME='NOTIFICATION_RECIPIENT' " +
            "  where TH.BATCH_ID = :batchId"
        )
        String findNotificationRecipient(
            @Param("batchId") Long batchId
        );

    @Query("select new com.maritz.core.jpa.support.util.Tuple3(programId, vfName, miscData) " +
            "from ProgramMisc " +
            "where programId in :programIds " +
            "and vfName = :vfName"
            )
    List<Tuple3<Long, String, String>> findMiscsByProgramIdAndVfName(@Param("programIds") Collection<Long> programIds, @Param("vfName") String vfName);


    void deleteByVfNameInAndProgramId(List<String> vfName, Long programId);

    List<ProgramMisc> findByProgramId(Long programId);
}
