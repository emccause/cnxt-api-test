package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Table(name = "VW_BUDGET_INFO_LIGHTWEIGHT")
public class VwBudgetInfoLightweight {

    @NotNull
    @Column(name="ID")
    private Long id;
    @NotNull
    @Column(name="BUDGET_TYPE_CODE")
    private String budgetTypeCode;
    @Column(name="BUDGET_NAME")
    private String budgetName;
    @Column(name="DISPLAY_NAME")
    private String displayName;
    @Column(name="BUDGET_DESC")
    private String budgetDesc;
    @Column(name="PARENT_BUDGET_ID")
    private Long parentBudgetId;
    @Column(name="INDIVIDUAL_GIVING_LIMIT")
    private Double individualGivingLimit;
    @NotNull
    @Column(name="FROM_DATE")
    private java.util.Date fromDate;
    @Column(name="THRU_DATE")
    private java.util.Date thruDate;
    @NotNull
    @Column(name="STATUS_TYPE_CODE")
    private String statusTypeCode;
    @Column(name="SUB_PROJECT_ID")
    private Long subProjectId;
    @Column(name="BUDGET_CONSTRAINT")
    private String budgetConstraint;
    @Column(name="PROJECT_NUMBER")
    private String projectNumber;
    @Column(name="SUB_PROJECT_NUMBER")
    private String subProjectNumber;
    @NotNull
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @Column(name="ALERT_AMOUNT")
    private Double alertAmount;
    @Column(name="GROUP_CONFIG_ID")
    private Long groupConfigId;
    @Column(name="GROUP_ID")
    private Long groupId;
    @Column(name="GROUP_NAME")
    private String groupName;
    @Column(name="BUDGET_OWNER")
    private Long budgetOwner;
    @Column(name="BUDGET_ALLOCATOR")
    private Long budgetAllocator;
    @Column(name="ALLOCATED_IN")
    private String allocatedIn;
    @Column(name="ALLOCATED_OUT")
    private String allocatedOut;
    @NotNull
    @Column(name="IS_USABLE")
    private String isUsable;
    @Column(name="OVERRIDE_PROJECT_NUMBER")
    private String overrideProjectNumber;
    @Column(name="OVERRIDE_SUB_PROJECT_NUMBER")
    private String overrideSubProjectNumber;
    @Column(name="OVERRIDE_LOCATION")
    private String overrideLocation;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public VwBudgetInfoLightweight() {
    }
    
    public Long getId() {
        return id;
    }
        
    public VwBudgetInfoLightweight setId(Long id) {
        this.id = id;
        return this;
    }

    public String getBudgetTypeCode() {
        return budgetTypeCode;
    }
        
    public VwBudgetInfoLightweight setBudgetTypeCode(String budgetTypeCode) {
        this.budgetTypeCode = budgetTypeCode;
        return this;
    }

    public String getBudgetName() {
        return budgetName;
    }
        
    public VwBudgetInfoLightweight setBudgetName(String budgetName) {
        this.budgetName = budgetName;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }
        
    public VwBudgetInfoLightweight setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public String getBudgetDesc() {
        return budgetDesc;
    }
        
    public VwBudgetInfoLightweight setBudgetDesc(String budgetDesc) {
        this.budgetDesc = budgetDesc;
        return this;
    }

    public Long getParentBudgetId() {
        return parentBudgetId;
    }
        
    public VwBudgetInfoLightweight setParentBudgetId(Long parentBudgetId) {
        this.parentBudgetId = parentBudgetId;
        return this;
    }

    public Double getIndividualGivingLimit() {
        return individualGivingLimit;
    }
        
    public VwBudgetInfoLightweight setIndividualGivingLimit(Double individualGivingLimit) {
        this.individualGivingLimit = individualGivingLimit;
        return this;
    }

    public java.util.Date getFromDate() {
        return fromDate;
    }
        
    public VwBudgetInfoLightweight setFromDate(java.util.Date fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public java.util.Date getThruDate() {
        return thruDate;
    }
        
    public VwBudgetInfoLightweight setThruDate(java.util.Date thruDate) {
        this.thruDate = thruDate;
        return this;
    }

    public String getStatusTypeCode() {
        return statusTypeCode;
    }
        
    public VwBudgetInfoLightweight setStatusTypeCode(String statusTypeCode) {
        this.statusTypeCode = statusTypeCode;
        return this;
    }

    public Long getSubProjectId() {
        return subProjectId;
    }
        
    public VwBudgetInfoLightweight setSubProjectId(Long subProjectId) {
        this.subProjectId = subProjectId;
        return this;
    }

    public String getBudgetConstraint() {
        return budgetConstraint;
    }
        
    public VwBudgetInfoLightweight setBudgetConstraint(String budgetConstraint) {
        this.budgetConstraint = budgetConstraint;
        return this;
    }

    public String getProjectNumber() {
        return projectNumber;
    }
        
    public VwBudgetInfoLightweight setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
        return this;
    }

    public String getSubProjectNumber() {
        return subProjectNumber;
    }
        
    public VwBudgetInfoLightweight setSubProjectNumber(String subProjectNumber) {
        this.subProjectNumber = subProjectNumber;
        return this;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }
        
    public VwBudgetInfoLightweight setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public Double getAlertAmount() {
        return alertAmount;
    }
        
    public VwBudgetInfoLightweight setAlertAmount(Double alertAmount) {
        this.alertAmount = alertAmount;
        return this;
    }

    public Long getGroupConfigId() {
        return groupConfigId;
    }
        
    public VwBudgetInfoLightweight setGroupConfigId(Long groupConfigId) {
        this.groupConfigId = groupConfigId;
        return this;
    }

    public Long getGroupId() {
        return groupId;
    }
        
    public VwBudgetInfoLightweight setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }
        
    public VwBudgetInfoLightweight setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public Long getBudgetOwner() {
        return budgetOwner;
    }
        
    public VwBudgetInfoLightweight setBudgetOwner(Long budgetOwner) {
        this.budgetOwner = budgetOwner;
        return this;
    }

    public Long getBudgetAllocator() {
        return budgetAllocator;
    }
        
    public VwBudgetInfoLightweight setBudgetAllocator(Long budgetAllocator) {
        this.budgetAllocator = budgetAllocator;
        return this;
    }

    public String getAllocatedIn() {
        return allocatedIn;
    }
        
    public VwBudgetInfoLightweight setAllocatedIn(String allocatedIn) {
        this.allocatedIn = allocatedIn;
        return this;
    }

    public String getAllocatedOut() {
        return allocatedOut;
    }
        
    public VwBudgetInfoLightweight setAllocatedOut(String allocatedOut) {
        this.allocatedOut = allocatedOut;
        return this;
    }

    public String getIsUsable() {
        return isUsable;
    }
        
    public VwBudgetInfoLightweight setIsUsable(String isUsable) {
        this.isUsable = isUsable;
        return this;
    }

    public String getOverrideProjectNumber() {
        return overrideProjectNumber;
    }
        
    public VwBudgetInfoLightweight setOverrideProjectNumber(String overrideProjectNumber) {
        this.overrideProjectNumber = overrideProjectNumber;
        return this;
    }

    public String getOverrideSubProjectNumber() {
        return overrideSubProjectNumber;
    }
        
    public VwBudgetInfoLightweight setOverrideSubProjectNumber(String overrideSubProjectNumber) {
        this.overrideSubProjectNumber = overrideSubProjectNumber;
        return this;
    }

    public String getOverrideLocation() {
        return overrideLocation;
    }
        
    public VwBudgetInfoLightweight setOverrideLocation(String overrideLocation) {
        this.overrideLocation = overrideLocation;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public VwBudgetInfoLightweight setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public VwBudgetInfoLightweight setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
