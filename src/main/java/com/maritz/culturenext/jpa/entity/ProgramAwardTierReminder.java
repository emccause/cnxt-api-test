package com.maritz.culturenext.jpa.entity;

import com.maritz.core.jdbc.annotation.ConcentrixDao;
import com.maritz.core.jpa.support.audit.Auditable;
import com.maritz.core.jpa.support.audit.AuditableEntityListener;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;


@ConcentrixDao
@Entity
@EntityListeners(AuditableEntityListener.class)
@Table(name = "PROGRAM_AWARD_TIER_REMINDER")
public class ProgramAwardTierReminder implements Auditable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", insertable = false, updatable = false)
    private Long id;
    @NotNull
    @Column(name="PROGRAM_AWARD_TIER_ID")
    private Long programAwardTierId;
    @Column(name="ENABLED")
    private boolean enabled;
    @NotNull
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @NotNull
    @CreatedBy
    @Column(name="CREATE_ID", updatable = false)
    private String createId;
    @NotNull
    @LastModifiedDate
    @Column(name="UPDATE_DATE")
    private java.util.Date updateDate;
    @NotNull
    @LastModifiedBy
    @Column(name="UPDATE_ID")
    private String updateId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;

    public ProgramAwardTierReminder(){}

    public ProgramAwardTierReminder(Long programAwardTierId, boolean enabled){
        this.programAwardTierId = programAwardTierId;
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProgramAwardTierId() {
        return programAwardTierId;
    }

    public void setProgramAwardTierId(Long programAwardTierId) {
        this.programAwardTierId = programAwardTierId;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }

    public ProgramAwardTierReminder setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getCreateId() {
        return createId;
    }

    public ProgramAwardTierReminder setCreateId(String createId) {
        this.createId = createId;
        return this;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public ProgramAwardTierReminder setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getUpdateId() {
        return updateId;
    }

    public ProgramAwardTierReminder setUpdateId(String updateId) {
        this.updateId = updateId;
        return this;
    }

    public long getCrc() {
        return crc;
    }

    public ProgramAwardTierReminder setCrc(long crc) {
        this.crc = crc;
        return this;
    }

    public boolean getMarkForDelete() {
        return markForDelete;
    }

    public ProgramAwardTierReminder setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
