package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Table(name = "VW_PAX_PROGRAM_BUDGET")
public class VwPaxProgramBudget {

    @NotNull
    @Column(name="PAX_ID")
    private Long paxId;
    @Column(name="PROGRAM_ID")
    private Long programId;
    @Column(name="BUDGET_ID")
    private Long budgetId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public VwPaxProgramBudget() {
    }
    
    public Long getPaxId() {
        return paxId;
    }
        
    public VwPaxProgramBudget setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public Long getProgramId() {
        return programId;
    }
        
    public VwPaxProgramBudget setProgramId(Long programId) {
        this.programId = programId;
        return this;
    }

    public Long getBudgetId() {
        return budgetId;
    }
        
    public VwPaxProgramBudget setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public VwPaxProgramBudget setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public VwPaxProgramBudget setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
