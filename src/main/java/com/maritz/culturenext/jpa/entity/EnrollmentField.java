package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;
import com.maritz.core.jpa.support.audit.Auditable;
import com.maritz.core.jpa.support.audit.AuditableEntityListener;

@ConcentrixDao
@Entity
@EntityListeners(AuditableEntityListener.class)
@Table(name = "ENROLLMENT_FIELD")
public class EnrollmentField implements Auditable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", insertable = false, updatable = false)
    private Long id;
    @NotNull
    @Column(name="FIELD_NAME")
    private String fieldName;
    @NotNull
    @Column(name="FIELD_DISPLAY_NAME")
    private String fieldDisplayName;
    @NotNull
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @NotNull
    @CreatedBy
    @Column(name="CREATE_ID", updatable = false)
    private String createId;
    @NotNull
    @LastModifiedDate
    @Column(name="UPDATE_DATE")
    private java.util.Date updateDate;
    @NotNull
    @LastModifiedBy
    @Column(name="UPDATE_ID")
    private String updateId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public EnrollmentField() {
    }
    
    public Long getId() {
        return id;
    }
        
    public EnrollmentField setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFieldName() {
        return fieldName;
    }
        
    public EnrollmentField setFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public String getFieldDisplayName() {
        return fieldDisplayName;
    }
        
    public EnrollmentField setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
        return this;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }
        
    public EnrollmentField setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getCreateId() {
        return createId;
    }
        
    public EnrollmentField setCreateId(String createId) {
        this.createId = createId;
        return this;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }
        
    public EnrollmentField setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getUpdateId() {
        return updateId;
    }
        
    public EnrollmentField setUpdateId(String updateId) {
        this.updateId = updateId;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public EnrollmentField setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public EnrollmentField setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
