package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Table(name = "VW_USER_GROUPS_SEARCH_DATA")
public class VwUserGroupsSearchData {

    @Column(name="NAME")
    private String name;
    @NotNull
    @Column(name="SEARCH_TYPE")
    private String searchType;
    @NotNull
    @Column(name="STATUS_TYPE_CODE")
    private String statusTypeCode;
    @Column(name="PAX_ID")
    private Long paxId;
    @Column(name="LANGUAGE_CODE")
    private String languageCode;
    @Column(name="PREFERRED_LOCALE")
    private String preferredLocale;
    @Column(name="LANGUAGE_LOCALE")
    private String languageLocale;
    @Column(name="CONTROL_NUM")
    private String controlNum;
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="MIDDLE_NAME")
    private String middleName;
    @Column(name="LAST_NAME")
    private String lastName;
    @Column(name="NAME_PREFIX")
    private String namePrefix;
    @Column(name="NAME_SUFFIX")
    private String nameSuffix;
    @Column(name="COMPANY_NAME")
    private String companyName;
    @Column(name="VERSION")
    private Integer version;
    @Column(name="JOB_TITLE")
    private String jobTitle;
    @Column(name="SYS_USER_ID")
    private Long sysUserId;
    @Column(name="MANAGER_PAX_ID")
    private Long managerPaxId;
    @Column(name="COUNTRY_CODE")
    private String countryCode;
    @Column(name="GROUP_ID")
    private Long groupId;
    @Column(name="GROUP_CONFIG_ID")
    private Long groupConfigId;
    @Column(name="FIELD")
    private String field;
    @Column(name="FIELD_DISPLAY_NAME")
    private String fieldDisplayName;
    @Column(name="GROUP_NAME")
    private String groupName;
    @Column(name="GROUP_TYPE")
    private String groupType;
    @Column(name="VISIBILITY")
    private String visibility;
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @Column(name="PAX_COUNT")
    private Integer paxCount;
    @Column(name="GROUP_COUNT")
    private Integer groupCount;
    @Column(name="TOTAL_PAX_COUNT")
    private Integer totalPaxCount;
    @Column(name="GROUP_PAX_ID")
    private Long groupPaxId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public VwUserGroupsSearchData() {
    }
    
    public String getName() {
        return name;
    }
        
    public VwUserGroupsSearchData setName(String name) {
        this.name = name;
        return this;
    }

    public String getSearchType() {
        return searchType;
    }
        
    public VwUserGroupsSearchData setSearchType(String searchType) {
        this.searchType = searchType;
        return this;
    }

    public String getStatusTypeCode() {
        return statusTypeCode;
    }
        
    public VwUserGroupsSearchData setStatusTypeCode(String statusTypeCode) {
        this.statusTypeCode = statusTypeCode;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }
        
    public VwUserGroupsSearchData setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public String getLanguageCode() {
        return languageCode;
    }
        
    public VwUserGroupsSearchData setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
        return this;
    }

    public String getPreferredLocale() {
        return preferredLocale;
    }
        
    public VwUserGroupsSearchData setPreferredLocale(String preferredLocale) {
        this.preferredLocale = preferredLocale;
        return this;
    }

    public String getLanguageLocale() {
        return languageLocale;
    }
        
    public VwUserGroupsSearchData setLanguageLocale(String languageLocale) {
        this.languageLocale = languageLocale;
        return this;
    }

    public String getControlNum() {
        return controlNum;
    }
        
    public VwUserGroupsSearchData setControlNum(String controlNum) {
        this.controlNum = controlNum;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }
        
    public VwUserGroupsSearchData setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }
        
    public VwUserGroupsSearchData setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }
        
    public VwUserGroupsSearchData setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getNamePrefix() {
        return namePrefix;
    }
        
    public VwUserGroupsSearchData setNamePrefix(String namePrefix) {
        this.namePrefix = namePrefix;
        return this;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }
        
    public VwUserGroupsSearchData setNameSuffix(String nameSuffix) {
        this.nameSuffix = nameSuffix;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }
        
    public VwUserGroupsSearchData setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public Integer getVersion() {
        return version;
    }
        
    public VwUserGroupsSearchData setVersion(Integer version) {
        this.version = version;
        return this;
    }

    public String getJobTitle() {
        return jobTitle;
    }
        
    public VwUserGroupsSearchData setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public Long getSysUserId() {
        return sysUserId;
    }
        
    public VwUserGroupsSearchData setSysUserId(Long sysUserId) {
        this.sysUserId = sysUserId;
        return this;
    }

    public Long getManagerPaxId() {
        return managerPaxId;
    }
        
    public VwUserGroupsSearchData setManagerPaxId(Long managerPaxId) {
        this.managerPaxId = managerPaxId;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }
        
    public VwUserGroupsSearchData setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public Long getGroupId() {
        return groupId;
    }
        
    public VwUserGroupsSearchData setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }

    public Long getGroupConfigId() {
        return groupConfigId;
    }
        
    public VwUserGroupsSearchData setGroupConfigId(Long groupConfigId) {
        this.groupConfigId = groupConfigId;
        return this;
    }

    public String getField() {
        return field;
    }
        
    public VwUserGroupsSearchData setField(String field) {
        this.field = field;
        return this;
    }

    public String getFieldDisplayName() {
        return fieldDisplayName;
    }
        
    public VwUserGroupsSearchData setFieldDisplayName(String fieldDisplayName) {
        this.fieldDisplayName = fieldDisplayName;
        return this;
    }

    public String getGroupName() {
        return groupName;
    }
        
    public VwUserGroupsSearchData setGroupName(String groupName) {
        this.groupName = groupName;
        return this;
    }

    public String getGroupType() {
        return groupType;
    }
        
    public VwUserGroupsSearchData setGroupType(String groupType) {
        this.groupType = groupType;
        return this;
    }

    public String getVisibility() {
        return visibility;
    }
        
    public VwUserGroupsSearchData setVisibility(String visibility) {
        this.visibility = visibility;
        return this;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }
        
    public VwUserGroupsSearchData setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public Integer getPaxCount() {
        return paxCount;
    }
        
    public VwUserGroupsSearchData setPaxCount(Integer paxCount) {
        this.paxCount = paxCount;
        return this;
    }

    public Integer getGroupCount() {
        return groupCount;
    }
        
    public VwUserGroupsSearchData setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
        return this;
    }

    public Integer getTotalPaxCount() {
        return totalPaxCount;
    }
        
    public VwUserGroupsSearchData setTotalPaxCount(Integer totalPaxCount) {
        this.totalPaxCount = totalPaxCount;
        return this;
    }

    public Long getGroupPaxId() {
        return groupPaxId;
    }
        
    public VwUserGroupsSearchData setGroupPaxId(Long groupPaxId) {
        this.groupPaxId = groupPaxId;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public VwUserGroupsSearchData setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public VwUserGroupsSearchData setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
