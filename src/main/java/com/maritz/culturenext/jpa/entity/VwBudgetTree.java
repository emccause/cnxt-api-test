package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;


import org.apache.commons.lang3.builder.ToStringBuilder;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Table(name = "VW_BUDGET_TREE")
public class VwBudgetTree {

    @Column(name="BUDGET_ID")
    private Long budgetId;
    @Column(name="ROOT")
    private Long root;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public VwBudgetTree() {
    }
    
    public Long getBudgetId() {
        return budgetId;
    }
        
    public VwBudgetTree setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }

    public Long getRoot() {
        return root;
    }
        
    public VwBudgetTree setRoot(Long root) {
        this.root = root;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public VwBudgetTree setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public VwBudgetTree setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
