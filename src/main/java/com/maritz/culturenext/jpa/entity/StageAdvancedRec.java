package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;


import org.apache.commons.lang3.builder.ToStringBuilder;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Entity
@Table(name = "STAGE_ADVANCED_REC")
public class StageAdvancedRec {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", insertable = false, updatable = false)
    private Long id;
    @Column(name="BATCH_ID")
    private Long batchId;
    @Column(name="AWARD_AMOUNT")
    private Double awardAmount;
    @Column(name="RECIPIENT_FIRST_NAME")
    private String recipientFirstName;
    @Column(name="RECIPIENT_LAST_NAME")
    private String recipientLastName;
    @Column(name="RECIPIENT_ID")
    private String recipientId;
    @Column(name="PUBLIC_HEADLINE")
    private String publicHeadline;
    @Column(name="PRIVATE_MESSAGE")
    private String privateMessage;
    @Column(name="PROGRAM_NAME")
    private String programName;
    @Column(name="PROGRAM_ID")
    private Long programId;
    @Column(name="BUDGET_NAME")
    private String budgetName;
    @Column(name="BUDGET_ID")
    private Long budgetId;
    @Column(name="AWARD_TYPE")
    private String awardType;
    @Column(name="SUBMITTER_FIRST_NAME")
    private String submitterFirstName;
    @Column(name="SUBMITTER_LAST_NAME")
    private String submitterLastName;
    @Column(name="SUBMITTER_ID")
    private String submitterId;
    @Column(name="ECARD_ID")
    private Long ecardId;
    @Column(name="VALUE")
    private String value;
    @Column(name="VALUE_ID")
    private Long valueId;
    @Column(name="MAKE_RECOGNITION_PRIVATE")
    private String makeRecognitionPrivate;
    @Column(name="ERROR_MESSAGE")
    private String errorMessage;
    @Column(name="TEMP_NOMINATION_ID")
    private Long tempNominationId;
    @Column(name="ORIGINAL_AMOUNT")
    private Double originalAmount;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public StageAdvancedRec() {
    }
    
    public Long getId() {
        return id;
    }
        
    public StageAdvancedRec setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }
        
    public StageAdvancedRec setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    public Double getAwardAmount() {
        return awardAmount;
    }
        
    public StageAdvancedRec setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
        return this;
    }

    public String getRecipientFirstName() {
        return recipientFirstName;
    }
        
    public StageAdvancedRec setRecipientFirstName(String recipientFirstName) {
        this.recipientFirstName = recipientFirstName;
        return this;
    }

    public String getRecipientLastName() {
        return recipientLastName;
    }
        
    public StageAdvancedRec setRecipientLastName(String recipientLastName) {
        this.recipientLastName = recipientLastName;
        return this;
    }

    public String getRecipientId() {
        return recipientId;
    }
        
    public StageAdvancedRec setRecipientId(String recipientId) {
        this.recipientId = recipientId;
        return this;
    }

    public String getPublicHeadline() {
        return publicHeadline;
    }
        
    public StageAdvancedRec setPublicHeadline(String publicHeadline) {
        this.publicHeadline = publicHeadline;
        return this;
    }

    public String getPrivateMessage() {
        return privateMessage;
    }
        
    public StageAdvancedRec setPrivateMessage(String privateMessage) {
        this.privateMessage = privateMessage;
        return this;
    }

    public String getProgramName() {
        return programName;
    }
        
    public StageAdvancedRec setProgramName(String programName) {
        this.programName = programName;
        return this;
    }

    public Long getProgramId() {
        return programId;
    }
        
    public StageAdvancedRec setProgramId(Long programId) {
        this.programId = programId;
        return this;
    }

    public String getBudgetName() {
        return budgetName;
    }
        
    public StageAdvancedRec setBudgetName(String budgetName) {
        this.budgetName = budgetName;
        return this;
    }

    public Long getBudgetId() {
        return budgetId;
    }
        
    public StageAdvancedRec setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }

    public String getAwardType() {
        return awardType;
    }
        
    public StageAdvancedRec setAwardType(String awardType) {
        this.awardType = awardType;
        return this;
    }

    public String getSubmitterFirstName() {
        return submitterFirstName;
    }
        
    public StageAdvancedRec setSubmitterFirstName(String submitterFirstName) {
        this.submitterFirstName = submitterFirstName;
        return this;
    }

    public String getSubmitterLastName() {
        return submitterLastName;
    }
        
    public StageAdvancedRec setSubmitterLastName(String submitterLastName) {
        this.submitterLastName = submitterLastName;
        return this;
    }

    public String getSubmitterId() {
        return submitterId;
    }
        
    public StageAdvancedRec setSubmitterId(String submitterId) {
        this.submitterId = submitterId;
        return this;
    }

    public Long getEcardId() {
        return ecardId;
    }
        
    public StageAdvancedRec setEcardId(Long ecardId) {
        this.ecardId = ecardId;
        return this;
    }

    public String getValue() {
        return value;
    }
        
    public StageAdvancedRec setValue(String value) {
        this.value = value;
        return this;
    }

    public Long getValueId() {
        return valueId;
    }
        
    public StageAdvancedRec setValueId(Long valueId) {
        this.valueId = valueId;
        return this;
    }

    public String getMakeRecognitionPrivate() {
        return makeRecognitionPrivate;
    }
        
    public StageAdvancedRec setMakeRecognitionPrivate(String makeRecognitionPrivate) {
        this.makeRecognitionPrivate = makeRecognitionPrivate;
        return this;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
        
    public StageAdvancedRec setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public Long getTempNominationId() {
        return tempNominationId;
    }
        
    public StageAdvancedRec setTempNominationId(Long tempNominationId) {
        this.tempNominationId = tempNominationId;
        return this;
    }
    
    public Double getOriginalAmount() {
        return originalAmount;
    }

    public StageAdvancedRec setOriginalAmount(Double originalAmount) {
        this.originalAmount = originalAmount;
        return this;
    }
    public long getCrc() {
        return crc;
    }
    
    public StageAdvancedRec setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public StageAdvancedRec setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
