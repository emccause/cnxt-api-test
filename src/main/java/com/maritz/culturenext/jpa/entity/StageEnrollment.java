package com.maritz.culturenext.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.maritz.core.jdbc.annotation.ConcentrixDao;

@ConcentrixDao
@Entity
@Table(name = "STAGE_ENROLLMENT")
public class StageEnrollment {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", insertable = false, updatable = false)
    private Long id;
    @NotNull
    @Column(name="BATCH_ID")
    private Long batchId;
    @Column(name="PARTICIPANT_ID")
    private String participantId;
    @Column(name="STATUS")
    private String status;
    @Column(name="PRIMARY_REPORT_ID")
    private String primaryReportId;
    @Column(name="PRIMARY_REPORT_PAX_ID")
    private Long primaryReportPaxId;
    @Column(name="SECONDARY_REPORT_ID")
    private String secondaryReportId;
    @Column(name="SECONDARY_REPORT_PAX_ID")
    private Long secondaryReportPaxId;
    @Column(name="FIRST_NAME")
    private String firstName;
    @Column(name="LAST_NAME")
    private String lastName;
    @Column(name="MIDDLE_NAME")
    private String middleName;
    @Column(name="TITLE")
    private String title;
    @Column(name="COMPANY_NAME")
    private String companyName;
    @Column(name="LOGIN_ID")
    private String loginId;
    @Column(name="PASSWORD")
    private String password;
    @Column(name="EMAIL_ADDRESS")
    private String emailAddress;
    @Column(name="PHONE_NUMBER")
    private String phoneNumber;
    @Column(name="PHONE_EXTENSION")
    private String phoneExtension;
    @Column(name="ADDRESS_LINE_1")
    private String addressLine1;
    @Column(name="ADDRESS_LINE_2")
    private String addressLine2;
    @Column(name="ADDRESS_LINE_3")
    private String addressLine3;
    @Column(name="ADDRESS_LINE_4")
    private String addressLine4;
    @Column(name="ADDRESS_LINE_5")
    private String addressLine5;
    @Column(name="CITY")
    private String city;
    @Column(name="STATE_PROVINCE_REGION")
    private String stateProvinceRegion;
    @Column(name="ZIP_CODE")
    private String zipCode;
    @Column(name="COUNTRY_CODE")
    private String countryCode;
    @Column(name="HIRE_DATE")
    private String hireDate;
    @Column(name="TERMINATION_DATE")
    private String terminationDate;
    @Column(name="BIRTH_DATE")
    private String birthDate;
    @Column(name="DEPARTMENT")
    private String department;
    @Column(name="COST_CENTER")
    private String costCenter;
    @Column(name="AREA")
    private String area;
    @Column(name="GRADE")
    private String grade;
    @Column(name="CUSTOM_1")
    private String custom1;
    @Column(name="CUSTOM_2")
    private String custom2;
    @Column(name="CUSTOM_3")
    private String custom3;
    @Column(name="CUSTOM_4")
    private String custom4;
    @Column(name="CUSTOM_5")
    private String custom5;
    @Column(name="PAX_ID")
    private Long paxId;
    @Column(name="PAX_GROUP_ID")
    private Long paxGroupId;
    @Column(name="ADDRESS_CHANGE_STATUS")
    private String addressChangeStatus;
    @Column(name="EMAIL_CHANGE_STATUS")
    private String emailChangeStatus;
    @Column(name="PHONE_CHANGE_STATUS")
    private String phoneChangeStatus;
    @Column(name="ENROLL_ERROR")
    private String enrollError;
    @Column(name="SYS_USER_CHANGE_STATUS")
    private String sysUserChangeStatus;
    @Column(name="STATUS_TYPE_CODE")
    private String statusTypeCode;
    @Column(name="PAX_CHANGE_STATUS")
    private String paxChangeStatus;
    @Column(name="PAX_GROUP_CHANGE_STATUS")
    private String paxGroupChangeStatus;
    @Column(name="USER_FUNCTION")
    private String userFunction;
    @Column(name="LANGUAGE_PREFERENCE")
    private String languagePreference;
    @Column(name="FULL_BIRTH_DATE")
    private String fullBirthDate;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public StageEnrollment() {
    }
    
    public Long getId() {
        return id;
    }
        
    public StageEnrollment setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }
        
    public StageEnrollment setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    public String getParticipantId() {
        return participantId;
    }
        
    public StageEnrollment setParticipantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public String getStatus() {
        return status;
    }
        
    public StageEnrollment setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getPrimaryReportId() {
        return primaryReportId;
    }
        
    public StageEnrollment setPrimaryReportId(String primaryReportId) {
        this.primaryReportId = primaryReportId;
        return this;
    }

    public Long getPrimaryReportPaxId() {
        return primaryReportPaxId;
    }
        
    public StageEnrollment setPrimaryReportPaxId(Long primaryReportPaxId) {
        this.primaryReportPaxId = primaryReportPaxId;
        return this;
    }

    public String getSecondaryReportId() {
        return secondaryReportId;
    }
        
    public StageEnrollment setSecondaryReportId(String secondaryReportId) {
        this.secondaryReportId = secondaryReportId;
        return this;
    }

    public Long getSecondaryReportPaxId() {
        return secondaryReportPaxId;
    }
        
    public StageEnrollment setSecondaryReportPaxId(Long secondaryReportPaxId) {
        this.secondaryReportPaxId = secondaryReportPaxId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }
        
    public StageEnrollment setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }
        
    public StageEnrollment setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }
        
    public StageEnrollment setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getTitle() {
        return title;
    }
        
    public StageEnrollment setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }
        
    public StageEnrollment setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getLoginId() {
        return loginId;
    }
        
    public StageEnrollment setLoginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public String getPassword() {
        return password;
    }
        
    public StageEnrollment setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmailAddress() {
        return emailAddress;
    }
        
    public StageEnrollment setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
        
    public StageEnrollment setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getPhoneExtension() {
        return phoneExtension;
    }
        
    public StageEnrollment setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
        return this;
    }

    public String getAddressLine1() {
        return addressLine1;
    }
        
    public StageEnrollment setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    public String getAddressLine2() {
        return addressLine2;
    }
        
    public StageEnrollment setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
        return this;
    }

    public String getAddressLine3() {
        return addressLine3;
    }
        
    public StageEnrollment setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
        return this;
    }

    public String getAddressLine4() {
        return addressLine4;
    }
        
    public StageEnrollment setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
        return this;
    }

    public String getAddressLine5() {
        return addressLine5;
    }
        
    public StageEnrollment setAddressLine5(String addressLine5) {
        this.addressLine5 = addressLine5;
        return this;
    }

    public String getCity() {
        return city;
    }
        
    public StageEnrollment setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStateProvinceRegion() {
        return stateProvinceRegion;
    }
        
    public StageEnrollment setStateProvinceRegion(String stateProvinceRegion) {
        this.stateProvinceRegion = stateProvinceRegion;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }
        
    public StageEnrollment setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }
        
    public StageEnrollment setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getHireDate() {
        return hireDate;
    }
        
    public StageEnrollment setHireDate(String hireDate) {
        this.hireDate = hireDate;
        return this;
    }

    public String getTerminationDate() {
        return terminationDate;
    }
        
    public StageEnrollment setTerminationDate(String terminationDate) {
        this.terminationDate = terminationDate;
        return this;
    }

    public String getBirthDate() {
        return birthDate;
    }
        
    public StageEnrollment setBirthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public String getDepartment() {
        return department;
    }
        
    public StageEnrollment setDepartment(String department) {
        this.department = department;
        return this;
    }

    public String getCostCenter() {
        return costCenter;
    }
        
    public StageEnrollment setCostCenter(String costCenter) {
        this.costCenter = costCenter;
        return this;
    }

    public String getArea() {
        return area;
    }
        
    public StageEnrollment setArea(String area) {
        this.area = area;
        return this;
    }

    public String getGrade() {
        return grade;
    }
        
    public StageEnrollment setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getCustom1() {
        return custom1;
    }
        
    public StageEnrollment setCustom1(String custom1) {
        this.custom1 = custom1;
        return this;
    }

    public String getCustom2() {
        return custom2;
    }
        
    public StageEnrollment setCustom2(String custom2) {
        this.custom2 = custom2;
        return this;
    }

    public String getCustom3() {
        return custom3;
    }
        
    public StageEnrollment setCustom3(String custom3) {
        this.custom3 = custom3;
        return this;
    }

    public String getCustom4() {
        return custom4;
    }
        
    public StageEnrollment setCustom4(String custom4) {
        this.custom4 = custom4;
        return this;
    }

    public String getCustom5() {
        return custom5;
    }
        
    public StageEnrollment setCustom5(String custom5) {
        this.custom5 = custom5;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }
        
    public StageEnrollment setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public Long getPaxGroupId() {
        return paxGroupId;
    }
        
    public StageEnrollment setPaxGroupId(Long paxGroupId) {
        this.paxGroupId = paxGroupId;
        return this;
    }

    public String getAddressChangeStatus() {
        return addressChangeStatus;
    }
        
    public StageEnrollment setAddressChangeStatus(String addressChangeStatus) {
        this.addressChangeStatus = addressChangeStatus;
        return this;
    }

    public String getEmailChangeStatus() {
        return emailChangeStatus;
    }
        
    public StageEnrollment setEmailChangeStatus(String emailChangeStatus) {
        this.emailChangeStatus = emailChangeStatus;
        return this;
    }

    public String getPhoneChangeStatus() {
        return phoneChangeStatus;
    }
        
    public StageEnrollment setPhoneChangeStatus(String phoneChangeStatus) {
        this.phoneChangeStatus = phoneChangeStatus;
        return this;
    }

    public String getEnrollError() {
        return enrollError;
    }
        
    public StageEnrollment setEnrollError(String enrollError) {
        this.enrollError = enrollError;
        return this;
    }

    public String getSysUserChangeStatus() {
        return sysUserChangeStatus;
    }
        
    public StageEnrollment setSysUserChangeStatus(String sysUserChangeStatus) {
        this.sysUserChangeStatus = sysUserChangeStatus;
        return this;
    }

    public String getStatusTypeCode() {
        return statusTypeCode;
    }
        
    public StageEnrollment setStatusTypeCode(String statusTypeCode) {
        this.statusTypeCode = statusTypeCode;
        return this;
    }

    public String getPaxChangeStatus() {
        return paxChangeStatus;
    }
        
    public StageEnrollment setPaxChangeStatus(String paxChangeStatus) {
        this.paxChangeStatus = paxChangeStatus;
        return this;
    }

    public String getPaxGroupChangeStatus() {
        return paxGroupChangeStatus;
    }
        
    public StageEnrollment setPaxGroupChangeStatus(String paxGroupChangeStatus) {
        this.paxGroupChangeStatus = paxGroupChangeStatus;
        return this;
    }

    public String getUserFunction() {
        return userFunction;
    }
        
    public StageEnrollment setUserFunction(String userFunction) {
        this.userFunction = userFunction;
        return this;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }
        
    public StageEnrollment setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
        return this;
    }

    public String getFullBirthDate() {
        return fullBirthDate;
    }
        
    public StageEnrollment setFullBirthDate(String fullBirthDate) {
        this.fullBirthDate = fullBirthDate;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public StageEnrollment setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public StageEnrollment setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
