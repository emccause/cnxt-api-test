package com.maritz.culturenext.budget.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.budget.dto.BudgetUtilizationStatsDTO;
import com.maritz.culturenext.budget.service.BudgetReportService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/budgets", description = "Endpoints that deal with the budgets")
public class BudgetReportRestService {

    @Inject private BudgetReportService budgetReportService;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private Security security;

    public static final String BUDGET_MANAGER_DASHBOARD_PATH = "budgets/manager-dashboard";
    
    //rest/budgets/stats
    @RequestMapping(value = "budgets/stats", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the usage details of the budgets for the query details specified.",
            notes = "percentages can be calculated against the 'totalBudget'")
    @ApiResponses(
            value = { @ApiResponse(code = 200, message = "successfully retrieved the details of the budget by queryId",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getAllBudgetStatsByQueryId(
            @ApiParam(value = "The query id for the query you want the details of.") @RequestParam(value = QUERY_ID_REST_PARAM,
                    required = true) String queryId)
            throws Throwable {

        return budgetReportService.getBudgetStatsByQueryId(null, queryId);
    }

    //rest/budgets/~/stats
    @RequestMapping(value = "budgets/{budgetId}/stats", method = RequestMethod.GET)
    @ApiOperation(
            value = "Returns the usage details of the budgets for the query details specified for a specific budget")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved the details of the specific budget by queryId",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getBudgetStatsByQueryId(
            @ApiParam(
                    value = "The id for the budget you want the details of.") 
                    @PathVariable(BUDGET_ID_REST_PARAM) String budgetIdString,
            @ApiParam(value = "The query id for the query you want the details of.") @RequestParam(value = QUERY_ID_REST_PARAM,
                    required = true) String queryId)
            throws Throwable {

        Long budgetId = convertPathVariablesUtil.getId(budgetIdString);

        return budgetReportService.getBudgetStatsByQueryId(budgetId, queryId);
    }

    //rest/budgets/stats-by-giving-limit
    @RequestMapping(value = "budgets/stats-by-giving-limit", method = RequestMethod.GET)
    @ApiOperation(
            value = "Calculates budget stats based on the giving limits of the budgets based on the given query id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved the details of the specific budget by queryId",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getManagerBudgetStatsByQueryId(
            @ApiParam(value = "The query id for the query you want the details of.") @RequestParam(value = QUERY_ID_REST_PARAM,
                    required = true) String queryId)
            throws Throwable {

        return budgetReportService.getManagerBudgetStatsByQueryId(queryId);
    }

    //rest/budgets/stats-by-giving-limit/~
    @RequestMapping(value = "budgets/stats-by-giving-limit/{paxId}", method = RequestMethod.GET)
    @ApiOperation(
            value = "Calculates budget stats based on the giving limits of the budgets based on the given query id "
                    + "for a specific participant")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved the details of the specific budget by queryId",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getManagerBudgetStatsForPaxByQueryId(
            @ApiParam(
                    value = "The id for the participant you want the details of.") 
                    @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "The query id for the query you want the details of.") @RequestParam(value = QUERY_ID_REST_PARAM,
                    required = true) String queryId)
            throws Throwable {

        Long paxId = convertPathVariablesUtil.getId(paxIdString);

        return budgetReportService.getManagerBudgetStatsForPaxByQueryId(paxId, queryId);
    }
    
    //rest/budgets/budget-owner-dashboard
    @RequestMapping(value = "budgets/budget-owner-dashboard", method = RequestMethod.GET)
    @ApiOperation(
            value = "Calculates budget stats based on the giving limits of the budgets belong to the logged in participant.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved the details of the specific budget",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getBudgetOwnerStats(
            @ApiParam(value = "Id of the budget id that you want the details of.") 
                @RequestParam(value = BUDGET_ID_REST_PARAM,required = false) String budgetIdsList,
            @ApiParam(value = "Start date to search the budgets.") 
                @RequestParam(value = FROM_DATE_REST_PARAM,required = false) String fromDate,
            @ApiParam(value = "End date to search the budgets.") 
                @RequestParam(value = THRU_DATE_REST_PARAM,required = false) String thruDate)
            throws Throwable {

        Long paxId = security.getPaxId();
        
        return budgetReportService.getBudgetOwnerDashboard(paxId, budgetIdsList, fromDate, thruDate);
    }

    //rest/budgets/manager-dashboard
    @PreAuthorize("@security.hasRole('MGR')")
    @RequestMapping(value = BUDGET_MANAGER_DASHBOARD_PATH, method = RequestMethod.GET)
    @ApiOperation(
            value = "Calculates budget stats based on budget utilization data by pax for direct reports.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully retrieved the details of the specific budget",
                    response = BudgetUtilizationStatsDTO.class) })
    @Permission("PUBLIC")
    public BudgetUtilizationStatsDTO getManagerDashboard(
            @ApiParam(value = "List of pax IDs to get utilization data for.") 
                @RequestParam(value = PAX_IDS_REST_PARAM,required = true) String paxIdsList,
            @ApiParam(value = "Start date to search the budgets.") 
                @RequestParam(value = FROM_DATE_REST_PARAM,required = true) String fromDate,
            @ApiParam(value = "End date to search the budgets.") 
                @RequestParam(value = THRU_DATE_REST_PARAM,required = true) String thruDate)
            throws Throwable {
        
        return budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }
}