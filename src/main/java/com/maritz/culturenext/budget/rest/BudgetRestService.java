package com.maritz.culturenext.budget.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetRequestDTO;
import com.maritz.culturenext.budget.service.BudgetService;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;
import com.maritz.culturenext.constants.ProjectConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/budgets", description = "Endpoints that deal with creating, editing, and returning budgets")
public class BudgetRestService {
    
    @Inject private BudgetService budgetService;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private Security security;
    
    // Service endpoint paths
    public static final String BUDGET_PATH = "budgets";
    public static final String BUDGET_PAX_PATH = "budgets/pax";
    public static final String BUDGET_LIST_PATH = "budgets/list";
    public static final String BUDGET_SEARCH_PATH = "budgets/search";
    public static final String BUDGET_BY_ID_PATH = "budgets/{" + BUDGET_ID_REST_PARAM + "}";
    public static final String ELIGIBLE_BUDGET_INDIVIDUAL_USED_PATH = "participants/{" + PARTICIPANT_ID_REST_PARAM + "}/budgets";
    public static final String ELIGIBLE_BUDGETS_FOR_PROGRAM_PATH = "participants/{" + GIVER_PAX_REST_PARAM + "}/programs/{" + PROGRAM_ID_REST_PARAM + "}/budgets";

    //rest/budgets
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = BUDGET_PATH, method = RequestMethod.POST)
    @ApiOperation(
            value = "Creates a budget with the details provided for a client/project. "
                    + "If its a distributed budget, then the child-budgets are also created.",
            notes = "The API will generate a unique sequential 'Accounting Id' "
                    + "i.e. sub-project number for the budget that is 6 digits length.<br><br>"
                    + "individualUsedAmount is only populated for giverec scenario.<br><br>"
                    + "Status is optional for POST since budgets are created in ACTIVE status by default. "
                    + "It is required only for PUT.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully created or updated the budget",
            response = BudgetDTO.class) })
    @Permission("PUBLIC")
    public BudgetDTO createBudget(
            @ApiParam(value = "The Budget that you want to update or insert") @RequestBody BudgetRequestDTO budgetDto
        ) throws Throwable {

        return budgetService.createBudget(budgetDto);
    }

    //rest/budgets/~
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "," 
            + PermissionConstants.ALLOCATOR_ROLE_NAME + "')")
    @RequestMapping(value = BUDGET_BY_ID_PATH, method = RequestMethod.PUT)
    @ApiOperation(value = "Updates a budget with the details provided for a client/project.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully updated the given budget",
            response = BudgetDTO.class) })
    @Permission("PUBLIC")
    public BudgetDTO updateBudgetById(
            @ApiParam(value = "The id of the budget you want to update.")
                @PathVariable(BUDGET_ID_REST_PARAM) String budgetIdString,
            @RequestBody BudgetRequestDTO budgetDto
        )throws Throwable {
        return budgetService.updateBudgetById(convertPathVariablesUtil.getId(budgetIdString), budgetDto);
    }

    //rest/budgets/~
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = BUDGET_BY_ID_PATH, method = RequestMethod.DELETE)
    @ApiOperation(value = "Inactivate a budget with the id provided for a client/project.",
            notes = "Budgets can only be inactivated if the budget has not been used yet. Also cannot inactivate a budget that "
                    + "has been assigned to a program (validation error). If its a distributed budget, "
                    + "then the child-budgets are also inactivated if unused.")
    @Permission("PUBLIC")
    public void deleteBudgetById(
            @ApiParam(value = "The id of the budget you want to inactivate")
                @PathVariable(BUDGET_ID_REST_PARAM) String budgetIdString
        ) throws Throwable {
        budgetService.deleteBudgetById(convertPathVariablesUtil.getId(budgetIdString));
    }

    //rest/budgets/~
    @RequestMapping(value = BUDGET_BY_ID_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves a budget by the id provided for a given client/project.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully created or updated the budget",
            response = BudgetDTO.class) })
    @Permission("PUBLIC")
    public BudgetDTO getBudgetById(
            @ApiParam(value = "The id of the budget you want to retrieve") 
                @PathVariable(BUDGET_ID_REST_PARAM) String budgetIdString
        ) throws Throwable {
        return budgetService.getBudgetById(convertPathVariablesUtil.getId(budgetIdString));
    }

    //rest/budgets
    @RequestMapping(value = BUDGET_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Returns a list of budgets in a client/project.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned a list of budgets based on the given parameters.",
                    response = BudgetDTO.class) })
    @Permission("PUBLIC")
    public PaginatedResponseObject<BudgetDTO> getBudgetList(
            @ApiParam(value="Search string - budget name contains this string", required = false)
                @RequestParam(value = SEARCH_STRING_REST_PARAM, required = false) String searchStr,
            @ApiParam(value="Comma separated list of statuses you're looking for.", required = false)    
                @RequestParam(value = STATUS_REST_PARAM, required = false) String statusList,
            @ApiParam(value="Indicates if child budgets should be returned in the search", required = false)
                @RequestParam(value = INCLUDE_CHILDREN_REST_PARAM, required = false) Boolean includeChildren,
            @ApiParam(value="Indicates if individual giving limit should be returned in the search", required = false)
                @RequestParam(value = HAS_GIVING_LIMIT_REST_PARAM, required = false) Boolean hasGivingLimit,
            @ApiParam(value="Indicates if has amount should be returned in the search", required = false)
                @RequestParam(value = HAS_AMOUNT_REST_PARAM, required = false) Boolean hasAmount,
            @ApiParam(value="Comma separated list of budget owners you're looking for.", required = false)    
                @RequestParam(value = BUDGET_OWNER_REST_PARAM, required = false) String budgetOwnerList,
            @ApiParam(value="Comma separated list of budget allocators you're looking for.", required = false)    
                @RequestParam(value = ALLOCATOR_REST_PARAM, required = false) String budgetAllocatorList,
            @ApiParam(value="page number to fetch", required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
            @ApiParam(value="size of page to fetch", required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
            @ApiParam(value="type of the budget", required = false)
                @RequestParam(value = BUDGET_TYPE_REST_PARAM, required = false)  String budgetType
        ) throws Throwable {
        
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(SEARCH_STRING_REST_PARAM, searchStr);
        requestParamMap.put(STATUS_REST_PARAM, statusList);
        requestParamMap.put(INCLUDE_CHILDREN_REST_PARAM, includeChildren);
        requestParamMap.put(HAS_GIVING_LIMIT_REST_PARAM, hasGivingLimit);
        requestParamMap.put(HAS_AMOUNT_REST_PARAM, hasAmount);
        requestParamMap.put(BUDGET_OWNER_REST_PARAM, budgetOwnerList);
        requestParamMap.put(ALLOCATOR_REST_PARAM, budgetAllocatorList);
        requestParamMap.put(BUDGET_TYPE_REST_PARAM, budgetType);

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setRequestParamMap(requestParamMap)
                .setPageNumber(pageNumber).setPageSize(pageSize);

        return budgetService.getBudgetList(
                requestDetails, searchStr, statusList,includeChildren, hasGivingLimit, hasAmount, budgetOwnerList,
                budgetAllocatorList, pageNumber, pageSize, budgetType
            );
    }

    //rest/budgets/list
    @RequestMapping(value = BUDGET_LIST_PATH, method = RequestMethod.POST)
    @ApiOperation(value = "Returns a list of budgets in a client/project.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned a list of budgets based on the given parameters.",
                    response = BudgetDTO.class) })
    @Permission("PUBLIC")
    public PaginatedResponseObject<BudgetDTO> getBudgetListPost(
            @ApiParam(value="Search string - budget name contains this string", required = false)
            @RequestParam(value = SEARCH_STRING_REST_PARAM, required = false) String searchStr,
        @ApiParam(value="Comma separated list of statuses you're looking for.", required = false)    
            @RequestParam(value = STATUS_REST_PARAM, required = false) String statusList,
        @ApiParam(value="Indicates if child budgets should be returned in the search", required = false)
            @RequestParam(value = INCLUDE_CHILDREN_REST_PARAM, required = false) Boolean includeChildren,
        @ApiParam(value="Indicates if individual giving limit should be returned in the search", required = false)
            @RequestParam(value = HAS_GIVING_LIMIT_REST_PARAM, required = false) Boolean hasGivingLimit,
        @ApiParam(value="Indicates if has amount should be returned in the search", required = false)
            @RequestParam(value = HAS_AMOUNT_REST_PARAM, required = false) Boolean hasAmount,
        @ApiParam(value="Comma separated list of budget owners you're looking for.", required = false)    
            @RequestParam(value = BUDGET_OWNER_REST_PARAM, required = false) String budgetOwnerList,
        @ApiParam(value="Comma separated list of budget allocators you're looking for.", required = false)    
            @RequestParam(value = ALLOCATOR_REST_PARAM, required = false) String budgetAllocatorList,
        @ApiParam(value="page number to fetch", required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
        @ApiParam(value="size of page to fetch", required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
        @ApiParam(value="type of the budget", required = false)
            @RequestParam(value = BUDGET_TYPE_REST_PARAM, required = false)  String budgetType
        ) throws Throwable {
        return getBudgetList(searchStr, statusList, includeChildren, hasGivingLimit, hasAmount, budgetOwnerList,
                budgetAllocatorList, pageNumber, pageSize, budgetType
            );
    }

    //rest/participants/~/budgets
    @RequestMapping(value = ELIGIBLE_BUDGET_INDIVIDUAL_USED_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Return the remaining budget to a specific user based on the Budget limit and Budget used.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned the budget remaining for a specific user."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<BudgetDTO> getBudgetEligibility(
            @ApiParam(value = PARTICIPANT_ID_REST_PARAM) 
                @PathVariable(value = PARTICIPANT_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = GIVING_LIMIT_ONLY_REST_PARAM, required = false) 
                @RequestParam(value = GIVING_LIMIT_ONLY_REST_PARAM,required = false) Boolean givingLimitOnly)
            throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        // Set default giving limit to false in case it's null
        if (givingLimitOnly == null) {
            givingLimitOnly = false;
        }

        return budgetService.getIndividualEligibleBudgets(paxId, givingLimitOnly);
    }
    
    //rest/participants/~/programs/~/budgets
    @RequestMapping(value = ELIGIBLE_BUDGETS_FOR_PROGRAM_PATH, method = RequestMethod.GET)
    @ApiOperation(
            value = "Return a list of all budgets that the specific giver is able to use for the program "
                    + "specified in the uri path.")
    @ApiResponses(
            value = { @ApiResponse(code = 200, message = "successfully returned the list of budgets for the program")})
    @Permission("PUBLIC")
    public List<BudgetDTO> getBudgetEligibility(
            @ApiParam(value = "Id of the program you want to return budgets for") @PathVariable(PROGRAM_ID_REST_PARAM) Long programId,
            @ApiParam(value = "The Id of the specific user you want to see eligible budgets for",
                    required = false) @RequestParam(value = GIVER_PAX_REST_PARAM, required = false) String giverPax)
            throws Throwable {

        Long paxId = security.getPaxId(giverPax);
        if (paxId == null) {
            paxId = security.getPaxId();
        }

        return budgetService.getEligibleBudgetsForProgram(paxId, programId);
    }
}
