package com.maritz.culturenext.budget.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.constants.ProjectConstants;

public class BudgetConstants {
    
    public static final String BUDGET_TYPE = "DISC";
    public static final String BUDGET_FUNDING = "FUND";
    public static final String BUDGET_FUNDING_MESSAGE = "Budget funding";
    public static final String DEFAULT_BUDGET_CATEGORY_CODE = "DEFAULT";
    public static final String BUDGET_OWNERS_GROUP_NAME = "Budget Owners";
    public static final String BUDGET_ALLOCATORS_GROUP_NAME = "Budget Allocators";
    public static final String BUDGET_TYPE_CODE_ALLOCATED = "ALLOCATED";
    public static final String BUDGET_TYPE_CODE_DISTRIBUTED = "DISTRIBUTED";
    public static final List<String> ROLE_CODE_LIST = Arrays.asList(RoleCode.BUDGET_ALLOCATOR.name(), RoleCode.BUDGET_OWNER.name(), RoleCode.BUDGET_USER.name());
    public static final List<String> GROUP_NAME_LIST = Arrays.asList(BUDGET_ALLOCATORS_GROUP_NAME, BUDGET_OWNERS_GROUP_NAME);
    
    //Left-pad the budgetID with 0's until it is 8 characters. That will be used as the system-generated subproject number.
    public static final String SUB_PROJECT_AUTO_GENERATE_FORMAT = "%08d"; 

    public static final String ERROR_MUST_BE_ALLOCATOR_TO_EDIT = "MUST_BE_ALLOCATOR_TO_EDIT";
    public static final String ERROR_MUST_BE_ALLOCATOR_TO_EDIT_MSG = "You cannot edit this budget because you are not the allocator";
    public static final String ERROR_MISMATCHED_BUDGET_IDS = "MISMATCHED_BUDGET_IDS";
    public static final String ERROR_MISMATCHED_BUDGET_IDS_MSG = "budgetId on URI path does not match budgetId in request";
    public static final String ERROR_NULL_NAME = "MISSING_BUDGET_NAME";
    public static final String ERROR_NULL_NAME_MSG = "Please enter a Budget Name";
    public static final String ERROR_NULL_TYPE = "MISSING_TYPE";
    public static final String ERROR_NULL_TYPE_MSG = "Please enter a budget type";
    public static final String ERROR_NULL_DATE = "MISSING_START_DATE";
    public static final String ERROR_NULL_DATE_MSG = "Please select a Start Date";
    public static final String ERROR_NULL_CONSTRAINT = "MISSING_BUDGET_CONSTRAINT";
    public static final String ERROR_NULL_CONSTRAINT_MSG = "Please enter a Budget Constraint";
    public static final String ERROR_NULL_PROJECT = "MISSING_PROJECT_NUMBER";
    public static final String ERROR_NULL_PROJECT_MSG = "Please enter a project number";
    public static final String ERROR_INVALID_BUDGET_ID = "INVALID_BUDGET_ID";
    public static final String ERROR_INVALID_BUDGET_ID_MSG = "budgetId does not exist in database";
    public static final String ERROR_INVALID_PARENT_BUDGET_ID = "INVALID_PARENT_BUDGET_ID";
    public static final String ERROR_INVALID_PARENT_BUDGET_ID_MSG = "parentBudgetId does not exist in database";
    public static final String ERROR_AMOUNT_NEGATIVE = "NEGATIVE_BUDGET_AMOUNT";
    public static final String ERROR_AMOUNT_NEGATIVE_MSG = "More points being allocated out of the budget than in";
    public static final String ERROR_UNDER_USED_AMOUNT = "BUDGET_AMOUNT_UNDER_USED_AMOUNT";
    public static final String ERROR_UNDER_USED_AMOUNT_MSG = "Budget Total Amount cannot be lower than used Amount";
    public static final String ERROR_INVALID_TYPE = "INVALID_TYPE";
    public static final String ERROR_INVALID_TYPE_MSG = "%s is not a valid budget type";
    public static final String ERROR_ACCOUNTING_ID_LENGTH = "INVALID_ACCOUNTING_ID_LENGTH";
    public static final String ERROR_ACCOUNTING_ID_LENGTH_MSG = "Accounting ID must be 1-8 characters";
    public static final String ERROR_INVALID_ACCOUNTING_ID = "INVALID_ACCOUNTING_ID";
    public static final String ERROR_INVALID_ACCOUNTING_ID_MSG = "Accounting ID must only be alphanumeric characters";
    public static final String ERROR_NULL_OVERRIDE_ACCOUNTING_ID = "NULL_OVERRIDE_ACCOUNTING_ID";
    public static final String ERROR_NULL_OVERRIDE_ACCOUNTING_ID_MSG = "Override Accounting ID cannot be null if Override Project Number is provided";
    public static final String ERROR_OVERRIDE_ACCOUNTING_ID_LENGTH = "INVALID_OVERRIDE_ACCOUNTING_ID_LENGTH";
    public static final String ERROR_OVERRIDE_ACCOUNTING_ID_LENGTH_MSG = "Override Accounting ID must be 1-8 characters";
    public static final String ERROR_INVALID_OVERRIDE_ACCOUNTING_ID = "INVALID_OVERRIDE_ACCOUNTING_ID";
    public static final String ERROR_INVALID_OVERRIDE_ACCOUNTING_ID_MSG = "Override Accounting ID must only be alphanumeric characters";
    public static final String ERROR_INVALID_OVERRIDE_LOCATION = "INVALID_OVERRIDE_LOCATION";
    public static final String ERROR_INVALID_OVERRIDE_LOCATION_MSG = "Override Location must be US_OVERRIDE or NON_US_OVERRIDE";
    public static final String ERROR_NULL_OVERRIDE_LOCATION = "NULL_OVERRIDE_LOCATION";
    public static final String ERROR_NULL_OVERRIDE_LOCATION_MSG = "Override Location is null and must be US_OVERRIDE or NON_US_OVERRIDE";
    public static final String ERROR_LIMIT_NEGATIVE = "NEGATIVE_GIVING_LIMIT";
    public static final String ERROR_LIMIT_NEGATIVE_MSG = "Individual Giving Limit cannot be negative or zero";
    public static final String ERROR_BUDGET_LIMIT = "GIVING_LIMIT_EXCEEDS_BUDGET_AMOUNT";
    public static final String ERROR_BUDGET_LIMIT_MSG = "Giving Limit is over Budget Total Amount";
    public static final String ERROR_INVALID_FROM_DATE = "INVALID_FROM_DATE";
    public static final String ERROR_INVALID_FROM_DATE_MSG = "From date is not a valid format";
    public static final String ERROR_INVALID_THRU_DATE = "INVALID_THRU_DATE";
    public static final String ERROR_INVALID_THRU_DATE_MSG = "Thru date is not a valid format";
    public static final String ERROR_INVALID_DATE_RANGE = "INVALID_DATE_RANGE";
    public static final String ERROR_INVALID_DATE_RANGE_MSG = "From date must be before thru date";
    public static final String ERROR_INVALID_CONSTRAINT = "INVALID_BUDGET_CONSTRAINT";
    public static final String ERROR_INVALID_CONSTRAINT_MSG = "Budget Constraint must be either SOFT_BUDGET or HARD_BUDGET";
    public static final String ERROR_INVALID_GROUP = "INVALID_GROUP_ID";
    public static final String ERROR_INVALID_GROUP_MSG = "That GroupId either does not exist or is INACTIVE";
    public static final String ERROR_MISSING_ALLOCATED_IN = "MISSING_BUDGET_ALLOCATED_IN";
    public static final String ERROR_MISSING_ALLOCATED_IN_MSG = "Allocated budgets must have an allocatedIn value";
    public static final String ERROR_MISSING_BUDGET_USERS = "MISSING_BUDGET_USERS";
    public static final String ERROR_MISSING_BUDGET_USERS_MSG = "Please specify at least one budget user.";
    public static final String ERROR_MISSING_BUDGET_DETAILS = "MISSING_BUDGET_DETAILS";
    public static final String ERROR_MISSING_BUDGET_DETAILS_MSG = "budgetDetails section must be populated.";
    public static final String ERROR_INVALID_PAX_ID = "INVALID_PAX_ID";
    public static final String ERROR_INVALID_PAX_ID_MSG = "%d is not a valid pax id";
    public static final String ERROR_INACTIVE_PAX_ID = "INACTIVE_PAX_ID";
    public static final String ERROR_INACTIVE_PAX_ID_MSG = "%d is not ACTIVE in the system";
    public static final String ERROR_MISSING_BUDGET_ID = "MISSING_BUDGET_ID";
    public static final String ERROR_MISSING_BUDGET_ID_MSG = "Budget Id is required.";
    public static final String ERROR_BUDGET_USED = "BUDGET_IN_USE";
    public static final String ERROR_BUDGET_USED_MSG = "Cannot delete a budget that is either used or currently tied to a program.";
    public static final String ERROR_INVALID_PAX = "INVALID_PARTICIPANT_ID";
    public static final String ERROR_INVALID_PAX_MSG = "Please enter a valid ID for participant";
    public static final String ERROR_NOT_ADMIN = "ADMIN_ROLE_REQUIRED";
    public static final String ERROR_NOT_ADMIN_MSG = "You must be an administrator to pass in a participant id other than your own";
    public static final String ERROR_NULL_PROGRAM_ID = "MISSING_PROGRAM_ID";
    public static final String ERROR_NULL_PROGRAM_ID_MSG = "Please enter a programId";
    public static final String ERROR_INVALID_PROGRAM_ID = "INVALID_PROGRAM_ID";
    public static final String ERROR_INVALID_PROGRAM_ID_MSG = "Please enter a valid programId";
    public static final String ERROR_INACTIVE_PROGRAM = "PROGRAM_NOT_ACTIVE";
    public static final String ERROR_INACTIVE_PROGRAM_MSG = "That program is not currently active";
    private static final String ERROR_MISSING_QUERY_ID = "QUERYID_NOT_SUPPLIED";
    public static final String QUERY_ID_FIELD = "queryId";
    public static final String ERROR_MISSING_QUERY_ID_MSG = "QueryId is required";
    public static final String ERROR_MISSING_PAX_ID = "PAXID_NOT_SUPPLIED";
    public static final String ERROR_MISSING_PAX_ID_MSG = "PaxId is required";
    public static final String ERROR_NO_BUDGETS_FOR_PAX = "NO_BUDGETS_FOR_PAX";
    public static final String ERROR_NO_BUDGETS_FOR_PAX_MSG = 
            "Either there are no budgets for the specified paxId or that paxId is not your direct report";
    public static final String ERROR_MISSING_FROM_DATE = "MISSING_FROM_DATE";
    public static final String ERROR_MISSING_FROM_DATE_MSG = "From date is required";
    public static final String ERROR_MISSING_THRU_DATE = "MISSING_THRU_DATE";
    public static final String ERROR_MISSING_THRU_DATE_MSG = "Thru date is required";
    public static final String ERROR_BUDGET_ID_NON_DIGIT_MSG = "Budget id must be a digit or a comma separated list of digits";
        
    public static final ErrorMessage MUST_BE_ALLOCATOR_TO_EDIT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MUST_BE_ALLOCATOR_TO_EDIT)
        .setField(ProjectConstants.BUDGET_ALLOCATOR)
        .setMessage(ERROR_MUST_BE_ALLOCATOR_TO_EDIT_MSG);
    
    public static final ErrorMessage MISMATCHED_BUDGET_IDS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISMATCHED_BUDGET_IDS)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(ERROR_MISMATCHED_BUDGET_IDS_MSG);
    
    public static final ErrorMessage NULL_NAME_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_NAME)
        .setField(ProjectConstants.NAME)
        .setMessage(ERROR_NULL_NAME_MSG);
    
    public static final ErrorMessage NULL_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ERROR_NULL_TYPE_MSG);

    public static final ErrorMessage NULL_DATE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_DATE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ERROR_NULL_DATE_MSG);
    
    public static final ErrorMessage NULL_CONSTRAINT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_CONSTRAINT)
        .setField(ProjectConstants.BUDGET_CONSTRAINT)
        .setMessage(ERROR_NULL_CONSTRAINT_MSG);
    
    public static final ErrorMessage NULL_PROJECT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_PROJECT)
        .setField(ProjectConstants.PROJECT_NUMBER)
        .setMessage(ERROR_NULL_PROJECT_MSG);
    
    public static final ErrorMessage INVALID_BUDGET_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_BUDGET_ID)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(ERROR_INVALID_BUDGET_ID_MSG);
    
    public static final ErrorMessage INVALID_PARENT_BUDGET_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_PARENT_BUDGET_ID)
        .setField(ProjectConstants.PARENT_BUDGET_ID)
        .setMessage(ERROR_INVALID_PARENT_BUDGET_ID_MSG);
        
    public static final ErrorMessage TOTAL_AMOUNT_NEGATIVE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_AMOUNT_NEGATIVE)
        .setField(ProjectConstants.TOTAL_AMOUNT)
        .setMessage(ERROR_AMOUNT_NEGATIVE_MSG);
    
    public static final ErrorMessage TOTAL_AMOUNT_UNDER_USED_AMOUNT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_UNDER_USED_AMOUNT)
        .setField(ProjectConstants.TOTAL_AMOUNT)
        .setMessage(ERROR_UNDER_USED_AMOUNT_MSG);
    
    public static final ErrorMessage INVALID_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ERROR_INVALID_TYPE_MSG);
    
    public static final ErrorMessage INVALID_ACCOUNTING_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ACCOUNTING_ID_LENGTH)
        .setField(ProjectConstants.ACCOUNTING_ID)
        .setMessage(ERROR_ACCOUNTING_ID_LENGTH_MSG);
    
    public static final ErrorMessage ACCOUNTING_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_ACCOUNTING_ID)
        .setField(ProjectConstants.ACCOUNTING_ID)
        .setMessage(ERROR_INVALID_ACCOUNTING_ID_MSG);
    
    public static final ErrorMessage INVALID_OVERRIDE_ACCOUNTING_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_OVERRIDE_ACCOUNTING_ID_LENGTH)
        .setField(ProjectConstants.OVERRIDE_ACCOUNTING_ID)
        .setMessage(ERROR_OVERRIDE_ACCOUNTING_ID_LENGTH_MSG);

    public static final ErrorMessage NULL_OVERRIDE_ACCOUNTING_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_OVERRIDE_ACCOUNTING_ID)
        .setField(ProjectConstants.OVERRIDE_ACCOUNTING_ID)
        .setMessage(ERROR_NULL_OVERRIDE_ACCOUNTING_ID_MSG);
    
    public static final ErrorMessage OVERRIDE_ACCOUNTING_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_OVERRIDE_ACCOUNTING_ID)
        .setField(ProjectConstants.OVERRIDE_ACCOUNTING_ID)
        .setMessage(ERROR_INVALID_OVERRIDE_ACCOUNTING_ID_MSG);

    public static final ErrorMessage INVALID_OVERRIDE_LOCATION_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_OVERRIDE_LOCATION)
        .setField(ProjectConstants.OVERRIDE_LOCATION)
        .setMessage(ERROR_INVALID_OVERRIDE_LOCATION_MSG);

    public static final ErrorMessage NULL_OVERRIDE_LOCATION_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_OVERRIDE_LOCATION)
        .setField(ProjectConstants.OVERRIDE_LOCATION)
        .setMessage(ERROR_NULL_OVERRIDE_LOCATION_MSG);

    public static final ErrorMessage GIVING_LIMIT_NEGATIVE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_LIMIT_NEGATIVE)
        .setField(ProjectConstants.INDIVIDUAL_GIVING_LIMIT)
        .setMessage(ERROR_LIMIT_NEGATIVE_MSG);
    
    public static final ErrorMessage GIVING_LIMIT_OVER_TOTAL_AMOUNT_MESSAGE = new ErrorMessage() 
        .setCode(ERROR_BUDGET_LIMIT)
        .setField(ProjectConstants.INDIVIDUAL_GIVING_LIMIT)
        .setMessage(ERROR_BUDGET_LIMIT_MSG);
    
    public static final ErrorMessage INVALID_FROM_DATE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_FROM_DATE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ERROR_INVALID_FROM_DATE_MSG);
    
    public static final ErrorMessage INVALID_THRU_DATE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_THRU_DATE)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ERROR_INVALID_THRU_DATE_MSG);
    
    public static final ErrorMessage INVALID_DATE_RANGE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_DATE_RANGE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ERROR_INVALID_DATE_RANGE_MSG);
    
    public static final ErrorMessage INVALID_CONSTRAINT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_CONSTRAINT)
        .setField(ProjectConstants.BUDGET_CONSTRAINT)
        .setMessage(ERROR_INVALID_CONSTRAINT_MSG);
    
    public static final ErrorMessage INVALID_GROUP_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_GROUP)
        .setField(ProjectConstants.GROUP_ID)
        .setMessage(ERROR_INVALID_GROUP_MSG);
    
    public static final ErrorMessage MISSING_BUDGET_ALLOCATED_IN_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_ALLOCATED_IN)
        .setField(ProjectConstants.ALLOCATED_IN)
        .setMessage(ERROR_MISSING_ALLOCATED_IN_MSG);

    public static final ErrorMessage MISSING_BUDGET_USERS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_BUDGET_USERS)
        .setField(ProjectConstants.BUDGET_USERS)
        .setMessage(ERROR_MISSING_BUDGET_USERS_MSG);
    
    public static final ErrorMessage MISSING_BUDGET_DETAILS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_BUDGET_DETAILS)
        .setField(ProjectConstants.BUDGET_DETAILS)
        .setMessage(ERROR_MISSING_BUDGET_DETAILS_MSG);
    
    public static final ErrorMessage INVALID_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_PAX_ID)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_INVALID_PAX_ID_MSG);
    
    public static final ErrorMessage INACTIVE_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INACTIVE_PAX_ID)    
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_INACTIVE_PAX_ID_MSG);
    
    public static final ErrorMessage MISSING_BUDGET_ID_ERROR = new ErrorMessage()
        .setCode(BudgetConstants.ERROR_MISSING_BUDGET_ID)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(BudgetConstants.ERROR_MISSING_BUDGET_ID_MSG);
    
    public static final ErrorMessage BUDGET_USED_MESSAGE = new ErrorMessage()
        .setCode(BudgetConstants.ERROR_BUDGET_USED)
        .setField(ProjectConstants.IS_USED)
        .setMessage(BudgetConstants.ERROR_BUDGET_USED_MSG);

    public static final ErrorMessage INVALID_PAX_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_PAX)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_INVALID_PAX_MSG);
    
    public static final ErrorMessage NOT_ADMIN_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NOT_ADMIN)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_NOT_ADMIN_MSG);
    
    public static final ErrorMessage NULL_PROGRAM_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ERROR_NULL_PROGRAM_ID_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ERROR_INVALID_PROGRAM_ID_MSG);
    
    public static final ErrorMessage INACTIVE_PROGRAM_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INACTIVE_PROGRAM)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ERROR_INACTIVE_PROGRAM_MSG);

    public static final ErrorMessage MISSING_QUERY_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_QUERY_ID)
            .setField(QUERY_ID_FIELD)
            .setMessage(ERROR_MISSING_QUERY_ID_MSG);

    public static final ErrorMessage MISSING_PAX_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_PAX_ID)
            .setField(ProjectConstants.PAX_ID)
            .setMessage(ERROR_MISSING_PAX_ID_MSG);

    public static final ErrorMessage NO_BUDGETS_FOR_PAX_MESSAGE = new ErrorMessage()
            .setCode(ERROR_NO_BUDGETS_FOR_PAX)
            .setField(ProjectConstants.PAX_ID)
            .setMessage(ERROR_NO_BUDGETS_FOR_PAX_MSG);

    public static final ErrorMessage MISSING_FROM_DATE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_FROM_DATE)
            .setField(ProjectConstants.FROM_DATE)
            .setMessage(ERROR_MISSING_FROM_DATE_MSG);

    public static final ErrorMessage MISSING_THRU_DATE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_THRU_DATE)
            .setField(ProjectConstants.THRU_DATE)
            .setMessage(ERROR_MISSING_THRU_DATE_MSG);

    public static final ErrorMessage BUDGET_ID_NON_DIGIT_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_BUDGET_ID)
            .setField(ProjectConstants.BUDGET_ID)
            .setMessage(ERROR_BUDGET_ID_NON_DIGIT_MSG);

}
