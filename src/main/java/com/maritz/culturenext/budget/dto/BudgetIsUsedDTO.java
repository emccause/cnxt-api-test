package com.maritz.culturenext.budget.dto;

public class BudgetIsUsedDTO {
    private Long budgetId;
    private boolean isUsed;

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public BudgetIsUsedDTO(Long budgetId, boolean isUsed) {
        this.budgetId = budgetId;
        this.isUsed = isUsed;
    }
}
