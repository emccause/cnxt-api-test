package com.maritz.culturenext.budget.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

// Use BudgetDTO for responses
public class BudgetRequestDTO {
    
    private Long budgetId;
    private Long parentBudgetId;
    private String name;
    private String displayName;
    private String description;
    private Double totalAmount;
    private Double usedAmount;
    private String type;
    private String accountingId;
    private Double individualGivingLimit;
    private Double individualUsedAmount;
    private Boolean usedInProgram;
    private String status;
    private String fromDate;
    private String thruDate;
    private String budgetConstraint;
    private String projectNumber;
    private Boolean isUsable;
    private EmployeeDTO budgetOwner;
    private BudgetDetailsRequestDTO budgetDetails;
    private List<BudgetRequestDTO> childBudgets;
    private Long proxyPaxId;
    private String overrideProjectNumber;
    private String overrideAccountingId;
    private String overrideLocation;
    
    public Long getBudgetId() {
        return budgetId;
    }
    
    public BudgetRequestDTO setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }
    
    public Long getParentBudgetId() {
        return parentBudgetId;
    }
    
    public BudgetRequestDTO setParentBudgetId(Long parentBudgetId) {
        this.parentBudgetId = parentBudgetId;
        return this;
    }
    
    public String getName() {
        return name;
    }
    
    public BudgetRequestDTO setName(String name) {
        this.name = name;
        return this;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public BudgetRequestDTO setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public BudgetRequestDTO setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public Double getTotalAmount() {
        return totalAmount;
    }
    
    public BudgetRequestDTO setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }
    
    public Double getUsedAmount() {
        return usedAmount;
    }
    
    public BudgetRequestDTO setUsedAmount(Double usedAmount) {
        this.usedAmount = usedAmount;
        return this;
    }
    
    public String getType() {
        return type;
    }
    
    public BudgetRequestDTO setType(String type) {
        this.type = type;
        return this;
    }
    
    public String getAccountingId() {
        return accountingId;
    }
    
    public BudgetRequestDTO setAccountingId(String accountingId) {
        this.accountingId = accountingId;
        return this;
    }
    
    public Double getIndividualGivingLimit() {
        return individualGivingLimit;
    }
    
    public BudgetRequestDTO setIndividualGivingLimit(Double individualGivingLimit) {
        this.individualGivingLimit = individualGivingLimit;
        return this;
    }
    
    public Double getIndividualUsedAmount() {
        return individualUsedAmount;
    }
    
    public BudgetRequestDTO setIndividualUsedAmount(Double individualUsedAmount) {
        this.individualUsedAmount = individualUsedAmount;
        return this;
    }
    
    public Boolean getUsedInProgram() {
        return usedInProgram;
    }
    
    public BudgetRequestDTO setUsedInProgram(Boolean usedInProgram) {
        this.usedInProgram = usedInProgram;
        return this;
    }
        
    public String getStatus() {
        return status;
    }
    
    public BudgetRequestDTO setStatus(String status) {
        this.status = status;
        return this;
    }
    
    public String getFromDate() {
        return fromDate;
    }
    
    public BudgetRequestDTO setFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }
    
    public String getThruDate() {
        return thruDate;
    }
    
    public BudgetRequestDTO setThruDate(String thruDate) {
        this.thruDate = thruDate;
        return this;
    }
    
    public String getBudgetConstraint() {
        return budgetConstraint;
    }
    
    public BudgetRequestDTO setBudgetConstraint(String budgetConstraint) {
        this.budgetConstraint = budgetConstraint;
        return this;
    }
    
    public String getProjectNumber() {
        return projectNumber;
    }
    
    public BudgetRequestDTO setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
        return this;
    }
    
    public Boolean getIsUsable() {
        return isUsable;
    }

    public BudgetRequestDTO setIsUsable(Boolean isUsable) {
        this.isUsable = isUsable;
        return this;
    }

    public EmployeeDTO getBudgetOwner() {
        return budgetOwner;
    }
    
    public BudgetRequestDTO setBudgetOwner(EmployeeDTO budgetOwner) {
        this.budgetOwner = budgetOwner;
        return this;
    }
    
    public BudgetDetailsRequestDTO getBudgetDetails() {
        return budgetDetails;
    }
    
    public BudgetRequestDTO setBudgetDetails(BudgetDetailsRequestDTO budgetDetails) {
        this.budgetDetails = budgetDetails;
        return this;
    }
    
    public List<BudgetRequestDTO> getChildBudgets() {
        return childBudgets;
    }
    
    public BudgetRequestDTO setChildBudgets(List<BudgetRequestDTO> childBudgets) {
        this.childBudgets = childBudgets;
        return this;
    }

    public Long getProxyPaxId() {
        return proxyPaxId;
    }

    public void setProxyPaxId(Long proxyPaxId) {
        this.proxyPaxId = proxyPaxId;
    }

    public String getOverrideProjectNumber() {
        return overrideProjectNumber;
    }

    public BudgetRequestDTO setOverrideProjectNumber(String overrideProjectNumber) {
        this.overrideProjectNumber = overrideProjectNumber;
        return this;
    }

    public String getOverrideAccountingId() {
        return overrideAccountingId;
    }

    public BudgetRequestDTO setOverrideAccountingId(String overrideAccountingId) {
        this.overrideAccountingId = overrideAccountingId;
        return this;
    }

    public String getOverrideLocation() {
        return overrideLocation;
    }

    public BudgetRequestDTO setOverrideLocation(String overrideLocation) {
        this.overrideLocation = overrideLocation;
        return this;
    }
}
