package com.maritz.culturenext.budget.dto;

import java.util.List;

public class BudgetAssignmentStubDTO {

    private BudgetEntityStubDTO budget;
    private List<BudgetEntityStubDTO> givingMembers;
    
    public BudgetEntityStubDTO getBudget() {
        return budget;
    }
    public void setBudget(BudgetEntityStubDTO budget) {
        this.budget = budget;
    }
    public List<BudgetEntityStubDTO> getGivingMembers() {
        return givingMembers;
    }
    public void setGivingMembers(List<BudgetEntityStubDTO> givingMembers) {
        this.givingMembers = givingMembers;
    }
    
    
}
