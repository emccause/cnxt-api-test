package com.maritz.culturenext.budget.dto;

import java.util.Collection;
import java.util.List;

import com.maritz.core.jpa.entity.Budget;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

public class RecursiveBudgetSaveDTO {
    
    private Budget budget;
    private Collection<RecursiveBudgetSaveDTO> children;
    private String subProjectNumber;
    private Double allocatedIn;
    private Double allocatedOut;
    private Long ownerPaxId;
    private List<GroupMemberStubDTO> budgetUserList;
    private Boolean canAllocate;
    private GroupMemberStubDTO parentEntity;
    private Boolean isUsable;
    private Long proxyPaxId;
    
    public Budget getBudget() {
        return budget;
    }

    public RecursiveBudgetSaveDTO setBudget(Budget budget) {
        this.budget = budget;
        return this;
    }
    
    public Collection<RecursiveBudgetSaveDTO> getChildren() {
        return children;
    }

    public RecursiveBudgetSaveDTO setChildren(Collection<RecursiveBudgetSaveDTO> children) {
        this.children = children;
        return this;
    }

    public String getSubProjectNumber() {
        return subProjectNumber;
    }

    public RecursiveBudgetSaveDTO setSubProjectNumber(String subProjectNumber) {
        this.subProjectNumber = subProjectNumber;
        return this;
    }

    public Double getAllocatedIn() {
        return allocatedIn;
    }

    public RecursiveBudgetSaveDTO setAllocatedIn(Double allocatedIn) {
        this.allocatedIn = allocatedIn;
        return this;
    }

    public Double getAllocatedOut() {
        return allocatedOut;
    }

    public RecursiveBudgetSaveDTO setAllocatedOut(Double allocatedOut) {
        this.allocatedOut = allocatedOut;
        return this;
    }

    public Long getOwnerPaxId() {
        return ownerPaxId;
    }

    public RecursiveBudgetSaveDTO setOwnerPaxId(Long ownerPaxId) {
        this.ownerPaxId = ownerPaxId;
        return this;
    }

    public List<GroupMemberStubDTO> getBudgetUserList() {
        return budgetUserList;
    }

    public RecursiveBudgetSaveDTO setBudgetUserList(List<GroupMemberStubDTO> budgetUserList) {
        this.budgetUserList = budgetUserList;
        return this;
    }

    public Boolean getCanAllocate() {
        return canAllocate;
    }

    public RecursiveBudgetSaveDTO setCanAllocate(Boolean canAllocate) {
        this.canAllocate = canAllocate;
        return this;
    }

    public GroupMemberStubDTO getParentEntity() {
        return parentEntity;
    }

    public RecursiveBudgetSaveDTO setParentEntity(GroupMemberStubDTO parentEntity) {
        this.parentEntity = parentEntity;
        return this;
    }

    public Boolean getIsUsable() {
        return isUsable;
    }

    public RecursiveBudgetSaveDTO setIsUsable(Boolean isUsable) {
        this.isUsable = isUsable;
        return this;
    }
    
    public Long getProxyPaxId() {
        return proxyPaxId;
    }

    public void setProxyPaxId(Long proxyPaxId) {
        this.proxyPaxId = proxyPaxId;
    }
}
