package com.maritz.culturenext.budget.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.EntityDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BudgetEntityStubDTO extends EntityDTO {
    
    private Long budgetId;
    private Long groupId;
    private Long paxId;

    public Long getGroupId() {
        return groupId;
    }

    public BudgetEntityStubDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }

    public BudgetEntityStubDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }        

}
