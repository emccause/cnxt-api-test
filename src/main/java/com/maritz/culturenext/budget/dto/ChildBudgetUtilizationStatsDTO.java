package com.maritz.culturenext.budget.dto;

public class ChildBudgetUtilizationStatsDTO extends BudgetUtilizationStatsDTO {
    
    Double budgetAllocated;
    Double childBudgetAvailable;
    Double childBudgetExpired;
    Double childBudgetUsed;
    
    public Double getBudgetAllocated() {
        return budgetAllocated;
    }
    public void setBudgetAllocated(Double budgetAllocated) {
        this.budgetAllocated = budgetAllocated;
    }
    public Double getChildBudgetAvailable() {
        return childBudgetAvailable;
    }
    public void setChildBudgetAvailable(Double childBudgetAvailable) {
        this.childBudgetAvailable = childBudgetAvailable;
    }
    public Double getChildBudgetExpired() {
        return childBudgetExpired;
    }
    public void setChildBudgetExpired(Double childBudgetExpired) {
        this.childBudgetExpired = childBudgetExpired;
    }
    public Double getChildBudgetUsed() {
        return childBudgetUsed;
    }
    public void setChildBudgetUsed(Double childBudgetUsed) {
        this.childBudgetUsed = childBudgetUsed;
    }    
}
