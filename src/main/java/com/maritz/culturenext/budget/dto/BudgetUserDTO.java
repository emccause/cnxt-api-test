package com.maritz.culturenext.budget.dto;

public class BudgetUserDTO {
    private Long budgetId;
    private String budgetUserType;
    private Long budgetUserId;

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public String getBudgetUserType() {
        return budgetUserType;
    }

    public void setBudgetUserType(String budgetUserType) {
        this.budgetUserType = budgetUserType;
    }

    public Long getBudgetUserId() {
        return budgetUserId;
    }

    public void setBudgetUserId(Long budgetUserId) {
        this.budgetUserId = budgetUserId;
    }

    public BudgetUserDTO(Long budgetId, String budgetUserType, Long budgetUserId) {
        this.budgetId = budgetId;
        this.budgetUserType = budgetUserType;
        this.budgetUserId = budgetUserId;
    }
}
