package com.maritz.culturenext.budget.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.EntityDTO;

public class BudgetDetailsDTO {

    private List<EntityDTO> budgetUsers;
    private Boolean canAllocate;
    private Double allocatedIn;
    private Double allocatedOut;

    public List<EntityDTO> getBudgetUsers() {
        return budgetUsers;
    }

    public BudgetDetailsDTO setBudgetUsers(List<EntityDTO> budgetUsers) {
        this.budgetUsers = budgetUsers;
        return this;
    }
    
    public Boolean getCanAllocate() {
        return canAllocate;
    }
    public BudgetDetailsDTO setCanAllocate(Boolean canAllocate) {
        this.canAllocate = canAllocate;
        return this;
    }
    
    public Double getAllocatedIn() {
        return allocatedIn;
    }
    
    public BudgetDetailsDTO setAllocatedIn(Double allocatedIn) {
        this.allocatedIn = allocatedIn;
        return this;
    }
    
    public Double getAllocatedOut() {
        return allocatedOut;
    }
    
    public BudgetDetailsDTO setAllocatedOut(Double allocatedOut) {
        this.allocatedOut = allocatedOut;
        return this;
    }
}
