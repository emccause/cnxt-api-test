package com.maritz.culturenext.budget.dto;

import java.util.ArrayList;
import java.util.List;

import com.maritz.culturenext.profile.dto.EntityDTO;

public class BudgetAssignmentDTO {

    private BudgetAssignmentBudgetDTO budget = new BudgetAssignmentBudgetDTO();
    private List<EntityDTO> givingMembers = new ArrayList<EntityDTO>();
    
    public BudgetAssignmentBudgetDTO getBudget() {
        return budget;
    }
    public void setBudget(BudgetAssignmentBudgetDTO budget) {
        this.budget = budget;
    }
    public List<EntityDTO> getGivingMembers() {
        return givingMembers;
    }
    public void setGivingMembers(List<EntityDTO> givingMembers) {
        this.givingMembers = givingMembers;
    }
    
    
}
