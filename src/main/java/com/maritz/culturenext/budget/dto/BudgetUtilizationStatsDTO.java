package com.maritz.culturenext.budget.dto;

public class BudgetUtilizationStatsDTO {
    
    Double budgetUsed;
    Double totalBudget;
    Double budgetAvailable;
    Double budgetExpired;
    
    public Double getBudgetUsed() {
        return budgetUsed;
    }
    public void setBudgetUsed(Double budgetUsed) {
        this.budgetUsed = budgetUsed;
    }
    public Double getTotalBudget() {
        return totalBudget;
    }
    public void setTotalBudget(Double totalBudget) {
        this.totalBudget = totalBudget;
    }
    public Double getBudgetAvailable() {
        return budgetAvailable;
    }
    public void setBudgetAvailable(Double budgetAvailable) {
        this.budgetAvailable = budgetAvailable;
    }
    public Double getBudgetExpired() {
        return budgetExpired;
    }
    public void setBudgetExpired(Double budgetExpired) {
        this.budgetExpired = budgetExpired;
    }
}
