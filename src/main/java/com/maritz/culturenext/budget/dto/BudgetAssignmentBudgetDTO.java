package com.maritz.culturenext.budget.dto;

public class BudgetAssignmentBudgetDTO {

    private Long budgetId;
    private String name;
    private String displayName;
    private Double totalAmount;
    private Double usedAmount;
    private String type;
    private Double individualGivingLimit;
    
    public Long getBudgetId() {
        return budgetId;
    }
    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public Double getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }
    public Double getUsedAmount() {
        return usedAmount;
    }
    public void setUsedAmount(Double usedAmount) {
        this.usedAmount = usedAmount;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Double getIndividualGivingLimit() {
        return individualGivingLimit;
    }
    public void setIndividualGivingLimit(Double individualGivingLimit) {
        this.individualGivingLimit = individualGivingLimit;
    }
    
    
}
