package com.maritz.culturenext.budget.dto;

import java.util.List;

public class AwardTierDTO implements Comparable<AwardTierDTO> {
    private Long awardTierId;
    private Long awardAmount;
    private Boolean allowRaising;
    private List<ApprovalDTO> approvals;
    private String type;
    private Long awardMin;
    private Long awardMax;
    private Boolean routeNextLevelManager; 
    private String status;
    private Boolean approvalReminderEmail;

    public Boolean getApprovalReminderEmail() { return approvalReminderEmail; }

    public void setApprovalReminderEmail(Boolean approvalReminderEmail) {
        this.approvalReminderEmail = approvalReminderEmail;
    }

    public Long getAwardAmount() {
        return awardAmount;
    }

    public void setAwardAmount(Long awardAmount) {
        this.awardAmount = awardAmount;
    }

    public Boolean getAllowRaising() {
        return allowRaising;
    }

    public void setAllowRaising(Boolean allowRaising) {
        this.allowRaising = allowRaising;
    }

    public List<ApprovalDTO> getApprovals() {
        return approvals;
    }

    public void setApprovals(List<ApprovalDTO> approvals) {
        this.approvals = approvals;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getAwardMin() {
        return awardMin;
    }

    public void setAwardMin(Long awardMin) {
        this.awardMin = awardMin;
    }

    public Long getAwardMax() {
        return awardMax;
    }

    public void setAwardMax(Long awardMax) {
        this.awardMax = awardMax;
    }

    public Boolean getRouteNextLevelManager() {
		return routeNextLevelManager;
	}

	public void setRouteNextLevelManager(Boolean routeNextLevelManager) {
		this.routeNextLevelManager = routeNextLevelManager;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        AwardTierDTO otherAwardTierDTO = (AwardTierDTO) obj;
        if ((awardAmount != null && otherAwardTierDTO.awardAmount == null)
                || (awardAmount == null && otherAwardTierDTO.awardAmount != null)) {
            return false;
        } else if ((awardAmount != null && otherAwardTierDTO.awardAmount != null)
                && !awardAmount.equals(otherAwardTierDTO.awardAmount)) {
            return false;
        }

        if ((awardMin != null && otherAwardTierDTO.awardMin == null)
                || (awardMin == null && otherAwardTierDTO.awardMin != null)) {
            return false;
        } else if ((awardMin != null && otherAwardTierDTO.awardMin != null)
                && !awardMin.equals(otherAwardTierDTO.awardMin)) {
            return false;
        }

        if ((awardMax != null && otherAwardTierDTO.awardMax == null)
                || (awardMax == null && otherAwardTierDTO.awardMax != null)) {
            return false;
        } else if ((awardMax != null && otherAwardTierDTO.awardMax != null)
                && !awardMax.equals(otherAwardTierDTO.awardMax)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((awardAmount == null) ? 0 : awardAmount.hashCode());
        return result;
    }

    @Override
    public int compareTo(AwardTierDTO awardTier) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (this == awardTier)
            return EQUAL;

        if (this.getAwardAmount() < awardTier.getAwardAmount())
            return BEFORE;
        if (this.getAwardAmount() > awardTier.getAwardAmount())
            return AFTER;

        return EQUAL;
    }

    public Long getAwardTierId() {
        return awardTierId;
    }

    public void setAwardTierId(Long awardTierId) {
        this.awardTierId = awardTierId;
    }
}
