package com.maritz.culturenext.budget.dto;

public class BudgetEligibilityDTO {

	private long programId;
	private long budgetId;
	
	public long getProgramId() {
		return programId;
	}
	
	public BudgetEligibilityDTO setProgramId(long programId) {
		this.programId = programId;
		return this;
	}
	
	public long getBudgetId() {
		return budgetId;
	}
	
	public BudgetEligibilityDTO setBudgetId(long budgetId) {
		this.budgetId = budgetId;
		return this;
	}
}
