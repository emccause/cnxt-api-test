package com.maritz.culturenext.budget.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class BudgetSummaryDTO {
    
    Long budgetId;
    String name;
    String displayName;
    Integer totalAmount;
    Integer usedAmount;
    String type;
    String createDate;
    String status;
    String fromDate;
    String thruDate;
    String budgetConstraint;
    boolean usedInProgram;
    EmployeeDTO budgetOwner;
    
    public BudgetSummaryDTO() {
    }
    
    public Long getBudgetId() {
        return budgetId;
    }
    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public Integer getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }
    public Integer getUsedAmount() {
        return usedAmount;
    }
    public void setUsedAmount(Integer usedAmount) {
        this.usedAmount = usedAmount;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getFromDate() {
        return fromDate;
    }
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
    public String getThruDate() {
        return thruDate;
    }
    public void setThruDate(String thruDate) {
        this.thruDate = thruDate;
    }
    public String getBudgetConstraint() {
        return budgetConstraint;
    }
    public void setBudgetConstraint(String budgetConstraint) {
        this.budgetConstraint = budgetConstraint;
    }
    public boolean isUsedInProgram() {
        return usedInProgram;
    }
    public void setUsedInProgram(boolean usedInProgram) {
        this.usedInProgram = usedInProgram;
    }
    public EmployeeDTO getBudgetOwner() {
        return budgetOwner;
    }
    public void setBudgetOwner(EmployeeDTO budgetOwner) {
        this.budgetOwner = budgetOwner;
    }
}
