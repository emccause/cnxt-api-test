package com.maritz.culturenext.budget.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class BudgetDTO {
    private Long budgetId;
    private Long parentBudgetId;
    private String name;
    private String displayName;
    private String description;
    private Double totalAmount;
    private Double usedAmount;
    private String type;
    private String accountingId;
    private Double individualGivingLimit;
    private Double individualUsedAmount;
    private Boolean usedInProgram;
    private String createDate;
    private String status;
    private String fromDate;
    private String thruDate;
    private String budgetConstraint;
    private String projectNumber;
    private Boolean isUsable;
    private EmployeeDTO budgetOwner;
    private BudgetDetailsDTO budgetDetails;
    private List<BudgetDTO> childBudgets;
    private String overrideProjectNumber;
    private String overrideAccountingId;
    private String overrideLocation;
    
    public Long getBudgetId() {
        return budgetId;
    }
    
    public BudgetDTO setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }
    
    public Long getParentBudgetId() {
        return parentBudgetId;
    }
    
    public BudgetDTO setParentBudgetId(Long parentBudgetId) {
        this.parentBudgetId = parentBudgetId;
        return this;
    }
    
    public String getName() {
        return name;
    }
    
    public BudgetDTO setName(String name) {
        this.name = name;
        return this;
    }
    
    public String getDisplayName() {
        return displayName;
    }
    
    public BudgetDTO setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public BudgetDTO setDescription(String description) {
        this.description = description;
        return this;
    }
    
    public Double getTotalAmount() {
        return totalAmount;
    }
    
    public BudgetDTO setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }
    
    public Double getUsedAmount() {
        return usedAmount;
    }
    
    public BudgetDTO setUsedAmount(Double usedAmount) {
        this.usedAmount = usedAmount;
        return this;
    }
    
    public String getType() {
        return type;
    }
    
    public BudgetDTO setType(String type) {
        this.type = type;
        return this;
    }
    
    public String getAccountingId() {
        return accountingId;
    }
    
    public BudgetDTO setAccountingId(String accountingId) {
        this.accountingId = accountingId;
        return this;
    }
    
    public Double getIndividualGivingLimit() {
        return individualGivingLimit;
    }
    
    public BudgetDTO setIndividualGivingLimit(Double individualGivingLimit) {
        this.individualGivingLimit = individualGivingLimit;
        return this;
    }
    
    public Double getIndividualUsedAmount() {
        return individualUsedAmount;
    }
    
    public BudgetDTO setIndividualUsedAmount(Double individualUsedAmount) {
        this.individualUsedAmount = individualUsedAmount;
        return this;
    }
    
    public Boolean getUsedInProgram() {
        return usedInProgram;
    }
    
    public BudgetDTO setUsedInProgram(Boolean usedInProgram) {
        this.usedInProgram = usedInProgram;
        return this;
    }
    
    public String getCreateDate() {
        return createDate;
    }
    
    public BudgetDTO setCreateDate(String createDate) {
        this.createDate = createDate;
        return this;
    }
    
    public String getStatus() {
        return status;
    }
    
    public BudgetDTO setStatus(String status) {
        this.status = status;
        return this;
    }
    
    public String getFromDate() {
        return fromDate;
    }
    
    public BudgetDTO setFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }
    
    public String getThruDate() {
        return thruDate;
    }
    
    public BudgetDTO setThruDate(String thruDate) {
        this.thruDate = thruDate;
        return this;
    }
    
    public String getBudgetConstraint() {
        return budgetConstraint;
    }
    
    public BudgetDTO setBudgetConstraint(String budgetConstraint) {
        this.budgetConstraint = budgetConstraint;
        return this;
    }
    
    public String getProjectNumber() {
        return projectNumber;
    }
    
    public BudgetDTO setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
        return this;
    }
    
    public Boolean getIsUsable() {
        return isUsable;
    }

    public BudgetDTO setIsUsable(Boolean isUsable) {
        this.isUsable = isUsable;
        return this;
    }

    public EmployeeDTO getBudgetOwner() {
        return budgetOwner;
    }
    
    public BudgetDTO setBudgetOwner(EmployeeDTO budgetOwner) {
        this.budgetOwner = budgetOwner;
        return this;
    }
    
    public BudgetDetailsDTO getBudgetDetails() {
        return budgetDetails;
    }
    
    public BudgetDTO setBudgetDetails(BudgetDetailsDTO budgetDetails) {
        this.budgetDetails = budgetDetails;
        return this;
    }
    
    public List<BudgetDTO> getChildBudgets() {
        return childBudgets;
    }
    
    public BudgetDTO setChildBudgets(List<BudgetDTO> childBudgets) {
        this.childBudgets = childBudgets;
        return this;
    }

    public String getOverrideProjectNumber() {
        return overrideProjectNumber;
    }

    public BudgetDTO setOverrideProjectNumber(String overrideProjectNumber) {
        this.overrideProjectNumber = overrideProjectNumber;
        return this;
    }

    public String getOverrideAccountingId() {
        return overrideAccountingId;
    }

    public BudgetDTO setOverrideAccountingId(String overrideAccountingId) {
        this.overrideAccountingId = overrideAccountingId;
        return this;
    }

    public String getOverrideLocation() {
        return overrideLocation;
    }

    public BudgetDTO setOverrideLocation(String overrideLocation) {
        this.overrideLocation = overrideLocation;
        return this;
    }
}
