package com.maritz.culturenext.budget.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;


public class ApprovalDTO implements Comparable<ApprovalDTO>{
    private Integer level;
    private String type;
    private EmployeeDTO pax;
    
    public Integer getLevel() {
        return level;
    }
    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public EmployeeDTO getPax() {
        return pax;
    }
    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ApprovalDTO other = (ApprovalDTO) obj;
        if (level == null) {
            if (other.level != null)
                return false;
        } else if (!level.equals(other.level))
            return false;
        return true;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((level == null) ? 0 : level.hashCode());
        return result;
    }
    
    @Override
    public int compareTo(ApprovalDTO approval) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;
        
        if(this == approval) return EQUAL;
        
        if(this.getLevel() < approval.getLevel()) return BEFORE;
        if(this.getLevel() > approval.getLevel()) return AFTER;
        
        return EQUAL;
    }
}
