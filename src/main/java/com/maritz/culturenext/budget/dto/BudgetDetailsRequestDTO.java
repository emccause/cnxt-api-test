package com.maritz.culturenext.budget.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

// Use BudgetDetailsDTO for responses
public class BudgetDetailsRequestDTO {

    private List<GroupMemberStubDTO> budgetUsers;
    private Boolean canAllocate;
    private Double allocatedIn;
    private Double allocatedOut;

    public List<GroupMemberStubDTO> getBudgetUsers() {
        return budgetUsers;
    }

    public BudgetDetailsRequestDTO setBudgetUsers(List<GroupMemberStubDTO> budgetUsers) {
        this.budgetUsers = budgetUsers;
        return this;
    }
    
    public Boolean getCanAllocate() {
        return canAllocate;
    }
    public BudgetDetailsRequestDTO setCanAllocate(Boolean canAllocate) {
        this.canAllocate = canAllocate;
        return this;
    }
    
    public Double getAllocatedIn() {
        return allocatedIn;
    }
    
    public BudgetDetailsRequestDTO setAllocatedIn(Double allocatedIn) {
        this.allocatedIn = allocatedIn;
        return this;
    }
    
    public Double getAllocatedOut() {
        return allocatedOut;
    }
    
    public BudgetDetailsRequestDTO setAllocatedOut(Double allocatedOut) {
        this.allocatedOut = allocatedOut;
        return this;
    }
}
