package com.maritz.culturenext.budget.dto;

public class BudgetTotalsFlatDTO {
    private Long budgetId;
    private Double totalAmount;
    private Double totalUsed;

    public BudgetTotalsFlatDTO(Long budgetId, Double totalAmount, Double totalUsed) {
        this.budgetId = budgetId;
        this.totalAmount = totalAmount;
        this.totalUsed = totalUsed;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getTotalUsed() {
        return totalUsed;
    }

    public void setTotalUsed(Double totalUsed) {
        this.totalUsed = totalUsed;
    }

}
