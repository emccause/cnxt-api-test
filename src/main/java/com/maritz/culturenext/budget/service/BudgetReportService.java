package com.maritz.culturenext.budget.service;

import com.maritz.culturenext.budget.dto.BudgetUtilizationStatsDTO;

public interface BudgetReportService {
        
    /**
    * Admin Budget Dashboard, but the passed in budget list is powered by the cached queryId.
    * 
    * Gets budget stats by taking a queryId (cacheId) parameter which will be used to search cached values to get a
     * budget ID list and start/end dates. A specific budgetId can be passed to filter only on that budget.
     * 
     * Extracts the query cache object using the ReportsQueryCacheHandler as well as the budget ids (as they are not
     * populated initially with the cache object) and, after validation, initiates the retrieval of the stats with the
     * specified budgets, and date range.
     * 
     * @param budgetId - budget ID to filter on
     * @param queryId ID of the query that specifies conditions for programs to satisfy
     * @return stats of programs that satisfy query criteriaReportsQueryCacheHandler
     * 
     * Documentation for getting the budget ID list from cache:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468862/Budgets+Query+by+Date+and+Audience
     * 
     * Documentation for the actual admin budget dashboard:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12156991/Admin+Budgets+Utilization
     */
    public BudgetUtilizationStatsDTO getBudgetStatsByQueryId(Long budgetId, String queryId) throws Exception;
    
    /**
     * Gets budget stats (manager view) by taking a queryId (cacheId) parameter which will be used to search cached
     * values to get a list of the manager's direct reports, start/end dates, and all budgets the direct reports can
     * give from
     * 
     * @param queryId ID of the query that specifies conditions for programs to satisfy
     * @return stats of programs that satisfy query criteriaReportsQueryCacheHandler
     * 
     * TODO I don't think this code is used anymore 
     */
    public BudgetUtilizationStatsDTO getManagerBudgetStatsByQueryId( String queryId) throws Exception;
    
    /**
     *
     * Gets budget stats (manager view) for a specific pax ID by taking a queryId (cacheId) parameter which will be used
     * to search cached values to get a list of all budgets that the specified pax ID can give from, as well as
     * start/end dates.
     * 
     * @param paxId ID of the person to pull specific budget usage stats for
     * @param queryId ID of the query that specifies conditions for programs to satisfy
     * @return stats of programs that satisfy query criteriaReportsQueryCacheHandler
     * 
     * TODO - I dont think this code is used anymore
     */
    public BudgetUtilizationStatsDTO getManagerBudgetStatsForPaxByQueryId(Long paxId, String queryId) throws Exception;
    
    /**
     * Budget utilization dashboard - Budget Owner view. This will show totals for all budgets owned by the pax (and their team).
     * 
     * @param budgetOwnerId - Pax ID of the budget owner
     * @param budgetIdsList - Comma delimited list of budget IDs to filter on
     * @param fromDate - Inclusive date to begin filtering data on
     * @param thruDate - Inclusive date to stop filtering data on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/77889736/Budget+Owner+Dashboard
     */
    BudgetUtilizationStatsDTO getBudgetOwnerDashboard(Long budgetOwnerId, String budgetIdsList, String fromDate, String thruDate);
    
    /**
     * Budget utilization dashboard - Manager view. This will show totals for budgets the manager and his team can give from.
     * This only includes budgets with giving limits (and allocated budgets).
     * @param paxIdsCommaSeparated
     * @param fromDate
     * @param thruDate
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/81723429/Manager+Budgets+Dashboard
     */
    public BudgetUtilizationStatsDTO getManagerBudgetDashboard(String paxIdsCommaSeparated, String fromDate, String thruDate);
}
