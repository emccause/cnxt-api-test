package com.maritz.culturenext.budget.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.budget.dto.ChildBudgetUtilizationStatsDTO;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dao.BudgetReportDao;
import com.maritz.culturenext.budget.dto.BudgetUtilizationStatsDTO;
import com.maritz.culturenext.budget.service.BudgetReportService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheHandler;
import com.maritz.culturenext.reports.constants.ReportConstants;

@Component
public class BudgetReportServiceImpl implements BudgetReportService {

    @Inject private BudgetInfoDao budgetInfoDao;
    @Inject private BudgetReportDao budgetReportDao;
    @Inject private ReportsQueryCacheHandler reportsQueryCacheHandler;

    @Override
    public ChildBudgetUtilizationStatsDTO getBudgetStatsByQueryId(Long budgetId, String queryId) throws Exception {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // validate queryId
        if (queryId == null || queryId.isEmpty()) {
            errors.add(BudgetConstants.MISSING_QUERY_ID_MESSAGE);
        }

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);
        
        // if budgetId was not provided, grab it from the cacheObject
        List<Long> budgetIdList = new ArrayList<>();
        if (budgetId == null) {
            //Note: If we decide not to use query logic to get a budget Id list here, null will return info for all budgets.
            budgetIdList = reportsQueryCacheHandler.getBudgetIdList(queryId);
            if (budgetIdList.size() == 0) {
                // Nothing found in cache for that query ID, must be invalid or expired
                errors.add(ReportConstants.INVALID_QUERY_ID_MESSAGE);
            }
        } else {
            budgetIdList.add(budgetId);
        }

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);
        
        //These will be the final values returned
        Double totalBudget = 0D;
        Double budgetUsed = 0D;
        Double budgetAvailable = 0D;
        Double budgetExpired = 0D;
        Double budgetAllocated = 0D;
        Double allocatedUsed = 0D;
        Double allocatedAvailable = 0D;
        Double allocatedExpired = 0D;
                
        List<Map<String, Object>> budgetTotalsList = budgetInfoDao.getBudgetTotalsFlat(budgetIdList);
                    
        if (!CollectionUtils.isEmpty(budgetTotalsList)) {
            for (Map<String, Object> budgetTotals : budgetTotalsList) {
                Double individualTotal = (Double) budgetTotals.get(ProjectConstants.TOTAL_AMOUNT);
                Double individualUsed = (Double) budgetTotals.get(ProjectConstants.TOTAL_USED);
                
                totalBudget += individualTotal;
                budgetUsed += individualUsed;
                
                if (budgetTotals.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ACTIVE.name())) {
                    budgetAvailable += (individualTotal - individualUsed);
                } else if (budgetTotals.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ENDED.name())) { 
                    budgetExpired += (individualTotal - individualUsed);
                }
            }

            if (budgetIdList.size() == 1) {
                //Pull info for the rest of the budget tree (all child budgets)
                List<Map<String, Object>> budgetTotalsChildren = budgetInfoDao.getChildBudgetTotals(budgetIdList);
                if (!CollectionUtils.isEmpty(budgetTotalsChildren)) {
                    Map<String, Object> budgetTree = budgetTotalsChildren.get(0);

                    allocatedUsed = (Double) budgetTree.get(ProjectConstants.TOTAL_USED);
                    budgetAllocated = (Double) budgetTree.get(ProjectConstants.TOTAL_AMOUNT);

                    //Grab the status of the parent budget to determine active/expired
                    //Only account for ACTIVE and ENDED budgets (exclude SCHEDULED and INACTIVE)
                    Map<String, Object> parentBudgetNode = budgetTotalsList.get(0);
                    if (parentBudgetNode.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ACTIVE.name())) {
                        allocatedAvailable = budgetAllocated - allocatedUsed;
                    } else if (parentBudgetNode.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ENDED.name())) {
                        allocatedExpired = budgetAllocated - allocatedUsed;
                    }            
                }
            }
        }
            
        // Step 5: Populate the DTO and return
        ChildBudgetUtilizationStatsDTO budgetUtilizationStatsDTO = new ChildBudgetUtilizationStatsDTO();
        budgetUtilizationStatsDTO.setBudgetAvailable(budgetAvailable);
        budgetUtilizationStatsDTO.setBudgetExpired(budgetExpired);
        budgetUtilizationStatsDTO.setBudgetUsed(budgetUsed);
        budgetUtilizationStatsDTO.setTotalBudget(totalBudget);
        //Child budget info
        budgetUtilizationStatsDTO.setBudgetAllocated(budgetAllocated);
        budgetUtilizationStatsDTO.setChildBudgetAvailable(allocatedAvailable);
        budgetUtilizationStatsDTO.setChildBudgetExpired(allocatedExpired);
        budgetUtilizationStatsDTO.setChildBudgetUsed(allocatedUsed);    

        return budgetUtilizationStatsDTO;
    }
    
    /**
     * Set the dto based on the nodes passed in
     * 
     * @param budgetStatNodes
     * @return
     */
    private BudgetUtilizationStatsDTO setBudgetUtilizationStatsDTO(List<Map<String, Object>> budgetStatNodes) {
        BudgetUtilizationStatsDTO budgetUtilizationStatsDTO = new BudgetUtilizationStatsDTO();
    
        Double budgetAvailable = 0d;
        Double budgetExpired = 0d;
        Double budgetUsedInPeriod = 0d;
        Double budgetUsedOutPeriod = 0d;
        Double budgetTotal = 0d;
        
        for (Map<String, Object> node : budgetStatNodes) {
            if (node.get(ProjectConstants.BUDGET_AVAILABLE) != null) {
                budgetAvailable += (Double) node.get(ProjectConstants.BUDGET_AVAILABLE);
            }
            if (node.get(ProjectConstants.BUDGET_EXPIRED) != null) {
                budgetExpired += (Double) node.get(ProjectConstants.BUDGET_EXPIRED);
            }
            if (node.get(ProjectConstants.BUDGET_USED_IN_PERIOD) != null) {
                budgetUsedInPeriod += (Double) node.get(ProjectConstants.BUDGET_USED_IN_PERIOD);
            }
            if (node.get(ProjectConstants.BUDGET_USED_OUTSIDE_PERIOD) != null ) {
                budgetUsedOutPeriod += (Double) node.get(ProjectConstants.BUDGET_USED_OUTSIDE_PERIOD);
            }
            if (node.get(ProjectConstants.BUDGET_TOTAL) != null) {
                budgetTotal += (Double) node.get(ProjectConstants.BUDGET_TOTAL);
            }
        }
        
        budgetUtilizationStatsDTO.setBudgetAvailable(budgetAvailable);
        budgetUtilizationStatsDTO.setBudgetExpired(budgetExpired);
        budgetUtilizationStatsDTO.setBudgetUsed(budgetUsedInPeriod + budgetUsedOutPeriod);
        budgetUtilizationStatsDTO.setTotalBudget(budgetTotal);

        return budgetUtilizationStatsDTO;
    }

    @Override
    public BudgetUtilizationStatsDTO getManagerBudgetStatsByQueryId(String queryId) throws Exception {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // validate queryId
        if (queryId == null || queryId.isEmpty()) {
            errors.add(BudgetConstants.MISSING_QUERY_ID_MESSAGE);
        }

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);

        // Get the cached list of direct reports' budgets
        Map<Long, List<Long>> budgetIdMap = reportsQueryCacheHandler.getPaxWithGivingLimitIdList(queryId);

        if (budgetIdMap == null || budgetIdMap.size() == 0) {
            // Nothing found in cache for that query ID, must be invalid or expired
            errors.add(ReportConstants.INVALID_QUERY_ID_MESSAGE);
        }

        // Consolidate all of the budgetIds in the map into one list
        Collection<List<Long>> budgetListCollection = budgetIdMap.values();
        List<Long> allBudgetList = new ArrayList<Long>();

        for (List<Long> budgetList : budgetListCollection) {
            for (int i = 0; i < budgetList.size(); i++) {
                // Add all budgetIds to one list (keep duplicates)
                allBudgetList.add(budgetList.get(i));
            }
        }

        // We only want the paxIDs that have budgets to show up in the list
        List<Long> directReportIdList = new ArrayList<Long>(budgetIdMap.keySet());

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);

        Date startDate = reportsQueryCacheHandler.getStartDate(queryId);
        Date endDate = reportsQueryCacheHandler.getEndDate(queryId);

        String directReportString = StringUtils.join(directReportIdList, ",");
        String budgetIdListString = StringUtils.join(allBudgetList, ",");

        List<Map<String, Object>> budgetStatNodes = budgetReportDao.getManagerBudgetStats(directReportString,
                budgetIdListString, startDate, endDate);

        return setBudgetUtilizationStatsDTO(budgetStatNodes);
    }

    @Override
    public BudgetUtilizationStatsDTO getManagerBudgetStatsForPaxByQueryId(Long paxId, String queryId) throws Exception {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // validate queryId
        if (queryId == null || queryId.isEmpty()) {
            errors.add(BudgetConstants.MISSING_QUERY_ID_MESSAGE);
        }

        // validate paxId
        if (paxId == null) {
            errors.add(BudgetConstants.MISSING_PAX_ID_MESSAGE);
        }

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);

        // Get the cached list of direct reports' budgets
        Map<Long, List<Long>> budgetIdMap = reportsQueryCacheHandler.getPaxWithGivingLimitIdList(queryId);

        if (budgetIdMap == null || budgetIdMap.size() == 0) {
            // Nothing found in cache for that query ID, must be invalid or expired
            errors.add(ReportConstants.INVALID_QUERY_ID_MESSAGE);
        }

        // Get the list of budgetIds for the specified pax
        List<Long> budgetIdsForPax = budgetIdMap.get(paxId);

        if (budgetIdsForPax == null || budgetIdsForPax.size() == 0) {
            // There are no budgets for that pax, or the pax is not a direct report
            errors.add(BudgetConstants.NO_BUDGETS_FOR_PAX_MESSAGE);
        }

        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);

        Date startDate = reportsQueryCacheHandler.getStartDate(queryId);
        Date endDate = reportsQueryCacheHandler.getEndDate(queryId);

        String budgetIdListString = StringUtils.join(budgetIdsForPax, ",");

        List<Map<String, Object>> budgetStatNodes = budgetReportDao.getManagerBudgetStats(String.valueOf(paxId),
                budgetIdListString, startDate, endDate);

        return setBudgetUtilizationStatsDTO(budgetStatNodes);
    }

    @Override
    public BudgetUtilizationStatsDTO getBudgetOwnerDashboard(Long budgetOwnerId, String budgetIdString,
            String fromDate, String thruDate) {
        
        validateBudgetOwnerParams(budgetIdString, fromDate, thruDate);
        
        BudgetUtilizationStatsDTO statsDto = new BudgetUtilizationStatsDTO();
        Long budgetId;
        Double totalUsed = 0D;
        Double budgetAvailable = 0D;
        Double budgetExpired = 0D;
        String status;
        
        if (budgetIdString == null) {
            //Find all the budgets owned by the pax and use VW_BUDGET_TOTALS_FLAT to sum up all amounts
            List<Map<String, Object>> nodes = budgetReportDao.getBudgetOwnerStats(budgetOwnerId);
            
            for (Map<String, Object> budgetTotalsNode : nodes) {
                totalUsed += (Double) budgetTotalsNode.get(ProjectConstants.BUDGET_USED);
                //Only account for ACTIVE and ENDED budgets (exclude SCHEDULED and INACTIVE)
                status = (String) budgetTotalsNode.get(ProjectConstants.STATUS_TYPE_CODE);
                if (status.equals(StatusTypeCode.ACTIVE.name())) {
                    budgetAvailable += (Double) budgetTotalsNode.get(ProjectConstants.BUDGET_AVAILABLE);
                } else  if (status.equals(StatusTypeCode.ENDED.name())) {
                    budgetExpired += (Double) budgetTotalsNode.get(ProjectConstants.BUDGET_AVAILABLE);
                }
            }
        } else {
            //Use VW_BUDGET_TOTALS to sum up the passed in budget and all child budgets under it
            budgetId = Long.valueOf(budgetIdString);
            List<Map<String, Object>> nodes = budgetInfoDao.getBudgetTotals(Arrays.asList(budgetId));
            Map<String, Object> budgetTotalsNode = nodes.get(0);
            Double totalAmount = (Double) budgetTotalsNode.get(ProjectConstants.TOTAL_AMOUNT);
            totalUsed = (Double) budgetTotalsNode.get(ProjectConstants.TOTAL_USED);
            status = (String) budgetTotalsNode.get(ProjectConstants.STATUS_TYPE_CODE);
            //Only account for ACTIVE and ENDED budgets (exclude SCHEDULED and INACTIVE)
            if (status.equals(StatusTypeCode.ACTIVE.name())) {
                budgetAvailable = (totalAmount - totalUsed);
            } else  if (status.equals(StatusTypeCode.ENDED.name())) {
                budgetExpired = (totalAmount - totalUsed);
            }
        }
        
        statsDto.setBudgetUsed(totalUsed);
        statsDto.setBudgetAvailable(budgetAvailable);
        statsDto.setBudgetExpired(budgetExpired);

        return statsDto;
    }

    /**
     * Validate request parameters on Budget Owner stats endpoint.
     * @param budgetIdsList
     * @param fromDate
     * @param thruDate
     */
    private void validateBudgetOwnerParams(String budgetIdsList, String fromDate, String thruDate) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        if (fromDate == null) {
            errors.add(BudgetConstants.MISSING_FROM_DATE_MESSAGE);
        }
        
        if (thruDate == null) {
            errors.add(BudgetConstants.MISSING_THRU_DATE_MESSAGE);
        }
        
        if (budgetIdsList != null
                && !budgetIdsList.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            errors.add(BudgetConstants.BUDGET_ID_NON_DIGIT_MESSAGE);
        }

        if (fromDate != null && thruDate != null) {
            Date startDate = DateUtil.convertFromString(fromDate);
            Date endDate = DateUtil.convertFromString(thruDate);
            
            if (startDate == null) {
                errors.add(BudgetConstants.INVALID_FROM_DATE_MESSAGE);
            }
            
            if (endDate == null) {
                errors.add(BudgetConstants.INVALID_THRU_DATE_MESSAGE);
            }
            
            if (startDate != null && endDate != null && startDate.after(endDate)) {
                errors.add(BudgetConstants.INVALID_DATE_RANGE_MESSAGE);
            }
        }
        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);
    }

    @Override
    public BudgetUtilizationStatsDTO getManagerBudgetDashboard(String paxIdsList, String fromDate,
            String thruDate) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        // paxIds list is required. 
        if (paxIdsList == null || paxIdsList.isEmpty()) {
            errors.add(ReportConstants.MISSING_PAX_IDS_MESSAGE);
        }
        
        // Only digits are valid on paxIds query parameter.
        else if (!paxIdsList.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            errors.add(ReportConstants.INVALID_PAX_IDS_CONTENT);
        }
        
        // Validate date ranges.
        Date startDate = null;
        if (fromDate != null && !fromDate.isEmpty()) {
            startDate = DateUtil.convertFromString(fromDate);
        }
        
        Date endDate = null;
        if (thruDate != null && !thruDate.isEmpty()) {
            endDate = DateUtil.convertFromString(thruDate);
        }
        
        errors.addAll(DateUtil.validateDateRange(startDate, endDate));
        
        // handle and throw any errors
        ErrorMessageException.throwIfHasErrors(errors);
        
        //These will be the final values returned
        Double budgetUsedInPeriod = 0D;
        Double budgetUsedOutsidePeriod = 0D;
        Double totalBudget = 0D;
        Double budgetAvailable = 0D;
        Double budgetExpired = 0D;
        
        /* Step 1: For a list of paxIds, find all budgets they can give from.
         * This will return budgetId and a count of how many of the passed in pax can use that budget.
         * Here we are filtering to only return budgets with giving limits (or allocated children budgets) */
        List<Map<String, Object>> eligibleBudgetList = budgetInfoDao.getEligibleBudgetsForPax(paxIdsList, Boolean.TRUE, Boolean.TRUE);
        
        List<Long> budgetIdList = new ArrayList<Long>();
        if (!CollectionUtils.isEmpty(eligibleBudgetList)) {
            for (Map<String, Object> node : eligibleBudgetList) {
                budgetIdList.add((Long) node.get(ProjectConstants.ID));
            }
        }
        
        // Step 2: Get the used in/out of period stats for the paxId list and date range
        List<Map<String, Object>> usedStatsList = budgetInfoDao.getBudgetUsedByPax(paxIdsList, startDate, endDate);

        //Need to keep track of what was spent out of active vs ended budgets
        Double usedActive = 0D;
        Double usedExpired = 0D;
        if (!CollectionUtils.isEmpty(usedStatsList)) {
            for (Long id : budgetIdList) {
                //Find the corresponding records in the usedStatsList
                for (Map<String, Object> node : usedStatsList) {
                    if (id.equals((Long) node.get(ProjectConstants.BUDGET_ID))) {
                        Double inPeriod = (Double) node.get(ProjectConstants.USED_IN_PERIOD);
                        Double outOfPeriod = (Double) node.get(ProjectConstants.USED_OUT_OF_PERIOD);

                        budgetUsedInPeriod += inPeriod;
                        budgetUsedOutsidePeriod += outOfPeriod;

                        //Only account for ACTIVE and ENDED budgets (exclude SCHEDULED and INACTIVE)
                        if (node.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ACTIVE.name())) {
                            usedActive += (inPeriod + outOfPeriod);
                        } else if (node.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ENDED.name())) {
                            usedExpired += (inPeriod + outOfPeriod); 
                        }
                    }
                }
            }
        }
        
        // Step 3: Get budget totals (used for allocated budgets and edge cases)
        List<Map<String,Object>> budgetTotalsList = new ArrayList<>();
        Iterable<List<Long>> budgetIdIterable = Iterables.partition(budgetIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> set : budgetIdIterable) {
            List<Map<String,Object>> totalsList = budgetInfoDao.getBudgetTotalsFlat(set);
            if (!CollectionUtils.isEmpty(totalsList)) {
                budgetTotalsList.addAll(totalsList);
            }
        }
        
        // Step 4: Multiply giving limit by the number of people that can use that budget. That will be TOTAL_AMOUNT
        if (!CollectionUtils.isEmpty(budgetTotalsList)) {
            for (Map<String, Object> budgetNode : eligibleBudgetList) {

                Long id = (Long) budgetNode.get(ProjectConstants.ID);

                //Find the corresponding record in the budgetTotals list
                Double overallTotal = 0D;
                Double overallUsed = 0D;
                for (Map<String, Object> totalsNode : budgetTotalsList) {
                    if (id.equals((Long) totalsNode.get(ProjectConstants.BUDGET_ID))) {
                        overallTotal = (Double) totalsNode.get(ProjectConstants.TOTAL_AMOUNT);
                        overallUsed = (Double) totalsNode.get(ProjectConstants.TOTAL_USED);
                        break;
                    }
                }

                Double individualTotal = 0D;
                if (!budgetNode.get(ProjectConstants.BUDGET_TYPE_CODE).equals(BudgetTypeEnum.ALLOCATED.name())) {
                    //For simple and distributed budgets, the total budget will be calculated by how many people can give from it
                    Double givingLimit = (Double) budgetNode.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT);
                    Integer paxCount = (Integer) budgetNode.get(ProjectConstants.PAX_COUNT);
                    individualTotal = givingLimit * paxCount;
                } else {
                    //For allocated budgets, the individual's amount is the same as the overall budget amount
                    individualTotal = overallTotal;
                }

                //If there is less overall budget than there is individual budget, we must use the lower number
                Double budgetTotal = (overallTotal < individualTotal) ? overallTotal : individualTotal;

                //Now we have to check if the budget has been spent (by someone not in the paxIdList)
                Double totalUsedByPaxList = 0D;
                Double budgetRemaining = 0D;
                if (!CollectionUtils.isEmpty(usedStatsList)) {
                    for (Map<String, Object> usedStatsNode : usedStatsList) {
                        if (id.equals((Long) usedStatsNode.get(ProjectConstants.BUDGET_ID))) {
                            Double usedInPeriod = (Double) usedStatsNode.get(ProjectConstants.USED_IN_PERIOD);
                            Double usedOutOfPeriod = (Double) usedStatsNode.get(ProjectConstants.USED_OUT_OF_PERIOD);
                            totalUsedByPaxList = usedInPeriod + usedOutOfPeriod;

                            //Don't account for anything spent by the paxIdList
                            overallUsed -= totalUsedByPaxList;                            
                        }
                    }
                }
                //This is how much budget is left 
                budgetRemaining = overallTotal - overallUsed;

                //If the user's total budget is less than what is left in the budget, use budgetRemaining
                if (budgetRemaining < budgetTotal) {
                    budgetTotal = budgetRemaining;
                }

                totalBudget += budgetTotal;

                
                //Only account for ACTIVE and ENDED budgets (exclude SCHEDULED and INACTIVE)
                if (budgetNode.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ACTIVE.name())) {
                    budgetAvailable += budgetTotal;
                } else if (budgetNode.get(ProjectConstants.STATUS_TYPE_CODE).equals(StatusTypeCode.ENDED.name())) {
                    budgetExpired += budgetTotal;
                }
            }
        }
    
        // Step 5: Populate the DTO and return
        BudgetUtilizationStatsDTO budgetUtilizationStatsDTO = new BudgetUtilizationStatsDTO();
        budgetUtilizationStatsDTO.setBudgetAvailable(budgetAvailable - usedActive);
        budgetUtilizationStatsDTO.setBudgetExpired(budgetExpired - usedExpired);
        budgetUtilizationStatsDTO.setBudgetUsed(budgetUsedInPeriod + budgetUsedOutsidePeriod);
        budgetUtilizationStatsDTO.setTotalBudget(totalBudget);
        
        return budgetUtilizationStatsDTO;
    }
    
}
