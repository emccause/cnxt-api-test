package com.maritz.culturenext.budget.service;

import java.util.List;

import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetRequestDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;

public interface BudgetService {

    /**
     * creates a budget with the data provided
     * 
     * @param budgetDto budget to create
     * @return BudgetDTO created budget
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847667/Create+Budget
     */
    public BudgetDTO createBudget(BudgetRequestDTO budgetDto);
    
    /**
     * Updates the budget with the id provided, based on the data provided
     * 
     * @param budgetId - id of the budget to update
     * @param parentBudgetDTO - info to update to
     * @return BudgetDTO updated budget info
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/77889839/Update+Budget
     */
    public BudgetDTO updateBudgetById(Long budgetId, BudgetRequestDTO parentBudgetDTO);
    
    /**
     * Deletes the budget with the id provided
     * 
     * @param budgetId - id of budget to delete
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338971/Delete+Budget+Example
     * 
     */
    public void deleteBudgetById(Long budgetId);
    
    /**
     * Returns the details for a specific budget. If the budget is of types DISTRIBUTED or ALLOCATED, 
     * then the child-budgets are also retrieved.
     * 
     * @param budgetId - id of budget being requested
     * @return BudgetDTO data on the budget requested
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/74514495/GET+Budget+by+ID
     */
    public BudgetDTO getBudgetById(Long budgetId);
        
    /**
     * Method to return a page of budget info based on the provided info
     * 
     * @param searchString - name of budget(s) you're looking for, will return budgets containing this string
     * @param statusList - comma separated list of statuses to process.  Currently supported are
     *             SCHEDULED, ACTIVE, ENDED,  ARCHIVED
     * @param includeChildren - indicates if child budgets should be returned in the search.
     * @param hasGivingLimit - if it is true, filter budgets where individual limit has been set.
     * @param hasAmount - if it is true, then only return budgets where TOTAL_AMOUNT <> 0 and USED_AMOUNT <> 0
     * @param budgetOwnerList - comma separated list of budget owner pax ids.
     * @param budgetAllocatorList - comma separated list of allocator pax ids.
     * @param pageNumber - number of page to return.  If null / less than 1, will default to 1
     * @param pageSize - size of page to return.  If null / less than 1, will default to 50
     * @param budgetType - type of the budget
     * 
     * @return PaginatedResponseObject<BudgetSummaryDTO> - pagination object containing requested page + meta data + links
     * 
     * Budget Search (GET):
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/45416472/Budget+Search
     * 
     * Budget List (POST - so we can handle thousands of params):
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/98014219/Budget+List+wrapper+for+Budget+Search
     */
    public PaginatedResponseObject<BudgetDTO> getBudgetList(
            PaginationRequestDetails requestDetails, String searchStr, String statusList, Boolean includeChildren, Boolean hasGivingLimit,
            Boolean hasAmount, String budgetOwnerList, String budgetAllocatorList, Integer pageNumber, Integer pageSize, String budgetType
        );
    
    /**
     * Get eligible budgets for program id and pax id
     * 
     * @param paxId - Pax id to search for
     * @param programId - Program id to search for 
     * @return List<BudgetDTO> - budgets the user can give from in the specified program
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/10518640/Giverec+-+Get+eligible+Budgets+Example
     */
    public List<BudgetDTO> getEligibleBudgetsForProgram(Long paxId, Long programId);
    
    /**
     * Method to return eligible budgets for a specific user based on the Budget limit and Budget used.
     * 
     * @param paxId
     * @param givingLimitOnly
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18219042/Get+Individual+Remaining+Budget
     */
    public List<BudgetDTO> getIndividualEligibleBudgets(Long paxId, Boolean givingLimitOnly);
}
