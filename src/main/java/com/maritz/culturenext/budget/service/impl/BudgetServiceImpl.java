package com.maritz.culturenext.budget.service.impl;

import java.util.HashMap;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.inject.Inject;

import com.maritz.culturenext.budget.dto.*;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.util.ListUtils;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Project;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.entity.SubProject;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.budget.dao.BudgetBulkDataManagementDao;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dao.BudgetSearchDao;
import com.maritz.culturenext.budget.service.BudgetService;
import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.BudgetConstraintEnum;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtBudgetRepository;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.StatusTypeUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class BudgetServiceImpl implements BudgetService {

    private static final String BUDGET_ALLOCATOR_ROLE = "ROLE_" + RoleCode.BUDGET_ALLOCATOR.name();

    @Inject private BudgetBulkDataManagementDao budgetBulkDataManagementDao;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private BudgetInfoDao budgetInfoDao;
    @Inject private BudgetSearchDao budgetsSearchDao;
    @Inject private CnxtBudgetRepository cnxtBudgetRepository;
    @Inject private ConcentrixDao<BudgetMisc> budgetMiscDao;
    @Inject private ConcentrixDao<Discretionary> discretionaryDao;
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<GroupsPax> groupsPaxDao;
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ConcentrixDao<Project> projectDao;
    @Inject private ConcentrixDao<SubProject> subProjectDao;
    @Inject private ConcentrixDao<Role> roleDao;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private PaxRepository paxRepository;
    @Inject private ProgramRepository programRepository;
    @Inject private ProxyService proxyService;
    @Inject private Security security;

    @Override
    public BudgetDTO createBudget(BudgetRequestDTO budgetDto) {
        return handleCreateUpdateBudget(budgetDto);
    }

    @Override
    public BudgetDTO updateBudgetById(Long budgetId, BudgetRequestDTO budgetDto) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        //Handle proxy
        Long proxyPaxId = null;
        if (security.isImpersonated()) {
            if (proxyService.doesPaxHaveProxyPermission(PermissionConstants.PROXY_BUDGETS, security.getPaxId(),
                    security.getImpersonatorPaxId())
                    || security.impersonatorHasPermission(PermissionManagementTypes.PROXY_BUDGETS_ANYONE.name())) {
                // impersonator is a proxy of the budget owner pax who has the PROXY_BUDGETS permission
                // or the impersonator has the PROXY_BUDGETS_ANYONE permission
                proxyPaxId = security.getImpersonatorPaxId();
                budgetDto.setProxyPaxId(proxyPaxId);
            } else {
                errors.add(NominationConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
            }
        }

        //The person (non-admin) editing the budget should be the allocator of the top budget sent in
        //This does not apply for the very top level parent budget
        if (budgetDto.getParentBudgetId() != null) {
            Boolean canEdit = (
                        security.hasRole(RoleCode.ADMIN)
                        || security.hasAuthority(budgetDto.getBudgetId(), TableName.BUDGET.name(), BUDGET_ALLOCATOR_ROLE)
                    );

            if (Boolean.FALSE.equals(canEdit)) {
                errors.add(BudgetConstants.MUST_BE_ALLOCATOR_TO_EDIT_MESSAGE);
            }
        }

        //Budget ID in path doesn't match budget ID in the DTO - only matters for top level budget
        if (!budgetId.equals(budgetDto.getBudgetId())) {
            errors.add(BudgetConstants.MISMATCHED_BUDGET_IDS_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);

        return handleCreateUpdateBudget(budgetDto);
    }

    @Override
    public void deleteBudgetById(Long budgetId) {
        List<ErrorMessage> errorMessages = new ArrayList<>();
        List<Long> budgetIds = new ArrayList<Long>();

        //Check isUsed flag for all budgets in the tree
        List<Map<String, Object>> nodes = budgetInfoDao.getIsUsedInfo(budgetId);
        if (!ListUtils.isEmpty(nodes)) {
            for (Map<String, Object> node : nodes) {

                if (Boolean.valueOf((String) node.get(ProjectConstants.IS_USED))) {
                    errorMessages.add(BudgetConstants.BUDGET_USED_MESSAGE);
                    break;
                }

                //Budget is not used - add it to the list
                budgetIds.add((Long) node.get(ProjectConstants.ID));

            }
        }

        ErrorMessageException.throwIfHasErrors(errorMessages);

        //Inactivate the specified budget IDs
        if (!ListUtils.isEmpty(budgetIds)) {
            budgetInfoDao.inactivateBudgets(budgetIds);
            budgetBulkDataManagementDao.cleanUpAclBudgetRoleEntries(RoleCode.BUDGET_OWNER.name(), budgetIds);
            budgetBulkDataManagementDao.cleanUpBudgetOwnerGroupMembership();
        }
    }

    @Override
    public BudgetDTO getBudgetById(Long budgetId) {

        if (budgetId == null) {
            throw new ErrorMessageException(BudgetConstants.MISSING_BUDGET_ID_ERROR);
        }

        // Get budget info
        List<Map<String, Object>> budgetInfoList = budgetInfoDao.getBudgetInfoById(budgetId);
        Boolean includeInactiveBudgets = true;
        String budgetTypeCode = "";
        List<BudgetDTO> budgetList;
        if (!budgetInfoList.isEmpty()){
        	budgetTypeCode = budgetInfoList.get(0).get(ProjectConstants.BUDGET_TYPE_CODE).toString();
        }        
        if (budgetTypeCode.equals(BudgetConstants.BUDGET_TYPE_CODE_DISTRIBUTED)) {
        	budgetList = populateBudgetList(budgetInfoList, false, includeInactiveBudgets);	
        }else {
        	budgetList = populateBudgetList(budgetInfoList, false, null);
        }

        BudgetDTO resultBudget = null;
        if (!CollectionUtils.isEmpty(budgetList)) {

            // Use map to address the parent budget objects.
            Map<Long, BudgetDTO> budgetMap = new HashMap<Long, BudgetDTO>();
            for(BudgetDTO budget : budgetList) {

                // First budget is the top.
                if (resultBudget == null) {
                    resultBudget = budget;
                }

                // Get parent budget from map and add the current budget into children list.
                BudgetDTO parentBudget = budgetMap.get(budget.getParentBudgetId());
                if (parentBudget != null) {
                    if (CollectionUtils.isEmpty(parentBudget.getChildBudgets())) {
                        parentBudget.setChildBudgets(new ArrayList<BudgetDTO>());
                    }
                    parentBudget.getChildBudgets().add(budget);
                }

                // Add the budget to map
                budgetMap.put(budget.getBudgetId(), budget);
            }
        }

        return resultBudget;
    }

    @Override
    public PaginatedResponseObject<BudgetDTO> getBudgetList(
            PaginationRequestDetails requestDetails, String searchString, String statusList, Boolean includeChildren,
            Boolean hasGivingLimit, Boolean hasAmount, String budgetOwnerList, String budgetAllocatorList,
            Integer pageNumber, Integer pageSize, String budgetType) {

        // get data
        List<Map<String, Object>> resultList = budgetsSearchDao.getBudgetList(
                    searchString, statusList, budgetOwnerList, includeChildren, hasGivingLimit,
                    hasAmount, budgetAllocatorList, pageNumber, pageSize, budgetType
                );

        // process data
        List<BudgetDTO> resultData = populateBudgetList(resultList, Boolean.TRUE, null);
        Integer totalResults = getTotalResults(resultList);

        // Return paginated data
        return new PaginatedResponseObject<BudgetDTO>(requestDetails, resultData, totalResults);
    }

    /**
     * Helper method called by POST and PUT budgets
     * This will first validate the passed in budget and all child budgets, do the saving, and then return the GET response
     *
     * @param budgetDto - budget to be created/updated
     * @return BudgetDTO - the budget as it exists in the database
     */
    protected BudgetDTO handleCreateUpdateBudget(BudgetRequestDTO budgetDto) {
        //Validation shared by all budget types - validate all budgets in the tree

        // Added this check here
        if (budgetDto.getBudgetId() != null) {
            // make sure all children budgets are taken into account
            validateBudgetTreeStructureRecursion(budgetDto, getBudgetById(budgetDto.getBudgetId()));
        }
        validateBudgetRecursion(budgetDto);
        saveBudget(budgetDto);

        return getBudgetById(budgetDto.getBudgetId());
    }

    /**
     * Makes sure all children budgets in the database are on the request
     *
     * @param request budget to be validated
     * @param existing budget structure (assumed loaded from database)
     */
    protected void validateBudgetTreeStructureRecursion(BudgetRequestDTO request, BudgetDTO existing) {
        if (ListUtils.isEmpty(existing.getChildBudgets())) {
            // no children budgets to check
            return;
        }

        if (ListUtils.isEmpty(request.getChildBudgets())) {
            // Children exist in DB but not in request
            throw new ErrorMessageException(
                    "request", "INCOMPLETE_BUDGET_TREE",
                    "Missing existing elements in the budget tree."
                );
        }

        // Collect all request children with ids (not new)
        List<BudgetRequestDTO> requestChildrenList = request.getChildBudgets()
                .stream()
                .filter(budget -> budget.getBudgetId() != null)
                .collect(Collectors.toList());

        if (existing.getChildBudgets().size() != requestChildrenList.size()) {
            // mismatch between existing budgets and request budgets
            throw new ErrorMessageException(
                    "request", "INCOMPLETE_BUDGET_TREE",
                    "Missing existing elements in the budget tree."
                );
        }

        // validate children
        // also makes sure that we have an actual match for each, not just same number of budgets
        requestChildrenList.forEach(requestBudget -> {
            BudgetDTO existingBudget = existing.getChildBudgets().stream()
                .filter(element -> element.getBudgetId().equals(requestBudget.getBudgetId()))
                .findFirst().orElse(null);
            if (existingBudget == null) {
                // didn't find a budget
                // this would throw an error later in validation but I want to avoid null pointers
                throw new ErrorMessageException(
                        "request", "INCOMPLETE_BUDGET_TREE",
                        "Missing existing elements in the budget tree."
                    );
            }
            // validate children
            validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
        });
    }

    /**
     * Recursively validates all fields on every budget in the tree structure
     *
     * @param budgetDto - Budget tree to be validated
     */
    protected void validateBudgetRecursion(BudgetRequestDTO budgetDto) {

        List<ErrorMessage> errors = new ArrayList<>();

        //Required fields
        if (StringUtils.isBlank(budgetDto.getProjectNumber())) {
            errors.add(BudgetConstants.NULL_PROJECT_MESSAGE);
        }
        if (StringUtils.isBlank(budgetDto.getName())) {
            errors.add(BudgetConstants.NULL_NAME_MESSAGE);
        }
        if (StringUtils.isBlank(budgetDto.getType())) {
            errors.add(BudgetConstants.NULL_TYPE_MESSAGE);
        }
        if (StringUtils.isBlank(budgetDto.getFromDate())) {
            errors.add(BudgetConstants.NULL_DATE_MESSAGE);
        }
        if (StringUtils.isBlank(budgetDto.getBudgetConstraint())) {
            errors.add(BudgetConstants.NULL_CONSTRAINT_MESSAGE);
        }

        //If budget description is an empty string, just set it to null
        if (StringUtils.isBlank(budgetDto.getDescription())) {
            budgetDto.setDescription(null);
        }

        ErrorMessageException.throwIfHasErrors(errors);

        if (budgetDto.getBudgetId() != null) {
            //budgetId does not exist in database
            Budget existingBudget = cnxtBudgetRepository.findOne(budgetDto.getBudgetId());
            if (existingBudget == null) {
                errors.add(BudgetConstants.INVALID_BUDGET_ID_MESSAGE);
            }
        }

        //budgetType is invalid
        if (!BudgetTypeEnum.getAllTypes().contains(budgetDto.getType())) {
            errors.add(BudgetConstants.INVALID_TYPE_MESSAGE.setMessage(
                String.format(BudgetConstants.ERROR_INVALID_TYPE_MSG, budgetDto.getType())));
        }

        if (!StringUtils.isBlank(budgetDto.getAccountingId())) {
            //accountingId is longer than 8 characters
            if (budgetDto.getAccountingId().length() > 8) {
                errors.add(BudgetConstants.INVALID_ACCOUNTING_ID_MESSAGE);
            }

            //accountingId is not alphanumeric
            if (!StringUtils.isAlphanumeric(budgetDto.getAccountingId())) {
                errors.add(BudgetConstants.ACCOUNTING_ID_FORMAT_MESSAGE);
            }
        }

        if (!StringUtils.isBlank(budgetDto.getOverrideProjectNumber())) {

            if (!StringUtils.isBlank(budgetDto.getOverrideAccountingId())) {
                //overrideAccountingId is longer than 8 characters
                if (budgetDto.getOverrideAccountingId().length() > 8) {
                    errors.add(BudgetConstants.INVALID_OVERRIDE_ACCOUNTING_ID_MESSAGE);
                }

                //overrideAccountingId is not alphanumeric
                if (!StringUtils.isAlphanumeric(budgetDto.getOverrideAccountingId())) {
                    errors.add(BudgetConstants.OVERRIDE_ACCOUNTING_ID_FORMAT_MESSAGE);
                }
            } else {
                errors.add(BudgetConstants.NULL_OVERRIDE_ACCOUNTING_ID_MESSAGE);
            }

            if (!StringUtils.isBlank(budgetDto.getOverrideLocation())) {
                //overrideLocation is not valid
                if(!VfName.US_OVERRIDE.name().equalsIgnoreCase(budgetDto.getOverrideLocation())
                && !VfName.NON_US_OVERRIDE.name().equalsIgnoreCase(budgetDto.getOverrideLocation())) {
                    errors.add(BudgetConstants.INVALID_OVERRIDE_LOCATION_MESSAGE);
                }
            } else {
                errors.add(BudgetConstants.NULL_OVERRIDE_LOCATION_MESSAGE);
            }

        }

        //fromDate is not a valid date format
        Date fromDate = DateUtil.convertFromString(budgetDto.getFromDate());
        if (fromDate == null) {
            errors.add(BudgetConstants.INVALID_FROM_DATE_MESSAGE);
        }

        String thruDateString;
        if (!StringUtils.isBlank(budgetDto.getThruDate())) {
            thruDateString = budgetDto.getThruDate();

            //thruDate is not a valid date format
            Date thruDate = DateUtil.convertFromString(thruDateString);
            if (thruDate == null) {
                errors.add(BudgetConstants.INVALID_THRU_DATE_MESSAGE);
            } else {
                //fromDate is later than thruDate
                if (thruDate.before(fromDate)) {
                    errors.add(BudgetConstants.INVALID_DATE_RANGE_MESSAGE);
                }
            }
        }

        //budgetConstraint is invalid
        if (BudgetConstraintEnum.getByStringValue(budgetDto.getBudgetConstraint()) == null) {
            errors.add(BudgetConstants.INVALID_CONSTRAINT_MESSAGE);
        }

        //budgetOwner is valid
        if (budgetDto.getBudgetOwner() != null && budgetDto.getBudgetOwner().getPaxId() != null) {
            //helper method will check pax exists and is ACTIVE (status check for new budgets only)
            errors.addAll(validateActivePaxExists(budgetDto.getBudgetOwner().getPaxId(), budgetDto.getBudgetId()));
        }

        Double allocatedIn = 0D;
        //budgetDetails section - dependent upon budget type
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        if (budgetDetails != null) {

            List<GroupMemberStubDTO> budgetUsers = budgetDetails.getBudgetUsers();
            //This does not apply to simple budgets or top level allocated & distributed budgets
            if (ListUtils.isEmpty(budgetUsers)) {
                if (budgetDto.getParentBudgetId() != null) {
                    errors.add(BudgetConstants.MISSING_BUDGET_USERS_MESSAGE);
                }
            } else {
                for (GroupMemberStubDTO budgetUser : budgetUsers) {
                    if (budgetUser.getPaxId() != null) {
                        //helper method will check pax exists and is ACTIVE (status check for new budgets only)
                        errors.addAll(validateActivePaxExists(budgetUser.getPaxId(), budgetDto.getBudgetId()));
                    } else if (budgetUser.getGroupId() != null) {
                        //groupId does not exist or is INACTIVE
                        Groups g = groupsDao.findById(budgetUser.getGroupId());
                        if (g == null || StatusTypeCode.INACTIVE.name().equalsIgnoreCase(g.getStatusTypeCode())) {
                            errors.add(BudgetConstants.INVALID_GROUP_MESSAGE);
                        }
                    }
                }
            }

            allocatedIn = budgetDetails.getAllocatedIn();

            if (allocatedIn == null) {
                errors.add(BudgetConstants.MISSING_BUDGET_ALLOCATED_IN_MESSAGE);
            }
        } else {
            errors.add(BudgetConstants.MISSING_BUDGET_DETAILS_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);

        //Recursively validate child budgets
        Double allocatedOut = 0D;
        if (!ListUtils.isEmpty(budgetDto.getChildBudgets())) {
            for (BudgetRequestDTO childBudget : budgetDto.getChildBudgets()) {
                validateBudgetRecursion(childBudget);
                BudgetDetailsRequestDTO childDetails = childBudget.getBudgetDetails();
                // add child allocated in as allocated out.
                allocatedOut += childDetails.getAllocatedIn();

                //parent budget id validation
                if (budgetDto.getBudgetId() != null) {
                    // TODO: Remove this logic with MP-11766 - UI should be handling setting parent budget id after initial creation
                    // should be able to go with else only
                    if (childBudget.getParentBudgetId() == null) {
                        childBudget.setParentBudgetId(budgetDto.getBudgetId());
                    }
                    else if (!budgetDto.getBudgetId().equals(childBudget.getParentBudgetId())) {
                        errors.add(BudgetConstants.INVALID_PARENT_BUDGET_ID_MESSAGE);
                    }
                }
            }
        }

        Double totalAmount = allocatedIn - allocatedOut;

        //totalAmount is negative
        if (totalAmount < 0D) {
            errors.add(BudgetConstants.TOTAL_AMOUNT_NEGATIVE_MESSAGE);
        }

        //totalAmount less than usedAmount (updates only)
        if (budgetDto.getBudgetId() != null) {
            Integer usedAmount = cnxtBudgetRepository.getBudgedUsed(budgetDto.getBudgetId());
            if (totalAmount < usedAmount) {
                errors.add(BudgetConstants.TOTAL_AMOUNT_UNDER_USED_AMOUNT_MESSAGE);
            }
        }

        Double givingLimit = budgetDto.getIndividualGivingLimit();
        if (givingLimit != null) {
            //individualGivingLimit is less than or equal to 0
            if (givingLimit <= 0D) {
                errors.add(BudgetConstants.GIVING_LIMIT_NEGATIVE_MESSAGE);
            }

            //individualGivingLimit is more than the totalAmount
            if (givingLimit > totalAmount) {
                errors.add(BudgetConstants.GIVING_LIMIT_OVER_TOTAL_AMOUNT_MESSAGE);
            }
        }

        ErrorMessageException.throwIfHasErrors(errors);
    }

    /**
     * Validates the given paxId exists in the database.
     * If the budgetId is null, that means a new budget is being created.
     * In that case, the paxId cannot be INACTIVE.
     *
     * @param paxId
     * @param budgetId
     * @return error messages
     */
    protected List<ErrorMessage> validateActivePaxExists(Long paxId, Long budgetId) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        //paxId does not exist
        if (paxDao.findById(paxId) == null) {
            errors.add(BudgetConstants.INVALID_PAX_ID_MESSAGE.setMessage(
                    String.format(BudgetConstants.ERROR_INVALID_PAX_ID_MSG, paxId)));
        }

        //pax cannot be INACTIVE (for NEW budgets only)
        if (budgetId == null) {
            SysUser sysUser = sysUserDao.findBy()
                    .where(ProjectConstants.PAX_ID).eq(paxId).findOne();
            if (sysUser != null && StatusTypeCode.INACTIVE.name().equalsIgnoreCase(sysUser.getStatusTypeCode())) {
                errors.add(BudgetConstants.INACTIVE_PAX_ID_MESSAGE.setMessage(
                        String.format(BudgetConstants.ERROR_INACTIVE_PAX_ID_MSG, paxId)));
            }
        }

        return errors;
    }

    /**
     * - Calls recursive method to save each BUDGET record
     * - Builds up a list of ACL's for BUDGET USER, OWNER, and ALLOCATOR so they can be bulk saved
     * - Builds up a list of BUDGET_MISC records so they can be bulk saved
     * - Calls helper method to update the budget's total amount
     * - Adds/Removes users from the appropriate Budget Owners / Allocators groups
     * - Handles subproject number saving
     * - Updates the individualGivingLimit for allocated budgets and bulk saves those
     *
     * @param budgetDto - Budget tree to be saved
     */
    protected void saveBudget(BudgetRequestDTO budgetDto) {

        // Grab the top level parent ID from the first child budget if it exists
        String topLevelBudgetId = null;
        if (!ListUtils.isEmpty(budgetDto.getChildBudgets())) {
            Long childId = budgetDto.getChildBudgets().get(0).getBudgetId();
            if (childId != null) {
                BudgetMisc bm = budgetMiscDao.findBy()
                        .where(ProjectConstants.BUDGET_ID).eq(childId)
                        .and(ProjectConstants.VF_NAME).eq(VfName.TOP_LEVEL_BUDGET_ID.name())
                        .findOne();
                if (bm != null) {
                    topLevelBudgetId = bm.getMiscData();
                }
            }
        }

        // Grab the parent's display name to be used throughout the tree
        // If parentBudgetId is not null, need to grab the display name from the top level parent budget
        String displayName = null;
        if (budgetDto.getParentBudgetId() != null) {
            String topLevelId = budgetMiscDao.findBy()
                    .where(ProjectConstants.BUDGET_ID).eq(budgetDto.getBudgetId())
                    .and(ProjectConstants.VF_NAME).eq(VfName.TOP_LEVEL_BUDGET_ID.name())
                    .findOne(ProjectConstants.MISC_DATA, String.class);

            try {
                Long topId = Long.valueOf(topLevelId);
                Budget topBudget = cnxtBudgetRepository.findOne(topId);

                displayName = (topBudget.getBudgetName() != null && !topBudget.getBudgetName().trim().isEmpty()) ?
                        topBudget.getBudgetName() : topBudget.getBudgetCode();
            } catch (NumberFormatException nfe) { }
        }

        //If topLevelId is null above, we need to update the display name via the parent budget (Manage My Budgets edge-case)
        if (budgetDto.getParentBudgetId() == null || displayName == null) {
            displayName = (budgetDto.getDisplayName() != null && !budgetDto.getDisplayName().trim().isEmpty()) ?
                    budgetDto.getDisplayName() : budgetDto.getName();
        }

        // Convert the budget request object into a flattened version
        RecursiveBudgetSaveDTO recursiveBudgetSaveDto = prepareRecursiveBudgetForSave(budgetDto, displayName);

        // Save budgets and flatten tree
        List<RecursiveBudgetSaveDTO> flattenedDtoList = new ArrayList<>();
        Map<Long, String> budgetIdSubProjectNumberMap = new HashMap<>();
        saveBudgetRecursion(recursiveBudgetSaveDto, budgetIdSubProjectNumberMap, flattenedDtoList);

        //Populate the parent budgetId on the original DTO (to be used in the get by id response)
        budgetDto.setBudgetId(recursiveBudgetSaveDto.getBudget().getId());

        // Now that we've handled tree logic, set up miscs / acl / groups set up

        // First, collect our entities
        List<Long> paxIdList = new ArrayList<>();
        List<Long> groupIdList = new ArrayList<>();
        for (RecursiveBudgetSaveDTO budgetSaveDto : flattenedDtoList) {
            if (budgetSaveDto.getOwnerPaxId() != null && !paxIdList.contains(budgetSaveDto.getOwnerPaxId())) {
                paxIdList.add(budgetSaveDto.getOwnerPaxId());
            }
            // This'll cover parent entity too since all parents must be tied to a budget as a user
            if (budgetSaveDto.getBudgetUserList() != null) {
                for (GroupMemberStubDTO budgetUser : budgetSaveDto.getBudgetUserList()) {
                    if (budgetUser.getPaxId() != null && !paxIdList.contains(budgetUser.getPaxId())) {
                        paxIdList.add(budgetUser.getPaxId());
                    } else if (budgetUser.getGroupId() != null && !groupIdList.contains(budgetUser.getGroupId())) {
                        groupIdList.add(budgetUser.getGroupId());
                    }
                }
            }
        }

        // get all their data
        List<EntityDTO> entityList = participantInfoDao.getInfo(paxIdList, groupIdList);

        // resolve roles
        List<Role> roleList = roleDao.findBy().where(ProjectConstants.ROLE_CODE).in(BudgetConstants.ROLE_CODE_LIST).find();

        Role budgetAllocatorRole = null;
        Role budgetOwnerRole = null;
        Role budgetUserRole = null;

        for (Role role : roleList) {
            if (RoleCode.BUDGET_ALLOCATOR.name().equals(role.getRoleCode())) {
                budgetAllocatorRole = role;
            } else if (RoleCode.BUDGET_OWNER.name().equals(role.getRoleCode())) {
                budgetOwnerRole = role;
            } else if (RoleCode.BUDGET_USER.name().equals(role.getRoleCode())) {
                budgetUserRole = role;
            }
        }

        // resolve groups
        List<Groups> groupList = groupsDao.findBy().where(ProjectConstants.GROUP_NAME).in(BudgetConstants.GROUP_NAME_LIST).find();

        Groups budgetAllocatorGroup = null;
        Groups budgetOwnerGroup = null;

        for (Groups group : groupList) {
            if (BudgetConstants.BUDGET_ALLOCATORS_GROUP_NAME.equals(group.getGroupName())) {
                budgetAllocatorGroup = group;
            } else if (BudgetConstants.BUDGET_OWNERS_GROUP_NAME.equals(group.getGroupName())) {
                budgetOwnerGroup = group;
            }
        }

        // Do the thing - collect misc / acl / groups pax entries

        List<BudgetMisc> budgetMiscList = new ArrayList<>();
        List<Acl> aclList = new ArrayList<>();
        List<GroupsPax> groupsPaxList = new ArrayList<>();

        for (RecursiveBudgetSaveDTO budgetSaveDto : flattenedDtoList) {
            List<EntityDTO> budgetUserList = new ArrayList<>();
            EntityDTO parentEntity = null;
            EmployeeDTO ownerPax = null;

            //Create TRANSACTION_HEADER and DISCRETIONARY records to update the budget's total amount
            updateBudgetAmount(budgetSaveDto);

            // handle budget users
            for (EntityDTO entity : entityList) {
                if (budgetSaveDto.getBudgetUserList() != null) {
                    for (GroupMemberStubDTO budgetUser : budgetSaveDto.getBudgetUserList()) {
                        // Handle pax / group mapping
                        if (budgetUser.getPaxId() != null && entity instanceof EmployeeDTO) {
                            EmployeeDTO employee = (EmployeeDTO) entity;

                            if (budgetUser.getPaxId().equals(employee.getPaxId())) {
                                budgetUserList.add(employee);
                            }
                        }
                        else if (budgetUser.getGroupId() != null && entity instanceof GroupDTO) {
                            GroupDTO group = (GroupDTO) entity;
                            if (budgetUser.getGroupId().equals(group.getGroupId())) {
                                budgetUserList.add(group);
                            }
                        }
                    }
                }

                // handle parent
                if (budgetSaveDto.getParentEntity() != null) {
                    if (budgetSaveDto.getParentEntity().getPaxId() != null && entity instanceof EmployeeDTO) {
                        EmployeeDTO employee = (EmployeeDTO) entity;
                        if (budgetSaveDto.getParentEntity().getPaxId().equals(employee.getPaxId())) {
                            parentEntity = employee;
                        }
                    } else if (budgetSaveDto.getParentEntity().getGroupId() != null && entity instanceof GroupDTO) {
                        GroupDTO group = (GroupDTO) entity;
                        if (budgetSaveDto.getParentEntity().getGroupId().equals(group.getGroupId())) {
                            parentEntity = group;
                        }
                    }
                }

                // handle budget owner
                if (budgetSaveDto.getOwnerPaxId() != null && entity instanceof EmployeeDTO) {
                    EmployeeDTO employee = (EmployeeDTO) entity;
                    if (budgetSaveDto.getOwnerPaxId().equals(employee.getPaxId())) {
                        ownerPax = employee;
                    }
                }
            }

            // handle own name
            if (budgetUserList != null && !budgetUserList.isEmpty()) {
                budgetMiscList.add(createNameMisc(
                        budgetSaveDto.getBudget().getId(), VfName.TIED_ENTITY_NAME.name(), budgetUserList.get(0)
                        ));
            }

            // handle couple parent related things - name and parent budget id
            if (parentEntity != null) {
                budgetMiscList.add(createNameMisc(
                        budgetSaveDto.getBudget().getId(), VfName.PARENT_ENTITY_NAME.name(), parentEntity
                    ));
            }

            //top level parent ID
            if (budgetSaveDto.getBudget().getParentBudgetId() != null) {
                //If topLevelBudgetId is null, use the parentId from the budgetDTO
                if (topLevelBudgetId == null) {
                    Long parentBudgetId = recursiveBudgetSaveDto.getBudget().getId();
                    String parentTopLevelId = budgetMiscDao.findBy()
                            .where(ProjectConstants.BUDGET_ID).eq(parentBudgetId)
                            .and(ProjectConstants.VF_NAME).eq(VfName.TOP_LEVEL_BUDGET_ID.name())
                            .findOne(ProjectConstants.MISC_DATA, String.class);

                    //If the TOP_LEVEL_BUDGET_ID exists, use that. Otherwise, use the parentBudgetId
                    if (parentTopLevelId != null) {
                        topLevelBudgetId = parentTopLevelId;
                    } else {
                        topLevelBudgetId = parentBudgetId.toString();
                    }
                }
                budgetMiscList.add(createBudgetMisc(
                        budgetSaveDto.getBudget().getId(), VfName.TOP_LEVEL_BUDGET_ID.name(), topLevelBudgetId));
            }

            // create miscs - allocated in, allocated out, is usable

            String allocatedIn = (budgetSaveDto.getAllocatedIn() != null) ? budgetSaveDto.getAllocatedIn().toString() : ProjectConstants.ZERO_STRING;
            budgetMiscList.add(createBudgetMisc(
                    budgetSaveDto.getBudget().getId(), VfName.ALLOCATED_IN.name(), allocatedIn
                ));

            String allocatedOut = (budgetSaveDto.getAllocatedOut() != null) ? budgetSaveDto.getAllocatedOut().toString() : ProjectConstants.ZERO_STRING;
            budgetMiscList.add(createBudgetMisc(
                    budgetSaveDto.getBudget().getId(), VfName.ALLOCATED_OUT.name(), allocatedOut
                ));

            String isUsable = (budgetSaveDto.getIsUsable() != null) ? budgetSaveDto.getIsUsable().toString() : ProjectConstants.FALSE.toUpperCase();
            budgetMiscList.add(createBudgetMisc(
                    budgetSaveDto.getBudget().getId(), VfName.IS_USABLE.name(), isUsable
                ));

            // Handle budget allocator
            if (Boolean.TRUE.equals(budgetSaveDto.getCanAllocate()) && !budgetUserList.isEmpty()) {
                for (EntityDTO user : budgetUserList) {
                    //Ties the BUDGET_ALLOCATOR role to the pax or group
                    aclList.add(createBudgetAcl(budgetAllocatorRole.getRoleId(), budgetSaveDto.getBudget().getId(), user));

                    //Puts users in the 'Budget Allocators' group
                    if (user instanceof EmployeeDTO) {
                        GroupsPax allocatorGroupsPax = new GroupsPax();
                        allocatorGroupsPax.setGroupId(budgetAllocatorGroup.getGroupId());
                        allocatorGroupsPax.setPaxId(((EmployeeDTO) user).getPaxId());
                        groupsPaxList.add(allocatorGroupsPax);
                    } else if (user instanceof GroupDTO) {
                        //All users in the 'user' group need to be put into the 'Budget Allocators' group
                        List<Long> paxInGroup = groupsPaxDao.findBy()
                                .where(ProjectConstants.GROUP_ID).eq(((GroupDTO) user).getGroupId())
                                .find(ProjectConstants.PAX_ID, Long.class);

                        if (paxInGroup != null) {
                            GroupsPax allocatorGroupsPax = new GroupsPax();
                            allocatorGroupsPax.setGroupId(budgetAllocatorGroup.getGroupId());
                            for (Long paxId : paxInGroup) {
                                allocatorGroupsPax.setPaxId(paxId);
                                groupsPaxList.add(allocatorGroupsPax);
                            }
                        }
                    }
                }
            } else {
                // delete existing allocator ACLs if no users OR allocator = false
                // Our ACL logic will delete the ACL entry if SUBJECT_TABLE is null, which gets set by the third param here
                aclList.add(createBudgetAcl(budgetAllocatorRole.getRoleId(), budgetSaveDto.getBudget().getId(), null));
            }

            // handle budget owner
            aclList.add(createBudgetAcl(budgetOwnerRole.getRoleId(), budgetSaveDto.getBudget().getId(), ownerPax));
            if (ownerPax != null) {
                GroupsPax ownerGroupsPax = new GroupsPax();
                ownerGroupsPax.setGroupId(budgetOwnerGroup.getGroupId());
                ownerGroupsPax.setPaxId(ownerPax.getPaxId());
                groupsPaxList.add(ownerGroupsPax);
            }

            // create budget users
            if (budgetUserList.isEmpty()) {
                // delete existing budget users
                aclList.add(createBudgetAcl(budgetUserRole.getRoleId(), budgetSaveDto.getBudget().getId(), null));
            } else {
                for (EntityDTO user : budgetUserList) {
                    aclList.add(createBudgetAcl(budgetUserRole.getRoleId(), budgetSaveDto.getBudget().getId(), user));
                }
            }
        }

        // Save the overrides for project and sub-project number
        if (budgetDto.getOverrideProjectNumber() != null && !StringUtils.isEmpty(budgetDto.getOverrideProjectNumber().trim())) {
            Project overrideProject = projectDao.findBy()
                    .where(ProjectConstants.PROJECT_NUMBER).eq(budgetDto.getOverrideProjectNumber()).findOne();

            if (overrideProject == null) {
                overrideProject = new Project();
                overrideProject.setProjectNumber(budgetDto.getOverrideProjectNumber());
                projectDao.save(overrideProject);
            }

            // handle new override sub project number creation
            String overrideSubProjectNumber = budgetDto.getOverrideAccountingId();

            SubProject overrideSubProject = subProjectDao.findBy()
                    .where(ProjectConstants.PROJECT_ID).eq(overrideProject.getId())
                    .and(ProjectConstants.SUB_PROJECT_NUMBER).eq(overrideSubProjectNumber).findOne();
            if (overrideSubProject == null) {
                overrideSubProject = new SubProject();
                overrideSubProject.setProjectId(overrideProject.getId());
                overrideSubProject.setSubProjectNumber(overrideSubProjectNumber);
                subProjectDao.save(overrideSubProject);
            }

            String overrideLocation = (budgetDto.getOverrideLocation() != null) ? budgetDto.getOverrideLocation() : VfName.US_OVERRIDE.name();
            BudgetMisc budgetMiscOverride = budgetMiscDao.findBy()
                    .where(ProjectConstants.BUDGET_ID).eq(budgetDto.getBudgetId())
                    .and(ProjectConstants.MISC_TYPE_CODE).eq(VfName.SUB_PROJECT_OVERRIDE.name())
                    .findOne();
            if (budgetMiscOverride == null) {
                budgetMiscOverride = new BudgetMisc();
            }
            budgetMiscOverride.setBudgetId(budgetDto.getBudgetId());
            budgetMiscOverride.setVfName(overrideLocation);
            budgetMiscOverride.setMiscData(overrideSubProject.getId().toString());
            budgetMiscOverride.setMiscDate(new Date());
            budgetMiscOverride.setMiscTypeCode(VfName.SUB_PROJECT_OVERRIDE.name());
            budgetMiscDao.save(budgetMiscOverride);
        }

        // save everything!
        budgetBulkDataManagementDao.saveACLEntries(aclList);
        budgetBulkDataManagementDao.saveBudgetMiscs(budgetMiscList);
        budgetBulkDataManagementDao.saveGroupsPaxEntries(groupsPaxList);
        budgetBulkDataManagementDao.cleanUpBudgetOwnerGroupMembership();
        budgetBulkDataManagementDao.cleanUpBudgetAllocatorGroupMembership();

        // Last thing - handle sub project numbers and individual giving limits

        // save new sub project numbers
        Project project = projectDao.findBy().where(ProjectConstants.PROJECT_NUMBER).eq(budgetDto.getProjectNumber()).findOne();

        if (project == null) {
            project = new Project();
            project.setProjectNumber(budgetDto.getProjectNumber());
            projectDao.create(project);
        }

        List<String> subProjectNumbersToHandle = new ArrayList<>();
        for (String subProjectNumber : budgetIdSubProjectNumberMap.values()) {
            if (!subProjectNumbersToHandle.contains(subProjectNumber)) {
                subProjectNumbersToHandle.add(subProjectNumber);
            }
        }
        budgetBulkDataManagementDao.saveSubProjectEntries(project.getId(), subProjectNumbersToHandle);

        // Now that we've saved, grab all sub projects and update ids
        List<SubProject> subProjectList = budgetBulkDataManagementDao.findExistingSubProjects(budgetDto.getProjectNumber());

        List<Budget> budgetsToUpdate = new ArrayList<>();
        for (RecursiveBudgetSaveDTO budgetSaveDto : flattenedDtoList) {
            Budget budget = budgetSaveDto.getBudget();
            for (SubProject subProject : subProjectList) { // handle sub projects
                if (subProject.getSubProjectNumber().equals(budgetSaveDto.getSubProjectNumber())) {
                    budget.setSubProjectId(subProject.getId());
                    break;
                }
            }

            budgetsToUpdate.add(budget);
        }

        cnxtBudgetRepository.save(budgetsToUpdate);
    }

    /**
     * Takes the BudgetDTO and maps that data into a RecursiveBudgetSaveDTO so it is easier to save recursively
     *
     * @param budgetDto - Budget tree to be saved
     * @param displayName - Display name of the top level parent
     * @return RecursiveBudgetSaveDTO
     */
    protected RecursiveBudgetSaveDTO prepareRecursiveBudgetForSave(BudgetRequestDTO budgetDto, String displayName) {

        RecursiveBudgetSaveDTO recursiveBudget = new RecursiveBudgetSaveDTO();
        Budget budget = null;
        //If the budget exists, pull it from the database to do the updates
        if (budgetDto.getBudgetId() != null) {
            budget = cnxtBudgetRepository.findOne(budgetDto.getBudgetId());
        } else {
            budget = new Budget();
            //Fields that are not editable but need to be set for new budgets
            budget.setParentBudgetId(budgetDto.getParentBudgetId()); // will be null if this is POST
            budget.setBudgetTypeCode(budgetDto.getType());
            budget.setBudgetCategoryCode(BudgetConstants.DEFAULT_BUDGET_CATEGORY_CODE);
            budget.setBudgetConstraint(budgetDto.getBudgetConstraint());
        }

        budget.setBudgetCode(budgetDto.getName());
        budget.setBudgetName(displayName);
        budget.setBudgetDesc(budgetDto.getDescription());
        budget.setStatusTypeCode(
                budgetDto.getStatus() == null ?
                        StatusTypeCode.ACTIVE.name()
                        : StatusTypeUtil.determineRequestStatus(budgetDto.getStatus())
            );
        budget.setIndividualGivingLimit(budgetDto.getIndividualGivingLimit());
        budget.setFromDate(DateUtil.convertFromString(budgetDto.getFromDate()));
        budget.setThruDate(DateUtil.convertFromString(budgetDto.getThruDate()));
        recursiveBudget.setBudget(budget);

        String subProjectNumber = budgetDto.getAccountingId();
        recursiveBudget.setSubProjectNumber(subProjectNumber); // if it doesn't exist, a new one will be created

        //Handle proxy
        Long proxyPaxId = budgetDto.getProxyPaxId();
        recursiveBudget.setProxyPaxId(proxyPaxId);

        BudgetDetailsRequestDTO details = budgetDto.getBudgetDetails();
        if (details != null) {
            recursiveBudget.setBudgetUserList(details.getBudgetUsers());
            recursiveBudget.setAllocatedIn(details.getAllocatedIn());
            recursiveBudget.setAllocatedOut(0D); // Don't copy in allocatedOut
            recursiveBudget.setCanAllocate(details.getCanAllocate());
        }

        if (budgetDto.getBudgetOwner() != null) {
            recursiveBudget.setOwnerPaxId(budgetDto.getBudgetOwner().getPaxId());
        }

        if (budgetDto.getChildBudgets() != null) {
            Collection<RecursiveBudgetSaveDTO> children = new ArrayList<>();
            for (BudgetRequestDTO child : budgetDto.getChildBudgets()) {
                //Need to set the proxyPaxId on the child budget as well so it gets carried all the way down the tree
                child.setProxyPaxId(proxyPaxId);
                RecursiveBudgetSaveDTO childBudget = prepareRecursiveBudgetForSave(child, displayName);
                if (!CollectionUtils.isEmpty(recursiveBudget.getBudgetUserList())) {
                    childBudget.setParentEntity(recursiveBudget.getBudgetUserList().get(0)); // Parent is first user
                }
                if (recursiveBudget.getAllocatedOut() != null && childBudget.getAllocatedIn() != null) {
                    recursiveBudget.setAllocatedOut(recursiveBudget.getAllocatedOut() + childBudget.getAllocatedIn());
                }
                children.add(childBudget);
            }
            recursiveBudget.setChildren(children);
        }
        recursiveBudget.setIsUsable(budgetDto.getIsUsable());

        return recursiveBudget;
    }

    /**
     * - Loops through the RecursiveBudgetSaveDTO and saves a BUDGET record for each budget in the tree.
     * - Saves subproject information so they can be properly created and tied to the budgets in bulk later
     * - Puts each budget from the tree in its own DTO and adds it to a flattened list of budgets (instead of a tree structure)
     *
     * @param budgetSaveDto - The top level budget that will have a BUDGET record created/updated
     * @param budgetIdSubProjectNumberMap - budget to subproject information to be used for bulk create/update of subproject numbers
     * @param flattenedDtoList - A flattened list of all budgets in the original BudgetDTO tree structure
     */
    protected void saveBudgetRecursion(RecursiveBudgetSaveDTO budgetSaveDto,
            Map<Long, String> budgetIdSubProjectNumberMap, List<RecursiveBudgetSaveDTO> flattenedDtoList) {

        Budget budgetDto = budgetSaveDto.getBudget();
        Budget budgetModel = null;
        if (budgetDto.getId() != null) {
            //Need to pull the existing budget from the database so it contains create/update data
            budgetModel = cnxtBudgetRepository.findOne(budgetSaveDto.getBudget().getId());

            //Update any fields on the DB model that are editable
            //Fields that are not currently editable: budgetCategoryCode, budgetConstraint, budgetTypeCode, parentBudgetId, subProjectId(done elsewhere)
            //Fields that are not currently used: alertAmount, awardType, budgetDesc
            budgetModel.setBudgetCode(budgetDto.getBudgetCode());
            budgetModel.setBudgetName(budgetDto.getBudgetName());
            budgetModel.setFromDate(budgetDto.getFromDate());
            budgetModel.setIndividualGivingLimit(budgetDto.getIndividualGivingLimit());
            budgetModel.setStatusTypeCode(budgetDto.getStatusTypeCode() == null ? StatusTypeCode.ACTIVE.name() : budgetDto.getStatusTypeCode());
            budgetModel.setThruDate(budgetDto.getThruDate());
        } else {
            budgetModel = budgetDto; //Brand new budget
            budgetModel.setStatusTypeCode(StatusTypeCode.ACTIVE.name()); //New budgets will always be active
        }

        cnxtBudgetRepository.save(budgetModel);

        // handle new sub project number creation
        String subProjectNumber = budgetSaveDto.getSubProjectNumber();
        if (subProjectNumber == null || subProjectNumber.trim().isEmpty()) {
            Long budgetId = budgetSaveDto.getBudget().getId();
            // autogenerate using budget id left padded with zeroes
            subProjectNumber = String.format(BudgetConstants.SUB_PROJECT_AUTO_GENERATE_FORMAT, budgetId.intValue());
            budgetIdSubProjectNumberMap.put(budgetId, subProjectNumber);
            budgetSaveDto.setSubProjectNumber(subProjectNumber);
        } else {
            budgetIdSubProjectNumberMap.put(budgetSaveDto.getBudget().getId(), budgetSaveDto.getSubProjectNumber());
        }
        flattenedDtoList.add(budgetSaveDto);

        if (budgetSaveDto.getChildren() != null && !budgetSaveDto.getChildren().isEmpty()) {
            for (RecursiveBudgetSaveDTO childBudget : budgetSaveDto.getChildren()) {
                if (childBudget.getBudget().getParentBudgetId() == null) {
                    childBudget.getBudget().setParentBudgetId(budgetModel.getId());
                }
                saveBudgetRecursion(childBudget, budgetIdSubProjectNumberMap, flattenedDtoList);
            }
        }
    }


    /**
     * Creates FUNDING records (TRANSACTION_HEADER and DISCRETIONARY) for a single budget
     * Uses debits/credits to handle simple budget funding as well as budget allocation
     *
     * @param budgetDto - Budget to save FUND transactions for
     */
    protected void updateBudgetAmount(RecursiveBudgetSaveDTO budgetDto) {
        //Total Amount
        Integer savedBudgetTotal = 0;
        savedBudgetTotal = cnxtBudgetRepository.getTotalBudget(budgetDto.getBudget().getId());
        if (savedBudgetTotal == null) {
            savedBudgetTotal = 0;
        }

        Double allocatedIn = budgetDto.getAllocatedIn();
        //totalAmount is allocatedIn - allocatedOut
        Double totalAmount = budgetDto.getAllocatedIn() - budgetDto.getAllocatedOut();

        //Budget amount has changed. Check if allocated in/out changed as well
        Double previousAllocatedIn = 0D;
        //These will be used for the discretionary record
        Long budgetIdDebit = null;
        Long budgetIdCredit = null;
        BudgetMisc previousAllocatedInMisc = budgetMiscDao.findBy()
                .where(ProjectConstants.VF_NAME).eq(VfName.ALLOCATED_IN.name())
                .and(ProjectConstants.BUDGET_ID).eq(budgetDto.getBudget().getId())
                .findOne();
        if (previousAllocatedInMisc != null && previousAllocatedInMisc.getMiscData() != null) {
            previousAllocatedIn =  Double.parseDouble(previousAllocatedInMisc.getMiscData());
        }

        //This will handle updating budget total for all budget types      
        if (allocatedIn > previousAllocatedIn) {
            budgetIdCredit = budgetDto.getBudget().getId();            
        } else if (allocatedIn < previousAllocatedIn) {
            budgetIdDebit = budgetDto.getBudget().getId();            
        }

        //This section handles budget allocation - moving points from one budget to another
        //Don't actually need to save allocated out - if we do we will get duplicate entries
        //Save the allocated IN entry (the reverse of that will be allocated out)
        if (!allocatedIn.equals(previousAllocatedIn)) {

            if (totalAmount == 0D) {
                //If there is no parentBudgetId, just a simple funding transaction
                if (budgetDto.getBudget().getParentBudgetId() == null) {
                    if (allocatedIn > previousAllocatedIn) {
                        budgetIdCredit = budgetDto.getBudget().getId();
                    } else if (allocatedIn < previousAllocatedIn) {
                        budgetIdDebit = budgetDto.getBudget().getId();
                    }
                } else {
                    //This section handles allocation that "skips" a level
                    if (allocatedIn > previousAllocatedIn) {
                        budgetIdCredit = budgetDto.getBudget().getId();
                        budgetIdDebit = budgetDto.getBudget().getParentBudgetId();
                    } else if (allocatedIn < previousAllocatedIn) {
                        budgetIdDebit = budgetDto.getBudget().getId();
                        budgetIdCredit = budgetDto.getBudget().getParentBudgetId();
                    }
                }
            }

            //Allocation Logic - one level at a time
            if (budgetDto.getBudget().getParentBudgetId() != null) {
            	
                if (budgetDto.getBudget().getBudgetTypeCode() != null && 
                	budgetDto.getBudget().getBudgetTypeCode().equals(BudgetConstants.BUDGET_TYPE_CODE_ALLOCATED)) {
                    if (allocatedIn > previousAllocatedIn) {
                        budgetIdCredit = budgetDto.getBudget().getId();
                        budgetIdDebit = budgetDto.getBudget().getParentBudgetId();
                    } else if (allocatedIn < previousAllocatedIn) {
                        budgetIdDebit = budgetDto.getBudget().getId();
                        budgetIdCredit = budgetDto.getBudget().getParentBudgetId();
                    }
                	
                }                
            }

            Double amount = Math.abs(allocatedIn - previousAllocatedIn);

            TransactionHeader transactionHeader = new TransactionHeader();
            transactionHeader.setTypeCode(BudgetConstants.BUDGET_TYPE);
            transactionHeader.setSubTypeCode(BudgetConstants.BUDGET_FUNDING);
            transactionHeader.setActivityDate(new Date());
            transactionHeader.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
            transactionHeader.setPerformanceValue(amount);
            transactionHeader.setProxyPaxId(budgetDto.getProxyPaxId());
            transactionHeaderDao.save(transactionHeader);

            Discretionary discretionary = new Discretionary();
            discretionary.setTransactionHeaderId(transactionHeader.getId());
            discretionary.setPerformanceDate(new Date());
            discretionary.setDescription(BudgetConstants.BUDGET_FUNDING_MESSAGE);
            discretionary.setBudgetIdDebit(budgetIdDebit);
            discretionary.setBudgetIdCredit(budgetIdCredit);
            discretionary.setAmount(amount);
            discretionaryDao.save(discretionary);
        }
    }

    protected BudgetMisc createBudgetMisc(Long budgetId, String vfName, String miscData) {
        BudgetMisc misc = new BudgetMisc();
        misc.setBudgetId(budgetId);
        misc.setVfName(vfName);
        misc.setMiscData(miscData);
        misc.setMiscDate(new Date());
        return misc;
    }

    protected BudgetMisc createNameMisc(Long budgetId, String vfName, EntityDTO entity) {
        String miscData = ProjectConstants.EMPTY_STRING;
        if (entity instanceof EmployeeDTO) {
            EmployeeDTO pax = (EmployeeDTO) entity;
            //Pull the name from the database to avoid SQL injection issues
            Pax dbPax = paxDao.findBy().where(ProjectConstants.PAX_ID).eq(pax.getPaxId()).findOne();
            miscData = dbPax.getFirstName() + ProjectConstants.SPACE + dbPax.getLastName();
        } else if (entity instanceof GroupDTO){
            //Pull group name from the database to avoid SQL injection issues
            GroupDTO group = (GroupDTO) entity;
            Groups dbGroup = groupsDao.findBy().where(ProjectConstants.GROUP_ID).eq(group.getGroupId()).findOne();
            miscData = dbGroup.getGroupDesc();
        }
        return createBudgetMisc(budgetId, vfName, miscData);
    }

    protected Acl createBudgetAcl(Long roleId, Long budgetId, EntityDTO entity) {
        Acl acl = new Acl();
        acl.setRoleId(roleId);
        acl.setTargetTable(TableName.BUDGET.name());
        acl.setTargetId(budgetId);
        if (entity instanceof EmployeeDTO) {
            EmployeeDTO pax = (EmployeeDTO) entity;
            acl.setSubjectTable(TableName.PAX.name());
            acl.setSubjectId(pax.getPaxId());
        } else if (entity instanceof GroupDTO){
            GroupDTO group = (GroupDTO) entity;
            acl.setSubjectTable(TableName.GROUPS.name());
            acl.setSubjectId(group.getGroupId());
        }
        return acl;
    }

    /**
     * Populate BudgetDTO objects and create a list.
     *
     * @param resultList - List of raw budget results from database to be processed
     * @param sumChildBudgets - If TRUE, use VW_BUDGET_TOTALS. If FALSE, use VW_BUDGET_TOTALS_FLAT.
     * @return List of BudgetDTOs populated based on the results provided.
     */
    protected List<BudgetDTO> populateBudgetList(List<Map<String, Object>> resultList, Boolean sumChildBudgets, Boolean includeInactive) {

        List<BudgetDTO> budgetList = new ArrayList<>();

        List<Long> budgetIdList = new ArrayList<>();
        List<Long> paxIdsToResolveList = new ArrayList<>();
        List<Long> groupIdsToResolveList = new ArrayList<>(); // this won't be used until we get to budget users

        Map<Long, Long> budgetIdToAllocatorIdMap = new HashMap<>();

        for (Map<String, Object> result : resultList) {
            BudgetDTO budget = new BudgetDTO();

            Long budgetId = (Long) result.get(ProjectConstants.ID);
            budgetIdList.add(budgetId); // Shouldn't have duplicates here.  Not a huge issue if we do, though

            budget.setBudgetId(budgetId);
            budget.setParentBudgetId((Long) result.get(ProjectConstants.PARENT_BUDGET_ID));
            budget.setName((String) result.get(ProjectConstants.BUDGET_NAME));
            budget.setDisplayName((String) result.get(ProjectConstants.DISPLAY_NAME) != null ?
                    (String) result.get(ProjectConstants.DISPLAY_NAME) :        //VW_BUDGET_INFO_LIGHTWEIGHT uses DISPLAY_NAME
                    (String) result.get(ProjectConstants.BUDGET_DISPLAY_NAME)); //VW_BUDGET_INFO uses BUDGET_DISPLAY_NAME
            budget.setDescription((String) result.get(ProjectConstants.BUDGET_DESC));
            budget.setType((String) result.get(ProjectConstants.BUDGET_TYPE_CODE));
            budget.setAccountingId((String) result.get(ProjectConstants.SUB_PROJECT_NUMBER));
            budget.setIndividualGivingLimit((Double) result.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT));
            budget.setIndividualUsedAmount((Double) result.get(ProjectConstants.INDIVIDUAL_USED_AMOUNT));
            budget.setBudgetConstraint((String) result.get(ProjectConstants.BUDGET_CONSTRAINT));
            budget.setProjectNumber((String) result.get(ProjectConstants.PROJECT_NUMBER));

            budget.setFromDate(DateUtil.convertToUTCDate((Date) result.get(ProjectConstants.FROM_DATE)));
            if (result.get(ProjectConstants.THRU_DATE) != null) {
                budget.setThruDate(DateUtil.convertToUTCDate((Date) result.get(ProjectConstants.THRU_DATE)));
            }
            budget.setCreateDate(DateUtil.convertToUTCDate((Date) result.get(ProjectConstants.CREATE_DATE)));

            Long ownerPaxId = (Long) result.get(ProjectConstants.BUDGET_OWNER);
            if (ownerPaxId != null) {
                budget.setBudgetOwner(new EmployeeDTO().setPaxId(ownerPaxId)); // drop in placeholder
                if (!paxIdsToResolveList.contains(ownerPaxId)) {
                    paxIdsToResolveList.add(ownerPaxId);
                }
            }

            // Get the implied status for UI display
            String realStatus = ((String) result.get(ProjectConstants.BUDGET_STATUS) != null) ?
                    (String) result.get(ProjectConstants.BUDGET_STATUS) :        //UP_BUDGET_INFO_BY_ID uses BUDGET_STATUS (and STATUS_TYPE_CODE on budget owner)
                    (String) result.get(ProjectConstants.STATUS_TYPE_CODE);        //VW_BUDGET_INFO (both) use STATUS_TYPE_CODE
            String impliedStatus = StatusTypeUtil.getImpliedStatus(realStatus, budget.getFromDate(),
                    budget.getThruDate());
            budget.setStatus(impliedStatus);

            BudgetDetailsDTO budgetDetail = new BudgetDetailsDTO();

            budgetDetail.setCanAllocate(Boolean.FALSE); // This will be updated to TRUE later if the allocator is also a user
            Long allocatorPaxId = (Long) result.get(ProjectConstants.BUDGET_ALLOCATOR);
            if (allocatorPaxId != null) {
                budgetIdToAllocatorIdMap.put(budgetId, allocatorPaxId); // store this for later
            }

            if (result.get(ProjectConstants.ALLOCATED_IN) != null) {
                budgetDetail.setAllocatedIn(Double.parseDouble((String) result.get(ProjectConstants.ALLOCATED_IN)));
            }

            if (result.get(ProjectConstants.ALLOCATED_OUT) != null) {
                budgetDetail.setAllocatedOut(Double.parseDouble((String) result.get(ProjectConstants.ALLOCATED_OUT)));
            }

            budget.setBudgetDetails(budgetDetail);

            budget.setIsUsable(Boolean.valueOf((String) result.get(ProjectConstants.IS_USABLE)));

            budget.setOverrideProjectNumber((String) result.get(ProjectConstants.OVERRIDE_PROJECT_NUMBER));
            budget.setOverrideAccountingId((String) result.get(ProjectConstants.OVERRIDE_SUB_PROJECT_NUMBER));
            budget.setOverrideLocation((String) result.get(ProjectConstants.OVERRIDE_LOCATION));

            budgetList.add(budget);
        }        
        List<Map<String, Object>> budgetTotalsList;        
        if (includeInactive != null && includeInactive == true) {
        	budgetTotalsList = budgetInfoDao.getBudgetTotalsFlat(budgetIdList,includeInactive);
        }else {
        // handle TOTAL_AMOUNT (totalAmount) and TOTAL_USED (usedAmount)
        // We want to call VW_BUDGET_TOTALS vs. VW_BUDGET_TOTALS_FLAT depenidng on what endpoint is calling this method
        	budgetTotalsList = (sumChildBudgets)? budgetInfoDao.getBudgetTotals(budgetIdList) : budgetInfoDao.getBudgetTotalsFlat(budgetIdList);
        }

        Map<Long,Optional<BudgetTotalsFlatDTO>> mapTotals=budgetList.stream()
                .collect(Collectors.toMap(id -> id.getBudgetId() , id -> Optional.empty()));

        budgetTotalsList.forEach(item ->
                mapTotals.computeIfPresent( Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()), (i, o)->o.isPresent()? o: Optional.of(
                        new BudgetTotalsFlatDTO(Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()),
                                Double.parseDouble(item.get(ProjectConstants.TOTAL_AMOUNT).toString()),
                                Double.parseDouble(item.get(ProjectConstants.TOTAL_USED).toString())
                        ))));
        for(ListIterator<Long> it=budgetIdList.listIterator(budgetIdList.size()); it.hasPrevious();) {
            mapTotals.get(it.previous()).ifPresent(item -> {
                budgetList.get(it.nextIndex()).setTotalAmount(item.getTotalAmount()).setUsedAmount(item.getTotalUsed());
            });
        }

        // Handle IS_USED (usedInProgram)
        List<Map<String, Object>> budgetUsedList = budgetInfoDao.getBudgetUsed(budgetIdList);

        Map<Long,Optional<BudgetIsUsedDTO>> mapAreUsed=budgetList.stream()
                .collect(Collectors.toMap(id -> id.getBudgetId() , id -> Optional.empty()));

        budgetUsedList.forEach(item ->
                mapAreUsed.computeIfPresent( Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()), (i, o)->o.isPresent()? o: Optional.of(
                        new BudgetIsUsedDTO(Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()),
                                Boolean.parseBoolean(item.get(ProjectConstants.IS_USED).toString()))
                        )));
        for(ListIterator<Long> it=budgetIdList.listIterator(budgetIdList.size()); it.hasPrevious();) {
            mapAreUsed.get(it.previous()).ifPresent(item -> {
                budgetList.get(it.nextIndex()).setUsedInProgram(item.isUsed());
            });
        }

        // handle budget users
        //map of null values retrieved from the database.
        Map<String,Object> nullRetrievedRow = new HashMap<>();
        nullRetrievedRow.put("budgetId", null);
        nullRetrievedRow.put("budgetUserType", null);
        nullRetrievedRow.put("budgetUserId", null);

        //getBudgetUsers and filter rows with null values
        List<Map<String, Object>> budgetUsersList = budgetInfoDao.getBudgetUsers(budgetIdList).parallelStream().filter(item -> !item.equals(nullRetrievedRow)).collect(Collectors.toList());

        //mapUsers is being used as a 'complete' list of ids, optional<BudgetUserDTO> based on the budgetList object
        Map<Long,Optional<BudgetUserDTO>> mapUsers=budgetList.stream()
                .collect(Collectors.toMap(id -> id.getBudgetId() , id -> Optional.empty()));

        //for each retrieved row, compare against mapUsers.
        // if budgetUsersList contains an id that is present in mapUsers then optional empty will be replaced by optional<BudgetUserDTO>(item.values)
        budgetUsersList.forEach(item ->
                mapUsers.computeIfPresent( Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()), (i, o)->o.isPresent() ? o: Optional.of(
                        new BudgetUserDTO(Long.parseLong(item.get(ProjectConstants.BUDGET_ID).toString()),
                                item.get(ProjectConstants.BUDGET_USER_TYPE).toString(),
                                Long.parseLong(item.get(ProjectConstants.BUDGET_USER_ID).toString()))
                )));

        //iterate through budgetIdList and look for each id in mapUsers<long,optional<BudgedUserDTO>>
        //if present then populate budget details
        for(ListIterator<Long> it=budgetIdList.listIterator(budgetIdList.size()); it.hasPrevious();) {
            mapUsers.get(it.previous()).ifPresent(item -> {
                String budgetUserType = item.getBudgetUserType();
                Long budgetUserId = item.getBudgetUserId();

                // Make sure list exists
                List<EntityDTO> budgetUserList = budgetList.get(it.nextIndex()).getBudgetDetails().getBudgetUsers();
                if (budgetUserList == null) {
                    budgetUserList = new ArrayList<>();
                }

                // will either be GROUPS or PAX
                if (TableName.PAX.name().equals(budgetUserType)) {
                    budgetUserList.add(new EmployeeDTO().setPaxId(budgetUserId)); // drop in placeholder
                    if (!paxIdsToResolveList.contains(budgetUserId)) {
                        paxIdsToResolveList.add(budgetUserId);
                    }

                    // check for budget allocator
                    Long allocatorPaxId = budgetIdToAllocatorIdMap.get(item.getBudgetId());
                    if (allocatorPaxId != null && allocatorPaxId.equals(budgetUserId)) {
                        budgetList.get(it.nextIndex()).getBudgetDetails().setCanAllocate(Boolean.TRUE);
                    }
                } else {
                    // Groups can't currently be allocators
                    budgetUserList.add(new GroupDTO().setGroupId(budgetUserId)); // drop in placeholder
                    if (!groupIdsToResolveList.contains(budgetUserId)) {
                        groupIdsToResolveList.add(budgetUserId);
                    }
                }
                budgetList.get(it.nextIndex()).getBudgetDetails().setBudgetUsers(budgetUserList);
            });

        }

        // replace placeholders with full data
        List<EntityDTO> entityList = participantInfoDao.getInfo(paxIdsToResolveList, groupIdsToResolveList);
        for (EntityDTO entity : entityList) {
            if (entity instanceof GroupDTO) {
                GroupDTO group = (GroupDTO) entity;
                // only need to check budget users
                for (BudgetDTO budget : budgetList) {
                    List<EntityDTO> budgetUserList = budget.getBudgetDetails().getBudgetUsers();
                    if (!ObjectUtils.isEmpty(budgetUserList)) {
                        for (int i = 0; i < budgetUserList.size(); i++) {
                            if (budgetUserList.get(i) instanceof GroupDTO) {
                                GroupDTO groupUser = (GroupDTO) budgetUserList.get(i);
                                // check if Ids match, if so, swap
                                if (group.getGroupId().equals(groupUser.getGroupId())) {
                                    budgetUserList.set(i, group);
                                }
                            }
                        }
                    }
                }
            }
            else {
                // PAX
                EmployeeDTO pax = (EmployeeDTO) entity;
                for (BudgetDTO budget : budgetList) {
                    // handle budget owner
                    if (budget.getBudgetOwner() != null && pax.getPaxId().equals(budget.getBudgetOwner().getPaxId())) {
                        budget.setBudgetOwner(pax);
                    }
                    // handle budget users
                    List<EntityDTO> budgetUserList = budget.getBudgetDetails().getBudgetUsers();
                    if (!ObjectUtils.isEmpty(budgetUserList)) {
                        for (int i = 0; i < budgetUserList.size(); i++) {
                            if (budgetUserList.get(i) instanceof EmployeeDTO) {
                                EmployeeDTO groupPax = (EmployeeDTO) budgetUserList.get(i);
                                // check if Ids match, if so, swap
                                if (pax.getPaxId().equals(groupPax.getPaxId())) {
                                    budgetUserList.set(i, pax);
                                }
                            }
                        }
                    }
                }
            }
        }

        // clear out budget.budgetDetails if all fields are empty
        for (BudgetDTO budget : budgetList) {
            BudgetDetailsDTO details = budget.getBudgetDetails();
            if (details != null) {
                if (details.getAllocatedIn() == null && details.getAllocatedOut() == null
                        && CollectionUtils.isEmpty(details.getBudgetUsers())) {
                    budget.setBudgetDetails(null);
                }
            }
        }

        return budgetList;
    }

    /**
     * Obtain the total records on the result list.
     * @param resultList
     * @return
     */
    protected int getTotalResults(List<Map<String, Object>> resultList) {
        int totalResults = 0 ;

        if (!CollectionUtils.isEmpty(resultList)) {
            totalResults = (Integer) resultList.get(0).get(ProjectConstants.TOTAL_RESULTS_KEY);
        }
        return totalResults;
    }

    @Override
    public List<BudgetDTO> getIndividualEligibleBudgets(Long paxId, Boolean givingLimitOnly) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // validate paxId
        validatePaxSelfOrAdmin(paxId, errors);

        ErrorMessageException.throwIfHasErrors(errors);

        List<Map<String, Object>> eligibleBudgetsMap = 
        			budgetInfoDao.getEligibleBudgets(paxId, getCurrentBudgetIdList(null, paxId), givingLimitOnly);

        return populateBudgetList(eligibleBudgetsMap, false,null);
    }

    @Override
    public List<BudgetDTO> getEligibleBudgetsForProgram(Long paxId, Long programId) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // Validate giverPax
        validatePaxSelfOrAdmin(paxId, errors);

        // Validate programID
        if (programId == null) {
            errors.add(BudgetConstants.NULL_PROGRAM_ID_MESSAGE);
        } else {
            // programId must be a real program
            Program program = programRepository.findOne(programId);
            if (program == null) {
                errors.add(BudgetConstants.INVALID_PROGRAM_ID_MESSAGE);
            } else {
                // program must be ACTIVE (Check implied statuses as well)
                String impliedStatus = StatusTypeUtil.getImpliedStatus(program.getStatusTypeCode(),
                        program.getFromDate(), program.getThruDate());
                if (!StatusTypeCode.ACTIVE.name().equalsIgnoreCase(impliedStatus)) {
                    errors.add(BudgetConstants.INACTIVE_PROGRAM_MESSAGE);
                }
            }
        }

        // If there are errors, throw them before we continue
        ErrorMessageException.throwIfHasErrors(errors);

        // Get the pax's eligible budgets for the specified program
        List<Map<String, Object>> eligibleBudgetsMap =
        		budgetInfoDao.getEligibleBudgets(paxId, getCurrentBudgetIdList(programId, paxId), Boolean.FALSE);

        return populateBudgetList(eligibleBudgetsMap, false,null);
    }

    protected List<Long> getBudgetIdListFromCacheList(Long programId, Long paxId) {
        List<BudgetEligibilityCacheDTO> eligibleBudgetList = budgetEligibilityCacheService.getEligibilityForPax(paxId);

        List<Long> budgetIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(eligibleBudgetList)) {
            for (BudgetEligibilityCacheDTO eligibleBudget : eligibleBudgetList) {
                if (programId != null && !programId.equals(eligibleBudget.getProgramId())) {
                    continue; // skip
                }
                if (!budgetIdList.contains(eligibleBudget.getBudgetId())) {
                    budgetIdList.add(eligibleBudget.getBudgetId());
                }
            }
        }
        return budgetIdList;
    }

    /**
     * Validate an existing pax and only an admin can access budget's that are not their own.
     * @param paxId
     * @param errors
     */
    protected void validatePaxSelfOrAdmin (Long paxId, ArrayList<ErrorMessage> errors) {
        // giverPax must be a real paxId
        if (!paxRepository.exists(paxId)) {
            errors.add(BudgetConstants.INVALID_PAX_MESSAGE);
        }

        // Only admin users can call this service with giverPax
        if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE) && !security.isMyPax(paxId)) {
            errors.add(BudgetConstants.NOT_ADMIN_MESSAGE);
        }
    }
    
    /**
    * Get the budget list dynamically instead of getting them from cache.
    * @param paxId
    * @return List of budget ids the participant can give points from
    */
    protected List<Long> getCurrentBudgetIdList(Long programId, Long paxId) {   	
        List<BudgetEligibilityDTO> budgetEligibilityList = new ArrayList<BudgetEligibilityDTO>();
        //Get eligible budgets from this participant
        List<Map<String, Object>> eligibleBudgetList = budgetInfoDao.getBudgetListByPax(paxId);
        
        if (!ListUtils.isEmpty(eligibleBudgetList)) {
	        for(Map<String, Object> budget : eligibleBudgetList) {
	        	 BudgetEligibilityDTO budgets = new BudgetEligibilityDTO();
	        	 if(budget.get(ProjectConstants.BUDGET_ID) != null) {
	        		 budgets.setBudgetId((Long)budget.get(ProjectConstants.BUDGET_ID));
	        	 }
	        	 if(budget.get(ProjectConstants.PROGRAM_ID) != null) {
	        		 budgets.setProgramId((Long)budget.get(ProjectConstants.PROGRAM_ID));
	        	 }
	        	 budgetEligibilityList.add(budgets);
	        }
        }
              
        List<Long> budgetIdList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(eligibleBudgetList)) {
            for (BudgetEligibilityDTO eligibleBudget : budgetEligibilityList) {
                if (programId != null && !programId.equals(eligibleBudget.getProgramId())) {
                    continue; // skip
                }
                if (!budgetIdList.contains(eligibleBudget.getBudgetId())) {
                    budgetIdList.add(eligibleBudget.getBudgetId());
                }
            }
        }
        return budgetIdList;
      }
}
