package com.maritz.culturenext.budget.dao;

import java.util.Collection;
import java.util.List;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.SubProject;

public interface BudgetBulkDataManagementDao {

    /**
     * Will either update existing misc entries or create them if they do not exist
     * 
     * @param budgetMiscEntries - list of budget miscs to process
     */
    void saveBudgetMiscs(Collection<BudgetMisc> budgetMiscEntries);
    
    /**
     * Will either update the acl entries or create new ones if they do not exist (matches on role and target)
     * 
     * @param aclEntries - list of acl entries to process
     */
    void saveACLEntries(Collection<Acl> aclEntries);
    
    /**
     * Will insert missing groups pax
     * 
     * @param groupsPaxEntries list of groups pax entries to proces
     */
    void saveGroupsPaxEntries(Collection<GroupsPax> groupsPaxEntries);
    
    /**
     * Removes all pax from the budget allocator group who do not have an ACL entry for allocator for any budget
     */
    void cleanUpBudgetAllocatorGroupMembership();
    
    /**
     * Removes all pax from the budget owner group who do not have an ACL entry for owner for any budget
     */
    void cleanUpBudgetOwnerGroupMembership();
    
    /**
     * returns all sub projects tied to the provided project number
     * 
     * @param projectNumber - project number to find sub projects for
     * @return List<SubProject> - list of sub project objects tied to that project number 
     */
    List<SubProject> findExistingSubProjects(String projectNumber);
    
    /**
     * creates sub project entries under the provided project id (if they do not already exist)
     * 
     * @param projectId - id of project to tie to
     * @param subProjectList - list of sub project numbers to create if they don't exist
     */
    void saveSubProjectEntries(Long projectId, Collection<String> subProjectList);
    
    /**
     * Clean up ACL entries for an specific role on budget list.
     * @param budgetId
     */
    void cleanUpAclBudgetRoleEntries(String role, List<Long> budgetId);
    
    
}
