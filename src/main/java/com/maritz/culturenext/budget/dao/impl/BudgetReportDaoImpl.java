package com.maritz.culturenext.budget.dao.impl;

import java.sql.Types;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.budget.dao.BudgetReportDao;

@Repository
public class BudgetReportDaoImpl extends AbstractDaoImpl implements BudgetReportDao {

    private static final String BUDGET_ID_LIST = "budgetIdList";
    private static final String BUDGET_OWNER_ID = "budgetOwnerId";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final String DIRECT_REPORT_LIST = "directReportList";
    private static final String BUDGET_STATUS_LIST = "statusList";
    
    private static final String BUDGET_GIVING_LIMIT_REPORT_QUERY_SPROC = "EXEC component.UP_BUDGET_GIVING_LIMIT_REPORT "
            + ":" + BUDGET_ID_LIST + ", :" + BUDGET_STATUS_LIST;

    private static final String BUDGET_STATS_QUERY_SPROC = "EXEC component.UP_BUDGET_STATS "
            + ":" + BUDGET_ID_LIST + ", :" + START_DATE + ", :" + END_DATE;

    private static final String MANAGER_BUDGET_STATS_QUERY_SPROC = "EXEC component.UP_MANAGER_BUDGET_STATS "
            + ":" + BUDGET_ID_LIST + ", :" + DIRECT_REPORT_LIST + ", :" + START_DATE + ", :" + END_DATE;

    private final String BUDGET_SUMMARY_REPORT_QUERY = "EXEC component.UP_BUDGET_SUMMARY_REPORT "
            + ":" + BUDGET_ID_LIST + ", :" + BUDGET_STATUS_LIST;
            
    private static final String BUDGET_OWNER_STATS_QUERY = 
            "WITH OWNED_BUDGETS (BUDGET_ID) AS ( " +
                "SELECT B.ID " +
                "FROM component.BUDGET B " +
                "INNER JOIN component.ACL A ON B.ID = A.TARGET_ID " +
                "INNER JOIN component.ROLE R ON A.ROLE_ID = R.ROLE_ID " +
                "WHERE R.ROLE_CODE = 'BUDGET_OWNER' " +
                "AND A.SUBJECT_ID = :" + BUDGET_OWNER_ID + " " +
                "UNION ALL " +
                "SELECT B.ID " +
                "FROM component.BUDGET B " +
                "INNER JOIN OWNED_BUDGETS OB ON B.PARENT_BUDGET_ID = OB.BUDGET_ID " +
            ") " +
            "SELECT SUM(BUDGET_AVAILABLE) AS 'BUDGET_AVAILABLE', SUM(TOTAL_USED) AS 'BUDGET_USED', STATUS_TYPE_CODE " +
            "FROM ( " +
                "SELECT v.BUDGET_ID, (v.TOTAL_AMOUNT - v.TOTAL_USED) AS 'BUDGET_AVAILABLE', v.TOTAL_USED, " +
                "CASE " +
                    "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
                    "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
                    "ELSE b.STATUS_TYPE_CODE " +
                "END AS 'STATUS_TYPE_CODE' " +
                "FROM OWNED_BUDGETS ob " +
                "JOIN component.VW_BUDGET_TOTALS_FLAT v ON v.BUDGET_ID = ob.BUDGET_ID " +
                "JOIN component.BUDGET b ON b.ID = v.BUDGET_ID " +
            ") DATA " +
            "GROUP BY STATUS_TYPE_CODE";
    
    @Override
    public List<Map<String, Object>> getBudgetStats(String budgetIdList, Date startDate, Date endDate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST, budgetIdList);
        params.addValue(START_DATE, startDate);
        params.addValue(END_DATE, endDate);

        return namedParameterJdbcTemplate.query(BUDGET_STATS_QUERY_SPROC, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getManagerBudgetStats(String directReportList, String budgetIdList, Date startDate,
            Date endDate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST, budgetIdList);
        params.addValue(DIRECT_REPORT_LIST, directReportList);
        params.addValue(START_DATE, startDate);
        params.addValue(END_DATE, endDate);

        return namedParameterJdbcTemplate.query(MANAGER_BUDGET_STATS_QUERY_SPROC, params, new CamelCaseMapRowMapper());
    }

    /**
     * Call stored-procedured to retrieve Budget Giving Limit report content.
     * @param budgetIdList: comma delimited budget id list.
     * @param statusList: comma delimited status list.
     */
    @Override
    public List<Map<String, Object>> getBudgetGivingLimit(String budgetIdList, String statusList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST, budgetIdList, Types.VARCHAR);
        params.addValue(BUDGET_STATUS_LIST, statusList, Types.VARCHAR);

        return namedParameterJdbcTemplate.query(BUDGET_GIVING_LIMIT_REPORT_QUERY_SPROC, params, new CamelCaseMapRowMapper());
    }

    // BUDGET REPORT SUMMARY    
    @Override
    public List<Map<String, Object>> getBudgetSummary(String budgetIdList, String statusList) {

        String queryBudgetSummaryReport = BUDGET_SUMMARY_REPORT_QUERY;

        MapSqlParameterSource params = new MapSqlParameterSource();
        
        params.addValue(BUDGET_ID_LIST, budgetIdList, Types.VARCHAR);
        params.addValue(BUDGET_STATUS_LIST, statusList, Types.VARCHAR);

        return namedParameterJdbcTemplate.query(queryBudgetSummaryReport, params, new CamelCaseMapRowMapper());        
    }

    @Override
    public List<Map<String, Object>> getBudgetOwnerStats(Long budgetOwnerId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_OWNER_ID, budgetOwnerId);

        return namedParameterJdbcTemplate.query(BUDGET_OWNER_STATS_QUERY, params, new CamelCaseMapRowMapper());
    }
}
