package com.maritz.culturenext.budget.dao.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.budget.dao.BudgetSearchDao;
import com.maritz.culturenext.constants.ProjectConstants;

@Repository
public class BudgetSearchDaoImpl extends AbstractDaoImpl implements BudgetSearchDao {
    
    private static final String BUDGET_DISPLAY_NAME_QUERY_PARAM = "BUDGET_DISPLAY_NAME";
    private static final String SCHEDULED_INCLUDED_QUERY_PARAM = "SCHEDULED_INCLUDED";
    private static final String ACTIVE_INCLUDED_QUERY_PARAM = "ACTIVE_INCLUDED";
    private static final String ENDED_INCLUDED_QUERY_PARAM = "ENDED_INCLUDED";
    private static final String ARCHIVED_INCLUDED_QUERY_PARAM = "ARCHIVED_INCLUDED";
    private static final String INCLUDE_CHILDREN_QUERY_PARAM = "INCLUDE_CHILDREN";
    private static final String HAS_GIVING_LIMIT_QUERY_PARAM = "HAS_GIVING_LIMIT";
    private static final String HAS_AMOUNT_QUERY_PARAM = "HAS_AMOUNT";
    private static final String BUDGET_OWNER_QUERY_PARAM = "BUDGET_OWNER_QUERY_PARAM";
    private static final String BUDGET_ALLOCATOR_QUERY_PARAM = "BUDGET_ALLOCATOR_QUERY_PARAM";
    private static final String START_PLACEHOLDER = "START";
    private static final String PAGE_SIZE_PLACEHOLDER = "PAGE_SIZE";
    private static final String BUDGET_TYPE_QUERY_PARAM = "BUDGET_TYPE";
    
    private static final String BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT_QUERY_SPROC = "EXEC UP_BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT "
            + ":" + BUDGET_OWNER_QUERY_PARAM 
            + ", :" + BUDGET_DISPLAY_NAME_QUERY_PARAM
            + ", :" + BUDGET_ALLOCATOR_QUERY_PARAM
            + ", :" + SCHEDULED_INCLUDED_QUERY_PARAM
            + ", :" + ACTIVE_INCLUDED_QUERY_PARAM
            + ", :" + ENDED_INCLUDED_QUERY_PARAM
            + ", :" + ARCHIVED_INCLUDED_QUERY_PARAM
            + ", :" + INCLUDE_CHILDREN_QUERY_PARAM
            + ", :" + HAS_GIVING_LIMIT_QUERY_PARAM
            + ", :" + HAS_AMOUNT_QUERY_PARAM
            + ", :" + START_PLACEHOLDER
            + ", :" + PAGE_SIZE_PLACEHOLDER
            + ", :" + BUDGET_TYPE_QUERY_PARAM;

    @Override
    public List<Map<String, Object>> getBudgetList(
            String searchString, String commaSeparatedStatusString, String listBudgetOwner, Boolean includeChildren,
            Boolean hasGivingLimit, Boolean hasAmount, String commaSeparatedAllocatorIds, Integer pageNumber, Integer pageSize, String budgetType
        ) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        if (searchString != null) {
            searchString = "%" + searchString + "%";
        }

        params.addValue(BUDGET_DISPLAY_NAME_QUERY_PARAM, searchString);

        // Resolve statuses.  Can't just do "where status in (list)" because SCHEDULED and ENDED are implied statuses
        if (!StringUtils.isBlank(commaSeparatedStatusString)) {
            List<String> statusList = Arrays.asList(commaSeparatedStatusString.split(ProjectConstants.COMMA_DELIM));
            params.addValue(
                    SCHEDULED_INCLUDED_QUERY_PARAM,
                    Boolean.toString(statusList.contains(StatusTypeCode.SCHEDULED.name()))
                );
            params.addValue(
                    ACTIVE_INCLUDED_QUERY_PARAM,
                    Boolean.toString(statusList.contains(StatusTypeCode.ACTIVE.name()))
                );
            params.addValue(
                    ENDED_INCLUDED_QUERY_PARAM,
                    Boolean.toString(statusList.contains(StatusTypeCode.ENDED.name()))
                );
            params.addValue(
                    ARCHIVED_INCLUDED_QUERY_PARAM,
                    Boolean.toString(statusList.contains(StatusTypeCode.ARCHIVED.name()))
                );
        } else {
            params.addValue(SCHEDULED_INCLUDED_QUERY_PARAM, null);
            params.addValue(ACTIVE_INCLUDED_QUERY_PARAM, null);
            params.addValue(ENDED_INCLUDED_QUERY_PARAM, null);
            params.addValue(ARCHIVED_INCLUDED_QUERY_PARAM, null);
        }

        
        params.addValue(INCLUDE_CHILDREN_QUERY_PARAM, Boolean.toString(Boolean.TRUE.equals(includeChildren)));
        params.addValue(HAS_GIVING_LIMIT_QUERY_PARAM, Boolean.toString(Boolean.TRUE.equals(hasGivingLimit)));
        params.addValue(HAS_AMOUNT_QUERY_PARAM, Boolean.toString(Boolean.TRUE.equals(hasAmount)));

        if (!StringUtils.isBlank(listBudgetOwner)) {
            params.addValue(BUDGET_OWNER_QUERY_PARAM, listBudgetOwner);
        } else {
            params.addValue(BUDGET_OWNER_QUERY_PARAM, null);
        }

        if (!StringUtils.isBlank(commaSeparatedAllocatorIds)) {
            params.addValue(BUDGET_ALLOCATOR_QUERY_PARAM, commaSeparatedAllocatorIds);
        } else {
            params.addValue(BUDGET_ALLOCATOR_QUERY_PARAM, null);
        }
        
        params.addValue(START_PLACEHOLDER, (pageNumber - 1) * pageSize);
        params.addValue(PAGE_SIZE_PLACEHOLDER, pageSize);
        params.addValue(BUDGET_TYPE_QUERY_PARAM, budgetType);

        return namedParameterJdbcTemplate.query(
                BUDGET_PAGINATED_SEARCH_LIGHTWEIGHT_QUERY_SPROC, params,  new CamelCaseMapRowMapper()
            );
    }
}
