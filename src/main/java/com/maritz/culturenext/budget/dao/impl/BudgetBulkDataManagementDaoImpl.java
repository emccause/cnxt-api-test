package com.maritz.culturenext.budget.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.SubProject;
import com.maritz.culturenext.budget.dao.BudgetBulkDataManagementDao;
import com.maritz.culturenext.constants.ProjectConstants;
@Component
public class BudgetBulkDataManagementDaoImpl extends AbstractDaoImpl implements BudgetBulkDataManagementDao {
    
    private static final String BUDGET_IDS_SQL_PARAM = "budgetIds";
    
    private static final String BUDGET_ID_PLACEHOLDER = "{BUDGET_ID}";
    private static final String VF_NAME_PLACEHOLDER = "{VF_NAME}";
    private static final String MISC_DATA_PLACEHOLDER = "{MISC_DATA}";
    private static final String ROLE_ID_PLACEHOLDER = "{ROLE_ID}";
    private static final String TARGET_TABLE_PLACEHOLDER = "{TARGET_TABLE}";
    private static final String TARGET_ID_PLACEHOLDER = "{TARGET_ID}";
    private static final String SUBJECT_TABLE_PLACEHOLDER = "{SUBJECT_TABLE}";
    private static final String SUBJECT_ID_PLACEHOLDER = "{SUBJECT_ID}";
    private static final String GROUP_ID_PLACEHOLDER = "{GROUP_ID}";
    private static final String PAX_ID_PLACEHOLDER = "{PAX_ID}";
    private static final String PROJECT_ID_PLACEHOLDER = "{PROJECT_ID}";
    private static final String SUB_PROJECT_NUMBER_PLACEHOLDER = "{SUB_PROJECT_NUMBER}";
    
    private static final String BUDGET_MISC_DATA_ENTRY_FRAGMENT =
            String.format("SELECT %s, %s, %s ", BUDGET_ID_PLACEHOLDER, VF_NAME_PLACEHOLDER, MISC_DATA_PLACEHOLDER);
    private static final String ACL_DATA_ENTRY_FRAGMENT = String.format("SELECT %s, %s, %s, %s, %s ",
                    ROLE_ID_PLACEHOLDER, TARGET_TABLE_PLACEHOLDER, TARGET_ID_PLACEHOLDER,
                    SUBJECT_TABLE_PLACEHOLDER, SUBJECT_ID_PLACEHOLDER
                );
    private static final String GROUPS_PAX_DATA_ENTRY_FRAGMENT = 
            String.format("SELECT %s, %s ", GROUP_ID_PLACEHOLDER, PAX_ID_PLACEHOLDER);
    private static final String SUB_PROJECT_DATA_ENTRY_FRAGMENT = 
            String.format("SELECT %s, %s ", PROJECT_ID_PLACEHOLDER, SUB_PROJECT_NUMBER_PLACEHOLDER);
    
    private static final String BUDGET_MISCS_DATA_PLACEHOLDER = "{BUDGET_MISC_DATA}";
    private static final String BUDGET_MISCS_MERGE_WRAPPER =
            "MERGE component.BUDGET_MISC AS TARGET " +
            "USING ( " + BUDGET_MISCS_DATA_PLACEHOLDER + " ) AS SOURCE (BUDGET_ID, VF_NAME, MISC_DATA) " +
            "ON (TARGET.BUDGET_ID = SOURCE.BUDGET_ID AND TARGET.VF_NAME = SOURCE.VF_NAME) " +
            "WHEN MATCHED THEN " +
                "UPDATE " +
                "SET MISC_DATA = SOURCE.MISC_DATA, MISC_DATE = GETDATE() " +
            "WHEN NOT MATCHED THEN " +
                "INSERT (BUDGET_ID, VF_NAME, MISC_DATA, MISC_DATE) " +
                "VALUES (SOURCE.BUDGET_ID, SOURCE.VF_NAME, SOURCE.MISC_DATA, GETDATE());";
    
    private static final String ACL_DATA_PLACEHOLDER = "{ACL_DATA}";
    private static final String ACL_MERGE_WRAPPER =
            "MERGE component.ACL AS TARGET " +
            "USING ( " + ACL_DATA_PLACEHOLDER + " ) AS SOURCE (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) " +
            "ON ( " +
                "TARGET.ROLE_ID = SOURCE.ROLE_ID " +
                "AND TARGET.TARGET_TABLE = SOURCE.TARGET_TABLE " +
                "AND TARGET.TARGET_ID = SOURCE.TARGET_ID " +
            ") " +
            "WHEN MATCHED AND SOURCE.SUBJECT_TABLE IS NOT NULL THEN " +
                "UPDATE " +
                "SET SUBJECT_TABLE = SOURCE.SUBJECT_TABLE, SUBJECT_ID = SOURCE.SUBJECT_ID " +
            "WHEN MATCHED AND SOURCE.SUBJECT_TABLE IS NULL THEN " +
                "DELETE " +
            "WHEN NOT MATCHED AND SOURCE.SUBJECT_TABLE IS NOT NULL THEN " +
                "INSERT (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) " +
                "VALUES (SOURCE.ROLE_ID, SOURCE.TARGET_TABLE, SOURCE.TARGET_ID, SOURCE.SUBJECT_TABLE, SOURCE.SUBJECT_ID);";
    
    private static final String GROUPS_PAX_DATA_PLACEHOLDER = "{GROUPS_PAX_DATA}";
    private static final String GROUPS_PAX_MERGE_WRAPPER = 
            "MERGE component.GROUPS_PAX AS TARGET " +
            "USING ( " + GROUPS_PAX_DATA_PLACEHOLDER + " ) AS SOURCE (GROUP_ID, PAX_ID) " +
            "ON ( " +
                "TARGET.GROUP_ID = SOURCE.GROUP_ID " +
                "AND TARGET.PAX_ID = SOURCE.PAX_ID " +
            ") " +
            "WHEN NOT MATCHED THEN  " +
                "INSERT (GROUP_ID, PAX_ID) " +
                "VALUES (SOURCE.GROUP_ID, SOURCE.PAX_ID);";
    
    private static final String BUDGET_ALLOCATOR_GROUP_DESC = "Budget Allocators";
    private static final String BUDGET_ALLOCATOR_ROLE_CODE = "BUDGET_ALLOCATOR";
    private static final String BUDGET_OWNER_GROUP_DESC = "Budget Owners";
    private static final String BUDGET_OWNER_ROLE_CODE = "BUDGET_OWNER";
    
    private static final String GROUP_DESC_PARAM = "GROUP_DESC";
    private static final String ROLE_CODE_PARAM = "ROLE_CODE";
    
    private static final String CLEAN_UP_BUDGET_GROUPS_BASE_QUERY =
            "DELETE FROM COMPONENT.GROUPS_PAX " +
            "WHERE GROUP_ID = ( " +
                "SELECT " +
                    "GROUP_ID " +
                "FROM COMPONENT.GROUPS " +
                "WHERE GROUP_TYPE_CODE='ROLE_BASED' AND GROUP_DESC = :" + GROUP_DESC_PARAM + " " +
            ") " +
                "AND PAX_ID NOT IN ( " +
                    "SELECT " +
                        "SUBJECT_ID " +
                    "FROM COMPONENT.ACL " +
                    "WHERE ROLE_ID = ( " +
                        "SELECT " +
                            "ROLE_ID " +
                        "FROM COMPONENT.ROLE " +
                        "WHERE ROLE_CODE = :" + ROLE_CODE_PARAM + " " +
                    ") " +
                    "AND SUBJECT_TABLE = 'PAX' " +
                    "UNION " +
                    "SELECT DISTINCT " +
                        "gp.PAX_ID " +
                    "FROM COMPONENT.ACL " +
                    "JOIN COMPONENT.GROUPS_PAX GP ON GP.GROUP_ID = ACL.SUBJECT_ID " +
                    "JOIN COMPONENT.GROUPS G ON G.GROUP_ID = GP.GROUP_ID " +
                    "WHERE ROLE_ID = ( " +
                        "SELECT " +
                            "ROLE_ID " +
                        "FROM COMPONENT.ROLE " +
                        "WHERE ROLE_CODE = :" + ROLE_CODE_PARAM + " " + 
                    ") " +
                    "AND SUBJECT_TABLE = 'GROUPS' " +
                    "AND GROUP_DESC <> :" + GROUP_DESC_PARAM + " " +
            ") ";
    
    private static final String PROJECT_NUMBER_PARAM = "PROJECT_NUMBER";
    private static final String SUB_PROJECT_LOOKUP_QUERY =
            "SELECT SUB.* FROM component.SUB_PROJECT SUB " +
            "LEFT JOIN component.PROJECT PRJ " +
            "ON SUB.PROJECT_ID = PRJ.ID " +
            "WHERE PRJ.PROJECT_NUMBER = :" + PROJECT_NUMBER_PARAM;

    private static final String SUB_PROJECT_DATA_PLACEHOLDER = "{SUB_PROJECT_DATA}";
    private static final String SUB_PROJECT_DATA_MERGE_WRAPPER = 
            "MERGE component.SUB_PROJECT AS TARGET " +
            "USING ( " + SUB_PROJECT_DATA_PLACEHOLDER + " ) AS SOURCE (PROJECT_ID, SUB_PROJECT_NUMBER) " +
            "ON ( " +
                "TARGET.PROJECT_ID = SOURCE.PROJECT_ID " +
                "AND TARGET.SUB_PROJECT_NUMBER = SOURCE.SUB_PROJECT_NUMBER " +
            ") " +
            "WHEN NOT MATCHED THEN  " +
                "INSERT (PROJECT_ID, SUB_PROJECT_NUMBER) " +
                "VALUES (SOURCE.PROJECT_ID, SOURCE.SUB_PROJECT_NUMBER); ";

    private static final String CLEAN_UP_BUDGET_ROLE_ACL_ENTRIES =
            "DELETE FROM component.ACL " +
            "WHERE ROLE_ID = ( " +
                        "SELECT " +
                            "ROLE_ID " +
                        "FROM component.ROLE " +
                        "WHERE ROLE_CODE = :" + ROLE_CODE_PARAM  + ") " +
            "AND TARGET_TABLE = 'BUDGET' " +
            "AND TARGET_ID IN (:" + BUDGET_IDS_SQL_PARAM + ")" ;
                    
    /**
     * Will either update existing misc entries or create them if they do not exist
     * 
     * @param budgetMiscEntries - list of budget miscs to process
     */
    @Override
    public void saveBudgetMiscs(Collection<BudgetMisc> budgetMiscEntries) {
        
        if (budgetMiscEntries == null || budgetMiscEntries.isEmpty()) {
            return; // nothing to see here
        }
        
        StringBuilder dataBuilder = new StringBuilder();
        
        Boolean appendUnion = false;
        for (BudgetMisc budgetMisc : budgetMiscEntries) {
            if (budgetMisc.getBudgetId() == null || budgetMisc.getVfName() == null) {
                continue; // uhh, what
            }
            String fragment = BUDGET_MISC_DATA_ENTRY_FRAGMENT;
            
            if (Boolean.TRUE.equals(appendUnion)) {
                dataBuilder.append(ProjectConstants.UNION_ALL_FRAGMENT);
            } else {
                appendUnion = Boolean.TRUE;
            }
            
            fragment = fragment.replace(BUDGET_ID_PLACEHOLDER, handleStringFormatting(budgetMisc.getBudgetId()));
            fragment = fragment.replace(VF_NAME_PLACEHOLDER, handleStringFormatting(budgetMisc.getVfName()));
            fragment = fragment.replace(MISC_DATA_PLACEHOLDER, handleStringFormatting(budgetMisc.getMiscData()));
            
            dataBuilder.append(fragment);
        }
        
        String query = BUDGET_MISCS_MERGE_WRAPPER.replace(BUDGET_MISCS_DATA_PLACEHOLDER, dataBuilder.toString());
        
        jdbcTemplate.execute(query);
    }

    /**
     * Will either update the acl entries or create new ones if they do not exist (matches on role and target)
     * 
     * @param aclEntries - list of acl entries to process
     */
    @Override
    public void saveACLEntries(Collection<Acl> aclEntries) {
        
        if (aclEntries == null || aclEntries.isEmpty()) {
            return; // nothing to see here
        }
        
        StringBuilder dataBuilder = new StringBuilder();
        
        Boolean appendUnion = false;
        for (Acl acl : aclEntries) {
            if (acl.getRoleId() == null || acl.getTargetTable() == null || acl.getTargetId() == null) {
                continue; // uhh, what
            }
            
            String fragment = ACL_DATA_ENTRY_FRAGMENT;
            
            if (Boolean.TRUE.equals(appendUnion)) {
                dataBuilder.append(ProjectConstants.UNION_ALL_FRAGMENT);
            } else {
                appendUnion = Boolean.TRUE;
            }
            
            fragment = fragment.replace(ROLE_ID_PLACEHOLDER, handleStringFormatting(acl.getRoleId()));
            fragment = fragment.replace(TARGET_TABLE_PLACEHOLDER, handleStringFormatting(acl.getTargetTable()));
            fragment = fragment.replace(TARGET_ID_PLACEHOLDER, handleStringFormatting(acl.getTargetId()));
            fragment = fragment.replace(SUBJECT_TABLE_PLACEHOLDER, handleStringFormatting(acl.getSubjectTable()));
            fragment = fragment.replace(SUBJECT_ID_PLACEHOLDER, handleStringFormatting(acl.getSubjectId()));
            
            dataBuilder.append(fragment);
        }
        
        String query = ACL_MERGE_WRAPPER.replace(ACL_DATA_PLACEHOLDER, dataBuilder.toString());
        
        jdbcTemplate.execute(query);
    }

    /**
     * Will insert missing groups pax
     * 
     * @param groupsPaxEntries list of groups pax entries to proces
     */
    @Override
    public void saveGroupsPaxEntries(Collection<GroupsPax> groupsPaxEntries) {
        
        if (groupsPaxEntries == null || groupsPaxEntries.isEmpty()) {
            return; // nothing to see here
        }
        
        StringBuilder dataBuilder = new StringBuilder();
        ArrayList<String> listPaxGroups = new ArrayList<String>();
        Boolean appendUnion = false;
        for (GroupsPax groupsPax : groupsPaxEntries) {
            if (groupsPax.getGroupId() == null || groupsPax.getPaxId() == null) {
                continue; // uhh, what
            }
            
            String stringGroupPax = groupsPax.getGroupId()+","+groupsPax.getPaxId();
            
            if (!listPaxGroups.contains(stringGroupPax)) {
                
                String fragment = GROUPS_PAX_DATA_ENTRY_FRAGMENT;
                
                listPaxGroups.add(stringGroupPax);
                
                if (Boolean.TRUE.equals(appendUnion)) {
                    dataBuilder.append(ProjectConstants.UNION_ALL_FRAGMENT);
                } else {
                    appendUnion = Boolean.TRUE;
                }
                
                fragment = fragment.replace(GROUP_ID_PLACEHOLDER, handleStringFormatting(groupsPax.getGroupId()));
                fragment = fragment.replace(PAX_ID_PLACEHOLDER, handleStringFormatting(groupsPax.getPaxId()));
                
                dataBuilder.append(fragment);
            }
        }
        
        String query = GROUPS_PAX_MERGE_WRAPPER.replace(GROUPS_PAX_DATA_PLACEHOLDER, dataBuilder.toString());
        
        jdbcTemplate.execute(query);
        
    }

    /**
     * Removes all pax from the budget allocator group who do not have an ACL entry for allocator for any budget
     */
    @Override
    public void cleanUpBudgetAllocatorGroupMembership() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_DESC_PARAM, BUDGET_ALLOCATOR_GROUP_DESC);
        params.addValue(ROLE_CODE_PARAM, BUDGET_ALLOCATOR_ROLE_CODE);
        
        namedParameterJdbcTemplate.update(CLEAN_UP_BUDGET_GROUPS_BASE_QUERY, params);
    }

    /**
     * Removes all pax from the budget owner group who do not have an ACL entry for owner for any budget
     */
    @Override
    public void cleanUpBudgetOwnerGroupMembership() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_DESC_PARAM, BUDGET_OWNER_GROUP_DESC);
        params.addValue(ROLE_CODE_PARAM, BUDGET_OWNER_ROLE_CODE);
        
        namedParameterJdbcTemplate.update(CLEAN_UP_BUDGET_GROUPS_BASE_QUERY, params);
    }

    /**
     * returns all sub projects tied to the provided project number
     * 
     * @param projectNumber - project number to find sub projects for
     * @return List<SubProject> - list of sub project objects tied to that project number 
     */
    @Override
    public List<SubProject> findExistingSubProjects(String projectNumber) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROJECT_NUMBER_PARAM, projectNumber);
        
        return namedParameterJdbcTemplate.query(SUB_PROJECT_LOOKUP_QUERY, params, new ConcentrixBeanRowMapper<SubProject>()
                .setBeanClass(SubProject.class)
                .setTable(jitBuilder.getTable(SubProject.class))
                .build());
        
    }

    /**
     * creates sub project entries under the provided project id (if they do not already exist)
     * 
     * @param projectId - id of project to tie to
     * @param subProjectList - list of sub project numbers to create if they don't exist
     */
    @Override
    public void saveSubProjectEntries(Long projectId, Collection<String> subProjectList) {
        if (projectId == null || subProjectList == null || subProjectList.isEmpty()) {
            return; // nothing to see here
        }
        
        StringBuilder dataBuilder = new StringBuilder();
        
        Boolean appendUnion = false;
        for (String subProjectNumber : subProjectList) {
            if (subProjectNumber == null || subProjectNumber.isEmpty()) {
                continue; // uhh, what
            }
            String fragment = SUB_PROJECT_DATA_ENTRY_FRAGMENT;
            
            if (Boolean.TRUE.equals(appendUnion)) {
                dataBuilder.append(ProjectConstants.UNION_ALL_FRAGMENT);
            } else {
                appendUnion = Boolean.TRUE;
            }
            
            fragment = fragment.replace(PROJECT_ID_PLACEHOLDER, handleStringFormatting(projectId));
            fragment = fragment.replace(SUB_PROJECT_NUMBER_PLACEHOLDER, handleStringFormatting(subProjectNumber));
            
            dataBuilder.append(fragment);
        }
        
        String query = SUB_PROJECT_DATA_MERGE_WRAPPER.replace(SUB_PROJECT_DATA_PLACEHOLDER, dataBuilder.toString());
        
        jdbcTemplate.execute(query);
        
    }
    
    private String handleStringFormatting(Object object) {
        if (object != null) {
            if (object instanceof String) {
                return String.format(ProjectConstants.SQL_STRING_WRAPPER, object.toString().replace("'", "''"));
            }
            return object.toString();
        }
        return ProjectConstants.NULL_VALUE;
    }

    @Override
    public void cleanUpAclBudgetRoleEntries(String role, List<Long> budgetId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ROLE_CODE_PARAM, role);
        params.addValue(BUDGET_IDS_SQL_PARAM, budgetId);
        
        namedParameterJdbcTemplate.update(CLEAN_UP_BUDGET_ROLE_ACL_ENTRIES, params);
    }

}
