package com.maritz.culturenext.budget.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface BudgetReportDao {

    public List<Map<String, Object>> getBudgetGivingLimit(String budgetIdList, String statusList);
    public List<Map<String, Object>> getBudgetStats(String budgetIdList, Date startDate, Date endDate);

    public List<Map<String, Object>> getManagerBudgetStats(String directReportList, String budgetIdList, Date startDate,
            Date endDate);
    public List<Map<String, Object>> getBudgetSummary(String budgetIdList, String statusList);
    public List<Map<String, Object>> getBudgetOwnerStats(Long budgetOwner);
    
}
