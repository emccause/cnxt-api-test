package com.maritz.culturenext.budget.dao;


import java.util.Date;
import java.util.List;
import java.util.Map;

import com.maritz.culturenext.budget.dto.BudgetEligibilityDTO;
import com.maritz.culturenext.enums.TableName;

public interface BudgetInfoDao {
    
    /**
     * Get Budget info details by ID, including children budgets.
     * @param budgetId
     * @return
     */
    List<Map<String, Object>> getBudgetInfoById(Long budgetId);
        
    /**
     * Get Budget Users data by a list of budget id
     * @param type participant's type (PAX or GROUPS)  
     * @param budgetUserIdList
     * @return
     */
    public Map<Long,Map<String, Object>> getBudgetUserDetails(TableName type, List<Long> budgetUserIdList);
    
    /**
     * This will return the budgetId and isUsed flag for the specified budgetId and all budgets under it in the tree
     * @param budgetId
     * @return
     */
    List<Map<String, Object>> getIsUsedInfo(Long budgetId);
    
    /**
     * Updates the status of the specified budgetIds to INACTIVE
     * @param budgetIds
     */
    void inactivateBudgets(List<Long> budgetIds);

    /**
     * Get eligible budgets and individual used for a specific participant.
     * Optionally narrow those results down to the budgets assigned to a specific program
     * @param paxId
     * @param budgetIdList
     * @param givingLimitOnly
     * @return
     */
    List<Map<String, Object>> getEligibleBudgets(Long paxId, List<Long> budgetIdList, Boolean givingLimitOnly);
    
    /**
     * Get a list of budgets that the specified pax are eligible to give from. 
     * Can be filtered by giving limit.
     * @param paxIdList - comma separated list of pax ids to pull budgets for
     * @param givingLimitOnly - 1 to only return ALLOCATED budgets and budgets with giving limits. 0 to return all budgets
     * @param includeEndedBudgets - 1 to include both ACTIVE and ENDED budgets. 0 to return only ACTIVE budgets.
     * @return
     */
    List<Map<String, Object>> getEligibleBudgetsForPax(String paxIdList, Boolean givingLimitOnly, Boolean includeEndedBudgets);
    
    /**
     * get info on budgets in list (for program service)
     * 
     * @param budgetIdList
     * @return
     */
    List<Map<String, Object>> getProgramBudgets(List<Long> budgetIdList);
    
    /**
     * get total of points used by provided pax in provided budget
     * 
     * @param paxId
     * @param budgetId
     * @return
     */
    Double getIndividualUsedAmount(Long paxId, Long budgetId);
    
    /**
     * Returns TOTAL_AMOUNT and TOTAL_USED for budgets provided (sums up over all sub-budgets too)
     * 
     * @param budgetIdList List of IDs to grab.  Leave null for ALL budgets
     * @return Map containing BUDGET_ID, TOTAL_AMOUNT, TOTAL_USED - one for each budget provided
     */
    List<Map<String, Object>> getBudgetTotals(List<Long> budgetIdList);
    
    /**
     * Returns TOTAL_AMOUNT and TOTAL_USED for budgets provided (does NOT sum up over all sub-budgets)
     * 
     * @param budgetIdList List of IDs to grab. Leave null for ALL budgets
     * @return Map containing BUDGET_ID, TOTAL_AMOUNT, TOTAL_USED - one for each budget provided
     */
    List<Map<String, Object>> getBudgetTotalsFlat(List<Long> budgetIdList);
    
    /**
     * Returns TOTAL_AMOUNT and TOTAL_USED for budgets provided included inactive budgets (does NOT sum up over all sub-budgets)
     * 
     * @param budgetIdList List of IDs to grab. Leave null for ALL budgets
     * @return Map containing BUDGET_ID, TOTAL_AMOUNT, TOTAL_USED - one for each budget provided
     */
    List<Map<String, Object>> getBudgetTotalsFlat(List<Long> budgetIdList, Boolean includeInactive);    
    /**
     * Calls VW_BUDGET_TOTALS for all budgets that have a PARENT_BUDGET_ID
     * matching the passed in budget ID. This will return the totals for the
     * entire budget tree (starting one level under the ID that is passed in)
     * 
     * @param budgetIdList List of budget IDs
     * @return Map containing BUDGET_ID, TOTAL_AMOUNT, TOTAL_USED
     */
    List<Map<String, Object>> getChildBudgetTotals(List<Long> budgetIdList);
    
    /**
     * Returns IS_USED for budgets provided (will check all sub-budgets too)
     * Returns TRUE if budget is tied to a program or has transactions created, else FALSE
     * 
     * @param budgetIdList List of IDs to grab.  Leave null for ALL budgets
     * @return Map containing BUDGET_ID, IS_USED, one for each budget provided
     */
    List<Map<String, Object>> getBudgetUsed(List<Long> budgetIdList);

    /**
     * Returns list of budget users for Ids provided
     * 
     * @param budgetIdList - List of IDs to grab.  Leave null for ALL budgets
     * @return Map containing BUDGET_ID, BUDGET_USER_TYPE, BUDGET_USER_ID, one for each budget provided
     */
    List<Map<String, Object>> getBudgetUsers(List<Long> budgetIdList);
    
    /**
     * Returns budget usage info filtered by paxId and date range
     * @param paxIdList - List of paxIds to filter on. Will return nothing if left blank
     * @param startDate - used to calculate in/out of period spending
     * @param endDate - used to calculate in/out of period spending
     * @return
     */
    List<Map<String, Object>> getBudgetUsedByPax(String paxIdList, Date startDate, Date endDate);
    
    
    /**
        * Returns a lists of Budgets the Participant can given points from
         * @param paxId - Participant identifier
         * @return List of budgetIds
         */
    List<Map<String, Object>> getBudgetListByPax(Long paxId);
}
