package com.maritz.culturenext.budget.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.ObjectUtils;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.TableName;


@Repository
public class BudgetInfoDaoImpl extends AbstractDaoImpl implements BudgetInfoDao {
    
	final Logger logger = LoggerFactory.getLogger(getClass());
    private static final String BUDGET_ID_PARAM = "BUDGET_ID_PARAM";
    private static final String BUDGET_LIST_PARAM = "BUDGET_LIST_PARAM";
    private static final String PAX_ID_PARAM = "PAX_ID_PARAM";
    private static final String BUDGET_ID_LIST_PARAM = "BUDGET_ID_LIST";
    private static final String GIVING_LIMIT_ONLY_PARAM = "GIVING_LIMIT_ONLY_PARAM";
    private static final String ACL_TABLE_PARAM = "ACL_TABLE_PARAM";
    private static final String BUDGET_USER_ID_LIST_PARAM = "BUDGET_USER_ID_LIST_PARAM";
    private static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST";
    private static final String START_DATE_SQL_PARAM = "START_DATE";
    private static final String END_DATE_SQL_PARAM = "END_DATE";
    private static final String INCLUDE_ENDED_BUDGETS_SQL_PARAM = "INCLUDE_ENDED_BUDGETS";

    private static final String BUDGET_INFO_BY_ID_QUERY_SPROC = "EXEC component.UP_BUDGET_INFO_BY_ID "
            + ":"+ BUDGET_ID_PARAM ;
    
    private static final String BUDGET_USERS_DETAILS_DATA_BY_ID_LIST_QUERY = 
            "SELECT V.* "
            + "FROM VW_USER_GROUPS_SEARCH_DATA V "
            + "WHERE ( '" + TableName.PAX.name() + "' = :" + ACL_TABLE_PARAM + " AND V.PAX_ID IN (:" + BUDGET_USER_ID_LIST_PARAM + ") )"
                + "OR ('" + TableName.GROUPS.name() + "' = :" + ACL_TABLE_PARAM + " AND V.GROUP_ID IN (:" + BUDGET_USER_ID_LIST_PARAM + ") )";


    private static final String CHECK_IS_USED_QUERY = 
            "SELECT b.ID, bu.IS_USED " +
            "FROM component.BUDGET b INNER JOIN component.VW_BUDGET_USED bu " +
            "on b.ID=bu.BUDGET_ID " +
            "LEFT JOIN component.BUDGET_MISC bm ON bm.BUDGET_ID = b.ID AND VF_NAME = 'TOP_LEVEL_BUDGET_ID' " +
            "WHERE b.ID = :" + BUDGET_ID_PARAM + " or b.PARENT_BUDGET_ID = :" + BUDGET_ID_PARAM +
            " or bm.MISC_DATA = :" + BUDGET_ID_PARAM;
    
    private static final String INACTIVATE_BUDGETS_QUERY = 
            "UPDATE BUDGET SET STATUS_TYPE_CODE = '" + StatusTypeCode.INACTIVE.name() + "' WHERE ID IN (:" + BUDGET_LIST_PARAM + ")";

    private static final String BUDGET_ELIGIBILITY_QUERY_SPROC = "EXEC component.UP_BUDGET_ELIGIBILITY "
            + ":"+ PAX_ID_PARAM + ", :" + BUDGET_ID_LIST_PARAM + ", :"+ GIVING_LIMIT_ONLY_PARAM ;
    
    private static final String PROGRAM_BUDGETS_QUERY = 
            "SELECT ID AS BUDGET_ID, BUDGET_NAME, DISPLAY_NAME, " +
            "BUDGET_TYPE_CODE, INDIVIDUAL_GIVING_LIMIT " +
            "FROM component.VW_BUDGET_INFO_LIGHTWEIGHT " +
            "WHERE ID in (:" + BUDGET_ID_PARAM + ")";
    
    private static final String INDIVIDUAL_USED_AMOUNT_QUERY = 
            "SELECT DISTINCT " +
            "(CASE " +
            "WHEN DEBITS.DEBITS is null then 0 " +
            "ELSE DEBITS.DEBITS " +
            "END) " +
            "- " +
            "(CASE " +
                "WHEN CREDITS.CREDITS is null then 0 " +
                "ELSE CREDITS.CREDITS " +
            "END) " +
            "AS 'TOTAL_USED' " +
            "FROM component.NOMINATION n " +
            "JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID " +
            "JOIN component.DISCRETIONARY disc ON disc.TARGET_ID = r.ID AND disc.TARGET_TABLE = 'RECOGNITION' " +
            "JOIN component.TRANSACTION_HEADER th ON disc.TRANSACTION_HEADER_ID = th.ID " +
                "AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED', 'REVERSED') " +
            "JOIN component.BUDGET b ON b.ID = disc.BUDGET_ID_DEBIT OR b.ID = disc.BUDGET_ID_CREDIT " +
            "LEFT JOIN  " +
            "( " +
                "SELECT disc_debit.BUDGET_ID_DEBIT AS 'ID', SUM(disc_debit.AMOUNT) AS 'DEBITS' " +
                "FROM component.NOMINATION n " +
                "JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID " +
                "JOIN component.DISCRETIONARY disc_debit ON disc_debit.TARGET_ID = r.ID AND disc_debit.TARGET_TABLE = 'RECOGNITION' " +
                "JOIN component.TRANSACTION_HEADER th ON disc_debit.TRANSACTION_HEADER_ID = th.ID "+
                    "AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED', 'REVERSED') " +
                "JOIN component.TRANSACTION_HEADER th_debit ON th_debit.ID = disc_debit.TRANSACTION_HEADER_ID "+
                    "AND th_debit.TYPE_CODE = 'DISC' AND th_debit.SUB_TYPE_CODE <> 'FUND' " +
                "WHERE N.SUBMITTER_PAX_ID = :"+ PAX_ID_PARAM + " " +
                "GROUP BY disc_debit.BUDGET_ID_DEBIT " +
            ") DEBITS " +
            "ON B.ID = DEBITS.ID " +
            "LEFT JOIN " +
            "( " +
                "SELECT disc_credit.BUDGET_ID_CREDIT AS 'ID', SUM(disc_credit.AMOUNT) AS 'CREDITS' " +
                "FROM component.NOMINATION n " +
                "JOIN component.RECOGNITION r ON r.NOMINATION_ID = n.ID " +
                "JOIN component.DISCRETIONARY disc_credit ON disc_credit.TARGET_ID = r.ID AND disc_credit.TARGET_TABLE = 'RECOGNITION' " +
                "JOIN component.TRANSACTION_HEADER th ON disc_credit.TRANSACTION_HEADER_ID = th.ID "+
                    "AND th.STATUS_TYPE_CODE NOT IN ('REJECTED', 'REJECTED_INACTIVE', 'REJECTED_RESTRICTED') " +
                "JOIN component.TRANSACTION_HEADER th_credit ON th_credit.ID = disc_credit.TRANSACTION_HEADER_ID "+
                    "AND th_credit.TYPE_CODE = 'DISC' AND th_credit.SUB_TYPE_CODE <> 'FUND' " +
                "WHERE N.SUBMITTER_PAX_ID = :"+ PAX_ID_PARAM + " " +
                "GROUP BY disc_credit.BUDGET_ID_CREDIT " +
            ") CREDITS " +
            "ON B.ID = CREDITS.ID " +
            "WHERE N.SUBMITTER_PAX_ID = :"+ PAX_ID_PARAM + " AND b.ID = :" + BUDGET_ID_PARAM;
    
    private String GET_BUDGET_TOTALS_QUERY =
            "SELECT bt.*, " +
            "CASE " +
                "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
                "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
                "ELSE b.STATUS_TYPE_CODE " +
            "END AS 'STATUS_TYPE_CODE' " +
            "FROM component.VW_BUDGET_TOTALS bt " +
            "JOIN component.BUDGET b " +
            "ON b.ID = bt.BUDGET_ID" +
            " WHERE bt.BUDGET_ID in (:" + BUDGET_ID_LIST_PARAM + ")";
    
    private String GET_BUDGET_TOTALS_FLAT_QUERY =
            "SELECT btf.*, " +
            "CASE " +
                "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
                "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
                "ELSE b.STATUS_TYPE_CODE " +
            "END AS  'STATUS_TYPE_CODE' " +
            "FROM component.VW_BUDGET_TOTALS_FLAT btf " +
            "JOIN component.BUDGET b ON b.ID = btf.BUDGET_ID " +
            "WHERE btf.BUDGET_ID IN (:" + BUDGET_ID_LIST_PARAM + ") " +
            "AND b.STATUS_TYPE_CODE <> 'INACTIVE'";
    
    private String GET_BUDGET_TOTALS_FLAT_QUERY_INCLUDE_INACTIVE =
            "SELECT btf.*, " +
            "CASE " +
                "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
                "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
                "ELSE b.STATUS_TYPE_CODE " +
            "END AS  'STATUS_TYPE_CODE' " +
            "FROM component.VW_BUDGET_TOTALS_FLAT btf " +
            "JOIN component.BUDGET b ON b.ID = btf.BUDGET_ID " +
            "WHERE btf.BUDGET_ID IN (:" + BUDGET_ID_LIST_PARAM + ") ";    

    private String GET_BUDGET_TOTALS_FLAT_QUERY_NO_BUDGET_IDS =
         "SELECT btf.*, " +
            "CASE " +
            "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
            "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
            "ELSE b.STATUS_TYPE_CODE " +
            "END AS  'STATUS_TYPE_CODE' " +
            "FROM component.VW_BUDGET_TOTALS_FLAT btf " +
            "JOIN component.BUDGET b ON b.ID = btf.BUDGET_ID " +
            "WHERE b.STATUS_TYPE_CODE <> 'INACTIVE'";
    
    private String GET_BUDGET_TOTALS_FLAT_QUERY_NO_BUDGET_IDS_INCLUDE_INACTIVE =
            "SELECT btf.*, " +
               "CASE " +
               "WHEN b.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
               "WHEN b.THRU_DATE < GETDATE() THEN 'ENDED' " +
               "ELSE b.STATUS_TYPE_CODE " +
               "END AS  'STATUS_TYPE_CODE' " +
               "FROM component.VW_BUDGET_TOTALS_FLAT btf " +
               "JOIN component.BUDGET b ON b.ID = btf.BUDGET_ID ";                     
    
    
    private String GET_CHILD_BUDGET_TOTALS_QUERY = 
            "SELECT b.PARENT_BUDGET_ID, SUM(TOTAL_USED) AS 'TOTAL_USED', SUM(TOTAL_AMOUNT) AS 'TOTAL_AMOUNT' " +
            "FROM component.BUDGET b " +
            "JOIN component.VW_BUDGET_TOTALS vbt ON vbt.BUDGET_ID = b.ID " +
            "WHERE b.PARENT_BUDGET_ID IN (:" + BUDGET_ID_LIST_PARAM + ") " +
            "GROUP BY b.PARENT_BUDGET_ID";
    
    private String GET_BUDGET_USED_QUERY =
            "SELECT * FROM component.VW_BUDGET_USED VBU " +
            " WHERE VBU.BUDGET_ID IN (:" + BUDGET_ID_LIST_PARAM + ") " ;

    private String GET_BUDGET_USERS_QUERY =
            "SELECT * FROM component.VW_BUDGET_USERS VBU " +
            " WHERE VBU.BUDGET_ID IN (:"+ BUDGET_ID_LIST_PARAM + ") " ;

    private String GET_ELIGIBLE_BUDGETS_FOR_PAX_QUERY = 
            "EXEC component.UP_GET_ELIGIBLE_BUDGETS_FOR_PAX :" + 
            PAX_ID_LIST_SQL_PARAM + ", :" + GIVING_LIMIT_ONLY_PARAM + ", :" + INCLUDE_ENDED_BUDGETS_SQL_PARAM;
    
    private String GET_BUDGET_USED_BY_PAX_QUERY = 
            "EXEC component.UP_BUDGET_USED_BY_PAX :" + PAX_ID_LIST_SQL_PARAM + ", :" + START_DATE_SQL_PARAM + ", :" + END_DATE_SQL_PARAM;    
    
    private String GET_BUDGET_LIST_BY_PAX_QUERY = 
    		"SELECT BUDGET_ID, PROGRAM_ID " +
    				"FROM component.VW_PAX_PROGRAM_BUDGET " +
    				"WHERE PAX_ID = :" + PAX_ID_PARAM + " ";
    /*
    private String GET_BUDGET_LIST_BY_PAX_QUERY = 
    		"SELECT VBU.BUDGET_ID, pm.PROGRAM_ID " +
    		    		"FROM component.VW_BUDGET_USERS VBU " +
    		    		"LEFT JOIN component.VW_BUDGET_TREE VBT on VBU.BUDGET_ID = VBT.BUDGET_ID " +
    		    		"LEFT JOIN component.PROGRAM_BUDGET pm ON (VBU.BUDGET_ID = pm.BUDGET_ID " +
    		    		"OR VBT.[ROOT] = pm.BUDGET_ID) " +
    		    		"WHERE BUDGET_USER_ID = :" + PAX_ID_PARAM + " ";
    		    		*/
    
    @Override
    public List<Map<String, Object>> getBudgetInfoById(Long budgetId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_PARAM, budgetId);

        return namedParameterJdbcTemplate.query(BUDGET_INFO_BY_ID_QUERY_SPROC, params, new CamelCaseMapRowMapper());
    }
    
    @Override
    public Map<Long,Map<String, Object>> getBudgetUserDetails(final TableName type, final List<Long> budgetUserIdList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ACL_TABLE_PARAM, type.name());
        params.addValue(BUDGET_USER_ID_LIST_PARAM, budgetUserIdList);

        return namedParameterJdbcTemplate.query(BUDGET_USERS_DETAILS_DATA_BY_ID_LIST_QUERY
                , params, new ResultSetExtractor<Map<Long,Map<String, Object>>>() {
            public Map<Long,Map<String, Object>> extractData(ResultSet rs) throws SQLException {
                Map<Long,Map<String, Object>> mapOfKeys = new HashMap<>();
                Long key = null;
                while (rs.next()) {
                    switch(type) {
                    case PAX: 
                        key = rs.getLong("PAX_ID");
                        break;
                    case GROUPS:
                        key = rs.getLong("GROUP_ID");
                        break;
                    default:
                        key = null;
                    }
                                  
                    mapOfKeys.put(key, new CamelCaseMapRowMapper().mapRow(rs, rs.getRow()));
                }
                return mapOfKeys;
            }
        });
    }

    
    @Override
    public List<Map<String, Object>> getIsUsedInfo(Long budgetId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_PARAM, budgetId);
        
        return namedParameterJdbcTemplate.query(CHECK_IS_USED_QUERY, params, new CamelCaseMapRowMapper());        
    }
    
    @Override
    public void inactivateBudgets(List<Long> budgetIdList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_LIST_PARAM, budgetIdList);

        namedParameterJdbcTemplate.update(INACTIVATE_BUDGETS_QUERY, params);
    }

    @Override
    public List<Map<String, Object>> getEligibleBudgets(Long paxId, List<Long> budgetIdList, Boolean givingLimitOnly) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PARAM, paxId);
        params.addValue(BUDGET_ID_LIST_PARAM, StringUtils.join(budgetIdList, ProjectConstants.COMMA_DELIM));
        params.addValue(GIVING_LIMIT_ONLY_PARAM, String.valueOf(givingLimitOnly));
        
        return namedParameterJdbcTemplate.query(BUDGET_ELIGIBILITY_QUERY_SPROC, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getProgramBudgets(List<Long> budgetIdList) {

        if (budgetIdList != null && budgetIdList.isEmpty()) {
            return new ArrayList<>();
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_PARAM, budgetIdList);

        return namedParameterJdbcTemplate.query(PROGRAM_BUDGETS_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public Double getIndividualUsedAmount(Long paxId, Long budgetId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PARAM, paxId);
        params.addValue(BUDGET_ID_PARAM, budgetId);

        try {
            return namedParameterJdbcTemplate.queryForObject(INDIVIDUAL_USED_AMOUNT_QUERY, params, Double.class);
        }
        catch (DataAccessException e) {
            return 0D;
        }
    }

    @Override
    public List<Map<String, Object>> getBudgetTotals(List<Long> budgetIdList) {
        if (budgetIdList != null && budgetIdList.isEmpty()) {
            return new ArrayList<>();
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST_PARAM, budgetIdList);

        return namedParameterJdbcTemplate.query(GET_BUDGET_TOTALS_QUERY, params, new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getBudgetTotalsFlat(List<Long> budgetIdList) {
        // note - original query retrieved all records when budgetIdList was null.
        String sqlQuery;                
        if (budgetIdList == null || budgetIdList.isEmpty()){
        	sqlQuery= GET_BUDGET_TOTALS_FLAT_QUERY_NO_BUDGET_IDS;
        	 MapSqlParameterSource params = new MapSqlParameterSource();
             params.addValue(BUDGET_ID_LIST_PARAM, budgetIdList);
             // note - original query retrieved all records when budgetIdList was null.
             return namedParameterJdbcTemplate.query(sqlQuery, params, new CamelCaseMapRowMapper());
        }else {
        	List<Map<String, Object>> result = new ArrayList<>();
        	try{
	        	sqlQuery= GET_BUDGET_TOTALS_FLAT_QUERY;
		        //Break the number of parameters in groups of 2000 for each "in" operator. 
		        //Sql server max param is 2100, so this keeps us safely below that
		        for (Collection<Long> budgetListpartition : Iterables.partition(budgetIdList, ProjectConstants.SQL_PARAM_COUNT_MAX)) {
		        	MapSqlParameterSource params = new MapSqlParameterSource();
		        	params.addValue(BUDGET_ID_LIST_PARAM, budgetListpartition);
		        	result.addAll(namedParameterJdbcTemplate.query(sqlQuery, params, new CamelCaseMapRowMapper()));        	
		        }
        	}catch(Exception e){
        		logger.error("Exception when pulling Budget Totals Flat.", e);
        	}
	        return result;
        }
        
    }
    
    public List<Map<String, Object>> getBudgetTotalsFlat(List<Long> budgetIdList, Boolean includeInactiveBudgets) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST_PARAM, budgetIdList);
        String sqlQuery = (budgetIdList == null || budgetIdList.size() == 0)  ?
        		GET_BUDGET_TOTALS_FLAT_QUERY_NO_BUDGET_IDS_INCLUDE_INACTIVE : GET_BUDGET_TOTALS_FLAT_QUERY_INCLUDE_INACTIVE;
        return namedParameterJdbcTemplate.query(sqlQuery, params, new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getChildBudgetTotals(List<Long> budgetIdList) {
        
        if (ObjectUtils.isEmpty(budgetIdList)) {
            return new ArrayList<>();
        }
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST_PARAM, StringUtils.join(budgetIdList, ProjectConstants.COMMA_DELIM));
        
        return namedParameterJdbcTemplate.query(GET_CHILD_BUDGET_TOTALS_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getBudgetUsed(List<Long> budgetIdList) {

        if (ObjectUtils.isEmpty(budgetIdList)) {
            return new ArrayList<>();
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST_PARAM, budgetIdList);

        return namedParameterJdbcTemplate.query(GET_BUDGET_USED_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getBudgetUsers(List<Long> budgetIdList) {

        if (ObjectUtils.isEmpty(budgetIdList)) {
            return new ArrayList<>();
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID_LIST_PARAM, budgetIdList);

        return namedParameterJdbcTemplate.query(GET_BUDGET_USERS_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getEligibleBudgetsForPax(String paxIdList, Boolean givingLimitOnly, Boolean includeEndedBudgets) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_LIST_SQL_PARAM, paxIdList);
        params.addValue(GIVING_LIMIT_ONLY_PARAM, givingLimitOnly);
        params.addValue(INCLUDE_ENDED_BUDGETS_SQL_PARAM, includeEndedBudgets);

        return namedParameterJdbcTemplate.query(GET_ELIGIBLE_BUDGETS_FOR_PAX_QUERY, params, new CamelCaseMapRowMapper());

    }
    
    @Override
    public List<Map<String, Object>> getBudgetUsedByPax(String paxIdList, Date startDate, Date endDate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_LIST_SQL_PARAM, paxIdList);
        params.addValue(START_DATE_SQL_PARAM, startDate);
        params.addValue(END_DATE_SQL_PARAM, endDate);

        return namedParameterJdbcTemplate.query(GET_BUDGET_USED_BY_PAX_QUERY, params, new CamelCaseMapRowMapper());

    }
    
    @Override
    public List<Map<String, Object>> getBudgetListByPax(Long paxId){
       	MapSqlParameterSource params = new MapSqlParameterSource();
       	params.addValue(PAX_ID_PARAM, paxId);
        	
       	return namedParameterJdbcTemplate.query(GET_BUDGET_LIST_BY_PAX_QUERY,params, new CamelCaseMapRowMapper());
    }
}