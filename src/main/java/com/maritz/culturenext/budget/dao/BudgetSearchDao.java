package com.maritz.culturenext.budget.dao;

import java.util.List;
import java.util.Map;

public interface BudgetSearchDao {
    
    /**
     * Method to retrieve from database a page of budget info based on the provided info
     * 
     * @param searchString - name of budget(s) you're looking for, will return budgets containing this string
     * @param commaSeparatedStatusString - comma separated list of statuses to process.  Currently supported are
     *             SCHEDULED, ACTIVE, ENDED,  ARCHIVED
     * @param lstBudgetOwner - comma separated list of budget owner pax ids.
     * @param includeChildren - indicates if child budgets should be returned in the search.
     * @param hasGivingLimit - if it is true, filter budgets where individual limit has been set.
     * @param hasAmount - if it is true, filter budgets where has amount.
     * @param commaSeparatedAllocatorIds - comma separated list of allocator pax ids.
     * @param pageNumber - number of page to return.
     * @param pageSize - size of page to return.
     * 
     * @return Page of raw data
     */
    List<Map<String, Object>> getBudgetList(
                String searchString, String commaSeparatedStatusString, String lstBudgetOwner,
                Boolean includeChildren, Boolean hasGivingLimit, Boolean hasAmount, String commaSeparatedAllocatorIds,
                Integer pageNumber, Integer pageSize, String budgetType
            );
}
