package com.maritz.culturenext.budget.util;

import java.util.Comparator;

import com.maritz.culturenext.budget.dto.ApprovalDTO;
import com.maritz.culturenext.enums.ApprovalTypeEnum;

public class ApprovalTypeComparator implements Comparator<ApprovalDTO> {

    @Override
    public int compare(ApprovalDTO approval, ApprovalDTO approvalCompare) {
        final int EQUAL = 0;
        
        //Validate Approval duplication if approval type is equal and it is other than PERSON,
        //for PERSON type, paxId needs to be verified as well.
        if(approval.getType().equals(approvalCompare.getType())){
            if(approval.getType().equals(ApprovalTypeEnum.PERSON.getType()) 
                    && approval.getPax() != null && approval.getPax().getPaxId().equals(approvalCompare.getPax().getPaxId())){
                return EQUAL;
            }
            else if(!approval.getType().equals(ApprovalTypeEnum.PERSON.getType())){
                return EQUAL;
            }
            else if( approval.getPax()!= null && approval.getPax().getPaxId()!=null 
                    && approvalCompare.getPax()!= null && approvalCompare.getPax().getPaxId()!=null) 
                return approval.getPax().getPaxId().compareTo(approvalCompare.getPax().getPaxId());
        }
        
        return approval.getType().compareTo(approvalCompare.getType());
    }
}
