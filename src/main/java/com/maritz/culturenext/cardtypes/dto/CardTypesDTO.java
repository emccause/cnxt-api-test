package com.maritz.culturenext.cardtypes.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardTypesDTO {

    private Long cardTypeId;
    private String cardTypeCode;
    private String cardTypeDesc;
    private Integer displaySequence;
    
    public Long getCardTypeId() {
        return cardTypeId;
    }
    public void setCardTypeId(Long cardTypeId) {
        this.cardTypeId = cardTypeId;
    }
    public String getCardTypeCode() {
        return cardTypeCode;
    }
    public void setCardTypeCode(String cardTypeCode) {
        this.cardTypeCode = cardTypeCode;
    }
    public String getCardTypeDesc() {
        return cardTypeDesc;
    }
    public void setCardTypeDesc(String cardTypeDesc) {
        this.cardTypeDesc = cardTypeDesc;
    }
    public Integer getDisplaySequence() {
        return displaySequence;
    }
    public void setDisplaySequence(Integer displaySequence) {
        this.displaySequence = displaySequence;
    }

}
