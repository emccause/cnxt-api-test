package com.maritz.culturenext.cardtypes.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.cardtypes.dto.CardTypesDTO;
import com.maritz.culturenext.cardtypes.services.CardTypesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/participants", description = "all calls card types for participants")
@RequestMapping("participants")
public class CardTypesRestService {

    @Inject private CardTypesService cardTypesService;
    @Inject private Security security;

    //rest/participants/~/card-types
    @RequestMapping(method = RequestMethod.GET, value = "{paxId}/card-types")
    @ApiOperation(value = "Returns the list of card types for participant")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the list of card types for participant", 
                    response = CardTypesDTO.class),
                    @ApiResponse(code = 400, message = "Request error"),
                    @ApiResponse(code = 404, message = "No content")
    })
    @Permission("PUBLIC")
    public List<CardTypesDTO> getRecognitionRecipients(
            @ApiParam(value = "PaxId to retrieve card types") @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        return cardTypesService.getCardTypesForPaxId(paxId);
    }

}
