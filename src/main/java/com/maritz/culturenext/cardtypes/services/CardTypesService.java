package com.maritz.culturenext.cardtypes.services;

import java.util.List;

import com.maritz.culturenext.cardtypes.dto.CardTypesDTO;

public interface CardTypesService {
    
    /**
     * TODO javadocs
     * 
     * @param paxId
     * @return
     */
    List<CardTypesDTO> getCardTypesForPaxId(Long paxId);
}