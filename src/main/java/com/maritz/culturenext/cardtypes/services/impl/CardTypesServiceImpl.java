package com.maritz.culturenext.cardtypes.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.maritz.culturenext.cardtypes.dao.CardTypesDao;
import com.maritz.culturenext.cardtypes.dto.CardTypesDTO;
import com.maritz.culturenext.cardtypes.services.CardTypesService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class CardTypesServiceImpl implements CardTypesService {
    
    @Inject private CardTypesDao cardTypesDao;
    
    @Override
    public List<CardTypesDTO> getCardTypesForPaxId(Long paxId) {
        
            List<Map<String, Object>> cardTypesList = cardTypesDao.getCardTypesForPaxId(paxId);
            List<CardTypesDTO> cardTypesDtoList = new ArrayList<CardTypesDTO>();
        
            if ( !CollectionUtils.isEmpty(cardTypesList) ) {
                for (Map<String, Object> node :  cardTypesList) {
                    CardTypesDTO cardTypesDto = new CardTypesDTO();
                    cardTypesDtoList.add(this.populateCardTypesDTO(cardTypesDto, node));
                }
            }
            
            return cardTypesDtoList;
        
    }

    private CardTypesDTO populateCardTypesDTO(CardTypesDTO cardTypeDto, Map<String, Object> node) {
        cardTypeDto.setCardTypeId((Long)node.get(ProjectConstants.CARD_TYPE_ID));
        cardTypeDto.setCardTypeCode((String)node.get(ProjectConstants.CARD_TYPE_CODE));
        cardTypeDto.setCardTypeDesc((String)node.get(ProjectConstants.CARD_TYPE_DESC));
        cardTypeDto.setDisplaySequence((Integer)node.get(ProjectConstants.DISPLAY_SEQUENCE));
        return cardTypeDto;
    }

}
