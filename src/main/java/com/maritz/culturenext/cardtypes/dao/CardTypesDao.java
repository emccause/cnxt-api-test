package com.maritz.culturenext.cardtypes.dao;

import java.util.List;
import java.util.Map;

public interface CardTypesDao {
    List<Map<String, Object>> getCardTypesForPaxId(Long paxId);
}
