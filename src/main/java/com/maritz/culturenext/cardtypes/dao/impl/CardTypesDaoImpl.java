package com.maritz.culturenext.cardtypes.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.cardtypes.dao.CardTypesDao;

@Repository
public class CardTypesDaoImpl extends AbstractDaoImpl implements CardTypesDao {
    
    private static final String PAX_ID_SQL_PARAM = "paxId";
    private static final String CARD_TYPES_BY_PAX_ID_QUERY = 
            "SELECT "+
            " CT.CARD_TYPE_ID "+
            ",CT.CARD_TYPE_CODE "+
            ",CT.CARD_TYPE_DESC "+
            ",CT.DISPLAY_SEQUENCE "+
            "FROM PAX_ACCOUNT PA "+
            "LEFT JOIN CARD_TYPE CT ON  CT.CARD_TYPE_CODE= PA.CARD_TYPE_CODE "+
            "WHERE PA.PAX_ID = :" + PAX_ID_SQL_PARAM + " "+ 
            "AND STATUS_TYPE_CODE='"+ StatusTypeCode.ACTIVE.name()+"' "+
            "ORDER BY DISPLAY_SEQUENCE ";
    
    @Override
    public List<Map<String, Object>> getCardTypesForPaxId(Long paxId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);

        return namedParameterJdbcTemplate.query(CARD_TYPES_BY_PAX_ID_QUERY, params, new CamelCaseMapRowMapper());
    }
}
