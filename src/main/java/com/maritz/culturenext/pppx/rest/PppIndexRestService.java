package com.maritz.culturenext.pppx.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.List;

import javax.inject.Inject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;
import com.maritz.culturenext.pppx.services.PppIndexService;

@RestController
@Api(value = "ppp-index", description = "Endpoints for PPP Index records in the database.")
public class PppIndexRestService {
    
    @Inject private PppIndexService pppIndexService;
    
    // Service endpoint paths
    public static final String PPP_INDEX_PATH = "ppp-index";
    public static final String PPP_INDEX_BY_ID_PATH = "ppp-index/{" + ID_REST_PARAM + "}";
    
    /**
     * Returns a list of all PPP Index Records
     */
    //rest/ppp-index
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = PPP_INDEX_PATH, method = RequestMethod.GET)
    @ApiOperation(value="Returns a list of all configured PPP indexes in the database.", 
            notes="This will be used to returns a list of all configured PPP indexes in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of PPP Index values", 
                    response = PppIndexDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PppIndexDTO> getPppIndexData(){
        return pppIndexService.getPppIndexData();
    }
    
    /**
     * Creates a new PPP Index record
     */
    //rest/ppp-index
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = PPP_INDEX_PATH, method = RequestMethod.POST)
    @ApiOperation(value="Saves a new PPP index record in the database.", 
            notes="This will be used to save a new PPP index record in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "PPP Index record successfully saved", 
                    response = PppIndexDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PppIndexDTO> createPppIndexData(
            @ApiParam(value="PPP Index to be saved", required = false)
            @RequestBody PppIndexDTO pppIndex
            ) throws Throwable {
        pppIndexService.createPppIndexData(pppIndex.getCountryId(), pppIndex.getPppValue());
        
        return pppIndexService.getPppIndexData();
    }

    /**
     * Updates all PPP Index Records
     */
    //rest/ppp-index/~
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = PPP_INDEX_PATH, method = RequestMethod.PUT)
    @ApiOperation(value="Updates the values for the existing PPP indexes.", 
            notes="This will be used to update the values for the existing PPP indexes in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "PPP Index records successfully updated", 
                    response = PppIndexDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PppIndexDTO> updatePppIndexes(
            @ApiParam(value="PPP Index List to be updated", required = false)
            @RequestBody List<PppIndexDTO> pppIndexList
            ) throws Throwable {
        pppIndexService.updatePppIndexData(pppIndexList);
        
        return pppIndexService.getPppIndexData();
    }
    
    //rest/ppp-index/~
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = PPP_INDEX_BY_ID_PATH, method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete a specific PPP index by ID.",
            notes = "Delete a specific PPP index by ID in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "PPP Index records successfully deleted"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public void deletePppIndexById(
            @ApiParam(value = "The id of the PPP index to be deleted")
                @PathVariable(ID_REST_PARAM) Long pppIndexId
            ) throws Throwable {
        pppIndexService.deletePppIndexById(pppIndexId);
    }
}
