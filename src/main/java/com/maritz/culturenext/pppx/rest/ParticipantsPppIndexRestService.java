package com.maritz.culturenext.pppx.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.List;

import javax.inject.Inject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;

@RestController
@Api(value = "/participants", description = "Endpoints for a list of participants, return any associated PPP index conversions "
        + "currently configured for the country they live in.")
public class ParticipantsPppIndexRestService {

    @Inject private ParticipantsPppIndexService participantsPppIndexService;
    
    // Service endpoint paths
    public static final String PPP_INDEX_PATH = "participants/ppp-index";
    
    /**
     * Retrieves Participants PPP Index records by IDs
     */
    //rest/participants/ppp-index
    @RequestMapping(value = PPP_INDEX_PATH, method = RequestMethod.GET)
    @ApiOperation(value="Returns a list of any associated PPP index conversions "
                    + "currently configured for the country participants live in.", 
            notes="This will be used to return a list of all participants and associated PPP index conversions "
                    + "configured in the database.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of Participants, Country and PPP Index conversions", 
                    response = ParticipantsPppIndexDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<ParticipantsPppIndexDTO> getParticipantsPppIndexDataByPaxIds(
            @ApiParam(value="Comma delimited list of valid pax ids")
            @RequestParam(value = PAX_IDS_REST_PARAM, required=false) String paxIds,
            @ApiParam(value="Comma delimited list of valid group ids")
            @RequestParam(value = GROUP_IDS_REST_PARAM, required=false) String groupIds){
        return participantsPppIndexService.getParticipantsPppxByPaxIds(paxIds, groupIds);
    }
}
