package com.maritz.culturenext.pppx.services;

import java.util.List;

import com.maritz.culturenext.pppx.dto.PppIndexDTO;

public interface PppIndexService {
    /** 
     * Retrieves all PPP Index records
     * @return List of PppIndexDTO
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/161611777/PPP+Index"
     *         target="_top">PPP Index</a>
     * 
     */
    List<PppIndexDTO> getPppIndexData();
    
    /** 
     * Inserts a new record in PPP Index Table
     * @param countryId  - Country ID
     * @param pppValue  - PPP Index Value
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/161611777/PPP+Index"
     *         target="_top">PPP Index</a>
     * 
     */
    void createPppIndexData(Long countryId, Double pppValue);
    
    /** 
     * Updates existing PPP Index records
     * @param pppIndexList  - List of PPP Index
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/161611777/PPP+Index"
     *         target="_top">PPP Index</a>
     * 
     */
    void updatePppIndexData(List<PppIndexDTO> pppIndexList);
    
    /** 
     * Deletes a PPP Index record by ID
     * @param id  - PPP Index ID
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/161611777/PPP+Index"
     *         target="_top">PPP Index</a>
     * 
     */
    void deletePppIndexById(Long id);
}
