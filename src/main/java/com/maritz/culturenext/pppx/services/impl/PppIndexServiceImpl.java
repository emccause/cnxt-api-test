package com.maritz.culturenext.pppx.services.impl;

import static com.maritz.culturenext.pppx.constants.PppIndexConstants.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.pppx.dao.PppIndexDao;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;
import com.maritz.culturenext.pppx.services.PppIndexService;

@Component
public class PppIndexServiceImpl implements PppIndexService {
    
    @Inject private PppIndexDao pppIndexDao;
    
    List<ErrorMessage> errors;

    @Override
    public List<PppIndexDTO> getPppIndexData() {
        return pppIndexDao.getPppIndexData();
    }

    @Override
    public void createPppIndexData(Long countryId, Double pppValue) {
        if(validateCountryId(countryId) && validatePppValue(pppValue)) {
            pppIndexDao.createPppIndexRecord(countryId, pppValue);
        }
        
    }
    
    /** 
     * Validates if Country exists and it is not on PPP Index table
     * @param countryId  - Country ID
     * @return boolean
     * 
     */
    private boolean validateCountryId(Long countryId) {
        boolean isValidCountry = false;
        errors = new ArrayList<>();
        
        if(countryId == null || pppIndexDao.isPppxCountryId(countryId)) {
            isValidCountry = false;
            errors.add(PPPX_ALREADY_EXISTS_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else if (!pppIndexDao.isValidCountryId(countryId)) {
            isValidCountry = false;
            errors.add(COUNTRY_NOT_FOUND_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else {
            isValidCountry = true;
        }
        
        return isValidCountry;
    }
    
    /** 
     * Validates PPP Index value
     * @param pppValue  - PPP Index Value
     * @return boolean
     * 
     */
    private boolean validatePppValue(Double pppValue) {
        boolean isValidPppValue = false;
        errors = new ArrayList<>();
        
        if(pppValue != null && pppValue > 0) {
            isValidPppValue = true;
        } else {
            errors.add(PPPX_WRONG_VALUE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        
        return isValidPppValue;
    }

    @Override
    public void updatePppIndexData(List<PppIndexDTO> pppIndexList) {
        if(pppIndexList != null) {
            Long defaultCountryId = pppIndexDao.getCountryIdByCountryCode(DEFAULT_PPP_INDEX_COUNTRY_CODE);
            for(PppIndexDTO pppIndex : pppIndexList) {
                if(pppIndex.getId() != null) {
                    PppIndexDTO pppIndexRecord = pppIndexDao.getPppIndexById(pppIndex.getId());
                    if(pppIndexRecord != null  && pppIndexRecord.getCountryId() != null && !pppIndexRecord.getCountryId().equals(defaultCountryId)) {
                        if(validatePppValue(pppIndex.getPppValue())) {
                            pppIndexDao.updatePppIndexRecord(pppIndex.getId(), pppIndex.getCountryId(), pppIndex.getPppValue());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void deletePppIndexById(Long id) {
        errors = new ArrayList<>();
        Long defaultCountryId = pppIndexDao.getCountryIdByCountryCode(DEFAULT_PPP_INDEX_COUNTRY_CODE);
        PppIndexDTO pppIndexRecord = pppIndexDao.getPppIndexById(id);
        if (id == null) {
            errors.add(PPPX_ID_NOT_FOUND_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else if(pppIndexRecord == null || pppIndexRecord.getCountryId() == null) {
            errors.add(PPPX_ID_NOT_FOUND_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else if (pppIndexRecord.getCountryId().equals(defaultCountryId)) {
            errors.add(DEFAULT_COUNTRY_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        } else {
            pppIndexDao.deletePppIndexById(id);
        }
    }

}

