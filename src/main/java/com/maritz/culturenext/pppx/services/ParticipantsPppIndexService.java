package com.maritz.culturenext.pppx.services;

import java.util.List;

import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;

public interface ParticipantsPppIndexService {
    /** 
     * Retrieves Participants PPP Index records by IDs
     * @param paxIdListStr - List of Pax IDs
     * @return List of ParticipantsPppIndexDTO
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/178847748/Get+PPP+Index+Values+for+Participants"
     *         target="_top">Get PPP Index Values for Participants</a>
     * 
     */
    List<ParticipantsPppIndexDTO> getParticipantsPppxByPaxIds(String paxIdListStr, String groupIdListStr);
}
