package com.maritz.culturenext.pppx.services.impl;

import static com.maritz.culturenext.pppx.constants.PppIndexConstants.*;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.pppx.dao.ParticipantsPppIndexDao;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.GroupsService;

@Component
public class ParticipantsPppIndexServiceImpl implements ParticipantsPppIndexService {
    
    @Inject private ParticipantsPppIndexDao participantsPppIndexDao;
    @Inject private GroupsService groupsService;
    
    List<ErrorMessage> errors;

    @Override
    public List<ParticipantsPppIndexDTO> getParticipantsPppxByPaxIds(String paxIdListStr, String groupIdListStr) {
        errors = new ArrayList<>();
        if (paxIdListStr == null && groupIdListStr == null) {
            errors.add(EMPTY_PAX_ID_LIST_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }
        String[] paxIdList = {};
        
        if (paxIdListStr != null) {
            paxIdList = paxIdListStr.split(ProjectConstants.COMMA_DELIM);
        }
        List<Long> paxIds = new ArrayList<Long>();
        for (String paxIdStr : paxIdList) {
            try {
                paxIds.add(Long.parseLong(paxIdStr));
            } catch(NumberFormatException nfe) {
                errors.add(INVALID_PAX_ID_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
        }
        
        String[] groupIdList = {};
        
        if (groupIdListStr != null) {
            groupIdList = groupIdListStr.split(ProjectConstants.COMMA_DELIM);
        }
        for (String groupIdStr : groupIdList) {
            try {
                List<EntityDTO> groupMembers = groupsService.getGroupMembers(Long.parseLong(groupIdStr), null, null, null);
                for(EntityDTO groupMember: groupMembers){
                    if(groupMember instanceof EmployeeDTO) {
                        EmployeeDTO pax = (EmployeeDTO) groupMember;
                        paxIds.add(pax.getPaxId());
                    } else if(groupMember instanceof GroupDTO){
                        GroupDTO group = (GroupDTO) groupMember;
                        List<EntityDTO> subgroupMembers = groupsService.getGroupMembers(group.getGroupId(), null, null, null);
                        for(EntityDTO subgroupMember: subgroupMembers){
                            if(subgroupMember instanceof EmployeeDTO) {
                                EmployeeDTO pax = (EmployeeDTO) subgroupMember;
                                paxIds.add(pax.getPaxId());
                            }
                        }
                    }
                }
            } catch(NumberFormatException nfe) {
                errors.add(INVALID_GROUP_ID_MESSAGE);
                throw new ErrorMessageException().addErrorMessages(errors);
            }
        }

        List<ParticipantsPppIndexDTO> participantsPppIndexList = new ArrayList<ParticipantsPppIndexDTO>();
        Iterable<List<Long>> paxIdIterable = Iterables.partition(paxIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> set : paxIdIterable) {
            List<ParticipantsPppIndexDTO> pppList = participantsPppIndexDao.getParticipantsPppxByPaxIds(set);
            if (!CollectionUtils.isEmpty(pppList)) {
                participantsPppIndexList.addAll(pppList);
            }
        }
        return participantsPppIndexList;
    }

}
