package com.maritz.culturenext.pppx.dao;

import java.util.List;

import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;

public interface ParticipantsPppIndexDao {
    /** 
     * Retrieves Participants PPP Index records by IDs
     * @param paxIds - List of Pax IDs
     * @return List of ParticipantsPppIndexDTO
     * 
     */
    List<ParticipantsPppIndexDTO> getParticipantsPppxByPaxIds(List<Long> paxIds);
}
