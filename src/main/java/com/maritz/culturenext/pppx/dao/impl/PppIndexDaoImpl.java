package com.maritz.culturenext.pppx.dao.impl;

import static com.maritz.culturenext.pppx.constants.PppIndexConstants.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.pppx.dao.PppIndexDao;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;

@Repository
public class PppIndexDaoImpl extends AbstractDaoImpl implements PppIndexDao {
    
    private static final String PPP_INDEX_DATA_QUERY = 
            "SELECT CAI.ID, "
            + "C.COUNTRY_ID, "
            + "CAI.AWARD_INDEX AS PPP_VALUE "
            + "FROM component.COUNTRY_AWARD_INDEX CAI "
            + "JOIN component.COUNTRY C ON C.COUNTRY_CODE = CAI.COUNTRY_CODE ";
    
    private static final String COUNTRY_CODE_QUERY = 
            "SELECT C.COUNTRY_ID "
            + "FROM component.COUNTRY C "
            + "WHERE COUNTRY_CODE = :" + ProjectConstants.COUNTRY_CODE;
    
    private static final String PPPX_COUNTRY_COUNT_QUERY = 
            "SELECT COUNT(*) CNT "
            + "FROM component.COUNTRY_AWARD_INDEX CAI "
            + "JOIN component.COUNTRY C ON C.COUNTRY_CODE = CAI.COUNTRY_CODE "
            + "WHERE COUNTRY_ID = :" + COUNTRY_ID;
    
    private static final String COUNTRY_COUNT_QUERY = 
            "SELECT COUNT(*) CNT "
            + "FROM component.COUNTRY "
            + "WHERE COUNTRY_ID = :" + COUNTRY_ID;
    
    private static final String INSERT_PPP_INDEX_QUERY = 
            "INSERT INTO component.COUNTRY_AWARD_INDEX (COUNTRY_CODE, AWARD_INDEX) "
            + "VALUES ((SELECT COUNTRY_CODE FROM component.COUNTRY WHERE COUNTRY_ID = :" + COUNTRY_ID + "), :" + PPP_VALUE + ")";
    
    private static final String UPDATE_PPP_INDEX_QUERY = 
            "UPDATE CAI "
            + "SET AWARD_INDEX = :" + PPP_VALUE
            + " FROM component.COUNTRY_AWARD_INDEX CAI "
            + "JOIN component.COUNTRY C ON C.COUNTRY_CODE = CAI.COUNTRY_CODE "
            + "WHERE CAI.ID = :" + ProjectConstants.ID
            + " AND C.COUNTRY_ID = :" + COUNTRY_ID;
    
    private static final String DELETE_PPP_INDEX_QUERY =
            "DELETE FROM component.COUNTRY_AWARD_INDEX "
            + "WHERE ID = :" + ProjectConstants.ID;

    @Override
    public List<PppIndexDTO> getPppIndexData() {
        String query = PPP_INDEX_DATA_QUERY + " ORDER BY CAI.ID ";
        
        List<Map<String, Object>> nodes = 
            namedParameterJdbcTemplate.query(query, new MapSqlParameterSource(), new CamelCaseMapRowMapper());
        
        ArrayList<PppIndexDTO> pppIndexList = new ArrayList<PppIndexDTO>();
        for (Map<String, Object> node: nodes) {
            PppIndexDTO pppIndex = new PppIndexDTO();
            pppIndex.setId((Long) node.get(ProjectConstants.ID));
            pppIndex.setCountryId((Long)node.get(COUNTRY_ID));
            pppIndex.setPppValue((Double)node.get(PPP_VALUE));
            
            pppIndexList.add(pppIndex);
        }
        
        return pppIndexList;    
    }

    @Override
    public PppIndexDTO getPppIndexById(Long id) {
        String query = PPP_INDEX_DATA_QUERY + " WHERE CAI.ID = :" + ProjectConstants.ID;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.ID, id);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());
        
        PppIndexDTO pppIndex = new PppIndexDTO();
        for (Map<String, Object> node: nodes) {
            pppIndex.setId((Long) node.get(ProjectConstants.ID));
            pppIndex.setCountryId((Long)node.get(COUNTRY_ID));
            pppIndex.setPppValue((Double)node.get(PPP_VALUE));
        }
        
        return pppIndex;
    }

    @Override
    public Long getCountryIdByCountryCode(String countryCode) {
        Long countryId = null;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.COUNTRY_CODE, countryCode);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(COUNTRY_CODE_QUERY, params, new CamelCaseMapRowMapper());
        
        for (Map<String, Object> node: nodes) {
            countryId = (Long)node.get(COUNTRY_ID);
        }
        
        return countryId;
    }

    @Override
    public boolean isPppxCountryId(Long countryId) {
        boolean isPppxCountryId = true;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COUNTRY_ID, countryId);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(PPPX_COUNTRY_COUNT_QUERY, params, new CamelCaseMapRowMapper());
        
        for (Map<String, Object> node: nodes) {
            isPppxCountryId = (int)node.get(CNT) > 0 ? true: false;
        }
        
        return isPppxCountryId;    
    }

    @Override
    public boolean isValidCountryId(Long countryId) {
        boolean isValidCountryId = false;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COUNTRY_ID, countryId);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(COUNTRY_COUNT_QUERY, params, new CamelCaseMapRowMapper());
        
        for (Map<String, Object> node: nodes) {
            isValidCountryId = (int)node.get(CNT) > 0 ? true: false;
        }
        
        return isValidCountryId;
    }

    @Override
    public void createPppIndexRecord(Long countryId, Double pppValue) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(COUNTRY_ID, countryId);
        params.addValue(PPP_VALUE, pppValue);
        
        namedParameterJdbcTemplate.update(INSERT_PPP_INDEX_QUERY, params);
    }

    @Override
    public void updatePppIndexRecord(Long id, Long countryId, Double pppValue) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.ID, id);
        params.addValue(COUNTRY_ID, countryId);
        params.addValue(PPP_VALUE, pppValue);
        
        namedParameterJdbcTemplate.update(UPDATE_PPP_INDEX_QUERY, params);
    }

    @Override
    public void deletePppIndexById(Long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.ID, id);
        
        namedParameterJdbcTemplate.update(DELETE_PPP_INDEX_QUERY, params);
    }
}