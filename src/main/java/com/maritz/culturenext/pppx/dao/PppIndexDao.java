package com.maritz.culturenext.pppx.dao;

import java.util.List;

import com.maritz.culturenext.pppx.dto.PppIndexDTO;

public interface PppIndexDao {
    /** 
     * Retrieves all PPP Index records
     * @return List of PppIndexDTO
     * 
     */
    List<PppIndexDTO> getPppIndexData();
    
    /** 
     * Retrieves a single PPP Index by ID
     * @param id  - PPP Index ID
     * @return PppIndexDTO
     * 
     */
    PppIndexDTO getPppIndexById(Long id);
    
    /** 
     * Retrieve Country ID of a given Country Code
     * @param countryCode  - Country Code
     * @return Country ID
     * 
     */
    Long getCountryIdByCountryCode(String countryCode);
    
    /** 
     * Validates if Country is on PPP Index table
     * @param countryId  - Country ID
     * @return boolean
     * 
     */
    boolean isPppxCountryId(Long countryId);
    
    /** 
     * Validates if Country exists in COUNTRY table
     * @param countryId  - Country ID
     * @return boolean
     * 
     */
    boolean isValidCountryId(Long countryId);
    
    /** 
     * Inserts a new record in PPP Index Table
     * @param countryId  - Country ID
     * @param pppValue  - PPP Index Value
     * 
     */
    void createPppIndexRecord(Long countryId, Double pppValue);
    
    /** 
     * Updates an existing PPP Index record
     * @param id  - PPP Index ID
     * @param countryId  - Country ID
     * @param pppValue  - PPP Index Value
     * 
     */
    void updatePppIndexRecord(Long id, Long countryId, Double pppValue);
    
    /** 
     * Deletes a PPP Index record by ID
     * @param id  - PPP Index ID
     * 
     */
    void deletePppIndexById(Long id);
}
