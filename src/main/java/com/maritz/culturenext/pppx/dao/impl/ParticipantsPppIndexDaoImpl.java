package com.maritz.culturenext.pppx.dao.impl;

import static com.maritz.culturenext.pppx.constants.PppIndexConstants.COUNTRY_ID;
import static com.maritz.culturenext.pppx.constants.PppIndexConstants.PPP_VALUE;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.pppx.dao.ParticipantsPppIndexDao;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;

@Repository
public class ParticipantsPppIndexDaoImpl extends AbstractDaoImpl implements ParticipantsPppIndexDao {
    
    private static final String PARTICIPANT_PPP_INDEX_BY_PAX_IDS_QUERY = 
            "SELECT P.PAX_ID, "
            + "C.COUNTRY_CODE, "
            + "C.COUNTRY_ID, "
            + "CAI.AWARD_INDEX AS PPP_VALUE "
            + "FROM component.PAX P "
            + "LEFT JOIN component.ADDRESS A ON A.PAX_ID = P.PAX_ID AND A.PREFERRED = 'Y' "
            + "LEFT JOIN component.COUNTRY C ON C.COUNTRY_CODE = A.COUNTRY_CODE "
            + "LEFT JOIN component.COUNTRY_AWARD_INDEX CAI ON CAI.COUNTRY_CODE = C.COUNTRY_CODE "
            + "WHERE P.PAX_ID IN (:" + ProjectConstants.PAX_IDS + ") "
            + "ORDER BY P.PAX_ID ";

    @Override
    public List<ParticipantsPppIndexDTO> getParticipantsPppxByPaxIds(List<Long> paxIds) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.PAX_IDS, paxIds);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(PARTICIPANT_PPP_INDEX_BY_PAX_IDS_QUERY, params, new CamelCaseMapRowMapper());
        
        List<ParticipantsPppIndexDTO> participantsPppIndexList = new ArrayList<ParticipantsPppIndexDTO>();
        for(Map<String, Object> node: nodes){
            ParticipantsPppIndexDTO participantsPppIndex = new ParticipantsPppIndexDTO();
            participantsPppIndex.setPaxId((Long) node.get(ProjectConstants.PAX_ID));
            participantsPppIndex.setCountryCode((String)node.get(ProjectConstants.COUNTRY_CODE));
            participantsPppIndex.setCountryId((Long)node.get(COUNTRY_ID));
            participantsPppIndex.setPppValue((Double)node.get(PPP_VALUE));
            
            participantsPppIndexList.add(participantsPppIndex);
        }
        
        return participantsPppIndexList;
    }

}
