package com.maritz.culturenext.pppx.dto;

public class PppIndexDTO {
    private Long id;
    private Long countryId;
    private Double pppValue;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public Long getCountryId() {
        return countryId;
    }
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    
    public Double getPppValue() {
        return pppValue;
    }
    public void setPppValue(Double pppValue) {
        this.pppValue = pppValue;
    }
}
