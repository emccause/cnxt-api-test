package com.maritz.culturenext.pppx.dto;

public class ParticipantsPppIndexDTO {
    private Long paxId;
    private String countryCode;
    private Long countryId;
    private Double pppValue;
    
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public Long getCountryId() {
        return countryId;
    }
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    public Double getPppValue() {
        return pppValue;
    }
    public void setPppValue(Double pppValue) {
        this.pppValue = pppValue;
    }
    
}
