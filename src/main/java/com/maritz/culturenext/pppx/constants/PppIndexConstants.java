package com.maritz.culturenext.pppx.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class PppIndexConstants {
    // Default Values
    public static final String DEFAULT_PPP_INDEX_COUNTRY_CODE = "US";
    
    // Database fields
    public static final String COUNTRY_ID = "countryId";
    public static final String PPP_VALUE = "pppValue";
    public static final String CNT = "cnt";
    
    // Errors
    public static final String ERROR_DEFAULT_COUNTRY_CODE = "ERROR_DEFAULT_COUNTRY";
    public static final String ERROR_DEFAULT_COUNTRY_MESSAGE = "Default Country cannot be modified.";
    public static final String ERROR_PPPX_ALREADY_EXISTS_CODE = "ERROR_PPPX_ALREADY_EXISTS";
    public static final String ERROR_PPPX_ALREADY_EXISTS_MESSAGE = "A Country can only have 1 PPPX Modifier.";
    public static final String ERROR_COUNTRY_NOT_FOUND_CODE = "ERROR_COUNTRY_NOT_FOUND";
    public static final String ERROR_COUNTRY_NOT_FOUND_MESSAGE = "Country not found.";
    public static final String ERROR_PPPX_WRONG_VALUE_CODE = "ERROR_PPPX_WRONG_VALUE";
    public static final String ERROR_PPPX_WRONG_VALUE_MESSAGE = "Number must be greater than zero";
    public static final String ERROR_PPPX_ID_NOT_FOUND_CODE = "ERROR_PPPX_ID_NOT_FOUND";
    public static final String ERROR_PPPX_ID_NOT_FOUND_MESSAGE = "Record was not found";
    public static final String ERROR_EMPTY_PAX_ID_LIST_CODE = "ERROR_EMPTY_PAX_ID_LIST";
    public static final String ERROR_EMPTY_PAX_ID_LIST_MESSAGE = "Pax ID and Group ID List is empty";
    public static final String ERROR_INVALID_PAX_ID_CODE = "ERROR_INVALID_PAX_ID";
    public static final String ERROR_INVALID_PAX_ID_MESSAGE = "Invalid Pax ID";
    public static final String ERROR_INVALID_GROUP_ID_CODE = "ERROR_INVALID_GROUP_ID";
    public static final String ERROR_INVALID_GROUP_ID_MESSAGE = "Invalid Group ID";
    // Error Messages
    public static final ErrorMessage DEFAULT_COUNTRY_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DEFAULT_COUNTRY_CODE)
        .setField(ProjectConstants.COUNTRY_CODE)
        .setMessage(ERROR_DEFAULT_COUNTRY_MESSAGE);

    public static final ErrorMessage PPPX_ALREADY_EXISTS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PPPX_ALREADY_EXISTS_CODE)
        .setField(COUNTRY_ID)
        .setMessage(ERROR_PPPX_ALREADY_EXISTS_MESSAGE);

    public static final ErrorMessage COUNTRY_NOT_FOUND_MESSAGE = new ErrorMessage()
        .setCode(ERROR_COUNTRY_NOT_FOUND_CODE)
        .setField(COUNTRY_ID)
        .setMessage(ERROR_COUNTRY_NOT_FOUND_MESSAGE);

    public static final ErrorMessage PPPX_WRONG_VALUE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PPPX_WRONG_VALUE_CODE)
        .setField(PPP_VALUE)
        .setMessage(ERROR_PPPX_WRONG_VALUE_MESSAGE);

    public static final ErrorMessage PPPX_ID_NOT_FOUND_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PPPX_ID_NOT_FOUND_CODE)
        .setField(ProjectConstants.ID)
        .setMessage(ERROR_PPPX_ID_NOT_FOUND_MESSAGE);

    public static final ErrorMessage EMPTY_PAX_ID_LIST_MESSAGE = new ErrorMessage()
        .setCode(ERROR_EMPTY_PAX_ID_LIST_CODE)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_EMPTY_PAX_ID_LIST_MESSAGE);

    public static final ErrorMessage INVALID_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_PAX_ID_CODE)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ERROR_INVALID_PAX_ID_MESSAGE);
    
    public static final ErrorMessage INVALID_GROUP_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_GROUP_ID_CODE)
            .setField(ProjectConstants.GROUP_ID)
            .setMessage(ERROR_INVALID_GROUP_ID_MESSAGE);
}
