package com.maritz.culturenext.permission.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.LogicalOperatorEnum;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.PermissionResolutionDao;
import com.maritz.culturenext.permission.dto.GroupPermissionsDTO;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.PermissionWrapperDTO;
import com.maritz.culturenext.permission.services.PermissionManagementService;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.calendar.constants.CalendarConstants;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class PermissionManagementServiceImpl implements PermissionManagementService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<Role> roleDao;
    @Inject private GroupConfigServiceDao groupConfigServiceDao;
    @Inject private PermissionResolutionDao permissionResolutionDao;
    
    @Override
    public List<PermissionWrapperDTO> getPermissionsForRoles(String commaSeparatedRoleNameList) {
        
        List<String> roleNameList = null;
        if(commaSeparatedRoleNameList != null && !commaSeparatedRoleNameList.trim().equals("")){
            roleNameList = Arrays.asList(commaSeparatedRoleNameList.trim().toUpperCase().split(","));
        }
        
        validateRoleRequest(RequestMethod.GET.toString(), roleNameList, null);
        
        // We need to find all the roles to look for
        List<String> permissionRolesToResolve = new ArrayList<>();
        
        if (roleNameList.contains(PermissionConstants.ADMIN_ROLE_NAME)) {
            permissionRolesToResolve.addAll(PermissionManagementTypes.getValidAdminPermissions());
        }
        if (roleNameList.contains(PermissionConstants.MANAGER_ROLE_NAME)) {
            permissionRolesToResolve.addAll(PermissionManagementTypes.getValidManagerPermissions());
        }
        if (roleNameList.contains(PermissionConstants.PARTICIPANT_ROLE_NAME)) {
            permissionRolesToResolve.addAll(PermissionManagementTypes.getValidParticipantPermissions());
        }
        
        // this method will get us a list of permissions set for each role passed in
        Map<String, List<String>> roleToPermissionListMap = permissionResolutionDao
                .getPermissionsForRolesInList(permissionRolesToResolve, roleNameList);
        
        List<PermissionWrapperDTO> permissionListToReturn = new ArrayList<>();
        for (String roleName : roleNameList) {
            PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
            permissionWrapper.setRole(roleName);
            
            List<String> permissionsForRoleList = roleToPermissionListMap.get(roleName);
            if (permissionsForRoleList == null) { // fallback in case there are no permissions mapped for the role
                permissionsForRoleList = new ArrayList<>();
            }
            
            // Process permissions - if permissionsForRoleList contains the permission it is enabled, 
            // otherwise it is disabled
            List<PermissionDTO> permissionStatusForRole = new ArrayList<>();
            if (PermissionConstants.ADMIN_ROLE_NAME.equals(roleName)) { 
                for (String validPermission : PermissionManagementTypes.getValidAdminPermissions()) {
                    permissionStatusForRole.add(new PermissionDTO().setPermissionCode(validPermission)
                            .setEnabled(permissionsForRoleList.contains(validPermission)));
                }
            }
            else if (PermissionConstants.MANAGER_ROLE_NAME.equals(roleName)) {
                for (String validPermission : PermissionManagementTypes.getValidManagerPermissions()) {
                    permissionStatusForRole.add(new PermissionDTO().setPermissionCode(validPermission)
                            .setEnabled(permissionsForRoleList.contains(validPermission)));
                }
            }
            else if (PermissionConstants.PARTICIPANT_ROLE_NAME.equals(roleName)) {
                for (String validPermission : PermissionManagementTypes.getValidParticipantPermissions()) {
                    permissionStatusForRole.add(new PermissionDTO().setPermissionCode(validPermission)
                            .setEnabled(permissionsForRoleList.contains(validPermission)));
                }
            }
            permissionWrapper.setPermissions(permissionStatusForRole);
            
            permissionListToReturn.add(permissionWrapper);
        }
        
        return permissionListToReturn;
    }

    @Override
    public List<PermissionWrapperDTO> updatePermissionsForRole(String requestType, 
            PermissionWrapperDTO permissionWrapper) {
        
        List<String> roleNameList = new ArrayList<>();
        roleNameList.add(permissionWrapper.getRole());
        
        validateRoleRequest(requestType, roleNameList, permissionWrapper);
        
        Role role = roleDao.findBy()
                .where(ProjectConstants.ROLE_NAME).eq(permissionWrapper.getRole())
                .findOne();
        
        // handle permissions
        for (PermissionDTO permission : permissionWrapper.getPermissions()) {
            if (permission.getEnabled() == true) {
                permissionResolutionDao.createPermissionForTypeIfNotExists(permission.getPermissionCode(), 
                        PermissionConstants.ROLE_ACL_TYPE, role.getRoleId());
            }
            else {
                permissionResolutionDao.deletePermissionForTypeIfExists(permission.getPermissionCode(), 
                        PermissionConstants.ROLE_ACL_TYPE, role.getRoleId());
            }
        }
        
        //Kick off the updating of the RIDEAU accounts (need to update status based on permissions)
        logger.info("Started - updateRideauAccountStatus");
        groupConfigServiceDao.updateRideauAccountStatus();
        logger.info("Finished - updateRideauAccountStatus");
        
        return getPermissionsForRoles(permissionWrapper.getRole());
    }

    @Override
    public List<PermissionWrapperDTO> getPermissionsForGroups(String commaSeparatedGroupIdList) {
        
        List<String> groupIdStringList = null;
        if (commaSeparatedGroupIdList != null && !commaSeparatedGroupIdList.trim().equals("")) {
            groupIdStringList = Arrays.asList(commaSeparatedGroupIdList.trim().split(","));
        }
        
        List<ErrorMessage> errors = new ArrayList<>();
        
        if (groupIdStringList == null) {
            errors.add(PermissionConstants.MISSING_GROUP_ID_MESSAGE);
        }
        ErrorMessageException.throwIfHasErrors(errors); // need to have at least one group
        
        List<Long> groupIdList = new ArrayList<Long>();
        
        for (String groupIdString : groupIdStringList) {
            try{
                groupIdList.add(Long.parseLong(groupIdString));
            }
            catch (Exception e) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.GROUP_ID)
                    .setCode(PermissionConstants.INVALID_GROUP_ID_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.INVALID_GROUP_ID_ERROR_MESSAGE, groupIdString)));
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors); // throw error for non-numeric group ids
        
        validateGroupRequest(RequestMethod.GET.toString(), groupIdList, null);
        
        // this method will get us a list of permissions set for each group passed in
        Map<Long, List<String>> groupToPermissionListMap = permissionResolutionDao
                .getPermissionsForGroupsInList(PermissionManagementTypes.getValidGroupPermissions(), groupIdList);
        
        
        List<PermissionWrapperDTO> permissionListToReturn = new ArrayList<>();
        for (Long groupId : groupIdList) {
            PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
            permissionWrapper.setGroupId(groupId);
            
            List<String> permissionsForGroupList = groupToPermissionListMap.get(groupId);
            if (permissionsForGroupList == null) { // fallback in case there are no permissions mapped for the group
                permissionsForGroupList = new ArrayList<>();
            }
            
            // Process permissions - if permissionsForGroupList contains the permission it is enabled, 
            // otherwise it is disabled
            List<PermissionDTO> permissionStatusForRole = new ArrayList<>();
            
            for (String validPermission : PermissionManagementTypes.getValidGroupPermissions()) {
                permissionStatusForRole.add(new PermissionDTO().setPermissionCode(validPermission)
                        .setEnabled(permissionsForGroupList.contains(validPermission)));
            }
            
            permissionWrapper.setPermissions(permissionStatusForRole);
            
            permissionListToReturn.add(permissionWrapper);
        }
        
        return permissionListToReturn;
    }

    @Override
    public List<PermissionWrapperDTO> updatePermissionsForGroup(String requestType, 
            PermissionWrapperDTO permissionWrapper) {
        
        if(permissionWrapper.getGroupId() != null) {
            List<Long> groupIdList = new ArrayList<>();
            groupIdList.add(permissionWrapper.getGroupId());
            validateGroupRequest(requestType, groupIdList, permissionWrapper);
        }
        else {
            List<ErrorMessage> errors = new ArrayList<>();
            errors.add(PermissionConstants.MISSING_GROUP_ID_MESSAGE);
            ErrorMessageException.throwIfHasErrors(errors);
        }
        
        // handle permissions
        for (PermissionDTO permission : permissionWrapper.getPermissions()) {
            if (permission.getEnabled() == true) {
                permissionResolutionDao.createPermissionForTypeIfNotExists(permission.getPermissionCode(), 
                        PermissionConstants.GROUPS_ACL_TYPE, permissionWrapper.getGroupId());
            }
            else {
                permissionResolutionDao.deletePermissionForTypeIfExists(permission.getPermissionCode(), 
                        PermissionConstants.GROUPS_ACL_TYPE, permissionWrapper.getGroupId());
            }
        }
        
        //Kick off the updating of the RIDEAU accounts (need to update status based on permissions)
        logger.info("Started - updateRideauAccountStatus");
        groupConfigServiceDao.updateRideauAccountStatus();
        logger.info("Finished - updateRideauAccountStatus");

        return getPermissionsForGroups(permissionWrapper.getGroupId().toString());
    }
    
    private void validateRoleRequest(String requestType, List<String> roleNameList, 
            PermissionWrapperDTO permissionWrapper) {
        
        List<ErrorMessage> errors = new ArrayList<>();
        
        if (roleNameList == null) {
            errors.add(PermissionConstants.MISSING_ROLE_NAME_MESSAGE);
        }
        ErrorMessageException.throwIfHasErrors(errors); // need to have at least one role
        
        for (String role : roleNameList) {
            if (role != null && !PermissionConstants.SUPPORTED_ROLES.contains(role)) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.ROLE_NAME)
                    .setCode(PermissionConstants.INVALID_ROLE_NAME_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.INVALID_ROLE_NAME_ERROR_MESSAGE, role)));
            }
        }
        ErrorMessageException.throwIfHasErrors(errors); // need to have at least one valid role
        
        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType) || 
                RequestMethod.PATCH.toString().equalsIgnoreCase(requestType)) {
            if (permissionWrapper.getGroupId() != null) {
                errors.add(PermissionConstants.CANNOT_MODIFY_GROUPS_HERE_MESSAGE);
            }
            
            if (permissionWrapper.getRole() == null) {
                errors.add(PermissionConstants.ROLE_MISSING_MESSAGE);
            }
            
            ErrorMessageException.throwIfHasErrors(errors); // make sure identifier is properly populated.
            
            String providedRole = permissionWrapper.getRole();
            
            List<String> validPermissionList = null;
            
            if (PermissionConstants.ADMIN_ROLE_NAME.equalsIgnoreCase(providedRole)) {
                validPermissionList = PermissionManagementTypes.getValidAdminPermissions();
            }
            else if (PermissionConstants.MANAGER_ROLE_NAME.equalsIgnoreCase(providedRole)) {
                validPermissionList = PermissionManagementTypes.getValidManagerPermissions();
            }
            else if (PermissionConstants.PARTICIPANT_ROLE_NAME.equalsIgnoreCase(providedRole)) {
                validPermissionList = PermissionManagementTypes.getValidParticipantPermissions();
            }
            // role is validated above already, won't get here if it's not supported
            validatePermissionList(requestType, errors, validPermissionList, permissionWrapper, 
                    String.format(PermissionConstants.ROLE_PERMISSION_ERROR_TEXT, providedRole));
        }
    }
    
    private void validateGroupRequest(String requestType, List<Long> groupIdList, 
            PermissionWrapperDTO permissionWrapper){

        List<ErrorMessage> errors = new ArrayList<>();
        
        List<Long> existingGroupIdList = groupsDao.findBy()
                .where(ProjectConstants.GROUP_ID).in(groupIdList).find(ProjectConstants.GROUP_ID, Long.class);
        
        // validate all groups passed in exist in the database
        for (Long providedGroupId : groupIdList) {
            Boolean exists = false;
            for (Long existingGroupId : existingGroupIdList) {
                if (existingGroupId.equals(providedGroupId)) {
                    exists = true; // found
                    break;
                }
            }
            
            if (exists == false) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.GROUP_ID)
                    .setCode(PermissionConstants.GROUP_DOES_NOT_EXIST_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.GROUP_DOES_NOT_EXIST_ERROR_MESSAGE, providedGroupId)));
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors); // groups exist and are all custom
        
        if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType) || 
                RequestMethod.PATCH.toString().equalsIgnoreCase(requestType)) {
            if (permissionWrapper.getRole() != null) {
                errors.add(PermissionConstants.CANNOT_MODIFY_ROLES_HERE_MESSAGE);
            }
            
            if (permissionWrapper.getGroupId() == null) {
                errors.add(PermissionConstants.GROUP_MISSING_MESSAGE);
            }
            
            ErrorMessageException.throwIfHasErrors(errors); // make sure identifier is properly populated.
            
            // group id is validated above already, won't get here if it's not supported
            validatePermissionList(requestType, errors, PermissionManagementTypes.getValidGroupPermissions(), 
                    permissionWrapper, String.format(PermissionConstants.GROUPS_PERMISSION_ERROR_TEXT, 
                            permissionWrapper.getGroupId()));
        }
    }
    
    private void validatePermissionList(String requestType, List<ErrorMessage> errors, List<String> validPermissionList,
            PermissionWrapperDTO permissionWrapper, String errorText){
        
        List<PermissionDTO> permissionList = permissionWrapper.getPermissions();
        if (permissionList != null && !permissionList.isEmpty()) {
            // validate permissions
            for (PermissionDTO permission : permissionList) { // make sure only allowed permissions are passed
                if (!validPermissionList.contains(permission.getPermissionCode())) {
                    errors.add(new ErrorMessage()
                        .setField(ProjectConstants.PERMISSIONS)
                        .setCode(PermissionConstants.INVALID_PERMISSION_ERROR_CODE)
                        .setMessage(String.format(PermissionConstants.INVALID_PERMISSION_ERROR_MESSAGE, 
                                permission.getPermissionCode(), errorText)));
                }
                if (permission.getEnabled() == null) {
                    errors.add(new ErrorMessage()
                        .setField(ProjectConstants.PERMISSIONS)
                        .setCode(PermissionConstants.INVALID_PERMISSION_ENABLED_ERROR_CODE)
                        .setMessage(String.format(PermissionConstants.INVALID_PERMISSION_ENABLED_ERROR_MESSAGE, 
                                permission.getPermissionCode())));
                }
            }
            
            if (RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) {
                // for PUT, make sure that ALL permissions are passed
                for (String expectedPermission : validPermissionList) {
                    Boolean exists = false;
                    for (PermissionDTO providedPermission : permissionList) {
                        if (expectedPermission.equalsIgnoreCase(providedPermission.getPermissionCode())) {
                            exists = true;
                            break;
                        }
                    }
                    if (exists == false) {
                        errors.add(new ErrorMessage()
                            .setField(ProjectConstants.PERMISSIONS)
                            .setCode(PermissionConstants.MISSING_PERMISSION_ERROR_CODE)
                            .setMessage(String.format(PermissionConstants.MISSING_PERMISSION_ERROR_MESSAGE, 
                                    expectedPermission, errorText)));
                    }
                }
            }
        }
        else {
            errors.add(PermissionConstants.MISSING_PERMISION_LIST_MESSAGE);
        }
        
        ErrorMessageException.throwIfHasErrors(errors); // handle any permission validation errors
    }

    @Override
    public PaginatedResponseObject<GroupPermissionsDTO> getGroupsByPermissions(String commaSeparatedPermissionList, String filter
            , Integer page, Integer size) {
        
        // handle page parameters
        if (page == null || page < 1) {
            page = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        if (size == null || size < 1) {
            size = ProjectConstants.DEFAULT_PAGE_SIZE;
        }
        
        List<String> permissionsArgList = StringUtils.parseDelimitedData(commaSeparatedPermissionList);
        
        // Validate arguments (permission list and filter)
        List<ErrorMessage> errors = validateGetQueryParams(permissionsArgList, filter, size);

        // handle any errors before updating the notification
        ErrorMessageException.throwIfHasErrors(errors);
        
        // Get query results.
        List<Map<String, Object>> nodes = 
                permissionResolutionDao.getGroupsByPermissions(permissionsArgList, filter, page, size);
        
        Integer totalResults = 0;
        List<GroupPermissionsDTO> permissionsByGroupList = new ArrayList<GroupPermissionsDTO>();
        if (!CollectionUtils.isEmpty(nodes)) {
            List<PermissionDTO> permissionsList = null;
            GroupPermissionsDTO permissionGroup = null;
            PermissionDTO permission = null;
            
            for (Map<String, Object> node: nodes) {
                
                // Populate groups
                permissionGroup = new GroupPermissionsDTO();
                permissionGroup.setGroup(new GroupDTO(node));
                
                // Populate permissions by groups, values are delimited by comma
                permissionsList  = new ArrayList<PermissionDTO>();
                List<String> permissions = 
                        StringUtils.parseDelimitedData((String) node.get(PermissionConstants.PERMISSION_CODES_LIST_FIELD));
                
                // All audience permission are displayed but enabled flag is set according to its status
                for (String audiencePermission : PermissionManagementTypes.getAudiencePermissions()) {
                    permission = new PermissionDTO();
                    permission.setPermissionCode(audiencePermission);
                    permission.setEnabled(permissions.contains(audiencePermission));
                    permissionsList.add(permission);
                }
                
                permissionGroup.setPermissions(permissionsList);
                permissionsByGroupList.add(permissionGroup);
                
            }
            // Total query result.
            totalResults = (Integer) nodes.get(0).get(ProjectConstants.TOTAL_RESULTS_KEY);
        }
        
        // Handle other parameters
        Map<String, Object> requestParameters = new HashMap<>();
        requestParameters.put(RestParameterConstants.PERMISSION_REST_PARAM, commaSeparatedPermissionList);
        requestParameters.put(RestParameterConstants.FILTER_REST_PARAM, filter);
        
        // Return paginated data
        return new PaginatedResponseObject<GroupPermissionsDTO>(permissionsByGroupList, requestParameters 
                , PermissionConstants.GROUP_PERMISSIONS_REST_ENDPOINT
                , totalResults, page, size);
    }
    
    /**
     * Validate permissions and filter passed in.
     * @param permissionsList
     * @param filter
     * @return
     */
    private List<ErrorMessage> validateGetQueryParams(List<String> permissionsList, String filter, Integer size) {
        List<ErrorMessage> errors = new ArrayList<>();
        
        // Validate pagination size
        if (size != null && size > CalendarConstants.MAX_PAGINATION_SIZE) {
            errors.add(CalendarConstants.PAGINATION_EXCEEDS_MAX_SIZE_MESSAGE);
        }
        
        // Validate permission list
        if (CollectionUtils.isEmpty(permissionsList)) {
            errors.add(PermissionConstants.MISSING_PERMISSIONS_CODES_ERROR);
        }
        else {
            for(String permission: permissionsList) {
                if (!PermissionManagementTypes.getAudiencePermissions().contains(permission)) {
                    errors.add(PermissionConstants.INVALID_PERMISSION_ERROR);
                }
            }
        }
        
        // Validate filter param - only AND or OR are allowed.
        if (filter == null || filter.isEmpty()) {
            errors.add(PermissionConstants.MISSING_PERMISSION_FILTER_ERROR);
        }
        else {
            try{
                LogicalOperatorEnum.valueOf(filter);
            } catch (IllegalArgumentException e) {
                errors.add(PermissionConstants.INVALID_PERMISSION_FILTER_ERROR);
            }
        }
        
        return errors;
    }
}
