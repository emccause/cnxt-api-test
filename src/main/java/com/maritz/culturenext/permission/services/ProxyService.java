package com.maritz.culturenext.permission.services;

import java.util.List;

import com.maritz.culturenext.permission.dto.ProxyDTO;

public interface ProxyService {
    
    /**
     * Returns a list of the pax's proxy users and their permissions 
     * 
     * @param paxId - Pax ID to return proxies for
     * @param commaSeparatedProxyTypeList - MY_PROXIES or PROXY_AS
     * @param commaSeparatedStatusList - Filter for user's status
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459645/Get+Proxies
     */
    List<ProxyDTO> getProxiesForPax(Long paxId, String commaSeparatedProxyTypeList, String commaSeparatedStatusList);
    
    /**
     * gets impersonator's permissions while proxying
     * 
     * @param paxId pax id of logged in user
     * @return ProxyDTO with what the impersonator is able to do
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/33882242/GET+Proxy
     */
    ProxyDTO getProxyPermissions(Long paxId);
    
    /**
     * Set up a new proxy user for the logged in pax ID, including their permissions
     * 
     * @param paxId - logged in pax ID
     * @param proxyPadId - proxy pax ID to be set up for the logged in user
     * @param requestType - POST or PUT
     * @param proxyDto - Contains the proxy's permissions
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459651/POST+Proxies+with+permissions
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459804/PUT+Proxy+permissions
     */
    ProxyDTO updateProxyForPax(Long paxId, Long proxyPadId, String requestType, ProxyDTO proxyDto);
    
    /**
     * Deletes a proxy user for the participant specified
     * 
     * @param paxId - ID to remove the proxy from (logged in pax ID)
     * @param proxyPaxId - The proxy pax that will be deleted from the logged in pax ID's proxy list
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459700/Delete+Proxy
     */
    void deleteProxyForPax(Long paxId, Long proxyPaxId);
    
    /**
     * TODO javadocs
     * 
     * @param proxyPermission
     * @param targetId
     * @param subjectId
     */
    Boolean doesPaxHaveProxyPermission(String proxyPermission, Long targetId, Long subjectId);

}
