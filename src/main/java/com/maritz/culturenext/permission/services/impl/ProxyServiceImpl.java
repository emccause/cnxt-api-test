package com.maritz.culturenext.permission.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.ProxyDao;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.ProxyDTO;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class ProxyServiceImpl implements ProxyService {
    
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ProxyDao proxyDao;
    @Inject private Security security;

    @Override
    public List<ProxyDTO> getProxiesForPax(Long paxId, String commaSeparatedProxyTypeList, 
            String commaSeparatedStatusList) {
        
        List<String> proxyTypeList = PermissionConstants.SUPPORTED_PROXY_TYPE_LIST;
        if(commaSeparatedProxyTypeList != null && !commaSeparatedProxyTypeList.trim().equals("")){
            proxyTypeList = Arrays.asList(commaSeparatedProxyTypeList.trim().toUpperCase().split(","));
        }
        
        List<String> statusList = new ArrayList<>(PermissionConstants.DEFAULT_PROXY_STATUS_LIST);
        statusList.add(StatusTypeCode.INACTIVE.name()); // can't log in as inactive, but return them here so users can handle them
        if(commaSeparatedStatusList != null && !commaSeparatedStatusList.trim().equals("")){
            statusList = Arrays.asList(commaSeparatedStatusList.trim().toUpperCase().split(","));
        }

        List<ErrorMessage> errors = new ArrayList<>();

        for (String proxyType : proxyTypeList) {
            if(proxyType == null || !PermissionConstants.SUPPORTED_PROXY_TYPE_LIST.contains(proxyType)) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.TYPE)
                    .setCode(PermissionConstants.INVALID_TYPE_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.INVALID_TYPE_ERROR_MESSAGE, proxyType)));
            }
        }
        ErrorMessageException.throwIfHasErrors(errors);
        
        return proxyDao.getProxiesForPax(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST, 
                paxId, proxyTypeList, statusList);
    }

    @Override
    public ProxyDTO getProxyPermissions(Long paxId) {
        // Check for permissions from being configured as a proxy
        ProxyDTO proxyDto = proxyDao.getProxyForSpecificPax(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST,
                security.getImpersonatorPaxId(), paxId, PermissionConstants.PROXY_AS);
        
        if (proxyDto == null) {
            // User is not configured as a proxy for the logged in user, must be in via admin permissions
            proxyDto = new ProxyDTO();
            proxyDto.setProxyPax(participantInfoDao.getEmployeeDTO(security.getImpersonatorPaxId()));
            proxyDto.setType(PermissionConstants.PROXY_AS);
        }
        
        List<PermissionDTO> permissionList = proxyDto.getPermissions();
        if (permissionList == null) {
            permissionList = new ArrayList<>();
        }
        
        for (String permissionCode : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            // Check if user has permission via being an admin
            PermissionDTO permission = null;
            // get the existing permission
            for (PermissionDTO permissionDto : permissionList) {
                if (permissionCode.equals(permissionDto.getPermissionCode())) {
                    permission = permissionDto;
                    break;
                }
            }
            if (permission == null) {
                permission = new PermissionDTO();
                permission.setPermissionCode(permissionCode);
                permission.setEnabled(Boolean.FALSE); // If not set, default is not having the permission
            }
            else {
                permissionList.remove(permission);
            }
            // check if impersonator has PROXY_ANYONE version of permission
            Boolean adminPermission = security.impersonatorHasPermission(permissionCode + 
                    PermissionConstants.ADMIN_PROXY_SUFFIX);
            if (Boolean.TRUE.equals(adminPermission)) {
                // Only override if admin HAS the value, otherwise, use the existing version.
                permission.setEnabled(Boolean.TRUE);    
            }
            // update the permission list
            permissionList.add(permission);
        }
        proxyDto.setPermissions(permissionList);
            
        return proxyDto;
    }
    
    @Override
    public ProxyDTO updateProxyForPax(Long paxId, Long proxyPaxId, String requestType, ProxyDTO proxyDto) {
        
        validateUpdateRequest(requestType, paxId, proxyPaxId, proxyDto);
        
        // if we're here, request passed validation.
        for (PermissionDTO permission : proxyDto.getPermissions()) {
            if (permission.getEnabled() == true) {
                proxyDao.createProxyPermissionForPaxIfNotExists(permission.getPermissionCode(), paxId, 
                        proxyDto.getProxyPax().getPaxId());
            }
            else {
                proxyDao.deleteProxyPermissionForPaxIfExists(permission.getPermissionCode(), paxId, 
                        proxyDto.getProxyPax().getPaxId());
            }
        }
        
        EmployeeDTO pax = participantInfoDao.getEmployeeDTO(proxyDto.getProxyPax().getPaxId());
        proxyDto.setProxyPax(pax);
        proxyDto.setType(PermissionConstants.MY_PROXIES);
        
        return proxyDto;
    }

    @Override
    public void deleteProxyForPax(Long paxId, Long proxyPaxId) {
        List<ErrorMessage> errors = new ArrayList<>();
        
        Pax pax = paxDao.findById(proxyPaxId);
        if (pax == null) {
            errors.add(new ErrorMessage()
                .setField(ProjectConstants.PROXY_PAX)
                .setCode(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_CODE)
                .setMessage(String.format(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_MESSAGE, proxyPaxId)));
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        Boolean entriesExist = proxyDao.doesAclEntryExist(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST, 
                paxId, proxyPaxId);
        
        if (entriesExist == false) {
            errors.add(new ErrorMessage()
                .setField(ProjectConstants.PROXY_PAX)
                .setCode(PermissionConstants.PAX_IS_NOT_A_PROXY_ERROR_CODE)
                .setMessage(String.format(PermissionConstants.PAX_IS_NOT_A_PROXY_ERROR_MESSAGE, proxyPaxId, paxId)));
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        for (String permission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            proxyDao.deleteProxyPermissionForPaxIfExists(permission, paxId, proxyPaxId);
        }
    }

    @Override
    public Boolean doesPaxHaveProxyPermission(String proxyPermission, Long targetId, Long subjectId) {
        return proxyDao.doesAclEntryExist(Arrays.asList(proxyPermission), targetId, subjectId);
    }
    
    private void validateUpdateRequest(String requestType, Long loggedInPaxId, Long proxyPaxId, ProxyDTO proxyDto) {
        
        List<ErrorMessage> errors = new ArrayList<>();
        
        // validate pax exists
        if (proxyDto.getProxyPax() != null && proxyDto.getProxyPax().getPaxId() != null) {
            Pax pax = paxDao.findById(proxyDto.getProxyPax().getPaxId());
            if (pax == null) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PROXY_PAX)
                    .setCode(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_MESSAGE,    proxyDto.getProxyPax().getPaxId())));
            }
        }
        else {
            errors.add(PermissionConstants.PAX_MISSING_MESSAGE);
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        // validate proxy pax is not the logged in pax (paxId in the proxyDto or the proxyPaxId)
        if (proxyDto.getProxyPax().getPaxId().equals(loggedInPaxId) 
                || (proxyPaxId != null && proxyPaxId.equals(loggedInPaxId))) {
            errors.add(PermissionConstants.ASSIGN_SELF_PROXY_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);
        
        if (!RequestMethod.POST.toString().equalsIgnoreCase(requestType)) {            
            Pax pax = paxDao.findById(proxyPaxId);
            if (pax == null) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PROXY_PAX)
                    .setCode(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_MESSAGE, proxyPaxId)));
            }
            if (!proxyDto.getProxyPax().getPaxId().equals(proxyPaxId)) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PROXY_PAX)
                    .setCode(PermissionConstants.PAX_ID_NOT_SAME_AS_PATH_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.PAX_ID_NOT_SAME_AS_PATH_ERROR_MESSAGE, proxyPaxId, 
                            proxyDto.getProxyPax().getPaxId())));
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        // validate permissions are allowed
        for (PermissionDTO permission : proxyDto.getPermissions()) {
            if (!PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST.contains(permission.getPermissionCode())) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PERMISSIONS)
                    .setCode(PermissionConstants.INVALID_PERMISSION_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.INVALID_PERMISSION_ERROR_MESSAGE_PROXY, 
                            permission.getPermissionCode())));
            }
            if (permission.getEnabled() == null) {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PERMISSIONS)
                    .setCode(PermissionConstants.INVALID_PERMISSION_ENABLED_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.INVALID_PERMISSION_ENABLED_ERROR_MESSAGE_PROXY,
                            permission.getPermissionCode())));
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        //validate permissions are not duplicated
        List<String> uniquePermissionList = new ArrayList<>();
        for (PermissionDTO permission : proxyDto.getPermissions()) {
            if (!uniquePermissionList.contains(permission.getPermissionCode())) {
                uniquePermissionList.add(permission.getPermissionCode());
            }
            else {
                errors.add(new ErrorMessage()
                    .setField(ProjectConstants.PERMISSIONS)
                    .setCode(PermissionConstants.DUPLICATE_PERMISSION_ERROR_CODE)
                    .setMessage(String.format(PermissionConstants.DUPLICATE_PERMISSION_ERROR_MESSAGE, 
                            permission.getPermissionCode())));
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors);        
        
        Long subjectPaxId = proxyDto.getProxyPax().getPaxId();
        
        // Method specific validation first
        Boolean entriesExist = proxyDao.doesAclEntryExist(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST, 
                loggedInPaxId, subjectPaxId);
        if(RequestMethod.POST.toString().equalsIgnoreCase(requestType) && entriesExist == true) {
            // POST: No ACL entries with proxyDTO.paxId as subject and security.getPaxId() as target can exist
            errors.add(new ErrorMessage()
                .setField(ProjectConstants.REQUEST)
                .setCode(PermissionConstants.ENTRY_ALREADY_EXISTS_ERROR_CODE)
                .setMessage(String.format(PermissionConstants.ENTRY_ALREADY_EXISTS_ERROR_MESSAGE, subjectPaxId)));
        }
        else if((RequestMethod.PUT.toString().equalsIgnoreCase(requestType) || 
                RequestMethod.PATCH.toString().equalsIgnoreCase(requestType)) && entriesExist == false) {
            // PUT: At least one ACL entry with proxyDTO.paxId as subject and security.getPaxId() as target must exist
            errors.add(new ErrorMessage()
                .setField(ProjectConstants.REQUEST)
                .setCode(PermissionConstants.ENTRY_DOES_NOT_EXIST_ERROR_CODE)
                .setMessage(String.format(PermissionConstants.ENTRY_DOES_NOT_EXIST_ERROR_MESSAGE, subjectPaxId)));
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        if (RequestMethod.POST.toString().equalsIgnoreCase(requestType) || 
                RequestMethod.PUT.toString().equalsIgnoreCase(requestType)) { // no more validation required for PATCH
            
            // make sure all fields are present on POST / PUT
            for (String supportedPermission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
                Boolean found = false;
                for (PermissionDTO permission : proxyDto.getPermissions()) {
                    if (supportedPermission.equalsIgnoreCase(permission.getPermissionCode())) {
                        found = true;
                        break;
                    }
                }
                if (found == false) {
                    errors.add(new ErrorMessage()
                        .setField(ProjectConstants.PERMISSIONS)
                        .setCode(PermissionConstants.MISSING_PERMISSION_ERROR_CODE)
                        .setMessage(String.format(PermissionConstants.MISSING_PERMISSION_ERROR_MESSAGE_PROXY, 
                                supportedPermission)));
                }
            }
            
            ErrorMessageException.throwIfHasErrors(errors);
        }
        
        // validate at least one value is TRUE for POST
        if (RequestMethod.POST.toString().equalsIgnoreCase(requestType)) {
            
            boolean trueValue = false;
            
            for (PermissionDTO permission : proxyDto.getPermissions()) {
                if (permission.getEnabled() == true) {
                    trueValue = true;
                    break;
                }
            }
            
            if (!trueValue) {
                errors.add(PermissionConstants.POST_ALL_FALSE_MESSAGE);
            }
            
            ErrorMessageException.throwIfHasErrors(errors);
        }
    }
}
