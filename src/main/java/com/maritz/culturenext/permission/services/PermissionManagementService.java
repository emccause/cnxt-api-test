package com.maritz.culturenext.permission.services;

import java.util.List;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.permission.dto.GroupPermissionsDTO;
import com.maritz.culturenext.permission.dto.PermissionWrapperDTO;

public interface PermissionManagementService {

    /**
     * Returns permission data for the specified role ID's
     * 
     * @param commaSeparatedRoleList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459616/GET+list+of+Permissions+by+Type
     */
    List<PermissionWrapperDTO> getPermissionsForRoles(String commaSeparatedRoleList);

    /**
     * Update permissions for specific roles
     * 
     * @param requestType - type of request being made. PUT or PATCH
     * @param permissionWrapper - list of permissions tied to a specific role
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459714/Update+Permissions+by+Type
     */
    List<PermissionWrapperDTO> updatePermissionsForRole(String requestType, PermissionWrapperDTO permissionWrapper);
    
    /**
     * Returns permission data for the specified group Id's
     * @param String commaSeparatedGroupIdList - list of group ID's to get permission data for
     * @return List<PermissionWrapperDTO>
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459616/GET+list+of+Permissions+by+Type
     */
    List<PermissionWrapperDTO> getPermissionsForGroups(String commaSeparatedGroupIdList);

    /**
     * Update permissions for specific groups
     * 
     * @param requestType - type of request being made. PUT or PATCH
     * @param permissionWrapper - list of permissions tied to a specific group
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/27459714/Update+Permissions+by+Type
     */
    List<PermissionWrapperDTO> updatePermissionsForGroup(String requestType, PermissionWrapperDTO permissionWrapper);
    
    /**
     * Retrieving all groups that have the permissions passed in
     * @param commaSeparatedPermissionList - Permission list separated by comma.
     * @param filter - The "filter" param can be "OR" or "AND". The query will be using "or" or "and" logic for each permissions.
     * @param page - Page number to fetch
     * @param size - Size of the page to fetch
     * @return List of groups details and their audience permissions.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/66191399/GET+groups+by+permissions
     */
    PaginatedResponseObject<GroupPermissionsDTO> getGroupsByPermissions (String commaSeparatedPermissionList, String filter
            , Integer page, Integer size );
}
