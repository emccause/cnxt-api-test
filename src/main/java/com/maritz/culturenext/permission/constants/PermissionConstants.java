package com.maritz.culturenext.permission.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;

public class PermissionConstants {
    
    // Endpoints
    public static final String GROUP_PERMISSIONS_REST_ENDPOINT = "/rest/users/groups/permissions";
    
    // ACL subject types
    public static final String ROLE_ACL_TYPE = "ROLE";
    public static final String GROUPS_ACL_TYPE = "GROUPS";
    
    // Roles used
    public static final String ADMIN_ROLE_NAME = "ADMIN";
    public static final String MARITZ_ADMIN_ROLE_NAME = "MTZ:ADM";
    public static final String CLIENT_ADMIN_ROLE_NAME = "CADM";
    public static final String MANAGER_ROLE_NAME = "MANAGER";
    public static final String PARTICIPANT_ROLE_NAME = "PARTICIPANT";
    public static final String ALLOCATOR_ROLE_NAME = "BUDGET_ALLOCATOR";

    public static final List<String> SUPPORTED_ROLES = Arrays.asList(ADMIN_ROLE_NAME, MANAGER_ROLE_NAME, 
            PARTICIPANT_ROLE_NAME);
    
    //Audience Permissioning
    public static final String ENABLE_PREFIX = "ENABLE_";
    public static final String DISABLE_PREFIX = "DISABLE_";
    public static final String COMMENTING_TYPE = "COMMENTING";
    public static final String LIKING_TYPE = "LIKING";
    public static final String ENGAGEMENT_SCORE_TYPE = "ENGAGE_SCORE";
    public static final String NETWORK_TYPE = "NETWORK";
    public static final String CALENDAR_TYPE = "CALENDAR";
    public static final List<String> VALID_AUDIENCE_PERMISSION_TYPES = Arrays.asList(
            COMMENTING_TYPE, LIKING_TYPE, ENGAGEMENT_SCORE_TYPE, NETWORK_TYPE, CALENDAR_TYPE);
    public static final Map<String, String> AUDIENCE_PERM_MAP = new HashMap<String, String>();
    static 
    {
        //Initialize the map
        AUDIENCE_PERM_MAP.put(COMMENTING_TYPE, ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED);
        AUDIENCE_PERM_MAP.put(LIKING_TYPE, ApplicationDataConstants.KEY_NAME_LIKING_ENABLED);
        AUDIENCE_PERM_MAP.put(ENGAGEMENT_SCORE_TYPE, ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED);
        AUDIENCE_PERM_MAP.put(NETWORK_TYPE, ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED);
        AUDIENCE_PERM_MAP.put(CALENDAR_TYPE, ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED);
    }
    
    // Fields
    public static final String PERMISSION_CODES_LIST_FIELD = "permissionCodesList";
    
    // Errors for role validation
    public static final String MISSING_ROLE_NAME_ERROR_CODE = "MISSING_ROLE_NAME";
    public static final String MISSING_ROLE_NAME_ERROR_MESSAGE = "Must provide at least one role name.";

    public static final String INVALID_ROLE_NAME_ERROR_CODE = "INVALID_ROLE_NAME";
    public static final String INVALID_ROLE_NAME_ERROR_MESSAGE = 
            "%s is not a valid role. Valid roles are ADMIN, MANAGER, PARTICIPANT.";

    public static final String CANNOT_MODIFY_GROUPS_HERE_ERROR_CODE = "CANNOT_MODIFY_GROUPS_HERE";
    public static final String CANNOT_MODIFY_GROUPS_HERE_ERROR_MESSAGE = 
            "Use the groups endpoint to update permissions for groups.";

    public static final String ROLE_MISSING_ERROR_CODE = "ROLE_MISSING";
    public static final String ROLE_MISSING_ERROR_MESSAGE = "Must provide role to modify permissions.";
    
    public static final String ROLE_PERMISSION_ERROR_TEXT = "the %s role.";
    
    // Errors for group validation
    public static final String MISSING_GROUP_ID_ERROR_CODE = "MISSING_GROUP_ID";
    public static final String MISSING_GROUP_ID_ERROR_MESSAGE = "Must provide at least one group id.";
    public static final String GROUP_MISSING_ERROR_MESSAGE = "Must provide group to modify permissions.";

    public static final String INVALID_GROUP_ID_ERROR_CODE = "INVALID_GROUP_ID";
    public static final String INVALID_GROUP_ID_ERROR_MESSAGE = "Group id %s is not a valid number.";
    
    public static final String GROUP_DOES_NOT_EXIST_ERROR_CODE = "GROUP_DOES_NOT_EXIST";
    public static final String GROUP_DOES_NOT_EXIST_ERROR_MESSAGE = "Group id %d does not exist.";
    
    public static final String CANNOT_MODIFY_ROLES_HERE_ERROR_CODE = "CANNOT_MODIFY_ROLES_HERE";
    public static final String CANNOT_MODIFY_ROLES_HERE_ERROR_MESSAGE = 
            "Use the role endpoint to update permissions for roles.";
    
    
    public static final String GROUPS_PERMISSION_ERROR_TEXT = "group id %d.";
    
    // errors for permission validation
    public static final String INVALID_PERMISSION_ERROR_CODE = "INVALID_PERMISSION";
    public static final String INVALID_PERMISSION_ERROR_MESSAGE = "The '%s' permission cannot be set for %s.";
    public static final String INVALID_PERMISSION_ERROR_MESSAGE_PROXY = "The '%s' proxy permission cannot be set.";
    
    public static final String INVALID_PERMISSION_ENABLED_ERROR_CODE = "INVALID_PERMISSION_ENABLED";
    public static final String INVALID_PERMISSION_ENABLED_ERROR_MESSAGE = 
            "Must provide a value for enabled for the '%s' permission.";
    public static final String INVALID_PERMISSION_ENABLED_ERROR_MESSAGE_PROXY = 
            "A value must be provided for enabled for the '%s' permission.";

    public static final String MISSING_PERMISSION_ERROR_CODE = "MISSING_PERMISSION";
    public static final String MISSING_PERMISSION_ERROR_MESSAGE = "The '%s' permission must be provided for %s.";
    public static final String MISSING_PERMISSION_ERROR_MESSAGE_PROXY = "The '%s' permission must be provided.";
    
    public static final String MISSING_PERMISSION_LIST_ERROR_CODE = "MISSING_PERMISSION_LIST";
    public static final String MISSING_PERMISSION_LIST_ERROR_MESSAGE = "Permission list is missing.";
    
    public static final String DUPLICATE_PERMISSION_ERROR_CODE = "DUPLICATE_PERMISSION";
    public static final String DUPLICATE_PERMISSION_ERROR_MESSAGE = "Please remove the duplicated permission '%s'.";
    
    public static final String POST_ALL_FALSE_ERROR_CODE = "POST_ALL_FALSE";
    public static final String POST_ALL_FALSE_ERROR_MESSAGE = "If all permissions are FALSE use a DELETE or PUT.";
    
    public static final String MISSING_PERMISSIONS_CODE = "MISSING_PERMISSIONS_CODE";
    public static final String MISSING_PERMISSIONS_CODE_ERROR_MESSAGE = "Permission list is missing.";
    public static final String INVALID_PERMISSIONS_CODE = "INVALID_PERMISSIONS_CODE";
    public static final String INVALID_PERMISSIONS_CODE_ERROR_MESSAGE = "Permission code is invalid";
    public static final String MISSING_PERMISSION_FILTER_CODE = "MISSING_PERMISSION_FILTER_CODE";
    public static final String MISSING_PERMISSION_FILTER_CODE_ERROR_MESSAGE = "Filter is missing.";
    public static final String INVALID_PERMISSION_FILTER_CODE = "INVALID_PERMISSION_FILTER_CODE";
    public static final String INVALID_PERMISSION_FILTER_CODE_ERROR_MESSAGE = "Filter is invalid";
    
    // proxy roles
    public static final String PROXY_BUDGETS = "PROXY_BUDGETS";
    public static final String PROXY_SETTINGS = "PROXY_SETTINGS";
    public static final String PROXY_GIVE_REC = "PROXY_GIVE_REC";
    public static final String PROXY_APPROVE_REC = "PROXY_APPROVE_REC";
    public static final String PROXY_VIEW_REPORTS = "PROXY_VIEW_REPORTS";
    public static final String MY_PROXIES = "MY_PROXIES";
    public static final String PROXY_AS = "PROXY_AS";
    public static final String ADMIN_PROXY_SUFFIX = "_ANYONE";
    
    public static final List<String> SUPPORTED_PROXY_PERMISSION_LIST = Arrays.asList(PROXY_GIVE_REC, PROXY_APPROVE_REC,
            PROXY_VIEW_REPORTS, PROXY_BUDGETS, PROXY_SETTINGS);
    public static final List<String> SUPPORTED_PROXY_TYPE_LIST = Arrays.asList(MY_PROXIES, PROXY_AS);
    
    public static final List<String> DEFAULT_PROXY_STATUS_LIST = Arrays.asList(StatusTypeCode.ACTIVE.name(),
            StatusTypeCode.NEW.name(), StatusTypeCode.LOCKED.name(), StatusTypeCode.SUSPENDED.name(), 
            StatusTypeCode.RESTRICTED.name());
    
    // proxy errors
    public static final String MISSING_TYPE_ERROR_CODE = "MISSING_TYPE";
    public static final String MISSING_TYPE_ERROR_MESSAGE = 
            "Proxy type must be provided. Valid types are MY_PROXIES and PROXY_AS.";
    
    public static final String INVALID_TYPE_ERROR_CODE = "INVALID_TYPE";
    public static final String INVALID_TYPE_ERROR_MESSAGE = 
            "%s is not a valid proxy type. Valid types are MY_PROXIES, PROXY_AS";

    public static final String PAX_DOES_NOT_EXIST_ERROR_CODE = "PAX_DOES_NOT_EXIST";
    public static final String PAX_DOES_NOT_EXIST_ERROR_MESSAGE = "Pax id %d does not exist";
    
    public static final String PAX_MISSING_ERROR_CODE = "PAX_MISSING";
    public static final String PAX_MISSING_ERROR_MESSAGE = "Must provide a pax to set proxy permissions for.";

    public static final String ENTRY_ALREADY_EXISTS_ERROR_CODE = "ENTRY_ALREADY_EXISTS";
    public static final String ENTRY_ALREADY_EXISTS_ERROR_MESSAGE = 
            "Permissions already exist for pax %d. You will need to update them.";
    
    public static final String ENTRY_DOES_NOT_EXIST_ERROR_CODE = "ENTRY_DOES_NOT_EXIST";
    public static final String ENTRY_DOES_NOT_EXIST_ERROR_MESSAGE = 
            "Permissions do not exist for pax %d. You will need to create them first.";

    public static final String PAX_ID_NOT_SAME_AS_PATH_ERROR_CODE = "PAX_ID_NOT_SAME_AS_PATH";
    public static final String PAX_ID_NOT_SAME_AS_PATH_ERROR_MESSAGE = 
            "Proxy pax ids must match.  Request path has pax id %d for proxy, body has %d.";

    public static final String PAX_IS_NOT_A_PROXY_ERROR_CODE = "PAX_IS_NOT_A_PROXY";
    public static final String PAX_IS_NOT_A_PROXY_ERROR_MESSAGE = "Pax id %d is not a proxy for pax %d.";
    
    public static final String ASSIGN_SELF_PROXY_ERROR_CODE = "ASSIGN_SELF_PROXY";
    public static final String ASSIGN_SELF_PROXY_ERROR_MESSAGE = "Proxy paxID cannot be the same as the target paxID.";
    
    // errors for proxying.
    public static final String PROXY_NOT_ALLOWED_ERROR_CODE = "DOES_NOT_HAVE_PROXY_PERMISSION";
    public static final String PROXY_NOT_ALLOWED_ERROR_MESSAGE = 
            "You do not have permission to do that while proxying.";
    
    //Error Messages
    public static final ErrorMessage PROXY_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PROXY)
        .setCode(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_CODE)
        .setMessage(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
    
    public static final ErrorMessage PAX_MISSING_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PROXY_PAX)
        .setCode(PermissionConstants.PAX_MISSING_ERROR_CODE)
        .setMessage(PermissionConstants.PAX_MISSING_ERROR_MESSAGE);
    
    public static final ErrorMessage ASSIGN_SELF_PROXY_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PROXY_PAX)
        .setCode(PermissionConstants.ASSIGN_SELF_PROXY_ERROR_CODE)
        .setMessage(PermissionConstants.ASSIGN_SELF_PROXY_ERROR_MESSAGE);
    
    public static final ErrorMessage POST_ALL_FALSE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PERMISSIONS)
        .setCode(PermissionConstants.POST_ALL_FALSE_ERROR_CODE)
        .setMessage(PermissionConstants.POST_ALL_FALSE_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_GROUP_ID_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.GROUP_ID)
        .setCode(PermissionConstants.MISSING_GROUP_ID_ERROR_CODE)
        .setMessage(PermissionConstants.MISSING_GROUP_ID_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_ROLE_NAME_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.ROLE_NAME)
        .setCode(PermissionConstants.MISSING_ROLE_NAME_ERROR_CODE)
        .setMessage(PermissionConstants.MISSING_ROLE_NAME_ERROR_MESSAGE);
    
    public static final ErrorMessage CANNOT_MODIFY_GROUPS_HERE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.GROUP_ID)
        .setCode(PermissionConstants.CANNOT_MODIFY_GROUPS_HERE_ERROR_CODE)
        .setMessage(PermissionConstants.CANNOT_MODIFY_GROUPS_HERE_ERROR_MESSAGE);
    
    public static final ErrorMessage CANNOT_MODIFY_ROLES_HERE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.ROLE)
        .setCode(PermissionConstants.CANNOT_MODIFY_ROLES_HERE_ERROR_CODE)
        .setMessage(PermissionConstants.CANNOT_MODIFY_ROLES_HERE_ERROR_MESSAGE);
    
    public static final ErrorMessage ROLE_MISSING_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.ROLE)
        .setCode(PermissionConstants.ROLE_MISSING_ERROR_CODE)
        .setMessage(PermissionConstants.ROLE_MISSING_ERROR_MESSAGE);
    
    public static final ErrorMessage GROUP_MISSING_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.GROUP_ID)
        .setCode(PermissionConstants.MISSING_GROUP_ID_ERROR_CODE)
        .setMessage(PermissionConstants.GROUP_MISSING_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_PERMISION_LIST_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PERMISSIONS)
        .setCode(PermissionConstants.MISSING_PERMISSION_LIST_ERROR_CODE)
        .setMessage(PermissionConstants.MISSING_PERMISSION_LIST_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_PERMISSIONS_CODES_ERROR = new ErrorMessage()
            .setField(ProjectConstants.PERMISSIONS)
            .setCode(MISSING_PERMISSIONS_CODE)
            .setMessage(MISSING_PERMISSIONS_CODE_ERROR_MESSAGE);
    
    public static final ErrorMessage INVALID_PERMISSION_ERROR = new ErrorMessage()
            .setField(ProjectConstants.PERMISSIONS)
            .setCode(INVALID_PERMISSIONS_CODE)
            .setMessage(INVALID_PERMISSIONS_CODE_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_PERMISSION_FILTER_ERROR = new ErrorMessage()
            .setField(ProjectConstants.FILTER)
            .setCode(MISSING_PERMISSION_FILTER_CODE)
            .setMessage(MISSING_PERMISSION_FILTER_CODE_ERROR_MESSAGE);
    
    public static final ErrorMessage INVALID_PERMISSION_FILTER_ERROR = new ErrorMessage()
            .setField(ProjectConstants.FILTER)
            .setCode(INVALID_PERMISSION_FILTER_CODE)
            .setMessage(INVALID_PERMISSION_FILTER_CODE_ERROR_MESSAGE);
}
