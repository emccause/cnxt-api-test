package com.maritz.culturenext.permission.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.ProxyDao;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.ProxyDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

@Repository
public class ProxyDaoImpl extends AbstractDaoImpl implements ProxyDao {

    // sql query result names
    private static final String PROXY_PERMISSION = "proxyPermission";
    private static final String PROXY_TYPE = "proxyType";

    // param placeholders
    private static final String PIVOT_PAX_ID_PLACEHOLDER = "PIVOT_PAX_ID";
    private static final String PROXY_ROLE_LIST_PLACEHOLDER =  "PROXY_ROLE_LIST";
    private static final String PROXY_TYPE_LIST_PLACEHOLDER =  "PROXY_TYPE_LIST";
    private static final String STATUS_LIST_PLACEHOLDER =  "STATUS_LIST";
    
    private static final String PERMISSION_ROLE_CODE_PLACEHOLDER = "PROXY_PERMISSION_ROLE_CODE";
    private static final String SUBJECT_ID_PLACEHOLDER = "SUBJECT_ID";
    private static final String TARGET_ID_PLACEHOLDER = "TARGET_ID";
    
    private static final String PROXY_QUERY =
            "SELECT * FROM ( " +
                "SELECT * FROM ( " + // MY PROXIES - LOGGED IN PAX = TARGET, PEOPLE WHO CAN PROXY = SUBJECT
                    "SELECT DISTINCT 'MY_PROXIES' AS 'PROXY_TYPE', ROLE.ROLE_CODE AS 'PROXY_PERMISSION', ACL.TARGET_ID AS PIVOT_PAX, PAX_DATA.* " +
                    "FROM component.VW_USER_GROUPS_SEARCH_DATA PAX_DATA " +
                    "INNER JOIN component.ACL " +
                        "ON SUBJECT_ID = PAX_DATA.PAX_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'PAX' " +
                        "AND ACL.TARGET_TABLE = 'PAX' " +
                        "AND ROLE.ROLE_CODE IN (:" + PROXY_ROLE_LIST_PLACEHOLDER + ") " +
                        "AND PAX_DATA.STATUS_TYPE_CODE IN (:" + STATUS_LIST_PLACEHOLDER + ") " +
                ") MY_PROXIES " +
                "UNION " +
                "SELECT * FROM ( " + // PROXY AS - LOGGED IN PAX = SUBJECT, PEOPLE LOGGED IN PAX CAN PROXY AS = TARGET
                    "SELECT DISTINCT 'PROXY_AS' AS 'PROXY_TYPE', ROLE.ROLE_CODE AS 'PROXY_PERMISSION', ACL.SUBJECT_ID AS PIVOT_PAX, PAX_DATA.* " +
                    "FROM component.VW_USER_GROUPS_SEARCH_DATA PAX_DATA " +
                    "INNER JOIN component.ACL " +
                        "ON TARGET_ID = PAX_DATA.PAX_ID " +
                    "INNER JOIN component.ROLE " +
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
                    "WHERE ACL.SUBJECT_TABLE = 'PAX' " +
                        "AND ACL.TARGET_TABLE = 'PAX' " +
                        "AND ROLE.ROLE_CODE IN (:" + PROXY_ROLE_LIST_PLACEHOLDER + ") " +
                        "AND PAX_DATA.STATUS_TYPE_CODE IN (:" + STATUS_LIST_PLACEHOLDER + ") " +
                ") PROXY_AS " +
            ") PROXY_DATA " +
                "WHERE PROXY_DATA.PIVOT_PAX = :" + PIVOT_PAX_ID_PLACEHOLDER + " " +
                    "AND PROXY_TYPE IN (:" + PROXY_TYPE_LIST_PLACEHOLDER + ") " +
                    "ORDER BY PROXY_DATA.NAME";
    
    private static final String PROXY_FOR_PAX_QUERY = 
            "SELECT DISTINCT r.ROLE_CODE AS 'PROXY_PERMISSION', a.SUBJECT_ID AS PIVOT_PAX, pax.* " +
            "FROM component.ACL a " +
            "JOIN component.ROLE r ON r.ROLE_ID = a.ROLE_ID " +
            "JOIN component.VW_USER_GROUPS_SEARCH_DATA pax ON pax.PAX_ID = a.SUBJECT_ID " +
            "WHERE r.ROLE_CODE IN (:" + PROXY_ROLE_LIST_PLACEHOLDER + ") " +
            "AND a.TARGET_TABLE = 'PAX' " +
            "AND a.SUBJECT_TABLE = 'PAX' " +
            "AND a.TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " " + 
            "AND a.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER;
    
    private static final String CHECK_IF_ACL_EXISTS_QUERY =
            "SELECT * FROM component.ACL " +
            "INNER JOIN component.ROLE " +
                "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
            "WHERE ROLE.ROLE_CODE IN (:" + PROXY_ROLE_LIST_PLACEHOLDER + ") " +
                "AND ACL.TARGET_TABLE = 'PAX' " + 
                "AND ACL.TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " " +
                "AND ACL.SUBJECT_TABLE = 'PAX' " +
                "AND ACL.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER;
    

    private static final String INSERT_ACL_IF_NOT_EXISTS = 
            "IF NOT EXISTS ( " + 
                "SELECT * FROM ACL " + 
                "INNER JOIN ROLE " + 
                    "ON ACL.ROLE_ID = ROLE.ROLE_ID " + 
                "WHERE ACL.TARGET_TABLE = 'PAX' " + 
                    "AND ACL.TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " " +
                    "AND ACL.SUBJECT_TABLE = 'PAX' " +
                    "AND ACL.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " +
                    "AND ROLE.ROLE_CODE = :" + PERMISSION_ROLE_CODE_PLACEHOLDER + " " + 
            ") " + 
            "BEGIN " + 
                "INSERT INTO ACL (ROLE_ID, TARGET_TABLE, TARGET_ID, SUBJECT_TABLE, SUBJECT_ID) "
                + "VALUES (("
                        + "SELECT ROLE_ID "
                        + "FROM ROLE "
                        + "WHERE ROLE_CODE = :" + PERMISSION_ROLE_CODE_PLACEHOLDER + "), "
                + "'PAX',:" + TARGET_ID_PLACEHOLDER + ", 'PAX', :" + SUBJECT_ID_PLACEHOLDER + ") " + 
            "END";

    private static final String DELETE_ACL_IF_EXISTS = 
            "IF EXISTS ( " + 
                    "SELECT * FROM ACL " + 
                    "INNER JOIN ROLE " + 
                        "ON ACL.ROLE_ID = ROLE.ROLE_ID " + 
                    "WHERE ACL.TARGET_TABLE = 'PAX' " + 
                        "AND ACL.TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " " +
                        "AND ACL.SUBJECT_TABLE = 'PAX' " +
                        "AND ACL.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " +
                        "AND ROLE.ROLE_CODE = :" + PERMISSION_ROLE_CODE_PLACEHOLDER + " " + 
            ") " + 
            "BEGIN " + 
                "DELETE FROM ACL "
                + "WHERE ROLE_ID = (SELECT ROLE_ID FROM ROLE WHERE ROLE_CODE = :" + PERMISSION_ROLE_CODE_PLACEHOLDER + ") "
                    + "AND TARGET_TABLE = 'PAX' "
                    + "AND TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " "
                    + "AND SUBJECT_TABLE = 'PAX' "
                    + "AND SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
            "END";


    @Override
    public List<ProxyDTO> getProxiesForPax(List<String> proxyPermissionList, Long paxId, 
            List<String> proxyTypeList, List<String> statusList) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROXY_ROLE_LIST_PLACEHOLDER, proxyPermissionList);
        params.addValue(PIVOT_PAX_ID_PLACEHOLDER, paxId);
        params.addValue(PROXY_TYPE_LIST_PLACEHOLDER, proxyTypeList);
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROXY_QUERY, params, new CamelCaseMapRowMapper());

        List<ProxyDTO> proxyList = new ArrayList<>();

        for (Map<String, Object> node : nodes) {
            EmployeeDTO pax = new EmployeeDTO(node, null);
            String proxyType = (String) node.get(PROXY_TYPE);
            String proxyPermission = (String) node.get(PROXY_PERMISSION);

            boolean found = false;
            for (ProxyDTO existingProxy : proxyList) { // update one of the existing records if a match exists
                if (existingProxy.getProxyPax().getPaxId().equals(pax.getPaxId())
                        && existingProxy.getType().equalsIgnoreCase(proxyType)) {
                    found = true;
                    List<PermissionDTO> permissionList = existingProxy.getPermissions();
                    permissionList.add(new PermissionDTO().setPermissionCode(proxyPermission).setEnabled(Boolean.TRUE));
                    existingProxy.setPermissions(permissionList);
                    break;
                }
            }
            if (found == false) { // doesn't exist, create
                ProxyDTO proxy = new ProxyDTO();
                proxy.setProxyPax(pax);
                proxy.setType(proxyType);
                List<PermissionDTO> permissionList = new ArrayList<>();
                permissionList.add(new PermissionDTO().setPermissionCode(proxyPermission).setEnabled(Boolean.TRUE));
                proxy.setPermissions(permissionList);
                proxyList.add(proxy);
            }

        }

        // populate all the FALSE values (if there's not already a permission entry, add an entry with enabled = false)
        for (ProxyDTO proxy : proxyList) {
            for (String proxyPermission : proxyPermissionList) {
                boolean found = false;
                for (PermissionDTO permission : proxy.getPermissions()) {
                    if (proxyPermission.equalsIgnoreCase(permission.getPermissionCode())) {
                        found = true;
                        break;
                    }
                }
                if (found == false) {
                    List<PermissionDTO> permissionList = proxy.getPermissions();
                    permissionList.add(new PermissionDTO().setPermissionCode(proxyPermission).setEnabled(Boolean.FALSE));
                    proxy.setPermissions(permissionList);
                }
            }
        }

        return proxyList;
    }

    @Override
    public Boolean doesAclEntryExist(List<String> proxyPermissionList, Long targetId, Long subjectId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROXY_ROLE_LIST_PLACEHOLDER, proxyPermissionList);
        params.addValue(TARGET_ID_PLACEHOLDER, targetId);
        params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(CHECK_IF_ACL_EXISTS_QUERY, params, new CamelCaseMapRowMapper());
        return !nodes.isEmpty();
    }

    @Override
    public void createProxyPermissionForPaxIfNotExists(String permissionName, Long targetId, Long subjectId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PERMISSION_ROLE_CODE_PLACEHOLDER, permissionName);
        params.addValue(TARGET_ID_PLACEHOLDER, targetId);
        params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);
        
        namedParameterJdbcTemplate.update(INSERT_ACL_IF_NOT_EXISTS, params);
    }

    @Override
    public void deleteProxyPermissionForPaxIfExists(String permissionName, Long targetId, Long subjectId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PERMISSION_ROLE_CODE_PLACEHOLDER, permissionName);
        params.addValue(TARGET_ID_PLACEHOLDER, targetId);
        params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);
        
        namedParameterJdbcTemplate.update(DELETE_ACL_IF_EXISTS, params);
    }
    
    @Override
    public ProxyDTO getProxyForSpecificPax(List<String> proxyPermissionList, Long paxId, Long proxyPaxId, String proxyType) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROXY_ROLE_LIST_PLACEHOLDER, proxyPermissionList);
        
        switch (proxyType) {
            case (PermissionConstants.MY_PROXIES):
                params.addValue(TARGET_ID_PLACEHOLDER, paxId);
                params.addValue(SUBJECT_ID_PLACEHOLDER, proxyPaxId);
                break;
            case (PermissionConstants.PROXY_AS):
                params.addValue(TARGET_ID_PLACEHOLDER, proxyPaxId);
                params.addValue(SUBJECT_ID_PLACEHOLDER, paxId);
                break;
            default:
                //Should never get here
                throw new ErrorMessageException().addErrorMessage(
                    new ErrorMessage()
                        .setField(ProjectConstants.TYPE)
                        .setCode(PermissionConstants.MISSING_TYPE_ERROR_CODE)
                        .setMessage(PermissionConstants.MISSING_TYPE_ERROR_MESSAGE));
        }

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROXY_FOR_PAX_QUERY, params, new CamelCaseMapRowMapper());

        if (!CollectionUtils.isEmpty(nodes)) {
            ProxyDTO proxyDTO = new ProxyDTO();
            proxyDTO.setProxyPax(new EmployeeDTO(nodes.get(0), null));
            proxyDTO.setType(proxyType);
            List<PermissionDTO> permissionList = new ArrayList<>();
            List<String> permissionStrings = new ArrayList<>();

            for (Map<String, Object> node : nodes) {
                String proxyPermission = (String) node.get(PROXY_PERMISSION);
                permissionList.add(new PermissionDTO().setPermissionCode(proxyPermission).setEnabled(Boolean.TRUE));
                permissionStrings.add(proxyPermission);
            }

            // populate all the FALSE values (if there's not already a permission entry, add an entry with enabled = false)
            for (String proxyPermission : proxyPermissionList) {
                if(!permissionStrings.contains(proxyPermission)){
                    permissionList.add(new PermissionDTO().setPermissionCode(proxyPermission).setEnabled(Boolean.FALSE));
                }
            }

            //Save the permission list to the DTO
            proxyDTO.setPermissions(permissionList);
            return proxyDTO;
        }

        return null;


    }
}
