package com.maritz.culturenext.permission.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.PermissionResolutionDao;

@Repository
public class PermissionResolutionDaoImpl extends PaginatingDaoImpl implements PermissionResolutionDao {
    
    // param placeholders
    private static final String ROLE_NAME_LIST_PLACEHOLDER = "ROLE_NAME_LIST";
    private static final String PERMISSION_LIST_PLACEHOLDER = "PERMISSION_LIST";
    private static final String GROUP_ID_LIST_PLACEHOLDER = "GROUP_ID_LIST";
    private static final String SUBJECT_TABLE_PLACEHOLDER = "SUBJECT_TABLE";
    private static final String SUBJECT_ID_PLACEHOLDER = "SUBJECT_ID";
    private static final String ROLE_NAME_PLACEHOLDER = "ROLE_NAME";
    private static final String PERMISSIONS_CODES_FILTER_PLACEHOLDER = "PERMISSIONS_CODES_FILTER";
    
    // sql query result names
    private static final String PERMISSION_NAME = "PERMISSION_NAME";
    private static final String ROLE_CODE = "ROLE_CODE";
    private static final String GROUP_ID = "GROUP_ID";
    
    private static final String ROLES_PERMISSION_QUERY =
            "SELECT " + 
                "ROLE.ROLE_NAME AS " + ROLE_CODE + ", " + 
                "PERMISSION.PERMISSION_CODE AS " + PERMISSION_NAME + " " + 
            "FROM component.ROLE " + 
            "INNER JOIN component.ROLE_PERMISSION " + 
                "ON ROLE.ROLE_ID = ROLE_PERMISSION.ROLE_ID " + 
            "INNER JOIN component.PERMISSION " + 
                "ON ROLE_PERMISSION.PERMISSION_ID = PERMISSION.PERMISSION_ID " +
            "WHERE ROLE.ROLE_NAME IN (:" + ROLE_NAME_LIST_PLACEHOLDER + ") " +
                "AND PERMISSION.PERMISSION_CODE IN (:" + PERMISSION_LIST_PLACEHOLDER + ")";
    
    private static final String GROUPS_PERMISSION_QUERY =
            "SELECT ACL.SUBJECT_ID AS " + GROUP_ID + ", PERMISSION_ROLE.ROLE_CODE AS " + PERMISSION_NAME + " " +
            "FROM ACL " +
            "INNER JOIN ROLE PERMISSION_ROLE " +
                "ON ACL.ROLE_ID = PERMISSION_ROLE.ROLE_ID " +
            "WHERE ACL.SUBJECT_ID IN (:" + GROUP_ID_LIST_PLACEHOLDER + ") " +
                "AND PERMISSION_ROLE.ROLE_CODE IN (:" + PERMISSION_LIST_PLACEHOLDER + ") " +
                "AND ACL.SUBJECT_TABLE = 'GROUPS'";
    
    private static final String INSERT_ACL_IF_NOT_EXISTS = 
            "IF NOT EXISTS ( " + 
                "SELECT * FROM ACL " + 
                "INNER JOIN ROLE " + 
                    "ON ACL.ROLE_ID = ROLE.ROLE_ID " + 
                "WHERE ACL.SUBJECT_TABLE = :" + SUBJECT_TABLE_PLACEHOLDER + " " + 
                    "AND ACL.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
                    "AND ROLE.ROLE_NAME = :" + ROLE_NAME_PLACEHOLDER + " " + 
            ") " + 
            "BEGIN " + 
                "INSERT INTO ACL (ROLE_ID, SUBJECT_TABLE, SUBJECT_ID) "
                    + "VALUES (("
                        + "SELECT ROLE_ID "
                        + "FROM ROLE "
                        + "WHERE ROLE_NAME = :" + ROLE_NAME_PLACEHOLDER + "), "
                    + ":" + SUBJECT_TABLE_PLACEHOLDER + ", :" + SUBJECT_ID_PLACEHOLDER + ") " 
                + "END";

    private static final String DELETE_ACL_IF_EXISTS = 
            "IF EXISTS ( " + 
                "SELECT * FROM ACL " + 
                "INNER JOIN ROLE " + 
                    "ON ACL.ROLE_ID = ROLE.ROLE_ID " + 
                "WHERE ACL.SUBJECT_TABLE = :" + SUBJECT_TABLE_PLACEHOLDER + " " + 
                    "AND ACL.SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
                    "AND ROLE.ROLE_NAME = :" + ROLE_NAME_PLACEHOLDER + " " + 
            ") " + 
            "BEGIN " + 
                "DELETE FROM ACL "
                + "WHERE ROLE_ID = ("
                        + "SELECT ROLE_ID "
                        + "FROM ROLE "
                        + "WHERE ROLE_NAME = :" + ROLE_NAME_PLACEHOLDER + ") "
                    + "AND SUBJECT_TABLE = :" + SUBJECT_TABLE_PLACEHOLDER + " "
                    + "AND SUBJECT_ID = :" + SUBJECT_ID_PLACEHOLDER + " " 
                + "END";
    
    private static final String INSERT_ROLE_PERMISSION_IF_NOT_EXISTS =
            "IF NOT EXISTS ( " + 
                "SELECT * " +
                "FROM component.ROLE_PERMISSION " + 
                "INNER JOIN component.PERMISSION " + 
                    "ON ROLE_PERMISSION.PERMISSION_ID = PERMISSION.PERMISSION_ID " + 
                "WHERE ROLE_PERMISSION.ROLE_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
                    "AND PERMISSION.PERMISSION_CODE = :" + ROLE_NAME_PLACEHOLDER + " " + 
            ") " + 
            "BEGIN " + 
                "INSERT INTO component.ROLE_PERMISSION (ROLE_ID, PERMISSION_ID) " + 
                "VALUES (:" + SUBJECT_ID_PLACEHOLDER + ", (SELECT PERMISSION_ID FROM component.PERMISSION WHERE PERMISSION_CODE = :" + ROLE_NAME_PLACEHOLDER + ")) " + 
            "END";
    
    private static final String DELETE_ROLE_PERMISSION_IF_EXISTS =
            "IF EXISTS ( " + 
                    "SELECT * " +
                    "FROM component.ROLE_PERMISSION " + 
                    "INNER JOIN component.PERMISSION " + 
                        "ON ROLE_PERMISSION.PERMISSION_ID = PERMISSION.PERMISSION_ID " + 
                    "WHERE ROLE_PERMISSION.ROLE_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
                    "AND PERMISSION.PERMISSION_CODE = :" + ROLE_NAME_PLACEHOLDER + " " + 
                ") " + 
                "BEGIN " + 
                    "DELETE FROM component.ROLE_PERMISSION " + 
                    "WHERE ROLE_ID = :" + SUBJECT_ID_PLACEHOLDER + " " + 
                        "AND PERMISSION_ID = (SELECT PERMISSION_ID FROM component.PERMISSION WHERE PERMISSION_CODE = :" + ROLE_NAME_PLACEHOLDER + ") " + 
                "END";
    
    private static final String GET_GROUPS_BY_PERMISSION = 
            "SELECT " + 
                "VW.GROUP_ID, VW.GROUP_CONFIG_ID, VW.FIELD, VW.FIELD_DISPLAY_NAME, VW.GROUP_NAME, " + 
                "VW.GROUP_TYPE, VW.VISIBILITY,VW.STATUS_TYPE_CODE, VW.CREATE_DATE, VW.PAX_COUNT, VW.GROUP_COUNT, " +
                "VW.TOTAL_PAX_COUNT, VW.GROUP_PAX_ID, " + 
                "LEFT(PC.PERMISSION_CODES, LEN(PC.PERMISSION_CODES)-1) AS PERMISSION_CODES_LIST " +
            "FROM VW_USER_GROUPS_SEARCH_DATA VW " + 
            "CROSS APPLY ( " +
                "SELECT " + 
                    "P.PERMISSION_CODE+',' " +
                "FROM PERMISSION P " +
                "INNER JOIN ROLE_PERMISSION RP ON P.PERMISSION_ID = RP.PERMISSION_ID " +
                "INNER JOIN ACL A ON RP.ROLE_ID = A.ROLE_ID AND A.SUBJECT_TABLE = 'GROUPS' AND A.SUBJECT_ID = VW.GROUP_ID " +
                "WHERE  P.PERMISSION_TYPE_CODE = 'FEATURE' " + 
                "FOR XML PATH('') " +
            ") AS PC(PERMISSION_CODES) " +
            "WHERE " + PERMISSIONS_CODES_FILTER_PLACEHOLDER;
    
    private static final String ORDER_GROUP_ID_ASC_CLAUSE = "GROUP_ID";

    @Override
    public Map<String, List<String>> getPermissionsForRolesInList(List<String> permissionList, 
            List<String> roleNameList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ROLE_NAME_LIST_PLACEHOLDER, roleNameList);
        params.addValue(PERMISSION_LIST_PLACEHOLDER, permissionList);
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(ROLES_PERMISSION_QUERY, params, 
                new ColumnMapRowMapper());
        Map<String, List<String>> roleToPermissionListMap = new HashMap<>();
            
        for (Map<String, Object> node : nodes) {
            String permission = (String) node.get(PERMISSION_NAME);
            String role = ((String) node.get(ROLE_CODE)).toUpperCase();
            
            List<String> permissionsGrantedList = roleToPermissionListMap.get(role);
            if (permissionsGrantedList == null) {
                permissionsGrantedList = new ArrayList<String>();
            }
            permissionsGrantedList.add(permission);
            roleToPermissionListMap.put(role, permissionsGrantedList);
        }
            
        return roleToPermissionListMap;
    }

    @Override
    public Map<Long, List<String>> getPermissionsForGroupsInList(List<String> permissionList, List<Long> groupIdList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_LIST_PLACEHOLDER, groupIdList);
        params.addValue(PERMISSION_LIST_PLACEHOLDER, permissionList);

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(GROUPS_PERMISSION_QUERY, params, 
                new ColumnMapRowMapper());
        Map<Long, List<String>> roleToPermissionListMap = new HashMap<>();

        for (Map<String, Object> node : nodes) {
            String permission = (String) node.get(PERMISSION_NAME);
            Long groupId = (Long) node.get(GROUP_ID);

            List<String> permissionsGrantedList = roleToPermissionListMap.get(groupId);
            if (permissionsGrantedList == null) {
                permissionsGrantedList = new ArrayList<String>();
            }
            permissionsGrantedList.add(permission);
            roleToPermissionListMap.put(groupId, permissionsGrantedList);
        }

        return roleToPermissionListMap;
    }

    @Override
    public void createPermissionForTypeIfNotExists(String roleName, String subjectTable, Long subjectId) {
        // Role based permissions are handled via ROLE_PERMISSION instead
        if (PermissionConstants.ROLE_ACL_TYPE.equalsIgnoreCase(subjectTable)) {
            createRolePermissionIfNotExists(roleName, subjectId);
        } else {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue(ROLE_NAME_PLACEHOLDER, roleName);
            params.addValue(SUBJECT_TABLE_PLACEHOLDER, subjectTable);
            params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);
            
            namedParameterJdbcTemplate.update(INSERT_ACL_IF_NOT_EXISTS, params);
        }
    }

    @Override
    public void deletePermissionForTypeIfExists(String roleName, String subjectTable, Long subjectId) {
        // Role based permissions are handled via ROLE_PERMISSION instead
        if (PermissionConstants.ROLE_ACL_TYPE.equalsIgnoreCase(subjectTable)) {
            deleteRolePermissionIfNotExists(roleName, subjectId);
        } else {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue(ROLE_NAME_PLACEHOLDER, roleName);
            params.addValue(SUBJECT_TABLE_PLACEHOLDER, subjectTable);
            params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);
            
            namedParameterJdbcTemplate.update(DELETE_ACL_IF_EXISTS, params);
        }        
    }
    
    private void createRolePermissionIfNotExists(String roleName, Long subjectId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ROLE_NAME_PLACEHOLDER, roleName);
        params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);

        namedParameterJdbcTemplate.update(INSERT_ROLE_PERMISSION_IF_NOT_EXISTS, params);
    }
    
    private void deleteRolePermissionIfNotExists(String roleName, Long subjectId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ROLE_NAME_PLACEHOLDER, roleName);
        params.addValue(SUBJECT_ID_PLACEHOLDER, subjectId);

        namedParameterJdbcTemplate.update(DELETE_ROLE_PERMISSION_IF_EXISTS, params);
    }

    /**
     * Retrieve group list by audience permissions.
     * @param permissionList - Audience permissions list.
     * @param filter - Possible values: AND or OR 
     * @return query result using CamelCaseRowMapper.
     */
    @Override
    public List<Map<String, Object>> getGroupsByPermissions(List<String> permissionList, String filter
            , Integer pageNumber, Integer pageSize) {
        
        String query = GET_GROUPS_BY_PERMISSION; 
        query = query.replace(PERMISSIONS_CODES_FILTER_PLACEHOLDER, createPermissionCodeFilter(permissionList, filter));
        
        return getPageWithTotalRowsCTE(query.toString(), null, new CamelCaseMapRowMapper()
                , ORDER_GROUP_ID_ASC_CLAUSE, pageNumber, pageSize);
    }
    
    /**
     * Utility method to create the WHERE clause according to the 'filter' param (AND or OR)  
     * @param permissionList
     * @param filter
     * @return
     */
    private String createPermissionCodeFilter(List<String> permissionList, String filter) {
        StringBuffer filterQuery = new StringBuffer();
        
        for (String permission: permissionList) {
            filterQuery.append(" PERMISSION_CODES LIKE '%").append(permission).append("%' ").append(filter);
        }
        filterQuery.delete(filterQuery.length() - filter.length(), filterQuery.length());

        return filterQuery.toString(); 
    }
}
