package com.maritz.culturenext.permission.dao;

import java.util.List;

import com.maritz.culturenext.permission.dto.ProxyDTO;

public interface ProxyDao {
    
    List<ProxyDTO> getProxiesForPax(List<String> proxyPermissionList, Long paxId, List<String> proxyTypeList, 
            List<String> statusList);
    ProxyDTO getProxyForSpecificPax(List<String> proxyPermissionList, Long paxId, Long proxyPaxId, String proxyType);
    
    Boolean doesAclEntryExist(List<String> proxyPermissionList, Long targetId, Long subjectId);
    
    void createProxyPermissionForPaxIfNotExists(String permissionName, Long targetId, Long subjectId);
    void deleteProxyPermissionForPaxIfExists(String permissionName, Long targetId, Long subjectId);

}
