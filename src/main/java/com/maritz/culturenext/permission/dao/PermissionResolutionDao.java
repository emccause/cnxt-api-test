package com.maritz.culturenext.permission.dao;

import java.util.List;
import java.util.Map;

public interface PermissionResolutionDao {
    
    Map<String, List<String>> getPermissionsForRolesInList(List<String> permissionList, List<String> roleNameList);
    Map<Long, List<String>> getPermissionsForGroupsInList(List<String> permissionList, List<Long> groupIdList);
    List<Map<String, Object>> getGroupsByPermissions(List<String> permissionList, String filter
            , Integer pageNumber, Integer pageSize); 
    
    void createPermissionForTypeIfNotExists(String roleName, String subjectTable, Long subjectId);
    void deletePermissionForTypeIfExists(String roleName, String subjectTable, Long subjectId);

}
