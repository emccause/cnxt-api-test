package com.maritz.culturenext.permission.dto;

import java.util.List;

public class PermissionWrapperDTO {
    
    private String role;
    private Long groupId;
    List<PermissionDTO> permissions;
    
    public String getRole() {
        return role;
    }
    
    public PermissionWrapperDTO setRole(String role) {
        this.role = role;
        return this;
    }
    
    public Long getGroupId() {
        return groupId;
    }
    
    public PermissionWrapperDTO setGroupId(Long groupId) {
        this.groupId = groupId;
        return this;
    }
    
    public List<PermissionDTO> getPermissions() {
        return permissions;
    }
    
    public PermissionWrapperDTO setPermissions(List<PermissionDTO> permissions) {
        this.permissions = permissions;
        return this;
    }

}
