package com.maritz.culturenext.permission.dto;

public class PermissionDTO {
    
    String permissionCode;
    Boolean enabled;
    
    public String getPermissionCode() {
        return permissionCode;
    }
    
    public PermissionDTO setPermissionCode(String permissionCode) {
        this.permissionCode = permissionCode;
        return this;
    }
    
    public Boolean getEnabled() {
        return enabled;
    }
    
    public PermissionDTO setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

}
