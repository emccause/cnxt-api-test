package com.maritz.culturenext.permission.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class ProxyDTO {
    
    EmployeeDTO proxyPax;
    String type;
    List<PermissionDTO> permissions;
    
    public EmployeeDTO getProxyPax() {
        return proxyPax;
    }
    
    public ProxyDTO setProxyPax(EmployeeDTO proxyPax) {
        this.proxyPax = proxyPax;
        return this;
    }
    
    public String getType() {
        return type;
    }
    
    public ProxyDTO setType(String type) {
        this.type = type;
        return this;
    }
    
    public List<PermissionDTO> getPermissions() {
        return permissions;
    }
    
    public ProxyDTO setPermissions(List<PermissionDTO> permissions) {
        this.permissions = permissions;
        return this;
    }

}
