package com.maritz.culturenext.permission.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.GroupDTO;

public class GroupPermissionsDTO {
    private GroupDTO group;
    private List<PermissionDTO> permissions;
    
    public GroupDTO getGroup() {
        return group;
    }
    public void setGroup(GroupDTO group) {
        this.group = group;
    }
    public List<PermissionDTO> getPermissions() {
        return permissions;
    }
    public void setPermissions(List<PermissionDTO> permissions) {
        this.permissions = permissions;
    }
}
