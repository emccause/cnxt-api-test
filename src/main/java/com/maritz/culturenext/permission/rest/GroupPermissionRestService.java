package com.maritz.culturenext.permission.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dto.GroupPermissionsDTO;
import com.maritz.culturenext.permission.services.PermissionManagementService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("users/groups")
@Api(value="/groups", description="All end points dealing with group permissions")
public class GroupPermissionRestService {
    
    @Inject private PermissionManagementService permissionService;

    //rest/users/groups/permissions
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "permissions", method = RequestMethod.GET)
    @ApiOperation(value = "Search for all groups associated with the permissions passed in given the filter modifier which can be OR or AND.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved groups - permission list", 
                    response = GroupPermissionsDTO.class)
    })
    @Permission("PUBLIC")
    public PaginatedResponseObject<GroupPermissionsDTO> getGroupsByPermissions(
            @ApiParam(value="comma separated list of permissions you're fetching groups for", required = true)
                @RequestParam(value = PERMISSION_REST_PARAM, required = true) String commaSeparatedPermissionList,
            @ApiParam(value="Logical operator (OR, AND) used to query the groups for each permissions", required = true)
                @RequestParam(value = FILTER_REST_PARAM, required = true) String filter,
            @ApiParam(value = "Page number to request") 
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer page,
            @ApiParam(value = "    Size of pagination response to request") 
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer size
            ) throws Throwable {
        return permissionService.getGroupsByPermissions(commaSeparatedPermissionList, filter, page, size);
    }
}