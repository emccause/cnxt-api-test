package com.maritz.culturenext.permission.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.permission.dto.ProxyDTO;
import com.maritz.culturenext.permission.services.ProxyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("participants")
@Api(value="/permissions", description="All end points dealing with proxy permissions")
public class ProxyRestService {
    
    @Inject private ProxyService proxyService;
    @Inject private Security security;

    //rest/participants/~/proxies
    @PreAuthorize("(@security.hasRole('" + ProjectConstants.ADMIN_ROLE + "', '" + ProjectConstants.CLIENT_ADMIN_ROLE + 
            "') " + "or @security.isMyPax(#paxIdString)) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies", method = RequestMethod.GET)
    @ApiOperation(value = "Gets list of proxies for user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved permissions", response = ProxyDTO.class)
    })
    @Permission("PUBLIC")
    public List<ProxyDTO> getProxyList(
            @ApiParam(value="pax id of the user to manage proxies for", required = true)
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="comma separated list of proxy types to return", required = false) 
                @RequestParam(value = TYPE_REST_PARAM, required = false) String commaSeparatedProxyTypeList,
            @ApiParam(value="comma separated list of statuses to filter", required = false) 
                @RequestParam(value = STATUS_REST_PARAM, required = false) String commaSeparatedStatusList
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return proxyService.getProxiesForPax(paxId, commaSeparatedProxyTypeList, 
                commaSeparatedStatusList);
    }

    //rest/participants/~/proxies/permissions
    @PreAuthorize("@security.isMyPax(#paxIdString) and @security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies/permissions", method = RequestMethod.GET)
    @ApiOperation(value = "Get the permissions/functions allowed while impersonated")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved permissions for pax", response = ProxyDTO.class)
    })
    @Permission("PUBLIC")
    public ProxyDTO getProxyPermissions(
            @ApiParam(value="pax id of the logged in user", required = true) @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return proxyService.getProxyPermissions(paxId);
    }

    //rest/participants/~/proxies
    @PreAuthorize("(@security.hasPermission('ASSIGN_PROXY_OTHERS') or (@security.isMyPax(#paxIdString) and "
            + "@security.hasPermission('ASSIGN_PROXY_SELF'))) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies", method = RequestMethod.POST)
    @ApiOperation(value = "Creates a proxy for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created proxy", response = ProxyDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProxyDTO createProxyForPax(
            @ApiParam(value="pax id of the user to manage proxies for", required = true) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @RequestBody ProxyDTO proxyDto
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return proxyService.updateProxyForPax(paxId, null, RequestMethod.POST.toString(), proxyDto);
    }

    //rest/participants/~/proxies/~
    @PreAuthorize("(@security.hasPermission('ASSIGN_PROXY_OTHERS') or "
            + "(@security.isMyPax(#paxIdString) and @security.hasPermission('ASSIGN_PROXY_SELF'))) and "
            + "!@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies/{proxyPaxId}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates a proxy for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated proxy", response = ProxyDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProxyDTO updateProxyForPax(
            @ApiParam(value="pax id of the user to manage proxies for", required = true) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="pax id of the proxy being managed", required = true) 
                @PathVariable(PROXY_PAX_ID_REST_PARAM) String proxyPaxIdString,
            @RequestBody ProxyDTO proxyDto
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long proxyPaxId = security.getId(proxyPaxIdString, null);
        if (proxyPaxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return proxyService.updateProxyForPax(paxId, proxyPaxId, RequestMethod.PUT.toString(), proxyDto);
    }

    //rest/participants/~/proxies/~
    @PreAuthorize("(@security.hasPermission('ASSIGN_PROXY_OTHERS') or (@security.isMyPax(#paxIdString) and "
            + "@security.hasPermission('ASSIGN_PROXY_SELF'))) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies/{proxyPaxId}", method = RequestMethod.PATCH)
    @ApiOperation(value = "Updates a proxy for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated proxy", response = ProxyDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ProxyDTO patchProxyForPax(
            @ApiParam(value="pax id of the user to manage proxies for", required = true) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="pax id of the proxy being managed", required = true) 
                @PathVariable(PROXY_PAX_ID_REST_PARAM) String proxyPaxIdString,
            @RequestBody ProxyDTO proxyDto
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long proxyPaxId = security.getId(proxyPaxIdString, null);
        if (proxyPaxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return proxyService.updateProxyForPax(paxId, proxyPaxId, RequestMethod.PATCH.toString(), proxyDto);
    }
    
    //rest/participants/~/proxies/~
    @PreAuthorize("(@security.hasPermission('ASSIGN_PROXY_OTHERS') or (@security.isMyPax(#paxIdString) and "
            + "@security.hasPermission('ASSIGN_PROXY_SELF'))) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/proxies/{proxyPaxId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "deletes a proxy for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated proxy", response = ProxyDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public void deleteProxyForPax(
            @ApiParam(value="pax id of the user to manage proxies for", required = true) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="pax id of the proxy being managed", required = true) 
                @PathVariable(PROXY_PAX_ID_REST_PARAM) String proxyPaxIdString
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long proxyPaxId = security.getId(proxyPaxIdString, null);
        if (proxyPaxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        proxyService.deleteProxyForPax(paxId, proxyPaxId);
    }

}
