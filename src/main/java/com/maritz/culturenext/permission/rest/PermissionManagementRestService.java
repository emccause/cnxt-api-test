package com.maritz.culturenext.permission.rest;

import java.util.List;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dto.PermissionWrapperDTO;
import com.maritz.culturenext.permission.services.PermissionManagementService;

@RestController
@RequestMapping("users/permissions")
@Api(value="/permissions", description="All end points dealing with user permission management")
public class PermissionManagementRestService {

    @Inject private PermissionManagementService permissionService;

    //rest/users/permissions/role
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "role", method = RequestMethod.GET)
    @ApiOperation(value = "Gets permissions for roles provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved permissions", 
                    response = PermissionWrapperDTO.class)
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> getPermissionsForRoles(
            @ApiParam(value="comma separated list of roles you're fetching permissions for", required = true)
            @RequestParam(value = ROLE_NAME_REST_PARAM, required = true) String commaSeparatedRoleList
            ) throws Throwable {
        return permissionService.getPermissionsForRoles(commaSeparatedRoleList);
    }

    //rest/users/permissions/role
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "role", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates all permissions for role provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated permissions", 
                    response = PermissionWrapperDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> updateAllPermissionsForRole(
            @RequestBody PermissionWrapperDTO permissionWrapper
            ) throws Throwable {
        return permissionService.updatePermissionsForRole(RequestMethod.PUT.toString(), permissionWrapper);
    }

    //rest/users/permissions/role
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "role", method = RequestMethod.PATCH)
    @ApiOperation(value = "Updates all permissions for role provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated permissions", 
                    response = PermissionWrapperDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> updatePermissionsForRole(
            @RequestBody PermissionWrapperDTO permissionWrapper
            ) throws Throwable {
        return permissionService.updatePermissionsForRole(RequestMethod.PATCH.toString(), permissionWrapper);
    }

    //rest/users/permissions/group
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "group", method = RequestMethod.GET)
    @ApiOperation(value = "Gets permissions for groups provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved permissions",
                    response = PermissionWrapperDTO.class)
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> getPermissionsForGroups(
            @ApiParam(value="comma separated list of groups you're fetching permissions for", required = true) 
                @RequestParam(value = GROUP_ID_REST_PARAM, required = true) String commaSeparatedGroupIdList
            ) throws Throwable {
        return permissionService.getPermissionsForGroups(commaSeparatedGroupIdList);
    }

    //rest/users/permissions/group
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "group", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates all permissions for role provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated permissions", 
                    response = PermissionWrapperDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> updateAllPermissionsForGroup(
            @RequestBody PermissionWrapperDTO permissionWrapper
            ) throws Throwable {
        return permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);
    }

    //rest/users/permissions/group
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(value = "group", method = RequestMethod.PATCH)
    @ApiOperation(value = "Updates all permissions for group provided")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated permissions", 
                    response = PermissionWrapperDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<PermissionWrapperDTO> updatePermissionsForGroup(
            @RequestBody PermissionWrapperDTO permissionWrapper
            ) throws Throwable {
        return permissionService.updatePermissionsForGroup(RequestMethod.PATCH.toString(), permissionWrapper);
    }

}
