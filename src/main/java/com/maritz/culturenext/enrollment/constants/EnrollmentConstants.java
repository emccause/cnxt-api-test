package com.maritz.culturenext.enrollment.constants;

public class EnrollmentConstants {
    
    public static final String INVALID_FILE_FORMAT = "INVALID_FILE_FORMAT";
    public static final String INVALID_FILE_FORMAT_MSG = "The uploaded file is not a CSV.";
    public static final String MISSING_CSV_FILE = "MISSING_CSV_FILE";
    public static final String MISSING_CSV_FILE_MSG = "At least one CSV file is required for this request.";
    public static final String SERVER_IO_ISSUES = "SERVER_IO_ISSUES";
    public static final String SERVER_IO_ISSUES_MSG = "There were issues when writting to the server.";
    public static final String ENROLLMENT_SUBFOLDER = "enrollment/";
    public static final String ENROLLMENT_SUBFOLDER_NAME = "enrollment";
    public static final String MISSING_ENROLLMENT_FOLDER = "The enrollment folder is missing.";
    public static final String MISSING_ENROLLMENT_FOLDER_MSG = "MISSING_ENROLLMENT_FOLDER";

}
