package com.maritz.culturenext.enrollment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.ZonedDateTime;

@ApiModel("Participant Enrollment Error")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ParticipantEnrollmentErrorDTO {
    private String firstName;
    private String lastName;
    private String controlNumber;
    private ZonedDateTime errorDatetime;
    private String error;

}
