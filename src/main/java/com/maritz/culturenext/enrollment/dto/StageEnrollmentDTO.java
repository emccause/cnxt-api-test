package com.maritz.culturenext.enrollment.dto;

import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.core.convert.converter.Converter;

import com.maritz.core.util.CollectionBuilder;
import com.maritz.core.util.ObjectUtils;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.jpa.entity.StageEnrollment;
import com.maritz.workday.dto.EmployeeDTO;

public class StageEnrollmentDTO {

    // Field length
    protected static final int MAX_FIRST_NAME_LENGTH = 100;
    protected static final int MAX_LAST_NAME_LENGTH = 100;
    protected static final int MAX_MIDDLE_NAME_LENGTH = 100;
    protected static final int MAX_EMAIL_ADDRESS_LENGTH = 140;
    protected static final int MAX_PHONE_NUMBER_LENGTH = 15;
    protected static final int MAX_PHONE_EXTENSION_LENGTH = 15;
    protected static final int MAX_ADDRESS_N_LENGTH = 100;
    protected static final int MAX_CITY_LENGTH = 100;
    protected static final int MAX_STATE_LENGTH = 100;
    protected static final int MAX_ZIP_LENGTH = 50;
    protected static final int MAX_LANGUAGE_CODE_LENGTH = 6;

    private Long id;
    private Long batchId;
    private String participantId;
    private String status;
    private String primaryReportId;
    private Long primaryReportPaxId;
    private String secondaryReportId;
    private Long secondaryReportPaxId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String title;
    private String companyName;
    private String loginId;
    private String password;
    private String emailAddress;
    private String phoneNumber;
    private String phoneExtension;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String addressLine4;
    private String addressLine5;
    private String city;
    private String stateProvinceRegion;
    private String zipCode;
    private String countryCode;
    private String hireDate;
    private String terminationDate;
    private String birthDate;
    private String fullBirthDate;
    private String department;
    private String costCenter;
    private String area;
    private String grade;
    private String custom1;
    private String custom2;
    private String custom3;
    private String custom4;
    private String custom5;
    private Long paxId;
    private Long paxGroupId;
    private String addressChangeStatus;
    private String emailChangeStatus;
    private String phoneChangeStatus;
    private String enrollError;
    private String sysUserChangeStatus;
    private String statusTypeCode;
    private String paxChangeStatus;
    private String paxGroupChangeStatus;
    private String userFunction;
    private String languagePreference;

    public StageEnrollmentDTO() {
    }

    public StageEnrollmentDTO(EmployeeDTO employee) {
        if (employee != null) {
            ObjectUtils.objectToObject(employee, this);

            setFirstName(StringUtils.trim(getFirstName(), MAX_FIRST_NAME_LENGTH));
            setMiddleName(StringUtils.trim(getMiddleName(), MAX_MIDDLE_NAME_LENGTH));
            setLastName(StringUtils.trim(getLastName(), MAX_LAST_NAME_LENGTH));
            setEmailAddress(StringUtils.trim(getEmailAddress(), MAX_EMAIL_ADDRESS_LENGTH));
            setPhoneNumber(StringUtils.trim(StringUtils.stripNonDigits(getPhoneNumber()), MAX_PHONE_NUMBER_LENGTH));
            setPhoneExtension(StringUtils.trim(StringUtils.stripNonDigits(getPhoneExtension()), MAX_PHONE_EXTENSION_LENGTH));
            setAddressLine1(StringUtils.trim(getAddressLine1(), MAX_ADDRESS_N_LENGTH));
            setAddressLine2(StringUtils.trim(getAddressLine2(), MAX_ADDRESS_N_LENGTH));
            setAddressLine3(StringUtils.trim(getAddressLine3(), MAX_ADDRESS_N_LENGTH));
            setAddressLine4(StringUtils.trim(getAddressLine4(), MAX_ADDRESS_N_LENGTH));
            setAddressLine5(StringUtils.trim(getAddressLine5(), MAX_ADDRESS_N_LENGTH));
            setCity(StringUtils.trim(getCity(), MAX_CITY_LENGTH));
            setLanguagePreference(StringUtils.trim(getLanguagePreference(), MAX_LANGUAGE_CODE_LENGTH));

            String zip = StringUtils.trim(getZipCode(), MAX_ZIP_LENGTH);
            if (zip != null) zip = zip.replace("-", "");
            setZipCode(zip);
        }
    }

    public StageEnrollment toStageEnrollment() {
        return toStageEnrollment(null);
    }

    public StageEnrollment toStageEnrollment(StageEnrollment stageEnrollment) {
        if (stageEnrollment == null) stageEnrollment = new StageEnrollment();
        return ObjectUtils.objectToObject(this, stageEnrollment);
    }

    public Long getId() {
        return id;
    }

    public StageEnrollmentDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }

    public StageEnrollmentDTO setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    public String getParticipantId() {
        return participantId;
    }

    public StageEnrollmentDTO setParticipantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public StageEnrollmentDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getPrimaryReportId() {
        return primaryReportId;
    }

    public StageEnrollmentDTO setPrimaryReportId(String primaryReportId) {
        this.primaryReportId = primaryReportId;
        return this;
    }

    public Long getPrimaryReportPaxId() {
        return primaryReportPaxId;
    }

    public StageEnrollmentDTO setPrimaryReportPaxId(Long primaryReportPaxId) {
        this.primaryReportPaxId = primaryReportPaxId;
        return this;
    }

    public String getSecondaryReportId() {
        return secondaryReportId;
    }

    public StageEnrollmentDTO setSecondaryReportId(String secondaryReportId) {
        this.secondaryReportId = secondaryReportId;
        return this;
    }

    public Long getSecondaryReportPaxId() {
        return secondaryReportPaxId;
    }

    public StageEnrollmentDTO setSecondaryReportPaxId(Long secondaryReportPaxId) {
        this.secondaryReportPaxId = secondaryReportPaxId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public StageEnrollmentDTO setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public StageEnrollmentDTO setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public StageEnrollmentDTO setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public StageEnrollmentDTO setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public StageEnrollmentDTO setCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getLoginId() {
        return loginId;
    }

    public StageEnrollmentDTO setLoginId(String loginId) {
        this.loginId = loginId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public StageEnrollmentDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public StageEnrollmentDTO setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public StageEnrollmentDTO setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public String getPhoneExtension() {
        return phoneExtension;
    }

    public StageEnrollmentDTO setPhoneExtension(String phoneExtension) {
        this.phoneExtension = phoneExtension;
        return this;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public StageEnrollmentDTO setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public StageEnrollmentDTO setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
        return this;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public StageEnrollmentDTO setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
        return this;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public StageEnrollmentDTO setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
        return this;
    }

    public String getAddressLine5() {
        return addressLine5;
    }

    public StageEnrollmentDTO setAddressLine5(String addressLine5) {
        this.addressLine5 = addressLine5;
        return this;
    }

    public String getCity() {
        return city;
    }

    public StageEnrollmentDTO setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStateProvinceRegion() {
        return stateProvinceRegion;
    }

    public StageEnrollmentDTO setStateProvinceRegion(String stateProvinceRegion) {
        this.stateProvinceRegion = stateProvinceRegion;
        return this;
    }

    public String getZipCode() {
        return zipCode;
    }

    public StageEnrollmentDTO setZipCode(String zipCode) {
        this.zipCode = zipCode;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public StageEnrollmentDTO setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getHireDate() {
        return hireDate;
    }

    public StageEnrollmentDTO setHireDate(String hireDate) {
        this.hireDate = hireDate;
        return this;
    }

    public String getTerminationDate() {
        return terminationDate;
    }

    public StageEnrollmentDTO setTerminationDate(String terminationDate) {
        this.terminationDate = terminationDate;
        return this;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public StageEnrollmentDTO setBirthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public String getFullBirthDate() {
        return fullBirthDate;
    }

    public StageEnrollmentDTO setFullBirthDate(String fullBirthDate) {
        this.fullBirthDate = fullBirthDate;
        return this;
    }

    public String getDepartment() {
        return department;
    }

    public StageEnrollmentDTO setDepartment(String department) {
        this.department = department;
        return this;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public StageEnrollmentDTO setCostCenter(String costCenter) {
        this.costCenter = costCenter;
        return this;
    }

    public String getArea() {
        return area;
    }

    public StageEnrollmentDTO setArea(String area) {
        this.area = area;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public StageEnrollmentDTO setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getCustom1() {
        return custom1;
    }

    public StageEnrollmentDTO setCustom1(String custom1) {
        this.custom1 = custom1;
        return this;
    }

    public String getCustom2() {
        return custom2;
    }

    public StageEnrollmentDTO setCustom2(String custom2) {
        this.custom2 = custom2;
        return this;
    }

    public String getCustom3() {
        return custom3;
    }

    public StageEnrollmentDTO setCustom3(String custom3) {
        this.custom3 = custom3;
        return this;
    }

    public String getCustom4() {
        return custom4;
    }

    public StageEnrollmentDTO setCustom4(String custom4) {
        this.custom4 = custom4;
        return this;
    }

    public String getCustom5() {
        return custom5;
    }

    public StageEnrollmentDTO setCustom5(String custom5) {
        this.custom5 = custom5;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }

    public StageEnrollmentDTO setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public Long getPaxGroupId() {
        return paxGroupId;
    }

    public StageEnrollmentDTO setPaxGroupId(Long paxGroupId) {
        this.paxGroupId = paxGroupId;
        return this;
    }

    public String getAddressChangeStatus() {
        return addressChangeStatus;
    }

    public StageEnrollmentDTO setAddressChangeStatus(String addressChangeStatus) {
        this.addressChangeStatus = addressChangeStatus;
        return this;
    }

    public String getEmailChangeStatus() {
        return emailChangeStatus;
    }

    public StageEnrollmentDTO setEmailChangeStatus(String emailChangeStatus) {
        this.emailChangeStatus = emailChangeStatus;
        return this;
    }

    public String getPhoneChangeStatus() {
        return phoneChangeStatus;
    }

    public StageEnrollmentDTO setPhoneChangeStatus(String phoneChangeStatus) {
        this.phoneChangeStatus = phoneChangeStatus;
        return this;
    }

    public String getEnrollError() {
        return enrollError;
    }

    public StageEnrollmentDTO setEnrollError(String enrollError) {
        this.enrollError = enrollError;
        return this;
    }

    public String getSysUserChangeStatus() {
        return sysUserChangeStatus;
    }

    public StageEnrollmentDTO setSysUserChangeStatus(String sysUserChangeStatus) {
        this.sysUserChangeStatus = sysUserChangeStatus;
        return this;
    }

    public String getStatusTypeCode() {
        return statusTypeCode;
    }

    public StageEnrollmentDTO setStatusTypeCode(String statusTypeCode) {
        this.statusTypeCode = statusTypeCode;
        return this;
    }

    public String getPaxChangeStatus() {
        return paxChangeStatus;
    }

    public StageEnrollmentDTO setPaxChangeStatus(String paxChangeStatus) {
        this.paxChangeStatus = paxChangeStatus;
        return this;
    }

    public String getPaxGroupChangeStatus() {
        return paxGroupChangeStatus;
    }

    public StageEnrollmentDTO setPaxGroupChangeStatus(String paxGroupChangeStatus) {
        this.paxGroupChangeStatus = paxGroupChangeStatus;
        return this;
    }

    public String getUserFunction() {
        return userFunction;
    }

    public StageEnrollmentDTO setUserFunction(String userFunction) {
        this.userFunction = userFunction;
        return this;
    }

    public String getLanguagePreference() {
        return languagePreference;
    }

    public StageEnrollmentDTO setLanguagePreference(String languagePreference) {
        this.languagePreference = languagePreference;
        return this;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public static List<StageEnrollmentDTO> fromEmployeeDTO(Iterable<EmployeeDTO> employees) {
        return CollectionBuilder.<StageEnrollmentDTO>newArrayList()
            .map(employees, fromEmployeeDTOMapper())
            .buildList()
        ;
    }

    public static Converter<EmployeeDTO, StageEnrollmentDTO> fromEmployeeDTOMapper() {
        return new Converter<EmployeeDTO, StageEnrollmentDTO>() {
            @Override
            public StageEnrollmentDTO convert(EmployeeDTO source) {
                return new StageEnrollmentDTO(source);
            }
        };
    }
}
