package com.maritz.culturenext.enrollment.batch;

import javax.inject.Inject;

import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;

import com.maritz.batch.launcher.BatchJobLauncher;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.services.FileService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.core.util.FileDTOPredicates;
import com.maritz.culturenext.enrollment.services.CardEnableDisableService;

public class EnrollmentFileJobLauncher {
    final Logger logger = LoggerFactory.getLogger(getClass());

    protected BatchJobLauncher batchJobLauncher;
    protected CardEnableDisableService cardEnableDisableService;
    protected FileService fileService;
    protected Job enrollmentFileJob;
    protected TriggeredTaskService triggeredTaskService;

    @Inject
    public EnrollmentFileJobLauncher setBatchJobLauncher(BatchJobLauncher batchJobLauncher) {
        this.batchJobLauncher = batchJobLauncher;
        return this;
    }

    @Inject
    public EnrollmentFileJobLauncher setCardEnableDisableService(CardEnableDisableService cardEnableDisableService) {
        this.cardEnableDisableService = cardEnableDisableService;
        return this;
    }

    public EnrollmentFileJobLauncher setFileService(FileService fileService) {
        this.fileService = fileService;
        return this;
    }

    @Inject
    public EnrollmentFileJobLauncher setEnrollmentFileJob(Job enrollmentFileJob) {
        this.enrollmentFileJob = enrollmentFileJob;
        return this;
    }

    @Inject
    public EnrollmentFileJobLauncher setTriggeredTaskService(TriggeredTaskService triggeredTaskService) {
        this.triggeredTaskService = triggeredTaskService;
        return this;
    }

    @TriggeredTask(batch=true)
    public void enrollmentFileJob(Batch batch) throws Exception {
        logger.info("Started - enrollmentFileJob - batchId: {}", null == batch ? "BatchId is null" : batch.getBatchId());
        batchJobLauncher.runJob(batch, enrollmentFileJob, fileService, "", FileDTOPredicates.fileNameStartsWith("enrollmentLoad"));
        batchJobLauncher.runJob(batch, enrollmentFileJob, fileService, EnrollmentConstants.ENROLLMENT_SUBFOLDER,x-> true); // process ALL files contained on inbox/enrollment/ folder
        logger.info("Finished - enrollmentFileJob");
        
        //Create RIDEAU accounts
        triggeredTaskService.runTriggeredTaskAsync("enableDisableRideauCardStatus");

        //Update group membership for enrollment groups
        triggeredTaskService.runTriggeredTaskAsync("processGroupMembership");
    }

}