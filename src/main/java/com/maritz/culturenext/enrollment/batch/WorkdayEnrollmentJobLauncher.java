package com.maritz.culturenext.enrollment.batch;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;

import com.maritz.batch.launcher.BatchJobLauncher;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.culturenext.enrollment.services.CardEnableDisableService;


public class WorkdayEnrollmentJobLauncher {
    final Logger logger = LoggerFactory.getLogger(getClass());

    protected BatchJobLauncher batchJobLauncher;
    protected CardEnableDisableService cardEnableDisableService;
    protected Job workdayEnrollmentJob;
    protected TriggeredTaskService triggeredTaskService;

    @Inject
    public WorkdayEnrollmentJobLauncher setBatchJobLauncher(BatchJobLauncher batchJobLauncher) {
        this.batchJobLauncher = batchJobLauncher;
        return this;
    }

    @Inject
    public WorkdayEnrollmentJobLauncher setCardEnableDisableService(CardEnableDisableService cardEnableDisableService) {
        this.cardEnableDisableService = cardEnableDisableService;
        return this;
    }
    
    @Inject
    public WorkdayEnrollmentJobLauncher setWorkdayEnrollmentJob(Job workdayEnrollmentJob) {
        this.workdayEnrollmentJob = workdayEnrollmentJob;
        return this;
    }
    
    @Inject
    public WorkdayEnrollmentJobLauncher setTriggeredTaskService(TriggeredTaskService triggeredTaskService) {
        this.triggeredTaskService = triggeredTaskService;
        return this;
    }

    @TriggeredTask(batch=true)
    public void workdayEnrollmentJob(Batch batch) throws Exception {
        logger.info("Started - Workday Enrollment Job - batchId: {}, ",  null == batch ? "BatchId is null" : batch.getBatchId());
        batchJobLauncher.runJob(batch, workdayEnrollmentJob);
        logger.info("Finished - Workday Enrollment Job");

        //Create RIDEAU accounts
        triggeredTaskService.runTriggeredTaskAsync("enableDisableRideauCardStatus");

        //Update group membership for enrollment groups
        triggeredTaskService.runTriggeredTaskAsync("processGroupMembership");
    }

}