package com.maritz.culturenext.enrollment.batch;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.maritz.core.dto.FileDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.services.FileService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.core.util.FileDTOPredicates;

@Service
public class EnrollmentFilePollingJob {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private FileService inboxFileService;
    @Inject private TriggeredTaskService triggeredTaskService;

    @TriggeredTask
    public void enrollmentFilePollingJob(Batch batch) throws Exception {
        logger.info("Enrollment File Polling Job - triggeredTask - batchId {}", null == batch ? "BatchId is null" : batch.getBatchId());

        List<FileDTO> inboxEnrollmentFiles = inboxFileService.getFiles(EnrollmentConstants.ENROLLMENT_SUBFOLDER).stream()
            .filter(f -> !f.getIsDirectory())
                .collect(Collectors.toList());

        inboxEnrollmentFiles.addAll(inboxFileService.getFiles("").stream()
                .filter(f -> !f.getIsDirectory()).filter(FileDTOPredicates.fileNameStartsWith("enrollmentLoad"))
                .collect(Collectors.toList()));

        inboxEnrollmentFiles.sort(Comparator.comparing(file -> file.getLastModified().getTime())) ;
        inboxEnrollmentFiles.forEach(s-> logger.info("enrollment file {}",s.getName()));

        if (!ObjectUtils.isEmpty(inboxEnrollmentFiles)) {
            triggeredTaskService.runTriggeredTaskAsync("enrollmentFileJob");
        }
    }
}