package com.maritz.culturenext.enrollment.batch.writer;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.support.AbstractItemStreamItemWriter;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.culturenext.enrollment.dao.ProcessEnrollmentDao;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.culturenext.jpa.entity.StageEnrollment;


public class StageEnrollmentWriter extends AbstractItemStreamItemWriter<StageEnrollmentDTO> implements StepExecutionListener {

    protected ConcentrixDao<StageEnrollment> stageEnrollmentDao;
    protected ProcessEnrollmentDao processEnrollmentDao;
    protected Long batchId;

    @Inject
    public StageEnrollmentWriter setStageEnrollmentDao(ConcentrixDao<StageEnrollment> stageEnrollmentDao) {
        this.stageEnrollmentDao = stageEnrollmentDao;
        return this;
    }

    @Inject
    public StageEnrollmentWriter setProcessEnrollmentDao(ProcessEnrollmentDao processEnrollmentDao) {
        this.processEnrollmentDao = processEnrollmentDao;
        return this;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
        //NOTE: this returns 0L when the value is null or not present
        batchId = stepExecution.getJobExecution().getJobParameters().getLong("batchId");
        if (batchId == 0L) batchId = null;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        ExitStatus exitStatus = stepExecution.getExitStatus();
        if (exitStatus.compareTo(ExitStatus.COMPLETED) == 0) {
            processEnrollmentDao.processEnrollmentRecords(batchId);
        }
        return null;
    }

    @Override
    public void write(List<? extends StageEnrollmentDTO> items) throws Exception {
        for (StageEnrollmentDTO item : items) {
            if (item.getStatus().equals(StatusTypeCode.ACTIVE.name())) {
                item.setStatus("A");
            } else if (item.getStatus().equals(StatusTypeCode.INACTIVE.name())) {
                item.setStatus("I");
            } else if (item.getStatus().equals(StatusTypeCode.RESTRICTED.name())) {
                item.setStatus("R");
            } else if (item.getStatus().equals(StatusTypeCode.SUSPENDED.name())) {
                item.setStatus("S");
            }
            if(item.getHireDate() != null && item.getHireDate().length() > 10) {
                item.setHireDate(StringUtils.substring(item.getHireDate(), 0, 10));
            }
            if(item.getTerminationDate() != null && item.getTerminationDate().length() > 10) {
	        	item.setTerminationDate(StringUtils.substring(item.getTerminationDate(), 0, 10));
            }
        }
        stageEnrollmentDao.findBy()
            .insert(toStageEnrollments(items))
        ;
    }

    protected Collection<StageEnrollment> toStageEnrollments(List<? extends StageEnrollmentDTO> items) {
        return items.stream()
            .map(dto -> dto.toStageEnrollment().setBatchId(batchId))
            .collect(Collectors.toList())
        ;
    }

}