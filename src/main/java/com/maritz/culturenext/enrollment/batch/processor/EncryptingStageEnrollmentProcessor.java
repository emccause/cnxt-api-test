package com.maritz.culturenext.enrollment.batch.processor;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.maritz.core.util.DateUtils;
import com.maritz.core.util.DateUtils.FullPartialDate;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;


public class EncryptingStageEnrollmentProcessor implements ItemProcessor<StageEnrollmentDTO, StageEnrollmentDTO> {
    final Logger logger = LoggerFactory.getLogger(getClass());

    protected Environment environment;
    protected PasswordEncoder passwordEncoder;

    @Inject
    protected EncryptingStageEnrollmentProcessor setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }

    @Inject
    public EncryptingStageEnrollmentProcessor setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        return this;
    }

    @Override
    public StageEnrollmentDTO process(StageEnrollmentDTO item) throws Exception {
        logger.debug("Enrollment File Processor - process() - batchId: {} paxId: {}",
                item.getBatchId(), item.getPaxId());

        //bcrypt the password if there is one
        if (item.getPassword() != null) {
            item.setPassword(passwordEncoder.encode(item.getPassword()));
        }
        else {
            // if SSO is disabled use default password
            Boolean isSSOEnable = environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false);
            if (!isSSOEnable) {
                item.setPassword(environment.getProperty("projectProfile.defaultPassword"));   //NOTE: this value should be bcrypted already
            }
        }

        FullPartialDate birthdate = DateUtils.parseFullOrPartialDate(item.getBirthDate());
        if (birthdate != null) {

            //NOTE: full birthdate is formatted yyyy-mm-dd
            if (birthdate.getFullDate() != null) {
                item.setFullBirthDate(passwordEncoder.encode(birthdate.getFullDate()));
            }

            //partial birthdate is formatted mm-dd
            item.setBirthDate(birthdate.getPartialDate());
        }

        return item;
    }

}