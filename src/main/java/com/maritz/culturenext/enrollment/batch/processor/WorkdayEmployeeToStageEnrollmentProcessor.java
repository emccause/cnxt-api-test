package com.maritz.culturenext.enrollment.batch.processor;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.core.env.Environment;

import com.maritz.core.util.PropertiesUtils;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.workday.dto.EmployeeDTO;


public class WorkdayEmployeeToStageEnrollmentProcessor implements ItemProcessor<EmployeeDTO, StageEnrollmentDTO> {
    final Logger logger = LoggerFactory.getLogger(getClass());

    protected Environment environment;

    @Inject
    protected WorkdayEmployeeToStageEnrollmentProcessor setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }

    @Override
    public StageEnrollmentDTO process(EmployeeDTO item) throws Exception {
        logger.debug("Workday Employee Enrollment Processor - process() - participantId: {}",
                item.getParticipantId());

        StageEnrollmentDTO stageEnrollment = new StageEnrollmentDTO(item);

        // if SSO is disabled and password is blank, use default password
        Boolean isSSOEnable = environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false);
        if (!isSSOEnable) {
            if (stageEnrollment.getPassword() == null) {
                stageEnrollment.setPassword(environment.getProperty("projectProfile.defaultPassword"));   //NOTE: this value should be bcrypted already
            }
        }

        // Replace primary report id with TOP if it is necessary.
        List<String> replaceWithTopList = PropertiesUtils.getCsvList(environment, "enrollment.replaceWithTop");
        if (replaceWithTopList != null) {
            if (replaceWithTopList.contains(stageEnrollment.getPrimaryReportId())) {
                stageEnrollment.setPrimaryReportId("TOP");
            }
        }

        return stageEnrollment;
    }

}