package com.maritz.culturenext.enrollment.batch.reader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.support.AbstractItemStreamItemReader;
import org.springframework.core.env.Environment;

import com.maritz.workday.dto.EmployeeDTO;
import com.maritz.workday.services.WorkdayEmployeeService;


public class WorkdayEmployeeServiceReader extends AbstractItemStreamItemReader<EmployeeDTO> {

    protected Environment environment;
    protected WorkdayEmployeeService workdayEmployeeService;
    protected List<EmployeeDTO> employees;
    protected Iterator<EmployeeDTO> employeeIterator;

    @Inject
    protected WorkdayEmployeeServiceReader setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }

    @Inject
    public WorkdayEmployeeServiceReader setWorkdayEmployeeService(WorkdayEmployeeService workdayEmployeeService) {
        this.workdayEmployeeService = workdayEmployeeService;
        return this;
    }

    @Override
    public EmployeeDTO read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        //load the the employees from workday if they have not been loaded yet
        if (employees == null) {

            String employeeId = environment.getProperty("workday.filterByEmployeeId");
            if (employeeId == null) {
                //NOTE: this loads all of them so the list can be very large
                employees = workdayEmployeeService.getEmployees();
            }
            else {
                //only load the specified employee record
                employees = new ArrayList<>();
                EmployeeDTO employee = workdayEmployeeService.getEmployee(employeeId);
                if (employee != null) employees.add(employee);
            }
            employeeIterator = employees.iterator();
        }

        //return the next employee or null if there are no more
        return employeeIterator.hasNext() ? employeeIterator.next() : null;
    }

}
