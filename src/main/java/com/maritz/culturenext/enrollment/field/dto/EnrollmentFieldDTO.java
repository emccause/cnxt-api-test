package com.maritz.culturenext.enrollment.field.dto;

public class EnrollmentFieldDTO {
    String name;
    String displayName;
    
    public void setName(String name) {
        this.name = name;    
    }
    public String getName() {
        return name;    
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;    
    }
    public String getDisplayName() {
        return displayName;    
    }

}
