package com.maritz.culturenext.enrollment.config.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("enrollment-config")
@Api(value="/enrollment-config", description="All end points dealing with enrollment configuration")
public class EnrollmentConfigRestService {

    @Inject private EnrollmentConfigService enrollmentConfigService;

    //rest/enrollment-config/fields
    @RequestMapping(value = "fields", method = RequestMethod.GET)
    @ApiOperation(value="Get all data from ENROLLMENT_FIELD table")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully retrieved and returned the contents of "
            + "the ENROLLMENT_FIELD table", responseContainer  = "List", response = EnrollmentFieldDTO.class) })
    @Permission("PUBLIC")
    public List<EnrollmentFieldDTO> getEnrollmentFields() throws Throwable{
        return enrollmentConfigService.getEnrollmentFields();
    }
}
