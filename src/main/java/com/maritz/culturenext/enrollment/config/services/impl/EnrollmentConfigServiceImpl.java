package com.maritz.culturenext.enrollment.config.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;
import com.maritz.culturenext.jpa.entity.EnrollmentField;

@Service
public class EnrollmentConfigServiceImpl implements EnrollmentConfigService{

    @Inject private ConcentrixDao<EnrollmentField> enrollmentFieldDao;
    
    @Override
    public List<EnrollmentFieldDTO> getEnrollmentFields() {
        List<EnrollmentFieldDTO> enrollmentFieldsDto = new ArrayList<EnrollmentFieldDTO>();
        List <EnrollmentField> enrollmentFields = enrollmentFieldDao.findBy().find();
        
        for(EnrollmentField enrollmentField : enrollmentFields) {
            EnrollmentFieldDTO tempDTO = new EnrollmentFieldDTO();
            tempDTO.setName(enrollmentField.getFieldName());
            tempDTO.setDisplayName(enrollmentField.getFieldDisplayName());
            
            enrollmentFieldsDto.add(tempDTO);    
        }
            
        return enrollmentFieldsDto;
    }
}
