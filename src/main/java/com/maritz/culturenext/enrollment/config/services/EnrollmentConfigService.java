package com.maritz.culturenext.enrollment.config.services;

import java.util.List;

import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;

public interface EnrollmentConfigService {
    
    /**
     * Returns a list of enrollment fields stored in the ENROLLMENT_FIELD table
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20054018/Get+Enrollment+Fields
     */
    public List<EnrollmentFieldDTO> getEnrollmentFields();

}
