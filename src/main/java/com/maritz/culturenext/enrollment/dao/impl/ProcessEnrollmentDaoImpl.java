package com.maritz.culturenext.enrollment.dao.impl;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enrollment.dao.ProcessEnrollmentDao;

public class ProcessEnrollmentDaoImpl extends AbstractDaoImpl implements ProcessEnrollmentDao {

    @Override
    public void processEnrollmentRecords(Long batchId) {
        this.namedParameterJdbcTemplate.update(
            "exec UP_ENROLL_FROM_STAGE_TABLE :batchId"
            ,new MapSqlParameterSource()
                .addValue(ProjectConstants.BATCH_ID, batchId)
        );
    }

}
