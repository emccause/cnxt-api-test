package com.maritz.culturenext.enrollment.dao;

public interface ProcessEnrollmentDao {

    public void processEnrollmentRecords(Long batchId);

}