package com.maritz.culturenext.enrollment.dao;

import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;

import java.util.List;

public interface ParticipantEnrollmentErrorDao {
    List<ParticipantEnrollmentErrorDTO> getParticipantEnrollmentErrors(int pageNumber, int pageSize);
}
