package com.maritz.culturenext.enrollment.dao.impl;


import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.enrollment.dao.CardEnableDisableDao;

@Repository
public class CardEnableDisableDaoImpl extends AbstractDaoImpl implements CardEnableDisableDao {

    private String ENABLE_DISABLE_PROCEDURE = "UP_ENABLE_DISABLE_CARD";

    @Override
    public void enableDisableRideauCardStatus() {
        SimpleJdbcCall call = new SimpleJdbcCall(dataSource)
            .withProcedureName(ENABLE_DISABLE_PROCEDURE);
        call.execute();
    }

}
