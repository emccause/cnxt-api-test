package com.maritz.culturenext.enrollment.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.enrollment.dao.ParticipantEnrollmentErrorDao;
import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.time.ZoneId;
import java.util.List;

@Repository
public class ParticipantEnrollmentErrorDaoImpl extends AbstractDaoImpl  implements ParticipantEnrollmentErrorDao {

    private static final String PAGE_NUMBER_PARAM = "PAGE_NUMBER";
    private static final String PAGE_SIZE_PARAM = "PAGE_SIZE";

    private static final String QUERY = "SELECT * FROM component.VW_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS "+
            " order by last_name asc, first_name asc " +
            " OFFSET ((:PAGE_NUMBER - 1) * :PAGE_SIZE) ROWS FETCH NEXT :PAGE_SIZE ROWS ONLY";

    private static final String LAST_NAME = "LAST_NAME";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String CONTROL_NUMBER = "CONTROL_NUMBER";
    private static final String ERROR_DATETIME = "ERROR_DATETIME";
    private static final String ERROR = "ERROR";

    public List<ParticipantEnrollmentErrorDTO> getParticipantEnrollmentErrors(int pageNumber, int pageSize){
        if (pageNumber <= 0 || pageSize <=0){
            throw new IndexOutOfBoundsException();
        }
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);

        return namedParameterJdbcTemplate.query(QUERY, params,
                (rs, rowNum) ->
                new ParticipantEnrollmentErrorDTO(
                        rs.getString(LAST_NAME),
                        rs.getString(FIRST_NAME),
                        rs.getString(CONTROL_NUMBER),
                        rs.getTimestamp(ERROR_DATETIME).toLocalDateTime().atZone(ZoneId.systemDefault()),
                        rs.getString(ERROR)
                ));
    }
}
