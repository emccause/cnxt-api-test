package com.maritz.culturenext.enrollment.dao;

public interface CardEnableDisableDao {
    public void enableDisableRideauCardStatus();
}
