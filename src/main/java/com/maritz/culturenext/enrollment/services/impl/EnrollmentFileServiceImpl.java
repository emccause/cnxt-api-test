package com.maritz.culturenext.enrollment.services.impl;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import com.maritz.core.dto.FileDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.services.FileService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import com.maritz.culturenext.enrollment.services.EnrollmentFileService;
import com.maritz.culturenext.enums.MediaTypesEnum;

@Component
public class EnrollmentFileServiceImpl implements EnrollmentFileService {
	/*
	 * Programmer Note:
	 *    If in the future other file types would be accepted, then a Factory pattern should be used instead.
	 */
	@Inject FileService inboxFileService;
	final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public Boolean doesEnrollmentFileExist(String filename) {
		return inboxFileService.exists(filename);
	}

	@Override
	public boolean filesOnServer(String path){
		return inboxFileService.getFiles(path).stream().filter(f -> !f.getIsDirectory()).count()>0;
	}

	@Override
	public boolean enrollmentFolderExists(){
		Optional<FileDTO> fileDTO = inboxFileService.getFiles().stream().filter(f -> f.getIsDirectory() && f.getName().compareTo(EnrollmentConstants.ENROLLMENT_SUBFOLDER_NAME) == 0).findFirst();
		return fileDTO.isPresent();
	}

	@Override
	public void uploadEnrollmentFile(List<MultipartFile> files) throws IOException {

		if(files.isEmpty() || files.get(0).isEmpty()) {
			throw new ErrorMessageException().addErrorMessage(ProjectConstants.ERROR_ALL_CAPS,
					EnrollmentConstants.MISSING_CSV_FILE,
					EnrollmentConstants.MISSING_CSV_FILE_MSG);
		}

		logger.info("processing files list on uploadEnrollmentFile");

		for (MultipartFile file : files){
			validateFileToUpload(file);     //validation
			String fullFileName = EnrollmentConstants.ENROLLMENT_SUBFOLDER + file.getOriginalFilename();

			if (!doesEnrollmentFileExist(EnrollmentConstants.ENROLLMENT_SUBFOLDER + file.getOriginalFilename())) {
				inboxFileService.add(EnrollmentConstants.ENROLLMENT_SUBFOLDER, file);
				logger.info("{} has been created.", fullFileName);
			}else{
				// we need to update the file name from the original file name. InboxFileService doesn't currently support overwrite, so we need to delete the old file
				inboxFileService.delete(fullFileName);
				inboxFileService.add(EnrollmentConstants.ENROLLMENT_SUBFOLDER, file);
				logger.info("{} has been updated.", fullFileName);
			}
		}

		logger.info("all files have been uploaded on inbox/enrollment properly");
	}

	@Override
	public void validateFileToUpload(MultipartFile fileUpload) {
		Boolean fileNameValid = fileUpload.getOriginalFilename().endsWith(MediaTypesEnum.TEXT_CSV.getFileExtension());
		Boolean mediaTypeValid = MediaTypesEnum.getCSVMediaTypes().contains(fileUpload.getContentType().toLowerCase());

		if (Boolean.FALSE.equals(fileNameValid) || Boolean.FALSE.equals(mediaTypeValid)) {
			throw new ErrorMessageException().addErrorMessage(fileUpload.getOriginalFilename(),
					EnrollmentConstants.INVALID_FILE_FORMAT,
					EnrollmentConstants.INVALID_FILE_FORMAT_MSG);
		}
	}
}
