package com.maritz.culturenext.enrollment.services;

import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;

import java.util.List;

public interface EnrollmentService {
    List<ParticipantEnrollmentErrorDTO> getCurrentParticipantEnrollmentErrors(int pageNumber, int pageSize) throws Throwable;
}
