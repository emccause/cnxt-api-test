package com.maritz.culturenext.enrollment.services;

public interface CardEnableDisableService {

    void enableDisableRideauCardStatus();

}
