package com.maritz.culturenext.enrollment.services.impl;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.culturenext.enrollment.dao.CardEnableDisableDao;
import com.maritz.culturenext.enrollment.services.CardEnableDisableService;

@Service
public class CardEnableDisableServiceImpl implements CardEnableDisableService  {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject CardEnableDisableDao cardEnableDisableDao;

    @Override
    @TriggeredTask
    public void enableDisableRideauCardStatus() {
        logger.info("Started enableDisableRideauCardStatus job. (updateRideauAccountStatus)");
        cardEnableDisableDao.enableDisableRideauCardStatus();
        logger.info("Finished enableDisableRideauCardStatus job. (updateRideauAccountStatus)");
    }

}
