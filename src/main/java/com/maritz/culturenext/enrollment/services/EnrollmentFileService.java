package com.maritz.culturenext.enrollment.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public interface EnrollmentFileService {

    /**
     * Places the file on the server.  Throws an error if it is not possible to write to the server
     *
     * @param List<MultipartFile> files files to be uploaded.
     * @throws IOException
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847474/Enrollment+File+processing+Example
     */
    void uploadEnrollmentFile(List<MultipartFile> files) throws IOException;



    /**
     * validates the type of file is the expected one.
     * @param fileUpload
     */
    void validateFileToUpload(MultipartFile fileUpload);

    /**
     * checks whether the enrollment file exists. The path is hardcoded in the implementing class.
     * @return
     */
    Boolean doesEnrollmentFileExist(String filename);

    boolean filesOnServer(String path);

	boolean enrollmentFolderExists();
}
