package com.maritz.culturenext.enrollment.services.impl;

import com.maritz.culturenext.enrollment.dao.ParticipantEnrollmentErrorDao;
import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;
import com.maritz.culturenext.enrollment.services.EnrollmentService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class EnrollmentServiceImpl implements EnrollmentService {

    @Inject
    private ParticipantEnrollmentErrorDao participantEnrollmentErrorDao;

    @Override
    public List<ParticipantEnrollmentErrorDTO> getCurrentParticipantEnrollmentErrors(int pageNumber, int pageSize) throws Throwable {
        return participantEnrollmentErrorDao.getParticipantEnrollmentErrors(pageNumber, pageSize);
    }
}
