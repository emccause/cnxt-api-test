package com.maritz.culturenext.enrollment.rest;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;
import com.maritz.culturenext.enrollment.services.EnrollmentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_NUMBER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_SIZE_REST_PARAM;

@RestController
@RequestMapping("enrollments")
public class EnrollmentRestService {

    @Inject
    private EnrollmentService enrollmentService;
    //GET /enrollment/errors/current
    @PreAuthorize("@security.hasRole('MTZ:ADM')")
    @RequestMapping(method = RequestMethod.GET, value = "/errors/current")
    @ApiOperation(value="Get all the groups", notes="This method allows a user to get all current participant enrollment errors")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrive of all participant enrollment errors"),
            @ApiResponse(code = 404, message = "Participant enrollment not found") })
    public List<ParticipantEnrollmentErrorDTO> getCurrentParticipantEnrollmentErrors(
            @ApiParam(value="page of activity feed used to paginate request.",required = true)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = true) int pageNumber,
            @ApiParam(value="The number of records per page.",required = true)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = true) int pageSize
    ) throws Throwable {
        return this.enrollmentService.getCurrentParticipantEnrollmentErrors(pageNumber, pageSize);
    }
}
