package com.maritz.culturenext.enrollment.rest;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.security.authentication.dto.UserRegisterRequestDTO;
import com.maritz.culturenext.security.authentication.dto.UserRegisterTokenResponseDTO;
import com.maritz.culturenext.security.authentication.services.UserRegisterService;

@RestController
@RequestMapping("users")
public class UserRegisterRestService {

    @Inject UserRegisterService userRegisterService;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @SecurityPolicy(authenticated=false)
    public UserRegisterTokenResponseDTO  registerUser(
        @RequestBody UserRegisterRequestDTO userRegisterRequest
    ) {
        return userRegisterService.register(userRegisterRequest);
    }

}