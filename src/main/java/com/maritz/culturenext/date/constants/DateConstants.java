package com.maritz.culturenext.date.constants;

public class DateConstants {

    public static final String UTC_TIMEZONE = "UTC";
    public static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm'Z'";
    public static final String ISO_8601_DATE_ONLY_FORMAT = "yyyy-MM-dd";
    
    // The below are two date formats used by components to store dates as strings
    public static final String COMPONENTS_DATE_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";
    public  static final String COMPONENTS_DATE_FORMAT_2 = "yyyyMMdd HH:mm:ss";

    public  static final String ERROR_FROM_DATE_INVALID = "FROM_DATE_INVALID";
    public  static final String ERROR_FROM_DATE_INVALID_MSG = "From Date is invalid";
    public  static final String ERROR_THRU_DATE_INVALID = "THRU_DATE_INVALID";
    public  static final String ERROR_THRU_DATE_INVALID_MSG = "Thru Date is invalid";
    public  static final String ERROR_FROM_DATE_AFTER_THRU_DATE = "FROM_DATE_AFTER_THRU_DATE";
    public  static final String ERROR_FROM_DATE_AFTER_THRU_DATE_MSG = "Thru date has to be after from date";

    public  static final String FROM_DATE_FIELD = "fromDate";
    public  static final String THRU_DATE_FIELD = "thruDate";

    // Date validation regex
    public static final String VALID_DATE_YYYY_MM_DD = "(^\\d{4}-\\d{2}-\\d{2}$)";
}
