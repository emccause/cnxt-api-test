package com.maritz.culturenext.system.util;

import java.io.File;

public class SystemUtil {
    
    private static final String CATALINA_BASE = "catalina.base";
    private static final String USER_DIR = "user.dir";
    
    /**
     * References catalina.base system property (set on server to /prj/userX/website), returns root directory for project (/prj/userX/)
     * @return root directory for project
     */
    public static String getRootDirectory() {
        String location = System.getProperty(CATALINA_BASE);
        if (location == null) {
            location = System.getProperty(USER_DIR);
        }
        location = stripEndingFileSeparator(location);
        location = location.substring(0, location.lastIndexOf(File.separator)); // move up a directory
        return location;
    }
    
    /**
     * This method takes a file location on the server and strips off the ending file separator if there is one
     * @param location
     * @return String
     */
    public static String stripEndingFileSeparator(String filePath) {
        if(filePath.endsWith(File.separator)){ // strip off ending separator if it exists
            filePath = filePath.substring(0, filePath.length() - File.separator.length());
        }
        return filePath;
    }

}
