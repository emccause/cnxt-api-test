package com.maritz.culturenext.participant.rest;

import static com.maritz.culturenext.constants.ProjectConstants.PARTICIPANTS_URI_BASE;
import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.rest.NotFoundErrorMessage;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.participant.service.ParticipantInfoService;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.maritz.culturenext.constants.ProjectConstants.CONTROL_NUMBER_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.CONTROL_NUMBER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_NUMBER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_SIZE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAX_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.STATUS_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.TYPE_REST_PARAM;


@RestController
@Api(value="/participants")
@Slf4j
public class ParticipantInfoRestService {

    private static final String GROUPS_BY_PARTICIPANT_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + PAX_ID_REST_PARAM + "}/" + ProjectConstants.GROUPS_URI_BASE;
    private static final String HIERARCHY_HISTORY_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/" + ProjectConstants.HIERARCHY_URI_BASE;
    private static final String UPDATE_PARTICIPANT_ADDRESS_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/update-address/{" + ADDRESS_ID_REST_PARAM + "}";
    private static final String EMAIL_HISTORY_SEARCH = "email-history";

    @Inject private ParticipantInfoService participantInfoService;
    @Inject private Security security;

    //rest/participants/~/groups
    @GetMapping(GROUPS_BY_PARTICIPANT_PATH)
    @ApiOperation(value = "Returns a paginated list of groups that a participant is in")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned the groups participant is in")
    })
    @Permission("PUBLIC")
    public PaginatedResponseObject<GroupDTO> getGroupsByParticipant(
            @ApiParam(value = "Id of the participant")
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "Comma-delimited list of ", required = false)
            @RequestParam(value = STATUS_REST_PARAM, required = false) String statusList,
            @ApiParam(value = "Comma-delimited list of ", required = false)
            @RequestParam(value = TYPE_REST_PARAM, required = false) String typeList,
            @ApiParam(value = "Page of activity feed used to paginate request", required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value = "Number of records per page.", required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) throws NoHandlerFoundException {

        Long pathPaxId = security.getPaxId(paxIdString);
        if(pathPaxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(PAX_ID_REST_PARAM, paxIdString);

        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(STATUS_REST_PARAM, statusList);
        requestParamMap.put(TYPE_REST_PARAM, typeList);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(GROUPS_BY_PARTICIPANT_PATH).setPathParamMap(pathParamMap)
                        .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);

        return participantInfoService.getGroupsByParticipant(
                requestDetails, pathPaxId, statusList, typeList, pageNumber, pageSize
        );
    }

    //rest/participants/~/overview
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @GetMapping(value = PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/overview", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returns the participant overview")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the participant overview."),
            @ApiResponse(code = 404, message = "Participant not found.", response = NotFoundErrorMessage.class)
    })
    public ParticipantDTO getParticipantOverview(
            @ApiParam(value = "Control Number of the participant")
            @PathVariable(CONTROL_NUMBER_PARAM) String controlNumber) {

        log.info("Get Participant Overview for control number: " + controlNumber );
        return participantInfoService.getParticipant(controlNumber);
    }

    //rest/participants/~/email-history
    //TODO update permissions for this endpoint
    @GetMapping(value = PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/email-history", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value="Fetch the email history of the participant aligned with the control number submitted",
            notes ="Values being returned are SENT_DATE the" +
                    "actual date the email was sent, SUBJECT the string in the subject line of the email," +
                    "EVENT_TYPE_DESCRIPTION the event that triggered the email i.e. 'Recognition - Manager Notification',")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully returned the list of participant emails.", response = EmployeeDTO.class)})
    @Permission("PUBLIC")
    public PaginatedResponseObject<EmailDTO> emailHistorySearch(
            @ApiParam(value="Control Number of email history being searched for.")
            @PathVariable(CONTROL_NUMBER_PARAM) String controlNumber,
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value="The number of records per page.",required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize

    ) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(CONTROL_NUMBER_PARAM, controlNumber);
        requestParamMap.put(PAGE_NUMBER_REST_PARAM, pageNumber);
        requestParamMap.put(PAGE_SIZE_REST_PARAM, pageSize);

        PaginationRequestDetails requestDetails = new PaginationRequestDetails().setRequestPath(EMAIL_HISTORY_SEARCH)
                .setRequestParamMap(requestParamMap).setPageNumber(pageNumber)
                .setPageSize(pageSize);

        return participantInfoService.emailMessageSearch(requestDetails, pageNumber, pageSize, controlNumber);

    }

    //rest/participants/~/enrollment
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @GetMapping(value = PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/enrollment", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returns the participant Enrollment History")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the participant Enrollment History.")
    })
    public List<EnrollmentHistoryDTO> getParticipantEnrollmentHistory(
            @ApiParam(value = "Control Number of the participant")
            @PathVariable(CONTROL_NUMBER_PARAM) String controlNumber) {

        log.info("Get Participant Enrollment History for control number: " + controlNumber );
        return participantInfoService.getParticipantEnrollmentHistory(controlNumber);
    }

    //rest/participants/~/hierarchy
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @GetMapping(value = HIERARCHY_HISTORY_PATH)
    @ApiOperation(value = "Returns a paginated list of hierarchy history")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned the hierarchy history of a participant")
    })
    public PaginatedResponseObject<HierarchyHistoryDTO> getHierarchyHistory(
            @ApiParam(value = "Control Number of the participant")
                @PathVariable(CONTROL_NUMBER_PARAM) String controlNumber,
            @ApiParam(value = "Page of activity feed used to paginate request", required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value = "Number of records per page.", required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(CONTROL_NUMBER_REST_PARAM, controlNumber);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(HIERARCHY_HISTORY_PATH).setPathParamMap(pathParamMap)
                        .setPageNumber(pageNumber).setPageSize(pageSize);

        return participantInfoService.getHierarchyHistory(
                controlNumber, requestDetails, pageNumber, pageSize
        );
    }

    // rest/participants/{control number}/roles
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @PutMapping(value = PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Change the participant roles")
    public void changeParticipantRoles(
            @ApiParam(value = "Control Number of the participant")
            @PathVariable(CONTROL_NUMBER_PARAM) String controlNumber,
            @ApiParam(value = "the roles to update")
            @RequestBody Set<Long> roleIds )throws ErrorMessageException {

        participantInfoService.changeParticipantRoles(controlNumber, roleIds);
    }

    //rest/participants/~/update-address/~
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @PutMapping(UPDATE_PARTICIPANT_ADDRESS_PATH)
    @ApiOperation(value = "Update the address of a participant")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated the address.", response = AddressDTO.class),
            @ApiResponse(code = 400, message = "Bad request body.", response = AddressDTO.class),
            @ApiResponse(code = 404, message = "Invalid address ID based on the control number"),})
    public AddressDTO updateParticipantAddress(
            @ApiParam(value = "Control number of the participant", required = true)
            @PathVariable(CONTROL_NUMBER_REST_PARAM) String controlNumber,
            @ApiParam(value = "Address id for participant's address", required = true)
            @PathVariable(ADDRESS_ID_REST_PARAM) Long addressId,
            @RequestBody AddressDTO addressDTO
    ) {

        return participantInfoService.updateParticipantAddress(controlNumber, addressId, RequestMethod.PUT.toString(), addressDTO);
    }


}
