package com.maritz.culturenext.participant.dao;

import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import java.util.List;
import java.util.Map;

public interface ParticipantInfoDao {

    EmployeeDTO getEmployeeDTO(Long paxId);

    List<EntityDTO> getInfo(List<Long> paxIds, List<Long> groupIds);

    List<Long> getParticipantGroups(Long paxId);

    Boolean isPaxEligibleReceiverInProgram(Long paxId, Long programId);

    EmployeeDTO getParticipantInfo(Long paxId, Long programId);

    List<Map<String, Object>> getPaxInfo(String paxIdList);

    /**
     * Return groups that a participant is a part of
     *
     * @param loggedInPaxId - currently logged in pax id
     * @param statusList - list of statuses as a comma delimited string
     * @param typeList - list of types as a comma delimited string
     * @param pageNumber - page number for pagination
     * @param pageSize - page size for pagination
     * @return List&lt;Map&lt;String,Object&gt;&gt; - list of string-object maps of
     *         the group data for that participant
     */
    List<Map<String, Object>> getGroupsByParticipant(Long loggedInPaxId, String statusList,
            String typeList, Integer pageNumber, Integer pageSize);

    /**
     * Gets the participant overview information
     *
     * @param controlNumber - Control number of the participant to retrieve the overview
     * @return ParticipantDTO - Participant overview
     */
    ParticipantDTO getParticipantOverview(String controlNumber);

    /**
     * Returns a page of group data that match the provided params.
     *
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param searchString - Text string
     *
     * @return page of data
     */
    List<Map<String, Object>> emailMessageSearch(Integer pageNumber, Integer pageSize, String searchString);

    List<EnrollmentHistoryDTO> getParticipantEnrollmentHistory(String controlNumber);
    List<Map<String, Object>>  getHierarchyHistory(String controlNumber, Integer pageNumber, Integer pageSize);
}
