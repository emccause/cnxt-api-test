// TODO: Inconsistency on how dates are saved: https://maritz.atlassian.net/browse/CNXT-543
package com.maritz.culturenext.participant.dao.impl;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.role.dao.RoleDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS1;
import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS2;
import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS3;
import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS4;
import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS5;
import static com.maritz.culturenext.constants.ProjectConstants.ADDRESS6;
import static com.maritz.culturenext.constants.ProjectConstants.AREA;
import static com.maritz.culturenext.constants.ProjectConstants.CITY;
import static com.maritz.culturenext.constants.ProjectConstants.COMMA_DELIM;
import static com.maritz.culturenext.constants.ProjectConstants.COMPANY_NAME;
import static com.maritz.culturenext.constants.ProjectConstants.CONTROL_NUM;
import static com.maritz.culturenext.constants.ProjectConstants.COST_CENTER;
import static com.maritz.culturenext.constants.ProjectConstants.COUNTRY_CODE;
import static com.maritz.culturenext.constants.ProjectConstants.DEPARTMENT;
import static com.maritz.culturenext.constants.ProjectConstants.DESCRIPTION;
import static com.maritz.culturenext.constants.ProjectConstants.EMAIL_ADDRESS;
import static com.maritz.culturenext.constants.ProjectConstants.FIRST_NAME;
import static com.maritz.culturenext.constants.ProjectConstants.FUNCTION;
import static com.maritz.culturenext.constants.ProjectConstants.GRADE;
import static com.maritz.culturenext.constants.ProjectConstants.GROUP_ID;
import static com.maritz.culturenext.constants.ProjectConstants.HIRE_DATE;
import static com.maritz.culturenext.constants.ProjectConstants.ID;
import static com.maritz.culturenext.constants.ProjectConstants.JOB_TITLE;
import static com.maritz.culturenext.constants.ProjectConstants.LAST_NAME;
import static com.maritz.culturenext.constants.ProjectConstants.MANAGER_CONTROL_NUMBER;
import static com.maritz.culturenext.constants.ProjectConstants.MISC_DATA;
import static com.maritz.culturenext.constants.ProjectConstants.PHONE_NUMBER;
import static com.maritz.culturenext.constants.ProjectConstants.POSTAL_CODE;
import static com.maritz.culturenext.constants.ProjectConstants.STATE;
import static com.maritz.culturenext.constants.ProjectConstants.STATUS_TYPE_CODE;
import static com.maritz.culturenext.constants.ProjectConstants.SYS_USER_NAME;
import static com.maritz.culturenext.date.constants.DateConstants.ISO_8601_DATE_ONLY_FORMAT;

@Slf4j
@Component
public class ParticipantInfoDaoImpl extends PaginatingDaoImpl implements ParticipantInfoDao {

    private static final String LOGGED_IN_PAX_ID_PARAM = "LOGGED_IN_PAX_ID";
    private static final String STATUS_LIST_PARAM = "STATUS_LIST";
    private static final String TYPE_LIST_PARAM = "TYPE_LIST";
    private static final String RS_SEARCH_TYPE_CAMEL_CASE = "searchType";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String PROGRAM_ID_SQL_PARAM = "PROGRAM_ID";
    private static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST";
    private static final String PAX_RESULT_TYPE = "P";
    private static final String CONTROL_NUMBER_PARAM = "CONTROL_NUMBER";
    private static final String CUSTOM_FIELD_PREFIX = "CUSTOM_";
    private static final int MAX_CUSTOM_FIELD = 5;
    private static final String CUSTOM_FIELD_NUMBER = "fieldNumber";
    private static final String CHANGE_DATE = "CHANGE_DATE";
    private static final String FROM_DATE = "FROM_DATE";
    private static final String THRU_DATE = "THRU_DATE";
    private static final String STATUS = "STATUS";

    private static final String PAGE_NUMBER_PARAM = "PAGE_NUMBER";
    private static final String PAGE_SIZE_PARAM = "PAGE_SIZE";
    private static final String SEARCH_STRING_SQL_PARAM = "SEARCH_STRING";

    private static final String WHERE_CLAUSE_PLACEHOLDER = "{WHERE_CLAUSE_PLACEHOLDER}";

    //TODO: Check component.UF_LIST_TO_TABLE performance https://maritz.atlassian.net/browse/CNXT-542
    private static final String PAX_WHERE_CLAUSE =
            "PAX_ID IN (SELECT * FROM UF_LIST_TO_TABLE(:" + PAX_ID_LIST_SQL_PARAM + ", ','))";
    private static final String GROUP_WHERE_CLAUSE =
            "GROUP_ID IN (SELECT * FROM UF_LIST_TO_TABLE(:" + GROUP_ID_LIST_SQL_PARAM + ", ','))";

    private static final String CLAUSE_COMBINER = " OR ";

    private static final String QUERY = "SELECT *" +
            " FROM VW_USER_GROUPS_SEARCH_DATA" +
            " WHERE " + WHERE_CLAUSE_PLACEHOLDER +
            "ORDER BY FIRST_NAME, LAST_NAME";

    private static final String GET_GROUP_IDS_FOR_PAX_ID =
            "SELECT group_id, pax_id " +
                    "FROM VW_GROUP_TOTAL_MEMBERSHIP " +
                    "WHERE pax_id = :" + PAX_ID_SQL_PARAM;

    private static final String QUERY_PAX_ELIGIBLE_RECEIVER_IN_PROGRAM =
            "SELECT 1 AS ELIGIBLE FROM COMPONENT.ACL ACL"
                    + " WHERE ACL.SUBJECT_ID = :" + PAX_ID_SQL_PARAM
                    + " AND ACL.SUBJECT_TABLE = 'PAX'"
                    + " AND ACL.TARGET_TABLE = 'PROGRAM'"
                    + "    AND ACL.TARGET_ID = :" + PROGRAM_ID_SQL_PARAM
                    + " AND ACL.ROLE_ID IN ("
                    + "    SELECT ROLE_ID "
                    + "    FROM ROLE "
                    + "    WHERE ROLE_CODE='PREC') "
                    + "UNION "
                    + "    SELECT 1"
                    + " FROM ACL ACL"
                    + " INNER JOIN VW_GROUP_TOTAL_MEMBERSHIP VGTM"
                    + " ON ACL.SUBJECT_ID=VGTM.group_id "
                    + " WHERE VGTM.pax_id = :" + PAX_ID_SQL_PARAM
                    + " AND ACL.SUBJECT_TABLE='GROUPS' "
                    + " AND ACL.TARGET_TABLE='PROGRAM'"
                    + " AND ACL.TARGET_ID = :" + PROGRAM_ID_SQL_PARAM
                    + " AND ACL.ROLE_ID IN ("
                    + "    SELECT ROLE_ID "
                    + "    FROM ROLE "
                    + " WHERE ROLE_CODE='PREC') ";

    private static final String PAX_INFO_QUERY = "EXEC component.UP_GET_PAX_INFO :" + PAX_ID_LIST_SQL_PARAM;

    private static final String GROUPS_BY_PARTICIPANT_QUERY =
            "SELECT "
                    + "G.GROUP_ID, "
                    + "G.GROUP_CONFIG_ID, "
                    + "GC.GROUP_CONFIG_NAME AS FIELD, "
                    + "GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME, "
                    + "G.GROUP_DESC AS GROUP_NAME, "
                    + "GT.GROUP_TYPE_DESC AS TYPE, "
                    + "GV.VISIBILITY, "
                    + "G.STATUS_TYPE_CODE AS STATUS, "
                    + "G.CREATE_DATE, "
                    + "(SELECT COUNT(*) FROM component.GROUPS_PAX "
                    + "WHERE GROUP_ID = G.GROUP_ID) AS PAX_COUNT, "
                    + "(SELECT COUNT(*) FROM component.VW_GROUPS_GROUPS "
                    + "WHERE GROUP_ID_1 = G.GROUP_ID) AS GROUP_COUNT, "
                    + "(SELECT COUNT(*) FROM component.VW_GROUP_TOTAL_MEMBERSHIP "
                    + "WHERE GROUP_ID = G.GROUP_ID) AS TOTAL_PAX_COUNT, "
                    + "G.PAX_ID AS GROUP_PAX_ID "
                    + "FROM component.GROUPS G "
                    + "LEFT OUTER JOIN component.GROUP_CONFIG GC "
                    + "ON G.GROUP_CONFIG_ID = GC.ID "
                    + "LEFT OUTER JOIN component.VW_GROUPS_VISIBILITY GV "
                    + "ON GV.GROUP_ID = G.GROUP_ID "
                    + "LEFT JOIN component.GROUP_TYPE GT "
                    + "ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE "
                    + "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP VGTM "
                    + "ON G.GROUP_ID = VGTM.GROUP_ID "
                    + "WHERE VGTM.PAX_ID = :" + LOGGED_IN_PAX_ID_PARAM + " "
                    + "AND ( :" + STATUS_LIST_PARAM + " IS NULL "
                    //TODO: Check component.UF_LIST_TO_TABLE performance https://maritz.atlassian.net/browse/CNXT-542
                    + "OR G.STATUS_TYPE_CODE IN (SELECT * FROM component.UF_LIST_TO_TABLE( :" + STATUS_LIST_PARAM + ", ','))) "
                    + "AND ( :" + TYPE_LIST_PARAM + " IS NULL "
                    //TODO: Check component.UF_LIST_TO_TABLE performance https://maritz.atlassian.net/browse/CNXT-542
                    + "OR GT.GROUP_TYPE_DESC IN (SELECT * FROM component.UF_LIST_TO_TABLE( :" + TYPE_LIST_PARAM + ", ','))) ";

    private static final String GROUPS_BY_PARTICIPANT_ORDER_BY = "G.GROUP_DESC ASC";

    private static final String PARTICIPANT_OVERVIEW_QUERY =
            "SELECT p.PAX_ID, " +
                "p.CONTROL_NUM, " +
                "p.FIRST_NAME, " +
                "p.LAST_NAME, " +
                "su.SYS_USER_NAME, " +
                "su.STATUS_TYPE_CODE, " +
                "e.EMAIL_ADDRESS, " +
                "p_manager.CONTROL_NUM AS 'MANAGER_CONTROL_NUMBER', " +
                "a.CITY, " +
                "a.STATE, " +
                "a.ZIP AS 'POSTAL_CODE', " +
                "a.COUNTRY_CODE, " +
                "a.address_id, " +
                "a.ADDRESS1, " +
                "a.ADDRESS2, " +
                "a.ADDRESS3, " +
                "a.ADDRESS4, " +
                "a.ADDRESS5, " +
                "a.ADDRESS6, " +
                "pm_hireDate.MISC_DATA AS 'HIRE_DATE', " +
                "pgm_title.MISC_DATA AS 'JOB_TITLE', " +
                "pgm_companyName.MISC_DATA AS 'COMPANY_NAME', " +
                "pgm_department.MISC_DATA AS 'DEPARTMENT', " +
                "pgm_costCenter.MISC_DATA AS 'COST_CENTER', " +
                "pgm_function.MISC_DATA AS 'FUNCTION', " +
                "pgm_grade.MISC_DATA AS 'GRADE', " +
                "pgm_area.MISC_DATA AS 'AREA', " +
                "ph.PHONE AS 'PHONE_NUMBER' " +
            "FROM component.PAX p " +
                "JOIN component.PAX_GROUP pg ON " +
                    "pg.PAX_ID = p.PAX_ID " +
                "JOIN component.SYS_USER su ON " +
                    "su.PAX_ID = p.PAX_ID " +
                "LEFT JOIN component.EMAIL e ON " +
                    "e.PAX_ID = p.PAX_ID " +
                    "AND e.PREFERRED = 'Y' " +
                "LEFT JOIN component.PHONE ph ON " +
                    "ph.PAX_ID = p.PAX_ID " +
                    "AND ph.PREFERRED = 'Y' " +
                "LEFT JOIN component.ADDRESS a ON " +
                    "a.PAX_ID = p.PAX_ID " +
                    "AND a.PREFERRED = 'Y' " +
                "LEFT JOIN component.PAX_MISC pm_hireDate ON " +
                    "pm_hireDate.PAX_ID = pg.PAX_ID " +
                    "AND pm_hireDate.VF_NAME = 'HIRE_DATE' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_title ON " +
                    "pgm_title.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_title.VF_NAME = 'JOB_TITLE' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_companyName ON " +
                    "pgm_companyName.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_companyName.VF_NAME = 'COMPANY_NAME' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_department ON " +
                    "pgm_department.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_department.VF_NAME = 'DEPARTMENT' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_costCenter ON " +
                    "pgm_costCenter.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_costCenter.VF_NAME = 'COST_CENTER' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_area ON " +
                    "pgm_area.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_area.VF_NAME = 'AREA' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_grade ON " +
                    "pgm_grade.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_grade.VF_NAME = 'GRADE' " +
                "LEFT JOIN component.PAX_GROUP_MISC pgm_function ON " +
                    "pgm_function.PAX_GROUP_ID = pg.PAX_GROUP_ID " +
                    "AND pgm_function.VF_NAME = 'FUNCTION' " +
                "LEFT JOIN component.RELATIONSHIP r ON " +
                    "r.PAX_ID_1 = p.PAX_ID " +
                    "AND r.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
                "LEFT JOIN component.PAX p_manager ON " +
                    "p_manager.PAX_ID = r.PAX_ID_2  " +
            "WHERE p.CONTROL_NUM  = :" + CONTROL_NUMBER_PARAM;

    private static final String PARTICIPANT_OVERVIEW_CUSTOM_FIELDS_QUERY =
            "SELECT " +
                "CAST(RIGHT(pgm.VF_NAME, " +
                "LEN(pgm.VF_NAME)-" + String.valueOf(CUSTOM_FIELD_PREFIX.length()) + ") AS INT) AS FIELD_NUMBER, " +
                "pgm.VF_NAME, " +
                "pgm.MISC_DATA " +
                ProjectConstants.FROM_FRAGMENT +
                " PAX p " +
                "JOIN PAX_GROUP pg ON " +
                    "p.PAX_ID = pg.PAX_ID " +
                "JOIN PAX_GROUP_MISC pgm ON " +
                    "pg.PAX_GROUP_ID = pgm.PAX_GROUP_ID " +
            "WHERE pgm.VF_NAME like '" + CUSTOM_FIELD_PREFIX +"%' " +
                "AND p.CONTROL_NUM = :" + CONTROL_NUMBER_PARAM +
            " ORDER BY FIELD_NUMBER";

    private static final String PARTICIPANT_OVERVIEW_GROUPS_QUERY =
            "SELECT g.GROUP_ID AS ID, " +
                //TODO: Fix Name column values stored in Description column https://maritz.atlassian.net/browse/CNXT-539
                "g.GROUP_DESC AS DESCRIPTION " +
                ProjectConstants.FROM_FRAGMENT + " component.PAX p " +
                "JOIN component.GROUPS_PAX gp ON " +
                    "gp.PAX_ID = p.PAX_ID  " +
                "JOIN component.groups g ON " +
                    "g.GROUP_ID = gp.GROUP_ID  " +
                ProjectConstants.WHERE_FRAGMENT +
                " p.CONTROL_NUM = :" + CONTROL_NUMBER_PARAM +
            " ORDER BY DESCRIPTION";

    private static String PARTICIPANT_ENROLLMENT_HISTORY_QUERY =
            "SELECT eh.CHANGE_DATE, " +
                "eh.FROM_DATE, " +
                "eh.THRU_DATE, " +
                "CASE  " +
                "WHEN (eh.THRU_DATE IS NULL " +
                "OR eh.THRU_DATE > GETDATE()) THEN 'ACTIVE' " +
                "ELSE 'INACTIVE' " +
                "END STATUS " +
                ProjectConstants.FROM_FRAGMENT + " (" +
                "SELECT PAX_ID, " +
                    "UPDATE_DATE CHANGE_DATE, " +
                    "FROM_DATE, " +
                    "THRU_DATE " +
                ProjectConstants.FROM_FRAGMENT +
                    " component.PAX_GROUP pg  " +
                "UNION " +
                "SELECT PAX_ID, " +
                    "UPDATE_DATE, " +
                    "FROM_DATE, " +
                    "UPDATE_DATE  " + // All HISTORY_PAX_GROUP enrollment should be 'INACTIVE'
                ProjectConstants.FROM_FRAGMENT +
                    " component.HISTORY_PAX_GROUP hpg) eh " +
                    // enrollment history
            "JOIN component.PAX p on " +
                "p.PAX_ID = eh.PAX_ID " +
                ProjectConstants.WHERE_FRAGMENT +
                " p.CONTROL_NUM = :" + CONTROL_NUMBER_PARAM  +
            " ORDER BY " +
                "eh.CHANGE_DATE DESC";

    private static final String EMAIL_HISTORY_MESSAGE_SEARCH = "exec component.UP_EMAIL_HISTORY_SEARCH" +
            " :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + SEARCH_STRING_SQL_PARAM;

    private static final String HIERARCHY_HISTORY_QUERY =
            "SELECT res.FROM_DATE, " +
                "res.THRU_DATE, " +
                "CONTROL_NUM2 CONTROL_NUMBER " +
                ProjectConstants.FROM_FRAGMENT +
                " ( " +
                "SELECT  " +
                    "p1.PAX_ID, " +
                    "p1.CONTROL_NUM CONTROL_NUM1, " +
                    "p2.CONTROL_NUM CONTROL_NUM2, " +
                    "hr.FROM_DATE, " +
                    "hr.UPDATE_DATE THRU_DATE,  " +
                    "RELATIONSHIP_TYPE_CODE " +
                ProjectConstants.FROM_FRAGMENT +
                    " HISTORY_RELATIONSHIP hr " +
                "JOIN PAX p1 ON " +
                    "hr.PAX_ID_1 = p1.PAX_ID " +
                "JOIN PAX p2 ON " +
                    "hr.PAX_ID_2 = p2.PAX_ID " +
                "WHERE  " +
                    "RELATIONSHIP_TYPE_CODE = 'REPORT_TO'  " +
                "UNION ALL " +
                "SELECT  " +
                    "p1.PAX_ID, " +
                    "p1.CONTROL_NUM CONTROL_NUM1, " +
                    "p2.CONTROL_NUM CONTROL_NUM2, " +
                    "r.FROM_DATE , " +
                    "r.THRU_DATE, " +
                    "RELATIONSHIP_TYPE_CODE " +
                ProjectConstants.FROM_FRAGMENT +
                    " RELATIONSHIP r " +
                "JOIN PAX p1 ON " +
                    "r.PAX_ID_1 = p1.PAX_ID " +
                "JOIN PAX p2 ON " +
                    "r.PAX_ID_2 = p2.PAX_ID " +
                "WHERE  " +
                    "RELATIONSHIP_TYPE_CODE = 'REPORT_TO'  " +
                ") res " +
                "JOIN component.PAX p on " +
                    "p.PAX_ID = res.PAX_ID " +
                ProjectConstants.WHERE_FRAGMENT +
                " res.CONTROL_NUM1 = :CONTROL_NUMBER  ";

    private static final String HIERARCHY_HISTORY_ORDER_BY = "res.FROM_DATE DESC";

    @Inject
    private RoleDao roleDao;

    @Override
    public EmployeeDTO getEmployeeDTO(Long paxId) {
        return (EmployeeDTO) Iterables.getFirst(getInfo(Arrays.asList(paxId), null), null);
    }

    @Override
    public List<EntityDTO> getInfo(List<Long> paxIds, List<Long> groupIds) {

        // check this first
        if (CollectionUtils.isEmpty(paxIds) && CollectionUtils.isEmpty(groupIds)) {
            return new ArrayList<EntityDTO>(); // no results
        }

        MapSqlParameterSource params = new MapSqlParameterSource();

        // build out where clause
        StringBuilder clauseBuilder = new StringBuilder();
        if (!CollectionUtils.isEmpty(paxIds)) {
            clauseBuilder.append(PAX_WHERE_CLAUSE);
            //TODO: Don't pass params as comma separated string https://maritz.atlassian.net/browse/CNXT-540
            params.addValue(PAX_ID_LIST_SQL_PARAM, org.apache.commons.lang3.StringUtils.join(paxIds, COMMA_DELIM));
        }

        if (!CollectionUtils.isEmpty(groupIds)) {
            if (clauseBuilder.length() > 0) {
                clauseBuilder.append(CLAUSE_COMBINER);
            }
            clauseBuilder.append(GROUP_WHERE_CLAUSE);
            //TODO: Don't pass params as comma separated string https://maritz.atlassian.net/browse/CNXT-540
            params.addValue(GROUP_ID_LIST_SQL_PARAM, org.apache.commons.lang3.StringUtils.join(groupIds, COMMA_DELIM));
        }

        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(
                        QUERY.replace(WHERE_CLAUSE_PLACEHOLDER, clauseBuilder.toString()),
                        params, new CamelCaseMapRowMapper()
                );
        ArrayList<EntityDTO> resultsList = new ArrayList<EntityDTO>();
        for (Map<String, Object> node : nodes) {
            if (PAX_RESULT_TYPE.equalsIgnoreCase((String) node.get(RS_SEARCH_TYPE_CAMEL_CASE))) {
                resultsList.add(new EmployeeDTO(node, null));
            } else {
                resultsList.add(new GroupDTO(node));
            }
        }
        return resultsList;
    }

    /**
     * @param paxId
     * @return list of groupIds
     * <p>
     * Retrieves a list of group pax belongs too.
     */
    @Override
    public List<Long> getParticipantGroups(Long paxId) {
        String query = GET_GROUP_IDS_FOR_PAX_ID;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, new CamelCaseMapRowMapper());
        if (!nodes.isEmpty()) {
            List<Long> ids = new ArrayList<>();
            for (Map<String, Object> node : nodes) {
                ids.add((Long) node.get(GROUP_ID));
            }
            return ids;
        }

        return new ArrayList<>();
    }

    /**
     * Get participant info, take in a programId which will use to verify if pax is eligible to receive from.
     *
     * @param paxId
     * @param programId
     * @return participant object
     */
    @Override
    public EmployeeDTO getParticipantInfo(Long paxId, Long programId) {
        EmployeeDTO pax = (EmployeeDTO) Iterables.getFirst(getInfo(Arrays.asList(paxId), null), null);

        if (pax != null) {
            pax.setEligibleReceiverInProgram(isPaxEligibleReceiverInProgram(paxId, programId));
        }

        return pax;
    }

    /**
     * Verify if pax is eligible to receive a recognition from program
     *
     * @param paxId
     * @param programId
     * @return
     */
    @Override
    public Boolean isPaxEligibleReceiverInProgram(Long paxId, Long programId) {

        String query = QUERY_PAX_ELIGIBLE_RECEIVER_IN_PROGRAM;

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);

        try {
            // Query returns a value if the program is related to pax.
            namedParameterJdbcTemplate.queryForObject(query, params, Integer.class);
            return true;

        } catch (EmptyResultDataAccessException emptyResultDataException) {

            // There is any relation, so pax is not eligible for receiving from program.
            return false;

        }
    }

    /**
     * Get pax_group_misc info for a list of paxIds
     *
     * @param paxIdList - comma separated list of paxIds to pull info for
     */
    @Override
    public List<Map<String, Object>> getPaxInfo(String paxIdList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        //TODO: Don't pass params as comma separated string https://maritz.atlassian.net/browse/CNXT-540
        params.addValue(PAX_ID_LIST_SQL_PARAM, paxIdList);

        return namedParameterJdbcTemplate.query(PAX_INFO_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getGroupsByParticipant(Long loggedInPaxId, String statusList, String typeList, Integer pageNumber, Integer pageSize) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(STATUS_LIST_PARAM, StringUtils.parseDelimitedData(statusList));
        params.addValue(TYPE_LIST_PARAM, StringUtils.parseDelimitedData(typeList));

        return getPageWithTotalRows(GROUPS_BY_PARTICIPANT_QUERY, params, new CamelCaseMapRowMapper(), GROUPS_BY_PARTICIPANT_ORDER_BY, pageNumber, pageSize);
    }

    @Override
    public ParticipantDTO getParticipantOverview(String controlNumber) {
        DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern(ISO_8601_DATE_ONLY_FORMAT);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(CONTROL_NUMBER_PARAM, controlNumber);

        Map<String, Object> paxInfo = namedParameterJdbcTemplate.queryForObject(PARTICIPANT_OVERVIEW_QUERY, params, new CamelCaseMapRowMapper());
        List<RoleDTO> roleDTOList = roleDao.getRolesForControlNumber(controlNumber);
        List<com.maritz.culturenext.dto.GroupDTO> groupDTOList =  namedParameterJdbcTemplate.query(PARTICIPANT_OVERVIEW_GROUPS_QUERY,
                params, (rs, rowNum) ->
                        new com.maritz.culturenext.dto.GroupDTO(
                                rs.getLong(ID),
                                //TODO: Fix Name column values stored in Description column https://maritz.atlassian.net/browse/CNXT-539
                                //  "DESCRIPTION" column is used because values in column "NAME" are null.
                                rs.getString(DESCRIPTION)
                        ));

        return new ParticipantDTO()
                .setControlNumber((String) paxInfo.get(CONTROL_NUM))
                .setFirstName((String) paxInfo.get(FIRST_NAME))
                .setLastName((String) paxInfo.get(LAST_NAME))
                .setUsername((String) paxInfo.get(SYS_USER_NAME))
                .setStatus((String) paxInfo.get(STATUS_TYPE_CODE))
                .setEmailAddress((String) paxInfo.get(EMAIL_ADDRESS))
                .setManagerControlNumber((String) paxInfo.get(MANAGER_CONTROL_NUMBER))
                .setJobTitle((String) paxInfo.get(JOB_TITLE))
                .setCompanyName((String) paxInfo.get(COMPANY_NAME))
                .setHireDate(paxInfo.get(HIRE_DATE) != null ?
                        LocalDate.parse((String) paxInfo.get(HIRE_DATE), dateformatter) : null)
                .setCostCenter((String) paxInfo.get(COST_CENTER))
                .setDepartment((String) paxInfo.get(DEPARTMENT))
                .setFunction((String) paxInfo.get(FUNCTION))
                .setGrade((String) paxInfo.get(GRADE))
                .setArea((String) paxInfo.get(AREA))
                .setPhoneNumber((String) paxInfo.get(PHONE_NUMBER))
                .setCustomFields(getCustomFields(params))
                .setAddress(getAddress(paxInfo))
                .setRoles(roleDTOList)
                .setGroups(groupDTOList);
    }

    @Override
    public List<EnrollmentHistoryDTO> getParticipantEnrollmentHistory(String controlNumber) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(CONTROL_NUMBER_PARAM, controlNumber);

        return namedParameterJdbcTemplate.query(PARTICIPANT_ENROLLMENT_HISTORY_QUERY,
                // TODO: Inconsistency on how dates are saved: https://maritz.atlassian.net/browse/CNXT-543
                params,
                (rs, rowNum) ->
                        new EnrollmentHistoryDTO(
                                rs.getTimestamp(CHANGE_DATE).toLocalDateTime().atZone(ZoneId.systemDefault()),
                                rs.getTimestamp(FROM_DATE).toLocalDateTime().atZone(ZoneId.systemDefault()),
                                rs.getObject(THRU_DATE) != null ? rs.getTimestamp(THRU_DATE).toLocalDateTime().atZone(
                                        ZoneId.systemDefault()) :
                                        rs.getTimestamp(CHANGE_DATE).toLocalDateTime().atZone(ZoneId.systemDefault()),
                                rs.getString(STATUS)
                        )
        );
    }

    @Override
    public List<Map<String, Object>>  getHierarchyHistory(String controlNumber, Integer pageNumber, Integer pageSize) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(CONTROL_NUMBER_PARAM, controlNumber);

        return getPageWithTotalRows(HIERARCHY_HISTORY_QUERY, params, new CamelCaseMapRowMapper(), HIERARCHY_HISTORY_ORDER_BY, pageNumber, pageSize);
    }

    private AddressDTO getAddress(Map<String, Object> result) {
        List<String> addressLines = new ArrayList<>();

        if (result.get(ID) != null) addressLines.add((String) result.get(ID));
        if (result.get(ADDRESS1) != null) addressLines.add((String) result.get(ADDRESS1));
        if (result.get(ADDRESS2) != null) addressLines.add((String) result.get(ADDRESS2));
        if (result.get(ADDRESS3) != null) addressLines.add((String) result.get(ADDRESS3));
        if (result.get(ADDRESS4) != null) addressLines.add((String) result.get(ADDRESS4));
        if (result.get(ADDRESS5) != null) addressLines.add((String) result.get(ADDRESS5));
        if (result.get(ADDRESS6) != null) addressLines.add((String) result.get(ADDRESS6));

        return new AddressDTO()
                .setCity((String) result.get(CITY))
                .setCountryCode((String) result.get(COUNTRY_CODE))
                .setState((String) result.get(STATE))
                .setPostalCode((String) result.get(POSTAL_CODE))
                .setAddressLines(addressLines);
    }

    private List <String> getCustomFields(MapSqlParameterSource params) {
        List<Map<String, Object>> customFields = namedParameterJdbcTemplate.query(PARTICIPANT_OVERVIEW_CUSTOM_FIELDS_QUERY,
            params, new CamelCaseMapRowMapper());

        List<String> resultsList = new ArrayList<>();
        for (int i=0; i < MAX_CUSTOM_FIELD; i++)
            resultsList.add(null);

        customFields.forEach(n -> {
            try {
                resultsList.set(((Integer) n.get(CUSTOM_FIELD_NUMBER)) - 1, (String) n.get(MISC_DATA));
            } catch (IndexOutOfBoundsException e) {
                log.error("Wrong custom field number: {} max expected is: {}", (Integer) (n.get(CUSTOM_FIELD_NUMBER)),
                    MAX_CUSTOM_FIELD);
            }
        });

        return resultsList;
    }

    @Override
    public List<Map<String, Object>> emailMessageSearch(Integer pageNumber, Integer pageSize, String searchString) {

        if (searchString == null) {
            searchString = ProjectConstants.EMPTY_STRING;
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(SEARCH_STRING_SQL_PARAM, searchString);

        List<Map<String, Object>> result = null;
        String queryToExecute = EMAIL_HISTORY_MESSAGE_SEARCH;
        if (log.isInfoEnabled()) {
            log.info("queryToExecute :" + queryToExecute);
        }
        final long startTime = System.nanoTime();
        result = namedParameterJdbcTemplate.query(queryToExecute, params, new CamelCaseMapRowMapper());
        final long endTime = System.nanoTime();
        if (log.isInfoEnabled()) {
            log.info("Total execution time:" + (endTime - startTime) / 1000000);
        }
        return result;
    }
}