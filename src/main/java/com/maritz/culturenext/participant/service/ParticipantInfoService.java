package com.maritz.culturenext.participant.service;

import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;

import java.util.List;
import java.util.Set;

public interface ParticipantInfoService {

    /**
     * Gets a list of groups that a participant is part of
     *
     * @param requestDetails - object for PaginatedResponseObject containing details of request
     * @param pathPaxId - currently logged in pax id
     * @param statusList - list of statuses as a comma delimited string
     * @param typeList - list of types as a comma delimited string
     * @param pageNumber - page number for pagination
     * @param pageSize - page size for pagination
     * @return PaginatedResponseObject&lt;GroupDTO&gt; - paginated list of GroupDTO that the participant is part of
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/98627485/Groups+by+participant
     */
    PaginatedResponseObject<GroupDTO> getGroupsByParticipant(PaginationRequestDetails requestDetails, Long pathPaxId,
                                                             String statusList, String typeList, Integer pageNumber, Integer pageSize);

    /**
     * Gets the participant overview information
     *
     * @param controlNumber - Control number of the participant to retrieve the overview
     * @return ParticipantDTO - Participant overview
     */
    ParticipantDTO getParticipant(String controlNumber);

    /**
     * Returns a page of group data that match the provided params.
     *
     * @param requestDetails - object for PaginatedResponseObject containing details of request
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param searchString - Text string
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847557/Groups+Search+Example
     */
    PaginatedResponseObject<EmailDTO> emailMessageSearch(PaginationRequestDetails requestDetails,
                                                         Integer pageNumber, Integer pageSize, String searchString);

    List<EnrollmentHistoryDTO> getParticipantEnrollmentHistory(String controlNumber);

    PaginatedResponseObject<HierarchyHistoryDTO> getHierarchyHistory(String controlNumber, PaginationRequestDetails requestDetails,
                                                                     Integer pageNumber, Integer pageSize);

    void changeParticipantRoles(String controlNumber, Set<Long> roleIds);

    /**
     * Update an participant ADDRESS record
     * @param controlNum
     * @param addressId
     */

    AddressDTO updateParticipantAddress (String controlNum, Long addressId, String requestType, AddressDTO addressDTO);
}
