package com.maritz.culturenext.participant.service.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.rest.NotFoundException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.participant.service.ParticipantInfoService;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.role.services.RoleService;
import com.maritz.culturenext.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static com.maritz.culturenext.constants.RestParameterConstants.CONTROL_NUMBER_REST_PARAM;

@Slf4j
@Component
public class ParticipantInfoServiceImpl implements ParticipantInfoService {

    private static final String DESIRED_ROLE_IDS = "DesiredRoleIds";
    private static final String ILLEGAL_ROLE = "ILLEGAL_ROLE";
    private static final String PAX = "PAX";
    private static final String ADDING_ILLEGAL_NEW_ROLE = "Adding illegal new role: ";
    private static final String REMOVING_ILLEGAL_ROLE = "Removing illegal role: ";
    private static final String CONTROL_NUM = "controlNum";
    private static final String FROM_DATE = "fromDate";
    private static final String THRU_DATE = "thruDate";
    private static final String CONTROL_NUMBER = "controlNumber";
    private static final String ACL_FIELD_ROLE_ID = "roleId";
    private static final String ACL_FIELD_SUBJECT_TABLE = "subjectTable";
    private static final String ACL_FIELD_SUBJECT_ID = "subjectId";


    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ConcentrixDao<Acl> aclDao;
    @Inject private ConcentrixDao<Pax> paxDao;

    @Inject
    private PaxRepository paxRepository;
    @Inject
    private AddressRepository addressRepository;
    @Inject
    private ConcentrixDao<Address> addressDao;
    @Inject
    private RoleService roleService;

    @Override
    public PaginatedResponseObject<GroupDTO> getGroupsByParticipant(
            PaginationRequestDetails requestDetails, Long pathPaxId, String statusList, String typeList, Integer pageNumber, Integer pageSize) {

        Integer totalResults = 0;

        List<Map<String, Object>> resultList = participantInfoDao.getGroupsByParticipant(pathPaxId, statusList, typeList, pageNumber, pageSize);

        List<GroupDTO> groupsByParticipantDTOList = new ArrayList<>();

        for (Map<String, Object> result : resultList) {

            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            GroupDTO rowData = new GroupDTO();

            rowData.setGroupId((Long) result.get(ProjectConstants.GROUP_ID));
            rowData.setGroupConfigId((Long) result.get(ProjectConstants.GROUP_CONFIG_ID));
            rowData.setField((String) result.get(ProjectConstants.FIELD));
            rowData.setFieldDisplayName((String) result.get(ProjectConstants.FIELD_DISPLAY_NAME));
            rowData.setGroupName((String) result.get(ProjectConstants.GROUP_NAME));
            rowData.setType((String) result.get(ProjectConstants.TYPE));
            rowData.setVisibility((String) result.get(ProjectConstants.VISIBILITY));
            rowData.setStatus((String) result.get(ProjectConstants.STATUS));
            rowData.setGroupPaxId((Long) result.get(ProjectConstants.GROUP_PAX_ID));
            rowData.setCreateDate(DateUtil.convertToUTCDateTime((Date) result.get(ProjectConstants.CREATE_DATE)));
            rowData.setPaxCount((Integer) result.get(ProjectConstants.PAX_COUNT));
            rowData.setGroupCount((Integer) result.get(ProjectConstants.GROUP_COUNT));
            rowData.setTotalPaxCount((Integer) result.get(ProjectConstants.TOTAL_PAX_COUNT));

            groupsByParticipantDTOList.add(rowData);
        }

        return new PaginatedResponseObject<>(requestDetails, groupsByParticipantDTOList, totalResults);
    }

    @Override
    public ParticipantDTO getParticipant(String controlNumber) {
        try {
            return participantInfoDao.getParticipantOverview(controlNumber);
        } catch (EmptyResultDataAccessException e) {
            log.error("No participant found with Control Number: {}", controlNumber);
            throw new NotFoundException(CONTROL_NUMBER_REST_PARAM, e);
        }
    }

    @Override
    public List<EnrollmentHistoryDTO> getParticipantEnrollmentHistory(String controlNumber) {

        return participantInfoDao.getParticipantEnrollmentHistory(controlNumber);
    }

    @Override
    public PaginatedResponseObject<EmailDTO> emailMessageSearch(PaginationRequestDetails requestDetails, Integer pageNumber,
                                                                Integer pageSize, String searchString) {

        return handleEmailMessageSearch(requestDetails, participantInfoDao.emailMessageSearch(pageNumber, pageSize, searchString));
    }

    /**
     * Converts group / pax records into paginated response.
     *
     * @param requestDetails   request details from REST layer
     * @param searchResultList search results from DAO layer
     * @return PaginatedResponseObject containing object version of data map.
     */
    private PaginatedResponseObject<EmailDTO> handleEmailMessageSearch(PaginationRequestDetails requestDetails,
                                                                       List<Map<String, Object>> searchResultList) {

        List<EmailDTO> resultsList = new ArrayList<>();
        Integer totalResults = 0;

        for (Map<String, Object> searchResult : searchResultList) {
            totalResults = (Integer) searchResult.get(ProjectConstants.TOTAL_RESULTS_KEY);
            resultsList.add((new EmailDTO(searchResult)));
        }

        return new PaginatedResponseObject<>(requestDetails, resultsList, totalResults);
    }

    @Override
    public PaginatedResponseObject<HierarchyHistoryDTO> getHierarchyHistory(String controlNumber, PaginationRequestDetails requestDetails,
                                                                            Integer pageNumber, Integer pageSize) {

        Integer totalResults = 0;
        List<Map<String, Object>> resultList = participantInfoDao.getHierarchyHistory(controlNumber, pageNumber, pageSize);
        List<HierarchyHistoryDTO> hierarchyHistoryDTOList = new ArrayList<>();

        for (Map<String, Object> result : resultList) {
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            Date fromDate = (Date) result.get(FROM_DATE);
            Date thruDate = (Date) result.get(THRU_DATE);
            HierarchyHistoryDTO rowData = new HierarchyHistoryDTO()
                    .setControlNumber((String) result.get(CONTROL_NUMBER))
                    .setFromDate(ZonedDateTime.of(LocalDateTime.ofInstant(fromDate.toInstant(), ZoneId.systemDefault()), ZoneId.systemDefault()))
                    .setThruDate(thruDate != null ?
                            ZonedDateTime.of(LocalDateTime.ofInstant(thruDate.toInstant(), ZoneId.systemDefault()), ZoneId.systemDefault()) : null);

            hierarchyHistoryDTOList.add(rowData);
        }
        return new PaginatedResponseObject<>(requestDetails, hierarchyHistoryDTOList, totalResults);
    }

    @Override
    public void changeParticipantRoles(String controlNumber, Set<Long> desiredRoleIds) throws ErrorMessageException {
        Set<Long> administrableRoleIds = roleService.getAdministrableRoleIds();
        Set<Long> currentRoleIds = roleService.getRolesForControlNumber(controlNumber).stream().map(RoleDTO::getId).collect(Collectors.toSet());

        Set<Long> addedRoleIds = difference(desiredRoleIds, currentRoleIds);
        Set<Long> removedRoleIds = difference(currentRoleIds, desiredRoleIds);

        Set<Long> illegalAddedRoleIds = difference(addedRoleIds, administrableRoleIds);
        Set<Long> illegalRemovedRoleIds = difference(removedRoleIds, administrableRoleIds);

        if (!illegalAddedRoleIds.isEmpty()){
            throw new ErrorMessageException().addErrorMessage(DESIRED_ROLE_IDS,
                    ILLEGAL_ROLE,
                    ADDING_ILLEGAL_NEW_ROLE +illegalAddedRoleIds);
        }

        if (!illegalRemovedRoleIds.isEmpty()){
            throw new ErrorMessageException().addErrorMessage(DESIRED_ROLE_IDS,
                    ILLEGAL_ROLE,
                    REMOVING_ILLEGAL_ROLE +illegalRemovedRoleIds);
        }
        Pax paxData = paxDao.findBy().where(CONTROL_NUM).eq(controlNumber).findOne();
        assert paxData != null;
        for( Long roleId : addedRoleIds) {
            Acl acl = new Acl();
            acl.setRoleId(roleId);
            acl.setSubjectTable(PAX);
            acl.setSubjectId(paxData.getPaxId());
            aclDao.save(acl);
        }
        for( Long roleId : removedRoleIds) {
            Acl acl = aclDao.findBy()
                    .where(ACL_FIELD_ROLE_ID).eq(roleId)
                    .and(ACL_FIELD_SUBJECT_TABLE).eq(PAX)
                    .and(ACL_FIELD_SUBJECT_ID).eq(paxData.getPaxId())
                    .findOne();
            aclDao.delete(acl);
        }
    }

    protected  <T> Set<T> difference(Set<T> one, Set<T> two) {
        Set<T> difference = new HashSet<>(one);
        difference.removeAll(two);
        return difference;
    }

    public AddressDTO updateParticipantAddress(String controlNum, Long addressId, String requestType, AddressDTO addressDTO) {

        Pax pax = paxRepository.findByControlNum(addressDTO.getControlNumber());

        if (pax == null) throw new NotFoundException("controlNum not found");

        Address address = addressRepository.findOne(addressDTO.getAddressId());
        if (address == null) throw new NotFoundException("address");

        if (!address.getPaxId().equals(pax.getPaxId())) throw new NotFoundException("address");

        if (!addressDTO.getAddressLines().isEmpty()) {
            address.setAddress1(addressDTO.getAddressLines().get(0));
        }
        if (addressDTO.getAddressLines().size() > 1) {
            address.setAddress2(addressDTO.getAddressLines().get(1));
        }
        if (addressDTO.getAddressLines().size() > 2) {
            address.setAddress3(addressDTO.getAddressLines().get(2));
        }
        if (addressDTO.getAddressLines().size() > 3) {
            address.setAddress4(addressDTO.getAddressLines().get(3));
        }
        if (addressDTO.getAddressLines().size() > 4) {
            address.setAddress5(addressDTO.getAddressLines().get(4));
        }
        if (addressDTO.getAddressLines().size() > 5) {
            address.setAddress6(addressDTO.getAddressLines().get(5));
        }
        address.setCity(addressDTO.getCity());
        address.setState(addressDTO.getState());
        address.setZip(addressDTO.getPostalCode());
        address.setCountryCode(addressDTO.getCountryCode());

        addressRepository.save(address);

        ArrayList<String> addressLines = new ArrayList<>(addressDTO.getAddressLines());

        return new AddressDTO()
                .setAddressId(address.getAddressId())
                .setAddressLines(addressLines)
                .setCity(address.getCity())
                .setState(address.getState())
                .setPostalCode(address.getZip())
                .setCountryCode(address.getCountryCode())
                ;
    }
}

