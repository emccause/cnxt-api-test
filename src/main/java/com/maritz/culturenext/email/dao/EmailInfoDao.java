package com.maritz.culturenext.email.dao;

import java.util.List;
import java.util.Map;

public interface EmailInfoDao {

    List<Map<String, Object>> getRecognitionReceivedRecipientEmailInfo(Long nominationId);

    List<Map<String, Object>> getRecognitionReceivedManagerEmailInfo(Long nominationId);

    List<Map<String, Object>> getNotifyOtherEmailInfo(Long nominationId);

    Map<String, Object> getEmailRecord(String emailAddress);

    List<Map<String, Object>> getPointLoadEmailInfo(Long batchId);
    
    void releaseNominationEmails(Long payoutId);

}
