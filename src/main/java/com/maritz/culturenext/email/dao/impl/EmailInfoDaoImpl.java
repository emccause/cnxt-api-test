package com.maritz.culturenext.email.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.ListUtils;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.email.dao.EmailInfoDao;

@Repository
public class EmailInfoDaoImpl extends AbstractDaoImpl implements EmailInfoDao {

    private static final String RECIPIENT_EMAIL_INFO_QUERY = "EXEC component.UP_GET_RECOGNITION_EMAIL_RECIPIENT_INFO :nominationId";

    private static final String MANAGER_EMAIL_INFO_QUERY = "EXEC component.UP_GET_RECOGNITION_EMAIL_MANAGER_INFO :nominationId";

    private static final String NOTIFY_OTHER_INFO_QUERY = "EXEC component.UP_GET_NOTIFY_OTHER_EMAIL_INFO :nominationId";

    private static final String SQL_RELEASE_NOMINATION_EMAILS = "exec component.UP_RELEASE_NOMINATION_EMAILS :payoutId";

    private static final String FIND_EMAIL_QUERY =
            "SELECT TOP 1 e.* " +
            "FROM component.EMAIL e " +
            "JOIN component.SYS_USER su ON su.PAX_ID = e.PAX_ID AND su.STATUS_TYPE_CODE <> 'INACTIVE' " +
            "WHERE e.PREFERRED = 'Y' AND e.EMAIL_ADDRESS = :" + ProjectConstants.EMAIL_ADDRESS;

    private static final String POINT_LOAD_EMAIL_QUERY =
            "SELECT pg.PAX_ID, p.PROGRAM_NAME, CONVERT(DECIMAL(10,0), d.AMOUNT)AS 'AMOUNT', d.DESCRIPTION AS 'HEADLINE', thm.MISC_DATA AS 'PAYOUT_TYPE' " +
            "FROM component.TRANSACTION_HEADER th " +
            "LEFT JOIN component.TRANSACTION_HEADER_MISC thm ON thm.TRANSACTION_HEADER_ID = th.ID AND thm.VF_NAME = 'PAYOUT_TYPE' " +
            "JOIN component.PAX_GROUP pg ON pg.PAX_GROUP_ID = th.PAX_GROUP_ID " +
            "JOIN component.PROGRAM p ON p.PROGRAM_ID = th.PROGRAM_ID " +
            "JOIN component.PROGRAM_MISC PM_NR ON PM_NR.PROGRAM_ID = TH.PROGRAM_ID AND PM_NR.VF_NAME = 'NOTIFICATION_RECIPIENT' " +
            "JOIN component.DISCRETIONARY d ON d.TRANSACTION_HEADER_ID = th.ID " +
            "WHERE th.STATUS_TYPE_CODE = 'APPROVED' AND th.BATCH_ID = :batchId " +
            "AND PM_NR.MISC_DATA = 'TRUE'";

    @Override
    public List<Map<String, Object>> getRecognitionReceivedRecipientEmailInfo(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.NOMINATION_ID, nominationId);

        return namedParameterJdbcTemplate.query(RECIPIENT_EMAIL_INFO_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getRecognitionReceivedManagerEmailInfo(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.NOMINATION_ID, nominationId);

        return namedParameterJdbcTemplate.query(MANAGER_EMAIL_INFO_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getNotifyOtherEmailInfo(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.NOMINATION_ID, nominationId);

        return namedParameterJdbcTemplate.query(NOTIFY_OTHER_INFO_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public Map<String, Object> getEmailRecord(String emailAddress) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.EMAIL_ADDRESS, emailAddress);

        List<Map<String, Object>> nodes =
                namedParameterJdbcTemplate.query(FIND_EMAIL_QUERY, params, new CamelCaseMapRowMapper());

        if (!ListUtils.isEmpty(nodes)) {
            return nodes.get(0);
        }

        return null;
    }

    @Override
    public List<Map<String, Object>> getPointLoadEmailInfo(Long batchId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.BATCH_ID, batchId);

        return namedParameterJdbcTemplate.query(POINT_LOAD_EMAIL_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    @Transactional
    public void releaseNominationEmails(Long payoutId) {
        namedParameterJdbcTemplate.update(
            SQL_RELEASE_NOMINATION_EMAILS
            ,new MapSqlParameterSource()
                .addValue(ProjectConstants.PAYOUT_ID, payoutId)
        );
    }
}
