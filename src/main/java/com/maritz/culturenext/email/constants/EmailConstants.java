package com.maritz.culturenext.email.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public final class EmailConstants {

    //Pulled these from maritz-notification 4.1.1
    //Not sure what they are used for but they are needed for PasswordEventListener
    public static final String RESET_PASSWORD_EVENT = "RESET_PASSWORD";
    public static final String CHANGE_PASSWORD_EVENT = "CHANGE_PASSWORD";
    public static final String PASSWORD_RESET_TOKEN_EVENT = "RSPWDN";

    //REST URI's for ecard image and client logo
    public static final String ECARD_URI = "/rest/projects/~/ecards/";
    public static final String LOGO_URI = "/rest/projects/~/images/logo";
    public static final String NOTIFICATIONS_URI = "/#/notifications/";
    public static final String SETTINGS_URI = "/profile-settings#/";


    public static final String HTML_FILE_TYPE = ".html";
    public static final String PASSWORD_RESET_NOTIFICATION_TEMPLATE_NAME = "password-reset-notification";
    public static final String PASSWORD_RESET_CONFIRMATION_TEMPLATE_NAME = "password-reset-confirmation";
    public static final String POINT_LOAD_TEMPLATE_NAME = "point-upload-notification";
    public static final String RECOGNITION_APPROVAL_TEMPLATE_NAME = "recognition-or-approval-notification";
    public static final String MILESTONE_REMINDER_TEMPLATE_NAME = "milestone-reminder";

    public static final String TO_RECEIVE_EMAIL_APPR = "toReceiveEmailAppr";
    public static final String TO_RECEIVE_EMAIL_REC = "toReceiveEmailRec";
    public static final String TO_RECEIVE_EMAIL_MGR_REC = "toReceiveEmailMgrRec";
    public static final String TO_RECEIVE_EMAIL_NOTIFY = "toReceiveEmailNotifyOthers";

    public static final String ERROR_EMAIL_NOT_PROCESSED = "EMAIL_NOT_PROCESSED";
    public static final String ERROR_EMAIL_NOT_PROCESSED_MSG = "Some number of emails failed to process";

    //Error Messages
    public static final ErrorMessage EMAIL_NOT_PROCESSED = new ErrorMessage()
            .setCode(ERROR_EMAIL_NOT_PROCESSED)
            .setField(ProjectConstants.EMAIL)
            .setMessage(ERROR_EMAIL_NOT_PROCESSED_MSG);

    /*
     * The caller references the constants using EmailConstants.EMAIL_BLAST_EVENT,
     * and so on. Thus, the caller should be prevented from constructing objects of
     * this class, by declaring this private constructor.
     */
    private EmailConstants() {
        //this prevents even the native class from
        //calling this constructor as well
        throw new AssertionError();
    }
}