package com.maritz.culturenext.email.event;

import org.springframework.context.ApplicationEvent;

public class RecognitionEvent extends ApplicationEvent {
        
    //Event to be used to send emails to every pax associated to a recognition or approval (specific NominationID)

    private Long nominationId;
    
    public RecognitionEvent(String eventType, Long nominationId) {
        super(eventType);

        this.nominationId = nominationId;
    }
    
    public String getEventType() {
        return (String) this.getSource();
    }
    
    public Long getNominationId() {
        return nominationId;
    }
}
