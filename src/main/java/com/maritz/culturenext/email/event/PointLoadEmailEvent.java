package com.maritz.culturenext.email.event;

import org.springframework.context.ApplicationEvent;

public class PointLoadEmailEvent extends ApplicationEvent {
    
    //Email event to be used to send emails to every pax associated to a point load (specific batchID)
    
    private Long batchId;    

    public PointLoadEmailEvent(String eventType, Long batchId) {
        super(eventType);
        
        this.batchId = batchId;
    }
    
    public String getEventType() {
        return (String) this.getSource();
    }
    
    public Long getBatchId() {
        return batchId;
    }
}