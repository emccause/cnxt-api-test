package com.maritz.culturenext.email.event;

import org.springframework.context.ApplicationEvent;

public class EmailEvent extends ApplicationEvent {
    
    //Generic email event to be used when only a paxId is needed
    
    private Long paxId;    

    public EmailEvent(String eventType, Long paxId) {
        super(eventType);
        
        this.paxId = paxId;
    }
    
    public String getEventType() {
        return (String) this.getSource();
    }
    
    public Long getPaxId() {
        return paxId;
    }
}