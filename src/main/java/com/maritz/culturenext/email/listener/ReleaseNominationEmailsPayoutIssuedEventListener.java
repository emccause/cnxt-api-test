package com.maritz.culturenext.email.listener;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.maritz.core.event.PayoutIssuedEvent;
import com.maritz.culturenext.email.dao.EmailInfoDao;

@Component
public class ReleaseNominationEmailsPayoutIssuedEventListener {

    private final EmailInfoDao emailInfoDao;

    public ReleaseNominationEmailsPayoutIssuedEventListener(
        EmailInfoDao emailInfoDao
    ) {
        this.emailInfoDao = emailInfoDao;
    }

    @EventListener
    public void handlePayoutIssued(PayoutIssuedEvent event) {
        //release the pending emails for the nomination / recognitions associated with the given payout id
        //if all payouts for the nomination / recognition are marked as issued.
        emailInfoDao.releaseNominationEmails(event.getPayout().getPayoutId());
    }

}
