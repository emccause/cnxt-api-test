package com.maritz.culturenext.email.listener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.program.ecard.services.EcardService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import com.google.common.collect.Iterables;
import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.dto.TargetDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationMisc;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.util.DateUtils;
import com.maritz.core.util.ObjectUtils;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.email.constants.EmailConstants;
import com.maritz.culturenext.email.dao.EmailInfoDao;
import com.maritz.culturenext.email.dto.EmailInfoDTO;
import com.maritz.culturenext.email.event.EmailEvent;
import com.maritz.culturenext.email.event.PointLoadEmailEvent;
import com.maritz.culturenext.email.event.RecognitionEvent;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.notification.service.NotificationRequest;
import com.maritz.notification.service.NotificationService;

import static com.maritz.culturenext.images.constants.FileImageTypes.PROJECT_LOGO;

/**
 * Listens for any events that generate an email to be sent. Current supported events are:
 * APR - Recognition Pending Approval (recognition-or-approval-received.html)
 * CHANGE_PASSWORD - Confirmation that your password has been changed (password-reset-confirmation.html)
 * RESET_PASSWORD - Confirmation that your password has been changed (password-reset-confirmation.html)
 * RECGPX - Notifying a participant that they have been recognized (recognition-or-approval-received.html)
 * RECGMN - Notifying a manager that their direct report has been recognized (recognition-or-approval-received.html)
 * RSPWDN - Email containing a link for a user to reset their password (password-reset-notification.html)
 * PNTDPT - Notifying a user they have received points via a point load (point-upload-notification.html)
 * NOTIFY_OTHER - Notifying a user that they have been copied on a recognition (recognition-or-approval-received.html)
 * MILESTONE_SERVICE_ANNIVERSARY_REMINDER - Notifying a manager that their direct report has an upcoming milestone
 */
@Component
public class EmailEventListener {
	private final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final Date FAR_FUTURE_DATE = DateUtils.parseDate("12/31/9999");

    private static final String EMAIL_URL_SIGNING_DURATION_PROPERTY = "url.signing.duration.email";
    private static final long ONE_HUNDRED_YEARS_IN_SECONDS = 100 * 365 * 24 * 60 * 60L;
    private static final String LOGO_IMAGE = "logo image";
    private static final String ECARD_IMAGE = "e-card image";

    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<ApplicationData> applicationDataDao;
    @Inject private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<NominationMisc> nominationMiscDao;
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private EmailInfoDao emailInfoDao;
    @Inject private Environment environment;
    @Inject private NotificationService notificationService;
    @Inject private PaxRepository paxRepository;
    @Inject private RecognitionService recognitionService;
    @Inject private TranslationUtil translationUtil;
    @Inject private AwardTypeService awardTypeService;
    @Inject private BatchService batchService;
    private EnvironmentUtil environmentUtil;
    private FileImageService fileImageService;
    private EcardService eCardService;
    private GcpUtil gcpUtil;

    @Inject
    public EmailEventListener setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    @Inject
    public EmailEventListener setFileImageService(FileImageService fileImageService) {
        this.fileImageService = fileImageService;
        return this;
    }

    @Inject
    public EmailEventListener setECardService(EcardService eCardService) {
        this.eCardService = eCardService;
        return this;
    }

    @Inject
    public EmailEventListener setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void handleSendEmailEvent(EmailEvent emailEvent) {
        logger.info("Sending email notification async event has started.");

        NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), emailEvent.getPaxId(), null);

        notificationService.sendNotification(notificationRequest);

        logger.info("Sending email notification async event has finished.");
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void handlePointLoadEmailEvent(PointLoadEmailEvent emailEvent) {
        logger.info("Sending point load email notifications async event has started.");

        List<NotificationRequest> notificationRequestList = new ArrayList<>();

        //Need to call the setupEmail method for every pax on the point load file
        List<Map<String, Object>> nodes = emailInfoDao.getPointLoadEmailInfo(emailEvent.getBatchId());

        if(!CollectionUtils.isEmpty(nodes)){
            for (Map<String, Object> node : nodes) {
                NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), (Long) node.get(ProjectConstants.PAX_ID), node);

                //Set REFERENCE_FK on the EMAIL_MESSAGE record to the batchId
                notificationRequest.setReferenceId(emailEvent.getBatchId());
                notificationRequestList.add(notificationRequest);
            }
        }
        notificationService.sendNotification(notificationRequestList);

        logger.info("Sending point load email notifications async event has finished.");
    }

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void handleMilestoneReminderEvent(MilestoneReminderEvent milestoneReminderEmailEvent) {
        Batch batch;
        if (milestoneReminderEmailEvent.getEventType().equals(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode())) {
            batch = batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_REMINDER_EMAIL.name());
        }
        else {
            batch = batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_REMINDER_EMAIL.name());
        }

        int totalRecords = 0;
        int totalErrorRecords = 0;

        logger.info("Sending milestone reminder email notifications async event has started.");

        for (PaxMilestoneReminderDTO paxMilestoneReminderDTO : milestoneReminderEmailEvent.getPaxMilestoneReminderList()) {
            totalRecords++;

            try {
                List<NotificationRequest> notificationRequestList = new ArrayList<>();
                NotificationRequest notificationRequest = setupEmail(milestoneReminderEmailEvent.getEventType(), paxMilestoneReminderDTO.getManagerPaxId(), getMilestoneReminderProgramData(paxMilestoneReminderDTO));
                notificationRequest.setReferenceId(batch.getBatchId());
                notificationRequestList.add(notificationRequest);
                notificationService.sendNotification(notificationRequestList);

            } catch (Exception e) {
                logBatchEvent(batch, BatchEventTypeCode.ERR.name(), e.getMessage(), "PAX", paxMilestoneReminderDTO.getPaxId());
                totalErrorRecords++;
            }
        }
        logBatchEvent(batch, BatchEventTypeCode.RECS.name(), String.valueOf(totalRecords));
        logBatchEvent(batch, BatchEventTypeCode.ERRS.name(), String.valueOf(totalErrorRecords));
        batchService.endBatch(batch);

        logger.info("Sending milestone reminder email notifications async event has finished.");
    }

    /**
     * This method handles published RecognitionEvents,
     * getting data and calling methods to fill out and send the email
     *
     * @param emailEvent - event being handled, in this case a RecognitionEvent
     * @return void
     */

    @Async
    @TransactionalEventListener(fallbackExecution = true)
    public void handleRecognitionEvent(RecognitionEvent emailEvent) {
        logger.info("Sending recognition or approval notification async event has started. Type: {}", emailEvent.getEventType());

        List<NotificationRequest> notificationRequestList = new ArrayList<>();

        //Get all of the email info from the database
        if (emailEvent.getEventType().equals(EventType.RECOGNITION_PARTICIPANT.getCode())) {
            List<Map<String, Object>> recognitionReceivedRecipientEmailInfo =
                    emailInfoDao.getRecognitionReceivedRecipientEmailInfo(emailEvent.getNominationId());
            
            for (Map<String, Object> node : recognitionReceivedRecipientEmailInfo) {
                NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID), node);
                notificationRequest.setReferenceId((Long) node.get(ProjectConstants.RECOGNITION_ID));
                notificationRequestList.add(notificationRequest);

            }
        } 
    	else if (emailEvent.getEventType().equals(EventType.RECOGNITION_MANAGER.getCode())) {
            List<Map<String, Object>> recognitionReceivedManagerEmailInfo =
                    emailInfoDao.getRecognitionReceivedManagerEmailInfo(emailEvent.getNominationId());

            for (Map<String, Object> node : recognitionReceivedManagerEmailInfo) {
                NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID), node);
                notificationRequest.setReferenceId(emailEvent.getNominationId());
                notificationRequestList.add(notificationRequest);
            
            }
        } 
    	else if (emailEvent.getEventType().equals(EventType.APPROVAL.getCode()) || emailEvent.getEventType().equals(EventType.APPROVAL_REMINDER.getCode())) {

            //There is no dao for approval, so we'll have to construct the model piece by piece
            
    		List<RecognitionDetailsDTO> recognitionDetailsDTOList = recognitionService
                    .getRecognitionDetails(null, emailEvent.getNominationId(), true, true, null, null, null, 1, Integer.MAX_VALUE)
                    .getData();
            

            //approverPaxIdMap will be a list of approverPaxIds mapped to how many people on that recognition they are responsible for approving
    		Map<Long, Integer> approverPaxIdMap = new HashMap<>();
            for (RecognitionDetailsDTO recognitionDetailsDTO : recognitionDetailsDTOList) {
                EmployeeDTO recognitionDetailsPaxInfo = recognitionDetailsDTO.getApprovalPax();
                if (recognitionDetailsPaxInfo != null && recognitionDetailsPaxInfo.getPaxId() != null) {
                    if (!approverPaxIdMap.containsKey(recognitionDetailsPaxInfo.getPaxId())) {
                        approverPaxIdMap.put(recognitionDetailsPaxInfo.getPaxId(), 1);
                    } else {
                        approverPaxIdMap.put(recognitionDetailsPaxInfo.getPaxId(),
                                approverPaxIdMap.get(recognitionDetailsPaxInfo.getPaxId()) + 1);
                    }
                }
            }

            //set up the approver email for each approverPaxId in the map
            for (Long key : approverPaxIdMap.keySet()) {

                NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), key, getApprovalReceivedEmailInfo(emailEvent.getNominationId(), key, emailEvent.getEventType()));
                notificationRequest.setReferenceId(emailEvent.getNominationId());
                notificationRequestList.add(notificationRequest);
                
            }
        } 
    	else if (emailEvent.getEventType().equals(EventType.NOTIFY_OTHER.getCode())) {
    		for (Map<String, Object> node : emailInfoDao.getNotifyOtherEmailInfo(emailEvent.getNominationId())) {

                NotificationRequest notificationRequest = setupEmail(emailEvent.getEventType(), (Long) node.get(ProjectConstants.PAX_ID), node);
                //Set REFERENCE_FK on the EMAIL_MESSAGE record to the nominationId
                notificationRequest.setReferenceId(emailEvent.getNominationId());
                notificationRequestList.add(notificationRequest);
            }

        }

        notificationService.sendNotification(notificationRequestList);


        logger.info("Sending recognition or approval notification async event has finished");
    }

    /**
     * Maps all data needed for an email to EmailInfoDTO
     * Then uses FreeMarker to retrieve the localized template and map it's placeholders to the data in that DTO.
     * Returns a NotificationRequest object that will be used by core to create an EMAIL_MESSAGE record
     * @param emailType - EventTypeCode of the email to be set up
     * @param paxId - paxId of the recipient of the email
     * @param node - map of additional data needed for the email
     * @return
     * @throws Exception
     */
    protected NotificationRequest setupEmail(String emailType, Long paxId, Map<String, Object> node) {

        //NotificationRequest object that will be used to send the email
        NotificationRequest notificationRequest = new NotificationRequest();

        EmailInfoDTO emailInfoDto = populateEmailInfoDTO(emailType, paxId, node);

        notificationRequest.setVariables(ObjectUtils.objectToMap(emailInfoDto));

        //Don't populate from and it will use the sending email in APPLICATION_DATA
        notificationRequest.setTo(emailInfoDto.getRecipientPaxId());
        notificationRequest.setSubject(emailInfoDto.getMessageHeader());
        notificationRequest.setEventType(emailType);
        
        Integer amount = emailInfoDto.getPointAmount();
        if (amount != null && amount.intValue() > 0 && StringUtils.isIn(emailType, EventType.RECOGNITION_PARTICIPANT.getCode(), EventType.RECOGNITION_MANAGER.getCode(), EventType.NOTIFY_OTHER.getCode())) {
            //There are points involved so delay sending the email until all associated payouts are marked as ISSUED
            notificationRequest.setSendDate(FAR_FUTURE_DATE);
        }
        else {
            notificationRequest.setSendDate(new Date());
        }

        return notificationRequest;
    }

    protected EmailInfoDTO populateEmailInfoDTO(String emailType, Long paxId, Map<String, Object> node) {
        EmailInfoDTO emailInfoDto = new EmailInfoDTO();

        //Populate the common data for all emails
        emailInfoDto.setEmailType(emailType);

        //Recipient Pax Information
        Pax pax = paxRepository.findOne(paxId);
        emailInfoDto.setRecipientPaxId(pax.getPaxId());
        emailInfoDto.setRecipientNameFirst(pax.getFirstName());
        emailInfoDto.setRecipientNameLast(pax.getLastName());
        emailInfoDto.setRecipientLocaleCode(pax.getPreferredLocale());

        //Client Branding & Links
        emailInfoDto.setPrimaryColor(environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR));
        emailInfoDto.setTextOnPrimaryColor(environment.getProperty(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR));
        String baseUrl = environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL);
        emailInfoDto.setClientUrl(baseUrl);
        emailInfoDto.setClientLogo(getClientLogoUrl(baseUrl));
        emailInfoDto.setSettingsUrl(baseUrl + EmailConstants.SETTINGS_URI);
        Calendar now = Calendar.getInstance();
        emailInfoDto.setCurrentYear(String.valueOf(now.get(Calendar.YEAR)));

        //Translatable Values
        List<String> applicationDataKeys = null;
        List<ApplicationData> applicationDataList = null;

        Number amount = node != null ? (Number)node.get(ProjectConstants.POINT_AMOUNT) : null;
        if (amount == null) amount = Integer.valueOf(0);
        
        //Email-specific data
        EventType eventType = EventType.getTypeFromCodeValue(emailType);
        switch (eventType) {
            case CHANGE_PASSWORD: //Same as RESET_PASSWORD
            case RESET_PASSWORD:  //Your password has changed
                //Translatable Values
                applicationDataKeys = Arrays.asList(
                        ApplicationDataConstants.KEY_NAME_PASSWORD_CONFIRMATION_HEADER);

                //Template
                emailInfoDto.setTemplateName(EmailConstants.PASSWORD_RESET_CONFIRMATION_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);
                break;

            case POINT_LOAD: //You've Received Points!
                //Translatable Values
                applicationDataKeys = Arrays.asList(ApplicationDataConstants.KEY_NAME_POINT_LOAD_HEADER);

                //Template
                emailInfoDto.setTemplateName(EmailConstants.POINT_LOAD_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                emailInfoDto.setPointAmount(((Double) node.get(ProjectConstants.AMOUNT)).intValue());

                String payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                if (payoutType != null) {
                    emailInfoDto.setPayoutType(payoutType);
                    AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
                    emailInfoDto.setDisplayName(awardType.getDisplayName());
                    emailInfoDto.setLongDesc(awardType.getLongDesc());
                    emailInfoDto.setAbs(awardType.getAbs());
                }
                emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                emailInfoDto.setHeadline((String) node.get(ProjectConstants.HEADLINE));

                break;

            case RECOGNITION_PARTICIPANT:
                //Recognition Received - Participant Notification
                //Translatable Values
                applicationDataKeys = Arrays.asList(
                        ApplicationDataConstants.KEY_NAME_RECGPX_HEADER,
                        ApplicationDataConstants.KEY_NAME_RECGPX_BUTTON);

                //Template
                emailInfoDto.setTemplateName(EmailConstants.RECOGNITION_APPROVAL_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                emailInfoDto.setPointAmount(amount.intValue());
                emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                emailInfoDto.setHeadline((String) node.get(ProjectConstants.HEADLINE));
                emailInfoDto.setComment((String) node.get(ProjectConstants.COMMENT));

                payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                if (payoutType != null) {
                    emailInfoDto.setPayoutType(payoutType);
                    AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
                    emailInfoDto.setDisplayName(awardType.getDisplayName());
                    emailInfoDto.setLongDesc(awardType.getLongDesc());
                    emailInfoDto.setAbs(awardType.getAbs());
                }

                if (node.get(ProjectConstants.SUBMITTER_PAX_ID) != null) {
                    emailInfoDto.setSubmitterPaxId((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
                }
                if (node.get(ProjectConstants.SUBMITTER_NAME_FIRST) != null) {
                    emailInfoDto.setSubmitterNameFirst((String) node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
                }
                if (node.get(ProjectConstants.SUBMITTER_NAME_LAST) != null) {
                    emailInfoDto.setSubmitterNameLast((String) node.get(ProjectConstants.SUBMITTER_NAME_LAST));
                }

                emailInfoDto.setEcardUrl(getECardUrl((Long) node.get(ProjectConstants.ECARD_ID), baseUrl));

                //Build notification link
                if (node.get(ProjectConstants.NOTIFICATION_ID) != null)
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI +
                            node.get(ProjectConstants.NOTIFICATION_ID).toString());
                else
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI);

                break;

            case RECOGNITION_MANAGER:
                //Recognition Received - Manager Notification
                //Translatable Values
                applicationDataKeys = Arrays.asList(
                        ApplicationDataConstants.KEY_NAME_RECGMN_HEADER,
                        ApplicationDataConstants.KEY_NAME_RECGMN_BUTTON);

                //Template
                emailInfoDto.setTemplateName(EmailConstants.RECOGNITION_APPROVAL_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                emailInfoDto.setPointAmount(amount.intValue());
                emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                emailInfoDto.setHeadline((String) node.get(ProjectConstants.HEADLINE));
                emailInfoDto.setComment((String) node.get(ProjectConstants.COMMENT));

                if (node.get(ProjectConstants.SUBMITTER_PAX_ID) != null) {
                    emailInfoDto.setSubmitterPaxId((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
                }
                if (node.get(ProjectConstants.SUBMITTER_NAME_FIRST) != null) {
                    emailInfoDto.setSubmitterNameFirst((String) node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
                }
                if (node.get(ProjectConstants.SUBMITTER_NAME_LAST) != null) {
                    emailInfoDto.setSubmitterNameLast((String) node.get(ProjectConstants.SUBMITTER_NAME_LAST));
                }

                emailInfoDto.setDirectReportFirst((String) node.get(ProjectConstants.DIRECT_REPORT_FIRST));
                emailInfoDto.setDirectReportLast((String) node.get(ProjectConstants.DIRECT_REPORT_LAST));
                emailInfoDto.setRecipientCount((Integer) node.get(ProjectConstants.RECIPIENT_COUNT));
                emailInfoDto.setDirectReportCount((Integer) node.get(ProjectConstants.DIRECT_REPORT_COUNT));

                payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                if (payoutType != null) {
                    emailInfoDto.setPayoutType(payoutType);
                    AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
                    emailInfoDto.setDisplayName(awardType.getDisplayName());
                    emailInfoDto.setLongDesc(awardType.getLongDesc());
                    emailInfoDto.setAbs(awardType.getAbs());
                }

                emailInfoDto.setEcardUrl(getECardUrl((Long) node.get(ProjectConstants.ECARD_ID), baseUrl));

                //Build notification link
                if (node.get(ProjectConstants.NOTIFICATION_ID) != null)
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI +
                            node.get(ProjectConstants.NOTIFICATION_ID));
                else
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI);

                emailInfoDto.setManagerText(getManagerText(node, emailType));

                break;

            case APPROVAL_REMINDER:
                    applicationDataKeys = Arrays.asList(
                            ApplicationDataConstants.KEY_NAME_APR_REMINDER_HEADER,
                            ApplicationDataConstants.KEY_NAME_APR_BUTTON);

            case APPROVAL:
                //Recognition - First Approval
                //Translatable Values
                if( applicationDataKeys == null) {
                    applicationDataKeys = Arrays.asList(
                            ApplicationDataConstants.KEY_NAME_APR_HEADER,
                            ApplicationDataConstants.KEY_NAME_APR_BUTTON);
                }

                //Template
                emailInfoDto.setTemplateName(EmailConstants.RECOGNITION_APPROVAL_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                emailInfoDto.setManagerText((String) node.get(ProjectConstants.MANAGER_TEXT));
                emailInfoDto.setSubmitterNameFirst((String) node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
                emailInfoDto.setSubmitterNameLast((String) node.get(ProjectConstants.SUBMITTER_NAME_LAST));

                emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                emailInfoDto.setPointAmount(amount.intValue());
                emailInfoDto.setHeadline((String) node.get(ProjectConstants.HEADLINE));
                emailInfoDto.setComment((String) node.get(ProjectConstants.COMMENT));

                payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                if (payoutType != null) {
                    emailInfoDto.setPayoutType(payoutType);
                    AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
                    emailInfoDto.setDisplayName(awardType.getDisplayName());
                    emailInfoDto.setLongDesc(awardType.getLongDesc());
                    emailInfoDto.setAbs(awardType.getAbs());
                }

                emailInfoDto.setEcardUrl(getECardUrl((Long) node.get(ProjectConstants.ECARD_ID), baseUrl));

                emailInfoDto.setNotificationUrl((String) node.get(ProjectConstants.NOTIFICATION_URL));

                break;

            case NOTIFY_OTHER:
                //Notify Other - You have been copied on a recognition
                //Translatable Values
                applicationDataKeys = Arrays.asList(
                        ApplicationDataConstants.KEY_NAME_NOTIFY_OTHER_HEADER,
                        ApplicationDataConstants.KEY_NAME_RECGMN_BUTTON);

                //Template
                emailInfoDto.setTemplateName(EmailConstants.RECOGNITION_APPROVAL_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                emailInfoDto.setPointAmount(amount.intValue());
                emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                emailInfoDto.setHeadline((String) node.get(ProjectConstants.HEADLINE));
                emailInfoDto.setComment((String) node.get(ProjectConstants.COMMENT));

                payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                if (payoutType != null) {
                    emailInfoDto.setPayoutType(payoutType);
                    AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
                    emailInfoDto.setDisplayName(awardType.getDisplayName());
                    emailInfoDto.setLongDesc(awardType.getLongDesc());
                    emailInfoDto.setAbs(awardType.getAbs());
                }

                emailInfoDto.setSubmitterPaxId((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
                emailInfoDto.setSubmitterNameFirst((String) node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
                emailInfoDto.setSubmitterNameLast((String) node.get(ProjectConstants.SUBMITTER_NAME_LAST));

                emailInfoDto.setRecipientNameFirst((String) node.get(ProjectConstants.RECIPIENT_NAME_FIRST));
                emailInfoDto.setRecipientNameLast((String) node.get(ProjectConstants.RECIPIENT_NAME_LAST));

                emailInfoDto.setEcardUrl(getECardUrl(toId(node.get(ProjectConstants.ECARD_ID)), baseUrl));

                //Build notification link
                if (node.get(ProjectConstants.NOTIFICATION_ID) != null)
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI +
                            node.get(ProjectConstants.NOTIFICATION_ID).toString());
                else
                    emailInfoDto.setNotificationUrl(baseUrl + EmailConstants.NOTIFICATIONS_URI);

                emailInfoDto.setManagerText(getManagerText(node, emailType));

                break;

            case SERVICE_ANNIVERSARY_REMINDER_TYPE:
                //Milestone - your direct report has an upcoming service anniversary

                //Template
                emailInfoDto.setTemplateName(EmailConstants.MILESTONE_REMINDER_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                String date = (String) node.get(ProjectConstants.MILESTONE_DATE);
                emailInfoDto.setDate(DateUtil.convertDateFormatString(date, "yyyy-MM-dd", "MMMM dd"));
                emailInfoDto.setMilestoneType(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode());
                applicationDataKeys = Arrays.asList(ApplicationDataConstants.KEY_NAME_MILESTONE_REMINDER_SERVICE_ANNIVERSARY_HEADER);

                setMilestoneReminderInfoOnEmail(emailInfoDto, node);

                break;

            case BIRTHDAY_REMINDER_TYPE:
                //Milestone - your direct report has an upcoming birthday

                //Template
                emailInfoDto.setTemplateName(EmailConstants.MILESTONE_REMINDER_TEMPLATE_NAME + EmailConstants.HTML_FILE_TYPE);

                //Additional Data
                date = (String) node.get(ProjectConstants.MILESTONE_DATE);
                emailInfoDto.setDate(DateUtil.convertDateFormatString(date, "MM-dd", "MMMM dd"));
                emailInfoDto.setMilestoneType(EventType.BIRTHDAY_REMINDER_TYPE.getCode());
                applicationDataKeys = Arrays.asList(ApplicationDataConstants.KEY_NAME_MILESTONE_REMINDER_BIRTHDAY_HEADER);

                setMilestoneReminderInfoOnEmail(emailInfoDto, node);

                break;

            /*
             * We can add other EventTypes here as needed - they all exist in the EVENT_TYPE table.
             */

        }

        //Translate the header
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(pax.getPreferredLocale());
        Map<Long, String> translations = null;

        //Pull any other translated values
        if (!CollectionUtils.isEmpty(applicationDataKeys)) {
            applicationDataList = applicationDataDao.findBy()
                    .where(ProjectConstants.KEY_NAME).in(applicationDataKeys).find();
        }

        if (applicationDataList != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (ApplicationData appData : applicationDataList) {
                rowIds.add(appData.getApplicationDataId());
                defaultMap.put(appData.getApplicationDataId(), appData.getValue());
            }

            if (pax.getPreferredLocale() == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                translations = defaultMap;
            } else {
                translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode,
                        TableName.APPLICATION_DATA.name(),
                        TranslationConstants.COLUMN_NAME_VALUE, rowIds, defaultMap);
            }
        }

        //Apply Translated Values to DTO
        return applyTranslations(emailInfoDto, applicationDataList, translations);
    }

    protected Long toId(Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Long) {
            return (Long) object;
        }
        if (object instanceof String) {
            return Long.parseLong((String) object);
        }
        throw new IllegalArgumentException("Unexpected class of ID encountered: " + object.getClass().getName() + "; " + object);
    }

    /**
     * Map the translated values pulled from the database to the DTO
     * @param emailInfoDTO
     * @param applicationDataList
     * @param translations
     * @return
     */

    private void setMilestoneReminderInfoOnEmail (EmailInfoDTO emailInfoDto, Map<String, Object> node) {
        emailInfoDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
        emailInfoDto.setPointAmount(((Double) node.get(ProjectConstants.POINT_AMOUNT)).intValue());

        String payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
        if (payoutType != null) {
            emailInfoDto.setPayoutType(payoutType);
            AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
            emailInfoDto.setDisplayName(awardType.getDisplayName());
            emailInfoDto.setLongDesc(awardType.getLongDesc());
            emailInfoDto.setAbs(awardType.getAbs());
        }

        //Direct Report Pax Information
        Pax directReportPax = paxRepository.findOne((Long) node.get(ProjectConstants.PAX_ID));
        emailInfoDto.setDirectReportFirst(directReportPax.getFirstName());
        emailInfoDto.setDirectReportLast(directReportPax.getLastName());
    }

    private EmailInfoDTO applyTranslations(EmailInfoDTO emailInfoDTO,
                                           List<ApplicationData> applicationDataList, Map<Long, String> translations) {

        for (ApplicationData applicationData : applicationDataList) {
            if (applicationData.getKeyName().contains("notification.email.subject")) {
                //Handles the header and subject line of the email
                emailInfoDTO.setMessageHeader(translations.get(applicationData.getApplicationDataId()));
            } else if (applicationData.getKeyName().endsWith("Button")) {
                //Handles the dynamic button text (only applies to recognition_or_approval_received.html)
                emailInfoDTO.setButtonText(translations.get(applicationData.getApplicationDataId()));
            }
        }

        return emailInfoDTO;
    }

    /**
     * This email populates the email info DTO
     *
     * @param nominationId - The nomination id for the approval email we are creating
     * @param approverPaxId - Pax Id of the approver the email is being sent to
     * @return - Returns the notification request populated by the email info dto
     */
    private Map<String, Object> getApprovalReceivedEmailInfo(Long nominationId, Long approverPaxId, String emailType) {

        Map<String, Object> approvalEmailMap = new HashMap<>();

        String baseUrl = environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL);

        Nomination nomination = nominationDao.findBy()
                .where(ProjectConstants.ID).eq(nominationId)
                .findOne();
        Recognition recognition = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.PENDING.toString())
                .findOne();
        Alert alert = alertDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationId)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.RECOGNITION_APPROVAL.toString())
                .and(ProjectConstants.PAX_ID).eq(approverPaxId)
                .findOne();
        Program program = programDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(nomination.getProgramId())
                .findOne();
        Pax receiverPax = paxDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(recognition.getReceiverPaxId())
                .findOne();
        Pax submitterPax = paxDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(nomination.getSubmitterPaxId())
                .findOne();
        NominationMisc nominationMisc = nominationMiscDao.findBy()
                .where(ProjectConstants.VF_NAME).eq(VfName.PAYOUT_TYPE.name())
                .and(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .findOne();

        approvalEmailMap.put(ProjectConstants.EMAIL_TYPE, EventType.APPROVAL.getCode());

        // Common info
        approvalEmailMap.put(ProjectConstants.NOMINATION_ID, nominationId);
        if (alert != null && alert.getId() != null) {
            approvalEmailMap.put(ProjectConstants.NOTIFICATION_ID, alert.getId());
        }

        //Recipient Information
        Pax approverPax = paxDao.findById(approverPaxId);

        approvalEmailMap.put(ProjectConstants.RECIPIENT_PAX_ID, approverPaxId);
        approvalEmailMap.put(ProjectConstants.RECIPIENT_LOCALE_CODE, approverPax.getPreferredLocale());
        approvalEmailMap.put(ProjectConstants.RECIPIENT_NAME_FIRST, receiverPax.getFirstName());
        approvalEmailMap.put(ProjectConstants.RECIPIENT_NAME_LAST, receiverPax.getLastName());

        if (nominationMisc != null) {
            approvalEmailMap.put(ProjectConstants.PAYOUT_TYPE, nominationMisc.getMiscData());
        }

        // Recipient Count, Direct Report Count, and Point Amount
        Integer recipientCount = 0;
        List<Recognition> recipientsList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                .and(ProjectConstants.PARENT_ID).isNull()
                .find();
        List<Long> recipientIds = new ArrayList<>();
        List<Long> recognitionIds = new ArrayList<>();
        Integer pointAmount = recognition.getAmount().intValue();

        if (recipientsList != null) {
            recipientCount = recipientsList.size();
            approvalEmailMap.put(ProjectConstants.RECIPIENT_COUNT, recipientCount);

            for (Recognition recg : recipientsList) {
                recipientIds.add(recg.getReceiverPaxId());
                recognitionIds.add(recg.getId());
            }
            List<Long> approvalPendingList = null;
            populateApprovalPendingList(recognitionIds,recipientsList, approverPaxId, approvalPendingList, pointAmount);
        }

        Integer directReportCount = 0;
        Pax directReportPax = null;

        List<Relationship> relationshipList = getRelationshipList(recipientIds, approverPaxId);


        if (!CollectionUtils.isEmpty(relationshipList)) {
            directReportPax = paxDao.findBy()
                    .where(ProjectConstants.PAX_ID).eq(relationshipList.get(0).getPaxId1())
                    .findOne();
            directReportCount = relationshipList.size();
        }

        approvalEmailMap.put(ProjectConstants.DIRECT_REPORT_COUNT, directReportCount);
        approvalEmailMap.put(ProjectConstants.POINT_AMOUNT, pointAmount);

        // Direct Report Info
        if (directReportPax != null) {
            approvalEmailMap.put(ProjectConstants.DIRECT_REPORT_FIRST, directReportPax.getFirstName());
            approvalEmailMap.put(ProjectConstants.DIRECT_REPORT_LAST, directReportPax.getLastName());
        }

        // Submitter Info
        approvalEmailMap.put(ProjectConstants.SUBMITTER_PAX_ID, submitterPax.getPaxId());
        approvalEmailMap.put(ProjectConstants.SUBMITTER_NAME_FIRST, submitterPax.getFirstName());
        approvalEmailMap.put(ProjectConstants.SUBMITTER_NAME_LAST, submitterPax.getLastName());

        // Program Info
        approvalEmailMap.put(ProjectConstants.PROGRAM_NAME, program.getProgramName());
        approvalEmailMap.put(ProjectConstants.PROGRAM_TYPE_CODE, program.getProgramTypeCode());

        // Common Info
        if (nomination.getEcardId() != null) {
            approvalEmailMap.put(ProjectConstants.ECARD_ID, nomination.getEcardId());
        }

        approvalEmailMap.put(ProjectConstants.COMMENT, nomination.getComment());
        approvalEmailMap.put(ProjectConstants.HEADLINE, nomination.getHeadlineComment());

        //Deep link to notifications page
        if (approvalEmailMap.get(ProjectConstants.NOTIFICATION_ID) != null) {
            approvalEmailMap.put(ProjectConstants.NOTIFICATION_URL,
                    baseUrl + EmailConstants.NOTIFICATIONS_URI + approvalEmailMap.get(ProjectConstants.NOTIFICATION_ID).toString());
        } else {
            approvalEmailMap.put(ProjectConstants.NOTIFICATION_URL,
                    baseUrl + EmailConstants.NOTIFICATIONS_URI);
        }

        approvalEmailMap.put(ProjectConstants.MANAGER_TEXT, getManagerText(approvalEmailMap, emailType));

        return approvalEmailMap;
    }

    /**
     * gets the list of approval pending.
     *
     * @param recognitionIds id from recognition table
     * @param recognitionsList list of recipients from recogniton table.
     * @param approverPaxId approver pax Id
     * @param approvalPendingList output parameter, returns the list of ids of pending approvals
     * @param pointAmount output parameter, returns the total amount of points of Recognition Ids that are in approval pending list repository
     */
    public void populateApprovalPendingList(List<Long> recognitionIds, List<Recognition> recognitionsList,
                                            Long approverPaxId, List<Long> approvalPendingList,
                                            Integer pointAmount) {
        //Point Amount should be total points the approver is responsible for approving
        if (recognitionIds!= null) {
            approvalPendingList = new ArrayList<>();
            Iterable<List<Long>> recognitionIdsListSplit =
                    Iterables.partition(recognitionIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
            for (List<Long> set : recognitionIdsListSplit) {
                List<Long> temporalList = approvalPendingDao.findBy()
                        .where(ProjectConstants.TARGET_ID).in(set)
                        .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                        .and(ProjectConstants.PAX_ID).eq(approverPaxId)
                        .find(ProjectConstants.TARGET_ID, Long.class);
                approvalPendingList.addAll(temporalList);
            }
        }
        if (!CollectionUtils.isEmpty(approvalPendingList)) {
            for (Recognition recg : recognitionsList) {
                if (approvalPendingList.contains(recg.getId())) {
                    pointAmount += recg.getAmount().intValue();
                }
            }
        }
    }

    /**
     * gets the list of Relationship.
     *
     * @param recipientIds ids from receivers pax
     * @param approverPaxId approver pax Id
     */
    public List<Relationship> getRelationshipList(List<Long> recipientIds, Long approverPaxId) {
        List<Relationship> relationshipList = new ArrayList<>();
        Iterable<List<Long>> recipientIdsListSplit =
                Iterables.partition(recipientIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> set : recipientIdsListSplit) {
            List<Relationship> temporalList = relationshipDao.findBy()
                    .where(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.toString())
                    .and(ProjectConstants.PAX_ID_2).eq(approverPaxId)
                    .and(ProjectConstants.PAX_ID_1).in(set)
                    .find();
            relationshipList.addAll(temporalList);
        }
        return relationshipList;
    }

    /**
     * This method takes a map of all data for an email and concats the appropriate
     * text for when there is more than one recipient
     *
     * @param approvalEmailMap - the map of the data model for the email
     * @param emailType - type of email being sent
     * @return String - the text to put in the model
     */
    protected String getManagerText(Map<String, Object> approvalEmailMap, String emailType) {

        //Pull the translated values for the dynamic text
        List<String> applicationDataKeys = null;
        List<ApplicationData> applicationDataList = null;

        applicationDataKeys = Arrays.asList(
                ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_AND,
                ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHER,
                ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHERS,
                ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS);

        if (!CollectionUtils.isEmpty(applicationDataKeys)) {
            applicationDataList = applicationDataDao.findBy()
                    .where(ProjectConstants.KEY_NAME).in(applicationDataKeys)
                    .find();
        }

        String recipientLanguage = approvalEmailMap.get(ProjectConstants.RECIPIENT_LOCALE_CODE).toString();
        String localeCode = (recipientLanguage != null) ? recipientLanguage : LocaleCodeEnum.en_US.name();

        Map<Long, String> dynamicStringMap = fetchTranslations(localeCode, applicationDataList);

        String translatedAnd = "and";
        String translatedOther = "other";
        String translatedOthers = "others";
        String translatedDirectReports = "of your direct reports";

        for (ApplicationData applicationData : applicationDataList) {
            if (applicationData.getKeyName().equalsIgnoreCase(
                    ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_AND)) {
                translatedAnd = dynamicStringMap.get(applicationData.getApplicationDataId());
            } else if (applicationData.getKeyName().equalsIgnoreCase(
                    ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHER)) {
                translatedOther = dynamicStringMap.get(applicationData.getApplicationDataId());
            } else if (applicationData.getKeyName().equalsIgnoreCase(
                    ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHERS)) {
                translatedOthers = dynamicStringMap.get(applicationData.getApplicationDataId());
            } else if (applicationData.getKeyName().equalsIgnoreCase(
                    ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS)) {
                translatedDirectReports = dynamicStringMap.get(applicationData.getApplicationDataId());
            }
        }

        StringBuilder sb = new StringBuilder();
        int recipientCount = (int) approvalEmailMap.get(ProjectConstants.RECIPIENT_COUNT);
        int directReportCount = (int) approvalEmailMap.get(ProjectConstants.DIRECT_REPORT_COUNT);
        int otherRecipients = 0;

        if (directReportCount != 0)
            otherRecipients = recipientCount - directReportCount;
        else
            otherRecipients = recipientCount - 1;

        if (recipientCount > 1) {
            if (directReportCount == 1) {
                //More than one person recognized; only one is a direct report
                sb.append(approvalEmailMap.get(ProjectConstants.DIRECT_REPORT_FIRST));
                sb.append(ProjectConstants.SPACE);
                sb.append(approvalEmailMap.get(ProjectConstants.DIRECT_REPORT_LAST));
                sb.append(ProjectConstants.SPACE);
                sb.append(translatedAnd);
                sb.append(ProjectConstants.SPACE);
                sb.append(otherRecipients);
                sb.append(ProjectConstants.SPACE);

                if (otherRecipients == 1)
                    sb.append(translatedOther);
                else
                    sb.append(translatedOthers);
            }
            else if (recipientCount == directReportCount) {
                //More than one person recognized; all of them are direct reports
                sb.append(directReportCount);
                sb.append(ProjectConstants.SPACE);
                sb.append(translatedDirectReports);
            }
            else if (directReportCount == 0) {
                //More than one person recognized; none of them are direct reports
                //This will only happen for approval emails (manager emails will always have a direct report)
                sb.append(approvalEmailMap.get(ProjectConstants.RECIPIENT_NAME_FIRST));
                sb.append(ProjectConstants.SPACE);
                sb.append(approvalEmailMap.get(ProjectConstants.RECIPIENT_NAME_LAST));
                sb.append(ProjectConstants.SPACE);
                sb.append(translatedAnd);
                sb.append(ProjectConstants.SPACE);
                sb.append(otherRecipients);
                sb.append(ProjectConstants.SPACE);

                if (otherRecipients == 1)
                    sb.append(translatedOther);
                else
                    sb.append(translatedOthers);
            }
            else {
                //More than one person recognized, only some are direct reports
                sb.append(directReportCount);
                sb.append(ProjectConstants.SPACE);
                sb.append(translatedDirectReports);
                sb.append(ProjectConstants.SPACE);
                sb.append(translatedAnd);
                sb.append(ProjectConstants.SPACE);
                sb.append(otherRecipients);
                sb.append(ProjectConstants.SPACE);

                if (otherRecipients == 1)
                    sb.append(translatedOther);
                else
                    sb.append(translatedOthers);
            }
        }
        else {
            //Only one person was recognized. Just use their name
            if (EventType.APPROVAL.getCode().equalsIgnoreCase(emailType)
                    || EventType.NOTIFY_OTHER.getCode().equalsIgnoreCase(emailType)
                    || EventType.APPROVAL_REMINDER.getCode().equalsIgnoreCase(emailType) ) {
                sb.append(approvalEmailMap.get(ProjectConstants.RECIPIENT_NAME_FIRST));
                sb.append(ProjectConstants.SPACE);
                sb.append(approvalEmailMap.get(ProjectConstants.RECIPIENT_NAME_LAST));
            } else if (EventType.RECOGNITION_MANAGER.getCode().equalsIgnoreCase(emailType)) {
                sb.append(approvalEmailMap.get(ProjectConstants.DIRECT_REPORT_FIRST));
                sb.append(ProjectConstants.SPACE);
                sb.append(approvalEmailMap.get(ProjectConstants.DIRECT_REPORT_LAST));
            }
        }

        return sb.toString();
    }


    /**
     * This method takes a list of application data and returns the translated values
     *
     * @param languageCode - code of the recipients preferred language
     * @param entries - list of application data constants to be translated
     * @return Map<Long, String> - map of translated values to the key of APPLICATION_DATA
     */
    private Map<Long, String> fetchTranslations(String languageCode, List<ApplicationData> entries) {
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);

        Map<Long, String> translations = null;
        if (entries != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (ApplicationData entry : entries) {
                rowIds.add(entry.getApplicationDataId());
                defaultMap.put(entry.getApplicationDataId(), entry.getValue());
            }

            if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                return defaultMap;
            }

            translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode,
                    TableName.APPLICATION_DATA.name(),
                    TranslationConstants.COLUMN_NAME_VALUE, rowIds, defaultMap);
        }

        return translations;
    }

    private void logBatchEvent(Batch batch, String typeCode, String message) {
        logBatchEvent(batch, typeCode, message, null, null);
    }

    private void logBatchEvent(Batch batch, String typeCode, String message, String targetTable, Long targetId) {
        BatchEventDTO batchEvent = new BatchEventDTO();
        batchEvent.setBatchId(batch.getBatchId());
        batchEvent.setType(typeCode);
        batchEvent.setMessage(message);
        batchEvent.setTarget(new TargetDTO(targetTable, targetId));
        batchService.createBatchEvent(batch, batchEvent);
    }

    private Map<String, Object> getMilestoneReminderProgramData(PaxMilestoneReminderDTO paxMilestoneReminderDTO) {
        Map<String, Object> programData = new HashMap<>();
        programData.put(ProjectConstants.PAX_ID, paxMilestoneReminderDTO.getPaxId());
        programData.put(ProjectConstants.PROGRAM_NAME, paxMilestoneReminderDTO.getProgramName());
        programData.put(ProjectConstants.MILESTONE_DATE, paxMilestoneReminderDTO.getMilestoneDate());
        programData.put(ProjectConstants.PAYOUT_TYPE, paxMilestoneReminderDTO.getPayoutType());

        if (paxMilestoneReminderDTO.getAwardAmount() == null) {
            programData.put(ProjectConstants.POINT_AMOUNT, 0D);

        } else {
            programData.put(ProjectConstants.POINT_AMOUNT, paxMilestoneReminderDTO.getAwardAmount());
        }

        return programData;
    }

    private String getClientLogoUrl(String baseUrl) {
        if (environmentUtil.readFromCdn()) {
            Files logoImage = fileImageService.searchForImage(PROJECT_LOGO);
            try {
                return signUrl(logoImage, LOGO_IMAGE, null);
            } catch (Exception e) {
                logger.error("Error encountered while attempting to return the URL for " + logoImage +
                        "; attempting to return a non-CDN URL as a backup", e);
            }
        }

        return baseUrl + EmailConstants.LOGO_URI;
    }

    private String getECardUrl(Long eCardId, String baseUrl) {
        if (eCardId == null) {
            return null;
        }

        if (environmentUtil.readFromCdn()) {
            Files eCardImage = eCardService.getECardImage(eCardId);
            try {
                return signUrl(eCardImage, ECARD_IMAGE, eCardId);
            } catch (Exception e) {
                logger.error("Error encountered while attempting to return the URL for " + eCardImage +
                        "; attempting to return a non-CDN URL as a backup", e);
            }
        }

        StringBuilder url = new StringBuilder()
                                    .append(baseUrl)
                                    .append(EmailConstants.ECARD_URI)
                                    .append(eCardId);
        return url.toString();
    }

    private String signUrl(Files imageFile, String imageDescriptor, Long id) {
        if (imageFile != null) {
            try {
                return gcpUtil.signUrl(imageFile, getSignedUrlDuration());
            } catch (Exception e) {
                throw new IllegalStateException("Error encountered while attempting to return the URL for " + imageFile +
                        "; attemping to return non-CDN URL as backup", e);
            }
        } else {
            throw new IllegalStateException("No " + imageDescriptor + " exists" + ((id != null) ? " for ID " + id : ""));
        }
    }

    private long getSignedUrlDuration() {
        return environmentUtil.getLongProperty(EMAIL_URL_SIGNING_DURATION_PROPERTY, ONE_HUNDRED_YEARS_IN_SECONDS);
    }
}