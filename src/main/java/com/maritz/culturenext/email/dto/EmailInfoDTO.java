package com.maritz.culturenext.email.dto;

public class EmailInfoDTO {
    
    //Needed for NotificationRequest Object
    String emailType;
    String templateName;
    Long nominationId;
    Long recognitionId;
    Long notificationId;
    
    //Dynamic Text
    String messageHeader;
    String managerText;
    String buttonText;
    String pointsText;
    String cashText;
    String cashDescriptor;
    
    //Recipient Info
    Long recipientPaxId;
    String recipientLocaleCode;
    String recipientNameFirst;
    String recipientNameLast;
    String recipientEmail;
    String username;
    
    //Submitter Info
    Long submitterPaxId;
    String submitterNameFirst;
    String submitterNameLast;
    
    //Application Properties
    String primaryColor;
    String secondaryColor;
    String textOnPrimaryColor;
    
    //URL Mappings
    String ecardUrl;
    String clientLogo;
    String clientUrl;
    String notificationUrl; 
    String settingsUrl;
    
    //Recognition Info
    Integer recipientCount;
    Integer directReportCount;
    String directReportFirst;
    String directReportLast;
    String programName;
    String programTypeCode;
    Integer pointAmount;
    String payoutType;
    String displayName;
    String shortDesc;
    String longDesc;
    boolean abs;
    String comment;
    String headline;    
    
    //Misc
    String currentYear;
    String resetToken;
    String date;
    String milestoneType;
    
    
    public String getEmailType() {
        return emailType;
    }
    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }
    public String getTemplateName() {
        return templateName;
    }
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
    public Long getNominationId() {
        return nominationId;
    }
    public void setNominationId(Long nominationId) {
        this.nominationId = nominationId;
    }
    public Long getRecognitionId() {
        return recognitionId;
    }
    public void setRecognitionId(Long recognitionId) {
        this.recognitionId = recognitionId;
    }
    public Long getNotificationId() {
        return notificationId;
    }
    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }
    public String getMessageHeader() {
        return messageHeader;
    }
    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }
    public String getManagerText() {
        return managerText;
    }
    public void setManagerText(String managerText) {
        this.managerText = managerText;
    }
    public String getButtonText() {
        return buttonText;
    }
    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }
    public String getPointsText() {
        return pointsText;
    }
    public void setPointsText(String pointsText) {
        this.pointsText = pointsText;
    }
    public String getCashText() {
        return cashText;
    }
    public void setCashText(String cashText) {
        this.cashText = cashText;
    }
    public String getCashDescriptor() {
        return cashDescriptor;
    }
    public void setCashDescriptor(String cashDescriptor) {
        this.cashDescriptor = cashDescriptor;
    }
    public Long getRecipientPaxId() {
        return recipientPaxId;
    }
    public void setRecipientPaxId(Long recipientPaxId) {
        this.recipientPaxId = recipientPaxId;
    }
    public String getRecipientLocaleCode() {
        return recipientLocaleCode;
    }
    public void setRecipientLocaleCode(String recipientLocaleCode) {
        this.recipientLocaleCode = recipientLocaleCode;
    }
    public String getRecipientNameFirst() {
        return recipientNameFirst;
    }
    public void setRecipientNameFirst(String recipientNameFirst) {
        this.recipientNameFirst = recipientNameFirst;
    }
    public String getRecipientNameLast() {
        return recipientNameLast;
    }
    public void setRecipientNameLast(String recipientNameLast) {
        this.recipientNameLast = recipientNameLast;
    }
    public String getRecipientEmail() {
        return recipientEmail;
    }
    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public Long getSubmitterPaxId() {
        return submitterPaxId;
    }
    public void setSubmitterPaxId(Long submitterPaxId) {
        this.submitterPaxId = submitterPaxId;
    }
    public String getSubmitterNameFirst() {
        return submitterNameFirst;
    }
    public void setSubmitterNameFirst(String submitterNameFirst) {
        this.submitterNameFirst = submitterNameFirst;
    }
    public String getSubmitterNameLast() {
        return submitterNameLast;
    }
    public void setSubmitterNameLast(String submitterNameLast) {
        this.submitterNameLast = submitterNameLast;
    }
    public String getPrimaryColor() {
        return primaryColor;
    }
    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }
    public String getSecondaryColor() {
        return secondaryColor;
    }
    public void setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
    }
    public String getTextOnPrimaryColor() {
        return textOnPrimaryColor;
    }
    public void setTextOnPrimaryColor(String textOnPrimaryColor) {
        this.textOnPrimaryColor = textOnPrimaryColor;
    }
    public String getEcardUrl() {
        return ecardUrl;
    }
    public void setEcardUrl(String ecardUrl) {
        this.ecardUrl = ecardUrl;
    }
    public String getClientLogo() {
        return clientLogo;
    }
    public void setClientLogo(String clientLogo) {
        this.clientLogo = clientLogo;
    }
    public String getClientUrl() {
        return clientUrl;
    }
    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }
    public String getNotificationUrl() {
        return notificationUrl;
    }
    public void setNotificationUrl(String notificationUrl) {
        this.notificationUrl = notificationUrl;
    }
    public String getSettingsUrl() {
        return settingsUrl;
    }
    public void setSettingsUrl(String settingsUrl) {
        this.settingsUrl = settingsUrl;
    }
    public Integer getRecipientCount() {
        return recipientCount;
    }
    public void setRecipientCount(Integer recipientCount) {
        this.recipientCount = recipientCount;
    }
    public Integer getDirectReportCount() {
        return directReportCount;
    }
    public void setDirectReportCount(Integer directReportCount) {
        this.directReportCount = directReportCount;
    }
    public String getDirectReportFirst() {
        return directReportFirst;
    }
    public void setDirectReportFirst(String directReportFirst) {
        this.directReportFirst = directReportFirst;
    }
    public String getDirectReportLast() {
        return directReportLast;
    }
    public void setDirectReportLast(String directReportLast) {
        this.directReportLast = directReportLast;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public String getProgramTypeCode() {
        return programTypeCode;
    }
    public void setProgramTypeCode(String programTypeCode) {
        this.programTypeCode = programTypeCode;
    }
    public Integer getPointAmount() {
        return pointAmount;
    }
    public void setPointAmount(Integer pointAmount) {
        this.pointAmount = pointAmount;
    }
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getShortDesc() {
        return shortDesc;
    }
    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }
    public String getLongDesc() {
        return longDesc;
    }
    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }
    public boolean isAbs() {
        return abs;
    }
    public void setAbs(boolean abs) {
        this.abs = abs;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getHeadline() {
        return headline;
    }
    public void setHeadline(String headline) {
        this.headline = headline;
    }
    public String getCurrentYear() {
        return currentYear;
    }
    public void setCurrentYear(String currentYear) {
        this.currentYear = currentYear;
    }
    public String getResetToken() {
        return resetToken;
    }
    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getMilestoneType() {
        return milestoneType;
    }
    public void setMilestoneType(String milestoneType) {
        this.milestoneType = milestoneType;
    }
    
}