package com.maritz.culturenext.icons.service;


import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.icons.dto.IconDTO;

import java.util.List;

public interface IconService {
    
    /**
     * This method takes an svg file as a multipart file and stores it in the database
     *
     * @param image The image file to be stored in the db
     * @return the populated icon DTO with the id of the image and the programActivity status
     * @throws Exception
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/24150067/Create+Icon
     */
    IconDTO createIcon(MultipartFile image) throws Exception;
    
    /**
     * This method takes an icon id and returns the associated svg xml for the icon
     *
     * @param id The id for the icon to be returned
     * @return The String xml value to be returned
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/24150071/Get+Icon+by+Id
     */
    ResponseEntity getIconById(Long id);
    
    /**
     * This method returns the list of icons in the db with the id and if it is associated to a program activity
     *
     * @param usedInPromotion - Specifies if the icon is currently tied to a PROGRAM_ACTIVITY record
     * @param type - type of icon to pull. See FileTypes.java
     * @return The list of icon DTOs for all the exist in the db
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/24150073/Get+all+Icons+for+Project
     */
    List<IconDTO> getListOfIcons(Boolean usedInPromotion, String type);
}
