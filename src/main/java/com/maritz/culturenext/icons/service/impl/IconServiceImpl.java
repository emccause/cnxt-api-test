package com.maritz.culturenext.icons.service.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityFiles;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.icons.constants.IconConstants;
import com.maritz.culturenext.icons.dto.IconDTO;
import com.maritz.culturenext.icons.service.IconService;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class IconServiceImpl implements IconService {

    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<ProgramActivityFiles> programActivityFilesDao;
    @Inject private ConcentrixDao<ProgramActivity> programActivityDao;
    @Inject private Security security;
    @Inject private FileImageService fileImageService;

    private EnvironmentUtil environmentUtil;
    private GcpUtil gcpUtil;


    @Inject
    public IconServiceImpl setEnvironmentUtil(EnvironmentUtil environmentUtil) {
        this.environmentUtil = environmentUtil;
        return this;
    }

    public IconServiceImpl setGcpUtil(GcpUtil gcpUtil) {
        this.gcpUtil = gcpUtil;
        return this;
    }

    @Override
    public IconDTO createIcon(MultipartFile image) throws Exception {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        // Get the svg image information
        Long imageSize = image.getSize();
        Long maxFileSizeBytes = ImageConstants.MAX_IMAGE_SIZE_MB * (1024 * 1024);

        if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            errorMessages.add(IconConstants.NOT_ADMIN_CODE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errorMessages).setStatus(HttpStatus.FORBIDDEN);
        }
        if (imageSize.compareTo(maxFileSizeBytes) > 0) {
            errorMessages.add(IconConstants.IMAGE_TOO_BIG_MESSAGE);
        }
        if (!image.getContentType().equals(MediaTypesEnum.IMAGE_SVG_XML.value())) {
            errorMessages.add(IconConstants.IMAGE_TYPE_CODE_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errorMessages);

        Files iconEntry = new Files();
        iconEntry.setFileTypeCode(FileImageTypes.JUMBOTRON.name());
        iconEntry.setMediaType(MediaTypesEnum.IMAGE_SVG.value());
        filesDao.save(iconEntry);

        StreamUtils.fromInputStream(iconEntry, "data", image.getInputStream());

        if (environmentUtil.writeToCdn()) {
            gcpUtil.uploadFile(iconEntry.getId(), image);
        }

        IconDTO iconDTO = new IconDTO();
        iconDTO.setId(iconEntry.getId());
        iconDTO.setUsedInPromotion(false);
        return iconDTO;
    }

    @Override
    public ResponseEntity getIconById(Long id) {
        Files file = filesDao.findBy().where(ProjectConstants.ID).eq(id).findOne();
        return fileImageService.prepareIconForResponse(file);
    }

    @Override
    public List<IconDTO> getListOfIcons(Boolean usedInPromotion, String type) {
        List<IconDTO> iconDTOs = new ArrayList<>();

        List<ErrorMessage> errorMessages = new ArrayList<>();

        List<Long> iconIds = null;

        if (type != null && !type.isEmpty() && !type.trim().isEmpty()) {
            if (FileImageTypes.getImageFileTypes().contains(type)) {
                iconIds = filesDao.findBy()
                        .where(ProjectConstants.FILE_TYPE_CODE).eq(type)
                        .find(ProjectConstants.ID, Long.class);
            } else {
                errorMessages.add(IconConstants.IMAGE_FILE_TYPE_CODE_MESSAGE);
            }
        } else {
            iconIds = filesDao.findBy()
                    .where(ProjectConstants.FILE_TYPE_CODE).in(FileTypes.getImageFileTypeCode())
                    .find(ProjectConstants.ID, Long.class);
        }

        if (CollectionUtils.isEmpty(iconIds)) {
            return iconDTOs;
        }

        Date currentDate = new Date();
        List<Long> activeProgramActivityIds = programActivityDao.findBy()
                .where(ProjectConstants.THRU_DATE).gt(currentDate)
                .and(ProjectConstants.FROM_DATE).lt(currentDate)
                .find(ProjectConstants.PROGRAM_ACTIVITY_ID, Long.class);
        List<Long> programActivityIconIds = programActivityFilesDao.findBy()
                .where(ProjectConstants.PROGRAM_ACTIVITY_ID).in(activeProgramActivityIds)
                .find(ProjectConstants.FILES_ID, Long.class);

        for (Long iconId : iconIds) {
            IconDTO iconDTO = new IconDTO();
            iconDTO.setId(iconId);
            if (programActivityIconIds != null && programActivityIconIds.contains(iconId)) {
                iconDTO.setUsedInPromotion(true);
            }
            if (usedInPromotion == null || usedInPromotion.equals(iconDTO.getUsedInPromotion())) {
                iconDTOs.add(iconDTO);
            }
        }

        return iconDTOs;
    }

}
