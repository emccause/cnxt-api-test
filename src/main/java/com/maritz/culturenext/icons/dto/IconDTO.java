package com.maritz.culturenext.icons.dto;


public class IconDTO {

    Long id;
    Boolean usedInPromotion = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getUsedInPromotion() {
        return usedInPromotion;
    }

    public void setUsedInPromotion(Boolean usedInPromotion) {
        this.usedInPromotion = usedInPromotion;
    }

}
