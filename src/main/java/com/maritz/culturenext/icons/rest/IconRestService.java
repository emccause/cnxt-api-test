package com.maritz.culturenext.icons.rest;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.icons.dto.IconDTO;
import com.maritz.culturenext.icons.service.IconService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;
import java.util.List;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

@RestController
@RequestMapping("icons")
@Api(value="/icons", description = "All endpoints dealing with svg icons")
public class IconRestService {

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private IconService iconService;

    //rest/icons
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Creates a new icon entry in the database and assigns an id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created icon entry in the db", response = IconDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    public IconDTO createIcon(
            @ApiParam(value = "The icon file to be uploaded", required = true) 
                @RequestParam(ICON_REST_PARAM) MultipartFile image) throws Exception {
        return iconService.createIcon(image);
    }

    //rest/icons/~
    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    @ApiOperation(value = "Gets the svg icon file from the db based on the id and returns it")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the svg file from the db", response =
                    String.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getIconById(
            @ApiParam(value = "The id used to locate the corresponding svg icon from the db") 
                @PathVariable(ID_REST_PARAM) String iconIdString
    ) throws NoHandlerFoundException {
        
        Long iconId = convertPathVariablesUtil.getId(iconIdString);
        return  iconService.getIconById(iconId);
    }

    //rest/icons
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Gets the list of svg files from the db")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the list of svg files that are in the db", 
                    response = IconDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<IconDTO> getListOfIcons(
            @ApiParam(value = "Used In Promotion status to filter by", required = false) 
                @RequestParam(value = USED_IN_PROMOTION_REST_PARAM, required = false) Boolean usedInPromotion,
            @ApiParam(value = "Type of image. Possible types AWARD_CODE_CERT, AWARD_CODE_ICON, JUMBOTRON, CAROUSEL", 
                required = false) @RequestParam(value = TYPE_REST_PARAM, required = false) String type
    ) {
        return iconService.getListOfIcons(usedInPromotion, type);
    }


}
