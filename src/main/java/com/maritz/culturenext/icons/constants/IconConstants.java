package com.maritz.culturenext.icons.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.images.constants.ImageConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;

public class IconConstants {
    
    public static final String ERROR_IMAGE_TYPE_CODE = "PROGRAM_ICON_WRONG_TYPE";
    public static final String ERROR_IMAGE_TYPE_MESSAGE = "Only .svg is supported";

    //Error Messages
    public static final ErrorMessage NOT_ADMIN_CODE_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_NOT_ADMIN_CODE)
        .setField(ProjectConstants.ICON)
        .setMessage(ProgramConstants.ERROR_NOT_ADMIN_CODE_MESSAGE);
    
    public static final ErrorMessage IMAGE_TOO_BIG_MESSAGE = new ErrorMessage()
        .setCode(ProgramConstants.ERROR_IMAGE_TOO_BIG_CODE)
        .setField(ProjectConstants.ICON)
        .setMessage(ProgramConstants.ERROR_IMAGE_TOO_BIG_MESSAGE);
    
    public static final ErrorMessage IMAGE_TYPE_CODE_MESSAGE = new ErrorMessage()
        .setCode(IconConstants.ERROR_IMAGE_TYPE_CODE)
        .setField(ProjectConstants.ICON)
        .setMessage(IconConstants.ERROR_IMAGE_TYPE_MESSAGE);
    
    public static final ErrorMessage IMAGE_FILE_TYPE_CODE_MESSAGE = new ErrorMessage()
        .setCode(ImageConstants.ERROR_IMAGE_FILE_TYPE_CODE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ImageConstants.ERROR_IMAGE_FILE_TYPE_CODE_MESSAGE);
}
