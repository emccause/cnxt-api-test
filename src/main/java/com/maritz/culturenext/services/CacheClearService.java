package com.maritz.culturenext.services;

import java.util.Collection;

import javax.inject.Inject;

import net.sf.ehcache.Ehcache;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class CacheClearService {

    @Inject private CacheManager cacheManager;
    
    /**
     * Method to clear caches - runs every 2 hours, starting 2 hours after server startup.
     * Loops through the caches and has them clear expired entries.
     * 
     * @return void
     */
    @Scheduled(initialDelay = 7200000, fixedDelay = 7200000)
    public void clearExpiredCachedData() {
        // get caches
        Collection<String> cacheNames = cacheManager.getCacheNames();
        for (String cacheName : cacheNames) {
            Cache cache = cacheManager.getCache(cacheName);
            Object cacheObject = cache.getNativeCache();
            if (cacheObject instanceof Ehcache) {
                // have cache clear expired entries
                Ehcache ehCache = (Ehcache) cacheObject;
                ehCache.evictExpiredElements();
            }
        }
    }

}
