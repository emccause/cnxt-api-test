package com.maritz.culturenext.countries.service;

import java.util.List;

import com.maritz.culturenext.countries.dto.CountryDTO;

public interface CountryService {
    /**
     * Returns a list of all Countries
     * @return List of CountryDTO
     * @see <a href="https://maritz.atlassian.net/wiki/spaces/M365/pages/161611777/PPP+Index"
     *         target="_top">PPP Index</a>
     */
    List<CountryDTO> getCountriesData();
}
