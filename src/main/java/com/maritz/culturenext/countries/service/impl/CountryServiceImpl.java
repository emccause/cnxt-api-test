package com.maritz.culturenext.countries.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.countries.dto.CountryDTO;
import com.maritz.culturenext.countries.service.CountryService;
import com.maritz.culturenext.countries.dao.CountryDao;

@Component
public class CountryServiceImpl implements CountryService {
    
    @Inject private CountryDao countryDao;

    @Override
    public List<CountryDTO> getCountriesData() {
        return countryDao.getCountriesData();
    }

}
