package com.maritz.culturenext.countries.dao;

import java.util.List;

import com.maritz.culturenext.countries.dto.CountryDTO;

public interface CountryDao {
    /** 
     * Retrieves the List of all Countries in COUNTRY table
     * @return List of CountryDTO
     * 
     */
    List<CountryDTO> getCountriesData();
}
