package com.maritz.culturenext.countries.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.countries.dto.CountryDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.countries.dao.CountryDao;

@Repository
public class CountryDaoImpl extends AbstractDaoImpl implements CountryDao {
    
    public static final String COUNTRY_NAME = "countryName";
    public static final String DISPLAY_STATUS = "displayStatus";
    
    private static final String COUNTRY_DATA_QUERY = 
            "SELECT C.COUNTRY_ID AS ID, "
            + "C.COUNTRY_CODE, "
            + "C.COUNTRY_NAME, "
            + "C.DISPLAY_STATUS_TYPE_CODE AS DISPLAY_STATUS "
            + "FROM component.COUNTRY C "
            + "ORDER BY C.COUNTRY_NAME ";
    
    @Override
    public List<CountryDTO> getCountriesData() {
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(COUNTRY_DATA_QUERY, new MapSqlParameterSource(), new CamelCaseMapRowMapper());
        
        ArrayList<CountryDTO> countries = new ArrayList<CountryDTO>();
        for(Map<String, Object> node: nodes){
            CountryDTO country = new CountryDTO();
            country.setId((Long) node.get(ProjectConstants.ID));
            country.setCountryCode((String)node.get(ProjectConstants.COUNTRY_CODE));
            country.setCountryName((String)node.get(COUNTRY_NAME));
            country.setDisplayStatus((String)node.get(DISPLAY_STATUS));
            
            countries.add(country);
        }
        
        return countries;
    }

}
