package com.maritz.culturenext.countries.rest;

import java.util.List;

import javax.inject.Inject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.countries.dto.CountryDTO;
import com.maritz.culturenext.countries.service.CountryService;

@RestController
@Api(value = "countries", description = "Endpoint for countries in the database.")
public class CountryRestService {
    
    @Inject private CountryService countryService;
    
    // Service endpoint paths
    public static final String COUNTRIES_PATH = "countries";
    
    /**
     * Returns a list of all Countries
     */
    //rest/countries
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = COUNTRIES_PATH, method = RequestMethod.GET)
    @ApiOperation(value="Retrieves all countries in the database.", 
    notes="This will be used to populate the drop-down for setting up PPP Index by country.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of countries", 
                    response = CountryDTO.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<CountryDTO> getCountries(){
        return countryService.getCountriesData();
    }

}
