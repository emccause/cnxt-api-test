package com.maritz.culturenext.countries.dto;

public class CountryDTO {
    private Long id;
    private String countryCode;
    private String countryName;
    private String displayStatus;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    
    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    
    public String getDisplayStatus() {
        return displayStatus;
    }
    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }
}
