package com.maritz.culturenext.issuance.service.impl;

import static com.maritz.core.util.Collectors.toMapIgnoreDups;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.support.util.Tuple3;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.culturenext.jpa.repository.CnxtTransactionHeaderRepository;
import com.maritz.issuance.rules.TransactionEarningsResult;
import com.maritz.issuance.services.impl.AbstractFinancialInformationProvider;

@Component
public class CultureNextFinancialInformationProvider extends AbstractFinancialInformationProvider {
    
	final Logger logger = LoggerFactory.getLogger(getClass());
    @Inject private CnxtTransactionHeaderRepository cnxtTransactionHeaderRepository;


    @Override
    public boolean canProcess(TransactionEarningsResult transactionEarningsResult) {
        // We don't support bundling here
        return transactionEarningsResult.getTransactions().size() == 1;
    }

    @Override
    public Collection<TransactionEarningsResult> process(Collection<TransactionEarningsResult> transactionEarningsResultCollection) {
        logger.info("CultureNextFinancialInformationProvider - process ln 38");
        // Collect all transactions into a list so we can look up in bulk
        List<TransactionHeader> transactionHeaderList = 
            transactionEarningsResultCollection.stream()
                .map(TransactionEarningsResult::getTransactions)
                .flatMap(Collection::stream) // Basically, flatten our list of lists into a single list
                .collect(Collectors.toList()).stream() // containing all transactions to be processed
                    .map(TransactionHeaderDetail::getHeader) // and then collect all the transaction headers off there
                    .collect(Collectors.toList()); // and then return them so we can bulk load data.
        
        /* tuple data model:
         * A: Transaction id
         * B: Payout Type
         * C: Project number
         * D: Sub project number
         */
        logger.info("CultureNextFinancialInformationProvider transactions collected - ln  54");
        Map<Long, Tuple3<String, String, String>> transactionToPayoutTypeProjectSubProjectMap = 
                cnxtTransactionHeaderRepository.findPayoutTypeProjectSubProjectNumberByTransactionHeaderId(
                        transactionHeaderList.stream()
                        .map(TransactionHeader::getId)
                        .collect(Collectors.toList())
                    )
            .stream()
            .collect( // Convert to map: key is transaction id, value is payoutType / project / sub project combo
                    toMapIgnoreDups(tuple -> tuple.getA(),
                            tuple -> new Tuple3<String, String, String>(
                                    tuple.getB(),
                                    tuple.getC(),
                                    tuple.getD()
                                )
                        )
            );
        logger.info("CultureNextFinancialInformationProvider map has been obtained - ln 71");
        // loop through records and process them.
        transactionEarningsResultCollection.stream()
            .forEach(ter -> {
                    // Get transaction header ID
                    TransactionHeader transactionHeader = ter.getTransactions().stream().findFirst().get().getHeader();
                    
                    // get payoutType / project / sub project combo
                    Tuple3<String, String, String> payoutTypeprojectSubProjectCombo = transactionToPayoutTypeProjectSubProjectMap.get(transactionHeader.getId());
                    String earningsTypeCode = payoutTypeprojectSubProjectCombo.getA();
                    String projectNumber = payoutTypeprojectSubProjectCombo.getB();
                    String subProjectNumber = payoutTypeprojectSubProjectCombo.getC();
                    
                    
                    // set financial information                
                    setFinancialInformation(ter, earningsTypeCode, projectNumber, subProjectNumber);
                });
        
        // We've processed things, so return it but log it first
        logger.info("CultureNextFinancialInformationProvider transaction earnings collection processed - ln 90");
        return transactionEarningsResultCollection;
    }
}
