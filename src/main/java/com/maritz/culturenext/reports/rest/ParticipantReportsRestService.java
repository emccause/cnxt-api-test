package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.ParticipantReportsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/reports", description = "detailed participant reports")
public class ParticipantReportsRestService {
    
    public static final String PAX_TRANSACTIONS_DETAIL_REPORT_PATH = "reports/pax-transactions-detail";
    public static final String PAX_SUMMARY_REPORT_PATH = "reports/pax-summary";
    public static final String MY_ACTIVITY_REPORT_PATH = "participants/{" + PAX_ID_REST_PARAM + "}/reports/pax-transactions-detail";

    @Inject private ParticipantReportsService participantReportsService;
    @Inject private Security security;

    //rest/reports/pax-transactions-detail
    @PreAuthorize("@security.hasRole('ADMIN','MGR')")
    @RequestMapping(value = PAX_TRANSACTIONS_DETAIL_REPORT_PATH, method = RequestMethod.GET, produces="text/csv")
    @ApiOperation(value = "Returns a CSV report of participant transaction details.")
    @Permission("PUBLIC")
    public void getParticipantTransactionsCsv(HttpServletResponse response,
            @ApiParam(value="the start date for the query")
                @RequestParam(value=FROM_DATE_REST_PARAM, required=true) String fromDate,
            @ApiParam(value="the end date for the query")
                @RequestParam(value=THRU_DATE_REST_PARAM, required=false) String thruDate,
            @ApiParam(value="paxId of the logged in user")
                @RequestParam(value=PAX_ID_REST_PARAM, required=false) String paxId,
            @ApiParam(value="groupId to specify the audience for which to pull data")
                @RequestParam(value=GROUP_ID_REST_PARAM, required=false) String groupId,
            @ApiParam(value="specifies audience as the paxID specified and any of their direct reports")
                @RequestParam(value=PAX_TEAM_REST_PARAM, required=false) String paxTeam,
            @ApiParam(value="specifies audience as the paxID specified and anyone under them in the hierarchy")
                @RequestParam(value=PAX_ORG_REST_PARAM, required=false) String paxOrg,
            @ApiParam(value="comma separated list of recognition type. Values are GIVEN or RECEIVED")
                @RequestParam(value=TYPE_REST_PARAM, required=true) String type,
            @ApiParam(value="comma separated list of recognition statuses. Values are APPROVED, REJECTED, PENDING, ISSUED")
                @RequestParam(value=STATUS_REST_PARAM, required=true) String status,
            @ApiParam(value="comma separated list of program types. "
                + "Values are PEER_TO_PEER, ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, and AWARD_CODE")
                @RequestParam(value=PROGRAM_TYPE_REST_PARAM, required=false) String programType,
            @ApiParam(value="specific programId for which to pull data for")
                @RequestParam(value=PROGRAM_ID_REST_PARAM, required=false) String programId,
            @ApiParam(value="optional fields tied to the recipient."
                + "Fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, "
                + "TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,"
                + "GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5")
                @RequestParam(value=RECIPIENT_FIELDS_REST_PARAM, required=false) String recipientFields,
            @ApiParam(value="optional fields tied to the submitter. Same fields as recipientFields.")
                @RequestParam(value=SUBMITTER_FIELDS_REST_PARAM, required=false) String submitterFields,
            @ApiParam(value="optional fields tied to the approver. Same fields as recipientFields.")
                @RequestParam(value=APPROVER_FIELDS_REST_PARAM, required=false) String approverFields
            ) throws Throwable {
        participantReportsService.getParticipantTransactionsCsv(
                response, fromDate, thruDate, null, groupId, paxTeam, paxOrg, type, status, 
                programType, programId, recipientFields, submitterFields, approverFields);
    }

    //rest/reports/pax-summary
    @PreAuthorize("@security.hasRole('ADMIN','MGR')")
    @RequestMapping(value = PAX_SUMMARY_REPORT_PATH, method = RequestMethod.GET, produces="text/csv")
    @ApiOperation(value = "Returns a CSV report summary of participant activity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getParticipantSummaryReport(HttpServletResponse response,
            @ApiParam(value="Comma delimited list of pax to include in audience")
            @RequestParam(value=PAX_ID_REST_PARAM, required=false)
                    String commaSeparatedPaxList,
            @ApiParam(value="Comma delimited list of pax to include direct reports in audience")
            @RequestParam(value=PAX_TEAM_REST_PARAM, required=false)
                    String commaSeparatedTeamList,
            @ApiParam(value="Comma delimited list of pax to include entire hierarchy in audience")
            @RequestParam(value=PAX_ORG_REST_PARAM, required=false)
                    String commaSeparatedOrgList,
            @ApiParam(value="Comma delimited list of groups to include members in audience")
            @RequestParam(value=GROUP_ID_REST_PARAM, required=false)
                    String commaSeparatedGroupList,
            @ApiParam(value="Date to find data after. " +
                    "If not provided will include all data since start of program")
            @RequestParam(value=FROM_DATE_REST_PARAM, required=false)
                    String fromDateString,
            @ApiParam(value="Date to find data before. " +
                    "If not provided will include all data through the time of the request")
            @RequestParam(value=THRU_DATE_REST_PARAM, required=false)
                    String thruDateString,
            @ApiParam(value="Comma delimited list of statuses to limit data to. " +
                    "If not provided, will include all statuses")
            @RequestParam(value=STATUS_REST_PARAM, required=false)
                    String commaSeparatedRecStatusList,
            @ApiParam(value="Comma delimited list of recognitition types, values are 'GIVEN' and 'RECEIVED'. " +
                    "If not provided, will include both")
            @RequestParam(value=TYPE_REST_PARAM, required=false)
                    String commaSeparatedRecTypeList,
            @ApiParam(value="Comma delimited list of program types to filter on. " +
                    "If not provided, will include all types")
            @RequestParam(value=PROGRAM_TYPE_REST_PARAM, required=false)
                    String commaSeparatedProgramTypeList,
            @ApiParam(value="Comma delimited list of program ids to filter on. " +
                    "If not provided, will include all programs (limited by program type param)")
            @RequestParam(value=PROGRAM_ID_REST_PARAM, required=false)
                    String commaSeparatedProgramIdList
    ) throws Throwable {
        participantReportsService.getParticipantSummaryReport(
                response, commaSeparatedPaxList, commaSeparatedTeamList, commaSeparatedOrgList,
                commaSeparatedGroupList, fromDateString, thruDateString, commaSeparatedRecStatusList,
                commaSeparatedRecTypeList, commaSeparatedProgramTypeList, commaSeparatedProgramIdList
        );
    }
    
    //rest/participants/~/reports/pax-transactions-detail
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = MY_ACTIVITY_REPORT_PATH, method = RequestMethod.GET, produces="text/csv")
    @ApiOperation(value = "Returns a CSV report of participant transaction details.")
    @Permission("PUBLIC")
    public void getMyActivityReportCsv(HttpServletResponse response,
            @ApiParam(value="the start date for the query")
                @RequestParam(value=FROM_DATE_REST_PARAM, required=true) String fromDate,
            @ApiParam(value="the end date for the query")
                @RequestParam(value=THRU_DATE_REST_PARAM, required=false) String thruDate,
            @ApiParam(value="paxId to specify the audience for which to pull data")
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value="comma separated list of recognition type. Values are GIVEN or RECEIVED")
                @RequestParam(value=TYPE_REST_PARAM, required=true) String type,
            @ApiParam(value="comma separated list of recognition statuses. Values are APPROVED, REJECTED, PENDING, ISSUED")
                @RequestParam(value=STATUS_REST_PARAM, required=true) String status,
            @ApiParam(value="comma separated list of program types. "
                + "Values are PEER_TO_PEER, ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, and AWARD_CODE")
                @RequestParam(value=PROGRAM_TYPE_REST_PARAM, required=false) String programType,
            @ApiParam(value="specific programId for which to pull data for")
                @RequestParam(value=PROGRAM_ID_REST_PARAM, required=false) String programId,
            @ApiParam(value="optional fields tied to the recipient."
                + "Fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, "
                + "TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,"
                + "GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5")
                @RequestParam(value=RECIPIENT_FIELDS_REST_PARAM, required=false) String recipientFields,
            @ApiParam(value="optional fields tied to the submitter. Same fields as recipientFields.")
                @RequestParam(value=SUBMITTER_FIELDS_REST_PARAM, required=false) String submitterFields,
            @ApiParam(value="optional fields tied to the approver. Same fields as recipientFields.")
                @RequestParam(value=APPROVER_FIELDS_REST_PARAM, required=false) String approverFields
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        participantReportsService.getParticipantTransactionsCsv(
                response, fromDate, thruDate, paxId, null, null, null, type, status, 
                programType, programId, recipientFields, submitterFields, approverFields);
    }
}
