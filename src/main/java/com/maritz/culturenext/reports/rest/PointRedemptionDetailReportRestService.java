package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.PointRedemptionDetailReportService;

@RestController
@Api(value = "/reports", description = "all calls dealing with reports")
@RequestMapping("reports")
public class PointRedemptionDetailReportRestService {

    @Inject private PointRedemptionDetailReportService pointRedemptionDetailReportService;
    
    //rest/reports/point-redemption-detail
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = "point-redemption-detail", method = RequestMethod.GET, produces="text/csv")
    @ApiOperation(value = "Returns a CSV report of point redemption details from ABS")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getPointRedemptionDetailReport(HttpServletResponse response,
            @ApiParam(value="groupId to include members in audience")
            @RequestParam(value = GROUP_ID_REST_PARAM, required=false) String groupId,
            @ApiParam(value="Date to find data after.")
            @RequestParam(value = FROM_DATE_REST_PARAM, required=false) String fromDateString,
            @ApiParam(value="Date to find data before. " +
                    "If not provided will include all data through the time of the request")
            @RequestParam(value = THRU_DATE_REST_PARAM, required=false) String thruDateString,
            @ApiParam(value="Optional parameters to include for pax")
            @RequestParam(value = RECIPIENT_FIELDS_REST_PARAM, required=false)
                String commaSeparatedOptionalFieldsList
            ) throws Throwable {
        pointRedemptionDetailReportService.getPointRedemptionDetailReport(
                    response, commaSeparatedOptionalFieldsList, groupId,
                        fromDateString, thruDateString
                );
    }
}