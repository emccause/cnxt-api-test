package com.maritz.culturenext.reports.rest;

import io.swagger.annotations.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.RecognitionReportService;
import com.maritz.culturenext.reports.service.ReportsQueryService;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/reports", description = "all calls dealing with recognitions")

public class ReportsQueryRestService {

    @Inject private RecognitionReportService recognitionReportService;
    @Inject private ReportsQueryService reportsQueryService;
    @Inject private Security security;
    
    //rest/participants/query
    @RequestMapping(value = "participants/query", method = RequestMethod.POST)
    @ApiOperation(value="Returns a query id (persistent query) for retrieving a list of all programs (the union) "
            + "that any participants in the query are eligible for (as givers or receivers).", 
            notes="It can also be used for retrieving a list of all budgets(the union) that any participants in "
                    + "the query have given or have received points from.<br><br>'query' in the URI indicates that"
                    + " the API consumer is sending a list of users/groups, type, etc. that they are wanting the "
                    + "details for.<br><br>Only return the programs that are in 'ACTIVE' status. i.e programs that "
                    + "are 'ACTIVE' and from-date is not in future.<br><br>This endpoint can be used for admin &"
                    + " manager reports details.<br><br>The 'members' property can include a list of groupIds or "
                    + "paxids for which to retrieve the list of programs for. The participants will be resolved from"
                    + " the groups.<br><br>The 'directReports' property will be used if you want retrieve the programs"
                    + " for just the direct-reports for the manager.<br><br>The 'orgReports' property will be used if "
                    + "you want retrieve the programs for all paxids under Manager's hierarchy/organization.. "
                    + "So either the 'directReoprts' or 'orgReports' will be specified , but not more both of these "
                    + "properties.<br><br>If all 3 fields are empty, you will get back data for ALL participants "
                    + "active during the date range.")
    @Permission("PUBLIC")
    public QueryIdDto getQuery( 
            @ApiParam(value = "Body containing the specifics of your search")
            @RequestBody ReportsQueryRequestDto query) throws Throwable {
        
        return reportsQueryService.getQueryId(query);         
    }
    
    //rest/programs-by-query-id
    @RequestMapping(value = "programs-by-query-id", method = RequestMethod.GET)
    @ApiOperation(value="Returns a list of all programs (the union) that any participants in the query "
            + "are eligible for (as givers or receivers)", notes="'queryId' param is specific to the request-params"
                    + " that they previously requested the report details for. The queryId will expire when the"
                    + " cache expires and will be invalid then. If other endpoints that require programs-list for"
                    + " the same queryId are invoked before this endpoint, then those endpoints will internally call"
                    + " this service first to populate the programs-list for the original request-params"
                    + " (date & audience)<br><br>This endpoint can be used for admin & manager reports details.")
    @Permission("PUBLIC")
    public List<Map<String, Object>> getProgramList( 
            @ApiParam(value="the Id of the query that is specific to a prior request")
            @RequestParam(value = QUERY_ID_REST_PARAM, required=true) String cacheId,
            @ApiParam(value = "language code for program translations", required = false) 
            @RequestParam (value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode) throws Throwable {
        
        return reportsQueryService.getProgramsForQuery(cacheId, languageCode);
    }
    
    //rest/budgets-by-query-id
    @RequestMapping(value = "budgets-by-query-id", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a list of all budgets(the union) that any participants in the query that "
            + "have given or have received points from.", notes = "The simple budgets & the distributed children"
                    + " budgets are retrieved.<br><br>'queryId' param is specific to the request-params "
                    + "(like dates-ranges & audience) that they previously requested the report details for.. "
                    + "The queryId will expire when the cache expires and will be invalid then.  If other endpoints"
                    + " that require budgets-list for the same queryId are invoked before this endpoint, then those"
                    + " endpoints will internally call this service first to populate the budgets-list for the "
                    + "original request-params (date & audience)<br><br>Only return the budgets that are in 'ACTIVE'"
                    + " status for the period requested.<br><br>This endpoint can be used for "
                    + "admin budget reports details.")
    @Permission("PUBLIC")
    public List<BudgetDTO> getBudgetList( 
            @ApiParam(value="the Id of the query that is specific to a prior request")
            @RequestParam(value = QUERY_ID_REST_PARAM, required=true) String cacheId) throws Throwable {
        
        return reportsQueryService.getBudgetsForQuery(cacheId);         
    }
    
    //rest/participants-by-query-id
    @RequestMapping(value = "participants-by-query-id", method = RequestMethod.GET)
    @ApiOperation(value = "Fetch participants with the specified search criteria query(persistent query)"
            + " that was requested and cached earlier with a queryId.", notes = "This can be used for populating"
                    + " a list of direct-reports on manager-reports pages. This endpoint will use the query-param"
                    + " 'queryId' returned by the prior endpoint (/rest/participants/query) that caches items like"
                    + " final-pax list, programs, budgets etc. based on requested criteria like date-range, audience."
                    + " The 'directReports' property will be used on the prior endpoint (/rest/participants/query)"
                    + " to indicate a request to fetch the directReports of the specified manager(s). The 'members'"
                    + " property can be populated with the manager-paxId on the prior endpoint "
                    + "(/rest/participants/query) if we need to retrieve the Manager in addition to their"
                    + " 'directReports' list (for report with data for 'me & my direct-reports).")
    @Permission("PUBLIC")
    public List<EmployeeDTO> getPaxList( 
            @ApiParam(value="the Id of the query that is specific to a prior request")
            @RequestParam(value = QUERY_ID_REST_PARAM, required=true) String cacheId, 
            @ApiParam(value = "boolean with 'true' value can used to retrieve/filter only participants with"
                    + " non-zero giving limit in their associated budgets.", required = false)
            @RequestParam(value = HAS_NON_ZERO_GIVING_LIMIT_REST_PARAM, required=false) String hasNonZeroGivingLimit
            ) throws Throwable {
        
        return reportsQueryService.getPaxList(cacheId, hasNonZeroGivingLimit);
    }

    //rest/reports/recognitions
    @RequestMapping(value = "reports/recognitions", method = RequestMethod.GET)
    @ApiOperation(value = "Returns CSV file of recognition stats based off the filters provided by the query params.",
    notes = "Only Admin & Managers have access. All of the query params are mandatory except for groupId. If no data"
            + " is found, an empty report will be returned.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 403, message = "User is not admin or a manager"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getRecognitionStats (HttpServletResponse response,
        @ApiParam(value = "The action of the participant. They either gave the recognition or approved the recognition")
    @RequestParam(value = RECOGNITION_TYPE_REST_PARAM, required = true) String recognitionType,
        @ApiParam(value = "The status of the recognition. This can be sent as a comma separated list")
    @RequestParam(value = STATUS_REST_PARAM, required = true) List<String> statusList,
        @ApiParam(value = "From date")@RequestParam(value = FROM_DATE_REST_PARAM, required = true) String fromDateString,
        @ApiParam(value = "To date")@RequestParam(value = TO_DATE_REST_PARAM, required = true) String toDateString,
        @ApiParam(value = "Group Id")@RequestParam(value = GROUP_ID_REST_PARAM, required = false) String groupIdString
    ) {

        Long paxId = security.getPaxId();
        if(paxId == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Long groupId = security.getId(groupIdString, null);

        return new ResponseEntity<Object>(recognitionReportService.getApprovalsReport(response, recognitionType,
                statusList, fromDateString, toDateString, groupId), HttpStatus.OK);
    }
}
