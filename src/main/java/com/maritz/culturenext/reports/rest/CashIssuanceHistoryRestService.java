package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.reports.dto.CashIssuanceHistoryDTO;
import com.maritz.culturenext.reports.dto.PendingCashIssuanceDTO;
import com.maritz.culturenext.reports.service.CashIssuanceHistoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = ProjectConstants.REPORTS_URI_BASE)
@Api(value = "reports", description = "all calls dealing with recognitions")

public class CashIssuanceHistoryRestService {
    
    private static final String PENDING_CASH_PATH = "/pending-cash-issuance";
    private static final String CASH_HISTORY_PATH = "/cash-issuance-history";
    
    @Inject private CashIssuanceHistoryService cashIssuanceHistoryService;
    
    //rest/reports/pending-cash-issuance
    @RequestMapping(value = PENDING_CASH_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Get a summation of currently pending cash issuances")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned pending cash issuance")
    })
    @Permission("PUBLIC")
    public PendingCashIssuanceDTO getPendingCashIssuance() {        
        return cashIssuanceHistoryService.getPendingCashIssuance(); 
    }
    
    //rest/reports/cash-issuance-history
    @RequestMapping(value = CASH_HISTORY_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Get a paginated list of all cash issuances made")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned cash issuance history")
    })
    @Permission("PUBLIC")
    public PaginatedResponseObject<CashIssuanceHistoryDTO> getCashIssuanceHistory(
        @ApiParam(value="page of cash issuance history used to paginate request.", required = false)
            @RequestParam(value = RestParameterConstants.PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
        @ApiParam(value="The number of records per page.",required = false)
            @RequestParam(value = RestParameterConstants.PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(ProjectConstants.REPORTS_URI_BASE + CASH_HISTORY_PATH)
                .setPageNumber(pageNumber)
                .setPageSize(pageSize);
        
        return cashIssuanceHistoryService.getCashIssuanceHistory(requestDetails, pageNumber, pageSize);
    }
}
