package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.TransactionAuditDetailReportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/reports", description = "all calls dealing with transaction reports")
@RequestMapping("reports")
public class TransactionAuditDetailCsvReportRestService {
    @Inject private  TransactionAuditDetailReportService transactionAuditDetailCsvReportService;
    
    //rest/reports/transaction-audit-detail
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = "transaction-audit-detail", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the Transaction Audit Detail Report will help admin look at points which have not been sent to ABS."
            + "This report can only be accessed by admins.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 403, message = "User is not admin or a manager"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getTransactionAuditDetailReport( HttpServletResponse response,
            @ApiParam(value="Comma delimited list of valid transaction statuses to filter on")
            @RequestParam(value = STATUS_REST_PARAM, required=false) String statusList) throws Throwable {
        transactionAuditDetailCsvReportService.createTransactionAuditDetailReport(response, statusList);
    }
}
