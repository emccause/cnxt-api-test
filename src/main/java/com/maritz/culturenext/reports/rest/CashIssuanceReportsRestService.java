package com.maritz.culturenext.reports.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_ID_REST_PARAM;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.CashIssuanceReportsService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/reports", description = "detailed participant reports")
@RequestMapping("reports")
public class CashIssuanceReportsRestService {

    @Inject private CashIssuanceReportsService cashIssuanceReportsService;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    // Service endpoint paths
    public static final String CASH_ISSUANCE_WITH_BATCH_ID_PATH = "cash-issuance/batch-id/{" + BATCH_ID_REST_PARAM + "}";
    public static final String CASH_ISSUANCE_PATH = "cash-issuance";
    

    //rest/reports/cash-issuance/batch-id/{batchId}
    @RequestMapping(value = CASH_ISSUANCE_WITH_BATCH_ID_PATH, method = RequestMethod.GET, produces="text/csv")
    @ApiOperation(value = "Returns a CSV report of cash issuance details.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getCashIssuanceById(HttpServletResponse response,
        @ApiParam(value = "The id of the batch you want to retrieve")
        @PathVariable(BATCH_ID_REST_PARAM) String batchIdString
    ) throws Throwable {
        Long batchId = convertPathVariablesUtil.getId(batchIdString);    
        cashIssuanceReportsService.getCashIssuanceReportById(response, batchId);
    }

    //rest/reports/cash-issuance/
    @RequestMapping(value = CASH_ISSUANCE_PATH, method = RequestMethod.POST)
    @ApiOperation(value = "Create cash issuance report.")
    @Permission("PUBLIC")
    public void createCashIssuanceById(HttpServletResponse response) throws Throwable {
            cashIssuanceReportsService.createCashIssuanceReport(response);
    }
    
    //rest/reports/cash-issuance/batch-id/{batchId}
    @RequestMapping(value = CASH_ISSUANCE_WITH_BATCH_ID_PATH, method = RequestMethod.PUT)
    @ApiOperation(value = "Update cash issuance report.")
    @Permission("PUBLIC")
    public void updateCashIssuanceById(HttpServletResponse response,
        @ApiParam(value = "The id of the batch you want to retrieve")
        @PathVariable(BATCH_ID_REST_PARAM) String batchIdString
    ) throws Throwable {
            Long batchId = convertPathVariablesUtil.getId(batchIdString);    
            cashIssuanceReportsService.updateCashIssuanceReportById(response, batchId);
    }
}
