package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.PendingEmailsReportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/reports", description = "held emails report")
@RequestMapping("reports")
public class PendingEmailsReportRestService {

    //public static final String HELD_EMAILS_REPORT_PATH = "reports/held-emails";

    @Inject private PendingEmailsReportService pendingEmailsReportService;

    //rest/reports/held-emails
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = "held-emails", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a CSV report summary of emails pending to be sent")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 403, message = "User is not admin or a manager"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getPendingEmailsReport(HttpServletResponse response) throws Throwable {
        pendingEmailsReportService.getPendingEmailsReport(response);
    }

}
