package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.service.BudgetCsvReportService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/reports", description = "all calls dealing with budget reports")
@RequestMapping("reports")
public class BudgetCsvReportRestService {
    @Inject private BudgetCsvReportService budgetReportService;
    
    //rest/reports/giving-limit-summary
    @PreAuthorize("@security.hasRole('ADMIN','MGR','BUDGET_OWNER')")
    @RequestMapping(value = "giving-limit-summary", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a CSV report of budgets with giving limit established. "
            + "This report can only be accessed by admins.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 403, message = "User is not admin or a manager"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getBudgetGivingLimitReport( HttpServletResponse response,
            @ApiParam(value="Comma delimited list of valid budget statuses to filter on")
            @RequestParam(value=STATUS_REST_PARAM, required=false) String status,
            @ApiParam(value="Comma delimited list of valid budget IDs to filter on")
            @RequestParam(value=BUDGET_ID_REST_PARAM, required=false) String budgetId) throws Throwable {

        budgetReportService.createBudgetGivingLimitReport(response, budgetId, status);
    }
    
    //rest/reports/budget-summary
    @PreAuthorize("@security.hasRole('ADMIN','MGR','BUDGET_OWNER')")
    @RequestMapping(value = "budget-summary", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a CSV report with budget summary. "
            + "This report can only be accessed by admins.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", responseContainer = "text/csv" ),
            @ApiResponse(code = 403, message = "User is not admin or a manager"),
            @ApiResponse(code = 400, message = "Request Error")
    })
    @Permission("PUBLIC")
    public void getBudgetSummaryReport( HttpServletResponse response,
            @ApiParam(value="Comma delimited list of valid budget statuses to filter on")
            @RequestParam(value=STATUS_REST_PARAM, required=false) String statusList,
            @ApiParam(value="Comma delimited list of valid budget IDs to filter on")
            @RequestParam(value=BUDGET_ID_REST_PARAM, required=false) String budgetIdList) throws Throwable {
        
        budgetReportService.createBudgetSummaryReport(response, budgetIdList, statusList);
    }
    
    //rest/reports/budget-transaction-detail
    @PreAuthorize("@security.hasRole('ADMIN','MGR','BUDGET_OWNER')")
    @RequestMapping(value = "budget-transaction-detail", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a CSV report of budget transaction detail to the status and budget id. This report can only be accessed by admins.")
    @Permission("PUBLIC")
    public void getPointsReport(HttpServletResponse response,
            @ApiParam(value="status is a list of valid budget statuses to filter on (ACTIVE, INACTIVE, "
                  + "ARCHIVED, SCHEDULED, ENDED)")
            @RequestParam(value = STATUS_REST_PARAM, required = false) String status,
            @ApiParam(value="budget id is a list of budgetIds to be filter on ")
            @RequestParam(value = BUDGET_ID_REST_PARAM, required = false) String budgetId,
            @ApiParam(value="optional fields tied to the recipient."
                  + "Fields are EMAIL, CITY, STATE, POSTAL_CODE,"
                  + "COUNTRY_CODE, TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,"
                  + "GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5")
            @RequestParam(value=RECIPIENT_FIELDS_REST_PARAM, required=false) String recipientFields,
            @ApiParam(value="optional fields tied to the submitter. Same fields as recipientFields.")
            @RequestParam(value=SUBMITTER_FIELDS_REST_PARAM, required=false) String submitterFields,
            @ApiParam(value="optional fields tied to the approver. Same fields as recipientFields.")
            @RequestParam(value=APPROVER_FIELDS_REST_PARAM, required=false) String approverFields
    ) throws Throwable {
        budgetReportService.getBudgetTransactionDetailsCSV(response, status, budgetId, recipientFields, submitterFields, approverFields);
    }
}
