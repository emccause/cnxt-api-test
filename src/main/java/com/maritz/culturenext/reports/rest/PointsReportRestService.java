package com.maritz.culturenext.reports.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.reports.dto.PointsReportRequestDTO;
import com.maritz.culturenext.reports.service.PointsReportService;
import com.maritz.culturenext.reports.util.DeliveryMethodTypes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "/reports", description = "all calls dealing with reports")
@RequestMapping("reports")
public class PointsReportRestService {

    @Inject private PointsReportService pointsReportService;

    //rest/reports/point-transactions/query
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM')")
    @RequestMapping(value = "point-transactions/query", method = RequestMethod.POST)
    @ApiOperation(value = "Returns a query id (persistent query) for report of points that have been given or"
            + " received with approved status. This report can only be accessed by admins.")
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPointsReportQuery(
            @ApiParam(value = "Body containing report type (GIVEN or RECEIVED), delivery method (EMAIL/CSV), "
                    + "fields to show and search filter by from date, to date and group id.") 
            @RequestBody PointsReportRequestDTO reportRequest)
                    throws Throwable {

        if (reportRequest == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // TODO MP-4339 (This is not a tech debt, but part of an improvement)
        // remove this validation when email delivery method is implemented
        if (DeliveryMethodTypes.EMAIL.toString().equals(reportRequest.getDeliveryMethod())) {
            return new ResponseEntity<Object>(HttpStatus.NOT_IMPLEMENTED);
        }
        
        return new ResponseEntity<Object>(pointsReportService.getQueryId(reportRequest),HttpStatus.OK);
    }

    //rest/reports/point-transactions
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM')")
    @RequestMapping(value = "point-transactions", method = RequestMethod.GET)
    @ApiOperation(value = "Returns a CSV report of points according to the query id. This report can "
            + "only be accessed by admins.")
    @Permission("PUBLIC")
    public void getPointsReport(HttpServletResponse response,
            @ApiParam(value="the Id of the query that is specific to a prior request")
    @RequestParam(value = QUERY_ID_REST_PARAM, required=true) String cacheId)
                    throws Throwable {

        if (cacheId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        pointsReportService.generatePointsReport(response, cacheId);
    }
}
