package com.maritz.culturenext.reports.constants;

public class PointsReportConstants {
    
    // Standard CSV headers
    public static final String ISSUANCE_DATE_HEADER = "Issuance Date";
    public static final String BUDGET_NAME_HEADER = "Issuing Budget Name";
    public static final String BUDGET_ID_HEADER = "Issuing Budget Accounting ID";
    public static final String POINTS_AWARDED_HEADER = "Points Awarded";
    public static final String PROGRAM_NAME_HEADER = "Issuing Program Name";
    
    // Recipient fields headers
    public static final String REC_PAX_ID_HEADER = "Recipient ID";
    public static final String REC_FIRST_NAME_HEADER = "Recipient First Name";
    public static final String REC_LAST_NAME_HEADER = "Recipient Last Name";
    
    // Submitter fields headers
    public static final String SUB_PAX_ID_HEADER = "Submitter ID";
    public static final String SUB_FIRST_NAME_HEADER = "Submitter First Name";
    public static final String SUB_LAST_NAME_HEADER = "Submitter Last Name";

    public static final String REC_PREFIX_ENROLL_HEADER = "Recipient's ";
    public static final String SUB_PREFIX_ENROLL_HEADER = "Submitter's ";    

}
