package com.maritz.culturenext.reports.constants;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.constants.ProjectConstants;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReportConstants {
    
    // errors
    public static final String ERR_THRU_DATE_BEFORE_FROM_DATE = "THRU_DATE_BEFORE_FROM_DATE";
    public static final String ERR_THRU_DATE_BEFORE_FROM_DATE_MSG = "Thru date must be after from date";
    public static final String ERROR_FROM_DATE_INVALID = "FROM_DATE_INVALID";
    public static final String ERROR_FROM_DATE_INVALID_MSG = "From Date is invalid";
    public static final String ERROR_THRU_DATE_INVALID = "THRU_DATE_INVALID";
    public static final String ERROR_THRU_DATE_INVALID_MSG = "Thru Date is invalid";
    public static final String ERROR_MISSING_RECOGNITION_TYPE_CODE = "ERROR_MISSING_RECOGNITION_TYPE";
    public static final String ERROR_MISSING_RECOGNITION_TYPE_MSG = "A recognition type is required";
    public static final String ERROR_MISSING_STATUS_TYPE_CODE = "MISSING_STATUS";
    public static final String ERROR_MISSING_STATUS_TYPE_MSG = "A status is required";
    public static final String ERROR_MISSING_FROM_DATE_TYPE_CODE = "MISSING_FROM_DATE";
    public static final String ERROR_MISSING_FROM_DATE_TYPE_MSG = "A from date is required";
    public static final String ERROR_MISSING_TO_DATE_TYPE_CODE = "MISSING_TO_DATE";
    public static final String ERROR_MISSING_TO_DATE_TYPE_MSG = "A to date is required";
    public static final String ERROR_INVALID_RECOGNITION_TYPE_CODE = "INVALID_RECOGNTION_TYPE";
    public static final String ERROR_INVALID_RECOGNITION_TYPE_MSG = "The recognition type is not valid";
    public static final String ERROR_INVALID_STATUS_TYPE_CODE = "INVALID_STATUS";
    public static final String ERROR_INVALID_STATUS_TYPE_MSG = "A status is not valid";
    public static final String ERROR_INVALID_FROM_DATE_TYPE_CODE = "INVALID_FROM_DATE";
    public static final String ERROR_INVALID_FROM_DATE_TYPE_MSG = "A from date is not valid";
    public static final String ERROR_INVALID_TO_DATE_TYPE_CODE = "INVALID_TO_DATE";
    public static final String ERROR_INVALID_TO_DATE_TYPE_MSG = "A to date is not valid";
    public static final String ERROR_FROM_DATE_IS_AFTER_TO_DATE_CODE = "FROM_DATE_IS_AFTER_TO_DATE";
    public static final String ERROR_FROM_DATE_IS_AFTER_TO_DATE_MSG = "The from date is after the to date";
    public static final String ERROR_NOT_ADMIN_OR_MANAGER_CODE_CODE = "NOT_ADMIN_OR_MANAGER";
    public static final String ERROR_NOT_ADMIN_OR_MANAGER_MSG = "Logged in user is not an admin nor Manager";
    public static final String ERROR_REPORT_TYPE_INVALID = "INVALID_REPORT_TYPE";
    public static final String ERROR_REPORT_TYPE_INVALID_MSG = "The report type is not valid";
    public static final String ERROR_REPORT_TYPE_MISSING = "MISSING_REPORT_TYPE";
    public static final String ERROR_REPORT_TYPE_MISSING_MSG = "Report type parameter is required";
    public static final String ERROR_DELIVERY_METHOD_INVALID = "INVALID_DELIVERY_METHOD";
    public static final String ERROR_DELIVERY_METHOD_INVALID_MSG = "The delivery method is not valid";
    public static final String ERROR_DELIVERY_EMAIL_MISSING = "MISSING_EMAIL_TYPE";
    public static final String ERROR_DELIVERY_EMAIL_MISSING_MSG = "An email is required for EMAIL delivery method";
    public static final String ERROR_RECIPIENT_FIELDS_INVALID = "INVALID_RECIPIENT_FIELDS";
    public static final String ERROR_RECIPIENT_FIELDS_INVALID_MSG = "At least one of the recipient fields is not valid";
    public static final String ERROR_SUBMITTER_FIELDS_INVALID = "INVALID_SUBMITTER_FIELDS";
    public static final String ERROR_SUBMITTER_FIELDS_INVALID_MSG = "At least one of the submitter fields is not valid";
    public static final String ERROR_GROUP_ID_INVALID = "INVALID_GROUP_ID";
    public static final String ERROR_GROUP_ID_INVALID_MSG = "The to group id is not valid";
    public static final String ERROR_MISSING_SUBCLIENT_PRODUCT_CODE ="MISSING_SUBCLIENT_OR_PRODUCT";
    public static final String SUBCLIENT_PRODUCT = "SUBCLIENT_PRODUCT";
    public static final String ERROR_MISSING_SUBCLIENT_PRODUCT_MSG ="Subclient and Product Code are required";
    public static final String ERROR_MISSING_PAX_IDS_CODE = "MISSING_PAX_IDS";
    public static final String ERROR_MISSING_PAX_IDS_MSG = "Pax id list is required to get budget stats.";
    public static final String ERROR_QUERYID_INVALID = "QUERYID_INVALID";
    public static final String ERROR_QUERYID_INVALID_MSG = "Query is invalid or expired";
    public static final String ERROR_QUERYID_NOT_SUPPLIED = "QUERYID_NOT_SUPPLIED";
    public static final String ERROR_QUERYID_NOT_SUPPLIED_MSG = "QueryId is required";
    public static final String ERROR_QUERY_NOT_CREATED_BY_LOGGED_IN_PAX_CODE = "QUERY_NOT_CREATED_BY_LOGGED_IN_PAX";
    public static final String ERROR_QUERY_NOT_CREATED_BY_LOGGED_IN_PAX_MESSAGE = "Query was not created by logged in pax.";
    
    public static final String GIVEN = "GIVEN";
    public static final String DENIED = "DENIED";

    public static final List<String> VALID_RECOGNITION_TYPE = new ArrayList<>(Arrays.asList(GIVEN, 
            StatusTypeCode.APPROVED.toString(), StatusTypeCode.AUTO_APPROVED.toString()));
    public static final List<String> VALID_STATUS = new ArrayList<>(Arrays.asList(StatusTypeCode.PENDING.toString(),
            StatusTypeCode.APPROVED.toString(), StatusTypeCode.AUTO_APPROVED.toString(), DENIED));

    public static final CellProcessor[] PROCESSOR = new CellProcessor[] {
            new NotNull(), // Recognition Date
            new NotNull(), // Submitter First Name
            new NotNull(), // Submitter Last Name
            new NotNull(), // Recipient First Name
            new NotNull(), // Recipient Last Name
            new Optional(new ParseDouble()), // Amount Given or Received
            null, // Budget Name
            new NotNull(), // Status
            null, // Approver First Name
            null, // Approver Last Name
            null, // Program Name
            null, // Program Header
            null, // Private Message
            null, // Reason comment
            null // Date of denied or approved
    };

    public static final String[] HEADERS = {"RecognitionDate", "SubmitterFirstName", "SubmitterLastName", 
            "RecipientFirstName","RecipientLastName", "Amount", "BudgetName", "Status", "ApproverFirstName",
            "ApproverLastName", "ProgramName","ProgramHeader", "PrivateMessage", "DenialReason",  "ApprovalDate"};

    public static final String FILE_NAME = "ApprovalReport";

    public static final int FIRST_RECORD = 0;

    //Valid custom fields
    public static final List<String> VALID_CUSTOM_FIELDS = Arrays.asList(
                "EMAIL", "CITY", "STATE", "POSTAL_CODE", "COUNTRY_CODE", "TITLE", "COMPANY_NAME", "DEPARTMENT",
                "COST_CENTER", "FUNCTION", "GRADE", "AREA", "CUSTOM_1", "CUSTOM_2", "CUSTOM_3", "CUSTOM_4", "CUSTOM_5"
            );
    
    // errors
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR = "PARAM_ONLY_SUPPORTS_DIGITS";
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE = "That param only supports ids as digits (0-9), separated by commas.";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR = "PARAM_ONLY_SUPPORTS_LETTERS";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR_MESSAGE = "That param only supports strings (A-Z, a-z), separated by commas.";
    
    public static final String ERROR_INVALID_RECIPIENT_FIELD = "INVALID_RECIPIENT_FIELD";
    public static final String ERROR_INVALID_FIELD_MSG = "%s is invalid.";
    
    // Point Redemption detail report constants
    
    public static final String POINT_REDEMPTION_DETAIL_REPORT_FILE_NAME = "Point Redemption Detail Report";
    
    // report headers
    public static final String PROJECT_NUMBER_HEADER = "Project No";
    public static final String ACCOUNTING_ID_HEADER = "Accounting ID";
    public static final String TRANSACTION_ID_HEADER = "Transaction ID";
    public static final String TRANSACTION_DATE_HEADER = "Transaction Date";
    public static final String TRANSACTION_CONFIRMATION_DATE_HEADER = "Transaction Confirmation Date";
    public static final String PARTICIPANT_ID_HEADER = "Participant ID";
    public static final String PARTICIPANT_FIRST_NAME_HEADER = "Participant First Name";
    public static final String PARTICIPANT_MIDDLE_NAME_HEADER = "Participant Middle Name";
    public static final String PARTICIPANT_LAST_NAME_HEADER = "Participant Last Name";
    public static final String POINTS_USED_HEADER = "Points Used";
    
    // query result headers
    public static final String PROJECT_NUMBER_QUERY_HEADER = "projectNumber";
    public static final String ACCOUNTING_ID_QUERY_HEADER = "subprojectNbr";
    public static final String TRANSACTION_ID_QUERY_HEADER = "transId";
    public static final String TRANSACTION_DATE_QUERY_HEADER = "transDate";
    public static final String TRANSACTION_CONFIRMATION_DATE_QUERY_HEADER = "confirmationDate";
    public static final String PARTICIPANT_ID_QUERY_HEADER = "controlNum";
    public static final String PARTICIPANT_FIRST_NAME_QUERY_HEADER = "firstName";
    public static final String PARTICIPANT_MIDDLE_NAME_QUERY_HEADER = "middleName";
    public static final String PARTICIPANT_LAST_NAME_QUERY_HEADER = "lastName";
    public static final String POINTS_USED_QUERY_HEADER = "amount";
    
    public static final List<CsvColumnPropertiesDTO> POINT_REDEMPTION_DETAIL_REPORT_COLUMN_MAPPING = Arrays.asList(
                new CsvColumnPropertiesDTO(PROJECT_NUMBER_HEADER, PROJECT_NUMBER_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(ACCOUNTING_ID_HEADER, ACCOUNTING_ID_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(TRANSACTION_ID_HEADER, TRANSACTION_ID_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(TRANSACTION_DATE_HEADER, TRANSACTION_DATE_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(TRANSACTION_CONFIRMATION_DATE_HEADER, TRANSACTION_CONFIRMATION_DATE_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(PARTICIPANT_ID_HEADER, PARTICIPANT_ID_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(PARTICIPANT_FIRST_NAME_HEADER, PARTICIPANT_FIRST_NAME_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(PARTICIPANT_MIDDLE_NAME_HEADER, PARTICIPANT_MIDDLE_NAME_QUERY_HEADER, null),
                new CsvColumnPropertiesDTO(PARTICIPANT_LAST_NAME_HEADER, PARTICIPANT_LAST_NAME_QUERY_HEADER, new NotNull()),
                new CsvColumnPropertiesDTO(POINTS_USED_HEADER, POINTS_USED_QUERY_HEADER, new ParseDouble())
            );
    
    //ErrorMessage Constants
    public static final ErrorMessage INVALID_QUERY_ID_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.QUERY_ID)
        .setCode(ERROR_QUERYID_INVALID)
        .setMessage(ERROR_QUERYID_INVALID_MSG);
    
    public static final ErrorMessage MISSING_QUERY_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_QUERYID_NOT_SUPPLIED)
            .setField(ProjectConstants.QUERY_ID)
            .setMessage(ERROR_QUERYID_NOT_SUPPLIED_MSG);
    
    public static final ErrorMessage QUERY_NOT_YOURS_ERROR = new ErrorMessage()
            .setCode(ERROR_QUERY_NOT_CREATED_BY_LOGGED_IN_PAX_CODE)
            .setField(ProjectConstants.QUERY_ID)
            .setMessage(ERROR_QUERY_NOT_CREATED_BY_LOGGED_IN_PAX_MESSAGE);
    
    public static final ErrorMessage NOT_ADMIN_OR_MANAGER_CODE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NOT_ADMIN_OR_MANAGER_CODE_CODE)
        .setMessage(ERROR_NOT_ADMIN_OR_MANAGER_MSG);
    
    public static final ErrorMessage MISSING_RECOGNITION_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_RECOGNITION_TYPE_CODE)
        .setField(ProjectConstants.RECOGNITION_TYPE)
        .setMessage(ERROR_MISSING_RECOGNITION_TYPE_MSG);
    
    public static final ErrorMessage INVALID_RECOGNITION_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_RECOGNITION_TYPE_CODE)
        .setField(ProjectConstants.RECOGNITION_TYPE)
        .setMessage(ERROR_INVALID_RECOGNITION_TYPE_MSG);
    
    public static final ErrorMessage MISSING_STATUS_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_STATUS_TYPE_CODE)
        .setField(ProjectConstants.STATUS)
        .setMessage(ERROR_MISSING_STATUS_TYPE_MSG);
    
    public static final ErrorMessage INVALID_STATUS_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_STATUS_TYPE_CODE)
        .setField(ProjectConstants.STATUS)
        .setMessage(ERROR_INVALID_STATUS_TYPE_MSG);
    
    public static final ErrorMessage MISSING_FROM_DATE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_FROM_DATE_TYPE_CODE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ERROR_MISSING_FROM_DATE_TYPE_MSG);
    
    public static final ErrorMessage INVALID_FROM_DATE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_FROM_DATE_TYPE_CODE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ERROR_INVALID_FROM_DATE_TYPE_MSG);
    
    public static final ErrorMessage MISSING_TO_DATE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_TO_DATE_TYPE_CODE)
        .setField(ProjectConstants.TO_DATE)
        .setMessage(ERROR_MISSING_TO_DATE_TYPE_MSG);
    
    public static final ErrorMessage INVALID_TO_DATE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_TO_DATE_TYPE_CODE)
        .setField(ProjectConstants.TO_DATE)
        .setMessage(ERROR_INVALID_TO_DATE_TYPE_MSG);
    
    public static final ErrorMessage INVALID_THRU_DATE_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_THRU_DATE_INVALID)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ERROR_THRU_DATE_INVALID);

    public static final ErrorMessage FROM_DATE_IS_AFTER_TO_DATE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_FROM_DATE_IS_AFTER_TO_DATE_CODE)
        .setField(ProjectConstants.TO_DATE)
        .setMessage(ERROR_FROM_DATE_IS_AFTER_TO_DATE_MSG);

    public static final ErrorMessage REPORT_TYPE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_REPORT_TYPE_MISSING)
        .setField(ProjectConstants.TYPE)
        .setMessage(ERROR_REPORT_TYPE_MISSING_MSG);
    
    public static final ErrorMessage REPORT_TYPE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_REPORT_TYPE_INVALID)
        .setField(ProjectConstants.TYPE)
        .setMessage(ERROR_REPORT_TYPE_INVALID_MSG);
    
    public static final ErrorMessage DELIVERY_EMAIL_MISSING_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DELIVERY_EMAIL_MISSING)
        .setField(ProjectConstants.DELIVERY_EMAIL)
        .setMessage(ERROR_DELIVERY_EMAIL_MISSING_MSG);
    
    public static final ErrorMessage DELIVERY_METHOD_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_DELIVERY_METHOD_INVALID)
        .setField(ProjectConstants.DELIVERY_METHOD)
        .setMessage(ERROR_DELIVERY_METHOD_INVALID_MSG);
    
    public static final ErrorMessage RECIPIENT_FIELDS_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_RECIPIENT_FIELDS_INVALID)
        .setField(ProjectConstants.RECIPIENT_FIELDS)
        .setMessage(ERROR_RECIPIENT_FIELDS_INVALID_MSG);
    
    public static final ErrorMessage SUBMITTER_FIELDS_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_SUBMITTER_FIELDS_INVALID)
        .setField(ProjectConstants.SUBMITTER_FIELDS)
        .setMessage(ERROR_SUBMITTER_FIELDS_INVALID_MSG);
    
    public static final ErrorMessage GROUP_ID_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_GROUP_ID_INVALID)
        .setField(ProjectConstants.GROUP_ID)
        .setMessage(ERROR_GROUP_ID_INVALID_MSG);
    
    public static final ErrorMessage MISSING_SUBCLIENT_PRODUCT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SUBCLIENT_PRODUCT_CODE)
        .setField(SUBCLIENT_PRODUCT)
        .setMessage(ERROR_MISSING_SUBCLIENT_PRODUCT_MSG);
    
    public static final ErrorMessage MISSING_PAX_IDS_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_PAX_IDS_CODE)
            .setField(RestParameterConstants.PAX_IDS_REST_PARAM)
            .setMessage(ERROR_MISSING_PAX_IDS_MSG);
    
    public static final ErrorMessage INVALID_PAX_IDS_CONTENT = new ErrorMessage()
            .setField(RestParameterConstants.PAX_ID_REST_PARAM)
            .setCode(PARAM_ONLY_SUPPORTS_DIGITS_ERROR)
            .setMessage(PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE);

}
