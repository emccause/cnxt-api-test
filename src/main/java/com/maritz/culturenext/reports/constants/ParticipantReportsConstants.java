package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class ParticipantReportsConstants {

    //File names (will be appended with the date)
    public static final String PARTICIPANT_TRANSACTIONS_FILENAME = "Participant_Transactions_Report_";

    //Valid recognition types
    public static final List<String> validTypes = Arrays.asList("GIVEN", "RECEIVED");

    //Valid custom fields
    public static final List<String> validCustomFields = Arrays.asList(
            "EMAIL", "CITY", "STATE", "POSTAL_CODE",
            "COUNTRY_CODE", "TITLE", "COMPANY_NAME", "DEPARTMENT", "COST_CENTER", "FUNCTION",
            "GRADE", "AREA", "CUSTOM_1", "CUSTOM_2", "CUSTOM_3", "CUSTOM_4", "CUSTOM_5");

    //Report Headers - Standard Fields
    public static final String TRANSACTION_ID_HEADER = "Transaction ID";
    public static final String NOMINATION_ID_HEADER = "Nomination ID";
    public static final String SUBMITTED_DATE_HEADER = "Submitted Date";
    public static final String RECIPIENT_ID_HEADER = "Recipient's ID";
    public static final String RECIPIENT_FIRST_NAME_HEADER = "Recipient's First Name";
    public static final String RECIPIENT_LAST_NAME_HEADER = "Recipient's Last Name";
    public static final String SUBMITTER_ID_HEADER = "Submitter's ID";
    public static final String SUBMITTER_FIRST_NAME_HEADER = "Submitter's First Name";
    public static final String SUBMITTER_LAST_NAME_HEADER = "Submitter's Last Name";
    public static final String APPROVER_ID_HEADER = "Approver's ID";
    public static final String APPROVER_FIRST_NAME_HEADER = "Approver's First Name";
    public static final String APPROVER_LAST_NAME_HEADER = "Approver's Last Name";
    public static final String AWARD_AMOUNT_HEADER = "Award Amount";
    public static final String PROGRAM_NAME_HEADER = "Program Name";
    public static final String PAYOUT_TYPE_HEADER = "Payout Type";
    public static final String VALUE_HEADER = "Value";
    public static final String HEADLINE_HEADER = "Headline";
    public static final String PROGRAM_TYPE_HEADER = "Program Type";
    public static final String BUDGET_NAME_HEADER = "Budget Name";
    public static final String STATUS_HEADER = "Recognition Status";
    public static final String APPROVAL_DATE_HEADER = "Approval Date";
    public static final String ISSUANCE_DATE_HEADER = "Issuance Date";
    public static final String AWARD_CODE_HEADER = "Award Code";
    public static final String BATCH_ID_HEADER = "Batch Id";
    public static final String PRIVATE_MESSAGE_HEADER = "Direct Message";

    public static final String RECIPIENT_HEADER_PREFIX = "Recipient's ";
    public static final String SUBMITTER_HEADER_PREFIX = "Submitter's ";
    public static final String APPROVER_HEADER_PREFIX = "Approver's ";
    
    //Error Messages
    public static final String ERROR_MISSING_FROM_DATE = "MISSING_FROM_DATE";
    public static final String ERROR_MISSING_FROM_DATE_MSG = "must be specified.";
    public static final String ERROR_INVALID_FROM_DATE_FORMAT = "INVALID_FROM_DATE_FORMAT";
    public static final String ERROR_INVALID_FROM_DATE_FORMAT_MSG = "must be in YYYY-MM-DD format.";
    public static final String ERROR_INVALID_THRU_DATE_FORMAT = "INVALID_THRU_DATE_FORMAT";
    public static final String ERROR_INVALID_THRU_DATE_FORMAT_MSG = "must be in YYYY-MM-DD format.";
    public static final String ERROR_THRU_DATE_BEFORE_FROM_DATE = "THRU_DATE_BEFORE_FROM_DATE";
    public static final String ERROR_THRU_DATE_BEFORE_FROM_DATE_MSG = "must be after fromDate.";
    public static final String ERROR_INVALID_GROUP_ID_FORMAT = "INVALID_GROUP_ID_FORMAT";
    public static final String ERROR_INVALID_GROUP_ID_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_GROUP_ID = "INVALID_GROUP_ID";
    public static final String ERROR_INVALID_GROUP_ID_MSG = "does not exist.";
    public static final String ERROR_INVALID_PAX_TEAM_FORMAT = "INVALID_PAX_TEAM_FORMAT";
    public static final String ERROR_INVALID_PAX_TEAM_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_PAX_TEAM = "INVALID_PAX_TEAM";
    public static final String ERROR_INVALID_PAX_TEAM_MSG = "pax ID does not exist.";
    public static final String ERROR_INVALID_PAX_ORG_FORMAT = "INVALID_PAX_ORG_FORMAT";
    public static final String ERROR_INVALID_PAX_ORG_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_PAX_ORG = "INVALID_PAX_ORG";
    public static final String ERROR_INVALID_PAX_ORG_MSG = "pax ID does not exist.";
    public static final String ERROR_MISSING_TYPE = "MISSING_TYPE";
    public static final String ERROR_MISSING_TYPE_MSG = "must be specified.";
    public static final String ERROR_INVALID_TYPE = "INVALID_TYPE";
    public static final String ERROR_INVALID_TYPE_MSG = "Valid types are GIVEN, RECEIVED.";
    public static final String ERROR_MISSING_STATUS = "MISSING_STATUS";
    public static final String ERROR_MISSING_STATUS_MSG = "must be specified.";
    public static final String ERROR_MISSING_PROGRAM_FILTER = "MISSING_PROGRAM_FILTER";
    public static final String ERROR_MISSING_PROGRAM_FILTER_MSG = "Either programType or programId must be specified.";
    public static final String ERROR_INVALID_PROGRAM_TYPE = "INVALID_PROGRAM_TYPE";
    public static final String ERROR_INVALID_PROGRAM_TYPE_MSG = "Valid program types are PEER_TO_PEER, ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, AWARD_CODE.";
    public static final String ERROR_INVALID_PROGRAM_ID_FORMAT = "INVALID_PROGRAM_ID_FORMAT";
    public static final String ERROR_INVALID_PROGRAM_ID_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_PROGRAM_ID = "INVALID_PROGRAM_ID";
    public static final String ERROR_INVALID_PROGRAM_ID_MSG = "does not exist.";
    public static final String ERROR_INVALID_RECIPIENT_FIELD = "INVALID_RECIPIENT_FIELD";
    public static final String ERROR_INVALID_RECIPIENT_FIELD_MSG = "%s is invalid.";
    public static final String ERROR_INVALID_SUBMITTER_FIELD = "INVALID_SUBMITTER_FIELD";
    public static final String ERROR_INVALID_SUBMITTER_FIELD_MSG = "%s is invalid.";
    public static final String ERROR_INVALID_APPROVER_FIELD = "INVALID_APPROVER_FIELD";
    public static final String ERROR_INVALID_APPROVER_FIELD_MSG = "%s is invalid.";
    public static final String ERROR_INVALID_PAX_ID_FORMAT = "INVALID_PAX_ID_FORMAT";
    public static final String ERROR_INVALID_PAX_ID_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_PAX_ID = "INVALID_PAX_ID";
    public static final String ERROR_INVALID_PAX_ID_MSG = "does not exist.";

    //ErrorMessage Constants
    public static final ErrorMessage MISSING_FROM_DATE_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_MISSING_FROM_DATE)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ParticipantReportsConstants.ERROR_MISSING_FROM_DATE_MSG);
    
    public static final ErrorMessage INVALID_FROM_DATE_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_FROM_DATE_FORMAT)
        .setField(ProjectConstants.FROM_DATE)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_FROM_DATE_FORMAT_MSG);
    
    public static final ErrorMessage INVALID_THRU_DATE_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_THRU_DATE_FORMAT)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_THRU_DATE_FORMAT_MSG);
    
    public static final ErrorMessage THRU_DATE_BEFORE_FROM_DATE_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_THRU_DATE_BEFORE_FROM_DATE)
        .setField(ProjectConstants.THRU_DATE)
        .setMessage(ParticipantReportsConstants.ERROR_THRU_DATE_BEFORE_FROM_DATE_MSG);
    
    public static final ErrorMessage INVALID_GROUP_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID_FORMAT)
        .setField(ProjectConstants.GROUP_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID_FORMAT_MSG);
    
    public static final ErrorMessage INVALID_GROUP_ID_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID)
        .setField(ProjectConstants.GROUP_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID_MSG);
    
    public static final ErrorMessage INVALID_PAX_TEAM_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM_FORMAT)
        .setField(ProjectConstants.PAX_TEAM)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM_FORMAT_MSG);
    
    public static final ErrorMessage INVALID_PAX_TEAM_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM)
        .setField(ProjectConstants.PAX_TEAM)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM_MSG);
    
    public static final ErrorMessage INVALID_PAX_ORG_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG_FORMAT)
        .setField(ProjectConstants.PAX_ORG)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG_FORMAT_MSG);
    
    public static final ErrorMessage INVALID_PAX_ORG_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG)
        .setField(ProjectConstants.PAX_ORG)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG_MSG);
    
    public static final ErrorMessage MISSING_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_MISSING_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ParticipantReportsConstants.ERROR_MISSING_TYPE_MSG);
    
    public static final ErrorMessage INVALID_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_TYPE_MSG);
    
    public static final ErrorMessage MISSING_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_MISSING_STATUS)
        .setField(ProjectConstants.STATUS)
        .setMessage(ParticipantReportsConstants.ERROR_MISSING_STATUS_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_TYPE)
        .setField(ProjectConstants.PROGRAM_TYPE)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_TYPE_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID_FORMAT)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID_FORMAT_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM_ID_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID)
        .setField(ProjectConstants.PROGRAM_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID_MSG);
    
    public static final ErrorMessage INVALID_PAX_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_ID_FORMAT)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_ID_FORMAT_MSG);
        
    public static final ErrorMessage INVALID_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(ParticipantReportsConstants.ERROR_INVALID_PAX_ID)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(ParticipantReportsConstants.ERROR_INVALID_PAX_ID_MSG);
}
