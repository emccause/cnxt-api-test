
package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;


public class TransactionAuditDetailCsvReportConstants {

    public static final String TRANSACTION_DETAIL_REPORT_FILE_NAME= "TRANSACTION_DETAIL_REPORT";
    
    public static final String ERROR_INVALID_STATUS = "ERROR_INVALID_STATUS";
    public static final String ERROR_INVALID_STATUS_MSG = "Invalid status for report: ";
    
    public static final String PAYOUT_ID_HEADER = "Payout ID";
    public static final String PAYOUT_STATUS_DATE_HEADER = "Payout Status Date";
    public static final String PARTICIPANT_ID_HEADER = "Participant ID";
    public static final String PARTICIPANT_FIRST_NAME_HEADER = "Participant First Name";
    public static final String PARTICIPANT_LAST_NAME_HEADER = "Participant Last Name";
    public static final String AWARD_AMOUNT_HEADER = "Award Amount";
    public static final String TRANSACTION_ID_HEADER = "Transaction ID";
    public static final String NOMINATION_ID_HEADER = "Nomination ID";
    public static final String SUBMITTERS_ID_HEADER = "Submitter's ID";
    public static final String SUBMITTERS_ID_FIRST_NAME_HEADER = "Submitter's First Name";
    public static final String SUBMITTERS_ID_LAST_NAME_HEADER = "Submitter's Last Name";
    public static final String BUDGET_NAME_HEADER = "Budget Name";
    public static final String PROJECT_NUMBER_HEADER = "Project #";
    public static final String SUB_PROJECT_NUMBER_HEADER = "Sub-Project # (Payout)";
    public static final String PAYOUT_STATUS_HEADER = "Payout Status";
    public static final String ERROR_MESSAGE_HEADER = "Error Message";
    
    public static final List<String> validStatusTransactionAuditDetailReportDB = Arrays.asList(
            StatusTypeCode.PROCESSED.name(), StatusTypeCode.ERROR.name(), StatusTypeCode.REQUESTED.name());
    
    public static final List<String> validStatusTransactionAuditDetailReportUI = Arrays.asList(
            StatusTypeCode.PENDING.name(), StatusTypeCode.ERROR.name(), StatusTypeCode.REQUESTED.name());

    public final static List<CsvColumnPropertiesDTO> TRANSACTION_DETAIL_CSV_COLUMNS_PROPS = Arrays.asList(
            new CsvColumnPropertiesDTO(PAYOUT_ID_HEADER, ProjectConstants.PAYOUT_ID, new ParseLong())
            , new CsvColumnPropertiesDTO(PAYOUT_STATUS_DATE_HEADER, ProjectConstants.PAYOUT_STATUS_DATE, 
                new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1)))
            , new CsvColumnPropertiesDTO(PARTICIPANT_ID_HEADER, ProjectConstants.PARTICIPANT_ID, null)
            , new CsvColumnPropertiesDTO(PARTICIPANT_FIRST_NAME_HEADER, ProjectConstants.PARTICIPANT_FIRST_NAME, null)
            , new CsvColumnPropertiesDTO(PARTICIPANT_LAST_NAME_HEADER, ProjectConstants.PARTICIPANT_LAST_NAME, null)
            , new CsvColumnPropertiesDTO(AWARD_AMOUNT_HEADER, ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble()))
            , new CsvColumnPropertiesDTO(TRANSACTION_ID_HEADER, ProjectConstants.TRANSACTION_ID, null)
            , new CsvColumnPropertiesDTO(NOMINATION_ID_HEADER, ProjectConstants.NOMINATION_ID, null)
            , new CsvColumnPropertiesDTO(SUBMITTERS_ID_HEADER, ProjectConstants.SUBMITTER_ID, null)
            , new CsvColumnPropertiesDTO(SUBMITTERS_ID_FIRST_NAME_HEADER, ProjectConstants.SUBMITTER_FIRST_NAME, null)
            , new CsvColumnPropertiesDTO(SUBMITTERS_ID_LAST_NAME_HEADER, ProjectConstants.SUBMITTER_LAST_NAME, null)
            , new CsvColumnPropertiesDTO(BUDGET_NAME_HEADER, ProjectConstants.BUDGET_NAME, null)
            , new CsvColumnPropertiesDTO(PROJECT_NUMBER_HEADER, ProjectConstants.PROJECT_NUMBER, null)
            , new CsvColumnPropertiesDTO(SUB_PROJECT_NUMBER_HEADER, ProjectConstants.SUB_PROJECT_NUMBER, null)
            , new CsvColumnPropertiesDTO(PAYOUT_STATUS_HEADER, ProjectConstants.STATUS, null)
            , new CsvColumnPropertiesDTO(ERROR_MESSAGE_HEADER, ProjectConstants.PAYOUT_ERROR_DESC, null));
}