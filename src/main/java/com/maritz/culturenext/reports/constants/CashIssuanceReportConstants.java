package com.maritz.culturenext.reports.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class CashIssuanceReportConstants {

    public static final String CASH_REPORT = "CASH_REPORT";
    public static final String ERROR_INVALID_BATCH_ID = "INVALID_BATCH_ID";
    public static final String ERROR_INVALID_BATCH_ID_MSG = "does not exist or incorrect status.";
    public static final String ERROR_PROCESSING_FILE = "ERROR_PROCESSING_FILE";
    public static final String ERROR_PROCESSING_FILE_MSG = "Error processing report";
    public static final String ERROR_CREATE_ISSUANCE_REPORT_MSG = "Error in method createCashIssuanceReport: ";
    
    public static final ErrorMessage INVALID_BATCH_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_BATCH_ID)
            .setField(ProjectConstants.BATCH_ID)
            .setMessage(ERROR_INVALID_BATCH_ID_MSG);
    
    public static final ErrorMessage ERROR_PROCESSING_FILE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_PROCESSING_FILE)
            .setField(CASH_REPORT)
            .setMessage(ERROR_PROCESSING_FILE_MSG);
}
