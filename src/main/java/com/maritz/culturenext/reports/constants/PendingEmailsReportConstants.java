package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class PendingEmailsReportConstants {

    // errors
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR = "PARAM_ONLY_SUPPORTS_DIGITS";
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE = "That param only supports ids as digits (0-9), separated by commas.";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR = "PARAM_ONLY_SUPPORTS_LETTERS";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR_MESSAGE = "That param only supports strings (A-Z, a-z), separated by commas.";
    public static final String DATE_FORMAT_INCORRECT_ERROR = "DATE_FORMAT_INCORRECT";
    public static final String DATE_FORMAT_INCORRECT_ERROR_MESSAGE = "Date format is " + DateConstants.ISO_8601_DATE_ONLY_FORMAT;

    public static final String REPORT_FILE_NAME = "Held_Emails_Report";

    //report headers
    public static final String EMAIL_MESSAGE_SENT_DATE_HEADER = "EMAIL MESSAGE SENT DATE";
    public static final String RECOGNITION_CREATE_DATE_HEADER = "RECOGNITION CREATE DATE";
    public static final String RECOGNITION_ID_HEADER = "RECOGNITION ID";
    public static final String EMAIL_REFERENCE_FK_HEADER = "EMAIL REFERENCE FK";
    public static final String SUBMITTER_PAX_ID_HEADER = "SUBMITTER PAX ID";
    public static final String SUBMITTER_CONTROL_NUMBER_HEADER = "SUBMITTER CONTROL NUM";
    public static final String SUBMITTER_FIRST_NAME_HEADER = "SUBMITTER FIRST NAME";
    public static final String SUBMITTER_LAST_NAME_HEADER = "SUBMITTER LAST NAME";
    public static final String RECEIVER_PAX_ID_HEADER = "RECEIVER PAX ID";
    public static final String RECEIVER_CONTROL_NUMBER_HEADER = "RECEIVER CONTROL NUM";
    public static final String RECEIVER_FIRST_NAME_HEADER = "RECEIVER FIRST NAME";
    public static final String RECEIVER_LAST_NAME_HEADER = "RECEIVER LAST NAME";
    public static final String PROGRAM_ID_HEADER = "PROGRAM ID";
    public static final String PROGRAM_NAME_HEADER = "PROGRAM NAME";
    public static final String BUDGET_ID_HEADER = "BUDGET ID";
    public static final String BUDGET_NAME_HEADER = "BUDGET NAME";
    public static final String POINTS_AMOUNT_HEADER = "POINTS GIVEN";
    public static final String HEADLINE_COMMENT_HEADER = "HEADLINE COMMENT";
    public static final String COMMENT_HEADER = "COMMENTS";
    public static final String PROJECT_NUMBER_HEADER = "PROJECT NUMBER";
    public static final String SUBPROJECT_NUMBER_HEADER = "SUBPROJECT NUMBER";
    public static final String COUNTRY_CODE_HEADER = "COUNTRY CODE";
    public static final String ECARD_ID_HEADER = "ECARD ID";

    //query result headers
    public static final String EMAIL_MESSAGE_SENT_DATE_QUERY_HEADER = "EMAIL_MESSAGE_SENT_DATE";
    public static final String RECOGNITION_CREATE_DATE_QUERY_HEADER = "RECOGNITION_CREATE_DATE";
    public static final String RECOGNITION_ID_QUERY_HEADER = "RECOGNITION_ID";
    public static final String EMAIL_REFERENCE_FK_QUERY_HEADER = "REFERENCE_FK";
    public static final String SUBMITTER_PAX_ID_QUERY_HEADER = "SUBMITTER_PAX_ID";
    public static final String SUBMITTER_CONTROL_NUMBER_QUERY_HEADER = "SUBMITTER_CONTROL_NUM";
    public static final String SUBMITTER_FIRST_NAME_QUERY_HEADER = "SUBMITTER_FIRST_NAME";
    public static final String SUBMITTER_LAST_NAME_QUERY_HEADER = "SUBMITTER_LAST_NAME";
    public static final String RECEIVER_PAX_ID_QUERY_HEADER = "RECEIVER_PAX_ID";
    public static final String RECEIVER_CONTROL_NUMBER_QUERY_HEADER = "RECEIVER_CONTROL_NUM";
    public static final String RECEIVER_FIRST_NAME_QUERY_HEADER = "RECEIVER_FIRST_NAME";
    public static final String RECEIVER_LAST_NAME_QUERY_HEADER = "RECEIVER_LAST_NAME";
    public static final String PROGRAM_ID_QUERY_HEADER = "PROGRAM_ID";
    public static final String PROGRAM_NAME_QUERY_HEADER = "PROGRAM_NAME";
    public static final String BUDGET_ID_QUERY_HEADER = "BUDGET_ID";
    public static final String BUDGET_NAME_QUERY_HEADER = "BUDGET_NAME";
    public static final String POINTS_AMOUNT_QUERY_HEADER = "POINTS";
    public static final String HEADLINE_COMMENT_QUERY_HEADER = "HEADLINE_COMMENT";
    public static final String COMMENT_QUERY_HEADER = "COMMENT";
    public static final String PROJECT_NUMBER_QUERY_HEADER = "PROJECT_NUMBER";
    public static final String SUBPROJECT_NUMBER_QUERY_HEADER = "SUBPROJECT_NUMBER";
    public static final String COUNTRY_CODE_QUERY_HEADER = "COUNTRY_CODE";
    public static final String ECARD_ID_QUERY_HEADER = "ECARD_ID";

    public static final List<CsvColumnPropertiesDTO> PENDING_EMAILS_REPORT_COLUMN_MAPPING = Arrays.asList(
            new CsvColumnPropertiesDTO(EMAIL_MESSAGE_SENT_DATE_HEADER, EMAIL_MESSAGE_SENT_DATE_QUERY_HEADER , null),
            new CsvColumnPropertiesDTO(RECOGNITION_CREATE_DATE_HEADER, RECOGNITION_CREATE_DATE_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(RECOGNITION_ID_HEADER, RECOGNITION_CREATE_DATE_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(EMAIL_REFERENCE_FK_HEADER, EMAIL_REFERENCE_FK_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(SUBMITTER_PAX_ID_HEADER, SUBMITTER_PAX_ID_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(SUBMITTER_CONTROL_NUMBER_HEADER, SUBMITTER_CONTROL_NUMBER_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(SUBMITTER_FIRST_NAME_HEADER, SUBMITTER_FIRST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(SUBMITTER_LAST_NAME_HEADER, SUBMITTER_LAST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(RECEIVER_PAX_ID_HEADER, RECEIVER_PAX_ID_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(RECEIVER_CONTROL_NUMBER_HEADER, RECEIVER_CONTROL_NUMBER_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(RECEIVER_FIRST_NAME_HEADER, RECEIVER_FIRST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(RECEIVER_LAST_NAME_HEADER,RECEIVER_LAST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(PROGRAM_ID_HEADER, PROGRAM_ID_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(PROGRAM_NAME_HEADER, PROGRAM_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(BUDGET_ID_HEADER, BUDGET_ID_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(BUDGET_NAME_HEADER, BUDGET_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(POINTS_AMOUNT_HEADER, POINTS_AMOUNT_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(HEADLINE_COMMENT_HEADER, HEADLINE_COMMENT_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(COMMENT_HEADER, COMMENT_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(PROJECT_NUMBER_HEADER, PROJECT_NUMBER_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(SUBPROJECT_NUMBER_HEADER, SUBPROJECT_NUMBER_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(COUNTRY_CODE_HEADER, COUNTRY_CODE_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(ECARD_ID_HEADER, ECARD_ID_QUERY_HEADER, null));

    //Error Message Constants
    public static final ErrorMessage FROM_DATE_FORMAT_INCORRECT_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.FROM_DATE)
            .setCode(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR)
            .setMessage(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR_MESSAGE);

    public static final ErrorMessage THRU_DATE_FORMAT_INCORRECT_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.THRU_DATE)
            .setCode(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR)
            .setMessage(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR_MESSAGE);

}

