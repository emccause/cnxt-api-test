package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class ParticipantSummaryReportConstants {
    
    // errors
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR = "PARAM_ONLY_SUPPORTS_DIGITS";
    public static final String PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE = "That param only supports ids as digits (0-9), separated by commas.";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR = "PARAM_ONLY_SUPPORTS_LETTERS";
    public static final String PARAM_ONLY_SUPPORTS_LETTERS_ERROR_MESSAGE = "That param only supports strings (A-Z, a-z), separated by commas.";
    public static final String DATE_FORMAT_INCORRECT_ERROR = "DATE_FORMAT_INCORRECT";
    public static final String DATE_FORMAT_INCORRECT_ERROR_MESSAGE = "Date format is " + DateConstants.ISO_8601_DATE_ONLY_FORMAT;
    
    public static final String REPORT_FILE_NAME = "Participant Summary Report";
    
    // report headers
    public static final String PARTICIPANT_ID_HEADER = "Participant Id";
    public static final String PARTICIPANT_FIRST_NAME_HEADER = "Participant First Name";
    public static final String PARTICIPANT_LAST_NAME_HEADER = "Participant Last Name";
    public static final String COUNTRY_CODE_HEADER = "Country Code";
    public static final String AREA_HEADER = "Area";
    public static final String DEPARTMENT_HEADER = "Department";
    public static final String LOGINS_HEADER = "Logins";
    public static final String RECOGNITIONS_RECEIVED_HEADER = "Recognitions Received";
    public static final String RECOGNITIONS_GIVEN_HEADER = "Recognitions Given";
    public static final String POINTS_RECEIVED_HEADER = "Points Received";
    public static final String POINTS_GIVEN_HEADER = "Points Given";
    public static final String GIVING_LIMIT_TOTAL_HEADER = "Giving Limit Total";
    public static final String GIVING_LIMIT_USED_HEADER = "Giving Limit Used";
    public static final String GIVING_LIMIT_AVAILABLE_HEADER = "Giving Limit Available";
    public static final String GIVING_LIMIT_EXPIRED_HEADER = "Giving Limit Expired";
    
    // query result headers
    public static final String PARTICIPANT_ID_QUERY_HEADER = "CONTROL_NUM";
    public static final String PARTICIPANT_FIRST_NAME_QUERY_HEADER = "FIRST_NAME";
    public static final String PARTICIPANT_LAST_NAME_QUERY_HEADER = "LAST_NAME";
    public static final String COUNTRY_CODE_QUERY_HEADER = "COUNTRY_CODE";
    public static final String AREA_QUERY_HEADER = "AREA";
    public static final String DEPARTMENT_QUERY_HEADER = "DEPARTMENT";
    public static final String LOGINS_QUERY_HEADER = "LOGIN_COUNT";
    public static final String RECOGNITIONS_RECEIVED_QUERY_HEADER = "RECEIVED_COUNT";
    public static final String RECOGNITIONS_GIVEN_QUERY_HEADER = "GIVEN_COUNT";
    public static final String POINTS_RECEIVED_QUERY_HEADER = "POINTS_RECEIVED";
    public static final String POINTS_GIVEN_QUERY_HEADER = "POINTS_GIVEN";
    public static final String GIVING_LIMIT_TOTAL_QUERY_HEADER = "GIVING_LIMIT_TOTAL";
    public static final String GIVING_LIMIT_USED_QUERY_HEADER = "GIVING_LIMIT_USED";
    public static final String GIVING_LIMIT_AVAILABLE_QUERY_HEADER = "GIVING_LIMIT_AVAILABLE";
    public static final String GIVING_LIMIT_EXPIRED_QUERY_HEADER = "GIVING_LIMIT_EXPIRED";
    
    public static final List<CsvColumnPropertiesDTO> PARTICIPANT_SUMMARY_REPORT_COLUMN_MAPPING = Arrays.asList(
            new CsvColumnPropertiesDTO(PARTICIPANT_ID_HEADER, PARTICIPANT_ID_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(PARTICIPANT_FIRST_NAME_HEADER, PARTICIPANT_FIRST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(PARTICIPANT_LAST_NAME_HEADER, PARTICIPANT_LAST_NAME_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(COUNTRY_CODE_HEADER, COUNTRY_CODE_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(AREA_HEADER, AREA_QUERY_HEADER, null),
            new CsvColumnPropertiesDTO(DEPARTMENT_HEADER, DEPARTMENT_QUERY_HEADER , null),
            new CsvColumnPropertiesDTO(LOGINS_HEADER, LOGINS_QUERY_HEADER, new ParseInt()),
            new CsvColumnPropertiesDTO(RECOGNITIONS_RECEIVED_HEADER, RECOGNITIONS_RECEIVED_QUERY_HEADER, new ParseInt()),
            new CsvColumnPropertiesDTO(RECOGNITIONS_GIVEN_HEADER, RECOGNITIONS_GIVEN_QUERY_HEADER, new ParseInt()),
            new CsvColumnPropertiesDTO(POINTS_RECEIVED_HEADER, POINTS_RECEIVED_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(POINTS_GIVEN_HEADER, POINTS_GIVEN_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(GIVING_LIMIT_TOTAL_HEADER, GIVING_LIMIT_TOTAL_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(GIVING_LIMIT_USED_HEADER, GIVING_LIMIT_USED_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(GIVING_LIMIT_AVAILABLE_HEADER, GIVING_LIMIT_AVAILABLE_QUERY_HEADER, new ParseDouble()),
            new CsvColumnPropertiesDTO(GIVING_LIMIT_EXPIRED_HEADER, GIVING_LIMIT_EXPIRED_QUERY_HEADER, new ParseDouble())
            );
    
    //Error Message Constants
    public static final ErrorMessage FROM_DATE_FORMAT_INCORRECT_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.FROM_DATE)
        .setCode(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR)
        .setMessage(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR_MESSAGE);

    public static final ErrorMessage THRU_DATE_FORMAT_INCORRECT_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.THRU_DATE)
        .setCode(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR)
        .setMessage(ParticipantSummaryReportConstants.DATE_FORMAT_INCORRECT_ERROR_MESSAGE);
    
    
}
