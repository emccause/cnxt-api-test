package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.ParseLong;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class BudgetCsvReportConstants {
    
    public static final String BUDGET_GIVING_LIMIT_FILE_NAME = "BUDGET_GIVING_LIMIT_REPORT";
    public static final String BUDGET_SUMMARY_FILE_NAME = "BUDGET_SUMMARY_REPORT";
    
    // Error messages
    public static final String ERROR_INVALID_BUDGET_ID = "ERROR_INVALID_BUDGET_ID";
    public static final String ERROR_INVALID_BUDGET_ID_MSG = "Budget id should contain only digits.";

    // Headers description
    public static final String BUDGET_ID_HEADER = "Budget ID";
    public static final String BUDGET_NAME_HEADER = "Budget Name";
    public static final String BUDGET_DISPLAY_NAME_HEADER = "Budget Display Name";
    public static final String BUDGET_GIVING_LIMIT_HEADER = "Budget Giving Limit";
    public static final String BUDGET_START_DATE_HEADER = "Budget Start Date";
    public static final String BUDGET_END_DATE_HEADER = "Budget End Date";
    public static final String BUDGET_USED_HEADER = "Budget Used";
    public static final String BUDGET_REMAINING_HEADER = "Budget Remaining";
    public static final String PARTICIPANT_ID_HEADER = "Participant ID";
    public static final String PARTICIPANT_FIRST_NAME_HEADER = "Participant's First Name";
    public static final String PARTICIPANT_LAST_NAME_HEADER = "Participant's Last Name";
    public static final String PARTICIPANT_AMOUNT_USED_HEADER = "Participant's Amount Used";
    public static final String PARTICIPANT_STATUS_HEADER = "Participant's Status";

    // Budget Giving Limit report additional fields.
    public static final String BUDGET_GIVING_LIMIT = "budgetGivingLimit";
    public static final String BUDGET_USED = "budgetUsed";
    public static final String BUDGET_REMAINING = "budgetRemaining";
    public static final String PAX_AMOUNT_USED = "participantAmountUsed";    

    // Define CSV report's columns properties (headers description, fieldMapping column's query, column's processors)
    // Budget Giving Limit Report
    public final static List<CsvColumnPropertiesDTO> BUDGET_GIVING_LIMIT_CSV_COLUMNS_PROPS = Arrays.asList(
        new CsvColumnPropertiesDTO(BUDGET_ID_HEADER, ProjectConstants.BUDGET_ID, new ParseLong())
        , new CsvColumnPropertiesDTO(BUDGET_NAME_HEADER, ProjectConstants.BUDGET_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_DISPLAY_NAME_HEADER, ProjectConstants.BUDGET_DISPLAY_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_GIVING_LIMIT_HEADER, BUDGET_GIVING_LIMIT, new ParseDouble())
        , new CsvColumnPropertiesDTO(BUDGET_START_DATE_HEADER, ProjectConstants.START_DATE
        , new Optional(new FmtDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT)))
        , new CsvColumnPropertiesDTO(BUDGET_END_DATE_HEADER, ProjectConstants.END_DATE
        , new Optional(new FmtDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT)))
        , new CsvColumnPropertiesDTO(BUDGET_USED_HEADER, BUDGET_USED, new ParseDouble())
        , new CsvColumnPropertiesDTO(BUDGET_REMAINING_HEADER, BUDGET_REMAINING,(new com.maritz.culturenext.util.ParseDouble()))
        , new CsvColumnPropertiesDTO(PARTICIPANT_ID_HEADER, ProjectConstants.CONTROL_NUM, null)
        , new CsvColumnPropertiesDTO(PARTICIPANT_FIRST_NAME_HEADER, ProjectConstants.FIRST_NAME, null)
        , new CsvColumnPropertiesDTO(PARTICIPANT_LAST_NAME_HEADER, ProjectConstants.LAST_NAME, null)
        , new CsvColumnPropertiesDTO(PARTICIPANT_AMOUNT_USED_HEADER, PAX_AMOUNT_USED, new ParseDouble())
        , new CsvColumnPropertiesDTO(PARTICIPANT_STATUS_HEADER, ProjectConstants.STATUS, null)        
    );
    
    //BUDGET SUMMARY REPORTS
    public static final String BUDGET_TYPE_HEADER = "Budget Type";
    public static final String PARENT_BUDGET_HEADER = "Parent Budget";
    public static final String BUDGET_STATUS_HEADER = "Budget Status";
    public static final String DISTRIBUTED_BUDGET_GROUP_HEADER = "Distributed Budget Group";
    public static final String GROUP_NAME_HEADER = "Group Name";
    public static final String PROJECT_NUMBER_HEADER = "Project Number";
    public static final String ACCOUNTING_ID_HEADER = "Accounting ID";
    public static final String TOTAL_BUDGET_HEADER = "Total Budget";
    public static final String INDIVIDUAL_GIVING_HEADER = "Individual Giving Limit";
    public static final String TOTAL_PARTICIPANTS_HEADER = "Total Participants";
    public static final String BUDGET_PER_PARTICIPANT_HEADER = "Budget Per Participant";
    public static final String BUDGET_OWNER_CONTROL_NUM_HEADER = "Budget Owner Control Number";
    public static final String BUDGET_OWNER_FIRST_NAME_HEADER = "Budget Owner First Name";
    public static final String BUDGET_OWNER_LAST_NAME_HEADER = "Budget Owner Last Name";
    public static final String BUDGET_ALLOCATED_IN_HEADER = "Allocated In";
    public static final String BUDGET_ALLOCATED_OUT_HEADER = "Allocated Out";

    public final static List<CsvColumnPropertiesDTO> BUDGET_SUMMARY_CSV_COLUMNS_PROPS = Arrays.asList(
        new CsvColumnPropertiesDTO(BUDGET_ID_HEADER, ProjectConstants.BUDGET_ID,  null)
        , new CsvColumnPropertiesDTO(BUDGET_TYPE_HEADER, ProjectConstants.BUDGET_TYPE_CODE, null)
        , new CsvColumnPropertiesDTO(PARENT_BUDGET_HEADER, ProjectConstants.PARENT_BUDGET_ID, null)
        , new CsvColumnPropertiesDTO(BUDGET_NAME_HEADER, ProjectConstants.BUDGET_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_DISPLAY_NAME_HEADER, ProjectConstants.BUDGET_DISPLAY_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_START_DATE_HEADER, ProjectConstants.START_DATE
                , new Optional(new FmtDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT)))
        , new CsvColumnPropertiesDTO(BUDGET_END_DATE_HEADER, ProjectConstants.END_DATE
                , new Optional(new FmtDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT)))
        , new CsvColumnPropertiesDTO(BUDGET_STATUS_HEADER, ProjectConstants.STATUS, null)
        , new CsvColumnPropertiesDTO(DISTRIBUTED_BUDGET_GROUP_HEADER, ProjectConstants.DISTRIBUTED_BUDGET_GROUP, null)
        , new CsvColumnPropertiesDTO(GROUP_NAME_HEADER, ProjectConstants.GROUP_NAME, null)
        , new CsvColumnPropertiesDTO(PROJECT_NUMBER_HEADER, ProjectConstants.PROJECT_NUMBER, null)
        , new CsvColumnPropertiesDTO(ACCOUNTING_ID_HEADER, ProjectConstants.ACCOUNTING_ID, null)
        , new CsvColumnPropertiesDTO(TOTAL_BUDGET_HEADER, ProjectConstants.TOTAL_AMOUNT, new Optional( new ParseDouble()))
        , new CsvColumnPropertiesDTO(BUDGET_USED_HEADER, BUDGET_USED, new Optional(new ParseDouble()))
        , new CsvColumnPropertiesDTO(BUDGET_REMAINING_HEADER, BUDGET_REMAINING,  new Optional(new ParseDouble()))
        , new CsvColumnPropertiesDTO(INDIVIDUAL_GIVING_HEADER, ProjectConstants.INDIVIDUAL_GIVING_LIMIT, new Optional(new ParseDouble()))
        , new CsvColumnPropertiesDTO(TOTAL_PARTICIPANTS_HEADER, ProjectConstants.TOTAL_PARTICIPANTS, new Optional(new ParseInt()))
        , new CsvColumnPropertiesDTO(BUDGET_PER_PARTICIPANT_HEADER, ProjectConstants.BUDGET_PER_PARTICIPANT, null)
        , new CsvColumnPropertiesDTO(BUDGET_OWNER_CONTROL_NUM_HEADER, ProjectConstants.BUDGET_OWNER_CONTROL_NUM, null)
        , new CsvColumnPropertiesDTO(BUDGET_OWNER_FIRST_NAME_HEADER, ProjectConstants.BUDGET_OWNER_FIRST_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_OWNER_LAST_NAME_HEADER, ProjectConstants.BUDGET_OWNER_LAST_NAME, null)
        , new CsvColumnPropertiesDTO(BUDGET_ALLOCATED_IN_HEADER, ProjectConstants.BUDGET_ALLOCATED_IN, new Optional(new ParseDouble()))
        , new CsvColumnPropertiesDTO(BUDGET_ALLOCATED_OUT_HEADER, ProjectConstants.BUDGET_ALLOCATED_OUT, new Optional(new ParseDouble())));
}
