package com.maritz.culturenext.reports.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class BudgetTransactionReportConstants {

    public static String BUDGET_TRANSACTION_DETAIL_FILE_NAME = "Budget_Transaction_Detail_Report_";
    
    //Valid recognition statuses
    public static final List<String> validStatuses = Arrays.asList(
                StatusTypeCode.ACTIVE.name(), StatusTypeCode.ARCHIVED.toString(), StatusTypeCode.INACTIVE.name(), 
                StatusTypeCode.ENDED.toString(), StatusTypeCode.SCHEDULED.toString());

        //Valid custom fields
    public static final List<String> validCustomFields = Arrays.asList(
                "EMAIL", "CITY", "STATE", "POSTAL_CODE",
                "COUNTRY_CODE", "TITLE", "COMPANY_NAME", "DEPARTMENT", "COST_CENTER", "FUNCTION",
                "GRADE", "AREA", "CUSTOM_1", "CUSTOM_2", "CUSTOM_3", "CUSTOM_4", "CUSTOM_5");
    
    //Report Headers - Standard Fields
    public static final String TRANSACTION_ID_HEADER = "Transaction ID";
    public static final String NOMINATION_ID_HEADER = "Nomination ID";
    public static final String SUBMITTED_DATE_HEADER = "Submitted Date";
    public static final String RECIPIENT_ID_HEADER = "Recipient's ID";
    public static final String RECIPIENT_FIRST_NAME_HEADER = "Recipient's First Name";
    public static final String RECIPIENT_LAST_NAME_HEADER = "Recipient's Last Name";
    public static final String SUBMITTER_ID_HEADER = "Submitter's ID";
    public static final String SUBMITTER_FIRST_NAME_HEADER = "Submitter's First Name";
    public static final String SUBMITTER_LAST_NAME_HEADER = "Submitter's Last Name";
    public static final String APPROVER_ID_HEADER = "Approver's ID";
    public static final String APPROVER_FIRST_NAME_HEADER = "Approver's First Name";
    public static final String APPROVER_LAST_NAME_HEADER = "Approver's Last Name";
    public static final String AWARD_AMOUNT_HEADER = "Award Amount";
    public static final String PAYOUT_TYPE_HEADER = "Payout Type";
    public static final String PROGRAM_NAME_HEADER = "Program Name";
    public static final String PROGRAM_TYPE_HEADER = "Program Type";
    public static final String BUDGET_NAME_HEADER = "Budget Name";
    public static final String BUDGET_STATUS_HEADER = "Budget Status";
    public static final String APPROVAL_DATE_HEADER = "Approval Date";
    public static final String ISSUANCE_DATE_HEADER = "Issuance Date";
    public static final String AWARD_CODE_HEADER = "Award Code";
    
    public static final String RECIPIENT_HEADER_PREFIX = "Recipient's ";
    public static final String SUBMITTER_HEADER_PREFIX = "Submitter's ";
    public static final String APPROVER_HEADER_PREFIX = "Approver's ";
    
    //Error message
    public static final String ERROR_MISSING_STATUS = "MISSING_STATUS";
    public static final String ERROR_MISSING_STATUS_MSG = "must be specified.";
    public static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    public static final String ERROR_INVALID_STATUS_MSG = " Valid statuses are ACTIVE, INACTIVE, ARCHIVED, SCHEDULED, ENDED.";
    public static final String ERROR_INVALID_BUDGET_ID_FORMAT = "INVALID_BUDGET_ID_FORMAT";
    public static final String ERROR_INVALID_BUDGET_ID_FORMAT_MSG = "must be a number.";
    public static final String ERROR_INVALID_BUDGET_ID = "INVALID_BUDGET_ID";
    public static final String ERROR_INVALID_BUDGET_ID_MSG = "does not exist.";
    public static final String ERROR_INVALID_RECIPIENT_FIELD = "INVALID_RECIPIENT_FIELD";
    public static final String ERROR_INVALID_RECIPIENT_FIELD_MSG = "%s is invalid.";
    public static final String ERROR_INVALID_SUBMITTER_FIELD = "INVALID_SUBMITTER_FIELD";
    public static final String ERROR_INVALID_APPROVER_FIELD = "INVALID_APPROVER_FIELD";
    public static final String ERROR_INVALID_FIELD_MSG = "%s is invalid.";    
    
    //Error Message Constants
    public static final ErrorMessage MISSING_STATUS_MESSAGE = new ErrorMessage()
        .setCode(BudgetTransactionReportConstants.ERROR_MISSING_STATUS)
        .setField(ProjectConstants.STATUS)
        .setMessage(BudgetTransactionReportConstants.ERROR_MISSING_STATUS_MSG);
    
    public static final ErrorMessage INVALID_STATUS_MESSAGE = new ErrorMessage()
        .setCode(BudgetTransactionReportConstants.ERROR_INVALID_STATUS)
        .setField(ProjectConstants.STATUS)
        .setMessage(BudgetTransactionReportConstants.ERROR_INVALID_STATUS_MSG);
    
    public static final ErrorMessage INVALID_BUDGET_ID_FORMAT_MESSAGE = new ErrorMessage()
        .setCode(BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID_FORMAT)
        .setField(ProjectConstants.BUDGET_ID)
        .setMessage(BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID_FORMAT_MSG);

}
