package com.maritz.culturenext.reports.service;

import javax.servlet.http.HttpServletResponse;

public interface CashIssuanceReportsService {
    /**
     * Get the csv cash issuance report
     * @param response: in the response the report is going to be written
     * @param batchIdString: the batch id associated to the report.
     * 
     * https://maritz.atlassian.net/wiki/display/M365/GET+Cash+Issuance+Report
     */
    void getCashIssuanceReportById(HttpServletResponse response, Long batchId);
        
    /**
     * Create the csv cash issuance report and the batchId record
     * 
     * https://maritz.atlassian.net/wiki/display/M365/POST+Cash+Issuance+Report
     */    
    void createCashIssuanceReport(HttpServletResponse response);
    
    /**
     * Update the csv cash issuance report and the batch status to COMPLETE
     * @param response: in the response the report is going to be written
     * @param batchIdString: the batch id associated to the report
     * 
     * https://maritz.atlassian.net/wiki/display/M365/PUT+Cash+Issuance+Report
     */    
    void updateCashIssuanceReportById(HttpServletResponse response, Long batchId);
}