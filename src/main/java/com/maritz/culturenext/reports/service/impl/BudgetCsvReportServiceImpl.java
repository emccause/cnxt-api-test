package com.maritz.culturenext.reports.service.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.ObjectUtils;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.reports.constants.BudgetCsvReportConstants;
import com.maritz.culturenext.reports.constants.BudgetTransactionReportConstants;
import com.maritz.culturenext.reports.dao.BudgetTransactionReportDao;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.service.BudgetCsvReportService;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;
import com.maritz.culturenext.budget.dao.BudgetReportDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.ProjectConstantsEnum;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.ift.CellProcessor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Service
public class BudgetCsvReportServiceImpl implements BudgetCsvReportService {

    @Inject private ConcentrixDao<Budget> budgetDao;
    @Inject private BudgetReportDao budgetReportDao;
    @Inject private BudgetTransactionReportDao budgetTransactionReportDao;
    @Inject private EnrollmentConfigService enrollmentConfigService;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private Security security;
    @Inject private Environment environment;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void createBudgetGivingLimitReport(HttpServletResponse response, String budgetIdsDelim, String statusDelim) {
        
        // Checking for valid delimited budgetId list format (only digits are allowed).
        if (budgetIdsDelim != null && !budgetIdsDelim.isEmpty() 
                && !budgetIdsDelim.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID)
                    .setField(ProjectConstants.BUDGET_ID)
                    .setMessage(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID_MSG));
        }
        
        // If the user is not an admin, we don't want to allow them to pass a null budgetId param (to return all budgets)
        if (!security.hasRole("ADMIN") && budgetIdsDelim == null) {
            budgetIdsDelim = "";
        }

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("createBudgetGivingLimitReport is calling budgetReportDao.getBudgetGivingLimit with the following params. " +
                            "budgetIdList {} statusIdList {} paxId {}"
                    , budgetIdsDelim,statusDelim,security.getPaxId());
        }

        // Get report content
        List<Map<String, Object>> reportContent = budgetReportDao.getBudgetGivingLimit(budgetIdsDelim, statusDelim);

        if(reportsLoggingEnabled && reportContent.get(0).isEmpty()) {
            logger.info("an EMPTY RESULT was retrieved by budgetReportDao.getBudgetGivingLimit with the following params. " +
                            "budgetIdList {} statusIdList {} paxId {}"
                    , budgetIdsDelim,statusDelim,security.getPaxId());
        }else if(reportsLoggingEnabled && !reportContent.get(0).isEmpty()){
            logger.info("createBudgetGivingLimitReport is calling CsvFileUtil.writeDownloadCSVFileFromMap with the following params. " +
                            "budgetGivingFileName {} responseStatus {} budgetGivingLimitColumns {} paxId {}"
                    , BudgetCsvReportConstants.BUDGET_GIVING_LIMIT_FILE_NAME,response.getStatus(),BudgetCsvReportConstants.BUDGET_GIVING_LIMIT_CSV_COLUMNS_PROPS,security.getPaxId());
        }

        // Creating CSV file report
        CsvFileUtil.writeDownloadCSVFileFromMap(response, BudgetCsvReportConstants.BUDGET_GIVING_LIMIT_FILE_NAME
                , reportContent, BudgetCsvReportConstants.BUDGET_GIVING_LIMIT_CSV_COLUMNS_PROPS);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("createBudgetGivingLimitReport execution has been completed in {} MILLISECONDS with the following params. " +
                            "budgetIdList {} statusIdList {} rowsAmount {} paxId {}"
                    ,estimatedTime, budgetIdsDelim,statusDelim,reportContent.size(),security.getPaxId());
        }
    }

    @Override
    public void getBudgetTransactionDetailsCSV(HttpServletResponse response, String statusString, String budgetIdString,
                                               String recipientFields, String submitterFields, String approverFields) {

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        List<String> recipientFieldsList = null;
        List<String> submitterFieldsList = null;
        List<String> approverFieldsList = null;

        //Validate all request parameters
        ArrayList<ErrorMessage> errors = validateReportParams(statusString, budgetIdString ,recipientFields, submitterFields, approverFields);

        //Throw errors before continuing
        ErrorMessageException.throwIfHasErrors(errors);

        if (recipientFields != null) {
            recipientFieldsList = Arrays.asList(recipientFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        if (submitterFields != null) {
            submitterFieldsList = Arrays.asList(submitterFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        if (approverFields!= null) {
            approverFieldsList = Arrays.asList(approverFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        // If the user is not an admin, we don't want to allow them to pass a null budgetId param (to return all budgets)
        if (!security.hasRole("ADMIN") && budgetIdString == null) {
            budgetIdString = "";
        }

        if(reportsLoggingEnabled) {
            logger.info("getBudgetTransactionDetailsCSV is calling budgetTransactionReportDao.getBudgetTransactionReportData with the following params. " +
                            "status {} budgetId {} paxId {}"
                    , statusString, budgetIdString, security.getPaxId());
        }

        List<Map<String, Object>> budgetData =
                budgetTransactionReportDao.getBudgetTransactionReportData(statusString, budgetIdString);

        if(reportsLoggingEnabled) {
            logger.info("BudgetCsvReportServiceImpl.getBudgetTransactionDetailsCSV has successfully called budgetTransactionReportDao.getBudgetTransactionReportData with the following params. " +
                            "status {} budgetId {} paxId {}"
                    , statusString,budgetIdString,security.getPaxId());
        }

        //Grab paxIds to pull data for
        List<Long> paxIds = new ArrayList<>();
        
        for (Map<String, Object> node : budgetData) {
            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);
            Long approverPaxId = (Long) node.get(ProjectConstants.APPROVER_PAX_ID); 
            
            if (!paxIds.contains(recipientPaxId)) {
                paxIds.add(recipientPaxId);
            }
            
            if (!paxIds.contains(submitterPaxId)) {
                paxIds.add(submitterPaxId);
            }
            
            if (!paxIds.contains(approverPaxId)) {
                paxIds.add(approverPaxId);
            }
        }
        
        // take our list of ids and convert it to a comma separated string
        String paxList = StringUtils.formatDelimitedData(ObjectUtils.toStringCollection(paxIds));
        
        //Get all pax info
        if(reportsLoggingEnabled) {
            logger.info("getBudgetTransactionDetailsCSV is calling participantInfoDao.getPaxInfo with the following params. " +
                            "paxList {} paxId {}"
                    , paxList,security.getPaxId());
        }
        List<Map<String, Object>> paxInfoList = participantInfoDao.getPaxInfo(paxList);

        if(reportsLoggingEnabled) {
            logger.info("BudgetCsvReportServiceImpl.getBudgetTransactionDetailsCSV has successfully called participantInfoDao.getPaxInfo with the following params. " +
                            "paxList {} paxId {}"
                    , paxList,security.getPaxId());
        }
        Map<Object,Map<String, Object>> userDataMap = new HashMap<>();        
        for (Map<String, Object> paxInfo : paxInfoList) {
        	userDataMap.put(paxInfo.get(ProjectConstants.PAX_ID),paxInfo);        	
        }
                
        //Combine the maps
        List<Map<String, Object>> reportData = new ArrayList<>();
        reportData.addAll(budgetData);
        for (Map<String, Object> record : reportData) {            
            Long budgetDataRecipientPax = (Long) record.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long budgetDataSubmitterPax = (Long) record.get(ProjectConstants.SUBMITTER_PAX_ID);
            Long budgetDataApproverPax = (Long) record.get(ProjectConstants.APPROVER_PAX_ID);
            
	            if (budgetDataRecipientPax != null){
	            	addPaxInfoToRecord(record, userDataMap.get(budgetDataRecipientPax) , "recipient",recipientFieldsList);
	            }
	            if (budgetDataSubmitterPax != null){
	            	addPaxInfoToRecord(record, userDataMap.get(budgetDataSubmitterPax) , "submitter",submitterFieldsList);
	            }
	            if (budgetDataApproverPax != null){
	            	addPaxInfoToRecord(record, userDataMap.get(budgetDataApproverPax) , "approver",approverFieldsList);
	            }          
                
        }        

        EnrollmentFieldReportUtil enrollmentFieldReportUtil =
                new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());

        //Create CSV report properties : headers, field mappings, processors
        Map<String, Object []> csvReportProps = createCSVReportProperties(reportData,
                enrollmentFieldReportUtil, recipientFieldsList, submitterFieldsList, approverFieldsList);

        String[] headers = (String[]) csvReportProps.get(ProjectConstants.HEADERS);
        String[] fieldMapping = (String[]) csvReportProps.get(ProjectConstants.FIELD_MAPPING);
        CellProcessor[] processor = (CellProcessor []) csvReportProps.get(ProjectConstants.PROCESSORS);

        String date = DateUtil.convertToDateOnlyFormat(new Date(), TimeZone.getDefault());
        String fileName = BudgetTransactionReportConstants.BUDGET_TRANSACTION_DETAIL_FILE_NAME + date;

        // Create CSV file
        CsvFileUtil.writeDownloadCSVFileFromMap(response, fileName, reportData, processor, headers, fieldMapping);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("getBudgetTransactionDetailsCSV execution has been completed in {} MILLISECONDS with the following params. " +
                            "status {} fileName {} rowsAmount {} paxId {}"
                    ,estimatedTime, response.getStatus(),reportData.size(),security.getPaxId());
        }

    }
    
    /**
     * Helper method to map the pax info to the report record
     * @param record - record to add info to
     * @param paxInfo - participant info to add to the record
     * @param prefix - will either be recipient, submitter, or approver
     */
    private Map<String, Object> addPaxInfoToRecord(Map<String, Object> record, Map<String, Object> paxInfo, String prefix, List <String>fieldList){
        
    	if (!isNullOrEmpty(paxInfo)&&fieldList != null){
    		
    		for (String field : fieldList) {    		
    			ProjectConstantsEnum enu = ProjectConstantsEnum.getEnumByName(field);
    			record.put((prefix + enu.label1),  paxInfo.get(enu.label2));
    		}    		
    	}        
        return record;
    }
    
    public boolean isNullOrEmpty(Map<?,?> map) {
        return (map == null || map.isEmpty());
    }
    
    /**
     * Validates all of the report parameters for the Participant Transaction Report
     * Returns a list of errors that will be handled by the calling method
     * @param statusString - comma separated list of recognition statuses. Values are APPROVED, DENIED, PENDING
     * @param budgetIdString - specific budgetId for which to pull data for
     * @param recipientFields - optional fields tied to the recipient.
     *         Fields are FIRST_NAME, LAST_NAME, EMAIL, CITY, STATE, POSTAL_CODE,
     *         COUNTRY_CODE, TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param submitterFields - optional fields tied to the submitter. Same fields as recipientFields.
     * @param approverFields - optional fields tied to the approver. Same fields as recipientFields.
     * @return ArrayList<ErrorMessage> - list of error messages
     */
    private ArrayList<ErrorMessage> validateReportParams(String statusString, String budgetIdString,
                                                         String recipientFields, String submitterFields, String approverFields) {

        ArrayList<ErrorMessage> errors = new ArrayList<>();

        //statusList
        if (statusString == null || statusString.trim().isEmpty()) {
            //status is null
            errors.add(BudgetTransactionReportConstants.MISSING_STATUS_MESSAGE);
        } else {
            List<String> statusList = Arrays.asList(statusString.split(ProjectConstants.COMMA_DELIM));
            //make sure all statuses are valid
            for (String status : statusList) {
                if (!BudgetTransactionReportConstants.validStatuses.contains(status)) {
                    //invalid status
                    errors.add(BudgetTransactionReportConstants.INVALID_STATUS_MESSAGE);
                    break;
                }
            }
        }

        //programId
        if (budgetIdString != null && budgetIdString.trim().length() > 0) {
            List<String> budgetIdList = Arrays.asList(budgetIdString.split(ProjectConstants.COMMA_DELIM));

            for (String budgetIdStr : budgetIdList) {

                Long budgetId = null;
                try {
                    budgetId = Long.valueOf(budgetIdStr);
                } catch (NumberFormatException nfe) {
                    //programIdString is not valid
                    errors.add(BudgetTransactionReportConstants.INVALID_BUDGET_ID_FORMAT_MESSAGE);
                }

                if (budgetId != null && !budgetDao.findBy().where(ProjectConstants.ID).eq(budgetId).exists()) {
                    //program does not exist
                    errors.add(new ErrorMessage()
                            .setCode(BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID)
                            .setField(ProjectConstants.BUDGET_ID)
                            .setMessage("budget id " + budgetId + " " + BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID_MSG));
                }
            }
        }

        //recipientFields
        errors.addAll(validateOptionalFields(
                recipientFields,
                BudgetTransactionReportConstants.ERROR_INVALID_RECIPIENT_FIELD,
                ProjectConstants.RECIPIENT_FIELDS,
                BudgetTransactionReportConstants.ERROR_INVALID_RECIPIENT_FIELD_MSG));


        //submitterFields
        errors.addAll(validateOptionalFields(
                submitterFields,
                BudgetTransactionReportConstants.ERROR_INVALID_SUBMITTER_FIELD,
                ProjectConstants.SUBMITTER_FIELDS,
                BudgetTransactionReportConstants.ERROR_INVALID_FIELD_MSG));


        //approverFields
        errors.addAll(validateOptionalFields(
                approverFields,
                BudgetTransactionReportConstants.ERROR_INVALID_APPROVER_FIELD,
                ProjectConstants.APPROVER_FIELDS,
                BudgetTransactionReportConstants.ERROR_INVALID_FIELD_MSG));

        return errors;
    }


    /**
     * Validates the optional custom fields that can be called for a report
     * Returns a list of errors that will be handled by the calling method
     * @param optionalFields - comma separated list of optional fields to include on the report
     *         Valid fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE,
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param code - value to set for ErrorMessage.code
     * @param field - value to set for ErrorMessage.field
     * @param message - value to set for ErrorMessage.message
     * @return ArrayList<ErrorMessage> - list of error messages
     */
    private ArrayList<ErrorMessage> validateOptionalFields(
            String optionalFields, String code, String field, String message) {

        ArrayList<ErrorMessage> errors = new ArrayList<>();

        if (optionalFields != null && optionalFields.length() != 0) {
            List<String> fieldList = Arrays.asList(optionalFields.split(ProjectConstants.COMMA_DELIM));

            //make sure all optional fields are valid
            for (String item : fieldList) {
                if (!BudgetTransactionReportConstants.validCustomFields.contains(item)) {
                    //invalid custom field
                    errors.add(new ErrorMessage()
                            .setCode(code)
                            .setField(field)
                            .setMessage(String.format(message, item)));
                    break;
                }
            }
        }

        return errors;
    }

    /**
     * Create map for CSV report's arrays properties (headers description, fieldMapping dto object, column's processors),
     * @param data - result set containing all of the report data
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @param recipientFields - Optional recipient information to include on the report
     * @param submitterFields - Optional submitter information to include on the report
     * @param approverFields - Optional approver information to include on the reports
     * @return Map for CSV properties; keys: headers, fieldMapping, processors
     */
    public static Map<String, Object []> createCSVReportProperties(
            List<Map<String, Object>> data, EnrollmentFieldReportUtil enrollmentFieldReportUtil,
            List<String> recipientFields, List<String> submitterFields, List<String> approverFields) {

        List<String> headersArray = new ArrayList<String>();
        List<String> fieldMappingArray = new ArrayList<String>();
        List<CellProcessor> processorArray = new ArrayList<CellProcessor>();

        List<CsvColumnPropertiesDTO> csvColumnsProps =
                createCSVColumnsProperties(data, enrollmentFieldReportUtil, recipientFields, submitterFields, approverFields);

        for (CsvColumnPropertiesDTO columnProps : csvColumnsProps) {
            headersArray.add(columnProps.getHeader());
            fieldMappingArray.add(columnProps.getField());
            processorArray.add(columnProps.getProcessor());
        }

        Map<String, Object []> csvReportPropsMap = new HashMap<String, Object []>();

        // Column's headers
        String[] headers = new String[headersArray.size()];
        headers = (String[]) headersArray.toArray(headers);
        csvReportPropsMap.put(ProjectConstants.HEADERS, headers);

        // Column's dto field mapping
        String[] fieldMapping = new String[fieldMappingArray.size()];
        fieldMapping = (String[]) fieldMappingArray.toArray(fieldMapping);
        csvReportPropsMap.put(ProjectConstants.FIELD_MAPPING, fieldMapping);

        // Column's processor
        CellProcessor[] processor = new CellProcessor[processorArray.size()];
        processor = (CellProcessor[]) processorArray.toArray(processor);
        csvReportPropsMap.put(ProjectConstants.PROCESSORS, processor);

        return csvReportPropsMap;
    }


    /**
     * Create CSV headers, fieldMapping and processor according to standard, request recipient and submitter fields.
     * @param data - result set containing all of the report data
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @param recipientFields - Optional recipient information to include on the report
     * @param submitterFields - Optional submitter information to include on the report
     * @param approverFields - Optional approver information to include on the reports
     * @return List of CSV coumns properties.
     */
    private static List<CsvColumnPropertiesDTO> createCSVColumnsProperties(
            List<Map<String, Object>> data, EnrollmentFieldReportUtil enrollmentFieldReportUtil,
            List<String> recipientFields, List<String> submitterFields, List<String> approverFields) {

        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();

        //Standard Fields - Tracking
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.TRANSACTION_ID_HEADER,
                ProjectConstants.TRANSACTION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.NOMINATION_ID_HEADER,
                ProjectConstants.NOMINATION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.SUBMITTED_DATE_HEADER,
                ProjectConstants.SUBMITTED_DATE, new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1)));

        //Standard Fields - Recipient
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.RECIPIENT_ID_HEADER,
                ProjectConstants.RECIPIENT_ID, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.RECIPIENT_FIRST_NAME_HEADER,
                ProjectConstants.RECIPIENT_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.RECIPIENT_LAST_NAME_HEADER,
                ProjectConstants.RECIPIENT_LAST_NAME, null));

        //Optional Fields - Recipient
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                BudgetTransactionReportConstants.RECIPIENT_HEADER_PREFIX, ProjectConstants.RECIPIENT, recipientFields);

        //Standard Fields - Submitter
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.SUBMITTER_ID_HEADER,
                ProjectConstants.SUBMITTER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.SUBMITTER_FIRST_NAME_HEADER,
                ProjectConstants.SUBMITTER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.SUBMITTER_LAST_NAME_HEADER,
                ProjectConstants.SUBMITTER_LAST_NAME, null));

        //Optional Fields - Submitter
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                BudgetTransactionReportConstants.SUBMITTER_HEADER_PREFIX, ProjectConstants.SUBMITTER, submitterFields);

        //Standard Fields - Approver
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.APPROVER_ID_HEADER,
                ProjectConstants.APPROVER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.APPROVER_FIRST_NAME_HEADER,
                ProjectConstants.APPROVER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.APPROVER_LAST_NAME_HEADER,
                ProjectConstants.APPROVER_LAST_NAME, null));

        //Optional Fields - Approver
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                BudgetTransactionReportConstants.APPROVER_HEADER_PREFIX, ProjectConstants.APPROVER, approverFields);

        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.PROGRAM_NAME_HEADER,
                ProjectConstants.PROGRAM_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.PROGRAM_TYPE_HEADER,
                ProjectConstants.PROGRAM_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.AWARD_AMOUNT_HEADER,
                ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.PAYOUT_TYPE_HEADER,
                ProjectConstants.PAYOUT_TYPE, null));
        //Standard Fields - Other
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.BUDGET_NAME_HEADER,
                ProjectConstants.BUDGET_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.BUDGET_STATUS_HEADER,
                ProjectConstants.BUDGET_STATUS, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.APPROVAL_DATE_HEADER,
                ProjectConstants.APPROVAL_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.ISSUANCE_DATE_HEADER,
                ProjectConstants.ISSUANCE_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.AWARD_CODE_HEADER,
                ProjectConstants.AWARD_CODE, null));

        return csvColumnsProps;
    }

    @Override
    public void createBudgetSummaryReport(HttpServletResponse response, String budgetIdsDelim, String statusList) {

        // Checking for valid delimited budgetId list format (only digits are allowed).
        //if (budgetIdsDelim != null && !budgetIdsDelim.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
        if (budgetIdsDelim != null && !budgetIdsDelim.isEmpty() 
                && !budgetIdsDelim.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID)
                    .setField(ProjectConstants.BUDGET_ID)
                    .setMessage(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID_MSG));
        }
        
        // If the user is not an admin, we don't want to allow them to pass a null budgetId param (to return all budgets)
        if (!security.hasRole("ADMIN") && budgetIdsDelim == null) {
            budgetIdsDelim = "";
        }

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();
        if(reportsLoggingEnabled) {
            logger.info("createBudgetSummaryReport is calling budgetReportDao.getBudgetSummary with the following params. " +
                            "budgetIds {} statusList {} paxId {}"
                    , budgetIdsDelim, statusList, security.getPaxId());
        }

        // Get report content
        List<Map<String, Object>> reportContent = budgetReportDao.getBudgetSummary(budgetIdsDelim,  statusList);

        // Creating CSV file report
        CsvFileUtil.writeDownloadCSVFileFromMap(response, BudgetCsvReportConstants.BUDGET_SUMMARY_FILE_NAME
                , reportContent, BudgetCsvReportConstants.BUDGET_SUMMARY_CSV_COLUMNS_PROPS);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("createBudgetSummaryReport execution has been completed in {} MILLISECONDS with the following params. " +
                            "budgetIdsDelim {} statusIdList {} rowsAmount {} paxId {}"
                    ,estimatedTime, budgetIdsDelim,statusList,reportContent.size(),security.getPaxId());
        }

    }
}
