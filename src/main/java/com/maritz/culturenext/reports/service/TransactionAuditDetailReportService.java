package com.maritz.culturenext.reports.service;

import javax.servlet.http.HttpServletResponse;

public interface TransactionAuditDetailReportService {
    
    /**
     * Downloads a report containing transactional data for records being sent to ABS (or another point bank).
     * It can be filtered on the status of the PAYOUT record tied to the recognition.
     * 
     * @param response - Tells the API to return the response as CSV
     * @param statusList - Comma separated list of payout statuses to filter on.
     *     Values are PENDING, REQUESTED, and ERROR
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/45416509/Transaction+Audit+Detail+Report
     */
    void createTransactionAuditDetailReport(HttpServletResponse response, String statusList);
}
