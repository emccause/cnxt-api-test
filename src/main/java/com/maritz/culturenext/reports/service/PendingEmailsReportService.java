package com.maritz.culturenext.reports.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.maritz.culturenext.reports.dao.impl.ParticipantSummaryReportDaoImpl;

public interface PendingEmailsReportService {


    /**
     * Performs validation and then loads data from the database and returns raw map
     * (split into two methods so that tests don't need to parse the CSV file)
     *
     * @see ParticipantSummaryReportDaoImpl.getParticipantSummaryReport for param info
     *
     * @return List<Map<String, Object>> - Raw list of results in map form
     */
    List<Map<String, Object>> getReportData();


    /**
     * Downloads a CSV report containing emails that are on hold with the data related to recognitions
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841301/Participant+Summary+Report
     */
    void getPendingEmailsReport(HttpServletResponse response);

}
