package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.PendingEmailsReportConstants;
import com.maritz.culturenext.reports.dao.PendingEmailsReportDao;
import com.maritz.culturenext.reports.service.PendingEmailsReportService;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;

@Component
public class PendingEmailsReportServiceImpl implements PendingEmailsReportService {


    @Inject private PendingEmailsReportDao pendingEmailsReportDao;
    @Inject private Environment environment;
    @Inject private Security security;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public List<Map<String, Object>> getReportData() {

	    	/*
	       return participantSummaryReportDao.getParticipantSummaryReport(
	                commaSeparatedPaxList, commaSeparatedTeamList, commaSeparatedOrgList, commaSeparatedGroupList,
	                fromDate, thruDate, commaSeparatedRecStatusList, commaSeparatedRecTypeList,

	               	commaSeparatedProgramTypeList, commaSeparatedProgramIdList
	        ); */
        return pendingEmailsReportDao.getPendingEmailsReport();
    }

    @Override
    public void getPendingEmailsReport(HttpServletResponse response) {

        List<Map<String, Object>> reportData = getReportData();

        StringBuilder fileNameBuilder = new StringBuilder(PendingEmailsReportConstants.REPORT_FILE_NAME);
        fileNameBuilder.append(ProjectConstants.SPACE);
        fileNameBuilder.append(DateUtil.getCurrentTimeInUTCDate());

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("getPendingEmailsReport is calling writeDownloadCSVFileFromMap with the following params. paxId {}",security.getPaxId());
        }

        CsvFileUtil.writeDownloadCSVFileFromMap(
                response, fileNameBuilder.toString(), reportData,
                PendingEmailsReportConstants.PENDING_EMAILS_REPORT_COLUMN_MAPPING
        );

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("getPendingEmailsReport execution has been completed in {} MILLISECONDS with the following params. " +
                            "status {} rowsAmount {} paxId {}"
                    ,estimatedTime,response.getStatus(), reportData.size(),security.getPaxId());
        }
    }
}
