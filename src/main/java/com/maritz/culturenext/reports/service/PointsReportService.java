package com.maritz.culturenext.reports.service;

import javax.servlet.http.HttpServletResponse;

import com.maritz.culturenext.reports.dto.PointsReportRequestDTO;
import com.maritz.culturenext.reports.dto.QueryIdDto;

public interface PointsReportService {
    
    /**
     * Generates the Issuance Report. A queryID is generated and saved in cache to be used to download the report.
     * The cached object will also contain the delivery method (will be either DOWNLOAD or EMAIL).
     * Currently only DOWNLOAD is supported. It will download the Issuance Report as a CSV.
     *  
     * @param requestDto
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/21332009/Create+Issuance+Report
     * 
     * NOTE: I believe this report has been replaced with the Participant Transactions report:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841310/Participant+Transactions+Report
     */
    QueryIdDto getQueryId(PointsReportRequestDTO requestDto) throws Exception;
    
    /**
     * Downloads the Issuance Report (currently only supports CSV).
     * The report will contain data about points that have been given and are in APPROVED status.
     * 
     * @param response
     * @param queryId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20054092/Get+Issuance+Report
     * 
     * NOTE: I believe this report has been replaced with the Participant Transactions report:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841310/Participant+Transactions+Report
     */
    void generatePointsReport(HttpServletResponse response, String queryId);
    
    
    void generatePointsReport(HttpServletResponse response, PointsReportRequestDTO request);

}
