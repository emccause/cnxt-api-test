package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.reports.constants.CashIssuanceReportConstants;
import com.maritz.culturenext.reports.constants.ParticipantReportsConstants;
import com.maritz.culturenext.reports.dao.CashIssuanceReportsDao;
import com.maritz.culturenext.reports.dao.ParticipantReportsDao;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.service.CashIssuanceReportsService;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;

@Component
public class CashIssuanceReportsServiceImpl implements CashIssuanceReportsService{
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private BatchUtil batchUtil;
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;
    @Inject private CashIssuanceReportsDao cashIssuanceReportsDao;
    @Inject private EnrollmentConfigService enrollmentConfigService;
    @Inject private ParticipantReportsDao participantReportsDao;
    @Inject private Environment environment;
    @Inject private Security security;

    private String CASH_ISSUANCE_REPOT = "Cash-Issuance-Report_";
    private String PROGRAM_TYPE_PARAM = "GIVEN,RECEIVED";
    
    @Override
    public void getCashIssuanceReportById(HttpServletResponse response, Long batchId) {
        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("getCashIssuanceReportById is calling batchFileDao.findBy with the following params. " +
                            "batchId {} paxId {}", batchId,security.getPaxId());
        }

        batchUtil.validateCashIssuanceBatchId(batchId);
        BatchFile batchFile = batchFileDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
        CsvFileUtil.writeDownloadCSVFileFromFile(response, batchFile);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("getCashIssuanceReportById execution has been completed in {} MILLISECONDS with the following params. " +
                    "batchId {} batchCrc {} paxId {}",estimatedTime ,batchId,batchFile.getCrc(),security.getPaxId());
        }
    }

    @Override
    public void updateCashIssuanceReportById(HttpServletResponse response, Long batchId) {

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("updateCashIssuanceReportById is calling batchFileDao.findBy with the following params. " +
                    "batchId {} paxId {}", batchId,security.getPaxId());
        }

        validateBatchIdTypeAndStatus(batchId,StatusTypeCode.NEW.name());
        Batch batch = batchDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
        batch.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
        batchDao.update(batch);
        
        BatchFile batchFile = batchFileDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
        if(batchFile!=null){
            CsvFileUtil.writeDownloadCSVFileFromFile(response, batchFile);

            if(reportsLoggingEnabled){
                long estimatedTime = System.currentTimeMillis() - startTime;
                logger.info("updateCashIssuanceReportById execution has been completed in {} MILLISECONDS with the following params. " +
                        "batchId {} batchCrc {} paxId {}",estimatedTime ,batchId,batchFile.getCrc(),security.getPaxId());
            }
        }
    }
    
    @Override
    public void createCashIssuanceReport(HttpServletResponse response) {
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        
        List<Map<String, Object>> processResults = cashIssuanceReportsDao.processCashPayouts();
        Long batchId = null;
        try{
            if(processResults !=null && processResults.size()>0){
                boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
                long startTime = System.currentTimeMillis();

                Map<String, Object> map = processResults.get(0);
                batchId= (Long)map.get(ProjectConstants.BATCH_ID);
                List<Map<String, Object>> reportData = 
                    participantReportsDao.getParticipantTransactionReportData(
                    null, null, null, null, null, null, PROGRAM_TYPE_PARAM, 
                    null, null, null, batchId);
                EnrollmentFieldReportUtil enrollmentFieldReportUtil = 
                    new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());
                
                String date = DateUtil.convertToDateOnlyFormat(new Date(), TimeZone.getDefault());
                String fileName = CASH_ISSUANCE_REPOT + date;
                
                String report = CsvFileUtil.buildCSVFileFromMap(reportData, createParticipantTransactionCsvColumnProps(
                    enrollmentFieldReportUtil));

                if(reportsLoggingEnabled) {
                    logger.info("createCashIssuanceReport is calling cashIssuanceReportsDao.saveCashIssuanceReport with the following params. " +
                            "batchId {} paxId {}", batchId, security.getPaxId());
                }

                List<Map<String, Object>> processSaveResults = cashIssuanceReportsDao.saveCashIssuanceReport(batchId,report,fileName);
                if(processSaveResults == null || processSaveResults.size() == 0 ) {
                    errors.add(CashIssuanceReportConstants.ERROR_PROCESSING_FILE_MESSAGE);
                }
                    
                Batch batch = batchDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
                batch.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
                batchDao.update(batch);
                    
                BatchFile batchFile = batchFileDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
                if( batchFile != null ) {
                    CsvFileUtil.writeDownloadCSVFileFromFile(response, batchFile);
                    if(reportsLoggingEnabled){
                        long estimatedTime = System.currentTimeMillis() - startTime;
                        logger.info("createCashIssuanceReport execution has been completed in {} MILLISECONDS with the following params. " +
                                        " status {} batchId {} paxId {}"
                                ,estimatedTime, response.getStatus(), batchId,security.getPaxId());
                    }
                }
            }
        }catch(Exception e){
            logger.error(CashIssuanceReportConstants.ERROR_CREATE_ISSUANCE_REPORT_MSG+e.getMessage());
            Batch batchError = batchDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
            batchError.setStatusTypeCode(StatusTypeCode.ERROR.name());
            batchDao.update(batchError);
        }
    }
        

    /**
     * Create CSV headers, fieldMapping and processor according to standard, request recipient and submitter fields.
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @return List of CSV coumns properties.
     */
    private static List<CsvColumnPropertiesDTO> createParticipantTransactionCsvColumnProps(
            EnrollmentFieldReportUtil enrollmentFieldReportUtil) {
        
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        
        //Standard Fields - Tracking
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.TRANSACTION_ID_HEADER,
                ProjectConstants.TRANSACTION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.NOMINATION_ID_HEADER,
                ProjectConstants.NOMINATION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTED_DATE_HEADER,
                ProjectConstants.SUBMITTED_DATE, new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1)));
        
        //Standard Fields - Recipient
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_ID_HEADER,
                ProjectConstants.RECIPIENT_ID, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_FIRST_NAME_HEADER,
                ProjectConstants.RECIPIENT_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_LAST_NAME_HEADER,
                ProjectConstants.RECIPIENT_LAST_NAME, null));
        
        //Standard Fields - Submitter
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_ID_HEADER,
                ProjectConstants.SUBMITTER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_FIRST_NAME_HEADER,
                ProjectConstants.SUBMITTER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_LAST_NAME_HEADER,
                ProjectConstants.SUBMITTER_LAST_NAME, null));
    
        //Standard Fields - Approver
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_ID_HEADER,
                ProjectConstants.APPROVER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_FIRST_NAME_HEADER,
                ProjectConstants.APPROVER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_LAST_NAME_HEADER,
                ProjectConstants.APPROVER_LAST_NAME, null));
        
        //Standard Fields - Other
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_AMOUNT_HEADER,
                ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PAYOUT_TYPE_HEADER,
                ProjectConstants.PAYOUT_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_NAME_HEADER,
                ProjectConstants.PROGRAM_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.VALUE_HEADER,
                ProjectConstants.VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.HEADLINE_HEADER,
                ProjectConstants.HEADLINE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_TYPE_HEADER,
                ProjectConstants.PROGRAM_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BUDGET_NAME_HEADER,
                ProjectConstants.BUDGET_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.STATUS_HEADER,
                ProjectConstants.RECOGNITION_STATUS, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVAL_DATE_HEADER,
                ProjectConstants.APPROVAL_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.ISSUANCE_DATE_HEADER,
                ProjectConstants.ISSUANCE_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_CODE_HEADER,
                ProjectConstants.AWARD_CODE, null));
        //BATCH_ID
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BATCH_ID_HEADER,
                ProjectConstants.BATCH_ID, null));
        
        return csvColumnsProps;    
    }

    private void validateBatchIdTypeAndStatus(Long batchId, String status){
        ArrayList<ErrorMessage> errors = new ArrayList<>();

            if (batchId != null && !batchDao.findBy()
                    .where(ProjectConstants.BATCH_ID)
                    .eq(batchId)
                    .and(ProjectConstants.BATCH_TYPE_CODE)
                    .eq(CashIssuanceReportConstants.CASH_REPORT)
                    .and(ProjectConstants.STATUS_TYPE_CODE)
                    .eq(status)
                    .exists()) {
                //batchId does not exist
                errors.add(CashIssuanceReportConstants.INVALID_BATCH_ID_MESSAGE);
            }
        
        ErrorMessageException.throwIfHasErrors(errors);
    }
}
