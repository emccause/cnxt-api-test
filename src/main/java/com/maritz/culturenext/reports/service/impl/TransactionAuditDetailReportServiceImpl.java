package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import com.maritz.culturenext.batch.TemplateConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.TransactionAuditDetailCsvReportConstants;
import com.maritz.culturenext.reports.dao.TransactionAuditDetailReportDao;
import com.maritz.culturenext.reports.service.TransactionAuditDetailReportService;

@Service
public class TransactionAuditDetailReportServiceImpl  implements TransactionAuditDetailReportService {
    
    @Inject private TransactionAuditDetailReportDao transactionDetailReportDao;
    @Inject private Environment environment;
    @Inject private Security security;

    final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Override
    public void createTransactionAuditDetailReport(HttpServletResponse response, String statusString) {
        
        //Validate all request parameters
        ArrayList<ErrorMessage> errors = validateReportParams(statusString);
        
        //Throw errors before continuing
        ErrorMessageException.throwIfHasErrors(errors);
        
        List<String> statusList = StringUtils.parseDelimitedData(statusString);

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("createTransactionAuditDetailReport is calling transactionDetailReportDao.getTransactionDetailReportData with the following param. " +
                            "statusList {} paxId {}", statusList == null ? "" : statusList.toString(),security.getPaxId());
        }

        // Get report content
        List<Map<String, Object>> reportContent = transactionDetailReportDao.getTransactionDetailReportData(statusList);

        if(reportsLoggingEnabled) {
            logger.info("createTransactionAuditDetailReport is calling writeDownloadCSVFileFromMap with the following params. filename {} rowsAmount {} paxId {}",
                    TransactionAuditDetailCsvReportConstants.TRANSACTION_DETAIL_REPORT_FILE_NAME, reportContent.size(),security.getPaxId());
        }
        // Creating CSV file report
        CsvFileUtil.writeDownloadCSVFileFromMap(response, TransactionAuditDetailCsvReportConstants.TRANSACTION_DETAIL_REPORT_FILE_NAME
                , reportContent, TransactionAuditDetailCsvReportConstants.TRANSACTION_DETAIL_CSV_COLUMNS_PROPS);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("createTransactionAuditDetailReport execution has been completed in {} MILLISECONDS with the following params. " +
                            "status {} rowsAmount {} paxId {}"
                    ,estimatedTime, response.getStatus(),reportContent.size(),security.getPaxId());
        }
        
    }
    
    private ArrayList<ErrorMessage> validateReportParams(String statusString) {
        
        ArrayList<ErrorMessage> errors = new ArrayList<>();

        if (statusString != null && statusString.trim().length() > 0) {
            List<String> statusList = Arrays.asList(statusString.split(ProjectConstants.COMMA_DELIM));
            for (String status : statusList) {
                if (!TransactionAuditDetailCsvReportConstants.validStatusTransactionAuditDetailReportUI.contains(status)) {
                    errors.add(new ErrorMessage()
                        .setCode(TransactionAuditDetailCsvReportConstants.ERROR_INVALID_STATUS)
                        .setField(ProjectConstants.STATUS)
                        .setMessage(TransactionAuditDetailCsvReportConstants.ERROR_INVALID_STATUS_MSG + status));
                    break;
                }
            }
        }
        return errors;    
    }
}