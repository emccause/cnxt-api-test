package com.maritz.culturenext.reports.service;

import javax.servlet.http.HttpServletResponse;

import com.maritz.culturenext.reports.dao.impl.ParticipantSummaryReportDaoImpl;

import java.util.List;
import java.util.Map;

public interface ParticipantReportsService {    
    
    /**
     * Downloads a CSV file containing participant transactional data.
     * For the my activity report, the data is filtered on the logged in paxID.
     * 
     * @param response - tells the service to respond with a CSV file
     * @param fromDateString - the start date for the query
     * @param thruDateString - the end date for the query
     * @param groupIdString - groupId to specify the audience for which to pull data
     * @param paxTeamString - specifies audience as the paxID specified and any of their direct reports
     * @param paxOrgString - specifies audience as the paxID specified and anyone under them in the hierarchy
     * @param typeString - comma separated list of recognition type. Values are GIVEN or RECEIVED
     * @param statusString - comma separated list of recognition statuses. Values are APPROVED, REJECTED, PENDING, ISSUED
     * @param programTypeString - comma separated list of program types.
     *         Values are PEER_TO_PEER, ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, and AWARD_CODE"
     * @param programIdString - specific programId for which to pull data for
     * @param recipientFields - optional fields tied to the recipient.
     *         Fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, 
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param submitterFields - optional fields tied to the submitter. Same fields as recipientFields.
     * @param approverFields - optional fields tied to the approver. Same fields as recipientFields.
     * @return CSV - Download of the result set in CSV format
     * 
     * Participant Transactions Report:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841310/Participant+Transactions+Report
     * 
     * My Activity Report:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/98889186/My+Activity+Report
     */
    void getParticipantTransactionsCsv(HttpServletResponse response, String fromDate, String thruDate, 
            Long paxId, String groupId, String paxTeam, String paxOrg, String typeList, 
            String statusList, String programTypeList, String programId, String recipientFields,
            String submitterFields, String approverFields);

    /**
     * Performs validation and then loads data from the database and returns raw map
     * (split into two methods so that tests don't need to parse the CSV file)
     *
     * @see ParticipantSummaryReportDaoImpl.getParticipantSummaryReport for param info
     *
     * @return List<Map<String, Object>> - Raw list of results in map form
     */
    List<Map<String, Object>> getReportData(
            String commaSeparatedPaxList, String commaSeparatedTeamList, String commaSeparatedOrgList,
            String commaSeparatedGroupList, String fromDateString, String thruDateString,
            String commaSeparatedRecStatusList, String commaSeparatedRecTypeList, String commaSeparatedProgramTypeList,
            String commaSeparatedProgramIdList
    );

    
    /**
     * Downloads a CSV report containing summarized participant data as well as current budget data for each participant.
     * Summarized data includes total logins, number of recognitions given and received, and points given and received.
     * Budget data for each participant includes their total giving limit, giving limit used, giving limit available,
     * and giving limit expired. The givng limit data is pulled real time and is not affected by the date filters.
     *  
     * @param response - Tells the API to download the response as a CSV
     * @param commaSeparatedPaxList - used to determine audience. This is a simple paxID filter
     * @param commaSeparatedTeamList - used to determine audience. This will include the paxID and their direct reports
     * @param commaSeparatedOrgList - used to determine audience. This will include the paxID and their entire organization under them
     * @param commaSeparatedGroupList - used to determine audience. This is a simple groupID filter
     * @param fromDateString - Inclusive date to begin filtering on
     * @param thruDateString - Inclusive date to stop filtering on 
     * @param commaSeparatedRecStatusList - Comma separated list of recognition statuses to filter on
     * @param commaSeparatedRecTypeList - Comma separated list of recognition types. Values are GIVEN or RECEIVED
     * @param commaSeparatedProgramTypeList - Comma separated list of program types to filter on
     * @param commaSeparatedProgramIdList - Program ID to filter on. Either this or programType will be used, never both.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841301/Participant+Summary+Report
     */
    void getParticipantSummaryReport(
            HttpServletResponse response, String commaSeparatedPaxList, String commaSeparatedTeamList,
            String commaSeparatedOrgList, String commaSeparatedGroupList, String fromDateString,
            String thruDateString, String commaSeparatedRecStatusList, String commaSeparatedRecTypeList,
            String commaSeparatedProgramTypeList, String commaSeparatedProgramIdList
    );
}
