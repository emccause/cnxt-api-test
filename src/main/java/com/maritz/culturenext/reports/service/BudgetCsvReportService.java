package com.maritz.culturenext.reports.service;

import javax.servlet.http.HttpServletResponse;

public interface BudgetCsvReportService {
    
    /**
     * Downloads a CSV report containing summarized budget data.
     * Can be filtered on budgetIDs and statuses
     * 
     * @param response - Tells the API to download the response as CSV
     * @param budgetIdsDelim - Comma separated list of budgetIDs to filter on
     * @param statusList - Comma separated list of statuses to filter on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/46628879/Budget+Summary+Report
     */
    void createBudgetSummaryReport(HttpServletResponse response, String budgetIdsDelim, String statusList);
    
    /**
     * Downloads a CSV report containing data about budgets that have giving limits,
     * along with the participants that can use those budgets.
     * Can be filtered on budgetIds and statuses
     * 
     * @param response - Tells the API to download the response as CSV
     * @param budgetIdsDelim - Comma separated list of budgetIDs to filter on
     * @param statusDelim - Comma separated list of statuses to filter on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/45416506/Giving+Limit+Summary+Report
     */
    void createBudgetGivingLimitReport(HttpServletResponse response, String budgetIdsDelim, String statusDelim);
    
    /**
     * Downloads a CSV file containing participant transactional data
     * 
     * @param response - Tells the API to download the response as CSV
     * @param statusString - Comma separated list of comma separated list of recognition statuses to filter on
     * @param budgetIdString - specific programId for which to pull data for
     * @param recipientFields - optional fields tied to the recipient.
     *         Fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE,
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param submitterFields - optional fields tied to the submitter. Same fields as recipientFields.
     * @param approverFields - optional fields tied to the approver. Same fields as recipientFields.
     * @return CSV - Download of the result set in CSV format
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/46628876/Budget+Transaction+Detail+Report
     */
    void getBudgetTransactionDetailsCSV(HttpServletResponse response, String statusString, String budgetIdString,
                                        String recipientFields, String submitterFields, String approverFields);
}
