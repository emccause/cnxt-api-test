package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.reports.dao.CashIssuanceHistoryDao;
import com.maritz.culturenext.reports.dto.CashIssuanceHistoryDTO;
import com.maritz.culturenext.reports.dto.PendingCashIssuanceDTO;
import com.maritz.culturenext.reports.service.CashIssuanceHistoryService;
import com.maritz.culturenext.util.DateUtil;

@Component
public class CashIssuanceHistoryServiceImpl implements CashIssuanceHistoryService {
    
    @Inject CashIssuanceHistoryDao cashIssuanceHistoryDao;
    
    @Override
    public PendingCashIssuanceDTO getPendingCashIssuance() {
        
        List<Map<String, Object>> resultList = cashIssuanceHistoryDao.getPendingCashIssuance();
        
        PendingCashIssuanceDTO data = new PendingCashIssuanceDTO();
        
        if(resultList != null && !resultList.isEmpty()) {
        
            Map<String, Object> result = resultList.get(0);
            
            data.setTotalRecords((Integer) result.get(ProjectConstants.TOTAL_RECORDS));
            data.setAwardAmount((Double) result.get(ProjectConstants.AWARD_AMOUNT));
        }
        
        return data;
    }

    @Override
    public PaginatedResponseObject<CashIssuanceHistoryDTO> getCashIssuanceHistory(PaginationRequestDetails requestDetails, Integer pageNumber, Integer pageSize) {
        
        Integer totalResults = 0;
        
        // Get data
        List<Map<String, Object>> resultList = cashIssuanceHistoryDao.getCashIssuanceHistory(pageNumber, pageSize);
        
        // Process data
        List<CashIssuanceHistoryDTO> cashIssuanceHistoryDTOList = new ArrayList<>();
        
        for (Map<String, Object> result : resultList) {
            
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            CashIssuanceHistoryDTO rowData = new CashIssuanceHistoryDTO();
            
            rowData.setReportCreateDate(DateUtil.convertToUTCDateTime((Date) result.get(ProjectConstants.REPORT_CREATE_DATE)));
            rowData.setBatchId((Long) result.get(ProjectConstants.BATCH_ID));
            rowData.setTotalRecords((Integer) result.get(ProjectConstants.TOTAL_RECORDS));
            rowData.setAwardAmount((Double) result.get(ProjectConstants.AWARD_AMOUNT));
            rowData.setReportType((String) result.get(ProjectConstants.REPORT_TYPE));
            rowData.setStatus((String) result.get(ProjectConstants.STATUS));
            
            cashIssuanceHistoryDTOList.add(rowData);
        }
        
        return new PaginatedResponseObject<>(requestDetails, cashIssuanceHistoryDTOList, totalResults);
    }
}
