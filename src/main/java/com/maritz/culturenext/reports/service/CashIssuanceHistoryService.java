package com.maritz.culturenext.reports.service;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.reports.dto.CashIssuanceHistoryDTO;
import com.maritz.culturenext.reports.dto.PendingCashIssuanceDTO;

public interface CashIssuanceHistoryService {
    
    /**
     * Returns a summation of pending cash issuance
     * 
     * @return DTO containing total records pending and total value of those records 
     * 
     * https://maritz.atlassian.net/wiki/display/M365/GET+Pending+Cash+Issuance
     */
    PendingCashIssuanceDTO getPendingCashIssuance();
    
    /**
     * Return a paginated list of cash issuance
     * 
     * @param requestDetails object for PaginatedResponseObject containing details of request
     * @param pageNumber page number for pagination
     * @param pageSize page size for pagination
     * @return PaginatedResponseObject containing a list of completed cash issuance
     * 
     * https://maritz.atlassian.net/wiki/display/M365/GET+Cash+Issuance+History
     */
    PaginatedResponseObject<CashIssuanceHistoryDTO> getCashIssuanceHistory(PaginationRequestDetails requestDetails, Integer pageNumber, Integer pageSize);
}
