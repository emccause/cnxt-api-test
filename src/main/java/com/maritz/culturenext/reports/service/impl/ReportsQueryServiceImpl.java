package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.util.ProgramTranslationUtil;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheHandler;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.ReportsQueryDataLoadDao;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;
import com.maritz.culturenext.util.DateUtil;

@Component
public class ReportsQueryServiceImpl implements ReportsQueryService {
    
    @Inject private ProgramTranslationUtil programTranslationUtil;
    @Inject private ReportsQueryCacheHandler reportsQueryCacheHandler;
    @Inject private ReportsQueryDataLoadDao reportsQueryDataLoadDao;
    @Inject private Security security;


    @Override
    public QueryIdDto getQueryId(ReportsQueryRequestDto dto) throws Exception {
        
        Long submitterPaxId = security.getPaxId();
        Date startDate = null;
        
        Date endDate = null;
        if(dto.getThruDate() == null){
            String currentThruDateString = DateUtil.convertToDateComponentsFormat(new Date(), TimeZone.getDefault());
            endDate = DateUtil.convertToEndOfDayString(currentThruDateString, TimeZone.getDefault()); 
            // default end date is current day
        }
        else{
            // convert the date string to the proper day (respective to the timezone that call was made)
            // with end of day time
            endDate = DateUtil.convertToEndOfDayString(dto.getThruDate(), TimeZone.getDefault());
        }

        // check if thru date was properly provided
        if(endDate == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.THRU_DATE, 
                    ReportConstants.ERROR_THRU_DATE_INVALID, ReportConstants.ERROR_THRU_DATE_INVALID_MSG);
        }

        if(dto.getFromDate() == null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.add(Calendar.DATE, -30); // default start date is 30 days before end date

            String startDateString =
                    DateUtil.convertToDateComponentsFormat(calendar.getTime(), TimeZone.getTimeZone("UTC"));
            startDate = DateUtil.convertToStartOfDayString(startDateString); // default end date is current day
        }
        else{
            // convert the date string to the proper day (respective to the timezone that call was made) 
            // with beginning of day time
            startDate = DateUtil.convertToStartOfDayString(dto.getFromDate());
        }

        // check if fromDate was properly provided
        if(startDate == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.FROM_DATE, 
                    ReportConstants.ERROR_FROM_DATE_INVALID, ReportConstants.ERROR_FROM_DATE_INVALID_MSG);
        }

        if(endDate.before(startDate)){
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.THRU_DATE, 
                    ReportConstants.ERR_THRU_DATE_BEFORE_FROM_DATE, ReportConstants.ERR_THRU_DATE_BEFORE_FROM_DATE_MSG);
        }
        List<Long> groupIdList = new ArrayList<Long>();
        List<Long> paxIdList = new ArrayList<Long>();
        
        if(dto.getMembers() != null){
            for(GroupMemberStubDTO stub: dto.getMembers()){
                if(stub.getGroupId() != null){
                    groupIdList.add(stub.getGroupId());
                }
                else if(stub.getPaxId() != null){
                    paxIdList.add(stub.getPaxId());
                }
            }
        }
        List<Long> directReportIdList = dto.getDirectReports();
        if(directReportIdList == null){
            directReportIdList = new ArrayList<Long>();
        }
        List<Long> orgReportIdList = dto.getOrgReports();
        if(orgReportIdList == null){
            orgReportIdList = new ArrayList<Long>();
        }
        String queryId = reportsQueryCacheHandler.createCache(submitterPaxId, startDate, endDate, groupIdList,
                paxIdList, directReportIdList, orgReportIdList);
        return new QueryIdDto().setQueryId(queryId);
    }

    @Override
    public List<Map<String, Object>> getProgramsForQuery(String cacheId, String languageCode) throws Exception {
        List<Map<String, Object>> results = 
                reportsQueryDataLoadDao.getProgramSummaryList(reportsQueryCacheHandler.getProgramIdList(cacheId));

        applyTranslationsToQueryResults(results, languageCode);

        return results;
    }
    
    @Override
    public List<BudgetDTO> getBudgetsForQuery(String cacheId) throws Exception {
        List<BudgetDTO> budgetList = new ArrayList<>();
        List<Map<String, Object>> nodes = 
                reportsQueryDataLoadDao.getBudgetSummaryList(reportsQueryCacheHandler.getBudgetIdList(cacheId));
        
        if (nodes != null && !nodes.isEmpty()) {
            for (Map<String, Object> node : nodes) {
                BudgetDTO budget = new BudgetDTO();
                budget.setBudgetId((Long) node.get(ProjectConstants.ID));
                budget.setName((String) node.get(ProjectConstants.BUDGET_NAME));
                budget.setDisplayName((String) node.get(ProjectConstants.BUDGET_DISPLAY_NAME));
                
                budget.setStatus((String) node.get(ProjectConstants.STATUS_TYPE_CODE));
                budgetList.add(budget);
            }
        }
        
        return budgetList;
    }
    
    @Override
    public List<EmployeeDTO> getPaxList(String cacheId, String hasNonZeroGivingLimit) throws Exception{
        List<Long> paxIdList = null;
        if(hasNonZeroGivingLimit != null && hasNonZeroGivingLimit.equalsIgnoreCase("TRUE")){
            paxIdList = new ArrayList<Long>(reportsQueryCacheHandler.getPaxWithGivingLimitIdList(cacheId).keySet());
        }
        else{
            paxIdList = reportsQueryCacheHandler.getFinalPaxIdList(cacheId);
        }
        return reportsQueryDataLoadDao.getPaxInfoList(paxIdList);
    }

    private void applyTranslationsToQueryResults(List<Map<String, Object>> queryResults, String languageCode) {
        for (Map<String, Object> result : queryResults) {
            ProgramSummaryDTO programDTO = new ProgramSummaryDTO();
            programDTO.setProgramId((Long) result.get(ProjectConstants.PROGRAM_ID));
            programDTO.setProgramName((String) result.get(ProjectConstants.PROGRAM_NAME));
            programDTO.setProgramDescriptionLong((String) result.get(ProjectConstants.PROGRAM_DESCRIPTION_LONG));
            programDTO.setProgramDescriptionShort((String) result.get(ProjectConstants.PROGRAM_DESCRIPTION_SHORT));

            programTranslationUtil.translateProgramFieldsOnProgramSummaryDTO(programDTO, languageCode);

            result.put(ProjectConstants.PROGRAM_NAME, programDTO.getProgramName());
            result.put(ProjectConstants.PROGRAM_DESCRIPTION_LONG, programDTO.getProgramDescriptionLong());
            result.put(ProjectConstants.PROGRAM_DESCRIPTION_SHORT, programDTO.getProgramDescriptionShort());
        }
    }
}
