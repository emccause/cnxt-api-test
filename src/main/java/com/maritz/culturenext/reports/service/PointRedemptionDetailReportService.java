package com.maritz.culturenext.reports.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

public interface PointRedemptionDetailReportService {
        
    /**
     * This report queries the PC cloud for data.
     * Retrieve the point redemption data and the participant details for the specified audience and date range
     * Return the data in a CSV file format
     * 
     * @param response - Tells the API to download the response as a CSV
     * @param commaSeparatedOptionalFieldsList - Comma separated list of optional fields to include on the report.
     *     These fields will all be relative to the recipient and can include: EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE,
     *     TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION, GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, and CUSTOM_5
     * @param groupId - groupID to filter on
     * @param fromDateString - Inclusive date to start filtering on
     * @param thruDateString - Inclusive date to stop filtering on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/45416481/Redemption+Detail+Report
     */
    void getPointRedemptionDetailReport(
                HttpServletResponse response, String commaSeparatedOptionalFieldsList, String groupId,
                String fromDateString, String thruDateString);
    
    /**
     * Retrieve the point redemption data and the participant details for the specified audience and date range
     * Uses the ABS product-codes and subclients from the properties to retrieve the redemption data
     
     * @param commaSeparatedOptionalFieldsList
     * @param groupId
     * @param fromDateString
     * @param thruDateString
     * @return
     */
    List<Map<String, Object>> getReportData(
                String commaSeparatedOptionalFieldsList,String groupId,
                String fromDateString, String thruDateString);

}