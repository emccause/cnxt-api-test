package com.maritz.culturenext.reports.service;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;

public interface ReportsQueryService {

    /**
     * Takes the passed in date and audience parameters and assigns them to a generated query ID.
     * Then saves this queryID will be in cache and ties it to the passed in parameters.
     * The cached queryID is used on the dashboard reporting pages, so that multiple reports can run
     * using the same filters (for date range and audience) and when any of those filters are updated,
     * all of the reports are updated at the same time to use the new params. 
     * 
     * @param dto - contains date and audience information used to build the query ID
     * @return queryID that is saved in cache
     * @throws Exception
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468811/Programs+Query+by+date+and+audience
     */
    QueryIdDto getQueryId(ReportsQueryRequestDto dto) throws Exception;
    
    /**
     * Uses the cached queryID to grab the audience, and then pull a list off all active
     * programs that any participants in the audience can give or receive from.
     * 
     * @param cacheId - The queryID that is saved in cache and contains date and audience filters
     * @param languageCode - used for translations
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468811/Programs+Query+by+date+and+audience
     */
    List<Map<String, Object>> getProgramsForQuery(String cacheId, String languageCode) throws Exception;
    
    /**
     * Uses the cached queryID to grab the audience, and then pull a list of all active
     * budgets that any participant in the audience has either given or received points from.
     * 
     * @param cacheId - The queryID that is saved in cache and contains the date and audience filters
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468862/Budgets+Query+by+Date+and+Audience
     */
    List<BudgetDTO> getBudgetsForQuery(String cacheId) throws Exception;
    
    /**
     * Looks up the cached queryID and returns the audience that is saved in cache.
     * Can be filtered to only return users that are associated with budgets that have giving limits.
     * 
     * @param cacheId - The queryID that is saved in cache 
     * @param hasNonZeroGivingLimit - if TRUE, only returns users that are tied to budgets with giving limits
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12156988/Get+Direct+Reports
     */
    List<EmployeeDTO> getPaxList(String cacheId, String hasNonZeroGivingLimit) throws Exception;
}
