package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import com.maritz.culturenext.reports.constants.ParticipantSummaryReportConstants;
import com.maritz.culturenext.reports.dao.ParticipantSummaryReportDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.ParticipantReportsConstants;
import com.maritz.culturenext.reports.dao.ParticipantReportsDao;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.service.ParticipantReportsService;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;

@Component
public class ParticipantReportsServiceImpl implements ParticipantReportsService {
    
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private EnrollmentConfigService enrollmentConfigService;
    @Inject private ParticipantReportsDao participantReportsDao;
    @Inject private ParticipantSummaryReportDao participantSummaryReportDao;
    @Inject private Environment environment;
    @Inject private Security security;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void getParticipantTransactionsCsv(HttpServletResponse response,
            String fromDateString, String thruDateString, Long paxId, 
            String groupIdString, String paxTeamString, String paxOrgString, 
            String typeString, String statusString, String programTypeString, 
            String programIdString,    String recipientFields, String submitterFields, 
            String approverFields) {
        
        //Validate all request parameters
        ArrayList<ErrorMessage> errors = validateReportParams(
                fromDateString, thruDateString, paxId, groupIdString, 
                paxTeamString, paxOrgString, typeString, statusString, 
                programTypeString, programIdString, recipientFields, 
                submitterFields, approverFields);
        
        //Throw errors before continuing
        ErrorMessageException.throwIfHasErrors(errors);
                
        //Convert String to Date/Long
        Date fromDate = DateUtil.convertToStartOfDayString(fromDateString, TimeZone.getDefault());
        
        Date thruDate = null;
        if (thruDateString != null) {
            thruDate = DateUtil.convertToEndOfDayString(thruDateString,  TimeZone.getDefault());
        }
        
        Long groupId = null;
        if (groupIdString != null) {
            groupId = Long.valueOf(groupIdString);
        }
        
        Long paxTeam = null;
        if (paxTeamString != null) {
            paxTeam = Long.valueOf(paxTeamString);
        }
        
        Long paxOrg = null;
        if (paxOrgString != null) {
            paxOrg = Long.valueOf(paxOrgString);
        }
        
        Long programId = null;
        if (programIdString != null) {
            programId = Long.valueOf(programIdString);
        }
        
        List<String> recipientFieldList = null;
        if (recipientFields != null) {
            recipientFieldList = Arrays.asList(recipientFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        List<String> submitterFieldList = null;
        if (submitterFields != null) {
            submitterFieldList = Arrays.asList(submitterFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        List<String> approverFieldList = null;
        if (approverFields != null) {
            approverFieldList = Arrays.asList(approverFields.split(ProjectConstants.COMMA_DELIM));
        }
        
        //Get the report data - will include all optional fields
        List<Map<String, Object>> reportData = 
            participantReportsDao.getParticipantTransactionReportData(
                fromDate, thruDate, paxId, groupId, paxTeam, paxOrg, typeString, 
                statusString, programTypeString, programId, null);
        
        EnrollmentFieldReportUtil enrollmentFieldReportUtil = 
                new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());
        
        String date = DateUtil.convertToDateOnlyFormat(new Date(), TimeZone.getDefault());
        String fileName = ParticipantReportsConstants.PARTICIPANT_TRANSACTIONS_FILENAME + date;

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("getParticipantTransactionsCsv is calling writeDownloadCSVFileFromMap to Write down Admin & Manager Report with the following params. " +
                            "status {} fileName {} fromDateString {} thruDateString {} paxId {} groupIdString {} paxTeamString {} paxOrgString {} typeString {} statusString {} programTypeString {}" +
                            " programIdString {} recipientFields {} submitterFields {} approverFields {}"
                    , response.getStatus(),fileName,fromDateString,thruDateString,paxId,groupIdString,paxTeamString,paxOrgString,typeString,statusString,programTypeString,
                    programIdString,recipientFields, submitterFields,approverFields);
        }

        if (paxId == null) {
            // Create CSV file for Admin & Manager Report
            CsvFileUtil.writeDownloadCSVFileFromMap(response, fileName, 
                    reportData, createParticipantTransactionCsvColumnProps(
                            enrollmentFieldReportUtil, recipientFieldList, submitterFieldList, approverFieldList));
            if(reportsLoggingEnabled){
                long estimatedTime = System.currentTimeMillis() - startTime;
                logger.info("getParticipantTransactionsCsv execution has been completed in {} MILLISECONDS with the following params. " +
                                "status {} "
                        ,estimatedTime, response.getStatus());
            }
        } else {
            //Create CSV file for My Activity Report
            CsvFileUtil.writeDownloadCSVFileFromMap(response, fileName, 
                    reportData, createParticipantTransactionMyActRptCsvColumnProps(
                            enrollmentFieldReportUtil, recipientFieldList, submitterFieldList, approverFieldList));
            if(reportsLoggingEnabled){
                long estimatedTime = System.currentTimeMillis() - startTime;
                logger.info("getParticipantTransactionsCsv execution has been completed in {} MILLISECONDS with the following params. " +
                                "status {}"
                        ,estimatedTime, response.getStatus());
            }
        }
    }
    
    /**
     * Validates all of the report parameters for the Participant Transaction Report
     * Returns a list of errors that will be handled by the calling method
     * @param fromDateString - the start date for the query
     * @param thruDateString - the end date for the query
     * @param paxIdString - paxId of the logged in user
     * @param groupIdString - groupId to specify the audience for which to pull data
     * @param paxTeamString - specifies audience as the paxID specified and any of their direct reports
     * @param paxOrgString - specifies audience as the paxID specified and anyone under them in the hierarchy
     * @param typeString - comma separated list of recognition type. Values are GIVEN or RECEIVED
     * @param statusString - comma separated list of recognition statuses. Values are APPROVED, DENIED, PENDING
     * @param programTypeString - comma separated list of program types.
     *         Values are PEER_TO_PEER, ECARD_ONLY, MANAGER_DISCRETIONARY, POINT_LOAD, and AWARD_CODE"
     * @param programIdString - specific programId for which to pull data for
     * @param recipientFields - optional fields tied to the recipient.
     *         Fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, 
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param submitterFields - optional fields tied to the submitter. Same fields as recipientFields.
     * @param approverFields - optional fields tied to the approver. Same fields as recipientFields.
     * @return ArrayList<ErrorMessage> - list of error messages
     */
    private ArrayList<ErrorMessage> validateReportParams(String fromDateString, String thruDateString, 
            Long paxId, String groupIdString, String paxTeamString, String paxOrgString, 
            String typeString, String statusString, String programTypeString, 
            String programIdString, String recipientFields, String submitterFields, 
            String approverFields) {
        
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        
        Date fromDate = null;
        Date thruDate = null;
        
        //fromDate
        if (fromDateString == null || fromDateString.length() == 0) {
            //fromDate is null
            errors.add(ParticipantReportsConstants.MISSING_FROM_DATE_MESSAGE);
        } else if (!fromDateString.matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            //fromDate is not formatted properly
            errors.add(ParticipantReportsConstants.INVALID_FROM_DATE_FORMAT_MESSAGE);
        } else {
            //Convert String to Date
            fromDate = DateUtil.convertToStartOfDayString(fromDateString, TimeZone.getDefault());
        }

        //thruDate
        if (thruDateString != null && !thruDateString.matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            //thruDate is not formatted properly
            errors.add(ParticipantReportsConstants.INVALID_THRU_DATE_FORMAT_MESSAGE);
        } else {
            //Convert String to Date
            thruDate = DateUtil.convertToEndOfDayString(thruDateString,  TimeZone.getDefault());
        }

        // Check if thru date is before from date
        if ((fromDate != null && thruDate != null) && (!fromDate.before(thruDate) || fromDate.equals(thruDate))) {
                errors.add(ParticipantReportsConstants.THRU_DATE_BEFORE_FROM_DATE_MESSAGE);
        }

        //paxId
        if (paxId != null && !paxDao.findBy().where(ProjectConstants.PAX_ID).eq(paxId).exists()) {
                //pax does not exist
                errors.add(ParticipantReportsConstants.INVALID_PAX_ID_MESSAGE);
        }
        
        //groupId
        if (groupIdString != null) {
            Long groupId = null;
            try {
                groupId = Long.valueOf(groupIdString);
            } catch (NumberFormatException nfe) {
                //groupIdString is not valid
                errors.add(ParticipantReportsConstants.INVALID_GROUP_ID_FORMAT_MESSAGE);
            }

            if (groupId != null && !groupsDao.findBy().where(ProjectConstants.GROUP_ID).eq(groupId).exists()) {
                //group does not exist
                errors.add(ParticipantReportsConstants.INVALID_GROUP_ID_MESSAGE);
            }
        }

        //paxTeam
        if (paxTeamString != null) {
            Long paxTeam = null;
            try {
                paxTeam = Long.valueOf(paxTeamString);
            } catch (NumberFormatException nfe) {
                //paxTeamString is not valid
                errors.add(ParticipantReportsConstants.INVALID_PAX_TEAM_FORMAT_MESSAGE);
            }

            if (paxTeam != null && !paxDao.findBy().where(ProjectConstants.PAX_ID).eq(paxTeam).exists()) {
                //pax does not exist
                errors.add(ParticipantReportsConstants.INVALID_PAX_TEAM_MESSAGE);
            }
        }

        //paxOrg
        if (paxOrgString != null) {
            Long paxOrg = null;
            try {
                paxOrg = Long.valueOf(paxOrgString);
            } catch (NumberFormatException nfe) {
                //paxOrgString is not valid
                errors.add(ParticipantReportsConstants.INVALID_PAX_ORG_FORMAT_MESSAGE);
            }

            if (paxOrg != null && !paxDao.findBy().where(ProjectConstants.PAX_ID).eq(paxOrg).exists()) {
                //pax does not exist
                errors.add(ParticipantReportsConstants.INVALID_PAX_ORG_MESSAGE);
            }
        }

        //typeList
        if (typeString == null || typeString.length() == 0) {
            //type is null
            errors.add(ParticipantReportsConstants.MISSING_TYPE_MESSAGE);
        } else {
            //make sure all types are valid
            List<String> typeList = Arrays.asList(typeString.split(ProjectConstants.COMMA_DELIM));
            for (String type : typeList) {
                if (!ParticipantReportsConstants.validTypes.contains(type)) {
                    //invalid type
                    errors.add(ParticipantReportsConstants.INVALID_TYPE_MESSAGE);
                    break;
                }
            }
        }

        //statusList
        if (statusString == null || statusString.length() == 0) {
            //status is null
            errors.add(ParticipantReportsConstants.MISSING_STATUS_MESSAGE);
        }

        //programTypeList
        if (programTypeString != null) {
            //make sure all program types are valid
            List<String> programTypeList = Arrays.asList(programTypeString.split(ProjectConstants.COMMA_DELIM));
            for (String programType : programTypeList) {
                if (!ProgramTypeEnum.getAllProgramTypes().contains(programType)) {
                    //invalid program type
                    errors.add(ParticipantReportsConstants.INVALID_PROGRAM_TYPE_MESSAGE);
                    break;
                }
            }
        }

        //programId
        if (programIdString != null) {
            Long programId = null;
            try {
                programId = Long.valueOf(programIdString);
            } catch (NumberFormatException nfe) {
                //programIdString is not valid
                errors.add(ParticipantReportsConstants.INVALID_PROGRAM_ID_FORMAT_MESSAGE);
            }

            if (programId != null && !programDao.findBy().where(ProjectConstants.PROGRAM_ID).eq(programId).exists()) {
                //program does not exist
                errors.add(ParticipantReportsConstants.INVALID_PROGRAM_ID_MESSAGE);
            }
        }

        //recipientFields
        errors.addAll(validateOptionalFields(
                recipientFields,
                ParticipantReportsConstants.ERROR_INVALID_RECIPIENT_FIELD,
                ProjectConstants.RECIPIENT_FIELDS,
                ParticipantReportsConstants.ERROR_INVALID_RECIPIENT_FIELD_MSG));
        
        
        //submitterFields
        errors.addAll(validateOptionalFields(
                submitterFields,
                ParticipantReportsConstants.ERROR_INVALID_SUBMITTER_FIELD,
                ProjectConstants.SUBMITTER_FIELDS,
                ParticipantReportsConstants.ERROR_INVALID_SUBMITTER_FIELD_MSG));
        

        //approverFields
        errors.addAll(validateOptionalFields(
                approverFields,
                ParticipantReportsConstants.ERROR_INVALID_APPROVER_FIELD,
                ProjectConstants.APPROVER_FIELDS,
                ParticipantReportsConstants.ERROR_INVALID_APPROVER_FIELD_MSG));
        
        return errors;
    }    
    
    /**
     * Validates the optional custom fields that can be called for a report
     * Returns a list of errors that will be handled by the calling method
     * @param optionalFields - comma separated list of optional fields to include on the report
     *         Valid fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, 
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param code - value to set for ErrorMessage.code
     * @param field - value to set for ErrorMessage.field
     * @param message - value to set for ErrorMessage.message
     * @return ArrayList<ErrorMessage> - list of error messages
     */
    private ArrayList<ErrorMessage> validateOptionalFields(
            String optionalFields, String code, String field, String message) {
        
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        
        if (optionalFields != null && optionalFields.length() != 0) {
            List<String> fieldList = Arrays.asList(optionalFields.split(ProjectConstants.COMMA_DELIM));

            //make sure all optional fields are valid
            for (String item : fieldList) {
                if (!ParticipantReportsConstants.validCustomFields.contains(item)) {
                    //invalid custom field
                    errors.add(new ErrorMessage()
                        .setCode(code)
                        .setField(field)
                        .setMessage(String.format(message, item)));
                    break;
                }
            }
        }
        
        return errors;
    }
        
    
    /**
     * Create CSV headers, fieldMapping and processor according to standard, request recipient and submitter fields.
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @param recipientFields - Optional recipient information to include on the report
     * @param submitterFields - Optional submitter information to include on the report
     * @param approverFields - Optional approver information to include on the reports
     * @return List of CSV columns properties for Admin & Manager Activity Report.
     */
    private static List<CsvColumnPropertiesDTO> createParticipantTransactionCsvColumnProps(
            EnrollmentFieldReportUtil enrollmentFieldReportUtil,
            List<String> recipientFields, List<String> submitterFields, List<String> approverFields) {
        
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        
        //Standard Fields - Tracking
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.TRANSACTION_ID_HEADER,
                ProjectConstants.TRANSACTION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.NOMINATION_ID_HEADER,
                ProjectConstants.NOMINATION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTED_DATE_HEADER,
                ProjectConstants.SUBMITTED_DATE, new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1)));
        
        //Standard Fields - Recipient
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_ID_HEADER,
                ProjectConstants.RECIPIENT_ID, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_FIRST_NAME_HEADER,
                ProjectConstants.RECIPIENT_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_LAST_NAME_HEADER,
                ProjectConstants.RECIPIENT_LAST_NAME, null));
        
        //Optional Fields - Recipient
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps, 
                ParticipantReportsConstants.RECIPIENT_HEADER_PREFIX, ProjectConstants.RECIPIENT, recipientFields);

        //Standard Fields - Submitter
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_ID_HEADER,
                ProjectConstants.SUBMITTER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_FIRST_NAME_HEADER,
                ProjectConstants.SUBMITTER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_LAST_NAME_HEADER,
                ProjectConstants.SUBMITTER_LAST_NAME, null));

        //Optional Fields - Submitter
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                ParticipantReportsConstants.SUBMITTER_HEADER_PREFIX, ProjectConstants.SUBMITTER, submitterFields);
        
        //Standard Fields - Approver
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_ID_HEADER,
                ProjectConstants.APPROVER_ID,  null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_FIRST_NAME_HEADER,
                ProjectConstants.APPROVER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_LAST_NAME_HEADER,
                ProjectConstants.APPROVER_LAST_NAME, null));
        
        //Optional Fields - Approver
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps, 
                ParticipantReportsConstants.APPROVER_HEADER_PREFIX, ProjectConstants.APPROVER, approverFields);
        
        //Standard Fields - Other
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_AMOUNT_HEADER,
                ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PAYOUT_TYPE_HEADER,
                ProjectConstants.PAYOUT_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_NAME_HEADER,
                ProjectConstants.PROGRAM_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.VALUE_HEADER,
                ProjectConstants.VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.HEADLINE_HEADER,
                ProjectConstants.HEADLINE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_TYPE_HEADER,
                ProjectConstants.PROGRAM_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BUDGET_NAME_HEADER,
                ProjectConstants.BUDGET_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.STATUS_HEADER,
                ProjectConstants.RECOGNITION_STATUS, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVAL_DATE_HEADER,
                ProjectConstants.APPROVAL_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.ISSUANCE_DATE_HEADER,
                ProjectConstants.ISSUANCE_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_CODE_HEADER,
                ProjectConstants.AWARD_CODE, null));
        //BATCH_ID
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BATCH_ID_HEADER,
                ProjectConstants.BATCH_ID, null));
        //COMMENT
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PRIVATE_MESSAGE_HEADER,
                ProjectConstants.COMMENT, null));
        
        return csvColumnsProps;    
    }

    /**
     * Create CSV headers, fieldMapping and processor according to standard, request recipient and submitter fields.
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @param recipientFields - Optional recipient information to include on the report
     * @param submitterFields - Optional submitter information to include on the report
     * @param approverFields - Optional approver information to include on the reports
     * @return List of CSV columns properties for My Activity Report.
     */
    private static List<CsvColumnPropertiesDTO> createParticipantTransactionMyActRptCsvColumnProps(
            EnrollmentFieldReportUtil enrollmentFieldReportUtil,
            List<String> recipientFields, List<String> submitterFields, List<String> approverFields) {
        
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        
        //Standard Fields - Tracking
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.TRANSACTION_ID_HEADER,
                ProjectConstants.TRANSACTION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.NOMINATION_ID_HEADER,
                ProjectConstants.NOMINATION_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTED_DATE_HEADER,
                ProjectConstants.SUBMITTED_DATE, new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1)));
        
        //Standard Fields - Recipient
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_FIRST_NAME_HEADER,
                ProjectConstants.RECIPIENT_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.RECIPIENT_LAST_NAME_HEADER,
                ProjectConstants.RECIPIENT_LAST_NAME, null));
        
        //Optional Fields - Recipient
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps, 
                ParticipantReportsConstants.RECIPIENT_HEADER_PREFIX, ProjectConstants.RECIPIENT, recipientFields);

        //Standard Fields - Submitter
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_FIRST_NAME_HEADER,
                ProjectConstants.SUBMITTER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.SUBMITTER_LAST_NAME_HEADER,
                ProjectConstants.SUBMITTER_LAST_NAME, null));

        //Optional Fields - Submitter
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                ParticipantReportsConstants.SUBMITTER_HEADER_PREFIX, ProjectConstants.SUBMITTER, submitterFields);
        
        //Standard Fields - Approver
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_FIRST_NAME_HEADER,
                ProjectConstants.APPROVER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVER_LAST_NAME_HEADER,
                ProjectConstants.APPROVER_LAST_NAME, null));
        
        //Optional Fields - Approver
        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps, 
                ParticipantReportsConstants.APPROVER_HEADER_PREFIX, ProjectConstants.APPROVER, approverFields);
        
        //Standard Fields - Other
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_AMOUNT_HEADER,
                ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PAYOUT_TYPE_HEADER,
                ProjectConstants.PAYOUT_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_NAME_HEADER,
                ProjectConstants.PROGRAM_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.VALUE_HEADER,
                ProjectConstants.VALUE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.HEADLINE_HEADER,
                ProjectConstants.HEADLINE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PROGRAM_TYPE_HEADER,
                ProjectConstants.PROGRAM_TYPE, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BUDGET_NAME_HEADER,
                ProjectConstants.BUDGET_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.STATUS_HEADER,
                ProjectConstants.RECOGNITION_STATUS, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.APPROVAL_DATE_HEADER,
                ProjectConstants.APPROVAL_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.ISSUANCE_DATE_HEADER,
                ProjectConstants.ISSUANCE_DATE, new Optional(new FmtDate(DateConstants.COMPONENTS_DATE_FORMAT_1))));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.AWARD_CODE_HEADER,
                ProjectConstants.AWARD_CODE, null));
        //BATCH_ID
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.BATCH_ID_HEADER,
                ProjectConstants.BATCH_ID, null));
        //COMMENT
        csvColumnsProps.add(new CsvColumnPropertiesDTO(ParticipantReportsConstants.PRIVATE_MESSAGE_HEADER,
                ProjectConstants.COMMENT, null));
        
        return csvColumnsProps;    
    }
    
    @Override
    public List<Map<String, Object>> getReportData(
            String commaSeparatedPaxList, String commaSeparatedTeamList, String commaSeparatedOrgList,
            String commaSeparatedGroupList, String fromDateString, String thruDateString,
            String commaSeparatedRecStatusList, String commaSeparatedRecTypeList, String commaSeparatedProgramTypeList,
            String commaSeparatedProgramIdList
    ) {

        // validate request
        List<ErrorMessage> errors = validateRequest(
                commaSeparatedPaxList, commaSeparatedTeamList, commaSeparatedOrgList, commaSeparatedGroupList,
                commaSeparatedRecStatusList, commaSeparatedRecTypeList,
                commaSeparatedProgramTypeList, commaSeparatedProgramIdList);

        // process from date
        Date fromDate = null;

        if (fromDateString != null && !fromDateString.trim().isEmpty()) {
            fromDate = DateUtil.convertToStartOfDayString(fromDateString);

            if (fromDate == null) { // DateUtil method will return null if it can't parse the date
                errors.add(ParticipantSummaryReportConstants.FROM_DATE_FORMAT_INCORRECT_MESSAGE);
            }
        }

        // process thru date
        Date thruDate = null;

        if (thruDateString != null && !thruDateString.trim().isEmpty()) {
            thruDate = DateUtil.convertToEndOfDayString(thruDateString);

            if (thruDate == null) { // DateUtil method will return null if it can't parse the date
                errors.add(ParticipantSummaryReportConstants.THRU_DATE_FORMAT_INCORRECT_MESSAGE);
            }
        }

        ErrorMessageException.throwIfHasErrors(errors);

        return participantSummaryReportDao.getParticipantSummaryReport(
                commaSeparatedPaxList, commaSeparatedTeamList, commaSeparatedOrgList, commaSeparatedGroupList,
                fromDate, thruDate, commaSeparatedRecStatusList, commaSeparatedRecTypeList,
                commaSeparatedProgramTypeList, commaSeparatedProgramIdList
        );
    }

    @Override
    public void getParticipantSummaryReport(
            HttpServletResponse response, String commaSeparatedPaxList, String commaSeparatedTeamList,
            String commaSeparatedOrgList, String commaSeparatedGroupList, String fromDateString,
            String thruDateString, String commaSeparatedRecStatusList, String commaSeparatedRecTypeList,
            String commaSeparatedProgramTypeList, String commaSeparatedProgramIdList
    ) {

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("getParticipantSummaryReport is calling getReportData with the following params. " +
                            "paxIdList {} teamList {} orgList {}  groupList {} fromDate {} thruDate {} recStatusList {} recTypeList {} programTypeList {} programIdList {} paxId {}"
                    , commaSeparatedPaxList,commaSeparatedTeamList,commaSeparatedOrgList,commaSeparatedGroupList,
                    fromDateString,thruDateString,commaSeparatedRecStatusList,commaSeparatedRecTypeList,
                    commaSeparatedProgramTypeList,commaSeparatedProgramIdList,security.getPaxId());
        }

        List<Map<String, Object>> reportData = getReportData(
                commaSeparatedPaxList, commaSeparatedTeamList,commaSeparatedOrgList, commaSeparatedGroupList,
                fromDateString, thruDateString, commaSeparatedRecStatusList, commaSeparatedRecTypeList,
                commaSeparatedProgramTypeList, commaSeparatedProgramIdList
        );

        StringBuilder fileNameBuilder = new StringBuilder(ParticipantSummaryReportConstants.REPORT_FILE_NAME);
        fileNameBuilder.append(ProjectConstants.SPACE);
        fileNameBuilder.append(DateUtil.getCurrentTimeInUTCDate());

        CsvFileUtil.writeDownloadCSVFileFromMap(
                response, fileNameBuilder.toString(), reportData,
                ParticipantSummaryReportConstants.PARTICIPANT_SUMMARY_REPORT_COLUMN_MAPPING
        );

        if(reportsLoggingEnabled) {
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("getParticipantSummaryReport execution has been completed in {} MILLISECONDS with the following params. " +
                            "paxIdList {} teamList {} orgList {}  groupList {} fromDate {} thruDate {} recStatusList {} recTypeList {} programTypeList {} programIdList {} paxId {}"
                    ,estimatedTime, commaSeparatedPaxList,commaSeparatedTeamList,commaSeparatedOrgList,commaSeparatedGroupList,
                    fromDateString,thruDateString,commaSeparatedRecStatusList,commaSeparatedRecTypeList,
                    commaSeparatedProgramTypeList,commaSeparatedProgramIdList,security.getPaxId());
        }
    }

    private List<ErrorMessage> validateRequest(
            String commaSeparatedPaxList, String commaSeparatedTeamList, String commaSeparatedOrgList,
            String commaSeparatedGroupList, String commaSeparatedRecStatusList, String commaSeparatedRecTypeList,
            String commaSeparatedProgramTypeList, String commaSeparatedProgramIdList
    ) {
        List<ErrorMessage> errors = new ArrayList<>();

        validateListContainsDigits(errors, commaSeparatedPaxList, ProjectConstants.PAX_ID);
        validateListContainsDigits(errors, commaSeparatedTeamList, ProjectConstants.TEAM_ID);
        validateListContainsDigits(errors, commaSeparatedOrgList, ProjectConstants.ORG_ID);
        validateListContainsDigits(errors, commaSeparatedGroupList, ProjectConstants.GROUP_ID);
        validateListContainsLetters(errors, commaSeparatedRecStatusList, ProjectConstants.STATUS);
        validateListContainsLetters(errors, commaSeparatedRecTypeList, ProjectConstants.TYPE);
        validateListContainsLetters(errors, commaSeparatedProgramTypeList, ProjectConstants.PROGRAM_TYPE);
        validateListContainsDigits(errors, commaSeparatedProgramIdList, ProjectConstants.PROGRAM_ID);

        return errors;
    }

    private void validateListContainsDigits(List<ErrorMessage> errors, String commaSeparatedList, String field) {
        if (commaSeparatedList != null
                && !commaSeparatedList.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            errors.add(new ErrorMessage()
                    .setField(field)
                    .setCode(ParticipantSummaryReportConstants.PARAM_ONLY_SUPPORTS_DIGITS_ERROR)
                    .setMessage(ParticipantSummaryReportConstants.PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE));
        }
    }

    private void validateListContainsLetters(List<ErrorMessage> errors, String commaSeparatedList, String field) {
        if (commaSeparatedList != null
                && !commaSeparatedList.matches(ProjectConstants.REGEX_DELIM_COMMA_LETTERS)) {
            errors.add(new ErrorMessage()
                    .setField(field)
                    .setCode(ParticipantSummaryReportConstants.PARAM_ONLY_SUPPORTS_LETTERS_ERROR)
                    .setMessage(ParticipantSummaryReportConstants.PARAM_ONLY_SUPPORTS_LETTERS_ERROR_MESSAGE)
            );
        }
    }
}
