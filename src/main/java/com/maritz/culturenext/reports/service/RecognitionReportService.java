package com.maritz.culturenext.reports.service;


import com.maritz.core.rest.ErrorMessageException;


import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface RecognitionReportService {
    
    /**
     * Downloads a CSV report containing transactional information about all recognitions that match the filter parameters.
     * 
     * @param response - Tells the API to download the report as a CSV
     * @param recognitionType - The action taken on the recognition. Values are GIVEN or APPROVED
     * @param status - Status of the recognition. Values are PENDING, APPROVED, and DENIED
     * @param fromDate - Inclusive date to begin filtering on
     * @param toDate - Inclusive date to stop filtering on
     * @param groupId - groupID to filter on
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497158/Get+Approvals+Report
     * 
     * NOTE: I believe this report has been replaced with the Participant Transactions report:
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841310/Participant+Transactions+Report
     */
    Void getApprovalsReport(HttpServletResponse response, String recognitionType, List<String> status, 
            String fromDate, String toDate, Long groupId) throws ErrorMessageException;
}
