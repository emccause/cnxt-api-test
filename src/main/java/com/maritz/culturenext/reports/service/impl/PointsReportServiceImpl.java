package com.maritz.culturenext.reports.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.reports.caching.QueryCacheHandler;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.PointsReportDao;
import com.maritz.culturenext.reports.dto.PointsReportCacheObjectDTO;
import com.maritz.culturenext.reports.dto.PointsReportContentDTO;
import com.maritz.culturenext.reports.dto.PointsReportRequestDTO;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.service.PointsReportService;
import com.maritz.culturenext.reports.util.DeliveryMethodTypes;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;
import com.maritz.culturenext.reports.util.PointsReportUtil;
import com.maritz.culturenext.reports.util.ReportTypes;

@Component
public class PointsReportServiceImpl implements PointsReportService {

    @Inject private ConcentrixDao<Groups> groupsDao;

    @Inject private EnrollmentConfigService enrollmentConfigService;
    
    @Inject private PointsReportDao pointsReportDao;
    
    @Inject private QueryCacheHandler<PointsReportCacheObjectDTO> queryCacheHandler;
    @Inject private Environment environment;
    @Inject private Security security;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public QueryIdDto getQueryId(PointsReportRequestDTO request) throws Exception {
        // Get enrollment fields data. Using EnrollmentFieldReportUtil to handle csv reports columns.
        EnrollmentFieldReportUtil enrollmentFieldReportUtil = 
                new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());
        
        // Verify request parameters.
        validateRequest( request, enrollmentFieldReportUtil);
        
        String queryId =  queryCacheHandler.createCache(new PointsReportCacheObjectDTO(request));
        
        return  new QueryIdDto().setQueryId(queryId);
    }
    
    @Override
    public void generatePointsReport(HttpServletResponse response, String queryId) {
        // Get cache object, query param.
        PointsReportCacheObjectDTO cacheObject = 
                queryCacheHandler.getCacheObject(queryId, PointsReportCacheObjectDTO.class);
        
        // Create CSV report.
        this.generatePointsReport(response,cacheObject.getReportRequest());
        
    }
    
    @Override
    public void generatePointsReport(HttpServletResponse response, PointsReportRequestDTO request) {        
        
        // Get enrollment fields data. Using EnrollmentFieldReportUtil to handle csv reports columns.
        EnrollmentFieldReportUtil enrollmentFieldReportUtil = 
                new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());
        
        // Verify request parameters.
        validateRequest(request, enrollmentFieldReportUtil);

        Date fromDate = DateUtil.convertToStartOfDayString(request.getFromDate());
        Date toDate = DateUtil.convertToEndOfDayString(request.getToDate());

        // Get report content.
        List<PointsReportContentDTO> reportContent = 
                pointsReportDao.getPointsReportContent(request.getType(), fromDate, toDate, request.getGroupId());

        // Default delivery method DOWNLOAD.
        DeliveryMethodTypes deliveryMethod = DeliveryMethodTypes.DOWNLOAD;;
        if(EnumUtils.isValidEnum(DeliveryMethodTypes.class, request.getDeliveryMethod())){
            deliveryMethod = DeliveryMethodTypes.valueOf(request.getDeliveryMethod());
        }
        
        switch(deliveryMethod){
            case DOWNLOAD:
                processDownloadDeliveryMethod(request, enrollmentFieldReportUtil, response, reportContent);
                break;
            default:
                break;
        }

    }
    
    private void processDownloadDeliveryMethod(PointsReportRequestDTO request, 
            EnrollmentFieldReportUtil enrollmentFieldReportUtil, HttpServletResponse response,
            List<PointsReportContentDTO> reportContent){
        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));

        //Create CSV report properties : headers, field mappings, processors 
        Map<String, Object []> csvReportProps = 
                PointsReportUtil.createCSVReportProperties(request,  enrollmentFieldReportUtil);

        String[] headers = (String[]) csvReportProps.get(ProjectConstants.HEADERS);
        String[] fieldMapping = (String[]) csvReportProps.get(ProjectConstants.FIELD_MAPPING);
        CellProcessor[] processor = (CellProcessor []) csvReportProps.get(ProjectConstants.PROCESSORS);
        
        //File name
        String filename = ReportTypes.valueOf(request.getType()).getFileName();

        long startTime = System.currentTimeMillis();
        if(reportsLoggingEnabled) {
            logger.info("processDownloadDeliveryMethod is calling writeDownloadCSVFile with the following params. " +
                            "status {} paxId {}", response.getStatus(), security.getPaxId());
        }
        // Create CSV file
        CsvFileUtil.writeDownloadCSVFile(response, filename, reportContent, processor, headers,fieldMapping);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("processDownloadDeliveryMethod execution has been completed in {} MILLISECONDS with the following params. " +
                    "status {} rowsAmount {} paxId {}", estimatedTime, response.getStatus(), reportContent.size(),security.getPaxId());
        }
    }

    /**
     * Verify request object and throw an exception in case it is invalid.
     * @param type Report's type.
     * @param request Report's request DTO.
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     */
    private void validateRequest(@Nonnull PointsReportRequestDTO request, 
            EnrollmentFieldReportUtil enrollmentFieldReportUtil) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        //Validate report type.
        if(request.getType() == null || request.getType().isEmpty()){
            errors.add(ReportConstants.REPORT_TYPE_MISSING_MESSAGE);
        }
        else {
            try{
                ReportTypes.valueOf(request.getType());
            }catch(IllegalArgumentException e){
                errors.add(ReportConstants.REPORT_TYPE_INVALID_MESSAGE);    
            }
        }
        
        // validate delivery method request.
        if (request.getDeliveryMethod() != null && !request.getDeliveryMethod().isEmpty()) {
            try{
                DeliveryMethodTypes deliveryMethod = DeliveryMethodTypes.valueOf(request.getDeliveryMethod());
                // validate deliveryEmail only if delivery method is Email.
                if (DeliveryMethodTypes.EMAIL.equals(deliveryMethod)
                        && (request.getDeliveryEmail() == null || request.getDeliveryEmail().isEmpty())) {
                    errors.add(ReportConstants.DELIVERY_EMAIL_MISSING_MESSAGE);
                }
            }catch(IllegalArgumentException e){
                errors.add(ReportConstants.DELIVERY_METHOD_INVALID_MESSAGE);
            }
        }

        // validate enrollment fields
        List<String> enrollmentFieldNames = enrollmentFieldReportUtil.getEnrollmentFieldNameAsList();
        
        if (request.getRecipientFields() != null && !request.getRecipientFields().isEmpty()) {
            for (String field : request.getRecipientFields()) {
                if (!enrollmentFieldNames.contains(field)) {
                    errors.add(ReportConstants.RECIPIENT_FIELDS_INVALID_MESSAGE);
                }
            }
        }
        
        // validate submitter enrollment fields
        if (request.getSubmitterFields() != null && !request.getSubmitterFields().isEmpty()) {
            for (String field : request.getSubmitterFields()) {
                if (!enrollmentFieldNames.contains(field)) {
                    errors.add(ReportConstants.SUBMITTER_FIELDS_INVALID_MESSAGE);
                }
            }
        }

        Date startDate =null;
        Date endDate = null;
        // validate from date
        if (request.getFromDate() == null || request.getFromDate().isEmpty()) {
            errors.add(ReportConstants.MISSING_FROM_DATE_TYPE_MESSAGE);
        } else if (!request.getFromDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            errors.add(new ErrorMessage()
                    .setCode(ReportConstants.ERROR_FROM_DATE_INVALID)
                    .setField(ProjectConstants.FROM_DATE)
                    .setMessage(ReportConstants.ERROR_FROM_DATE_INVALID_MSG));
        } else {
             startDate = DateUtil.convertToStartOfDayString(request.getFromDate());
        }
        // validate to date
        if (request.getToDate() == null || request.getToDate().isEmpty()) {
            errors.add(ReportConstants.MISSING_TO_DATE_TYPE_MESSAGE);
        } else if (!request.getToDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {
            errors.add(ReportConstants.INVALID_TO_DATE_TYPE_MESSAGE);
        }
        else{
            endDate = DateUtil.convertToEndOfDayString(request.getToDate());
        }
        // validate fromDate before ToDate only if both has value.

        if (startDate != null && endDate != null && !startDate.before(endDate) && !startDate.equals(endDate)) {
            errors.add(ReportConstants.FROM_DATE_IS_AFTER_TO_DATE_MESSAGE);
        }

        // validate group id
        if (request.getGroupId() != null && groupsDao.findById(request.getGroupId()) == null) {
            errors.add(ReportConstants.GROUP_ID_INVALID_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);
    }
    
}
