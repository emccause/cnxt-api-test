package com.maritz.culturenext.reports.service.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.base.Splitter;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.reports.constants.ParticipantReportsConstants;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.PointRedemptionAudienceDao;
import com.maritz.culturenext.reports.dao.PointRedemptionDetailReportDao;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.service.PointRedemptionDetailReportService;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;
import com.maritz.culturenext.reports.util.ParameterValidationUtil;

@Component
public class PointRedemptionDetailReportServiceImpl implements PointRedemptionDetailReportService {

    @Inject private EnrollmentConfigService enrollmentConfigService;
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ParameterValidationUtil parameterValidationUtil;
    @Inject private PointRedemptionDetailReportDao pointRedemptionDetailReportDao;
    @Inject private PointRedemptionAudienceDao pointRedemptionAudienceDao;
    @Value("${mars.subclient:}") private String subclientProductList;

    @Override
    public List<Map<String, Object>> getReportData(String commaSeparatedOptionalFieldsList, 
            String groupIdString, String fromDateString, String thruDateString) {
        
        List<ErrorMessage> errors = validateRequest(commaSeparatedOptionalFieldsList, 
                groupIdString, fromDateString, thruDateString);
        
        //get all subclients & product-codes (ex 'FCP=1494.2,GNC=1494.10')
        Map<String, String> subClientProductMap = Splitter.on(",").withKeyValueSeparator("=").split(subclientProductList); 
        List<String> productList = new ArrayList<>(subClientProductMap.keySet()); 
        List<String> subClientList = new ArrayList<>(subClientProductMap.values());
        
        if (subClientList.isEmpty() || productList.isEmpty() ) {
            //subClient or product is null
            errors.add(ReportConstants.MISSING_SUBCLIENT_PRODUCT_MESSAGE);
        }        

        ErrorMessageException.throwIfHasErrors(errors);
        
        Date fromDate = DateUtil.convertToStartOfDayString(fromDateString, TimeZone.getDefault());
        
        Date thruDate = null;
        if (thruDateString != null) {
            thruDate = DateUtil.convertToEndOfDayString(thruDateString,  TimeZone.getDefault());
        }

        //determine audience
        List<Map<String, Object>> audienceListMap =
                pointRedemptionAudienceDao.getRedemptionAudience(groupIdString, fromDate, thruDate);
        if (CollectionUtils.isEmpty(audienceListMap)) {
            return audienceListMap; // no one in audience, return empty data
        }
        
        // convert audience to a list
        List<Long> audienceList = new ArrayList<>();
        for (Map<String, Object> dataMap : audienceListMap) {
            Long paxId = (Long) dataMap.get(ProjectConstants.PAX_ID);
            if (paxId != null && !audienceList.contains(paxId)) { 
                audienceList.add(paxId);    
            }
        }
        
        // Get data from PCloud
        List<Map<String, Object>> redemptionData =
                pointRedemptionDetailReportDao.getRedemptionData(productList, subClientList, fromDate, thruDate);

        DecimalFormat df = new DecimalFormat("#");
        
        // combine audience with PCloud data - add to final results if it's in both
        List<Map<String, Object>> finalResults = new ArrayList<>();
        for (Map<String, Object> redemptionEntry : redemptionData) {
            Long paxId = (Long) redemptionEntry.get(ProjectConstants.PAX_ID);
            
            if (audienceList.contains(paxId)) {
                // format data
                Double transactionId = (Double) redemptionEntry.get(ReportConstants.TRANSACTION_ID_QUERY_HEADER);
                redemptionEntry.put(ReportConstants.TRANSACTION_ID_QUERY_HEADER, df.format(transactionId));
                
                Date transDate = (Date) redemptionEntry.get(ReportConstants.TRANSACTION_DATE_QUERY_HEADER);
                redemptionEntry.put(ReportConstants.TRANSACTION_DATE_QUERY_HEADER, DateUtil.convertToUTCDateTime(transDate));
                
                Date transConfirmDate = (Date) redemptionEntry.get(ReportConstants.TRANSACTION_CONFIRMATION_DATE_QUERY_HEADER);
                if (transConfirmDate != null) {
                    redemptionEntry.put(ReportConstants.TRANSACTION_CONFIRMATION_DATE_QUERY_HEADER, DateUtil.convertToUTCDateTime(transConfirmDate));
                }

                finalResults.add(redemptionEntry);
            }
        }
        
        return finalResults;
    }

    @Override
    public void getPointRedemptionDetailReport(
                HttpServletResponse response, String commaSeparatedOptionalFieldsList, String groupIdString,
                String fromDateString, String thruDateString
            ) {
        
        // Resolve report data
        List<Map<String, Object>> reportData = getReportData(
                commaSeparatedOptionalFieldsList, groupIdString,
                fromDateString, thruDateString
            );
        
        // Handle resolution of column mappings
        List<CsvColumnPropertiesDTO> reportColumnPropertiesList = new ArrayList<>(ReportConstants.POINT_REDEMPTION_DETAIL_REPORT_COLUMN_MAPPING);
        
        if (commaSeparatedOptionalFieldsList != null && !commaSeparatedOptionalFieldsList.trim().isEmpty()) {
            EnrollmentFieldReportUtil enrollmentFieldReportUtil = 
                    new EnrollmentFieldReportUtil(enrollmentConfigService.getEnrollmentFields());
            
            List<String> optionalFieldsList = Arrays.asList(commaSeparatedOptionalFieldsList.split(ProjectConstants.COMMA_DELIM));
            
            enrollmentFieldReportUtil.addCSVOptionalColumnsProps(reportColumnPropertiesList, ProjectConstants.EMPTY_STRING, ProjectConstants.RECIPIENT, optionalFieldsList);
        }
        
        // Resolve file name
        StringBuilder fileNameBuilder = new StringBuilder(ReportConstants.POINT_REDEMPTION_DETAIL_REPORT_FILE_NAME);
        fileNameBuilder.append(ProjectConstants.SPACE);
        fileNameBuilder.append(DateUtil.getCurrentTimeInUTCDate());
        
        // Write file to response
        CsvFileUtil.writeDownloadCSVFileFromMap(response, fileNameBuilder.toString(), reportData, reportColumnPropertiesList);
    }
    
    /**
     * Validate the requested parameters
     * @param commaSeparatedOptionalFieldsList
     * @param groupIdString
     * @param fromDateString
     * @param thruDateString
     * @return
     */
    private List<ErrorMessage> validateRequest(
                String commaSeparatedOptionalFieldsList, String groupIdString,
                String fromDateString, String thruDateString) {
        List<ErrorMessage> errors = new ArrayList<>();

        Date fromDate = null;
        Date thruDate = null;
        //fromDate
        if (fromDateString == null || fromDateString.length() == 0) {
            //fromDate is null
            errors.add(ReportConstants.MISSING_FROM_DATE_TYPE_MESSAGE);
        } else {
            fromDate = DateUtil.convertToStartOfDayString(fromDateString);
            
            if (fromDate == null) { // DateUtil method will return null if it can't parse the date
                errors.add(ReportConstants.INVALID_FROM_DATE_TYPE_MESSAGE);
            }
        }

        //thruDate
        if (thruDateString != null && !thruDateString.trim().isEmpty()) {
            thruDate = DateUtil.convertToEndOfDayString(thruDateString);
            
            if (thruDate == null) { // DateUtil method will return null if it can't parse the date
                errors.add(ReportConstants.INVALID_THRU_DATE_TYPE_MESSAGE);
            }
        }

        // Check if thru date is before from date
        if ((fromDate != null && thruDate != null) && (!fromDate.before(thruDate) || fromDate.equals(thruDate))) {
                errors.add(ReportConstants.FROM_DATE_IS_AFTER_TO_DATE_MESSAGE);
        }
        
        if (groupIdString != null) {
            Long groupId = null;
            try {
                groupId = Long.valueOf(groupIdString);
            } catch (NumberFormatException nfe) {
                //groupIdString is not valid
                errors.add(ParticipantReportsConstants.INVALID_GROUP_ID_FORMAT_MESSAGE);
            }

            if (groupId != null && !groupsDao.findBy().where(ProjectConstants.GROUP_ID).eq(groupId).exists()) {
                //group does not exist
                errors.add(ParticipantReportsConstants.INVALID_GROUP_ID_MESSAGE);
            }
        }
        
        errors.addAll(parameterValidationUtil.validateOptionalFields(
                    commaSeparatedOptionalFieldsList,
                    ReportConstants.ERROR_INVALID_RECIPIENT_FIELD,
                    ProjectConstants.RECIPIENT_FIELDS,
                    ReportConstants.ERROR_INVALID_FIELD_MSG)
                );
        
        return errors;
    }

}