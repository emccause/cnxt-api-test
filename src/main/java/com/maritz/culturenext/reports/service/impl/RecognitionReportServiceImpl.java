package com.maritz.culturenext.reports.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.ApprovalReportDao;
import com.maritz.culturenext.reports.dto.ApprovalReportDTO;
import com.maritz.culturenext.reports.service.RecognitionReportService;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PaxUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;

@Component
public class RecognitionReportServiceImpl implements RecognitionReportService {

    @Inject private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ApprovalReportDao approvalReportDao;
    @Inject private RecognitionService recognitionService;
    @Inject private Security security;
    @Inject private PaxUtil paxUtil;
    @Inject private Environment environment;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Void getApprovalsReport(HttpServletResponse response, String recognitionType, List<String> status, 
            String fromDateString, String toDateString, Long groupId) throws ErrorMessageException {

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("RecognitionReportServiceImpl.getApprovalsReport has been called with the following params. " +
                            "HttpStatus {} recognitionType {} statusList {} fromDate {} toDate {} groupId {} paxId {}"
                    , response.getStatus(),recognitionType,status.toString(),fromDateString,toDateString,groupId,security.getPaxId());
        }
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        List<ApprovalReportDTO> approvalReport = new ArrayList<>();

        //Check if logged in user is Admin or a Manager
        if(!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE, ProjectConstants.MANAGER_ROLE)) {
            errors.add(ReportConstants.NOT_ADMIN_OR_MANAGER_CODE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors).setStatus(HttpStatus.FORBIDDEN);
        }

        // Validate recognitionType
        if (recognitionType == null || recognitionType.length() == 0) {

            errors.add(ReportConstants.MISSING_RECOGNITION_TYPE_MESSAGE);

        } else if (!ReportConstants.VALID_RECOGNITION_TYPE.contains(recognitionType)) {

            errors.add(ReportConstants.INVALID_RECOGNITION_TYPE_MESSAGE);

        }
        // Validate status
        if (status == null || status.isEmpty()) {

            errors.add(ReportConstants.MISSING_STATUS_TYPE_MESSAGE);

        } else {
            for (String type: status) {
                if (!ReportConstants.VALID_STATUS.contains(type)){
                    errors.add(ReportConstants.INVALID_STATUS_TYPE_MESSAGE);
                    break;
                }
            }
        }

        // Validate fromDate
        if (fromDateString == null || fromDateString.length() == 0) {

            errors.add(ReportConstants.MISSING_FROM_DATE_TYPE_MESSAGE);

        } else if (!fromDateString.matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {

            errors.add(ReportConstants.INVALID_FROM_DATE_TYPE_MESSAGE);
        }
        // Validate toDate
        if (toDateString == null || toDateString.length() == 0) {

            errors.add(ReportConstants.MISSING_TO_DATE_TYPE_MESSAGE);

        } else if (!toDateString.matches(DateConstants.VALID_DATE_YYYY_MM_DD)) {

            errors.add(ReportConstants.INVALID_TO_DATE_TYPE_MESSAGE);

        }

        // Throw exception if any errors exists
        if (!errors.isEmpty()) {
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        //Convert String to Date
        Date fromDate = DateUtil.convertToStartOfDayString(fromDateString, TimeZone.getDefault());
        Date toDate = DateUtil.convertToEndOfDayString(toDateString,  TimeZone.getDefault());

        // Check if from date is before to date
        if (!fromDate.before(toDate) || fromDate.equals(toDate)) {
            errors.add(ReportConstants.FROM_DATE_IS_AFTER_TO_DATE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }


        if (security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {

            approvalReport = getApprovalsForAdmin(recognitionType, status, fromDate, toDate, groupId);

        } else if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE) && 
                security.hasRole(ProjectConstants.MANAGER_ROLE)) {

            approvalReport = getApprovalForManager(recognitionType, status, fromDate, toDate);
        }

        //Create csv and download
        CsvFileUtil.writeDownloadCSVFile(response, ReportConstants.FILE_NAME, approvalReport, 
                ReportConstants.PROCESSOR, ReportConstants.HEADERS,null);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("getApprovalsReport execution has been completed in {} MILLISECONDS with the following params. " +
                                    "HttpStatus {} recognitionType {} statusList {} fromDate {} toDate {} groupId {} rowsAmount {} paxId {}"
                            , estimatedTime,response.getStatus(),recognitionType,status.toString(),fromDateString,toDateString,groupId,approvalReport.size(),security.getPaxId());
        }
        return null;
    }


    private List<ApprovalReportDTO> getApprovalsForAdmin (String recognitionType, List<String> statusList, 
            Date fromDate, Date toDate, Long groupId) {
        
        List<ApprovalHistory> approvalHistoryList = new ArrayList<>();
        List<ApprovalPending> approvalPendingList = new ArrayList<>();

        if (statusList.contains(StatusTypeCode.APPROVED.toString()) || statusList.contains(ReportConstants.DENIED)) {
            approvalHistoryList = getApprovalHistory(recognitionType, statusList, fromDate, toDate, groupId, null);
        }

        if (statusList.contains(StatusTypeCode.PENDING.toString())) {
            approvalPendingList = getApprovalPending(recognitionType, fromDate, toDate, groupId, null);
        }


        return createApprovalReport(updateRecognitionDetails(approvalHistoryList, approvalPendingList));
    }

    private List<ApprovalReportDTO> getApprovalForManager (String recognitionType, List<String> statusList, 
            Date fromDate, Date toDate) {

        Long managerPaxId = security.getPaxId();
        List<ApprovalHistory> approvalHistoryList = new ArrayList<>();
        List<ApprovalPending> approvalPendingList = new ArrayList<>();

        //Get relationship, get pax that report to logged in manager
        List<Relationship> relationshipList = relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_2).eq(managerPaxId)
                .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.toString())
                .find();


        List<Long> paxIds = new ArrayList<>();
        for(Relationship relationship: relationshipList) {
            paxIds.add(relationship.getPaxId1());
        }

        //include manager pax
        paxIds.add(managerPaxId);

        if (statusList.contains(StatusTypeCode.APPROVED.toString()) || statusList.contains(ReportConstants.DENIED)) {
            approvalHistoryList = getApprovalHistory(recognitionType, statusList, fromDate, toDate, null, paxIds);
        }

        if (statusList.contains(StatusTypeCode.PENDING.toString())) {
            approvalPendingList = getApprovalPending(recognitionType, fromDate, toDate, null, paxIds);
        }

        return createApprovalReport(updateRecognitionDetails(approvalHistoryList, approvalPendingList));

    }

    private List<ApprovalReportDTO> createApprovalReport (List<RecognitionDetailsDTO> recognitionList) {

        List<ApprovalReportDTO> approvalReportList = new ArrayList<>();

        for (RecognitionDetailsDTO recognition: recognitionList) {
            ApprovalReportDTO approvalReport = new ApprovalReportDTO();

            if (recognition.getFromPax() != null) {
                approvalReport.setSubmitterFirstName(recognition.getFromPax().getFirstName());
                approvalReport.setSubmitterLastName(recognition.getFromPax().getLastName());
            }
            if (recognition.getToPax() != null) {
                approvalReport.setRecipientFirstName(recognition.getToPax().getFirstName());
                approvalReport.setRecipientLastName(recognition.getToPax().getLastName());
            }
            approvalReport.setAmount(recognition.getAwardAmount());
            approvalReport.setStatus(recognition.getStatus());
            approvalReport.setDenialReason(recognition.getApprovalComment());
            approvalReport.setApprovalDate(recognition.getDate());
            if (recognition.getApprovalPax() != null) {
                approvalReport.setApproverFirstName(recognition.getApprovalPax().getFirstName());
                approvalReport.setApproverLastName(recognition.getApprovalPax().getLastName());
            }
            approvalReport.setBudgetName(recognition.getBudgetName());
            approvalReport.setProgramName(recognition.getProgram());
            approvalReport.setProgramHeader(recognition.getHeadlineComment());
            approvalReport.setPrivateMessage(recognition.getNominationComment());

            approvalReportList.add(approvalReport);
        }

        return approvalReportList;
    }

    private List<ApprovalHistory> getApprovalHistory (String recognitionType, List<String> statusList, 
            Date fromDate, Date toDate, Long groupId, List<Long> managerPaxIds) {

        List<ApprovalHistory> approvalHistoryList = new ArrayList<>();
        List<Map<String, Object>> approvalList = new ArrayList<>();

        List<String> statusQuery = new ArrayList<>();

        //Create an array for query "REJECTED", "APPROVED"
        if (statusList.contains(StatusTypeCode.APPROVED.toString())) {
            statusQuery.add(StatusTypeCode.APPROVED.toString());
        }
        if (statusList.contains(ReportConstants.DENIED)) {
            statusQuery.add(StatusTypeCode.REJECTED.toString());
        }

        if (groupId == null && managerPaxIds == null) {

            //Get Approval History by Approval Status and dates
            approvalHistoryList = approvalHistoryDao.findBy()
                    .where(ProjectConstants.STATUS_TYPE_CODE).in(statusQuery)
                    .and(ProjectConstants.APPROVAL_DATE).between(fromDate, toDate)
                    .find();

        } else if (groupId != null) {

            if (recognitionType.equals(StatusTypeCode.APPROVED.toString())) {
                //Query by PaxIds that are the recognitions approval
                //Get Approval History by Approval Status, Approval Pax and dates
                approvalList = approvalReportDao.getApprovalHistoryByApproversForAdmin(statusQuery,
                        fromDate, toDate, groupId);
            } else if (recognitionType.equals(ReportConstants.GIVEN)) {
                //Query by PaxIds that gave recognitions
                //Get Approval History by Approval Status, Given Pax and dates
                approvalList = approvalReportDao.getApprovalHistoryByGiversForAdmin(statusQuery, 
                        fromDate, toDate, groupId);
            }

        } else if (managerPaxIds != null) {

            if (recognitionType.equals(StatusTypeCode.APPROVED.toString())) {
                //Query by PaxIds that are the recognitions approval
                //Get Approval History by Approval Status, Approval Pax and dates
                approvalList = approvalReportDao.getApprovalHistoryByApproversForManager(statusQuery, 
                        fromDate, toDate, managerPaxIds);
            } else if (recognitionType.equals(ReportConstants.GIVEN)) {
                //Query by PaxIds that gave recognitions
                //Get Approval History by Approval Status, Given Pax and dates
                approvalList = approvalReportDao.getApprovalHistoryByGiversForManager(statusQuery, 
                        fromDate, toDate, managerPaxIds);
            }

        }

        if (!approvalList.isEmpty()) {
            for(Map<String, Object> approval: approvalList) {
                ApprovalHistory approvalHistory = new ApprovalHistory();
                approvalHistory.setId((Long) approval.get(ProjectConstants.ID));
                approvalHistory.setPaxId((Long) approval.get(ProjectConstants.PAX_ID));
                approvalHistory.setApprovalProcessId((Long) approval.get(ProjectConstants.APPROVAL_PROCESS_ID));
                approvalHistory.setTargetId((Long) approval.get(ProjectConstants.TARGET_ID));
                approvalHistory.setTargetTable((String) approval.get(ProjectConstants.TARGET_TABLE));
                approvalHistory.setProxyPaxId((Long) approval.get(ProjectConstants.PROXY_PAX_ID));
                approvalHistory.setApprovalDate((Date) approval.get(ProjectConstants.APPROVAL_DATE));
                approvalHistory.setApprovalLevel((Integer) approval.get(ProjectConstants.APPROVAL_LEVEL));
                approvalHistory.setStatusTypeCode((String) approval.get(ProjectConstants.STATUS_TYPE_CODE));
                approvalHistory.setApprovalNotes((String) approval.get(ProjectConstants.APPROVAL_NOTES));

                approvalHistoryList.add(approvalHistory);
            }
        }


        return approvalHistoryList;
    }

    private List<ApprovalPending> getApprovalPending (String recognitionType, Date fromDate, Date toDate, 
            Long groupId, List<Long> managerPaxIds) {

        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        List<Map<String, Object>> approvalResult = new ArrayList<>();

        if (groupId == null && managerPaxIds == null) {
            // Get pending approvals by dates
            approvalResult = approvalReportDao.getApprovalPendingByDates(fromDate, toDate);

        } else if (groupId != null) {

            if (recognitionType.equals(StatusTypeCode.APPROVED.toString())) {
                // Query by pending approval assigned to group
                // Get pending approvals by dates and groupId
                approvalResult = approvalReportDao.getApprovalPendingByApproversForAdmin(fromDate, toDate, groupId);
            } else if (recognitionType.equals(ReportConstants.GIVEN)) {
                // Query by pending approval given by group
                // Get pending approvals by dates and groupId
                approvalResult = approvalReportDao.getApprovalPendingByGiversForAdmin(fromDate, toDate, groupId);
            }

        } else if (managerPaxIds != null) {

            if (recognitionType.equals(StatusTypeCode.APPROVED.toString())) {
                // Query by pending approval assigned to Pax that report to manager
                // Get pending approvals by dates and pax ids
                approvalResult = approvalReportDao.getApprovalPendingByApproversForManager(fromDate,
                        toDate, managerPaxIds);
            } else if (recognitionType.equals(ReportConstants.GIVEN)) {
                // Query by pending approval given by Pax that report to manager
                // Get pending approvals by dates and pax ids
                approvalResult = approvalReportDao.getApprovalPendingByGiversForManager(fromDate,
                        toDate, managerPaxIds);
            }

        }

        if (!approvalResult.isEmpty()) {
            for(Map<String, Object> approval: approvalResult){
                ApprovalPending approvalPending = new ApprovalPending();
                approvalPending.setId((Long) approval.get(ProjectConstants.ID));
                approvalPending.setPaxId((Long) approval.get(ProjectConstants.PAX_ID));
                approvalPending.setApprovalProcessId((Long) approval.get(ProjectConstants.APPROVAL_PROCESS_ID));
                approvalPending.setTargetId((Long) approval.get(ProjectConstants.TARGET_ID));
                approvalPending.setTargetTable((String) approval.get(ProjectConstants.TARGET_TABLE));
                approvalPending.setNextApproverPaxId((Long) approval.get(ProjectConstants.NEXT_APPROVER_PAX_ID));

                approvalPendingList.add(approvalPending);
            }
        }

        return approvalPendingList;
    }

    private List<RecognitionDetailsDTO> updateRecognitionDetails (List<ApprovalHistory> approvalHistoryList,
            List<ApprovalPending> approvalPendingList) {

        List<Long> recIds = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();


        Map<Long, ApprovalHistory> approvalHistoryMap = new HashMap<>();
        if (!approvalHistoryList.isEmpty()) {
            for(ApprovalHistory approval: approvalHistoryList) {
                //Map and filter out auto approved
                if (approval.getPaxId() != null || 
                        approval.getStatusTypeCode().equals(ProjectConstants.AUTO_APPROVED)) {
                    recIds.add(approval.getTargetId());
                    paxIds.add(approval.getPaxId());
                    approvalHistoryMap.put(approval.getTargetId(), approval);
                }

            }
        }


        Map<Long, ApprovalPending> approvalPendingMap = new HashMap<>();
        if (!approvalPendingList.isEmpty()) {
            for(ApprovalPending approval: approvalPendingList) {
                recIds.add(approval.getTargetId());
                if (approval.getPaxId() != null) {
                    paxIds.add(approval.getPaxId());
                }
                approvalPendingMap.put(approval.getTargetId(), approval);
            }
        }


        Map<Long, EmployeeDTO> employeePax = paxUtil.getPaxIdEmployeeDTOMap(paxIds);

        List<RecognitionDetailsDTO> recognitionDetailsList = recognitionService.getRecognitionDetailsByIds(recIds);


        if (!recognitionDetailsList.isEmpty()) {
            for(RecognitionDetailsDTO recognitionDetails: recognitionDetailsList) {
                Long id = recognitionDetails.getId();

                if(recognitionDetails.getStatus() != null && 
                        recognitionDetails.getStatus().equals(StatusTypeCode.PENDING.toString())) {

                    ApprovalPending approvalPending = approvalPendingMap.get(id);
                    recognitionDetails.setApprovalPax(employeePax.get(approvalPending.getPaxId()));

                } else {
                    ApprovalHistory approvalHistory = approvalHistoryMap.get(id);

                    if (approvalHistory != null) {
                        String formatDate = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT,
                                Locale.ENGLISH).format(approvalHistory.getApprovalDate());
                        recognitionDetails.setDate(formatDate);
                        recognitionDetails.setApprovalPax(employeePax.get(approvalHistory.getPaxId()));
                        if (approvalHistory.getStatusTypeCode().equals(StatusTypeCode.REJECTED.toString())) {
                            recognitionDetails.setApprovalComment(approvalHistory.getApprovalNotes());
                        }
                    }

                }

            }

        }


        return recognitionDetailsList;
    }

}
