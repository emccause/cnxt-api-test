package com.maritz.culturenext.reports.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.reports.constants.TransactionAuditDetailCsvReportConstants;
import com.maritz.culturenext.reports.dao.TransactionAuditDetailReportDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.apache.commons.collections4.CollectionUtils;

import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class TransactionAuditDetailReportDaoImpl extends AbstractDaoImpl implements TransactionAuditDetailReportDao {
    
    private static final String STATUS_LIST_PLACEHOLDER = "STATUS_LIST";
    private static final String TRANSACTION_DETAIL_REPORT_QUERY = 
        "SELECT " +
        "P.PAYOUT_ID,  " +
        "P.STATUS_DATE AS PAYOUT_STATUS_DATE,  " +
        "PX.CONTROL_NUM AS PARTICIPANT_ID,  " +
        "PX.FIRST_NAME AS PARTICIPANT_FIRST_NAME,  " +
        "PX.LAST_NAME AS PARTICIPANT_LAST_NAME,  " +
        "P.PAYOUT_AMOUNT AS AWARD_AMOUNT,  " +
        "TH.ID AS TRANSACTION_ID, " +
        "N.ID AS NOMINATION_ID, " +
        "SUB_PX.CONTROL_NUM AS SUBMITTER_ID, " +
        "SUB_PX.FIRST_NAME AS SUBMITTER_FIRST_NAME, " +
        "SUB_PX.LAST_NAME AS SUBMITTER_LAST_NAME, " +
        "B.DISPLAY_NAME AS BUDGET_NAME, " +
        "P.PROJECT_NUMBER, " +
        "P.SUB_PROJECT_NUMBER, " +
        "CASE P.STATUS_TYPE_CODE  " +
        "WHEN 'PROCESSED' THEN 'PENDING'  " +
        "ELSE P.STATUS_TYPE_CODE  " +
        "END AS STATUS, " +
        "PE.PAYOUT_ERROR_DESC AS PAYOUT_ERROR_DESC " +
        "FROM component.PAYOUT P  " +
        "LEFT JOIN component.PAYOUT_ERROR PE ON PE.PAYOUT_ID = P.PAYOUT_ID  " +
        "JOIN component.PAX PX ON PX.PAX_ID = P.PAX_ID  " +
        "JOIN component.EARNINGS_PAYOUT EP ON EP.PAYOUT_ID = P.PAYOUT_ID  " +
        "JOIN component.TRANSACTION_HEADER_EARNINGS THE ON THE.EARNINGS_ID = EP.EARNINGS_ID  " +
        "JOIN component.TRANSACTION_HEADER TH ON TH.ID = THE.TRANSACTION_HEADER_ID  " +
        "JOIN component.DISCRETIONARY D ON D.TRANSACTION_HEADER_ID = TH.ID  " +
        "LEFT JOIN component.RECOGNITION R ON R.ID = D.TARGET_ID AND D.TARGET_TABLE = 'RECOGNITION'  " +
        "LEFT JOIN component.NOMINATION N ON N.ID = R.NOMINATION_ID  " +
        "LEFT JOIN component.PAX SUB_PX ON SUB_PX.PAX_ID = N.SUBMITTER_PAX_ID  " +
        "JOIN component.VW_BUDGET_INFO_LIGHTWEIGHT B ON B.ID = D.BUDGET_ID_DEBIT  " +
        "WHERE P.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " ) ";
    
    /** 
     * Retrieve the transaction details for the given status
     * @param statusList  - List of budget status
     * @return transaction detail results in node format
     * 
     */
    @Override
    public List<Map<String, Object>> getTransactionDetailReportData(List<String> statusList) {
        String sql = TRANSACTION_DETAIL_REPORT_QUERY;
        
        if (!CollectionUtils.isEmpty(statusList)) {
            if (statusList.contains(StatusTypeCode.PENDING.name())) {
                Collections.replaceAll(statusList, StatusTypeCode.PENDING.name(), StatusTypeCode.PROCESSED.name());
            }
        } else {
            statusList = TransactionAuditDetailCsvReportConstants.validStatusTransactionAuditDetailReportDB;
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        
        return namedParameterJdbcTemplate.query(sql, params, new CamelCaseMapRowMapper());
    }
}
