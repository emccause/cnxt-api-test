package com.maritz.culturenext.reports.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.reports.dao.CashIssuanceHistoryDao;

@Component
public class CashIssuanceHistoryDaoImpl extends PaginatingDaoImpl implements CashIssuanceHistoryDao {
        
    private static final String PENDING_CASH_ISSUANCE_QUERY =    
        "SELECT "
            + "COUNT(P.PAYOUT_ID) AS 'TOTAL_RECORDS', "
            + "SUM(P.PAYOUT_AMOUNT) AS 'AWARD_AMOUNT' "
        + "FROM component.PAYOUT P "
        + "WHERE P.STATUS_TYPE_CODE = 'PROCESSED' "
        + "AND P.PAYOUT_TYPE_CODE = 'CASH' ";
    
    private static final String CASH_ISSUANCE_HISTORY_QUERY =
        "SELECT "
            + "B.CREATE_DATE AS 'REPORT_CREATE_DATE', "
            + "B.BATCH_ID AS 'BATCH_ID', "
            + "COUNT(P.PAYOUT_ID) AS 'TOTAL_RECORDS', "
            + "SUM(P.PAYOUT_AMOUNT) AS 'AWARD_AMOUNT', "
            + "'AdHoc' AS 'REPORT_TYPE', "
            + "B.STATUS_TYPE_CODE AS 'STATUS' "
        + "FROM component.PAYOUT P "
        + "JOIN component.BATCH B ON B.BATCH_ID=P.BATCH_ID "
        + "WHERE P.PAYOUT_TYPE_CODE = 'CASH' "
        + "AND B.BATCH_TYPE_CODE = 'CASH_REPORT' "
        + "GROUP BY B.BATCH_ID, B.CREATE_DATE, B.STATUS_TYPE_CODE ";
        
    private static final String CASH_ISSUANCE_HISTORY_ORDER_BY = "REPORT_CREATE_DATE DESC";


    @Override
    public List<Map<String, Object>> getPendingCashIssuance() {
        return namedParameterJdbcTemplate.query(PENDING_CASH_ISSUANCE_QUERY, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getCashIssuanceHistory(Integer pageNumber, Integer pageSize) {
        return getPageWithTotalRows(CASH_ISSUANCE_HISTORY_QUERY, null, new CamelCaseMapRowMapper(), CASH_ISSUANCE_HISTORY_ORDER_BY , pageNumber, pageSize);
    }
}
