package com.maritz.culturenext.reports.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.ApprovalReportDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ApprovalReportDaoImpl extends AbstractDaoImpl implements ApprovalReportDao {

    private final String GROUP_ID_PLACEHOLDER = "GROUP_ID_PLACEHOLDER";
    private final String SUBMITTER_PAX_IDS_PLACEHOLDER = "SUBMITTER_PAX_IDS_PLACEHOLDER";
    private final String STATUS_LIST_PLACEHOLDER = "STATUS_LIST_PLACEHOLDER";
    private final String FROM_DATE_PLACEHOLDER = "FROM_DATE_PLACEHOLDER";
    private final String TO_DATE_PLACEHOLDER = "TO_DATE_PLACEHOLDER";
    private final String APPROVAL_PAX_PLACEHOLDER = "APPROVAL_PAX_PLACEHOLDER";

    private final String GET_GROUP_MEMBERS =
            "(SELECT M.pax_id FROM component.VW_GROUP_TOTAL_MEMBERSHIP M " +
                    "JOIN " +
                    "(SELECT UG.PAX_ID FROM component.VW_USER_GROUPS_SEARCH_DATA UG " +
                        "WHERE UG.SEARCH_TYPE = 'P') USG " +
                    "ON M.pax_id = USG.PAX_ID " +
                    "WHERE M.group_id IN ( :" + GROUP_ID_PLACEHOLDER + " ) ) ";

    // Nomination Calls
    private final String GET_NOMINATIONS_BY_GROUP =
            "(SELECT N.ID, N.SUBMITTER_PAX_ID FROM component.NOMINATION N " +
                    "JOIN " + GET_GROUP_MEMBERS + " PX " +
                    "ON N.SUBMITTER_PAX_ID = PX.pax_id )";

    private final String GET_NOMINATIONS_BY_SUBMITTER_PAXIDS =
            "(SELECT N.ID, N.SUBMITTER_PAX_ID FROM component.NOMINATION N " +
                    "WHERE N.SUBMITTER_PAX_ID IN ( :" + SUBMITTER_PAX_IDS_PLACEHOLDER + " ) )";

    private final String GET_NOMINATIONS_BY_DATES =
            "(SELECT N.ID, N.SUBMITTER_PAX_ID, N.SUBMITTAL_DATE FROM component.NOMINATION N " +
                    "WHERE N.SUBMITTAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ")";

    private final String GET_NOMINATIONS_SUBMITTER_PAX_DATES =
            "(SELECT N.ID, N.SUBMITTER_PAX_ID, N.SUBMITTAL_DATE FROM component.NOMINATION N " +
                    "WHERE N.SUBMITTER_PAX_ID IN ( :" + SUBMITTER_PAX_IDS_PLACEHOLDER + " ) " +
                    "AND (N.SUBMITTAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ") )";

    private final String GET_NOMINATIONS_GROUP_DATES =
            "(SELECT N.ID, N.SUBMITTER_PAX_ID, N.SUBMITTAL_DATE FROM component.NOMINATION N " +
                    "JOIN " + GET_GROUP_MEMBERS + " PX " +
                    "ON N.SUBMITTER_PAX_ID = PX.pax_id " +
                    "WHERE (N.SUBMITTAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" +TO_DATE_PLACEHOLDER +") )";

    // Recognition Calls

    private final String GET_RECOGNITION_BY_NOMINATION_GROUP =
            "(SELECT R.ID, R.NOMINATION_ID FROM component.RECOGNITION R " +
                    "JOIN " + GET_NOMINATIONS_BY_GROUP + " NM " +
                    "ON R.NOMINATION_ID = NM.ID )";

    private final String GET_RECOGNITION_BY_NOMINATION_IDS =
            "(SELECT R.ID, R.NOMINATION_ID FROM component.RECOGNITION R " +
                    "JOIN " + GET_NOMINATIONS_BY_SUBMITTER_PAXIDS + " NM " +
                    "ON R.NOMINATION_ID = NM.ID )";

    private final String GET_PENDING_RECOGNITION_BY_NOMINATION_DATES =
            "(SELECT R.ID, R.NOMINATION_ID, R.STATUS_TYPE_CODE FROM component.RECOGNITION R " +
                    "JOIN " + GET_NOMINATIONS_BY_DATES + " NM " +
                    "ON R.NOMINATION_ID = NM.ID " +
                    "WHERE STATUS_TYPE_CODE = 'PENDING' )";

    private final String GET_RECOGNITION_BY_NOMINATION_SUBMITTER_PAX_DATES =
            "(SELECT R.ID, R.NOMINATION_ID, R.STATUS_TYPE_CODE FROM component.RECOGNITION R " +
                    "JOIN " + GET_NOMINATIONS_SUBMITTER_PAX_DATES + " NM " +
                    "ON R.NOMINATION_ID = NM.ID " +
                    "WHERE STATUS_TYPE_CODE = 'PENDING' )";

    private final String GET_PENDING_RECOGNITION_BY_NOMINATION_DATES_GROUP =
            "(SELECT R.ID, R.NOMINATION_ID, R.STATUS_TYPE_CODE FROM component.RECOGNITION R " +
                    "JOIN " + GET_NOMINATIONS_GROUP_DATES + " NM " +
                    "ON R.NOMINATION_ID = NM.ID " +
                    "WHERE STATUS_TYPE_CODE = 'PENDING' )";

    // Approval History Calls

    private final String GET_APPROVAL_HISTORY_BY_APPROVER_GROUP =
            "SELECT AH.ID, AH.PAX_ID, AH.APPROVAL_PROCESS_ID, AH.TARGET_ID, AH.PROXY_PAX_ID, " +
                    "AH.APPROVAL_DATE, AH.APPROVAL_LEVEL, AH.STATUS_TYPE_CODE, AH.APPROVAL_NOTES " +
                    "FROM component.APPROVAL_HISTORY AH " +
                    "JOIN " + GET_GROUP_MEMBERS + " PX " +
                    "ON AH.PAX_ID = PX.pax_id " +
                    "WHERE AH.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " ) " +
                    "AND AH.TARGET_TABLE = 'RECOGNITION' " +
                    "AND (AH.APPROVAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ")";

    private final String GET_APPROVAL_HISTORY_GIVER_GROUP =
            "SELECT AH.ID, AH.PAX_ID, AH.APPROVAL_PROCESS_ID, AH.TARGET_ID, AH.PROXY_PAX_ID, " +
                    "AH.APPROVAL_DATE, AH.APPROVAL_LEVEL, AH.STATUS_TYPE_CODE, AH.APPROVAL_NOTES " +
                    "FROM component.APPROVAL_HISTORY AH " +
                    "JOIN " + GET_RECOGNITION_BY_NOMINATION_GROUP + " R " +
                    "ON AH.TARGET_ID = R.ID AND AH.TARGET_TABLE = 'RECOGNITION'" +
                    "WHERE AH.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " ) " +
                    "AND (AH.APPROVAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ")";

    private final String GET_APPROVAL_HISTORY_APPROVER_PAX =
            "SELECT AH.ID, AH.PAX_ID, AH.APPROVAL_PROCESS_ID, AH.TARGET_ID, AH.PROXY_PAX_ID, " +
                    "AH.APPROVAL_DATE, AH.APPROVAL_LEVEL, AH.STATUS_TYPE_CODE, AH.APPROVAL_NOTES " +
                    "FROM component.APPROVAL_HISTORY AH " +
                    "WHERE AH.PAX_ID IN ( :" + APPROVAL_PAX_PLACEHOLDER + " ) " +
                    "AND AH.TARGET_TABLE = 'RECOGNITION' " +
                    "AND AH.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " ) " +
                    "AND (AH.APPROVAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ")";

    private final String GET_APPROVAL_HISTORY_GIVER_PAX =
            "SELECT AH.ID, AH.PAX_ID, AH.APPROVAL_PROCESS_ID, AH.TARGET_ID, AH.PROXY_PAX_ID, " +
                    "AH.APPROVAL_DATE, AH.APPROVAL_LEVEL, AH.STATUS_TYPE_CODE, AH.APPROVAL_NOTES " +
                    "FROM component.APPROVAL_HISTORY AH " +
                    "JOIN " + GET_RECOGNITION_BY_NOMINATION_IDS + " RC " +
                    "ON AH.TARGET_ID = RC.ID AND AH.TARGET_TABLE = 'RECOGNITION' " +
                    "WHERE AH.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " ) " +
                    "AND (AH.APPROVAL_DATE BETWEEN :" + FROM_DATE_PLACEHOLDER + " AND :" + TO_DATE_PLACEHOLDER + ")";

    // Approval Pending Calls

    private final String GET_APPROVAL_PENDING_BY_DATES =
            "Select AP.ID, AP.PAX_ID, AP.APPROVAL_PROCESS_ID, AP.TARGET__ID, AP.NEXT_APPROVER_PAX_ID "+
                    "FROM component.APPROVAL_PENDING AP " +
                    "JOIN " + GET_PENDING_RECOGNITION_BY_NOMINATION_DATES + " RC " +
                    "ON AP.TARGET_ID = RC.ID AND AP.TARGET_TABLE = 'RECOGNITION'";

    private final String GET_APPROVAL_PENDING_BY_APPROVERS =
            "Select AP.ID, AP.PAX_ID, AP.APPROVAL_PROCESS_ID, AP.TARGET_ID, AP.NEXT_APPROVER_PAX_ID "
            + "FROM component.APPROVAL_PENDING AP " +
                    "JOIN " + GET_PENDING_RECOGNITION_BY_NOMINATION_DATES + " RC " +
                    "ON AP.TARGET_ID = RC.ID AND AP.TARGET_TABLE = 'RECOGNITION' " +
                    "WHERE AP.PAX_ID IN ( :" + APPROVAL_PAX_PLACEHOLDER + " )";


    private final String GET_APPROVAL_PENDING_BY_GIVEN =
            "Select AP.ID, AP.PAX_ID, AP.APPROVAL_PROCESS_ID, AP.TARGET_ID, AP.NEXT_APPROVER_PAX_ID "
            + "FROM component.APPROVAL_PENDING AP " +
                    "JOIN " + GET_RECOGNITION_BY_NOMINATION_SUBMITTER_PAX_DATES + " RC " +
                    "ON AP.TARGET_ID = RC.ID AND AP.TARGET_TABLE = 'RECOGNITION'";

    private final String GET_APPROVAL_PENDING_BY_APPROVER_GROUP =
            "Select AP.ID, AP.PAX_ID, AP.APPROVAL_PROCESS_ID, AP.TARGET_ID, AP.NEXT_APPROVER_PAX_ID "
            + "FROM component.APPROVAL_PENDING AP " +
                    "JOIN " + GET_GROUP_MEMBERS + " PX " +
                    "ON AP.PAX_ID = PX.pax_id " +
                    "WHERE AP.TARGET_TABLE = 'RECOGNITION'";

    private final String GET_APPROVAL_PENDING_BY_GIVEN_GROUP =
            "Select AP.ID, AP.PAX_ID, AP.APPROVAL_PROCESS_ID, AP.TARGET_ID, AP.NEXT_APPROVER_PAX_ID "
            + "FROM component.APPROVAL_PENDING AP " +
                    "JOIN " + GET_PENDING_RECOGNITION_BY_NOMINATION_DATES_GROUP + " RC " +
                    "ON AP.TARGET_ID = RC.ID " + 
                    "WHERE AP.TARGET_TABLE = 'RECOGNITION'";


    @Override
    public List<Map<String, Object>> getApprovalHistoryByApproversForAdmin(List<String> statusList,
            Date fromTime, Date toTime, Long groupId) {

        String approvalHistoryQuery = GET_APPROVAL_HISTORY_BY_APPROVER_GROUP;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalHistoryQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalHistoryByGiversForAdmin(List<String> statusList, 
            Date fromTime, Date toTime, Long groupId) {

        String approvalHistoryQuery = GET_APPROVAL_HISTORY_GIVER_GROUP;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalHistoryQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalHistoryByApproversForManager(List<String> statusList, 
            Date fromTime, Date toTime, List<Long> paxIds) {
        if (paxIds == null || paxIds.isEmpty()) {
            return new ArrayList<>();
        }

        String approvalHistoryQuery = GET_APPROVAL_HISTORY_APPROVER_PAX;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(APPROVAL_PAX_PLACEHOLDER, paxIds);
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalHistoryQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalHistoryByGiversForManager(List<String> statusList, 
            Date fromTime, Date toTime, List<Long> paxIds) {
        if (paxIds == null || paxIds.isEmpty()) {
            return new ArrayList<>();
        }

        String approvalHistoryQuery = GET_APPROVAL_HISTORY_GIVER_PAX;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(SUBMITTER_PAX_IDS_PLACEHOLDER, paxIds);
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalHistoryQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalPendingByDates(Date fromTime, Date toTime) {
        String approvalPendingQuery = GET_APPROVAL_PENDING_BY_DATES;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalPendingQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalPendingByApproversForAdmin(Date fromTime, Date toTime, Long groupId) {
        String approvalPendingQuery = GET_APPROVAL_PENDING_BY_APPROVER_GROUP;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalPendingQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalPendingByGiversForAdmin(Date fromTime, Date toTime, Long groupId) {
        String approvalPendingQuery = GET_APPROVAL_PENDING_BY_GIVEN_GROUP;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);

        return namedParameterJdbcTemplate.query(approvalPendingQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalPendingByApproversForManager(Date fromTime, 
            Date toTime, List<Long> paxIds) {
        if (paxIds == null || paxIds.isEmpty()) {
            return new ArrayList<>();
        }

        String approvalPendingQuery = GET_APPROVAL_PENDING_BY_APPROVERS;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);
        params.addValue(APPROVAL_PAX_PLACEHOLDER, paxIds);

        return namedParameterJdbcTemplate.query(approvalPendingQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getApprovalPendingByGiversForManager(Date fromTime,Date toTime,List<Long> paxIds) {
        if (paxIds == null || paxIds.isEmpty()) {
            return new ArrayList<>();
        }

        String approvalPendingQuery = GET_APPROVAL_PENDING_BY_GIVEN;
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(FROM_DATE_PLACEHOLDER, fromTime);
        params.addValue(TO_DATE_PLACEHOLDER, toTime);
        params.addValue(SUBMITTER_PAX_IDS_PLACEHOLDER, paxIds);

        return namedParameterJdbcTemplate.query(approvalPendingQuery, params, new CamelCaseMapRowMapper());
    }
}
