package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PointRedemptionAudienceDao {

    List<Map<String, Object>> getRedemptionAudience(String groupIdString, Date startDate, Date endDate);
}
