package com.maritz.culturenext.reports.dao;

import java.util.List;
import java.util.Map;

public interface TransactionAuditDetailReportDao {
    
    List<Map<String, Object>> getTransactionDetailReportData(List<String> statusList);

}
