package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public interface ReportsQueryDataLoadDao {
    
    List<Long> getAllPax(Date startDate, Date endDate, List<Long> groupIdList, List<Long> paxIdList, 
            List<Long> directReportIdList, List<Long> orgReportIdList, Boolean allMembers) throws Exception;
    List<Long> getAllPrograms(Date startDate, Date endDate, List<Long> groupIdList, List<Long> paxIdList, 
            List<Long> directReportIdList, List<Long> orgReportIdList, Boolean allMembers) throws Exception;
    List<Map<String, Object>> getProgramSummaryList(List<Long> programIdList);
    List<Long> getAllBudgets(Date endDate) throws Exception;
    List<Map<String, Object>> getBudgetSummaryList(List<Long> budgetIdList);
    Map<Long, List<Long>> getPaxToBudgetIdMap(Date startDate, Date endDate, List<Long> groupIdList, 
            List<Long> paxIdList, List<Long> directReportIdList, List<Long> orgReportIdList,
            Boolean nonZeroGivingLimit) throws Exception;
    List<EmployeeDTO> getPaxInfoList(List<Long> paxIdList);

}
