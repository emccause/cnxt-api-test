package com.maritz.culturenext.reports.dao;


import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ApprovalReportDao {
    List<Map<String, Object>> getApprovalHistoryByApproversForAdmin (List<String> statusList, Date fromTime, 
            Date toTime, Long groupId);
    List<Map<String, Object>> getApprovalHistoryByGiversForAdmin (List<String> statusList, Date fromTime, 
            Date toTime, Long groupId);
    List<Map<String, Object>> getApprovalHistoryByApproversForManager (List<String> statusList, Date fromTime, 
            Date toTime, List<Long> submitterPaxIds);
    List<Map<String, Object>> getApprovalHistoryByGiversForManager (List<String> statusList, Date fromTime, 
            Date toTime, List<Long> submitterPaxIds);
    List<Map<String, Object>> getApprovalPendingByDates (Date fromTime, Date toTime);
    List<Map<String, Object>> getApprovalPendingByApproversForAdmin (Date fromTime, Date toTime, Long groupId);
    List<Map<String, Object>> getApprovalPendingByGiversForAdmin (Date fromTime, Date toTime, Long groupId);
    List<Map<String, Object>> getApprovalPendingByApproversForManager (Date fromTime, Date toTime, List<Long> paxIds);
    List<Map<String, Object>> getApprovalPendingByGiversForManager (Date fromTime, Date toTime, List<Long> paxIds);
}
