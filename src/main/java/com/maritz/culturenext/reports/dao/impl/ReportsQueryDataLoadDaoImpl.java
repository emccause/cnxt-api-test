package com.maritz.culturenext.reports.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.AclTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.reports.dao.ReportsQueryDataLoadDao;
import com.maritz.culturenext.util.DateUtil;

@Repository
public class ReportsQueryDataLoadDaoImpl extends AbstractDaoImpl implements ReportsQueryDataLoadDao {
    
    private static final Logger logger = LoggerFactory.getLogger(ReportsQueryDataLoadDaoImpl.class);
    
    private static final String START_DATE_SQL_PARAM = "START_DATE";
    private static final String END_DATE_SQL_PARAM = "END_DATE";
    private static final String HIERARCHY_PAX_ID_LIST_SQL_PARAM = "HIERARCHY_PAX_ID_LIST";
    private static final String PAX_ID_LIST_SQL_PARAM = "PAX_ID_LIST";
    private static final String DIRECT_REPORT_PAX_ID_LIST_SQL_PARAM = "DIRECT_REPORT_PAX_ID_LIST";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST";
    private static final String BUDGET_ID_LIST_SQL_PARAM = "BUDGET_ID_LIST";
    
    private static final String PARENT_BUDGET_ID = "PARENT_BUDGET_ID";
    
    private static final String NOT_SUPPORTED_STATUS_LIST = "'RESTRICTED', 'INACTIVE'";
    
    private static final String START_DATE_DECLARATION_FRAGMENT =
        "DECLARE @START_DATE DATETIME2 = CONVERT(DATETIME2, :" + START_DATE_SQL_PARAM + "); ";
    private static final String END_DATE_DECLARATION_FRAGMENT = 
        "DECLARE @END_DATE DATETIME2 = DATEADD(MS, -1, DATEADD(D, 1, CONVERT(DATETIME2, :" + END_DATE_SQL_PARAM + "))); ";

    private static final String CREATE_PAX_TEMP_TABLE_FRAGMENT = "CREATE TABLE #FINAL_PAX (PAX_ID BIGINT); ";
    private static final String CREATE_GROUP_TEMP_TABLE_FRAGMENT ="CREATE TABLE #FINAL_GROUPS (GROUP_ID BIGINT); ";
    private static final String POPULATE_PAX_TEMP_TABLE_FRAGMENT = 
        "WITH HIER_LIST(MGR_ID, PAX_ID) AS " +
        "(SELECT R.PAX_ID_2, R.PAX_ID_1  " +
        "FROM component.RELATIONSHIP R WHERE PAX_ID_2 IN (:" + HIERARCHY_PAX_ID_LIST_SQL_PARAM + ") " +
        "AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
        "AND UPDATE_DATE < @END_DATE " +
        "UNION ALL " +
        "SELECT R.PAX_ID_2, R.PAX_ID_1  " +
        "FROM component.HISTORY_RELATIONSHIP R WHERE PAX_ID_2 IN (:" + HIERARCHY_PAX_ID_LIST_SQL_PARAM + ") " +
        "AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
        "AND UPDATE_DATE < @END_DATE " +
        "AND @START_DATE < CHANGE_DATE " +
        "UNION ALL " +
        "SELECT R.PAX_ID_2, R.PAX_ID_1 " +
        "FROM component.RELATIONSHIP R " +
        "JOIN HIER_LIST H ON R.PAX_ID_2 = H.PAX_ID " +
        "AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
        "AND UPDATE_DATE < @END_DATE " +
        "UNION ALL " +
        "SELECT R.PAX_ID_2, R.PAX_ID_1 " +
        "FROM component.HISTORY_RELATIONSHIP R " +
        "JOIN HIER_LIST H ON R.PAX_ID_2 = H.PAX_ID " +
        "AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
        "AND UPDATE_DATE < @END_DATE " +
        "AND @START_DATE < CHANGE_DATE) " +
        "INSERT INTO #FINAL_PAX SELECT DISTINCT DATA.PAX_ID FROM ( " +
        // HISTORICAL_FIRST_LEVEL_GROUP_MEMBERS
        "(SELECT HGP.PAX_ID, HGP.FIRST_LEVEL_GROUP, HGP.SECOND_LEVEL_GROUP, HGP.THIRD_LEVEL_GROUP FROM ( " +
        "SELECT PAX_ID, GROUP_ID AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, NULL AS THIRD_LEVEL_GROUP"
        + " FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, NULL AS THIRD_LEVEL_GROUP "
        + "FROM component.HISTORY_GROUPS_PAX WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HGP " +
        " INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + ") "
                + "AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +")"
                + " AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG " +
        "on HGP.FIRST_LEVEL_GROUP = HG.GROUP_ID " +
        ") UNION " +
        // HISTORICAL_SECOND_LEVEL_GROUP_MEMBERS
        "(SELECT HPG.PAX_ID, HG.GROUP_ID AS FIRST_LEVEL_GROUP, HGG.FK2 AS SECOND_LEVEL_GROUP,"
        + " NULL AS THIRD_LEVEL_GROUP FROM  " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + ")"
                + " AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + 
            ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 "
        + "FROM component.HISTORY_ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 "
            + "AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG " + // HISTORICAL GROUPS - GROUPS
        "ON HG.GROUP_ID = HGG.FK1 " +
        "INNER JOIN  " +
        "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
        + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HPG " + // HISTORICAL GROUPS - PAX
        "ON HGG.FK2 = HPG.GROUP_ID " +
        ") UNION " +
        // HISTORICAL_THIRD_LEVEL_GROUP_MEMBERS
        "(SELECT HPG.PAX_ID, HG.GROUP_ID AS FIRST_LEVEL_GROUP, HGG1.FK2 AS SECOND_LEVEL_GROUP, HGG2.FK2 AS "
        + "THIRD_LEVEL_GROUP FROM " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + ") "
                + "AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + 
        ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION "
        + "WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG1 " + // HISTORICAL GROUPS - GROUPS
        "ON HG.GROUP_ID = HGG1.FK1 " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION "
        + "WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG2 " + // HISTORICAL GROUPS - GROUPS
        "ON HGG1.FK2 = HGG2.FK1 " +
        "INNER JOIN  " +
        "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
        + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HPG " + // HISTORICAL GROUPS - PAX
        "ON HGG2.FK2 = HPG.GROUP_ID " +
        ") UNION " +
        // PAX PROVIDED
        "(SELECT PAX_ID, NULL AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, NULL AS THIRD_LEVEL_GROUP "
        + "FROM component.PAX WHERE PAX_ID IN (:" + PAX_ID_LIST_SQL_PARAM + ") " +
        ") UNION " +
        // DIRECT REPORTS
        "(SELECT PAX_ID_1 AS PAX_ID, NULL AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, "
        + "NULL AS THIRD_LEVEL_GROUP FROM component.RELATIONSHIP WHERE PAX_ID_2 IN (:" + 
        DIRECT_REPORT_PAX_ID_LIST_SQL_PARAM + ") AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' "
                + "AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID_1 AS PAX_ID, NULL AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, "
        + "NULL AS THIRD_LEVEL_GROUP FROM component.HISTORY_RELATIONSHIP WHERE  PAX_ID_2 IN (:" +
        DIRECT_REPORT_PAX_ID_LIST_SQL_PARAM + ") AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' AND"
                + " UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") UNION " +
        "SELECT PAX_ID, NULL AS FIRST_LEVEL_GROUP, NULL AS SECOND_LEVEL_GROUP, "
        + "NULL AS THIRD_LEVEL_GROUP FROM HIER_LIST " +
        ") AS DATA " +
        // MAKE SURE EVERYTHING WAS ACTIVE DURING THE TIME PERIOD
        "INNER JOIN  " +
        "( SELECT PAX_ID FROM component.SYS_USER WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID FROM component.HISTORY_SYS_USER WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HSS " +
        "ON DATA.PAX_ID = HSS.PAX_ID " +
        "LEFT JOIN " +
        "( SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + 
        ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HGG1 " +
        "ON DATA.FIRST_LEVEL_GROUP = HGG1.GROUP_ID " +
        "LEFT JOIN " +
        "( SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST + 
        ") AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HGG2 " +
        "ON DATA.SECOND_LEVEL_GROUP = HGG2.GROUP_ID " +
        "LEFT JOIN " +
        "( SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE " +
        "UNION  " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE STATUS_TYPE_CODE NOT IN (" + NOT_SUPPORTED_STATUS_LIST +
        ") AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HGG3 " +
        "ON DATA.THIRD_LEVEL_GROUP = HGG3.GROUP_ID " +
        "WHERE (FIRST_LEVEL_GROUP IN (:" + GROUP_ID_LIST_SQL_PARAM + ") OR FIRST_LEVEL_GROUP IS NULL) " +
        "AND (DATA.FIRST_LEVEL_GROUP IS NULL OR HGG1.GROUP_ID IS NOT NULL) " +
        "AND (DATA.SECOND_LEVEL_GROUP IS NULL OR HGG2.GROUP_ID IS NOT NULL) " +
        "AND (DATA.THIRD_LEVEL_GROUP IS NULL OR HGG3.GROUP_ID IS NOT NULL) ";
    
    private static final String POPULATE_ALL_PAX_TEMP_TABLE_FRAGMENT = 
            "INSERT INTO #FINAL_PAX SELECT DISTINCT PAX_ID FROM component.PAX WHERE CREATE_DATE < @END_DATE ";

    private static final String GET_FINAL_PAX_SELECT_QUERY_FRAGMENT = "SELECT * FROM #FINAL_PAX ";

    private static final String POPULATE_GROUPS_TEMP_TABLE_FRAGMENT = 
        "INSERT INTO #FINAL_GROUPS SELECT DISTINCT GROUP_ID FROM ( " +
        // FIRST LEVEL GROUP
        "SELECT #FINAL_PAX.PAX_ID, HG.GROUP_ID FROM #FINAL_PAX " +
        "INNER JOIN " +
        "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
        + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HPG " +
        "ON #FINAL_PAX.PAX_ID = HPG.PAX_ID " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS "
        + "WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG " +
        "ON HPG.GROUP_ID = HG.GROUP_ID " +
        "UNION " +
        // SECOND LEVEL GROUP
        "SELECT #FINAL_PAX.PAX_ID, HG2.GROUP_ID FROM #FINAL_PAX " +
        "INNER JOIN " +
        "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
        + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HPG1 " +
        "ON #FINAL_PAX.PAX_ID = HPG1.PAX_ID " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS "
        + "WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG1 " +
        "ON HPG1.GROUP_ID = HG1.GROUP_ID " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION"
        + " WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG2 " + // HISTORICAL GROUPS - GROUPS
        "ON HG1.GROUP_ID = HGG2.FK1 " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS"
        + " WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG2 " +
        "ON HGG2.FK2 = HG2.GROUP_ID " +
        "UNION " +
        // THIRD LEVEL GROUP
        "SELECT #FINAL_PAX.PAX_ID, HG3.GROUP_ID FROM #FINAL_PAX " +
        "INNER JOIN " +
        "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
        + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HPG1 " +
        "ON #FINAL_PAX.PAX_ID = HPG1.PAX_ID " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS "
        + "WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG1 " +
        "ON HPG1.GROUP_ID = HG1.GROUP_ID " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION "
        + "WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG2 " + // HISTORICAL GROUPS - GROUPS
        "ON HG1.GROUP_ID = HGG2.FK1 " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS "
        + "WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG2 " +
        "ON HGG2.FK2 = HG2.GROUP_ID " +
        "INNER JOIN " +
        "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION "
        + "WHERE ASSOCIATION_TYPE_ID = 1000 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE " +
        ") AS HGG3 " + // HISTORICAL GROUPS - GROUPS
        "ON HG2.GROUP_ID = HGG3.FK1 " +
        "INNER JOIN " +
        "(SELECT GROUP_ID FROM component.GROUPS WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT GROUP_ID FROM component.HISTORY_GROUPS "
        + "WHERE STATUS_TYPE_CODE = 'ACTIVE' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HG3 " +
        "ON HGG3.FK2 = HG3.GROUP_ID " +
        ") AS HISTORICAL_GROUPS ";
    
    private static final String POPULATE_ALL_GROUPS_TEMP_TABLE_FRAGMENT =
            "INSERT INTO #FINAL_GROUPS SELECT DISTINCT DATA.GROUP_ID FROM (" +
            "SELECT GROUP_ID FROM component.GROUPS WHERE UPDATE_DATE < @END_DATE " +
            "UNION ALL " +
            "SELECT GROUP_ID FROM component.HISTORY_GROUPS WHERE CHANGE_DATE < @END_DATE) AS DATA ";
    
    private static final String GET_HISTORICAL_PROGRAMS_FRAGMENT = 
        "SELECT DISTINCT HACL.PROGRAM_ID FROM ( " +
        "SELECT TARGET_ID AS PROGRAM_ID, SUBJECT_TABLE, SUBJECT_ID "
        + "FROM component.ACL WHERE TARGET_TABLE = 'PROGRAM' AND UPDATE_DATE < @END_DATE " +
        "UNION " +
        "SELECT TARGET_ID AS PROGRAM_ID, SUBJECT_TABLE, SUBJECT_ID "
        + "FROM component.HISTORY_ACL "
        + "WHERE TARGET_TABLE = 'PROGRAM' AND UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE " +
        ") AS HACL " +
        "INNER JOIN " +
        "component.PROGRAM PRGM " +
        "ON HACL.PROGRAM_ID = PRGM.PROGRAM_ID " +
        "AND PRGM.FROM_DATE < @END_DATE " +
        "WHERE (SUBJECT_TABLE = 'GROUPS' AND SUBJECT_ID IN (SELECT GROUP_ID FROM #FINAL_GROUPS)) " +
        "OR (SUBJECT_TABLE = 'PAX' AND SUBJECT_ID IN (SELECT PAX_ID FROM #FINAL_PAX)) ";

    private static final String DROP_TEMP_PAX_TABLE_FRAGMENT ="DROP TABLE #FINAL_PAX ";
    private static final String DROP_TEMP_GROUP_TABLE_FRAGMENT ="DROP TABLE #FINAL_GROUPS";
    
    private static final String GET_ALL_BUDGETS_QUERY =
            "SELECT ID FROM component.BUDGET WHERE CREATE_DATE < @END_DATE AND FROM_DATE < @END_DATE";
    
    private static final String PROGRAM_ID_LIST_SQL_PARAM = "PROGRAM_ID_LIST";    
    private static final String GET_PROGRAM_SUMMARY_QUERY = 
            "SELECT " +
            "PRGM.PROGRAM_ID AS PROGRAM_ID, " +
            "PRGM.PROGRAM_NAME AS PROGRAM_NAME, " +
            "PRGM.PROGRAM_DESC AS PROGRAM_DESCRIPTION_SHORT, " +
            "PRGM.PROGRAM_LONG_DESC AS PROGRAM_DESCRIPTION_LONG, " +
            "PT.PROGRAM_TYPE_NAME AS PROGRAM_TYPE, " +
            "PRGM.PROGRAM_CATEGORY_CODE AS PROGRAM_CATEGORY, " +
            "PRGM.CREATE_DATE, " +
            "PRGM.FROM_DATE, " +
            "PRGM.THRU_DATE, " +
            "CASE " +
            "WHEN PRGM.STATUS_TYPE_CODE = 'INACTIVE' THEN 'ENDED' " +
            "WHEN PRGM.STATUS_TYPE_CODE = 'ACTIVE' AND PRGM.THRU_DATE IS NOT NULL "
            + "AND PRGM.THRU_DATE < GETDATE() THEN 'ENDED' " +
            "WHEN PRGM.STATUS_TYPE_CODE = 'ACTIVE' AND PRGM.FROM_DATE > GETDATE() THEN 'SCHEDULED' " +
            "ELSE PRGM.STATUS_TYPE_CODE " +
            "END AS STATUS, " +
            "NULL AS CREATOR_PAX " +
            "FROM component.PROGRAM PRGM " +
            "INNER JOIN component.PROGRAM_TYPE PT " +
            "ON PRGM.PROGRAM_TYPE_CODE = PT.PROGRAM_TYPE_CODE " +
            "WHERE PROGRAM_ID IN (:" + PROGRAM_ID_LIST_SQL_PARAM + ") ";
        
    private static final String GET_BUDGET_ELIGIBILITY_FRAGMENT = 
            "SELECT " +
            "PB.PROGRAM_ID, " +
            "PB.BUDGET_ID, " +
            "ACL.SUBJECT_TABLE, " +
            "ACL.SUBJECT_ID, " +
            "B.BUDGET_TYPE_CODE " +
            "FROM component.PROGRAM_BUDGET PB " +
            "INNER JOIN component.BUDGET B " +
            "ON PB.BUDGET_ID = B.ID " +
            "INNER JOIN component.ACL ACL " +
            "ON PB.ID = ACL.TARGET_ID " +
            "AND ACL.TARGET_TABLE = '" + AclTypeCode.PROGRAM_BUDGET.name() + "' " +
            "INNER JOIN component.ROLE ROLE " +
            "ON ACL.ROLE_ID = ROLE.ROLE_ID " +
            "AND ROLE.ROLE_CODE = '" + RoleCode.PROGRAM_BUDGET_GIVER.name() + "' " +
            "WHERE PB.PROGRAM_ID IN ( " + GET_HISTORICAL_PROGRAMS_FRAGMENT + " ) " +
            "AND B.FROM_DATE < @END_DATE ";
    
    private static final String GIVING_LIMIT_WHERE_CLAUSE = 
            "AND ( " +
            "( " +
            "B.BUDGET_TYPE_CODE = 'SIMPLE' " +
            "AND B.INDIVIDUAL_GIVING_LIMIT IS NOT NULL " +
            "AND B.INDIVIDUAL_GIVING_LIMIT > 0 " +
            ") " +
            "OR " +
            "B.BUDGET_TYPE_CODE = 'DISTRIBUTED' " +
            ") ";
    
    private static final String GET_PAX_WITH_GROUPS_FRAGMENT =
            "SELECT DISTINCT " +
            "GTM.PAX_ID, " +
            "GTM.GROUP_ID " +
            "FROM component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
            "INNER JOIN #FINAL_PAX FP " +
            "ON FP.PAX_ID = GTM.PAX_ID " +
            "ORDER BY GTM.PAX_ID ";
    
    private static final String GET_PAX_CHILD_BUDGETS = 
            "SELECT  " +
            "HGP.PAX_ID,  " +
            "HB.ID AS CHILD_BUDGET_ID,  " +
            "HB.PARENT_BUDGET_ID  " +
            "FROM  " +
            "(SELECT PAX_ID, GROUP_ID FROM component.GROUPS_PAX WHERE UPDATE_DATE < @END_DATE  " +
            "UNION " +
            "SELECT PAX_ID, GROUP_ID FROM component.HISTORY_GROUPS_PAX "
            + "WHERE UPDATE_DATE < @END_DATE AND @START_DATE < CHANGE_DATE  " +
            ") AS HGP " +
            "INNER JOIN " +
            "(SELECT FK1, FK2 FROM component.ASSOCIATION WHERE ASSOCIATION_TYPE_ID=3600 AND UPDATE_DATE < @END_DATE " +
            "UNION " +
            "SELECT FK1, FK2 FROM component.HISTORY_ASSOCIATION "
            + "WHERE ASSOCIATION_TYPE_ID = 3600 AND UPDATE_DATE < @END_DATE AND CHANGE_DATE > @START_DATE  " +
            ") AS HBG " +
            "ON HGP.GROUP_ID = HBG.FK2 " +
            "INNER JOIN " +
            "(SELECT ID, PARENT_BUDGET_ID FROM component.BUDGET WHERE UPDATE_DATE < @END_DATE  " +
            "UNION " +
            "SELECT ID, PARENT_BUDGET_ID FROM component.HISTORY_BUDGET "
            + "WHERE UPDATE_DATE < @END_DATE  AND CHANGE_DATE > @START_DATE  " +
            ") AS HB  " +
            "ON HBG.FK1 = HB.ID " +
            "WHERE HGP.PAX_ID IN (SELECT PAX_ID FROM #FINAL_PAX) ";
    private static final String PAX_ID = "PAX_ID";
    private static final String CHILD_BUDGET_ID = "CHILD_BUDGET_ID";
    private static final String PAX_MAP_KEY = "PAX";
    private static final String GROUP_MAP_KEY = "GROUPS";

    private static final String PAX_INFO_QUERY = 
            "SELECT DISTINCT " +
                    "CASE   " +
                    "    WHEN LAST_NAME IS NULL AND FIRST_NAME IS NULL THEN COMPANY_NAME  " +
                    "    ELSE FIRST_NAME + ' ' + LAST_NAME " +
                    "END AS 'SEARCH_NAME', " +
                    "P.PAX_ID, " +
                    "P.LANGUAGE_CODE, " +
                    "P.PREFERRED_LOCALE, " +
                    "P.CONTROL_NUM, " +
                    "P.FIRST_NAME, " +
                    "P.MIDDLE_NAME, " +
                    "P.LAST_NAME, " +
                    "P.NAME_PREFIX, " +
                    "P.NAME_SUFFIX, " +
                    "P.COMPANY_NAME, " +
                    "PF.VERSION, " +
                    "SU.STATUS_TYPE_CODE, " +
                    "PGM.MISC_DATA AS 'JOB_TITLE', " +
                    "RL.PAX_ID_2 AS 'MANAGER_PAX_ID' " +
                    "FROM COMPONENT.PAX P " +
                    "LEFT JOIN COMPONENT.SYS_USER SU " +
                    "ON SU.PAX_ID = P.PAX_ID " +
                    "LEFT JOIN COMPONENT.PAX_GROUP PG " +
                    "ON PG.PAX_ID = P.PAX_ID " +
                    "LEFT JOIN COMPONENT.PAX_GROUP_MISC PGM " +
                    "ON PGM.PAX_GROUP_ID = PG.PAX_GROUP_ID " +
                    "AND PGM.VF_NAME = 'JOB_TITLE' " +
                    "LEFT JOIN COMPONENT.RELATIONSHIP RL " +
                    "ON RL.PAX_ID_1 = P.PAX_ID " +
                    "AND RL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
                    "LEFT JOIN COMPONENT.PAX_FILES PF " +
                    "ON PF.PAX_ID = P.PAX_ID " +
                    "WHERE P.PAX_ID IN (:" + PAX_ID_LIST_SQL_PARAM + ") ";
    
    private static final String GET_BUDGET_SUMMARY_LIST_SQL = 
            "SELECT ID, BUDGET_NAME, DISPLAY_NAME AS BUDGET_DISPLAY_NAME, STATUS_TYPE_CODE " +
            "FROM component.VW_BUDGET_INFO_LIGHTWEIGHT " +
            "WHERE ID IN (:"  + BUDGET_ID_LIST_SQL_PARAM + ") and THRU_DATE is NULL";
        
    @Override
    @Transactional
    public List<Long> getAllPax(Date startDate, Date endDate, List<Long> groupIdList, List<Long> paxIdList,
            List<Long> directReportIdList, List<Long> orgReportIdList, Boolean allMembers) throws Exception {
        if (allMembers) {
            return handleTempTableQueries(startDate, endDate, groupIdList, paxIdList, directReportIdList,
                    orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + POPULATE_ALL_PAX_TEMP_TABLE_FRAGMENT, 
                    GET_FINAL_PAX_SELECT_QUERY_FRAGMENT, DROP_TEMP_PAX_TABLE_FRAGMENT);
        }
        return handleTempTableQueries(startDate, endDate, groupIdList, paxIdList, directReportIdList, 
                orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + POPULATE_PAX_TEMP_TABLE_FRAGMENT, 
                GET_FINAL_PAX_SELECT_QUERY_FRAGMENT, DROP_TEMP_PAX_TABLE_FRAGMENT);
    }

    @Override
    @Transactional
    public List<Long> getAllPrograms(Date startDate, Date endDate, List<Long> groupIdList, List<Long> paxIdList, 
            List<Long> directReportIdList, List<Long> orgReportIdList, Boolean allMembers) throws Exception {
        if (allMembers) {
            return handleTempTableQueries(startDate, endDate, groupIdList, paxIdList, directReportIdList, 
                    orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + CREATE_GROUP_TEMP_TABLE_FRAGMENT + 
                    POPULATE_ALL_PAX_TEMP_TABLE_FRAGMENT + POPULATE_ALL_GROUPS_TEMP_TABLE_FRAGMENT, 
                    GET_HISTORICAL_PROGRAMS_FRAGMENT, DROP_TEMP_PAX_TABLE_FRAGMENT + DROP_TEMP_GROUP_TABLE_FRAGMENT);
        }
        return handleTempTableQueries(startDate, endDate, groupIdList, paxIdList, directReportIdList, 
                orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + CREATE_GROUP_TEMP_TABLE_FRAGMENT + 
                POPULATE_PAX_TEMP_TABLE_FRAGMENT + POPULATE_GROUPS_TEMP_TABLE_FRAGMENT,
                GET_HISTORICAL_PROGRAMS_FRAGMENT, DROP_TEMP_PAX_TABLE_FRAGMENT + DROP_TEMP_GROUP_TABLE_FRAGMENT);
    }
    
    @Override
    public List<Map<String, Object>> getProgramSummaryList(List<Long> programIdList) {
    
        if (programIdList == null || programIdList.isEmpty()) {
            return new ArrayList<Map<String, Object>>();
        }
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID_LIST_SQL_PARAM, programIdList);

        return namedParameterJdbcTemplate.query(GET_PROGRAM_SUMMARY_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Long> getAllBudgets(Date endDate) throws Exception {
        return jdbcTemplate.queryForList(getDeclarations(null, endDate) + GET_ALL_BUDGETS_QUERY, Long.class);
    }

    @Override
    public List<Map<String, Object>> getBudgetSummaryList(List<Long> budgetIdList) {
        if (CollectionUtils.isEmpty(budgetIdList)) {
            return new ArrayList<Map<String, Object>>();
        }        
        List<Map<String, Object>> result = new ArrayList<>();
        //Break the number of parameters in groups of 2000 for each "in" operator. 
        //Sql server max param is 2100, so this keeps us safely below that
        for (Collection<Long> budgetListpartition : Iterables.partition(budgetIdList, ProjectConstants.SQL_PARAM_COUNT_MAX)) {        	
        	MapSqlParameterSource params = new MapSqlParameterSource();
        	params.addValue(BUDGET_ID_LIST_SQL_PARAM, budgetListpartition);
        	result.addAll(namedParameterJdbcTemplate.query(GET_BUDGET_SUMMARY_LIST_SQL, params, new CamelCaseMapRowMapper()));        	
        }
        return result;
    }

    @Override
    public Map<Long, List<Long>> getPaxToBudgetIdMap(Date startDate, Date endDate, List<Long> groupIdList, 
            List<Long> paxIdList, List<Long> directReportIdList, List<Long> orgReportIdList, 
            Boolean nonZeroGivingLimit) throws Exception {
        String query = null;
        if (groupIdList.isEmpty() && paxIdList.isEmpty() && directReportIdList.isEmpty() && orgReportIdList.isEmpty()) {
            query = prepQuery(startDate, endDate, groupIdList, paxIdList, directReportIdList, 
                    orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + CREATE_GROUP_TEMP_TABLE_FRAGMENT +
                    POPULATE_ALL_PAX_TEMP_TABLE_FRAGMENT + POPULATE_ALL_GROUPS_TEMP_TABLE_FRAGMENT);
        } else {
            query = prepQuery(startDate, endDate, groupIdList, paxIdList, directReportIdList, 
                    orgReportIdList, CREATE_PAX_TEMP_TABLE_FRAGMENT + CREATE_GROUP_TEMP_TABLE_FRAGMENT + 
                    POPULATE_PAX_TEMP_TABLE_FRAGMENT + POPULATE_GROUPS_TEMP_TABLE_FRAGMENT);
        }
        
        String budgetEligibility = GET_BUDGET_ELIGIBILITY_FRAGMENT;
        if (nonZeroGivingLimit) {
            budgetEligibility += GIVING_LIMIT_WHERE_CLAUSE;
        }
        
        SingleConnectionDataSource scds = null;
        JdbcTemplate scdsJdbcTemplate = null;
        List<Map<String, Object>> budgetEligibilityList = null;
        List<Map<String, Object>> paxWithGroupsList = null;
        List<Map<String, Object>> paxToChildBudgetList = null;
        try {
            scds = new SingleConnectionDataSource(jdbcTemplate.getDataSource().getConnection(), true);
            scdsJdbcTemplate = new JdbcTemplate(scds);
            scdsJdbcTemplate.update(query);
            budgetEligibilityList = 
                    scdsJdbcTemplate.query(getDeclarations(startDate, endDate) + budgetEligibility, new ColumnMapRowMapper());
            paxWithGroupsList = scdsJdbcTemplate.query(GET_PAX_WITH_GROUPS_FRAGMENT, new ColumnMapRowMapper());
            paxToChildBudgetList = 
                    scdsJdbcTemplate.query(getDeclarations(startDate, endDate) + GET_PAX_CHILD_BUDGETS, new ColumnMapRowMapper());
            scdsJdbcTemplate.execute(DROP_TEMP_PAX_TABLE_FRAGMENT + DROP_TEMP_GROUP_TABLE_FRAGMENT);
        } catch (Exception e) {
            logger.error("The data load query has failed. " + e.getMessage());
            throw e;
        } finally {
            scds.destroy();
        }
        // None of the below would be needed if budget eligibility was able to be parsed by SQL
        
        if (budgetEligibilityList == null || budgetEligibilityList.isEmpty() || paxWithGroupsList == null || paxWithGroupsList.isEmpty()) {
            return new HashMap<Long, List<Long>>();
        }
        
        // process the data
        List<Long> parentBudgetIdList = new ArrayList<Long>();
        // build up eligibility model of budgets
        Map<Long,Map<String,List<Long>>> budgetEligibilityModelMap = new HashMap<Long,Map<String,List<Long>>>();
        for (Map<String,Object> budgetEligibilityEntry: budgetEligibilityList) {
            Long budgetId = (Long) budgetEligibilityEntry.get("BUDGET_ID");
            String budgetTypeCode = (String) budgetEligibilityEntry.get("BUDGET_TYPE_CODE");
            String subjectTable = (String) budgetEligibilityEntry.get("SUBJECT_TABLE");
            Long subjectId = (Long) budgetEligibilityEntry.get("SUBJECT_ID");
            
            if (budgetTypeCode.equalsIgnoreCase(BudgetTypeEnum.DISTRIBUTED.name())) {
                if (!parentBudgetIdList.contains(budgetId)) {
                    parentBudgetIdList.add(budgetId);
                }
            }
            Map<String, List<Long>> memberMap = budgetEligibilityModelMap.get(budgetId);
            List<Long> budgetPaxIdList = null;
            List<Long> budgetGroupIdList = null;
            if (memberMap == null) {
                memberMap = new HashMap<String, List<Long>>();
                budgetPaxIdList = new ArrayList<Long>();
                budgetGroupIdList = new ArrayList<Long>();
            } else { 
                budgetPaxIdList = memberMap.get(PAX_MAP_KEY);
                budgetGroupIdList = memberMap.get(GROUP_MAP_KEY);
            }
            // break the giving members list into a pax id array and a group id array for later processing
            if (AclTypeCode.GROUPS.name().equalsIgnoreCase(subjectTable)) {
                budgetGroupIdList.add(subjectId);
            } else { // pax
                budgetPaxIdList.add(subjectId);
            }
            memberMap.put(PAX_MAP_KEY, budgetPaxIdList);
            memberMap.put(GROUP_MAP_KEY, budgetGroupIdList);
            
            budgetEligibilityModelMap.put(budgetId, memberMap);
        }
        
        // Build up pax / group model
        Map<Long, List<Long>> paxGroupMemberModelMap = new HashMap<Long, List<Long>>();
        for (Map<String,Object> paxWithGroupObject: paxWithGroupsList) {
            Long paxId = (Long) paxWithGroupObject.get("PAX_ID");
            Long groupId = (Long) paxWithGroupObject.get("GROUP_ID");
            // load up pax's list of groups, if it exists
            List<Long> paxGroupIdList = paxGroupMemberModelMap.get(paxId);
            if (paxGroupIdList == null) {
                paxGroupIdList = new ArrayList<Long>();
            }
            if (!paxGroupIdList.contains(groupId)) {
                // add the group id if it doesn't already exist in the list
                paxGroupIdList.add(groupId);
            }
            paxGroupMemberModelMap.put(paxId, paxGroupIdList);
        }
        
        Map<Long, List<Long>> paxWithGivingLimitBudgetIdMap = new HashMap<Long, List<Long>>();
        List<Long> allBudgetsIdList = new ArrayList<Long>();
        
        // loop through pax to see if they're eligible for any of the budgets
        for (Long paxId: paxGroupMemberModelMap.keySet()) {
            List<Long> paxGroupIdList = paxGroupMemberModelMap.get(paxId);
            for (Long budgetId: budgetEligibilityModelMap.keySet()) {
                Map<String, List<Long>> memberMap = budgetEligibilityModelMap.get(budgetId);
                List<Long> budgetPaxIdList = memberMap.get(PAX_MAP_KEY);
                List<Long> budgetGroupIdList = memberMap.get(GROUP_MAP_KEY);
                boolean matchFound = false;
                // check pax against budget's eligible members
                if (budgetPaxIdList.contains(paxId)) {
                    matchFound = true;
                }
                if (matchFound == false) {
                    // check pax's groups gainst budget's eligible members
                    for (Long groupId: paxGroupIdList) {
                        if (budgetGroupIdList.contains(groupId)) {
                            matchFound = true;
                            // no need to search further
                            break;
                        }
                    }
                }
                if (matchFound == true) {
                    List<Long> paxBudgetIdList = paxWithGivingLimitBudgetIdMap.get(paxId);
                    if (paxBudgetIdList == null) {
                        paxBudgetIdList = new ArrayList<Long>();
                    }
                    if (!paxBudgetIdList.contains(budgetId)) {
                        paxBudgetIdList.add(budgetId);
                        paxWithGivingLimitBudgetIdMap.put(paxId, paxBudgetIdList);
                    }
                    if (!allBudgetsIdList.contains(budgetId)) {
                        allBudgetsIdList.add(budgetId);
                    }
                }
            }
        }
        
        
        Map<Long, Map<Long, Long>> paxToChildBudgetMap = new HashMap<Long, Map<Long, Long>>();
        for (Map<String, Object> node: paxToChildBudgetList) {
            Long paxId = (Long) node.get(PAX_ID);
            Long childBudgetId = (Long) node.get(CHILD_BUDGET_ID);
            Long parentBudgetId = (Long) node.get(PARENT_BUDGET_ID);
            // Build up a mapping of parent id : child id for each pax
            Map<Long, Long> budgetMap = paxToChildBudgetMap.get(paxId);
            if (budgetMap == null) {
                budgetMap = new HashMap<Long, Long>();
            }
            if (!budgetMap.containsKey(parentBudgetId)) {
                budgetMap.put(parentBudgetId, childBudgetId);
            }
            paxToChildBudgetMap.put(paxId, budgetMap);
        }
        
        List<Long> idsToRemove = new ArrayList<Long>();
        for (Long paxId: paxWithGivingLimitBudgetIdMap.keySet()) {
            Map<Long,Long> parentChildBudgetMap = paxToChildBudgetMap.get(paxId);
            List<Long> paxBudgetIdList = paxWithGivingLimitBudgetIdMap.get(paxId);
            if (parentChildBudgetMap == null) {
                // remove any remaining parent budget ids (not eligible for any children of that budget)
                paxBudgetIdList.removeAll(parentBudgetIdList);
                if (paxBudgetIdList.isEmpty()) {
                    idsToRemove.add(paxId);
                }
                continue;
            }
            for (Long parentBudgetId: parentChildBudgetMap.keySet()) {
                if(paxBudgetIdList.contains(parentBudgetId)){
                    Long childBudgetId = parentChildBudgetMap.get(parentBudgetId);
                    // replace parent budget id with child budget id
                    paxBudgetIdList.add(childBudgetId);
                    paxBudgetIdList.remove(parentBudgetId);
                    // handle all budgets list
                    if (!allBudgetsIdList.contains(childBudgetId)) {
                        allBudgetsIdList.add(childBudgetId);
                    }
                    if (nonZeroGivingLimit == true) {
                        // Parent budgets don't have giving limits, so remove
                        allBudgetsIdList.remove(parentBudgetId);
                    }
                }
            }
            // remove any remaining parent budget ids (not eligible for any children of that budget)
            paxBudgetIdList.removeAll(parentBudgetIdList);
            if (paxBudgetIdList.isEmpty()) {
                idsToRemove.add(paxId);
            }
        }
        
        if (nonZeroGivingLimit == true) {
            // remove any remaining parent budget ids (not eligible for any children of that budget)
            allBudgetsIdList.removeAll(parentBudgetIdList);
        }
        
        if (!idsToRemove.isEmpty()) {
            for (Long id: idsToRemove) {
                paxWithGivingLimitBudgetIdMap.remove(id);
            }
        }
        
        paxWithGivingLimitBudgetIdMap.put(0L, allBudgetsIdList);
        
        return paxWithGivingLimitBudgetIdMap;
    }

    @Override
    public List<EmployeeDTO> getPaxInfoList(List<Long> paxIdList) {
        if (paxIdList == null || paxIdList.isEmpty()) {
            return new ArrayList<EmployeeDTO>();
        }
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_LIST_SQL_PARAM, paxIdList);
        
        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(PAX_INFO_QUERY, params, new CamelCaseMapRowMapper());

        ArrayList<EmployeeDTO> resultsList = new ArrayList<EmployeeDTO>();
        for (Map<String, Object> node: nodes) {
            resultsList.add(new EmployeeDTO(node, null));
        }
        return resultsList;
    }
    
    private List<Long> handleTempTableQueries(Date startDate, Date endDate, List<Long> groupIdList,
            List<Long> paxIdList, List<Long> directReportIdList, List<Long> orgReportIdList, String populate,
            String select, String cleanUp) throws Exception {

        String query = prepQuery(startDate, endDate, groupIdList, paxIdList, directReportIdList,
                orgReportIdList, populate);

        SingleConnectionDataSource scds = null;
        JdbcTemplate scdsJdbcTemplate = null;
        
        try {
            scds = new SingleConnectionDataSource(jdbcTemplate.getDataSource().getConnection(), true);
            scdsJdbcTemplate = new JdbcTemplate(scds);
            scdsJdbcTemplate.update(query);
            List<Long> nodes = scdsJdbcTemplate.queryForList(getDeclarations(startDate, endDate) + select, Long.class);
            scdsJdbcTemplate.execute(cleanUp);
            return nodes;
        } catch (Exception e) {
            logger.error("The data load query has failed. " + e.getMessage());
            throw e;
        } finally {
            scds.destroy();
        }
    }
    
    private String prepQuery(Date startDate, Date endDate, List<Long> groupIdList, List<Long> paxIdList,
            List<Long> directReportIdList, List<Long> orgReportIdList, String populate) {
        List<Long> finalGroupIdList = new ArrayList<Long>();
        finalGroupIdList.addAll(groupIdList);
        if (finalGroupIdList.isEmpty()) {
            finalGroupIdList.add(0L);
        }
        
        List<Long> finalPaxIdList = new ArrayList<Long>();
        finalPaxIdList.addAll(paxIdList);
        if (finalPaxIdList.isEmpty()) {
            finalPaxIdList.add(0L);
        }
        
        List<Long> finalDirectReportIdList = new ArrayList<Long>();
        finalDirectReportIdList.addAll(directReportIdList);
        if (finalDirectReportIdList.isEmpty()) {
            finalDirectReportIdList.add(0L);
        }

        List<Long> finalOrgReportIdList = new ArrayList<Long>();
        finalOrgReportIdList.addAll(orgReportIdList);
        if (finalOrgReportIdList.isEmpty()) {
            finalOrgReportIdList.add(0L);
        }
        
        String query = getDeclarations(startDate, endDate) + populate;
        
        StringBuilder groupIdListStringBuilder = new StringBuilder();
        for (Long id : finalGroupIdList) {
            groupIdListStringBuilder.append(id + ",");
        }
        groupIdListStringBuilder.deleteCharAt(groupIdListStringBuilder.length() - 1);
        query = query.replace(":" + GROUP_ID_LIST_SQL_PARAM, groupIdListStringBuilder.toString());
        
        StringBuilder paxIdListStringBuilder = new StringBuilder();
        for (Long id : finalPaxIdList) {
            paxIdListStringBuilder.append(id + ",");
        }
        paxIdListStringBuilder.deleteCharAt(paxIdListStringBuilder.length() - 1);
        query = query.replace(":" + PAX_ID_LIST_SQL_PARAM, paxIdListStringBuilder.toString());
        
        StringBuilder directReportIdListStringBuilder = new StringBuilder();
        for (Long id : finalDirectReportIdList) {
            directReportIdListStringBuilder.append(id + ",");
        }
        directReportIdListStringBuilder.deleteCharAt(directReportIdListStringBuilder.length() - 1);
        query = query.replace(":" + DIRECT_REPORT_PAX_ID_LIST_SQL_PARAM, directReportIdListStringBuilder.toString());
        
        StringBuilder orgReportIdListStringBuilder = new StringBuilder();
        for (Long id : finalOrgReportIdList) {
            orgReportIdListStringBuilder.append(id + ",");
        }
        orgReportIdListStringBuilder.deleteCharAt(orgReportIdListStringBuilder.length() - 1);
        query = query.replace(":" + HIERARCHY_PAX_ID_LIST_SQL_PARAM, orgReportIdListStringBuilder.toString());
        
        return query;
    }
    
    private String getDeclarations(Date startDate, Date endDate) {
        
        String declarations = new String();

        if (startDate != null) {
            declarations += START_DATE_DECLARATION_FRAGMENT.replace(":" + START_DATE_SQL_PARAM, "'" + 
                    DateUtil.convertToUTCDate(startDate) + "'");
        }
        if (endDate != null) {
            declarations += END_DATE_DECLARATION_FRAGMENT.replace(":" + END_DATE_SQL_PARAM,  "'" + 
                    DateUtil.convertToUTCDate(endDate) + "'");
        }
        
        return declarations;
    }
}
