package com.maritz.culturenext.reports.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.ParticipantReportsDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ParticipantReportsDaoImpl extends AbstractDaoImpl implements ParticipantReportsDao {
    
    private static final String FROM_DATE_SQL_PARAM = "FROM_DATE";
    private static final String THRU_DATE_SQL_PARAM = "THRU_DATE";
    private static final String PAX_ID_SQL_PARAM = "PAX_ID";
    private static final String GROUP_ID_SQL_PARAM = "GROUP_ID";
    private static final String PAX_TEAM_SQL_PARAM = "PAX_TEAM";
    private static final String PAX_ORG_SQL_PARAM = "PAX_ORG";
    private static final String TYPE_LIST_SQL_PARAM = "TYPE_LIST";
    private static final String STATUS_LIST_SQL_PARAM = "STATUS_LIST";
    private static final String PROGRAM_TYPE_LIST_SQL_PARAM = "PROGRAM_TYPE_LIST";
    private static final String PROGRAM_ID_SQL_PARAM = "PROGRAM_ID";
    private static final String BATCH_ID_SQL_PARAM = "BATCH_ID";

    private static final String PARTICIPANT_TRANSACTIONS_REPORT_QUERY = 
            "EXEC UP_PARTICIPANT_TRANSACTIONS_REPORT "
            + ":" + FROM_DATE_SQL_PARAM + ", "
            + ":" + THRU_DATE_SQL_PARAM + ", "
            + ":" + PAX_ID_SQL_PARAM + ", "
            + ":" + GROUP_ID_SQL_PARAM + ", "
            + ":" + PAX_TEAM_SQL_PARAM + ", "
            + ":" + PAX_ORG_SQL_PARAM + ", "
            + ":" + TYPE_LIST_SQL_PARAM + ", "
            + ":" + STATUS_LIST_SQL_PARAM + ", "
            + ":" + PROGRAM_TYPE_LIST_SQL_PARAM + ", "
            + ":" + PROGRAM_ID_SQL_PARAM + ", "
            + ":" + BATCH_ID_SQL_PARAM;

    @Override
    public List<Map<String, Object>> getParticipantTransactionReportData(
            Date fromDate, Date thruDate, Long paxId, Long groupId, 
            Long paxTeam, Long paxOrg, String typeList, String statusList,
            String programTypeList, Long programId, Long batchId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(FROM_DATE_SQL_PARAM, fromDate);
        params.addValue(THRU_DATE_SQL_PARAM, thruDate);
        params.addValue(PAX_ID_SQL_PARAM, paxId);
        params.addValue(GROUP_ID_SQL_PARAM, groupId);
        params.addValue(PAX_TEAM_SQL_PARAM, paxTeam);
        params.addValue(PAX_ORG_SQL_PARAM, paxOrg);
        params.addValue(TYPE_LIST_SQL_PARAM, typeList);
        params.addValue(STATUS_LIST_SQL_PARAM, statusList);
        params.addValue(PROGRAM_TYPE_LIST_SQL_PARAM, programTypeList);
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);
        params.addValue(BATCH_ID_SQL_PARAM, batchId);

        return namedParameterJdbcTemplate.query(
                PARTICIPANT_TRANSACTIONS_REPORT_QUERY, params, new CamelCaseMapRowMapper());
    }
}
