package com.maritz.culturenext.reports.dao;

import java.util.List;
import java.util.Map;

public interface CashIssuanceHistoryDao {
    List<Map<String, Object>> getPendingCashIssuance();
    List<Map<String, Object>> getCashIssuanceHistory(Integer pageNumber, Integer pageSize);
}
