package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PendingEmailsReportDao {

    List<Map<String, Object>> getPendingEmailsReport();
}