package com.maritz.culturenext.reports.dao;

import java.util.List;
import java.util.Map;

public interface CashIssuanceReportsDao {

    /**
     * Process cash payouts
     * @return a list of typeClass objects, populated with the csv file content.
     */
    List<Map<String, Object>> processCashPayouts();
    
    /**
     * Save cash issuance report
     * @param batchId 
     * @param file 
     * @param fileName 
     * @return a map with the processing results
     */
    List<Map<String, Object>> saveCashIssuanceReport(Long batchId, String file, String fileName);
}
