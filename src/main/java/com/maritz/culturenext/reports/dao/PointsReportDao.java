package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;

import com.maritz.culturenext.reports.dto.PointsReportContentDTO;

public interface PointsReportDao {
    List<PointsReportContentDTO> getPointsReportContent(String queryType, Date start, Date end, Long groupId );
}
