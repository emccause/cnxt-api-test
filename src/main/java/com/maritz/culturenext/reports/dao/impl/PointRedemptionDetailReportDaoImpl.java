package com.maritz.culturenext.reports.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.ReportingDaoImpl;
import com.maritz.culturenext.reports.dao.PointRedemptionDetailReportDao;

@Repository
public class PointRedemptionDetailReportDaoImpl extends ReportingDaoImpl implements PointRedemptionDetailReportDao {
    
    private static final String START_DATE_PLACEHOLDER = "START_DATE";
    private static final String END_DATE_PLACEHOLDER = "END_DATE";
    private static final String SUB_CLIENT_NUMBER_LIST_PLACEHOLDER = "SUB_CLIENT_NUMBER_LIST";
    private static final String PRODUCT_CODE_LIST_PLACEHOLDER = "PRODUCT_CODE_LIST";
    
    private static final String REDEMPTION_QUERY =
            "SELECT DISTINCT " + 
                "PAX_ID, " + 
                "PROJECT_NUMBER, " + 
                "SUBPROJECT_NBR, " + 
                "TRANS_ID, " + 
                "TRANS_DATE, " + 
                "CONFIRMATION_DATE, " + 
                "PARTICIPANT_NBR AS CONTROL_NUM, " + 
                "FIRST_NAME, " + 
                "MIDDLE_NAME, " + 
                "LAST_NAME, " + 
                "ABS(POINTS) AS AMOUNT, " + 
                "CLIENT_NUMBER, " + 
                "PRODUCT_NBR, " + 
                "EMAIL_ADDRESS AS RECIPIENT_EMAIL, " + 
                "CITY AS RECIPIENT_CITY, " + 
                "STATE AS RECIPIENT_STATE, " + 
                "ZIP AS RECIPIENT_POSTAL_CODE, " + 
                "TITLE AS RECIPIENT_TITLE, " + 
                "COMPANY_NAME AS RECIPIENT_COMPANY_NAME, " + 
                "DEPARTMENT AS RECIPIENT_DEPARTMENT, " + 
                "COST_CENTER AS RECIPIENT_COST_CENTER, " + 
                "PAX_FUNCTION AS RECIPIENT_FUNCTION, " + 
                "GRADE AS RECIPIENT_GRADE, " + 
                "AREA AS RECIPIENT_AREA, " + 
                "CUSTOM_1 AS RECIPIENT_CUSTOM_1, " + 
                "CUSTOM_2 AS RECIPIENT_CUSTOM_2, " + 
                "CUSTOM_3 AS RECIPIENT_CUSTOM_3, " + 
                "CUSTOM_4 AS RECIPIENT_CUSTOM_4, " + 
                "CUSTOM_5 AS RECIPIENT_CUSTOM_5 " + 
            "FROM UDM7.CX_RPT_Redemption_Report " + 
            "WHERE " + 
                " (:" + START_DATE_PLACEHOLDER + " IS NULL OR TRANS_DATE > :" + START_DATE_PLACEHOLDER + ") " + 
                "AND (:" + END_DATE_PLACEHOLDER + " IS NULL OR TRANS_DATE < :" + END_DATE_PLACEHOLDER + ") " + 
                "AND CLIENT_NUMBER IN ( :" + SUB_CLIENT_NUMBER_LIST_PLACEHOLDER + " ) " + 
                "AND PRODUCT_NBR IN ( :" + PRODUCT_CODE_LIST_PLACEHOLDER + " ) " + 
            "ORDER BY TRANS_DATE, LAST_NAME, FIRST_NAME";

    /**
     * Retrieve the redemption data  for the specified date-range and ABS product-codes and subclient-numbers 
     * @param productCodes - List of ABS product-codes
     * @param subClientNumbers - List of ABS subclient-numbers
     * @param startDate - retrieve data after this date
     * @param endDate - if this is provided, will only retrieve data before this date 
     * @return
     */
    @Override
    public List<Map<String, Object>> getRedemptionData(List<String> productCodes, List<String> subClientNumbers, Date startDate, Date endDate) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PRODUCT_CODE_LIST_PLACEHOLDER, productCodes);
        params.addValue(SUB_CLIENT_NUMBER_LIST_PLACEHOLDER, subClientNumbers);
        params.addValue(START_DATE_PLACEHOLDER, startDate);
        params.addValue(END_DATE_PLACEHOLDER, endDate);
        
        return namedParameterJdbcTemplate.query(REDEMPTION_QUERY, params, new CamelCaseMapRowMapper());
    }

}