package com.maritz.culturenext.reports.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.dao.PointsReportDao;
import com.maritz.culturenext.reports.dto.PointsReportContentDTO;
import com.maritz.culturenext.reports.util.ReportTypes;

@Repository
public class PointsReportDaoImpl extends AbstractDaoImpl implements PointsReportDao {

    private final String GROUP_ID_PLACEHOLDER = "groupId";
    private final String FROM_DATE_PLACEHOLDER = "fromDate";
    private final String TO_DATE_PLACEHOLDER = "toDate";

    private final String GROUP_MEMBER_GIVERS_FILTER_JOIN_CLAUSE =
            " INNER JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GR_MEM "
            + "ON GR_MEM.pax_id =PSUB.PAX_ID AND GR_MEM.group_id = :"+GROUP_ID_PLACEHOLDER;

    private final String GROUP_MEMBER_RECEIVERS_FILTER_JOIN_CLAUSE =
            " INNER JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GR_MEM "
            + "ON GR_MEM.pax_id =PREC.PAX_ID AND GR_MEM.group_id = :"+GROUP_ID_PLACEHOLDER;

    private final String JOIN_CLAUSE_PLACEHOLDER = "{JOIN_CLAUSE_PLACEHOLDER}";

    private final String POINTS_REPORT_QUERY =
            "SELECT "
            //Issuance date for  recognitions is the PAYOUT_DATE  from PAYOUT.
            //Otherwise it is taken from NOMIMATION SUBMITTAL DATEe
            + "CASE WHEN payout.PAYOUT_DATE IS NULL THEN NOM.SUBMITTAL_DATE "
            + "ELSE payout.PAYOUT_DATE "
            + "END AS ISSUANCE_DATE,"
            //RECIPIENT FIELDS
            + "PREC.PAX_ID AS RECIPIENT_PAX_ID,"
            + "PREC.FIRST_NAME AS RECIPIENT_FIRST_NAME,"
            + "PREC.LAST_NAME AS RECIPIENT_LAST_NAME,"
            + "EMREC.EMAIL_ADDRESS AS RECIPIENT_EMAIL,"
            + "ADREC.CITY AS RECIPIENT_CITY,"
            + "ADREC.STATE AS RECIPIENT_STATE,"
            + "ADREC.COUNTRY_CODE AS RECIPIENT_COUNTRY_CODE,"
            + "ADREC.ZIP AS RECIPIENT_POSTAL_CODE,"
            + "PM_REC_AREA.MISC_DATA AS RECIPIENT_AREA,"
            + "PM_REC_CIA.MISC_DATA AS RECIPIENT_COMPANY_NAME,"
            + "PM_REC_COST.MISC_DATA AS RECIPIENT_COST_CENTER,"
            + "PM_REC_C1.MISC_DATA AS RECIPIENT_CUSTOM1,"
            + "PM_REC_C2.MISC_DATA AS RECIPIENT_CUSTOM2,"
            + "PM_REC_C3.MISC_DATA AS RECIPIENT_CUSTOM3,"
            + "PM_REC_C4.MISC_DATA AS RECIPIENT_CUSTOM4,"
            + "PM_REC_C5.MISC_DATA AS RECIPIENT_CUSTOM5,"
            + "PM_REC_DEPT.MISC_DATA AS RECIPIENT_DEPT,"
            + "PM_REC_FX.MISC_DATA AS RECIPIENT_FUNCTION,"
            + "PM_REC_GRADE.MISC_DATA AS RECIPIENT_GRADE,"
            + "PM_REC_TITLE.MISC_DATA AS RECIPIENT_TITLE,"
            //SUBMITTER FIELDS
            + "ISNULL(PSUB.PAX_ID,0) AS SUBMITTER_PAX_ID,"
            + "ISNULL(PSUB.FIRST_NAME,'Unknown') AS SUBMITTER_FIRST_NAME,"
            + "ISNULL(PSUB.LAST_NAME, 'Unknown') AS SUBMITTER_LAST_NAME,"
            + "EMSUB.EMAIL_ADDRESS AS SUBMITTER_EMAIL,"
            + "ADSUB.CITY AS SUBMITTER_CITY,"
            + "ADSUB.STATE AS SUBMITTER_STATE,"
            + "ADSUB.COUNTRY_CODE AS SUBMITTER_COUNTRY_CODE,"
            + "ADSUB.ZIP AS SUBMITTER_POSTAL_CODE,"
            + "PM_SUB_AREA.MISC_DATA AS SUBMITTER_AREA,"
            + "PM_SUB_CIA.MISC_DATA AS SUBMITTER_COMPANY_NAME,"
            + "PM_SUB_COST.MISC_DATA AS SUBMITTER_COST_CENTER,"
            + "PM_SUB_C1.MISC_DATA AS SUBMITTER_CUSTOM1,"
            + "PM_SUB_C2.MISC_DATA AS SUBMITTER_CUSTOM2,"
            + "PM_SUB_C3.MISC_DATA AS SUBMITTER_CUSTOM3,"
            + "PM_SUB_C4.MISC_DATA AS SUBMITTER_CUSTOM4,"
            + "PM_SUB_C5.MISC_DATA AS SUBMITTER_CUSTOM5,"
            + "PM_SUB_DEPT.MISC_DATA AS SUBMITTER_DEPT,"
            + "PM_SUB_FX.MISC_DATA AS SUBMITTER_FUNCTION,"
            + "PM_SUB_GRADE.MISC_DATA AS SUBMITTER_GRADE,"
            + "PM_SUB_TITLE.MISC_DATA AS SUBMITTER_TITLE,"
            + "BGTD.BUDGET_CODE AS BUDGET_NAME, "
            + "BGTD.ID AS BUDGET_ID, "
            + "ISNULL(payout.PAYOUT_AMOUNT,0) AS POINTS_AWARDED,"
            + "PR.PROGRAM_NAME AS PROGRAM_NAME"
            + " FROM component.PAYOUT payout (NOLOCK ) "
            + " INNER JOIN component.EARNINGS_PAYOUT earnings_payout (NOLOCK ) "
                + "ON payout.PAYOUT_ID = earnings_payout.PAYOUT_ID "
            + " INNER JOIN component.EARNINGS earnings (NOLOCK ) "
                + "ON earnings.EARNINGS_ID = earnings_payout.EARNINGS_ID "
            + " INNER JOIN component.TRANSACTION_HEADER_EARNINGS transaction_header_earnings (NOLOCK ) "
                + "ON earnings.EARNINGS_ID = transaction_header_earnings.EARNINGS_ID "
            + " INNER JOIN component.TRANSACTION_HEADER transaction_header (NOLOCK ) "
                + "ON transaction_header.ID = transaction_header_earnings.TRANSACTION_HEADER_ID "
            + " INNER JOIN component.DISCRETIONARY discretionary (NOLOCK ) "
                + "ON transaction_header.ID = discretionary.TRANSACTION_HEADER_ID "
            + " LEFT JOIN component.PROGRAM PR ON PR.PROGRAM_ID = transaction_header.PROGRAM_ID "
            + " INNER JOIN component.PAX_GROUP PGR ON PGR.PAX_GROUP_ID = transaction_header.pax_group_id"
            + " LEFT JOIN component.RECOGNITION R ON discretionary.TARGET_ID = R.ID AND discretionary.TARGET_TABLE = 'RECOGNITION' "
            + " LEFT JOIN component.NOMINATION NOM ON NOM.ID = R.NOMINATION_ID "
            + " LEFT JOIN component.PAX PREC ON PREC.PAX_ID = PGR.PAX_ID "
            + " LEFT JOIN component.EMAIL EMREC ON PREC.PAX_ID = EMREC.PAX_ID AND EMREC.EMAIL_TYPE_CODE = 'BUSINESS'"
            + " LEFT JOIN component.ADDRESS ADREC ON PREC.PAX_ID = ADREC.PAX_ID AND ADREC.ADDRESS_TYPE_CODE = 'BUSINESS'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_AREA "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_AREA.PAX_GROUP_ID AND PM_REC_AREA.VF_NAME = 'AREA'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_CIA "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_CIA.PAX_GROUP_ID AND PM_REC_CIA.VF_NAME = 'COMPANY_NAME'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_COST"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_COST.PAX_GROUP_ID AND PM_REC_COST.VF_NAME = 'COST_CENTER'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_C1"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_C1.PAX_GROUP_ID AND PM_REC_C1.VF_NAME = 'CUSTOM_1'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_C2 "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_C2.PAX_GROUP_ID AND PM_REC_C2.VF_NAME = 'CUSTOM_2'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_C3"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_C3.PAX_GROUP_ID AND PM_REC_C3.VF_NAME = 'CUSTOM_3'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_C4 "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_C4.PAX_GROUP_ID AND PM_REC_C4.VF_NAME = 'CUSTOM_4'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_C5 "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_C5.PAX_GROUP_ID AND PM_REC_C5.VF_NAME = 'CUSTOM_5'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_DEPT"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_DEPT.PAX_GROUP_ID AND PM_REC_DEPT.VF_NAME = 'DEPARTMENT'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_FX"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_FX.PAX_GROUP_ID AND PM_REC_FX.VF_NAME = 'FUNCTION'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_GRADE "
            + "     ON PGR.PAX_GROUP_ID = PM_REC_GRADE.PAX_GROUP_ID AND PM_REC_GRADE.VF_NAME = 'GRADE'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_REC_TITLE"
            + "     ON PGR.PAX_GROUP_ID = PM_REC_TITLE.PAX_GROUP_ID AND PM_REC_TITLE.VF_NAME = 'JOB_TITLE'"
            + " LEFT JOIN component.PAX PSUB ON NOM.SUBMITTER_PAX_ID = PSUB.PAX_ID"
            + " LEFT JOIN component.EMAIL EMSUB ON PSUB.PAX_ID = EMSUB.PAX_ID AND EMSUB.EMAIL_TYPE_CODE = 'BUSINESS'"
            + " LEFT JOIN component.ADDRESS ADSUB ON PSUB.PAX_ID = ADSUB.PAX_ID AND ADSUB.ADDRESS_TYPE_CODE = 'BUSINESS'"
            + " LEFT JOIN component.PAX_GROUP PGSUB ON PSUB.PAX_ID = PGSUB.PAX_ID"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_AREA "
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_AREA.PAX_GROUP_ID AND PM_SUB_AREA.VF_NAME = 'AREA'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_CIA "
            + "     ON PGR.PAX_GROUP_ID = PM_SUB_CIA.PAX_GROUP_ID AND PM_SUB_CIA.VF_NAME = 'COMPANY_NAME'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_COST"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_COST.PAX_GROUP_ID AND PM_SUB_COST.VF_NAME = 'COST_CENTER'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_C1"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_C1.PAX_GROUP_ID AND PM_SUB_C1.VF_NAME = 'CUSTOM_1'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_C2 "
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_C2.PAX_GROUP_ID AND PM_SUB_C2.VF_NAME = 'CUSTOM_2'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_C3"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_C3.PAX_GROUP_ID AND PM_SUB_C3.VF_NAME = 'CUSTOM_3'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_C4 "
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_C4.PAX_GROUP_ID AND PM_SUB_C4.VF_NAME = 'CUSTOM_4'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_C5 "
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_C5.PAX_GROUP_ID AND PM_SUB_C5.VF_NAME = 'CUSTOM_5'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_DEPT"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_DEPT.PAX_GROUP_ID AND PM_SUB_DEPT.VF_NAME = 'DEPARTMENT'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_FX"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_FX.PAX_GROUP_ID AND PM_SUB_FX.VF_NAME = 'FUNCTION'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_GRADE "
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_GRADE.PAX_GROUP_ID AND PM_SUB_GRADE.VF_NAME = 'GRADE'"
            + " LEFT JOIN component.PAX_GROUP_MISC PM_SUB_TITLE"
            + "     ON PGSUB.PAX_GROUP_ID = PM_SUB_TITLE.PAX_GROUP_ID AND PM_SUB_TITLE.VF_NAME = 'JOB_TITLE'"
            + " LEFT JOIN component.BUDGET BGTD "
                + "ON BGTD.ID = isnull(discretionary.BUDGET_ID_DEBIT, discretionary.BUDGET_ID_CREDIT)"
            + JOIN_CLAUSE_PLACEHOLDER
            + " WHERE  payout.PAYOUT_DATE >=  :" + FROM_DATE_PLACEHOLDER +" "
            + " AND payout.PAYOUT_DATE <= :" + TO_DATE_PLACEHOLDER +" "
            + " AND payout.STATUS_TYPE_CODE = 'ISSUED' "
            + " AND transaction_header.SUB_TYPE_CODE <> 'FUND' "
            + " ORDER BY ISSUANCE_DATE ASC";

    @Override
    public List<PointsReportContentDTO> getPointsReportContent(String queryType, Date startDate, Date endDate, Long groupId) {
        List<PointsReportContentDTO> reportContent = new ArrayList<PointsReportContentDTO>();
        String queryPointsReport = POINTS_REPORT_QUERY;

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(FROM_DATE_PLACEHOLDER, startDate);
        params.addValue(TO_DATE_PLACEHOLDER, endDate);

        //Add groupId filter depending on queryType (group's members are: INCOME = receivers , ISSUANCE = submitters). 
        if (groupId != null) {
            if (queryType.equals(ReportTypes.RECEIVED.toString())) {
                queryPointsReport = 
                        queryPointsReport.replace(JOIN_CLAUSE_PLACEHOLDER, GROUP_MEMBER_RECEIVERS_FILTER_JOIN_CLAUSE);
            } else {
                queryPointsReport = 
                        queryPointsReport.replace(JOIN_CLAUSE_PLACEHOLDER, GROUP_MEMBER_GIVERS_FILTER_JOIN_CLAUSE);
            }

            params.addValue(GROUP_ID_PLACEHOLDER, groupId);
        } else {
            queryPointsReport = queryPointsReport.replace(JOIN_CLAUSE_PLACEHOLDER,"");
        }

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(queryPointsReport, params, new CamelCaseMapRowMapper());

        for (Map<String, Object> node: nodes) {
            reportContent.add(populateDto(node));
        }

        return reportContent;
    }

    /**
     * Create a DTO object from query results.
     * @param node SQL query result
     * @return PointsReportContentDTO object
     */
    private PointsReportContentDTO populateDto(Map<String, Object> node) {
        PointsReportContentDTO reportPointsGiven = new PointsReportContentDTO();

        // Standard fields
        reportPointsGiven.setIssuanceDate((Date) node.get(ProjectConstants.ISSUANCE_DATE));
        reportPointsGiven.setBudgetName((String) node.get(ProjectConstants.BUDGET_NAME));
        reportPointsGiven.setBudgetId((Long) node.get(ProjectConstants.BUDGET_ID));
        reportPointsGiven.setPointsAwarded((Double) node.get(ProjectConstants.POINTS_AWARDED));
        reportPointsGiven.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));

        // Recipient fields
        reportPointsGiven.setRecipientPaxId((Long) node.get(ProjectConstants.RECIPIENT_PAX_ID));
        reportPointsGiven.setRecipientFirstName((String) node.get(ProjectConstants.RECIPIENT_FIRST_NAME));
        reportPointsGiven.setRecipientLastName((String) node.get(ProjectConstants.RECIPIENT_LAST_NAME));
        reportPointsGiven.setRecipientCompanyName((String) node.get(ProjectConstants.RECIPIENT_COMPANY_NAME));
        reportPointsGiven.setRecipientTitle((String) node.get(ProjectConstants.RECIPIENT_TITLE));
        reportPointsGiven.setRecipientEmail((String) node.get(ProjectConstants.RECIPIENT_EMAIL));
        reportPointsGiven.setRecipientCity((String) node.get(ProjectConstants.RECIPIENT_CITY));
        reportPointsGiven.setRecipientState((String) node.get(ProjectConstants.RECIPIENT_STATE));
        reportPointsGiven.setRecipientCountryCode((String) node.get(ProjectConstants.RECIPIENT_COUNTRY_CODE));
        reportPointsGiven.setRecipientDepartment((String) node.get(ProjectConstants.RECIPIENT_DEPARTMENT));
        reportPointsGiven.setRecipientCostCenter((String) node.get(ProjectConstants.RECIPIENT_COST_CENTER));
        reportPointsGiven.setRecipientArea((String) node.get(ProjectConstants.RECIPIENT_AREA));
        reportPointsGiven.setRecipientGrade((String) node.get(ProjectConstants.RECIPIENT_GRADE));
        reportPointsGiven.setRecipientFunction((String) node.get(ProjectConstants.RECIPIENT_FUNCTION));
        reportPointsGiven.setRecipientCustom1((String) node.get(ProjectConstants.RECIPIENT_CUSTOM1));
        reportPointsGiven.setRecipientCustom2((String) node.get(ProjectConstants.RECIPIENT_CUSTOM2));
        reportPointsGiven.setRecipientCustom3((String) node.get(ProjectConstants.RECIPIENT_CUSTOM3));
        reportPointsGiven.setRecipientCustom4((String) node.get(ProjectConstants.RECIPIENT_CUSTOM4));
        reportPointsGiven.setRecipientCustom5((String) node.get(ProjectConstants.RECIPIENT_CUSTOM5));

        // Submitter fields
        reportPointsGiven.setSubmitterPaxId((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
        reportPointsGiven.setSubmitterFirstName((String) node.get(ProjectConstants.SUBMITTER_FIRST_NAME));
        reportPointsGiven.setSubmitterLastName((String) node.get(ProjectConstants.SUBMITTER_LAST_NAME));
        reportPointsGiven.setSubmitterCompanyName((String) node.get(ProjectConstants.SUBMITTER_COMPANY_NAME));
        reportPointsGiven.setSubmitterTitle((String) node.get(ProjectConstants.SUBMITTER_TITLE));
        reportPointsGiven.setSubmitterEmail((String) node.get(ProjectConstants.SUBMITTER_EMAIL));
        reportPointsGiven.setSubmitterCity((String) node.get(ProjectConstants.SUBMITTER_CITY));
        reportPointsGiven.setSubmitterState((String) node.get(ProjectConstants.SUBMITTER_STATE));
        reportPointsGiven.setSubmitterCountryCode((String) node.get(ProjectConstants.SUBMITTER_COUNTRY_CODE));
        reportPointsGiven.setSubmitterDepartment((String) node.get(ProjectConstants.SUBMITTER_DEPARTMENT));
        reportPointsGiven.setSubmitterCostCenter((String) node.get(ProjectConstants.SUBMITTER_COST_CENTER));
        reportPointsGiven.setSubmitterArea((String) node.get(ProjectConstants.SUBMITTER_AREA));
        reportPointsGiven.setSubmitterGrade((String) node.get(ProjectConstants.SUBMITTER_GRADE));
        reportPointsGiven.setSubmitterFunction((String) node.get(ProjectConstants.SUBMITTER_FUNCTION));
        reportPointsGiven.setSubmitterCustom1((String) node.get(ProjectConstants.SUBMITTER_CUSTOM1));
        reportPointsGiven.setSubmitterCustom2((String) node.get(ProjectConstants.SUBMITTER_CUSTOM2));
        reportPointsGiven.setSubmitterCustom3((String) node.get(ProjectConstants.SUBMITTER_CUSTOM3));
        reportPointsGiven.setSubmitterCustom4((String) node.get(ProjectConstants.SUBMITTER_CUSTOM4));
        reportPointsGiven.setSubmitterCustom5((String) node.get(ProjectConstants.SUBMITTER_CUSTOM5));

        return reportPointsGiven;
    }
}
