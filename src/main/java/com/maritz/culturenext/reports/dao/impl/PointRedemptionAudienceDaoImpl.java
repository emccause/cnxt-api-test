package com.maritz.culturenext.reports.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.PointRedemptionAudienceDao;
import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;

@Repository
public class PointRedemptionAudienceDaoImpl extends AbstractDaoImpl implements PointRedemptionAudienceDao {
    private static final String START_DATE_PLACEHOLDER = "START_DATE";
    private static final String END_DATE_PLACEHOLDER = "END_DATE";
    private static final String GROUP_ID_PLACEHOLDER = "GROUP_ID";
    
    private static final String AUDIENCE_QUERY = "EXEC UP_HISTORICAL_AUDIENCE NULL, NULL, NULL, " +
             ":" + GROUP_ID_PLACEHOLDER + ", " +
             ":" + START_DATE_PLACEHOLDER + ", " +
             ":" + END_DATE_PLACEHOLDER;

    /**
     * resolve the audience for the group and date range specified
     * @param groupIdString
     * @param startDate
     * @param endDate
     * @return
     */
    @Override
    public List<Map<String, Object>> getRedemptionAudience(String groupIdString, Date startDate, Date endDate) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(GROUP_ID_PLACEHOLDER, groupIdString);
        params.addValue(START_DATE_PLACEHOLDER, startDate);
        params.addValue(END_DATE_PLACEHOLDER, endDate);
        
        return namedParameterJdbcTemplate.query(
                AUDIENCE_QUERY, params, new CamelCaseMapRowMapper());
    }

}
