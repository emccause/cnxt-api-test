package com.maritz.culturenext.reports.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.reports.dao.PendingEmailsReportDao;

@Repository
public class PendingEmailsReportDaoImpl extends AbstractDaoImpl implements PendingEmailsReportDao {


    private static final String QUERY = "EXEC component.UP_PENDING_EMAILS_REPORT ";

    /**
     * Calls stored proc and returns results.
     *
     * if all of the first four parameters are null, the audience is all pax active in the date range
     * @param commaSeparatedPaxList - this is a list of pax to be individually added to audience
     * @param commaSeparatedTeamList - this is a list of pax to add direct reports to to audience
     * @param commaSeparatedOrgList - this is a list of pax to add entire hierarchy to audience
     * @param commaSeparatedGroupList - this is a list of groups to add members to audience
     * @param fromDateString - if this is provided, will only provide data after this date (default is program start)
     * @param thruDateString - if this is provided, will only provide data before this date (default is current time)
     * @param commaSeparatedRecStatusList - this is a list of recognition statuses to look for.
     *         APPROVED also includes AUTO_APPROVED, and REJECTED also includes REJECTED_SUSPENDED and REJECTED_INACTIVE
     * @param commaSeparatedRecTypeList - will filter to recognitions GIVEN or RECEIVED by the audience (provide both for both)
     * @param commaSeparatedProgramTypeList - if this is provided, will only look at programs of these types
     * @param commaSeparatedProgramIdList - if this is provided, will only look at these individual programs
     *         NOTE: commaSeparatedProgramTypeList filter applies to these programs if provided
     *
     * @return List<Map<String, Object>> data
     */
    @Override
    public List<Map<String, Object>> getPendingEmailsReport() {

        return namedParameterJdbcTemplate.query(QUERY, new ColumnMapRowMapper());
    }

}
