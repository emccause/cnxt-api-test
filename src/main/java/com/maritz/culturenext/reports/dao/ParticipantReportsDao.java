package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ParticipantReportsDao {
    List<Map<String, Object>> getParticipantTransactionReportData(
            Date fromDate, Date thruDate, Long paxId, Long groupId, Long paxTeam, 
            Long paxOrg, String typeList, String statusList, String programTypeList, 
            Long programId, Long batchId);
}
