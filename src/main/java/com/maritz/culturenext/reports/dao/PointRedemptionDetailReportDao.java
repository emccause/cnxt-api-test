package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PointRedemptionDetailReportDao {

    List<Map<String, Object>> getRedemptionData(List<String> productCodes, List<String> subClientNumbers, Date startDate, Date endDate);
}