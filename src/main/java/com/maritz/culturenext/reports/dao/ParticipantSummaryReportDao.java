package com.maritz.culturenext.reports.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ParticipantSummaryReportDao {

    List<Map<String, Object>> getParticipantSummaryReport(
            String commaSeparatedPaxList, String commaSeparatedTeamList,
            String commaSeparatedOrgList, String commaSeparatedGroupList,
            Date fromDate, Date thruDate, String commaSeparatedRecStatusList,
            String commaSeparatedRecTypeList, String commaSeparatedProgramTypeList,
            String commaSeparatedProgramIdList
        );
}
