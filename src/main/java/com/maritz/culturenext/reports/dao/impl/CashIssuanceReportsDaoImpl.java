package com.maritz.culturenext.reports.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.CashIssuanceReportsDao;

@Repository
public class CashIssuanceReportsDaoImpl  extends AbstractDaoImpl implements CashIssuanceReportsDao {
    /*
    Create a batch of the new type
    Find all payout records that are in PROCESSED status with Payout type of CASH
    Update those records to save the BATCH_ID and make their status ISSUED 
    If for some reason the report generation fails, we need to put the payouts back into PROCESSED status
    Download the participant transaction CSV, passing in the batchID as a parameter.
    Save that data into BATCH_FILE (don't actually download a .csv file at this point)
    Update BATCH record to COMPLETE status
    Read the .csv text from BATCH_FILE and return it as an actual .csv
    */
    
    private static final String BATCH_ID_SQL_PARAM = "BATCH_ID_PARAM";
    private static final String FILE_SQL_PARAM = "FILE_PARAM";
    private static final String FILE_NAME_SQL_PARAM = "FILE_NAME_PARAM";
    
    private static final String PROCESS_CASH_PAYOUTS_QUERY = 
            "EXEC UP_PROCESS_CASH_PAYOUTS ";
    
    private static final String SAVE_CASH_ISSUANCE_REPORT_QUERY = 
            "EXEC UP_SAVE_CASH_ISSUANCE_REPORT "
                    + ":" + BATCH_ID_SQL_PARAM + ", "
                    + ":" + FILE_SQL_PARAM + ", "
                    + ":" + FILE_NAME_SQL_PARAM;
    
    @Override
    public List<Map<String, Object>> processCashPayouts() {
        return namedParameterJdbcTemplate.query(PROCESS_CASH_PAYOUTS_QUERY, new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> saveCashIssuanceReport(Long batchId, String file, String fileName) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);
        params.addValue(FILE_SQL_PARAM, file);
        params.addValue(FILE_NAME_SQL_PARAM, fileName);
        
        return namedParameterJdbcTemplate.query(SAVE_CASH_ISSUANCE_REPORT_QUERY, params, new CamelCaseMapRowMapper());
    }
}
