package com.maritz.culturenext.reports.dao;

import java.util.List;
import java.util.Map;

public interface BudgetTransactionReportDao {
    
    List<Map<String, Object>> getBudgetTransactionReportData(String statusList, String budgetIdList);

}
