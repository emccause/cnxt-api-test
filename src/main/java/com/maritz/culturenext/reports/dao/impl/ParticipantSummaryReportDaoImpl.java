package com.maritz.culturenext.reports.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.reports.dao.ParticipantSummaryReportDao;

@Repository
public class ParticipantSummaryReportDaoImpl extends AbstractDaoImpl implements ParticipantSummaryReportDao {
    
    private static final String PAX_ID_LIST_PARAM = "PAX_ID_LIST";
    private static final String TEAM_ID_LIST_PARAM = "TEAM_ID_LIST";
    private static final String ORG_ID_LIST_PARAM = "ORG_ID_LIST";
    private static final String GROUP_ID_LIST_PARAM = "GROUP_ID_LIST";
    private static final String FROM_DATE_PARAM = "FROM_DATE";
    private static final String THRU_DATE_PARAM = "THRU_DATE";
    private static final String REC_STATUS_LIST_PARAM = "REC_STATUS_LIST";
    private static final String REC_TYPE_LIST_PARAM = "REC_TYPE_LIST";
    private static final String PROGRAM_TYPE_LIST_PARAM = "PROGRAM_TYPE_LIST";
    private static final String PROGRAM_ID_LIST_PARAM = "PROGRAM_ID_LIST";
    
    private static final String QUERY = "EXEC component.UP_PARTICIPANT_SUMMARY_REPORT " +
         ":" + PAX_ID_LIST_PARAM + ", " +
         ":" + TEAM_ID_LIST_PARAM + ", " +
         ":" + ORG_ID_LIST_PARAM + ", " +
         ":" + GROUP_ID_LIST_PARAM + ", " +
         ":" + FROM_DATE_PARAM + ", " +
         ":" + THRU_DATE_PARAM + ", " +
         ":" + REC_STATUS_LIST_PARAM + ", " +
         ":" + REC_TYPE_LIST_PARAM + ", " +
         ":" + PROGRAM_TYPE_LIST_PARAM + ", " +
         ":" + PROGRAM_ID_LIST_PARAM;

    /**
     * Calls stored proc and returns results.
     * 
     * if all of the first four parameters are null, the audience is all pax active in the date range 
     * @param commaSeparatedPaxList - this is a list of pax to be individually added to audience
     * @param commaSeparatedTeamList - this is a list of pax to add direct reports to to audience
     * @param commaSeparatedOrgList - this is a list of pax to add entire hierarchy to audience
     * @param commaSeparatedGroupList - this is a list of groups to add members to audience
     * @param fromDateString - if this is provided, will only provide data after this date (default is program start)
     * @param thruDateString - if this is provided, will only provide data before this date (default is current time)
     * @param commaSeparatedRecStatusList - this is a list of recognition statuses to look for.
     *         APPROVED also includes AUTO_APPROVED, and REJECTED also includes REJECTED_SUSPENDED and REJECTED_INACTIVE
     * @param commaSeparatedRecTypeList - will filter to recognitions GIVEN or RECEIVED by the audience (provide both for both)
     * @param commaSeparatedProgramTypeList - if this is provided, will only look at programs of these types
     * @param commaSeparatedProgramIdList - if this is provided, will only look at these individual programs
     *         NOTE: commaSeparatedProgramTypeList filter applies to these programs if provided
     * 
     * @return List<Map<String, Object>> data
     */
    @Override
    public List<Map<String, Object>> getParticipantSummaryReport(
                String commaSeparatedPaxList, String commaSeparatedTeamList, String commaSeparatedOrgList,
                String commaSeparatedGroupList, Date fromDate, Date thruDate, String commaSeparatedRecStatusList,
                String commaSeparatedRecTypeList, String commaSeparatedProgramTypeList, String commaSeparatedProgramIdList
            ) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_LIST_PARAM, commaSeparatedPaxList);
        params.addValue(TEAM_ID_LIST_PARAM, commaSeparatedTeamList);
        params.addValue(ORG_ID_LIST_PARAM, commaSeparatedOrgList);
        params.addValue(GROUP_ID_LIST_PARAM, commaSeparatedGroupList);
        params.addValue(FROM_DATE_PARAM, fromDate);
        params.addValue(THRU_DATE_PARAM, thruDate);
        params.addValue(REC_STATUS_LIST_PARAM, commaSeparatedRecStatusList);
        params.addValue(REC_TYPE_LIST_PARAM, commaSeparatedRecTypeList);
        params.addValue(PROGRAM_TYPE_LIST_PARAM, commaSeparatedProgramTypeList);
        params.addValue(PROGRAM_ID_LIST_PARAM, commaSeparatedProgramIdList);
        
        return namedParameterJdbcTemplate.query(QUERY, params, new ColumnMapRowMapper());
    }

}
