package com.maritz.culturenext.reports.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.BudgetTransactionReportDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class BudgetTransactionReportDaoImpl extends AbstractDaoImpl implements BudgetTransactionReportDao {

    private static final String STATUS_LIST_PLACEHOLDER = "STATUS_LIST";
    private static final String BUDGET_ID_LIST_PLACEHOLDER = "BUDGET_ID_LIST";
    private static final String BUDGET_TRANSACTIONS_REPORT_QUERY = 
            "EXEC UP_BUDGET_TRANSACTION_REPORT "
            + ":" + STATUS_LIST_PLACEHOLDER + ", "
            + ":" + BUDGET_ID_LIST_PLACEHOLDER;
    
    /** 
     * Retrieve the budget transaction details for the given stautus & budgetIds
     * @param statusList  - List of budget status
     * @param budgetIdList - List of budget ids
     * @return budget transaction results in node format
     * 
     */
    @Override
    public List<Map<String, Object>> getBudgetTransactionReportData(String statusList, 
            String budgetIdList) {

        String sql = BUDGET_TRANSACTIONS_REPORT_QUERY;
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(BUDGET_ID_LIST_PLACEHOLDER, budgetIdList);
        
        return namedParameterJdbcTemplate.query(sql, params, new CamelCaseMapRowMapper());
    }
}
