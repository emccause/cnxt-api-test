package com.maritz.culturenext.reports.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.apache.commons.lang3.text.WordUtils;

import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

/**
 * Util class for handling Enrollment Field data to use it into CSV reports.
 */
public class EnrollmentFieldReportUtil {

    private List<EnrollmentFieldDTO> enrollmentFields;

    private Map<String, String> enrollmentMap;

    public EnrollmentFieldReportUtil(List<EnrollmentFieldDTO> enrollmentFields) {
        this. enrollmentFields = enrollmentFields;

        // Create a map for facilitating access to displayName property by name.
        enrollmentMap = new HashMap<>();
        for (EnrollmentFieldDTO field : enrollmentFields) {
            enrollmentMap.put(field.getName(), field.getDisplayName());
        }
    }

    /**
     * Get Enrollment Field's names as list
     * @return List of field name property.
     */
    public List<String> getEnrollmentFieldNameAsList() {
        return (List<String>) CollectionUtils.<EnrollmentFieldDTO, String>collect(enrollmentFields, TransformerUtils.invokerTransformer("getName"));
    }

    /**
     * Get Enrollment Field's display names as list
     * @return List of display name property.
     */
    public List<String> getEnrollmentFieldNameDisplayAsList() {
        //return (List<String>) CollectionUtils.collect(enrollmentFields, TransformerUtils.invokerTransformer("getDisplayName"));
        return (List<String>) CollectionUtils.<EnrollmentFieldDTO, String>collect(enrollmentFields, TransformerUtils.invokerTransformer("getDisplayName"));
    }

    /**
     * Get display name property by field name
     * @return Display name.
     */
    public String getEnrollmentFieldDisplayName(String fieldName) {
        return enrollmentMap.get(fieldName);
    }

    /**
     * Transform field name into field mapping
     *
     * @param fieldName (e.g. COST_CENTER)
     * @return field mapping (e.g. CostCenter)
     */
    public String getEnrollmentFieldMapping(String fieldName) {
        return new String(WordUtils.capitalizeFully(fieldName.replace('_', ' '))).replace(" ", "");
    }


    /**
     * Add CSV column's properties for Enrollment Fields
     * @param csvColumnsProps List to populate with CSV column's properties
     * @param prefixHeader Prefix for header's name (e.g.  for Area, prefixHeader= Recipient's ;
     *             result = Recipient's Area)
     * @param prefixField Prefix for field mappings (e.g.  for Area, prefixField= recipient; result = recipientArea)
     * @param requestFields List csvColumnsProps updated with Enrollment field's CSV properties.
     */
    public void addCSVOptionalColumnsProps(List<CsvColumnPropertiesDTO> csvColumnsProps, String prefixHeader,
            String prefixField, List<String> requestFields) {

        CsvColumnPropertiesDTO csvColumn;
        String headerField;
        String enrollFieldMapping;

        if (requestFields != null && (!requestFields.get(ReportConstants.FIRST_RECORD).trim().isEmpty())){
            for (String recipientField : requestFields) {
                headerField = prefixHeader+getEnrollmentFieldDisplayName(recipientField);
                enrollFieldMapping = prefixField+getEnrollmentFieldMapping(recipientField);
                csvColumn = new CsvColumnPropertiesDTO(headerField, enrollFieldMapping, null);
                csvColumnsProps.add(csvColumn);
            }
        }
    }

}
