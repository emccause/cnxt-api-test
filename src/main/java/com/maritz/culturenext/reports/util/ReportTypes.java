package com.maritz.culturenext.reports.util;

public enum ReportTypes {
    RECEIVED("Award_Income_Detail"),
    GIVEN("Award_Issuance_Detail");
    
    private String fileName;
    
    private ReportTypes(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
