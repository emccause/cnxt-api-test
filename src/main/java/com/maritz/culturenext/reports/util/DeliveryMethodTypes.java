package com.maritz.culturenext.reports.util;

public enum DeliveryMethodTypes {
    DOWNLOAD,
    EMAIL;
}
