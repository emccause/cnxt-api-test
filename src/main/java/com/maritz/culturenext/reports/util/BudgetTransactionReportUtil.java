package com.maritz.culturenext.reports.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.BudgetTransactionReportConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class BudgetTransactionReportUtil {
    public static Map<String, Object[]> createBudgetTransactionCSVProperties() {
        List<String> headersList = new ArrayList<>();
        List<String> fieldMappingList = new ArrayList<>();
        List<CellProcessor> processorList = new ArrayList<>();

        for (CsvColumnPropertiesDTO columnProps : createCSVColumnProperties()) {
            headersList.add(columnProps.getHeader());
            fieldMappingList.add(columnProps.getField());
            processorList.add(columnProps.getProcessor());
        }

        Map<String, Object []> budgetTransactionCSVMap = new HashMap<String, Object []>();

        // Column's headers
        String[] headers = headersList.toArray(new String[headersList.size()]);
        budgetTransactionCSVMap.put(ProjectConstants.HEADERS, headers);

        // Column's dto field mapping
        String[] fieldMapping = fieldMappingList.toArray(new String[fieldMappingList.size()]);
        budgetTransactionCSVMap.put(ProjectConstants.FIELD_MAPPING, fieldMapping);

        // Column's processor
        CellProcessor[] processor = processorList.toArray(new CellProcessor[processorList.size()]);
        budgetTransactionCSVMap.put(ProjectConstants.PROCESSORS, processor);

        return budgetTransactionCSVMap;
    }

    private static List<CsvColumnPropertiesDTO> createCSVColumnProperties() {
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<>();

        csvColumnsProps.add(new CsvColumnPropertiesDTO(BudgetTransactionReportConstants.NOMINATION_ID_HEADER, ProjectConstants.NOMINATION_ID, null));

        return csvColumnsProps;
    }
}
