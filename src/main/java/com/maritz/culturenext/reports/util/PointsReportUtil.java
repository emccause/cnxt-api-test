package com.maritz.culturenext.reports.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.constants.PointsReportConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.dto.PointsReportRequestDTO;

public class PointsReportUtil {
    
    /**
     * Create map for CSV report's arrays properties (headers description, 
     * fieldMapping dto object, column's processors),
     * @param request Report's request DTO
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @return Map for CSV properties; keys: headers, fieldMapping, processors
     */
    public static     Map<String, Object []> createCSVReportProperties(PointsReportRequestDTO request,
            EnrollmentFieldReportUtil enrollmentFieldReportUtil ){
        List<String> headersArray = new ArrayList<String>();
        List<String> fieldMappingArray = new ArrayList<String>();
        List<CellProcessor> processorArray = new ArrayList<CellProcessor>();

        List<CsvColumnPropertiesDTO> csvColumnsProps = createCSVColumnsProperties(request, enrollmentFieldReportUtil);

        for (CsvColumnPropertiesDTO columnProps : csvColumnsProps) {
            headersArray.add(columnProps.getHeader());
            fieldMappingArray.add(columnProps.getField());
            processorArray.add(columnProps.getProcessor());
        }
        
        Map<String, Object []> csvReportPropsMap = new HashMap<String, Object []>();
        
        // Column's headers
        String[] headers = new String[headersArray.size()];
        headers = (String[]) headersArray.toArray(headers);
        csvReportPropsMap.put(ProjectConstants.HEADERS, headers);
        
        // Column's dto field mapping
        String[] fieldMapping = new String[fieldMappingArray.size()];
        fieldMapping = (String[]) fieldMappingArray.toArray(fieldMapping);
        csvReportPropsMap.put(ProjectConstants.FIELD_MAPPING, fieldMapping);
        
        // Column's processor
        CellProcessor[] processor = new CellProcessor[processorArray.size()];
        processor = (CellProcessor[]) processorArray.toArray(processor);
        csvReportPropsMap.put(ProjectConstants.PROCESSORS, processor);
        
        return csvReportPropsMap;
    }

    /**
     * Create CSV headers, fieldMapping and processor according to standard, request recipient and submitter fields.
     * @param request Request DTO
     * @param enrollmentFieldReportUtil Util class to handle Enrollment Field CSV columns.
     * @return List of CSV coumns properties.
     */
    private static List<CsvColumnPropertiesDTO> createCSVColumnsProperties(PointsReportRequestDTO request, 
            EnrollmentFieldReportUtil enrollmentFieldReportUtil) {
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        // Standard fields

        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.ISSUANCE_DATE_HEADER,
                ProjectConstants.ISSUANCE_DATE, new FmtDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT)));

        // Recipients header
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.REC_PAX_ID_HEADER,
                ProjectConstants.RECIPIENT_PAX_ID, new ParseLong()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.REC_FIRST_NAME_HEADER,
                ProjectConstants.RECIPIENT_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.REC_LAST_NAME_HEADER,
                ProjectConstants.RECIPIENT_LAST_NAME, null));

        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                PointsReportConstants.REC_PREFIX_ENROLL_HEADER,
                ProjectConstants.RECIPIENT, request.getRecipientFields());

        // Submitter fields
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.SUB_PAX_ID_HEADER,
                ProjectConstants.SUBMITTER_PAX_ID,  new ParseLong()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.SUB_FIRST_NAME_HEADER,
                ProjectConstants.SUBMITTER_FIRST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.SUB_LAST_NAME_HEADER,
                ProjectConstants.SUBMITTER_LAST_NAME, null));

        enrollmentFieldReportUtil.addCSVOptionalColumnsProps(csvColumnsProps,
                PointsReportConstants.SUB_PREFIX_ENROLL_HEADER,
                ProjectConstants.SUBMITTER, request.getSubmitterFields());

        // standard fields
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.BUDGET_NAME_HEADER,
                ProjectConstants.BUDGET_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.BUDGET_ID_HEADER,
                ProjectConstants.BUDGET_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.POINTS_AWARDED_HEADER,
                ProjectConstants.POINTS_AWARDED, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(PointsReportConstants.PROGRAM_NAME_HEADER,
                ProjectConstants.PROGRAM_NAME, null));
        
        return csvColumnsProps;
        
    }

}
