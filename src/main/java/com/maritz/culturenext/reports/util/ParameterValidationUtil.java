package com.maritz.culturenext.reports.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.ReportConstants;

@Component
public class ParameterValidationUtil {

    public void validateListContainsDigits(List<ErrorMessage> errors, String commaSeparatedList, String field) {
        if (commaSeparatedList != null
                && !commaSeparatedList.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            errors.add(new ErrorMessage()
                .setField(field)
                .setCode(ReportConstants.PARAM_ONLY_SUPPORTS_DIGITS_ERROR)
                .setMessage(ReportConstants.PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE)
            );
        }
    }
    
    public void validateListContainsLetters(List<ErrorMessage> errors, String commaSeparatedList, String field) {
        if (commaSeparatedList != null
                && !commaSeparatedList.matches(ProjectConstants.REGEX_DELIM_COMMA_LETTERS)) {
            errors.add(new ErrorMessage()
                .setField(field)
                .setCode(ReportConstants.PARAM_ONLY_SUPPORTS_LETTERS_ERROR)
                .setMessage(ReportConstants.PARAM_ONLY_SUPPORTS_LETTERS_ERROR_MESSAGE)
            );
        }
    }
    
    /**
     * Validates the optional custom fields that can be called for a report
     * Returns a list of errors that will be handled by the calling method
     * @param optionalFields - comma separated list of optional fields to include on the report
     *         Valid fields are EMAIL, CITY, STATE, POSTAL_CODE, COUNTRY_CODE, 
     *         TITLE, COMPANY_NAME, DEPARTMENT, COST_CENTER, FUNCTION,
     *         GRADE, AREA, CUSTOM_1, CUSTOM_2, CUSTOM_3, CUSTOM_4, CUSTOM_5
     * @param code - value to set for ErrorMessage.code
     * @param field - value to set for ErrorMessage.field
     * @param message - value to set for ErrorMessage.message
     * @return ArrayList<ErrorMessage> - list of error messages
     */
    public ArrayList<ErrorMessage> validateOptionalFields(
            String optionalFields, String code, String field, String message) {
        
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        
        if (optionalFields != null && optionalFields.length() != 0) {
            List<String> fieldList = Arrays.asList(optionalFields.split(ProjectConstants.COMMA_DELIM));

            //make sure all optional fields are valid
            for (String item : fieldList) {
                if (!ReportConstants.VALID_CUSTOM_FIELDS.contains(item)) {
                    //invalid custom field
                    errors.add(new ErrorMessage()
                    .setCode(code)
                    .setField(field)
                    .setMessage(String.format(message, item)));
                    break;
                }
            }
        }
        
        return errors;
    }

}