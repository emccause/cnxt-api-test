package com.maritz.culturenext.reports.dto;

import java.util.List;

import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;

public class ReportsQueryRequestDto {
    
    String fromDate;
    String thruDate;
    List<GroupMemberStubDTO> members;
    List<Long> directReports;
    List<Long> orgReports;

    public String getFromDate() {
        return fromDate;
    }

    public ReportsQueryRequestDto setFromDate(String fromDate) {
        this.fromDate = fromDate;
        return this;
    }

    public String getThruDate() {
        return thruDate;
    }

    public ReportsQueryRequestDto setThruDate(String thruDate) {
        this.thruDate = thruDate;
        return this;
    }

    public List<GroupMemberStubDTO> getMembers() {
        return members;
    }

    public ReportsQueryRequestDto setMembers(List<GroupMemberStubDTO> members) {
        this.members = members;
        return this;
    }

    public List<Long> getDirectReports() {
        return directReports;
    }

    public ReportsQueryRequestDto setDirectReports(List<Long> directReports) {
        this.directReports = directReports;
        return this;
    }

    public List<Long> getOrgReports() {
        return orgReports;
    }

    public ReportsQueryRequestDto setOrgReports(List<Long> orgReports) {
        this.orgReports = orgReports;
        return this;
    }

}
