package com.maritz.culturenext.reports.dto;

public class PointsReportCacheObjectDTO {
    private PointsReportRequestDTO reportRequest;
    
    public PointsReportCacheObjectDTO() {
        super();
    }
    
    public PointsReportCacheObjectDTO(PointsReportRequestDTO reportRequest) {
        this.reportRequest = reportRequest;
    }

    public PointsReportRequestDTO getReportRequest() {
        return reportRequest;
    }
    public void setReportRequest(PointsReportRequestDTO reportRequest) {
        this.reportRequest = reportRequest;
    }

}
