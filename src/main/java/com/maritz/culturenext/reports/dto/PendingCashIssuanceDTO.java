package com.maritz.culturenext.reports.dto;

public class PendingCashIssuanceDTO {
    
    private Integer totalRecords;
    private Double awardAmount;
    
    public Integer getTotalRecords() {
        return totalRecords;
    }
    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }
}
