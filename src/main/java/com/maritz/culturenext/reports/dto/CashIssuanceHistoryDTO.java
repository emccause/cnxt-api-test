package com.maritz.culturenext.reports.dto;

public class CashIssuanceHistoryDTO {

    private String reportCreateDate;
    private Long batchId;
    private Integer totalRecords;
    private Double awardAmount;
    private String reportType;
    private String status;
    
    public String getReportCreateDate() {
        return reportCreateDate;
    }
    public void setReportCreateDate(String reportCreateDate) {
        this.reportCreateDate = reportCreateDate;
    }
    public Long getBatchId() {
        return batchId;
    }
    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }
    public Integer getTotalRecords() {
        return totalRecords;
    }
    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }
    public String getReportType() {
        return reportType;
    }
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
