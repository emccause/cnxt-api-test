package com.maritz.culturenext.reports.dto;

import java.util.List;

public class PointsReportRequestDTO {
    private String type;
    private String deliveryMethod;
    private List<String> recipientFields;
    private List<String> submitterFields;
    private String fromDate;
    private String toDate;
    private Long groupId;
    private String deliveryEmail;
    
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDeliveryMethod() {
        return deliveryMethod;
    }
    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }
    public List<String> getRecipientFields() {
        return recipientFields;
    }
    public void setRecipientFields(List<String> recipientFields) {
        this.recipientFields = recipientFields;
    }
    public List<String> getSubmitterFields() {
        return submitterFields;
    }
    public void setSubmitterFields(List<String> submitterFields) {
        this.submitterFields = submitterFields;
    }
    public String getFromDate() {
        return fromDate;
    }
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }
    public String getToDate() {
        return toDate;
    }
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
    public Long getGroupId() {
        return groupId;
    }
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    public String getDeliveryEmail() {
        return deliveryEmail;
    }
    public void setDeliveryEmail(String deliveryEmail) {
        this.deliveryEmail = deliveryEmail;
    }

}
