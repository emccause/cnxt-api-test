package com.maritz.culturenext.reports.dto;

import java.util.Date;

public class PointsReportContentDTO {
    private Date issuanceDate;
    private Long recipientPaxId;
    private String recipientFirstName;
    private String recipientLastName;
    private String recipientCompanyName;
    private String recipientTitle;
    private String recipientEmail;
    private String recipientCity;
    private String recipientState;
    private String recipientCountryCode;
    private String recipientPostalCode;
    private String recipientDepartment;
    private String recipientCostCenter;
    private String recipientArea;
    private String recipientGrade;
    private String recipientFunction;
    private String recipientCustom1;
    private String recipientCustom2;
    private String recipientCustom3;
    private String recipientCustom4;
    private String recipientCustom5;
    private Long submitterPaxId;
    private String submitterFirstName;
    private String submitterLastName;
    private String submitterCompanyName;
    private String submitterTitle;
    private String submitterEmail;
    private String submitterCity;
    private String submitterState;
    private String submitterCountryCode;
    private String submitterPostalCode;
    private String submitterDepartment;
    private String submitterCostCenter;
    private String submitterArea;
    private String submitterGrade;
    private String submitterFunction;
    private String submitterCustom1;
    private String submitterCustom2;
    private String submitterCustom3;
    private String submitterCustom4;
    private String submitterCustom5;
    private String budgetName;
    private Long budgetId;
    private Double pointsAwarded;
    private String programName;
    
    public Date getIssuanceDate() {
        return issuanceDate;
    }
    public void setIssuanceDate(Date issuanceDate) {
        this.issuanceDate = issuanceDate;
    }

    public String getBudgetName() {
        return budgetName;
    }
    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }
    public Long getBudgetId() {
        return budgetId;
    }
    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
    public Double getPointsAwarded() {
        return pointsAwarded;
    }
    public void setPointsAwarded(Double pointsAwarded) {
        this.pointsAwarded = pointsAwarded;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public Long getRecipientPaxId() {
        return recipientPaxId;
    }
    public void setRecipientPaxId(Long recipientPaxId) {
        this.recipientPaxId = recipientPaxId;
    }
    public String getRecipientFirstName() {
        return recipientFirstName;
    }
    public void setRecipientFirstName(String recipientFirstName) {
        this.recipientFirstName = recipientFirstName;
    }
    public String getRecipientLastName() {
        return recipientLastName;
    }
    public void setRecipientLastName(String recipientLastName) {
        this.recipientLastName = recipientLastName;
    }
    public String getRecipientCompanyName() {
        return recipientCompanyName;
    }
    public void setRecipientCompanyName(String recipientCompanyName) {
        this.recipientCompanyName = recipientCompanyName;
    }
    public String getRecipientTitle() {
        return recipientTitle;
    }
    public void setRecipientTitle(String recipientTitle) {
        this.recipientTitle = recipientTitle;
    }
    public String getRecipientEmail() {
        return recipientEmail;
    }
    public void setRecipientEmail(String recipientEmail) {
        this.recipientEmail = recipientEmail;
    }
    public String getRecipientCity() {
        return recipientCity;
    }
    public void setRecipientCity(String recipientCity) {
        this.recipientCity = recipientCity;
    }
    public String getRecipientState() {
        return recipientState;
    }
    public void setRecipientState(String recipientState) {
        this.recipientState = recipientState;
    }
    public String getRecipientCountryCode() {
        return recipientCountryCode;
    }
    public void setRecipientCountryCode(String recipientCountryCode) {
        this.recipientCountryCode = recipientCountryCode;
    }
    public String getRecipientDepartment() {
        return recipientDepartment;
    }
    public void setRecipientDepartment(String recipientDepartment) {
        this.recipientDepartment = recipientDepartment;
    }
    public String getRecipientCostCenter() {
        return recipientCostCenter;
    }
    public void setRecipientCostCenter(String recipientCostCenter) {
        this.recipientCostCenter = recipientCostCenter;
    }
    public String getRecipientArea() {
        return recipientArea;
    }
    public void setRecipientArea(String recipientArea) {
        this.recipientArea = recipientArea;
    }
    public String getRecipientGrade() {
        return recipientGrade;
    }
    public void setRecipientGrade(String recipientGrade) {
        this.recipientGrade = recipientGrade;
    }
    public String getRecipientFunction() {
        return recipientFunction;
    }
    public void setRecipientFunction(String recipientFunction) {
        this.recipientFunction = recipientFunction;
    }
    public String getRecipientCustom1() {
        return recipientCustom1;
    }
    public void setRecipientCustom1(String recipientCustom1) {
        this.recipientCustom1 = recipientCustom1;
    }
    public String getRecipientCustom2() {
        return recipientCustom2;
    }
    public void setRecipientCustom2(String recipientCustom2) {
        this.recipientCustom2 = recipientCustom2;
    }
    public String getRecipientCustom3() {
        return recipientCustom3;
    }
    public void setRecipientCustom3(String recipientCustom3) {
        this.recipientCustom3 = recipientCustom3;
    }
    public String getRecipientCustom4() {
        return recipientCustom4;
    }
    public void setRecipientCustom4(String recipientCustom4) {
        this.recipientCustom4 = recipientCustom4;
    }
    public String getRecipientCustom5() {
        return recipientCustom5;
    }
    public void setRecipientCustom5(String recipientCustom5) {
        this.recipientCustom5 = recipientCustom5;
    }
    public Long getSubmitterPaxId() {
        return submitterPaxId;
    }
    public void setSubmitterPaxId(Long submitterPaxId) {
        this.submitterPaxId = submitterPaxId;
    }
    public String getSubmitterFirstName() {
        return submitterFirstName;
    }
    public void setSubmitterFirstName(String submitterFirstName) {
        this.submitterFirstName = submitterFirstName;
    }
    public String getSubmitterLastName() {
        return submitterLastName;
    }
    public void setSubmitterLastName(String submitterLastName) {
        this.submitterLastName = submitterLastName;
    }
    public String getSubmitterCompanyName() {
        return submitterCompanyName;
    }
    public void setSubmitterCompanyName(String submitterCompanyName) {
        this.submitterCompanyName = submitterCompanyName;
    }
    public String getSubmitterTitle() {
        return submitterTitle;
    }
    public void setSubmitterTitle(String submitterTitle) {
        this.submitterTitle = submitterTitle;
    }
    public String getSubmitterEmail() {
        return submitterEmail;
    }
    public void setSubmitterEmail(String submitterEmail) {
        this.submitterEmail = submitterEmail;
    }
    public String getSubmitterCity() {
        return submitterCity;
    }
    public void setSubmitterCity(String submitterCity) {
        this.submitterCity = submitterCity;
    }
    public String getSubmitterState() {
        return submitterState;
    }
    public void setSubmitterState(String submitterState) {
        this.submitterState = submitterState;
    }
    public String getSubmitterCountryCode() {
        return submitterCountryCode;
    }
    public void setSubmitterCountryCode(String submitterCountryCode) {
        this.submitterCountryCode = submitterCountryCode;
    }
    public String getSubmitterDepartment() {
        return submitterDepartment;
    }
    public void setSubmitterDepartment(String submitterDepartment) {
        this.submitterDepartment = submitterDepartment;
    }
    public String getSubmitterCostCenter() {
        return submitterCostCenter;
    }
    public void setSubmitterCostCenter(String submitterCostCenter) {
        this.submitterCostCenter = submitterCostCenter;
    }
    public String getSubmitterArea() {
        return submitterArea;
    }
    public void setSubmitterArea(String submitterArea) {
        this.submitterArea = submitterArea;
    }
    public String getSubmitterGrade() {
        return submitterGrade;
    }
    public void setSubmitterGrade(String submitterGrade) {
        this.submitterGrade = submitterGrade;
    }
    public String getSubmitterFunction() {
        return submitterFunction;
    }
    public void setSubmitterFunction(String submitterFunction) {
        this.submitterFunction = submitterFunction;
    }
    public String getSubmitterCustom1() {
        return submitterCustom1;
    }
    public void setSubmitterCustom1(String submitterCustom1) {
        this.submitterCustom1 = submitterCustom1;
    }
    public String getSubmitterCustom2() {
        return submitterCustom2;
    }
    public void setSubmitterCustom2(String submitterCustom2) {
        this.submitterCustom2 = submitterCustom2;
    }
    public String getSubmitterCustom3() {
        return submitterCustom3;
    }
    public void setSubmitterCustom3(String submitterCustom3) {
        this.submitterCustom3 = submitterCustom3;
    }
    public String getSubmitterCustom4() {
        return submitterCustom4;
    }
    public void setSubmitterCustom4(String submitterCustom4) {
        this.submitterCustom4 = submitterCustom4;
    }
    public String getSubmitterCustom5() {
        return submitterCustom5;
    }
    public void setSubmitterCustom5(String submitterCustom5) {
        this.submitterCustom5 = submitterCustom5;
    }
    public String getRecipientPostalCode() {
        return recipientPostalCode;
    }
    public void setRecipientPostalCode(String recipientPostalCode) {
        this.recipientPostalCode = recipientPostalCode;
    }
    public String getSubmitterPostalCode() {
        return submitterPostalCode;
    }
    public void setSubmitterPostalCode(String submitterPostalCode) {
        this.submitterPostalCode = submitterPostalCode;
    }
}
