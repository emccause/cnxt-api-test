package com.maritz.culturenext.reports.dto;

import org.supercsv.cellprocessor.ift.CellProcessor;

public class CsvColumnPropertiesDTO {
    private String header;
    private String field;
    private CellProcessor processor;
    
    public CsvColumnPropertiesDTO(String header, String field, CellProcessor processor) {
        super();
        this.header = header;
        this.field = field;
        this.processor = processor;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public CellProcessor getProcessor() {
        return processor;
    }

    public void setProcessor(CellProcessor processor) {
        this.processor = processor;
    }
    
    
    
}
