package com.maritz.culturenext.reports.dto;

public class QueryIdDto {

    String queryId;

    public String getQueryId() {
        return queryId;
    }

    public QueryIdDto setQueryId(String queryId) {
        this.queryId = queryId;
        return this;
    }
    
}
