package com.maritz.culturenext.reports.caching;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.ReportConstants;

@Component
public class QueryCacheHandler<T> {
    private static final Logger logger = LoggerFactory.getLogger(QueryCacheHandler.class);
    
    @Inject @Named("reportsQueryCache") private Cache cache;
    
    public String createCache(T cacheObject) throws Exception{
        String keyBase = cacheObject.hashCode() + ":" + System.currentTimeMillis();
        String key = Base64.encodeBase64String(keyBase.getBytes("UTF-8"));
        
        cache.put(key, cacheObject);
        
        return key;
    }
    
    @SuppressWarnings("unchecked")
    public T getCacheObject(String cacheId, Class<T> cacheType){
        // Determine if query is missing
        if (cacheId == null) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED)
                    .setField(ProjectConstants.QUERY_ID)
                    .setMessage(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED_MSG));
        }

        T cacheObject = null;
        ValueWrapper wrappedObject = (ValueWrapper) cache.get(cacheId);
        if (wrappedObject != null) {
            try {
                cacheObject = (T) wrappedObject.get();
                
                if (!cacheType.isInstance(cacheObject)) {
                    logger.error("Attempted to get a cache object which it is not an instance of required class.");
                    cacheObject = null;
                }
            } catch (ClassCastException e) {
                logger.error("Attempted to cast the wrappedObject to a class of which it is not an instance.");
            }
        }
        
        if (cacheObject == null) {
            throw new ErrorMessageException().addErrorMessage(ReportConstants.INVALID_QUERY_ID_MESSAGE)
                .setStatus(HttpStatus.NOT_FOUND);
        }

        return cacheObject;
    }
    
}
