package com.maritz.culturenext.reports.caching;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class ReportsQueryCacheObject {

    Long creatorPaxId;
    Date startDate;
    Date endDate;
    List<Long> groupIdList;
    List<Long> paxIdList;
    List<Long> directReportIdList;
    List<Long> orgReportIdList;
    Boolean allMembers;
    
    List<Long> finalPaxIdList;

    List<Long> programIdList;
    List<Long> budgetIdList;

    Map<Long, List<Long>> paxWithGivingLimitBudgetIdMap;
    List<Long> budgetWithGivingLimitIdList;

    public Long getCreatorPaxId() {
        return creatorPaxId;
    }

    public ReportsQueryCacheObject setCreatorPaxId(Long creatorPaxId) {
        this.creatorPaxId = creatorPaxId;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public ReportsQueryCacheObject setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public ReportsQueryCacheObject setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public List<Long> getGroupIdList() {
        return groupIdList;
    }

    public ReportsQueryCacheObject setGroupIdList(List<Long> groupIdList) {
        this.groupIdList = groupIdList;
        return this;
    }

    public List<Long> getPaxIdList() {
        return paxIdList;
    }

    public ReportsQueryCacheObject setPaxIdList(List<Long> paxIdList) {
        this.paxIdList = paxIdList;
        return this;
    }

    public List<Long> getDirectReportIdList() {
        return directReportIdList;
    }

    public ReportsQueryCacheObject setDirectReportIdList(List<Long> directReportIdList) {
        this.directReportIdList = directReportIdList;
        return this;
    }

    public List<Long> getOrgReportIdList() {
        return orgReportIdList;
    }

    public ReportsQueryCacheObject setOrgReportIdList(List<Long> orgReportIdList) {
        this.orgReportIdList = orgReportIdList;
        return this;
    }
    
    public Boolean getAllMembers() {
        return allMembers;
    }

    public ReportsQueryCacheObject setAllMembers(Boolean allMembers) {
        this.allMembers = allMembers;
        return this;
    }

    public List<Long> getFinalPaxIdList() {
        return finalPaxIdList;
    }

    public ReportsQueryCacheObject setFinalPaxIdList(List<Long> finalPaxIdList) {
        this.finalPaxIdList = finalPaxIdList;
        return this;
    }

    public List<Long> getProgramIdList() {
        return programIdList;
    }

    public ReportsQueryCacheObject setProgramIdList(List<Long> programIdList) {
        this.programIdList = programIdList;
        return this;
    }

    public List<Long> getBudgetIdList() {
        return budgetIdList;
    }

    public ReportsQueryCacheObject setBudgetIdList(List<Long> budgetIdList) {
        this.budgetIdList = budgetIdList;
        return this;
    }

    public Map<Long, List<Long>> getPaxWithGivingLimitBudgetIdMap() {
        return paxWithGivingLimitBudgetIdMap;
    }

    public ReportsQueryCacheObject setPaxWithGivingLimitBudgetIdMap(Map<Long, List<Long>> paxWithGivingLimitBudgetIdMap) {
        this.paxWithGivingLimitBudgetIdMap = paxWithGivingLimitBudgetIdMap;
        return this;
    }

    public List<Long> getBudgetWithGivingLimitIdList() {
        return budgetWithGivingLimitIdList;
    }

    public ReportsQueryCacheObject setBudgetWithGivingLimitIdList(List<Long> budgetWithGivingLimitIdList) {
        this.budgetWithGivingLimitIdList = budgetWithGivingLimitIdList;
        return this;
    }
}
