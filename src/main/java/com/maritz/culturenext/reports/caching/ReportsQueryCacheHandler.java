package com.maritz.culturenext.reports.caching;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.codec.binary.Base64;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.ReportsQueryDataLoadDao;

@Component
public class ReportsQueryCacheHandler {
    
    @Inject @Named("reportsQueryCache") private Cache cache;
    @Inject private ReportsQueryDataLoadDao reportsQueryDataLoadDao;
    @Inject private Security security;

    public String createCache(Long submitterPaxId, Date startDate, Date endDate, List<Long> groupIdList,
            List<Long> paxIdList, List<Long> directReportIdList, List<Long> orgReportIdList) throws Exception {
        
        String keyBase = submitterPaxId + ":" + System.currentTimeMillis();
        String key = Base64.encodeBase64String(keyBase.getBytes("UTF-8"));
        
        ReportsQueryCacheObject cacheObject = new ReportsQueryCacheObject();
        cacheObject.setCreatorPaxId(submitterPaxId);
        cacheObject.setStartDate(startDate);
        cacheObject.setEndDate(endDate);
        cacheObject.setGroupIdList(groupIdList);
        cacheObject.setPaxIdList(paxIdList);
        cacheObject.setDirectReportIdList(directReportIdList);
        cacheObject.setOrgReportIdList(orgReportIdList);
        Boolean allMembers = false;
        if(groupIdList.isEmpty() && paxIdList.isEmpty() && directReportIdList.isEmpty() && orgReportIdList.isEmpty()){
            allMembers = true;
        }
        cacheObject.setAllMembers(allMembers);
        
        cacheObject.setFinalPaxIdList(reportsQueryDataLoadDao.getAllPax(startDate, endDate, groupIdList,
                paxIdList, directReportIdList, orgReportIdList, allMembers));
        
        cache.put(key, cacheObject);
        
        return key;
    }

    public ReportsQueryCacheObject getCacheObject(String cacheId) {
        // determine if query is missing
        if (cacheId == null) {
            throw new ErrorMessageException(ReportConstants.MISSING_QUERY_ID_MESSAGE);
        }

        ValueWrapper wrappedObject = (ValueWrapper) cache.get(cacheId);
        
        ReportsQueryCacheObject cacheObject = null;
        if (wrappedObject != null) {
            cacheObject = (ReportsQueryCacheObject) wrappedObject.get();
        }
        
        if (cacheObject == null) {
            throw new ErrorMessageException().addErrorMessage(ReportConstants.INVALID_QUERY_ID_MESSAGE);
        }

        // If logged in pax is not the creator of the cache, throw an error.
        if (!security.getPaxId().equals(cacheObject.getCreatorPaxId())) {
            throw new ErrorMessageException()
            .addErrorMessage(ReportConstants.QUERY_NOT_YOURS_ERROR)
            .setStatus(HttpStatus.FORBIDDEN);
        }
        
        return cacheObject;
    }

    public Date getStartDate(String cacheId) {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        return cacheObject.getStartDate();
    }

    public Date getEndDate(String cacheId) {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        return cacheObject.getEndDate();
    }

    public List<Long> getFinalPaxIdList(String cacheId) throws Exception {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        List<Long> finalPaxIdList = cacheObject.getFinalPaxIdList();
        
        if(finalPaxIdList == null){
            finalPaxIdList = reportsQueryDataLoadDao.getAllPax(cacheObject.getStartDate(), cacheObject.getEndDate(),
                    cacheObject.getGroupIdList(), cacheObject.getPaxIdList(), cacheObject.getDirectReportIdList(),
                    cacheObject.getOrgReportIdList(), cacheObject.getAllMembers());
            cacheObject.setFinalPaxIdList(finalPaxIdList);
        }
        
        return finalPaxIdList;
    }

    public List<Long> getProgramIdList(String cacheId) throws Exception {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        List<Long> programIdList = cacheObject.getProgramIdList();
        
        if(programIdList == null){
            programIdList = reportsQueryDataLoadDao.getAllPrograms(cacheObject.getStartDate(), 
                    cacheObject.getEndDate(), cacheObject.getGroupIdList(), cacheObject.getPaxIdList(),
                    cacheObject.getDirectReportIdList(), cacheObject.getOrgReportIdList(), cacheObject.getAllMembers());
            cacheObject.setProgramIdList(programIdList);
        }

        return programIdList;
    }

    public List<Long> getBudgetIdList(String cacheId) throws Exception {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        List<Long> budgetIdList = cacheObject.getBudgetIdList();
        
        if(budgetIdList == null){
            budgetIdList = new ArrayList<Long>();
            boolean allMembers = cacheObject.getAllMembers();
            
            if(allMembers == true){
                budgetIdList = reportsQueryDataLoadDao.getAllBudgets(cacheObject.getEndDate());
            }
            else{
                Map<Long,List<Long>> budgetMap = reportsQueryDataLoadDao.getPaxToBudgetIdMap(cacheObject.getStartDate(),
                        cacheObject.getEndDate(), cacheObject.getGroupIdList(), cacheObject.getPaxIdList(),
                        cacheObject.getDirectReportIdList(), cacheObject.getOrgReportIdList(), false);
                if(budgetMap != null){
                    budgetIdList = budgetMap.get(0L);
                }
                
            }
            cacheObject.setBudgetIdList(budgetIdList);
        }
        
        return budgetIdList;
    }

    public Map<Long, List<Long>> getPaxWithGivingLimitIdList(String cacheId) throws Exception {
        
        ReportsQueryCacheObject cacheObject = getCacheObject(cacheId);
        
        Map<Long, List<Long>> paxWithGivingLimitIdList = cacheObject.getPaxWithGivingLimitBudgetIdMap();
        
        if(paxWithGivingLimitIdList == null){
            paxWithGivingLimitIdList = reportsQueryDataLoadDao.getPaxToBudgetIdMap(cacheObject.getStartDate(),
                    cacheObject.getEndDate(), cacheObject.getGroupIdList(), cacheObject.getPaxIdList(), 
                    cacheObject.getDirectReportIdList(), cacheObject.getOrgReportIdList(), true);

            if(paxWithGivingLimitIdList.containsKey(0L)){
                cacheObject.setBudgetWithGivingLimitIdList(paxWithGivingLimitIdList.remove(0L));
            }
            else{
                cacheObject.setBudgetWithGivingLimitIdList(new ArrayList<Long>());
            }
            cacheObject.setPaxWithGivingLimitBudgetIdMap(paxWithGivingLimitIdList);
        }

        return paxWithGivingLimitIdList;
    }

}
