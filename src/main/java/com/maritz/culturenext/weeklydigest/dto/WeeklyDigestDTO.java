package com.maritz.culturenext.weeklydigest.dto;

public class WeeklyDigestDTO {

    Long batchId;
    String fromMonth;
    Integer fromDay;
    String toMonth;
    Integer toDay;
    Long paxId;
    String preferredLocale;
    String emailAddress;
    Boolean receiveEmailDigest;
    String messageHeader;
    Integer networkCount;
    Integer directReportCount;
    Integer myBalance;
    Integer myBudget;
    Double expiringPoints;
    String expiringDate;
    Integer recGivenPax;
    Integer recReceivedPax;
    Integer likeReceivedPax;
    Integer commentReceivedPax;
    Integer recGivenTeam;
    Integer recReceivedTeam;
    Integer likeReceivedTeam;
    Integer commentReceivedTeam;
    Integer likeGivenTeam;
    Integer commentGivenTeam;
    Integer myApprovals;
    Integer networkAnniversaries;
    Integer networkBirthdays;
    Integer observances;
    Integer otherEvents;
    Integer nonNetworkAnniversaries;
    Integer nonNetworkBirthdays;
    String clientLogo;
    String clientUrl;
    String giveRecUrl;
    String notificationUrl;
    String profileUrl;
    String reportingUrl;
    String calendarUrl;
    String settingsUrl;
    String primaryColor;
    String secondaryColor;
    String textOnPrimaryColor;
    String pointBank;
    Boolean hasActivity;
    Boolean hasTeamActivity;
    Boolean likingEnabled;
    Boolean commentingEnabled;
    Boolean networkEnabled;
    Boolean calendarEnabled;
    Boolean serviceAnniversaryEnabled;
    String currentYear;

    public Long getBatchId() {
        return batchId;
    }
    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }
    public String getFromMonth() {
        return fromMonth;
    }
    public void setFromMonth(String fromMonth) {
        this.fromMonth = fromMonth;
    }
    public Integer getFromDay() {
        return fromDay;
    }
    public void setFromDay(Integer fromDay) {
        this.fromDay = fromDay;
    }
    public String getToMonth() {
        return toMonth;
    }
    public void setToMonth(String toMonth) {
        this.toMonth = toMonth;
    }
    public Integer getToDay() {
        return toDay;
    }
    public void setToDay(Integer toDay) {
        this.toDay = toDay;
    }
    public Long getPaxId() {
        return paxId;
    }
    public void setPaxId(Long paxId) {
        this.paxId = paxId;
    }
    public String getPreferredLocale() {
        return preferredLocale;
    }
    public void setPreferredLocale(String preferredLocale) {
        this.preferredLocale = preferredLocale;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public Boolean getReceiveEmailDigest() {
        return receiveEmailDigest;
    }
    public void setReceiveEmailDigest(Boolean receiveEmailDigest) {
        this.receiveEmailDigest = receiveEmailDigest;
    }
    public String getMessageHeader() {
        return messageHeader;
    }
    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }
    public Integer getDirectReportCount() {
        return directReportCount;
    }
    public void setDirectReportCount(Integer directReportCount) {
        this.directReportCount = directReportCount;
    }
    public Integer getNetworkCount() {
        return networkCount;
    }
    public void setNetworkCount(Integer networkCount) {
        this.networkCount = networkCount;
    }
    public Integer getMyBalance() {
        return myBalance;
    }
    public void setMyBalance(Integer myBalance) {
        this.myBalance = myBalance;
    }
    public Integer getMyBudget() {
        return myBudget;
    }
    public void setMyBudget(Integer myBudget) {
        this.myBudget = myBudget;
    }
    public Double getExpiringPoints() {
        return expiringPoints;
    }
    public void setExpiringPoints(Double expiringPoints) {
        this.expiringPoints = expiringPoints;
    }
    public String getExpiringDate() {
        return expiringDate;
    }
    public void setExpiringDate(String expiringDate) {
        this.expiringDate = expiringDate;
    }
    public Integer getRecGivenPax() {
        return recGivenPax;
    }
    public void setRecGivenPax(Integer recGivenPax) {
        this.recGivenPax = recGivenPax;
    }
    public Integer getRecReceivedPax() {
        return recReceivedPax;
    }
    public void setRecReceivedPax(Integer recReceivedPax) {
        this.recReceivedPax = recReceivedPax;
    }
    public Integer getLikeReceivedPax() {
        return likeReceivedPax;
    }
    public void setLikeReceivedPax(Integer likeReceivedPax) {
        this.likeReceivedPax = likeReceivedPax;
    }
    public Integer getCommentReceivedPax() {
        return commentReceivedPax;
    }
    public void setCommentReceivedPax(Integer commentReceivedPax) {
        this.commentReceivedPax = commentReceivedPax;
    }
    public Integer getRecGivenTeam() {
        return recGivenTeam;
    }
    public void setRecGivenTeam(Integer recGivenTeam) {
        this.recGivenTeam = recGivenTeam;
    }
    public Integer getRecReceivedTeam() {
        return recReceivedTeam;
    }
    public void setRecReceivedTeam(Integer recReceivedTeam) {
        this.recReceivedTeam = recReceivedTeam;
    }
    public Integer getLikeReceivedTeam() {
        return likeReceivedTeam;
    }
    public void setLikeReceivedTeam(Integer likeReceivedTeam) {
        this.likeReceivedTeam = likeReceivedTeam;
    }
    public Integer getCommentReceivedTeam() {
        return commentReceivedTeam;
    }
    public void setCommentReceivedTeam(Integer commentReceivedTeam) {
        this.commentReceivedTeam = commentReceivedTeam;
    }
    public Integer getLikeGivenTeam() {
        return likeGivenTeam;
    }
    public void setLikeGivenTeam(Integer likeGivenTeam) {
        this.likeGivenTeam = likeGivenTeam;
    }
    public Integer getCommentGivenTeam() {
        return commentGivenTeam;
    }
    public void setCommentGivenTeam(Integer commentGivenTeam) {
        this.commentGivenTeam = commentGivenTeam;
    }
    public Integer getMyApprovals() {
        return myApprovals;
    }
    public void setMyApprovals(Integer myApprovals) {
        this.myApprovals = myApprovals;
    }
    public Integer getNetworkAnniversaries() {
        return networkAnniversaries;
    }
    public void setNetworkAnniversaries(Integer networkAnniversaries) {
        this.networkAnniversaries = networkAnniversaries;
    }
    public Integer getNetworkBirthdays() {
        return networkBirthdays;
    }
    public void setNetworkBirthdays(Integer networkBirthdays) {
        this.networkBirthdays = networkBirthdays;
    }
    public Integer getObservances() {
        return observances;
    }
    public void setObservances(Integer observances) {
        this.observances = observances;
    }
    public Integer getOtherEvents() {
        return otherEvents;
    }
    public void setOtherEvents(Integer otherEvents) {
        this.otherEvents = otherEvents;
    }
    public Integer getNonNetworkAnniversaries() {
        return nonNetworkAnniversaries;
    }
    public void setNonNetworkAnniversaries(Integer nonNetworkAnniversaries) {
        this.nonNetworkAnniversaries = nonNetworkAnniversaries;
    }
    public Integer getNonNetworkBirthdays() {
        return nonNetworkBirthdays;
    }
    public void setNonNetworkBirthdays(Integer nonNetworkBirthdays) {
        this.nonNetworkBirthdays = nonNetworkBirthdays;
    }
    public String getClientLogo() {
        return clientLogo;
    }
    public void setClientLogo(String clientLogo) {
        this.clientLogo = clientLogo;
    }
    public String getClientUrl() {
        return clientUrl;
    }
    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }
    public String getGiveRecUrl() {
        return giveRecUrl;
    }
    public void setGiveRecUrl(String giveRecUrl) {
        this.giveRecUrl = giveRecUrl;
    }
    public String getNotificationUrl() {
        return notificationUrl;
    }
    public void setNotificationUrl(String notificationUrl) {
        this.notificationUrl = notificationUrl;
    }
    public String getProfileUrl() {
        return profileUrl;
    }
    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }
    public String getReportingUrl() {
        return reportingUrl;
    }
    public void setReportingUrl(String reportingUrl) {
        this.reportingUrl = reportingUrl;
    }
    public String getCalendarUrl() {
        return calendarUrl;
    }
    public void setCalendarUrl(String calendarUrl) {
        this.calendarUrl = calendarUrl;
    }
    public String getSettingsUrl() {
        return settingsUrl;
    }
    public void setSettingsUrl(String settingsUrl) {
        this.settingsUrl = settingsUrl;
    }
    public String getPrimaryColor() {
        return primaryColor;
    }
    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }
    public String getSecondaryColor() {
        return secondaryColor;
    }
    public void setSecondaryColor(String secondaryColor) {
        this.secondaryColor = secondaryColor;
    }
    public String getTextOnPrimaryColor() {
        return textOnPrimaryColor;
    }
    public void setTextOnPrimaryColor(String textOnPrimaryColor) {
        this.textOnPrimaryColor = textOnPrimaryColor;
    }
    public String getPointBank() {
        return pointBank;
    }
    public void setPointBank(String pointBank) {
        this.pointBank = pointBank;
    }
    public Boolean getHasActivity() {
        return hasActivity;
    }
    public void setHasActivity(Boolean hasActivity) {
        this.hasActivity = hasActivity;
    }
    public Boolean getHasTeamActivity() {
        return hasTeamActivity;
    }
    public void setHasTeamActivity(Boolean hasTeamActivity) {
        this.hasTeamActivity = hasTeamActivity;
    }
    public Boolean getLikingEnabled() {
        return likingEnabled;
    }
    public void setLikingEnabled(Boolean likingEnabled) {
        this.likingEnabled = likingEnabled;
    }
    public Boolean getCommentingEnabled() {
        return commentingEnabled;
    }
    public void setCommentingEnabled(Boolean commentingEnabled) {
        this.commentingEnabled = commentingEnabled;
    }
    public Boolean getNetworkEnabled() {
        return networkEnabled;
    }
    public void setNetworkEnabled(Boolean networkEnabled) {
        this.networkEnabled = networkEnabled;
    }
    public Boolean getCalendarEnabled() {
        return calendarEnabled;
    }
    public void setCalendarEnabled(Boolean calendarEnabled) {
        this.calendarEnabled = calendarEnabled;
    }
    public Boolean getServiceAnniversaryEnabled() {
        return serviceAnniversaryEnabled;
    }
    public void setServiceAnniversaryEnabled(Boolean serviceAnniversaryEnabled) {
        this.serviceAnniversaryEnabled = serviceAnniversaryEnabled;
    }
    public String getCurrentYear() {
        return currentYear;
    }
    public void setCurrentYear(String currentYear) {
        this.currentYear = currentYear;
    }
}