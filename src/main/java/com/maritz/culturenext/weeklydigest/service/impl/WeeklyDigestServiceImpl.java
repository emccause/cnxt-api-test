package com.maritz.culturenext.weeklydigest.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.google.common.base.Splitter;
import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.batch.BatchConstants;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.PermissionConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.PointBankEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.weeklydigest.dao.PointBalanceDao;
import com.maritz.culturenext.weeklydigest.dao.WeeklyDigestDao;
import com.maritz.culturenext.weeklydigest.dao.BigQueryPointBalanceDao;
import com.maritz.culturenext.weeklydigest.dto.WeeklyDigestDTO;
import com.maritz.culturenext.weeklydigest.service.WeeklyDigestService;
import com.maritz.notification.service.NotificationRequest;
import com.maritz.notification.service.NotificationService;

@Service
public class WeeklyDigestServiceImpl implements WeeklyDigestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private BatchService batchService;
    @Inject private ConcentrixDao<ApplicationData> applicationDataDao;
    @Inject private Environment environment;
    @Inject private NotificationService notificationService;
    @Inject private PointBalanceDao pointBalanceDao;
    @Inject private TranslationUtil translationUtil;
    @Inject private WeeklyDigestDao weeklyDigestDao;
    @Inject private BigQueryPointBalanceDao bigQueryPointBalanceDao;
    
    private static final String WEEKLY_DIGEST_EVENT_TYPE = "WEEKLY_DIGEST";

    //The base URL, ex: http://culturenext-maritzdev2.cloudapp.net/maritz-culturenext
    private static String baseUrl = "";
    private static String primaryColor = "";
    private static String secondaryColor = "";
    private static String textOnPrimaryColor = "";

    //How many freemarker templates we want to loop through and create at one time, before garbage collection happens
    public static final int LOOP_SIZE = 5000;

    @TriggeredTask(batch=true)
    public void generateWeeklyDigest(Batch batch) {
        logger.info("Started generateWeeklyDigest job. BatchId: {} ",  null == batch ? "BatchId is null" : batch.getBatchId());
        createEmails(batch.getBatchId());
        logger.info("Finished generateWeeklyDigest job. BatchId: {}",  null == batch ? "BatchId is null" : batch.getBatchId());
    }

    /**
     * Queries the WEEKLY_DIGEST table for the given BATCH_ID
     * and uses FreeMarker to generate the .html templates for each email
     * and save them in the EMAIL_MESSAGE table.
     * @parm batchId - ID for the associated BATCH record
     */
    /**
     *
     */
    /**
     *
     */
    @Override
    public void createEmails(Long batchId) {

        //Only need to pull these values from the database one time
        baseUrl = environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL);
        primaryColor = environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR);
        secondaryColor = environment.getProperty(ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR);
        textOnPrimaryColor = environment.getProperty(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR);

        //Call the SQL that populates the reporting table and returns the results
        List<Map<String, Object>> nodes = weeklyDigestDao.getWeeklyDigestInfo(batchId);
        
        //Get point balances from Atlas instead of GCP
        Map<String, String> pointBalanceMap = new HashMap<>();
        try {
        	if (environment.getProperty(ApplicationDataConstants.KEY_NAME_POINT_BANK).equals(PointBankEnum.ABS.name())) {
        		
        		//Get all subclients & product codes (ex 'FCP=1494.2,GNC=1494.10')
        		String subclientProductList = environment.getProperty(ApplicationDataConstants.KEY_NAME_MARS_SUBCLIENT);
        		Map<String, String> subclientProductMap = Splitter.on(",").withKeyValueSeparator("=").split(subclientProductList);
        		List<String> productList = new ArrayList<>(subclientProductMap.keySet());
        		List<String> subclientList = new ArrayList<>(subclientProductMap.values());
        		
        		//Access url to get the environment bigQuery has to connect to
        		baseUrl = environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL);
        		String env = (baseUrl.contains("/u-") || baseUrl.contains("/d-")) ? "UAT" : "PROD";
        		
        		//Get point balances based on product and client codes
        		pointBalanceMap = bigQueryPointBalanceDao.getPointBalances(env, productList, subclientList);
        	}
        } catch(Exception e) {
        	String filePath = System.getProperty("user.home") + File.separator + "gcp" + File.separator + "application.json";
        	logger.error("Query to Atlas has failed. " + e.getMessage());
        	logger.error("Credentials file located at " + filePath);
        }
        
        int initialSize = nodes.size();
        int processedCount = 0;
        int count = 0; //Total number of emails successfully saved in the EMAIL_MESSAGE table

        logger.info("Total records is " + initialSize);

        //Process FreeMarker templates in a separate method, in batches of a predetermined size (LOOP_SIZE)
        //This way java can do garbage collection in between method calls
        while (processedCount < initialSize) {
            int toIndex = (LOOP_SIZE > nodes.size()) ? nodes.size() : LOOP_SIZE;

            logger.info("Completed templates: " + processedCount);
            logger.info("Beginning to populate the next " + toIndex + " freemarker templates");

            List<Map<String, Object>> nodeSubList = new ArrayList<>();
            nodeSubList = nodes.subList(0, toIndex);

            //This method will process the FreeMarker template and save the result in EMAIL_MESSAGE
            //It will return the number of emails that got sucessfully saved in the database.
            count += generateEmails(nodeSubList, pointBalanceMap, batchId);

            //Remove the already processed records from the list (less to have in memory)
            nodes.removeAll(nodeSubList);

            processedCount += toIndex;

        }

        logger.info("Finished processing freemarker templates.");
        
        batchService.createBatchEvent(batchId, new BatchEventDTO()
            .setType(BatchEventTypeCode.RECS.name())
            .setMessage(String.valueOf(count))
        );
    }


    /**
     * Loops through a sub-set of the WEEKLY_DIGEST result set and processes the FreeMarker template.
     * Then creates a NotificationRequest object for each template and saves it in the EMAIL_MESSAGE table.
     * @param nodeSubList - sub-set of the WEEKLY_DIGEST result set to loop through
     * @param pointBalanceMap - map of CONTROL_NUM to ABS point balances
     * @param batchId
     * @return int - number of emails saved in the EMAIL_MESSAGE table
     */
    private int generateEmails(List<Map<String, Object>> nodeSubList, Map<String, String> pointBalanceMap, Long batchId) {
        //Loop through the result set and populate the DTO using the results (the DTO is what maps to the template)
        Collection<NotificationRequest> notificationRequests = new ArrayList<>(nodeSubList.size());
        for (Map<String, Object> node : nodeSubList) {
            //Populate the emailInfoDTO with values from the database
            WeeklyDigestDTO weeklyDigestDto = populateWeeklyDigestDto(node);

            //Save point balance if it was returned
            if (!pointBalanceMap.isEmpty() && pointBalanceMap.get(node.get(ProjectConstants.CONTROL_NUM)) != null) {
                weeklyDigestDto.setMyBalance(Double.valueOf((pointBalanceMap.get(node.get(ProjectConstants.CONTROL_NUM)))).intValue());
            }
            else {
                //The freemarker template will hide the MyBalance section if the value is 0.
                weeklyDigestDto.setMyBalance(0);
            }

            //The user should only get the email if they have recent recognition activity
            if (weeklyDigestDto.getHasActivity()
                    || weeklyDigestDto.getHasTeamActivity()
                    || !weeklyDigestDto.getMyApprovals().equals(0)
                    || !weeklyDigestDto.getNetworkAnniversaries().equals(0)
                    || !weeklyDigestDto.getNetworkBirthdays().equals(0)) {

                //Populate the NotificationRequest object and save it in EMAIL_MESSAGE
                //count += notificationService.sendNotification();
                notificationRequests.add(populateNotificationRequest(new NotificationRequest(), weeklyDigestDto));
            }
        }

        return notificationService.sendNotification(notificationRequests);
    }

    /**
     * Maps the SQL result of one row from the WEEKLY_DIGEST table into a WeeklyDigestDTO object
     *
     * @param node - the map of the SQL result set
     */
    private WeeklyDigestDTO populateWeeklyDigestDto(Map<String, Object> node) {

        WeeklyDigestDTO weeklyDigestDto = new WeeklyDigestDTO();

        //Info from SQL result set
        weeklyDigestDto.setBatchId((Long) node.get(ProjectConstants.BATCH_ID));
        weeklyDigestDto.setPaxId((Long) node.get(ProjectConstants.PAX_ID));
        weeklyDigestDto.setPreferredLocale((String) node.get(ProjectConstants.PREFERRED_LOCALE));
        weeklyDigestDto.setDirectReportCount((Integer) node.get(ProjectConstants.DIRECT_REPORT_COUNT));
        weeklyDigestDto.setNetworkCount((Integer) node.get(ProjectConstants.NETWORK_COUNT));
        //get my budget balance, check for null and set to zero
        weeklyDigestDto.setMyBudget((node.get(ProjectConstants.MY_BUDGET) == null ? 0 : ((Double) node.get(ProjectConstants.MY_BUDGET)).intValue()) );
        weeklyDigestDto.setRecGivenPax((Integer) node.get(ProjectConstants.REC_GIVEN_PAX));
        weeklyDigestDto.setRecReceivedPax((Integer) node.get(ProjectConstants.REC_RECEIVED_PAX));
        weeklyDigestDto.setLikeReceivedPax((Integer) node.get(ProjectConstants.LIKE_RECEIVED_PAX));
        weeklyDigestDto.setCommentReceivedPax((Integer) node.get(ProjectConstants.COMMENT_RECEIVED_PAX));
        weeklyDigestDto.setRecGivenTeam((Integer) node.get(ProjectConstants.REC_GIVEN_TEAM));
        weeklyDigestDto.setRecReceivedTeam((Integer) node.get(ProjectConstants.REC_RECEIVED_TEAM));
        weeklyDigestDto.setLikeReceivedTeam((Integer) node.get(ProjectConstants.LIKE_RECEIVED_TEAM));
        weeklyDigestDto.setCommentReceivedTeam((Integer) node.get(ProjectConstants.COMMENT_RECEIVED_TEAM));
        weeklyDigestDto.setLikeGivenTeam((Integer) node.get(ProjectConstants.LIKE_GIVEN_TEAM));
        weeklyDigestDto.setCommentGivenTeam((Integer) node.get(ProjectConstants.COMMENT_GIVEN_TEAM));
        weeklyDigestDto.setMyApprovals((Integer) node.get(ProjectConstants.MY_APPROVALS));
        weeklyDigestDto.setNetworkAnniversaries((Integer) node.get(ProjectConstants.NETWORK_ANNIVERSARIES));
        weeklyDigestDto.setNetworkBirthdays((Integer) node.get(ProjectConstants.NETWORK_BIRTHDAYS));
        weeklyDigestDto.setObservances((Integer) node.get(ProjectConstants.OBSERVANCES));
        weeklyDigestDto.setOtherEvents((Integer) node.get(ProjectConstants.OTHER_EVENTS));
        weeklyDigestDto.setNonNetworkAnniversaries((Integer) node.get(ProjectConstants.NON_NETWORK_ANNIVERSARIES));
        weeklyDigestDto.setNonNetworkBirthdays((Integer) node.get(ProjectConstants.NON_NETWORK_BIRTHDAYS));

        //Expiring points section has been de-scoped, so just set it as 0 (will not display in template due to conditionals)
        weeklyDigestDto.setExpiringPoints(0D);

        //Client color scheme
        weeklyDigestDto.setPrimaryColor(primaryColor);
        weeklyDigestDto.setSecondaryColor(secondaryColor);
        weeklyDigestDto.setTextOnPrimaryColor(textOnPrimaryColor);

        //Build up the URLs
        String logoUrl = baseUrl + BatchConstants.LOGO_URI;
        weeklyDigestDto.setClientLogo(logoUrl);
        weeklyDigestDto.setClientUrl(baseUrl);

        //Deep links to different pages
        weeklyDigestDto.setGiveRecUrl(baseUrl + BatchConstants.DEEP_LINK_GIVEREC_MODAL);
        weeklyDigestDto.setNotificationUrl(baseUrl + BatchConstants.DEEP_LINK_NOTIFICATIONS_URI);
        weeklyDigestDto.setProfileUrl(baseUrl + BatchConstants.DEEP_LINK_PROFILE_URI);
        weeklyDigestDto.setReportingUrl(baseUrl + BatchConstants.DEEP_LINK_REPORTING_URI);
        weeklyDigestDto.setCalendarUrl(baseUrl + BatchConstants.DEEP_LINK_CALENDAR_URI);
        weeklyDigestDto.setSettingsUrl(baseUrl + BatchConstants.DEEP_LINK_SETTINGS_URI);

        //Dates        
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime((Date) node.get(ProjectConstants.START_DATE));
        
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime((Date) node.get(ProjectConstants.END_DATE));
        
        weeklyDigestDto.setFromDay(startCalendar.get(Calendar.DAY_OF_MONTH)); //1-31
        weeklyDigestDto.setToDay(endCalendar.get(Calendar.DAY_OF_MONTH));
        Calendar now = Calendar.getInstance();
        weeklyDigestDto.setCurrentYear(String.valueOf(now.get(Calendar.YEAR)));

        //Handle translation for the subject line and date range
        weeklyDigestDto = getTranslatedEmailContent(weeklyDigestDto, startCalendar.get(Calendar.MONTH), endCalendar.get(Calendar.MONTH));

        //Check user's permissions to hide certain sections
        weeklyDigestDto = resolveUserPermissions(weeklyDigestDto);

        //Check the projectProfile.pointBank - will toggle display of "My Balance" section
        weeklyDigestDto.setPointBank(environment.getProperty(ApplicationDataConstants.KEY_NAME_POINT_BANK));

        //Data used for conditional display of certain sections
        if (!weeklyDigestDto.getRecGivenPax().equals(0) || !weeklyDigestDto.getRecReceivedPax().equals(0)
                || !weeklyDigestDto.getLikeReceivedPax().equals(0) || !weeklyDigestDto.getCommentReceivedPax().equals(0)) {
            weeklyDigestDto.setHasActivity(true);
        } else {
            weeklyDigestDto.setHasActivity(false);
        }

        if (!weeklyDigestDto.getRecGivenTeam().equals(0) || !weeklyDigestDto.getRecReceivedTeam().equals(0)
                || !weeklyDigestDto.getLikeReceivedTeam().equals(0) || !weeklyDigestDto.getCommentReceivedTeam().equals(0)
                || !weeklyDigestDto.getLikeGivenTeam().equals(0) || !weeklyDigestDto.getCommentGivenTeam().equals(0)) {
            weeklyDigestDto.setHasTeamActivity(true);
        } else {
            weeklyDigestDto.setHasTeamActivity(false);
        }

        return weeklyDigestDto;
    }

    /**
     * Fetches the translated values for the email subject line and
     * the month range for which the email is being generated.
     * Saves the translated values to the DTO and then returns it.
     * @param WeeklyDigestDTO - the DTO to hold the translated values
     * @param fromMonth - 0-11 representation of the month
     * @param toMonth - 0-11 representation of the month
     * @return WeeklyDigestDTO
     */
    public WeeklyDigestDTO getTranslatedEmailContent(WeeklyDigestDTO weeklyDigestDto, int fromMonth, int toMonth) {

        ArrayList<String> applicationDataKeys = new ArrayList<>();
        applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_WEEKLY_DIGEST_HEADER);

        switch (fromMonth) {
        case 0:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JANUARY);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_JANUARY);
            break;
        case 1:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_FEBRUARY);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_FEBRUARY);
            break;
        case 2:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_MARCH);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_MARCH);
            break;
        case 3:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_APRIL);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_APRIL);
            break;
        case 4:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_MAY);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_MAY);
            break;
        case 5:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JUNE);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_JUNE);
            break;
        case 6:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JULY);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_JULY);
            break;
        case 7:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_AUGUST);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_AUGUST);
            break;
        case 8:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_SEPTEMBER);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_SEPTEMBER);
            break;
        case 9:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_OCTOBER);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_OCTOBER);
            break;
        case 10:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_NOVEMBER);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_NOVEMBER);
            break;
        case 11:
            applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_DECEMBER);
            weeklyDigestDto.setFromMonth(ApplicationDataConstants.KEY_NAME_DECEMBER);
            break;
        }

        if (fromMonth != toMonth) {
            switch (toMonth) {
            case 0:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JANUARY);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_JANUARY);
                break;
            case 1:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_FEBRUARY);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_FEBRUARY);
                break;
            case 2:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_MARCH);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_MARCH);
                break;
            case 3:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_APRIL);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_APRIL);
                break;
            case 4:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_MAY);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_MAY);
                break;
            case 5:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JUNE);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_JUNE);
                break;
            case 6:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_JULY);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_JULY);
                break;
            case 7:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_AUGUST);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_AUGUST);
                break;
            case 8:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_SEPTEMBER);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_SEPTEMBER);
                break;
            case 9:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_OCTOBER);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_OCTOBER);
                break;
            case 10:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_NOVEMBER);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_NOVEMBER);
                break;
            case 11:
                applicationDataKeys.add(ApplicationDataConstants.KEY_NAME_DECEMBER);
                weeklyDigestDto.setToMonth(ApplicationDataConstants.KEY_NAME_DECEMBER);
                break;
            }
        } else {
            weeklyDigestDto.setToMonth(weeklyDigestDto.getFromMonth());
        }

        List<ApplicationData> applicationDataList = applicationDataDao.findBy()
            .where(ProjectConstants.KEY_NAME).in(applicationDataKeys)
            .find()
        ;

        Map<Long, String> emailLanguageMap = fetchTranslations(weeklyDigestDto.getPreferredLocale(), applicationDataList);

        //Update the translated value on the DTO
        for (ApplicationData applicationData : applicationDataList) {
            if (applicationData.getKeyName().equals(ApplicationDataConstants.KEY_NAME_WEEKLY_DIGEST_HEADER)) {
                weeklyDigestDto.setMessageHeader(emailLanguageMap.get(applicationData.getApplicationDataId()));
            }
            else {
                if (applicationData.getKeyName().equals(weeklyDigestDto.getFromMonth())) {
                    weeklyDigestDto.setFromMonth(emailLanguageMap.get(applicationData.getApplicationDataId()));
                }
                if (applicationData.getKeyName().equals(weeklyDigestDto.getToMonth())) {
                    weeklyDigestDto.setToMonth(emailLanguageMap.get(applicationData.getApplicationDataId()));
                }
            }
        }

        return weeklyDigestDto;
    }

    /**
     * Pulls permission data for a user and maps it to the weeklyDigestDto
     * @param weeklyDigestDto
     * @return
     */
    public WeeklyDigestDTO resolveUserPermissions(WeeklyDigestDTO weeklyDigestDto) {
        Map<String, Object> node = weeklyDigestDao.getUsersPermissions(weeklyDigestDto.getPaxId());
        String permission;

        if (node != null) {

            //LIKING permissions
            permission = (String) node.get(ProjectConstants.LIKING);
            if (permission == null) {
                weeklyDigestDto.setLikingEnabled(
                        Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED)));
            } else if (permission.equals(PermissionConstants.ENABLE_PREFIX + PermissionConstants.LIKING_TYPE)) {
                weeklyDigestDto.setLikingEnabled(true);
            } else if (permission.equals(PermissionConstants.DISABLE_PREFIX + PermissionConstants.LIKING_TYPE)) {
                weeklyDigestDto.setLikingEnabled(false);
            }

            //TODO MP-9657 - Use likingEnabled for the conditional checks
            if (!weeklyDigestDto.getLikingEnabled()) {
                weeklyDigestDto.setLikeReceivedPax(0);
                weeklyDigestDto.setLikeReceivedTeam(0);
                weeklyDigestDto.setLikeGivenTeam(0);
            }

            //COMMENTING permission
            permission = (String) node.get(ProjectConstants.COMMENTING);
            if (permission == null) {
                weeklyDigestDto.setCommentingEnabled(
                        Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED)));
            } else if (permission.equals(PermissionConstants.ENABLE_PREFIX + PermissionConstants.COMMENTING_TYPE)) {
                weeklyDigestDto.setCommentingEnabled(true);
            } else if (permission.equals(PermissionConstants.DISABLE_PREFIX + PermissionConstants.COMMENTING_TYPE)) {
                weeklyDigestDto.setCommentingEnabled(false);
            }

            //TODO MP-9657 - Use commentingEnabled for the conditional checks
            if (!weeklyDigestDto.getCommentingEnabled()) {
                weeklyDigestDto.setCommentReceivedPax(0);
                weeklyDigestDto.setCommentReceivedTeam(0);
                weeklyDigestDto.setCommentGivenTeam(0);
            }

            //NETWORK permission
            permission = (String) node.get(ProjectConstants.NETWORK);
            if (permission == null) {
                weeklyDigestDto.setNetworkEnabled(
                        Boolean.valueOf(environment.getProperty(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED)));
            } else if (permission.equals(PermissionConstants.ENABLE_PREFIX + PermissionConstants.NETWORK_TYPE)) {
                weeklyDigestDto.setNetworkEnabled(true);
            } else if (permission.equals(PermissionConstants.DISABLE_PREFIX + PermissionConstants.NETWORK_TYPE)) {
                weeklyDigestDto.setNetworkEnabled(false);
            }

            //CALENDAR permission TODO MP-9567
            weeklyDigestDto.setCalendarEnabled(true);

            //SERVICE_ANNIVERSARY permission TODO MP-9567
            weeklyDigestDto.setServiceAnniversaryEnabled(true);
        }

        return weeklyDigestDto;
    }

    /**
     * Queries the translationUtil to create a map of IDs and their
     * associated translations for the specified languageCode.
     * If no translations are found, English is returned by default
     * @param languageCode
     * @param entries
     * @return
     */
    private Map<Long, String> fetchTranslations(String languageCode, List<ApplicationData> entries) {
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);

        Map<Long, String> translations = null;
        if (entries != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (ApplicationData entry : entries) {
                rowIds.add(entry.getApplicationDataId());
                defaultMap.put(entry.getApplicationDataId(), entry.getValue());
            }

            if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                return defaultMap;
            }

            translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode,
                    TableName.APPLICATION_DATA.name(),
                    TranslationConstants.COLUMN_NAME_VALUE, rowIds, defaultMap);
        }

        return translations;
    }

    /**
     * This method populates a notificationRequest object with the information stored in a weeklyDigestDTO
     *
     * @param notificationReuqest - The request object to be populated
     * @param weeklyDigestDTO - The DTO containing the information to save in the notificationRequest
     * @param htmlContent - The .html code that represents the email template with all placeholders resolved
     */
    private NotificationRequest populateNotificationRequest(NotificationRequest notificationRequest, WeeklyDigestDTO weeklyDigestDto) {

        //Don't populate from and it will use the sending email in APPLICATION_DATA
        notificationRequest.setTo(weeklyDigestDto.getPaxId());
        notificationRequest.setSubject(weeklyDigestDto.getMessageHeader());
        notificationRequest.setEventType(WEEKLY_DIGEST_EVENT_TYPE);
        Calendar now = Calendar.getInstance();
        now.set(Calendar.DAY_OF_WEEK, 2); //Set the day to Monday (1-7, 1 being Sunday)
        now.set(Calendar.HOUR_OF_DAY, 7); //0-23
        notificationRequest.setSendDate(now.getTime());

        //The REFERENCE_FK for Weekly Digest emails will be the BATCH_ID that created the email.
        notificationRequest.setReferenceId(weeklyDigestDto.getBatchId());
        
        //Maritz-Notification will pass all variables to the FreeMarker engine
        notificationRequest.setVariables(ObjectUtils.objectToMap(weeklyDigestDto));

        return notificationRequest;
    }

}