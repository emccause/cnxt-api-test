package com.maritz.culturenext.weeklydigest.service;

public interface WeeklyDigestService {

    void createEmails(Long batchId);

}