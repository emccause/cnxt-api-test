package com.maritz.culturenext.weeklydigest.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.weeklydigest.dao.WeeklyDigestDao;

@Repository
public class WeeklyDigestDaoImpl extends AbstractDaoImpl implements WeeklyDigestDao {


    private static final String BATCH_ID_SQL_PARAM = "batchId";
    private static final String PAX_ID_SQL_PARAM = "paxId";

    private static final String WEEKLY_DIGEST_INFO_QUERY =
            "EXEC component.UP_WEEKLY_DIGEST :" + BATCH_ID_SQL_PARAM;

    private static final String PERMISSIONS_QUERY =
            "SELECT " +
                "MIN(CASE WHEN r.ROLE_CODE IN ('ENABLE_LIKING', 'DISABLE_LIKING') THEN r.ROLE_CODE END) AS LIKING, " +
                "MIN(CASE WHEN r.ROLE_CODE IN ('ENABLE_COMMENTING', 'DISABLE_COMMENTING') THEN r.ROLE_CODE END) AS COMMENTING, " +
                "MIN(CASE WHEN r.ROLE_CODE IN ('ENABLE_NETWORK', 'DISABLE_NETWORK') THEN r.ROLE_CODE END) AS NETWORK " +
            "FROM component.ROLE r " +
            "JOIN component.ACL acl ON r.ROLE_ID = acl.ROLE_ID " +
            "JOIN component.VW_GROUP_TOTAL_MEMBERSHIP gtm ON acl.SUBJECT_TABLE = 'GROUPS' AND acl.SUBJECT_ID = gtm.group_id " +
            "JOIN component.PAX p ON p.PAX_ID = gtm.PAX_ID OR (acl.SUBJECT_TABLE = 'PAX' AND acl.SUBJECT_ID = p.PAX_ID) " +
            "WHERE p.PAX_ID IS NOT NULL " +
            "AND r.ROLE_CODE IN ('ENABLE_LIKING', 'ENABLE_COMMENTING', 'ENABLE_NETWORK', 'DISABLE_LIKING', 'DISABLE_COMMENTING', 'DISABLE_NETWORK') " +
            "AND gtm.PAX_ID = :" + PAX_ID_SQL_PARAM;

    @Override
    public List<Map<String, Object>> getWeeklyDigestInfo(Long batchId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);

        return namedParameterJdbcTemplate.query(WEEKLY_DIGEST_INFO_QUERY, params, new CamelCaseMapRowMapper());
    }

    @Override
    public Map<String, Object> getUsersPermissions(Long paxId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, paxId);

        List<Map<String,Object>> nodes = namedParameterJdbcTemplate.query(PERMISSIONS_QUERY, params, new CamelCaseMapRowMapper());

        if (!CollectionUtils.isEmpty(nodes)) {
            return nodes.get(0);
        }

        return null;
    }

}
