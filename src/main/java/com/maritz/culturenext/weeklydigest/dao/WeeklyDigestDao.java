package com.maritz.culturenext.weeklydigest.dao;

import java.util.List;
import java.util.Map;

public interface WeeklyDigestDao {

    List<Map<String, Object>> getWeeklyDigestInfo(Long batchId);

    Map<String, Object> getUsersPermissions(Long paxId);

}
