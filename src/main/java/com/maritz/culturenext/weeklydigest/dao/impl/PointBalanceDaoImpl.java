package com.maritz.culturenext.weeklydigest.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.ReportingDaoImpl;
import com.maritz.culturenext.weeklydigest.dao.PointBalanceDao;

@Repository
public class PointBalanceDaoImpl extends ReportingDaoImpl implements PointBalanceDao {

    private static final String PRODUCT_CODE_SQL_PARAM = "productCode";
    private static final String CLIENT_NUMBER_SQL_PARAM = "clientNumber";

    private static final String POINT_BALANCE_QUERY =
            "SELECT ACCOUNT_ID, PARTICIPANT_NBR, FIRST_NAME, LAST_NAME, SUM(POINT_BALANCE) POINT_BALANCE, "
            + "SUM(POINT_BALANCE) * POINT_VALUE DOLLAR_BALANCE, Client_Number, Product_Nbr, Client_Src_Nm "
            + "FROM UDM7.CX_RPT_Pax_Balance "
            + "WHERE PRODUCT_NBR IN (:" + PRODUCT_CODE_SQL_PARAM + ") "
            + "AND CLIENT_NUMBER IN (:" + CLIENT_NUMBER_SQL_PARAM + ") "
            + "AND POINT_BALANCE > 0 "
            + "GROUP BY ACCOUNT_ID, PARTICIPANT_NBR, FIRST_NAME, LAST_NAME, "
            + "POINT_VALUE, Client_Number, Product_Nbr, Client_Src_Nm";

    @Override
    public List<Map<String, Object>> getPointBalances(List<String> productList, List<String> subclientList) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PRODUCT_CODE_SQL_PARAM, productList);
        params.addValue(CLIENT_NUMBER_SQL_PARAM, subclientList);

        return namedParameterJdbcTemplate.query(POINT_BALANCE_QUERY, params, new CamelCaseMapRowMapper());
    }

}
