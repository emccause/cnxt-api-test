package com.maritz.culturenext.weeklydigest.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.weeklydigest.dao.BigQueryPointBalanceDao;

@Repository
public class BigQueryPointBalanceDaoImpl implements BigQueryPointBalanceDao {	
	
	public static final String POINT_BALANCE_TABLE = "RewardsPointBalance";
	public static final String PARTICIPANT_TABLE = "Participant";
	public static final String PARTICIPANT_NBR_FIELD = "PARTICIPANTNBR";
	public static final String POINT_BALANCE_FIELD = "POINT_BALANCE";
	public String project_id = "";
	public String dataset = "";
	public String serviceAccount = "";
	public String productId = "";
	public String privateKeyId = "";
	public String privateKey = "";
	public String clientEmail = "";
	public String clientId = "";
	public String clientX509CertUrl = "";
	public String authUri = "";
	public String tokenUri = "";
	public String authproviderX509CertUrl = "";
	
	@Inject private Environment environ;
	
	@Override
	public Map<String, String> getPointBalances(String environment, List<String> productList, List<String> subclientList) throws Exception{
		
		String[] products = productList.toArray(new String[productList.size()]); 
		String[] clients = subclientList.toArray(new String[subclientList.size()]);
		
		//return runQuery(environment, products, clients);
		TableResult results = runQuery(environment, products, clients);
		
		return processResults(results);
	}
	
	@Override
	public TableResult runQuery(String environment, String[] products, String[] clients) throws Exception {
		
		switch (environment) {
			case "UAT": 
				project_id = "data-lake-pre-production";
				dataset = "domainobjects_current_dev";
				break;
			case "PROD":
				project_id = "data-lake-production-246315";
				dataset = "domainobjects_current_prod";
				break;
			default:
				project_id = "data-lake-pre-production";
				dataset = "domainobjects_current_dev";
		}
		
		String pointBalanceQuery = 
				"SELECT Bal.ACCOUNT_ID, Bal.PARTICIPANT_NBR AS PARTICIPANTNBR, Pax.FirstName AS FIRST_NAME, Pax.LastName AS LAST_NAME, "
				+ "Bal.POINT_BALANCE,Bal.DOLLAR_BALANCE,Bal.Client_Number,Bal.Product_Nbr, Pax.ClientName AS Client_Src_Nm "
				+ "FROM (SELECT AccountId AS ACCOUNT_ID, PaxNumber AS PARTICIPANT_NBR, PaxId, "
				+ "SUM(PointBalance) AS POINT_BALANCE, SUM(DollarBalance) AS DOLLAR_BALANCE, "
				+ "ClientKey.SubClient AS Client_Number, ClientKey.Product AS Product_Nbr, ClientKey.ClientName AS Client_Src_Nm   "
				+ "FROM " + project_id + "." + dataset + "." + POINT_BALANCE_TABLE + " "
				+ "WHERE ClientKey.SubClient IN UNNEST( @clientsParam ) "
				+ "AND ClientKey.Product IN UNNEST( @productsParam ) "
				+ "GROUP BY AccountId, PaxNumber, PaxId, ClientKey.SubClient, ClientKey.Product, ClientKey.ClientName) Bal "
				+ "INNER JOIN (SELECT distinct ClientKey.ClientName AS ClientName, ClientKey.ClientNumber AS ClientNumber, "
				+ "FirstName, LastName, ParticipantId AS CultureNextId, ExternalPaxId AS ABSId "
				+ "FROM " + project_id + "." + dataset + "." + PARTICIPANT_TABLE + " "
				+ "WHERE ClientKey.Source = 'CNEXT' ) Pax "
				+ " ON Bal.PARTICIPANT_NBR = Pax.ABSId;"; 
		
		//***************************************
		//Get Service Account Properties 
		serviceAccount = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_TYPE);
		authUri = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_AUTH_URI);
		tokenUri = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_TOKEN_URI);
		authproviderX509CertUrl = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_AUTH_PROVIDER_X509_CERT_URL);
		//Get Service Account Properties with evironment tag
		productId = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_PRODUCT_ID + "." + environment);
		privateKeyId = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_PRIVATE_KEY_ID + "." + environment);
		privateKey = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_PRIVATE_KEY + "." + environment);
		clientEmail = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_CLIENT_EMAIL + "." + environment);
		clientId = environ.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ACCOUNT_CLIENT_ID + "." + environment);
		clientX509CertUrl = environ.getProperty(ApplicationDataConstants.SERVICE_ACCOUNT_CLIENT_X509_CERT_URL + "." + environment);
		
		//create JSON file in memory to get Big Query Data
		String data = "{";
		String dataType = "\"type\": \"" +  serviceAccount  +  "\",";
		String dataProjectId = "\"dataProjectId\": \"" +  productId  +  "\",";
		String dataPrivateKeyId = "\"private_key_id\": \"" +  privateKeyId  +  "\",";
		//Note privateKey: from database no escape key for \ needed
		//Note privateKey: from application.properties escape key for \ needed
		String dataPrivateKey = "\"private_key\": \"" + privateKey + "\",";
		String dataClientEmail = "\"client_email\": \"" + clientEmail + "\",";
		String dataClientId = "\"client_id\": \"" + clientId + "\",";
		String dataAuthUri = "\"auth_uri\": \"" + authUri + "\",";
		String dataTokenUri = "\"token_uri\": \"" + tokenUri + "\",";
		String dataAuthProviderX509CertUrl = "\"auth_provider_x509_cert_url\": \"" + authproviderX509CertUrl + "\",";
		String dataCientX509CertUrl = "\"client_x509_cert_url\": \"" + clientX509CertUrl + "\" ";
		String dataEnd = "}";
		
		//Creates an output stream
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GoogleCredentials credentials;

		// Writes data to the output stream
		out.write(data.getBytes());
		out.write(dataType.getBytes());
		out.write(dataProjectId.getBytes());
		out.write(dataPrivateKeyId.getBytes());
		out.write(dataPrivateKey.getBytes());
		out.write(dataClientEmail.getBytes());
		out.write(dataClientId.getBytes());
		out.write(dataAuthUri.getBytes());
		out.write(dataTokenUri.getBytes());
		out.write(dataAuthProviderX509CertUrl.getBytes());
		out.write(dataCientX509CertUrl.getBytes());
		out.write(dataEnd.getBytes());

	    //Get credentials from JSON in out stream
	    try (ByteArrayInputStream inStream = new ByteArrayInputStream( out.toByteArray() )){
	        	  credentials = ServiceAccountCredentials.fromStream(inStream);
	    }
	          
	    out.close();
		//***************************************
		
		//Get client that will be used to send requests from file credentials
		BigQuery bigQuery = BigQueryOptions.newBuilder()
						.setCredentials(credentials).setProjectId(project_id).build().getService();
		
		//Convert clients and products to BigQuery parameters
		QueryParameterValue clientList = QueryParameterValue.array(clients, String.class);
		QueryParameterValue productList = QueryParameterValue.array(products, String.class);
		
		//Create the query job for point balance
		QueryJobConfiguration queryConfig = QueryJobConfiguration.newBuilder(pointBalanceQuery)
										.setUseLegacySql(false)
										.addNamedParameter("clientsParam", clientList)
										.addNamedParameter("productsParam", productList)
										.build();
		
		//Create job ID so we can safely retry
		JobId jobId = JobId.of(UUID.randomUUID().toString()); 
		Job queryJob = bigQuery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
		 
		//Wait for the job to complete 
		queryJob = queryJob.waitFor();
		
		if(queryJob == null) { 
			throw new RuntimeException("Job no longer exists"); 
		}
		else if(queryJob.getStatus().getError() != null) { 
			throw new RuntimeException(queryJob.getStatus().getExecutionErrors().toString()); 
		}
		
		//Get results from query
		TableResult result =  queryJob.getQueryResults(); 
		
		return result;
	}
	
	public Map<String, String> processResults(TableResult result) {
		Map<String, String> pointBalanceMap = new HashMap<String, String>();
		
		for(FieldValueList row : result.iterateAll()) {
			//Getting Participant_nbr and point balance from results, adding to map
			pointBalanceMap.put(row.get(PARTICIPANT_NBR_FIELD).getStringValue(), row.get(POINT_BALANCE_FIELD).getStringValue());
		}
        
		if(pointBalanceMap.isEmpty()) throw new RuntimeException("No results were found in this query");
		
		return pointBalanceMap;
	}
}
