package com.maritz.culturenext.weeklydigest.dao;

import java.util.List;
import java.util.Map;

import com.google.cloud.bigquery.TableResult;

public interface BigQueryPointBalanceDao {
	
	Map<String, String> getPointBalances(String environment, List<String> productList, List<String> subclientList) throws Exception;

	TableResult runQuery(String environment, String[] products, String[] clients) throws Exception;
}
