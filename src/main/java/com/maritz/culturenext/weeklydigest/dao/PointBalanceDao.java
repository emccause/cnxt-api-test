package com.maritz.culturenext.weeklydigest.dao;

import java.util.List;
import java.util.Map;

public interface PointBalanceDao {

    List<Map<String, Object>> getPointBalances(List<String> productList, List<String> subclientList);

}