package com.maritz.culturenext.search.service;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.ParticipantSearchResultDTO;

public interface SearchService {

    /**
     * Returns a page of pax data that match the provided params.
     *
     * @param requestDetails - object for PaginatedResponseObject containing details of request
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param searchString - Text string to search for.  Either name will contain or control num will match.
     * @param commaSeparatedStatusList - Filter results to be in this list
     * @param loggedInPaxId - id of pax logged in (used for excludeSelf)
     * @param excludeSelf - whether to exclude the logged in pax from the results or not
     * @param commaSeparatedGroupIdList - list of group ids.  If not null, all pax will be in at least one group
     * @param programId - if pax can receive from this program, ELIGIBLE_RECEIVER_IN_PROGRAM will be true (1)
     *
     * @return page of data
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847563/Participant+Search+Example
     */
    PaginatedResponseObject<EntityDTO> paxSearch(PaginationRequestDetails requestDetails,
            Integer pageNumber, Integer pageSize, String searchString,
            String commaSeparatedStatusList, Long loggedInPaxId, Boolean excludeSelf,
            String commaSeparatedGroupIdList, Integer programId);

    /**
     * Returns a page of pax data that match the provided params.
     *
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param firstName - Text string to search for firstName
     * @param lastName - Text string to search for lastName
     * @param controlNumber - Text string to search for controlNumber
     * @param username - Text string to search for sysUserName
     * @param managerControlNumber - Text string to search for managerControlNumber
     * @param emailAddress - Text string to search for emailAddress
     *
     * @return page of data
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847563/Participant+Search+Example
     */
    PaginatedResponseObject<ParticipantSearchResultDTO> participantSearch(PaginationRequestDetails requestDetails, Integer pageNumber,
                                                                       Integer pageSize, String firstName, String lastName,
                                                                       String controlNumber, String username, String managerControlNumber,
                                                                       String emailAddress);

    /**
     * Returns a page of group data that match the provided params.
     *
     * @param requestDetails - object for PaginatedResponseObject containing details of request
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param searchString - Text string
     * @param groupConfigId - Limit scope of search - results will be in this group config type
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847557/Groups+Search+Example
     */
    PaginatedResponseObject<EntityDTO> groupSearch(PaginationRequestDetails requestDetails,
            Integer pageNumber, Integer pageSize, String searchString, Long groupConfigId);

    /**
     * Returns a page of pax or group data that match the provided params
     *
     * @param requestDetails - object for PaginatedResponseObject containing details of request
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @param controlNumber - Text string to search for.  Either name will contain (group or pax) or control num will match (pax only).
     * @param commaSeparatedStatusList - Filter results to be in this list
     * @param loggedInPaxId - id of pax logged in (used for excludeSelf)
     * @param excludeSelf - whether to exclude the logged in pax from the results or not
     * @param commaSeparatedVisibilityList - Filter group results to be in this list
     * @param commaSeparatedGroupTypeList - List of group types to filter group results
     *
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847663/Combined+Search+ver+2
     */
    PaginatedResponseObject<EntityDTO> combinedSearch(PaginationRequestDetails requestDetails,
            Integer pageNumber, Integer pageSize, String controlNumber,
            String commaSeparatedStatusList, Long loggedInPaxId, Boolean excludeSelf,
            String commaSeparatedVisibilityList, String commaSeparatedGroupTypeList
            , Integer budgetId, Integer programId
            );


}
