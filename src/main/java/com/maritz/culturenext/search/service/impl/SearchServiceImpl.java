package com.maritz.culturenext.search.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.ParticipantSearchResultDTO;
import org.springframework.stereotype.Component;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.search.constants.SearchConstants;
import com.maritz.culturenext.search.dao.SearchDao;
import com.maritz.culturenext.search.service.SearchService;

@Component
public class SearchServiceImpl implements SearchService {

    @Inject private SearchDao searchDao;

    @Override
    public PaginatedResponseObject<EntityDTO> paxSearch(PaginationRequestDetails requestDetails, Integer pageNumber,
            Integer pageSize, String searchString, String commaSeparatedStatusList, Long loggedInPaxId,
            Boolean excludeSelf, String commaSeparatedGroupIdList, Integer programId) {

        return handleSearchResults(requestDetails, searchDao.paxSearch(pageNumber, pageSize, searchString,
                commaSeparatedStatusList, loggedInPaxId, excludeSelf, commaSeparatedGroupIdList, programId));
    }

    @Override
    public PaginatedResponseObject<ParticipantSearchResultDTO> participantSearch(PaginationRequestDetails requestDetails, Integer pageNumber,
                                                                              Integer pageSize, String firstName, String lastName, String controlNumber, String username,
                                                                              String managerControlNumber, String emailAddress) {

        return handlePaxSearchAdminResults(requestDetails, searchDao.participantSearch(pageNumber, pageSize, firstName, lastName,
                controlNumber, username, managerControlNumber, emailAddress));
    }


    @Override
    public PaginatedResponseObject<EntityDTO> groupSearch(PaginationRequestDetails requestDetails, Integer pageNumber,
            Integer pageSize, String searchString, Long groupConfigId) {

        return handleSearchResults(requestDetails, searchDao.groupSearch(pageNumber, pageSize, searchString, groupConfigId));
    }

    @Override
    public PaginatedResponseObject<EntityDTO> combinedSearch(PaginationRequestDetails requestDetails,
            Integer pageNumber, Integer pageSize, String searchString, String commaSeparatedStatusList,
            Long loggedInPaxId, Boolean excludeSelf, String commaSeparatedVisibilityList,
            String commaSeparatedGroupTypeList
            , Integer budgetId, Integer programId
    		) {

        if (searchString == null || searchString.length() < SearchConstants.COMBINED_SEARCH_MINIMUM_STRING_LENGTH) {
            throw new ErrorMessageException().addErrorMessage(SearchConstants.SEARCH_STRING_TOO_SHORT_ERROR);
        }

        return handleSearchResults(requestDetails, searchDao.combinedSearch(pageNumber, pageSize, searchString,
                commaSeparatedStatusList, loggedInPaxId, excludeSelf, commaSeparatedVisibilityList,
                commaSeparatedGroupTypeList
                , budgetId, programId
        		));
    }

    /**
     * Converts group / pax records into paginated response.
     *
     * @param requestDetails request details from REST layer
     * @param searchResultList search results from DAO layer
     *
     * @return PaginatedResponseObject containing object version of data map.
     */
    private PaginatedResponseObject<EntityDTO> handleSearchResults(PaginationRequestDetails requestDetails,
            List<Map<String, Object>> searchResultList) {

        List<EntityDTO> resultsList = new ArrayList<>();
        Integer totalResults = 0;

        for (Map<String, Object> searchResult : searchResultList) {
            totalResults = (Integer) searchResult.get(ProjectConstants.TOTAL_RESULTS_KEY);
            String searchType = (String) searchResult.get(ProjectConstants.SEARCH_TYPE);
            if (ProjectConstants.SEARCH_DATA_PAX_TYPE.equalsIgnoreCase(searchType)) {
                resultsList.add(new EmployeeDTO(searchResult, null));
            }
            else {
                resultsList.add(new GroupDTO(searchResult));
            }
        }

        return new PaginatedResponseObject<>(requestDetails, resultsList, totalResults);
    }

    /**
     * Converts group / pax records into paginated response.
     *
     * @param requestDetails request details from REST layer
     * @param searchResultList search results from DAO layer
     *
     * @return PaginatedResponseObject containing object version of data map.
     */
    private PaginatedResponseObject<ParticipantSearchResultDTO> handlePaxSearchAdminResults(PaginationRequestDetails requestDetails,
            List<Map<String, Object>> searchResultList) {

        List<ParticipantSearchResultDTO> resultsList = new ArrayList<>();
        Integer totalResults = 0;

        for (Map<String, Object> searchResult : searchResultList) {
            totalResults = (Integer) searchResult.get(ProjectConstants.TOTAL_RESULTS_KEY);
            resultsList.add((new ParticipantSearchResultDTO(searchResult)));
        }

        return new PaginatedResponseObject<>(requestDetails, resultsList, totalResults);
    }



}
