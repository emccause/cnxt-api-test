package com.maritz.culturenext.search.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.ParticipantSearchResultDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.search.service.SearchService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
public class SearchRestService {
    
    @Inject private SearchService searchService;
    @Inject private Security security;

    private static final String PARTICIPANTS_SEARCH_PATH = ProjectConstants.PARTICIPANTS_URI_BASE;
    private static final String GROUPS_SEARCH_PATH = "groups";
    private static final String COMBINED_SEARCH_PATH = "search";

    // rest/participants
    @GetMapping(value = PARTICIPANTS_SEARCH_PATH, headers = FUTURE_VERSION_HEADER, params = SEARCH_STRING_REST_PARAM)
    @ApiOperation(value="Fetch participants with the specified search criteria query-param.", 
        notes ="Also narrow down the search further by the filter cirteria - groups specified and "
                + "the statuses specified. The parameters for page and size are optional. If those fields are "
                + "left blank a default of page 1 and page-size 50 will be used. It will return the first "
                + "50 participants and alphabetic sort will be used.<br><br>Filter Criteria for groups & "
                + "status will support the 'AND' & the 'OR' logic. i.e for Multiple criteria within "
                + "category use OR criteria; for Multiple criteria between categories use AND criteria.<br><br>"
                + "ex. Return users where users belong to groups: (City-Chicago OR City-New York) AND "
                + "(Cost-Center-1 OR Cost-Center-2 OR Cost-Center-3) AND Status = (Active OR Locked) <br><br>"
                + "the API will determine the enrollment field(GroupConfigId) for the groupIds and "
                + "group them for AND & OR logic")
    @ApiResponses(value = { @ApiResponse(code = 200, 
        message = "successfully returned the list of participants.", response = EmployeeDTO.class)})
    @Permission("PUBLIC")
    public PaginatedResponseObject<EntityDTO> paxSearch(
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
            @ApiParam(value="The number of records per page.",required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
            @ApiParam(value="Basic string of what you are searching for") 
                @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchString,
            @ApiParam(value="Status of the group ex. ACTIVE,LOCKED",required = false) 
                @RequestParam(value = STATUS_REST_PARAM, required = false) String commaSeparatedStatusList,
            @ApiParam(value="If excludeSelf is true the logged in user is filtered out", required = false) 
                @RequestParam(value = EXCLUDE_SELF_REST_PARAM, required = false) Boolean excludeSelf,
            @ApiParam(value="Id's of the groups you want to search for ex 2,3,4,8,9",required = false) 
                @RequestParam(value = GROUP_IDS_REST_PARAM, required = false) String commaSeparatedGroupIdList,
            @ApiParam(value="Program Id",required = false) 
                @RequestParam(value = PROGRAM_ID_REST_PARAM, required = false) Integer programId
            ) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(SEARCH_STRING_REST_PARAM, searchString);
        requestParamMap.put(GROUP_IDS_REST_PARAM, commaSeparatedGroupIdList);
        requestParamMap.put(STATUS_REST_PARAM, commaSeparatedStatusList);
        requestParamMap.put(PROGRAM_ID_REST_PARAM, programId);
        requestParamMap.put(EXCLUDE_SELF_REST_PARAM, excludeSelf);

        PaginationRequestDetails requestDetails = new PaginationRequestDetails().setRequestPath(PARTICIPANTS_SEARCH_PATH)
                                                        .setRequestParamMap(requestParamMap).setPageNumber(pageNumber)
                                                        .setPageSize(pageSize);
        
        return searchService.paxSearch(requestDetails, pageNumber, pageSize, searchString, commaSeparatedStatusList,
                                        security.getPaxId(), excludeSelf, commaSeparatedGroupIdList, programId);
        
    }

    //rest/groups
    @GetMapping(value = GROUPS_SEARCH_PATH, headers = {FUTURE_VERSION_HEADER})
    @ApiOperation(value = "Get a list of groups by search criteria")
    @Permission("PUBLIC")
    public PaginatedResponseObject<EntityDTO> getGroupSearchResultsPag(
            @ApiParam(value = "String of contents you are searching for") 
            @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchString,
            @ApiParam(value = "group sub-type id") 
            @RequestParam(value = GROUP_CONFIG_ID_REST_PARAM, required = false) Long groupConfigId,
            @ApiParam(value = "Page of notification used to paginate the request",required = false) 
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value = "The number of records per page.", required = false) 
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE;
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE;
            }
        }

        Map<String, Object> pathParamMap = new HashMap<>();
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(SEARCH_STRING_REST_PARAM, searchString);
        requestParamMap.put(GROUP_CONFIG_ID_REST_PARAM, groupConfigId);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails()
                .setRequestPath(GROUPS_SEARCH_PATH)
                .setPathParamMap(pathParamMap)
                .setRequestParamMap(requestParamMap)
                .setPageNumber(pageNumber)
                .setPageSize(pageSize);

        return searchService.groupSearch(requestDetails, pageNumber, pageSize, searchString, groupConfigId);
    }

    //rest/search
    @GetMapping(value = COMBINED_SEARCH_PATH, headers = {FUTURE_VERSION_HEADER})
    @ApiOperation(value="Returns a list of participants or groups based on the searchStr value.", 
    notes ="If results are not found an empty array will be sent. The parameters for page and size are optional, "
            + "but can be used to find additional results. If those fields are left blank a default of page 1 and "
            + "size 50 will be used.<br><br>"
            +" New object model with separate objects for participants and groups is returned. "
            + "A heterogenous list (list with group objects & pax objects is returned), "
            + "If the record contains a paxId it is a person. If the record contains a groupId it will be a group. "
            + "If they have a groupId and groupPaxId it is a personal-group. "
            + "The groupPaxId should always be the paxId of the user who initiated the search. "
            + "The new Pax object will contain 2 new properties status & jobTitle.<br><br>"
            +" Group 'type' values are - 'ENROLLMENT', 'CUSTOM', 'ADMIN','ROLE_BASED', 'PERSONAL'<br><br>"
            +" status' values are 'ACTIVE', 'INACTIVE','LOCKED','NEW','SUSPENDED','RESTRICTED','RESET'<br><br>"
            +" Group 'visibility' values are 'PUBLIC', 'ADMIN'<br><br>"
            +" 'excludeSelf' param support doc added 6/22/2015 : In order to prevent users from giving recognition to "
            + "themselves, we need to exclude them in the combined search results. it's boolean value with 'true' to "
            + "indicate exclusion of self from results. Default would be that self is included (excludeSelf=false).")
    @Permission("PUBLIC")
    public PaginatedResponseObject<EntityDTO> getSearchResults(
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
            @ApiParam(value="The number of records per page.",required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
            @ApiParam(value="Basic string of what you are searching for") 
            @RequestParam(value = SEARCH_STRING_REST_PARAM, required = true) String searchString,
            @ApiParam(value="Status of the group ex. ACTIVE,LOCKED",required = false) 
            @RequestParam(value = STATUS_REST_PARAM, required = false) String commaSeparatedStatusList,
            @ApiParam(value="If excludeSelf is true the logged in user is filtered out", required = false) 
            @RequestParam(value = EXCLUDE_SELF_REST_PARAM, required = false) Boolean excludeSelf,
            @ApiParam(value="values are 'PUBLIC', 'ADMIN'", required = false) 
            @RequestParam(value = VISIBILITY_REST_PARAM, required = false) String commaSeparatedVisibilityList,
            @ApiParam(value="values are - 'ENROLLMENT', 'CUSTOM', 'ADMIN','ROLE_BASED', 'PERSONAL'", required = false)
            @RequestParam(value = GROUP_TYPE_REST_PARAM, required = false) String commaSeparatedGroupTypeList,
            @ApiParam(value = "Id of the budget id that you want the details of.") 			        
        	@RequestParam(value = BUDGET_ID_REST_PARAM,required = false) Integer budgetId,
    		@ApiParam(value = "Id of the program id that you want the details of.") 			        
    	 	@RequestParam(value = PROGRAM_ID_REST_PARAM,required = false) Integer programId) {
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        if (excludeSelf == null) {
            excludeSelf = false;
        }

        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(SEARCH_STRING_REST_PARAM, searchString);
        requestParamMap.put(STATUS_REST_PARAM, commaSeparatedStatusList);
        requestParamMap.put(EXCLUDE_SELF_REST_PARAM, excludeSelf);
        requestParamMap.put(VISIBILITY_REST_PARAM, commaSeparatedVisibilityList);
        requestParamMap.put(GROUP_TYPE_REST_PARAM, commaSeparatedGroupTypeList);
        requestParamMap.put(BUDGET_ID_REST_PARAM, budgetId);
        requestParamMap.put(PROGRAM_ID_REST_PARAM, programId);

        PaginationRequestDetails requestDetails = new PaginationRequestDetails().setRequestPath(COMBINED_SEARCH_PATH)
                .setRequestParamMap(requestParamMap).setPageNumber(pageNumber)
                .setPageSize(pageSize);

        return searchService.combinedSearch(requestDetails, pageNumber, pageSize, searchString,
                commaSeparatedStatusList, security.getPaxId(), excludeSelf, commaSeparatedVisibilityList,
                commaSeparatedGroupTypeList
                , budgetId, programId
        		);             
    }

    //rest/participants
    //@PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @GetMapping(value = PARTICIPANTS_SEARCH_PATH, params = "!" + SEARCH_STRING_REST_PARAM)
    @Permission("PUBLIC")
    @ApiOperation(value="Fetch participants with the specified search criteria query-param.",
            notes ="Will fill out")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully returned the list of participants.", response = EmployeeDTO.class)})
    public PaginatedResponseObject<ParticipantSearchResultDTO> participantSearch(
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
            @ApiParam(value="The number of records per page.",required = false)
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = FIRST_NAME_PARAM, required = false) String firstName,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = LAST_NAME_PARAM, required = false) String lastName,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = CONTROL_NUMBER_PARAM, required = false) String controlNumber,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = USER_NAME_PARAM, required = false) String username,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = MGR_CONTROL_NUMBER_PARAM, required = false) String managerControlNumber,
            @ApiParam(value="Basic string of what you are searching for")
            @RequestParam(value = EMAIL_ADDRESS_PARAM, required = false) String emailAddress
    ) {

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }

        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }

        if (firstName == null && lastName == null && controlNumber == null && username == null
                && managerControlNumber == null && emailAddress == null) {
            throw new InvalidParameterException("At least one parameter must be populated. i.e firstName," +
                    "lastName, controlNumber, username, managerControlNumber, or emailAddress.");
        }

        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(PAGE_NUMBER_REST_PARAM, pageNumber);
        requestParamMap.put(PAGE_SIZE_REST_PARAM, pageSize);
        requestParamMap.put(FIRST_NAME_PARAM, firstName);
        requestParamMap.put(LAST_NAME_PARAM, lastName);
        requestParamMap.put(CONTROL_NUMBER_PARAM, controlNumber);
        requestParamMap.put(USER_NAME_PARAM, username);
        requestParamMap.put(MGR_CONTROL_NUMBER_PARAM, managerControlNumber);
        requestParamMap.put(EMAIL_ADDRESS_PARAM, emailAddress);

        PaginationRequestDetails requestDetails = new PaginationRequestDetails().setRequestPath(PARTICIPANTS_SEARCH_PATH)
                .setRequestParamMap(requestParamMap).setPageNumber(pageNumber)
                .setPageSize(pageSize);

        return searchService.participantSearch(requestDetails, pageNumber, pageSize, firstName, lastName, controlNumber,
                username, managerControlNumber, emailAddress);

    }
}
