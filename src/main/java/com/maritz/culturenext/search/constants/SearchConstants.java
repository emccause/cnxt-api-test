package com.maritz.culturenext.search.constants;

import com.maritz.core.rest.ErrorMessage;

public class SearchConstants {
    
    public static final Integer COMBINED_SEARCH_MINIMUM_STRING_LENGTH = 2;
    public static final String SEARCH_FIELD = "search";
    public static final String SEARCH_STRING_TOO_SHORT_CODE = "SEARCH_STRING_TOO_SHORT";
    public static final String SEARCH_STRING_TOO_SHORT_MESSAGE = "Must provide at least 2 characters.";

    //PaxSearchAdminDTO field mapping names
    public static final String CONTROL_NUMBER = "controlNumber";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USER_NAME = "userName";
    public static final String STATUS = "status";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String MANAGER_CONTROL_NUMBER = "managerControlNumber";
    public static final String JOB_TITLE = "jobTitle";
    public static final String STATE = "state";
    public static final String COUNTRY = "country";

    //EmailHistorySearchDTO field mapping names
    public static final String SEND_DATE = "sendDate";
    public static final String SENT_DATE = "sentDate";
    public static final String SUBJECT = "subject";
    public static final String EVENT_TYPE_DESC = "eventTypeDesc";
    public static final String STATUS_TYPE_CODE = "statusTypeCode";
    public static final String SEND_ATTEMPTS = "sendAttempts";
    public static final String ERROR = "error";

    
    public static final ErrorMessage SEARCH_STRING_TOO_SHORT_ERROR = new ErrorMessage()
            .setField(SEARCH_FIELD)
            .setCode(SEARCH_STRING_TOO_SHORT_CODE)
            .setMessage(SEARCH_STRING_TOO_SHORT_MESSAGE);
    
}
