package com.maritz.culturenext.search.dao.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.culturenext.jpa.entity.VwUserGroupsSearchData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.search.dao.SearchDao;

import javax.inject.Inject;

@Component
public class SearchDaoImpl extends PaginatingDaoImpl implements SearchDao {
    private static Logger log = LoggerFactory.getLogger(SearchDaoImpl.class);
    @Inject
    private ConcentrixDao<VwUserGroupsSearchData> vwUserGroupsSearchDataDao;

    private static final String SEARCH_STRING_SQL_PARAM = "SEARCH_STRING";
    private static final String SEARCH_STRING_RAW_SQL_PARAM = "SEARCH_STRING_RAW";
    private static final String SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM = "SEARCH_STRING_EMAIL_ADDRESS_RAW";
    private static final String STATUS_LIST_SQL_PARAM = "STATUS_LIST";
    private static final String LOGGED_IN_PAX_ID_SQL_PARAM = "LOGGED_IN_PAX_ID";
    private static final String EXCLUDE_SELF_SQL_PARAM = "EXCLUDE_SELF";
    private static final String GROUP_ID_LIST_SQL_PARAM = "GROUP_ID_LIST";
    private static final String PROGRAM_ID_SQL_PARAM = "PROGRAM_ID";
    
    private static final String GROUP_CONFIG_ID_PARAM = "GROUP_CONFIG_ID";
    private static final String PAGE_NUMBER_PARAM = "PAGE_NUMBER";
    private static final String PAGE_SIZE_PARAM = "PAGE_SIZE";
    private static final String VISIBILITY_LIST_SQL_PARAM = "VISIBILITY_LIST";
    private static final String GROUP_TYPE_LIST_SQL_PARAM = "GROUP_TYPE_LIST";
    
    private static final String DEFAULT_VISIBILITY_LIST = "PUBLIC,ADMIN";
    private static final String BUDGET_ID_SQL_PARAM = "BUDGET_ID";

    public static final String FIRST_NAME_PARAM = "firstName";
    public static final String LAST_NAME_PARAM = "lastName";
    public static final String CONTROL_NUMBER_PARAM = "controlNumber";
    public static final String USER_NAME_PARAM = "username";
    public static final String MGR_CONTROL_NUMBER_PARAM = "managerControlNumber";
    public static final String EMAIL_ADDRESS_PARAM = "emailAddress";
    
    private static final String PAX_SEARCH_QUERY = "SELECT " +
            "UGSD.*, " +
            "PROGRAM_DATA.ELIGIBLE_RECEIVER_IN_PROGRAM " +
        "FROM component.VW_USER_GROUPS_SEARCH_DATA UGSD " +
        "LEFT JOIN component.UF_LIST_TO_TABLE(:" + STATUS_LIST_SQL_PARAM + ", ',') STATUS_LIST " +
            "ON UGSD.STATUS_TYPE_CODE = STATUS_LIST.items " +
        "LEFT JOIN ( " +
            "SELECT DISTINCT " +
                "GTM.PAX_ID " +
            "FROM component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
            "LEFT JOIN component.UF_LIST_TO_TABLE_ID(:" + GROUP_ID_LIST_SQL_PARAM + ", ',') GROUP_LIST " +
                "ON GTM.group_id = GROUP_LIST.items " +
            "WHERE :" + GROUP_ID_LIST_SQL_PARAM + " IS NULL " +
                "OR GROUP_LIST.items IS NOT NULL " +
        ") GROUP_PAX " +
            "ON UGSD.PAX_ID = GROUP_PAX.pax_id " +
        "LEFT JOIN ( " +
            "SELECT DISTINCT " +
                "PAX.PAX_ID, " +
                "1 AS ELIGIBLE_RECEIVER_IN_PROGRAM  " + // If this is populated then they can receive from the program provided
            "FROM component.ROLE " +
            "LEFT JOIN component.ACL " +
                "ON ROLE.ROLE_ID = ACL.ROLE_ID " +
                "AND ACL.TARGET_TABLE = 'PROGRAM' " +
            "LEFT JOIN component.VW_GROUP_TOTAL_MEMBERSHIP GTM " +
                "ON ACL.SUBJECT_ID = GTM.group_id " +
                "AND ACL.SUBJECT_TABLE = 'GROUPS' " +
            "LEFT JOIN component.PAX " +
                "ON ( " +
                    "GTM.pax_id = PAX.PAX_ID " +
                    "OR ( " +
                        "ACL.SUBJECT_ID = PAX.PAX_ID " +
                        "AND ACL.SUBJECT_TABLE = 'PAX' " +
                    ") " +
                ") " +
            "WHERE ROLE.ROLE_CODE = 'PREC' " +
                "AND ACL.TARGET_ID = :" + PROGRAM_ID_SQL_PARAM + " " +
        ") PROGRAM_DATA " +
            "ON UGSD.PAX_ID = PROGRAM_DATA.PAX_ID " +
        "WHERE UGSD.SEARCH_TYPE = 'P' " +
            "AND ( " +
                "UGSD.NAME COLLATE Latin1_general_CI_AI LIKE :" + SEARCH_STRING_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
                "OR UGSD.CONTROL_NUM COLLATE Latin1_general_CI_AI = :" + SEARCH_STRING_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
                "OR UGSD.EMAIL_ADDRESS = :" + SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM + " COLLATE Latin1_general_CI_AI " +
            ") " +
            "AND (:" + STATUS_LIST_SQL_PARAM + " IS NULL OR STATUS_LIST.items IS NOT NULL) " +
            "AND (:" + GROUP_ID_LIST_SQL_PARAM + " IS NULL OR GROUP_PAX.pax_id IS NOT NULL) " +
            "AND (:" + EXCLUDE_SELF_SQL_PARAM + " = 0 OR UGSD.PAX_ID <> :" + LOGGED_IN_PAX_ID_SQL_PARAM + ")";
    private static final String PAX_SEARCH_ORDER_BY = "UGSD.NAME ASC";
    
    private static final String GROUP_SEARCH_QUERY = 
            "SELECT * FROM ( " +
            "SELECT DISTINCT " +
            "G.GROUP_ID AS GROUP_ID, " +
            "GC.ID AS GROUP_CONFIG_ID, " +
            "GC.GROUP_CONFIG_NAME AS FIELD, " +
            "GC.GROUP_CONFIG_DESC AS FIELD_DISPLAY_NAME, " +
            "G.GROUP_DESC AS GROUP_NAME, " +
            "CASE " +
                "WHEN GC.GROUP_CONFIG_DESC IS NOT NULL THEN CONCAT(GC.GROUP_CONFIG_DESC, ' - ', G.GROUP_DESC) " +
                "ELSE G.GROUP_DESC " +
            "END AS DISPLAY_NAME, " +
            "GT.GROUP_TYPE_DESC AS TYPE, " +
            "GV.VISIBILITY AS VISIBILITY, " +
            "G.STATUS_TYPE_CODE AS STATUS, " +
            "G.CREATE_DATE AS CREATE_DATE, " +
            "(SELECT COUNT(*) FROM component.GROUPS_PAX WHERE GROUP_ID = G.GROUP_ID) AS PAX_COUNT, " +
            "(SELECT COUNT(*) FROM component.VW_GROUPS_GROUPS WHERE GROUP_ID_1 = G.GROUP_ID) AS GROUP_COUNT, " +
            "(SELECT COUNT(*) FROM component.VW_GROUP_TOTAL_MEMBERSHIP WHERE GROUP_ID = G.GROUP_ID) AS TOTAL_PAX_COUNT, " +
            "G.PAX_ID AS GROUP_PAX_ID " +
            "FROM COMPONENT.GROUPS G " +
            "LEFT JOIN component.GROUP_CONFIG GC " +
            "ON G.GROUP_CONFIG_ID = GC.ID " +
            "INNER JOIN component.GROUP_TYPE GT " +
            "ON GT.GROUP_TYPE_CODE = G.GROUP_TYPE_CODE " +
            "LEFT JOIN component.VW_GROUPS_VISIBILITY GV " +
            "ON G.GROUP_ID = GV.GROUP_ID " +
            "WHERE G.PAX_ID IS NULL  " +
            "AND GT.GROUP_TYPE_DESC NOT IN ('HIERARCHY') " +
            "AND G.GROUP_DESC COLLATE Latin1_general_CI_AI LIKE :" + SEARCH_STRING_SQL_PARAM + " COLLATE Latin1_general_CI_AI " + 
            "AND ( :" + GROUP_CONFIG_ID_PARAM + " IS NULL OR  G.GROUP_CONFIG_ID = :" + GROUP_CONFIG_ID_PARAM + " ) " +
            ") AS SEARCH_QUERY ";
    
    private static final String GROUP_SEARCH_ORDER_BY = "DISPLAY_NAME ASC";
    
    private static final String COMBINED_SEARCH_QUERY = "exec component.UP_COMBINED_SEARCH_QUERY" +
            " :" + SEARCH_STRING_SQL_PARAM +
            ", :" + SEARCH_STRING_RAW_SQL_PARAM +
            ", :" + SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM +
            ", :" + VISIBILITY_LIST_SQL_PARAM +
            ", :" + LOGGED_IN_PAX_ID_SQL_PARAM +
            ", :" + STATUS_LIST_SQL_PARAM +
            ", :" + GROUP_TYPE_LIST_SQL_PARAM +
            ", :" + EXCLUDE_SELF_SQL_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM;

    private static final String COMBINED_SEARCHALLOCATED_QUERY = "exec component.UP_COMBINED_SEARCHALLOCATED_QUERY " +
            " :" + SEARCH_STRING_SQL_PARAM +
            ", :" + SEARCH_STRING_RAW_SQL_PARAM +
            ", :" + SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM +
            ", :" + VISIBILITY_LIST_SQL_PARAM +
            ", :" + LOGGED_IN_PAX_ID_SQL_PARAM +
            ", :" + STATUS_LIST_SQL_PARAM +
            ", :" + GROUP_TYPE_LIST_SQL_PARAM +
            ", :" + EXCLUDE_SELF_SQL_PARAM +
            ", :" + BUDGET_ID_SQL_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM;

    private static final String COMBINED_SEARCHALLOCATEDPROGRAM_QUERY = "exec component.UP_COMBINED_SEARCHALLOCATEDPROGRAM_QUERY" +
            " :" + SEARCH_STRING_SQL_PARAM +
            ", :" + SEARCH_STRING_RAW_SQL_PARAM +
            ", :" + SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM +
            ", :" + VISIBILITY_LIST_SQL_PARAM +
            ", :" + LOGGED_IN_PAX_ID_SQL_PARAM +
            ", :" + STATUS_LIST_SQL_PARAM +
            ", :" + GROUP_TYPE_LIST_SQL_PARAM +
            ", :" + EXCLUDE_SELF_SQL_PARAM +
            ", :" + PROGRAM_ID_SQL_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM;

    private static final String PAX_SEARCH_ADMIN_QUERY = "exec component.UP_PAX_SEARCH_ADMIN_QUERY" +
            " :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + FIRST_NAME_PARAM +
            ", :" + LAST_NAME_PARAM +
            ", :" + CONTROL_NUMBER_PARAM +
            ", :" + USER_NAME_PARAM +
            ", :" + MGR_CONTROL_NUMBER_PARAM +
            ", :" + EMAIL_ADDRESS_PARAM;
    
    @Override
    public List<Map<String, Object>> paxSearch(Integer pageNumber, Integer pageSize, String searchString,
            String commaSeparatedStatusList, Long loggedInPaxId, Boolean excludeSelf,
            String commaSeparatedGroupIdList, Integer programId) {
        
        if (searchString == null) {
            searchString = ProjectConstants.EMPTY_STRING; // String.format would search for %null% - we want %%
        }

        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SEARCH_STRING_SQL_PARAM, String.format(ProjectConstants.SEARCH_STRING_FORMAT, searchString));
        params.addValue(SEARCH_STRING_RAW_SQL_PARAM, searchString);
        params.addValue(SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM, searchString);
        params.addValue(STATUS_LIST_SQL_PARAM, commaSeparatedStatusList);
        params.addValue(LOGGED_IN_PAX_ID_SQL_PARAM, loggedInPaxId);
        params.addValue(EXCLUDE_SELF_SQL_PARAM, excludeSelf);
        params.addValue(GROUP_ID_LIST_SQL_PARAM, commaSeparatedGroupIdList);
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);
        
        return getPageWithTotalRows(PAX_SEARCH_QUERY, params, new CamelCaseMapRowMapper(), PAX_SEARCH_ORDER_BY, pageNumber, pageSize);
    }

    @Override
    public List<Map<String, Object>> groupSearch(Integer pageNumber, Integer pageSize, String searchString,
            Long groupConfigId) {
        
        if (searchString == null) {
            searchString = ProjectConstants.EMPTY_STRING; // String.format would search for %null% - we want %%
        }
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SEARCH_STRING_SQL_PARAM , String.format(ProjectConstants.SEARCH_STRING_FORMAT, searchString));
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(GROUP_CONFIG_ID_PARAM, groupConfigId);

        return getPageWithTotalRows(GROUP_SEARCH_QUERY, params, new CamelCaseMapRowMapper(), GROUP_SEARCH_ORDER_BY, pageNumber, pageSize);
    }

    @Override
    public List<Map<String, Object>> combinedSearch(Integer pageNumber, Integer pageSize, String searchString,
            String commaSeparatedStatusList, Long loggedInPaxId, Boolean excludeSelf,
            String commaSeparatedVisibilityList, String commaSeparatedGroupTypeList
            , Integer budgetId, Integer programId) {
        
        if (searchString == null) {
            searchString = ProjectConstants.EMPTY_STRING; // String.format would search for %null% - we want %%
        }
        
        if (commaSeparatedVisibilityList == null || commaSeparatedVisibilityList.isEmpty()) {
        	commaSeparatedVisibilityList = DEFAULT_VISIBILITY_LIST;
        }

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(SEARCH_STRING_SQL_PARAM, String.format(ProjectConstants.SEARCH_STRING_FORMAT, searchString));
        params.addValue(SEARCH_STRING_RAW_SQL_PARAM, searchString);
        params.addValue(SEARCH_STRING_EMAIL_ADDRESS_RAW_SQL_PARAM, searchString);
        params.addValue(STATUS_LIST_SQL_PARAM, commaSeparatedStatusList);
        params.addValue(LOGGED_IN_PAX_ID_SQL_PARAM, loggedInPaxId);
        params.addValue(EXCLUDE_SELF_SQL_PARAM, excludeSelf);
        params.addValue(VISIBILITY_LIST_SQL_PARAM, commaSeparatedVisibilityList);
        params.addValue(GROUP_TYPE_LIST_SQL_PARAM, commaSeparatedGroupTypeList);
        params.addValue(PROGRAM_ID_SQL_PARAM, programId);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);

        params.addValue(BUDGET_ID_SQL_PARAM, budgetId);
        if (log.isInfoEnabled()) {
            log.info("parameters:" + params.getValues().keySet().stream().map(key -> key + "=" + params.getValues().get(key))
                    .collect(Collectors.joining(",\n ", "{", "}")));
        }

        List<Map<String, Object>> result = null;
        String queryToExecute = null;
        if (budgetId == null && programId == null) {
            queryToExecute = COMBINED_SEARCH_QUERY;
        } else if (programId != null) {
            queryToExecute = COMBINED_SEARCHALLOCATEDPROGRAM_QUERY;
        } else {
            queryToExecute = COMBINED_SEARCHALLOCATED_QUERY;
        }
        if (log.isInfoEnabled()) {
            log.info("queryToExecute :" + queryToExecute);
        }
        final long startTime = System.nanoTime();
        result = namedParameterJdbcTemplate.query(queryToExecute, params, new CamelCaseMapRowMapper());
        final long endTime = System.nanoTime();
        if (log.isInfoEnabled()) {
            log.info("Total execution time:" + (endTime - startTime) / 1000000);
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> participantSearch(Integer pageNumber, Integer pageSize, String firstName, String lastName,
                                                    String controlNumber, String sysUserName, String managerControlNumber,
                                                    String emailAddress) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(FIRST_NAME_PARAM, firstName);
        params.addValue(LAST_NAME_PARAM, lastName);
        params.addValue(CONTROL_NUMBER_PARAM, controlNumber);
        params.addValue(USER_NAME_PARAM, sysUserName);
        params.addValue(MGR_CONTROL_NUMBER_PARAM, managerControlNumber);
        params.addValue(EMAIL_ADDRESS_PARAM, emailAddress);

        List<Map<String, Object>> result = null;
        String queryToExecute = PAX_SEARCH_ADMIN_QUERY;
        result = namedParameterJdbcTemplate.query(queryToExecute, params, new CamelCaseMapRowMapper());
        return result;

    }
}
