package com.maritz.culturenext.calendar.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;

@Component
public class CalendarTranslationUtil {

    @Inject private TranslationUtil translationUtil;

    /**
     * Translates names of the given list of calendar entry items
     * @param calendarEntryList - List of CalendarEntry entities
     * @param languageCode - Language code to translate to
     * @return CalendarEntry
     */
    public List<CalendarEventDTO> translateCalendarEntryNames(List<CalendarEventDTO> calendarEntryList, String languageCode) {
        Map<Long, CalendarEventDTO> calendarEntryMap = new HashMap<>();
        Map<Long, String> defaultNameMap = new HashMap<>();
        Map<Long, String> defaultDescMap = new HashMap<>();

        for (CalendarEventDTO ce : calendarEntryList) {
            calendarEntryMap.put(ce.getId(), ce);
            defaultNameMap.put(ce.getId(), ce.getName());
            defaultDescMap.put(ce.getId(), ce.getDescription());
        }

        Map<Long, String> translatedNameMap = translationUtil.getTranslationsForListOfIds(languageCode,
                TableName.CALENDAR_ENTRY.name(),
                TranslationConstants.COLUMN_NAME_CALENDAR_ENTRY_NAME,
                new ArrayList<>(calendarEntryMap.keySet()),
                defaultNameMap);

        Map<Long, String> translatedDescMap = translationUtil.getTranslationsForListOfIds(languageCode,
                TableName.CALENDAR_ENTRY.name(),
                TranslationConstants.COLUMN_NAME_CALENDAR_ENTRY_DESC,
                new ArrayList<>(calendarEntryMap.keySet()),
                defaultDescMap);

        // Combine all unique translated calendar ids
        Set<Long> translatedIds = new HashSet<>();
        translatedIds.addAll(translatedNameMap.keySet());
        translatedIds.addAll(translatedDescMap.keySet());

        for (Long translatedId : translatedIds) {
            String nameTranslation = translatedNameMap.get(translatedId);
            String descTranslation = translatedDescMap.get(translatedId);

            if (nameTranslation != null) {
                calendarEntryMap.get(translatedId).setName(nameTranslation);
            }
            if (descTranslation != null) {
                calendarEntryMap.get(translatedId).setDescription(descTranslation);
            }
        }

        //sort the translated list of events by startDate.
        Collection<CalendarEventDTO> collectionCalendarEventDTO = calendarEntryMap.values();
        List<CalendarEventDTO> sortedCalendarEntryList = new ArrayList<>(collectionCalendarEventDTO);
        Collections.sort(sortedCalendarEntryList, new CalendarStartDateCompareUtil());
        
        return sortedCalendarEntryList;
    }
}
