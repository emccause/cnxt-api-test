package com.maritz.culturenext.calendar.util;

import java.util.Comparator;

import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;

public class CalendarStartDateCompareUtil implements Comparator<CalendarEventDTO> {
    /**
     * Compare the start date from two CalendarEventDTO´s.
     * @param calendarEventDTO1
     * @param calendarEventDTO2
     * @return
     */
    @Override
    public int compare(CalendarEventDTO calendarEventDTO1, CalendarEventDTO calendarEventDTO2) {
        return calendarEventDTO1.getStartDate().compareTo(calendarEventDTO2.getStartDate());
    }
}
