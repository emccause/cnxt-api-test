package com.maritz.culturenext.calendar.config.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.CalendarSubType;
import com.maritz.core.jpa.entity.CalendarType;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.enums.CalendarEventType;
import com.maritz.culturenext.calendar.config.dto.CalendarConfigDTO;
import com.maritz.culturenext.calendar.config.services.CalendarConfigService;
import com.maritz.culturenext.calendar.constants.CalendarConstants;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class CalendarConfigServiceImpl implements CalendarConfigService {

    private CalendarEventType subTypes = CalendarEventType.SUB_TYPES;

    @Inject private ConcentrixDao<CalendarSubType> calendarSubTypeDao;
    @Inject private ConcentrixDao<CalendarType> calendarTypeDao;

    @Override
    public CalendarConfigDTO getConfigCalendarEventTypes() {

        CalendarConfigDTO calendarConfig = new CalendarConfigDTO();
        List<String> configs = new ArrayList<>();

        // Get calendar subtypes
        List<CalendarSubType> calendarSubTypeList = calendarSubTypeDao.findBy()
                .where(ProjectConstants.CALENDAR_SUB_TYPE_CODE).in(subTypes.getListOfCalendarEventSubTypes())
                .and(ProjectConstants.DISPLAY_STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();

        if (!CollectionUtils.isEmpty(calendarSubTypeList)) {
            for (CalendarSubType type : calendarSubTypeList) {
                configs.add(type.getCalendarSubTypeCode());
            }
        }

        CalendarType holidayCalendarType = calendarTypeDao.findBy()
                .where(ProjectConstants.CALENDAR_TYPE_CODE).eq(CalendarEventType.HOLIDAY.toString())
                .findOne();

        if (holidayCalendarType.getDisplayStatusTypeCode().equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
            configs.add(holidayCalendarType.getCalendarTypeCode());
        }

        calendarConfig.setEventTypes(configs);

        return calendarConfig;
    }

    @Override
    public CalendarConfigDTO setConfigCalendarEventTypes(CalendarConfigDTO calendarConfigDTO)
            throws ErrorMessageException {

        // Get Holiday calendar type
        CalendarType holidayCalendarType = calendarTypeDao.findBy()
                .where(ProjectConstants.CALENDAR_TYPE_CODE).eq(CalendarEventType.HOLIDAY.toString())
                .findOne();

        List<String> validTypes = new ArrayList<>();
        // Add Holiday type code to valid types
        validTypes.add(holidayCalendarType.getCalendarTypeCode());

        // Get list of subtypes
        List<CalendarSubType> calendarSubTypeList = calendarSubTypeDao.findBy()
                .where(ProjectConstants.CALENDAR_SUB_TYPE_CODE).in(subTypes.getListOfCalendarEventSubTypes())
                .or(ProjectConstants.CALENDAR_TYPE_ID).eq(holidayCalendarType.getId())
                .find();

        // Map subtypes to subtype codes
        List<CalendarSubType> holidaySubTypes = new ArrayList<>();
        Map<String, CalendarSubType> personalSubTypeMap = new HashMap<>();
        for (CalendarSubType calendarSubType : calendarSubTypeList) {

            if (calendarSubType.getCalendarTypeId().equals(holidayCalendarType.getId())) {
                holidaySubTypes.add(calendarSubType);
            } else {
                validTypes.add(calendarSubType.getCalendarSubTypeCode());
                personalSubTypeMap.put(calendarSubType.getCalendarSubTypeCode(), calendarSubType);
            }
        }

        // Validate types
        if (calendarConfigDTO.getEventTypes() == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.EVENT_TYPE,
                    CalendarConstants.ERROR_MISSING_EVENT_TYPES_CODE, 
                    CalendarConstants.ERROR_MISSING_EVENT_TYPES_MSG);
        } else {
            for (String type : calendarConfigDTO.getEventTypes()) {
                if (!validTypes.contains(type)) {
                    throw new ErrorMessageException().addErrorMessage(ProjectConstants.EVENT_TYPE,
                            CalendarConstants.ERROR_INVALID_EVENT_TYPE_CODE,
                            CalendarConstants.ERROR_INVALID_EVENT_TYPE_MSG);
                }
            }
        }

        // Set Service_Anniversary and Birthday status
        if (calendarConfigDTO.getEventTypes().contains(CalendarEventType.BIRTHDAY.toString())) {
            personalSubTypeMap.get(CalendarEventType.BIRTHDAY.toString())
                    .setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        } else {
            personalSubTypeMap.get(CalendarEventType.BIRTHDAY.toString())
                    .setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        }

        if (calendarConfigDTO.getEventTypes().contains(CalendarEventType.SERVICE_ANNIVERSARY.toString())) {
            personalSubTypeMap.get(CalendarEventType.SERVICE_ANNIVERSARY.toString())
                    .setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        } else {
            personalSubTypeMap.get(CalendarEventType.SERVICE_ANNIVERSARY.toString())
                    .setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        }

        // Save Entities
        calendarSubTypeDao.update(personalSubTypeMap.get(CalendarEventType.BIRTHDAY.toString()));
        calendarSubTypeDao.update(personalSubTypeMap.get(CalendarEventType.SERVICE_ANNIVERSARY.toString()));

        // Set Holidays
        if (calendarConfigDTO.getEventTypes().contains(CalendarEventType.HOLIDAY.toString())) {
            setHolidaySubtypeDisplayStatus(holidaySubTypes, StatusTypeCode.ACTIVE.name());
            holidayCalendarType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        } else {
            setHolidaySubtypeDisplayStatus(holidaySubTypes, StatusTypeCode.INACTIVE.name());
            holidayCalendarType.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        }

        calendarTypeDao.update(holidayCalendarType);

        return getConfigCalendarEventTypes();
    }

    private void setHolidaySubtypeDisplayStatus(List<CalendarSubType> calendarSubType, String status) {
        for (CalendarSubType type : calendarSubType) {
            type.setDisplayStatusTypeCode(status);
            calendarSubTypeDao.update(type);
        }
    }

}
