package com.maritz.culturenext.calendar.config.rest;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.calendar.config.dto.CalendarConfigDTO;
import com.maritz.culturenext.calendar.config.services.CalendarConfigService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

@RestController
@Api(value = "/calendar-config", description = "All end points dealing with calendar configs")
public class CalendarConfigRestService {

    @Inject private CalendarConfigService calendarConfigService;
    @Inject private Security security;

    //rest/calendar-config
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "/calendar-config", method = RequestMethod.PUT)
    @ApiOperation(value = "Endpoint to config calendar event types")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "success", response = CalendarConfigDTO.class),
            @ApiResponse(code = 400, message = "Request error"), @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 403, message = "Forbidden") })
    @Permission("PUBLIC")
    public CalendarConfigDTO
            setConfigCalendarEventTypes(@RequestBody CalendarConfigDTO calendarConfigDTO) {

        return calendarConfigService.setConfigCalendarEventTypes(calendarConfigDTO);
    }

    //rest/calendar-config
    @RequestMapping(value = "/calendar-config", method = RequestMethod.GET)
    @ApiOperation(value = "Endpoint to config calendar event types")
    @Permission("PUBLIC")
    public CalendarConfigDTO getConfigCalendarEventType() throws Throwable {

        Long paxId = security.getPaxId();
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return calendarConfigService.getConfigCalendarEventTypes();
    }
}
