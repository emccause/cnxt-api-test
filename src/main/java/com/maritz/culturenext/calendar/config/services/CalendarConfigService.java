package com.maritz.culturenext.calendar.config.services;


import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.calendar.config.dto.CalendarConfigDTO;

public interface CalendarConfigService {
    
    /**
     * Returns calendar configuration settings. Current event types include SERVICE_ANNIVERSARY and BIRTHDAY.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22183961/Get+Calendar+Config
     */
    CalendarConfigDTO getConfigCalendarEventTypes();
    
    /**
     * Set up the configured calendar event types
     * 
     * @param calendarConfigDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22183966/Update+Calendar+Config
     */
    CalendarConfigDTO setConfigCalendarEventTypes(CalendarConfigDTO calendarConfigDTO) throws ErrorMessageException;
}
