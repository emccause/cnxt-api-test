package com.maritz.culturenext.calendar.config.dto;


import java.util.List;

public class CalendarConfigDTO {

    List<String> eventTypes;

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }
}
