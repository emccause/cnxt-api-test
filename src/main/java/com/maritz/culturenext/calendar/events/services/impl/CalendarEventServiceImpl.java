package com.maritz.culturenext.calendar.events.services.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.CalendarEntry;
import com.maritz.core.jpa.entity.CalendarSubType;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.calendar.constants.CalendarConstants;
import com.maritz.culturenext.calendar.events.dao.CalendarEventsDao;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.calendar.events.services.CalendarEventService;
import com.maritz.culturenext.calendar.util.CalendarTranslationUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.AudienceEnum;
import com.maritz.culturenext.enums.CalendarSubTypeCode;
import com.maritz.culturenext.jpa.entity.VwUserGroupsSearchData;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class CalendarEventServiceImpl implements CalendarEventService {

    @Inject private CalendarEventsDao calendarEventsDao;
    @Inject private CalendarTranslationUtil calendarTranslationUtil;
    @Inject private ConcentrixDao<Acl> aclDao;
    @Inject private ConcentrixDao<CalendarEntry> calendarEntryDao;
    @Inject private ConcentrixDao<CalendarSubType> calendarSubTypeDao;
    @Inject private ConcentrixDao<Role> roleDao;
    @Inject private ConcentrixDao<VwUserGroupsSearchData> vwUserGroupsSearchDataDao;

    @Inject private Security security;

    @Override
    public CalendarEventDTO createCalendarEvent(CalendarEventDTO calendarEventDTO) throws ErrorMessageException {

        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // Validate that current logged in user is Admin to make changes to calendar event
        if (!security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            errors.add(CalendarConstants.NON_ADMIN_MESSAGE);
        }

        // Validate name
        if (calendarEventDTO.getName() == null || calendarEventDTO.getName().length() == 0) {
            errors.add(CalendarConstants.MISSING_NAME_CODE_MESSAGE);
        }

        // Validate startDate
        if (calendarEventDTO.getStartDate() == null || calendarEventDTO.getStartDate().length() == 0) {

            errors.add(CalendarConstants.MISSING_START_DATE_MESSAGE);

        } else if (!calendarEventDTO.getStartDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)
                || !DateUtil.verifyCalendarDate(calendarEventDTO.getStartDate())) {

            errors.add(CalendarConstants.INVALID_START_DATE_MESSAGE);
        }

        // Validate audience
        if (calendarEventDTO.getAudience() == null || calendarEventDTO.getAudience().isEmpty()) {
            errors.add(CalendarConstants.MISSING_AUDIENCE_CODE_MESSAGE);

        } else if (!validateAudience(calendarEventDTO.getAudience())) {

            errors.add(CalendarConstants.INVALID_AUDIENCE_CODE_MESSAGE);
        }

        // Validate endDate
        if (calendarEventDTO.getEndDate() != null
                && (!calendarEventDTO.getEndDate().matches(DateConstants.VALID_DATE_YYYY_MM_DD)
                        || !DateUtil.verifyCalendarDate(calendarEventDTO.getEndDate()))) {

            errors.add(CalendarConstants.INVALID_END_DATE_MESSAGE);

        } else if (calendarEventDTO.getStartDate() != null
                && (calendarEventDTO.getEndDate() == null || calendarEventDTO.getEndDate().length() == 0)) {
            calendarEventDTO.setEndDate(calendarEventDTO.getStartDate());
        }

        // Throw exception if any errors exists
        ErrorMessageException.throwIfHasErrors(errors);

        // Convert String to Date
        Date startDate = DateUtil.convertToStartOfDayString(calendarEventDTO.getStartDate(), TimeZone.getDefault());
        Date endDate = DateUtil.convertToStartOfDayString(calendarEventDTO.getEndDate(), TimeZone.getDefault());

        // Check if startDate is before or equal to endDate
        if (startDate != null && endDate != null && startDate.after(endDate)) {
            errors.add(CalendarConstants.ERROR_START_DATE_AFTER_END_DATE_CODE_MESSAGE);
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        CalendarEntry calendarEntry = new CalendarEntry();

        calendarEntry.setCalendarEntryName(calendarEventDTO.getName());
        calendarEntry.setFromDate(startDate);
        calendarEntry.setThruDate(endDate);
        calendarEntry.setCalendarEntryDesc(calendarEventDTO.getDescription());
        calendarEntry.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        calendarEntry.setAllDayEventStatusTypeCode(StatusTypeCode.ACTIVE.name());

        // Need to find the type based on what is passed in as a valid type
        CalendarSubType calendarSubType = calendarSubTypeDao.findBy()
                .where(ProjectConstants.CALENDAR_SUB_TYPE_CODE).eq(CalendarSubTypeCode.CUSTOM.toString())
                .findOne();
        calendarEntry.setCalendarSubTypeId(calendarSubType.getId());

        calendarEntryDao.save(calendarEntry);

        Long roleId = roleDao.findBy()
                .where(ProjectConstants.ROLE_NAME).eq(CalendarConstants.AUDIENCE)
                .findOne(ProjectConstants.ROLE_ID, Long.class);
        Long calendarEntryId = calendarEntry.getId();

        for (GroupMemberStubDTO entityDTO : calendarEventDTO.getAudience()) {
            Acl acl = new Acl();

            acl.setRoleId(roleId);
            acl.setTargetTable(CalendarConstants.CALENDAR);
            acl.setTargetId(calendarEntryId);
            if (entityDTO.getGroupId() != null) {
                acl.setSubjectTable(CalendarConstants.GROUPS);
                acl.setSubjectId(entityDTO.getGroupId());
            }
            if (entityDTO.getPaxId() != null) {
                acl.setSubjectTable(CalendarConstants.PAX);
                acl.setSubjectId(entityDTO.getPaxId());
            }

            aclDao.save(acl);
        }

        CalendarEventDTO response = new CalendarEventDTO();
        response.setId(calendarEntryId);
        response.setName(calendarEntry.getCalendarEntryName());
        response.setStartDate(calendarEventDTO.getStartDate());
        response.setEndDate(calendarEventDTO.getEndDate());
        response.setAudience(calendarEventDTO.getAudience());
        response.setDescription(calendarEntry.getCalendarEntryDesc());
        response.setType(calendarSubType.getCalendarSubTypeCode());

        return response;
    }

    @Override
    public PaginatedResponseObject<CalendarEventDTO> getCalendarEventsForParticipant(
            Long paxId, String startDate, String endDate, String languageCode, String audience
            , String eventTypes, Integer page, Integer size) throws ErrorMessageException {

        // Parse comma delimited event types
        List<String> eventTypeList = StringUtils.parseDelimitedData(eventTypes);
        
        // handle page parameters
        if (page == null || page < 1) {
            page = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        if (size == null || size < 1) {
            size = ProjectConstants.DEFAULT_PAGE_SIZE;
        }
        
        // Validate query parameters
        validateGetQueryParams(startDate, endDate, audience, eventTypeList, size);
        
        AudienceEnum audienceVal = null;
        
        // Default audience value - ALL
        if (audience == null) {
            audienceVal = AudienceEnum.ALL;
        } else {
            audienceVal = AudienceEnum.valueOf(audience);
        }

        Date fromDate = DateUtil.convertToStartOfDayString(startDate, TimeZone.getDefault());
        Date toDate = DateUtil.convertToStartOfDayString(endDate, TimeZone.getDefault());
        
        // Getting query data.
        Integer totalResults = 0;
        List<CalendarEventDTO> calendarEventsDTOList = calendarEventsDao.getActiveCalendarEvents(paxId,
                fromDate, toDate, eventTypeList, audienceVal.name(), page, size);
        
        if (!calendarEventsDTOList.isEmpty()) {

            if (languageCode != null && !ProjectConstants.DEFAULT_LOCALE_CODE.equals(languageCode)) {
                calendarEventsDTOList = calendarTranslationUtil.translateCalendarEntryNames(calendarEventsDTOList, languageCode);
            }
            
            // Total query result.
            totalResults = calendarEventsDTOList.get(0).getTotalResults();
        }
        
        // Handle other parameters
        Map<String, Object> requestParameters = new HashMap<>();
        requestParameters.put(RestParameterConstants.START_DATE_REST_PARAM, startDate);
        requestParameters.put(RestParameterConstants.END_DATE_REST_PARAM, endDate);
        requestParameters.put(RestParameterConstants.LANGUAGE_CODE_REST_PARAM, languageCode);
        requestParameters.put(RestParameterConstants.AUDIENCE_REST_PARAM,audience);
        requestParameters.put(RestParameterConstants.EVENT_TYPE_REST_PARAM, eventTypes);

        // Return paginated data
        return new PaginatedResponseObject<CalendarEventDTO>(calendarEventsDTOList, requestParameters 
                , String.format(CalendarConstants.CALENDAR_EVENT_REST_ENDPOINT, paxId)
                , totalResults, page, size);
    }


    private boolean validateAudience(List<GroupMemberStubDTO> audience) {
        boolean foundEmptyIdAndGroup = audience.stream().anyMatch(item -> item.getPaxId() == null && item.getGroupId() == null);
        if (foundEmptyIdAndGroup)
            return false;
        List<Long> queryPaxIds = audience.stream().map(GroupMemberStubDTO::getPaxId).filter(Objects::nonNull).collect(Collectors.toList());
        List<Long> queryGroupIds = audience.stream().map(GroupMemberStubDTO::getGroupId).filter(Objects::nonNull).collect(Collectors.toList());

        if (!queryPaxIds.isEmpty()) {

            List paxList = vwUserGroupsSearchDataDao.findBy()
                    .where(ProjectConstants.PAX_ID).in(queryPaxIds)
                    .and(ProjectConstants.STATUS_TYPE_CODE).ne(StatusTypeCode.INACTIVE.name())
                    .find().stream().map(VwUserGroupsSearchData::getPaxId).distinct().collect(Collectors.toList());
            Collection result = CollectionUtils.subtract(queryPaxIds, paxList);
            if (!result.isEmpty())
                return false;
        }

        if (!queryGroupIds.isEmpty()) {

            List groupsList = vwUserGroupsSearchDataDao.findBy()
                    .where(ProjectConstants.GROUP_ID).in(queryGroupIds)
                    .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                    .find().stream().map(VwUserGroupsSearchData::getGroupId).distinct().collect(Collectors.toList());;
            Collection result = CollectionUtils.subtract(queryGroupIds, groupsList);
            if (!result.isEmpty())
                return false;
        }
        return true;
    }
    
    /**
     * Checking query input parameters.
     * @param startDate
     * @param endDate
     * @param audience
     * @param eventTypes
     * @param size
     */
    private void validateGetQueryParams(String startDate, String endDate, String audience, List<String> eventTypes, Integer size) {
        
        List<ErrorMessage> errors = new ArrayList<>();
        
        // Validate pagination size
        if (size != null && size > CalendarConstants.MAX_PAGINATION_SIZE) {
            errors.add(CalendarConstants.PAGINATION_EXCEEDS_MAX_SIZE_MESSAGE);
        }

        // Validate startDate
        if (startDate == null || startDate.length() == 0) {

            errors.add(CalendarConstants.MISSING_START_DATE_MESSAGE);

        } else if (!startDate.matches(DateConstants.VALID_DATE_YYYY_MM_DD) || !DateUtil.verifyCalendarDate(startDate)) {

            errors.add(CalendarConstants.INVALID_START_DATE_MESSAGE);
        }

        // Convert String to Date
        Date fromDate = DateUtil.convertToStartOfDayString(startDate, TimeZone.getDefault());
        Date toDate = null;

        // Validate endDate
        if (endDate != null
                && (!endDate.matches(DateConstants.VALID_DATE_YYYY_MM_DD) || !DateUtil.verifyCalendarDate(endDate))) {

            errors.add(CalendarConstants.INVALID_END_DATE_MESSAGE);

        } else if (endDate != null && endDate.length() != 0) {
            toDate = DateUtil.convertToStartOfDayString(endDate, TimeZone.getDefault());
        }

        // Check if startDate is before or equal to endDate
        if (fromDate != null && toDate != null && fromDate.after(toDate)) {
            errors.add(CalendarConstants.ERROR_START_DATE_AFTER_END_DATE_CODE_MESSAGE);
        }
        
        // Validate audience's value
        if (audience != null) {
            try {
                AudienceEnum.valueOf(audience);
            } catch (IllegalArgumentException e) {
                errors.add(CalendarConstants.INVALID_AUDIENCE_CODE_MESSAGE);
            }
        }
        
        // Validate eventTypes values
        if (CollectionUtils.isNotEmpty(eventTypes)) {
            for (String type : eventTypes) {
                try {
                    CalendarSubTypeCode.valueOf(type);
                } catch (IllegalArgumentException e) {
                    errors.add(CalendarConstants.INVALID_EVENT_TYPE_MESSAGE);
                    break;
                }
            }
        }
        
        // Throw exception if any errors exists
        ErrorMessageException.throwIfHasErrors(errors);
    }
}
