package com.maritz.culturenext.calendar.events.dao;

import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;

import java.util.Date;
import java.util.List;

public interface CalendarEventsDao {
    
    List<CalendarEventDTO> getActiveCalendarEvents(Long paxId, Date startDate, Date endDate
            , List<String> calendarSubTypes, String audience, Integer pageNumber, Integer pageSize);
    
}
