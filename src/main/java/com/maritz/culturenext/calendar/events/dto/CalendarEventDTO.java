package com.maritz.culturenext.calendar.events.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CalendarEventDTO {
    private Long id;
    private String name;
    private String startDate;
    private String endDate;
    private String description;
    private String type;
    private Long targetId;
    private EmployeeDTO pax;
    private List<GroupMemberStubDTO> audience;
    private Integer totalResults;
    private String targetTable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GroupMemberStubDTO> getAudience() {
        return audience;
    }

    public void setAudience(List<GroupMemberStubDTO> audience) {
        this.audience = audience;
    }

    public EmployeeDTO getPax() {
        return pax;
    }

    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTargetTable(String name) {
        this.targetTable = name;
    }

    public String getTargetTable() {
        return targetTable;
    }
}