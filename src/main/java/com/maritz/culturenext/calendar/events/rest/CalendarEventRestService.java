package com.maritz.culturenext.calendar.events.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.security.annotation.Permission;
import com.maritz.core.security.Security;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.calendar.events.services.CalendarEventService;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.permission.constants.PermissionConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

@RestController
@Api(value = "/calendar-events", description = "All end points dealing with calendar events")
public class CalendarEventRestService {

    @Inject private CalendarEventService calendarEventService;
    @Inject private Security security;

    //rest/calendar-events
    @PreAuthorize("!@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.CALENDAR_TYPE + "')")
    @RequestMapping(value = "calendar-events", method = RequestMethod.POST)
    @ApiOperation(value = "Endpoint to create calendar events. Returns the newly created Calendar Event")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully created calendar event",
            response = CalendarEventDTO.class), @ApiResponse(code = 400, message = "Request error"), @ApiResponse(code = 403, message = "Forbidden") })
    @Permission("PUBLIC")
    public CalendarEventDTO createCalendarEvent(@RequestBody CalendarEventDTO calendarEventDTO) {
        return calendarEventService.createCalendarEvent(calendarEventDTO);
    }

    //rest/participants/~/calendar-events
    @PreAuthorize("!@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.CALENDAR_TYPE + "')")
    @RequestMapping(
            value = "participants/{" + PAX_ID_REST_PARAM + "}/calendar-events"
            , method = RequestMethod.GET)
    @ApiOperation(value = "Endpoint returns a list of calendar events for the participant.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "success", response = CalendarEventDTO.class),
            @ApiResponse(code = 400, message = "Request error"), @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 403, message = "Forbidden") })
    @Permission("PUBLIC")
    public PaginatedResponseObject<CalendarEventDTO> getCalendarEventsForParticipant(
            @PathVariable(value = PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "The date to start the search. This is inclusive.") 
                @RequestParam(value = START_DATE_REST_PARAM, required = true) String startDate,
            @ApiParam(value = "The date to end the search. This is inclusive. ") 
                @RequestParam(value = END_DATE_REST_PARAM, required = false) String endDate,
            @ApiParam(value = "The language that the event should be returned in") 
                @RequestParam(value = LANGUAGE_CODE_REST_PARAM,required = false) String languageCode,
            @ApiParam(value = "Audience to filter by.  Options are 'ALL' and 'NETWORK'") 
                @RequestParam(value = AUDIENCE_REST_PARAM,required = false) String audience,
            @ApiParam(value = "Comma separated list of event types to filter") 
                @RequestParam(value = EVENT_TYPE_REST_PARAM,required = false) String eventTypes,
            @ApiParam(value = "Page number to request") 
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer page,
            @ApiParam(value = "    Size of pagination response to request") 
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer size
            ) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return calendarEventService.getCalendarEventsForParticipant(paxId, startDate, endDate
                , languageCode, audience, eventTypes, page, size);

    }

}
