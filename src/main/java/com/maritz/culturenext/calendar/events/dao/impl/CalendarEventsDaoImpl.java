package com.maritz.culturenext.calendar.events.dao.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.*;

import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.enums.CalendarSubTypeCode;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.util.DateUtil;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.hibernate.query.procedure.internal.ProcedureParameterImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.calendar.events.dao.CalendarEventsDao;
import com.maritz.culturenext.constants.ProjectConstants;


@Repository
public class CalendarEventsDaoImpl extends PaginatingDaoImpl implements CalendarEventsDao {

    protected final EntityManager em;
    @Inject
    private ParticipantInfoDao participantInfoDao;

    private static final String IS_ADMIN_PLACEHOLDER = "IS_ADMIN_PARAM";
    private static final String AUDIENCE_PLACEHOLDER = "AUDIENCE_PARAM";
    private static final String CALENDAR_SUB_TYPES_PLACEHOLDER = "CALENDAR_SUB_TYPES_PARAM";
    private static final String END_DATE_PLACEHOLDER = "END_DATE_PARAM";
    private static final String PAX_ID_PLACEHOLDER = "PAX_ID_PARAM";
    private static final String START_DATE_PLACEHOLDER = "START_DATE_PARAM";
    private static final String START_PLACEHOLDER = "START";
    private static final String PAGE_SIZE_PLACEHOLDER = "PAGE_SIZE";

    public CalendarEventsDaoImpl (EntityManager em) {
        this.em = em;
    }

    private static final String GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX_QUERY =
            "EXEC component.UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX "+
            " :" + PAX_ID_PLACEHOLDER +
            ", :" + IS_ADMIN_PLACEHOLDER +
            ", :" + AUDIENCE_PLACEHOLDER +
            ", :" + CALENDAR_SUB_TYPES_PLACEHOLDER +
            ", :" + START_DATE_PLACEHOLDER +
            ", :" + END_DATE_PLACEHOLDER +
            ", :" + START_PLACEHOLDER +
            ", :" + PAGE_SIZE_PLACEHOLDER;

    /**
     * Get active calendar events according to filters.
     * @param paxId - logged pax id
     * @param startDate
     * @param endDate
     * @param calendarSubTypes - event types:
*         US_OBSERVED,RELIGIOUS,COMMON,COMPANY,PROGRAM,CUSTOM,BIRTHDAY,SERVICE_ANNIVERSARY
     * @param audience - ALL or NETWORK
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public List<CalendarEventDTO> getActiveCalendarEvents(Long paxId, Date startDate,
                                                          Date endDate, List<String> calendarSubTypes, String audience,
                                                          Integer pageNumber, Integer pageSize) {

        StoredProcedureQuery query = em.createStoredProcedureQuery("component.UP_GET_ACTIVE_CALENDAR_EVENTS_FOR_PAX")
                .registerStoredProcedureParameter("PAX_ID", Long.class, ParameterMode.IN)
                .registerStoredProcedureParameter("IS_ADMIN", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("START_DATE", Date.class, ParameterMode.IN)
                .registerStoredProcedureParameter("END_DATE", Date.class,ParameterMode.IN)
                .registerStoredProcedureParameter("CALENDAR_SUB_TYPES", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("AUDIENCE", String.class, ParameterMode.IN)
                .registerStoredProcedureParameter("START_PAGE", Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter("PAGE_SIZE", Integer.class, ParameterMode.IN)
                .setParameter("PAX_ID", paxId)
                .setParameter("IS_ADMIN",
                        String.valueOf(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)))
                .setParameter("START_DATE",startDate)
                .setParameter("END_DATE", endDate)
                .setParameter("CALENDAR_SUB_TYPES", CollectionUtils.isNotEmpty(calendarSubTypes)?
                        String.join(",",calendarSubTypes):"")
                .setParameter("AUDIENCE",audience)
                .setParameter("START_PAGE",(pageNumber - 1) * pageSize)
                .setParameter("PAGE_SIZE",pageSize);

        setStoreProcedureEnableNullParameters(query);

        boolean status = query.execute();
        List<CalendarEventDTO> result = (List<CalendarEventDTO>) query.getResultList().stream()
                .map(item -> {
                    CalendarEventDTO calendarEventDTO = new CalendarEventDTO();
                    System.out.println(Arrays.toString((Object[])item));
                    calendarEventDTO.setId ((Long) ((Object[])item)[0]);
                    calendarEventDTO.setName((String) ((Object[])item)[1]);
                    calendarEventDTO.setDescription((String) ((Object[])item)[2]);
                    calendarEventDTO.setStartDate((String) DateUtil.convertToUTCDate((Date)((Object[])item)[3]));
                    calendarEventDTO.setEndDate((String) DateUtil.convertToUTCDate((Date)((Object[])item)[4]));
                    calendarEventDTO.setTargetId((Long) ((Object[])item)[5]);
                    calendarEventDTO.setType((String) ((Object[])item)[6]);
                    if (calendarEventDTO.getTargetId() != null
                        && (CalendarSubTypeCode.BIRTHDAY.name().equals(calendarEventDTO.getType())
                        || CalendarSubTypeCode.SERVICE_ANNIVERSARY.name().equals(calendarEventDTO.getType()))) {

                        calendarEventDTO.setPax( participantInfoDao.getEmployeeDTO(calendarEventDTO.getTargetId()));
                    }

                    calendarEventDTO.setTotalResults ((Integer) ((Object[])item)[7]);
                    return calendarEventDTO;
                }).collect(Collectors.toList());
        return result;
    }

    public void setStoreProcedureEnableNullParameters(StoredProcedureQuery storedProcedureQuery) {
        if (storedProcedureQuery == null || storedProcedureQuery.getParameters() == null)
            return;

        for (Parameter parameter : storedProcedureQuery.getParameters()) {
            ((ProcedureParameterImpl) parameter).enablePassingNulls(true);
        }
    }
}
