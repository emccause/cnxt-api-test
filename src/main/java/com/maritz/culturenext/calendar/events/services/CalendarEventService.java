package com.maritz.culturenext.calendar.events.services;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;

public interface CalendarEventService {
    
    /**
     * Create a CUSTOM calendar event
     * 
     * @param calendarEventDTO - calendar event details
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/21069840/Create+Calendar+Event
     */
    CalendarEventDTO createCalendarEvent(CalendarEventDTO calendarEventDTO) throws ErrorMessageException;

    /**
     * Get a list of all calendar events configured that the pax ID is a part of the audience.
     * 
     * @param paxId
     * @param startDate
     * @param endDate
     * @param languageCode
     * @param audience
     * @param delimEvenTypes
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/21069845/Get+Calendar+Events+for+Participant
     */
    PaginatedResponseObject<CalendarEventDTO> getCalendarEventsForParticipant(Long paxId, String startDate, String endDate, String languageCode
            , String audience, String delimEvenTypes, Integer page, Integer size ) throws ErrorMessageException;

}
