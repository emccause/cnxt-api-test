package com.maritz.culturenext.calendar.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class CalendarConstants {
    
    // Endpoints
    public static final String CALENDAR_EVENT_REST_ENDPOINT = "/rest/participants/%s/calendar-events";
    
    //Errors
    public final static String ERROR_MISSING_NAME_CODE = "MISSING_NAME";
    public final static String ERROR_MISSING_NAME_MSG = "A event name is required";
    public final static String ERROR_MISSING_START_DATE_CODE = "MISSING_START_DATE";
    public final static String ERROR_MISSING_START_DATE_MSG = "A start date required";
    public final static String ERROR_MISSING_AUDIENCE_CODE = "MISSING_AUDIENCE";
    public final static String ERROR_MISSING_AUDIENCE_MSG = "An audience is required";
    public final static String ERROR_INVALID_START_DATE_CODE = "INVALID_START_DATE";
    public final static String ERROR_INVALID_START_DATE_MSG = "The provided start date is not a valid date";
    public final static String ERROR_START_DATE_AFTER_END_DATE_CODE = "START_DATE_AFTER_END_DATE";
    public final static String ERROR_START_DATE_AFTER_END_DATE_MSG = "Start date is after end date";
    public final static String ERROR_INVALID_END_DATE_CODE = "INVALID_END_DATE";
    public final static String ERROR_INVALID_END_DATE_MSG = "The provided end date is not a valid date";
    public final static String ERROR_INVALID_AUDIENCE_CODE = "INVALID_AUDIENCE";
    public final static String ERROR_INVALID_AUDIENCE_MSG = "At least one provided audience is invalid";
    public final static String ERROR_NOT_ADMIN_CODE = "ADMIN_ROLE_REQUIRED";
    public final static String ERROR_NOT_ADMIN_CREATE_MSG = "You must be an administrator to create a calendar event";
    public final static String ERROR_MISSING_EVENT_TYPES_CODE = "MISSING_EVENT_TYPES";
    public final static String ERROR_MISSING_EVENT_TYPES_MSG = "Event types are required";
    public final static String ERROR_INVALID_EVENT_TYPE_CODE = "INVALID_EVENT_TYPE";
    public final static String ERROR_INVALID_EVENT_TYPE_MSG = "At least one event type is invalid";
    
    // Calendar pagination
    public static final Integer MAX_PAGINATION_SIZE = 500;
    public static final String ERROR_PAGINATION_EXCEEDS_MAX_SIZE_CODE = "PAGINATION_EXCEEDS_MAX_SIZE";
    public static final String ERROR_PAGINATION_EXCEEDS_MAX_SIZE_MSG = "Pagination max size is " + MAX_PAGINATION_SIZE;

    //Other
    public static final String GROUPS = "GROUPS";
    public static final String PAX = "PAX";
    public static final String CALENDAR = "CALENDAR";
    public static final String AUDIENCE = "Audience";
    
    //Error Messages
    public static final ErrorMessage NON_ADMIN_MESSAGE = new ErrorMessage()
            .setCode(CalendarConstants.ERROR_NOT_ADMIN_CODE)
            .setField(ProjectConstants.PAX_ID)
            .setMessage(CalendarConstants.ERROR_NOT_ADMIN_CREATE_MSG);
    
    public static final ErrorMessage MISSING_NAME_CODE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.NAME)
            .setCode(CalendarConstants.ERROR_MISSING_NAME_CODE)
            .setMessage(CalendarConstants.ERROR_MISSING_NAME_MSG);
    
    public static final ErrorMessage MISSING_START_DATE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.START_DATE)
            .setCode(CalendarConstants.ERROR_MISSING_START_DATE_CODE)
            .setMessage(CalendarConstants.ERROR_MISSING_START_DATE_MSG);
            
    public static final ErrorMessage INVALID_START_DATE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.START_DATE)
            .setCode(CalendarConstants.ERROR_INVALID_START_DATE_CODE)
            .setMessage(CalendarConstants.ERROR_INVALID_START_DATE_MSG);
            
    public static final ErrorMessage INVALID_END_DATE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.END_DATE)
            .setCode(CalendarConstants.ERROR_INVALID_END_DATE_CODE)
            .setMessage(CalendarConstants.ERROR_INVALID_END_DATE_MSG);
    
    public static final ErrorMessage ERROR_START_DATE_AFTER_END_DATE_CODE_MESSAGE = new ErrorMessage()
            .setCode(CalendarConstants.ERROR_START_DATE_AFTER_END_DATE_CODE)
            .setField(ProjectConstants.START_DATE)
            .setMessage(CalendarConstants.ERROR_START_DATE_AFTER_END_DATE_MSG);
    
    public static final ErrorMessage MISSING_AUDIENCE_CODE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.AUDIENCE)
            .setCode(CalendarConstants.ERROR_MISSING_AUDIENCE_CODE)
            .setMessage(CalendarConstants.ERROR_MISSING_AUDIENCE_MSG);
    
    public static final ErrorMessage INVALID_AUDIENCE_CODE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.AUDIENCE)
            .setCode(CalendarConstants.ERROR_INVALID_AUDIENCE_CODE)
            .setMessage(CalendarConstants.ERROR_INVALID_AUDIENCE_MSG);

    public static final ErrorMessage INVALID_EVENT_TYPE_MESSAGE = new ErrorMessage()
            .setField(ProjectConstants.EVENT_TYPE)
            .setCode(CalendarConstants.ERROR_INVALID_EVENT_TYPE_CODE)
            .setMessage(CalendarConstants.ERROR_INVALID_EVENT_TYPE_MSG);
    
    public static final ErrorMessage PAGINATION_EXCEEDS_MAX_SIZE_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.SIZE)
        .setCode(CalendarConstants.ERROR_PAGINATION_EXCEEDS_MAX_SIZE_CODE)
        .setMessage(CalendarConstants.ERROR_PAGINATION_EXCEEDS_MAX_SIZE_MSG);
}
