package com.maritz.culturenext.pointupload.constants;

public enum PointUploadFileHeadersEnum {
    PARTICIPANT_ID("PARTICIPANT ID"),
    AWARD_AMOUNT("AWARD AMOUNT"),
    PROGRAM_NAME("PROGRAM NAME"),
    BUDGET_NAME("BUDGET NAME"),
    DESCRIPTION("DESCRIPTION");

    private String name;

    private static final int RECORD_COUNT = 5;

    PointUploadFileHeadersEnum(String name) {
        this.name = name;
    }

    public String getHeaderName() {
        return name;
    }

    public static int getRecordCount(){
        return RECORD_COUNT;
    }
}
