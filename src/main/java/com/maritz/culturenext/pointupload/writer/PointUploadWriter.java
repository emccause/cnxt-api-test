package com.maritz.culturenext.pointupload.writer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.batch.item.ItemWriter;
import org.springframework.core.env.Environment;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.services.TransactionService;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.pointupload.dto.DiscretionaryExtended;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;


public class PointUploadWriter implements ItemWriter<List<DiscretionaryExtended>> {

    private BatchService batchService;
    private Environment environment;
    private TransactionHeaderMiscDao transactionHeaderMiscDao;
    private TransactionService transactionService;

    @Inject
    public PointUploadWriter setBatchService(BatchService batchService) {
        this.batchService = batchService;
        return this;
    }
    
    @Inject
    public PointUploadWriter setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }
    
    @Inject
    public PointUploadWriter setTransactionHeaderMiscDao(TransactionHeaderMiscDao transactionHeaderMiscDao) {
        this.transactionHeaderMiscDao = transactionHeaderMiscDao;
        return this;
    }
    
    @Inject
    public PointUploadWriter setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
        return this;
    }
    
    @Override
    public void write(List<? extends List<DiscretionaryExtended>> items) throws Exception {
        for (List<DiscretionaryExtended> batchFileRecords : items) {
            Long batchId = null;

            Collection<TransactionHeaderDetail<Discretionary>> transactionHeaderDetails = new ArrayList<>(batchFileRecords.size());
            for (DiscretionaryExtended item : batchFileRecords) {
                if (batchId == null) batchId = item.getTransactHeader().getBatchId();

                transactionHeaderDetails.add(new TransactionHeaderDetail<>(
                    item.getTransactHeader(), 
                    item.getDiscretionary(),
                    Arrays.asList(new TransactionHeaderMisc()
                            .setVfName(VfName.PAYOUT_TYPE.name())
                            .setMiscData(item.getPayoutType().toUpperCase())
                            .setMiscDate(new Date()),
                        new TransactionHeaderMisc()
                            .setVfName(VfName.DEFAULT_PROJECT_NUMBER.name())
                            .setMiscData(item.getProjectNumber())
                            .setMiscDate(new Date()),
                        new TransactionHeaderMisc()
                            .setVfName(VfName.DEFAULT_SUB_PROJECT_NUMBER.name())
                            .setMiscData(item.getSubProjectNumber())
                            .setMiscDate(new Date()),
                        new TransactionHeaderMisc()
                            .setVfName(VfName.ORIGINAL_AMOUNT.name())
                            .setMiscData(item.getOriginalAmount().toString())
                            .setMiscDate(new Date())
                    )
                ));
            }
            transactionService.create(transactionHeaderDetails);

            //Update batch status to PENDING_RELEASE
            batchService.updateBatchStatus(batchId, StatusTypeCode.PENDING_RELEASE.name());

            //Create End-Time batch_event
            batchService.createBatchEvent(batchId, new BatchEventDTO()
                .setType(BatchEventTypeCode.ENDTIM.name())
                .setMessage(DateUtils.formatDatetimeUTC(new Date()))
            );
        }
        
        //Check for configurable sub-project overrides (the default is created above)
        if (environment.getProperty(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE) != null) {
            transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        }
    }
}
