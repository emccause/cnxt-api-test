package com.maritz.culturenext.pointupload.processor;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.SubProject;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.BudgetMiscRepository;
import com.maritz.core.jpa.repository.BudgetRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.ProjectRepository;
import com.maritz.core.jpa.repository.SubProjectRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.jpa.support.util.TransactionTypeCode;
import com.maritz.core.security.location.PaxGroupService;
import com.maritz.core.services.BatchService;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.pointupload.constants.PointUploadFileHeadersEnum;
import com.maritz.culturenext.pointupload.dao.PointUploadDao;
import com.maritz.culturenext.pointupload.dto.BudgetUsedDTO;
import com.maritz.culturenext.pointupload.dto.DiscretionaryExtended;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.culturenext.util.StatusTypeUtil;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import javax.inject.Inject;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PointUploadProcessor implements ItemProcessor<BatchFile, List<DiscretionaryExtended>> {
    final Logger logger = LoggerFactory.getLogger(getClass());

    private static final String PROGRAM_ACTIVITY_TYPE_CODE = "PNTU";
    private static final String INVALID_FILE_FORMAT = "Invalid File Format";
    private static final List<String> US_COUNTRY_CODES = Arrays.asList("US", "PR");
    
    private AddressRepository addressRepository;
    private BatchFileService batchFileService;
    private BatchRepository batchRepository;
    private BatchService batchService;
    private BudgetMiscRepository budgetMiscRepository;
    private BudgetRepository budgetRepository;
    private ParticipantsPppIndexService participantsPppIndexService;
    private PaxGroupService paxGroupService;
    private PaxRepository paxRepository;
    private PointUploadDao pointUploadDao;
    private ProgramMiscRepository programMiscRepository;
    private ProgramRepository programRepository;
    private ProjectRepository projectRepository;
    private SubProjectRepository subProjectRepository;
    private SysUserRepository sysUserRepository;

    @Inject
    public PointUploadProcessor setAddressRepository(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setBatchFileService(BatchFileService batchFileService) {
        this.batchFileService = batchFileService;
        return this;
    }

    @Inject
    public PointUploadProcessor setBatchRepository(BatchRepository batchRepository) {
        this.batchRepository = batchRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setBatchService(BatchService batchService) {
        this.batchService = batchService;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setBudgetMiscRepository(BudgetMiscRepository budgetMiscRepository) {
        this.budgetMiscRepository = budgetMiscRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setBudgetRepository(BudgetRepository budgetRepository) {
        this.budgetRepository = budgetRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setParticipantsPppIndexService(ParticipantsPppIndexService participantsPppIndexService) {
        this.participantsPppIndexService = participantsPppIndexService;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setPaxGroupService(PaxGroupService paxGroupService) {
        this.paxGroupService = paxGroupService;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setPaxRepository(PaxRepository paxRepository) {
        this.paxRepository = paxRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setPointUploadDao(PointUploadDao pointUploadDao) {
        this.pointUploadDao = pointUploadDao;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setProgramMiscRepository(ProgramMiscRepository programMiscRepository) {
        this.programMiscRepository = programMiscRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setProgramRepository(ProgramRepository programRepository) {
        this.programRepository = programRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setSubProjectRepository(SubProjectRepository subProjectRepository) {
        this.subProjectRepository = subProjectRepository;
        return this;
    }
    
    @Inject
    public PointUploadProcessor setSysUserRepository(SysUserRepository sysUserRepository) {
        this.sysUserRepository = sysUserRepository;
        return this;
    }
    
    @Override
    public List<DiscretionaryExtended> process(BatchFile batchFile) throws Exception {
        logger.debug("Point Upload Processor - Process() - BatchFileId: {} File Upload: {}",
                null == batchFile ? "BatchFile is null" : batchFile.getBatchId(), batchFile.getFileUpload());

        if (batchFile.getFileUpload() == null) return null;
        List<DiscretionaryExtended> validRecords = new ArrayList<>();
        Batch batch = batchRepository.findOne(batchFile.getBatchId());

        batchService.updateBatchStatus(batch, StatusTypeCode.IN_PROGRESS.name());

        //Create Start-Time batch_event
        batchService.createBatchEvent(batch, new BatchEventDTO()
            .setType(BatchEventTypeCode.STRTIM.name())
            .setMessage(DateUtils.formatDatetimeUTC(new Date()))
        );

        List<Integer> errorIndexArray = new ArrayList<>();
        int recordCount = 0;

        StringBuilder columnHeaders = new StringBuilder(256);
        StringBuilder errors = new StringBuilder(1024);
        StringBuilder recordError = new StringBuilder(256);

        Long programId = null;
        List<BudgetUsedDTO> budgetUsedList = new ArrayList<>();
        boolean validColumnsHeaders = false;
        String payoutType = null;
        Boolean isPppxApplied = Boolean.FALSE;

        try (
            Reader in = new StringReader(batchFile.getFileUpload());
            CSVParser parser = new CSVParser(in, CSVFormat.EXCEL);
        ) {
            for (CSVRecord record : parser) {

                // the file header is recordCount = 0, so it doesn't get validated below
                if (recordCount != 0) {

                    //Reset the errors for this record
                    recordError.setLength(0);

                    DiscretionaryExtended discretionaryExt = new DiscretionaryExtended();

                    //Check for any missing data
                    if (record.get(0) == null || record.get(0).isEmpty()) recordError.append("Participant ID is required. ");
                    if (record.get(1) == null || record.get(1).isEmpty()) recordError.append("Point Amount is required. ");
                    if (record.get(2) == null || record.get(2).isEmpty()) recordError.append("Program Name is required. ");
                    if (record.get(3) == null || record.get(3).isEmpty()) recordError.append("Budget Name is required. ");

                    if (record.get(4) == null || record.get(4).isEmpty()) recordError.append("Description is required. ");
                    else if (record.get(4).length() > 40) recordError.append("Data in Description exceeds field length(40 char.) ");

                    discretionaryExt.setParticipantId(record.get(0));

                    //Only convert point amount if they send one
                    if (record.get(1) != null && !record.get(1).isEmpty()) {
                        try {
                            discretionaryExt.setPointAmount(Double.valueOf(record.get(1)));
                            discretionaryExt.setOriginalAmount(Double.valueOf(record.get(1)));
                        }
                        catch (NumberFormatException nfe) {
                            recordError.append("The Point Amount format is invalid. ");
                        }
                    }
                    discretionaryExt.setProgramName(record.get(2));
                    discretionaryExt.setBudgetName(record.get(3));
                    discretionaryExt.setDescription(record.get(4));

                    if (recordError.length() == 0) {
                        //No errors, continue
                        Long paxId = paxRepository.findPaxIdByControlNum(discretionaryExt.getParticipantId());
                        if (paxId != null) {

                            //Make sure the user is not INACTIVE or RESTRICTED
                            SysUser sysUser = sysUserRepository.findByPaxId(paxId).get(0);
                            String status = sysUser.getStatusTypeCode();

                            if (status != null && status.equalsIgnoreCase(StatusTypeCode.INACTIVE.name())) {
                                recordError.append("Participant ID is Inactive. ");
                            }
                            else if (status != null && status.equalsIgnoreCase(StatusTypeCode.RESTRICTED.name())) {
                                recordError.append("Participant ID is Restricted. ");
                            }

                            //Make sure the user is not terminated
                            Long paxGroupId = paxGroupService.getPaxGroupId(paxId);
                            if (paxGroupId == null) {
                                recordError.append("Participant has been terminated in the system. ");
                            }

                            discretionaryExt.getTransactHeader().setPaxGroupId(paxGroupId);
                            Program program = getProgramByName(discretionaryExt.getProgramName());
                            if (program != null && program.getProgramId() != null ) {
                                //Make sure this is a point load program
                                if (!program.getProgramTypeCode().equalsIgnoreCase(PROGRAM_ACTIVITY_TYPE_CODE)) {
                                    recordError.append("Invalid Program Type. ");
                                }

                                //Make sure the program is ACTIVE
                                if (!StatusTypeUtil.getImpliedStatus(program.getStatusTypeCode(), program.getFromDate(), program.getThruDate()).equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                                    recordError.append("Program is not active. ");
                                }

                                // Verify program's payout type.
                                payoutType = programMiscRepository.findBy()
                                        .where(ProjectConstants.PROGRAM_ID).eq(program.getProgramId())
                                        .and(ProjectConstants.VF_NAME).eq(VfName.SELECTED_AWARD_TYPE.name())
                                        .findOne(ProjectConstants.MISC_DATA, String.class);
                                if (payoutType == null) {
                                    recordError.append("Program's payout type is not set. ");
                                }
                                
                                if (recordError.length() == 0) {
                                    //No errors, continue

                                    //Save the first programID on the file. We can only have one program at a time
                                    if (recordCount == 1 || programId == null) {
                                        programId = program.getProgramId();
                                    }
                                    else {
                                        //Make sure there is not more than one program on the file
                                        if (!program.getProgramId().equals(programId)) {
                                            recordError.append("Multiple Programs on file. ");
                                        }
                                    }

                                    String pppxEnabled = programMiscRepository.findBy()
                                            .where(ProjectConstants.PROGRAM_ID).eq(program.getProgramId())
                                            .and(ProjectConstants.VF_NAME).eq(VfName.PPP_INDEX_ENABLED.name())
                                            .findOne(ProjectConstants.MISC_DATA, String.class);
                                    Boolean isPppxEnabled = Boolean.parseBoolean(pppxEnabled);
                                    
                                    ParticipantsPppIndexDTO partPppx = participantsPppIndexService.getParticipantsPppxByPaxIds(String.valueOf(paxId), null).get(0);
                                    if(isPppxEnabled && partPppx.getPppValue() != null){
                                        discretionaryExt.setPointAmount(Math.ceil(discretionaryExt.getPointAmount() * partPppx.getPppValue()));
                                        isPppxApplied = Boolean.TRUE;
                                    }

                                    Budget budget = budgetRepository.findBy()
                                           .where(ProjectConstants.BUDGET_NAME).eq(discretionaryExt.getBudgetName())
                                           .findOne();

                                    if (budget != null) {
                                        //Make sure this budget is associated to the specified program
                                        Map<String, Object> programBudget = pointUploadDao.getProgramBudget(program.getProgramId(), budget.getId(), budget.getBudgetName());
                                        if (programBudget == null) {
                                            recordError.append("Program associated to invalid Budget. ");
                                        }

                                        //Make sure this budget is ACTIVE
                                        if (!StatusTypeUtil.getImpliedStatus(budget.getStatusTypeCode(), budget.getFromDate(), budget.getThruDate()).equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                                            recordError.append("Budget is no longer active. ");
                                        }

                                        // Grab default project/subproject from the budget
                                        SubProject sp = subProjectRepository.findOne(budget.getSubProjectId());
                                        String subProjectNumber = sp.getSubProjectNumber();
                                        String projectNumber = projectRepository.findOne(sp.getProjectId()).getProjectNumber();

                                        // Check for country-based overrides for project/subproject number
                                        BudgetMisc bm = budgetMiscRepository.findBy()
                                                .where(ProjectConstants.BUDGET_ID).eq(budget.getId())
                                                .and(ProjectConstants.MISC_TYPE_CODE).eq(VfName.SUB_PROJECT_OVERRIDE).findOne();                                                

                                        if (bm != null) {
                                            String recipientCountry = addressRepository.findPreferredByPaxId(paxId).getCountryCode();
                                            if (recipientCountry != null) {
                                                SubProject overrideSp = subProjectRepository.findOne(Long.valueOf(bm.getMiscData()));
                                                String overrideSubProjectNumber = overrideSp.getSubProjectNumber();
                                                String overrideProjectNumber = projectRepository.findOne(overrideSp.getProjectId()).getProjectNumber();

                                                // Only apply override if the recipient lives in an applicable country
                                                if (VfName.US_OVERRIDE.name().equals(bm.getVfName()) 
                                                        && US_COUNTRY_CODES.contains(recipientCountry)) {
                                                    subProjectNumber = overrideSubProjectNumber;
                                                    projectNumber = overrideProjectNumber;
                                                } else if (VfName.NON_US_OVERRIDE.name().equals(bm.getVfName())
                                                        && !US_COUNTRY_CODES.contains(recipientCountry)) {
                                                    subProjectNumber = overrideSubProjectNumber;
                                                    projectNumber = overrideProjectNumber;
                                                }                                                    
                                            } else {
                                                recordError.append("Recipient does not have a valid address");
                                            }
                                        }

                                        discretionaryExt.setPayoutType(payoutType);
                                        discretionaryExt.setProjectNumber(projectNumber);
                                        discretionaryExt.setSubProjectNumber(subProjectNumber);
                                        logger.info("PointUploadProcessor - BatchId: {} Payout Type: {} Project num: {} subproject num: {}",
                                                batch.getBatchId(), payoutType, projectNumber, subProjectNumber);
                                        if (recordError.length() == 0) {
                                            //No errors, continue                                               

                                            //Handle negative amounts
                                            if (discretionaryExt.getPointAmount() > 0) discretionaryExt.getDiscretionary().setBudgetIdDebit(budget.getId());
                                            if (discretionaryExt.getPointAmount() < 0) discretionaryExt.getDiscretionary().setBudgetIdCredit(budget.getId());
                                            discretionaryExt.getDiscretionary().setAmount(Math.abs(discretionaryExt.getPointAmount()));
                                            discretionaryExt.getDiscretionary().setPerformanceDate(new Date());
                                            discretionaryExt.getDiscretionary().setDescription(discretionaryExt.getDescription());
                                            discretionaryExt.getTransactHeader().setStatusTypeCode(StatusTypeCode.PENDING.name());
                                            discretionaryExt.getTransactHeader().setTypeCode(TransactionTypeCode.DISC.name());
                                            discretionaryExt.getTransactHeader().setSubTypeCode(TransactionTypeCode.DISC.name());
                                            discretionaryExt.getTransactHeader().setActivityDate(new Date());
                                            discretionaryExt.getTransactHeader().setBatchId(batchFile.getBatchId());
                                            discretionaryExt.getTransactHeader().setProgramId(programId);
                                            validRecords.add(discretionaryExt);

                                            //Keep track of the total points used in each budget
                                            if (recordCount == 1) {
                                                //First record - create a new budgetUsedDTO
                                                BudgetUsedDTO budgetUsedDto = new BudgetUsedDTO();
                                                budgetUsedDto.setBudgetId(budget.getId());
                                                budgetUsedDto.setPointsUploaded(discretionaryExt.getPointAmount());
                                                budgetUsedList.add(budgetUsedDto);
                                            }
                                            else {
                                                boolean dtoExists = false;
                                                //Check if this budget already has a DTO
                                                for (BudgetUsedDTO budgetUsedDto : budgetUsedList) {
                                                    if (budgetUsedDto.getBudgetId().equals(budget.getId())) {
                                                        Double currentPoints = budgetUsedDto.getPointsUploaded();
                                                        currentPoints += discretionaryExt.getPointAmount();
                                                        budgetUsedDto.setPointsUploaded(currentPoints);
                                                        dtoExists = true;
                                                        break;
                                                    }
                                                }

                                                if (!dtoExists) {
                                                    //Create a new BudgetUsedDTO and add it to the list
                                                    BudgetUsedDTO buDTO = new BudgetUsedDTO();
                                                    buDTO.setBudgetId(budget.getId());
                                                    buDTO.setPointsUploaded(discretionaryExt.getPointAmount());
                                                    budgetUsedList.add(buDTO);
                                                }
                                            }

                                            recordCount++;
                                            continue;
                                        }
                                    }
                                    else {
                                        //Budget ID is null
                                        recordError.append("Budget not found. ");
                                    }
                                }
                            }
                            else {
                                //Program Activity ID is null
                                recordError.append("Invalid Program Name. ");
                            }
                        }
                        else {
                            //Pax_ID is null
                            recordError.append("Participant ID not found. ");
                        }
                    }
                }
                else {
                    recordCount++;
                    validColumnsHeaders = validateColumnsHeaders(record);
                    if (validColumnsHeaders) {
                        for (int i = 0; i < record.size(); i++) {
                            columnHeaders.append("\"").append(record.get(i)).append("\",");
                        }
                        columnHeaders.append("\"Error(s)\"\n");
                        continue;
                    }

                       errorIndexArray.add(0);
                       break;
                }

                errorIndexArray.add(recordCount);
                recordCount++;

                //Add this record to the errors
                for (int i = 0; i < record.size(); i++) {
                    errors.append("\"").append(record.get(i)).append("\",");
                }
                errors.append("\"").append(recordError).append("\"\n");
            }
        }

        //Create batch events for total records, ok records, error records, and ppp index applied record
        batchService.createBatchEvents(batch, Arrays.asList(
            new BatchEventDTO()
                .setType(BatchEventTypeCode.RECS.name())
                .setMessage(String.valueOf(recordCount-1))
           ,new BatchEventDTO()
               .setType(BatchEventTypeCode.OKRECS.name())
               .setMessage(String.valueOf(validRecords.size()))
           ,new BatchEventDTO()
               .setType(BatchEventTypeCode.ERRS.name())
               .setMessage(String.valueOf(errorIndexArray.size()))
           ,new BatchEventDTO()
               .setType("PPP_INDEX_APPLIED")
               .setMessage(String.valueOf(isPppxApplied))
        ));

        if (!errorIndexArray.isEmpty() || !validColumnsHeaders || --recordCount == 0) {
            if (recordCount == 0) {
                batchService.endBatch(batch);
            }
            else {
                if (!validColumnsHeaders) {
                    batchFileService.updateBatchFileError(batchFile, INVALID_FILE_FORMAT);
                }
                else {
                    //Save the errors in the BATCH_FILE table
                    batchFileService.updateBatchFileError(batchFile, columnHeaders.append(errors).toString());
                }

                batchService.endBatch(batch.setStatusTypeCode(StatusTypeCode.ERROR.name()));
            }
            logger.info("Point Upload Processor - Process() - line 485");
            return null;
        }
        else if (batch != null && !validRecords.isEmpty()) {

            boolean budgetExceeded = false;
            for (BudgetUsedDTO budgetUsedDto : budgetUsedList) {
                //Get current budget available for each budget on the file
                budgetUsedDto.setBudgetAvailable(pointUploadDao.getBudgetAvailable(budgetUsedDto.getBudgetId()));

                //Make sure there are enough points in the budget
                if (budgetUsedDto.getPointsUploaded() > budgetUsedDto.getBudgetAvailable()) {
                    budgetExceeded = true;
                    break;
                }
            }

            if (budgetExceeded) {
                //Don't process anything if budget exceeded
                batchService.endBatch(batch.setStatusTypeCode(StatusTypeCode.BUDGET_EXCEEDED.name()));
                return null;
            }
            logger.info("Point Upload Processor - Process() ln 507 Returned valid records");
            return validRecords;
        }
        logger.info("Point Upload Processor - Process() - ln 510 Returned null" );
        return null;
    }

    private boolean validateColumnsHeaders(CSVRecord headers) {
        int headerCount = headers.size();
        if (headerCount != PointUploadFileHeadersEnum.getRecordCount()) {
            return false;
        }
        boolean validHeader;
        for (int index=0; index<headers.size(); index++) {
            validHeader = false;
            switch (index) {
                case 0: validHeader =
                        headers.get(index).toUpperCase().contains(PointUploadFileHeadersEnum.PARTICIPANT_ID.getHeaderName());
                        break;
                case 1: validHeader =
                        headers.get(index).toUpperCase().contains(PointUploadFileHeadersEnum.AWARD_AMOUNT.getHeaderName());
                        break;
                case 2: validHeader =
                        headers.get(index).toUpperCase().contains(PointUploadFileHeadersEnum.PROGRAM_NAME.getHeaderName());
                        break;
                case 3: validHeader =
                        headers.get(index).toUpperCase().contains(PointUploadFileHeadersEnum.BUDGET_NAME.getHeaderName());
                        break;
                case 4: validHeader =
                        headers.get(index).toUpperCase().contains(PointUploadFileHeadersEnum.DESCRIPTION.getHeaderName());
                        break;
            } if(validHeader) {
                continue;
            }
            return false;
        } return true;
    }

    protected Program getProgramByName(String progName) {
        return programRepository.findBy()
            .where(ProjectConstants.PROGRAM_NAME).eq(progName)
            .findOne()
        ;
    }
}
