package com.maritz.culturenext.pointupload.dto;

import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeader;

public class DiscretionaryExtended {

    private String participantId;
    private Double pointAmount;
    private String programName;
    private String budgetName;
    private String description;
    private String payoutType;
    private String projectNumber;
    private String subProjectNumber;
    private TransactionHeader transactHeader = new TransactionHeader();
    private Discretionary discretionary = new Discretionary();
    private Double originalAmount;

    public Discretionary getDiscretionary() {
        return this.discretionary;
    }

    public void setDiscretionary(Discretionary discretionary) {
        this.discretionary = discretionary;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public Double getPointAmount() {
        return pointAmount;
    }

    public void setPointAmount(Double pointAmount) {
        this.pointAmount = pointAmount;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }

    public String getSubProjectNumber() {
        return subProjectNumber;
    }

    public void setSubProjectNumber(String subProjectNumber) {
        this.subProjectNumber = subProjectNumber;
    }

    public TransactionHeader getTransactHeader() {
        return transactHeader;
    }

    public void setTransactHeader(TransactionHeader transactHeader) {
        this.transactHeader = transactHeader;
    }

    public Double getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(Double originalAmount) {
        this.originalAmount = originalAmount;
    }

    @Override
    public String toString() {
        return "BudgetAllocationExtended [participantId=" + participantId + ", pointAmount=" + pointAmount
                + ", programName=" + programName + ", budgetName=" + budgetName + ", description=" + description
                + ", transactHeader=" + transactHeader + ", originalAmount=" + originalAmount + "]";
    }

}
