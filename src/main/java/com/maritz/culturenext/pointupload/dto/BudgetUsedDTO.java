package com.maritz.culturenext.pointupload.dto;

public class BudgetUsedDTO {

    private Long budgetId;
    private Double pointsUploaded;
    private Double budgetAvailable;

    public BudgetUsedDTO() {
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public Double getPointsUploaded() {
        return pointsUploaded;
    }

    public void setPointsUploaded(Double pointsUploaded) {
        this.pointsUploaded = pointsUploaded;
    }

    public Double getBudgetAvailable() {
        return budgetAvailable;
    }

    public void setBudgetAvailable(Double budgetAvailable) {
        this.budgetAvailable = budgetAvailable;
    }

}
