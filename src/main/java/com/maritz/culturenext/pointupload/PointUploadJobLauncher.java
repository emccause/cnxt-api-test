package com.maritz.culturenext.pointupload;

import javax.inject.Inject;

import org.springframework.batch.core.Job;

import com.maritz.batch.launcher.BatchJobLauncher;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.task.annotation.TriggeredTask;


public class PointUploadJobLauncher {

    protected BatchJobLauncher batchJobLauncher;
    protected Job pointUploadJob;

    @Inject
    public PointUploadJobLauncher setBatchJobLauncher(BatchJobLauncher batchJobLauncher) {
        this.batchJobLauncher = batchJobLauncher;
        return this;
    }

    @Inject
    public PointUploadJobLauncher setPointUploadJob(Job pointUploadJob) {
        this.pointUploadJob = pointUploadJob;
        return this;
    }

    @TriggeredTask
    public void pointUploadJob(Batch batch) throws Exception {
        batchJobLauncher.runJob(batch, pointUploadJob);
    }

}