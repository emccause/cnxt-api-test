package com.maritz.culturenext.pointupload.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.pointupload.dao.PointUploadDao;

@Component
public class PointUploadDaoImpl extends AbstractDaoImpl implements PointUploadDao {
    
    private static final String BUDGET_ID = "budgetId";
    private static final String BUDGET_NAME = "budgetName";
    private static final String PROGRAM_ID = "programId";

    private static final String BUDGET_USED_QUERY =
        "select " +
        "(CASE " +
                "when DEBITS.DEBITS is null then 0 " +
                "else DEBITS.DEBITS " +
        "end) " +
        "- " +
        "(CASE " +
            "when CREDITS.CREDITS is null then 0 " +
            "else CREDITS.CREDITS   " +
        "end) " +
        "AS 'TOTAL_USED' " +
        "from component.budget b " +
        "left join ( " +
            "select b.id, SUM(disc_debit.amount) AS 'DEBITS' " +
            "from component.budget b  " +
            "left join component.DISCRETIONARY disc_debit " +
            "on disc_debit.budget_id_debit = b.id " +
            "join component.transaction_header th_debit " +
            "on th_debit.id = disc_debit.transaction_header_id " +
            "and th_debit.TYPE_CODE = 'DISC' " +
            "and th_debit.SUB_TYPE_CODE <> 'FUND' " +
            "and th_debit.status_type_code in ('PENDING', 'APPROVED') " +
            "group by b.id " +
        ")DEBITS on b.id = DEBITS.id " +
        "left join ( " +
            "select b.id,SUM(disc_credit.amount) 'CREDITS' " +
            "from component.budget b  " +
            "left join component.DISCRETIONARY disc_credit " +
            "on disc_credit.budget_id_credit = b.id " +
            "join component.transaction_header th_credit " +
            "on th_credit.id = disc_credit.transaction_header_id " +
            "and th_credit.TYPE_CODE = 'DISC' " +
            "and th_credit.SUB_TYPE_CODE <> 'FUND' " +
            "and th_credit.status_type_code in ('PENDING', 'APPROVED') " +
            "group by b.id " +
        ") CREDITS on b.id = CREDITS.id " +
        "where b.id = :budgetId ";

    private static final String BUDGET_TOTAL_QUERY =
        "select " +
        "(CASE " +
            "when CREDITS.CREDITS is null then 0 " +
            "else CREDITS.CREDITS " +
        "end) " +
        "- " +
        "(CASE " +
            "when DEBITS.DEBITS is null then 0 " +
            "else DEBITS.DEBITS " +
        "end) " +
        "AS 'TOTAL_AMOUNT' " +
        "from component.budget b " +
        "left join ( " +
        "select b.id, SUM(disc_debit.amount) AS 'DEBITS' " +
        "from component.budget b " +
        "left join component.DISCRETIONARY disc_debit " +
        "on disc_debit.budget_id_debit = b.id " +
        "join component.transaction_header th_debit " +
        "on th_debit.id = disc_debit.transaction_header_id " +
        "and th_debit.TYPE_CODE = 'DISC' " +
        "and th_debit.SUB_TYPE_CODE = 'FUND' " +
        "group by b.id )DEBITS " +
        "on b.id = DEBITS.id " +
        "left join ( " +
        "select b.id, SUM(disc_credit.amount) AS 'CREDITS' " +
        "from component.budget b " +
        "left join component.DISCRETIONARY disc_credit " +
        "on disc_credit.budget_id_credit = b.id " +
        "join component.transaction_header th_credit " +
        "on th_credit.id = disc_credit.transaction_header_id " +
        "and th_credit.TYPE_CODE = 'DISC' " +
        "and th_credit.SUB_TYPE_CODE = 'FUND' " +
        "group by b.id ) CREDITS " +
        "on b.id = CREDITS.id " +
        "where b.id = :budgetId ";

    private static final String PROGRAM_BUDGET_QUERY =
        "SELECT b.* " +
        "FROM component.PROGRAM_BUDGET pb " +
        "     inner join component.budget b on b.id = pb.budget_id" +
        "            and b.budget_name = :budgetName " +
        "LEFT JOIN  " +
        "( " +
        "SELECT * FROM component.BUDGET " +
        "WHERE BUDGET_TYPE_CODE = 'DISTRIBUTED' AND ID = :budgetId " +
        ") b_distributed ON b_distributed.PARENT_BUDGET_ID = pb.BUDGET_ID " +
        "LEFT JOIN "  +
        "( " +
        "SELECT * FROM component.BUDGET " +
        "WHERE BUDGET_TYPE_CODE = 'SIMPLE' AND ID = :budgetId " +
        ") b_simple ON b_simple.ID = pb.BUDGET_ID " +
        "WHERE pb.PROGRAM_ID = :programId " +
        "AND (b_distributed.PARENT_BUDGET_ID = pb.BUDGET_ID " +
        "OR b_simple.ID = pb.BUDGET_ID) ";
            
    @Override
    public Map<String, Object> getProgramBudget(Long programId, Long budgetId, String budgetName) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID, programId);
        params.addValue(BUDGET_ID, budgetId);
        params.addValue(BUDGET_NAME, budgetName);

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(PROGRAM_BUDGET_QUERY, params, new CamelCaseMapRowMapper());
        if (!nodes.isEmpty()) {
            return nodes.get(0);
        }

        return null;
    }

    @Override
    public Double getBudgetAvailable(Long budgetId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BUDGET_ID, budgetId);

        Double totalBudget = namedParameterJdbcTemplate.queryForObject(BUDGET_TOTAL_QUERY, params, Double.class);
        Double totalUsed = namedParameterJdbcTemplate.queryForObject(BUDGET_USED_QUERY, params, Double.class);

        return (totalBudget - totalUsed);
    }
}
