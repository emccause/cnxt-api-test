package com.maritz.culturenext.pointupload.dao;

import java.util.Map;

public interface PointUploadDao {

    /**
     * TODO JavaDocs
     * @param programId
     * @param budgetId
     * @param budgetName
     * @return
     */
    Map<String, Object> getProgramBudget(Long programId, Long budgetId, String budgetName);

    /**
     * TODO JavaDocs
     * @param budgetId
     * @return
     */
    Double getBudgetAvailable(Long budgetId);

}
