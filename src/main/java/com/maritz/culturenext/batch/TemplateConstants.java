package com.maritz.culturenext.batch;

import com.maritz.core.rest.ErrorMessage;

public class TemplateConstants {
    public static final String HEADERS = "headers";
    public static final String FIELD_MAPPING = "fieldMapping";
    public static final String PROCESSORS = "processors";
    public static final String RECOGNITION_TEMPLATE = "Recognition_template";
    public static final String TYPE = "type";

    //cvs columns
    public static final String PARTICIPANT_FIRST_NAME = "Participant First Name";
    public static final String PARTICIPANT_LAST_NAME = "Participant Last Name";
    public static final String PARTICIPANT_ID_MANDATORY = "Participant ID - Mandatory";
    public static final String AWARD_AMOUNT = "Award Amount";

    //errors
    public static final String ERR_INVALID_TEMPLATE = "INVALID_TEMPLATE";
    public static final String ERR_INVALID_MSG = "Invalid template. Valid template types are ADV_REC_BULK_UPLOAD, CPTD, and ADV_REC_UPLOAD.";

    //Error Message Constants
    public static final ErrorMessage INVALID_TEMPLATE_TYPE_MESSAGE = new ErrorMessage()
            .setCode(ERR_INVALID_TEMPLATE)
            .setField(TYPE)
            .setMessage(ERR_INVALID_MSG);
}
