package com.maritz.culturenext.batch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public final class BatchConstants {
    
    //REST URI's for ecard image and client logo
    public static final String LOGO_URI = "/rest/projects/~/images/logo";
    public static final String DEEP_LINK_NOTIFICATIONS_URI = "/#/notifications/";
    public static final String DEEP_LINK_PROFILE_URI = "/profile#/";
    public static final String DEEP_LINK_REPORTING_URI = "/reporting#/";
    public static final String DEEP_LINK_CALENDAR_URI = "/calendar#/";
    public static final String DEEP_LINK_SETTINGS_URI = "/profile-settings#/";
    public static final String DEEP_LINK_GIVEREC_MODAL = "/#/?modal=giveRec";

    // Errors
    private static final String ERROR_INVALID_FROM_DATE = "INVALID_FROM_DATE";
    private static final String ERROR_INVALID_FROM_DATE_MSG = "Specified fromDate is invalid.";
    private static final String ERROR_FROM_DATE_IN_FUTURE = "FROM_DATE_IN_FUTURE";
    private static final String ERROR_FROM_DATE_IN_FUTURE_MSG = "Specified fromDate should not be in the future";

    private static final String ERROR_MISSING_UPDATE_REQUEST = "MISSING_UPDATE_REQUEST";
    private static final String ERROR_MISSING_UPDATE_REQUEST_MSG = "Request to update Point Load activity is missing";
    private static final String ERROR_MISSING_BATCH_ID = "MISSING_BATCH_ID";
    private static final String ERROR_MISSING_BATCH_ID_MSG = "Batch ID is required for updating a Point Load activity";
    private static final String ERROR_INVALID_BATCH_ID = "INVALID_BATCH_ID";
    private static final String ERROR_INVALID_BATCH_ID_MSG = "Specified Batch ID is not valid";
    private static final String ERROR_MISSING_ACTION = "MISSING_ACTION";
    private static final String ERROR_MISSING_ACTION_MSG = "Action is required for updating a Point Load activity";
    private static final String ERROR_INVALID_ACTION = "INVALID_ACTION";
    private static final String ERROR_INVALID_ACTION_MSG = "Specified action is not valid";
    private static final String ERROR_INVALID_ACTION_TRANSITION = "INVALID_ACTION_TRANSITION";
    private static final String ERROR_INVALID_ACTION_TRANSITION_MSG = "Specified action cannot be performed on the "
        + "batch in its current status";
    private static final String ERROR_NON_POINT_LOAD_BATCH = "NON_POINT_LOAD_BATCH";
    private static final String ERROR_NON_POINT_LOAD_BATCH_MSG = "Specified batch is not a Point Load batch";

    private static final String ERROR_BATCH_NOT_FOUND = "BATCH_NOT_FOUND";
    private static final String BATCH_NOT_FOUND_FIELD = "batch";
    public static final String BATCH_NOT_FOUND_MSG = "No batch found for the given batch ID: ";
    private static final String ERROR_STATUS_MISSING = "STATUS_MISSING";
    private static final String STATUS_MISSING_MSG = "The updated status is missing";
    private static final String ERROR_STATUS_NOT_UPDATEABLE = "STATUS_NOT_UPDATEABLE";
    public static final String STATUS_NOT_UPDATEABLE_MSG = "This batch's status is not allowed to be updated: ";
    private static final String ERROR_STATUS_NOT_ALLOWED = "STATUS_NOT_ALLOWED";
    public static final String STATUS_NOT_ALLOWED_MSG = "This status cannot be applied to a batch: ";
    
    //ErrorMessage Constants
    public static final ErrorMessage INVALID_FROM_DATE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_FROM_DATE)
            .setField(ProjectConstants.FROM_DATE)
            .setMessage(ERROR_INVALID_FROM_DATE_MSG);

    public static final ErrorMessage FROM_DATE_IN_FUTURE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_FROM_DATE_IN_FUTURE)
            .setField(ProjectConstants.FROM_DATE)
            .setMessage(ERROR_FROM_DATE_IN_FUTURE_MSG);

    public static final ErrorMessage MISSING_UPDATE_REQUEST = new ErrorMessage()
            .setCode(ERROR_MISSING_UPDATE_REQUEST)
            .setMessage(ERROR_MISSING_UPDATE_REQUEST_MSG);

    public static final ErrorMessage MISSING_BATCH_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_BATCH_ID)
            .setField(ProjectConstants.BATCH_ID)
            .setMessage(ERROR_MISSING_BATCH_ID_MSG);

    public static final ErrorMessage INVALID_BATCH_ID_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_BATCH_ID)
            .setField(ProjectConstants.BATCH_ID)
            .setMessage(ERROR_INVALID_BATCH_ID_MSG);

    public static final ErrorMessage MISSING_ACTION_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_ACTION)
            .setField(ProjectConstants.ACTION)
            .setMessage(ERROR_MISSING_ACTION_MSG);

    public static final ErrorMessage INVALID_ACTION_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_ACTION)
            .setField(ProjectConstants.ACTION)
            .setMessage(ERROR_INVALID_ACTION_MSG);

    public static final ErrorMessage NON_POINT_LOAD_BATCH_MESSAGE = new ErrorMessage()
            .setCode(ERROR_NON_POINT_LOAD_BATCH)
            .setMessage(ERROR_NON_POINT_LOAD_BATCH_MSG);

    public static final ErrorMessage INVALID_ACTION_TRANSITION_MESSAGE = new ErrorMessage()
            .setCode(ERROR_INVALID_ACTION_TRANSITION)
            .setMessage(ERROR_INVALID_ACTION_TRANSITION_MSG);

    public static final ErrorMessage BATCH_NOT_FOUND_MESSAGE = new ErrorMessage()
            .setCode(ERROR_BATCH_NOT_FOUND)
            .setField(BATCH_NOT_FOUND_FIELD)
            .setMessage(BATCH_NOT_FOUND_MSG);

    public static final ErrorMessage STATUS_MISSING_MESSAGE = new ErrorMessage()
            .setCode(ERROR_STATUS_MISSING)
            .setField(ProjectConstants.STATUS)
            .setMessage(STATUS_MISSING_MSG);

    public static final ErrorMessage STATUS_NOT_UPDATEABLE_MESSAGE = new ErrorMessage()
            .setCode(ERROR_STATUS_NOT_UPDATEABLE)
            .setField(ProjectConstants.STATUS)
            .setMessage(STATUS_NOT_UPDATEABLE_MSG);

    public static final ErrorMessage STATUS_NOT_ALLOWED_MESSAGE = new ErrorMessage()
            .setCode(ERROR_STATUS_NOT_ALLOWED)
            .setField(ProjectConstants.STATUS)
            .setMessage(STATUS_NOT_ALLOWED_MSG);

    // Statuses
    public static final String ALERT_SUB_TYPE_RELEASE = "RELEASE";

    public static final List<String> STAGED_STATUSES = Arrays.asList(StatusTypeCode.NEW.name(),
                                                                     StatusTypeCode.IN_PROGRESS.toString(),
                                                                     StatusTypeCode.PENDING_RELEASE.toString());

    public static final List<String> UPDATEABLE_STATUSES = Arrays.asList(
        StatusTypeCode.PENDING_RELEASE.name()
    );

    public static final List<String> STATUS_UPDATE_OPTIONS = Arrays.asList(
        StatusTypeCode.CANCELLED.name(),
        StatusTypeCode.QUEUED.name()
    );

    /**
     * Get list of all default statuses.
     *
     * @return list of all default statuses
     */
    public static List<String> defaultStatuses() {
        List<String> defaultStatuses = new ArrayList<>();

        for(StatusTypeCode status : StatusTypeCode.values()) {
            if(!StatusTypeCode.ARCHIVED.name().equals(status.name())) {
                defaultStatuses.add(status.name());
            }
        }

        return defaultStatuses;
    }

    /*
     * The caller references the constants using BatchConstants.EMAIL_BLAST_EVENT,
     * and so on. Thus, the caller should be prevented from constructing objects of
     * this class, by declaring this private constructor.
     */
    private BatchConstants() {
        //this prevents even the native class from calling this constructor as well
        throw new AssertionError();
    }
}