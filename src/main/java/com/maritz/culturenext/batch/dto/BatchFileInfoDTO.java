package com.maritz.culturenext.batch.dto;

import com.maritz.culturenext.util.DateUtil;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;

/**
 * Abstract class that contains common elements for BatchFile information.
 */
@ApiModel(description = "DTO representing common information for a batch file. Information includes"
        + " the ID of the batch record, the start date of the batch process, the total number of records "
        + "processed in the batch, the number of records that were successfully processed as well as failed, "
        + "the status of the batch process, and the name of the file associated with the batch process.")
public class BatchFileInfoDTO {
    protected Long batchId;
    protected String startDate;
    protected Integer totalRecords;
    protected Integer goodRecords;
    protected Integer errorRecords;
    protected String status;
    protected String fileName;
    protected Boolean fileOnServer;

    @ApiModelProperty(position = 1, required = true, value = "ID of the batch record")
    public Long getBatchId() {
        return batchId;
    }

    public BatchFileInfoDTO setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    @ApiModelProperty(position = 2, required = false, value = "start date of the batch process")
    public String getStartDate() {
        return startDate;
    }

    public BatchFileInfoDTO setStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    @ApiModelProperty(position = 3, required = false, value = "total number of records processed by the batch process")
    public Integer getTotalRecords() {
        return totalRecords;
    }

    public BatchFileInfoDTO setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
        return this;
    }

    @ApiModelProperty(position = 4, required = false,
            value = "number of records successfully processed by the batch process")
    public Integer getGoodRecords() {
        return goodRecords;
    }

    public BatchFileInfoDTO setGoodRecords(Integer goodRecords) {
        this.goodRecords = goodRecords;
        return this;
    }

    @ApiModelProperty(position = 5, required = false,
            value = "number of records that encountered a failure while being processed by the batch process")
    public Integer getErrorRecords() {
        return errorRecords;
    }

    public BatchFileInfoDTO setErrorRecords(Integer errorRecords) {
        this.errorRecords = errorRecords;
        return this;
    }

    @ApiModelProperty(position = 6, required = false, value = "status of the batch process")
    public String getStatus() {
        return status;
    }

    public BatchFileInfoDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    @ApiModelProperty(position = 7, required = false, value = "name of the file associated with the batch process")
    public String getFileName() {
        return fileName;
    }

    public BatchFileInfoDTO setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }
    
    @ApiModelProperty(position = 8, required = false, value = "TRUE if there is an enrollment file currently on the server")
    public Boolean getFileOnServer() {
        return fileOnServer;
    }
    
    public BatchFileInfoDTO setFileOnServer(Boolean fileOnServer) {
        this.fileOnServer = fileOnServer;
        return this;
    }

    /**
     * Formats the date value provided within the object mapping.
     *
     * @param objectMapValue value provided within the object mapping
     * @return formatted string representation of the date value in the object mapping, null otherwise
     */
    protected String convertDateObjectMapValue(Object objectMapValue) {
        if (objectMapValue == null) {
            return null;
        }

        if (objectMapValue instanceof String) {
            if (((String) objectMapValue).contains("T")) {
                objectMapValue = ((String) objectMapValue).replace("T", " ");
            }
            if (((String) objectMapValue).contains(".")) {
                objectMapValue = ((String) objectMapValue).substring(0, ((String) objectMapValue).length() - 4);
            }
            Date date = DateUtil.convertFromString((String) objectMapValue);
            return date != null ? DateUtil.convertToUTCDateTime(date) : null;
        } else {
            // not instance of expected type so just return null
            return null;
        }
    }
}
