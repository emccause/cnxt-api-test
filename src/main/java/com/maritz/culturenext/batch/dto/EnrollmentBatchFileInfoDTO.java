package com.maritz.culturenext.batch.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BatchFile DTO used in conjunction with Enrollment information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing batch file information for an enrollment process. "
        + "Includes common information as the date the batch process was set up and the finish date of "
        + "the batch process.")
public class EnrollmentBatchFileInfoDTO extends BatchFileInfoDTO {
    private String setupDate;
    private String finishDate;

    public EnrollmentBatchFileInfoDTO() { }

    @ApiModelProperty(position = 8, required = false, value = "date that the batch process was set up")
    public String getSetupDate() {
        return setupDate;
    }

    public void setSetupDate(String setupDate) {
        this.setupDate = setupDate;
    }

    @ApiModelProperty(position = 9, required = false, value = "date that the batch process finished")
    public String getFinishDate() {
        return finishDate;
    }

    public EnrollmentBatchFileInfoDTO setFinishDate(String finishDate) {
        this.finishDate = finishDate;
        return this;
    }
}
