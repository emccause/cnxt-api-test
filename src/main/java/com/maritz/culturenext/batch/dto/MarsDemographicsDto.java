package com.maritz.culturenext.batch.dto;

/**
 * DTO used for MarsDemographicsDaoImpl.getDemographicsUpdate
 * @author muddam
 *
 */
public class MarsDemographicsDto {

    private String cardTypeCode;
    private String cardNum;
    private Long paxId;
    private String controlNum;
    private String userName;
    private String password;
    private String projectNumber;
    private String subProjectNumber;
    private String lastName;
    private String firstName;
    private String middleName;
    private String nameSuffix;
    private String emailAddress;
    private String address1;
    private String address2;
    private String address3;
    private String address4;
    private String address5;
    private String address6;
    private String city;
    private String state;
    private String zip;
    private String countryCode;
    private String participantNightPhone;
    private String participantFaxPhone;
    private String participantDayPhone;
    
    public MarsDemographicsDto() {
    }

    public String getCardTypeCode() {
        return cardTypeCode;
    }

    public MarsDemographicsDto setCardTypeCode(String cardTypeCode) {
        this.cardTypeCode = cardTypeCode;
        return this;
    }

    public String getCardNum() {
        return cardNum;
    }

    public MarsDemographicsDto setCardNum(String cardNum) {
        this.cardNum = cardNum;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }

    public MarsDemographicsDto setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public String getControlNum() {
        return controlNum;
    }

    public MarsDemographicsDto setControlNum(String controlNum) {
        this.controlNum = controlNum;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public MarsDemographicsDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public MarsDemographicsDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getProjectNumber() {
        return projectNumber;
    }

    public MarsDemographicsDto setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
        return this;
    }

    public String getSubProjectNumber() {
        return subProjectNumber;
    }

    public MarsDemographicsDto setSubProjectNumber(String subProjectNumber) {
        this.subProjectNumber = subProjectNumber;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public MarsDemographicsDto setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public MarsDemographicsDto setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public MarsDemographicsDto setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public MarsDemographicsDto setNameSuffix(String nameSuffix) {
        this.nameSuffix = nameSuffix;
        return this;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public MarsDemographicsDto setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public String getAddress1() {
        return address1;
    }

    public MarsDemographicsDto setAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public String getAddress2() {
        return address2;
    }

    public MarsDemographicsDto setAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public String getAddress3() {
        return address3;
    }

    public MarsDemographicsDto setAddress3(String address3) {
        this.address3 = address3;
        return this;
    }

    public String getAddress4() {
        return address4;
    }

    public MarsDemographicsDto setAddress4(String address4) {
        this.address4 = address4;
        return this;
    }

    public String getAddress5() {
        return address5;
    }

    public MarsDemographicsDto setAddress5(String address5) {
        this.address5 = address5;
        return this;
    }

    public String getAddress6() {
        return address6;
    }

    public MarsDemographicsDto setAddress6(String address6) {
        this.address6 = address6;
        return this;
    }

    public String getCity() {
        return city;
    }

    public MarsDemographicsDto setCity(String city) {
        this.city = city;
        return this;
    }

    public String getState() {
        return state;
    }

    public MarsDemographicsDto setState(String state) {
        this.state = state;
        return this;
    }

    public String getZip() {
        return zip;
    }

    public MarsDemographicsDto setZip(String zip) {
        this.zip = zip;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public MarsDemographicsDto setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getParticipantNightPhone() {
        return participantNightPhone;
    }

    public MarsDemographicsDto setParticipantNightPhone(String participantNightPhone) {
        this.participantNightPhone = participantNightPhone;
        return this;
    }

    public String getParticipantFaxPhone() {
        return participantFaxPhone;
    }

    public MarsDemographicsDto setParticipantFaxPhone(String participantFaxPhone) {
        this.participantFaxPhone = participantFaxPhone;
        return this;
    }

    public String getParticipantDayPhone() {
        return participantDayPhone;
    }

    public MarsDemographicsDto setParticipantDayPhone(String participantDayPhone) {
        this.participantDayPhone = participantDayPhone;
        return this;
    }
    
}