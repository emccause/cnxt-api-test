package com.maritz.culturenext.batch.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.util.ConversionUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * BatchFile DTO used in conjunction with Point Load program information.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing batch file information for a Point Load process. Includes common batch "
        + "information as well as the processing date of the Point Load activity and the total points "
        + "associated with the Point Load action.")
public class PointLoadBatchFileInfoDTO extends BatchFileInfoDTO {

    // node object mapping column names
    private static final String BATCH_ID = "BATCH_ID";
    private static final String STATUS = "STATUS";
    private static final String START_DATE = "START_DATE";
    private static final String PROCESS_DATE = "PROCESS_DATE";
    private static final String FILE_NAME = "FILE_NAME";
    private static final String TOTAL_RECORDS = "TOTAL_RECORDS";
    private static final String GOOD_RECORDS = "GOOD_RECORDS";
    private static final String ERROR_RECORDS = "ERROR_RECORDS";
    private static final String TOTAL_POINTS = "TOTAL_POINTS";
    private static final String PPP_INDEX_APPLIED = "PPP_INDEX_APPLIED";

    private String processDate;
    private Integer totalPoints;
    private Boolean pppIndexApplied;

    /**
     * Default constructor.
     */
    public PointLoadBatchFileInfoDTO() {
    }

    public PointLoadBatchFileInfoDTO(BatchFileInfoDTO batchFileInfoDTO) {
        this.batchId = batchFileInfoDTO.batchId;
        this.errorRecords = batchFileInfoDTO.errorRecords;
        this.fileName = batchFileInfoDTO.fileName;
        this.goodRecords = batchFileInfoDTO.goodRecords;
        this.startDate = batchFileInfoDTO.startDate;
        this.status = batchFileInfoDTO.status;
        this.totalRecords = batchFileInfoDTO.totalRecords;
    }

    /**
     * Constructor from object mapping (query).
     *
     * @param node object mapping from query
     */
    public PointLoadBatchFileInfoDTO(Map<String, Object> node) {
        // check for missing mapping
        if (node == null) {
            return;
        }

        // construct DTO from object mapping values
        if (node.containsKey(BATCH_ID)) {
            this.batchId = (Long) node.get(BATCH_ID);
        }

        if (node.containsKey(STATUS)) {
            this.status = (String) node.get(STATUS);
        }

        if (node.containsKey(FILE_NAME)) {
            this.fileName = (String) node.get(FILE_NAME);
        }

        if (node.containsKey(START_DATE)) {
            this.startDate = convertDateObjectMapValue(node.get(START_DATE));
        }

        if (node.containsKey(PROCESS_DATE)) {
            this.processDate = convertDateObjectMapValue(node.get(PROCESS_DATE));
        }

        if (node.containsKey(TOTAL_RECORDS)) {
            this.totalRecords = ConversionUtil.convertIntegerObjectMapValue(node.get(TOTAL_RECORDS));
        }

        if (node.containsKey(GOOD_RECORDS)) {
            this.goodRecords = ConversionUtil.convertIntegerObjectMapValue(node.get(GOOD_RECORDS));
        }

        if (node.containsKey(ERROR_RECORDS)) {
            this.errorRecords = ConversionUtil.convertIntegerObjectMapValue(node.get(ERROR_RECORDS));
        }

        if (node.containsKey(TOTAL_POINTS)) {
            this.totalPoints = ConversionUtil.convertIntegerObjectMapValue(node.get(TOTAL_POINTS));
        }

        if (node.containsKey(PPP_INDEX_APPLIED)) {
            this.pppIndexApplied = Boolean.parseBoolean((String)node.get(PPP_INDEX_APPLIED));
        }
    }

    @ApiModelProperty(position = 8, required = false, value = "processing date of the Point Load activity")
    public String getProcessDate() {
        return processDate;
    }

    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }

    @ApiModelProperty(position = 9, required = false, value = "total points associated with the Point Load activity")
    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    @ApiModelProperty(position = 10, required = false, value = "PPP Index Applied Flag")
    public Boolean getPppIndexApplied() {
        return pppIndexApplied;
    }

    public void setPppIndexApplied(Boolean pppIndexApplied) {
        this.pppIndexApplied = pppIndexApplied;
    }
}
