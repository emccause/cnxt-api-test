package com.maritz.culturenext.batch.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(description = "DTO representing a request for updating a Point Load activity. Contains the ID of the batch "
        + "representing the Point Load activity as well as the update action to initiate.")
public class PointLoadActivityUpdateRequestDTO {
    private Long batchId;
    private String action;

    @ApiModelProperty(position = 1, required = true, value = "ID of the batch")
    public Long getBatchId() {
        return batchId;
    }
    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    @ApiModelProperty(position = 2, required = true, value = "update action to initiate")
    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }
}
