package com.maritz.culturenext.batch.dto;

public class BatchStatusDTO {
    private Long batchId;
    private String status;

    public Long getBatchId() {
        return batchId;
    }

    public BatchStatusDTO setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public BatchStatusDTO setStatus(String status) {
        this.status = status;
        return this;
    }
}
