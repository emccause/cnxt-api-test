package com.maritz.culturenext.batch.util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import com.maritz.core.util.StringUtils;
import com.maritz.core.util.stream.DeleteOnCloseFileInputStream;

public class ExcelToCSVInputStream {

    private Workbook workbook = null;
    private DataFormatter formatter = null;
    private FormulaEvaluator evaluator = null;

    private InputStream excelInputStream;
    private InputStream delimitedInputStream;

    private boolean closeExcelInputStream = true;
    private int sheetNum = 0;
    private int maxRowWidth = 0;
    private char delimiter;
    private char quote = '"';

    public ExcelToCSVInputStream(InputStream in) {
        excelInputStream = in;
    }

    public ExcelToCSVInputStream setSheetNum(int sheetNum) {
        this.sheetNum = sheetNum;
        return this;
    }

    public ExcelToCSVInputStream setDelimiter(char delimiter) {
        this.delimiter = delimiter;
        return this;
    }

    public ExcelToCSVInputStream setQuote(char quote) {
        this.quote = quote;
        return this;
    }

    public ExcelToCSVInputStream setCloseExcelInputStream(boolean closeExcelInputStream) {
        this.closeExcelInputStream = closeExcelInputStream;
        return this;
    }

    public InputStream build() throws IOException, InvalidFormatException {
        try {
            openWorkbook(excelInputStream);
            delimitedInputStream = new DeleteOnCloseFileInputStream(writeCSVData(convertToCSV()));
            return delimitedInputStream;
        } finally {
            if (closeExcelInputStream) {
                excelInputStream.close();
                excelInputStream = null;
            }
        }
    }

    private void openWorkbook(InputStream inputStream) throws IOException, InvalidFormatException {
        workbook = WorkbookFactory.create(inputStream);
        evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        formatter = new DataFormatter(true);
    }

    private List<List<String>> convertToCSV() {
        List<List<String>> rows = new ArrayList<>();

        Sheet sheet = this.workbook.getSheetAt(sheetNum);
        if (sheet.getPhysicalNumberOfRows() > 0) {

            // Note down the index number of the bottom-most row and
            // then iterate through all of the rows on the sheet starting
            // from the very first row - number 1 - even if it is missing.
            // Recover a reference to the row and then call another method
            // which will strip the data from the cells and build lines
            // for inclusion in the resulting CSV file.
            int lastRowNum = sheet.getLastRowNum();
            for (int j = 0; j <= lastRowNum; j++) {
                Row excelRow = sheet.getRow(j);
                if (excelRow != null)
                    rows.add(rowToCSV(excelRow));
            }
        }

        return rows;
    }

    private List<String> rowToCSV(Row row) {
        List<String> csvRow = new ArrayList<>();

        int lastCellNum = row.getLastCellNum();
        for (int i = 0; i < lastCellNum; i++) {
            Cell cell = row.getCell(i);
            if (cell == null) {
                csvRow.add("");
            } else if (cell.getCellType() != Cell.CELL_TYPE_FORMULA) {
                csvRow.add(formatter.formatCellValue(cell));
            } else {
                csvRow.add(formatter.formatCellValue(cell, evaluator));
            }
        }

        if (lastCellNum > maxRowWidth)
            maxRowWidth = lastCellNum;

        return csvRow;
    }

    private File writeCSVData(List<List<String>> rows) throws IOException {
        byte[] lineSeparator = System.lineSeparator().getBytes(StandardCharsets.UTF_8);

        File delimitedDataFile = File.createTempFile("delimitedDataFile", null);
        try {
            try (OutputStream delimitedOutputStream = new BufferedOutputStream(
                    new FileOutputStream(delimitedDataFile))) {
                for (List<String> row : rows) {

                    // add blank cells to pad out the row
                    int rowSize = row.size();
                    for (int i = rowSize; i < maxRowWidth; i++) {
                        row.add("");
                    }

                    byte[] delimitedRow = StringUtils.formatDelimitedData(row, delimiter, quote)
                            .getBytes(StandardCharsets.UTF_8);
                    delimitedOutputStream.write(delimitedRow, 0, delimitedRow.length);
                    delimitedOutputStream.write(lineSeparator, 0, lineSeparator.length);
                }
            }
            return delimitedDataFile;
        } catch (Throwable t) {
            if (delimitedDataFile != null) {
                delimitedDataFile.delete();
                delimitedDataFile = null;
            }
            throw new IOException("Could not write out temp delimited data file", t);
        }
    }
}
