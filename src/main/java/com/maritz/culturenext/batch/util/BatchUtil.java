package com.maritz.culturenext.batch.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.repository.BatchEventRepository;
import com.maritz.core.jpa.repository.BatchFileRepository;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.support.util.BatchTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.MapBuilder;
import com.maritz.culturenext.batch.BatchConstants;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.reports.constants.CashIssuanceReportConstants;
import com.maritz.culturenext.util.FileDownloadUtil;

@Component
public class BatchUtil {

    private static final String BATCH_STATUS_QUEUED = "QUEUED";
    private static final String BATCH_STATUS_IN_PROGRESS = "IN_PROGRESS";
    private static final String EMPTY_EXCLUDE_STATUSES_FILLER = "none";

    private static final String INVALID_BATCH_ID = "INVALID_BATCH_ID";
    private static final String INVALID_BATCH_ID_MSG = "BatchId does not exist: ";
    private static final String BATCH_TYPE_PLACEHOLDER = "{TYPE}";
    private static final String INVALID_TYPE_BATCH_ID = "INVALID_{TYPE}_BATCH_ID";
    private static final String INVALID_TYPE_BATCH_ID_MSG = " is not a valid {TYPE} batch";

    private static final String MISSING_BATCH_FILE = "MISSING_BATCH_FILE";
    private static final String MISSING_BATCH_FILE_MSG = "Error file does not exist for given BatchId: ";
    private static final String FILENAME_COLUMN = "filename";
    private static final String FILE_UPLOAD_ERROR_COLUMN = "fileUploadError";

    @Inject private BatchFileRepository batchFileRepository;
    @Inject private BatchRepository batchRepository;
    @Inject private BatchEventRepository batchEventRepository;
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;

    public Long getLatestCompletedBatchInfoByType(String batchType, Boolean includeInProgress) {
        ArrayList<String> excludedBatchStatuses = new ArrayList<>();
        if (batchType.equalsIgnoreCase(BatchType.POINT_LOAD.getCode())) {
            excludedBatchStatuses.add(BATCH_STATUS_QUEUED);
            excludedBatchStatuses.add(BATCH_STATUS_IN_PROGRESS);
        } else {
            if (batchType.equalsIgnoreCase(BatchType.ENROLLMENT_LOAD.getCode())) {
                excludedBatchStatuses.add(BATCH_STATUS_QUEUED);
            }

            if (!includeInProgress) {
                excludedBatchStatuses.add(BATCH_STATUS_IN_PROGRESS);
            }
        }
        if (excludedBatchStatuses.isEmpty()) {
            excludedBatchStatuses.add(EMPTY_EXCLUDE_STATUSES_FILLER);
        }
        Long latestBatchId = batchRepository.findLatestBatchIdByTypeAndStatusNotIn(batchType, excludedBatchStatuses);
        return latestBatchId;
    }

    /**
     * Download an error log file for the associated batch ID
     * 
     * @param batchId
     * @param response
     * @param batchType
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/95531156/GET+Enrollment+Error+Log+by+Batch+ID
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847474/Enrollment+File+processing+Example
     */
    public ResponseEntity<Object> getErrorLogForBatch(Long batchId, HttpServletResponse response, String batchType) {
        Batch batch = null;
        if (batchId != null) {
            // Make sure this is a valid batch of the requested type
            batch = batchRepository.findOne(batchId);
            if (batch == null) {
                throw new ErrorMessageException(new ErrorMessage()
                        .setCode(INVALID_BATCH_ID)
                        .setField(ProjectConstants.BATCH_ID)
                        .setMessage(INVALID_BATCH_ID_MSG + batchId));
            } else if (batchType == null) {
                batchType = batch.getBatchTypeCode(); // Retrieve batchType if batch is not null and a batchType is not provided
            }

            if (!batch.getBatchTypeCode().equalsIgnoreCase(batchType) && !batch.getBatchTypeCode().equalsIgnoreCase(BatchType.WORKDAY_ENROLLMENT.getCode())) {
                throw new ErrorMessageException(new ErrorMessage()
                        .setCode(INVALID_TYPE_BATCH_ID.replace(BATCH_TYPE_PLACEHOLDER, batchType))
                        .setField(ProjectConstants.BATCH_ID)
                        .setMessage(batchId + INVALID_TYPE_BATCH_ID_MSG.replace(BATCH_TYPE_PLACEHOLDER, batchType)));
            }
        } else {
            // No batchId is passed, use the latest completed Enrollment batch
            batchId = getLatestCompletedBatchInfoByType(batchType, false);
        }
        
        // Enrollment files themselves are processed as children of the main enrollment job batch
        if (BatchTypeCode.ENRL.name().equalsIgnoreCase(batchType) && (batch == null || !batch.getBatchTypeCode().equalsIgnoreCase(BatchType.WORKDAY_ENROLLMENT.getCode()))) {
            batchId = Long.parseLong(
                    batchEventRepository.findBy()
                        .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.CHILD_BATCH_ID)
                        .and(ProjectConstants.BATCH_ID).eq(batchId)
                        .findOne(ProjectConstants.MESSAGE, String.class)
                );
        }

        String errorLog = null;
        String filename = null;
        Map<String, ?> dataMap = batchFileRepository.findBy()
                .where(ProjectConstants.BATCH_ID).eq(batchId)
                .findOne(MapBuilder.<String, Class<?>> newHashMap()
                        .add(FILE_UPLOAD_ERROR_COLUMN, String.class)
                        .add(FILENAME_COLUMN, String.class).build());
        if (dataMap != null && !dataMap.isEmpty()) {
            errorLog = (String) dataMap.get(FILE_UPLOAD_ERROR_COLUMN);
            filename = "error_" + dataMap.get(FILENAME_COLUMN);
            // Force error file to be .csv format
            if (!filename.endsWith(".csv")) {
                filename = FilenameUtils.removeExtension(filename);
                filename += ".csv";
            }
        }

        if (errorLog == null || filename == null) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(MISSING_BATCH_FILE)
                    .setField(ProjectConstants.BATCH_ID)
                    .setMessage(MISSING_BATCH_FILE_MSG + batchId))
            .setStatus(HttpStatus.NOT_FOUND);
        }

        FileDownloadUtil.downloadFile(response, errorLog, MediaTypesEnum.TEXT_CSV.value(), filename);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    public void validateBatchId(Long batchId){
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        if (batchId == null || !batchFileDao.findBy()
                .where(ProjectConstants.BATCH_ID)
                .eq(batchId)
                .exists()) {

            // Add batch id error
            errors.add(BatchConstants.INVALID_BATCH_ID_MESSAGE);
        }
        ErrorMessageException.throwIfHasErrors(errors);
    }

    public void validateCashIssuanceBatchId(Long batchId){
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        if (batchId == null || !batchDao.findBy()
                .where(ProjectConstants.BATCH_ID)
                .eq(batchId)
                .and(ProjectConstants.BATCH_TYPE_CODE)
                .eq(CashIssuanceReportConstants.CASH_REPORT)
                .exists()) {

            // Add batch id error
            errors.add(CashIssuanceReportConstants.INVALID_BATCH_ID_MESSAGE);
        }
        ErrorMessageException.throwIfHasErrors(errors);
    }

    public Batch validateBatchStatusDto(BatchStatusDTO batchStatusDto) {
        Collection<ErrorMessage> errors = new ArrayList<>();

        String status = batchStatusDto.getStatus();
        if (StringUtils.isBlank(status)) {
            errors.add(BatchConstants.STATUS_MISSING_MESSAGE);
        } else if (!BatchConstants.STATUS_UPDATE_OPTIONS.contains(status)) {
            // Can't update a batch to this status
            errors.add(BatchConstants.STATUS_NOT_ALLOWED_MESSAGE
                           .setMessage(BatchConstants.STATUS_NOT_ALLOWED_MSG + status));
        }

        Long batchId = batchStatusDto.getBatchId();
        Batch batch = batchDao.findById(batchId);

        // Batch not found
        if (batch == null) {
            errors.add(BatchConstants.BATCH_NOT_FOUND_MESSAGE
                           .setMessage(BatchConstants.BATCH_NOT_FOUND_MSG + batchId));
        } else if (!BatchConstants.UPDATEABLE_STATUSES.contains(batch.getStatusTypeCode())) {
            // The current status is not updateable
            errors.add(BatchConstants.STATUS_NOT_UPDATEABLE_MESSAGE
                           .setMessage(BatchConstants.STATUS_NOT_UPDATEABLE_MSG + batch.getStatusTypeCode()));
        }

        ErrorMessageException.throwIfHasErrors(errors);

        return batch;
    }
}
