package com.maritz.culturenext.batch.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.culturenext.batch.TemplateConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.recognition.constants.RecognitionBulkUploadConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;

public class TemplateUtil {
    public static Map<String, Object[]> createRecognitionTemplateCSVProperties() {
        List<String> headersList = new ArrayList<>();
        List<String> fieldMappingList = new ArrayList<>();
        List<CellProcessor> processorList = new ArrayList<>();

        for (CsvColumnPropertiesDTO columnProps : createCSVColumnProperties()) {
            headersList.add(columnProps.getHeader());
            fieldMappingList.add(columnProps.getField());
            processorList.add(columnProps.getProcessor());
        }

        Map<String, Object[]> templateCSVMap = new HashMap<String, Object[]>();

        // Column's headers
        String[] headers = headersList.toArray(new String[headersList.size()]);
        templateCSVMap.put(TemplateConstants.HEADERS, headers);

        // Column's dto field mapping
        String[] fieldMapping = fieldMappingList.toArray(new String[fieldMappingList.size()]);
        templateCSVMap.put(TemplateConstants.FIELD_MAPPING, fieldMapping);

        // Column's processor
        CellProcessor[] processor = processorList.toArray(new CellProcessor[processorList.size()]);
        templateCSVMap.put(TemplateConstants.PROCESSORS, processor);

        return templateCSVMap;
    }

    private static List<CsvColumnPropertiesDTO> createCSVColumnProperties() {
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<>();

        csvColumnsProps.add(new CsvColumnPropertiesDTO(TemplateConstants.PARTICIPANT_FIRST_NAME,
                ProjectConstants.FIRST_NAME, null));
        csvColumnsProps.add(
                new CsvColumnPropertiesDTO(TemplateConstants.PARTICIPANT_LAST_NAME, ProjectConstants.LAST_NAME, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(TemplateConstants.PARTICIPANT_ID_MANDATORY,
                ProjectConstants.CONTROL_NUM, null));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(TemplateConstants.AWARD_AMOUNT, ProjectConstants.AWARD_AMOUNT,
                new Optional(new ParseLong())));

        return csvColumnsProps;
    }
    
    public static Map<String, Object[]> createStageAdvanceRecTemplateCSVProperties() {
        List<String> headersList = new ArrayList<>();
        List<String> fieldMappingList = new ArrayList<>();
        List<CellProcessor> processorList = new ArrayList<>();

        for (CsvColumnPropertiesDTO columnProps : createStageAdvanceRecCsvColumnProps(false)) {
            headersList.add(columnProps.getHeader());
            fieldMappingList.add(columnProps.getField());
            processorList.add(columnProps.getProcessor());
        }

        Map<String, Object[]> templateCSVMap = new HashMap<String, Object[]>();

        // Column's headers
        String[] headers = headersList.toArray(new String[headersList.size()]);
        templateCSVMap.put(TemplateConstants.HEADERS, headers);

        // Column's dto field mapping
        String[] fieldMapping = fieldMappingList.toArray(new String[fieldMappingList.size()]);
        templateCSVMap.put(TemplateConstants.FIELD_MAPPING, fieldMapping);

        // Column's processor
        CellProcessor[] processor = processorList.toArray(new CellProcessor[processorList.size()]);
        templateCSVMap.put(TemplateConstants.PROCESSORS, processor);

        return templateCSVMap;
    }
    
    public static List<CsvColumnPropertiesDTO> createStageAdvanceRecCsvColumnProps() {
        return createStageAdvanceRecCsvColumnProps(true);
    }
    public static List<CsvColumnPropertiesDTO> createStageAdvanceRecCsvColumnProps(boolean addErrorColumn) {
        
        List<CsvColumnPropertiesDTO> csvColumnsProps = new ArrayList<CsvColumnPropertiesDTO>();
        
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_AWARD_AMOUNT,
                ProjectConstants.AWARD_AMOUNT, new Optional(new ParseDouble())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_RECIPIENT_FIRST_NAME,
                ProjectConstants.RECIPIENT_FIRST_NAME, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_RECIPIENT_LAST_NAME,
                ProjectConstants.RECIPIENT_LAST_NAME, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_RECIPIENT_ID,
                ProjectConstants.RECIPIENT_ID, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_PUBLIC_HEADLINE,
                RecognitionBulkUploadConstants.PUBLIC_HEADLINE, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_PRIVATE_MESSAGE,
                ProjectConstants.PRIVATE_MESSAGE, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_PROGRAM_NAME,
                ProjectConstants.PROGRAM_NAME, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_PROGRAM_ID,
                ProjectConstants.PROGRAM_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_BUDGET_NAME,
                ProjectConstants.BUDGET_NAME, new Optional ()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_BUDGET_ID,
                ProjectConstants.BUDGET_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_SUBMITTER_FIRST_NAME,
                ProjectConstants.SUBMITTER_FIRST_NAME, new Optional ()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_SUBMITTER_LAST_NAME,
                ProjectConstants.SUBMITTER_LAST_NAME, new Optional ()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_SUBMITTER_ID,
                ProjectConstants.SUBMITTER_ID, new Optional()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_ECARD_ID,
                ProjectConstants.ECARD_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_VALUE,
                RecognitionBulkUploadConstants.VALUE, new Optional ()));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_VALUE_ID,
                RecognitionBulkUploadConstants.VALUE_ID, new Optional(new ParseLong())));
        csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_MAKE_RECOGNITION_PRIVATE,
                RecognitionBulkUploadConstants.MAKE_RECOGNITION_PRIVATE, new Optional ()));
        if(addErrorColumn) {
            csvColumnsProps.add(new CsvColumnPropertiesDTO(RecognitionBulkUploadConstants.FIELD_ERROR_MESSAGE,
                RecognitionBulkUploadConstants.ERROR_MESSAGE, new Optional ()));
        }
        
        return csvColumnsProps;    
    }
}
