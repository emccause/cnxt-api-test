package com.maritz.culturenext.batch.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.SQLException;
import java.util.Date;

import javax.inject.Inject;

import com.maritz.core.jpa.support.util.StatusTypeCode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.BatchEventRepository;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.util.stream.CRC32InputStream;
import com.maritz.culturenext.batch.dto.BatchFileInfoDTO;
import com.maritz.culturenext.batch.service.BatchUploadFileService;
import com.maritz.culturenext.batch.util.ExcelToCSVInputStream;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.util.DateUtil;

@Component
public class BatchUploadFileServiceImpl implements BatchUploadFileService {
    final Logger logger = LoggerFactory.getLogger(getClass());
    
    // The length of the buffer that will be used when reading a file
    private static final int APPEND_CHAR_BUFFER_LENGTH = 655360;
    @Inject private BatchEventRepository batchEventRepository;
    @Inject private BatchRepository batchRepository;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;
    @Inject private PaxRepository paxRepository;
    
    
    @Override
    public BatchFileInfoDTO uploadBatchFile(Long paxId, String batchTypeCode, String mimeType, String fileName, 
            long size, InputStream inputStream) throws Exception {
        // Create a Batch entry
        Batch batch = new Batch();
        batch.setBatchTypeCode(batchTypeCode);
        batch.setStatusTypeCode(StatusTypeCode.NEW.name());
        batchRepository.save(batch);
        
        Long batchId = batch.getBatchId();
        
        // strip the path from the filename
        int i = fileName.replace('\\', '/').lastIndexOf('/');
        if (i >= 0) fileName = fileName.substring(i + 1);
        
        // Push the data into our BATCH_FILE table for further offline processing.
        BatchFile batchFile = new BatchFile();
        batchFile.setFilename(fileName);
        batchFile.setFileUpload("");
        batchFile.setBatchId(batchId);
        batchFile.setMediaType(mimeType.equals(MediaTypesEnum.MS_EXCEL.value()) ? MediaTypesEnum.TEXT_CSV.value() : mimeType);
        batchFileDao.save(batchFile);

        //write the input stream to the database
        CRC32InputStream crc32InputStream = getCRC32InputStream(mimeType, fileName, inputStream);
        /*
         //write() method is not working as expected for InputStream, it is not saved into database
        batchFileDao.findBy()
            .where("batchFileId").eq(batchFile.getId())
            .write("fileUpload", crc32InputStream, (int)size);
        */
        Reader reader = new InputStreamReader(crc32InputStream);
        appendToFileUpload(batchFile,reader);
        
        long crc32 = crc32InputStream.getCRC32();
        
        // Create an entry in the Batch event table for user
        Pax pax = paxRepository.findOne(paxId);
        BatchEvent batchEvent = new BatchEvent();
        batchEvent.setBatchEventTypeCode(BatchEventType.USER_ID.getCode());
        batchEvent.setBatchId(batchId);
        batchEvent.setMessage(pax.getControlNum());
        batchEvent.setStatusTypeCode(StatusTypeCode.COMPLETE.toString());
        batchEventRepository.save(batchEvent);
        
        batchEvent = new BatchEvent();
        batchEvent.setBatchEventTypeCode(BatchEventType.FILE_NAME.getCode());
        batchEvent.setBatchId(batchId);
        batchEvent.setMessage(fileName);
        batchEvent.setStatusTypeCode(StatusTypeCode.COMPLETE.toString());
        batchEventRepository.save(batchEvent);
        
        //write the crc32 value as batch event
        batchEvent = new BatchEvent();
        batchEvent.setBatchEventTypeCode(BatchEventType.FILE_CRC.getCode());
        batchEvent.setBatchId(batchId);
        batchEvent.setMessage(String.valueOf(crc32));
        batchEvent.setStatusTypeCode(StatusTypeCode.COMPLETE.toString());
        batchEventRepository.save(batchEvent);
        
        reader.close();
        crc32InputStream.close();
        
        return new BatchFileInfoDTO()
            .setBatchId(batchId)
            .setFileName(fileName)
            .setStatus(batch.getStatusTypeCode())
            .setStartDate(DateUtil.convertToUTCDateTime(new Date()))
        ;
    }
    
    private CRC32InputStream getCRC32InputStream(String mimeType, String fileName, InputStream inputStream) 
            throws Exception {
        if (fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
            ExcelToCSVInputStream excelToCsvStream = new ExcelToCSVInputStream(inputStream);
            excelToCsvStream.setDelimiter(',');
            return new CRC32InputStream(excelToCsvStream.build());
        } 
        else {
            return new CRC32InputStream(inputStream);
        }
    }
    private void appendToFileUpload(BatchFile batchFile, Reader reader) throws SQLException,
    IOException {
        char[] buf = new char[APPEND_CHAR_BUFFER_LENGTH];
        int numRead = 0;
        String readData = null;
        String fileUploadConcat = null;
        
        if (batchFile != null){
            while ((numRead = reader.read(buf)) != -1) {
                readData = String.valueOf(buf, 0, numRead);
                
                if (readData != null) {
                    //concatenate read buffer;
                    fileUploadConcat = batchFile.getFileUpload() + readData;
                    batchFile.setFileUpload(fileUploadConcat);

                    batchFileDao.update(batchFile);        
                }
            }
        }
    }    
}
