package com.maritz.culturenext.batch.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.AdvancedRecognitionFileContentDTO;
import com.maritz.culturenext.batch.RecognitionUploadConstants;
import com.maritz.culturenext.batch.TemplateConstants;
import com.maritz.culturenext.batch.service.RecognitionUploadService;
import com.maritz.culturenext.batch.util.ExcelToCSVInputStream;
import com.maritz.culturenext.batch.util.TemplateUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class RecognitionUploadServiceImpl implements RecognitionUploadService {

    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ParticipantInfoDao participantDao;

    @Override
    public List<AdvancedRecognitionFileContentDTO> uploadAdvancedRecognitionFile(@Nonnull Long programId,
            @Nonnull MultipartFile file) {

        // Validating file content.
        if (!ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.contains(file.getContentType())) {
            throw new ErrorMessageException()
                    .addErrorMessage(RecognitionUploadConstants.FILE_FIELD,
                            RecognitionUploadConstants.ERROR_FILE_CONTENT_TYPE_INVALID,
                            RecognitionUploadConstants.ERROR_FILE_CONTENT_TYPE_INVALID_MSG)
                    .setStatus(HttpStatus.BAD_REQUEST);
        }

        // Reading file content
        List<AdvancedRecognitionFileContentDTO> fileContent = null;
        Map<String, Object[]> recognitionTemplateCSVProps = TemplateUtil.createRecognitionTemplateCSVProperties();

        InputStream inputStream = null;

        try {

            // Converting Excel files to CSV.
            if (file.getOriginalFilename().endsWith(".xls") || file.getOriginalFilename().endsWith(".xlsx")) {
                ExcelToCSVInputStream excelToCsvStream = new ExcelToCSVInputStream(file.getInputStream());
                excelToCsvStream.setDelimiter(',');
                inputStream = excelToCsvStream.build();
            } else {
                inputStream = file.getInputStream();
            }

            fileContent = CsvFileUtil.readCSVFILE(new InputStreamReader(inputStream),
                    AdvancedRecognitionFileContentDTO.class,
                    (CellProcessor[]) recognitionTemplateCSVProps.get(TemplateConstants.PROCESSORS),
                    (String[]) recognitionTemplateCSVProps.get(TemplateConstants.FIELD_MAPPING));
        } catch (Exception e) {
            throw new ErrorMessageException()
                    .addErrorMessage(RecognitionUploadConstants.FILE_FIELD,
                            RecognitionUploadConstants.ERROR_FILE_CONTENT_DATA_INVALID,
                            RecognitionUploadConstants.ERROR_FILE_CONTENT_DATA_INVALID_MSG)
                    .setStatus(HttpStatus.BAD_REQUEST);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        // Adding pax object
        for (AdvancedRecognitionFileContentDTO record : fileContent) {

            Long paxId = paxDao.findBy()
                    .where(ProjectConstants.CONTROL_NUM).eq(record.getControlNum())
                    .findOne(ProjectConstants.PAX_ID, Long.class);

            if (paxId != null) {
                record.setPax(participantDao.getParticipantInfo(paxId, programId));
            }else {
                EmployeeDTO employee = new EmployeeDTO ();
                employee.setControlNum(record.getControlNum());
                employee.setFirstName(record.getFirstName());
                employee.setLastName(record.getLastName());
                employee.setPaxId(RecognitionUploadConstants.DEFAULT_PAX_ID_WHEN_IT_DOESNT_EXIST);
                employee.setStatus(RecognitionUploadConstants.ERROR_PAX_ID_DOESNT_EXIST);
                employee.setEligibleReceiverInProgram(false);
                record.setPax(employee);
            }
        }
        
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fileContent;
    }

}
