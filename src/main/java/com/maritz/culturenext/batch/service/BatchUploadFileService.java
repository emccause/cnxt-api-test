package com.maritz.culturenext.batch.service;

import java.io.InputStream;

import com.maritz.culturenext.batch.dto.BatchFileInfoDTO;

public interface BatchUploadFileService {

    /**
     * TODO javadoc
     * 
     * @param paxId
     * @param batchTypeCode
     * @param mimeType
     * @param fileName
     * @param size
     * @param inputStream
     * @return
     * @throws Exception
     */
    BatchFileInfoDTO uploadBatchFile(Long paxId, String batchTypeCode, String mimeType, String fileName, long size,
            InputStream inputStream) throws Exception;
}
