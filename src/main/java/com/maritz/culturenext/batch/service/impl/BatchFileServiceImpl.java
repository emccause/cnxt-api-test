package com.maritz.culturenext.batch.service.impl;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.util.FileDownloadUtil;

@Component
public class BatchFileServiceImpl implements BatchFileService {

    @Inject private BatchUtil batchUtil;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;

    @Override
    public ResponseEntity<Object> generateBatchFile(HttpServletResponse response, Long batchId) {
        batchUtil.validateBatchId(batchId); // Validate batch id before fetching associated file
        BatchFile batchFile = batchFileDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();

        String fileContent = batchFile.getFileUpload();
        String filename = batchFile.getFilename();
        String fileType = batchFile.getMediaType();

        FileDownloadUtil.downloadFile(response, fileContent, fileType, filename);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @Transactional
    public boolean updateBatchFileError(BatchFile batchFile, String fileUploadError){
        Long batchFileId = batchFile.getId();
        batchFile = batchFileDao.findById(batchFileId);
        if (batchFile != null) {
            batchFile.setFileUploadError(fileUploadError);
            batchFileDao.update(batchFile);
            return true;
        }
        return false;
    }
}
