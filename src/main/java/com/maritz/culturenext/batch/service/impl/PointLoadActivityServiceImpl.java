package com.maritz.culturenext.batch.service.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.batch.BatchConstants;
import com.maritz.culturenext.batch.PointLoadUpdateAction;
import com.maritz.culturenext.batch.dao.PointLoadActivityDao;
import com.maritz.culturenext.batch.dto.BatchFileInfoDTO;
import com.maritz.culturenext.batch.dto.PointLoadActivityUpdateRequestDTO;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.BatchUploadFileService;
import com.maritz.culturenext.batch.service.PointLoadActivityService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.email.event.PointLoadEmailEvent;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.culturenext.util.DateUtil;

@Component
public class PointLoadActivityServiceImpl implements PointLoadActivityService {
    private static final Logger logger = LoggerFactory.getLogger(PointLoadActivityServiceImpl.class);

    private static final Integer SUBTRACT_SIX_MONTHS = -6;

    //error messages and codes
    private static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    private static final String ERROR_INVALID_STATUS_MSG = "Specified status is invalid: ";

    // statuses
    private static final List<String> DEFAULT_STATUSES = BatchConstants.defaultStatuses();
    private static final String DEFAULT_STATUSES_STRING = org.apache.commons.lang3.StringUtils
            .join(DEFAULT_STATUSES, ",");

    @Inject protected ApplicationEventPublisher applicationEventPublisher;
    @Inject private BatchUploadFileService batchUploadFileService;
    @Inject private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private PointLoadActivityDao pointLoadActivityDao;
    
    @Nonnull
    @Override
    @Transactional
    public List<PointLoadBatchFileInfoDTO> getPointLoadActivity(Date fromDate, String statusString) {
        // determine fromDate to use for query
        fromDate = determineFromDate(fromDate);

        // determine statuses to use for query
        String statuses = determineStatuses(statusString);

        // determine thruDate to use for query (should be current date)
        Date thruDate = new Date();

        // validate given arguments
        validateQueryArguments(fromDate, statuses);

        // first, execute archiving of records older than 6 months (to exclude from later query)
        pointLoadActivityDao.archivePointLoadActivities(generateSixMonthsPriorDate());

        // then execute query with the file name and the determined fromDate
        List<PointLoadBatchFileInfoDTO> pointLoadActivities =
                pointLoadActivityDao.getPointLoadActivities(fromDate, thruDate, statuses);

        // adjust statuses of the activities
        for(PointLoadBatchFileInfoDTO pointLoadActivity : pointLoadActivities) {
            adjustPointLoadActivityStatus(pointLoadActivity);
        }

        return pointLoadActivities;
    }

    @Override
    @Nonnull
    @Transactional
    public PointLoadBatchFileInfoDTO updatePointLoadActivity(
            PointLoadActivityUpdateRequestDTO pointLoadActivityUpdateRequestDTO) {
        // initial validation for the request
        List<ErrorMessage> errors = validatePointLoadUpdateRequest(pointLoadActivityUpdateRequestDTO);
        ErrorMessageException.throwIfHasErrors(errors);

        Long batchId = pointLoadActivityUpdateRequestDTO.getBatchId();
        String updateAction = pointLoadActivityUpdateRequestDTO.getAction();
        PointLoadUpdateAction action = PointLoadUpdateAction.getPointLoadUpdateAction(updateAction);

        // depending on the action, update the batch accordingly
        PointLoadBatchFileInfoDTO pointLoadBatchFileInfoDTO;

        try {
            pointLoadBatchFileInfoDTO = pointLoadActivityDao.updatePointLoadActivity(batchId, action);
        } catch(Exception e) {
            // something went wrong with the update so just update to the failed status
            PointLoadUpdateAction errorAction;

            switch (action) {
                case RELEASE:
                    errorAction = PointLoadUpdateAction.FAILED_RELEASE;
                    break;

                case CANCEL:
                    errorAction = PointLoadUpdateAction.FAILED_CANCEL;
                    break;

                default:
                    // no other handling so just throw error
                    throw e;
            }

            // execute another update with the failed action
            pointLoadBatchFileInfoDTO = pointLoadActivityDao.updatePointLoadActivity(batchId, errorAction);
        }

        //Make sure NEW status is being sent back as IN_PROGRESS
        adjustPointLoadActivityStatus(pointLoadBatchFileInfoDTO);
        
        if(BatchConstants.ALERT_SUB_TYPE_RELEASE.equals(action.getAction().toString())) {
            String notificationRecipient = cnxtProgramMiscRepository.findNotificationRecipient(batchId);
            if(ProjectConstants.TRUE.equalsIgnoreCase(notificationRecipient)){
                //Create Notification (ALERT record)
                pointLoadActivityDao.createPointUploadNotification(batchId);
                //Send Emails
                applicationEventPublisher.publishEvent(new PointLoadEmailEvent(EventType.POINT_LOAD.getCode(), batchId));
            }
        }

        return pointLoadBatchFileInfoDTO;
    }

    // helper methods

    /**
     * Validates the specified arguments for the Point Load query.
     *
     * @param fromDate fromDate determined from the given query parameter
     * @param statusString statuses specified through the query parameter
     */
    private void validateQueryArguments(Date fromDate, @Nonnull String statusString) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        Date currentDate = DateUtil.convertFromUTCStringDateTime(DateUtil.getCurrentTimeInUTCDateTime());

        // fromDate validation
        if(fromDate == null) {
            errors.add(BatchConstants.INVALID_FROM_DATE_MESSAGE);
        } else if(fromDate.after(currentDate)) {
            errors.add(BatchConstants.FROM_DATE_IN_FUTURE_MESSAGE);
        }

        // status validation (only if provided and not default value of all statuses)
        if(!DEFAULT_STATUSES_STRING.equals(statusString)) {
            // split the string up (comma separated list of statuses)
            StringTokenizer stringTokenizer = new StringTokenizer(statusString, ",");
            while (stringTokenizer.hasMoreTokens()) {
                // get the next status and uppercase (ignore casing of status)
                String statusToken = stringTokenizer.nextToken().toUpperCase();

                // check if status is invalid or if status is archived
                if (!DEFAULT_STATUSES.contains(statusToken) && !StatusTypeCode.ARCHIVED.toString().equals(statusToken)) {
                    errors.add(new ErrorMessage()
                            .setCode(ERROR_INVALID_STATUS)
                            .setField(ProjectConstants.STATUS)
                            .setMessage(ERROR_INVALID_STATUS_MSG + statusToken));
                }
            }
        }

        ErrorMessageException.throwIfHasErrors(errors);
    }

    /**
     * Status input handling.
     *
     * @param statusString status string
     * @return statuses determined
     */
    @Nonnull
    private String determineStatuses(String statusString) {
        // handling of missing or empty string (interpreted as all statuses)
        if(statusString == null || statusString.isEmpty()) {
            return DEFAULT_STATUSES_STRING;
        }

        return statusString;
    }

    /**
     * Determines fromDate from the given string.
     *
     * @return fromDate determined from given string
     */
    @CheckForNull
    private Date determineFromDate(Date fromDate) {
        if (fromDate != null) return fromDate;        
        // handling of missing fromDate specified within request (set to six months prior)
        return generateSixMonthsPriorDate();
    }

    @Nonnull
    private Date generateSixMonthsPriorDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, SUBTRACT_SIX_MONTHS);
        return calendar.getTime();
    }
    
    @Override
    public PointLoadBatchFileInfoDTO uploadPointLoadFile(Long paxId, String mimeType, String fileName, long size, 
            InputStream inputStream) throws Exception {
        BatchFileInfoDTO batchFileInfoDTO = batchUploadFileService.uploadBatchFile(paxId, 
                BatchType.POINT_LOAD.getCode(), mimeType, fileName, size, inputStream);
        if (batchFileInfoDTO != null) {
            // NEW, QUEUED, and IN_PROGRESS get displayed as IN_PROGRESS
            String batchStatus = batchFileInfoDTO.getStatus();
            if (StringUtils.isIn(batchStatus, StatusTypeCode.NEW.name(), StatusTypeCode.QUEUED.toString())) {
                batchFileInfoDTO.setStatus(StatusTypeCode.IN_PROGRESS.toString());
            }
            return new PointLoadBatchFileInfoDTO(batchFileInfoDTO);
        }
        return null;
    }

    /**
     * Validates the request to update a Point Load Activity (batch info).
     *
     * @param pointLoadActivityUpdateRequestDTO update request
     */
    private List<ErrorMessage> validatePointLoadUpdateRequest(
            PointLoadActivityUpdateRequestDTO pointLoadActivityUpdateRequestDTO) {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();

        // check if user sent a request (if not, return with a missing request error)
        if(pointLoadActivityUpdateRequestDTO == null) {
            errors.add(BatchConstants.MISSING_UPDATE_REQUEST);

            return errors;
        }

        // check if the user sent a valid request (valid batch ID and action)

        // validate batch ID and valid batch existence
        Batch batch = null;
        Long batchId = pointLoadActivityUpdateRequestDTO.getBatchId();
        if(batchId == null) {
            errors.add(BatchConstants.MISSING_BATCH_ID_MESSAGE);
        } else {
            try {
                batch = batchDao.findById(batchId);
            } catch(Exception e) {
                logger.error("Error occurred while verifying batch existence!!", e);
            }

            if(batch == null) {
                errors.add(BatchConstants.INVALID_BATCH_ID_MESSAGE);
            }
        }

        String action = pointLoadActivityUpdateRequestDTO.getAction();
        if(action == null) {
            errors.add(BatchConstants.MISSING_ACTION_MESSAGE);
        } else if(!PointLoadUpdateAction.getUpdateActions().contains(action)) {
            errors.add(BatchConstants.INVALID_ACTION_MESSAGE);
        } else if(batch != null) {
            // verify that the batch, if valid, is in the correct status and state to initiate the action
            if(!BatchType.POINT_LOAD.getCode().equals(batch.getBatchTypeCode())) {
                errors.add(BatchConstants.NON_POINT_LOAD_BATCH_MESSAGE);
            } else {
                PointLoadUpdateAction pointLoadUpdateAction =
                        PointLoadUpdateAction.getPointLoadUpdateAction(action);

                String batchStatusTypeCode = batch.getStatusTypeCode();

                switch (pointLoadUpdateAction) {
                    case CANCEL:
                        if(!StatusTypeCode.PENDING_RELEASE.toString().equals(batchStatusTypeCode) &&
                                !StatusTypeCode.BUDGET_EXCEEDED.toString().equals(batchStatusTypeCode)) {
                            errors.add(BatchConstants.INVALID_ACTION_TRANSITION_MESSAGE);
                        }
                        break;
                    case RELEASE:
                        if(!StatusTypeCode.PENDING_RELEASE.toString().equals(batchStatusTypeCode)) {
                            errors.add(BatchConstants.INVALID_ACTION_TRANSITION_MESSAGE);
                        }
                        break;
                    case REPROCESS:
                        if(!StatusTypeCode.BUDGET_EXCEEDED.toString().equals(batchStatusTypeCode)) {
                            errors.add(BatchConstants.INVALID_ACTION_TRANSITION_MESSAGE);
                        }
                        break;
                    default:
                        logger.error("Should not have gotten here (validated for correct Point Load update action)!!");
                        break;
                }
            }
        }

        return errors;
    }

    /**
     * Adjusts the Point Load activity status (to match the correct formatting rules).
     *
     * @param pointLoadActivity Point Load activity to adjust
     */
    private void adjustPointLoadActivityStatus(@Nonnull PointLoadBatchFileInfoDTO pointLoadActivity) {
        String activityStatus = pointLoadActivity.getStatus();

        // NEW and QUEUED get displayed as IN_PROGRESS
        if(StatusTypeCode.NEW.name().equals(activityStatus) ||
                StatusTypeCode.QUEUED.toString().equals(activityStatus)) {
            pointLoadActivity.setStatus(StatusTypeCode.IN_PROGRESS.toString());
        }
    }
}
