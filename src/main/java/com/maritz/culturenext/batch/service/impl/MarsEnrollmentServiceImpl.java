package com.maritz.culturenext.batch.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import java.util.stream.Collectors;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.dto.TargetDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.repository.PaxAccountRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.services.BatchService;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.core.util.DateUtils;

import com.maritz.core.util.PropertiesUtils;
import com.maritz.culturenext.batch.dao.MarsDemographicsDao;
import com.maritz.culturenext.batch.dto.MarsDemographicsDto;
import com.maritz.culturenext.batch.service.MarsEnrollmentService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.mars.dto.ParticipantAddressDTO;
import com.maritz.mars.dto.ParticipantDemographicsDTO;
import com.maritz.mars.dto.ParticipantIdentityDTO;
import com.maritz.mars.dto.ParticipantPhoneDTO;
import com.maritz.mars.services.MarsException;
import com.maritz.mars.services.MarsParticipantService;
import com.microsoft.sqlserver.jdbc.StringUtils;

@Component
public class MarsEnrollmentServiceImpl implements MarsEnrollmentService {
    final Logger logger = LoggerFactory.getLogger(getClass());
    
    private static final String PROPERTY_PREFIX = "triggeredTask.marsEnrollmentUpdateJob";
    
    private ApplicationDataService applicationDataService;
    private BatchService batchService;
    private Environment environment;
    private MarsDemographicsDao marsEnrollmentUpdateDao;
    private MarsParticipantService marsParticipantService;
    private PaxAccountRepository paxAccountRepository;
    
    @Inject
    public MarsEnrollmentServiceImpl setApplicationDataService(ApplicationDataService applicationDataService) {
        this.applicationDataService = applicationDataService;
        return this;
    }
    
    @Inject
    public MarsEnrollmentServiceImpl setBatchService(BatchService batchService) {
        this.batchService = batchService;
        return this;
    }

    @Inject
    public MarsEnrollmentServiceImpl setEnvironment(Environment environment) {
        this.environment = environment;
        return this;
    }

    @Inject
    public MarsEnrollmentServiceImpl setMarsEnrollmentUpdateDao(MarsDemographicsDao marsEnrollmentUpdateDao) {
        this.marsEnrollmentUpdateDao = marsEnrollmentUpdateDao;
        return this;
    }

    @Inject
    public MarsEnrollmentServiceImpl setMarsParticipantService(MarsParticipantService marsParticipantService) {
        this.marsParticipantService = marsParticipantService;
        return this;
    }

    @Inject
    public MarsEnrollmentServiceImpl setPaxAccountRepository(PaxAccountRepository paxAccountRepository) {
        this.paxAccountRepository = paxAccountRepository;
        return this;
    }

    @TriggeredTask
    @Override
    public void marsEnrollmentUpdateJob(Batch batch) {

        Integer pageNumber = 1;
        Integer totalRecords = 0;
        Integer goodRecords = 0;
        Integer failedRecords = 0;
        List<BatchEventDTO> batchEventList = new ArrayList<>();

        logger.info("Mars Enrollment Update Job - triggeredTask - batchId: {}", null == batch ? "BatchId is null" :
                batch.getBatchId());

        Integer recordsPerIteration = environment.getProperty(
                PROPERTY_PREFIX + ".recordsPerIteration", Integer.class
            );
        List<Long> expectedMarsErrorCodeList = PropertiesUtils.getCsvList(
                environment, PROPERTY_PREFIX + ".expectedMarsErrorCodeList"
            ).stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
        
        // Effectively batch this, new user enrollment / new award type creation will create a large number of records
        Boolean shouldRepeat = Boolean.TRUE;
        while (Boolean.TRUE.equals(shouldRepeat)) {
            // call query to obtain data
            List<MarsDemographicsDto> recordList = marsEnrollmentUpdateDao.getDemographicsUpdate(
                    pageNumber, recordsPerIteration
                );
            
            if (CollectionUtils.isNotEmpty(recordList)) {
                totalRecords += recordList.size();
                
                for (MarsDemographicsDto record: recordList) {
                    
                    ParticipantDemographicsDTO participantDemographics = getParticipantDemographicsDTO(record);
                    
                    ParticipantIdentityDTO participantIdentity = new ParticipantIdentityDTO()
                            .setPaxId(record.getPaxId())
                            .setPid(record.getControlNum())
                            .setAccountNumber(record.getCardNum())
                            .setCardType(record.getCardTypeCode())
                            .setUserName(record.getUserName()) // These will be null for us only
                            .setPassword(record.getPassword());
                    
                    try {
                        if (StringUtils.isEmpty(record.getCardNum())) {
                            // We don't have a pax account record for this card type, create
                            marsParticipantService.createParticipantAccount(
                                    participantIdentity, record.getProjectNumber(),
                                    record.getSubProjectNumber(), null, participantDemographics
                                );
                        }
                        else {
                            // update existing record
                            marsParticipantService.updateParticipantAccount(
                                    participantIdentity, record.getProjectNumber(),
                                    record.getSubProjectNumber(), null, participantDemographics
                            );
                        }
                    }
                    catch (MarsException me) {
                        logger.error("A MARS exception occurred.", me);

                        // Save the error message
                        batchEventList.add(new BatchEventDTO()
                                .setType(BatchEventTypeCode.ERR.name())
                                .setTarget(new TargetDTO(TableName.PAX.name(), record.getPaxId()))
                                .setMessage("Account creation / update failed: " + me.getMessage())
                                .setReference(me.getTransactionId())
                                .setStatus(StatusTypeCode.COMPLETE.name()))
                            ;
                        
                        failedRecords++;
                        if (expectedMarsErrorCodeList.contains(me.getResponseCode())) {
                            // MARS is down - log and bail out
                            insertBatchEvents(batch, batchEventList, totalRecords, goodRecords, failedRecords);
                            throw me;
                        }
                        else {
                            // try next record
                            continue;
                        }
                    }
                    catch (Exception e) {                        
                        logger.error("A Non-MARS exception occurred.", e);

                        // Save the error message
                        batchEventList.add(new BatchEventDTO()
                                .setType(BatchEventTypeCode.ERR.name())
                                .setTarget(new TargetDTO(TableName.PAX.name(), record.getPaxId()))
                                .setMessage("Account creation / update failed: " + e.getMessage())
                                .setStatus(StatusTypeCode.COMPLETE.name()))
                            ;

                        failedRecords++;

                        // We want to re-process, so just bail out
                        insertBatchEvents(batch, batchEventList, totalRecords, goodRecords, failedRecords);
                        throw e;
                    }
                    
                    // record processed successfully
                    goodRecords++;
                }
            }
            logger.info("Processed " + totalRecords + " accounts so far.");
            shouldRepeat = recordsPerIteration.equals(recordList.size());
            pageNumber++;
        }
        logger.info("finished bulk account creation");
        paxAccountRepository.flush();
        
        // Completed (possibly with errors), log time
        applicationDataService.setApplicationData(
                ApplicationDataConstants.KEY_NAME_LAST_PROCESSED_DATE,
                DateUtils.formatDatetimeMillisUTC(new Date())
            );

        // Log that we ran
        insertBatchEvents(batch, batchEventList, totalRecords, goodRecords, failedRecords);
    }
    
    /** 
     * Inserts batch events generated while processing
     * 
     * @param batch - batch record to tie events to
     * @param batchEventList - events to tie to batch (probably errors, if any)
     * @param totalRecords - total number of records processed (might have ended early)
     * @param goodRecords - total number of records successfully processed through MARS
     * @param failedRecords - total number of records that failed to process through MARS
     */
    private void insertBatchEvents(
            Batch batch, List<BatchEventDTO> batchEventList,
            Integer totalRecords, Integer goodRecords, Integer failedRecords
        ) {

        // Store record counts
        batchEventList.add(new BatchEventDTO()
                .setType(BatchEventTypeCode.RECS.name())
                .setMessage(String.valueOf(totalRecords))
            );
        batchEventList.add(new BatchEventDTO()
                .setType(BatchEventTypeCode.OKRECS.name())
                .setMessage(String.valueOf(goodRecords))
            );
        batchEventList.add(new BatchEventDTO()
                .setType(BatchEventTypeCode.ERRS.name())
                .setMessage(String.valueOf(failedRecords))
            );
        
        // Save all our events (records, error messages)
        batchService.createBatchEvents(batch.getBatchId(), batchEventList);
    }
    
    /**
     * Sets the attributes to create a full/partially-full ParticipantDemographicsDTO object
     * @param record - map with info from the database
     * @return
     */
    private ParticipantDemographicsDTO getParticipantDemographicsDTO(MarsDemographicsDto record) {
        return new ParticipantDemographicsDTO()
            .setFirstName(record.getFirstName())
            .setLastName(record.getLastName())
            .setMiddleName(record.getMiddleName())
            .setMiddleInitial(record.getMiddleName() == null ? null : record.getMiddleName().substring(0, 1))
            .setSuffix(record.getNameSuffix())
            .setEmailAddress(record.getEmailAddress())
            .setAddresses(getParticipantAddressDTO(record))
            .setPhoneNumbers(getParticipantPhoneDTO(record))
        ;
    }

    /**
     * Sets the attributes to create a full/partially-full ParticipantAddressDTO list
     * @param record - map with info from the database
     * @return
     */
    private List<ParticipantAddressDTO> getParticipantAddressDTO(MarsDemographicsDto record) {
        return Arrays.asList(new ParticipantAddressDTO()
            .setAddressType(ProjectConstants.HOME_CONTACT_TYPE)
            .setAddressLine1(record.getAddress1())
            .setAddressLine2(record.getAddress2())
            .setAddressLine3(record.getAddress3())
            .setAddressLine4(record.getAddress4())
            .setAddressLine5(record.getAddress5())
            .setAddressLine6(record.getAddress6())
            .setCity(record.getCity())
            .setCountryCode(record.getCountryCode())
            .setPostalCode(record.getZip())
            .setStateProvince(record.getState())
        );
    }

    /**
     * Sets the attributes to create a full/partially-full ParticipantPhoneDTO list
     * @param record - map with info from the database
     * @return
     */
    private List<ParticipantPhoneDTO> getParticipantPhoneDTO(MarsDemographicsDto record) {
        List<ParticipantPhoneDTO> phoneList = new ArrayList<>();
        if (record.getParticipantNightPhone() != null) {
            ParticipantPhoneDTO homePhone = new ParticipantPhoneDTO();
            homePhone.setPhoneNumber(record.getParticipantNightPhone());
            homePhone.setPhoneType(ProjectConstants.HOME_CONTACT_TYPE);
            phoneList.add(homePhone);
        }
        if (record.getParticipantDayPhone() != null) {
            ParticipantPhoneDTO businessPhone = new ParticipantPhoneDTO();
            businessPhone.setPhoneNumber(record.getParticipantDayPhone());
            businessPhone.setPhoneType(ProjectConstants.BUSINESS_CONTACT_TYPE);
            phoneList.add(businessPhone);
        }
        if (record.getParticipantFaxPhone() != null) {
            ParticipantPhoneDTO faxPhone = new ParticipantPhoneDTO();
            faxPhone.setPhoneNumber(record.getParticipantFaxPhone());
            faxPhone.setPhoneType(ProjectConstants.FAX_CONTACT_TYPE);
            phoneList.add(faxPhone);
        }
        return phoneList;
    }
}
