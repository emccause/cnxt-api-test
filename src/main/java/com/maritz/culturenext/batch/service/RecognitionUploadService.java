package com.maritz.culturenext.batch.service;

import java.util.List;

import javax.annotation.Nonnull;

import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.recognition.dto.AdvancedRecognitionFileContentDTO;

public interface RecognitionUploadService {

    /**
     * Uploading and processing an advanced recognition template file. Populating pax object only if participant is valid.
     * 
     * @param programId : is used to check eligibility
     * @param file
     * @return file content object.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773829/Upload+Advanced+Recognition+Template
     */
    public List<AdvancedRecognitionFileContentDTO> uploadAdvancedRecognitionFile(@Nonnull Long programId,
            @Nonnull MultipartFile file);

}
