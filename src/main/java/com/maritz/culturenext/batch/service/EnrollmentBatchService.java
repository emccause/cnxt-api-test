package com.maritz.culturenext.batch.service;

import java.util.Date;
import java.util.List;

import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;

public interface EnrollmentBatchService {

    /**
     * Get processing details for a specific batch by type code
     * 
     * @param batchTypes
     * @param includeInProgress
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847474/Enrollment+File+processing+Example
     */
    EnrollmentBatchFileInfoDTO getHistoryLatestByTypes(List<String> batchTypes, Boolean includeInProgress);
    
    /**
     * Get processing details for a specific enrollment batch
     * 
     * @param batchId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/95531125/GET+Enrollment+by+Batch+ID
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847474/Enrollment+File+processing+Example
     */
    EnrollmentBatchFileInfoDTO getHistory(Long batchId);
    
    /**
     * Get the complete list of processing history with specified fromDate to current date. 
     * Default will be last week (last 7 days of enrollment data )if no fromDate is provided.
     * 
     * @param fromDate
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/95531091/Get+Enrollment+History+by+Date
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/8847474/Enrollment+File+processing+Example
     */
    List<EnrollmentBatchFileInfoDTO> getHistorySince(Date fromDate);

}
