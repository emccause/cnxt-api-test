package com.maritz.culturenext.batch.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import com.maritz.core.jpa.entity.BatchFile;

public interface BatchFileService {

    /**
     * Fetches a batch file
     * @param response - response
     * @param batchId - batchId to generate file for
     */
    ResponseEntity<Object> generateBatchFile(HttpServletResponse response, Long batchId);

    boolean updateBatchFileError(BatchFile batchFile, String fileUploadError);
}
