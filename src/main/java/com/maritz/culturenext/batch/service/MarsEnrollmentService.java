package com.maritz.culturenext.batch.service;

import com.maritz.core.jpa.entity.Batch;

public interface MarsEnrollmentService {

    void marsEnrollmentUpdateJob(Batch batch);
    
}
