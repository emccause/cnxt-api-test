package com.maritz.culturenext.batch.service.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.batch.service.BatchService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class BatchServiceImpl implements BatchService {
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private BatchUtil batchUtil;
    @Inject private ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;

    @Override
    public BatchStatusDTO updateBatchStatus(BatchStatusDTO batchStatusDto) {
        Batch batch = batchUtil.validateBatchStatusDto(batchStatusDto);

        String status = batchStatusDto.getStatus();

        // Update the status
        batch.setStatusTypeCode(status);
        batchDao.update(batch);

        // Delete the batch's row(s) in STAGE_ADVANCED_REC if the batch type is ADV_REC_BULK_UPLOAD and the new status
        // is CANCELLED
        if (BatchType.ADV_REC_BULK_UPLOAD.name().equals(batch.getBatchTypeCode())
            && StatusTypeCode.CANCELLED.name().equals(status)) {
            deleteStageAdvanceRecForBatch(batch.getBatchId());
        }

        return batchStatusDto;
    }

    @Override
    public void deleteStageAdvanceRecForBatch(Long batchId) {
        List<StageAdvancedRec> advancedRecListToDelete =
            stageAdvancedRecDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).find();
        for (StageAdvancedRec advanceRec : advancedRecListToDelete) {
            stageAdvancedRecDao.delete(advanceRec);
        }
    }
}
