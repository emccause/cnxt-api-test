package com.maritz.culturenext.batch.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.core.rest.NotFoundException;
import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import org.springframework.stereotype.Component;

import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.repository.*;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.batch.dao.EnrollmentBatchEventDao;
import com.maritz.culturenext.batch.dao.EnrollmentBatchHistoryDao;
import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.EnrollmentBatchService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enrollment.services.EnrollmentFileService;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.util.DateUtil;
import org.springframework.util.CollectionUtils;

@Component
public class EnrollmentBatchServiceImpl implements EnrollmentBatchService {

    private static final String REQUEST = "request";
    private static final String BATCH_TYPE_NOT_ENROLLMENT_MESSAGE = "That batch id does not correspond to "
            + "an enrollment batch.";
    private static final String BATCH_TYPE_NOT_ENROLLMENT = "BATCH_TYPE_NOT_ENROLLMENT";

    private static final Integer SUBTRACT_ONE_WEEK = -7;

    @Inject private BatchEventRepository batchEventRepository;
    @Inject private BatchRepository batchRepository;
    @Inject private BatchUtil batchUtil;
    @Inject private EnrollmentBatchHistoryDao enrollmentBatchHistoryDao;
    @Inject private EnrollmentBatchEventDao enrollmentBatchEventDao;
    @Inject private EnrollmentFileService enrollmentFileService;

    @Override
    public EnrollmentBatchFileInfoDTO getHistoryLatestByTypes(List<String> batchTypes, Boolean includeInProgress) {
        Long latestBatchId = null;
        String batchType = null;

        // Loop through the batch types and find the latest batch id from all types
        for(String batch : batchTypes) {
            Long batchId = batchUtil.getLatestCompletedBatchInfoByType(batch, includeInProgress);

            // If batchId is greater than latestBatchId, it is more recent, so update latestBatchId and batchType
            if (latestBatchId == null || (batchId != null && batchId > latestBatchId)) {
                latestBatchId = batchId;
                batchType = batch;
            }
        }

        if (latestBatchId == null || batchType == null || !enrollmentFileService.enrollmentFolderExists()) {
            return null;
        }
        if (batchType.equalsIgnoreCase(BatchType.ENROLLMENT_LOAD.getCode()) || batchType.equalsIgnoreCase(BatchType.WORKDAY_ENROLLMENT.getCode())) {
            return getInfoForEnrollmentBatchId(latestBatchId);
        } else if (batchType.equalsIgnoreCase(BatchType.GROUP_PROCESSING.getCode())) {
            return getInfoForEnrollmentGroupBatchId(latestBatchId);
        }
        return null;
    }

    @Override
    public EnrollmentBatchFileInfoDTO getHistory(Long batchId) {
        EnrollmentBatchFileInfoDTO infoDTO = getInfoForEnrollmentBatchId(batchId);
        if (infoDTO == null) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(BATCH_TYPE_NOT_ENROLLMENT)
                    .setField(REQUEST)
                    .setMessage(BATCH_TYPE_NOT_ENROLLMENT_MESSAGE));
        }
        return infoDTO;
    }

    @Override
    public List<EnrollmentBatchFileInfoDTO> getHistorySince(Date fromDate) {
        if (fromDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, SUBTRACT_ONE_WEEK);
            fromDate = calendar.getTime();
        }
        return enrollmentBatchHistoryDao.getBatchHistorySince(fromDate);
    }

    private EnrollmentBatchFileInfoDTO getInfoForEnrollmentBatchId(Long batchId) {
        Batch batch = batchRepository.findOne(batchId);
        if (batch == null || (!batch.getBatchTypeCode().equals(BatchType.ENROLLMENT_LOAD.getCode()) && !batch.getBatchTypeCode().equals(BatchType.WORKDAY_ENROLLMENT.getCode()))) {
            return null;
        }

        EnrollmentBatchFileInfoDTO enrollmentBatchFileInfoDTO = new EnrollmentBatchFileInfoDTO();
        enrollmentBatchFileInfoDTO.setBatchId(batchId);
        enrollmentBatchFileInfoDTO.setStatus(batch.getStatusTypeCode());

        List<Map<String, Object>> batchEvents = enrollmentBatchEventDao.getBatchEvents(batchId, batch.getBatchTypeCode());

        if (!CollectionUtils.isEmpty(batchEvents)) {
            for (Map<String, Object> batchEvent : batchEvents) {
                String batchEventType = batchEvent.get(ProjectConstants.BATCH_EVENT_TYPE_CODE).toString();

                if(batchEventType.equals(BatchEventType.START_TIME.getCode())) {
                    Date startDate = DateUtils.parseDatetimeUTC(batchEvent.get(ProjectConstants.MESSAGE).toString());
                    if (startDate == null) {
                        startDate = DateUtils.parse(batchEvent.get(ProjectConstants.MESSAGE).toString());
                    }
                    enrollmentBatchFileInfoDTO.setStartDate(DateUtil.convertToUTCDateTime(startDate));
                } else if(batchEventType.equals(BatchEventType.END_TIME.getCode())) {
                    Date finishDate = DateUtils.parseDatetimeUTC(batchEvent.get(ProjectConstants.MESSAGE).toString());
                    if (finishDate == null) {
                        finishDate = DateUtils.parse(batchEvent.get(ProjectConstants.MESSAGE).toString());
                    }
                    enrollmentBatchFileInfoDTO.setFinishDate(DateUtil.convertToUTCDateTime(finishDate));
                } else if(batchEventType.equals(BatchEventType.FILE_NAME.getCode())) {
                    enrollmentBatchFileInfoDTO.setFileName(batchEvent.get(ProjectConstants.MESSAGE).toString());
                } else if(batchEventType.equals(BatchEventType.RECORDS.getCode())) {
                    enrollmentBatchFileInfoDTO.setTotalRecords(Integer.parseInt(batchEvent.get(ProjectConstants.MESSAGE).toString()));
                } else if(batchEventType.equals(BatchEventType.OK_RECORDS.getCode())) {
                    enrollmentBatchFileInfoDTO.setGoodRecords(Integer.parseInt(batchEvent.get(ProjectConstants.MESSAGE).toString()));
                } else if(batchEventType.equals(BatchEventType.ERRORS.getCode())) {
                    enrollmentBatchFileInfoDTO.setErrorRecords(Integer.parseInt(batchEvent.get(ProjectConstants.MESSAGE).toString()));
                }
            }
        }

        //Check if there is an enrollment file on the server
        try {
            enrollmentBatchFileInfoDTO.setFileOnServer(enrollmentFileService.filesOnServer(EnrollmentConstants.ENROLLMENT_SUBFOLDER));
        } catch (NotFoundException e){
            return null; //Missing Enrollment folder.
        }

        return enrollmentBatchFileInfoDTO;
    }

    private EnrollmentBatchFileInfoDTO getInfoForEnrollmentGroupBatchId(Long batchId) {
        Batch batch = batchRepository.findOne(batchId);
        if (batch == null || !batch.getBatchTypeCode().equals(BatchType.GROUP_PROCESSING.getCode())) {
            return null;
        }

        EnrollmentBatchFileInfoDTO enrollmentBatchFileInfoDTO = new EnrollmentBatchFileInfoDTO();
        enrollmentBatchFileInfoDTO.setBatchId(batchId);
        enrollmentBatchFileInfoDTO.setStatus(batch.getStatusTypeCode());

        BatchEvent setupTimeBatchEvent = batchEventRepository.findByBatchIdAndBatchEventTypeCode(batchId,
                BatchEventType.QUEUE_TIME.getCode());
        if (setupTimeBatchEvent != null) {
            Date startDate = DateUtil.convertFromString(setupTimeBatchEvent.getMessage());
            enrollmentBatchFileInfoDTO.setSetupDate(DateUtil.convertToUTCDateTime(startDate));
        }

        BatchEvent startTimeBatchEvent = batchEventRepository.findByBatchIdAndBatchEventTypeCode(batchId,
                BatchEventType.START_TIME.getCode());
        if (startTimeBatchEvent != null) {
            Date startDate = DateUtil.convertFromString(startTimeBatchEvent.getMessage());
            enrollmentBatchFileInfoDTO.setStartDate(DateUtil.convertToUTCDateTime(startDate));
        }

        BatchEvent finishTimeBatchEvent = batchEventRepository.findByBatchIdAndBatchEventTypeCode(batchId,
                BatchEventType.END_TIME.getCode());
        if (finishTimeBatchEvent != null) {
            Date finishDate = DateUtil.convertFromString(finishTimeBatchEvent.getMessage());
            enrollmentBatchFileInfoDTO.setFinishDate(DateUtil.convertToUTCDateTime(finishDate));
        }

        return enrollmentBatchFileInfoDTO;
    }
}
