package com.maritz.culturenext.batch.service;

import com.maritz.culturenext.batch.dto.BatchStatusDTO;

public interface BatchService {
    /**
     * Will update the batch status if it is currently an updateable status and is being changed to a valid status option
     * and will remove a row from STAGE_ADVANCED_REC if the status is CANCELLED.
     *
     * @param batchStatusDto contains batch status information
     * @return BatchStatusDTO containing the request body
     *
     * https://maritz.atlassian.net/wiki/display/M365/PUT+-+Update+Batch+Status
     */
    BatchStatusDTO updateBatchStatus(BatchStatusDTO batchStatusDto);

    /**
     * Deletes all records from STAGE_ADVANCED_REC that are tied to the batchID.
     * This is only used for CANCELLED or FAILED batches - so we don't clutter up that table
     * @param batchId
     */
    void deleteStageAdvanceRecForBatch(Long batchId);
}
