package com.maritz.culturenext.batch.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import com.maritz.culturenext.batch.dto.BatchFileInfoDTO;
import com.maritz.culturenext.batch.dto.PointLoadActivityUpdateRequestDTO;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;

public interface PointLoadActivityService {

    /**
     * TODO javadocs
     * 
     * @param fromDate
     * @param statusString
     * @return
     */
    List<PointLoadBatchFileInfoDTO> getPointLoadActivity(Date fromDate, String statusString);

    /**
     * TODO javadocs
     * 
     * @param paxId
     * @param mimeType
     * @param fileName
     * @param size
     * @param inputStream
     * @return
     * @throws Exception
     */
    BatchFileInfoDTO uploadPointLoadFile(Long paxId, String mimeType, String fileName, long size,
            InputStream inputStream) throws Exception;

    /**
     * TODO javadocs
     * 
     * @param pointLoadBatchFileInfoRequestDTO
     * @return
     */
    PointLoadBatchFileInfoDTO
            updatePointLoadActivity(PointLoadActivityUpdateRequestDTO pointLoadBatchFileInfoRequestDTO);
}
