package com.maritz.culturenext.batch;

import java.util.ArrayList;
import java.util.List;

public enum PointLoadUpdateAction {
    CANCEL("CANCEL"),
    RELEASE("RELEASE"),
    REPROCESS("REPROCESS"),
    FAILED_RELEASE("FAILED_RELEASE"),
    FAILED_CANCEL("FAILED_CANCEL")
    ;

    private final String action;

    /**
     * Private constructor (can't create any other actions)
     *
     * @param action action value
     */
    private PointLoadUpdateAction(final String action) {
        this.action = action;
    }

    /**
     * action value
     *
     * @return action value
     */
    public String getAction() {
        return this.action;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return action;
    }

    /**
     * Get a list of all update actions.
     *
     * @return list of all update actions
     */
    public static List<String> getUpdateActions() {
        List<String> updateActions = new ArrayList<String>();

        updateActions.add(CANCEL.getAction());
        updateActions.add(RELEASE.getAction());
        updateActions.add(REPROCESS.getAction());
        return updateActions;
    }

    /**
     * Retrieves the respective PointLoadUpdateAction from the given action.
     *
     * @param action specified action
     * @return respective PointLoadUpdateAction
     */
    public static PointLoadUpdateAction getPointLoadUpdateAction(String action) {
        for(PointLoadUpdateAction pointLoadUpdateAction : values()) {
            if(pointLoadUpdateAction.getAction().equals(action)) {
                return pointLoadUpdateAction;
            }
        }

        throw new IllegalArgumentException("Specified action is not a valid PointLoadUpdateAction!!");
    }
}
