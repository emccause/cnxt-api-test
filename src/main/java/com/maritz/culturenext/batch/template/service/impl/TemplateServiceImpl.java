package com.maritz.culturenext.batch.template.service.impl;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import com.maritz.core.security.Security;
import com.maritz.culturenext.batch.TemplateConstants;
import com.maritz.culturenext.batch.template.service.TemplateService;
import com.maritz.culturenext.batch.util.TemplateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.util.CsvFileUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.BatchFileTemplate;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.FileDownloadUtil;

import org.supercsv.cellprocessor.ift.CellProcessor;

import java.util.Map;

@Component
public class TemplateServiceImpl implements TemplateService {

    @Inject private ConcentrixDao<BatchFileTemplate> batchFileTemplateDao;
    @Inject private Environment environment;
    @Inject private Security security;

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public ResponseEntity<Object> getPointUploadTemplate(HttpServletResponse response) {

        BatchFileTemplate batchFileTemplate = batchFileTemplateDao.findBy()
                .where(ProjectConstants.BATCH_TYPE_CODE).eq(BatchType.POINT_LOAD.getCode())
                .findOne();

        if (batchFileTemplate == null) {
            throw new ErrorMessageException()
                    .addErrorMessage("request", "TEMPLATE_MISSING", "Template file cannot be located.")
                    .setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String csv = batchFileTemplate.getFileContents();
        String filename = batchFileTemplate.getFilename();

        FileDownloadUtil.downloadFile(response, csv, MediaTypesEnum.TEXT_CSV.value(), filename);

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    @Override
    public void generateAdvancedRecognitionTemplate(HttpServletResponse response) {
        Map<String, Object[]> recognitionTemplateCSVProps = TemplateUtil.createRecognitionTemplateCSVProperties();

        String[] headers = (String[]) recognitionTemplateCSVProps.get(TemplateConstants.HEADERS);
        String[] fieldMapping = (String[]) recognitionTemplateCSVProps.get(TemplateConstants.FIELD_MAPPING);
        CellProcessor[] processor = (CellProcessor[]) recognitionTemplateCSVProps.get(TemplateConstants.PROCESSORS);

        boolean reportsLoggingEnabled = Boolean.valueOf(environment.getProperty("reports.logging.enabled"));
        long startTime = System.currentTimeMillis();

        if(reportsLoggingEnabled) {
            logger.info("generateAdvancedRecognitionTemplate is calling writeDownloadCSVFile with the following params. " +
                            "fileName {} dto {} paxId {}"
                    , TemplateConstants.RECOGNITION_TEMPLATE,null,security.getPaxId());
        }

        CsvFileUtil.writeDownloadCSVFile(response, TemplateConstants.RECOGNITION_TEMPLATE, null, processor, headers,
                fieldMapping);

        if(reportsLoggingEnabled){
            long estimatedTime = System.currentTimeMillis() - startTime;
            logger.info("generateAdvancedRecognitionTemplate execution has been completed in {} MILLISECONDS with the following params. " +
                            "fileName {} dto {} paxId {}"
                    , TemplateConstants.RECOGNITION_TEMPLATE,null,security.getPaxId());
        }
    }


    @Override
    public ResponseEntity<Object> generateTemplate(HttpServletResponse response, String type) {
        BatchFileTemplate batchFileTemplate = batchFileTemplateDao.findBy()
                .where(ProjectConstants.BATCH_TYPE_CODE).eq(type)
                .findOne();

        if (batchFileTemplate == null) {
            throw new ErrorMessageException()
                    .addErrorMessage(TemplateConstants.INVALID_TEMPLATE_TYPE_MESSAGE)
                    .setStatus(HttpStatus.BAD_REQUEST);
        }

        String csv = batchFileTemplate.getFileContents();
        String filename = batchFileTemplate.getFilename();

        FileDownloadUtil.downloadFile(response, csv, MediaTypesEnum.TEXT_CSV.value() ,filename);

        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
