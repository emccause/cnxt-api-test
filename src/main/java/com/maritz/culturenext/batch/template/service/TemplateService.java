package com.maritz.culturenext.batch.template.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

public interface TemplateService {
    /**
     * Fetches the file template for the point upload process
     * @param response - response
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/12156978/Point+Upload+services+Examples
     */
    public ResponseEntity<Object> getPointUploadTemplate(HttpServletResponse response);

    /**
     * Fetches the file template for advanced rec process
     * @param response - response
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773823/Get+Recognition+Template
     */
    public void generateAdvancedRecognitionTemplate(HttpServletResponse response);

    /**
     * Fetches the file template for any valid type
     * @param response - response
     * @param type - Valid types are ADV_REC_BULK_UPLOAD, CPTD, or ADV_REC_UPLOAD
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/97093802/Get+CSV+Template+by+type
     */
    public ResponseEntity<Object> generateTemplate(HttpServletResponse response, String type);
}
