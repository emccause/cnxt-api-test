package com.maritz.culturenext.batch.template.rest;

import java.util.Map;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.batch.template.service.TemplateService;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("templates")
@Api(value = "/batch-processes", description = "Endpoint for batch related processes")
public class TemplateRestService {

    @Inject private TemplateService templateService;

    //rest/templates/point-upload
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "point-upload", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the file-template for the Point Upload process.",
            notes = "This will be used to create the Points File to be uploaded into the system for point "
                    + "deposit transactions. The endpoint will return the template file in csv format. "
                    + "This template would remain versioned with the service that processes it.<br><br>"
                    + "The 'Points Upload' template contains the following values: Participant ID,Point Amount,"
                    + "Program Name,Budget Name,Description All fields are required.")
    @ApiResponses(
            value = { @ApiResponse(code = 200, message = "successfully retrieved and returned the notification count",
                    response = Map.class), @ApiResponse(code = 400, message = "Status is empty.") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPointUploadTemplate(HttpServletResponse response) throws Throwable {
        return new ResponseEntity<Object>(templateService.getPointUploadTemplate(response), HttpStatus.OK);
    }

    //rest/templates/recognitions
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM')")
    @RequestMapping(value = "recognitions", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the advanced recognition template")
    @ApiResponse(code = 200, message = "Successful retrieved advanced recognition template", response = Map.class)
    @Permission("PUBLIC")
    public ResponseEntity<Object> getAdvancedRecognitionTemplate(HttpServletResponse response) throws Throwable {
        templateService.generateAdvancedRecognitionTemplate(response);

        return new ResponseEntity<Object>(HttpStatus.OK);
    }

    //rest/templates/${type}
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM')")
    @RequestMapping(value = "{type}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets a template")
    @ApiResponse(code = 200, message = "Successful retrieval of file template", response = Map.class)
    @Permission("PUBLIC")
    public ResponseEntity<Object> getTemplate(
            @ApiParam(value = "The type of template to fetch") @PathVariable(TYPE_REST_PARAM) String type,
            HttpServletResponse response
    ) throws Throwable {
        templateService.generateTemplate(response, type);
        return new ResponseEntity<Object>(HttpStatus.OK);
    }
}
