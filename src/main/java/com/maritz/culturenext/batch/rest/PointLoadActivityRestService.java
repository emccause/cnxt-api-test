package com.maritz.culturenext.batch.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;
import com.maritz.culturenext.batch.dto.BatchFileInfoDTO;
import com.maritz.culturenext.batch.dto.PointLoadActivityUpdateRequestDTO;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.PointLoadActivityService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("")
@Api(value = "/batch-processes", description = "Endpoint for batch related processes")
public class PointLoadActivityRestService {

    @Inject private BatchUtil batchUtil;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private PointLoadActivityService pointLoadService;
    @Inject private Security security;

    //rest/point-uploads
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "point-uploads", method = RequestMethod.GET)
    @ApiOperation(value = "Gets point upload history.",
            notes = "This returns point upload history. It gets current and history records, and also archives records "
                    + "older than 6 months.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully found and retrieved the correct point upload process details") })
    @Permission("PUBLIC")
    public List<PointLoadBatchFileInfoDTO> getPointLoadActivity(
            @ApiParam(
                    value = "Possible Statuses:<br>NEW<br>IN_PROGRESS<br>ERROR<br>PENDING_RELEASE<br>BUDGET_EXCEEDED"
                            + "<br>COMPLETE<br>CANCELLED<br>FAILED<br>FAILED_RELEASE<br>FAILED_CANCEL<br>ARCHIVED",
                    required = false) @RequestParam(value = STATUS_REST_PARAM, required = false) String status,
            @ApiParam(
                    value = "Date to start from, default is to fetch 6 months of history records, plus the staged "
                            + "records (new, in_progress, pending_release)",
                    required = false) @RequestParam(value = FROM_DATE_REST_PARAM, required = false) String fromDateStr) {
        
        // Fix - After upgrading core 4.5.3, DateUtils.parse method is returning current date when argument is null.
        // null fromDate will be transformed to six months prior current date on pointLoadService inner function.
        Date fromDate = fromDateStr != null ? DateUtils.parse(fromDateStr) : null;
        
        return pointLoadService.getPointLoadActivity(fromDate, status);
    }

    //rest/point-uploads
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "point-uploads", method = RequestMethod.POST)
    @ApiOperation(value = "Upload the Point-Upload file for point deposit transactions.",
            notes = "Upload and save the file to the"
                    + "database for processing. returns success or failure for the upload file process.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully found and retrieved the correct enrollment process details",
            response = PointLoadBatchFileInfoDTO.class) })
    @Permission("PUBLIC")
    public BatchFileInfoDTO uploadPointLoadFile(@ApiParam(
            value = "Target file the user wishes to upload") @RequestParam(value = FILE_REST_PARAM) MultipartFile fileUpload)
            throws Exception {
        if (fileUpload == null
                || !ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.contains(fileUpload.getContentType().toLowerCase())) {
            throw new NoHandlerFoundException(null, null, null);
        }

        return pointLoadService.uploadPointLoadFile(security.getPaxId(),
                fileUpload.getContentType(), fileUpload.getOriginalFilename(), fileUpload.getSize(),
                fileUpload.getInputStream());
    }

    //rest/point-uploads/~/errorlog
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "point-uploads/{batchId}/errorlog", method = RequestMethod.GET)
    @ApiOperation(
            value = "gets the point-upload error log file for the requested batchId. The endpoint will return "
                    + "the error file.")
    @ApiResponses(
            value = { @ApiResponse(code = 200, message = "successfully found and retrieved the correct error log") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getErrorLogPointUpload(
            @ApiParam(
                    value = "The id of the batch the user wishes to retrieve the error log for") 
                    @PathVariable(BATCH_ID_REST_PARAM) String batchIdString,
            HttpServletResponse response) throws Throwable {

        Long batchId = convertPathVariablesUtil.getId(batchIdString);

        return new ResponseEntity<Object>(batchUtil.getErrorLogForBatch(batchId, response, BatchType.POINT_LOAD.getCode()), HttpStatus.OK);
    }

    //rest/point-uploads
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "point-uploads", method = RequestMethod.PUT)
    @ApiOperation(
            value = "Update the status/action for point-upload batch - includes only the fields that are eligible for "
                    + "update (i.e. status) for the batch-Id.",
            notes = "The process/action related to the 'action' will need to be performed by the API and relevant "
                    + "'status' needs to be sent back in the response. The status will be validated per action<br><br>"
                    + "Statuses specifically related to Point Upload activity include the following: NEW, IN_PROGRESS ,"
                    + " ERROR, PENDING_RELEASE(for upload-successful), BUDGET_EXCEEDED, COMPLETE "
                    + "(For processing completed), CANCELLED, FAILED, FAILED_RELEASE, FAILED_CANCEL, ARCHIVED<br><br>"
                    + "*Actions to send are :*<br>"
                    + "Cancel (CANCEL) - Only from PENDING_RELEASE or BUDGET_EXCEEDED<br>"
                    + "Release (RELEASE) - Only from PENDING_RELEASE<br>"
                    + "Reprocess (REPROCESS) - Only from BUDGET_EXCEEDED<br>"
                    + "The 'Actions' to be performed and resulting 'Status' results in Response are:<br>"
                    + "*Actions -> Status results in Response:*<br><br>"
                    + "New (NEW) > Processing (IN_PROGRESS) (returned immediately)<br>"
                    + "Reprocess (REPROCESS) > Processing (IN_PROGRESS) "
                    + "(returned immediately, async job executes later)<br>"
                    + "Release (RELEASE) > Completed (COMPLETE)<br>"
                    + "Release (RELEASE) > Failed Release (FAILED_RELEASE)<br>"
                    + "Cancel (CANCEL) > Cancelled (CANCELLED)<br>"
                    + "Cancel (CANCEL) > Failed Cancelled (FAILED_CANCEL)<br>"
                    + "*Async Transitions - Change on server-side detected on next fetch*<br><br>"
                    + "Processing (IN_PROGRESS) > Error (ERROR)<br>"
                    + "Processing (IN_PROGRESS) > Budget Exceeded (BUDGET_EXCEEDED)<br>"
                    + "Processing (IN_PROGRESS) > Pending Release (PENDING_RELEASE)<br>"
                    + "Processing (IN_PROGRESS) > Failed (FAILED)<br><br>"
                    + "Note: File upload errors handled as 400/422 responses (not statuses), defined in MP-2380")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully found and retrieved the correct error log",
            response = PointLoadBatchFileInfoDTO.class) })
    @Permission("PUBLIC")
    public PointLoadBatchFileInfoDTO updatePointLoadActivity(
            @ApiParam(
                    value = "DTO containing the batch id and an action.<br><br>Possible actions:<br>CANCEL<br>RELEASE"
                            + "<br>REPROCESS") 
            @RequestBody PointLoadActivityUpdateRequestDTO pointLoadActivityUpdateRequestDTO)
            throws Throwable {
        return pointLoadService.updatePointLoadActivity(pointLoadActivityUpdateRequestDTO);

    }
}
