package com.maritz.culturenext.batch.rest;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/files", description = "Endpoints for downloading batch files")
public class BatchFileRestService {

    @Inject private BatchUtil batchUtil;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private BatchFileService batchFileService;

    //rest/files/~
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = "files/{batchId}", method = RequestMethod.GET)
    @ApiOperation(value = "Get the uploaded file associated to the passed in batch ID.")
    @ApiResponses(value = {
            @ApiResponse(code = 200,
                    message = "successfully found and retrieved the originally uploaded file",
                    response = ResponseEntity.class),
            @ApiResponse(code = 400, message = "no batchId entered, or the id does not correspond to a Batch") })
    @Permission("PUBLIC")

    public ResponseEntity<Object> getBatchFile(
            @ApiParam(value = "The Id for the specific batch file you want.") @PathVariable(BATCH_ID_REST_PARAM) String batchIdString,
            HttpServletResponse response) throws Throwable {

        Long batchId = convertPathVariablesUtil.getId(batchIdString);
        return new ResponseEntity<Object>(batchFileService.generateBatchFile(response, batchId), HttpStatus.OK);
    }

    //rest/files/~/errorlog
    @PreAuthorize("@security.hasRole('ADMIN')")
    @RequestMapping(value = "files/{batchId}/errorlog", method = RequestMethod.GET)
    @ApiOperation(value = "gets the error log file for the requested batchId.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully found and retrieved the error file tied to this batch") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getErrorLogBatchId(
            @ApiParam(value = "The Id for the specific batch you want.")
            @PathVariable(BATCH_ID_REST_PARAM) String batchIdString,
            HttpServletResponse response) throws Throwable {

        Long batchId = convertPathVariablesUtil.getId(batchIdString);
        return new ResponseEntity<Object>(batchUtil.getErrorLogForBatch(batchId, response, null), HttpStatus.OK);
    }
}