package com.maritz.culturenext.batch.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.FILE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.FROM_DATE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.INCLUDE_IN_PROGRESS_REST_PARAM;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.annotation.Permission;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.EnrollmentBatchService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.enrollment.services.EnrollmentFileService;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/batch-processes", description = "Endpoint for batch related processes")
public class EnrollmentBatchFileRestService {

    private static final String PATH_ENROLLMENTS = "enrollments";
    private static final String PATH_ENROLLMENTS_BATCH_ID = "enrollments/{batchId}";
    private static final String PATH_ENROLLMENTS_BATCH_ID_ERRORLOG = "enrollments/{batchId}/errorlog";
    private static final String PATH_ENROLLMENTS_CONFIG_GROUPS_PROCESSING_STATUS = "enrollment-config/groups-processing-status";
    private static final String PATH_ENROLLMENTS_LATEST_UPLOADED_FILE = "enrollments/latest";
    
    @Inject private BatchUtil batchUtil;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private EnrollmentBatchService enrollmentBatchService;
    @Inject private EnrollmentFileService enrollmentFileService;

    //rest/enrollments/
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS, method = RequestMethod.POST)
    @ApiOperation(value = "Upload a .csv enrollment file. It is placed on the server in the inbox/enrollment folder "
            + "where it will sit and wait to be picked up and processed by the enrollment batch job",
            notes = "status will indicate:<br><br> 200 - Success " +
                    "403 - User does not have permission to hit this endpoint (must be an admin)"+
                    "400 - An error occurred (currently only have expected errors for non-csv files)" +
                    "422 - Internal Server Error (could not write the file to the server)")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "file was correct and succesfully put in inbox folder"),
            @ApiResponse(code = 400,message = "An error occurred (currently only have expected errors for non-csv files)"),
            @ApiResponse(code = 403,message = "User does not have permission to hit this endpoint (must be an admin)"),
            @ApiResponse(code = 422,message = "Internal Server Error (could not write the file to the server)")})

    @Permission("PUBLIC")
    public void insertEnrollementFile(
            @RequestParam(value = FILE_REST_PARAM) List<MultipartFile> filesUpload)
            throws Throwable {
        enrollmentFileService.uploadEnrollmentFile(filesUpload);
    }

    //rest/enrollments/latest
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS_LATEST_UPLOADED_FILE, method = RequestMethod.GET)
    @ApiOperation(value = "Get Latest enrollment file load processing details",
            notes = "status will indicate:<br><br> SUCCESS - file/records processed fine with or without data errors"
                    + "<br> FAILED_NO_FILE - No file present<br> FAILED_FILE_FORMAT - File format errors or "
                    + "parsing errors.<br> FAILED_TIMEOUT- File load timeout error"
                    + "<br> FAILED_UNKNOWN - Other non-data/non-format errors etc.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully retrieved and returned the latest enrollment process details",
            response = EnrollmentBatchFileInfoDTO.class) })
    @Permission("PUBLIC")
    public EnrollmentBatchFileInfoDTO getEnrollmentBatchInfoLatest(@ApiParam(
            value = "'includeInProgress=True will include in progress files if applicable",
            required = false) @RequestParam(value = INCLUDE_IN_PROGRESS_REST_PARAM, required = false) Boolean includeInProgress)
            throws Throwable {

        if (includeInProgress == null) {
            includeInProgress = Boolean.TRUE;
        }

        List<String> batchTypes = new ArrayList<>();
        batchTypes.add(BatchType.ENROLLMENT_LOAD.getCode());
        batchTypes.add(BatchType.WORKDAY_ENROLLMENT.getCode());

        
        return enrollmentBatchService.getHistoryLatestByTypes(batchTypes, includeInProgress);
    }

    //rest/enrollments/~
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS_BATCH_ID, method = RequestMethod.GET)
    @ApiOperation(value = "Get enrollment file load processing details for a specific batch.",
            notes = "this will return the details of a specified batch identified by the batchId variable.")
    @ApiResponses(value = {
            @ApiResponse(code = 200,
                    message = "successfully found and retrieved the correct enrollment process details",
                    response = EnrollmentBatchFileInfoDTO.class),
            @ApiResponse(code = 400, message = "no batchId entered, or the id does not correspond to a Batch") })
    @Permission("PUBLIC")
    public EnrollmentBatchFileInfoDTO getEnrollmentBatchInfoForId(
            @ApiParam(value = "The Id for the specific batch you want.") @PathVariable(BATCH_ID_REST_PARAM) String batchIdString)
            throws Throwable {

        Long batchId = convertPathVariablesUtil.getId(batchIdString);

        return enrollmentBatchService.getHistory(batchId);
    }

    //rest/enrollments
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS, method = RequestMethod.GET)
    @ApiOperation(value = "Get the complete list of processing history",
            notes = "You can specify a date with ?fromDate=YYYY-MM-DD.  Default will be the last 7 days of data if no "
                    + "date is provided.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully  retrieved the enrollment processing history", response = List.class) })
    @Permission("PUBLIC")
    public List<EnrollmentBatchFileInfoDTO> getEnrollmentBatchInfoSinceDate(
            @ApiParam(value = "The date the user wants to begin at when fetching the batch history",
                    required = false) @RequestParam(value = FROM_DATE_REST_PARAM, required = false) String fromDate)
            throws Throwable {

        return enrollmentBatchService.getHistorySince(DateUtils.parse(fromDate));
    }

    //rest/enrollments/~/errorlog
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS_BATCH_ID_ERRORLOG, method = RequestMethod.GET)
    @ApiOperation(value = "gets the error log file for the requested batchId.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully found and retrieved the correct enrollment process details") })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getErrorLogBatchId(
            @ApiParam(value = "The Id for the specific batch you want.") @PathVariable(BATCH_ID_REST_PARAM) String batchIdString,
            HttpServletResponse response) throws Throwable {

        Long batchId = convertPathVariablesUtil.getId(batchIdString);

        return new ResponseEntity<Object>(batchUtil.getErrorLogForBatch(batchId, response, BatchType.ENROLLMENT_LOAD.getCode()), HttpStatus.OK);
    }

    //rest/enrollment-config/groups-processing-status
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')"
            + "and !@security.isImpersonated()")
    @RequestMapping(value = PATH_ENROLLMENTS_CONFIG_GROUPS_PROCESSING_STATUS, method = RequestMethod.GET)
    @ApiOperation(value = "Return status of automated enrollment group processing.")
    @ApiResponses(value = { @ApiResponse(code = 200,
            message = "successfully found and retrieved the correct enrollment process details",
            response = EnrollmentBatchFileInfoDTO.class) })
    @Permission("PUBLIC")
    public EnrollmentBatchFileInfoDTO
            getEnrollmentConfigBatchInfoLatest(@ApiParam(
                    value = "includeInProgress will return processes under the 'IN_PROGRESS' status as well as the "
                            + "defaults.", required = false) @RequestParam(value = INCLUDE_IN_PROGRESS_REST_PARAM,
                            required = false) Boolean includeInProgress)
                    throws Throwable {

        if (includeInProgress == null) {
            includeInProgress = Boolean.TRUE;
        }

        List<String> batchTypes = new ArrayList<>();
        batchTypes.add(BatchType.GROUP_PROCESSING.getCode());

        return enrollmentBatchService.getHistoryLatestByTypes(batchTypes, includeInProgress);
    }

}
