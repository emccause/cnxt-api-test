package com.maritz.culturenext.batch.rest;

import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.batch.service.BatchService;
import com.maritz.culturenext.jpa.repository.CnxtBatchRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_NUMBER_OF_DAYS;
import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_STATUS_TYPE_CODE;

import java.util.List;

@RestController
@Api(value = "/batch")//all calls dealing with batch processes
public class BatchRestService {
    @Inject private BatchService batchService;
    @Inject private Security security;
    @Inject private CnxtBatchRepository cnxtBatchRepository;

    private static final String BATCH_URI_BASE = "batch/{" + BATCH_ID_REST_PARAM + "}";
    private static final String BATCH_INFO_URI_BASE = "batchInfo/{" + BATCH_ID_REST_PARAM + "}";
    private static final String BATCH_TYPE_URI_BASE = "batchType";

    //rest/batch/~
    @PutMapping(path = BATCH_URI_BASE)
    @ApiOperation(value="Returns the success from updating the batch status for the given batchId",
        notes="This method allows a user to update the batch status for a given batchId")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Successful update of batch status")
    })
    @Permission("PUBLIC")
    public BatchStatusDTO updateBatchStatus(
        @ApiParam(value = "Batch ID of the batch the user wishes to update the status of")
        @PathVariable(BATCH_ID_REST_PARAM) String batchIdString,
        @ApiParam(value = "Information for updating the batch status")
        @RequestBody BatchStatusDTO batchStatusDto)
        throws NoHandlerFoundException {

        Long batchId = security.getId(batchIdString, null);

        if (batchId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        batchStatusDto.setBatchId(batchId);

        return batchService.updateBatchStatus(batchStatusDto);
    }    
    
    //rest/batchType
    @PreAuthorize("@security.hasRole('ADMIN')")
    @GetMapping(path = BATCH_TYPE_URI_BASE)
    @ApiOperation(value="Returns the information of the batch type codes that are availables")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the batch code information."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<String> getBatchType(){        
    	return cnxtBatchRepository.findBatchTypeCodes();
    }
    
    //rest/batchInfo/~
    @PreAuthorize("@security.hasRole('ADMIN')")
    @GetMapping(path = BATCH_INFO_URI_BASE)
    @ApiOperation(value="Return the information of the batch for the given batchId or all the iformation from the current day")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the batch information."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<Batch> getBatchInfo(
    		@ApiParam(value = "Batch ID of the batch that the user wants to retrieve the information, "
    				+ "~ as parameter if want to pull all the information from the current day", required= false)
    		@PathVariable(value = BATCH_ID_REST_PARAM, required = false) String batchIdString
        ){        
    	Long batchId = security.getId(batchIdString, null);
    	if(batchId == null){
    		return cnxtBatchRepository.findLatestBatchInformation();
        }else {
        	return cnxtBatchRepository.findBatchInformationById(batchId);
        }
    }
    
    //rest/batchInfoByStatusTypeCode/~/~/
    @PreAuthorize("@security.hasRole('ADMIN')")
    @GetMapping(path = "batchInfoByStatusTypeCode/{numberOfDays}/{statusTypeCode}")
    @ApiOperation(value="Return the information of the batch for the given statusTypeCode and the previous number of days required, for example"
    		+ "0 current date, 1 curent day and yesterday, 2 current day and two days before, etc...")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the batch information."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<Batch> getBatchInfoByStatusTypeCode(
    		@ApiParam(value = "Number of days from which the information will be retrieved", required= true)
    		@PathVariable(value = BATCH_NUMBER_OF_DAYS, required = true) Long numberOfDays,
    		@ApiParam(value="batch type code", required = true)
    		@PathVariable(value = BATCH_STATUS_TYPE_CODE, required = true) String statusTypeCode
        ){
        	return cnxtBatchRepository.findBatchInformationByStatusTypeCode(numberOfDays, statusTypeCode);
    } 
}
