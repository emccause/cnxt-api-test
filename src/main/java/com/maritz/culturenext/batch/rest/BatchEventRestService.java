package com.maritz.culturenext.batch.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.jpa.repository.CnxtBatchEventRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import static com.maritz.culturenext.constants.RestParameterConstants.BATCH_ID_REST_PARAM;


@RestController
@Api(value = "/batchEvent")
public class BatchEventRestService {
		
	@Inject private CnxtBatchEventRepository cnxtBatchEventRepository;
	@Inject private Security security;
	
	private static final String BATCH_INFO_URI_BASE = "batchEvent/{" + BATCH_ID_REST_PARAM + "}";

		
	//rest/batchEvent/~
    @PreAuthorize("@security.hasRole('ADMIN')")
    @GetMapping(path = BATCH_INFO_URI_BASE)
    @ApiOperation(value="Return the information of the batch event for the given batchId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the batch information."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<BatchEvent> getBatchInfo(
    		@ApiParam(value = "Batch ID of the batch that the user wants to retrieve the information", required= true)
    		@PathVariable(value = BATCH_ID_REST_PARAM, required = true) String batchIdString
        ){        
    	Long batchId = security.getId(batchIdString, null);
    		return cnxtBatchEventRepository.findBatchEventInformationById(batchId);
        }
}
	


