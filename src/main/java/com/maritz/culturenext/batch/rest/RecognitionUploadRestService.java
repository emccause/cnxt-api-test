package com.maritz.culturenext.batch.rest;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.batch.service.RecognitionUploadService;
import com.maritz.culturenext.recognition.dto.AdvancedRecognitionFileContentDTO;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;

@RestController
@RequestMapping("programs/{programId}")
@Api(value = "/recognition-upload", description = "all calls dealing with recognition upload")
public class RecognitionUploadRestService {

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private RecognitionUploadService recognitionUploadService;

    //rest/programs/~/recognitions/validation
    @PreAuthorize("@security.hasRole('MTZ:ADM','CADM') and !@security.isImpersonated()")
    @RequestMapping(value = "recognitions/validation", method = RequestMethod.POST)
    @ApiOperation(
            value = "Processing an advanced recognition file and returning a JSON representation of the data entered.")
    @ApiResponse(code = 200, message = "Successful retrieved advanced recognition file content", response = Map.class)
    @Permission("PUBLIC")
    public List<AdvancedRecognitionFileContentDTO> uploadAdvancedRecognitionTemplate(
            @ApiParam(value = "Program id that was selected for the recognition.")
            @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString,
            @ApiParam(value = "Target file to upload.") @RequestParam(value = FILE_REST_PARAM,
                    required = true) MultipartFile file)
            throws Throwable {

        Long programId = convertPathVariablesUtil.getId(programIdString);

        return recognitionUploadService.uploadAdvancedRecognitionFile(programId, file);
    }
}
