package com.maritz.culturenext.batch.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.batch.BatchConstants;
import com.maritz.culturenext.batch.PointLoadUpdateAction;
import com.maritz.culturenext.batch.dao.PointLoadActivityDao;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.constants.ProjectConstants;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class PointLoadActivityDaoImpl extends AbstractDaoImpl implements PointLoadActivityDao {

    private static final String START_DATE_PARAM = "startDate";
    private static final String END_DATE_PARAM = "endDate";
    private static final String STATUSES_PARAM = "statuses";
    private static final String ARCHIVE_DATE_PARAM = "archiveDate";
    private static final String BATCH_ID_PARAM = "batchId";
    private static final String ACTION_PARAM = "action";

    private static final String POINT_LOAD_PROCESSING_HISTORY_QUERY = 
            "EXEC component.UP_GET_POINT_LOAD_ACTIVITIES :" +
            START_DATE_PARAM + ", :" + END_DATE_PARAM + ", :" + STATUSES_PARAM;

    private static final String POINT_LOAD_ARCHIVE_QUERY = 
            "UPDATE component.BATCH " +
            "SET STATUS_TYPE_CODE = '" + StatusTypeCode.ARCHIVED.name() + "' " + 
            "WHERE CREATE_DATE < :" + ARCHIVE_DATE_PARAM + " " +
            "AND STATUS_TYPE_CODE NOT IN (:" + STATUSES_PARAM + ") " + 
            "AND BATCH_TYPE_CODE = 'CPTD'";

    private static final String UPDATE_POINT_LOAD_ACTIVITY_QUERY = 
            "EXEC component.UP_UPDATE_POINT_LOAD_ACTIVITY :" +
            BATCH_ID_PARAM + ", :" + ACTION_PARAM;
    
    private static final String CREATE_POINT_LOAD_ALERT_QUERY = 
            "INSERT INTO component.ALERT (PAX_ID, TARGET_ID, PROGRAM_ID, ALERT_TYPE_CODE, ALERT_SUB_TYPE_CODE, STATUS_TYPE_CODE, STATUS_DATE, TARGET_TABLE) " +
            "SELECT PG.PAX_ID, TH.ID, TH.PROGRAM_ID, " +
                    "'" + ProjectConstants.DEFAULT_ALERT_TYPE_CODE + "', " + 
                    "'" + AlertSubType.POINT_LOAD.name() + "', " +
                    "'" + StatusTypeCode.NEW.name() + "', GETDATE(), " +
                    "'" + TableName.TRANSACTION_HEADER.name() + "' " +
            "FROM component.TRANSACTION_HEADER TH " +
            "LEFT JOIN component.PAX_GROUP PG ON PG.PAX_GROUP_ID = TH.PAX_GROUP_ID " + 
            "WHERE BATCH_ID = :" + BATCH_ID_PARAM; 

    @Nonnull
    @Override
    public List<PointLoadBatchFileInfoDTO> getPointLoadActivities(@Nonnull Date fromDate, @Nonnull Date thruDate,
            String statusString) {
        // setup of the query parameters
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(START_DATE_PARAM, fromDate);
        params.addValue(END_DATE_PARAM, thruDate);
        params.addValue(STATUSES_PARAM, statusString);

        // initiate the query
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(POINT_LOAD_PROCESSING_HISTORY_QUERY,
                params, new ColumnMapRowMapper());

        List<PointLoadBatchFileInfoDTO> batchFileInfos = new ArrayList<PointLoadBatchFileInfoDTO>();
        if (!nodes.isEmpty()) {
            // results were returned from the query so translate the results (object mapping)
            for (Map<String, Object> node : nodes) {
                batchFileInfos.add(new PointLoadBatchFileInfoDTO(node));
            }
        }
        return batchFileInfos;
    }

    @Override
    public void archivePointLoadActivities(@Nonnull Date archiveDate) {
        // set up statuses not to archive
        List<String> nonArchivableStatuses = new ArrayList<>(BatchConstants.STAGED_STATUSES);
        nonArchivableStatuses.add(StatusTypeCode.ARCHIVED.toString());

        // setup of the query parameters
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ARCHIVE_DATE_PARAM, archiveDate);
        params.addValue(STATUSES_PARAM, nonArchivableStatuses);

        // initiate the update query
        namedParameterJdbcTemplate.update(POINT_LOAD_ARCHIVE_QUERY, params);
    }

    @Nonnull
    @Override
    public PointLoadBatchFileInfoDTO updatePointLoadActivity(@Nonnull Long batchId,
            @Nonnull PointLoadUpdateAction pointLoadUpdateAction) {
        // setup for query
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_PARAM, batchId);
        params.addValue(ACTION_PARAM, pointLoadUpdateAction.getAction());

        return executeUpdatePointLoadActivityQuery(UPDATE_POINT_LOAD_ACTIVITY_QUERY, params, new ColumnMapRowMapper());
    }

    /**
     * Initiates the given Point Load update query with the given parameters. Returns the updated batch information.
     *
     * @param query update query
     * @param params parameters
     * @param rowMapper mapper for query execution
     * @return updated batch information
     */
    private PointLoadBatchFileInfoDTO executeUpdatePointLoadActivityQuery(@Nonnull String query,
            @Nonnull SqlParameterSource params, @Nonnull RowMapper<Map<String, Object>> rowMapper) {

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(query, params, rowMapper);

        // ensure correct number of mappings
        if (nodes == null || nodes.size() != 1) {
            throw new RuntimeException("The procedure to update the specified Point Load activity has failed!");
        }

        // need to convert the result and return
        Map<String, Object> pointLoadActivityMapping = nodes.get(0);

        return new PointLoadBatchFileInfoDTO(pointLoadActivityMapping);
    }
    
    @Override
    public void createPointUploadNotification(Long batchId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_PARAM, batchId);

        namedParameterJdbcTemplate.update(CREATE_POINT_LOAD_ALERT_QUERY, params);
    }
}
