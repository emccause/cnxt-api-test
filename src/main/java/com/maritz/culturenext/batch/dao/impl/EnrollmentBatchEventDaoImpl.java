package com.maritz.culturenext.batch.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.batch.dao.EnrollmentBatchEventDao;
import com.maritz.culturenext.constants.ProjectConstants;

import com.maritz.culturenext.enums.BatchType;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class EnrollmentBatchEventDaoImpl extends AbstractDaoImpl implements EnrollmentBatchEventDao {

    private static final String EVENT_QUERY = 
            "SELECT BATCH_EVENT.BATCH_EVENT_TYPE_CODE, BATCH_EVENT.MESSAGE " +
            "FROM component.BATCH_EVENT " +
            "LEFT JOIN component.BATCH_EVENT PARENT_EVENT " +
            "ON PARENT_EVENT.MESSAGE = BATCH_EVENT.BATCH_ID " +
            "AND PARENT_EVENT.BATCH_EVENT_TYPE_CODE = 'CHILD_BATCH_ID' " +
            "WHERE PARENT_EVENT.BATCH_ID = :" + ProjectConstants.BATCH_ID;

    private static final String WORKDAY_ENROLLMENT_QUERY =
            "SELECT BATCH_EVENT.BATCH_EVENT_TYPE_CODE, BATCH_EVENT.MESSAGE " +
            "FROM component.BATCH_EVENT " +
            "WHERE BATCH_EVENT_TYPE_CODE != 'TASK_NAME' AND BATCH_EVENT.BATCH_ID = :" + ProjectConstants.BATCH_ID;

    @Override
    public List<Map<String, Object>> getBatchEvents (Long batchId, String batchTypeCode) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.BATCH_ID, batchId);

        if (batchTypeCode.equals(BatchType.WORKDAY_ENROLLMENT.getCode())) {
            return namedParameterJdbcTemplate.query(WORKDAY_ENROLLMENT_QUERY, params, new CamelCaseMapRowMapper());
        } else {
            return namedParameterJdbcTemplate.query(EVENT_QUERY, params, new CamelCaseMapRowMapper());
        }
    }
}
