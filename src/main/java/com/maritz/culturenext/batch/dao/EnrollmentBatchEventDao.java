package com.maritz.culturenext.batch.dao;

import java.util.List;
import java.util.Map;

public interface EnrollmentBatchEventDao {
    
    /**
     * Retrieves list of batch events for a given batch id
     *
     * @param batchId - Unique identifier of batch record
     * @param batchType - Type of the batch being used
     * @return Collection of objects containing a batch code and batch event
     */
    List<Map<String, Object>> getBatchEvents(Long batchId, String batchType);
}
