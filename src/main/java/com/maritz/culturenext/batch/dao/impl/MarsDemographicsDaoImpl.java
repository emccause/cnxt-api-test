package com.maritz.culturenext.batch.dao.impl;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.culturenext.batch.dao.MarsDemographicsDao;
import com.maritz.culturenext.batch.dto.MarsDemographicsDto;

@Component
public class MarsDemographicsDaoImpl extends AbstractDaoImpl implements MarsDemographicsDao {

    @Override
    public List<MarsDemographicsDto> getDemographicsUpdate(Integer pageNumber, Integer pageSize) {
        return namedParameterJdbcTemplate.query(
                "EXEC component.UP_ABS_DEMOGRAPHICS_UPDATE :PAGE_NUMBER, :PAGE_SIZE",
                new MapSqlParameterSource()
                    .addValue("PAGE_NUMBER", pageNumber)
                    .addValue("PAGE_SIZE", pageSize),
                new ConcentrixBeanRowMapper<MarsDemographicsDto>()
                .setBeanClass(MarsDemographicsDto.class)
                .build()
            );
    }

}
