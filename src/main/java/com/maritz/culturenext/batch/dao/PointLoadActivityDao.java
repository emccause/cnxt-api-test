package com.maritz.culturenext.batch.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

import com.maritz.culturenext.batch.PointLoadUpdateAction;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;

public interface PointLoadActivityDao {
    @Nonnull
    List<PointLoadBatchFileInfoDTO> getPointLoadActivities(@Nonnull Date fromDate, @Nonnull Date thruDate,
            String statusString);

    void archivePointLoadActivities(@Nonnull Date archiveDate);

    @Nonnull
    PointLoadBatchFileInfoDTO updatePointLoadActivity(@Nonnull Long batchId,
            @Nonnull PointLoadUpdateAction pointLoadUpdateAction);
    
    void createPointUploadNotification(Long batchId);
}
