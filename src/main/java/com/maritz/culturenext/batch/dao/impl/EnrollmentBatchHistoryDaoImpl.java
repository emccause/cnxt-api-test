package com.maritz.culturenext.batch.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.batch.dao.EnrollmentBatchHistoryDao;
import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;
import com.maritz.culturenext.util.DateUtil;

@Repository
public class EnrollmentBatchHistoryDaoImpl extends AbstractDaoImpl implements EnrollmentBatchHistoryDao {
    
    private static final String DATE_PARAM = "date";

    private static final String HISTORY_QUERY =
            "SELECT ENROLLMENT_BATCH.BATCH_ID, ENROLLMENT_BATCH.STATUS_TYPE_CODE, START_TIME_EVENT.MESSAGE as START_TIME, " +
            "FINISH_TIME_EVENT.MESSAGE AS END_TIME, FILE_NAME_EVENT.MESSAGE as FILE_NAME, " +
            "TOTAL_RECS_EVENT.MESSAGE AS TOTAL_RECS, OKAY_RECS_EVENT.MESSAGE AS OKAY_RECS, " +
            "ERR_RECS_EVENT.MESSAGE as ERR_RECS " +
            "FROM " +
            "(" +
                    "SELECT " +
                            "CASE " +
                                "WHEN PARENT.BATCH_TYPE_CODE = 'WORKDAY_ENRL' THEN PARENT.BATCH_ID " +
                                "WHEN PARENT.BATCH_TYPE_CODE = 'ENRL' THEN MESSAGE " +
                            "END AS BATCH_ID_FOR_EVENT, " +
                            "PARENT.BATCH_ID, " +
                            "PARENT.STATUS_TYPE_CODE " +
                    "FROM component.batch PARENT " +
                    "LEFT JOIN component.BATCH_EVENT " +
                            "ON PARENT.BATCH_ID = BATCH_EVENT.BATCH_ID " +
                            "AND BATCH_EVENT.BATCH_EVENT_TYPE_CODE = 'CHILD_BATCH_ID' " +
                    "LEFT JOIN component.BATCH RECORD_BATCH " +
                            "ON BATCH_EVENT.MESSAGE = PARENT.BATCH_ID " +
                    "WHERE PARENT.CREATE_DATE > :" + DATE_PARAM + " " +
                    "AND ( " +
                            "PARENT.BATCH_TYPE_CODE = 'ENRL' " +
                            "OR PARENT.BATCH_TYPE_CODE = 'WORKDAY_ENRL' " +
                    ") " +
            ")AS ENROLLMENT_BATCH " +
            "LEFT JOIN component.BATCH_EVENT START_TIME_EVENT " +
                    "ON START_TIME_EVENT.BATCH_ID = ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND START_TIME_EVENT.BATCH_EVENT_TYPE_CODE = 'STRTIM' " +
            "LEFT JOIN component.BATCH_EVENT FINISH_TIME_EVENT " +
                    "ON FINISH_TIME_EVENT.BATCH_ID = ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND FINISH_TIME_EVENT.BATCH_EVENT_TYPE_CODE = 'ENDTIM' " +
            "LEFT JOIN component.BATCH_EVENT FILE_NAME_EVENT " +
                    "ON FILE_NAME_EVENT.BATCH_ID = ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND FILE_NAME_EVENT.BATCH_EVENT_TYPE_CODE = 'FILE' " +
            "LEFT JOIN component.BATCH_EVENT TOTAL_RECS_EVENT " +
                    "ON TOTAL_RECS_EVENT.BATCH_ID = ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND TOTAL_RECS_EVENT.BATCH_EVENT_TYPE_CODE = 'RECS' " +
            "LEFT JOIN component.BATCH_EVENT OKAY_RECS_EVENT " +
                    "ON OKAY_RECS_EVENT.BATCH_ID = ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND OKAY_RECS_EVENT.BATCH_EVENT_TYPE_CODE = 'OKRECS' " +
            "LEFT JOIN component.BATCH_EVENT ERR_RECS_EVENT " +
                    "ON ERR_RECS_EVENT.BATCH_ID =  ENROLLMENT_BATCH.BATCH_ID_FOR_EVENT " +
                    "AND ERR_RECS_EVENT.BATCH_EVENT_TYPE_CODE = 'ERRS' " +
            "ORDER BY BATCH_ID asc";

    private static final String BATCH_ID = "BATCH_ID";
    private static final String STATUS = "STATUS_TYPE_CODE";
    private static final String START_TIME = "START_TIME";
    private static final String END_TIME = "END_TIME";
    private static final String FILE_NAME = "FILE_NAME";
    private static final String TOTAL_RECS = "TOTAL_RECS";
    private static final String OKAY_RECS = "OKAY_RECS";
    private static final String ERR_RECS = "ERR_RECS";
    
    
    @Override
    public List<EnrollmentBatchFileInfoDTO> getBatchHistorySince(Date date) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(DATE_PARAM, date, Types.TIMESTAMP);

        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(HISTORY_QUERY, params, new ColumnMapRowMapper());

        ArrayList<EnrollmentBatchFileInfoDTO> resultsList = new ArrayList<EnrollmentBatchFileInfoDTO>();
        for (Map<String, Object> node: nodes) {
            EnrollmentBatchFileInfoDTO result = new EnrollmentBatchFileInfoDTO();

            result.setBatchId((Long) node.get(BATCH_ID));
            result.setStatus((String) node.get(STATUS));
            String startTime = (String) node.get(START_TIME);
            if (startTime != null) {
                Date startDate = DateUtils.parseDatetimeUTC(startTime);
                result.setStartDate(DateUtil.convertToUTCDateTime(startDate));
            }
            String endTime = (String) node.get(END_TIME);
            if (endTime != null) {
                Date endDate = DateUtils.parseDatetimeUTC(endTime);
                result.setFinishDate(DateUtil.convertToUTCDateTime(endDate));
            }
            String fileName = (String) node.get(FILE_NAME);
            if (fileName != null) {
                result.setFileName(fileName);
            }
            String totalRecs = (String) node.get(TOTAL_RECS);
            if (totalRecs != null) {
                result.setTotalRecords(Integer.parseInt(totalRecs));
            }
            String okayRecs = (String) node.get(OKAY_RECS);
            if (okayRecs != null) {
                result.setGoodRecords(Integer.parseInt(okayRecs));
            }
            String errRecs = (String) node.get(ERR_RECS);
            if (errRecs != null) {
                result.setErrorRecords(Integer.parseInt(errRecs));
            }

            resultsList.add(result);
        }
        return resultsList;
    }

}
