package com.maritz.culturenext.batch.dao;

import java.util.Date;
import java.util.List;

import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;

public interface EnrollmentBatchHistoryDao {
    
    List<EnrollmentBatchFileInfoDTO> getBatchHistorySince(Date date);
    
}
