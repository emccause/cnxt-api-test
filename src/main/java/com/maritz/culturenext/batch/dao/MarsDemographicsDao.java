package com.maritz.culturenext.batch.dao;

import java.util.List;

import com.maritz.culturenext.batch.dto.MarsDemographicsDto;

public interface MarsDemographicsDao {
    
    /**
     * Fetches MARS account details to create / update
     * 
     * @param pageNumber - page number to return
     * @param pageSize - size of page
     * @return complete record set
     */
    List<MarsDemographicsDto> getDemographicsUpdate(Integer pageNumber, Integer pageSize);
    
}
