package com.maritz.culturenext.batch;

public class RecognitionUploadConstants {
    
    // Errors
    public static final String FILE_FIELD = "file";
    public static final String ERROR_FILE_CONTENT_TYPE_INVALID = "ERROR_FILE_CONTENT_TYPE_INVALID";
    public static final String ERROR_FILE_CONTENT_TYPE_INVALID_MSG = "Invalid file content type";
    public static final String ERROR_FILE_CONTENT_DATA_INVALID = "ERROR_FILE_CONTENT_DATA_INVALID";
    public static final String ERROR_FILE_CONTENT_DATA_INVALID_MSG = "Invalid file content data";
    public static final String ERROR_PAX_ID_DOESNT_EXIST = "ERROR_PAX_ID_DOESNT_EXIST";
    public static final Long DEFAULT_PAX_ID_WHEN_IT_DOESNT_EXIST = -1L;
}
