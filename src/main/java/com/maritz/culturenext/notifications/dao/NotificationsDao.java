package com.maritz.culturenext.notifications.dao;

import java.util.List;
import java.util.Map;

public interface NotificationsDao {
    
    /**
     * Returns raw data for nomination details for the pax provided.  status and type are optional filter lists
     * 
     * @param paxId - id of pax to fetch notifications for (required)
     * @param alertStatusListString - comma separated list of statuses to limit results to (optional)
     * @param alertTypeListString - comma separated list of notification types to limit results to (optional)
     * @param pageNumber - page number being fetched (required)
     * @param pageSize - size of page being fetched (required)
     * @return List<Map<String, Object>> - raw contents of page of notification data.
     */
    List<Map<String, Object>> getNotificationList(
            Long loggedInPaxId, String alertStatusListString, String alertTypeListString,
            Integer pageNumber, Integer pageSize, Long alertId
        );

}
