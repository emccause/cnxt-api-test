package com.maritz.culturenext.notifications.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.notifications.dao.NotificationsDao;

@Component
public class NotificationsDaoImpl extends AbstractDaoImpl implements NotificationsDao {
    
    private static final String LOGGED_IN_PAX_ID_PARAM = "LOGGED_IN_PAX_ID";
    private static final String ALERT_STATUS_LIST_PARAM = "ALERT_STATUS_LIST";
    private static final String ALERT_TYPE_LIST_PARAM = "ALERT_TYPE_LIST";
    private static final String PAGE_NUMBER_PARAM = "PAGE_NUMBER";
    private static final String PAGE_SIZE_PARAM = "PAGE_SIZE";
    private static final String ALERT_ID_PARAM = "ALERT_ID";
    
    private static final String QUERY = "EXEC component.UP_NOTIFICATION_LIST" +
            " :" + LOGGED_IN_PAX_ID_PARAM +
            ", :" + ALERT_STATUS_LIST_PARAM +
            ", :" + ALERT_TYPE_LIST_PARAM +
            ", :" + PAGE_NUMBER_PARAM +
            ", :" + PAGE_SIZE_PARAM +
            ", :" + ALERT_ID_PARAM;

    /**
     * Returns raw data for nomination details for the pax provided.  status and type are optional filter lists
     * 
     * @param paxId - id of pax to fetch notifications for (required)
     * @param alertStatusListString - comma separated list of statuses to limit results to (optional)
     * @param alertTypeListString - comma separated list of notification types to limit results to (optional)
     * @param pageNumber - page number being fetched (required)
     * @param pageSize - size of page being fetched (required)
     * @return List<Map<String, Object>> - raw contents of page of notification data.
     */
    @Override
    public List<Map<String, Object>> getNotificationList(Long loggedInPaxId, String alertStatusListString,
            String alertTypeListString, Integer pageNumber, Integer pageSize, Long alertId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(LOGGED_IN_PAX_ID_PARAM, loggedInPaxId);
        params.addValue(ALERT_STATUS_LIST_PARAM, alertStatusListString);
        params.addValue(ALERT_TYPE_LIST_PARAM, alertTypeListString);
        params.addValue(PAGE_NUMBER_PARAM, pageNumber);
        params.addValue(PAGE_SIZE_PARAM, pageSize);
        params.addValue(ALERT_ID_PARAM, alertId);
        
        return namedParameterJdbcTemplate.query(QUERY, params, 
                new ColumnMapRowMapper());
    }

}
