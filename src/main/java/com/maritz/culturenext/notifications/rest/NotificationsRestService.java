package com.maritz.culturenext.notifications.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import static com.maritz.culturenext.constants.RestParameterConstants.*;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.notifications.dto.NotificationCountDTO;
import com.maritz.culturenext.notifications.dto.NotificationDTO;
import com.maritz.culturenext.notifications.services.NotificationsService;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;
import com.maritz.culturenext.constants.ProjectConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value="/notifications", description="All end points dealing with the news feed")
public class NotificationsRestService {
    
    private static final String GET_NOTIFICATION_BY_ID_PATH = "participants/{paxId}/notifications/{notificationId}";
    private static final String GET_NOTIFICATION_LIST_PATH = "participants/{paxId}/notifications";
    private static final String GET_NOTIFICATION_COUNT_PATH = "participants/{paxId}/notifications/count";

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private NotificationsService notificationService;
    @Inject private Security security;

    //rest/participants/~/notifications/~
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = GET_NOTIFICATION_BY_ID_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "This endpoint retrieves an single notification by id for the requesting participant.",
            notes = "If the ID belongs to a notification that is not for the requesting particpant, a 403 will be "
                    + "returned.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "successfully get notification"),
            @ApiResponse(code = 404, message = "pax or notification are not found") })
    @Permission("PUBLIC")
    public NotificationDTO getNotificationById(
            @ApiParam(value = "Participant Id", required = true)
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "Notification Id", required = true)
                @PathVariable(NOTIFICATION_ID_REST_PARAM) String notificationIdString
            ) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        NotificationDTO notification = notificationService.getNotificationById(paxId, convertPathVariablesUtil.getId(notificationIdString));
        
        if (notification == null) { // no notification with that id exists for the logged in user
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return notification;
    }

    //rest/participants/~/notifications
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = GET_NOTIFICATION_LIST_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Returns all 'active' notifications for the user. i.e. all Approval notifications "
                    + "(regardless of time) and all other active notifications (for last 30 days) "
                    + "for the logged in user. (approvals would be post-mvp)"
                    + "active notifications include status-types of VIEWED/ NEW (not ARCHIVED)"
                    + "RECOGNITION_APPROVED, RECOGNITION_DENIED types are for recognitions that were submitted by "
                    + "logged in user and were just approved or denied. (approvals would be post-mvp)"
                    + "The 'toPax' returned is based on the requestor and is the direct report of any manager "
                    + "retrieving a notification if the notification is for only 1 direct report "
                    + "(otherwise for multi-recipient, count is used to display a notification message)."
                    + "directReportCount is the count of the directReports for a manager if its a type of "
                    + "MANAGER_RECOGNITION. It is used to display messages like '3 direct reports and 10 others' were "
                    + "recognized."
                    + "recipientCount is the total count of recipients (will be >1 for nomination with multiple "
                    + "recognitions. will be 1 for the recipient)."
                    + "If its a multiple recipient nomination, the recipientCount and directReportCount can be greater "
                    + "than one. The private message, points-issued will be populated for a manager also.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Loaded notifications", response = NominationRequestDTO.class),
            @ApiResponse(code = 403, message = "Requested someone else's notifications") })
    @Permission("PUBLIC")
    public PaginatedResponseObject<NotificationDTO> getNotificationList(
        @ApiParam(value = "Participant Id", required = true) 
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
        @ApiParam(value = "values for this are VIEWED, NEW,ARCHIVED", required = true) 
            @RequestParam(value = STATUS_REST_PARAM, required = false) String alertStatusListString,
        @ApiParam(value = "values are APPROVAL, RECOGNITION (recognition for the requestor), MANAGER_RECOGNITION "
                        + "(recognition for the requestor's reports), RECOGNITION_APPROVED, RECOGNITION_DENIED, "
                        + "POINT_UPLOAD", required = false) 
            @RequestParam(value = TYPE_REST_PARAM, required = false) String alertTypeListString,
        @ApiParam(value = "Page of notification used to paginate the request",required = false) 
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber,
        @ApiParam(value = "The number of records per page.", required = false) 
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);
        
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(PAX_ID_REST_PARAM, paxId);
        
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(STATUS_REST_PARAM, alertStatusListString);
        requestParamMap.put(TYPE_REST_PARAM, alertTypeListString);

        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(GET_NOTIFICATION_LIST_PATH).setPathParamMap(pathParamMap)
                    .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);
        
        return notificationService.getNotificationList(
                requestDetails, paxId, alertStatusListString, alertTypeListString, pageNumber, pageSize
            );
    }
    
    //rest/participants/~/notifications/count
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = GET_NOTIFICATION_COUNT_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Returns a count of notifications that match the status and type filters provided (optional)")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "successfully retrieved and returned the notification count", response = NotificationCountDTO.class),
            @ApiResponse(code = 400, message = "Status is empty.") })
    @Permission("PUBLIC")
    public NotificationCountDTO getNotificationCount(
        @ApiParam(value = "Participant Id", required = true) 
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
        @ApiParam(value = "values for this are VIEWED, NEW,ARCHIVED", required = true) 
            @RequestParam(value = STATUS_REST_PARAM, required = false) String alertStatusListString,
        @ApiParam(value = "values are APPROVAL, RECOGNITION (recognition for the requestor), MANAGER_RECOGNITION "
                        + "(recognition for the requestor's reports), RECOGNITION_APPROVED, RECOGNITION_DENIED, "
                        + "POINT_UPLOAD", required = false) 
            @RequestParam(value = TYPE_REST_PARAM, required = false) String alertTypeListString) {
        
        Long paxId = security.getPaxId(paxIdString);
        
        return notificationService.getNotificationCount(paxId, alertStatusListString, alertTypeListString);
    }

    //rest/participants/~/notifications/~/recognitions
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = "participants/{paxId}/notifications/{notificationId}/recognitions", method = RequestMethod.GET)
    @ApiOperation(value = "This endpoint retrieves a list of recognitions for the given notification id sorted by first, "
                    + "then last name of recognition pax.",
            notes = "If the ID belongs to a notification that is not for the requesting particpant, a 403 will be "
                    + "returned.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "successfully get notification"),
            @ApiResponse(code = 400, message = "pax or notification are not found"),
            @ApiResponse(code = 403, message = "Notification exists but belongs to a different participant") })
    @Permission("PUBLIC")
    public List<RecognitionDetailsDTO> getRecognitionsByNotificationId(@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(NOTIFICATION_ID_REST_PARAM) String notificationIdString,
            @ApiParam(value = "ExcludePrivate, if set to true, private recipients will not be included in the list") 
                @RequestParam(value = EXCLUDE_PRIVATE_REST_PARAM, required = false) Boolean excludePrivate,
            @ApiParam(value = "The Manager Pax Id is only checked when the exclude private flag is set to false, "
                            + "in which case we return all private records")
                @RequestParam(value = MANAGER_PAX_ID_REST_PARAM, required = false) Long managerPaxId,
            @ApiParam(value = "The comma-delimited list of statuses, the recognition records to be returned for these "
                            + "statuses")
                @RequestParam(value = STATUS_REST_PARAM, required = false) String status,
                //TODO Pagination Techdebt: MP-7501 & MP-7519
            @ApiParam(value = "The number of people per page used to Paginate the request") 
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @ApiParam(value = "Page of the people raise feed used to Paginate the request") 
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page
                )throws NoHandlerFoundException {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        Long notificationId = convertPathVariablesUtil.getId(notificationIdString);

        return notificationService.getRecognitionsByNotification(paxId, notificationId,
                excludePrivate, managerPaxId, status, page, size);
    }
}
