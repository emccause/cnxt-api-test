package com.maritz.culturenext.notifications.services;

import java.util.List;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.notifications.dto.NotificationCountDTO;
import com.maritz.culturenext.notifications.dto.NotificationDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;

public interface NotificationsService {

    /**
     * Returns detail for a single nomination - same as what is calculated below, except for it calls to
     *  the activity feed service to populate the activity tied to that notification, if it exists.
     *  Will load pending activities for approval notifications instead of only approved data.
     * 
     * @param paxId - id of pax to fetch notification for (required) 
     * @param alertId - id of notification to fetch (required)
     * @return NotificationDTO - notification details with activity populated
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/77889748/GET+Notification
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/25001986/Get+Notification+By+Id
     */
    NotificationDTO getNotificationById(Long paxId, Long alertId);
    
    /**
     * Returns paginated list of nomination details for the pax provided.  status and type are optional filter lists
     * 
     * @param requestDetails - constructed in rest layer, used in pagination flow (optional)
     * @param paxId - id of pax to fetch notifications for (required)
     * @param alertStatusListString - comma separated list of statuses to limit results to (optional)
     * @param alertTypeListString - comma separated list of notification types to limit results to (optional)
     * @param pageNumber - page number being fetched (required)
     * @param pageSize - size of page being fetched (required)
     * @return PaginatedResponseObject<NotificationDTO> - page of notification data.
     *         if requestDetails is not populated, only the data property is populated.
     * 
     * https://maritz.atlassian.net/wiki/display/M365/GET+Notification
     */
    PaginatedResponseObject<NotificationDTO> getNotificationList(
            PaginationRequestDetails requestDetails, Long paxId, String alertStatusListString,
            String alertTypeListString, Integer pageNumber, Integer pageSize
        );
    
    /**
     * Returns the count of notifications that match the requested status and type filters.
     * 
     * @param paxId - id of pax to fetch notification count for (Required)
     * @param alertStatusListString - comma separated list of statuses to limit results to (optional)
     * @param alertTypeListString - comma separated list of notification types to limit results to (optional)
     * @return NotificationCountDTO - Count of notifications that match the filters provided.
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/23560201/Get+Notification+Count+by+Status
     */
    NotificationCountDTO getNotificationCount(Long paxId, String alertStatusListString, String alertTypeListString);

    /**
     * Returns a list of recognitions for the given notification id sorted by first, then last name of recognition pax
     * 
     * @param paxId id of logged in pax
     * @param nominationId id of nomination to get list for
     * @param excludePrivate whether recipients with "private" share rec preference should be included
     * @param managerPaxId not actually used, but will throw an error if this doesn't match the logged in pax
     * @param delimitedStatusList comma delimited list of statuses to limit results to
     * @param returnApproverRecPoints : if it is true, pointsIssued property is populated for approvers.
     * @param page for pagination - what page of results to return (default is 1)
     * @param size for pagination - size of pages (default is 50)
     * 
     * @return List<RecognitionDetailsDTO> list of recognition details.
     */
    List<RecognitionDetailsDTO> getRecognitionsByNotification(Long paxId, Long notificationId, Boolean excludePrivate,
            Long managerPaxId, String delimitedStatusList, Integer page, Integer size);
}
