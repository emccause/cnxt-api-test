
package com.maritz.culturenext.notifications.services.impl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.AlertMisc;
import com.maritz.core.jpa.support.util.Tuple3;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.alert.constants.AlertConstants;
import com.maritz.culturenext.alert.dao.AlertDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.notifications.constants.NotificationConstants;
import com.maritz.culturenext.notifications.dao.NotificationsDao;
import com.maritz.culturenext.notifications.dto.*;
import com.maritz.culturenext.notifications.services.NotificationsService;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.impl.DetailProfileServiceImpl;
import com.maritz.culturenext.program.dto.MilestoneMiscDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.social.comment.dto.CommentDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.util.TranslationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import com.maritz.culturenext.jpa.repository.CnxtLookupRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@EnableCaching(proxyTargetClass= true)
public class NotificationsServiceImpl implements NotificationsService {
	private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private AlertDao customAlertDao;
    @Inject private ActivityFeedService activityFeedService;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<AlertMisc> alertMiscDao;
    @Inject private NotificationsDao notificationDao;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private RecognitionService recognitionService;
    @Inject private DetailProfileServiceImpl  detailProfileInfo;    
    @Inject private CnxtLookupRepository cnxtLookupRepository;
    @Inject private TranslationUtil translationUtil;
    
    private Map<Long,String> lookupTranslationsMap = new HashMap<Long,String>();
    private Map<String,Long> lookupCategoryCodeMap = new HashMap<String,Long>();
	private Map<Long,String> lookupDescMap = new HashMap<Long,String>();
	private Map<Long, String> translations = new HashMap<Long, String>();
	
    
	public NotificationsServiceImpl(){
		
	}
	
    @Override
    public NotificationDTO getNotificationById(Long paxId, Long alertId) {
        PaginatedResponseObject<NotificationDTO> object = getNotificationDetails(
                null, paxId, null, null, ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE, alertId
            );

        if (object.getData().isEmpty()) {
            return null;
        }

        NotificationDTO notification = object.getData().get(0); // should only be one

        Boolean ignoreStatus = NotificationConstants.APPROVAL_NOTIFICATION_TYPES.contains(notification.getType());

        if (!AlertSubType.EXCLUDE_LIST.contains(notification.getType())) {
            notification.setActivity(
                    activityFeedService.getActivityById(paxId, notification.getActivityFeedItemId(), null, null, null, ignoreStatus, Boolean.TRUE,"")
            );
        }

        return notification;
    }

    @Override
    public PaginatedResponseObject<NotificationDTO> getNotificationList(
            PaginationRequestDetails requestDetails, Long paxId, String alertStatusListString,
            String alertTypeListString, Integer pageNumber, Integer pageSize
        ) {
        return getNotificationDetails(
                requestDetails, paxId, alertStatusListString, alertTypeListString, pageNumber, pageSize, null
            );
    }

    /**
     * Returns paginated list of nomination details for the pax provided.  status and type are optional filter lists
     *
     * @param requestDetails - constructed in rest layer, used in pagination flow (optional)
     * @param paxId - id of pax to fetch notifications for (required)
     * @param alertStatusListString - comma separated list of statuses to limit results to (optional)
     * @param alertTypeListString - comma separated list of notification types to limit results to (optional)
     * @param pageNumber - page number being fetched (required)
     * @param pageSize - size of page being fetched (required)
     * @return PaginatedResponseObject<NotificationDTO> - page of notification data.
     *         if requestDetails is not populated, only the data property is populated.
     */
    private PaginatedResponseObject<NotificationDTO> getNotificationDetails(
            PaginationRequestDetails requestDetails, Long paxId, String alertStatusListString,
            String alertTypeListString, Integer pageNumber, Integer pageSize, Long alertId
        ) {

        // Get our raw data
        List<Map<String, Object>> notificationDataList = notificationDao.getNotificationList(
                paxId, alertStatusListString, alertTypeListString, pageNumber, pageSize, alertId
            );

        Integer totalResults = 0;
        List<NotificationDTO> notificationList = new ArrayList<>();
        List<Long> paxIdsToResolveList = new ArrayList<>();
        
        String languageCode=null;
        try{            	
        		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
        	}catch(NullPointerException e){
        		languageCode = TranslationConstants.EN_US;
        }
        try {
        	populatePayoutTypeInformation(languageCode);
        }catch(NullPointerException e) {
        	logger.info("Exception when pulling lookup data", e);	
        }
        // Process raw data into DTOs
        for (Map<String, Object> notificationEntry : notificationDataList) {

            totalResults = (Integer) notificationEntry.get(NotificationConstants.TOTAL_RESULTS_FIELD);

            String notificationType = (String) notificationEntry.get(NotificationConstants.NOTIFICATION_TYPE_FIELD);
            NotificationDTO notification = null;

            // Type specific data population - data common between types is handled later
            // Comments
            if (
                    AlertSubType.COMMENTS_SUB_TYPE_LIST.contains(notificationType)
                ) {
                CommentNotificationDTO commentNotification = new CommentNotificationDTO();

                CommentDTO commentDto = new CommentDTO();

                Long commentId = (Long) notificationEntry.get(NotificationConstants.COMMENT_ID_FIELD);
                if (commentId != null) { // just in case
                    commentDto.setId(commentId);
                    commentDto.setText((String) notificationEntry.get(NotificationConstants.COMMENT_TEXT_FIELD));
                    commentDto.setTime(
                            DateUtil.convertToUTCDateTime(
                                    (Date) notificationEntry.get(NotificationConstants.COMMENT_CREATE_DATE_FIELD)
                                )
                        );
                    commentDto.setStatus((String) notificationEntry.get(NotificationConstants.COMMENT_STATUS_FIELD));
                    Long commentPaxId = (Long) notificationEntry.get(NotificationConstants.COMMENT_PAX_ID_FIELD);
                    commentDto.setPax(new EmployeeDTO().setPaxId(commentPaxId));

                    if (!paxIdsToResolveList.contains(commentPaxId)) {
                        paxIdsToResolveList.add(commentPaxId);
                    }
                }

                commentNotification.setComment(commentDto);

                // ADD REPORTER PAX FROM ALERT_MISC
                if (AlertSubType.REPORT_COMMENT_ABUSE.name().equalsIgnoreCase(notificationType)) {

                    Alert alert = alertDao.findById((Long)notificationEntry.get(NotificationConstants.NOTIFICATION_ID_FIELD));

                    AlertMisc alertMisc = alertMiscDao.findBy()
                            .where(ProjectConstants.ALERT_ID).eq(alert.getId())
                            .and(ProjectConstants.VF_NAME).eq(VfName.REPORTER_PAX.name())
                            .findOne();

                    if ( alertMisc !=null ){
                        EmployeeDTO reporterPax = participantInfoDao.getEmployeeDTO(Long.parseLong(alertMisc.getMiscData()));
                        if (reporterPax != null){
                            commentNotification.setReporterPax(reporterPax);
                        }
                    }
                }

                commentNotification.setCommenterCount(
                        (Integer) notificationEntry.get(NotificationConstants.COMMENTER_COUNT_FIELD)
                    );

                notification = commentNotification;
            }
            // likes
            else if (
                    AlertSubType.LIKES_SUB_TYPE_LIST.contains(notificationType)
                ) {
                LikeNotificationDTO likeNotification = new LikeNotificationDTO();
                likeNotification.setLikeCount((Integer) notificationEntry.get(NotificationConstants.LIKE_COUNT_FIELD));

                Long likePaxId = (Long) notificationEntry.get(NotificationConstants.LIKE_PAX_ID_FIELD);
                likeNotification.setLikePax(new EmployeeDTO().setPaxId(likePaxId));

                if (!paxIdsToResolveList.contains(likePaxId)) {
                    paxIdsToResolveList.add(likePaxId);
                }

                notification = likeNotification;
            }
            // raises
            else if (
                    AlertSubType.RAISES_SUB_TYPE_LIST.contains(notificationType)
                ) {
                RaiseNotificationDTO raiseNotification = new RaiseNotificationDTO();
                raiseNotification.setRaiseRecipientCount(
                        (Integer) notificationEntry.get(NotificationConstants.RAISE_RECIPIENT_COUNT_FIELD)
                    );

                notification = raiseNotification;
            }
            // recognitions
            else if (
                    AlertSubType.RECOGNITIONS_SUB_TYPE_LIST.contains(notificationType)
                ) {
                RecognitionNotificationDTO recognitionNotification = new RecognitionNotificationDTO();
                recognitionNotification.setNotificationHeadline(
                        (String) notificationEntry.get(NotificationConstants.NOMINATION_HEADLINE_FIELD)
                    );
                recognitionNotification.setPointsIssued(
                        (Double) notificationEntry.get(NotificationConstants.POINTS_ISSUED_FIELD)
                    );
                recognitionNotification.setPayoutType(
                        (String) notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD)
                    );
                
                recognitionNotification.setPayoutDisplayName(this.translations.get(this.lookupCategoryCodeMap.get(
                        		notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD))));
                
                notification = recognitionNotification;
            }
            // approvals
            else if (
                    AlertSubType.APPROVALS_SUB_TYPE_LIST.contains(notificationType)
                ){
                ApprovalNotificationDTO approvalNotification = new ApprovalNotificationDTO();
                approvalNotification.setTotalPoints(
                        (Double) notificationEntry.get(NotificationConstants.POINTS_ISSUED_FIELD)
                    );
                approvalNotification.setPayoutType(
                        (String) notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD)
                    );
                
                approvalNotification.setPayoutDisplayName(this.translations.get(this.lookupCategoryCodeMap.get(
                		notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD))));

                notification = approvalNotification;
            }
            // point loads
            else if (AlertSubType.POINT_LOAD.name().equalsIgnoreCase(notificationType)) {
                PointLoadNotificationDTO pointLoadNotification = new PointLoadNotificationDTO();
                pointLoadNotification.setPointsIssued(
                        (Double) notificationEntry.get(NotificationConstants.POINTS_ISSUED_FIELD)
                    );
                pointLoadNotification.setPayoutType(
                        (String) notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD)
                    );

                pointLoadNotification.setPayoutDisplayName(this.translations.get(this.lookupCategoryCodeMap.get(
                		notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD))));

                notification = pointLoadNotification;
            }
            // milestones
            else if (AlertSubType.MILESTONE_REMINDER.name().equalsIgnoreCase(notificationType)) {
                MilestoneNotificationDTO milestoneNotification = new MilestoneNotificationDTO();
                MilestoneMiscDTO milestone = new MilestoneMiscDTO();
                milestone.setDate(
                        (String) notificationEntry.get(NotificationConstants.MILESTONE_DATE_FIELD)
                    );
                milestone.setType(
                        (String) notificationEntry.get(NotificationConstants.MILESTONE_TYPE)
                    );
                if (milestone.getType().equalsIgnoreCase(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE)) {
                    milestone.setYearsOfService(
                            Integer.parseInt(notificationEntry.get(NotificationConstants.MILESTONE_YEARS_OF_SERVICE_FIELD).toString())
                        );
                }
                milestoneNotification.setMilestone(milestone);
                milestoneNotification.setPointsIssued(
                        (Double) notificationEntry.get(NotificationConstants.POINTS_ISSUED_FIELD)
                );
                milestoneNotification.setPayoutType(
                        (String) notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD)
                );
                milestoneNotification.setPayoutDisplayName(this.translations.get(this.lookupCategoryCodeMap.get(
                		notificationEntry.get(NotificationConstants.PAYOUT_TYPE_FIELD))));
                notification = milestoneNotification;
            }
            // everything else that doesn't have specific data
            else {
                notification = new NotificationDTO();
            }

            // data common to all notification types

            notification.setId((Long) notificationEntry.get(NotificationConstants.NOTIFICATION_ID_FIELD));
            notification.setStatus((String) notificationEntry.get(NotificationConstants.NOTIFICATION_STATUS_FIELD));
            notification.setCreateDate(
                    DateUtil.convertToUTCDateTime(
                            (Date) notificationEntry.get(NotificationConstants.NOTIFICATION_CREATE_DATE_FIELD)
                        )
                );
            notification.setStatusDate(
                    DateUtil.convertToUTCDateTime(
                            (Date) notificationEntry.get(NotificationConstants.NOTIFICATION_STATUS_DATE_FIELD)
                        )
                );
            // This will be null if there's no nomination (point load)
            Date nominationCreateDate = (Date) notificationEntry.get(NotificationConstants.NOMINATION_CREATE_DATE_FIELD);
            if (nominationCreateDate != null) {
                notification.setActivityCreateDate(DateUtil.convertToUTCDateTime(nominationCreateDate));
            }
            notification.setType(notificationType);
            notification.setActivityFeedItemId(
                    (Long) notificationEntry.get(NotificationConstants.ACTIVITY_FEED_ITEM_ID_FIELD)
                );
            notification.setNominationId((Long) notificationEntry.get(NotificationConstants.NOMINATION_ID_FIELD));
            notification.setProgramName((String) notificationEntry.get(NotificationConstants.PROGRAM_NAME_FIELD));

            // pax data
            Long fromPaxId = (Long) notificationEntry.get(NotificationConstants.FROM_PAX_ID_FIELD);
            if (fromPaxId != null) {
                notification.setFromPax(new EmployeeDTO().setPaxId(fromPaxId));

                if (!paxIdsToResolveList.contains(fromPaxId)) {
                    paxIdsToResolveList.add(fromPaxId);
                }
            }

            Long toPaxId = (Long) notificationEntry.get(NotificationConstants.TO_PAX_ID_FIELD);
            if (toPaxId != null) {
                notification.setToPax(new EmployeeDTO().setPaxId(toPaxId));

                if (!paxIdsToResolveList.contains(toPaxId)) {
                    paxIdsToResolveList.add(toPaxId);
                }
            }

            // counts
            NotificationDetailCountsDTO countDto = new NotificationDetailCountsDTO();
            countDto.setTotalRecipients(
                    (Integer) notificationEntry.get(NotificationConstants.TOTAL_RECIPIENT_COUNT_FIELD)
                );
            countDto.setApprovedRecipients(
                    (Integer) notificationEntry.get(NotificationConstants.APPROVED_RECIPIENT_COUNT_FIELD)
                );
            countDto.setDirectReports(
                    (Integer) notificationEntry.get(NotificationConstants.DIRECT_REPORT_COUNT_FIELD)
                );

            notification.setCounts(countDto);

            notificationList.add(notification);
        }

        // get our pax objects
        List<EntityDTO> paxList = participantInfoDao.getInfo(paxIdsToResolveList, null);

        // replace placeholders
        for (NotificationDTO notification : notificationList) {

            EmployeeDTO commentPax = null;
            EmployeeDTO likePax = null;
            EmployeeDTO fromPax = null;
            EmployeeDTO toPax = null;

            if (notification instanceof CommentNotificationDTO) {
                commentPax = ((CommentNotificationDTO) notification).getComment().getPax();
            }
            else if (notification instanceof LikeNotificationDTO) {
                likePax = ((LikeNotificationDTO) notification).getLikePax();
            }

            fromPax = notification.getFromPax();
            toPax = notification.getToPax();

            for (EntityDTO entity : paxList) {
                if (!(entity instanceof EmployeeDTO)) {
                    continue; // skip, shouldn't ever happen
                }

                EmployeeDTO pax = (EmployeeDTO) entity;

                if (commentPax != null) {
                    if (pax.getPaxId().equals(commentPax.getPaxId())) {
                        ((CommentNotificationDTO) notification).getComment().setPax(pax);
                    }
                }
                if (likePax != null) {
                    if (pax.getPaxId().equals(likePax.getPaxId())) {
                        ((LikeNotificationDTO) notification).setLikePax(pax);
                    }
                }
                if (fromPax != null) {
                    if (pax.getPaxId().equals(fromPax.getPaxId())) {
                        notification.setFromPax(pax);
                    }
                }
                if (toPax != null) {
                    if (pax.getPaxId().equals(toPax.getPaxId())) {
                        notification.setToPax(pax);
                    }
                }
            }
        }

        // done!
        return new PaginatedResponseObject<>(requestDetails, notificationList, totalResults);
    }
    

    /**
     * sets payout type related information from LOOKUP table
     * @param languageCode
     */
    public void populatePayoutTypeInformation(String languageCode) throws NullPointerException{
    	
    	List<Tuple3<Long, String, String>> lookupCategoryCodeIds = cnxtLookupRepository.getLookupCategoryCodeId();    	
    	    
    		if (lookupCategoryCodeIds != null) {

                for (Tuple3<Long,String,String> item : lookupCategoryCodeIds){
                	lookupCategoryCodeMap.put(item.getB(),item.getA());
                	lookupDescMap.put(item.getA(),item.getC());
                }
                this.translations = fetchTranslations(languageCode, lookupDescMap);
    		}
    }
    
    
    private Map<Long, String> fetchTranslations(String languageCode, Map<Long, String> defaultMap) {
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);        
        
        if (defaultMap != null) {
            List<Long> rowIds = new ArrayList<Long>(defaultMap.keySet());            
            if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {
                return defaultMap;
            }
            this.lookupTranslationsMap =
            translationUtil.getTranslationsForListOfIds(formattedLanguageCode, 
                    TableName.LOOKUP.name(), TranslationConstants.COLUMN_NAME_LOOKUP_DESC, 
                    rowIds, defaultMap);
        }
        return this.lookupTranslationsMap;
    }
    
    @Override
    public NotificationCountDTO getNotificationCount(Long paxId, String alertStatusListString,
            String alertTypeListString) {

        // Get our raw data - limiting to one result minimizes data since we only need totalResults
        List<Map<String, Object>> notificationDataList = notificationDao.getNotificationList(
                paxId, alertStatusListString, alertTypeListString, ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE, null
            );

        Integer totalResults = 0;

        // if we have results, we have a non-zero totalResults
        if (notificationDataList != null && !notificationDataList.isEmpty()) {
            totalResults = (Integer) notificationDataList.get(ProjectConstants.FIRST_RESULT_INDEX)
                                        .get(NotificationConstants.TOTAL_RESULTS_FIELD);
        }

        return new NotificationCountDTO().setAlertCount(totalResults);
    }

    @Override
    public List<RecognitionDetailsDTO> getRecognitionsByNotification(Long paxId, Long notificationId,
            Boolean excludePrivate, Long managerPaxId, String delimitedStatusList, Integer page, Integer size) {

        Alert alert = alertDao.findById(notificationId);

        if (alert == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.NOTIFICATION_ID,
                            AlertConstants.ERROR_NOTIFICATION_NOT_FOUND,
                            AlertConstants.ERROR_NOTIFICATION_NOT_FOUND_MSG)
                    .setStatus(HttpStatus.NOT_FOUND);
        } else if (!paxId.equals(alert.getPaxId())) {
            throw new ErrorMessageException().addErrorMessage(AlertConstants.NOTIFICATION_FIELD,
                            AlertConstants.ERROR_NOTIFICATION_BELONGS_TO_ANOTHER_PAX,
                            AlertConstants.ERROR_NOTIFICATION_BELONGS_TO_ANOTHER_PAX_MSG)
                    .setStatus(HttpStatus.FORBIDDEN);
        }

        Long nominationId = customAlertDao.getNominationByAlertId(notificationId);

        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<RecognitionDetailsDTO>();
        if (nominationId != null) {
            recognitionDetailsList = recognitionService.getRecognitionDetails(nominationId, excludePrivate,
                    managerPaxId, delimitedStatusList, true);
        }

        return PaginationUtil.getPaginatedList(recognitionDetailsList, page, size);
    }
    public Map<Long, String> getLookupTranslationsMap() {
		return lookupTranslationsMap;
	}

	public void setLookupTranslationsMap(Map<Long, String> lookupTranslationsMap) {
		this.lookupTranslationsMap = lookupTranslationsMap;
	}


    public Map<String, Long> getLookupCategoryCodeMap() {
		return lookupCategoryCodeMap;
	}

	public void setLookupCategoryCodeMap(Map<String, Long> lookupCategoryCodeMap) {
		this.lookupCategoryCodeMap = lookupCategoryCodeMap;
	}

	public Map<Long, String> getLookupDescMap() {
		return lookupDescMap;
	}

	public void setLookupDescMap(Map<Long, String> lookupDescMap) {
		this.lookupDescMap = lookupDescMap;
	}

	public Map<Long, String> getTranslations() {
		return translations;
	}

	public void setTranslations(Map<Long, String> translations) {
		this.translations = translations;
	}
	

}
