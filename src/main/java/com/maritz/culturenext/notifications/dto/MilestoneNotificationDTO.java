package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.program.dto.MilestoneMiscDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MilestoneNotificationDTO extends NotificationDTO {

    private MilestoneMiscDTO milestone;
    private Double pointsIssued;
    private String payoutType;
    private String payoutDisplayName;

    public MilestoneMiscDTO getMilestone() {
        return milestone;
    }

    public MilestoneNotificationDTO setMilestone(MilestoneMiscDTO milestone) {
        this.milestone = milestone;
        return this;
    }

    public Double getPointsIssued() {
        return pointsIssued;
    }

    public void setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
    }

    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }

    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
}
