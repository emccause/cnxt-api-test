package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PointLoadNotificationDTO extends NotificationDTO {
    
    private Double pointsIssued;
    private String payoutType;
    private String payoutDisplayName;

    public Double getPointsIssued() {
        return pointsIssued;
    }

    public PointLoadNotificationDTO setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
        return this;
    }
    
    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }

    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
}