package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentNotificationDTO extends NotificationDTO {

    private CommentDTO comment;
    private Integer commenterCount;
    private EmployeeDTO reporterPax;
    
    public CommentDTO getComment() {
        return comment;
    }
    
    public CommentNotificationDTO setComment(CommentDTO comment) {
        this.comment = comment;
        return this;
    }
    
    public Integer getCommenterCount() {
        return commenterCount;
    }
    
    public CommentNotificationDTO setCommenterCount(Integer commenterCount) {
        this.commenterCount = commenterCount;
        return this;
    }
    
    public EmployeeDTO getReporterPax() {
        return reporterPax;
    }

    public void setReporterPax(EmployeeDTO reporterPax) {
        this.reporterPax = reporterPax;
    }
}