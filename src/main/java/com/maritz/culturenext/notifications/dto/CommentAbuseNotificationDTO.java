package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentAbuseNotificationDTO extends NotificationDTO {
    
    private CommentDTO comment;

    public CommentDTO getComment() {
        return comment;
    }

    public CommentAbuseNotificationDTO setComment(CommentDTO comment) {
        this.comment = comment;
        return this;
    }
}