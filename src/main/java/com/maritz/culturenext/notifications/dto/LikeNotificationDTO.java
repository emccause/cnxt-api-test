package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LikeNotificationDTO extends NotificationDTO {
    
    private EmployeeDTO likePax;
    private Integer likeCount;
    
    public EmployeeDTO getLikePax() {
        return likePax;
    }
    
    public LikeNotificationDTO setLikePax(EmployeeDTO likePax) {
        this.likePax = likePax;
        return this;
    }
    
    public Integer getLikeCount() {
        return likeCount;
    }
    
    public LikeNotificationDTO setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
        return this;
    }
}