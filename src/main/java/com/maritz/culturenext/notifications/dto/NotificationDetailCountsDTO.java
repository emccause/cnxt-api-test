package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationDetailCountsDTO {
    
    private Integer totalRecipients;
    private Integer approvedRecipients;
    private Integer directReports;
    
    public Integer getTotalRecipients() {
        return totalRecipients;
    }
    
    public NotificationDetailCountsDTO setTotalRecipients(Integer totalRecipients) {
        this.totalRecipients = totalRecipients;
        return this;
    }
    
    public Integer getApprovedRecipients() {
        return approvedRecipients;
    }
    
    public NotificationDetailCountsDTO setApprovedRecipients(Integer approvedRecipients) {
        this.approvedRecipients = approvedRecipients;
        return this;
    }
    
    public Integer getDirectReports() {
        return directReports;
    }
    
    public NotificationDetailCountsDTO setDirectReports(Integer directReports) {
        this.directReports = directReports;
        return this;
    }
}