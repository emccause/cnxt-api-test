package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationDTO {

    private Long id;
    private String status;
    private String createDate;
    private String statusDate;
    private String activityCreateDate;
    private String type;
    private Long activityFeedItemId;
    private Long nominationId;
    private String programName;
    private EmployeeDTO fromPax;
    private EmployeeDTO toPax;
    private NotificationDetailCountsDTO counts;
    private NewsfeedItemDTO activity;

    public Long getId() {
        return id;
    }

    public NotificationDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public NotificationDTO setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCreateDate() {
        return createDate;
    }

    public NotificationDTO setCreateDate(String createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public NotificationDTO setStatusDate(String statusDate) {
        this.statusDate = statusDate;
        return this;
    }

    public String getActivityCreateDate() {
        return activityCreateDate;
    }

    public NotificationDTO setActivityCreateDate(String activityCreateDate) {
        this.activityCreateDate = activityCreateDate;
        return this;
    }

    public String getType() {
        return type;
    }
    public NotificationDTO setType(String type) {
        this.type = type;
        return this;
    }

    public Long getActivityFeedItemId() {
        return activityFeedItemId;
    }

    public NotificationDTO setActivityFeedItemId(Long activityFeedItemId) {
        this.activityFeedItemId = activityFeedItemId;
        return this;
    }

    public Long getNominationId() {
        return nominationId;
    }

    public NotificationDTO setNominationId(Long nominationId) {
        this.nominationId = nominationId;
        return this;
    }

    public String getProgramName() {
        return programName;
    }

    public NotificationDTO setProgramName(String programName) {
        this.programName = programName;
        return this;
    }

    public EmployeeDTO getFromPax() {
        return fromPax;
    }

    public NotificationDTO setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
        return this;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public NotificationDTO setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
        return this;
    }

    public NotificationDetailCountsDTO getCounts() {
        return counts;
    }

    public NotificationDTO setCounts(NotificationDetailCountsDTO counts) {
        this.counts = counts;
        return this;
    }

    public NewsfeedItemDTO getActivity() {
        return activity;
    }

    public NotificationDTO setActivity(NewsfeedItemDTO activity) {
        this.activity = activity;
        return this;
    }
}