package com.maritz.culturenext.notifications.dto;

public class NotificationCountDTO {
    
    private Integer alertCount;

    public Integer getAlertCount() {
        return alertCount;
    }

    public NotificationCountDTO setAlertCount(Integer alertCount) {
        this.alertCount = alertCount;
        return this;
    }

}
