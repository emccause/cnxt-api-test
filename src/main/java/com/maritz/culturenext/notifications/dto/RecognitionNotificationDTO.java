package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecognitionNotificationDTO extends NotificationDTO {
    
    private String notificationHeadline;
    private Double pointsIssued;
    private String payoutType;
    private String payoutDisplayName;

    public String getNotificationHeadline() {
        return notificationHeadline;
    }
    
    public RecognitionNotificationDTO setNotificationHeadline(String notificationHeadline) {
        this.notificationHeadline = notificationHeadline;
        return this;
    }
    
    public Double getPointsIssued() {
        return pointsIssued;
    }
    
    public RecognitionNotificationDTO setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
        return this;
    }
    
    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }

    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
}