package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RaiseNotificationDTO extends NotificationDTO {

    private Integer raiseRecipientCount;

    public Integer getRaiseRecipientCount() {
        return raiseRecipientCount;
    }

    public RaiseNotificationDTO setRaiseRecipientCount(Integer raiseRecipientCount) {
        this.raiseRecipientCount = raiseRecipientCount;
        return this;
    }    
}