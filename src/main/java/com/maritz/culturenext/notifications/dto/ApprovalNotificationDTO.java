package com.maritz.culturenext.notifications.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApprovalNotificationDTO extends NotificationDTO {

    private Double totalPoints;
    private String payoutType;
    private String payoutDisplayName;

    public Double getTotalPoints() {
        return totalPoints;
    }

    public ApprovalNotificationDTO setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
        return this;
    }
    
    public String getPayoutType() {
        return payoutType;
    }

    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }

    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }

    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
}