package com.maritz.culturenext.notifications.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.culturenext.enums.AlertSubType;

public class NotificationConstants {
    public static final String TOTAL_RESULTS_FIELD = "TOTAL_RESULTS";
    public static final String NOTIFICATION_ID_FIELD = "NOTIFICATION_ID";
    public static final String NOTIFICATION_TYPE_FIELD = "NOTIFICATION_TYPE";
    public static final String NOTIFICATION_STATUS_FIELD = "NOTIFICATION_STATUS";
    public static final String NOTIFICATION_CREATE_DATE_FIELD = "NOTIFICATION_CREATE_DATE";
    public static final String NOTIFICATION_STATUS_DATE_FIELD = "NOTIFICATION_STATUS_DATE";
    public static final String ACTIVITY_FEED_ITEM_ID_FIELD = "ACTIVITY_FEED_ITEM_ID";
    public static final String NOMINATION_ID_FIELD = "NOMINATION_ID";
    public static final String PROGRAM_NAME_FIELD = "PROGRAM_NAME";
    public static final String NOMINATION_CREATE_DATE_FIELD = "NOMINATION_CREATE_DATE";
    public static final String TOTAL_RECIPIENT_COUNT_FIELD = "TOTAL_RECIPIENT_COUNT";
    public static final String APPROVED_RECIPIENT_COUNT_FIELD = "APPROVED_RECIPIENT_COUNT";
    public static final String DIRECT_REPORT_COUNT_FIELD = "DIRECT_REPORT_COUNT";
    public static final String FROM_PAX_ID_FIELD = "FROM_PAX_ID";
    public static final String TO_PAX_ID_FIELD = "TO_PAX_ID";
    public static final String POINTS_ISSUED_FIELD = "POINTS_ISSUED";
    public static final String NOMINATION_HEADLINE_FIELD = "NOMINATION_HEADLINE";
    public static final String RAISE_RECIPIENT_COUNT_FIELD = "RAISE_RECIPIENT_COUNT";
    public static final String LIKE_COUNT_FIELD = "LIKE_COUNT";
    public static final String LIKE_PAX_ID_FIELD = "LIKE_PAX_ID";
    public static final String COMMENTER_COUNT_FIELD = "COMMENTER_COUNT";
    public static final String COMMENT_ID_FIELD = "COMMENT_ID";
    public static final String COMMENT_PAX_ID_FIELD = "COMMENT_PAX_ID";
    public static final String COMMENT_TEXT_FIELD = "COMMENT_TEXT";
    public static final String COMMENT_CREATE_DATE_FIELD = "COMMENT_CREATE_DATE";
    public static final String COMMENT_STATUS_FIELD = "COMMENT_STATUS";
    public static final String PAYOUT_TYPE_FIELD = "PAYOUT_TYPE";
    public static final String PAYOUT_DISPLAY_NAME_FIELD = "PAYOUT_DISPLAY_NAME";
    public static final String MILESTONE_DATE_FIELD = "MILESTONE_DATE";
    public static final String MILESTONE_YEARS_OF_SERVICE_FIELD = "MILESTONE_YEARS_OF_SERVICE";
    public static final String MILESTONE_TYPE = "MILESTONE_TYPE";
    public static final String PINNACLE_NOTIFICATION_RECIPIENT = "PINNACLE_NOTIFICATION_RECIPIENT";
    public static final String PINNACLE_NOTIFICATION_RECIPIENT_MGR = "PINNACLE_NOTIFICATION_MGR";
    public static final String PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR = "PINNACLE_NOTIFICATION_2ND_MGR";

    public static final List<String> APPROVAL_NOTIFICATION_TYPES = Arrays.asList(
            AlertSubType.RECOGNITION_PENDING_APPROVAL.name(),
            AlertSubType.RECOGNITION_APPROVAL.name(),
            AlertSubType.RECOGNITION_DENIED.name()
        );
}
