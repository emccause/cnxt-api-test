package com.maritz.culturenext.constants;

public class ErrorConstants {
    
    // Award Tier - Invalid award tier id
    public static final String KEY_NAME_ROUTE_NEXT_LEVEL_MANAGER_INVALID_AWARD_TIER_ID = "keyNameRouteNextLevelManagerInvalidAwardTierId";
    
}
