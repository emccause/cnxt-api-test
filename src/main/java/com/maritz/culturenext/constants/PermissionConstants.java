package com.maritz.culturenext.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PermissionConstants {

    public static final String ENABLE_PREFIX = "ENABLE_";
    public static final String DISABLE_PREFIX = "DISABLE_";
    public static final String COMMENTING_TYPE = "COMMENTING";
    public static final String LIKING_TYPE = "LIKING";
    public static final String ENGAGEMENT_SCORE_TYPE = "ENGAGE_SCORE";
    public static final String NETWORK_TYPE = "NETWORK";
    public static final String CALENDAR_TYPE = "CALENDAR";
    public static final String SERVICE_ANNIVERSARY_TYPE = "SERVICE_ANNIVERSARY";

    public static final List<String> VALID_AUDIENCE_PERMISSION_TYPES = Arrays.asList(
            COMMENTING_TYPE, LIKING_TYPE, ENGAGEMENT_SCORE_TYPE, NETWORK_TYPE, CALENDAR_TYPE);

    public static final Map<String, String> AUDIENCE_PERM_MAP = new HashMap<>();
    static
    {
        //Initialize the map
        AUDIENCE_PERM_MAP.put(COMMENTING_TYPE, ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED);
        AUDIENCE_PERM_MAP.put(LIKING_TYPE, ApplicationDataConstants.KEY_NAME_LIKING_ENABLED);
        AUDIENCE_PERM_MAP.put(ENGAGEMENT_SCORE_TYPE, ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED);
        AUDIENCE_PERM_MAP.put(NETWORK_TYPE, ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED);
        AUDIENCE_PERM_MAP.put(CALENDAR_TYPE, ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED);
    }

}