package com.maritz.culturenext.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;

public class ProjectConstants {

	private ProjectConstants(){}
    // Default values
    public static final String DEFAULT_LOCALE_CODE = "en_US";
    public static final String DEFAULT_ALERT_TYPE_CODE = "SCRN";
    public static final String INBOX = "inbox";
    public static final String UNION_ALL_FRAGMENT = "UNION ALL ";
    public static final String SQL_STRING_WRAPPER = "'%s'";
    public static final String NULL_VALUE = "NULL";
    public static final String SPACE = " ";
    public static final String COLON = ":";
    public static final String EMPTY_STRING = "";
    public static final String COMMA_DELIM = ",";
    public static final String DOT_DELIM = ".";
    public static final String SLASH = "/";
    public static final String UNDERSCORE = "_";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String YES_CHAR = "Y";
    public static final String NO_CHAR = "N";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String PNG = "png";
    public static final String JPEG = "jpeg";
    public static final String JPG = "jpg";
    public static final String SVG = "svg";
    public static final String CSV = "csv";
    public static final String MS_EXCEL = "vnd.ms-excel";
    public static final String OFFICE_DOCUMENT_SPREADSHEET = "vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String AUTO_APPROVED = "auto-approved";
    public static final String ZERO_STRING = "0";
    public static final String SEARCH_STRING_FORMAT = "%%%s%%";
    public static final String SEARCH_DATA_PAX_TYPE = "P";
    public static final String RIDEAU_CARD_TYPE = "RIDEAU";
    public static final String MINUS_ONE_STRING = "-1";
    public static final String FROM_FRAGMENT = "FROM";
    public static final String WHERE_FRAGMENT = "WHERE";

    // This error is used in a lot of places - all other error messages will be in their own specific constants classes
    public static final String ERROR_SQL_CODE = "SQL_ERROR";

    // APPROVED statuses
    public static List<String> APPROVED_STATUSES = Arrays.asList(
            StatusTypeCode.APPROVED.toString(),
            StatusTypeCode.AUTO_APPROVED.toString());
    public static List<String> REJECTED_STATUSES = Arrays.asList(
            StatusTypeCode.REJECTED.toString(),
            StatusTypeCode.REJECTED_INACTIVE.toString(),
            StatusTypeCode.REJECTED_RESTRICTED.toString());

    // Admin constants
    public static final String ADMIN_ROLE = "MTZ:ADM"; // This will be used for security.hasRole()
    public static final String ADMIN_ROLE_CODE = "ADM"; // This is the actual ROLE_CODE in the database
    public static final String CLIENT_ADMIN_ROLE = "CADM";
    public static final String PARTICIPANT_ROLE = "RECG";
    public static final String PULSEM_ROLE = "3rdP:PLSM";
    public static final String MANAGER_ROLE = "MGR";

    public static final String BUSINESS_CONTACT_TYPE = "BUSINESS";
    public static final String FAX_CONTACT_TYPE = "FAX";
    public static final String HOME_CONTACT_TYPE = "HOME";

    // Column Names (camelCase)

    // ACL
    public static final String ID = "id";
    public static final String TARGET_TABLE = "targetTable";
    public static final String TARGET_ID = "targetId";

    // ADDRESS
    public static final String CITY = "city";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String POSTAL_CODE = "postalCode";
    public static final String ADDRESS1 = "address1";
    public static final String ADDRESS2 = "address2";
    public static final String ADDRESS3 = "address3";
    public static final String ADDRESS4 = "address4";
    public static final String ADDRESS5 = "address5";
    public static final String ADDRESS6 = "address6";

    // ALERT
    public static final String ALERT_SUB_TYPE_CODE = "alertSubTypeCode";

    // APPLICATION_DATA
    public static final String APPLICATION_DATA_ID = "applicationDataId";
    public static final String KEY_NAME = "keyName";
    public static final String VALUE = "value";

    // APPROVAL_HISTORY
    public static final String APPROVAL_LEVEL = "approvalLevel";
    public static final String APPROVAL_DATE = "approvalDate";
    public static final String APPROVAL_NOTES = "approvalNotes";

    // APPROVAL_PENDING
    public static final String APPROVAL_PROCESS_ID = "approvalProcessId";
    public static final String TARGET_SUB_ID = "targetSubId";

    // APPROVAL_PROCESS
    public static final String LAST_APPROVAL_DATE = "lastApprovalDate";

    // BATCH
    public static final String BATCH_ID = "batchId";
    public static final String BATCH_TYPE_CODE = "batchTypeCode";

    // BATCH_EVENT
    public static final String BATCH_EVENT_TYPE_CODE = "batchEventTypeCode";
    public static final String BATCH_EVENT_TYPE_CODE_ERR = "ERR";
    public static final String BATCH_EVENT_MESSAGE_ACCESS_IS_DENIED = "Access is denied.";
    // BATCH_FILE
    public static final String FILE_UPLOAD = "fileUpload";

    //BUDGET
    public static final String BUDGET_ALLOCATOR = "budgetAllocator";
    public static final String BUDGET_TYPE_CODE = "budgetTypeCode";
    public static final String BUDGET_NAME = "budgetName";
    public static final String BUDGET_OWNER_CONTROL_NUM = "budgetOwnerControlNum";
    public static final String BUDGET_OWNER_FIRST_NAME = "budgetOwnerFirstName";
    public static final String BUDGET_OWNER_LAST_NAME = "budgetOwnerLastName";
    public static final String BUDGET_STATUS = "budgetStatus";
    public static final String BUDGET_DISPLAY_NAME = "budgetDisplayName";
    public static final String BUDGET_DESC = "budgetDesc";
    public static final String BUDGET_USER = "budgetUser";
    public static final String BUDGET_USER_TYPE = "budgetUserType";
    public static final String BUDGET_USER_ID = "budgetUserId";
    public static final String PARENT_BUDGET_ID = "parentBudgetId";
    public static final String INDIVIDUAL_GIVING_LIMIT = "individualGivingLimit";
    public static final String INDIVIDUAL_USED_AMOUNT = "individualUsedAmount";
    public static final String FROM_DATE = "fromDate";
    public static final String THRU_DATE = "thruDate";
    public static final String BUDGET_CONSTRAINT = "budgetConstraint";
    public static final String CREATE_DATE = "createDate";
    public static final String STATUS_TYPE_CODE = "statusTypeCode";
    public static final String TYPES = "types";
    public static final String DISTRIBUTED_BUDGET_GROUP = "distributedBudgetGroup";
    public static final String TOTAL_PARTICIPANTS = "totalParticipants";
    public static final String BUDGET_PER_PARTICIPANT= "budgetPerParticipant";
    public static final String BUDGET_ALLOCATED_IN = "allocatedIn";
    public static final String BUDGET_ALLOCATED_OUT = "allocatedOut";

    // BUGET_ALERT
    public static final String BUDGET_ID = "budgetId";

    // BUDGET_MISC
    public static final String MISC_TYPE_CODE = "miscTypeCode";

    // CALENDAR_ENTRY
    public static final String CALENDAR_ENTRY_NAME = "calendarEntryName";
    public static final String CALENDAR_ENTRY_DESC = "calendarEntryDesc";

    // CALENDAR_SUB_TYPE
    public static final String CALENDAR_TYPE_ID = "calendarTypeId";
    public static final String CALENDAR_SUB_TYPE_CODE = "calendarSubTypeCode";
    public static final String DISPLAY_STATUS_TYPE_CODE = "displayStatusTypeCode";

    // CALENDAR_TYPE
    public static final String CALENDAR_TYPE_CODE = "calendarTypeCode";

    // CARD_TYPE
    public static final String CARD_TYPE_ID = "cardTypeId";
    public static final String CARD_TYPE_CODE = "cardTypeCode";
    public static final String CARD_TYPE_DESC = "cardTypeDesc";
    public static final String DISPLAY_SEQUENCE = "displaySequence";

    // COMMENT
    public static final String COMMENT = "comment";

    // EMAIL
    public static final String EMAIL_TYPE_CODE = "emailTypeCode";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String PREFERRED = "preferred";

    // EMAIL_MESSAGE
    public static final String REFERENCE_FK = "referenceFk";

    // EVENT_TYPE
    public static final String EVENT_TYPE_CODE = "eventTypeCode";

    // FILES
    public static final String FILE_NAME = "fileName";
    public static final String DATA = "data";
    public static final String FILES_TABLE = "FILES";

    // FILE_TYPE
    public static final String FILE_TYPE_CODE = "fileTypeCode";

    // GROUP_SPECIFIC_CONFIG
    public static final List<String> GROUP_SPECIFIC_KEY_LIST = Arrays.asList(
            "PRIMARY_COLOR",
            "SECONDARY_COLOR",
            "TEXT_ON_PRIMARY_COLOR");

    // GROUPS
    public static final String GROUP_ID = "groupId";
    public static final String GROUP_NAME = "groupName";
    public static final String GROUP_DESC = "groupDesc";
    public static final String GROUP_CONFIG_ID = "groupConfigId";

    // LOCALE_INFO
    public static final String LOCALE_CODE = "localeCode";

    // LOOKUP
    public static final String LOOKUP_CATEGORY_CODE = "lookupCategoryCode";
    public static final String LOOKUP_CODE = "lookupCode";
    public static final String LOOKUP_DESC = "lookupDesc";
    public static final String LOOKUP_TYPE_CODE = "lookupTypeCode";

    // NEWSFEED_ITEM
    public static final String FROM_PAX_ID = "fromPaxId";
    public static final String FK1 = "fk1";
    public static final String NEWSFEED_ITEM_TYPE_CODE = "newsfeedItemTypeCode";
    public static final String MESSAGE = "message";
    public static final String NEWSFEED_VISIBILITY = "newsfeedVisibility";

    // NEWSFEED_ITEM_PAX
    public static final String NEWSFEED_ITEM_ID = "newsfeedItemId";

    // NOMINATION
    public static final String ECARD_ID = "ecardId";
    public static final String HEADLINE_COMMENT = "headlineComment";
    public static final String SUBMITTAL_DATE = "submittalDate";

    // PAX
    public static final String PAX_ID = "paxId";
    public static final String PAX_IDS = "paxIds";
    public static final String LANGUAGE_CODE = "languageCode";
    public static final String PREFERRED_LOCALE = "preferredLocale";
    public static final String CONTROL_NUM = "controlNum";
    public static final String FIRST_NAME = "firstName";
    public static final String MIDDLE_NAME = "middleName";
    public static final String LAST_NAME = "lastName";
    public static final String NAME_PREFIX = "namePrefix";
    public static final String NAME_SUFFIX = "nameSuffix";
    public static final String COMPANY_NAME = "companyName";
    public static final String LANGUAGE_LOCALE = "languageLocale";
    public static final String PAX_ID_GETTER = "getPaxId";
    public static final String MANAGER_CONTROL_NUMBER = "managerControlNumber";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String CONTROL_NUMBER_PARAM = "controlNumber";

    // PAX_ACCOUNT
    public static final String ISSUED = "issued";

    // PAX_FILES
    public static final String FILES_ID = "filesId";
    public static final String VERSION = "version";

    // PAX_GROUP
    public static final String PAX_GROUP_ID = "paxGroupId";

    // PAX_MISC
    public static final String VF_NAME = "vfName";
    public static final String MISC_DATA = "miscData";
    public static final String HIRE_DATE = "hireDate";

    // PAYOUT
    public static final String PARTICIPANT_ID = "participantId";
    public static final String PARTICIPANT_FIRST_NAME = "participantFirstName";
    public static final String PARTICIPANT_LAST_NAME = "participantLastName";
    public static final String PAYOUT_ID = "payoutId";
    public static final String PAYOUT_STATUS_DATE = "payoutStatusDate";
    public static final String PAYOUT_ERROR_DESC = "payoutErrorDesc";

    // PAYOUT_ITEM
    public static final String PAYOUT_ITEM_ID = "payoutItemId";
    public static final String PAYOUT_ITEM_NAME = "payoutItemName";
    public static final String PRODUCT_CODE = "productCode";
    public static final String SUBCLIENT = "subclient";

    // PAYOUT_VENDOR
    public static final String PAYOUT_VENDOR_ID = "payoutVendorId";
    public static final String PAYOUT_VENDOR_NAME = "payoutVendorName";

    // PHRASE_TRANSLATION
    public static final String TRANSLATABLE_PHRASE_ID = "translatablePhraseId";

    // PROGRAM
    public static final String PROGRAM_ID = "programId";
    public static final String PROGRAM = "program";
    public static final String PROGRAM_NAME = "programName";
    public static final String PROGRAM_MISC_ID = "programMiscId";
    public static final String PROGRAM_FILES_ID_GETTER = "getFilesId";
    public static final String PROGRAM_ID_SQL_PARAM = "PROGRAM_ID";
    public static final String REMINDER_DAYS = "reminderDays";


    // PROGRAM_ACTIVITY
    public static final String PROGRAM_ACTIVITY_ID = "programActivityId";

    // PROGRAM_ACTIVITY_TYPE
    public static final String PROGRAM_ACTIVITY_TYPE_CODE = "programActivityTypeCode";

    // PROGRAM_AWARD_TIERS
    public static final String MIN_AMOUNT = "minAmount";
    public static final String MAX_AMOUNT = "maxAmount";

    // PROGRAM_CRITERIA
    public static final String RECOGNITION_CRITERIA_ID = "recognitionCriteriaId";

    // PROJECT
    public static final String PROJECT_ID = "projectId";
    public static final String PROJECT_NUMBER = "projectNumber";

    // RECOGNITION
    public static final String NOMINATION_ID = "nominationId";
    public static final String PARENT_ID = "parentId";
    public static final String AMOUNT = "amount";

    // RECOGNITION_CRITERIA
    public static final String RECOGNITION_CRITERIA_NAME = "recognitionCriteriaName";

    // RELATIONSHIP
    public static final String RELATIONSHIP_TYPE_CODE = "relationshipTypeCode";
    public static final String PAX_ID_1 = "paxId1";
    public static final String PAX_ID_2 = "paxId2";

    // ROLE
    public static final String ROLE_ID = "roleId";
    public static final String ROLE_CODE = "roleCode";
    public static final String ROLE_NAME = "roleName";

    // SUBJECT
    public static final String SUBJECT_ID = "subjectId";
    public static final String SUBJECT_TABLE = "subjectTable";

    // SUB_PROJECT
    public static final String SUB_PROJECT_ID = "subProjectId";
    public static final String SUB_PROJECT_NUMBER = "subProjectNumber";

    // SYS_USER
    public static final String SYS_USER_ID = "sysUserId";
    public static final String SYS_USER_NAME = "sysUserName";

    // TRANSACTION_HEADER
    public static final String TRANSACTION_HEADER_ID = "transactionHeaderId";
    public static final String PERFORMANCE_METRIC = "performanceMetric";

    // TEST
    public static final String TEST = "test";

    // TRANSLATABLE
    public static final String TABLE_NAME = "tableName";
    public static final String COLUMN_NAME = "columnName";

    // TRANSLATABLE_PHRASE
    public static final String TRANSLATABLE_ID = "translatableId";
    public static final String TO_TRANSLATE = "toTranslate";

    // VW_BUDGET_INFO
    public static final String ALLOCATED_IN = "allocatedIn";
    public static final String ALLOCATED_OUT = "allocatedOut";
    public static final String BUDGET_OWNER = "budgetOwner";
    public static final String TOTAL_AMOUNT = "totalAmount";
    public static final String TOTAL_USED = "totalUsed";
    public static final String IS_USED = "isUsed";
    public static final String IS_USABLE = "isUsable";

    // VW_BUDGET_TREE
    public static final String ROOT = "root";

    //PC Cloud Columns
    public static final String POINT_BALANCE = "pointBalance";
    public static final String PARTICIPANT_NBR = "participantNbr";

    // EmployeeDTO field mapping names
    public static final String PROFILE_PICTURE_VERSION = "version";
    public static final String ELIGIBLE_RECEIVER_IN_PROGRAM = "eligibleReceiverInProgram";

    // CUSTOM QUERIES - Keep these alphabetized
    public static final String ABS = "abs";
    public static final String ACCOUNTING_ID = "accountingId";
    public static final String ACTION = "action";
    public static final String ACTIVITY_ID = "activityId";
    public static final String ACTIVITY_FEED_FILTERS = "activityFeedFilter";
    public static final String ACTIVITY_TYPE = "activityType";
    public static final String ALERT_ID = "alertId";
    public static final String ALERT_STATUS_DATE_UTC = "alertStatusDateUTC";
    public static final String ALERT_CREATE_DATE = "alertCreateDate";
    public static final String ALERT_CREATE_DATE_UTC = "alertCreateDateUTC";
    public static final String ALERT_STATUS_DATE = "alertStatusDate";
    public static final String APPROVAL_PAX = "approvalPax";
    public static final String APPROVED_RECIPIENT_COUNT = "approvedRecipientCount";
    public static final String APPROVER = "approver";
    public static final String APPROVER_CITY = "approverCity";
    public static final String APPROVER_FIRST_NAME = "approverFirstName";
    public static final String APPROVER_FIELDS = "approverFields";
    public static final String APPROVER_ID = "approverId";
    public static final String APPROVER_LAST_NAME = "approverLastName";
    public static final String APPROVER_PAX = "approverPax";
    public static final String APPROVER_PAX_ID = "approverPaxId";
    public static final String APPROVER_STATE = "approverState";
    public static final String AREA = "area";
    public static final String AUDIENCE = "audience";
    public static final String AWARDS = "awards";
    public static final String AWARD_AMOUNT = "awardAmount";
    public static final String AWARD_TIER = "awardTier";
    public static final String AWARD_TIERS = "awardTiers";
    public static final String AWARD_TIER_ASSIGNMENTS = "awardTierAssignments";
    public static final String AWARD_VALUE = "awardValue";
    public static final String BIRTH_DATE = "birthDate";
    public static final String BUDGET_ASSIGNMENTS = "budgetAssignments";
    public static final String BUDGET_AVAILABLE = "budgetAvailable";
    public static final String BUDGET_DETAILS = "budgetDetails";
    public static final String BUDGET_EXPIRED = "budgetExpired";
    public static final String BUDGET_TOTAL = "budgetTotal";
    public static final String BUDGET_USED = "budgetUsed";
    public static final String BUDGET_USED_IN_PERIOD = "budgetUsedInPeriod";
    public static final String BUDGET_USED_OUTSIDE_PERIOD = "budgetUsedOutsidePeriod";
    public static final String BUDGET_USERS = "budgetUsers";
    public static final String CALENDAR_ENABLED = "calendarEnabled";
    public static final String CAN_RAISE_ANY_TIER = "canRaiseAnyTier";
    public static final String CAN_RAISE_PROGRAM_BUDGET = "canRaiseProgramBudget";
    public static final String CAN_RAISE_TIER = "canRaiseTier";
    public static final String CATALOG_DEEP_LINK = "catalogDeepLink";
    public static final String CATALOG_URL = "catalogUrl";
    public static final String CLIENT_LOGO = "clientLogo";
    public static final String CLIENT_NAME = "clientName";
    public static final String CLIENT_URL = "clientUrl";
    public static final String COMMENT_COUNT = "commentCount";
    public static final String COMMENT_DATE = "commentDate";
    public static final String COMMENT_GIVEN_TEAM = "commentGivenTeam";
    public static final String COMMENT_ID = "commentId";
    public static final String COMMENT_PAX_ID = "commentPaxId";
    public static final String COMMENT_PAX_STATUS = "commentPaxStatus";
    public static final String COMMENT_RECEIVED_PAX = "commentReceivedPax";
    public static final String COMMENT_RECEIVED_TEAM = "commentReceivedTeam";
    public static final String COMMENT_TEXT = "commentText";
    public static final String COMMENT_STATUS = "commentStatus";
    public static final String COMMENTER_COUNT = "commenterCount";
    public static final String COMMENTING = "commenting";
    public static final String COMMENTING_ENABLED = "commentingEnabled";
    public static final String COST_CENTER = "costCenter";
    public static final String COUNT = "count";
    public static final String CUSTOM_1 = "custom1";
    public static final String CUSTOM_2 = "custom2";
    public static final String CUSTOM_3 = "custom3";
    public static final String CUSTOM_4 = "custom4";
    public static final String CUSTOM_5 = "custom5";
    public static final String DELIVERY_EMAIL = "deliveryEmail";
    public static final String DELIVERY_METHOD = "deliveryMethod";
    public static final String DEPARTMENT = "department";
    public static final String DESCRIPTION = "description";
    public static final String DETAILS = "details";
    public static final String DIRECT_REPORT_COUNT = "directReportCount";
    public static final String DIRECT_REPORT_FIRST = "directReportFirst";
    public static final String DIRECT_REPORT_LAST = "directReportLast";
    public static final String DIRECT_REPORT_PAX_ID = "directReportPaxId";
    public static final String DISPLAY_NAME = "displayName";
    public static final String DOWNLOAD = "download";
    public static final String ECARD_NAME = "ecardName";
    public static final String ECARD_URL = "ecardUrl";
    public static final String ELIGIBILITY = "eligibility";
    public static final String EMAIL = "email";
    public static final String EMAIL_TYPE = "emailType";
    public static final String EMPLOYEE_ID = "employeeId";
    public static final String END_DATE = "endDate";
    public static final String ENGAGEMENT_SCORE_ENABLED = "engagementScoreEnabled";
    public static final String ERROR_ALL_CAPS = "ERROR";
    public static final String EVENT_TYPE = "eventType";
    public static final String FIELD = "field";
    public static final String FIELD_DISPLAY_NAME = "fieldDisplayName";
    public static final String FIELD_MAPPING = "fieldMapping";
    public static final String FILTER = "filter";
    public static final String FROM_EMAIL_ADDRESS = "fromEmailAddress";
    public static final String FROM_PAX = "fromPax";
    public static final String FUNCTION = "function";
    public static final String FUTURE = "future";
    public static final String GIVEN_RAISE = "givenRaise";
    public static final String GIVING_MEMBERS = "givingMembers";
    public static final String GRADE = "grade";
    public static final String GROUP = "group";
    public static final String GROUP_COUNT = "groupCount";
    public static final String GROUP_PAX_ID = "groupPaxId";
    public static final String GROUP_TYPE = "groupType";
    public static final String HEADERS = "headers";
    public static final String HEADLINE = "headline";
    public static final String HISTORY_ID = "historyId";
    public static final String ICON = "icon";
    public static final String IDS = "ids";
    public static final String IMAGE = "image";
    public static final String IMAGE_ID = "imageId";
    public static final String INVALID_EMAIL_FORMAT = "INVALID_EMAIL_FORMAT";
    public static final String INVALID_EMAIL_FORMAT_MESSAGE = "Invalid email format";
    public static final String INVOLVED_PAX_ID = "involvedPaxId";
    public static final String ISSUANCE_DATE = "issuanceDate";
    public static final String JOB_TITLE = "jobTitle";
    public static final String KEY = "key";
    public static final String LEVEL = "level";
    public static final String LIKE_COUNT = "likeCount";
    public static final String LIKE_GIVEN_TEAM = "likeGivenTeam";
    public static final String LIKE_PAX_ID = "likePaxId";
    public static final String LIKE_RECEIVED_PAX = "likeReceivedPax";
    public static final String LIKE_RECEIVED_TEAM = "likeReceivedTeam";
    public static final String LIKING = "liking";
    public static final String LIKING_ENABLED = "likingEnabled";
    public static final String LOGGED_IN_PAX_ID = "loggedInPaxId";
    public static final String LOGGED_IN_USER_STATUS = "loggedInUserStatus";
    public static final String LOGIN_DATE = "loginDate";
    public static final String MANAGER_PAX_ID = "managerPaxId";
    public static final String MANAGER_TEXT = "managerText";
    public static final String MILESTONE_DATE = "milestoneDate";
    public static final String MILESTONE_REMINDER = "milestoneReminder";
    public static final String MILESTONE_REMINDERS = "milestoneReminders";
    public static final String MY_APPROVALS = "myApprovals";
    public static final String MY_BALANCE = "myBalance";
    public static final String MY_BUDGET = "myBudget";
    public static final String MY_COMMENT_COUNT = "myCommentCount";
    public static final String MY_RAISED_COUNT = "myRaisedCount";
    public static final String NAME = "name";
    public static final String NETWORK = "network";
    public static final String NETWORK_ANNIVERSARIES = "networkAnniversaries";
    public static final String NETWORK_BIRTHDAYS = "networkBirthdays";
    public static final String NETWORK_COUNT = "networkCount";
    public static final String NEXT_APPROVER_PAX_ID = "nextApproverPaxId";
    public static final String NOMINATION = "nomination";
    public static final String NOMINATIONS = "nominations";
    public static final String NOMINATION_IDS = "nominationIds";
    public static final String NOMINATION_SUBMITTAL_DATE = "nominationSubmittalDate";
    public static final String NON_NETWORK_ANNIVERSARIES = "nonNetworkAnniversaries";
    public static final String NON_NETWORK_BIRTHDAYS = "nonNetworkBirthdays";
    public static final String NOTIFICATION = "notification";
    public static final String NOTIFICATION_ID = "notificationId";
    public static final String NOTIFICATION_URL = "notificationUrl";
    public static final String OBSERVANCES = "observances";
    public static final String ORG_ID = "orgId";
    public static final String ORIGINAL_AMOUNT = "originalAmount";
    public static final String OTHER_EVENTS = "otherEvents";
    public static final String OVERRIDE_ACCOUNTING_ID = "overrideAccountingId";
    public static final String OVERRIDE_LOCATION = "overrideLocation";
    public static final String OVERRIDE_PROJECT_NUMBER = "overrideProjectNumber";
    public static final String OVERRIDE_SUB_PROJECT_NUMBER = "overrideSubProjectNumber";
    public static final String PARTNER_SSO_URL = "partnerSsoUrl";
    public static final String PASSWORD_RESET_TOKEN = "passwordResetToken";
    public static final String PAX = "pax";
    public static final String PAX_COUNT = "paxCount";
    public static final String PAX_FUNCTION = "paxFunction";
    public static final String PAX_ORG = "paxOrg";
    public static final String PAX_TEAM = "paxTeam";
    public static final String PAYOUT_TYPE = "payoutType";
    public static final String PERMISSIONS = "permissions";
    public static final String PIC_ID = "picId";
    public static final String POINT_AMOUNT = "pointAmount";
    public static final String POINT_BANK = "pointBank";
    public static final String POINT_LOAD = "pointLoad";
    public static final String POINTS = "points";
    public static final String POINTS_AWARDED = "pointsAwarded";
    public static final String POINTS_ISSUED = "pointsIssued";
    public static final String PREFERENCES = "preferences";
    public static final String PRIMARY_COLOR = "primaryColor";
    public static final String PRINTABLE = "printable";
    public static final String PRIMARY_COLOR_KEY = "PRIMARY_COLOR";
    public static final String PRIVATE_MESSAGE = "privateMessage";
    public static final String PROCESS_DATE = "processDate";
    public static final String PROCESSORS = "processors";
    public static final String PROGRAM_AWARD_TIER_ID = "programAwardTierId";
    public static final String PROGRAM_CATEGORY = "programCategory";
    public static final String PROGRAM_DESCRIPTION_SHORT = "programDescriptionShort";
    public static final String PROGRAM_DESCRIPTION_LONG = "programDescriptionLong";
    public static final String PROGRAM_IMAGE_ID = "programImageId";
    public static final String PROGRAM_IS_ACTIVE = "programIsActive";
    public static final String PROGRAM_STATUS = "programStatus";
    public static final String PROGRAM_TYPE = "programType";
    public static final String PROGRAM_TYPE_CODE = "programTypeCode";
    public static final String PROGRAM_VISIBILITY = "programVisibility";
    public static final String PROGRAM_BUDGET = "programBudget";
    public static final String PROXY = "proxy";
    public static final String PROXY_PAX = "proxyPax";
    public static final String PROXY_PAX_ID = "proxyPaxId";
    public static final String QUERY_ID = "queryId";
    public static final String RAISED_COUNT = "raisedCount";
    public static final String RAISED_PAX_ID = "raisedPaxId";
    public static final String RAISED_POINT_TOTAL = "raisedPointTotal";
    public static final String RAISES = "raises";
    public static final String REC_GIVEN_PAX = "recGivenPax";
    public static final String REC_GIVEN_TEAM = "recGivenTeam";
    public static final String REC_RECEIVED_PAX = "recReceivedPax";
    public static final String REC_RECEIVED_TEAM = "recReceivedTeam";
    public static final String RECEIVE_EMAIL_DIGEST = "receiveEmailDigest";
    public static final String RECEIVER_PAX_ID = "receiverPaxId";
    public static final String RECEIVERS = "receivers";
    public static final String RECIPIENT = "recipient";
    public static final String RECIPIENT_AREA = "recipientArea";
    public static final String RECIPIENT_CITY = "recipientCity";
    public static final String RECIPIENT_COMPANY_NAME = "recipientCompanyName";
    public static final String RECIPIENT_COST_CENTER = "recipientCostCenter";
    public static final String RECIPIENT_COUNT = "recipientCount";
    public static final String RECIPIENT_COUNTRY_CODE = "recipientCountryCode";
    public static final String RECIPIENT_CUSTOM1 = "recipientCustom1";
    public static final String RECIPIENT_CUSTOM2 = "recipientCustom2";
    public static final String RECIPIENT_CUSTOM3 = "recipientCustom3";
    public static final String RECIPIENT_CUSTOM4 = "recipientCustom4";
    public static final String RECIPIENT_CUSTOM5 = "recipientCustom5";
    public static final String RECIPIENT_DEPARTMENT = "recipientDepartment";
    public static final String RECIPIENT_EMAIL = "recipientEmail";
    public static final String RECIPIENT_FIELDS = "recipientFields";
    public static final String RECIPIENT_FIRST_NAME = "recipientFirstName";
    public static final String RECIPIENT_FUNCTION = "recipientFunction";
    public static final String RECIPIENT_GRADE = "recipientGrade";
    public static final String RECIPIENT_ID = "recipientId";
    public static final String RECIPIENT_LAST_NAME = "recipientLastName";
    public static final String RECIPIENT_LOCALE_CODE = "recipientLocaleCode";
    public static final String RECIPIENT_NAME_FIRST = "recipientNameFirst";
    public static final String RECIPIENT_NAME_LAST = "recipientNameLast";
    public static final String RECIPIENT_PAX_ID = "recipientPaxId";
    public static final String RECIPIENT_POSTAL_CODE = "recipientPostalCode";
    public static final String RECIPIENT_STATE = "recipientState";
    public static final String RECIPIENT_TITLE = "recipientTitle";
    public static final String RECOGNITION = "recognition";
    public static final String RECOGNITION_CRITERIA = "recognitionCriteria";
    public static final String RECOGNITION_CRITERIA_IDS = "recognitionCriteriaIds";
    public static final String RECOGNITION_CRITERIA_JSON = "recognitionCriteriaJson";
    public static final String RECOGNITION_ID = "recognitionId";
    public static final String RECOGNITION_RECEIVED_DATE = "recognitionReceivedDate";
    public static final String RECOGNITION_STATUS = "recognitionStatus";
    public static final String RECOGNITION_TYPE = "recognitionType";
    public static final String REFERENCE_ID = "referenceId";
    public static final String PPP_INDEX_APPLIED = "pppIndexApplied";
    public static final String REPORT_ABUSE_ADMIN = "reportAbuseAdmin";
    public static final String REPORT_CREATE_DATE = "reportCreateDate";
    public static final String REPORT_TYPE = "reportType";
    public static final String REQUEST = "request";
    public static final String REVERSAL_COMMENT = "reversalComment";
    public static final String ROLE = "role";
    public static final String SEARCH_TYPE = "searchType";
    public static final String SECONDARY_COLOR = "secondaryColor";
    public static final String SECONDARY_COLOR_KEY = "SECONDARY_COLOR";
    public static final String SERVICE_ANNIVERSARY_BASE_URL = "serviceAnniversaryBaseUrl";
    public static final String SERVICE_ANNIVERSARY_ENABLED = "serviceAnniversaryEnabled";
    public static final String SHORT_DESC = "shortDesc";
    public static final String LONG_DESC = "longDesc";
    public static final String SITE_LANGUAGES = "siteLanguages";
    public static final String SIZE = "size";
    public static final String SSO_ENABLED = "ssoEnabled";
    public static final String START_DATE = "startDate";
    public static final String STATUS = "status";
    public static final String SUB_CLIENT = "subClient";
    public static final String SUBMITTED_DATE = "submittedDate";
    public static final String SUBMITTER = "submitter";
    public static final String SUBMITTER_AREA = "submitterArea";
    public static final String SUBMITTER_CITY = "submitterCity";
    public static final String SUBMITTER_COMPANY_NAME = "submitterCompanyName";
    public static final String SUBMITTER_COST_CENTER = "submitterCostCenter";
    public static final String SUBMITTER_COUNTRY_CODE = "submitterCountryCode";
    public static final String SUBMITTER_CUSTOM1 = "submitterCustom1";
    public static final String SUBMITTER_CUSTOM2 = "submitterCustom2";
    public static final String SUBMITTER_CUSTOM3 = "submitterCustom3";
    public static final String SUBMITTER_CUSTOM4 = "submitterCustom4";
    public static final String SUBMITTER_CUSTOM5 = "submitterCustom5";
    public static final String SUBMITTER_DEPARTMENT = "submitterDepartment";
    public static final String SUBMITTER_EMAIL = "submitterEmail";
    public static final String SUBMITTER_FIELDS = "submitterFields";
    public static final String SUBMITTER_FIRST_NAME = "submitterFirstName";
    public static final String SUBMITTER_FUNCTION = "submitterFunction";
    public static final String SUBMITTER_GRADE = "submitterGrade";
    public static final String SUBMITTER_ID = "submitterId";
    public static final String SUBMITTER_LAST_NAME = "submitterLastName";
    public static final String SUBMITTER_NAME_FIRST = "submitterNameFirst";
    public static final String SUBMITTER_NAME_LAST = "submitterNameLast";
    public static final String SUBMITTER_PAX_ID = "submitterPaxId";
    public static final String SUBMITTER_POSTAL_CODE = "submitterPostalCode";
    public static final String SUBMITTER_STATE = "submitterState";
    public static final String SUBMITTER_TITLE = "submitterTitle";
    public static final String SUPPORT_EMAIL = "supportEmail";
    public static final String TEAM_ID = "teamId";
    public static final String TEMP_NOMINATION_ID = "tempNominationId";
    public static final String TEXT_ON_PRIMARY_COLOR = "textOnPrimaryColor";
    public static final String TEXT_ON_PRIMARY_COLOR_KEY = "TEXT_ON_PRIMARY_COLOR";
    public static final String TEXT_PRIMARY_COLOR = "textOnPrimaryColor";
    public static final String TIME = "time";
    public static final String TITLE = "title";
    public static final String TO_DATE = "toDate";
    public static final String TO_PAX = "toPax";
    public static final String TOTAL_AWARDS = "totalAwards";
    public static final String TOTAL_PAX_COUNT = "totalPaxCount";
    public static final String TOTAL_RECORDS = "totalRecords";
    public static final String TRANSACTION_ID = "transactionId";
    public static final String TYPE = "type";
    public static final String UNASSIGNED_APPROVER_PAX_ID = "unassignedApproverPaxId";
    public static final String UPDATE_DATE = "updateDate";
    public static final String UPLOAD = "upload";
    public static final String UPDATE_ECARDS_IMAGES = "update_ecards_images";
    public static final String USED_IN_PERIOD = "usedInPeriod";
    public static final String USED_OUT_OF_PERIOD = "usedOutOfPeriod";
    public static final String VISIBILITY = "visibility";

    // Award Code program fields
    public static final String AWARD_CODE = "awardCode";
    public static final String CLAIMING_INSTRUCTIONS = "claimingInstructions";
    public static final String ICON_ID = "iconId";
    public static final String IS_AWARD_CODE_PRINTABLE = "isPrintable";
    public static final String IS_AWARD_CODE_DOWNLOADABLE = "isDownloadable";
    public static final String CERTIFICATE_IDS = "certIds";
    public static final String GET_ID = "getId";

    // Milestone fields
    public static final String PROGRAM_ACTIVITY_NAME = "programActivityName";

    // sql server max param is 2100, so this keeps us safely below that
    public static final Integer SQL_PARAM_COUNT_MAX = 2000;

    public static final String ENABLE_ADVANCED_REC_PERMISSION = "ENABLE_ADVANCED_REC";

    /**
     * Deprecated in favor of MediaTypesEnum.
     */
    @Deprecated
    public static final List<String> SUPPORTED_UPLOAD_FILES_TYPES = Arrays.asList(
            "text/csv",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

    public static final String GROUPS_URI_BASE = "groups";
    public static final String PARTICIPANTS_URI_BASE = "participants";
    public static final String REPORTS_URI_BASE = "reports";
    public static final String HIERARCHY_URI_BASE = "hierarchy";
    public static final String EMAIL_HISTORY_URI_BASE = "email-history";

    // pagination
    public static final Integer DEFAULT_PAGE_NUMBER = 1;
    public static final Integer MAX_PAGE_SIZE = Integer.MAX_VALUE;
    public static final Integer DEFAULT_PAGE_SIZE = 50;
    public static final String TOTAL_RESULTS_KEY = "totalResults";
    public static final Integer ITEM_BY_ID_PAGE_SIZE = 1;
    public static final Integer FIRST_RESULT_INDEX = 0;

    // Request Mapping Headers - Keep these alphabetized
    public static final String VERSION_REQUEST_HEADER = "version";

    // REGEX
    public static final String REGEX_DELIM_COMMA_DIGITS = "^[0-9]+(,[0-9]+)*$";
    public static final String REGEX_DELIM_COMMA_LETTERS = "^[a-zA-Z_]+(,[a-zA-Z_]+)*$";

    // Errors
    public static final ErrorMessage INTERNAL_ERROR = new ErrorMessage()
            .setCode("APPLICATION_ERROR")
            .setField("application")
            .setMessage("An application error has occured.");
    
    public static final ErrorMessage INVALID_EMAIL_FORMAT_ERROR = new ErrorMessage()
            .setCode(INVALID_EMAIL_FORMAT)
            .setField(EMAIL)
            .setMessage(INVALID_EMAIL_FORMAT_MESSAGE);

    public static final String IMAGE_FILE = "ImageFile";
    
}
