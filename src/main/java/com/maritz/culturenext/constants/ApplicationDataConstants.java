package com.maritz.culturenext.constants;

public class ApplicationDataConstants {

    //All KEY_NAME values are case sensitive
    //Project Profile
    public static final String KEY_NAME_ACTIVITY_FEED_FILTER = "projectProfile.activityFeedFilter";
    public static final String KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT = "projectProfile.activityFeedFilter.defaultFilter";
    public static final String KEY_NAME_SUPPORT_EMAIL = "projectProfile.supportEmail";
    public static final String KEY_NAME_PRIMARY_COLOR = "projectProfile.colors.primaryColor";
    public static final String KEY_NAME_SECONDARY_COLOR = "projectProfile.colors.secondaryColor";
    public static final String KEY_NAME_TEXT_ON_COLOR = "projectProfile.colors.textOnPrimaryColor";
    public static final String KEY_NAME_CLIENT_DISPLAY_NAME = "projectProfile.displayName";
    public static final String KEY_NAME_SSO_URL = "projectProfile.partnerSsoUrl";
    public static final String KEY_NAME_CLIENT_URL = "projectProfile.clientUrl";
    public static final String KEY_NAME_SSO_ENABLED = "projectProfile.ssoEnabled";
    public static final String KEY_NAME_REPORT_ABUSE_ADMIN = "projectProfile.reportAbuseAdmin";
    public static final String KEY_NAME_LIKING_ENABLED = "projectProfile.likingEnabled";
    public static final String KEY_NAME_LOGIN_HELP_MESSAGE_NO_TPL = "projectProfile.login.help.message.noTpl";
    public static final String KEY_NAME_REPORTING_ENABLED = "projectProfile.reportingEnabled";
    public static final String KEY_NAME_COMMENTING_ENABLED = "projectProfile.commentingEnabled";
    public static final String KEY_NAME_ENGAGEMENT_ENABLED = "projectProfile.engagementScoreEnabled";
    public static final String KEY_NAME_POINT_BANK = "projectProfile.pointBank";
    public static final String KEY_NAME_NETWORKING_ENABLED = "projectProfile.networkConnectionsEnabled";
    public static final String KEY_NAME_UNASSIGNED_APPROVER = "projectProfile.unassignedApproverPaxId";
    public static final String KEY_NAME_RAISE_APPROVAL_ENABLED = "projectProfile.raiseApprovalEnabled";
    public static final String KEY_NAME_CALENDAR_ENABLED = "projectProfile.calendarEnabled";
    public static final String KEY_NAME_SEARCH_ENABLED = "projectProfile.activityFeedFilter.searchEnabled";
    public static final String KEY_NAME_PRIVACY_BASE_URL = "projectProfile.client.privacy.baseUrl";
    public static final String PROJECT_PROFILE_VIEW = "projectProfile.view";
    public static final String KEY_NAME_NEW_RELIC_APP_ID = "projectProfile.newRelicAppId";
    public static final String KEY_NAME_CDN_URL = "projectProfile.cdnUrl";

    //Project Profile - Email Preferences
    public static final String KEY_NAME_EMAIL_WHEN_RECOGNIZED = "projectProfile.emailPreferences.whenRecognized";
    public static final String KEY_NAME_EMAIL_DIRECT_RPT_RECOGNIZED = "projectProfile.emailPreferences.directRptRecognized";
    public static final String KEY_NAME_EMAIL_APPROVAL_NEEDED = "projectProfile.emailPreferences.approvalNeeded";
    public static final String KEY_NAME_EMAIL_WEEKLY_DIGEST = "projectProfile.emailPreferences.weeklyDigest";
    public static final String KEY_NAME_NOTIFY_OTHERS = "projectProfile.emailPreferences.notifyOthers";
    public static final String KEY_MILESTONE_REMINDER = "projectProfile.emailPreferences.milestoneReminder";

    //Project Profile - Terms and Conditions
    public static final String KEY_NAME_TC_EDIT_DATE = "program.termsAndConditions.editDate";
    public static final String KEY_NAME_TC_ENFORCE_TC_AT_LOGIN = "program.termsAndConditions.enable";
    public static final String KEY_NAME_TC_MAJOR_CHANGE_DATE = "program.termsAndConditions.acceptAfterDate";
    public static final String KEY_NAME_TC_SHOW_IN_FOOTER = "program.termsAndConditions.showInFooter";

    //Project Profile - Service Anniversary
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_ENABLED = "projectProfile.serviceAnniversaryEnabled";
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL = "projectProfile.serviceAnniversary.baseUrl";
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_REDIRECT_LINK = "projectProfile.serviceAnniversary.redirectLink";
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_PROVIDER = "projectProfile.serviceAnniversary.provider";
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_PRODUCT_CODE = "projectProfile.serviceAnniversary.productCode";
    public static final String KEY_NAME_SERVICE_ANNIVERSARY_SUBCLIENT = "projectProfile.serviceAnniversary.subclient";

    //Project Profile - SEO
    public static final String KEY_NAME_SEO_CLIENT_TITLE = "projectProfile.seo.clientTitle";
    public static final String KEY_NAME_SEO_CLIENT_DESC = "projectProfile.seo.clientDesc";

    //Project Messages
    public static final String KEY_NAME_ADMIN_HELP_TEXT = "projectMessages.adminOverviewHelpText";
    public static final String KEY_NAME_EMPLOYEE_ID = "projectMessages.employeeId";
    public static final String KEY_NAME_LOGIN_WELCOME_MESSAGE = "projectMessages.loginWelcomeMessage";
    public static final String KEY_NAME_USERNAME_PLACEHOLDER = "projectMessages.loginUsernamePlaceholder";
    public static final String KEY_NAME_PASSWORD_PLACEHOLDER = "projectMessages.loginPasswordPlaceholder";
    public static final String KEY_NAME_SSO_WELCOME_MESSAGE = "projectMessages.loginSSOWelcomeMessage";
    public static final String KEY_NAME_SSO_LOGOUT_MESSAGE = "projectMessages.logoutSSOMessage";
    public static final String KEY_NAME_SSO_ERROR_MESSAGE = "projectMessages.SSOAccessErrorMessage";
    public static final String KEY_NAME_SSO_LINK_TEXT = "projectMessages.partnerSSOLinkText";

    //ABS Setup
    public static final String KEY_NAME_LAST_PROCESSED_DATE = "demographicUpdates.processedDate";
    public static final String KEY_NAME_MARS_PRODUCTS = "mars.product";
    public static final String KEY_NAME_MARS_SUBCLIENT = "mars.subclient";
    public static final String KEY_NAME_SUBPROJECT_OVERRIDE = "vendorprocessing.subproject.override";
    public static final String KEY_NAME_RIDEAU_TOKEN_ISSUER = "token.rideau.issuer";
    public static final String KEY_NAME_ABS_VENDOR_PRODUCTS = "abs.payoutVendor.payoutTypes";

    //Email Content
    public static final String KEY_NAME_RECGPX_HEADER = "notification.email.subject.RECGPX";
    public static final String KEY_NAME_RECGPX_BUTTON = "emailProcessor.viewRecognitionButton";
    public static final String KEY_NAME_RECGMN_HEADER = "notification.email.subject.RECGMN";
    public static final String KEY_NAME_RECGMN_BUTTON = "emailProcessor.viewActivityButton";
    public static final String KEY_NAME_APR_HEADER = "notification.email.subject.APR";
    public static final String KEY_NAME_APR_REMINDER_HEADER = "notification.email.subject.APR_REMINDER";
    public static final String KEY_NAME_APR_BUTTON = "emailProcessor.takeActionButton";
    public static final String KEY_NAME_RSPWDN_HEADER = "notification.email.subject.RSPWDN";
    public static final String KEY_NAME_CREATE_PASSWORD_HEADER = "notification.email.subject.CREATE_PASSWORD";
    public static final String KEY_NAME_WEEKLY_DIGEST_HEADER = "notification.email.subject.WEEKLY_DIGEST";
    public static final String KEY_NAME_DYNAMIC_STRING_AND = "emailProcessor.dynamicString.and";
    public static final String KEY_NAME_DYNAMIC_STRING_OTHER = "emailProcessor.dynamicString.other";
    public static final String KEY_NAME_DYNAMIC_STRING_OTHERS = "emailProcessor.dynamicString.others";
    public static final String KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS = "emailProcessor.dynamicString.directReports";
    public static final String KEY_NAME_PASSWORD_CONFIRMATION_HEADER = "notification.email.subject.CHANGE_PASSWORD";
    public static final String KEY_NAME_POINT_LOAD_HEADER = "notification.email.subject.PNTDPT";
    public static final String KEY_NAME_NOTIFY_OTHER_HEADER = "notification.email.subject.NOTIFY_OTHERS";
    public static final String KEY_NAME_MILESTONE_REMINDER_SERVICE_ANNIVERSARY_HEADER = "notification.email.subject.MILESTONE_REMINDER_SERVICE_ANNIVERSARY";
    public static final String KEY_NAME_MILESTONE_REMINDER_BIRTHDAY_HEADER = "notification.email.subject.MILESTONE_REMINDER_BIRTHDAY";

    //Email - Translatable Content
    public static final String KEY_NAME_JANUARY = "emailProcessor.dynamicString.jan";
    public static final String KEY_NAME_FEBRUARY = "emailProcessor.dynamicString.feb";
    public static final String KEY_NAME_MARCH = "emailProcessor.dynamicString.mar";
    public static final String KEY_NAME_APRIL = "emailProcessor.dynamicString.apr";
    public static final String KEY_NAME_MAY = "emailProcessor.dynamicString.may";
    public static final String KEY_NAME_JUNE = "emailProcessor.dynamicString.jun";
    public static final String KEY_NAME_JULY = "emailProcessor.dynamicString.jul";
    public static final String KEY_NAME_AUGUST = "emailProcessor.dynamicString.aug";
    public static final String KEY_NAME_SEPTEMBER = "emailProcessor.dynamicString.sep";
    public static final String KEY_NAME_OCTOBER = "emailProcessor.dynamicString.oct";
    public static final String KEY_NAME_NOVEMBER = "emailProcessor.dynamicString.nov";
    public static final String KEY_NAME_DECEMBER = "emailProcessor.dynamicString.dec";

    // SENDING EMAIL ADDRESS
    public static final String KEY_NAME_NOTIFICATION_FROM_EMAIL_ADDRESS = "notification.email.fromEmailAddress";

    // SERVICE ACCOUNT
    public static final String KEY_NAME_SERVICE_ACCOUNT_TYPE = "serviceAccount.type";
    public static final String KEY_NAME_SERVICE_ACCOUNT_AUTH_URI = "serviceAccount.authUri";
    public static final String KEY_NAME_SERVICE_ACCOUNT_TOKEN_URI = "serviceAccount.tokenUri";
    public static final String KEY_NAME_SERVICE_ACCOUNT_AUTH_PROVIDER_X509_CERT_URL = "serviceAccount.authproviderX509CertUrl";
    public static final String KEY_NAME_SERVICE_ACCOUNT_PRODUCT_ID = "serviceAccount.productId";  //.UAT or .PROD
    public static final String KEY_NAME_SERVICE_ACCOUNT_PRIVATE_KEY_ID = "serviceAccount.privateKeyId";  //.UAT or .PROD
    public static final String KEY_NAME_SERVICE_ACCOUNT_PRIVATE_KEY = "serviceAccount.privateKey";  //.UAT or .PROD
    public static final String KEY_NAME_SERVICE_ACCOUNT_CLIENT_EMAIL = "serviceAccount.clientEmail";  //.UAT or .PROD
    public static final String KEY_NAME_SERVICE_ACCOUNT_CLIENT_ID = "serviceAccount.clientId";  //.UAT or .PROD
    public static final String SERVICE_ACCOUNT_CLIENT_X509_CERT_URL = "serviceAccount.clientX509CertUrl";  //.UAT or .PROD

}
