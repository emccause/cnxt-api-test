package com.maritz.culturenext.constants;

import java.util.HashMap;
import java.util.Map;

public enum ProjectConstantsEnum {
	
	//Enumerator contains multiple values values: label1, label2, name
	EMAIL("Email","emailAddress","EMAIL"),
	CITY("City","city","CITY"),
	STATE("State","state","STATE"),
	POSTAL_CODE("PostalCode","zip","POSTAL_CODE"),
	COUNTRY_CODE("CountryCode","countryCode","COUNTRY_CODE"),
	TITLE("Title","title","TITLE"),
	COMPANY_NAME("CompanyName","companyName","COMPANY_NAME"),
	DEPARTMENT("Department","department","DEPARTMENT"),
	COST_CENTER("CostCenter","costCenter","COST_CENTER"),
	FUNCTION("Function","paxFunction","FUNCTION"),
	GRADE("Grade","grade","GRADE"),
	AREA("Area","area","AREA"),
	CUSTOM_1("Custom1","custom1","CUSTOM_1"),
	CUSTOM_2("Custom2","custom2","CUSTOM_2"),
	CUSTOM_3("Custom3","custom3","CUSTOM_3"),
	CUSTOM_4("Custom4","custom4","CUSTOM_4"),
	CUSTOM_5("Custom5","custom5","CUSTOM_5");
	 
	    public final String label1,label2,name;
		    
	    	 
	    ProjectConstantsEnum(String label1, String label2, String name) {
	    	this.label1 = label1;
	    	this.label2 = label2;
	    	this.name = name;	    	
		}	
	    
	    private static final Map<String, ProjectConstantsEnum> BY_LABEL1 = new HashMap<>();
	    private static final Map<String, ProjectConstantsEnum> BY_LABEL2 = new HashMap<>();	    
	    private static final Map<String, ProjectConstantsEnum> BY_NAME = new HashMap<>();
	     
	    static {
	        for (ProjectConstantsEnum e : values()) {
	            BY_LABEL1.put(e.label1, e);
	            BY_LABEL2.put(e.label2, e);	            
	            BY_NAME.put(e.name, e);
	        }
	    }
	    
	    public static ProjectConstantsEnum valueOfLabel1(String label1) {
	        return BY_LABEL1.get(label1);
	    }
	 
	    public static ProjectConstantsEnum valueOfLabel2(String label2) {
	        return BY_LABEL2.get(label2);
	    }	 
	    public static ProjectConstantsEnum getEnumByName(String name) {
	        return BY_NAME.get(name);
	    }
	 
	    
}

