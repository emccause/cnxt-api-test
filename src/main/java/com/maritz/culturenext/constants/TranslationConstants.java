package com.maritz.culturenext.constants;

import java.util.Arrays;
import java.util.List;

import com.maritz.culturenext.enums.TableName;

public class TranslationConstants {

    public static final String COLUMN_NAME_CALENDAR_ENTRY_DESC = "CALENDAR_ENTRY_DESC";
    public static final String COLUMN_NAME_CALENDAR_ENTRY_NAME = "CALENDAR_ENTRY_NAME";
    public static final String COLUMN_NAME_LOOKUP_DESC = "LOOKUP_DESC";
    public static final String COLUMN_NAME_PROGRAM_DESC = "PROGRAM_DESC";
    public static final String COLUMN_NAME_PROGRAM_LONG_DESC = "PROGRAM_LONG_DESC";
    public static final String COLUMN_NAME_PROGRAM_MISC_DATA = "PROGRAM_MISC_DATA";
    public static final String COLUMN_NAME_PROGRAM_NAME = "PROGRAM_NAME";
    public static final String COLUMN_NAME_RECOGNITION_CRITERIA_DESC = "RECOGNITION_CRITERIA_DESC";
    public static final String COLUMN_NAME_RECOGNITION_CRITERIA_NAME = "RECOGNITION_CRITERIA_NAME";
    public static final String COLUMN_NAME_VALUE = "VALUE";    
    
    public static final String TABLE_NAME_PROGRAM = "PROGRAM";
    public static final String EN_US = "en_US";
    
   
    // Static list groupings
    public static final List<String> RECOGNITION_CRITERIA_COLUMNS = 
            Arrays.asList(COLUMN_NAME_RECOGNITION_CRITERIA_NAME, COLUMN_NAME_RECOGNITION_CRITERIA_DESC);
    public static final List<String> PROGRAM_COLUMNS = 
            Arrays.asList(COLUMN_NAME_PROGRAM_NAME, COLUMN_NAME_PROGRAM_DESC, COLUMN_NAME_PROGRAM_LONG_DESC);
    public static final List<String> PROGRAM_MISC_COLUMNS = 
            Arrays.asList(COLUMN_NAME_PROGRAM_MISC_DATA);

    // Table list grouping
    public static final List<String> TABLES = Arrays.asList(
            TableName.APPLICATION_DATA.name(),
            TableName.CALENDAR_ENTRY.name(),
            TableName.FILES.name(),
            TableName.LOOKUP.name(),
            TableName.PROGRAM.name(),
            TableName.PROGRAM_MISC.name(),
            TableName.RECOGNITION_CRITERIA.name()        
        );

    
}
