package com.maritz.culturenext.nomination.dao;

import java.util.List;
import java.util.Map;

public interface NominationDao {

    /**
     * Return TransactionDTO with details by nomination id 
     * 
     * if programType is MANAGER_DISCRETIONARY, PEER_TO_PEER, ECARD_ONLY or AWARD_CODE
     * 
     * @param nominationId - nomination id 
     * 
     * @return Return a TransactionDTO
     *
     * https://maritz.atlassian.net/wiki/display/M365/Get+Recognitions+for+Nomination     
     */
    List<Map<String, Object>> getNominationById(Long nominationId);

    List<Map<String, Object>> getNominationWithoutParentByProgramId(Long programId);
}
