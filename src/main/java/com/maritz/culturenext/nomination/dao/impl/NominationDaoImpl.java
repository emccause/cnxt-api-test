package com.maritz.culturenext.nomination.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.nomination.dao.NominationDao;

@Repository
public class NominationDaoImpl extends AbstractDaoImpl implements NominationDao {

    private static final String NOMINATION_ID_PLACEHOLDER = "NOMINATION_ID";
    private static final String PROGRAM_ID_PLACEHOLDER = "PROGRAM_ID";

    private static final String QUERY_GET_NOMINATION_BY_ID = 
    "SELECT DISTINCT (N.ID) AS 'NOMINATION_ID' " +
        ",N.CREATE_DATE " +
        ",NM.MISC_DATA AS 'PAYOUT_TYPE' " +
        ",P.PROGRAM_NAME " +
        ",N.STATUS_TYPE_CODE AS 'STATUS' " +
        ",N.HEADLINE_COMMENT AS 'HEADLINE' " +
        ",N.COMMENT " +
        ",N.SUBMITTER_PAX_ID AS 'FROM_PAX' " +
        "FROM NOMINATION N " +
        "LEFT JOIN NOMINATION_MISC NM ON N.ID=NM.NOMINATION_ID AND NM.VF_NAME='PAYOUT_TYPE' " +
        "LEFT JOIN PROGRAM P ON N.PROGRAM_ID=P.PROGRAM_ID " +
        "WHERE N.ID = :" + NOMINATION_ID_PLACEHOLDER;

    private static final String QUERY_GET_NOMINATION_WITHOUT_PARENT_BY_PROGRAM_ID = "SELECT top 1 ID,NOMINATION_TYPE_CODE,PROGRAM_ID,SUBMITTER_PAX_ID,BUDGET_ID,"+
            "ECARD_ID,PROXY_PAX_ID,PARENT_ID,NOMINATION_NAME,SUBMITTAL_DATE,WORKFLOW_STATE," +
            "PENDING_LEVEL,BATCH_ID,COMMENT_JUSTIFICATION,COMMENT,NEWSFEED_VISIBILITY," +
            "HEADLINE_COMMENT,STATUS_TYPE_CODE,CREATE_DATE,CREATE_ID,UPDATE_DATE," +
            "UPDATE_ID FROM component.NOMINATION WHERE PROGRAM_ID = :"+ PROGRAM_ID_PLACEHOLDER+" AND PARENT_ID is null";

@Override
    public List<Map<String, Object>> getNominationById(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);
            
        return namedParameterJdbcTemplate.query(QUERY_GET_NOMINATION_BY_ID, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getNominationWithoutParentByProgramId(Long programId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PROGRAM_ID_PLACEHOLDER, programId);
        return namedParameterJdbcTemplate.query(QUERY_GET_NOMINATION_WITHOUT_PARENT_BY_PROGRAM_ID, params, new CamelCaseMapRowMapper());
    }

}
