package com.maritz.culturenext.social.comment.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.core.jpa.support.util.StatusTypeCode;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Comment;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.comment.constants.CommentConstants;
import com.maritz.culturenext.social.comment.dao.CommentDao;
import com.maritz.culturenext.social.comment.dto.CommentDTO;
import com.maritz.culturenext.social.comment.services.CommentService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class CommentServiceImpl implements CommentService {

    @Inject private AlertService alertService;
    @Inject private ConcentrixDao <Alert> alertDao;
    @Inject private ConcentrixDao<Comment> commentDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private CommentDao actualCommentDao;
    @Inject private Environment environment;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private Security security;

    //Notification comment recognition types
    private static final String COMMENT_RECOGNITION_RECEIVER = "COMMENT_RECOGNITION_RECEIVER";
    private static final String COMMENT_RECOGNITION_SUBMITTER = "COMMENT_RECOGNITION_SUBMITTER";

    @Override
    public List<CommentDTO> getCommentsForActivityCurrent(Integer page, Integer size, Long activityID, Long paxId) {
        // Will use this in case of errors. Still unused at this point in time.
        List<CommentDTO> commentItems = new ArrayList<CommentDTO>();

        List<Comment> commentResults = commentDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(activityID)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .desc(ProjectConstants.COMMENT_DATE)
                .find();


        if (commentResults != null && !commentResults.isEmpty()) {
            for (Comment node : commentResults) {
                CommentDTO commentDTO = new CommentDTO();
                commentDTO = populateDTO(commentDTO, node);
                commentItems.add(commentDTO);
            }
        }

        return PaginationUtil.getPaginatedList(commentItems, page, size);
    }
    
    @Override
    public PaginatedResponseObject<CommentDTO> getCommentsForActivity(Integer pageNumber, Integer pageSize, Long targetId, Long paxId) {

        // handle page parameters
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        if (pageSize == null || pageSize < 1) {
            pageSize = ProjectConstants.DEFAULT_PAGE_SIZE;
        }
        
        // Handle other parameters
        Map<String, Object> requestParameters = new HashMap<>();
        
        // get data
        List<Map<String, Object>> resultList = actualCommentDao.getCommentsForActivity(pageNumber, pageSize, targetId, paxId);
        
        // process data
        List<CommentDTO> resultData = new ArrayList<>();
        Integer totalResults = 0;
        
        for (Map<String, Object> result : resultList) {
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            CommentDTO rowData = new CommentDTO();
            
            rowData.setPax(new EmployeeDTO(result, ProjectConstants.EMPTY_STRING));
            rowData.setStatus((String) result.get(ProjectConstants.COMMENT_PAX_STATUS));
            rowData.setId((Long) result.get(ProjectConstants.COMMENT_ID));
            rowData.setText((String) result.get(ProjectConstants.COMMENT_TEXT));
            rowData.setTime(DateUtil.convertToUTCDateTime((Date) result.get(ProjectConstants.COMMENT_DATE)));
            resultData.add(rowData);
        }
    
        String commentRestEndpoint = CommentConstants.COMMENT_REST_ENDPOINT
                .replace(CommentConstants.REPLACE_PAX_ID, paxId.toString())
                .replace(CommentConstants.REPLACE_TARGET_ID, targetId.toString());
        
        // Return paginated data
        return new PaginatedResponseObject<CommentDTO>(resultData, requestParameters, 
                commentRestEndpoint, totalResults, pageNumber, pageSize);
    }

    @Override
    public CommentDTO createCommentOnActivity(Long paxId, Long id, CommentDTO commentDTO) {
        return createComment(paxId, id, CommentConstants.NEWSFEED_ITEM_TABLE, commentDTO);
    }

    @Override
    public CommentDTO updateComment(Long paxId, Long commentId, CommentDTO commentDTO) {

        Date date = new Date();
        CommentDTO commentResponse = new CommentDTO();
        String text = commentDTO.getText();
        Long reportAbuseAdminPaxId = getReportAbuseAdminPaxId();
        Comment commentEntity = findCommentById(commentId);
        String oldStatus = commentEntity.getStatusTypeCode();
        
        validateCommentDTO(commentDTO, commentEntity, paxId, reportAbuseAdminPaxId, commentId);

        // Update comment either as admin passing judgment or as user
        if (CommentConstants.REPORTED_STATUS_CODE.equals(commentEntity.getStatusTypeCode()) && 
                security.getPaxId().equals(reportAbuseAdminPaxId)) {
            commentEntity.setStatusTypeCode(commentDTO.getStatus());
            commentEntity.setAdminPaxId(reportAbuseAdminPaxId);
            commentEntity.setAdminUpdateTime(new Date());
        } else {
            commentEntity.setId(commentDTO.getId());
            commentEntity.setComment(text);
            commentEntity.setCommentDate(date);
            commentEntity.setStatusTypeCode(commentDTO.getStatus());
            commentEntity.setCommentDate(date);
        }

        commentDao.update(commentEntity);
        
        //Only want to update notification status if the comment was previously in REPORTED status
        if (oldStatus.equals(StatusTypeCode.REPORTED.name())) {
            updateCommentNotifications(commentEntity.getTargetId());
        }

        //Construct response
        commentResponse.setId(commentEntity.getId());
        commentResponse.setText(commentEntity.getComment());
        commentResponse.setTime(DateUtil.convertToUTCDateTime(commentEntity.getCommentDate()));
        commentResponse.setStatus(commentEntity.getStatusTypeCode());
        commentResponse.setPax(participantInfoDao.getEmployeeDTO(paxId));

        return commentResponse;
    }

    @Override
    public CommentDTO reportComment(Long paxId, Long activityId, Long commentId, CommentDTO commentDTO) {

        Comment commentEntity = findCommentById(commentId);

        //Validate Status
        if (!(CommentConstants.REPORTED_STATUS_CODE.equals(commentDTO.getStatus()))) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_INVALID_STATUS_TYPE,
                    CommentConstants.ERR_INVALID_STATUS_TYPE_MSG);
        }

        if (commentEntity.getPaxId().equals(paxId)) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_CANNOT_REPORT_OWN_COMMENT,
                    CommentConstants.ERR_CANNOT_REPORT_OWN_COMMENT_MSG);
        }

        CommentDTO commentResponse = new CommentDTO();
        commentEntity.setStatusTypeCode(commentDTO.getStatus());
        commentDao.update(commentEntity);

        // Construct response
        commentResponse.setId(commentEntity.getId());
        commentResponse.setText(commentEntity.getComment());
        commentResponse.setTime(DateUtil.convertToUTCDateTime(commentEntity.getCommentDate()));
        commentResponse.setStatus(commentEntity.getStatusTypeCode());
        commentResponse.setPax(participantInfoDao.getEmployeeDTO(paxId));

        updateCommentNotifications(activityId);
        alertService.addReportedCommentAdminAlert(activityId, commentEntity);

        return commentResponse;
    }

    @Override
    public void deleteCommentOnActivity(Long paxId, Long activityId, Long commentId) throws ErrorMessageException {
        Comment comment = commentDao.findById(commentId);
        
        if (comment == null || comment.getPaxId() == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT_ID,
                    CommentConstants.ERR_COMMENT_NOT_FOUND,
                    CommentConstants.ERR_COMMENT_NOT_FOUND_MSG).setStatus(HttpStatus.NOT_FOUND);
        }

        if (!paxId.equals(comment.getPaxId())) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.PAX_ID,
                    CommentConstants.ERR_PAX_ID_MISMATCH,
                    CommentConstants.ERR_PAX_ID_MISMATCH_MSG).setStatus(HttpStatus.FORBIDDEN);
        }

        commentDao.delete(comment);
        updateCommentNotifications(activityId);
    }

    @Override
    public Comment findCommentById(Long id) {
        Comment commentEntity = commentDao.findById(id);
        if(commentEntity == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_COMMENT_NOT_FOUND,
                    CommentConstants.ERR_COMMENT_NOT_FOUND_MSG).setStatus(HttpStatus.NOT_FOUND);
        }
        return commentEntity;
    }

    @Override
    public Long getReportAbuseAdminPaxId() {
        String reportAbuseAdminData = environment.getProperty(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN);                
        
        Long reportAbuseAdminPaxId = null;
        if (reportAbuseAdminData != null && !reportAbuseAdminData.isEmpty()) {
            reportAbuseAdminPaxId = Long.valueOf(reportAbuseAdminData);
        }
        return reportAbuseAdminPaxId;
    }

    // Helper Functions
    public CommentDTO populateDTO(CommentDTO commentDTO, Comment node) {
        commentDTO.setId(node.getId());
        commentDTO.setTime(DateUtil.convertToUTCDateTime(node.getCommentDate()));
        commentDTO.setText(node.getComment());
        commentDTO.setStatus(node.getStatusTypeCode());

        EmployeeDTO paxDTO = participantInfoDao.getEmployeeDTO(node.getPaxId());

        commentDTO.setPax(paxDTO);

        return commentDTO;
    }

    private CommentDTO createComment(Long paxId, Long targetId, String targetTable, CommentDTO commentDTO) {

        Date date = new Date();
        CommentDTO commentResponse = new CommentDTO();
        String text = commentDTO.getText();

        Comment commentEntity = new Comment();
        
        checkTextLength(text);

        //Save Comment
        commentEntity.setPaxId(paxId);
        commentEntity.setComment(commentDTO.getText());
        commentEntity.setTargetTable(targetTable);
        commentEntity.setTargetId(targetId);
        commentEntity.setCommentDate(date);
        commentEntity.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        commentDao.save(commentEntity);

        alertService.addNotificationForRecognition(targetId, paxId, COMMENT_RECOGNITION_RECEIVER,
                COMMENT_RECOGNITION_SUBMITTER);

        //Construct response
        commentResponse.setId(commentEntity.getId());
        commentResponse.setText(commentEntity.getComment());
        commentResponse.setTime(DateUtil.convertToUTCDateTime(commentEntity.getCommentDate()));
        commentResponse.setPax(participantInfoDao.getEmployeeDTO(paxId));
        commentResponse.setStatus(commentEntity.getStatusTypeCode());

        return commentResponse;

    }

    private void checkTextLength(String text) {
        if (StringUtils.length(text) > CommentConstants.MAX_COMMENT_LENGTH) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_COMMENT_TOO_LONG,
                    CommentConstants.ERR_COMMENT_TOO_LONG_MSG);
        }
    }

    private void validateCommentDTO(CommentDTO commentDTO, Comment commentEntity, Long paxId, 
            Long reportAbuseAdminPaxId, Long commentId) {
        String text = commentDTO.getText();
        
        //Validate comment Id
        if (commentDTO.getId() == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_COMMENT_ID_IS_NULL,
                    CommentConstants.ERR_COMMENT_ID_IS_NULL_MSG);
        } else if (!commentDTO.getId().equals(commentId)) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_COMMENT_ID_MISMATCH,
                    CommentConstants.ERR_COMMENT_ID_MISMATCH_MSG);
        }
        checkTextLength(text);

        //Validate PaxID
        if (!paxId.equals(commentEntity.getPaxId()) && 
                !CommentConstants.REPORTED_STATUS_CODE.equals(commentDTO.getStatus())
                && !security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_PAX_ID_MISMATCH,
                    CommentConstants.ERR_PAX_ID_MISMATCH_MSG);
        }

        //Validate Comment not already marked abusive
        if (commentEntity.getStatusTypeCode().equals(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE)) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_CANNOT_MODIFY_ABUSIVE_COMMENT,
                    CommentConstants.ERR_CANNOT_MODIFY_ABUSIVE_COMMENT_MSG);
        }

        //Validate Status
        if (commentDTO.getStatus() == null || !(commentDTO.getStatus().equals(StatusTypeCode.ACTIVE.name())
                || commentDTO.getStatus().equals(CommentConstants.REPORTED_STATUS_CODE)
                || commentDTO.getStatus().equals(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE))
                ) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_INVALID_STATUS_TYPE,
                    CommentConstants.ERR_INVALID_STATUS_TYPE_MSG);
        }

        //validate comment Text
        if(!paxId.equals(commentEntity.getPaxId()) && !(commentEntity.getComment().equals(commentDTO.getText())) ) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_CANNOT_CHANGE_COMMENT,
                    CommentConstants.ERR_CANNOT_CHANGE_COMMENT_MSG).setStatus(HttpStatus.FORBIDDEN);
        }

        //validate that user can perform this action
        if ((commentDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()) || 
                commentDTO.getStatus().equals(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE))
                && !(commentEntity.getStatusTypeCode().equals(StatusTypeCode.ACTIVE.name()))) {
            if (commentEntity.getStatusTypeCode().equals(CommentConstants.REPORTED_STATUS_CODE) &&
                    !security.getPaxId().equals(reportAbuseAdminPaxId)) {
                throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                        CommentConstants.ERR_CANNOT_MODIFY_REPORTED_COMMENT,
                        CommentConstants.ERR_CANNOT_MODIFY_REPORTED_COMMENT_MSG).setStatus(HttpStatus.FORBIDDEN);
            }
        }

        //validate the status is not changing directly to abusive from active or vice versa
        if (commentDTO.getStatus().equals(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE) &&
                commentEntity.getStatusTypeCode().equals(StatusTypeCode.ACTIVE.name())
                || (commentDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()) &&
                        commentEntity.getStatusTypeCode().equals(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE))) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.COMMENT,
                    CommentConstants.ERR_CANNOT_SKIP_REPORTED_STATE,
                    CommentConstants.ERR_CANNOT_SKIP_REPORTED_STATE_MSG).setStatus(HttpStatus.BAD_REQUEST);
        }
        
    }
    
    /**
     * Update comment notifications.
     * If a comment is deleted and there are no other active comments, we will delete the notification.
     * When you report a comment and there are no other active comments, the notification will be updated to CANCELLED
     * If a REPORTED comment is updated back to ACTIVE, we will put the notification in it's prior status.
     * If a REPORTED comment is updated to ABUSE_CONFIRMED, we will delete the notification if there are no other active comments.
     * @param activityId - Id of activity to check
     */
    private void updateCommentNotifications(Long activityId) {
        NewsfeedItem newsfeedItem = newsfeedItemDao.findById(activityId);
        List<Comment> commentList = commentDao.findBy()
                .where(ProjectConstants.TARGET_TABLE).eq(TableName.NEWSFEED_ITEM.name())
                .and(ProjectConstants.TARGET_ID).eq(activityId)
                .find();

        boolean active = false;
        boolean reported = false;
        if (!CollectionUtils.isEmpty(commentList)) {
            //See what status these comments are in
            for (Comment comment : commentList) {
                String status = comment.getStatusTypeCode();
                if (status.equals(StatusTypeCode.ACTIVE.name())) {
                    active = true;
                    break; //If at least one comment is active, we need to keep the alert
                } else if (status.equals(StatusTypeCode.REPORTED.name())) {
                    reported = true;
                }
            }
        }

        // get comment notification for receivers and submitter.
        List<Alert> alertList = alertDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(newsfeedItem.getId())
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).in(AlertSubType.COMMENT_RECOGNITION_RECEIVER.toString(),
                        AlertSubType.COMMENT_RECOGNITION_SUBMITTER.toString())
                .find();
        
        if (alertList != null) {
            if (active) {
                //If a REPORTED comment is updated back to ACTIVE, we will put the notification in it's prior status
                for (Alert alert : alertList) {
                    //TODO MP-10060 Use the notification's prior status once we have the HISTORY_ALERT table
                    alert.setStatusTypeCode(StatusTypeCode.NEW.toString());
                    alert.setStatusDate(new Date());
                    alertDao.save(alert);
                }
            }

            if (!active && reported) {
                //When you report a comment and there are no other active comments, the notification will be updated to CANCELLED
                for (Alert alert : alertList) {
                    alert.setStatusTypeCode(StatusTypeCode.CANCELLED.toString());
                    alert.setStatusDate(new Date());
                    alertDao.save(alert);
                }
            }

            if ((!active && !reported) || commentList.isEmpty()) {
                //If a comment is deleted and there are no other active comments, we will delete the notification
                //If a REPORTED comment is updated to ABUSE_CONFIRMED, we will delete the notification if there are no other active comments
                for (Alert alert : alertList) {
                    alertDao.delete(alert);
                }
            }
        }
    }
}
