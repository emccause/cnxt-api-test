package com.maritz.culturenext.social.comment.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentDTO {
    private Long id;
    private String time;
    private String text;
    private EmployeeDTO pax;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public EmployeeDTO getPax() {
        return pax;
    }

    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




}