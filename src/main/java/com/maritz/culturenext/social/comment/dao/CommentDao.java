package com.maritz.culturenext.social.comment.dao;

import java.util.List;
import java.util.Map;

public interface CommentDao {
    
    public List<Map<String, Object>> getCommentsForActivity(Integer pageNumber, Integer pageSize, Long activityIDInt, Long paxId);

}
