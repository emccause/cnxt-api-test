package com.maritz.culturenext.social.comment.services;

import java.util.List;

import com.maritz.core.jpa.entity.Comment;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

public interface CommentService {
    
    /**
     * Current Version
     * Returns a list of all comments on an activity tile
     * 
     * @param page
     * @param size
     * @param activityIDInt - activity tile ID
     * @param paxId - logged in pax ID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15564847/Get+a+list+of+Comments+on+an+Activity
     */
    List<CommentDTO> getCommentsForActivityCurrent(Integer page, Integer size, Long activityIDInt, Long paxId);
    
    /**
     * Future Version
     * Returns a list of all comments on an activity tile
     * 
     * @param pageNumber
     * @param pageSize
     * @param activityIDInt - activity tile ID
     * @param paxId - logged in pax ID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15564847/Get+a+list+of+Comments+on+an+Activity
     */
    PaginatedResponseObject<CommentDTO> getCommentsForActivity(Integer pageNumber, Integer pageSize, Long activityIDInt, Long paxId);
    
    /**
     * Creates a comment on an activity tile
     * 
     * @param paxId - Person submitting the comment
     * @param id - ID of the activity tile
     * @param commentDTO - Comment to be posted
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15564849/Create+Comment+on+an+Activity
     */
    CommentDTO createCommentOnActivity(Long paxId, Long id, CommentDTO commentDTO);
    
    /**
     * Updates an existing comment on an activity tile
     * 
     * @param paxId - Logged in pax ID. Must be the submitter of the comment
     * @param commentId - ID of the comment to edit
     * @param commentDTO - New comment to save
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20054120/Update+Comment+on+Activity
     */
    CommentDTO updateComment(Long paxId, Long commentId, CommentDTO commentDTO);
    
    /**
     * Report a comment as abusive. This will update the comment's status to REPORTED and create an alert.
     * 
     * @param paxId
     * @param activityId
     * @param commentId
     * @param commentDTO
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20054120/Update+Comment+on+Activity
     */
    CommentDTO reportComment(Long paxId, Long activityId, Long commentId, CommentDTO commentDTO);
    
    /**
     * Deletes a comment from an activity tile. The pax ID can only delete comments they have submitted.
     * 
     * @param paxId - Logged in pax ID. Must be the submitter of the comment
     * @param activityId - Activity ID to remove the comment from
     * @param commentId - ID of the comment record
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/20054118/Delete+Comment+from+an+Activity
     */
    void deleteCommentOnActivity(Long paxId, Long activityId, Long commentId);
    
    /**
     * TODO javadocs
     * 
     * @param id
     * @return
     */
    Comment findCommentById(Long id);
    
    /**
     * TODO javadocs
     * 
     * @return
     */
    Long getReportAbuseAdminPaxId();
    
    /**
     * TODO javadocs
     * 
     * @param commentDTO
     * @param node
     * @return
     */
    CommentDTO populateDTO(CommentDTO commentDTO, Comment node);
}
