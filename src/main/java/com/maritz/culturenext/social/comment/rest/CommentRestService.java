package com.maritz.culturenext.social.comment.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.social.comment.constants.CommentConstants;
import com.maritz.culturenext.social.comment.dto.CommentDTO;
import com.maritz.culturenext.social.comment.services.CommentService;
import com.maritz.culturenext.constants.ProjectConstants;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.List;

import javax.inject.Inject;

@RestController
@RequestMapping("participants")
@Api(value="/comment", description="All end points dealing with comments")
public class CommentRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private CommentService commentService;
    @Inject private Security security;

    //rest/participants/~/activity/~/comments
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() " +
            " and @permissionUtil.hasPermission('" + PermissionConstants.COMMENTING_TYPE + "')")
    @RequestMapping(value = "{paxId}/activity/{activityId}/comments", method = RequestMethod.GET, headers = {CURRENT_VERSION_HEADER})
    @ApiOperation(value="Returns one comment from the list of comments")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "successfully returned the comment list for the given activity.", response = CommentDTO.class),
            @ApiResponse(code = 403, message = "User does not have permission.")
            }
    )
    @Permission("PUBLIC")
    public List<CommentDTO> getCommentFeed (
            @ApiParam(value = "Page of comment feed used to paginate the request", required = false) 
                @RequestParam( value=PAGE_REST_PARAM,required = false ) Integer page,
            @ApiParam(value = "The number of records per page.", required = false) 
                @RequestParam( value=SIZE_REST_PARAM,required = false ) Integer size,
            @ApiParam(value = "Participant Id of the user you want") 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "Activity Id for the activity you want") 
                @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdInt
    ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdInt, null);
        if (activityId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return commentService.getCommentsForActivityCurrent(page, size, activityId, paxId);
    }

    //rest/participants/~/activity/~/comments
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() " +
            " and @permissionUtil.hasPermission('" + PermissionConstants.COMMENTING_TYPE + "')")
    @RequestMapping(value = "{paxId}/activity/{targetId}/comments", method = RequestMethod.GET, headers = {"version=future"})
    @ApiOperation(value="Returns one comment from the list of comments")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "successfully returned the comment list for the given activity.", response = CommentDTO.class),
            @ApiResponse(code = 403, message = "User does not have permission.")
            }
    )
    @Permission("PUBLIC")
    public @ResponseBody PaginatedResponseObject<CommentDTO> getCommentsForActivity (
            @ApiParam(value="id of activity to filter by", required = false) 
                @PathVariable(TARGET_ID_REST_PARAM) String targetIdString, 
            @ApiParam(value="id of pax to filter by",required = false) 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString, 
            @ApiParam(value="page number to fetch", required = false) 
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
            @ApiParam(value="size of page to fetch", required = false) 
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
            ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long targetId = security.getId(targetIdString, null);
        if (targetId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return commentService.getCommentsForActivity(pageNumber, pageSize, targetId, paxId);
    }

    //rest/participants/~/activity/~/comments
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() " +
            " and @permissionUtil.hasPermission('" + PermissionConstants.COMMENTING_TYPE + "')")
    @RequestMapping(value = "{paxId}/activity/{activityId}/comments", method = RequestMethod.POST)
    @ApiOperation(value = "Endpoint for the action of 'commenting' on an Activity item by a participant."
            + " Returns the saved comment record")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully created activity comment", response = CommentDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission.")
    })
    @Permission("PUBLIC")
    public CommentDTO createActivityComment(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,
            @RequestBody CommentDTO commentDTO) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if (activityId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return commentService.createCommentOnActivity(paxId, activityId, commentDTO);
    }

    //rest/participants/~/activity/~/comments/~
    @PreAuthorize("!@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.COMMENTING_TYPE + "')")
    @RequestMapping(value = "{paxId}/activity/{activityId}/comments/{commentId}", method = RequestMethod.PUT )
    @ApiOperation(value="Endpoint for the action of updating a comment on an Activity item. "
            + "Returns the newly updated comment record.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully updated comment on activity",response = CommentDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "No content"),
            @ApiResponse(code = 403, message = "User does not have permission.")
    })
    @Permission("PUBLIC")
    public ResponseEntity<CommentDTO> updateActivityComment(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,
            @PathVariable(COMMENT_ID_REST_PARAM) String commentIdString,
            @RequestBody CommentDTO commentDTO) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Long activityId = security.getId(activityIdString, null);
        if (activityId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Long commentId = security.getId(commentIdString, null);
        if (commentId == null) {
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        if (!security.isMyPax(paxId) && 
                !security.hasRole(ProjectConstants.ADMIN_ROLE,ProjectConstants.CLIENT_ADMIN_ROLE)) {    
            return  new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } 

        if (commentDTO.getStatus() != null && commentDTO.getStatus().equals(CommentConstants.REPORTED_STATUS_CODE)) {
            return new ResponseEntity<>(commentService.reportComment(paxId, activityId, commentId, commentDTO),
                    HttpStatus.OK);    
        }
        
        return new ResponseEntity<>(commentService.updateComment(paxId, commentId, commentDTO), 
                HttpStatus.OK);
        
    }

    //rest/participants/~/activity/~/comments/~
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() " +
            " and @permissionUtil.hasPermission('" + PermissionConstants.COMMENTING_TYPE + "')")
    @RequestMapping(value = "{paxId}/activity/{activityId}/comments/{commentId}", method = RequestMethod.DELETE )
    @ApiOperation(value="Deletes a comment on an Activity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted activity comment"),
            @ApiResponse(code = 403, message = "User not authenticated or does not have permission"),
            @ApiResponse(code = 404, message = "No content")
    })
    @Permission("PUBLIC")
    public void deleteActivityComment(@PathVariable(ProjectConstants.PAX_ID) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,  
            @PathVariable(COMMENT_ID_REST_PARAM) String commentIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if(activityId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long commentId;
        try {
            commentId = Long.parseLong(commentIdString);
        } catch (NumberFormatException e) {
            logger.error("Malformed commentId on comment delete");
            throw new NoHandlerFoundException(null, null, null);
        }
    
        commentService.deleteCommentOnActivity(paxId, activityId, commentId);
    }
}
