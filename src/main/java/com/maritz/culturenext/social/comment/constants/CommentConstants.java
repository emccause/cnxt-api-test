package com.maritz.culturenext.social.comment.constants;

public class CommentConstants {
    
    public static final int MAX_COMMENT_LENGTH = 250;
    
    // Status type codes
    public static final String REPORTED_STATUS_CODE = "REPORTED";
    public static final String ABUSE_CONFIRMED_STATUS_CODE = "ABUSE_CONFIRMED";
    public static final String NEWSFEED_ITEM_TABLE = "NEWSFEED_ITEM";
    
    //Pagination Related Constants
    public static final String REPLACE_PAX_ID = "replace_pax_id";
    public static final String REPLACE_TARGET_ID = "replace_target_id";
    public static final String COMMENT_REST_ENDPOINT = "/participants/" + REPLACE_PAX_ID + "/activity/" + REPLACE_TARGET_ID + "/comments";

    // Error codes
    public static final String ERR_COMMENT_TOO_LONG = "COMMENT_TOO_LONG";
    public static final String ERR_COMMENT_NOT_FOUND = "COMMENT_NOT_FOUND";
    public static final String ERR_COMMENT_ID_MISMATCH = "COMMENT_ID_MISMATCH";
    public static final String ERR_COMMENT_ID_IS_NULL = "COMMENT_ID_NULL";
    public static final String ERR_PAX_ID_MISMATCH = "PAX_ID_MISMATCH";
    public static final String ERR_INVALID_STATUS_TYPE = "INVALID_STATUS_TYPE";
    public static final String ERR_CANNOT_CHANGE_COMMENT = "CANNOT_CHANGE_COMMENT";
    public static final String ERR_CANNOT_REPORT_OWN_COMMENT = "CANNOT_REPORT_OWN_COMMENT";
    public static final String ERR_CANNOT_MODIFY_REPORTED_COMMENT = "ERR_CANNOT_MODIFY_REPORTED_COMMENT";
    public static final String ERR_CANNOT_MODIFY_ABUSIVE_COMMENT = "ERR_CANNOT_MODIFY_ABUSIVE_COMMENT";
    public static final String ERR_CANNOT_SKIP_REPORTED_STATE = "ERR_CANNOT_SKIP_REPORTED_STATE";
    
    // Error messages
    public static final String ERR_COMMENT_TOO_LONG_MSG = "Comment text is too long";
    public static final String ERR_COMMENT_NOT_FOUND_MSG = "Comment is not found";
    public static final String ERR_COMMENT_ID_MISMATCH_MSG = "Comment ID in URL does not match ID in request";
    public static final String ERR_COMMENT_ID_IS_NULL_MSG = "Comment ID is missing";
    public static final String ERR_PAX_ID_MISMATCH_MSG = "User Pax ID does not match Comment's Pax ID";
    public static final String ERR_INVALID_STATUS_TYPE_MSG = "the provided status type is invalid";
    public static final String ERR_CANNOT_CHANGE_COMMENT_MSG = "Cannot change other users comments";
    public static final String ERR_CANNOT_REPORT_OWN_COMMENT_MSG = "Users cannot report their own comments";
    public static final String ERR_CANNOT_MODIFY_REPORTED_COMMENT_MSG = 
            "Only the report abuse admin may modify a reported comment";
    public static final String ERR_CANNOT_MODIFY_ABUSIVE_COMMENT_MSG = 
            "Users cannot change a comment that has been deemed abusive";
    public static final String ERR_CANNOT_SKIP_REPORTED_STATE_MSG = 
            "A comment must go through the REPORTED state before being deemed abusive or active";
}
