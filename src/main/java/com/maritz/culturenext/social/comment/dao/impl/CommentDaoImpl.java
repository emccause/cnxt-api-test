package com.maritz.culturenext.social.comment.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.social.comment.dao.CommentDao;

@Repository
public class CommentDaoImpl extends PaginatingDaoImpl implements CommentDao {
    
    private static final String TARGET_ID_PLACEHOLDER = "TARGET_ID";
    
    private static final String COMMENT_PAGINATION_QUERY = "SELECT " +
            "COMMENT.ID AS COMMENT_ID, " +
            "COMMENT.COMMENT_DATE AS COMMENT_DATE, " +
            "COMMENT.COMMENT AS COMMENT_TEXT, " +
            "COMMENT.STATUS_TYPE_CODE AS COMMENT_PAX_STATUS, " +
            "UGSD.* " +
            "FROM COMMENT COMMENT " +
            "INNER JOIN VW_USER_GROUPS_SEARCH_DATA UGSD " + 
                "ON COMMENT.PAX_ID = UGSD.PAX_ID " + 
            "WHERE COMMENT.TARGET_ID = :" + TARGET_ID_PLACEHOLDER + " " +
                "AND COMMENT.STATUS_TYPE_CODE = '" + StatusTypeCode.ACTIVE.name() + "' ";
    
    private static final String COMMENT_PAGINATION_ORDER_BY = "COMMENT_DATE DESC";
    
    @Override
    public List<Map<String, Object>> getCommentsForActivity(Integer pageNumber, Integer pageSize, Long targetId, Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(TARGET_ID_PLACEHOLDER, targetId);
        
        return getPageWithTotalRowsCTE(COMMENT_PAGINATION_QUERY, params, new CamelCaseMapRowMapper(), COMMENT_PAGINATION_ORDER_BY, pageNumber, pageSize);
    }
}
