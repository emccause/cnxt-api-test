package com.maritz.culturenext.social.like.dao;

import java.util.List;

import com.maritz.culturenext.social.like.dto.LikeDTO;

public interface PaginatedLikeListDao {

    List<LikeDTO> getListOfLikes(String targetTable, Long targetId);

}
