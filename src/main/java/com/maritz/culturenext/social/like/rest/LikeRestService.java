package com.maritz.culturenext.social.like.rest;

import java.util.List;

import static com.maritz.culturenext.constants.RestParameterConstants.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.social.like.dto.LikeDTO;
import com.maritz.culturenext.social.like.service.LikeService;

@RestController
@RequestMapping("participants")
@Api(value="/likes", description="All end points dealing with likes")
public class LikeRestService {

    @Inject private LikeService likeService;
    @Inject private Security security;

    //rest/participants/~/activity/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/activity/{activityId}/likes", method = RequestMethod.POST)
    @ApiOperation(value="Creates a like entry for an activity", 
    notes="This method allows a user to like a newsfeed activity.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully liked", response = NewsfeedItemDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found") 
            }
    )
    @Permission("PUBLIC")
    public NewsfeedItemDTO likeActivity(@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if(activityId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return likeService.likeActivity(paxId, activityId);        
    }

    //rest/participants/~/programs/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/programs/{programId}/likes", method = RequestMethod.POST)
    @ApiOperation(value="Creates a like entry for a program", notes="This method allows a user to like a program.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully liked", response = ProgramFullDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found")
            }
    )
    @Permission("PUBLIC")
    public ProgramFullDTO likeProgram(@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long programId = security.getId(programIdString, null);
        if(programId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return likeService.likeProgram(paxId, programId);        
    }

    //rest/participants/~/activity/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/activity/{activityId}/likes", method = RequestMethod.DELETE)
    @ApiOperation(value="Deletes a like entry for an activity.", 
    notes="This method allows a user to unlike a newsfeed activity.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully unlikedliked",    response = NewsfeedItemDTO.class), 
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found")
            }
    )
    @Permission("PUBLIC")
    public NewsfeedItemDTO unlikeActivity(@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if(activityId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return likeService.unlikeActivity(paxId, activityId);    
    }

    //rest/participants/~/programs/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/programs/{programId}/likes", method = RequestMethod.DELETE)
    @ApiOperation(value="Deletes a like entry for a program", 
    notes="This method allows a user to unlike for a program.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully liked", response = ProgramFullDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found")
            }
    )
    @Permission("PUBLIC")
    public ProgramFullDTO unlikeProgram(@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long programId = security.getId(programIdString, null);
        if(programId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return likeService.unlikeProgram(paxId, programId);        
    }
    
    //rest/participants/~/activity/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/activity/{activityId}/likes", method = RequestMethod.GET)
    @ApiOperation(value="Get list of likes for an activity", 
    notes="This method allows a user to get a paginated list of likes for a newsfeed activity.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully processed", response = List.class), 
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found")
            }
    )
    @Permission("PUBLIC")
    public List<LikeDTO> getLikesForActivity(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString, 
            // TODO Pagination Techdebt: MP-7516 & MP-7533
            @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if(activityId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return likeService.getLikesForActivity(paxId, activityId, page, size);
    }

    //rest/participants/~/programs/~/likes
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.LIKING_TYPE + "')")   
    @RequestMapping(value = "{paxId}/programs/{programId}/likes", method = RequestMethod.GET)
    @ApiOperation(value="Get list of likes for a program",
            notes="This method allows a user to get a paginated list of likes for a program.")
    @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "Successfully processed", response = List.class),        
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 403, message = "User does not have permission"),
            @ApiResponse(code = 404, message = "Participant not found")
            }
    )
    @Permission("PUBLIC")
    public List<LikeDTO> getLikesForProgram(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(PROGRAM_ID_REST_PARAM) String programIdString, 
            // TODO Pagination Techdebt: MP-7517 & MP-7534
            @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page, 
            @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        Long programId = security.getId(programIdString, null);
        if(programId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return likeService.getLikesForProgram(programId, page, size);        
    }

}
