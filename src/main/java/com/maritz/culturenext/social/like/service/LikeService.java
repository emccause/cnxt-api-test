package com.maritz.culturenext.social.like.service;

import java.util.List;

import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.social.like.dto.LikeDTO;

public interface LikeService {

    /**
     * Add a like to an activity tile
     * 
     * @param paxId - logged in pax ID
     * @param activityId - activity tile ID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15040579/Like+an+Activity
     */
    NewsfeedItemDTO likeActivity(Long paxId, Long activityId);
    
    /**
     * Add a like to a program
     * 
     * @param paxId - logged in pax ID
     * @param programId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15040585/Like+a+Program
     */
    ProgramFullDTO likeProgram(Long paxId, Long programId);

    /** 
     * Remove a like from an activity tile
     * 
     * @param paxId
     * @param activityId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15040581/Delete+Like+of+an+Activity
     */
    NewsfeedItemDTO unlikeActivity(Long paxId, Long activityId);
    
    /**
     * Remove a like from a program
     * 
     * @param paxId
     * @param programId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15040583/Delete+or+Remove+the+Like+of+a+Program
     */
    ProgramFullDTO unlikeProgram(Long paxId, Long programId);
    
    /**
     * Get a list of all likes tied to an activity tile
     * 
     * @param paxId
     * @param activityId
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/14581876/Get+the+pax-list+for+Likes+of+an+activity
     */
    List<LikeDTO> getLikesForActivity(Long paxId, Long activityId, Integer page, Integer size);
    
    /**
     * Get a list of all likes tied to a program
     * 
     * @param programId
     * @param page
     * @param size
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/15040573/Get+the+pax-list+for+Likes+of+a+Program
     */
    List<LikeDTO> getLikesForProgram(Long programId, Integer page, Integer size);
}
