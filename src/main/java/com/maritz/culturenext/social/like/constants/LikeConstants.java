package com.maritz.culturenext.social.like.constants;

public class LikeConstants {
    public static final String ERR_ACTIVITY_DOES_NOT_EXIST = "ACTIVITY_DOES_NOT_EXIST";
    public static final String ERR_ACTIVITY_DOES_NOT_EXIST_MSG = "That activity does not exist.";
    public static final String ERR_ACTIVITY_INVALID = "ACTIVITY_INVALID";
    public static final String ERR_ACTIVITY_INVALID_MSG = "That activity is not valid.";

    public static final String ERR_PROGRAM_DOES_NOT_EXIST = "PROGRAM_DOES_NOT_EXIST";
    public static final String ERR_PROGRAM_DOES_NOT_EXIST_MSG = "That program does not exist.";
    public static final String PROGRAM_TABLE = "PROGRAM";

    public static final String ERR_ALREADY_LIKED = "ALREADY_LIKED";
    public static final String ERR_ALREADY_LIKED_MSG = "You have already liked that.";

    public static final String ERR_NOT_LIKED = "NOT_LIKED";
    public static final String ERR_NOT_LIKED_MSG = "You do not like that.";

}
