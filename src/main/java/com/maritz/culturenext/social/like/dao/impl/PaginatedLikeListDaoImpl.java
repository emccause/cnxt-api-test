package com.maritz.culturenext.social.like.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.like.dao.PaginatedLikeListDao;
import com.maritz.culturenext.social.like.dto.LikeDTO;
import com.maritz.culturenext.util.DateUtil;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class PaginatedLikeListDaoImpl extends AbstractDaoImpl implements PaginatedLikeListDao {

    private static final String LIKE_DATE = "LIKE_DATE";    
    private static final String TARGET_TABLE_SQL_PARAM = "TARGET_TABLE";
    private static final String TARGET_ID_SQL_PARAM = "TARGET_ID";
    
    private static final String STATUS_TYPE_CODE = "STATUS_TYPE_CODE";
    private static final String VIEW_PAX_ID ="VW.PAX_ID";
    private static final String LANGUAGE_CODE = "LANGUAGE_CODE";
    private static final String PREFERRED_LOCALE = "PREFERRED_LOCALE";
    private static final String CONTROL_NUM = "CONTROL_NUM";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String MIDDLE_NAME = "MIDDLE_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String NAME_PREFIX = "NAME_PREFIX";
    private static final String NAME_SUFFIX = "NAME_SUFFIX";
    private static final String COMPANY_NAME = "COMPANY_NAME";
    private static final String PROFILE_PICTURE_VERSION = "VERSION";
    private static final String JOB_TITLE = "JOB_TITLE";
    private static final String MANAGER_PAX_ID = "MANAGER_PAX_ID";
    
    //Result Set Columns (need to be camelCase)
    private static final String RS_LIKE_DATE = "likeDate";
    
    private static final String QUERY = 
            "SELECT " +
            " " + LIKE_DATE + " " +
            ", " + STATUS_TYPE_CODE + " " +
            ", " + VIEW_PAX_ID + " " +
            ", " + LANGUAGE_CODE + " " +
            ", " + PREFERRED_LOCALE + " " +
            ", " + CONTROL_NUM + " " +
            ", " + FIRST_NAME + " " +
            ", " + MIDDLE_NAME + " " +
            ", " + LAST_NAME + " " +
            ", " + NAME_PREFIX + " " +
            ", " + NAME_SUFFIX + " " +
            ", " + COMPANY_NAME + " " +
            ", " + PROFILE_PICTURE_VERSION + " " +
            ", " + JOB_TITLE + " " +
            ", " + MANAGER_PAX_ID + " " +
            "FROM component.LIKES INNER JOIN component.VW_USER_GROUPS_SEARCH_DATA VW ON LIKES.PAX_ID = VW.PAX_ID " +
            "WHERE LIKES.TARGET_TABLE = :" + TARGET_TABLE_SQL_PARAM + " AND LIKES.TARGET_ID = :" + TARGET_ID_SQL_PARAM +" " +
            "ORDER BY LIKES.LIKE_DATE DESC, VW.NAME ";

    @Override
    public List<LikeDTO> getListOfLikes(String targetTable, Long targetId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(TARGET_TABLE_SQL_PARAM, targetTable);
        params.addValue(TARGET_ID_SQL_PARAM, targetId);
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(QUERY, params, new CamelCaseMapRowMapper());

        ArrayList<LikeDTO> resultsList = new ArrayList<LikeDTO>();
        for (Map<String, Object> node: nodes) {
                
            LikeDTO like = new LikeDTO();
            Date likeDate = (Date) node.get(RS_LIKE_DATE);
            if (likeDate != null) {
                like.setTime(DateUtil.convertToUTCDateTime(likeDate));
            }
            like.setPax(new EmployeeDTO(node, null));
            resultsList.add(like);
                
        }
        return resultsList;
    }
}
