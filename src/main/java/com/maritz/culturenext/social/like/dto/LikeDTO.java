package com.maritz.culturenext.social.like.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class LikeDTO {

    String time;
    EmployeeDTO pax;
    
    public String getTime() {
        return time;
    }
    
    public LikeDTO setTime(String time) {
        this.time = time;
        return this;
    }
    
    public EmployeeDTO getPax() {
        return pax;
    }
    
    public LikeDTO setPax(EmployeeDTO pax) {
        this.pax = pax;
        return this;
    }
}
