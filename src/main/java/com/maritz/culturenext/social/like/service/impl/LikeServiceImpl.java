package com.maritz.culturenext.social.like.service.impl;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Likes;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.social.like.constants.LikeConstants;
import com.maritz.culturenext.social.like.dao.PaginatedLikeListDao;
import com.maritz.culturenext.social.like.dto.LikeDTO;
import com.maritz.culturenext.social.like.service.LikeService;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class LikeServiceImpl implements LikeService {

    @Inject private AlertService alertService;
    @Inject private ConcentrixDao <Alert> alertDao;
    @Inject private ConcentrixDao<Likes> likesDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ActivityFeedService activityFeedService;
    @Inject private PaginatedLikeListDao paginatedLikeListDao;
    @Inject private ProgramService programService;
    @Inject private Security security;

    @Override
    public NewsfeedItemDTO likeActivity(Long paxId, Long activityId) {

        validateActivity(paxId, activityId);
        doLike(paxId, TableName.NEWSFEED_ITEM.name(), activityId);

        alertService.addNotificationForRecognition(activityId, security.getPaxId(),
                AlertSubType.LIKE_RECOGNITION_RECEIVER.toString(), AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString());

        return activityFeedService.getActivityById(paxId, activityId, null, null, null, Boolean.FALSE, Boolean.FALSE, ""); // get default feed
    }

    @Override
    public ProgramFullDTO likeProgram(Long paxId, Long programId) {

        validateProgram(programId);
        doLike(paxId, LikeConstants.PROGRAM_TABLE, programId);

        return programService.getProgramByID(programId, null);
    }

    @Override
    public NewsfeedItemDTO unlikeActivity(Long paxId, Long activityId) {

        validateActivity(paxId, activityId);
        doUnlike(paxId, TableName.NEWSFEED_ITEM.name(), activityId);
        deleteLikeNotifications(activityId);
        return activityFeedService.getActivityById(paxId, activityId, null, null, null, Boolean.FALSE, Boolean.FALSE, ""); // get default feed
    }

    @Override
    public ProgramFullDTO unlikeProgram(Long paxId, Long programId) {

        validateProgram(programId);
        doUnlike(paxId, LikeConstants.PROGRAM_TABLE, programId);

        return programService.getProgramByID(programId, null);
    }

    @Override
    public List<LikeDTO> getLikesForActivity(Long paxId, Long activityId, Integer page, Integer size) {

        validateActivity(paxId, activityId);

        return PaginationUtil.getPaginatedList(paginatedLikeListDao
                .getListOfLikes(TableName.NEWSFEED_ITEM.name(), activityId), page, size);

    }

    @Override
    public List<LikeDTO> getLikesForProgram(Long programId, Integer page, Integer size) {

        validateProgram(programId);

        return PaginationUtil.getPaginatedList(paginatedLikeListDao
                .getListOfLikes(LikeConstants.PROGRAM_TABLE, programId), page, size);
    }

    private void validateActivity(Long paxId, Long activityId) {
        NewsfeedItem newsfeedItem = newsfeedItemDao.findById(activityId);

        try{
            activityFeedService.getActivityById(paxId, activityId, null, null, null, Boolean.FALSE, Boolean.FALSE, ""); // get default feed
        }
        catch(Exception e) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.ACTIVITY_ID,
                    LikeConstants.ERR_ACTIVITY_INVALID, LikeConstants.ERR_ACTIVITY_INVALID_MSG);
        }

        if(newsfeedItem == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.ACTIVITY_ID,
                    LikeConstants.ERR_ACTIVITY_DOES_NOT_EXIST, LikeConstants.ERR_ACTIVITY_DOES_NOT_EXIST_MSG);
        }
    }

    private void validateProgram(Long programId) {
        Program program = programDao.findById(programId);
        if(program == null){
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.PROGRAM_ID,
                    LikeConstants.ERR_PROGRAM_DOES_NOT_EXIST, LikeConstants.ERR_PROGRAM_DOES_NOT_EXIST_MSG);
        }
    }

    private void doLike(Long paxId, String targetTable, Long targetId) {

        Likes like = likesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.TARGET_TABLE).eq(targetTable)
                .and(ProjectConstants.TARGET_ID).eq(targetId)
                .findOne();

        if(like != null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.REQUEST,
                    LikeConstants.ERR_ALREADY_LIKED, LikeConstants.ERR_ALREADY_LIKED_MSG);
        }

        like = new Likes();
        like.setPaxId(paxId);
        like.setTargetTable(targetTable);
        like.setTargetId(targetId);
        like.setLikeDate(new Date());
        likesDao.save(like);
    }

    private void doUnlike(Long paxId, String targetTable, Long targetId) {

        Likes like = likesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.TARGET_TABLE).eq(targetTable)
                .and(ProjectConstants.TARGET_ID).eq(targetId)
                .findOne();

        if(like == null) {
            throw new ErrorMessageException().addErrorMessage(ProjectConstants.REQUEST,
                    LikeConstants.ERR_NOT_LIKED, LikeConstants.ERR_NOT_LIKED_MSG);
        }

        likesDao.delete(like);
    }

    /**
     * Check if there is no like records anymore and delete the like notification.
     * @param activityId
     */
    private void deleteLikeNotifications(Long activityId) {
        NewsfeedItem newsfeedItem = newsfeedItemDao.findById(activityId);

        Likes like = likesDao.findBy()
                .where(ProjectConstants.TARGET_TABLE).eq(TableName.NEWSFEED_ITEM.name())
                .and(ProjectConstants.TARGET_ID).eq(activityId)
                .findOne();
        //all like records have been deleted.
        if(like == null) {
            //get LIKE notification for receivers and submitter.
            List<Alert> alertList = alertDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(newsfeedItem.getId())
                    .and(ProjectConstants.ALERT_SUB_TYPE_CODE).in(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString(),
                            AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                    .find();
            //delete notifications
            for(Alert alert : alertList) {
                alertDao.delete(alert);
            }
        }
    }
}
