package com.maritz.culturenext.awardtype.dao;

import java.util.List;
import java.util.Map;

public interface AwardTypeDao {

    /**
     * Get all configured award types
     */
    public List<Map<String, Object>>  getAllAwardTypes();

    /**
     * Get award type by payout type
     */
    public List<Map<String, Object>> getAwardTypeByPayoutType(String payoutType);
    
    /**
     * Delete an existing award type. Can only be deleted 
     * if isUsed = false (If the award type is not currently tied to a program and has never been used)
     * 
     * @param id - identifier of the award type to delete
     * 
     */
    public void deleteAwardType(Long id);
    
    /**
     * Return a user's eligibility to receive the award types configured at the site level
     * @param paxId - the user's pax ID
     */
    public List<Map<String, Object>>  getAwardTypeEligibilityToReceiveList(Long paxId);

    /**
     * Returns a list of concatenated product codes to be used to update the "mars.product" property
     * Ex. DPP=DPP
     */
    public List<Map<String, Object>> getMarsProductValues();
    
    /**
     * Returns a list of concatenated subclient number to be used to update the "mars.subclient" property
     * Ex. DPP=1234.5
     */
    public List<Map<String, Object>> getMarsSubclientValues();
}
