package com.maritz.culturenext.awardtype.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.awardtype.dao.AwardTypeDao;
import com.maritz.culturenext.enums.LookupCodeEnum;

@Component
public class AwardTypeDaoImpl extends AbstractDaoImpl implements AwardTypeDao {
    
    private static final String PAX_ID_PARAM = "paxId";
    private static final String PAYOUT_ITEM_ID_PARAM = "payoutItemId";
    private static final String PAYOUT_TYPE_PARAM = "payoutType";
    
    private static final String SP_TO_DELETE_AWARD_TYPE = 
            "EXEC component.UP_DELETE_AWARD_TYPE :payoutItemId";

    private static final String SP_GET_AWARD_TYPE_BY_PAYOUT_TYPE =
            "EXEC component.UP_GET_AWARD_TYPE_BY_PAYOUT_TYPE :payoutType";

    private static final String QUERY_GET_ALL_AWARD_TYPES =
            "SELECT " +
                "LOOKUP_PAYOUT.ID AS ID, " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.PAYOUT_TYPE.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.DISPLAY_NAME.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.SHORT_DESC.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.LONG_DESC.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.AWARD_VALUE.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.ABS.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.SUB_CLIENT.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.PROJECT_NUMBER.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.SUB_PROJECT_NUMBER.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.CATALOG_URL.name() + ", " +
                "LOOKUP_PAYOUT." + LookupCodeEnum.CATALOG_DEEP_LINK.name() + ", " +
                "CASE " +
                    "WHEN ( " +
                        "NOT EXISTS " +
                        "( " +
                            "SELECT NULL " +
                            "FROM component.PROGRAM_MISC AS PM " +
                            "WHERE PM.MISC_DATA = LOOKUP_PAYOUT.PAYOUT_TYPE " +
                                "AND PM.VF_NAME = 'SELECTED_AWARD_TYPE' " +
                        ") " +
                        "AND NOT EXISTS " +
                        "( " +
                            "SELECT NULL " +
                            "FROM component.EARNINGS AS E " +
                            "WHERE E.EARNINGS_TYPE_CODE = LOOKUP_PAYOUT.PAYOUT_TYPE " +
                        ") " +
                    ") " +
                    "THEN 'FALSE' " +
                    "ELSE 'TRUE' " +
                "END AS IS_USED " +
            "FROM " +
                "( " +
                    "SELECT DISTINCT(PI.PAYOUT_ITEM_ID) AS ID, " +
                        LookupCodeEnum.PAYOUT_TYPE.name() + ", " +
                        LookupCodeEnum.DISPLAY_NAME.name() + ", " +
                        LookupCodeEnum.SHORT_DESC.name() + ", " +
                        LookupCodeEnum.LONG_DESC.name() + ", " +
                        LookupCodeEnum.AWARD_VALUE.name() + ", " +
                        LookupCodeEnum.ABS.name() + ", " +
                        LookupCodeEnum.SUB_CLIENT.name() + ", " +
                        "LOOKUP_PIVOTED." + LookupCodeEnum.PROJECT_NUMBER + ", " +
                        "LOOKUP_PIVOTED." + LookupCodeEnum.SUB_PROJECT_NUMBER.name() + ", " +
                        LookupCodeEnum.CATALOG_URL.name() + ", " +
                        LookupCodeEnum.CATALOG_DEEP_LINK.name() + " " +
                    "FROM " +
                    "( " +
                    "SELECT LOOKUP_CATEGORY_CODE, " +
                        "LOOKUP_CODE, " +
                        "LOOKUP_DESC " +
                    "FROM component.LOOKUP " +
                    "WHERE LOOKUP_TYPE_CODE = 'AWARD_TYPE' " +
                    ") AS LOOKUP_TO_BE_PIVOTED PIVOT (MAX(LOOKUP_DESC) FOR LOOKUP_CODE IN (PAYOUT_TYPE, " +
                    "DISPLAY_NAME, SHORT_DESC, LONG_DESC, AWARD_VALUE, ABS, SUB_CLIENT, PROJECT_NUMBER, " +
                    "SUB_PROJECT_NUMBER, CATALOG_URL, CATALOG_DEEP_LINK) ) AS LOOKUP_PIVOTED " +
                "INNER JOIN component.PAYOUT_ITEM PI " +
                "ON PI.PAYOUT_ITEM_NAME = LOOKUP_PIVOTED.PAYOUT_TYPE " +
            ") AS LOOKUP_PAYOUT " +
            "ORDER BY LOOKUP_PAYOUT.DISPLAY_NAME";
    
    private static final String QUERY_GET_AWARD_TYPE_ELIGIBILITY_BY_PAX_ID =
            "SELECT " + 
            "DISTINCT(PI.PAYOUT_ITEM_ID) AS AWARD_TYPE_ID, " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.LOOKUP_DISPLAY_NAME_ID.name() + ", " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.PAYOUT_TYPE.name() + ", " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.DISPLAY_NAME.name() + ", " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.ABS.name() + ", " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.CATALOG_URL.name() + ", " +
            "LOOKUP_PIVOTED."+LookupCodeEnum.CATALOG_DEEP_LINK.name() + ", " +
            "CASE " + 
                "WHEN (PAX_PAYOUT.PAYOUT_TYPE_FOR_PAX IS NULL AND PAX_ISSUED_PAYOUT.PAYOUT_TYPE IS NULL) " + 
                "THEN 'FALSE' " + 
                "ELSE 'TRUE' " + 
            "END AS ELIGIBLE_TO_RECEIVE " +
            "FROM ( "+
                 "SELECT " + 
                 "LOOKUP_CATEGORY_CODE, " + 
                 "LOOKUP_CODE, "+
                 "LOOKUP_DESC " + 
                 "FROM component.LOOKUP " + 
            "WHERE LOOKUP_TYPE_CODE ='AWARD_TYPE'"+
            "union all  select LOOKUP_CATEGORY_CODE as 'LOOKUP_CATEGORY_CODE','LOOKUP_DISPLAY_NAME_ID' as 'LOOKUP_CODE',CAST(id as varchar(10)) as 'LOCKUP_DESC' from component.LOOKUP where LOOKUP_CODE = 'DISPLAY_NAME'"+                 
            ") AS LOOKUP_TO_BE_PIVOTED " + 
            "PIVOT (  "+
                    "MAX(LOOKUP_DESC) "+
                    "FOR LOOKUP_CODE IN " + 
                    "( " +
                        LookupCodeEnum.PAYOUT_TYPE.name() + ", " + 
                        LookupCodeEnum.DISPLAY_NAME.name() + ", " +
                        LookupCodeEnum.ABS.name() + ", " +
                        LookupCodeEnum.CATALOG_URL.name() + ", " +
                        LookupCodeEnum.CATALOG_DEEP_LINK.name() + ", " +
                        LookupCodeEnum.LOOKUP_DISPLAY_NAME_ID.name() +
                    " ) " +
            ") AS LOOKUP_PIVOTED  " + 
            "INNER JOIN component.PAYOUT_ITEM PI ON PI.PAYOUT_ITEM_NAME = LOOKUP_PIVOTED.PAYOUT_TYPE " + 
            "LEFT JOIN component.PROGRAM_MISC PM ON PM.MISC_DATA = PAYOUT_TYPE AND PM.VF_NAME='SELECTED_AWARD_TYPE' " + 
            "LEFT JOIN component.EARNINGS E ON E.EARNINGS_TYPE_CODE = LOOKUP_PIVOTED.PAYOUT_TYPE " + 
            "LEFT JOIN ( " + 
                            "SELECT " + 
                                "DISTINCT PM.MISC_DATA AS PAYOUT_TYPE_FOR_PAX " + 
                            "FROM component.ACL  " + 
                            "INNER JOIN component.ROLE ON ROLE.ROLE_ID = ACL.ROLE_ID " + 
                            "INNER JOIN component.PROGRAM P ON P.PROGRAM_ID = ACL.TARGET_ID  " + 
                            "INNER JOIN component.PROGRAM_MISC PM  ON P.PROGRAM_ID = PM.PROGRAM_ID AND PM.VF_NAME='SELECTED_AWARD_TYPE' " + 
                            "WHERE ROLE.ROLE_NAME IN ('Program Receiver') AND ACL.TARGET_TABLE = 'PROGRAM'  AND P.STATUS_TYPE_CODE = 'ACTIVE' " + 
                            "AND P.FROM_DATE <= GETDATE()  AND (  P.THRU_DATE IS NULL OR  P.THRU_DATE >= GETDATE() )  " + 
                            "AND (  (  ACL.SUBJECT_TABLE = 'GROUPS' AND  ACL.SUBJECT_ID IN " + 
                            "(SELECT GROUP_ID FROM component.GROUPS_PAX WHERE PAX_ID = :" + PAX_ID_PARAM + " ) )  OR " + 
                            "( ACL.SUBJECT_TABLE = 'PAX' AND  ACL.SUBJECT_ID = :" + PAX_ID_PARAM + ")  ) " + 
                        ") AS PAX_PAYOUT ON PAX_PAYOUT.PAYOUT_TYPE_FOR_PAX = LOOKUP_PIVOTED.PAYOUT_TYPE " + 
            "LEFT JOIN ( " + 
                            "SELECT DISTINCT (PAYOUT_TYPE_CODE) AS PAYOUT_TYPE " + 
                            "FROM component.PAYOUT " + 
                            "WHERE STATUS_TYPE_CODE='ISSUED' " + 
                            "AND PAX_ID = :" + PAX_ID_PARAM + " "+ 
                        ") AS PAX_ISSUED_PAYOUT ON PAX_ISSUED_PAYOUT.PAYOUT_TYPE = LOOKUP_PIVOTED.PAYOUT_TYPE " + 
            "ORDER BY LOOKUP_PIVOTED.DISPLAY_NAME ";

    private static final String QUERY_GET_MARS_PRODUCTS = 
            "SELECT CONCAT(pi.PRODUCT_CODE, '=', pi.PRODUCT_CODE) AS 'PRODUCT_CODE' " +
            "FROM component.PAYOUT_ITEM pi " +
            "JOIN component.LOOKUP l " +
                "ON l.LOOKUP_CATEGORY_CODE = pi.PAYOUT_ITEM_NAME " +
                "AND l.LOOKUP_TYPE_CODE = 'AWARD_TYPE' " +
                "AND l.LOOKUP_CODE = 'ABS' " +
                "AND l.LOOKUP_DESC = 'TRUE'";
    
    private static final String QUERY_GET_MARS_SUBCLIENTS = 
            "SELECT CONCAT(pi.PRODUCT_CODE, '=', pi.SUBCLIENT) AS 'SUBCLIENT' " +
            "FROM component.PAYOUT_ITEM pi " +
            "JOIN component.LOOKUP l " +
                "ON l.LOOKUP_CATEGORY_CODE = pi.PAYOUT_ITEM_NAME " +
                "AND l.LOOKUP_TYPE_CODE = 'AWARD_TYPE' " +
                "AND l.LOOKUP_CODE = 'ABS' " +
                "AND l.LOOKUP_DESC = 'TRUE'";
    
    @Override
    public List<Map<String, Object>> getAllAwardTypes() {
        return namedParameterJdbcTemplate.query(QUERY_GET_ALL_AWARD_TYPES, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getAwardTypeByPayoutType(String payoutType) {

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(PAYOUT_TYPE_PARAM, payoutType);

        return namedParameterJdbcTemplate.query(
                SP_GET_AWARD_TYPE_BY_PAYOUT_TYPE, params, new CamelCaseMapRowMapper());

    }

    @Override
    public void deleteAwardType(Long id) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAYOUT_ITEM_ID_PARAM, id);
        
        namedParameterJdbcTemplate.query(SP_TO_DELETE_AWARD_TYPE, params, new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getMarsProductValues() {
        MapSqlParameterSource params = new MapSqlParameterSource();
    
        return namedParameterJdbcTemplate.query(QUERY_GET_MARS_PRODUCTS, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getMarsSubclientValues() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        
        return namedParameterJdbcTemplate.query(QUERY_GET_MARS_SUBCLIENTS, params, new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getAwardTypeEligibilityToReceiveList(Long paxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PARAM, paxId);
        return namedParameterJdbcTemplate.query(QUERY_GET_AWARD_TYPE_ELIGIBILITY_BY_PAX_ID, params,
                new CamelCaseMapRowMapper());
    }
}
