package com.maritz.culturenext.awardtype.service;

import java.util.List;

import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeEligibilityToReceiveDTO;

public interface AwardTypeService {

    /**
     * List all configured award types
     */
    public List<AwardTypeDTO> getAwardTypesList();

    /**
     * Get award type by payout type
     */
    public AwardTypeDTO getAwardTypeByPayoutType(String payoutType);
    
    /**
     * Delete an existing award type. Can only be deleted 
     * if isUsed = false (If the award type is not currently tied to a program and has never been used)
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/163250195/Multiple+Award+Types+Configuration
     * 
     * @param id - identifier of the award type to delete
     * 
     */
    public void deleteAwardType(Long id);
    
    /**
     * Inserts a new Award Type. Validates that PayoutItem name does not exist in the database. 
     * @param awardTypeDTO award type with empty id
     * @return awardTypeDTO with id set.
     * 
     *  https://maritz.atlassian.net/wiki/spaces/M365/pages/163250195/Multiple+Award+Types+Configuration
     */
    public List<AwardTypeDTO> insertAwardType(AwardTypeDTO awardTypeDTO);

    /**
     * Updates an existing Award Type.
     * @param awardTypeDTO award type
     * @return awardTypeDTO with id set.
     * 
     *  https://maritz.atlassian.net/wiki/spaces/M365/pages/163250195/Multiple+Award+Types+Configuration
     */
    public List<AwardTypeDTO> updateAwardType(AwardTypeDTO awardTypeDto);
    
    /**
    *  Return a user's eligibility to receive the award types configured at the site level.
    *  @param paxId - user's id
    *
    *  https://maritz.atlassian.net/wiki/spaces/M365/pages/235372545/User+s+Eligibility+to+Receive+Awards
    */
    public List<AwardTypeEligibilityToReceiveDTO> getAwardTypeEligibilityToReceiveList(String languageCode, Long paxId);
}
