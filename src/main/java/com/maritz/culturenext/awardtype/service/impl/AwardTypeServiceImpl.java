package com.maritz.culturenext.awardtype.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.CardType;
import com.maritz.core.jpa.entity.EarningsType;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.LookupCategory;
import com.maritz.core.jpa.entity.PayoutItem;
import com.maritz.core.jpa.entity.PayoutType;
import com.maritz.core.jpa.entity.PayoutVendor;
import com.maritz.core.jpa.entity.Project;
import com.maritz.core.jpa.entity.SubProject;
import com.maritz.core.jpa.repository.PayoutVendorRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.awardtype.constants.AwardTypeConstants;
import com.maritz.culturenext.awardtype.dao.AwardTypeDao;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeDetailsDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeEligibilityToReceiveDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.LookupCodeEnum;
import com.maritz.culturenext.enums.LookupTypeEnum;
import com.maritz.culturenext.enums.PayoutVendorEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.util.LocaleUtil;
import com.maritz.culturenext.util.TranslationUtil;

@Component
public class AwardTypeServiceImpl implements AwardTypeService {

    @Inject private ApplicationDataService applicationDataService;
    @Inject private AwardTypeDao awardTypeDao;
    @Inject private ConcentrixDao<CardType> cardTypeDao;
    @Inject private ConcentrixDao<EarningsType> earningsTypeDao;
    @Inject private ConcentrixDao<Lookup> lookupDao;
    @Inject private ConcentrixDao<LookupCategory> lookupCategoryDao;
    @Inject private ConcentrixDao<PayoutItem> payoutItemDao;
    @Inject private ConcentrixDao<PayoutType> payoutTypeDao;
    @Inject private ConcentrixDao<Project> projectDao;
    @Inject private ConcentrixDao<SubProject> subProjectDao;
    @Inject private PayoutVendorRepository payoutVendorRepository;
    @Inject private TranslationUtil translationUtil;
    
    private static final String EY_AWARD_TYPE = "EYD";
    
    @Override
    public List<AwardTypeDTO> getAwardTypesList() {
        return populateAwardTypes(awardTypeDao.getAllAwardTypes());
    }

    @Override
    public AwardTypeDTO getAwardTypeByPayoutType(String payoutType) {
        return populateAwardType(awardTypeDao.getAwardTypeByPayoutType(payoutType).get(0));
    }


    @Override
    public void deleteAwardType(Long id) {
        awardTypeDao.deleteAwardType(id);
    }

    @Override
    public List<AwardTypeDTO> insertAwardType(AwardTypeDTO awardTypeDto) {        
        handleCreateUpdateAwardType(awardTypeDto);
        return getAwardTypesList();
    }
    
    @Override
    public List<AwardTypeDTO> updateAwardType(AwardTypeDTO awardTypeDto) {
        handleCreateUpdateAwardType(awardTypeDto);
        return getAwardTypesList();
    }
    
    /**
     * Map a result set to a list of AwardTypeDTO's
     * @param nodes - result set containing award type data
     * @return List<AwardTypeDTO>
     */
    private List<AwardTypeDTO> populateAwardTypes(List<Map<String, Object>> nodes) {
        
        List<AwardTypeDTO> resultsList = new ArrayList<AwardTypeDTO>();
        
        for(Map<String, Object> node : nodes) {
            
            AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
            AwardTypeDetailsDTO awardTypesDetailsDTO  = new AwardTypeDetailsDTO();

            ObjectUtils.mapToObject(node, awardTypeDTO);
            ObjectUtils.mapToObject(node, awardTypesDetailsDTO);
            
            awardTypeDTO.setDetails(awardTypesDetailsDTO);
            
            resultsList.add(awardTypeDTO);
        }
        
        return resultsList;
    }

    /**
     * Map a result set to an AwardTypeDTO
     * @param node - result set containing award type data
     * @return AwardTypeDTO
     */
    private AwardTypeDTO populateAwardType(Map<String, Object> node) {

        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        AwardTypeDetailsDTO awardTypeDetailsDTO = new AwardTypeDetailsDTO();

        ObjectUtils.mapToObject(node, awardTypeDTO);
        ObjectUtils.mapToObject(node, awardTypeDetailsDTO);

        awardTypeDTO.setDetails(awardTypeDetailsDTO);

        return awardTypeDTO;
    }

    /**
     * Helper method called by POST and PUT for award types
     * This will first validate the award type (or list of award types), then do the saving.
     * @param AwardTypeDto - The award type to be created or updated
     */
    private void handleCreateUpdateAwardType(AwardTypeDTO awardTypeDto) {
        validateAwardType(awardTypeDto);
        saveAwardType(awardTypeDto);    
    }
    
    /**
     * Validates the award type being inserted or updated and throws any errors
     * @param awardTypeDto - award type to validate
     */
    public void validateAwardType(AwardTypeDTO awardTypeDto) {
        ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        boolean isUpdate = awardTypeDto.getId() != null;
        
        if (awardTypeDto.getAbs() == null) {
            errors.add(AwardTypeConstants.MISSING_TYPE_MESSAGE);
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
        
        //For non-ABS award types, dynamically create the payoutType
        String payoutTypeCode = null;
        if (!awardTypeDto.getAbs()) {
            String displayName = awardTypeDto.getDisplayName();
            if (displayName == null) {
                errors.add(AwardTypeConstants.MISSING_DISPLAY_NAME_MESSAGE);
            } else {
                // PayoutTypeCode should not be updated for Non-ABS once its generated. ie, It should only be generated from the displayName in create mode 
                if (!isUpdate) {
                    payoutTypeCode = displayName.replaceAll(ProjectConstants.SPACE, ProjectConstants.UNDERSCORE).toUpperCase();
                } 
                if (awardTypeDto.getDetails() == null) {
                    if (isUpdate) {
                        errors.add(AwardTypeConstants.MISSING_DETAILS_MESSAGE);
                    } else {
                        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
                        details.setPayoutType(payoutTypeCode);
                        awardTypeDto.setDetails(details);
                    }
                } else {
                    if (isUpdate) {
                        payoutTypeCode = awardTypeDto.getDetails().getPayoutType();
                    } else {
                        awardTypeDto.getDetails().setPayoutType(payoutTypeCode);
                    }
                }
            }
        } else {
            if (awardTypeDto.getDetails() == null) {
                errors.add(AwardTypeConstants.MISSING_DETAILS_MESSAGE);
            } else {
                payoutTypeCode = awardTypeDto.getDetails().getPayoutType();
            }
        }
        
        if (payoutTypeCode == null) {
            errors.add(AwardTypeConstants.MISSING_PAYOUT_TYPE_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);
        
        // Check if a PAYOUT_ITEM record of this type already exists
        if (payoutItemDao.findBy().where(ProjectConstants.PAYOUT_ITEM_NAME).eq(payoutTypeCode).exists()) {
            if (!isUpdate){
                errors.add(AwardTypeConstants.AWARD_TYPE_EXISTS_MESSAGE);
            } 
        } else {
            if (isUpdate){
                errors.add(AwardTypeConstants.AWARD_TYPE_DOES_NOT_EXIST);
            }
        }
        
        // Validation for ABS types only
        if (awardTypeDto.getAbs()) {
            
            // Required fields
            if (awardTypeDto.getAwardValue() == null) {
                errors.add(AwardTypeConstants.MISSING_AWARD_VALUE_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getSubClient() == null) {
                errors.add(AwardTypeConstants.MISSING_SUBCLIENT_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getProjectNumber() == null) {
                errors.add(AwardTypeConstants.MISSING_PROJECT_NUMBER_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getSubProjectNumber() == null) {
                errors.add(AwardTypeConstants.MISSING_SUB_PROJECT_NUMBER_MESSAGE);
            }
            
            ErrorMessageException.throwIfHasErrors(errors);
            
            // Max lengths
            if (payoutTypeCode.trim().length() > AwardTypeConstants.ABS_PRODUCT_CODE_LENGTH) {
                errors.add(AwardTypeConstants.PAYOUT_TYPE_MAX_LENGTH_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getSubClient().trim().length() > AwardTypeConstants.ABS_SUBCLIENT_LENGTH) {
                errors.add(AwardTypeConstants.SUBCLIENT_MAX_LENGTH_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getProjectNumber().trim().length() > 
                    AwardTypeConstants.ABS_PROJECT_NUMBER_LENGTH) {
                errors.add(AwardTypeConstants.PROJET_NUMBER_MAX_LENGTH_MESSAGE);
            }
            
            if (awardTypeDto.getDetails().getSubProjectNumber().trim().length() > 
                    AwardTypeConstants.ABS_SUB_PROJECT_NUMBER_LENGTH) {
                errors.add(AwardTypeConstants.SUB_PROJECT_NUMBER_MAX_LENGTH_MESSAGE);
            }
        }
        
        ErrorMessageException.throwIfHasErrors(errors);
    }
    
    /**
     * Inserts or updates a single award type
     * @param awardTypeDto - award type being inserted/updated
     */
    @Transactional
    private void saveAwardType(AwardTypeDTO awardTypeDto) {
        
        String payoutTypeCode = awardTypeDto.getDetails().getPayoutType().toUpperCase();
        String displayName = awardTypeDto.getDisplayName();
        boolean isUpdate = awardTypeDto.getId() != null;
        
        // For non-ABS award types, default the awardValue to 1 if it is null
        if (!awardTypeDto.getAbs() && awardTypeDto.getAwardValue() == null) {
            awardTypeDto.setAwardValue(1D);
        }
        
        //Save/update the CARD_TYPE. Need to do this first due to a FK constraint on PAYOUT_ITEM
        if (awardTypeDto.getAbs()) {
            CardType cardType = null;
            if (isUpdate) {
                cardType = cardTypeDao.findBy().where(ProjectConstants.CARD_TYPE_CODE).eq(payoutTypeCode).findOne();
            }
            if (cardType == null) {
                cardType = new CardType();
            }
            if (displayName.equalsIgnoreCase(AwardTypeConstants.DEFAULT_SERVICE_ANNIVERSARY_CARD_TYPE_DESC)
                    || displayName.equalsIgnoreCase(AwardTypeConstants.DEFAULT_SERVICE_ANNIVERSARY_CARD_TYPE_DESC_RUN_TOGETHER)) {
                cardType.setCardTypeCode(payoutTypeCode)
                        .setCardTypeDesc(AwardTypeConstants.DEFAULT_SERVICE_ANNIVERSARY_CARD_TYPE_DESC)
                        .setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
                cardTypeDao.save(cardType);
            }else {
                cardType.setCardTypeCode(payoutTypeCode)
                        .setCardTypeDesc(AwardTypeConstants.DEFAULT_CARD_TYPE_DESC)
                        .setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
                cardTypeDao.save(cardType);
            }
        }
        
        //Fetch PAYOUT_VENDOR_ID (FK on PAYOUT_ITEM)
        Long payoutVendorMars = null;
        Long payoutVendorAbs = null;
        Long payoutVendorThirdParty = null;
        
        List<PayoutVendor> payoutVendorList = payoutVendorRepository.findAll();
        for (PayoutVendor payoutVendor : payoutVendorList) {
            if (PayoutVendorEnum.MARS.getName().equals(payoutVendor.getPayoutVendorName())) {
                payoutVendorMars = payoutVendor.getPayoutVendorId();
            } else if (PayoutVendorEnum.ABS.getName().equals(payoutVendor.getPayoutVendorName())) {
                payoutVendorAbs = payoutVendor.getPayoutVendorId();
            } else if (PayoutVendorEnum.THIRD_PARTY.getName().equals(payoutVendor.getPayoutVendorName())) {
                payoutVendorThirdParty = payoutVendor.getPayoutVendorId();
            }
        }

        ApplicationDataDTO absProducts = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_ABS_VENDOR_PRODUCTS);
        
        //Grab the correct payoutVendorId to use on the PAYOUT_ITEM table.
        Long payoutVendorId;
        if (!awardTypeDto.getAbs()) {
            //non-ABS award types always get "3rd Party Items"
            payoutVendorId = payoutVendorThirdParty;
        } else if (absProducts!= null && absProducts.getValue().contains(payoutTypeCode)) {
            //The productCode "EYD" needs to map to "ABS" (not issued via MARS)
            payoutVendorId = payoutVendorAbs;
        } else {
            //All other ABS types get mapped to "MARS" for real time issuance
            payoutVendorId = payoutVendorMars;
        }
        
        //Create/Update PAYOUT_ITEM record
        PayoutItem payoutItem = null;
        if (isUpdate) {
            payoutItem =  payoutItemDao.findBy().where(ProjectConstants.PAYOUT_ITEM_NAME).eq(payoutTypeCode).findOne();
        }
        if (payoutItem == null) {
            payoutItem = new PayoutItem();
        }
        payoutItem.setPayoutItemName(payoutTypeCode);
        payoutItem.setPayoutItemDesc(awardTypeDto.getDisplayName());
        payoutItem.setPayoutItemAmount(awardTypeDto.getAwardValue());
        payoutItem.setProductCode(payoutTypeCode);
        payoutItem.setPayoutVendorId(payoutVendorId);
        if (awardTypeDto.getDetails().getSubClient() != null) {
            payoutItem.setSubclient(awardTypeDto.getDetails().getSubClient());
        }
        if (awardTypeDto.getAbs()) {
            payoutItem.setCardTypeCode(payoutTypeCode);
        }
        payoutItemDao.save(payoutItem);
        
        //Create PAYOUT_TYPE and EARNINGS_TYPE records
        if (!isUpdate) {
            PayoutType payoutType = new PayoutType();
            payoutType.setPayoutTypeCode(payoutTypeCode);
            payoutType.setPayoutTypeDesc(awardTypeDto.getDisplayName());
            payoutType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
            payoutTypeDao.save(payoutType);
            EarningsType earningsType = new EarningsType();
            earningsType.setEarningsTypeCode(payoutTypeCode);
            earningsType.setEarningsTypeName(payoutTypeCode); // Payout job matches EARNINGS_TYPE to PAYOUT_ITEM based on name
            earningsType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
            earningsTypeDao.save(earningsType);
        }

        //Create PROJECT and SUB_PROJECT records
        if (awardTypeDto.getDetails().getProjectNumber() != null 
                && awardTypeDto.getDetails().getSubProjectNumber() != null) {

            Project project = projectDao.findBy().where(ProjectConstants.PROJECT_NUMBER)
                    .eq(awardTypeDto.getDetails().getProjectNumber()).findOne();
            if (project == null) {
                project = new Project();
                project.setProjectNumber(awardTypeDto.getDetails().getProjectNumber());
                projectDao.save(project);
            }

            SubProject subProject = subProjectDao.findBy()
                    .where(ProjectConstants.PROJECT_ID).eq(project.getId())
                    .and(ProjectConstants.SUB_PROJECT_NUMBER).eq(awardTypeDto.getDetails().getSubProjectNumber()).findOne();
            if (subProject == null) {
                subProject = new SubProject();
                subProject.setProjectId(project.getId());
                subProject.setSubProjectNumber(awardTypeDto.getDetails().getSubProjectNumber());
                subProjectDao.save(subProject);
            }
        }
        
        //Create LOOKUP_CATEGORY record
        if (!isUpdate ) {
            LookupCategory lookupCategory = new LookupCategory();
            lookupCategory.setLookupCategoryCode(payoutTypeCode);
            lookupCategoryDao.save(lookupCategory);
        }

        //LOOKUP - PAYOUT_TYPE
        Lookup lookupPayoutType = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(),payoutTypeCode, 
                LookupCodeEnum.PAYOUT_TYPE.name(), awardTypeDto.getDetails().getPayoutType());
        lookupDao.save(lookupPayoutType);
        // Create translation data
        translationUtil.saveTranslationData(
                TableName.LOOKUP.name(), TranslationConstants.COLUMN_NAME_LOOKUP_DESC, 
                lookupPayoutType.getId(), StatusTypeCode.ACTIVE.name());
        
        //LOOKUP - DISPLAY_NAME
        Lookup lookupDisplayName = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                payoutTypeCode, LookupCodeEnum.DISPLAY_NAME.name(), awardTypeDto.getDisplayName());
        lookupDao.save(lookupDisplayName);
        
        //LOOKUP - LONG_DESC
        if (awardTypeDto.getLongDesc() != null) {
            Lookup lookupLongDesc = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                    payoutTypeCode, LookupCodeEnum.LONG_DESC.name(), awardTypeDto.getLongDesc());
            lookupDao.save(lookupLongDesc);
            // Create translation data
                translationUtil.saveTranslationData(
                        TableName.LOOKUP.name(), TranslationConstants.COLUMN_NAME_LOOKUP_DESC, 
                        lookupLongDesc.getId(), StatusTypeCode.ACTIVE.name());
        }
    
        //LOOKUP - AWARD_VALUE
        Lookup lookupAwardValue = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                payoutTypeCode, LookupCodeEnum.AWARD_VALUE.name(), awardTypeDto.getAwardValue().toString());
        lookupDao.save(lookupAwardValue);
        
        //LOOKUP - ABS
        Lookup lookupAbs = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), payoutTypeCode, 
                LookupCodeEnum.ABS.name(), awardTypeDto.getAbs().toString());
        lookupDao.save(lookupAbs);
        
        //LOOKUP - SUB_CLIENT
        if (awardTypeDto.getDetails().getSubClient() != null) {    
            Lookup lookupSubClient = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), payoutTypeCode, 
                    LookupCodeEnum.SUB_CLIENT.name(), awardTypeDto.getDetails().getSubClient());
            lookupDao.save(lookupSubClient);
        }
        
        //LOOKUP - PROJECT_NUMBER
        if (awardTypeDto.getDetails().getProjectNumber() != null) {
            Lookup lookupProjectNumber = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                    payoutTypeCode, LookupCodeEnum.PROJECT_NUMBER.name(), awardTypeDto.getDetails().getProjectNumber());
            lookupDao.save(lookupProjectNumber);
        }
        
        //LOOKUP - SUB_PROJECT_NUMBER
        if (awardTypeDto.getDetails().getSubProjectNumber() != null) {
            Lookup lookupSubProjectNumber = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                    payoutTypeCode, LookupCodeEnum.SUB_PROJECT_NUMBER.name(), 
                    awardTypeDto.getDetails().getSubProjectNumber());
            lookupDao.save(lookupSubProjectNumber);
        }
    
        //LOOKUP - CATALOG_URL
        Lookup lookupCatalogUrl = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), 
                payoutTypeCode, LookupCodeEnum.CATALOG_URL.name(), awardTypeDto.getDetails().getCatalogUrl());
        if (awardTypeDto.getDetails().getCatalogUrl() != null) {
            lookupDao.save(lookupCatalogUrl);
        } else if (isUpdate) {
            //Delete the LOOKUP record if the update sends us a null value
            if (lookupCatalogUrl.getId() != null) {
                lookupDao.delete(lookupCatalogUrl);
            }
        }
        
        Lookup lookupDeepLinkUrl = getLookUpItem(isUpdate, LookupTypeEnum.AWARD_TYPE.name(), payoutTypeCode, 
                LookupCodeEnum.CATALOG_DEEP_LINK.name(), awardTypeDto.getDetails().getCatalogDeepLink());
        if (awardTypeDto.getDetails().getCatalogDeepLink() != null) {
            lookupDao.save(lookupDeepLinkUrl);
        } else if (isUpdate) {
            //Delete the LOOKUP record if the update sends us a null value
            if (lookupDeepLinkUrl.getId() != null) {
                lookupDao.delete(lookupDeepLinkUrl);
            }
        }
        
        //Update the properties for "mars.product" and "mars.subclient"
        if (awardTypeDto.getAbs()) {
            updateMarsProperties(awardTypeDto);
        }
    }
    
    protected Lookup getLookUpItem(boolean isUpdate, String lookupTypeCode, String lookupCategoryCode, 
        String lookupCode, String lookupDescription) {
        Lookup lookup = null;
        if (isUpdate) {
            lookup = lookupDao.findBy().where(ProjectConstants.LOOKUP_CATEGORY_CODE).eq(lookupCategoryCode)
                    .and(ProjectConstants.LOOKUP_TYPE_CODE).eq(lookupTypeCode)
                    .and(ProjectConstants.LOOKUP_CODE).eq(lookupCode).findOne();
        }
        if (lookup == null) {
            lookup = new Lookup();
        }
        lookup.setLookupTypeCode(lookupTypeCode);
        lookup.setLookupCategoryCode(lookupCategoryCode);
        lookup.setLookupCode(lookupCode);
        lookup.setLookupDesc(lookupDescription);
        return lookup;
    }
    
    /**
     * Updates the APPLICATION_DATA properties used by MARS (mars.product and mars.subclient)
     * @param awardTypeDto
     */
    private void updateMarsProperties(AwardTypeDTO awardTypeDto) {
            
        //Update the "mars.product" property
        List<Map<String, Object>> productNodes = awardTypeDao.getMarsProductValues();
        String productValue = "";
        Boolean firstRecord = true;
        if (productNodes != null) {
            for (Map<String, Object> product : productNodes) {
                if (!firstRecord) {
                    productValue += ProjectConstants.COMMA_DELIM;
                }
                productValue += (String) product.get(ProjectConstants.PRODUCT_CODE);
                firstRecord = false;
            }
        }
        
        ApplicationDataDTO marsProduct = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_MARS_PRODUCTS);
        if (marsProduct == null) {
            marsProduct = new ApplicationDataDTO();
            marsProduct.setKey(ApplicationDataConstants.KEY_NAME_MARS_PRODUCTS);
            marsProduct.setValue(productValue);
        } else {
            marsProduct.setValue(productValue);
        }
        applicationDataService.setApplicationData(marsProduct);

        //Update the "mars.subclient" property
        List<Map<String, Object>> subclientNodes = awardTypeDao.getMarsSubclientValues();
        String subclientValue = "";
        firstRecord = true;
        if (subclientNodes != null) {
            for (Map<String, Object> subclient : subclientNodes) {
                if (!firstRecord) {
                    subclientValue += ProjectConstants.COMMA_DELIM;
                }
                subclientValue += (String) subclient.get(ProjectConstants.SUBCLIENT);
                firstRecord = false;
            }
        }
        
        ApplicationDataDTO marsSubclient = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_MARS_SUBCLIENT);
        if (marsSubclient == null) {
            marsSubclient = new ApplicationDataDTO();
            marsSubclient.setKey(ApplicationDataConstants.KEY_NAME_MARS_SUBCLIENT);
            marsSubclient.setValue(subclientValue);
        } else {
            marsSubclient.setValue(subclientValue);
        }
        applicationDataService.setApplicationData(marsSubclient);
    }
    
    @Override
    public List<AwardTypeEligibilityToReceiveDTO> getAwardTypeEligibilityToReceiveList(String languageCode,Long paxId) {
        return populateAwardTypeEligibilityToReceiveDTO(languageCode,awardTypeDao.getAwardTypeEligibilityToReceiveList(paxId));
    }

    /**
     * Map a result set to a list of AwardTypeEligibilityToReceiveDTO's
     * @param nodes - result set containing award type data
     * @return List<AwardTypeEligibilityToReceiveDTOO>
     */
    private List<AwardTypeEligibilityToReceiveDTO> populateAwardTypeEligibilityToReceiveDTO(String languageCode, List<Map<String, Object>> nodes) {
        
        List<AwardTypeEligibilityToReceiveDTO> resultsList = new ArrayList<AwardTypeEligibilityToReceiveDTO>();
        List<AwardTypeEligibilityToReceiveDTO> resultsListTranslated = new ArrayList<AwardTypeEligibilityToReceiveDTO>();
                
        
        for(Map<String, Object> node : nodes) {
            AwardTypeEligibilityToReceiveDTO awardTypeEligibilityDTO = new AwardTypeEligibilityToReceiveDTO();
            ObjectUtils.mapToObject(node, awardTypeEligibilityDTO);
            resultsList.add(awardTypeEligibilityDTO);
        }
        String formattedLanguageCode = LocaleUtil.formatLocaleCode(languageCode);
        if (languageCode == null || ProjectConstants.DEFAULT_LOCALE_CODE.equals(formattedLanguageCode)) {                
        	return resultsList;
        }else {
            //Call the method for translate the displayName properties from list of AwardTypeEligibilityToReceiveDTO
            Map<Long, String> translations = fetchTranslations(formattedLanguageCode, resultsList);
            
	        for(AwardTypeEligibilityToReceiveDTO awardTypeElement : resultsList) {        		       	        	
	        	//Set the translated value into Object awardTypeEligibilityDTO, info is getting from Map "translations"
	        	awardTypeElement.setDisplayName(translations.get(awardTypeElement.getLookupDisplayNameId()));
	        	resultsListTranslated.add(awardTypeElement);
	        }	    
	        return resultsListTranslated;
        }
        	        	    
    }
        
    private Map<Long, String> fetchTranslations(String formattedLanguageCode, List<AwardTypeEligibilityToReceiveDTO> entries) {
        

        Map<Long, String> translations = null;
        if (entries != null) {
            List<Long> rowIds = new ArrayList<>();
            Map<Long, String> defaultMap = new HashMap<>();
            for (AwardTypeEligibilityToReceiveDTO entry : entries) {
                rowIds.add(entry.getLookupDisplayNameId());
                defaultMap.put(entry.getLookupDisplayNameId(), entry.getDisplayName());
            }
            
            translations = translationUtil.getTranslationsForListOfIds(formattedLanguageCode, 
                    TableName.LOOKUP.name(), TranslationConstants.COLUMN_NAME_LOOKUP_DESC, 
                    rowIds, defaultMap);
        }
        return translations;
    }
}
