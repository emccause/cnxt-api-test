package com.maritz.culturenext.awardtype.dto;

public class AwardTypeDetailsDTO {

    private String payoutType;
    private String subClient;
    private String projectNumber;
    private String subProjectNumber;
    private String catalogUrl;
    private String catalogDeepLink;
    
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getSubClient() {
        return subClient;
    }
    public void setSubClient(String subClient) {
        this.subClient = subClient;
    }
    public String getProjectNumber() {
        return projectNumber;
    }
    public void setProjectNumber(String projectNumber) {
        this.projectNumber = projectNumber;
    }
    public String getSubProjectNumber() {
        return subProjectNumber;
    }
    public void setSubProjectNumber(String subProjectNumber) {
        this.subProjectNumber = subProjectNumber;
    }
    public String getCatalogUrl() {
        return catalogUrl;
    }
    public void setCatalogUrl(String catalogUrl) {
        this.catalogUrl = catalogUrl;
    }
    public String getCatalogDeepLink() {
        return catalogDeepLink;
    }
    public void setCatalogDeepLink(String catalogDeepLink) {
        this.catalogDeepLink = catalogDeepLink;
    }

}
