package com.maritz.culturenext.awardtype.dto;

public class AwardTypeDTO {

    private Long id;
    private String displayName;
    private String longDesc;
    private Double awardValue;
    private Boolean abs;
    private Boolean isUsed;
    private AwardTypeDetailsDTO details;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public String getLongDesc() {
        return longDesc;
    }
    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }
    public Double getAwardValue() {
        return awardValue;
    }
    public void setAwardValue(Double awardValue) {
        this.awardValue = awardValue;
    }
    public Boolean getAbs() {
        return abs;
    }
    public void setAbs(Boolean abs) {
        this.abs = abs;
    }
    public Boolean getIsUsed() {
        return isUsed;
    }
    public void setIsUsed(Boolean isUsed) {
        this.isUsed = isUsed;
    }
    public AwardTypeDetailsDTO getDetails() {
        return details;
    }
    public void setDetails(AwardTypeDetailsDTO details) {
        this.details = details;
    }
}
