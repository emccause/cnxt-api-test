package com.maritz.culturenext.awardtype.dto;

public class AwardTypeEligibilityToReceiveDTO {

    private Long awardTypeId;
    private String payoutType;
    private String displayName;
    private Boolean eligibleToReceive;
    private Boolean abs;
    private String catalogUrl;
    private String catalogDeepLink;
    private Long lookupDisplayNameId;
        
    public Long getAwardTypeId() {
        return awardTypeId;
    }
    public void setAwardTypeId(Long awardTypeId) {
        this.awardTypeId = awardTypeId;
    }
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getDisplayName() {
        return displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
    public Boolean getEligibleToReceive() {
        return eligibleToReceive;
    }
    public void setEligibleToReceive(Boolean eligibleToReceive) {
        this.eligibleToReceive = eligibleToReceive;
    }
    public Boolean  getAbs() {
        return abs;
    }
    public void setAbs(Boolean abs) {
        this.abs = abs;
    }
    public String getCatalogUrl() {
        return catalogUrl;
    }
    public void setCatalogUrl(String catalogUrl) {
        this.catalogUrl = catalogUrl;
    }
    public String getCatalogDeepLink() {
        return catalogDeepLink;
    }
    public void setCatalogDeepLink(String catalogDeepLink) {
        this.catalogDeepLink = catalogDeepLink;
    }
	public Long getLookupDisplayNameId() {
		return lookupDisplayNameId;
	}
	public void setLookupDisplayNameId(Long lookupDisplayNameId) {
		this.lookupDisplayNameId = lookupDisplayNameId;
	}
    
    
}
