package com.maritz.culturenext.awardtype.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class AwardTypeConstants {

    // Max lengths in ABS
    public static final int ABS_PRODUCT_CODE_LENGTH = 3;
    public static final int ABS_SUBCLIENT_LENGTH = 30;
    public static final int ABS_PROJECT_NUMBER_LENGTH = 25;
    public static final int ABS_SUB_PROJECT_NUMBER_LENGTH = 8;
    
    public static final String DEFAULT_CARD_TYPE_DESC = "SHOP REWARDS";
    public static final String DEFAULT_SERVICE_ANNIVERSARY_CARD_TYPE_DESC = "Service Anniversary";
    public static final String DEFAULT_SERVICE_ANNIVERSARY_CARD_TYPE_DESC_RUN_TOGETHER = "ServiceAnniversary";
    
    // Error messages
    public static final String ERROR_AWARDTYPE_EXISTS = "AWARD_TYPE_EXISTS";
    public static final String ERROR_AWARDTYPE_DOES_NOT_EXIST = "AWARD_TYPE_DOES_NOT_EXIST";

    public static final String ERROR_AWARDTYPE_EXISTS_MSG = "An award type with this payoutType already exists.";
    public static final String ERROR_AWARDTYPE_DOES_NOT_EXIST_MSG = "An award type with this payoutType does not exist.";

    public static final String ERROR_MISSING_DISPLAY_NAME = "MISSING_DISPLAY_NAME";
    public static final String ERROR_MISSING_DISPLAY_NAME_MSG = "Display name must be provided for non-ABS award types.";
    public static final String ERROR_MISSING_PAYOUT_TYPE = "MISSING_PAYOUT_TYPE";
    public static final String ERROR_MISSING_PAYOUT_TYPE_MSG = "Payout Type must be provided";
    public static final String ERROR_MISSING_TYPE = "MISSING_ABS_TYPE";
    public static final String ERROR_MISSING_TYPE_MSG = "ABS or non-ABS award type must be specified";
    public static final String ERROR_MISSING_DETAILS = "MISSING_DETAILS_SECTION";
    public static final String ERROR_MISSING_DETAILS_MSG = "The details section must be populated for ABS award types";
    public static final String ERROR_MISSING_SUBCLIENT = "MISSING_SUBCLIENT";
    public static final String ERROR_MISSING_SUBCLIENT_MSG = "SubClient number must be provided for ABS award types";
    public static final String ERROR_MISSING_PROJECT_NUMBER = "MISSING_PROJECT_NUMBER";
    public static final String ERROR_MISSING_PROJECT_NUMBER_MSG = "Project Number must be provided for ABS award types";
    public static final String ERROR_MISSING_SUB_PROJECT_NUMBER = "MISSING_SUB_PROJECT_NUMBER";
    public static final String ERROR_MISSING_SUB_PROJECT_NUMBER_MSG = "Sub Project Number must be provided for ABS award types";
    public static final String ERROR_MISSING_AWARD_VALUE = "MISSING_AWARD_VALUE";
    public static final String ERROR_MISSING_AWARD_VALUE_MSG = "Award Value must be provided for ABS award types";
    public static final String ERROR_PAYOUT_TYPE_MAX_LENGTH = "PAYOUT_TYPE_MAX_LENGTH";
    public static final String ERROR_PAYOUT_TYPE_MAX_LENGTH_MSG = 
            "Max length for an ABS payoutType is " + ABS_PRODUCT_CODE_LENGTH;
    public static final String ERROR_SUBCLIENT_MAX_LENGTH = "SUBCLIENT_MAX_LENGTH";
    public static final String ERROR_SUBCLIENT_MAX_LENGTH_MSG = 
            "Max length for an ABS subclient number is " + ABS_SUBCLIENT_LENGTH;
    public static final String ERROR_PROJECT_NUMBER_MAX_LENGTH = "PROJECT_NUMBER_MAX_LENGTH";
    public static final String ERROR_PROJECT_NUMBER_MAX_LENGTH_MSG = 
            "Max length for an ABS project number is " + ABS_PROJECT_NUMBER_LENGTH;
    public static final String ERROR_SUB_PROJECT_NUMBER_MAX_LENGTH = "SUB_PROJECT_NUMBER_MAX_LENGTH";
    public static final String ERROR_SUB_PROJECT_NUMBER_MAX_LENGTH_MSG = 
            "Max length for an ABS sub-project number is " + ABS_SUB_PROJECT_NUMBER_LENGTH;
    
    public static final ErrorMessage AWARD_TYPE_EXISTS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_AWARDTYPE_EXISTS)
        .setField(ProjectConstants.PAYOUT_TYPE)
        .setMessage(ERROR_AWARDTYPE_EXISTS_MSG);
    
    public static final ErrorMessage AWARD_TYPE_DOES_NOT_EXIST = new ErrorMessage()
            .setCode(ERROR_AWARDTYPE_DOES_NOT_EXIST)
            .setField(ProjectConstants.PAYOUT_TYPE)
            .setMessage(ERROR_AWARDTYPE_DOES_NOT_EXIST_MSG);

    
    public static final ErrorMessage MISSING_DISPLAY_NAME_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_DISPLAY_NAME)
        .setField(ProjectConstants.DISPLAY_NAME)
        .setMessage(ERROR_MISSING_DISPLAY_NAME_MSG);
    
    public static final ErrorMessage MISSING_PAYOUT_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_PAYOUT_TYPE)
        .setField(ProjectConstants.PAYOUT_TYPE)
        .setMessage(ERROR_MISSING_PAYOUT_TYPE_MSG);
    
    public static final ErrorMessage MISSING_TYPE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_TYPE)
        .setField(ProjectConstants.ABS)
        .setMessage(ERROR_MISSING_TYPE_MSG);
    
    public static final ErrorMessage MISSING_DETAILS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_DETAILS)
        .setField(ProjectConstants.DETAILS)
        .setMessage(ERROR_MISSING_DETAILS_MSG);

    public static final ErrorMessage MISSING_SUBCLIENT_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SUBCLIENT)
        .setField(ProjectConstants.SUBCLIENT)
        .setMessage(ERROR_MISSING_SUBCLIENT_MSG);

    public static final ErrorMessage MISSING_PROJECT_NUMBER_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_PROJECT_NUMBER)
        .setField(ProjectConstants.PROJECT_NUMBER)
        .setMessage(ERROR_MISSING_PROJECT_NUMBER_MSG);

    public static final ErrorMessage MISSING_SUB_PROJECT_NUMBER_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_SUB_PROJECT_NUMBER)
        .setField(ProjectConstants.SUB_PROJECT_NUMBER)
        .setMessage(ERROR_MISSING_SUB_PROJECT_NUMBER_MSG);
    
    public static final ErrorMessage MISSING_AWARD_VALUE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_MISSING_AWARD_VALUE)
        .setField(ProjectConstants.AWARD_VALUE)
        .setMessage(ERROR_MISSING_AWARD_VALUE_MSG);
    
    public static final ErrorMessage PAYOUT_TYPE_MAX_LENGTH_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PAYOUT_TYPE_MAX_LENGTH)
        .setField(ProjectConstants.PAYOUT_TYPE)
        .setMessage(ERROR_PAYOUT_TYPE_MAX_LENGTH_MSG);
    
    public static final ErrorMessage SUBCLIENT_MAX_LENGTH_MESSAGE = new ErrorMessage()
        .setCode(ERROR_SUBCLIENT_MAX_LENGTH)
        .setField(ProjectConstants.SUBCLIENT)
        .setMessage(ERROR_SUBCLIENT_MAX_LENGTH_MSG);
    
    public static final ErrorMessage PROJET_NUMBER_MAX_LENGTH_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PROJECT_NUMBER_MAX_LENGTH)
        .setField(ProjectConstants.PROJECT_NUMBER)
        .setMessage(ERROR_PROJECT_NUMBER_MAX_LENGTH_MSG);
    
    public static final ErrorMessage SUB_PROJECT_NUMBER_MAX_LENGTH_MESSAGE = new ErrorMessage()
        .setCode(ERROR_SUB_PROJECT_NUMBER_MAX_LENGTH)
        .setField(ProjectConstants.SUB_PROJECT_NUMBER)
        .setMessage(ERROR_SUB_PROJECT_NUMBER_MAX_LENGTH_MSG);
    
}
