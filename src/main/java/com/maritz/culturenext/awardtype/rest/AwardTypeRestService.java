package com.maritz.culturenext.awardtype.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.ID_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.LANGUAGE_CODE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.TARGET_ID_REST_PARAM;

import java.util.List;

import javax.inject.Inject;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.annotation.Permission;
import com.maritz.core.security.Security;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeEligibilityToReceiveDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

@RestController
@Api(value = "/award-types", description = "Endpoints that deal with award types")
public class AwardTypeRestService {

    @Inject private AwardTypeService awardTypeService;
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private Security security;
    
    // Service endpoint paths
    public static final String AWARD_TYPES_PATH = "award-types";
    public static final String GET_AWARD_TYPE_BY_ID = AWARD_TYPES_PATH + "/{" + ID_REST_PARAM + "}";
    public static final String GET_AWARD_TYPE_BY_USERS_ID_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + TARGET_ID_REST_PARAM + "}/" + AWARD_TYPES_PATH;
    
    //rest/award-types
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @RequestMapping(value = AWARD_TYPES_PATH, method = RequestMethod.POST)
    @ApiOperation(value = "Create an the award type")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully created an award type."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<AwardTypeDTO> createAwardType(
                @ApiParam(value = "The award type you want to insert") @RequestBody AwardTypeDTO awardTypeDto
            ) throws Throwable {
        return awardTypeService.insertAwardType(awardTypeDto);
    }

    //rest/award-types/{id}
    @PreAuthorize("@security.hasRole('" + PermissionConstants.MARITZ_ADMIN_ROLE_NAME + "')")
    @RequestMapping(value = GET_AWARD_TYPE_BY_ID , method = RequestMethod.PUT)
    @ApiOperation(value = "Update an existing award type with the id provided.",
            notes = "Update an existing award type. ")
    @Permission("PUBLIC")
    public List<AwardTypeDTO> updateAwardType(
            @ApiParam(value = "The id of the award type to update.")
            @PathVariable(ID_REST_PARAM) String idString,
            @ApiParam(value = "The award type you want to update")
            @RequestBody AwardTypeDTO awardTypeDto
        ) throws Throwable {
        
        Long id = convertPathVariablesUtil.getId(idString);
        
        return awardTypeService.updateAwardType(awardTypeDto);
    }    


    //rest/award-types
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')")
    @RequestMapping(value = AWARD_TYPES_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Retrieves all the award types.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned all the award types."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<AwardTypeDTO> getAwardTypesList() throws Throwable {
        return awardTypeService.getAwardTypesList();
    }

    //rest/award-types/{id}
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "')")
    @RequestMapping(value = GET_AWARD_TYPE_BY_ID , method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete an existing award type with the id provided.",
            notes = "Delete an existing award type. Can only be deleted if isUsed = false "
                    + "(If the award type is not currently tied to a program and has never been used).")
    @Permission("PUBLIC")
    public void deleteAwardTypeById(
            @ApiParam(value = "The id of the award type to delete.")
            @PathVariable(ID_REST_PARAM) String idString
        ) throws Throwable {
        
        Long id = convertPathVariablesUtil.getId(idString);
        
        awardTypeService.deleteAwardType(id);
    }
    
    //rest/participants/~/award-types
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = GET_AWARD_TYPE_BY_USERS_ID_PATH, method = RequestMethod.GET)
    @ApiOperation(value = " Return a user's eligibility to receive the award types configured at the site level")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully returned the award types."),
            @ApiResponse(code = 400, message = "Request error") })
    @Permission("PUBLIC")
    public List<AwardTypeEligibilityToReceiveDTO> getAwardTypeEligibilityToReceiveList (
            @ApiParam(value="Id of the pax") 
            @PathVariable(TARGET_ID_REST_PARAM) String targetIdString,
            @ApiParam(value="Language code.", required = false)
            @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode
        ) throws Throwable {        
        Long pathPaxId = security.getPaxId(targetIdString);
        if(pathPaxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }        
        return awardTypeService.getAwardTypeEligibilityToReceiveList(languageCode,pathPaxId);
    }
}
