package com.maritz.culturenext.survey.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.dto.AuthTokenDTO;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.survey.services.SurveyApiService;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("participants")
public class SurveyApiRestService {

    private final SurveyApiService surveyApiService;

    public SurveyApiRestService(
        SurveyApiService surveyApiService
    ) {
        this.surveyApiService = surveyApiService;
    }

    //rest/participants/{paxId}/survey-api-token
    @GetMapping("{paxId}/survey-api-token")
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, authorization="@security.isMyPax(#pax.getPaxId())")
    @ApiOperation(value = "Returns a token for the survey api.")
    public AuthTokenDTO getSurveyApiToken(
        @PathVariable("paxId") Pax pax
    ) {
        return surveyApiService.generateSurveyApiToken(pax.getPaxId());
    }

}
