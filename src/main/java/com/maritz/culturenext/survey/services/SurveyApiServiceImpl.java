package com.maritz.culturenext.survey.services;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.maritz.core.dto.AuthTokenDTO;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.culturenext.constants.ApplicationDataConstants;

import io.jsonwebtoken.Claims;


@Service("surveyApiService")
public class SurveyApiServiceImpl implements SurveyApiService {

    private final Environment environment;
    private final TokenService surveyApiTokenService;

    public SurveyApiServiceImpl(
        Environment environment,
        TokenService surveyApiTokenService
    ) {
        this.environment = environment;
        this.surveyApiTokenService = surveyApiTokenService;
    }

    @Override
    public AuthTokenDTO generateSurveyApiToken(Long paxId) {
        Claims claims = surveyApiTokenService.createClaims();
        claims.setSubject(paxId.toString());
        claims.put("paxId", paxId);
        claims.put("clientName", environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME));

        return new AuthTokenDTO()
            .setAuthToken(surveyApiTokenService.createToken(claims))
        ;
    }

}
