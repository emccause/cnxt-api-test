package com.maritz.culturenext.survey.services;

import com.maritz.core.dto.AuthTokenDTO;

public interface SurveyApiService {

    AuthTokenDTO generateSurveyApiToken(Long paxId);

}
