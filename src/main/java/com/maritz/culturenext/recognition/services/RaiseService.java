package com.maritz.culturenext.recognition.services;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;

import java.util.List;

public interface RaiseService {
    
    /**
     * Gets the list of raises on a nomination
     * 
     * @param nominationId - The nomination id that is being searched for raises
     * @param size - The request page size
     * @param page - The page number requested
     * @param statusList - List list of statuses for which the raises are being returned
     * @param fromPaxId - The submitter of the raises
     * @param involvedPaxId - paxID to use for a lookup. Can look up by nomination submitter, receiver, or manager
     * @return The list of raiseDTO based on the request criteria, returns an empty list if none return
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/16744480/Get+a+list+of+Raises
     */
    List<RaiseDTO> getRaisesForNominationCurrent(Long nominationId, Integer size, Integer page, List<String> statusList, 
            Long fromPaxId, Long involvedPaxId);
    
    /**
     * Gets a paginated list of raises on a nomination
     * 
     * @param nominationId - The nomination id that is being searched for raises
     * @param size - The request page size
     * @param page - The page number requested
     * @param statusList - List list of statuses for which the raises are being returned
     * @param fromPaxId - The submitter of the raises
     * @param involvedPaxId - paxID to use for a lookup. Can look up by nomination submitter, receiver, or manager
     * @return The list of raiseDTO based on the request criteria, returns an empty list if none return
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/16744480/Get+a+list+of+Raises
     */
    PaginatedResponseObject<RaiseDTO> getRaisesForNomination(Long nominationId, Integer size, Integer page, List<String> statusList, 
            Long fromPaxId, Long involvedPaxId);
    
    /**
     * This method is used to populate a list of users that can be raised on a recognition
     * 
     * @param activityId - The newsfeed item id used to locate the recognition record
     * @param excludePrivate - Boolean used to check if private users should be returned
     * @param delimitedStatus - comma delimited list of statuses to limit results to
     * @param size - The size of the page
     * @param page - The page number
     * @return The list of people that can be raised on a recognition
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/18743327/Get+Pax+list+of+Recipients+for+a+recognition+Activity
     */
    List<EntityDTO> getRecipientsForRaise(Long activityId, Boolean excludePrivate, String delimitedStatus, 
            Integer size, Integer page);
    
    /**
     * Creates the raise records on a nomination
     * 
     * @param nominationId The nomination id for which the raises are created
     * @param raises The list of raises on the nomination
     * @return The newly created list of raise records
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19169288/Create+Raises
     */
    List<RaiseDTO> createRaises(Long nominationId, List<RaiseDTO> raises);
    
    /**
     * Validates and then updates the list of raises passed in
     * 
     * @param raises The list of raises that need to be updated
     * @return list of raises post-update
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773800/Update+Raise
     */
    List<RaiseDTO> updateRaises(List<RaiseDTO> raises);
    
    /**
     * gets a list of recipients for a nomination
     * 
     * @param nominationId id of nomination to get recipients for
     * @param excludePrivate boolean - should recipients who have their share_rec
     *          preference set to false be returned or not?
     * @return list of ids of recipients
     */
    List<Long> getRecipientsOnNomination(Long nominationId, Boolean excludePrivate);
    
    /**
     * gets a list of recipients for a nomination based on activity id
     * 
     * @param activityId - id of activity to get recipients for
     * @param excludePrivate - boolean - should recipients who have their 
     *         share_rec preference set to false be returned or not?
     * @param statusList - List of status to filter on
     * @return list of ids of recipients
     */
    List<Long> getRecipientsOnActivity(Long activityId, Boolean excludePrivate, List<String> statusList);
    
    /**
     * Get the id of the pax who either is responsible for approving the recognition or who already approved it
     * 
     * @param recognitionId id of the recognition to find the approver for
     * @return id of the approver
     */
    Long getApproverPaxIdForRecognition(Long recognitionId);
}
