package com.maritz.culturenext.recognition.services;

import java.util.concurrent.Future;

public interface AdvancedRecognitionThreadService {
    
    /**
     * Loads STAGE_ADVANCED_REC records, converts into nominationDTO, runs through nomination validation and reports errors if they exist
     * 
     * @param batchId
     * @param tempNominationId
     * @param errorsOccurred
     */
    public Boolean validateNomination(Long batchId, Long tempNominationId);
    
    /**
     * Async wrapper for above
     */
    public Future<Boolean> validateNominationAsync(Long batchId, Long tempNominationId);
    
    /**
     * Loads STAGE_ADVANCED_REC records, converts into nominationDTO, creates nominationDTO and sends to nomination service to save
     * 
     * @param batchId
     * @param tempNominationId
     * @param errorsOccurred
     * @return 
     */
    public void createNomination(Long batchId, Long tempNominationId);
    
    /**
     * Async wrapper for above
     */
    public void createNominationAsync(Long batchId, Long tempNominationId);

}
