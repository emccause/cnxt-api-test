package com.maritz.culturenext.recognition.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.ProgramCriteria;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.repository.RecognitionCriteriaRepository;
import com.maritz.core.jpa.support.findby.FindBy;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.profile.services.impl.DetailProfileServiceImpl;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.RecognitionCriteriaDTO;
import com.maritz.culturenext.recognition.services.RecognitionCriteriaService;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class RecognitionCriteriaServiceImpl implements RecognitionCriteriaService {

    @Inject private ConcentrixDao<NominationCriteria> nominationCriteriaDao;
    @Inject private ConcentrixDao<ProgramCriteria> programCriteriaDao;
    @Inject private RecognitionCriteriaRepository recognitionCriteriaRepository;
    @Inject private TranslationUtil translationUtil;
    @Inject private DetailProfileServiceImpl  detailProfileInfo;

    //error codes and messages
    private static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    private static final String ERROR_INVALID_STATUS_MSG = "Invalid status passed into service";
    private static final String ERROR_INVALID_IDS = "INVALID_IDS";
    private static final String ERROR_INVALID_IDS_MSG = "Ids must only contain numbers";

    @Override
    public List<RecognitionCriteriaDTO> getRecognitionCriteria(String statusCSV, String languageCode, String idList) {

        List<Long> ids = new ArrayList<Long>();
        if (idList != null) {
            for (String id : idList.split(",")) {
                try {
                    ids.add(Long.valueOf(id));
                } catch (Exception e) {
                    throw new ErrorMessageException(ProjectConstants.IDS, ERROR_INVALID_IDS, ERROR_INVALID_IDS_MSG);
                }
            }
        }

        FindBy<RecognitionCriteria> recognitionCriteriaFindBy = recognitionCriteriaRepository
                .findBy().where(ProjectConstants.STATUS_TYPE_CODE).in(validateStatusCSV(statusCSV));

        if (!CollectionUtils.isEmpty(ids)) {
            recognitionCriteriaFindBy = recognitionCriteriaFindBy.and(ProjectConstants.ID).in(ids);
        }
        recognitionCriteriaFindBy = recognitionCriteriaFindBy.orderAsc(ProjectConstants.RECOGNITION_CRITERIA_NAME);        
        if (languageCode==null) {
        	try{            	
        		languageCode = detailProfileInfo.getDetailProfileInfo().getPax().getLanguageLocale();
        	}catch(NullPointerException e){languageCode = TranslationConstants.EN_US;}
        }
        return populateRecognitionCriteriaDTOFromEntity(recognitionCriteriaFindBy.findAll(), languageCode);
    }

    @Override
    public RecognitionCriteriaDTO getRecognitionCriteriaById(Long recognitionCriteriaId, String languageCode) {
        return populateRecognitionCriteriaDTOFromSingleEntity(queryById(recognitionCriteriaId), languageCode);
    }

    @Override
    public List<RecognitionCriteriaDTO> insertRecognitionCriteria(
            List<RecognitionCriteriaDTO> recognitionCriteriaDTOList) {

        List <RecognitionCriteria> recognitionCriteriaEntityList =
                validateAndCreateEntityList(recognitionCriteriaDTOList, ProjectConstants.POST, null);

        recognitionCriteriaRepository.save(recognitionCriteriaEntityList);

        // create translation data
        setTranslationRecognitionCriteriaList(recognitionCriteriaEntityList);

        return populateRecognitionCriteriaDTOFromEntity(recognitionCriteriaEntityList, null);
    }

    @Override
    public RecognitionCriteriaDTO updateRecognitionCriteria( RecognitionCriteriaDTO recognitionCriteriaDTO,
            Long recognitionCriteriaId) {
        List<RecognitionCriteriaDTO> recognitionCriteriaDTOList = new ArrayList<>();
        recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
        List<RecognitionCriteria> recognitionCriteriaEntityList =
                validateAndCreateEntityList(recognitionCriteriaDTOList, ProjectConstants.PUT, recognitionCriteriaId);
        recognitionCriteriaRepository.save(recognitionCriteriaEntityList);
        return getRecognitionCriteriaById(recognitionCriteriaId, null);
    }

    private List<String> validateStatusCSV(String statusCSV) {
        List<String> statusList = new ArrayList<>();

        if (statusCSV == null || statusCSV.length() == 0) {
            statusList.add(StatusTypeCode.ACTIVE.name());
        }
        else {
            StringTokenizer stringTokenizer = new StringTokenizer(statusCSV, ",");
            while (stringTokenizer.hasMoreTokens()) {
                String tokenStatus = stringTokenizer.nextToken();
                if (tokenStatus.equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                    statusList.add(tokenStatus);
                } else if (tokenStatus.equalsIgnoreCase(StatusTypeCode.INACTIVE.name())) {
                    statusList.add(tokenStatus);
                } else {
                    throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(ERROR_INVALID_STATUS)
                        .setField(ProjectConstants.STATUS)
                        .setMessage(ERROR_INVALID_STATUS_MSG)
                    );
                }
            }
        }

        return statusList;
    }

    private RecognitionCriteria queryById(Long recognitionCriteriaId) {
        return recognitionCriteriaRepository.findOne(recognitionCriteriaId);
    }

    public List<RecognitionCriteriaDTO> populateRecognitionCriteriaDTOFromEntity(
            List<RecognitionCriteria> recognitionCriteriaList, String languageCode) {
        List<RecognitionCriteriaDTO> recognitionCriteriaDTOList  = new ArrayList<>();
        for (RecognitionCriteria recognitionCriteria : recognitionCriteriaList) {
            recognitionCriteriaDTOList.add(populateRecognitionCriteriaDTOFromSingleEntity(recognitionCriteria,
                    languageCode));
        }
        return recognitionCriteriaDTOList;
    }

    /**
     * Used for PUT and POST to populate a DTO from an Entity
     * @param recognitionCriteria
     * @return
     */
    public RecognitionCriteriaDTO populateRecognitionCriteriaDTOFromSingleEntity(
            RecognitionCriteria recognitionCriteria, String languageCode) {
        Map<String, String> defaultMap = new HashMap<>();
        defaultMap.put(TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_NAME,
                recognitionCriteria.getRecognitionCriteriaName());
        defaultMap.put(TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_DESC,
                recognitionCriteria.getRecognitionCriteriaDesc());

        Map<String, String> translations = translationUtil.getTranslationsForListOfColumns(languageCode,
                TableName.RECOGNITION_CRITERIA.name(), TranslationConstants.RECOGNITION_CRITERIA_COLUMNS,
                recognitionCriteria.getId(), defaultMap);

        RecognitionCriteriaDTO recognitionCriteriaDTO  = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setId(recognitionCriteria.getId());
        recognitionCriteriaDTO.setDisplayName(translations.get(
                TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_NAME));
        recognitionCriteriaDTO.setDescription(translations.get(
                TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_DESC));
        recognitionCriteriaDTO.setStatus(recognitionCriteria.getStatusTypeCode());
        return recognitionCriteriaDTO;
    }

    /**
     * Validate DTO objects passed in and if no errors are found they
     * will be translated into entities and returned in a list
     * @param recognitionCriteriaDTOList
     * @param requestType
     * @return
     */
    public List<RecognitionCriteria> validateAndCreateEntityList(
            List <RecognitionCriteriaDTO> recognitionCriteriaDTOList, String requestType, Long recognitionCriteriaId) {
        List<RecognitionCriteria> recognitionCriteriaEntityList = new ArrayList<>();
        List<ErrorMessage> errors = new ArrayList<>();

        for (RecognitionCriteriaDTO recognitionCriteriaDTO : recognitionCriteriaDTOList) {
            RecognitionCriteria recognitionCriteriaEntity = new RecognitionCriteria();

            if (recognitionCriteriaDTO.getDisplayName() == null ||
                    recognitionCriteriaDTO.getDisplayName().length() == 0) {
                errors.add(NominationConstants.CRITERIA_NAME_NULL_MESSAGE);
            }
            else if (recognitionCriteriaDTO.getDisplayName().length() > 100) {
                errors.add(NominationConstants.CRITERIA_NAME_TOO_LONG_MESSAGE);
            }

            //get the existing criteria records for the given name
            List<RecognitionCriteria> criterias = recognitionCriteriaRepository
                    .findByRecognitionCriteriaName(recognitionCriteriaDTO.getDisplayName());

            if (requestType.equals(ProjectConstants.PUT)) {
                if (recognitionCriteriaDTO.getId() == null) {
                    errors.add(NominationConstants.ERROR_ID_NULL_MESSAGE);
                }
                else {
                    if (recognitionCriteriaId == null || recognitionCriteriaDTO.getId() != recognitionCriteriaId) {
                        errors.add(NominationConstants.ERROR_ID_MISMATCHED_MESSAGE);
                    }
                    else {
                        //check for a duplicate record by name
                        recognitionCriteriaEntity = null;
                        if (!criterias.isEmpty()) {
                            recognitionCriteriaEntity = criterias.get(0);
                            if (criterias.size() > 1 || recognitionCriteriaEntity.getId() != recognitionCriteriaId) {
                                errors.add(NominationConstants.CRITERIA_EXISTS_MESSAGE);
                                recognitionCriteriaEntity = null;
                            }
                        }
                        else {
                            recognitionCriteriaEntity = recognitionCriteriaRepository.findOne(recognitionCriteriaId);
                        }

                        if (recognitionCriteriaEntity != null) {

                            recognitionCriteriaEntity.setRecognitionCriteriaName(
                                    recognitionCriteriaDTO.getDisplayName());
                            if (recognitionCriteriaDTO.getStatus() == null ||
                                    recognitionCriteriaDTO.getStatus().length() == 0) {
                                errors.add(NominationConstants.NULL_STATUS_MESSAGE);
                            }
                            else if (recognitionCriteriaDTO.getStatus()
                                    .equalsIgnoreCase(StatusTypeCode.INACTIVE.name())) {
                                if (programCriteriaDao.findBy().where("recognitionCriteriaId").eq(recognitionCriteriaId).exists()
                                        || nominationCriteriaDao.findBy().eq(recognitionCriteriaId).exists()) {
                                    //rec criteria is in use, throw error
                                    errors.add(NominationConstants.CRITERIA_VALUE_IN_USE_MESSAGE);
                                }
                                recognitionCriteriaEntity.setStatusTypeCode(recognitionCriteriaDTO.getStatus());
                            }
                            else if (recognitionCriteriaDTO.getStatus()
                                    .equalsIgnoreCase(StatusTypeCode.ACTIVE.name())) {
                                recognitionCriteriaEntity.setStatusTypeCode(recognitionCriteriaDTO.getStatus());
                            }
                            else {
                                errors.add(NominationConstants.INVALID_STATUS_MESSAGE);
                            }
                        }
                    }
                }
            }
            else if (requestType.equals(ProjectConstants.POST)) {
                //validate that a criteria of that name doesn't exist
                if (!criterias.isEmpty()) {
                    errors.add(NominationConstants.CRITERIA_EXISTS_MESSAGE);
                }
                else {
                    //Auto save status as ACTIVE on POST
                    recognitionCriteriaEntity.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
                    recognitionCriteriaEntity.setRecognitionCriteriaName(recognitionCriteriaDTO.getDisplayName());
                }
            }

            if (recognitionCriteriaDTO.getDescription() != null
                && !recognitionCriteriaDTO.getDescription().equals("")) {
                if (recognitionCriteriaDTO.getDescription().length() > 255) {
                    errors.add(NominationConstants.CRITERIA_DESCRIPTION_TOO_LONG_MESSAGE);
                }
                else {
                    recognitionCriteriaEntity.setRecognitionCriteriaDesc(recognitionCriteriaDTO.getDescription());
                }
            }
            recognitionCriteriaEntityList.add(recognitionCriteriaEntity);
        }

        //Check for errors and throw any that are found before attempting save
        ErrorMessageException.throwIfHasErrors(errors);
        return recognitionCriteriaEntityList;
    }

    /**
     * Create translatable content by list
     * @param recognitionCriteriaList
     */
    private void setTranslationRecognitionCriteriaList(List<RecognitionCriteria> recognitionCriteriaList){
        for(RecognitionCriteria recognitionCriteria: recognitionCriteriaList){
            setTranslationRecognitionCriteriaId(recognitionCriteria.getId());
        }
    }

    /**
     * Create translatable content by recognition criteria id
     * @param recognitionCriteriaList
     */
    private void setTranslationRecognitionCriteriaId(Long recognitionCriteriaId){
        translationUtil.saveTranslationData(TableName.RECOGNITION_CRITERIA.name(),
                TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_NAME,
                recognitionCriteriaId, StatusTypeCode.ACTIVE.name());
        translationUtil.saveTranslationData(TableName.RECOGNITION_CRITERIA.name(),
                TranslationConstants.COLUMN_NAME_RECOGNITION_CRITERIA_DESC,
                recognitionCriteriaId, StatusTypeCode.ACTIVE.name());
    }

}
