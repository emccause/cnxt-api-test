package com.maritz.culturenext.recognition.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.TransformerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.ListUtils;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.AuxiliaryNotification;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.NominationMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramCriteria;
import com.maritz.core.jpa.entity.ProgramEcard;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.entity.RecognitionMisc;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.LookupRepository;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.core.jpa.support.util.AclTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.alert.util.AlertUtil;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dto.BudgetEligibilityDTO;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.email.event.RecognitionEvent;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtNominationRepository;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.dto.RecognitionCriteriaSimpleDTO;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.PaxDistinctDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.GroupsService;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeInfoDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeNominationDetailsDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;
import com.maritz.culturenext.recognition.dao.NominationDetailsDao;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RecognitionSimpleDTO;
import com.maritz.culturenext.recognition.dto.RecognitionStatsDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.StatusTypeUtil;

@Component
public class NominationServiceImpl implements NominationService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private AlertService alertService;
    @Inject private AlertUtil alertUtil;
    @Inject private ApplicationEventPublisher applicationEventPublisher;
    @Inject private ApprovalService approvalService;
    @Inject private BudgetInfoDao budgetInfoDao;
    @Inject private CnxtNominationRepository cnxtNominationRepository;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<AuxiliaryNotification> auxiliaryNotificationDao;
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private ConcentrixDao<BatchEvent> batchEventDao;
    @Inject private ConcentrixDao<BudgetMisc> budgetMiscDao;
    @Inject private ConcentrixDao<FileItem> fileItemDao;
    @Inject private ConcentrixDao<Files> filesDao;
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<NominationCriteria> nominationCriteriaDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<ProgramCriteria> programCriteriaDao;
    @Inject private ConcentrixDao<ProgramEcard> programEcardDao;
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Inject private ConcentrixDao<RecognitionMisc> recognitionMiscDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private ConcentrixDao<NominationMisc> nominationMiscDao;
    @Inject private Environment environment;
    @Inject private GroupsService groupsService;
    @Inject private LookupRepository lookupRepository;
    @Inject private NominationDataBulkSaveDao nominationDataBulkSaveDao;
    @Inject private NominationDetailsDao nominationDetailsDao;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ParticipantsPppIndexService participantsPppIndexService;
    @Inject private PaxCountDao paxCountDao;
    @Inject private ProgramEligibilityQueryDao programEligibilityQueryDao;
    @Inject private ProgramMiscRepository programMiscRepository;
    @Inject private ProxyService proxyService;
    @Inject private RecognitionService recognitionService;
    @Inject private Security security;
    @Inject private TransactionHeaderMiscDao transactionHeaderMiscDao;    

    @Transactional
    @Override
    public NominationDetailsDTO createStandardNomination(Long submitterPaxId, NominationRequestDTO nominationDto, Boolean validate) {
        try {
            return buildNominationDetails(submitterPaxId, nominationDto, validate, NominationConstants.RECOGNITION_NOMINATION_TYPE);
        } catch (ErrorMessageException eme) {
            throw eme;
        } catch (DuplicateKeyException duplicateKeyException) {
        	logger.error("Cannot insert duplicate Nomination:",duplicateKeyException );
        	return new NominationDetailsDTO();
    	}   catch (RuntimeException re) {
            logger.error("Unexpected error: Give recognition failed:", re);
            throw re;
    	}
    }

    private NominationDetailsDTO buildNominationDetails(Long submitterPaxId, NominationRequestDTO nominationRequestDTO, Boolean validate, String nominationTypeCode){
        if (CollectionUtils.isEmpty(nominationRequestDTO.getReceivers())) {
            throw new ErrorMessageException().addErrorMessage(NominationConstants.MISSING_RECIEVERS_MESSAGE);
        }
        
        String validationStr = "*** buildNominationDetails: ";
        validationStr = validationStr + " submitterPaxId=" + convertLongToString(submitterPaxId) + " nominationTypeCode=" + convertString(nominationTypeCode);
        
        Long proxyPaxId = null;
        if (validate) {
            proxyPaxId = getProxyPaxId(submitterPaxId);
        }

        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        Map<Integer, Set<Long>> paxIdsByAmount = new HashMap<>();
        Set<Integer> originalAmounts = new HashSet<>();
        Map<Integer, Long> awardTierByAmount = new HashMap<>();
        Map<Long, Integer> origAmountByPaxId = new HashMap<>();
        Map<Long, Integer> origAmountByGroupId = new HashMap<>();

        for (GroupMemberAwardDTO receiver : nominationRequestDTO.getReceivers()) {

            if (receiver.getPaxId() != null) {

                setPaxCollections (receiver, receiver.getPaxId(), paxIds, awardTierByAmount,
                        paxIdsByAmount, origAmountByPaxId, originalAmounts);

            }

        }

        for (GroupMemberAwardDTO receiver : nominationRequestDTO.getReceivers()) {

            if (receiver.getGroupId() != null) {
            	validationStr = validationStr + " recGroupId=" + convertLongToString(receiver.getGroupId());
                Groups group = groupsDao.findById(receiver.getGroupId());
                if (group.getGroupTypeCode().equals(GroupType.PERSONAL.getCode())) {
                    List<EntityDTO> groupMembers = groupsService.getGroupMembers(receiver.getGroupId(), null, null, null);
                    for(EntityDTO groupMember: groupMembers){
                        if(groupMember instanceof EmployeeDTO) {
                            EmployeeDTO pax = (EmployeeDTO) groupMember;
                            if (!paxIds.contains(pax.getPaxId()) && !submitterPaxId.equals(pax.getPaxId())) {
                                setPaxCollections (receiver, pax.getPaxId(), paxIds, awardTierByAmount,
                                        paxIdsByAmount, origAmountByPaxId, originalAmounts);
                            }
                        }
                    }
                } else {
                    // Groups are not allowed in advanced recognition, originalAmount is same value.
                    groupIds.add(receiver.getGroupId());
                    origAmountByGroupId.put(receiver.getGroupId(), receiver.getOriginalAmount());
                }

            }

        }

        if (Boolean.TRUE.equals(validate)) { // bypass for bulk advanced rec
            ErrorMessageException.throwIfHasErrors(validateNomination(
                    submitterPaxId, nominationRequestDTO, paxIds, groupIds, TableName.RECOGNITION.name(), Boolean.FALSE
                ));
        }

        Map<Integer, List<PaxDistinctDTO>> paxInfoByAmount = new HashMap<>();
        List<PaxDistinctDTO> distinctPaxObjects = null;

        //Using Original Amount to determine if a recognition is Simple or Advance Rec
        if (originalAmounts.size() > 1) {
            // It is an advanced recognition.
            for(Map.Entry<Integer, Set<Long>>  paxAmount: paxIdsByAmount.entrySet()){
                distinctPaxObjects = paxCountDao.getDistinctPaxObject(submitterPaxId, null,
                        new ArrayList<> (paxAmount.getValue()));
                if (!CollectionUtils.isEmpty(distinctPaxObjects)) {
                    paxInfoByAmount.put(paxAmount.getKey(), distinctPaxObjects);
                }
            }
        } else {
            // Simple recognition, originalAmount and awardTierId are the same for all the receivers.
            distinctPaxObjects = paxCountDao.getDistinctPaxObject(submitterPaxId, groupIds, paxIds);
            if (!CollectionUtils.isEmpty(distinctPaxObjects)) {
                String pppxEnabled = programMiscRepository.findBy()
                        .where(ProjectConstants.PROGRAM_ID).eq(nominationRequestDTO.getProgramId())
                        .and(ProjectConstants.VF_NAME).eq(VfName.PPP_INDEX_ENABLED.name())
                        .findOne(ProjectConstants.MISC_DATA, String.class);
                Boolean isPppxEnabled = Boolean.parseBoolean(pppxEnabled);

                Map<Integer, List<PaxDistinctDTO>> paxDistinctDTOsByAmount = new HashMap<>();
                List<PaxDistinctDTO> paxObjsTmp;
                Integer awardAmount = nominationRequestDTO.getReceivers().get(0).getOriginalAmount();

                for (PaxDistinctDTO paxObject : distinctPaxObjects) {
                    ParticipantsPppIndexDTO partPppx = participantsPppIndexService.getParticipantsPppxByPaxIds(paxObject.getPaxId().toString(), null).get(0);
                    if(isPppxEnabled && partPppx.getPppValue() != null && awardAmount != null && awardAmount.intValue() > 0){
                        awardAmount = (int) Math.ceil(nominationRequestDTO.getReceivers().get(0).getOriginalAmount() * partPppx.getPppValue());
                    } else {
                        awardAmount = nominationRequestDTO.getReceivers().get(0).getOriginalAmount();
                    }
                    paxObjsTmp =  paxDistinctDTOsByAmount.get(awardAmount);
                    if (paxObjsTmp == null) {
                        paxObjsTmp  = new ArrayList<>();
                    }
                    paxObjsTmp.add(paxObject);
                    paxDistinctDTOsByAmount.put(awardAmount, paxObjsTmp);
                }
                for(Map.Entry<Integer, List<PaxDistinctDTO>>  paxObjAmount: paxDistinctDTOsByAmount.entrySet()){
                    paxInfoByAmount.put(paxObjAmount.getKey(), new ArrayList<> (paxObjAmount.getValue()));
                    awardTierByAmount.put(paxObjAmount.getKey(),
                            nominationRequestDTO.getReceivers().get(0).getAwardTierId());
                }
            }
        }

        if (paxInfoByAmount == null || paxInfoByAmount.isEmpty()) { // All receivers are inactive / suspended
            throw new ErrorMessageException().addErrorMessage(
                    new ErrorMessage()
                    .setCode(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS)
                    .setMessage(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS_MSG)
                    .setField(ProjectConstants.RECEIVERS));
        }

        List<ProgramMisc> programFlags = programMiscDao.findBy()
                .where(ProjectConstants.VF_NAME).in(NominationConstants.NEWSFEED_VISIBILITY_PROGRAM_MISC_NAME,
                        NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME,
                        NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME)
                .and(ProjectConstants.PROGRAM_ID).eq(nominationRequestDTO.getProgramId()).active()
                .find();

        String newsfeedVisibility = NewsfeedVisibility.RECIPIENT_PREF.name();
        boolean notifyRecipient = true;
        boolean notifyRecipientManager = true;
        for (ProgramMisc flag : programFlags) {
            if (flag.getVfName().equalsIgnoreCase(NominationConstants.NEWSFEED_VISIBILITY_PROGRAM_MISC_NAME)) {
                newsfeedVisibility = flag.getMiscData(); // should be one of the following:
                                                        // "RECIPIENT_PREF", "PRIVATE", "PUBLIC", "NONE"
            } else if (flag.getVfName().equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME)) {
                notifyRecipient = Boolean.parseBoolean(flag.getMiscData());  // should be "TRUE" or "FALSE"
            } else if (flag.getVfName()
                    .equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME)) {
                notifyRecipientManager = Boolean.parseBoolean(flag.getMiscData());  // should be "TRUE" or "FALSE"
            }
        }
        
        validationStr = validationStr + " newsfeedVisibility=" + convertString(newsfeedVisibility) + " notifyRecipient=" + convertBooleanToString(notifyRecipient)  + " notifyRecipientManager=" + convertBooleanToString(notifyRecipientManager);
        
        //Overwrite newsfeed visibility only if program visibility is set to "PUBLIC" or "RECIPIENT_PREF" and isPrivate is TRUE
        if ((newsfeedVisibility.equalsIgnoreCase(NewsfeedVisibility.PUBLIC.name())
                || newsfeedVisibility.equalsIgnoreCase(NewsfeedVisibility.RECIPIENT_PREF.name())) &&
                Boolean.TRUE.equals(nominationRequestDTO.getIsPrivate())) {
            newsfeedVisibility = NewsfeedVisibility.PRIVATE.name();
        }
        
        validationStr = validationStr + " Checked for overwrite newsfeedVisibility=" + convertString(newsfeedVisibility);

        logger.info( validationStr );
        validationStr = "*** buildNominationDetails: ";
        
        Nomination nomination = createNominationEntity(submitterPaxId, nominationRequestDTO, proxyPaxId, nominationTypeCode);

        NewsfeedItem newsfeedItem = new NewsfeedItem();
        newsfeedItem.setFromPaxId(submitterPaxId);
        newsfeedItem.setNewsfeedItemTypeCode(TableName.RECOGNITION.name());
        newsfeedItem.setMessage(nominationRequestDTO.getHeadline());
        newsfeedItem.setStatusTypeCode(StatusTypeCode.APPROVED.name());
        newsfeedItem.setNewsfeedVisibility(newsfeedVisibility);

        validationStr = validationStr + " TableName.RECOGNITION.name=" + convertString(TableName.RECOGNITION.name());
        
        List<Recognition> recognitionList = new ArrayList<>();

        ArrayList<Long> managerPaxIdList = new ArrayList<>();

        Program program = programDao.findById(nominationRequestDTO.getProgramId());
        validationStr = validationStr + " nominationRequestDTO.getProgramId()=" + convertLongToString(nominationRequestDTO.getProgramId());
        
        Boolean hasPoints = Boolean.FALSE;
        for (Map.Entry<Integer,List<PaxDistinctDTO>> paxByAmount : paxInfoByAmount.entrySet()) {

            Integer awardAmount = paxByAmount.getKey();
            Long awardTierId = awardTierByAmount.get(awardAmount);
            Integer originalAmount = getOriginalAmount(nominationRequestDTO, paxByAmount.getValue().get(0).getPaxId());

            if(nominationRequestDTO != null && paxByAmount.getValue().get(0).getPaxId() != null && submitterPaxId != null &&originalAmount !=null && originalAmount == 0) originalAmount = getOriginalAmountForGroupCheckSubmitterPaxID(nominationRequestDTO, paxByAmount.getValue().get(0).getPaxId(), submitterPaxId);
            List<Long> paxIdsForRecognition = (List<Long>) CollectionUtils.<PaxDistinctDTO,Long>collect(paxByAmount.getValue(), TransformerUtils.invokerTransformer(ProjectConstants.PAX_ID_GETTER));

            //UPDATE recognition amount to have the original amount validated
            Map<Long, String> recognitionStatusMap = determineRecognitionStatusOnCreation(submitterPaxId,
                    nominationRequestDTO.getProgramId(), originalAmount , paxIdsForRecognition, nomination.getId());
            for (PaxDistinctDTO pax : paxByAmount.getValue()) {
                Recognition recognition = new Recognition();

                if (awardAmount != null && awardAmount > 0 && (awardTierId != null || program.getProgramTypeCode().equals(ProgramTypeEnum.MILESTONE.getCode()))) {
                    recognition.setProgramAwardTierId(awardTierId);
                    recognition.setAmount(awardAmount.doubleValue());
                    hasPoints = Boolean.TRUE;
                }

                recognition.setReceiverPaxId(pax.getPaxId());
                recognition.setGroupId(pax.getGroupId()); // this is the first group that was recognized that
                                                    //pax is a member of, or null if pax was individually recognized
                recognition.setViewed(ProjectConstants.NO_CHAR);
                if (recognitionStatusMap != null && recognitionStatusMap.get(pax.getPaxId()) != null) {
                    String status = recognitionStatusMap.get(pax.getPaxId());
                    validationStr = validationStr + " status=" + convertString(status) + " recognitionStatusMap.get(pax.getPaxId()=" + convertLongToString(pax.getPaxId());
                    recognition.setStatusTypeCode(status);
                    if(status.equalsIgnoreCase(StatusTypeCode.PENDING.name())){
                        nomination.setStatusTypeCode(StatusTypeCode.PENDING.name()); // if there are any pending
                                                        //recognitions, we want the nomination to be in pending status
                        validationStr = validationStr + " pax.getPaxId()=" + convertLongToString(pax.getPaxId()) + " status=PENDING ";
                    }
                } else {
                    recognition.setStatusTypeCode(StatusTypeCode.AUTO_APPROVED.name());
                    validationStr = validationStr + " pax.getPaxId()=" + convertLongToString(pax.getPaxId()) + " status=AUTO_APPROVED ";
                }
                validationStr = validationStr + " what is recognition.getStatusTypeCode() = " + convertString(recognition.getStatusTypeCode());
                recognitionList.add(recognition);
                if(pax.getManagerPaxId() != null && !managerPaxIdList.contains(pax.getManagerPaxId())){
                    managerPaxIdList.add(pax.getManagerPaxId());
                    validationStr = validationStr + " pax.getManagerPaxId()=" + convertLongToString(pax.getManagerPaxId());
                }

                if (pax.getGroupId() != null) {
                    origAmountByPaxId.put(pax.getPaxId(), origAmountByGroupId.get(pax.getGroupId()));
                }
            }
        }

        nominationDao.save(nomination);
        
        validationStr = validationStr + " Nomination saved nomination.ID=" + convertLongToString(nomination.getId());
        logger.info( validationStr );
        validationStr = "*** buildNominationDetails: ";


        //Create AUXILIARY_NOTIFICATION records for anyone specified in "notify others"
        List<EmployeeDTO> notifyOthers = nominationRequestDTO.getNotifyOthers();
        List<Long> notifyOtherList = new ArrayList<>();

        if (notifyOthers != null && !notifyOthers.isEmpty()) {
            for (EmployeeDTO notifyOther : notifyOthers) {
                notifyOtherList.add(notifyOther.getPaxId());
                AuxiliaryNotification aux = new AuxiliaryNotification();
                aux.setNominationId(nomination.getId());
                aux.setPaxId(notifyOther.getPaxId());
                auxiliaryNotificationDao.save(aux);
            }
        }

        //Only save NEWSFEED_VISIBILTY if the value is TRUE.
        //This means the recognition is private, and should be hidden from the newsfeed.
        if (nominationRequestDTO.getIsPrivate() != null && Boolean.TRUE.equals(nominationRequestDTO.getIsPrivate())) {
            NominationMisc nominationMisc = new NominationMisc();
            nominationMisc.setNominationId(nomination.getId());
            nominationMisc.setVfName(VfName.NEWSFEED_VISIBILITY.name());
            nominationMisc.setMiscData(nominationRequestDTO.getIsPrivate().toString());
            nominationMisc.setMiscDate(new Date());
            nominationMiscDao.save(nominationMisc);
            validationStr = validationStr + " nominationMiscDao.save(nominationMisc) nomination.ID=" + convertLongToString(nomination.getId());
        }

        // We have points, save a misc
        if (Boolean.TRUE.equals(hasPoints)) {
            NominationMisc nominationMiscPayout = new NominationMisc();
            nominationMiscPayout.setNominationId(nomination.getId());
            nominationMiscPayout.setVfName(VfName.PAYOUT_TYPE.name());
            nominationMiscPayout.setMiscData(nominationRequestDTO.getPayoutType());
            nominationMiscPayout.setMiscDate(new Date());
            nominationMiscDao.save(nominationMiscPayout);
            validationStr = validationStr + " nominationMiscDao.save(nominationMiscPayout) nomination.ID=" + convertLongToString(nomination.getId());
        }

        if (nominationRequestDTO.getRecognitionCriteriaIds() != null && !nominationRequestDTO.getRecognitionCriteriaIds().isEmpty()) {
            ArrayList<NominationCriteria> nominationCriteriaList = new ArrayList<>();
            for (Long criteriaId: nominationRequestDTO.getRecognitionCriteriaIds()) {
                NominationCriteria nominationCriteria = new NominationCriteria();
                nominationCriteria.setRecognitionCriteriaId(criteriaId);
                nominationCriteria.setNominationId(nomination.getId());
                nominationCriteriaList.add(nominationCriteria);
            }
            nominationDataBulkSaveDao.saveNominationCriteriaList(nominationCriteriaList);
            validationStr = validationStr + " nominationDataBulkSaveDao.saveNominationCriteriaList(nominationCriteriaList) nomination.ID=" + convertLongToString(nomination.getId());
        }
        
        logger.info( validationStr );
        validationStr = "*** buildNominationDetails: ";

        newsfeedItem.setTargetId(nomination.getId());
        newsfeedItem.setTargetTable(TableName.NOMINATION.name());
        newsfeedItem.setStatusTypeCode(nomination.getStatusTypeCode());
        newsfeedItemDao.save(newsfeedItem);

        for (Recognition recognition: recognitionList) {
            recognition.setNominationId(nomination.getId());
        }
        nominationDataBulkSaveDao.saveRecognitionList(recognitionList);
        validationStr = validationStr + " nominationDataBulkSaveDao.saveRecognitionList(recognitionList) nomination.ID=" + convertLongToString(nomination.getId());

        recognitionList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                .and(ProjectConstants.PARENT_ID).isNull()
                .find(); // reload recognitions (Parent Id check)

        for (Recognition recognition : recognitionList) {
            if (recognition.getProgramAwardTierId() != null && recognition.getAmount() != null && !origAmountByPaxId.isEmpty()) {
                RecognitionMisc recMisc = new RecognitionMisc();
                recMisc.setRecognitionId(recognition.getId());
                validationStr = validationStr + " recognition.getId()=" + convertLongToString(recognition.getId()) + " recognition.getStatusTypeCode()=" + convertString(recognition.getStatusTypeCode() + " nomination.ID=" + convertLongToString(nomination.getId()));
                recMisc.setVfName(VfName.ORIGINAL_AMOUNT.name());
                recMisc.setMiscData(origAmountByPaxId.get(recognition.getReceiverPaxId()).toString());

                recognitionMiscDao.save(recMisc);
                validationStr = validationStr + " recognitionMiscDao.save(recMisc) nomination.ID=" + convertLongToString(nomination.getId());
            }

            if (nominationRequestDTO.getGiveAnonymous() != null && nominationRequestDTO.getGiveAnonymous()) {
                RecognitionMisc giveAnonymousMisc = new RecognitionMisc();
                giveAnonymousMisc.setRecognitionId(recognition.getId());
                validationStr = validationStr + " recognition.getId()=" + convertLongToString(recognition.getId()) + " recognition.getStatusTypeCode()=" + convertString(recognition.getStatusTypeCode() + " nomination.ID=" + convertLongToString(nomination.getId()));
                giveAnonymousMisc.setVfName(VfName.GIVE_ANONYMOUS.name());
                giveAnonymousMisc.setMiscData(nominationRequestDTO.getGiveAnonymous().toString());

                recognitionMiscDao.save(giveAnonymousMisc);
                validationStr = validationStr + " recognitionMiscDao.save(giveAnonymousMisc) nomination.ID=" + convertLongToString(nomination.getId());
            }
        }
        
        logger.info( validationStr );
        validationStr = "*** buildNominationDetails: ";
        
        List<Recognition> approvedRecognitionList = new ArrayList<>();
        for (Recognition recognition : recognitionList) {
            if (recognition.getStatusTypeCode().equalsIgnoreCase(StatusTypeCode.AUTO_APPROVED.name())) {
                approvedRecognitionList.add(recognition);
            }
        }

        if (!approvedRecognitionList.isEmpty()) {
            // get managers specific to the auto_approved recipients
            List<Long> approvedManagerReceiversPaxIdList = new ArrayList<>();
            if (nomination.getStatusTypeCode().equalsIgnoreCase(StatusTypeCode.APPROVED.name())){
                // get all managers
                approvedManagerReceiversPaxIdList.addAll(managerPaxIdList);
            } else {
                // get approved managers
                for (Recognition recognition : approvedRecognitionList) {
                    Long managerPaxId = relationshipDao.findBy()
                            .where(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.name())
                            .and(ProjectConstants.PAX_ID_1).eq(recognition.getReceiverPaxId())
                            .findOne(ProjectConstants.PAX_ID_2, Long.class);
                    if (!approvedManagerReceiversPaxIdList.contains(managerPaxId)) {
                        approvedManagerReceiversPaxIdList.add(managerPaxId);
                    }
                }
            }

            // generate alerts
            alertUtil.generateRecognitionAlerts(nomination, approvedRecognitionList,
                    approvedManagerReceiversPaxIdList, null, notifyOtherList);

            // send emails
            if (notifyRecipient) {	
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.RECOGNITION_PARTICIPANT.getCode(), nomination.getId()));
            }
            if (notifyRecipientManager) {
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.RECOGNITION_MANAGER.getCode(), nomination.getId()));
            }
            if (!notifyOtherList.isEmpty()) {
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.NOTIFY_OTHER.getCode(), nomination.getId()));
            }
        }

        insertTransactionData(nominationRequestDTO, nomination);
        validationStr = validationStr + " insertTransactionData(nominationRequestDTO, nomination) nomination.ID=" + convertLongToString(nomination.getId());

        // create history for auto approved recognitions.
        approvalService.createAutoApprovalHistory(recognitionList, nominationRequestDTO.getProgramId());

        //re-evalute newsfeed status in lieu of some recognition being auto approved.
        if (newsfeedItem != null && !(newsfeedVisibility.equalsIgnoreCase(NewsfeedVisibility.NONE.name()))) {
            int recognitionsApprovedCount = 0;
            try {
                recognitionsApprovedCount = recognitionDao.findBy()
                        .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                        .and(ProjectConstants.STATUS_TYPE_CODE).in(ProjectConstants.APPROVED_STATUSES)
                        .count();
            } catch (Exception e) {
                //no records exists set count to zero
                recognitionsApprovedCount = 0;
            }

            if (recognitionsApprovedCount > 0) {// If at least one recognition is approved, newsfeed item is approved.
                newsfeedItem.setStatusTypeCode(StatusTypeCode.APPROVED.name());
                newsfeedItemDao.update(newsfeedItem);
            }
        }
        
        Map<Integer, List<Recognition>> recognitionsByAmount = new HashMap<>();
        
        for (Recognition recognition : recognitionList) {
        	
        	Integer amount  = getOriginalAmount(nominationRequestDTO, recognition.getReceiverPaxId());
          
        	if(nominationRequestDTO != null && recognition.getReceiverPaxId() != null && submitterPaxId != null &&amount !=null && amount == 0)
                amount= getOriginalAmountForGroupCheckSubmitterPaxID(nominationRequestDTO,recognition.getReceiverPaxId() , submitterPaxId);
        	
            List<Recognition> recByAmount = recognitionsByAmount.get(amount);
            if (recByAmount == null) {
                recByAmount = new ArrayList<>();
            }
            recByAmount.add(recognition);
            recognitionsByAmount.put(amount, recByAmount);
        }

        boolean sendApprovalEmailReceiver = false;
        for (Map.Entry<Integer, List<Recognition>> recognitionAmount: recognitionsByAmount.entrySet()) {
            sendApprovalEmailReceiver |= approvalService.createPendingApprovals(
                    recognitionAmount.getValue(), submitterPaxId, nominationRequestDTO.getProgramId(),
                    recognitionAmount.getKey(), 1L, nomination.getId());
            validationStr = validationStr + " sendApprovalEmailReceiver nomination.getId()=" + convertLongToString(nomination.getId()) + " submitterPaxId=" + convertLongToString(submitterPaxId);
        }

        // only create approval alerts if needed
        if (StatusTypeCode.PENDING.name().equalsIgnoreCase(nomination.getStatusTypeCode())) {
            // create notification for recognitions approvers.
            alertService.addRecognitionApprovalAlert(nomination.getId());

            if (sendApprovalEmailReceiver) {
                //send email alert if in the case there is need for the email
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), nomination.getId()));

                //create notification for recognition submitters who have pending recognitions
                alertService.addPendingRecognitionSubmitterAlert(nomination.getId(), submitterPaxId);
                validationStr = validationStr + " addPendingRecognitionSubmitterAlert nomination.getId()=" + convertLongToString(nomination.getId()) + " submitterPaxId=" + convertLongToString(submitterPaxId);
            }
        }
        
        validationStr = validationStr + " nomination.ID=" + convertLongToString(nomination.getId()) + " submitterPaxId=" + convertLongToString(submitterPaxId) + " sendApprovalEmailReceiver=" + convertBooleanToString(sendApprovalEmailReceiver);
        //buildNominationDetails:
        logger.info( validationStr );
        return getNominationById(nomination.getId().toString(), submitterPaxId);
    }

    @Override
    public List<ErrorMessage> validateNomination(Long paxId,
                                                 NominationRequestDTO nominationDto,
                                                 List<Long> paxIds,
                                                 List<Long> groupIds,
                                                 String validationType,
                                                 Boolean isBatchProcess) {

        List<ErrorMessage> errors = new ArrayList<>();

        String validationStr  = "*** validateNomination:";
        
        logger.info("start validation - " + validationStr);
        validationStr  = "*** validateNomination: ";

        Program program = null;
        if (nominationDto.getProgramId() == null) {
            errors.add(NominationConstants.PROGRAM_ID_MISSING);
        } else {
            //program validation: prevent to give from a POINT LOAD program type.
            program = programDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(nominationDto.getProgramId())
                    .findOne();
            validationStr = validationStr + " ProgramId=" + convertLongToString(nominationDto.getProgramId());
            if (program == null) {
                errors.add(NominationConstants.INVALID_PROGRAM);
            } else if (program.getProgramTypeCode().equals(NominationConstants.PROGRAM_POINT_LOAD_TYPE)) {
                // Can't give from POINT_LOAD programs here
                errors.add(NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION_MESSAGE);
            } else if (
                    !validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE)
                    && ProgramTypeEnum.AWARD_CODE.name().equalsIgnoreCase(program.getProgramTypeCode())
                ) {
                // Can't give from AWARD_CODE programs unless you're actually going through the AWARD_CODE flow.
                errors.add(NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION_MESSAGE);
            } else {
            	validationStr = validationStr + " ProgramTypeCode=" + convertString(program.getProgramTypeCode()) + " paxId=" + convertLongToString(paxId);
                logger.info("start pax validation - " + validationStr);
                validationStr  = "*** validateNomination: ";
                // award code recs don't have recipients defined here.
                if (!programEligibilityQueryDao.canPaxGiveFromProgram(paxId, nominationDto.getProgramId())) {
                    errors.add(NominationConstants.INELIGIBLE_SUBMITTER);
                }

                //  Make sure that everyone can give / receive from the program
                if (!validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE)) {
                    List<Map<String, Object>> recipientMapList =
                            programEligibilityQueryDao.getEligibleReceiversForProgram(nominationDto.getProgramId(),
                                                                                      paxIds,
                                                                                      groupIds);

                    if (!recipientMapList.isEmpty()) {
                        List<Long> recipientPaxIds = new ArrayList<>();
                        List<Long> recipientGroupIds = new ArrayList<>();

                        // Build a list of the eligible pax IDs and group IDs
                        for (Map<String, Object> recipientMap : recipientMapList) {
                            Long subjectId = (Long) recipientMap.get(ProjectConstants.SUBJECT_ID);
                            if (AclTypeCode.PAX.name().equals(recipientMap.get(ProjectConstants.SUBJECT_TABLE))) {
                                recipientPaxIds.add(subjectId);
                            }
                            if (AclTypeCode.GROUPS.name().equals(recipientMap.get(ProjectConstants.SUBJECT_TABLE))) {
                                recipientGroupIds.add(subjectId);
                            }
                        }

                        // Remove all the eligible recipients
                        List<Long> ineligiblePaxIds = new ArrayList<>(paxIds);
                        ineligiblePaxIds.removeAll(recipientPaxIds);
                        List<Long> ineligibleGroupIds = new ArrayList<>(groupIds);
                        ineligibleGroupIds.removeAll(recipientGroupIds);

                        for (Long ineligiblePaxId : ineligiblePaxIds) {
                            errors.add(new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER)
                                    .setMessage(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER_MSG)
                                    .setField(NominationConstants.RECEIVERS_PAX + ineligiblePaxId));
                        }

                        for (Long ineligibleGroupId : ineligibleGroupIds) {
                            errors.add(new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_INELIGIBLE_GROUP_RECEIVER)
                                    .setMessage(NominationConstants.ERROR_INELIGIBLE_GROUP_RECEIVER_MSG)
                                    .setField(NominationConstants.RECEIVERS_GROUP + ineligibleGroupId));
                        }
                    } else {
                        errors.add(NominationConstants.NO_ELIGIBLE_RECEIVERS);
                    }
                }
            }
        }

        if (!CollectionUtils.isEmpty(errors)) {
        	logger.info("ending  validation due to errors - " + validationStr);
            return errors; // can't go further
        }

        logger.info("start logged in user validation - " + validationStr);
        validationStr  = "*** validateNomination: ";

        SysUser validSysUser = sysUserDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.STATUS_TYPE_CODE).notIn(NominationConstants.STATUSES_THAT_CANT_GIVE_REC)
                .findOne();

        if (validSysUser == null) {
            errors.add(NominationConstants.ERROR_SUBMITTER_CANNOT_RECOGNIZE_MESSAGE);
        }

        if (!CollectionUtils.isEmpty(paxIds)) {
            // Check if they're trying to self-recognize (will be dropped from groups during save,
            //only worry about directly including themselves)
            if (paxIds.contains(paxId)) {
                errors.add(NominationConstants.ERROR_CANNOT_RECOGNIZE_SELF_MESSAGE);
            }

            Set<Long> uniquePaxIds = new HashSet<> (paxIds);
            if (uniquePaxIds.size() != paxIds.size()) {
                errors.add(NominationConstants.ERROR_RECEIVER_DUPLICATED_MESSAGE);
            }
        }

        boolean budgetIdValid = nominationDto.getBudgetId() != null && !nominationDto.getBudgetId().equals(0L);

        if (validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE) &&
                nominationDto instanceof AwardCodeRequestDTO) {

            AwardCodeRequestDTO awardCodeDto = (AwardCodeRequestDTO) nominationDto;

            if (CollectionUtils.isEmpty(awardCodeDto.getReceivers())) {
                errors.add(NominationConstants.MISSING_RECIEVERS_MESSAGE);

                return errors; // can't go further
            }

            if (awardCodeDto.getReceivers().size() > 1) {
                errors.add(NominationConstants.MULTIPLE_RECEIVERS_AWARD_CODE_ERROR_MESSAGE);
            }

            if (awardCodeDto.getAwardCode() != null) {
                // validate quantity
                AwardCodeInfoDTO awardCodeInfo = awardCodeDto.getAwardCode();
                // null or less than 1.
                if (awardCodeInfo.getQuantity() == null || awardCodeInfo.getQuantity().compareTo(1) < 0) {
                    errors.add(NominationConstants.INVALID_QUANTITY_MESSAGE);
                } else if (awardCodeInfo.getQuantity() > NominationConstants.AWARD_CODE_MAX_CERTIFICATE_QUANTITY) {
                    errors.add(NominationConstants.ERROR_TOO_MANY_CODES_ERROR_MESSAGE);
                }

                if (NominationConstants.AWARD_CODE_RECOGNITION_PRINT_VALIDATION_TYPE.equals(validationType)) {
                	validationStr = validationStr + " validationType=" + convertString(validationType);
                	// Check if the program is printable
                    ProgramMisc printableMisc = programMiscDao.findBy()
                            .where(ProjectConstants.PROGRAM_ID).eq(program.getProgramId())
                            .and(ProjectConstants.VF_NAME).eq(VfName.IS_PRINTABLE.name())
                            .findOne();
                    if (printableMisc == null ||
                            Boolean.FALSE.equals(Boolean.parseBoolean(printableMisc.getMiscData()))) {
                        errors.add(NominationConstants.NOT_PRINTABLE_ERROR_MESSAGE);
                    }

                    // validate certificate id
                    if (awardCodeInfo.getCertId() == null || awardCodeInfo.getCertId().equals(0L)) {
                        errors.add(NominationConstants.MISSING_CERT_ERROR_MESSAGE);
                    } else {
                        Files certificate = filesDao.findById(awardCodeInfo.getCertId());
                        if (certificate == null) {
                            errors.add(NominationConstants.INVALID_CERT_ERROR_MESSAGE);
                        } else if (!FileTypes.AWARD_CODE_CERT.name().equalsIgnoreCase(certificate.getFileTypeCode())) {
                            errors.add(NominationConstants.INVALID_CERT_ERROR_MESSAGE);
                        } else {
                            FileItem fileItem = fileItemDao.findBy()
                                    .where(ProjectConstants.TARGET_ID).eq(program.getProgramId())
                                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.PROGRAM.name())
                                    .and(ProjectConstants.FILES_ID).eq(awardCodeInfo.getCertId())
                                    .findOne();
                            if (fileItem == null) {
                                errors.add(NominationConstants.CERT_NOT_CONFIGURED_ERROR_MESSAGE);
                            }
                            validationStr = validationStr + " TableName.PROGRAM.name=" + convertString(TableName.PROGRAM.name());
                        }
                    }

                }

                // make sure giveAnonymous is populated
                if (awardCodeInfo.getGiveAnonymous() == null) {
                    errors.add(NominationConstants.MISSING_GIVE_ANONYMOUS_ERROR_MESSAGE);
                }
            } else {
                errors.add(NominationConstants.MISSING_AWARD_CODE_ATTRIBUTE_ERROR_MESSAGE);
            }

            if (!errors.isEmpty()) {
                return errors; // otherwise we'll get errors later
            }
        }

        logger.info("start award amount validation - " + validationStr);
        validationStr  = "*** validateNomination: ";

        boolean awardTierValid = true;
        boolean awardAmountValid = true;
        boolean isGroupAsReceiver = false;
        boolean errorFound = false;

        boolean hasAdvRecPermission = true;
        if (!Boolean.TRUE.equals(isBatchProcess)) {
            hasAdvRecPermission = security.hasPermission(ProjectConstants.ENABLE_ADVANCED_REC_PERMISSION);
        }

        Long awardTierTmp = null;
        Integer originalAmountTmp = null;
        Lookup matchedPayoutType = null;
        if (nominationDto.getPayoutType() != null) {
            matchedPayoutType = lookupRepository.findBy()
                    .where("lookupCode").eq("PAYOUT_TYPE")
                    .and("lookupDesc").eq(nominationDto.getPayoutType())
                    .findOne();
        }
        for (GroupMemberAwardDTO receiver : nominationDto.getReceivers()) {

            // Different original amounts are allowed only for advanced recognition
            isBatchProcess |=  originalAmountTmp != null && !receiver.getOriginalAmount().equals(originalAmountTmp);

            // Checking if there is a groupId as receiver.
            isGroupAsReceiver |= receiver.getGroupId() != null;

            // AwardAmount is not valid if it is null, empty or if it has different values when
            //submitter doesn't have adv recognition permissions.
            awardAmountValid = !( receiver.getAwardAmount() == null || receiver.getAwardAmount().equals(0) ||
                    (!hasAdvRecPermission && isBatchProcess) );

            // AwardTier is not valid if it is null, empty or if it has different values when
            //submitter doesn't have adv recognition permissions.
            awardTierValid = !(
                    receiver.getAwardTierId() == null || receiver.getAwardTierId().equals(0L) ||
                            (!hasAdvRecPermission && awardTierTmp != null && !receiver.getAwardTierId().equals(awardTierTmp))
            );

            if (isBatchProcess && !hasAdvRecPermission) {
                errors.add(NominationConstants.ADVANCED_RECOGNITION_NOT_ALLOWED_MESSAGE);

                break;
            }
            if (!validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE)
                    && receiver.getPaxId() == null
                    && receiver.getGroupId() == null) {
                errors.add(NominationConstants.RECEIVER_REQUIRED_MESSAGE);

                break;
            }

            if (awardAmountValid && !awardTierValid && !program.getProgramTypeCode().equals(ProgramTypeEnum.MILESTONE.getCode())) {
                errors.add(NominationConstants.AWARD_TIER_MISSING_MESSAGE);

                errorFound = true;

            } else if (!awardAmountValid && awardTierValid) {
                errors.add(NominationConstants.ERROR_AWARD_AMOUNT_MISSING_MESSAGE);

                errorFound = true;
            }

            if (awardAmountValid && !budgetIdValid) {
                errors.add(NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT_MESSAGE);

                errorFound = true;

            } else if (!awardAmountValid && budgetIdValid && !program.getProgramTypeCode().equals(ProgramTypeEnum.MILESTONE.getCode())) {
                errors.add(NominationConstants.AWARD_AMOUNT_REQUIRED_FOR_BUDGET_MESSAGE);

                errorFound = true;
            }

            // Groups are not allowed on advanced recognition.
            if (isBatchProcess && isGroupAsReceiver) {
                errors.add(NominationConstants.GROUPS_NOT_ALLOWED_MESSAGE);

                errorFound = true;
            }

            // if there is a budget / award Amount, there MUST be a payoutType / payoutType should be a valid value
            if (awardAmountValid && budgetIdValid && matchedPayoutType == null) {
                    errors.add(NominationConstants.PAYOUT_TYPE_INVALID_MESSAGE);
                    errorFound = true;
            }
                        
            originalAmountTmp = receiver.getOriginalAmount();
            awardTierTmp = receiver.getAwardTierId();            

            // If budget is valid means recognitions will include points, then original amount variable should not be null
            if (budgetIdValid && originalAmountTmp == null) {                
                    errors.add(NominationConstants.AWARD_AMOUNT_REQUIRED_FOR_BUDGET_MESSAGE);
                    errorFound = true;                
            }
             
            if (errorFound) {
                break;
            }

            originalAmountTmp = receiver.getOriginalAmount();
            awardTierTmp = receiver.getAwardTierId();
            validationStr = validationStr + " originalAmountTmp=" + convertIntegerToString(originalAmountTmp) + " awardTierTmp=" + convertLongToString(awardTierTmp);
        }

        logger.info("start budget eligibility validation - " + validationStr);
        validationStr  = "*** validateNomination: ";

        // Validating budget configuration
        if (awardAmountValid && budgetIdValid && (awardTierValid || program.getProgramTypeCode().equals(ProgramTypeEnum.MILESTONE.getCode()))) {
        	
        	validationStr = validationStr + " awardAmountValid=" + String.valueOf(awardAmountValid) + " budgetIdValid=" + convertBooleanToString(budgetIdValid) + " awardTierValid=" + String.valueOf(awardTierValid);
            
        	//Validate that the pax can give from this program/budget combination
        	List<BudgetEligibilityDTO> budgetEligibilityList = getEligibleBudgetList(paxId);

            if (CollectionUtils.isEmpty(budgetEligibilityList)) {
                // Pax can't give from any budget
                errors.add(NominationConstants.INVALID_BUDGET_MESSAGE);
            } else {
                //validate budget status & determine how much is left in the budget
                BudgetEligibilityDTO matchedBudget = null;
                for (BudgetEligibilityDTO budget : budgetEligibilityList) {
                    if (!nominationDto.getBudgetId().equals(budget.getBudgetId())
                            || !nominationDto.getProgramId().equals(budget.getProgramId())
                            ) {
                        continue; // skip this budget, not the one provided
                    }
                    matchedBudget = budget; // budget provided is configured for the program
                    break;
                }

                if (matchedBudget != null) {
                    // get budget details
                    List<Map<String, Object>> eligibleBudgetList =
                            budgetInfoDao.getEligibleBudgets(paxId, Arrays.asList(matchedBudget.getBudgetId()), Boolean.FALSE);

                    Map<String, Object> matchedBudgetDetails = eligibleBudgetList.get(0);
                    if (matchedBudgetDetails != null) {
                        Integer paxCount = 0;
                        if (validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE) &&
                                nominationDto instanceof AwardCodeRequestDTO) {
                            AwardCodeRequestDTO awardCodeDto = (AwardCodeRequestDTO) nominationDto;
                            paxCount = awardCodeDto.getAwardCode().getQuantity();
                        } else {
                            paxCount = paxCountDao.getCountDistinctPax(groupIds, paxIds, false);
                        }
                        errors.addAll(checkBudgetRemainingForIndividual(nominationDto, paxId, paxCount, matchedBudgetDetails, program.getProgramTypeCode()));
                    } else {
                        // We can't get details for the budget for the pax, throw an error
                        errors.add(NominationConstants.INVALID_BUDGET_MESSAGE);
                    }
                } else {
                    // Can't give from this budget for this program
                    errors.add(NominationConstants.INVALID_BUDGET_MESSAGE);
                }
            }
        }

        if (!validationType.equals(NominationConstants.RAISE)) {
            // Headline validation
            if (nominationDto.getHeadline() == null
                    || nominationDto.getHeadline().trim().equals(ProjectConstants.EMPTY_STRING)) {
                //Don't throw the error for AWARD_CODE programs
                if (!validationType.startsWith(NominationConstants.AWARD_CODE_RECOGNITION_TYPE)) {
                    errors.add(NominationConstants.HEADLINE_MISSING_MESSAGE);
                }
            } else if (nominationDto.getHeadline().length() > NominationConstants.MAX_HEADLINE_CHAR_LENGTH) {
                errors.add(NominationConstants.HEADLINE_TOO_LONG_MESSAGE);
            }

            if (nominationDto.getComment() != null
                    && nominationDto.getComment().length() > NominationConstants.MAX_COMMENT_CHAR_LENGTH) {
                errors.add(NominationConstants.COMMENT_TOO_LONG_MESSAGE);
            }

            //ecard validation
            if (nominationDto.getImageId() != null) {
                List<Long> validEcardIds = programEcardDao.findBy()
                        .where(ProjectConstants.PROGRAM_ID).eq(nominationDto.getProgramId())
                        .find(ProjectConstants.ECARD_ID, Long.class);
                if (!CollectionUtils.isEmpty(validEcardIds)) {
                    if (!validEcardIds.contains(nominationDto.getImageId())) {
                        errors.add(NominationConstants.ECARD_INVALID_MESSAGE);
                    }
                } else {
                    errors.add(NominationConstants.ECARD_NOT_SET_UP_MESSAGE);
                }
            }

            // criteria validation
            List<Long> validCriteriaIds = programCriteriaDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(nominationDto.getProgramId())
                    .find(ProjectConstants.RECOGNITION_CRITERIA_ID, Long.class);
            if (!CollectionUtils.isEmpty(validCriteriaIds)) {
                if (CollectionUtils.isEmpty(nominationDto.getRecognitionCriteriaIds())) {
                    errors.add(NominationConstants.CRITERIA_REQUIRED_MESSSAGE);
                } else if (!validCriteriaIds.containsAll(nominationDto.getRecognitionCriteriaIds())) {
                    errors.add(NominationConstants.CRITERIA_INVALID_MESSAGE);
                }
            } else if (!CollectionUtils.isEmpty(nominationDto.getRecognitionCriteriaIds())) {
                errors.add(NominationConstants.CRITERIA_NOT_SET_UP_MESSAGE);
            }
        }

        logger.info("finished validation - " + validationStr);
        return errors;
    }

    @Override
    public void insertTransactionData(NominationRequestDTO nominationDto, Nomination nomination) {
        if (nominationDto.getReceivers().get(0).getAwardAmount() != null &&
                nominationDto.getBudgetId() != null && nominationDto.getReceivers().get(0).getAwardAmount() > 0) {
            // save these so we can use to easily create budget allocations for all transactions.
        	logger.info("insertTransactionData() starting - ln 1011");
        	createBatchLinkingEvents(nomination);
            nominationDataBulkSaveDao.createTransactionHeaderAndDiscretionaryEntries(nomination);
            
            // Create TRANSACTION_HEADER_MISC records

            // Payout Type and Default project/subproject info
            transactionHeaderMiscDao.createPayoutType();
            transactionHeaderMiscDao.createDefaultProjectSubproject();

            // Global or US overrides for project/subproject
            BudgetMisc bm = budgetMiscDao.findBy()
                    .where(ProjectConstants.BUDGET_ID).eq(nomination.getBudgetId())
                    .and(ProjectConstants.MISC_TYPE_CODE).eq(VfName.SUB_PROJECT_OVERRIDE.name()).findOne();

            if (bm != null) {
                if (VfName.US_OVERRIDE.name().equals(bm.getVfName())) {
                    transactionHeaderMiscDao.createUsOverrideProjectSubproject();
                } else if (VfName.NON_US_OVERRIDE.name().equals(bm.getVfName())) {
                    transactionHeaderMiscDao.createNonUsOverrideProjectSubproject();
                }
            }

            // Site-level configurable override for subproject
            if (environment.getProperty(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE) != null) {
                transactionHeaderMiscDao.createConfigurableSubprojectOverride();
            }
        }
        logger.info("Finishing insertTransactionData() - ln 1039");
    }

    /**
     * This method takes a nomination and writes to the batch and batch event tables
     * used for bulk nominations (tying budgets to allocations)
     * @param nomination Nomination for which the batch linking events are written to the db
     */
    private void createBatchLinkingEvents(Nomination nomination) {
        Batch linkingBatch = new Batch();
        linkingBatch.setBatchTypeCode(BatchType.NOM_TH_LINK.getCode());
        linkingBatch.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
        batchDao.save(linkingBatch);

        BatchEvent linkingBatchEvent = new BatchEvent();
        linkingBatchEvent.setBatchEventTypeCode(BatchEventType.NOM_TH_LINK.getCode());
        linkingBatchEvent.setBatchId(linkingBatch.getBatchId());
        linkingBatchEvent.setTargetTable(TableName.NOMINATION.name());
        linkingBatchEvent.setTargetId(nomination.getId());
        linkingBatchEvent.setMessage(ProjectConstants.EMPTY_STRING + nomination.getId());
        linkingBatchEvent.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
        batchEventDao.save(linkingBatchEvent);
    }

    /**
     * This method checks the amount remaining on a budget and
     * returns errors based on whether or not it has enough in it
     * @param nominationDto Nomination record used for amount and budget
     * @param paxId paxId of the submitter
     * @param paxCount Number of pax Id's trying to count a budget against
     * @param budget budget being used
     * @return List of error messages based on whether or not there is enough in the given budget to give from
     */
    private List<ErrorMessage> checkBudgetRemainingForIndividual(NominationRequestDTO nominationDto,
            Long paxId, int paxCount, Map<String, Object> budget, String programTypeCode) {
        List<ErrorMessage> errors = new ArrayList<>();

        String status = StatusTypeUtil.getImpliedStatus((String) budget.get(ProjectConstants.STATUS_TYPE_CODE),
                (Date) budget.get(ProjectConstants.FROM_DATE), (Date) budget.get(ProjectConstants.THRU_DATE));
        if (!status.equals(StatusTypeCode.ACTIVE.name())) {
            errors.add(NominationConstants.BUDGET_NOT_ACTIVE_MESSAGE);
        }

        Double nomAmount = 0d;

        for (GroupMemberAwardDTO receiver : nominationDto.getReceivers()) {
            nomAmount += (double) (receiver.getAwardAmount());
        }

        if (nominationDto instanceof AwardCodeRequestDTO) { // otherwise we're just grabbing 10 points
            nomAmount *= paxCount;
        }


        Double totalAmount = 0D;
        Double totalUsed = 0D;

        // handle TOTAL_AMOUNT (totalAmount) and TOTAL_USED (usedAmount)
        List<Map<String, Object>> budgetTotalsList =  budgetInfoDao.getBudgetTotalsFlat(Arrays.asList(nominationDto.getBudgetId()));

        if (!CollectionUtils.isEmpty(budgetTotalsList)) {
            Map<String, Object> budgetTotalsEntry = budgetTotalsList.get(0);
            totalAmount = (Double) budgetTotalsEntry.get(ProjectConstants.TOTAL_AMOUNT);
            totalUsed = (Double) budgetTotalsEntry.get(ProjectConstants.TOTAL_USED);
        }

        Double totalRemaining = totalAmount - totalUsed;

        if (nomAmount > totalRemaining) {
            errors.add(NominationConstants.AWARD_AMOUNT_HIGER_THAN_BUDGET_MESSAGE);
        }

        //handle individualGivingLimit, for allocated budgets, the individual's amount is the same as the overall budget amount
        Double individualGivingLimit = 0D;
        if (!BudgetTypeEnum.ALLOCATED.name().equals(budget.get(ProjectConstants.BUDGET_TYPE_CODE))) {
            individualGivingLimit = (Double) budget.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT);
        } else {
            individualGivingLimit = totalAmount;
        }

        if (individualGivingLimit != null) {
            Double individualUsedAmount = budgetInfoDao.getIndividualUsedAmount(paxId, nominationDto.getBudgetId());
            Double totalIndividualRemaining = individualGivingLimit - individualUsedAmount;
            if(nomAmount > totalIndividualRemaining){
                errors.add(NominationConstants.AWARD_AMOUNT_HIGHER_THAN_LIMIT_MESSAGE);
            }
        }
        List<ProgramAwardTier> awardTierList = programAwardTierDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(nominationDto.getProgramId())
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .find();
        if (!programTypeCode.equals(ProgramTypeEnum.MILESTONE.getCode())) {
            errors.addAll(checkProgramAwardTiers(nominationDto, awardTierList));
        }

        return errors;
    }

    /**
     * Service used to check the program award tiers against the nomination request
     * @param nominationDto Nomination whose original amount will be checked against the program award tiers
     * @param awardTierList List of program award tiers whose data allows or won't allow the specific originalAmount
     * @return List of error messages for validation
     */
    private List<ErrorMessage> checkProgramAwardTiers(NominationRequestDTO nominationDto,
            List<ProgramAwardTier> awardTierList) {
        List<ErrorMessage> errors = new ArrayList<>();
        if(CollectionUtils.isEmpty(awardTierList)){
            errors.add(NominationConstants.NO_AWARD_TIERS_CONFIGURED_MESSAGE);
        } else {
            boolean isAmountValid = false;

            for(GroupMemberAwardDTO receiver : nominationDto.getReceivers()) {

                isAmountValid = false;

                if (receiver.getOriginalAmount() != null && !receiver.getOriginalAmount().equals(0)) {
                    for (ProgramAwardTier tier: awardTierList) {
                        if (tier.getId().equals(receiver.getAwardTierId()) &&
                                (tier.getMinAmount() <= receiver.getOriginalAmount() &&
                                (tier.getMaxAmount() == null || receiver.getOriginalAmount() <= tier.getMaxAmount()))) {
                            if (tier.getIncrement().equals(0D) ||
                                    (receiver.getOriginalAmount() - tier.getMinAmount()) % tier.getIncrement() == 0) {
                                isAmountValid = true;
                                break;
                            }
                        }
                    }
                }

                if (!isAmountValid) {
                    break;
                }
            }

            if (isAmountValid == false) {
                errors.add(NominationConstants.ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS_MESSAGE);
            }
        }
        return errors;
    }

    @Override
    public int getNominationCountBySubmitter(Long paxId) {
        return cnxtNominationRepository.getNominationCountBySubmitter(paxId);
    }

    @Override
    public String getLastRecognitionGiven(Long paxId) {

        Nomination recGiven = cnxtNominationRepository.findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                paxId, StatusTypeCode.APPROVED.name());

        if (recGiven != null) {
            Date recGivenDate = recGiven.getSubmittalDate();
            return DateUtil.convertToUTCDateTime(recGivenDate);
        }
        return null;
    }

    @Override
    public NominationDetailsDTO getNominationById(String nominationIdString, Long paxId) {
        NominationDetailsDTO nominationDetailsDto = null;
        boolean populatePrivate = false;
        String commentNom = null;
        Long nominationId;

        try {
            nominationId = Long.valueOf(nominationIdString);
        } catch(NumberFormatException ne) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                .setCode(NominationConstants.ERROR_INVALID_NOMINATION)
                .setField(ProjectConstants.NOMINATION_ID)
                .setMessage(NominationConstants.ERROR_INVALID_NOMINATION_MSG));
        }
        //Check if user is Admin
        if (security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE) &&
                security.isMyPax(paxId)) {
            populatePrivate = true;
        }

        Nomination nomination = cnxtNominationRepository.findOne(nominationId);

        if (nomination != null) {
            nominationDetailsDto = new NominationDetailsDTO();
            nominationDetailsDto.setNominationId(nomination.getId());
            nominationDetailsDto.setStatus(nomination.getStatusTypeCode());
            nominationDetailsDto.setGiverPax(nomination.getSubmitterPaxId());
            nominationDetailsDto.setProgramId(nomination.getProgramId());
            nominationDetailsDto.setHeadline(nomination.getHeadlineComment());
            nominationDetailsDto.setBudgetId(nomination.getBudgetId());
            nominationDetailsDto.setCreateDate(nomination.getSubmittalDate().toString());

            // get comment to populate if it is required, depending on user role.
            commentNom = nomination.getComment();

            // set flag if user is the giver
            if (nominationDetailsDto.getGiverPax().equals(paxId)) {
                populatePrivate = true;
            }

            //GET PROGRAM_TYPE
            Program program = programDao.findById(nominationDetailsDto.getProgramId());
            ProgramTypeEnum programType = null;

            if (program != null) {
                programType = ProgramTypeEnum.programTypeByCode(program.getProgramTypeCode());

                if (programType != null) {
                    // Set program type
                    nominationDetailsDto.setProgramType(programType.name());
                } else { // reminder to add new program types into ProgramTypeEnum.
                    logger.error("Not valid program type code for program " + program.getProgramId());
                }
                nominationDetailsDto.setProgramName(program.getProgramName());
            }

            //GET PAYOUT TYPE
            populatePayoutType(nominationDetailsDto);

            //GET IMAGE_ID
            populateImageId(nominationDetailsDto, programType.name());

            //GET RECOGNITIONS AND ALERTS INFO
            populateRecognitionsAndAlerts(nominationDetailsDto, populatePrivate, paxId);

            //GET RECOGNITION CRITERIA
            populateRecognitionCriteria(nominationDetailsDto);

            //GET RECOGNITION PRIVACY (only get if isPrivate is not null)
            populateRecognitionPrivacy(nominationDetailsDto);

            //Get Newsfeed Item id
            Long newsfeedItemId = newsfeedItemDao.findBy()
                    .where(ProjectConstants.NEWSFEED_ITEM_TYPE_CODE).in(NewsfeedItemTypeCode.allTypes())
                    .and(ProjectConstants.TARGET_ID).eq(nominationId)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                    .findOne(ProjectConstants.ID,Long.class);

            nominationDetailsDto.setNewsFeedItemId(newsfeedItemId);

            //GET NOTIFY_OTHERS
            List<EmployeeDTO> notifyOthers = new ArrayList<>();
            List<Long> notifyOtherPaxIds = auxiliaryNotificationDao.findBy()
                    .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                    .find(ProjectConstants.PAX_ID, Long.class);

            if (notifyOtherPaxIds != null && !notifyOtherPaxIds.isEmpty()) {
                for (Long id : notifyOtherPaxIds) {
                    EmployeeDTO pax = participantInfoDao.getEmployeeDTO(id);
                    notifyOthers.add(pax);
                }
            }
            nominationDetailsDto.setNotifyOthers(notifyOthers);

            //Populate Comment, AwardAmount only if pax logged is giver, receiver or an admin
            if (populatePrivate) {
                nominationDetailsDto.setComment(commentNom);
            } else {
                nominationDetailsDto.setComment(null);
            }

            if (ProgramTypeEnum.AWARD_CODE.equals(programType)) {
                //Populate award code information
                populateAwardCodeDetail(nominationDetailsDto);
            }

            return nominationDetailsDto;
        }

        throw new ErrorMessageException().addErrorMessage(
            new ErrorMessage()
                .setCode(NominationConstants.ERROR_NOMINATION_NOT_EXIST)
                .setField(ProjectConstants.NOMINATION_ID)
                .setMessage(NominationConstants.ERROR_NOMINATION_NOT_EXIST_MSG));
    }

    @Override
    public List<NominationDetailsDTO> getNominationDetails(String nominationIdList, Long paxId) {
        List<NominationDetailsDTO> nominationDetailsList = new ArrayList<>();
        boolean populatePrivate = false;

        //Make sure at least one nominationID was passed in
        List<ErrorMessage> errors = new ArrayList<>();
        if (nominationIdList == null || nominationIdList.isEmpty()) {
            errors.add(NominationConstants.MISSING_NOMINATION_ID_MESSAGE);
        } else if (!nominationIdList.matches(ProjectConstants.REGEX_DELIM_COMMA_DIGITS)) {
            // Checking for valid delimited nominationId list format (only digits are allowed).
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_INVALID_NOMINATION)
                    .setField(ProjectConstants.NOMINATION_IDS)
                    .setMessage(NominationConstants.ERROR_INVALID_NOMINATION_MSG));
        }

        //Handle errors
        ErrorMessageException.throwIfHasErrors(errors);

        String[] nominationIdStrings = nominationIdList.split(ProjectConstants.COMMA_DELIM);
        List<Long> nominationIds = new ArrayList<>();
        for (String id : nominationIdStrings) {
            nominationIds.add(Long.valueOf(id));
        }

        //Check if user is Admin
        if (security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE) &&
                security.isMyPax(paxId)) {
            populatePrivate = true;
        }

        //Get base nomination info
        List<Map<String, Object>> nodes = nominationDetailsDao.getNominationCommonInfo(nominationIds);

        if (!CollectionUtils.isEmpty(nodes)) {
            for (Map<String, Object> node : nodes) {
                NominationDetailsDTO nominationDetailsDto = new NominationDetailsDTO();

                //Map the common information
                nominationDetailsDto.setNominationId((Long) node.get(ProjectConstants.NOMINATION_ID));
                nominationDetailsDto.setStatus((String) node.get(ProjectConstants.STATUS_TYPE_CODE));
                nominationDetailsDto.setGiverPax((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
                nominationDetailsDto.setProgramId((Long) node.get(ProjectConstants.PROGRAM_ID));
                nominationDetailsDto.setHeadline((String) node.get(ProjectConstants.HEADLINE_COMMENT));
                if (node.get(ProjectConstants.PAYOUT_TYPE) != null) {
                    String payoutType = (String) node.get(ProjectConstants.PAYOUT_TYPE);
                    nominationDetailsDto.setPayoutType(payoutType);
                    String displayName = lookupRepository.findBy()
                            .where("lookupCategoryCode").eq(payoutType)
                            .and("lookupCode").eq("DISPLAY_NAME")
                            .findOne("lookupDesc", String.class);
                    if (displayName != null) {
                        nominationDetailsDto.setPayoutDisplayName(displayName);
                    }
                }
                if (node.get(ProjectConstants.BUDGET_ID) != null)
                    nominationDetailsDto.setBudgetId((Long) node.get(ProjectConstants.BUDGET_ID));
                nominationDetailsDto.setProgramType((String) node.get(ProjectConstants.PROGRAM_TYPE_CODE));
                nominationDetailsDto.setProgramName((String) node.get(ProjectConstants.PROGRAM_NAME));
                if (node.get(ProjectConstants.NEWSFEED_ITEM_ID) != null)
                    nominationDetailsDto.setNewsFeedItemId((Long) node.get(ProjectConstants.NEWSFEED_ITEM_ID));
                nominationDetailsDto.setCreateDate((String) node.get(ProjectConstants.SUBMITTAL_DATE));

                // get comment to populate if it is required, depending on user role.
                String commentNom = (String) node.get(ProjectConstants.COMMENT);


                //set flag if user is the giver
                if (nominationDetailsDto.getGiverPax().equals(paxId)) {
                    populatePrivate = true;
                }

                //Image ID
                populateImageId(nominationDetailsDto, nominationDetailsDto.getProgramType());

                //Recognitions and AlertIds
                populateRecognitionsAndAlerts(nominationDetailsDto, populatePrivate, paxId);

                //Recognition Criteria
                populateRecognitionCriteria(nominationDetailsDto);

                //Populate Comment, AwardAmount only if pax logged is giver, receiver or an admin
                if (populatePrivate) {
                    nominationDetailsDto.setComment(commentNom);
                } else {
                    nominationDetailsDto.setComment(null);
                }

                //Award Code Detail
                if (nominationDetailsDto.getProgramType().equals(ProgramTypeEnum.AWARD_CODE.name())) {
                    populateAwardCodeDetail(nominationDetailsDto);
                }

                nominationDetailsList.add(nominationDetailsDto);
            }
        }

        return nominationDetailsList;
    }

    /**
     * Helper method that populates the programType and imageId on the passed in NominationDetailsDTO
     * If programType is AWARD_CODE, imageId will be the iconID
     * Otherwise, imageId will be the ecardID
     * @param nominationDetailsDto - DTO to add the programType and imageId to
     * @param programType - Type of program. Determines if the ID is for an icon or ecard
     */
    private void populateImageId(NominationDetailsDTO nominationDetailsDto, String programType) {
        // set image id; AWARD_CODE = program award code icon, otherwise ecarId
        if (ProgramTypeEnum.AWARD_CODE.name().equals(programType)) {
            List<Long> filesIds = fileItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetailsDto.getProgramId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.PROGRAM.name())
                .find(ProjectConstants.FILES_ID, Long.class);

            Long programIconId = filesDao.findBy()
                    .where(ProjectConstants.ID).in(filesIds)
                    .and(ProjectConstants.FILE_TYPE_CODE).eq(FileTypes.AWARD_CODE_ICON)
                    .findOne(ProjectConstants.ID, Long.class);

            nominationDetailsDto.setImageId(programIconId);
        } else {
            Long ecardId = nominationDao.findBy()
                .where(ProjectConstants.ID).eq(nominationDetailsDto.getNominationId())
                .and(ProjectConstants.PARENT_ID).isNull()
                .findOne(ProjectConstants.ECARD_ID, Long.class);

            nominationDetailsDto.setImageId(ecardId);
        }
    }

    /**
     * Helper method that populates the isPrivate flag on the passed in NominationDetailsDTO
     * @param nominationDetailsDto - DTO to add the isPrivate flag to
     */
    private void populateRecognitionPrivacy(NominationDetailsDTO nominationDetailsDto) {
        String isPrivate = nominationMiscDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationDetailsDto.getNominationId())
                .and(ProjectConstants.VF_NAME).eq(VfName.NEWSFEED_VISIBILITY.name())
                .findOne(ProjectConstants.MISC_DATA, String.class);

        nominationDetailsDto.setIsPrivate(isPrivate != null ? Boolean.parseBoolean(isPrivate) : Boolean.FALSE);
    }

    /**
     * Helper method that populates the recognitions and alertIds sub-objects on the passed in NominationDetailsDTO
     * @param nominationDetailsDto - DTO to add the recognition and alert sub objects to
     * @param populatePrivate - flag used to determine if the awardAmount should be populated -
     * will be true for admins, givers, and receivers
     * @param paxId - logged in user's paxID
     */
    private void populateRecognitionsAndAlerts(
            NominationDetailsDTO nominationDetailsDto, boolean populatePrivate, Long paxId) {
        List<RecognitionSimpleDTO> recognitionNominationList = new ArrayList<>();
        List<Long> alertIds = new ArrayList<>();

        List<Recognition> recognitionList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationDetailsDto.getNominationId())
                .and(ProjectConstants.PARENT_ID).isNull()
                .find(); // Parent Id check

        for (Recognition recognition: recognitionList) {
            RecognitionSimpleDTO recognitionNom = new RecognitionSimpleDTO();
            recognitionNom.setReceiverPaxId(recognition.getReceiverPaxId());
            recognitionNom.setStatus(recognition.getStatusTypeCode());

            //Populate Amount if user logged is the receiver, giver or an admin
            if (paxId.equals(recognitionNom.getReceiverPaxId()) || populatePrivate) {
                populatePrivate = true;
            }

            if (populatePrivate) {
                recognitionNom.setAwardAmount(recognition.getAmount());
                recognitionNom.setAwardTierId(recognition.getProgramAwardTierId());
            }

            recognitionNominationList.add(recognitionNom);

            //Get alert info for each recognition
            Long alertId = alertDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(recognition.getId())
                    .findOne(ProjectConstants.ID, Long.class);
            if (alertId != null) {
                alertIds.add(alertId);
            }
        }

        //Add Recognition List
        if (!recognitionNominationList.isEmpty()) {
            if (!populatePrivate) { //if pax logged is not related to nomination, erase amount.
                for (RecognitionSimpleDTO recognition: recognitionNominationList ) {
                    recognition.setAwardAmount(null);
                }
            }

            nominationDetailsDto.setRecognitions(recognitionNominationList);
        }

        //Add alert List
        nominationDetailsDto.setAlertIds(alertIds);
    }

    /**
     * Helper method that populates the recognitionCriteria sub-object on the passed in NominationDetailsDTO
     * @param nominationDetailsDto - DTO to add the recognitionCriteria sub-object to
     */
    private void populateRecognitionCriteria(NominationDetailsDTO nominationDetailsDto) {
        //Get Nomination Criteria Ids
        List<Long> criteriaIds = nominationCriteriaDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationDetailsDto.getNominationId())
                .find(ProjectConstants.RECOGNITION_CRITERIA_ID, Long.class);

        List<RecognitionCriteriaSimpleDTO> recognitionCriteriaDTOList = new ArrayList<>();

        if (!criteriaIds.isEmpty()) {
            //Get Recognition Criteria
            List<RecognitionCriteria> recognitionCriteriaList = recognitionCriteriaDao.findBy()
                .where(ProjectConstants.ID).in(criteriaIds)
                .find();

            if (!CollectionUtils.isEmpty(recognitionCriteriaList)) {
                for (RecognitionCriteria rc : recognitionCriteriaList) {
                    RecognitionCriteriaSimpleDTO recognitionCriteriaDTO = new RecognitionCriteriaSimpleDTO();
                    recognitionCriteriaDTO.setId(rc.getId());
                    recognitionCriteriaDTO.setDisplayName(rc.getRecognitionCriteriaName());
                    recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
                }
            }
        }
        nominationDetailsDto.setRecognitionCriteria(recognitionCriteriaDTOList);
    }

    /**
     * Helper method that populates the awardCodeDetail sub-object on the passed in NominationDetailsDTO
     * @param nominationDetailsDto - DTO to add the awardCodeDetail sub-object to
     */
    private void populateAwardCodeDetail(NominationDetailsDTO nominationDetailsDto) {
        AwardCodeNominationDetailsDTO awardCodeDetailDTO = new AwardCodeNominationDetailsDTO();

        //Get recognitionId
        Long recognitionId = recognitionDao.findBy()
            .where(ProjectConstants.NOMINATION_ID).eq(nominationDetailsDto.getNominationId())
            .and(ProjectConstants.PARENT_ID).isNull()
            .findOne(ProjectConstants.ID, Long.class);

        //awardCode
        RecognitionMisc awardCodeMisc = recognitionMiscDao.findBy()
            .where(ProjectConstants.RECOGNITION_ID).eq(recognitionId)
            .and(ProjectConstants.VF_NAME).eq(VfName.AWARD_CODE)
            .findOne();
        if (awardCodeMisc != null) {
            awardCodeDetailDTO.setAwardCode(awardCodeMisc.getMiscData());
        } else {
            logger.error("RecognitionMisc - AWARD_CODE not saved for nomination ID {0}", nominationDetailsDto.getNominationId());
        }

        //certId
        RecognitionMisc certIdMisc = recognitionMiscDao.findBy()
            .where(ProjectConstants.RECOGNITION_ID).eq(recognitionId)
            .and(ProjectConstants.VF_NAME).eq(VfName.CERT_ID)
            .findOne();

        Long certId = null;
        if (certIdMisc != null) {
            try {
                certId = new Long(certIdMisc.getMiscData());
            }
            catch (NumberFormatException nfe) {
                logger.error("Error! CertificateID is not valid for nomination ID {0}",nominationDetailsDto.getNominationId());
            }
            awardCodeDetailDTO.setCertId(certId);
        } else {
            logger.error("RecognitionMisc - CERT_ID not saved for nomination ID {0}",nominationDetailsDto.getNominationId());
        }

        //claimingInstructions
        ProgramMisc claimingInstructionsMisc = programMiscDao.findBy()
            .where(ProjectConstants.PROGRAM_ID).eq(nominationDetailsDto.getProgramId())
            .and(ProjectConstants.VF_NAME).eq(VfName.CLAIM_INSTRUCTIONS)
            .findOne();

        if (claimingInstructionsMisc != null) {
            awardCodeDetailDTO.setClaimingInstructions(claimingInstructionsMisc.getMiscData());
        } else {
            logger.error("ProgramMisc - CLAIMING_INSTRUCTIONS not saved for nomination ID {0}",nominationDetailsDto.getNominationId());
        }

        //fileName
        if (certId != null) {
            String filename = filesDao.findBy()
                .where(ProjectConstants.ID).eq(certId)
                .findOne(ProjectConstants.NAME, String.class);

            awardCodeDetailDTO.setFileName(filename);
        }

        nominationDetailsDto.setAwardCodeDetail(awardCodeDetailDTO);
    }

    /**
     * Helper method that populates the payout type on the passed in NominationDetailsDTO
     * @param nominationDetailsDto - DTO to add the payout type to
     */
    private void populatePayoutType(NominationDetailsDTO nominationDetailsDto) {
        String payoutType = nominationMiscDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationDetailsDto.getNominationId())
                .and(ProjectConstants.VF_NAME).eq(VfName.PAYOUT_TYPE.name())
                .findOne(ProjectConstants.MISC_DATA, String.class);
        nominationDetailsDto.setPayoutType(payoutType);
    }

    @Override
    public RecognitionStatsDTO getRecognitionStats(Long paxId) {
        RecognitionStatsDTO recognitionStats = new RecognitionStatsDTO();

        recognitionStats.setCountSubmitted(getNominationCountBySubmitter(paxId));
        recognitionStats.setCountReceived(recognitionService.getRecognitionCountByRecipient(paxId));
        recognitionStats.setLastRecognitionGiven(getLastRecognitionGiven(paxId));

        return recognitionStats;
    }

    @Override
    public Map<Long, String> determineRecognitionStatusOnCreation(Long submitterPaxId, Long programId,
            Integer awardAmount, List<Long> paxIdsFromNomination, Long nominationId) {
    	
    	String validationStr = "*** determineRecognitionStatusOnCreation: ";
    	validationStr = validationStr + " submitterPaxId=" + convertLongToString(submitterPaxId) + " programId=" + convertLongToString(programId) + " awardAmount=" + convertIntegerToString(awardAmount) + " nominationId=" +  convertLongToString(nominationId);

        List<ApprovalProcessConfig> configs =
                approvalService.fetchApprovalProcessConfigsForAwardTier(programId, awardAmount);

        
        // Populate a map of receiver pax ids to recognition status string
        Map<Long, String> receiverRecognitionStatusMap = new HashMap<>();

        for (Long paxId : paxIdsFromNomination) {
            receiverRecognitionStatusMap.put(paxId, null);
        }
        
        Map<Long, Boolean> routeNextLevelManagerAwardTierMap = null;
        boolean routeNextLevelManager = false;

        if (configs != null) {      

        	//If configs is not null means the program contains an Award Tier, therefore
        	//we need to check if there is an Award Tied Id tied to VF_NAME "ROUTE_NEXT_LEVEL_MANAGER" from database
        	//by default if there is no asociation then ROUTE_NEXT_LEVEL_MANAGER is false
        	
        	routeNextLevelManagerAwardTierMap  = approvalService.fetchRouteNextLevelManagerStatus(programId);
        	        	
            for (ApprovalProcessConfig config : configs) {
            	
            	if (!routeNextLevelManagerAwardTierMap.isEmpty()) {
            		if (routeNextLevelManagerAwardTierMap.containsKey(config.getTargetId())) {
            			routeNextLevelManager = routeNextLevelManagerAwardTierMap.get(config.getTargetId());
            		}
            	}            	
            	if (config.getApprovalLevel() == 1) {
                    // Bulk fetch the relationships for all receivers and the submitter
                    List<Long> paxIds = new ArrayList<>(receiverRecognitionStatusMap.keySet());
                    paxIds.add(submitterPaxId);

                    Map <Long, Long> approvalMap = approvalService.bulkFetchApprovers(
                            paxIds,
                            submitterPaxId,
                            config.getApprovalTypeCode(),
                            config.getApproverPaxId(),
                            nominationId,
                            routeNextLevelManager
                    );

                    for (Long receiverPaxId : receiverRecognitionStatusMap.keySet()) {
                        // If there is no approval necessary, or the approver is the submitter, set to APPROVED
                        if (approvalMap.get(receiverPaxId) != null) {
                            receiverRecognitionStatusMap.put(receiverPaxId, StatusTypeCode.PENDING.name());
                            validationStr = validationStr + " receiverPaxId=" + convertLongToString(receiverPaxId) + " StatusTypeCode=PENDING ";
                        } else {
                            receiverRecognitionStatusMap.put(receiverPaxId, StatusTypeCode.AUTO_APPROVED.name());
                            validationStr = validationStr + " receiverPaxId=" + convertLongToString(receiverPaxId) + " StatusTypeCode=AUTO_APPROVED ";
                        }
                    }
                }
            }
        }

        // Default any remaining receivers to APPROVED
        for (Long receiverPaxId : receiverRecognitionStatusMap.keySet()) {
            if (receiverRecognitionStatusMap.get(receiverPaxId) == null) {
                receiverRecognitionStatusMap.put(receiverPaxId, StatusTypeCode.AUTO_APPROVED.name());
                validationStr = validationStr + " Default receiverPaxId=" + convertLongToString(receiverPaxId) + " StatusTypeCode=AUTO_APPROVED" + " receiverPaxId=" + convertLongToString(receiverPaxId);
            }
        }
        logger.info(validationStr);
        return receiverRecognitionStatusMap;
    }

    private Nomination createNominationEntity(Long submitterPaxId, NominationRequestDTO nominationRequestDTO, Long proxyPaxId, String nominationTypeCode) {
    	String validationStr = "*** createNominationEntity:";  	
    	validationStr = validationStr + " submitterPaxId=" + convertLongToString(submitterPaxId) + " proxyPaxId=" + convertLongToString(proxyPaxId) + " nominationTypeCode=" + convertString(nominationTypeCode);
  	
    	Nomination nomination = new Nomination();
        nomination.setProgramId(nominationRequestDTO.getProgramId());
        nomination.setSubmitterPaxId(submitterPaxId);
        nomination.setEcardId(nominationRequestDTO.getImageId());
        nomination.setHeadlineComment(nominationRequestDTO.getHeadline());
        nomination.setProxyPaxId(proxyPaxId);
        nomination.setBatchId(nominationRequestDTO.getBatchId());
        nomination.setNominationTypeCode(nominationTypeCode);
        nomination.setIdentifier(nominationRequestDTO.getProgramIdentifierByNomination());
        if (nominationRequestDTO.getComment() != null && !nominationRequestDTO.getComment().isEmpty()) {
            nomination.setComment(nominationRequestDTO.getComment());
        }

        if (nominationRequestDTO.getBudgetId() != null) {
            nomination.setBudgetId(nominationRequestDTO.getBudgetId());
        }

        nomination.setSubmittalDate(new Date());
        nomination.setStatusTypeCode(StatusTypeCode.APPROVED.name());
        
        validationStr = validationStr + " StatusTypeCode=APPROVED";
        logger.info(validationStr);
        return nomination;
    }

    private Long getProxyPaxId(Long submitterPaxId) {
        Long proxyPaxId = null;
        if (security.getPaxId() != null && !security.getPaxId().equals(submitterPaxId)) { // give as pax
            if (security.hasPermission(PermissionManagementTypes.PROXY_GIVE_REC_ANYONE.name())) {
                proxyPaxId = security.getPaxId();
            } else {
                throw new AccessDeniedException(ProjectConstants.BATCH_EVENT_MESSAGE_ACCESS_IS_DENIED);
            }
        } else if (security.isImpersonated()) { // Proxy as
            if (proxyService.doesPaxHaveProxyPermission(PermissionConstants.PROXY_GIVE_REC, submitterPaxId,
                    security.getImpersonatorPaxId())
                    || security.impersonatorHasPermission(PermissionManagementTypes.PROXY_GIVE_REC_ANYONE.name())) {
                // impersonator is a proxy of the submitter pax who has the PROXY_GIVE_REC permission
                // or the impersonator has the PROXY_GIVE_REC_ANYONE permission
                proxyPaxId = security.getImpersonatorPaxId();
            } else {
                List<ErrorMessage> errors = new ArrayList<>();
                errors.add(NominationConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
                ErrorMessageException.throwIfHasErrors(errors);
            }
        }
        return proxyPaxId;
    }



    private void setPaxCollections (GroupMemberAwardDTO receiver, Long paxId, List<Long> paxIds, Map<Integer, Long> awardTierByAmount,
            Map<Integer, Set<Long>> paxIdsByAmount, Map<Long, Integer> origAmountByPaxId, Set<Integer> originalAmounts) {

        setPaxIdsCollection(paxId, paxIds);

        Set<Long> paxIdsTmp =  paxIdsByAmount.get(receiver.getAwardAmount());
        if (paxIdsTmp == null) {
            paxIdsTmp  = new HashSet<>();

            setAwardTierByAmountCollection(awardTierByAmount, receiver.getAwardAmount(), receiver.getAwardTierId());
        }
        paxIdsTmp.add(paxId);
        setPaxIdsByAmountCollection(paxIdsByAmount, paxIdsTmp, receiver.getAwardAmount());
        setOrigAmountByPaxIdCollection(paxId, origAmountByPaxId, receiver.getOriginalAmount());
        setOriginalAmountsCollection(originalAmounts, receiver.getOriginalAmount());

    }

    private void setPaxIdsCollection(Long paxId, List<Long> paxIds) {
        paxIds.add(paxId);
    }

    private void setAwardTierByAmountCollection(Map<Integer, Long> awardTierByAmount, Integer awardAmount, Long awardTierId) {
        awardTierByAmount.put(awardAmount, awardTierId);
    }

    private void setPaxIdsByAmountCollection(Map<Integer, Set<Long>> paxIdsByAmount, Set<Long> paxIdsTmp, Integer awardAmount) {
        paxIdsByAmount.put(awardAmount, paxIdsTmp);
    }

    private void setOrigAmountByPaxIdCollection(Long paxId, Map<Long, Integer> origAmountByPaxId, Integer originalAmount) {
        origAmountByPaxId.put(paxId, originalAmount);
    }

    private void setOriginalAmountsCollection(Set<Integer> originalAmounts, Integer originalAmount) {
        originalAmounts.add(originalAmount);
    }


    /**
     * Method to obtain the original amount value from the recognition
     * @param nominationRequestDTO
     * @param paxId
     * @return Original amount value
     */
    private Integer getOriginalAmount(NominationRequestDTO nominationRequestDTO, long paxId){

    	Integer originalAmount = 0;

    	for (GroupMemberAwardDTO receiver : nominationRequestDTO.getReceivers()) {

            if (receiver.getPaxId() != null && receiver.getPaxId() == paxId) {
                originalAmount = receiver.getOriginalAmount();
            }
        }

    	return originalAmount;
    }

    /**
    *Method to get list of eligible budgets for Pax
    *Params: PaxId
    *Returns List containing valid budgets Ids **/
    protected List<BudgetEligibilityDTO> getEligibleBudgetList(Long paxId) {
    	List<BudgetEligibilityDTO> budgetEligibilityList = new ArrayList<>();
    	//Get the list of eligible budgets for the provided pax
    	List<Map<String, Object>> eligibleBudgetList = budgetInfoDao.getBudgetListByPax(paxId);

        if (!ListUtils.isEmpty(eligibleBudgetList)) {
	        for(Map<String, Object> budget : eligibleBudgetList) {
	        	 BudgetEligibilityDTO budgets = new BudgetEligibilityDTO();
	        	 if(budget.get(ProjectConstants.BUDGET_ID) != null) {
	        		 budgets.setBudgetId((Long)budget.get(ProjectConstants.BUDGET_ID));
	        	 }
	        	 if(budget.get(ProjectConstants.PROGRAM_ID) != null) {
	        		 budgets.setProgramId((Long)budget.get(ProjectConstants.PROGRAM_ID));
	        	 }
	        	 budgetEligibilityList.add(budgets);
	        }
        }
        return budgetEligibilityList;
     }

     private String convertLongToString(Long  longValue) {
    	String convertedLongValue = ProjectConstants.NULL_VALUE;
    	if (longValue != null) {
    		convertedLongValue = Long.toString(longValue);
    	}
    	return convertedLongValue;
    }
    private String convertString(String  strValue) {
    	String convertedValue = ProjectConstants.NULL_VALUE;
    	if (strValue != null) {
    		convertedValue = strValue;
    	}
    	return convertedValue;
    }
    private String convertIntegerToString(Integer  integerValue) {
    	String convertedIntegerValue = ProjectConstants.NULL_VALUE;
    	if (integerValue != null) {
    		convertedIntegerValue = Integer.toString(integerValue);
    	}
    	return convertedIntegerValue;
    }
    private String convertBooleanToString(Boolean booleanValue) {
    	String convertBooleanValue = ProjectConstants.NULL_VALUE;
    	if ( booleanValue != null ) {
    		convertBooleanValue = String.valueOf(booleanValue);
    	}
    	return convertBooleanValue;
    }

     /**
     * Method to obtain the original amount value from the recognition
     * @param nominationRequestDTO
     * @param paxId
     * @return Original amount value
     */
    private Integer getOriginalAmountForGroupCheckSubmitterPaxID(NominationRequestDTO nominationRequestDTO, long paxId, Long submitterPaxId){ //.for member long paxId = receiver

    	Integer originalAmount = 0;   //figure out why this amount is not being set to one and the config is not being populated

    	for (GroupMemberAwardDTO receiver : nominationRequestDTO.getReceivers()) {

            if (receiver.getGroupId() != null && !submitterPaxId.equals(paxId)) { //.receiver.getpaxId() is null ** For member receiver.getPaxId is not null
                originalAmount = receiver.getOriginalAmount();
            }
        }

    	return originalAmount;
    } 
}
