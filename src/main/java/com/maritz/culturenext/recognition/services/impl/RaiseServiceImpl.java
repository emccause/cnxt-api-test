package com.maritz.culturenext.recognition.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.dto.RecognitionSimpleDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import com.maritz.culturenext.recognition.services.RaiseService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class RaiseServiceImpl implements RaiseService {
    private static final String SELECTED_AWARD_TYPE = "SELECTED_AWARD_TYPE";

    @Inject private AlertService alertService;
    @Inject private ApprovalService approvalService;
    @Inject private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Inject private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private Environment environment;
    @Inject private NominationService nominationService;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private PaxCountDao paxCountDao;
    @Inject private PaxRepository paxRepository;
    @Inject private RaiseDao raiseDao;
    @Inject private Security security;
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;

    @Override
    public List<RaiseDTO> getRaisesForNominationCurrent(Long nominationId, Integer size, Integer page, 
            List<String> statusList, Long fromPaxId, Long involvedPaxId) {
        List<RaiseDTO> raiseItems = new ArrayList<>();
        List<String> statuses = new ArrayList<>();
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        // Prepare the Status list based on fromPaxId and PaxId
        if (fromPaxId == null || statusList == null || !fromPaxId.equals(security.getPaxId())) {
            statuses.add(StatusTypeCode.APPROVED.toString());
        } else {
            statuses = statusList;
        }
        
        if (statuses.contains(StatusTypeCode.APPROVED.toString())) {
            statuses.add(StatusTypeCode.AUTO_APPROVED.toString());
        }
        
        // Validate involvedPaxId exists
        if (involvedPaxId != null && !paxRepository.exists(involvedPaxId)) {
            errors.add(NominationConstants.PAX_ID_INVALID);
        }
    
        ErrorMessageException.throwIfHasErrors(errors);
        
        List<Map<String, Object>> raiseList = raiseDao.getListOfRaisesOnNomination(nominationId, fromPaxId, 
                statuses, null, involvedPaxId);

        for (Map<String, Object> node : raiseList) {
            RaiseDTO raiseDTO = populateRaiseDTO(node);

            raiseDTO.setApprovalTimestamp(approvalService.determineApprovalTimestampByRecognitionId(raiseDTO.getId()));

            raiseItems.add(raiseDTO);
        }

        return PaginationUtil.getPaginatedList(raiseItems, page, size);
    }
    
    @Override
    public PaginatedResponseObject<RaiseDTO> getRaisesForNomination(Long nominationId, Integer size, Integer page
            , List<String> statusList , Long fromPaxId, Long involvedPaxId) {
        
        List<RaiseDTO> raiseItems = new ArrayList<>();
        List<String> statuses = new ArrayList<>();
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        // handle page parameters
        if (page == null || page < 1) {
            page = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        if (size == null || size < 1) {
            size = ProjectConstants.DEFAULT_PAGE_SIZE;
        }
        
        // Prepare the Status list based on fromPaxId and PaxId
        if (fromPaxId == null || statusList == null || !fromPaxId.equals(security.getPaxId())) {
            statuses.add(StatusTypeCode.APPROVED.toString());
        } else {
            statuses = statusList;
        }
        
        if (statuses.contains(StatusTypeCode.APPROVED.toString())) {
            statuses.add(StatusTypeCode.AUTO_APPROVED.toString());
        }
        
        // Validate involvedPaxId exists
        if (involvedPaxId != null && !paxRepository.exists(involvedPaxId)) {
            errors.add(NominationConstants.PAX_ID_INVALID);
        }
    
        ErrorMessageException.throwIfHasErrors(errors);
        
        List<Map<String, Object>> raiseList = raiseDao.getListOfRaisesOnNomination(page, size, nominationId
                , fromPaxId, statuses, null, involvedPaxId);

        Integer totalResults = 0;
        for (Map<String, Object> node : raiseList) {
            totalResults = (Integer) node.get(ProjectConstants.TOTAL_RESULTS_KEY);
            
            RaiseDTO raiseDTO = populateRaiseDTO(node);
            raiseDTO.setApprovalTimestamp(approvalService.determineApprovalTimestampByRecognitionId(raiseDTO.getId()));
            raiseItems.add(raiseDTO);
        }
        
        // Handle other parameters
        Map<String, Object> requestParameters = new HashMap<>();
        requestParameters.put(RestParameterConstants.FROM_PAX_ID_REST_PARAM, fromPaxId);
        requestParameters.put(RestParameterConstants.INVOLVED_PAX_ID_REST_PARAM, involvedPaxId);
        requestParameters.put(RestParameterConstants.STATUS_REST_PARAM, statusList);

        return new PaginatedResponseObject<RaiseDTO>(raiseItems, requestParameters 
                , String.format(NominationConstants.RAISE_GET_ENDPOINT,nominationId)
                , totalResults, page, size);

    }

    @Override
    public List<RaiseDTO> createRaises(Long nominationId, List<RaiseDTO> raises) {

        List<RaiseDTO> raiseResponseList = new ArrayList<>();

        for (RaiseDTO raiseNode : raises) {
            raiseResponseList.add(createRaise(raiseNode, nominationId));
        }

        String raiseApprovalsEnabledStr = 
                environment.getProperty(ApplicationDataConstants.KEY_NAME_RAISE_APPROVAL_ENABLED);
        if (raiseApprovalsEnabledStr != null && Boolean.valueOf(raiseApprovalsEnabledStr)) {
            alertService.addRaiseApprovalAlert(nominationId);
        }

        return raiseResponseList;
    }

    @Override
    public List<RaiseDTO> updateRaises(List<RaiseDTO> raises) {

        if (CollectionUtils.isEmpty(raises)) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_MISSING_RAISES)
                    .setMessage(NominationConstants.ERROR_MISSING_RAISES_MSG)
                    .setField(ProjectConstants.RAISES));
        }

        Map<Long, Nomination> nominationIdToNominationMap;
        Map<Long, Recognition> recognitionMap = new HashMap<>();
        List<RaiseDTO> responseRaises = new ArrayList<>();
        List<Long> recognitionIds = new ArrayList<>();
        List<Long> nominationIds = new ArrayList<>();

        for (RaiseDTO raise : raises) {
            if (raise.getId() == null) {
                throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(NominationConstants.ERROR_INVALID_RAISE_ID)
                        .setMessage(NominationConstants.ERROR_INVALID_RAISE_ID_MSG)
                        .setField(ProjectConstants.RAISES));
            }
            recognitionIds.add(raise.getId());
        }

        List<Recognition> raiseRecognitions = recognitionDao.findById(recognitionIds);
        for (Recognition recognition : raiseRecognitions) {
            recognitionMap.put(recognition.getId(), recognition);
            nominationIds.add(recognition.getNominationId());
        }

        List<Nomination> raiseNominations = nominationDao.findById(nominationIds);

        // Map that associates a recognitionId to a nomination
        nominationIdToNominationMap = associateNominationIdToNominationMap(raiseNominations);

        // Need to first validate that all of the raise records are valid for updating
        for (RaiseDTO raiseDTO : raises) {
            Recognition recognition = recognitionMap.get(raiseDTO.getId());
            validateRaiseForUpdate(raiseDTO, recognitionMap.get(raiseDTO.getId()), 
                    nominationIdToNominationMap.get(recognition.getNominationId()));

        }

        // Update the raise records in the database
        for (RaiseDTO raiseDTO : raises) {
            Recognition recognition = recognitionMap.get(raiseDTO.getId());
            responseRaises.add(updateRaiseRecord(raiseDTO, recognitionMap.get(raiseDTO.getId()),
                    nominationIdToNominationMap.get(recognition.getNominationId())));
        }

        approvalService.approveOrRejectRaises(responseRaises, security.getPaxId());

        // Determine and set approval timestamps after approval changes have been made
        Map<Long, Date> recognitionIdToApprovalDate = 
                approvalService.determineApprovalTimestampByRecognitionId(recognitionIds);
        for (RaiseDTO responseRaise : responseRaises) {
            responseRaise.setApprovalTimestamp(recognitionIdToApprovalDate.get(responseRaise.getId()));
        }

        // Return the raise records passed in after having updated the db
        return responseRaises;
    }

    @Override
    public List<EntityDTO> getRecipientsForRaise(Long activityId, Boolean excludePrivate, String delimitedStatus, 
            Integer size, Integer page) {
        
        List<String> statusList = new ArrayList<String>();
        if (delimitedStatus != null && !delimitedStatus.isEmpty()) {
            statusList = StringUtils.parseDelimitedData(delimitedStatus);
        }
        
        List<Long> paxList = getRecipientsOnActivity(activityId, excludePrivate, statusList);

        return PaginationUtil.getPaginatedList(participantInfoDao.getInfo(paxList, null), page, size);
    }

    @Override
    public List<Long> getRecipientsOnNomination(Long nominationId, Boolean excludePrivate) {
        List<Long> paxList = new ArrayList<>();

        // Get all user Pax
        List<Map<String, Object>> receiverPaxList = raiseDao.getListOfPaxOnNomination(security.getPaxId(),
                nominationId, excludePrivate);

        for (Map<String, Object> node : receiverPaxList) {
            paxList.add((Long) node.get(NominationConstants.RAISE_RETURN_RECEIVER_PAX_ID));
        }

        return paxList;
    }
    
    @Override
    public List<Long> getRecipientsOnActivity(Long activityId, Boolean excludePrivate, List<String> statusList) {
        List<Long> paxList = new ArrayList<>();

        // Get all user Pax
        List<Map<String, Object>> receiverPaxList = raiseDao.getListOfPaxOnActivity(security.getPaxId(), activityId,
                excludePrivate, statusList);

        for (Map<String, Object> node : receiverPaxList) {
            paxList.add((Long) node.get(NominationConstants.RAISE_RETURN_RECEIVER_PAX_ID));
        }

        return paxList;
    }

    /**
     * This method creates and saves the raise record after validating it
     * @param raise The raise from the request to be saved
     * @param nominationId The nomination id used to chain the raises together
     * @return The return Raise object after the record is saved
     */
    private RaiseDTO createRaise(RaiseDTO raise, Long nominationId) {
        RaiseDTO raiseResponse;    

        Nomination nominationEntity = new Nomination();
        final Recognition recognitionEntity = new Recognition();

        if(raise.getToPax() == null || raise.getToPax().getPaxId() == null){
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.MISSING_RECEIVERS)
                    .setMessage(NominationConstants.MISSING_RECEIVERS_MSG)
                    .setField(ProjectConstants.RECEIVERS));
        }
        if (raise.getFromPax() == null || raise.getFromPax().getPaxId() == null) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.MISSING_SUBMITTERS)
                    .setMessage(NominationConstants.MISSING_SUBMITTERS_MSG)
                    .setField(ProjectConstants.FROM_PAX_ID));
        }

        Long programId = nominationDao.findById(nominationId).getProgramId();

        GroupMemberAwardDTO receiver = new GroupMemberAwardDTO();
        receiver.setPaxId(raise.getToPax().getPaxId());
        receiver.setAwardAmount(raise.getAwardAmount().intValue());
        receiver.setAwardTierId(raise.getAwardTierId());
        receiver.setOriginalAmount(raise.getAwardAmount().intValue());
        
        NominationRequestDTO validateNominationRequestDTO = new NominationRequestDTO();
        validateNominationRequestDTO.setReceivers(Arrays.asList(receiver));
        validateNominationRequestDTO.setProgramId(programId);
        validateNominationRequestDTO.setBudgetId(raise.getBudgetId());
        setPayoutTypeAssociatedWithProgram (validateNominationRequestDTO);
        List<ErrorMessage> errors = nominationService.validateNomination(raise.getFromPax().getPaxId(),
                                                                            validateNominationRequestDTO, Arrays.asList(receiver.getPaxId()),
                                                                            new ArrayList<Long>(), NominationConstants.RAISE, Boolean.FALSE);
        errors = validateRaiseRecipient(errors, raise.getFromPax().getPaxId(), 
                raise.getToPax().getPaxId(), nominationId);
        if(errors != null && !errors.isEmpty()) {
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        List<Map<String, Object>> allPax = paxCountDao.getDistinctPax(raise.getFromPax().getPaxId(),
                null,  Arrays.asList(receiver.getPaxId()));
        if(allPax == null || allPax.isEmpty()){ // All receivers are inactive / suspended
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS)
                    .setMessage(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS_MSG)
                    .setField(ProjectConstants.RECEIVERS));
        }

        nominationEntity.setSubmitterPaxId(raise.getFromPax().getPaxId());
        nominationEntity.setSubmittalDate(new Date());

        final Long toPaxId = raise.getToPax().getPaxId();
        Map<Long, String> raiseStatusMap = 
                nominationService.determineRecognitionStatusOnCreation(raise.getFromPax().getPaxId(), 
                        programId, raise.getAwardAmount().intValue(), Arrays.asList(toPaxId), nominationId);

        // Check that raise approvals are enabled for project
        String raiseApprovalsEnabledStr = 
                environment.getProperty(ApplicationDataConstants.KEY_NAME_RAISE_APPROVAL_ENABLED);
        String statusTypeCode = raiseApprovalsEnabledStr != null && 
                Boolean.valueOf(raiseApprovalsEnabledStr) ? 
                        raiseStatusMap.get(toPaxId) : 
                            StatusTypeCode.APPROVED.toString();
        nominationEntity.setStatusTypeCode(statusTypeCode);

        if(raise.getAwardAmount() != null && raise.getBudgetId() != null && raise.getAwardAmount() > 0){
            nominationEntity.setBudgetId(raise.getBudgetId());
        }

        Long parentId = raiseDao.getRecognitionParentId(nominationId, raise.getToPax().getPaxId());

        // Check if recognition has been approved
        Recognition recognition = recognitionDao.findById(parentId);
        if (!ProjectConstants.APPROVED_STATUSES.contains(recognition.getStatusTypeCode())) {
            throw new ErrorMessageException().addErrorMessage(NominationConstants.RAISE, 
                    NominationConstants.ERROR_RECOGNITION_NOT_APPROVED,
                    NominationConstants.ERROR_RECOGNITION_NOT_APPROVED_MSG);
        }

        nominationEntity.setParentId(nominationId);
        nominationEntity.setNominationTypeCode("ECAW");
        nominationEntity.setProgramId(programId);

        nominationDao.save(nominationEntity);

        recognitionEntity.setNominationId(nominationEntity.getId());
        recognitionEntity.setReceiverPaxId(toPaxId);
        recognitionEntity.setViewed(ProjectConstants.NO_CHAR);
        recognitionEntity.setProgramAwardTierId(raise.getAwardTierId());
        recognitionEntity.setAmount(raise.getAwardAmount());
        recognitionEntity.setStatusTypeCode(statusTypeCode);
        recognitionEntity.setParentId(parentId);

        recognitionDao.save(recognitionEntity);

        nominationService.insertTransactionData(validateNominationRequestDTO, nominationEntity);

        if (statusTypeCode.equals(StatusTypeCode.PENDING.toString())) {
            approvalService.createPendingApprovals(Arrays.asList(recognitionEntity), 
                    nominationEntity.getSubmitterPaxId(), programId, raise.getAwardAmount().intValue(), 1L, nominationId);
        }
        else {
            // create history for auto approved recognitions.
            approvalService.createAutoApprovalHistory(Arrays.asList(recognitionEntity), programId);
        }

        raiseResponse = createRaiseDTO(nominationEntity, recognitionEntity);

        raiseResponse.setApprovalTimestamp(approvalService.determineApprovalTimestampByRecognitionId(
                recognitionEntity.getId()));

        // Send notifications to raise receivers and approvers
        alertService.addRaiseReceiverAlert(nominationEntity.getId());
        
        return raiseResponse;
    }

    /**
     * Gets the payout type associated with the program id passed in thru the dto.
     * 
     * @param validateNominationRequestDTO
     */
    public void setPayoutTypeAssociatedWithProgram(NominationRequestDTO validateNominationRequestDTO) {
        ProgramMisc programMisc = programMiscDao.findBy().where(ProjectConstants.VF_NAME).eq(SELECTED_AWARD_TYPE)
            .and(ProjectConstants.PROGRAM_ID).eq(validateNominationRequestDTO.getProgramId()).findOne();
        if (programMisc != null) {
            validateNominationRequestDTO.setPayoutType(programMisc.getMiscData());
        }
    }

    /**
     * The raise update validation
     * @param requestRaise The requesting raise to be validated
     * @return The original raise record that has been fully checked
     */
    private void validateRaiseForUpdate(RaiseDTO requestRaise, Recognition raiseRecognition,
            Nomination raiseNomination) {
        List<ErrorMessage> errors = new ArrayList<>();

        // Populate the raise record from the database
        RaiseDTO storedRaise = createRaiseDTO(raiseNomination, raiseRecognition);

        if (storedRaise.getApprovalPax() != null && 
                !storedRaise.getApprovalPax().getPaxId().equals(security.getPaxId())) {
            errors.add(NominationConstants.APPROVAL_PAX_MISMATCH);
        }

        if (ProjectConstants.APPROVED_STATUSES.contains(storedRaise.getStatus())
                || ProjectConstants.REJECTED_STATUSES.contains(storedRaise.getStatus())) {
            errors.add(NominationConstants.ALREADY_APPROVED_OR_REJECTED_MESSAGE);
        }
        if(!CollectionUtils.isEmpty(errors)){
            throw new ErrorMessageException().addErrorMessages(errors);
        }

    }

    /**
     * Updates the actual raise record after validation of all requests have been completed
     * @param requestRaise The requesting raise to be updated from
     * @return The passed in raise record that was used to make the update
     */
    private RaiseDTO updateRaiseRecord(RaiseDTO requestRaise, Recognition raiseRecognition,
            Nomination raiseNomination) {

        raiseRecognition.setStatusTypeCode(requestRaise.getStatus());
        raiseRecognition.setComment(requestRaise.getComment());
        recognitionDao.save(raiseRecognition);

        return createRaiseDTO(raiseNomination, raiseRecognition);
    }

    /**
     * Helper method for update raise that associates a recognitionId to a Nomination record
     * @param nominations List of nomination records
     * @return a map that associates a recognition id to a nomination
     */
    private Map<Long, Nomination> associateNominationIdToNominationMap(List<Nomination> nominations) {
        Map<Long, Nomination> nominationIdToNominationMap = new HashMap<>();
        for (Nomination nomination : nominations) {
            nominationIdToNominationMap.put(nomination.getId(), nomination);
        }

        return nominationIdToNominationMap;
    }

    /**
     * The method takes database nomination and recognition entites and 
     * creates the raiseDTO from the two entites since they are a pair
     * @param nominationEntity Raise nomination entity
     * @param recognitionEntity Raise recognition entity
     * @return the populated RaiseDTO object
     */
    private RaiseDTO createRaiseDTO(Nomination nominationEntity, Recognition recognitionEntity) {
        RaiseDTO raiseDTO = new RaiseDTO();

        raiseDTO.setId(recognitionEntity.getId());
        raiseDTO.setTime(nominationEntity.getSubmittalDate().toString());
        raiseDTO.setAwardTierId(recognitionEntity.getProgramAwardTierId());
        raiseDTO.setAwardAmount(recognitionEntity.getAmount());
        raiseDTO.setStatus(recognitionEntity.getStatusTypeCode());
        raiseDTO.setBudgetId(nominationEntity.getBudgetId());
        raiseDTO.setFromPax(participantInfoDao.getEmployeeDTO(nominationEntity.getSubmitterPaxId()));
        raiseDTO.setToPax(participantInfoDao.getEmployeeDTO(recognitionEntity.getReceiverPaxId()));
        raiseDTO.setComment(recognitionEntity.getComment());
        raiseDTO.setApprovalPax(participantInfoDao.getEmployeeDTO(getApproverPaxIdForRecognition(
                recognitionEntity.getId())));

        return raiseDTO;
    }

    @Override
    public Long getApproverPaxIdForRecognition(Long recognitionId) {
        Long approverPaxId = null;

        ApprovalPending approvalPending = approvalPendingDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(recognitionId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                .findOne();
        ApprovalHistory approvalHistory = approvalHistoryDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(recognitionId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                .findOne();

        if (approvalPending != null) {
            approverPaxId = approvalPending.getPaxId();
        } else if (approvalHistory != null) {
            approverPaxId = approvalHistory.getPaxId();
        }

        return approverPaxId;
    }

    /**
     * Method used to populated the raise record pulled from the db, not using concentrix
     * @param node String to Object mapping column names to values returned from the db used to populate raiseDTO object
     * @return the newly created raiseDTO object
     */
    private RaiseDTO populateRaiseDTO(Map<String, Object> node) {
        RaiseDTO raiseDTO = new RaiseDTO();

        raiseDTO.setId((Long) node.get(ProjectConstants.ID));

        raiseDTO.setTime(DateUtil.convertToUTCDateTime(((Date) node.get(ProjectConstants.UPDATE_DATE))));
        if (node.get(ProjectConstants.PROGRAM_AWARD_TIER_ID) != null && node.get(ProjectConstants.AMOUNT) != null) {
            raiseDTO.setAwardTierId((long) node.get(ProjectConstants.PROGRAM_AWARD_TIER_ID));
            raiseDTO.setAwardAmount((double) node.get(ProjectConstants.AMOUNT));
        }
        raiseDTO.setStatus((String) node.get(ProjectConstants.STATUS_TYPE_CODE));

        EmployeeDTO fromPaxDTO = participantInfoDao.getEmployeeDTO((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID));
        raiseDTO.setFromPax(fromPaxDTO);

        EmployeeDTO toPaxDTO = participantInfoDao.getEmployeeDTO((Long) 
                node.get(NominationConstants.RAISE_RETURN_RECEIVER_PAX_ID));
        raiseDTO.setToPax(toPaxDTO);

        raiseDTO.setComment((String) node.get(ProjectConstants.COMMENT));
        raiseDTO.setApprovalPax(participantInfoDao.getEmployeeDTO(getApproverPaxIdForRecognition(raiseDTO.getId())));

        return raiseDTO;
    }
    
    /**
     * validates a raise
     * @param errors error list to be populated
     * @param fromPax id of pax who submitted the raise
     * @param toPax id of the pax who is receiving the raise
     * @param nominationId id of the nomination for which a recognition is being raised
     * @return list of errors (if any)
     */
    private List<ErrorMessage> validateRaiseRecipient(List<ErrorMessage> errors, Long fromPax, 
            Long toPax, Long nominationId) {

        NominationDetailsDTO nominationDetailsDTO = 
                nominationService.getNominationById(nominationId.toString(), fromPax);
                
        if (raiseDao.isAlreadyRaised(fromPax, toPax, nominationId)) {
            errors.add(NominationConstants.CANNOT_RAISE_AGAIN_MESSAGE);
        }
        
        if (nominationDetailsDTO != null){
            Double awardAmount = null;
            
            for(RecognitionSimpleDTO recognition: nominationDetailsDTO.getRecognitions()){
                if(toPax == recognition.getReceiverPaxId()){
                    awardAmount = recognition.getAwardAmount();
                }
            }
            
            if (fromPax.equals(nominationDetailsDTO.getGiverPax()) && awardAmount != null && awardAmount > 0) {
                errors.add(NominationConstants.CANNOT_RAISE_OWN_RECOGNITION_MESSAGE);
            }
        }
        
        if (!raiseDao.isValidRecognition(fromPax, toPax, nominationId)) {
            errors.add(NominationConstants.INVALID_RECOGNITION_MESSAGE);
        }
        return errors;
    }
}
