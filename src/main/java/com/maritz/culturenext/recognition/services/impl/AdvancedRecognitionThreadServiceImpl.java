package com.maritz.culturenext.recognition.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.BulkAdvancedRecNominationDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.AdvancedRecognitionThreadService;
import com.maritz.culturenext.recognition.services.NominationService;

@Component
public class AdvancedRecognitionThreadServiceImpl implements AdvancedRecognitionThreadService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ConcentrixDao<Pax> paxDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;
    @Inject private NominationService nominationService;

    @Override
    public Boolean validateNomination(Long batchId, Long tempNominationId) {
        logger.info("Starting validation for batchId " + batchId + ", tempNominationId " + tempNominationId);
        
        BulkAdvancedRecNominationDTO nomination = getNominationRequestObject(batchId, tempNominationId);
        
        if (nomination == null) {
            logger.info("Didn't find STAGE_ADVANCED_REC records for batchId " + batchId + ", tempNominationId " + tempNominationId + ", returning TRUE");
            // dunno what happened, we have no stage records for that object - get out
            return Boolean.TRUE;
        }
        
        List<Long> paxIdList = new ArrayList<>(nomination.getRecipientPaxIdControlNumMap().values());

        logger.info("Starting nomination validation for batchId " + batchId + ", tempNominationId " + tempNominationId);
        List<ErrorMessage> errorMessageList = nominationService.validateNomination(
                                                nomination.getSubmitterPaxId(), nomination.getNomination(), paxIdList,
                                                new ArrayList<Long>(), TableName.RECOGNITION.name(), Boolean.TRUE
                                            );
        
        if (Boolean.FALSE.equals(nomination.getDuplicatesFound()) && ObjectUtils.isEmpty(errorMessageList)) {
            logger.info("No errors for batchId " + batchId + ", tempNominationId " + tempNominationId + ", returning FALSE");
            return Boolean.FALSE;
        }

        // errors happened, loop through and handle

        logger.info("Handling errors for batchId " + batchId + ", tempNominationId " + tempNominationId);
        for (ErrorMessage errorMessage : errorMessageList) {
            handleErrorMessage(nomination, errorMessage);
        }
        
        // save all records
        logger.info("Cleaning STAGE_ADVANCED_REC records for batchId " + batchId + ", tempNominationId " + tempNominationId);
        for (StageAdvancedRec record : nomination.getRecordList()) {
            // strip off last comma
            String errorMessage = record.getErrorMessage();
            if (!StringUtils.isEmpty(errorMessage)) {
                record.setErrorMessage(errorMessage.substring(0, errorMessage.length() - 1));    
            }
            stageAdvancedRecDao.save(record);
        }

        // return true, we had errors
        logger.info("Had errors for batchId " + batchId + ", tempNominationId " + tempNominationId + ", returning TRUE");
        return Boolean.TRUE;
    }

    @Override
    public Future<Boolean> validateNominationAsync(Long batchId, Long tempNominationId) {
        return new AsyncResult<>(validateNomination(batchId, tempNominationId));
    }

    @Override
    public void createNomination(Long batchId, Long tempNominationId) {
        logger.info("Creating nomination for batchId " + batchId + ", tempNominationId " + tempNominationId);
        BulkAdvancedRecNominationDTO nomination = getNominationRequestObject(batchId, tempNominationId);
        nominationService.createStandardNomination(nomination.getSubmitterPaxId(), nomination.getNomination(), Boolean.FALSE);
        logger.info("Submitted nomination for batchId " + batchId + ", tempNominationId " + tempNominationId);
    }

    @Override
    public void createNominationAsync(Long batchId, Long tempNominationId) {
        createNomination(batchId, tempNominationId);
    }
    
    /**
     * Fetches STAGE_ADVANCED_REC records for the provided batch id and temp nomination id,
     * converts into a NominationRequestDTO, records submitter pax id, a control num : pax id map and record list
     * 
     * @param batchId - id of batch to fetch
     * @param tempNominationId - id of subset of batch to fetch
     * @return BulkAdvancedRecNominationDTO - DTO containing details listed above. 
     */
    private BulkAdvancedRecNominationDTO getNominationRequestObject(Long batchId, Long tempNominationId) {
        logger.info("Building nomination object for batchId " + batchId + ", tempNominationId " + tempNominationId);
        
        List<StageAdvancedRec> nominationRecordList = stageAdvancedRecDao.findBy()
                                                        .where(ProjectConstants.BATCH_ID).eq(batchId)
                                                        .and(ProjectConstants.TEMP_NOMINATION_ID).eq(tempNominationId)
                                                        .find();
        
        if (ObjectUtils.isEmpty(nominationRecordList)) {
            logger.info("No STAGE_ADVANCED_REC records found for batchId " + batchId + ", tempNominationId " + tempNominationId);
            // no results for some reason, return null;
            return null;
        }
        
        BulkAdvancedRecNominationDTO nomination = new BulkAdvancedRecNominationDTO();
        
        NominationRequestDTO requestObject = new NominationRequestDTO();

        // get first record for most fields.
        StageAdvancedRec firstRecord = nominationRecordList.get(0);
        
        // set up list of control nums to resolve
        List<String> controlNumList = new ArrayList<>();
        
        // handle pax first
        List<String> duplicateControlNumList = new ArrayList<>();
        for (StageAdvancedRec record : nominationRecordList) {
            if (controlNumList.contains(record.getRecipientId().trim())) {
                duplicateControlNumList.add(record.getRecipientId().trim());
                continue;
            }
            controlNumList.add(record.getRecipientId().trim());
        }

        // if we found duplicates, we'll need to log errors later.
        nomination.setDuplicatesFound(!duplicateControlNumList.isEmpty());
        
        logger.info("Finding submitter pax for batchId " + batchId + ", tempNominationId " + tempNominationId);
        Pax submitterPax = paxDao.findBy().where(ProjectConstants.CONTROL_NUM).eq(firstRecord.getSubmitterId().trim()).findOne();
        nomination.setSubmitterPaxId(submitterPax.getPaxId());
        
        // get pax objects
        List<Pax> paxList = new ArrayList<>();
        
        // partition list to avoid sql param cap
        logger.info("Finding recipients for batchId " + batchId + ", tempNominationId " + tempNominationId);
        Iterable<List<String>> controlNumIterable = Iterables.partition(controlNumList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<String> set : controlNumIterable) {
            paxList.addAll(
                    paxDao.findBy().where(ProjectConstants.CONTROL_NUM).in(set).find()
                );
        }
        
        // populate values, let nulls propagate through
        requestObject.setHeadline(firstRecord.getPublicHeadline());
        requestObject.setProgramId(firstRecord.getProgramId());
        Long criteriaId = firstRecord.getValueId();
        if (criteriaId != null) {
            requestObject.setRecognitionCriteriaIds(Arrays.asList(criteriaId));
        }
        requestObject.setComment(firstRecord.getPrivateMessage());
        requestObject.setImageId(firstRecord.getEcardId());
        requestObject.setBudgetId(firstRecord.getBudgetId());
        requestObject.setBatchId(batchId);
        
        // map award type to payout type
        String awardType = firstRecord.getAwardType();
        if (awardType != null) {
            requestObject.setPayoutType(awardType);
        }
        requestObject.setIsPrivate(Boolean.parseBoolean(firstRecord.getMakeRecognitionPrivate()));
        
        logger.info("Finding award tiers for batchId " + batchId + ", tempNominationId " + tempNominationId);
        List<ProgramAwardTier> awardTierList = programAwardTierDao.findBy()
                                                    .where(ProjectConstants.PROGRAM_ID).eq(firstRecord.getProgramId())
                                                    .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                                                    .find();
        
        // handle recipients
        logger.info("Building recipient objects for batchId " + batchId + ", tempNominationId " + tempNominationId);
        Map<String, Long> recipientPaxIdControlNumMap = new HashMap<>();
        List<GroupMemberAwardDTO> recipientList = new ArrayList<>();
        for (StageAdvancedRec record : nominationRecordList) {
            for (Pax pax : paxList) {
                if (pax.getControlNum().equalsIgnoreCase(record.getRecipientId().trim())) {
                    // we've found it
                    recipientPaxIdControlNumMap.put(record.getRecipientId().trim(), pax.getPaxId());
                    GroupMemberAwardDTO recipient = new GroupMemberAwardDTO();
                    recipient.setPaxId(pax.getPaxId());
                    Double awardAmount = record.getAwardAmount();
                    Double originalAmount = record.getOriginalAmount();
                    if (awardAmount != null) {
                        recipient.setAwardAmount(awardAmount.intValue()); // not sure why this uses int
                        if (originalAmount != null) {
                            recipient.setOriginalAmount(originalAmount.intValue());
                        } else {
                            recipient.setOriginalAmount(awardAmount.intValue());
                        }
                        // find tier id
                        Boolean found = false;
                        for (ProgramAwardTier awardTier : awardTierList) {
                            if (awardTier.getMinAmount() <= recipient.getOriginalAmount() && awardTier.getMaxAmount() >= recipient.getOriginalAmount()) {
                                recipient.setAwardTierId(awardTier.getId());
                                found = true;
                                break;
                            }
                        }
                        if (Boolean.FALSE.equals(found)) {
                            logger.info("Didn't find award tier mapping to awardAmount " + awardAmount + " for batchId " + batchId + ", tempNominationId " + tempNominationId);
                            record.setErrorMessage(NominationConstants.ERROR_AWARD_AMOUNT_NOT_IN_AWARD_TIERS_MSG + ProjectConstants.COMMA_DELIM);
                            // We should see this come back as an error later
                        }
                    }
                    recipientList.add(recipient);
                    break;
                }
            }
        }
        requestObject.setReceivers(recipientList);
        
        nomination.setNomination(requestObject);
        nomination.setRecipientPaxIdControlNumMap(recipientPaxIdControlNumMap);
        
        if (Boolean.TRUE.equals(nomination.getDuplicatesFound())) {
            for (String controlNum : duplicateControlNumList) {
                for (StageAdvancedRec record : nominationRecordList) {
                    if (controlNum.equalsIgnoreCase(record.getRecipientId().trim())) {
                        record.setErrorMessage(NominationConstants.ERROR_RECEIVER_DUPLICATED_MSG);
                    }
                }
            }
        }
        
        nomination.setRecordList(nominationRecordList);

        logger.info("Returning nomination object batchId " + batchId + ", tempNominationId " + tempNominationId);
        return nomination;
    }
    
    /**
     * Handles propagating an error message to STAGE_ADVANCED_REC rows.
     * Will track down the specific record for certain error codes, or will apply to all records
     * 
     * @param nomination - object containing nomination details
     * @param errorMessage - error message to handle
     */
    private void handleErrorMessage(BulkAdvancedRecNominationDTO nomination, ErrorMessage errorMessage) {
        logger.info("Handling error " + errorMessage.getCode() + " for batch " + nomination.getNomination().getBatchId());
        
        // states where we need to find a specific record to set error messages on
        if (NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER.equals(errorMessage.getCode())) {
            // field is receivers/pax/{paxId} - single pax id
            String paxIdString = errorMessage.getField().substring(NominationConstants.RECEIVERS_PAX.length());
            
            Map<String, Long> recipientPaxIdControlNumMap = nomination.getRecipientPaxIdControlNumMap();
            Long paxId = Long.parseLong(paxIdString);
            String recipientId = null;
            for (String controlNum : recipientPaxIdControlNumMap.keySet()) {
                if (paxId.equals(recipientPaxIdControlNumMap.get(controlNum))) {
                    // found it!
                    recipientId = controlNum;
                    break;
                }
            }

            for (StageAdvancedRec record : nomination.getRecordList()) {
                if (recipientId == null || recipientId.equals(record.getRecipientId())) { // should never be null, but, if it is, apply to all records
                    handleErrorMessage(record, errorMessage.getMessage());
                }
            }
        }
        else if (NominationConstants.ERROR_CANNOT_RECOGNIZE_SELF.equals(errorMessage.getCode())) {
            // loop through rows to find one where submitter = receiver 
            for (StageAdvancedRec record : nomination.getRecordList()) {
                if (record.getSubmitterId().equalsIgnoreCase(record.getRecipientId())) {
                    handleErrorMessage(record, errorMessage.getMessage());
                }
            }
        }
        else if (NominationConstants.ERROR_RECEIVER_DUPLICATED.equals(errorMessage.getCode())) {
            // one or more recipients is duplicated, mark all duplicate rows
            List<String> recipientList = new ArrayList<>(); // all recipients
            List<String> duplicateRecipientList = new ArrayList<>(); // duplicate recipients
            for (StageAdvancedRec record : nomination.getRecordList()) {
                // force control nums as upper case to make sure we match properly
                if (recipientList.contains(record.getRecipientId().toUpperCase())) {
                    duplicateRecipientList.add(record.getRecipientId().toUpperCase());
                }
                else {
                    recipientList.add(record.getRecipientId().toUpperCase()); 
                }
            }
            
            // we've established duplicates, now set error messages on those rows
            for (StageAdvancedRec record : nomination.getRecordList()) {
                if (duplicateRecipientList.contains(record.getRecipientId().toUpperCase())) {
                    handleErrorMessage(record, errorMessage.getMessage());
                }
            }
        }
        else if (NominationConstants.ERROR_AWARD_TIER_MISSING.equals(errorMessage.getCode())) {
            // We already recorded this error earlier, no need to do anything
        }
        else {

            // propagate to all records
            for (StageAdvancedRec record : nomination.getRecordList()) {
                handleErrorMessage(record, errorMessage.getMessage());
            }    
        }
    }
    
    /**
     * Sets record error message, handles null checking.
     * 
     * @param record -  STAGE_ADVANCED_REC record to have error message set
     * @param errorMessage -  error message to be set.
     */
    private void handleErrorMessage(StageAdvancedRec record, String errorMessage) {
        logger.info("Writing error message for STAGE_ADVANCED_REC record" + record.getId());
        String recordError = record.getErrorMessage();
        if (recordError == null) {
            recordError = ProjectConstants.EMPTY_STRING;
        }
        record.setErrorMessage(recordError + errorMessage + ProjectConstants.COMMA_DELIM);
    }
}
