package com.maritz.culturenext.recognition.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.task.annotation.TriggeredTask;
import com.maritz.core.util.stream.CRC32InputStream;
import com.maritz.culturenext.batch.TemplateConstants;
import com.maritz.culturenext.batch.service.BatchService;
import com.maritz.culturenext.batch.util.ExcelToCSVInputStream;
import com.maritz.culturenext.batch.util.TemplateUtil;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.constants.RecognitionBulkUploadConstants;
import com.maritz.culturenext.recognition.dao.RecognitionBulkUploadDao;
import com.maritz.culturenext.recognition.dto.RecognitionBulkResponseDTO;
import com.maritz.culturenext.recognition.dto.StageAdvancedRecDTO;
import com.maritz.culturenext.recognition.services.AdvancedRecognitionThreadService;
import com.maritz.culturenext.recognition.services.RecognitionBulkUploadService;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;

@Component
public class RecognitionBulkUploadServiceImpl implements RecognitionBulkUploadService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private ConcentrixDao<BatchEvent> batchEventDao;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;
    @Inject private BatchService batchService;
    @Inject private ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;
    @Inject private AdvancedRecognitionThreadService advancedRecognitionThreadService;
    @Inject private BudgetInfoDao budgetInfoDao;
    @Inject private PaxRepository paxRepository;
    @Inject private RecognitionBulkUploadDao recognitionBulkUploadDao;

    @Override
    public RecognitionBulkResponseDTO recognitionsBulkUpload(Long paxId,
            MultipartFile file) throws Exception {
        logger.info("Recognition bulk upload file uploaded!");
        RecognitionBulkResponseDTO recognitionBulkResponseDTO = new RecognitionBulkResponseDTO();

        Batch batch = new Batch();
        batch.setBatchTypeCode(BatchType.ADV_REC_BULK_UPLOAD.name());
        batch.setStatusTypeCode(StatusTypeCode.IN_PROGRESS.name());
        batchDao.save(batch);

        Long batchId = batch.getBatchId();

        BatchFile batchFile = new BatchFile();
        String filename = file.getOriginalFilename();
        filename = filename.substring(0,
                filename.lastIndexOf(ProjectConstants.DOT_DELIM) + 1).concat(ProjectConstants.CSV);
        batchFile.setFilename(filename);
        batchFile.setFileUpload("");
        batchFile.setBatchId(batchId);
        batchFile.setMediaType(MediaTypesEnum.TEXT_CSV.value());
        batchFileDao.save(batchFile);

        InputStream inputStream =  getInputStream(file.getContentType(), file.getOriginalFilename(), file.getInputStream());

        Map<String, Object[]> recognitionTemplateCSVProps = TemplateUtil.createStageAdvanceRecTemplateCSVProperties();

        List<StageAdvancedRecDTO> fileContent = new ArrayList<>();
        Boolean validFile = true;
        try {
            logger.info("Reading file for batchId " + batchId);
            fileContent = CsvFileUtil.readCSVFILE(new InputStreamReader(inputStream),
                    StageAdvancedRecDTO.class,
                    (CellProcessor[]) recognitionTemplateCSVProps.get(TemplateConstants.PROCESSORS),
                    (String[]) recognitionTemplateCSVProps.get(TemplateConstants.FIELD_MAPPING));
        } catch (Exception ex) {

            logger.info("Invalid file format for batchId " + batchId);
            // We should only get errors here if there's a formatting error with the file.
            logger.error(ex.getMessage());
            validFile = false;
            recognitionBulkResponseDTO.setBatchId(batchId);
            recognitionBulkResponseDTO.setFileName(file.getOriginalFilename());
            recognitionBulkResponseDTO.setProcessDate(DateUtil.getCurrentTimeInUTCDateTime());
            recognitionBulkResponseDTO.setStatus(StatusTypeCode.FAILED.name());
            batchFile.setFileUploadError("Invalid file format");
            batchFileDao.update(batchFile);
            batch.setStatusTypeCode(StatusTypeCode.FAILED.name());
            batchDao.save(batch);
        }

        //Only save records if it was a valid file
        if (validFile) {
            logger.info("Creating STAGE_ADVANCED_REC records for batchId " + batchId);
            try {
                addRecordsToStageAdvanceRec(batchId, fileContent);
            }catch(Exception ex){
                logger.info("Error while creating records in STAGE_ADVANCED_REC for batchId: " + batchId);
                logger.error(ex.getMessage(),ex);
                validFile = false;
                recognitionBulkResponseDTO.setBatchId(batchId);
                recognitionBulkResponseDTO.setFileName(file.getOriginalFilename());
                recognitionBulkResponseDTO.setProcessDate(DateUtil.getCurrentTimeInUTCDateTime());
                recognitionBulkResponseDTO.setStatus(StatusTypeCode.FAILED.name());
                batchFile.setFileUploadError("There was an error processing the file, possible reasons: One of the fields exceeded the max length allowed");
                batchFileDao.update(batchFile);
                batch.setStatusTypeCode(StatusTypeCode.FAILED.name());
                batchDao.save(batch);
                batchService.deleteStageAdvanceRecForBatch(batchId);
            }
        }

        //write the input stream to the database
        CRC32InputStream crc32InputStream = getCRC32InputStream(file.getContentType(), file.getOriginalFilename(), file.getInputStream());

        //Take the file (csv, xls, or xlsx) and save it into BATCH_FILE and get a CRC
        Reader reader = new InputStreamReader(crc32InputStream, StandardCharsets.UTF_8);
        appendToFileUpload(batchFile, reader);

        List<Map<String, Object>> listRecognitionBulkResponse = new ArrayList<>();
        if (validFile) {

            long crc32 = crc32InputStream.getCRC32();

            Pax pax = paxRepository.findOne(paxId);

            logger.info("Calling initial validation for batchId " + batchId);
            listRecognitionBulkResponse = recognitionBulkUploadDao.validateRecognitionBulkUpload
                    (batchId, pax.getControlNum(), String.valueOf(crc32), filename);
        }

        reader.close();
        crc32InputStream.close();
        inputStream.close();

        if (validFile) {
            recognitionBulkResponseDTO = mapToRecognitionBulkResponseDTO(listRecognitionBulkResponse);

            if(StatusTypeCode.FAILED.name().equals(recognitionBulkResponseDTO.getStatus())) {
                logger.info("batchId " + batchId + " failed validation. Marking as FAILED");
                List<Map<String, Object>> fileData = recognitionBulkUploadDao.getStageDataByBatchId(batchId);
                String fileUploadError = CsvFileUtil.buildCSVFileFromMap(fileData, TemplateUtil.createStageAdvanceRecCsvColumnProps());
                batchFile.setFileUploadError(fileUploadError);
                batchFileDao.update(batchFile);
                batchService.deleteStageAdvanceRecForBatch(batchId);
            }
        }

        logger.info("Recognition bulk upload file process complete.");
        return recognitionBulkResponseDTO;
    }

    /**
     * Run on a schedule to process oldest ADV_REC_BULK_UPLOAD batch that is QUEUED. Runs through nomination validation
     * asynchronously and then validates point amounts for the entire file.  If there are no errors, kicks off nomination
     * creation asynchronously.
     *
     */
    @Override
    @TriggeredTask(name="recognitionBulkUploadProcessJob")
    public void runBatchJob() {
        logger.info("Starting batch job.");
        // Find a batch to process - oldest ADV_REC_BULK_UPLOAD batch in QUEUED status
        Batch batchToProcess = batchDao.findBy()
            .where(ProjectConstants.BATCH_TYPE_CODE).eq(BatchType.ADV_REC_BULK_UPLOAD.name())
            .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.QUEUED.name())
            .asc(ProjectConstants.BATCH_ID)
            .findOne();

        if (batchToProcess == null) {
            logger.info("Ending batch job.");
            // no queued batches, we're done
            return;
        }

        logger.info("Processing batch " + batchToProcess.getBatchId());

        // update status and save immediately so we don't get duplicates
        batchToProcess.setStatusTypeCode(StatusTypeCode.IN_PROGRESS.name());
        batchDao.save(batchToProcess);

        // Note start time
        BatchEvent batchEventStart = new BatchEvent();
        batchEventStart.setBatchId(batchToProcess.getBatchId());
        batchEventStart.setBatchEventTypeCode(BatchEventType.START_TIME.getCode());
        batchEventStart.setMessage(DateUtil.getCurrentTimeInUTCDateTime());
        batchEventStart.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
        batchEventDao.save(batchEventStart);

        Long batchId = batchToProcess.getBatchId();

        Long maxNominationId = stageAdvancedRecDao.findBy()
                                    .where(ProjectConstants.BATCH_ID).eq(batchId)
                                    .desc(ProjectConstants.TEMP_NOMINATION_ID)
                                    .findOne(ProjectConstants.TEMP_NOMINATION_ID, Long.class);

        if (maxNominationId == null) {
            logger.info("No maxNominationId found, marking as complete and saving - batchId " + batchId);
            //Empty file - just complete the batch and return
            batchToProcess.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
            batchDao.save(batchToProcess);
        } else {
            List<Future<Boolean>> futureCollection = new ArrayList<>();
            for (Long tempNominationId = 1L; tempNominationId <= maxNominationId; tempNominationId ++) {
                logger.info("Creating async process for batchId " + batchId + ", tempNominationId " + tempNominationId);

                // validate
                futureCollection.add(
                        advancedRecognitionThreadService.validateNominationAsync(batchId, tempNominationId)
                        );
            }

            Boolean errorsOccurred = Boolean.FALSE;
            for (Future<Boolean> future : futureCollection) {
                try {
                    errorsOccurred = (future.get() || errorsOccurred);
                } catch (Exception e) {
                    logger.error(NominationConstants.UNEXPECTED_ERROR_OCCURRED_MESSAGE, e);

                    errorsOccurred = Boolean.TRUE;

                    List<StageAdvancedRec> recordList = stageAdvancedRecDao.findBy()
                            .where(ProjectConstants.BATCH_ID).eq(batchId)
                            .find();
                    applyErrorMessage(NominationConstants.UNEXPECTED_ERROR_OCCURRED_MESSAGE, recordList);
                }
            }

            logger.info("Errors after nomination validation? " + errorsOccurred);

            /*
             * These next two validation steps are meant to make sure that we don't overload a budget / giving limit
             * over the entire batch. Since we don't save any nominations in the batch unless all nominations pass
             * validation, we need to check that there are enough points to give the entire file even if every individual
             * nomination passes validation.
             */

            // do individual giving limit validation
            List<Map<String, Object>> paxBudgetDetailsMapList = recognitionBulkUploadDao.getPaxBudgetUsageDetails(batchId);
            for (Map<String, Object> paxBudgetDetailsMap : paxBudgetDetailsMapList) {
                errorsOccurred = (validatePaxBudgetTotal(paxBudgetDetailsMap, batchId) || errorsOccurred);
            }

            logger.info("Errors after individual giving limt validation? " + errorsOccurred);

            // make sure we aren't going over the total amount remaining in the budget
            List<Map<String, Object>> budgetDetailsMapList = recognitionBulkUploadDao.getBudgetUsageDetails(batchId);
            for (Map<String, Object> budgetDetailsMap : budgetDetailsMapList) {
                errorsOccurred = (validateBudgetTotal(budgetDetailsMap, batchId) || errorsOccurred);
            }

            logger.info("Errors after budget total validation? " + errorsOccurred);

            String batchFinalStatus = batchToProcess.getStatusTypeCode(); // default value
            if (Boolean.FALSE.equals(errorsOccurred)) {
                for (Long tempNominationId = 1L; tempNominationId <= maxNominationId; tempNominationId ++) {
                    logger.info("Creating nomination for batchId " + batchId + ", tempNominationId " + tempNominationId);
                    // create nominations and call it a day
                    advancedRecognitionThreadService.createNominationAsync(batchId, tempNominationId);
                }

                // mark batch status
                batchFinalStatus = StatusTypeCode.COMPLETE.name();
            }
            else {
                logger.info("Errors occurred.  Writing to file error.");

                // get records
                List<Map<String, Object>> fileData = recognitionBulkUploadDao.getStageDataByBatchId(batchId);

                // write to csv file
                String fileUploadError = CsvFileUtil.buildCSVFileFromMap(fileData, TemplateUtil.createStageAdvanceRecCsvColumnProps());

                // write to BATCH_FILE.FILE_UPLOAD_ERROR
                BatchFile batchFile = batchFileDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchId).findOne();
                batchFile.setFileUploadError(fileUploadError);
                batchFileDao.update(batchFile);

                logger.info("Wrote to batchFile.fileUploadError for batchId " + batchId);

                // mark batch status
                batchFinalStatus = StatusTypeCode.FAILED.name();
            }

            // update batch status
            batchToProcess.setStatusTypeCode(batchFinalStatus);
            batchDao.save(batchToProcess);

            logger.info("Updated batchId " + batchId + " to " + batchFinalStatus + " status.");

            // delete stage records
            List<StageAdvancedRec> advancedRecListToDelete = stageAdvancedRecDao.findBy()
                    .where(ProjectConstants.BATCH_ID).eq(batchId).find();
            for (StageAdvancedRec advanceRec : advancedRecListToDelete) {
                stageAdvancedRecDao.delete(advanceRec);
            }
        }

        // Note end time
        BatchEvent batchEventEnd = new BatchEvent();
        batchEventEnd.setBatchId(batchToProcess.getBatchId());
        batchEventEnd.setBatchEventTypeCode(BatchEventType.END_TIME.getCode());
        batchEventEnd.setMessage(DateUtil.getCurrentTimeInUTCDateTime());
        batchEventEnd.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
        batchEventDao.save(batchEventEnd);

        logger.info("Ending batch job.");
    }

    @Override
    public PaginatedResponseObject<RecognitionBulkResponseDTO> getBulkUploadHistory(
            PaginationRequestDetails requestDetails, String statusList, Integer pageNumber, Integer pageSize) {

        Integer totalResults = 0;

        List<Map<String, Object>> resultList = recognitionBulkUploadDao.getBulkUploadHistory(statusList, pageNumber, pageSize);

        List<RecognitionBulkResponseDTO> bulkUploadHistoryDTOList = new ArrayList<>();

        for (Map<String, Object> result : resultList) {

            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            RecognitionBulkResponseDTO rowData = new RecognitionBulkResponseDTO();

            rowData.setBatchId((Long) result.get(ProjectConstants.BATCH_ID));
            rowData.setStatus((String) result.get(ProjectConstants.STATUS));
            rowData.setFileName((String) result.get(ProjectConstants.FILE_NAME));
            rowData.setProcessDate(DateUtil.convertToUTCDateTime((Date) result.get(ProjectConstants.PROCESS_DATE)));
            rowData.setTotalRows(Integer.parseInt((String) result.get(RecognitionBulkUploadConstants.TOTAL_ROWS)));
            rowData.setTotalAwardAmount(Integer.parseInt((String) result.get(ProjectConstants.TOTAL_AWARDS)));
            rowData.setNominationCount(Integer.parseInt((String) result.get(ProjectConstants.NOMINATIONS)));
            rowData.setPppIndexApplied(Boolean.parseBoolean((String) result.get(ProjectConstants.PPP_INDEX_APPLIED)));
            bulkUploadHistoryDTOList.add(rowData);
        }

        return new PaginatedResponseObject<>(requestDetails, bulkUploadHistoryDTOList, totalResults);
    }

    private RecognitionBulkResponseDTO mapToRecognitionBulkResponseDTO(List<Map<String, Object>> list) {
        RecognitionBulkResponseDTO recognitionBulkResponseDTO = null;
        if (!CollectionUtils.isEmpty(list) && list.size()>0) {
            for (Map<String, Object> node : list) {
                recognitionBulkResponseDTO = new RecognitionBulkResponseDTO();
                recognitionBulkResponseDTO.setBatchId((Long) node.get(ProjectConstants.BATCH_ID));
                recognitionBulkResponseDTO.setFileName((String) node.get(ProjectConstants.FILE_NAME));
                recognitionBulkResponseDTO.setDuplicateFileContents
                (Boolean.valueOf((String) node.get(RecognitionBulkUploadConstants.DUPLICATE_FILE_CONTENTS)));
                recognitionBulkResponseDTO.setDuplicateFileName
                (Boolean.valueOf((String) node.get(RecognitionBulkUploadConstants.DUPLICATE_FILE_NAME)).booleanValue());
                recognitionBulkResponseDTO.setNominationCount
                (((Integer) node.get(RecognitionBulkUploadConstants.NOMINATION_COUNT)));
                recognitionBulkResponseDTO.setStatus((String) node.get(ProjectConstants.STATUS));
                recognitionBulkResponseDTO.setTotalAwardAmount
                (((Integer) node.get(RecognitionBulkUploadConstants.TOTAL_AWARD_AMOUNT)));
                recognitionBulkResponseDTO.setTotalRows
                (((Integer) node.get(RecognitionBulkUploadConstants.TOTAL_ROWS)));
                recognitionBulkResponseDTO.setPppIndexApplied
                (Boolean.parseBoolean((String) node.get(ProjectConstants.PPP_INDEX_APPLIED)));
            }
        }
        return recognitionBulkResponseDTO;
    }

    private CRC32InputStream getCRC32InputStream(String mimeType, String fileName, InputStream inputStream)
            throws Exception {
        if (fileName.endsWith(RecognitionBulkUploadConstants.XLS) ||
            fileName.endsWith(RecognitionBulkUploadConstants.XLSX)) {
            ExcelToCSVInputStream excelToCsvStream = new ExcelToCSVInputStream(inputStream);
            excelToCsvStream.setDelimiter(RecognitionBulkUploadConstants.DELIMITER);
            return new CRC32InputStream(excelToCsvStream.build());
        }
        else {
            return new CRC32InputStream(inputStream);
        }
    }

    private void appendToFileUpload(BatchFile batchFile, Reader reader) throws SQLException,IOException {

        logger.info("Writing to BATCH_FILE for batchId " + batchFile.getBatchId());
        char[] buf = new char[RecognitionBulkUploadConstants.APPEND_CHAR_BUFFER_LENGTH];
        int numRead = 0;
        String readData = null;
        String fileUploadConcat = null;
        if (batchFile != null){
            while ((numRead = reader.read(buf)) != -1) {
                readData = String.valueOf(buf, 0, numRead);
                if (readData != null) {
                    fileUploadConcat = batchFile.getFileUpload() + readData;
                    batchFile.setFileUpload(fileUploadConcat);
                    batchFileDao.update(batchFile);
                }
            }
        }
    }

    private void addRecordsToStageAdvanceRec(Long batchId, List<StageAdvancedRecDTO> content){
        StageAdvancedRec stageAdvancedRec = null;
        for (StageAdvancedRecDTO contentDTO : content) {
            stageAdvancedRec = new StageAdvancedRec();
            BeanUtils.copyProperties(contentDTO, stageAdvancedRec);
            stageAdvancedRec.setBatchId(batchId);
            if(stageAdvancedRec.getPublicHeadline()!=null &&
                    stageAdvancedRec.getPublicHeadline().length() > RecognitionBulkUploadConstants.PUBLIC_HEADLINE_MAX_LENGHT) {
                    //truncate the string to avoid application error
                    stageAdvancedRec.setPublicHeadline(stageAdvancedRec.getPublicHeadline()
                    .substring(RecognitionBulkUploadConstants.START_POSITION,
                    RecognitionBulkUploadConstants.PUBLIC_HEADLINE_MAX_LENGHT));
            }
            stageAdvancedRecDao.create(stageAdvancedRec);
        }
    }

    /**
     * Runs through budget validation for the pax / budget combo defined in the map entry.
     * Checks total amount of recognitions for that combo on the file against budget total / giving limit
     *
     * @param paxBudgetDetailsEntry - map of total points by pax / budget combo
     * @param batchId - id of batch so we can do error processing
     * @return boolean - were there any errors recorded
     */
    private Boolean validatePaxBudgetTotal(Map<String, Object> paxBudgetDetailsEntry, Long batchId) {

        // grab all our values from the map
        String controlNum = (String) paxBudgetDetailsEntry.get(ProjectConstants.CONTROL_NUM);
        Long paxId = (Long) paxBudgetDetailsEntry.get(ProjectConstants.PAX_ID);
        Long budgetId = (Long) paxBudgetDetailsEntry.get(ProjectConstants.BUDGET_ID);
        Double totalAmount = (Double) paxBudgetDetailsEntry.get(ProjectConstants.TOTAL_AMOUNT);
        Double individualGivingLimit = (Double) paxBudgetDetailsEntry.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT);

        // check individual giving limit
        logger.info("Checking individual giving limit for budgetId " + budgetId + ", paxId " + paxId + " batchId " + batchId);
        if (totalAmount != null && individualGivingLimit != null) {
            Double individualUsedAmount = budgetInfoDao.getIndividualUsedAmount(paxId, budgetId);
            Double totalIndividualRemaining = individualGivingLimit - individualUsedAmount;

            if (totalAmount > totalIndividualRemaining) {
                // we're above giving limit for that budget. Log error and return.

                logger.info("Went above giving limit for budgetId " + budgetId + ", paxId " + paxId + " batchId " + batchId);
                List<StageAdvancedRec> recordList = stageAdvancedRecDao.findBy()
                        .where(ProjectConstants.BATCH_ID).eq(batchId)
                        .and(ProjectConstants.BUDGET_ID).eq(budgetId)
                        .and(ProjectConstants.SUBMITTER_ID).eq(controlNum)
                        .find();

                // log errors
                applyErrorMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT_MSG, recordList);

                // Had errors :(

                logger.info("Had errors while validating pax budget total for budgetId " + budgetId + ", paxId " + paxId + " batchId " + batchId + ", returning TRUE");
                return Boolean.TRUE;
            }
        }

        // No errors :)
        logger.info("No errors while validating pax budget total for budgetId " + budgetId + ", paxId " + paxId + " batchId " + batchId + ", returning FALSE");
        return Boolean.FALSE;
    }
    /**
     * Runs through budget validation for the budget in the map entry.
     *
     * @param budgetDetailsEntry - map of total points by budget
     * @param batchId - id of batch so we can do error processing
     * @return boolean - were there any errors recorded
     */
    private Boolean validateBudgetTotal(Map<String, Object> budgetDetailsEntry, Long batchId) {

        // grab all our values from the map
        Long budgetId = (Long) budgetDetailsEntry.get(ProjectConstants.BUDGET_ID);
        Double totalAmount = (Double) budgetDetailsEntry.get(ProjectConstants.TOTAL_AMOUNT);
        Double budgetTotal = (Double) budgetDetailsEntry.get(ProjectConstants.BUDGET_TOTAL);

        // If we had errors, update all the records for that budget
        logger.info("Checking batch budget total for budgetId " + budgetId + " batchId " + batchId);
        if (totalAmount != null && totalAmount > budgetTotal) {
            logger.info("Went above budgetTotal for budgetId " + budgetId + " batchId " + batchId);
            List<StageAdvancedRec> recordList = stageAdvancedRecDao.findBy()
                                                .where(ProjectConstants.BATCH_ID).eq(batchId)
                                                .and(ProjectConstants.BUDGET_ID).eq(budgetId)
                                                .find();

            // log errors
            applyErrorMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGER_THAN_BUDGET_MSG, recordList);

            // Had errors :(
            logger.info("Had errors while validating batch budget total for budgetId " + budgetId + " batchId " + batchId + ", returning TRUE");
            return Boolean.TRUE;
        }

        // No errors :)
        logger.info("No errors while validating batch budget total for budgetId " + budgetId + " batchId " + batchId + ", returning FALSE");
        return Boolean.FALSE;
    }

    /**
     * applies the provided error message to each record in the provided list
     * won't apply the error if the record already has that error
     * will auto append a comma if the record already had other errors
     *
     * @param errorMessage - error message to be applied
     * @param recordList - list of STAGE_ADVANCED_REC records to apply error to
     */
    private void applyErrorMessage(String errorMessage, List<StageAdvancedRec> recordList) {
        for (StageAdvancedRec record : recordList) {
            String recordErrorMessage = record.getErrorMessage();
            if (recordErrorMessage != null) {
                if (recordErrorMessage.contains(errorMessage)) {
                    // we've already reported this error, skip
                    continue;
                }
                // we have at least one previous error, add this to it
                recordErrorMessage = recordErrorMessage.concat(ProjectConstants.COMMA_DELIM);
            }
            else {
                recordErrorMessage = ProjectConstants.EMPTY_STRING;
            }
            recordErrorMessage = recordErrorMessage.concat(errorMessage);

            // set error message and save
            record.setErrorMessage(recordErrorMessage);
            stageAdvancedRecDao.save(record);

        }
    }

    private InputStream getInputStream(String mimeType, String fileName, InputStream inputStream)
            throws Exception {
        if (fileName.endsWith(RecognitionBulkUploadConstants.XLS) ||
            fileName.endsWith(RecognitionBulkUploadConstants.XLSX)) {
            ExcelToCSVInputStream excelToCsvStream = new ExcelToCSVInputStream(inputStream);
            excelToCsvStream.setDelimiter(RecognitionBulkUploadConstants.DELIMITER);
            return excelToCsvStream.build();
        }
        else {
            return inputStream;
        }
    }
}
