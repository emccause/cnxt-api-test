package com.maritz.culturenext.recognition.services;

import java.util.List;

import com.maritz.culturenext.recognition.dto.RecognitionCriteriaDTO;

public interface RecognitionCriteriaService {
    
    /**
     * Return a list of all recognition criteria (values) configured.
     * 
     * @param status
     * @param languageCode
     * @param idList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338990/Value+Recognition+Criteria+Example
     */
    List<RecognitionCriteriaDTO> getRecognitionCriteria(String status, String languageCode, String idList);
    
    /**
     * Create a new recognition criteria (value)
     * 
     * @param recognitionCriteriaDTOList
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338990/Value+Recognition+Criteria+Example
     */
    List<RecognitionCriteriaDTO> insertRecognitionCriteria(List<RecognitionCriteriaDTO> recognitionCriteriaDTOList);
    
    /**
     * Update an existing recognition criteria (value)
     * 
     * @param recognitionCriteriaDTO
     * @param recognitionCriteriaId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/9338990/Value+Recognition+Criteria+Example
     */
    RecognitionCriteriaDTO updateRecognitionCriteria(RecognitionCriteriaDTO recognitionCriteriaDTO,
            Long recognitionCriteriaId);
    
    /**
     * Return recognition criteria details by ID
     * 
     * @param recognitionCriteriaId
     * @param languageCode
     */
    RecognitionCriteriaDTO getRecognitionCriteriaById(Long recognitionCriteriaId, String languageCode);
}