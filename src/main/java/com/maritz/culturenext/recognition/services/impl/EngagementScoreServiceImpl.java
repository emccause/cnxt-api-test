package com.maritz.culturenext.recognition.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Comment;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.PaxFiles;
import com.maritz.core.jpa.repository.LoginHistoryRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.security.Security;
import com.maritz.culturenext.util.PermissionUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.jpa.repository.CnxtNominationRepository;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.recognition.dao.EngagementScoreDao;
import com.maritz.culturenext.recognition.dto.EngagementScore;
import com.maritz.culturenext.recognition.dto.ScoreDTO;
import com.maritz.culturenext.recognition.services.EngagementScoreService;

@Component
public class EngagementScoreServiceImpl implements EngagementScoreService {

    @Inject private CnxtNominationRepository cnxtNominationRepository;
    @Inject private ConcentrixDao<Comment> commentDao;
    @Inject private ConcentrixDao<PaxFiles> paxFilesDao;
    @Inject private EngagementScoreDao engagementScoreDao;
    @Inject private LoginHistoryRepository loginHistoryRepository;
    @Inject private PermissionUtil permissionUtil;
    @Inject private Security security;

    @Override
    public EngagementScore getEngagementScoreByPaxId(Long paxId) {
        if (paxId == null) return null;

        EngagementScore engagementScore = new EngagementScore();
        ScoreDTO total = new ScoreDTO();
        ScoreDTO lastLogin = new ScoreDTO();
        ScoreDTO lastRecGiven = new ScoreDTO();
        ScoreDTO lastRecReceived = new ScoreDTO();
        ScoreDTO lastComment = new ScoreDTO();
        ScoreDTO profilePic = new ScoreDTO();

        DateTime now = new DateTime();
        Days diff = null;

        //Last Login (>30 Days, 0%, >15, 10%, <15, 20%)
        Date loginDate = loginHistoryRepository.findMostRecentLoginDateByPaxId(paxId);

        lastLogin.setMax(adjustScore(20));
        if (loginDate != null) {
            DateTime loginDateTime = new DateTime(loginDate);
            diff = Days.daysBetween(loginDateTime, now);

            if (diff.isLessThan(Days.days(15))) {
                lastLogin.setScore(adjustScore(20));
            } else if (diff.isLessThan(Days.days(30))) {
                lastLogin.setScore(adjustScore(10));
            } else {
                lastLogin.setScore(0);
            }
        } else {
            lastLogin.setScore(0);
        }

        engagementScore.setLastLogin(lastLogin);

        //Last Recognition Given (>30 Days, 0%, >15, 10%, <15, 20%)
        Nomination recGiven = cnxtNominationRepository.findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                paxId, StatusTypeCode.APPROVED.toString());

        if (recGiven != null) {
            Date recGivenDate = recGiven.getSubmittalDate();

            if (recGivenDate != null) {
                DateTime recGivenDateTime = new DateTime(recGivenDate);
                diff = Days.daysBetween(recGivenDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    lastRecGiven.setScore(adjustScore(20));
                } else if (diff.isLessThan(Days.days(30))) {
                    lastRecGiven.setScore(adjustScore(10));
                } else {
                    lastRecGiven.setScore(0);
                }
            }
        } else {
            lastRecGiven.setScore(0);
        }

        lastRecGiven.setMax(adjustScore(20));
        engagementScore.setLastRecGiven(lastRecGiven);


        //Last Recognition Received (>30 Days, 0%, >15, 10%, <15, 20%)
        Date recReceivedDate = cnxtNominationRepository.getLastRecReceived(paxId);

        if (recReceivedDate != null) {
            DateTime recReceivedDateTime = new DateTime(recReceivedDate);
            diff = Days.daysBetween(recReceivedDateTime, now);

            if (diff.isLessThan(Days.days(15))) {
                lastRecReceived.setScore(adjustScore(20));
            } else if (diff.isLessThan(Days.days(30))) {
                lastRecReceived.setScore(adjustScore(10));
            } else {
                lastRecReceived.setScore(0);
            }
        } else {
            lastRecReceived.setScore(0);
        }

        lastRecReceived.setMax(adjustScore(20));
        engagementScore.setLastRecReceived(lastRecReceived);


        //Last Comment (>30 Days, 0%, >15, 10%, <15, 20%)
        lastComment.setScore(0);
        Comment comment = commentDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .desc(ProjectConstants.COMMENT_DATE)
                .findOne();

        if (comment != null) {
            Date commentDate = comment.getCommentDate();
            if (commentDate != null) {
                DateTime commentDateTime = new DateTime(commentDate);
                diff = Days.daysBetween(commentDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    lastComment.setScore(20);
                } else if (diff.isLessThan(Days.days(30))) {
                    lastComment.setScore(10);
                } else {
                    lastComment.setScore(0);
                }
            }
        }

        lastComment.setMax(20);
        
        //Don't add this if the user doesn't have commenting permissions
        if (permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)) {
            engagementScore.setLastComment(lastComment);
        }


        //Profile Pic (N, 0%, Y, 20%)
        PaxFiles paxFiles = paxFilesDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .findOne();

        if (paxFiles != null) { // && (paxFiles.getPicture() != null || paxFile.getPictureIcon() != null) ) {
            profilePic.setScore(adjustScore(20));
        } else {
            profilePic.setScore(0);
        }

        profilePic.setMax(adjustScore(20));
        engagementScore.setProfilePic(profilePic);


        //Display the total % based on above criteria. 
        int totalScore = 0;
        
        //If the user does not have commenting permissions, last comment should not factor into total score
        if (!permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)) {
            totalScore = adjustScore(lastLogin.getScore())
                    + adjustScore(lastRecGiven.getScore())
                    + adjustScore(lastRecReceived.getScore())
                    + adjustScore(profilePic.getScore());
        } else {
            totalScore = lastLogin.getScore()
                + lastRecGiven.getScore()
                + lastRecReceived.getScore()
                + lastComment.getScore()
                + profilePic.getScore();
        }

        total.setMax(100);
        total.setScore(totalScore);
        engagementScore.setTotal(total);

        return engagementScore;        
    }

    @Override
    public EngagementScore getEngagementScore() {
        return getEngagementScoreByPaxId(security.getPaxId());
    }

    @Override
    public Integer bulkFetchEngagementScoreTotal(List<Long> paxIdList) {
        
        // grab this once
        Boolean commentingEnabled = permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE);
        
        // get our details
        List<Map<String, Object>> engagementScoreDetailsList = engagementScoreDao.getEngagementScoreDetails(paxIdList);
                
        Integer engagementScoreTotal = 0;
        for (Map<String, Object> engagementScoreEntry : engagementScoreDetailsList) {

            DateTime now = new DateTime();
            Days diff = null;

            // Last Login (>30 Days, 0%, >15, 10%, <15, 20%)
            Date loginDate = (Date) engagementScoreEntry.get(ProjectConstants.LOGIN_DATE);
            if (loginDate != null) {
                DateTime loginDateTime = new DateTime(loginDate);
                diff = Days.daysBetween(loginDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    engagementScoreTotal += adjustScore(20, commentingEnabled);
                } else if (diff.isLessThan(Days.days(30))) {
                    engagementScoreTotal += adjustScore(10, commentingEnabled);
                }
                // otherwise, don't increase 
            }

            // Last Recognition Given (>30 Days, 0%, >15, 10%, <15, 20%)
            Date nominationSubmittalDate = (Date) engagementScoreEntry.get(ProjectConstants.NOMINATION_SUBMITTAL_DATE);
            if (nominationSubmittalDate != null) {
                DateTime recGivenDateTime = new DateTime(nominationSubmittalDate);
                diff = Days.daysBetween(recGivenDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    engagementScoreTotal += adjustScore(20, commentingEnabled);
                } else if (diff.isLessThan(Days.days(30))) {
                    engagementScoreTotal += adjustScore(10, commentingEnabled);
                }
                // otherwise, don't increase 
            }

            // Last Recognition Received (>30 Days, 0%, >15, 10%, <15, 20%)
            Date recognitionReceivedDate = (Date) engagementScoreEntry.get(ProjectConstants.RECOGNITION_RECEIVED_DATE);
            if (recognitionReceivedDate != null) {
                DateTime recReceivedDateTime = new DateTime(recognitionReceivedDate);
                diff = Days.daysBetween(recReceivedDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    engagementScoreTotal += adjustScore(20, commentingEnabled);
                } else if (diff.isLessThan(Days.days(30))) {
                    engagementScoreTotal += adjustScore(10, commentingEnabled);
                }
                // otherwise, don't increase 
            }
            
            // Last Comment (>30 Days, 0%, >15, 10%, <15, 20%)
            // only check if commenting is enabled
            Date commentDate = (Date) engagementScoreEntry.get(ProjectConstants.COMMENT_DATE);
            if (Boolean.TRUE.equals(commentingEnabled) && commentDate != null) {
                DateTime commentDateDateTime = new DateTime(commentDate);
                diff = Days.daysBetween(commentDateDateTime, now);

                if (diff.isLessThan(Days.days(15))) {
                    engagementScoreTotal += adjustScore(20, commentingEnabled);
                } else if (diff.isLessThan(Days.days(30))) {
                    engagementScoreTotal += adjustScore(10, commentingEnabled);
                }
                // otherwise, don't increase 
                
            }
            
            // Profile Pic (N, 0%, Y, 20%)
            Long picId = (Long) engagementScoreEntry.get(ProjectConstants.PIC_ID);
            
            if (picId != null) {
                engagementScoreTotal += adjustScore(20, commentingEnabled);
            }
            // otherwise, don't increase 
            
        }
        
        engagementScoreTotal = Integer.valueOf((int) Math.ceil(engagementScoreTotal.floatValue() / 
                Integer.valueOf(paxIdList.size()).floatValue()));
        
        return engagementScoreTotal;
    }
    
    /**
     * Adjusts the passed in score based on the user's COMMENTING permissions
     * If the user does not have COMMENTING permissions, the scores need to be adjusted
     * as listed below so the totals still add up to 100 when 
     * last comment score is removed from the equation.
     *         20 points should become 25 points
            10 points should become 13 points (rounded up from 12.5)
     * @param Integer score - the score to be adjusted
     * @returns Integer - the adjusted score
     */
    private Integer adjustScore(Integer score) {
        return adjustScore(score, permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE));
    }
    /**
     * Adjusts the passed in score based on the user's COMMENTING permissions
     * If the user does not have COMMENTING permissions, the scores need to be adjusted
     * as listed below so the totals still add up to 100 when 
     * last comment score is removed from the equation.
     *         20 points should become 25 points
            10 points should become 13 points (rounded up from 12.5)
     * @param Integer score - the score to be adjusted
     * @param Boolean commentingEnabled - if commenting is enabled for the user.
     * @returns Integer - the adjusted score
     */
    private Integer adjustScore(Integer score, Boolean commentingEnabled) {
        if (Boolean.FALSE.equals(commentingEnabled)) {
            if (score.equals(20)) {
                return 25;
            } else if (score.equals(10)) {
                return 13;
            }
        }
        return score;
    }
}
