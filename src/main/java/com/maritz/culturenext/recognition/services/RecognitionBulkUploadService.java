package com.maritz.culturenext.recognition.services;

import org.springframework.web.multipart.MultipartFile;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.recognition.dto.RecognitionBulkResponseDTO;

public interface RecognitionBulkUploadService {

    /**
     *  Will handle a bulk upload of recognitions from the Bulk_Recognition_Template.csv template
     * 
     * @param paxId
     * @param file - file to be loaded into STAGE_ADVANCED_REC
     * @return RecognitionBulkResponseDTO with details
     * 
     * https://maritz.atlassian.net/wiki/display/M365/POST+bulk+upload+of+advanced+recognition
     */
    public RecognitionBulkResponseDTO recognitionsBulkUpload(Long paxId, MultipartFile file) throws Exception;
    
    /**
     * Run on a schedule to process oldest ADV_REC_BULK_UPLOAD batch that is QUEUED. Runs through nomination validation
     * asynchronously and then validates point amounts for the entire file.  If there are no errors, kicks off nomination
     * creation asynchronously. 
     * 
     */
    public void runBatchJob();
    
    /**
     *  Gets data needed for the bulk upload history tables
     *  
     *  @param requestDetails - object for PaginatedResponseObject containing details of request
     *  @param statusList - list of statuses as strings to search on
     *  @param pageNumber - page number for pagination
     *  @param pageSize - page size for pagination
     *  @return PaginatedResponseObject &lt;RecognitionBulkResponseDTO&gt; - list of responses containing the information needed for bulk upload history
     *  
     *  https://maritz.atlassian.net/wiki/display/M365/GET+Bulk+Advanced+Rec+History
     */
    public PaginatedResponseObject<RecognitionBulkResponseDTO> getBulkUploadHistory(PaginationRequestDetails requestDetails, String statusList, Integer pageNumber, Integer pageSize);
}
