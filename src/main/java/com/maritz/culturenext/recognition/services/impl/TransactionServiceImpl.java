package com.maritz.culturenext.recognition.services.impl;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.services.impl.TransactionBatch;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.core.util.ObjectUtils;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.TransactionConstants;
import com.maritz.culturenext.recognition.dao.TransactionDetailsDao;
import com.maritz.culturenext.recognition.dto.TransactionDetailsDTO;
import com.maritz.culturenext.recognition.dto.TransactionReversalDTO;
import com.maritz.culturenext.recognition.services.TransactionService;
import com.maritz.culturenext.util.DateUtil;

@Component
public class TransactionServiceImpl implements TransactionService {

    private Security security;
    private TransactionDetailsDao transactionDetailsDao;
    private com.maritz.core.services.TransactionService transactionService;

    @Inject
    public TransactionServiceImpl setSecurity(Security security) {
        this.security = security;
        return this;
    }

    @Inject
    public TransactionServiceImpl setTransactionDetailsDao(TransactionDetailsDao transactionDetailsDao) {
        this.transactionDetailsDao = transactionDetailsDao;
        return this;
    }

    @Inject
    public TransactionServiceImpl setTransactionService(com.maritz.core.services.TransactionService transactionService) {
        this.transactionService = transactionService;
        return this;
    }


    @Override
    public PaginatedResponseObject<TransactionDetailsDTO> getTransactionsSearch(PaginationRequestDetails requestDetails,
            String programIdCommaSeparated, String awardCodeCommaSeparated, String statusCommaSeparated,
            String recipientPaxIdCommaSeparated, String submitterPaxIdCommaSeparated, String programTypeCommaSeparated,
            String startDate, String endDate, Integer pageNumber, Integer pageSize) {

        List<String> programIdList = null;
        if (programIdCommaSeparated != null) {
            programIdList = StringUtils.parseDelimitedData(programIdCommaSeparated);
        }

        List<String> awardCodeList = null;
        if (awardCodeCommaSeparated != null) {
            awardCodeList = StringUtils.parseDelimitedData(awardCodeCommaSeparated);
        }

        List<String> statusList = null;
        if (statusCommaSeparated != null) {
            statusList = StringUtils.parseDelimitedData(statusCommaSeparated);
        }

        List<String> recipientPaxIdList = null;
        if (recipientPaxIdCommaSeparated != null) {
            recipientPaxIdList = StringUtils.parseDelimitedData(recipientPaxIdCommaSeparated);
        }

        List<String> submitterPaxIdList = null;
        if (submitterPaxIdCommaSeparated != null) {
            submitterPaxIdList = StringUtils.parseDelimitedData(submitterPaxIdCommaSeparated);
        }

        List<String> programTypeList = null;
        if (programTypeCommaSeparated != null) {
            programTypeList = StringUtils.parseDelimitedData(programTypeCommaSeparated);
        }

        // get data
        List<Map<String, Object>> resultList = transactionDetailsDao.getTransactionSearch(security.getPaxId(),
                programIdList, awardCodeList, statusList, recipientPaxIdList, submitterPaxIdList, programTypeList
                , DateUtil.convertToStartOfDayString(startDate), DateUtil.convertToEndOfDayString(endDate)
                , pageNumber, pageSize);

        // process data
        List<TransactionDetailsDTO> resultData = new ArrayList<>();
        Integer totalResults = 0;

        for (Map<String, Object> result : resultList) {
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);

            TransactionDetailsDTO rowData = new TransactionDetailsDTO();
            rowData.setAwardCode((String) result.get(ProjectConstants.AWARD_CODE));
            rowData.setDateCreated(DateUtil.convertToUTCDateTime((Date)
                    result.get(ProjectConstants.CREATE_DATE)));
            rowData.setStatus((String) result.get(ProjectConstants.STATUS));
            rowData.setNominationId((Long) result.get(ProjectConstants.NOMINATION_ID));
            rowData.setAwardAmount((Double) result.get(ProjectConstants.AWARD_AMOUNT));
            rowData.setIssuedByPax(new EmployeeDTO(result, ProjectConstants.SUBMITTER));
            rowData.setRecipientPax(new EmployeeDTO(result, ProjectConstants.RECIPIENT));
            rowData.setTransactionId((Long)result.get(ProjectConstants.TRANSACTION_ID));
            rowData.setRecipientCount((Integer) result.get(ProjectConstants.RECIPIENT_COUNT));
            rowData.setProgramType((String) result.get(ProjectConstants.PROGRAM_TYPE));
            rowData.setProgramName((String) result.get(ProjectConstants.PROGRAM_NAME));
            rowData.setPayoutType((String) result.get(ProjectConstants.PAYOUT_TYPE));
            resultData.add(rowData);
        }

        // Return paginated data
        return new PaginatedResponseObject<>(requestDetails, resultData, totalResults);
    }

    @Override
    @Transactional
    public void reverseTransactions(TransactionReversalDTO reversals) {
        Collection<ErrorMessage> errors = new ArrayList<>();

        //Null checks
        if (CollectionUtils.isEmpty(reversals.getTransactionIds())) {
            errors.add(TransactionConstants.MISSING_TRANSACTION_IDS_MESSAGE);
        }
        if (reversals.getReversalComment() == null || reversals.getReversalComment().trim().isEmpty()) {
            errors.add(TransactionConstants.MISSING_REVERSAL_COMMENT_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errors);

        //Find any associated raises and add those to the list as well.
        //Remove any transaction IDs that are not in APPROVED status
        Collection<TransactionHeader> transactionHeadersToReverse = transactionDetailsDao.getTransactionsToReverse(reversals.getTransactionIds());
        if (transactionHeadersToReverse != null && !transactionHeadersToReverse.isEmpty()) {

            //load existing transactions that need to be reversed
            Collection<TransactionHeaderDetail<?>> transactionsToReverse = transactionService.findByHeaders(transactionHeadersToReverse);

            //create reversal transactions and save them
            TransactionBatch<Discretionary> reversalTransactions = createReversalTransactions(transactionsToReverse);
            transactionService.create(reversalTransactions);

            transactionDetailsDao.reverseTransactions(extractTransactionHeaderIds(transactionHeadersToReverse), reversals.getReversalComment(), security.getPaxId());
        }
    }

    protected TransactionBatch<Discretionary> createReversalTransactions(Collection<TransactionHeaderDetail<?>> transactionsToReverse) {
        TransactionBatch<Discretionary> reversalBatch = new TransactionBatch<>();

        for (TransactionHeaderDetail<?> transactionToReverse : transactionsToReverse) {
            TransactionHeaderDetail<Discretionary> reversalTransaction = createReversalTransaction(transactionToReverse);
            reversalBatch.addTransaction(reversalTransaction);
        }

        return reversalBatch;
    }

    protected TransactionHeaderDetail<Discretionary> createReversalTransaction(TransactionHeaderDetail<?> transactionToReverse) {

        TransactionHeader header = ObjectUtils.objectToObject(transactionToReverse.getHeader(), TransactionHeader.class);
        header.setId(null);
        header.setStatusTypeCode(StatusTypeCode.APPROVED.name());
        header.setActivityDate(new Date());

        Discretionary detail = ObjectUtils.objectToObject(transactionToReverse.getDetail(), Discretionary.class);
        detail.setId(null);
        detail.setTransactionHeaderId(null);

        //negate the amount
        detail.setAmount(-detail.getAmount());

        List<TransactionHeaderMisc> miscs = null;
        List<TransactionHeaderMisc> reverseMiscs = transactionToReverse.getMiscs();
        if (reverseMiscs != null && !reverseMiscs.isEmpty()) {
            miscs = new ArrayList<>(reverseMiscs.size());
            for (TransactionHeaderMisc reverseMisc : reverseMiscs) {
                miscs.add(new TransactionHeaderMisc()
                    .setVfName(reverseMisc.getVfName())
                    .setMiscData(reverseMisc.getMiscData())
                    .setMiscDate(reverseMisc.getMiscDate())
                    .setMiscTypeCode(reverseMisc.getMiscTypeCode())
                );
            }
        }

        return new TransactionHeaderDetail<>(header, detail, miscs, transactionToReverse.getPaxId());
    }

    protected List<Long> extractTransactionHeaderIds(Collection<TransactionHeader> transactionHeaders) {
        return transactionHeaders.stream()
            .map(h -> h.getId())
            .collect(toList())
        ;
    }

}
