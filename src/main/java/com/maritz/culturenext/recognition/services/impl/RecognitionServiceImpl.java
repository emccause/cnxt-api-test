package com.maritz.culturenext.recognition.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.util.PaginationUtil;
import com.maritz.culturenext.util.PaxUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.TransactionDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.constants.RecognitionConstants;
import com.maritz.culturenext.recognition.dao.RecognitionDetailsDao;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RaiseService;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.recognition.util.RecognitionSortUtil;
import com.maritz.culturenext.reports.constants.ParticipantReportsConstants;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.nomination.dao.NominationDao;
import com.maritz.culturenext.transaction.dao.TransactionDao;

@Service
public class RecognitionServiceImpl implements RecognitionService {

    @Inject private ApprovalService approvalService;
    @Inject private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Inject private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Inject private ConcentrixDao<Budget> budgetDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private NominationDao nominationsDao;
    @Inject private PaxUtil paxUtil;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private RaiseService raiseService;
    @Inject private RecognitionDetailsDao recognitionDetailsDao;
    @Inject private Security security;
    @Inject private TransactionDao transactionDao;

    @Override
    public Integer getRecognitionCountByRecipient(Long paxId) {
        return cnxtRecognitionRepository.getRecognitionCountByRecipient(paxId);
    }

    @Override
    public List<RecognitionDetailsDTO> getRecognitionDetailsByIds(List<Long> recognitionIdList) {

        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<>();

        //Get recognitions by Ids
        List<Recognition> recognitionList = new ArrayList<>();
        
        Iterable<List<Long>> recognitionIdListSplit =
                Iterables.partition(recognitionIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> set : recognitionIdListSplit) {
            recognitionList.addAll(recognitionDao.findBy()
                    .where(ProjectConstants.ID).in(set)
                    .find());
        }

        if (!recognitionList.isEmpty()) {

            //Get list of nomination ids & recievedPax ids
            List<Long> nominationIdList = new ArrayList<>();
            List<Long> paxIdList = new ArrayList<>();
            for (Recognition recognition : recognitionList) {
                if (!nominationIdList.contains(recognition.getNominationId())) {
                    nominationIdList.add(recognition.getNominationId());
                }
                
                if (!paxIdList.contains(recognition.getReceiverPaxId())) {
                    paxIdList.add(recognition.getReceiverPaxId());
                }
            }

            //Get nominations by ids
            List<Nomination> nominationList = new ArrayList<>();
            Iterable<List<Long>> nominationIdListSplit =
                    Iterables.partition(nominationIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
            for (List<Long> set : nominationIdListSplit) {
                nominationList.addAll(nominationDao.findById(set));
            }

            //Get lists of budget ids & program ids
            //Map nominationId to nomination
            List<Long> budgetIdList = new ArrayList<>();
            List<Long> programIdList = new ArrayList<>();
            Map<Long, Nomination> nominationIdToNominationMap = new HashMap<>();
            for (Nomination nomination : nominationList) {
                nominationIdToNominationMap.put(nomination.getId(), nomination);
                paxIdList.add(nomination.getSubmitterPaxId());
                if (!programIdList.contains(nomination.getProgramId())) {
                    programIdList.add((nomination.getProgramId()));
                }
                if (nomination.getBudgetId() != null && !budgetIdList.contains(nomination.getBudgetId())) {
                    budgetIdList.add(nomination.getBudgetId());
                }
            }

            //Get budgets by budgetIds
            //Map budgetId to budget
            Map<Long, String> budgetIdToBudgetNameMap = new HashMap<>();
            Iterable<List<Long>> budgetIdListSplit =
                    Iterables.partition(budgetIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
            for (List<Long> set : budgetIdListSplit) {
                List<Budget> budgetList = budgetDao.findById(set);
                for (Budget budget : budgetList) {
                    budgetIdToBudgetNameMap.put(budget.getId(), budget.getBudgetName());
                }
            }

            //Get programs by programIds
            //Map programIds to program
            Map<Long, String> programIdToProgramNameMap = new HashMap<>();
            Iterable<List<Long>> programIdListSplit =
                    Iterables.partition(programIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
            for (List<Long> set : programIdListSplit) {
                List<Program> programList = programDao.findBy()
                        .where(ProjectConstants.PROGRAM_ID).in(set)
                        .find();
                for (Program program : programList) {
                    programIdToProgramNameMap.put(program.getProgramId(), program.getProgramName());
                }
            }

            //Get map of paxId to EmployeeDTO
            Map<Long, EmployeeDTO> paxIdToEmployeeObjectMap = new HashMap<>();
            Iterable<List<Long>> paxIdListSplit =
                    Iterables.partition(paxIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
            for (List<Long> set : paxIdListSplit) {
                paxIdToEmployeeObjectMap.putAll(paxUtil.getPaxIdEmployeeDTOMap(set));
            }
            
            Long loggedPaxId = security.getPaxId();

            //Build recognition detail list
            for (Recognition recognition : recognitionList) {
                RecognitionDetailsDTO recognitionDetails = new RecognitionDetailsDTO();
                Nomination nomination = nominationIdToNominationMap.get(recognition.getNominationId());

                recognitionDetails.setId(recognition.getId());
                recognitionDetails.setAwardAmount(recognition.getAmount());
                recognitionDetails.setStatus(recognition.getStatusTypeCode());
                recognitionDetails.setNominationId(recognition.getNominationId());
                recognitionDetails.setComment(recognition.getComment());
                recognitionDetails.setToPax(paxIdToEmployeeObjectMap.get(recognition.getReceiverPaxId()));

                recognitionDetails.setFromPax(paxIdToEmployeeObjectMap.get(nomination.getSubmitterPaxId()));
                recognitionDetails.setBudgetId(nomination.getBudgetId());
                recognitionDetails.setNominationComment(nomination.getComment());
                recognitionDetails.setHeadlineComment(nomination.getHeadlineComment());

                recognitionDetails.setBudgetName(budgetIdToBudgetNameMap.get(nomination.getBudgetId()));

                recognitionDetails.setProgram(programIdToProgramNameMap.get(nomination.getProgramId()));
                
                // approval map hasn't been provided on this method, pointsIssued for approvers is not required.
                recognitionDetails.setPointsIssued(
                        pointsIssuedDetails(loggedPaxId, recognitionDetails, false) ? recognition.getAmount() : null);

                recognitionDetailsList.add(recognitionDetails);
            }
        }

        return recognitionDetailsList;
    }

    @Deprecated
    @Override
    public List<RecognitionDetailsDTO> getRecognitionDetails(Long nominationId, Boolean excludePrivate,
                                    Long managerPaxId, String delimitedStatusList, Boolean returnApproverRecPoints) {
        
        if (managerPaxId != null && !security.getPaxId().equals(managerPaxId)) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                            .setField(ProjectConstants.MANAGER_PAX_ID)
                            .setCode(NominationConstants.ERROR_INVALID_MANAGER_PAX_ID)
                            .setMessage(NominationConstants.ERROR_INVALID_MANAGER_PAX_ID_MSG)
            );
        }

        if (nominationId == null) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                            .setField(ProjectConstants.NOMINATION_ID)
                            .setCode(NominationConstants.ERROR_INVALID_NOMINATION)
                            .setMessage(NominationConstants.ERROR_INVALID_NOMINATION_MSG)
            );
        }
        
        if(returnApproverRecPoints == null){
            returnApproverRecPoints = false;
        }

        // Get all relevant data (nomination, recognitions, pax objects)
        Nomination nomination = nominationDao.findById(nominationId);

        if (nomination == null) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                            .setField(ProjectConstants.NOMINATION_ID)
                            .setCode(NominationConstants.ERROR_NOMINATION_NOT_EXIST)
                            .setMessage(NominationConstants.ERROR_NOMINATION_NOT_EXIST_MSG)
            );
        }

        List<Recognition> recognitions;

        // we'll also want to add a status query param to only return APPROVED
        if (delimitedStatusList != null) {
            recognitions = recognitionDao.findBy()
                    .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                    .and(ProjectConstants.STATUS_TYPE_CODE).in(StringUtils.parseDelimitedData(delimitedStatusList))
                    .find();
        } else {
            recognitions = recognitionDao.findBy()
                    .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                    .and(ProjectConstants.STATUS_TYPE_CODE).ne(StatusTypeCode.REVERSED.name())
                    .find();
        }

        if (excludePrivate == null) {
            excludePrivate = false;
        }

        List<Long> paxIds = new ArrayList<>();
        List<Long> recognitionIds = new ArrayList<>();
        paxIds.add(nomination.getSubmitterPaxId());
        for (Recognition recognition : recognitions) {
            paxIds.add(recognition.getReceiverPaxId());
            recognitionIds.add(recognition.getId());
        }
        
        //Convert the recognitionId list into an iterable, so we can call the SQL in sets of 2000 (max SQL param limit)
        Iterable<List<Long>> recognitionIdIterable = Iterables.partition(recognitionIds, ProjectConstants.SQL_PARAM_COUNT_MAX);

        // Get ApprovalPending entities for each recognition
        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        for (List<Long> recognitionSet : recognitionIdIterable) {
            List<ApprovalPending> approvalPendingSubList = approvalPendingDao.findBy()
                    .where(ProjectConstants.TARGET_ID).in(recognitionSet)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                    .find();
            if (!CollectionUtils.isEmpty(approvalPendingSubList)) {
                approvalPendingList.addAll(approvalPendingSubList);
            }
        }
        
        // Get ApprovalHistory entities for recognitions without approvalPending entries
        List<ApprovalHistory> approvalHistoryList = new ArrayList<>();
        for (List<Long> recognitionSet : recognitionIdIterable) {
            List<ApprovalHistory> approvalHistorySubList = approvalHistoryDao.findBy()
                    .where(ProjectConstants.TARGET_ID).in(recognitionSet)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                    .find();
            if (!CollectionUtils.isEmpty(approvalHistorySubList)) {
                approvalHistoryList.addAll(approvalHistorySubList);
            }
        }

        // Add approvers to paxIds and set up map for the later combine method
        Map<Long, Long> recognitionToApproverMap = new HashMap<>();
        Map<Long, String> recognitionToApprovalCommentMap = new HashMap<>();
        for (ApprovalPending approvalPending : approvalPendingList) {
            paxIds.add(approvalPending.getPaxId());
            recognitionToApproverMap.put(approvalPending.getTargetId(), approvalPending.getPaxId());
        }
        if (approvalHistoryList != null) {
            for (ApprovalHistory approvalHistory : approvalHistoryList) {
                paxIds.add(approvalHistory.getPaxId());
                recognitionToApproverMap.put(approvalHistory.getTargetId(), approvalHistory.getPaxId());
                recognitionToApprovalCommentMap.put(approvalHistory.getTargetId(),
                        approvalHistory.getApprovalNotes());
            }
        }

        List<Long> nominationRecipients = raiseService.getRecipientsOnNomination(nominationId, excludePrivate);
        
        Map<Long, EmployeeDTO> employeeMap = paxUtil.getPaxIdEmployeeDTOMap(paxIds);

        Map<Long, Date> recognitionIdToApprovalDateMap = new HashMap<>();
        for (List<Long> recognitionSet : recognitionIdIterable) {
            recognitionIdToApprovalDateMap.putAll(
                approvalService.determineApprovalTimestampByRecognitionId(recognitionSet));
        }

        return combineDataToRecognitionDetails(nomination, recognitions, employeeMap, recognitionToApproverMap,
                nominationRecipients, recognitionIdToApprovalDateMap,
                recognitionToApprovalCommentMap, returnApproverRecPoints);
    }

    @Deprecated
    @Override
    public List<RecognitionDetailsDTO> getRecognitionDetailsPagination(Long nominationId, Boolean excludePrivate,
                    Long managerPaxId, String status, Integer page, Integer size, Boolean returnApproverRecPoints) {
        
        return PaginationUtil.getPaginatedList(
                getRecognitionDetails(nominationId, excludePrivate, managerPaxId, status, returnApproverRecPoints),
                page, size);
    }

    @Override
    public PaginatedResponseObject<RecognitionDetailsDTO> getRecognitionDetails(
            PaginationRequestDetails requestDetails, Long nominationId, Boolean overrideVisibilty, Boolean showPoints,
            Long paxId, String commaSeparatedStatusTypeCodeList, Long notificationId, Integer pageNumber, Integer pageSize
        ) {
        
        if (overrideVisibilty == null) {
            // Avoid a 1 == NULL check which could cause execution plan problems
            overrideVisibilty = Boolean.FALSE;
        }
        
        // handle context / role based point view override
        Boolean overridePointsLogic = Boolean.FALSE;
        if (Boolean.TRUE.equals(showPoints)) {
            // admins and managers have access to contexts that should show all points
            overridePointsLogic = security.hasRole(PermissionConstants.ADMIN_ROLE_NAME, ProjectConstants.MANAGER_ROLE);
        }
        
        // Check if we're handling in the context of a notification - make sure it exists for this pax
        // if this is false, don't show points to the approver pax (no notification exists or we're not in notification view)
        Boolean showApproverPoints = false;
        if (notificationId != null) {
            Alert alert = alertDao.findBy()
                    .where(ProjectConstants.ID).eq(notificationId)
                    .and(ProjectConstants.PAX_ID).eq(paxId)
                    .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.RECOGNITION_APPROVAL.name())
                    .and(ProjectConstants.TARGET_ID).eq(nominationId)
                    .findOne();
            // id provided is an approval alert for the logged in user for this nomination
            if (alert != null) {
                showApproverPoints = Boolean.TRUE;
            }
            
        }
        
        List<Map<String, Object>> resultList = recognitionDetailsDao.getRecognitionsForNomination(
                paxId, nominationId, overrideVisibilty, commaSeparatedStatusTypeCodeList, pageNumber, pageSize
            );

        DateFormat sdf = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT, Locale.ENGLISH);
        Integer totalResults = 0;
        List<RecognitionDetailsDTO> dtoList = new ArrayList<>();
        List<Long> paxIdToResolveList = new ArrayList<>();
        
        for (Map<String, Object> result : resultList) {
            totalResults = (Integer) result.get(ProjectConstants.TOTAL_RESULTS_KEY);
            
            RecognitionDetailsDTO dto = new RecognitionDetailsDTO();
            dto.setId((Long) result.get(ProjectConstants.ID));
            dto.setStatus((String) result.get(ProjectConstants.STATUS_TYPE_CODE));
            dto.setNominationId((Long) result.get(ProjectConstants.NOMINATION_ID));
            dto.setBudgetId((Long) result.get(ProjectConstants.BUDGET_ID));
            dto.setProgram((String) result.get(RecognitionConstants.PROGRAM));
            dto.setApprovalComment((String) result.get(RecognitionConstants.APPROVAL_COMMENT));
            dto.setComment((String) result.get(ProjectConstants.COMMENT));
            dto.setNominationComment((String) result.get(RecognitionConstants.NOMINATION_COMMENT));
            dto.setHeadlineComment((String) result.get(ProjectConstants.HEADLINE_COMMENT));
            dto.setBudgetName((String) result.get(ProjectConstants.BUDGET_NAME));
            dto.setPointsIssued((Double) result.get(ProjectConstants.AMOUNT)); // we'll handle this later
            dto.setRaiseAmount((Double) result.get(RecognitionConstants.RAISE_AMOUNT));
            
            Date submittalDate = (Date) result.get(ProjectConstants.SUBMITTAL_DATE);
            if (submittalDate != null) {
                dto.setDate(sdf.format(submittalDate));    
            }
            dto.setTransactionHeaderId((Long) result.get(ProjectConstants.TRANSACTION_HEADER_ID));
            
            // conditional logic / placeholders
            Long submitterPaxId = (Long) result.get(ProjectConstants.SUBMITTER_PAX_ID);
            if (submitterPaxId != null) {
                // drop in a placeholder that we'll resolve later
                dto.setFromPax(new EmployeeDTO().setPaxId(submitterPaxId));
                if (!paxIdToResolveList.contains(submitterPaxId)) {
                    paxIdToResolveList.add(submitterPaxId);
                }
            }
            Long receiverPaxId = (Long) result.get(ProjectConstants.RECEIVER_PAX_ID);
            if (receiverPaxId != null) {
                // drop in a placeholder that we'll resolve later
                dto.setToPax(new EmployeeDTO().setPaxId(receiverPaxId));
                if (!paxIdToResolveList.contains(receiverPaxId)) {
                    paxIdToResolveList.add(receiverPaxId);
                }
            }
            Long approverPaxId = (Long) result.get(RecognitionConstants.APPROVAL_PENDING_PAX_ID);
            if (approverPaxId == null) {
                // might have already been approved by someone
                approverPaxId = (Long) result.get(RecognitionConstants.APPROVAL_HISTORY_PAX_ID);
            }
            // will be null for AUTO_APPROVED
            if (approverPaxId != null) {
                // drop in a placeholder that we'll resolve later
                dto.setApprovalPax(new EmployeeDTO().setPaxId(approverPaxId));
                if (!paxIdToResolveList.contains(approverPaxId)) {
                    paxIdToResolveList.add(approverPaxId);
                }
            }
            Date approvalDate = (Date) result.get(ProjectConstants.LAST_APPROVAL_DATE);
            if (approvalDate == null) {
                // The only time this should happen is for auto approved recognitions
                // so we use the create date of the recognition
                approvalDate = (Date) result.get(ProjectConstants.CREATE_DATE);
            }
            dto.setApprovalTimestamp(approvalDate);
            
            dtoList.add(dto);
        }
        Map<Long, EmployeeDTO> employeeDtoMap = paxUtil.getPaxIdEmployeeDTOMap(paxIdToResolveList);
        
        // drop in placeholders, handle pointsIssued
        for (RecognitionDetailsDTO dto : dtoList) {
            // resolve placeholders, if pax isn't null it will have an id
            if (dto.getToPax() != null) {
                dto.setToPax(employeeDtoMap.get(dto.getToPax().getPaxId()));
            }
            
            if (dto.getFromPax() != null) {
                dto.setFromPax(employeeDtoMap.get(dto.getFromPax().getPaxId()));
            }

            if (dto.getApprovalPax() != null) {
                dto.setApprovalPax(employeeDtoMap.get(dto.getApprovalPax().getPaxId()));
            }
            
            // Make sure that we don't show points if the logged in user shouldn't be able to see points
            if (Boolean.FALSE.equals(overridePointsLogic) && Boolean.FALSE.equals(pointsIssuedDetails(paxId, dto, showApproverPoints))) {
                dto.setPointsIssued(null);
                dto.setRaiseAmount(null);
            }
        }

        return new PaginatedResponseObject<>(requestDetails, dtoList, totalResults);
    }

    /**
     * combines data into a list of recognition details
     * @param nominationList nomination object that recognitions belong to
     * @param recognitionList recognition list to process
     * @param employeeDtoList list of all pax related to the nomination - submitter, receiver(s), approver(s)
     * @param recognitionIdToApproverIdMap map of recognition ids and the pax id of the approver for that recognition
     * @param nominationRecipientIdList list of pax ids for recognition recipients
     * @param recognitionIdToApprovalDateMap map of recognition ids and their approval date (if approved)
     * @param recognitionIdToApprovalCommentMap map of recognition ids and their approval comments (if they exist)
     * @param returnApproverRecPoints should points be returned
     * @return list of recognition details
     */
    private List<RecognitionDetailsDTO> combineDataToRecognitionDetails(Nomination nominationList,
                                                                List<Recognition> recognitionList,
                                                                Map<Long, EmployeeDTO> employeeDtoList,
                                                                Map<Long, Long> recognitionIdToApproverIdMap,
                                                                List<Long> nominationRecipientIdList,
                                                                Map<Long, Date> recognitionIdToApprovalDateMap,
                                                                Map<Long, String> recognitionIdToApprovalCommentMap,
                                                                Boolean returnApproverRecPoints) {
        
        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<>();

        EmployeeDTO giverPax = employeeDtoList.get(nominationList.getSubmitterPaxId());
        Program program = programDao.findById(nominationList.getProgramId());

        Long loggedPaxId = security.getPaxId();
        
        for (Recognition recognition : recognitionList) {
            if (nominationRecipientIdList.contains(recognition.getReceiverPaxId())) {
                RecognitionDetailsDTO recognitionDetails = new RecognitionDetailsDTO();
                Budget budget = budgetDao.findById(nominationList.getBudgetId());

                recognitionDetails.setId(recognition.getId());
                recognitionDetails.setFromPax(giverPax);
                recognitionDetails.setToPax(employeeDtoList.get(recognition.getReceiverPaxId()));
                recognitionDetails.setStatus(recognition.getStatusTypeCode());
                recognitionDetails.setNominationId(recognition.getNominationId());
                recognitionDetails.setApprovalPax(employeeDtoList.get(
                        recognitionIdToApproverIdMap.get(recognition.getId())));
                recognitionDetails.setApprovalTimestamp(recognitionIdToApprovalDateMap.get(recognition.getId()));
                recognitionDetails.setBudgetId(nominationList.getBudgetId());
                recognitionDetails.setProgram(program.getProgramName());
                recognitionDetails.setApprovalComment(recognitionIdToApprovalCommentMap.get(recognition.getId()));
                recognitionDetails.setComment(recognition.getComment());
                recognitionDetails.setNominationComment(nominationList.getComment());
                recognitionDetails.setHeadlineComment(nominationList.getHeadlineComment());
                if (budget != null) {
                    recognitionDetails.setBudgetName(budget.getBudgetName());
                }

                String formatDate = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT, Locale.ENGLISH)
                    .format(nominationList.getSubmittalDate());

                recognitionDetails.setDate(formatDate);
                
                recognitionDetails.setPointsIssued(
                        pointsIssuedDetails(loggedPaxId, recognitionDetails, returnApproverRecPoints) ?
                                recognition.getAmount() : null);        
                
                recognitionDetailsList.add(recognitionDetails);
            }
        }
        Collections.sort(recognitionDetailsList, RecognitionSortUtil.firstAndLastNameComparator);
        return recognitionDetailsList;
    }
    
    /**
     * If paxId is the giver, receiver, receiver's manager or approver (only if it is required) 
     * points issued should be populated.
     * @param paxId logged in pax id
     * @param recognitionDetails details object to process
     * @param returnApproverRecPoints should points be shown to approver
     */
    private Boolean pointsIssuedDetails(Long paxId, RecognitionDetailsDTO recognitionDetails,
                                        Boolean returnApproverRecPoints) {
        
        if (paxId == null) return false; //Sending of approval emails - don't have paxId
        
        boolean isPaxGiver = recognitionDetails.getFromPax() != null ?
                paxId.equals(recognitionDetails.getFromPax().getPaxId()) : false;
        boolean isPaxReceiver = recognitionDetails.getToPax() != null ?
                paxId.equals(recognitionDetails.getToPax().getPaxId()) : false;
        boolean isPaxReceiverManager = recognitionDetails.getToPax() != null ?
                paxId.equals(recognitionDetails.getToPax().getManagerPaxId()) : false;
        boolean isPaxApprover = recognitionDetails.getApprovalPax() != null ?
                paxId.equals(recognitionDetails.getApprovalPax().getPaxId()) : false;
        
        return (isPaxGiver || isPaxReceiver || isPaxReceiverManager) 
                    && !recognitionDetails.getStatus().contains(StatusTypeCode.REJECTED.name()) 
                || (returnApproverRecPoints && isPaxApprover);    
    }

    @Override
    public TransactionDTO getNominationOrTransactionByID(String programType, Long transactionHeaderId) {
        TransactionDTO transactionDTO = null;
        if ( programType == null || 
                 !ProgramTypeEnum.getAllProgramTypes().contains(programType)) {
                //invalid program type
                throw new ErrorMessageException()
                .addErrorMessage(ParticipantReportsConstants.INVALID_PROGRAM_TYPE_MESSAGE);
        }    
            
        if (ProgramTypeEnum.POINT_LOAD.toString().equals(programType)) {
            List<Map<String, Object>> resultList = transactionDao.
                    getTransactionById(transactionHeaderId);
            
            transactionDTO = mapListToTransactionDTO(programType, resultList);
            
        } else {

            List<Map<String, Object>> resultList = nominationsDao.
                    getNominationById(transactionHeaderId);
                    
            transactionDTO = mapListToTransactionDTO(programType, resultList);
        }
        return transactionDTO;
    }

    private TransactionDTO mapListToTransactionDTO (String programType, List<Map<String, Object>> resultList){
        DateFormat sdf = new SimpleDateFormat(DateConstants.ISO_8601_DATE_FORMAT, Locale.ENGLISH);
        TransactionDTO transactionDto = null;
        if (!CollectionUtils.isEmpty(resultList)) {
            Map<String, Object> result = (Map<String, Object>)resultList.get(0);
            transactionDto = new TransactionDTO();
            
            if (ProgramTypeEnum.POINT_LOAD.toString().equals(programType)) {
                //For point loads, populate transactionId on the DTO. 
                // toPax populated approverPax 
                transactionDto.setTransactionId((Long) result.get(ProjectConstants.TRANSACTION_ID));
                
                Date createDate = (Date) result.get(ProjectConstants.CREATE_DATE);
                if (createDate != null) {
                    transactionDto.setCreateDate(sdf.format(createDate));
                }
                
                transactionDto.setProgramName((String) result.get(ProjectConstants.PROGRAM_NAME));
                transactionDto.setStatus((String) result.get(ProjectConstants.STATUS));
                transactionDto.setHeadline((String) result.get(ProjectConstants.HEADLINE));
                transactionDto.setComment((String) result.get(ProjectConstants.COMMENT));
                transactionDto.setReversalComment((String) result.get(ProjectConstants.REVERSAL_COMMENT));
                transactionDto.setPayoutType((String) result.get(ProjectConstants.PAYOUT_TYPE));
                transactionDto.setAwardAmount((Double) result.get(ProjectConstants.AWARD_AMOUNT));
                
                Long toPax = (Long) result.get(ProjectConstants.TO_PAX);
                if(toPax != null){
                    transactionDto.setToPax(participantInfoDao.getEmployeeDTO(toPax));
                }
                
                Long approverPax = (Long) result.get(ProjectConstants.APPROVER_PAX);
                if(approverPax != null){
                    transactionDto.setApproverPax(participantInfoDao.getEmployeeDTO(approverPax));
                }
            }else{
                //They will only need nominationId, fromPax, createDate, program name, status, headline, comment, payoutType.
                //(leave transactionId null since there could be multiple transactions)
                transactionDto.setNominationId((Long) result.get(ProjectConstants.NOMINATION_ID));
                
                Long fromPax = (Long) result.get(ProjectConstants.FROM_PAX);
                if(fromPax != null){
                    transactionDto.setFromPax((participantInfoDao.getEmployeeDTO(fromPax)));
                }
                
                Date createDate = (Date) result.get(ProjectConstants.CREATE_DATE);
                if (createDate != null) {
                    transactionDto.setCreateDate(sdf.format(createDate));
                }
                
                transactionDto.setProgramName((String) result.get(ProjectConstants.PROGRAM_NAME));
                transactionDto.setStatus((String) result.get(ProjectConstants.STATUS));
                transactionDto.setHeadline((String) result.get(ProjectConstants.HEADLINE));
                transactionDto.setComment((String) result.get(ProjectConstants.COMMENT));
                transactionDto.setPayoutType((String) result.get(ProjectConstants.PAYOUT_TYPE));
            }
        }
        return transactionDto;
    }
    
}
