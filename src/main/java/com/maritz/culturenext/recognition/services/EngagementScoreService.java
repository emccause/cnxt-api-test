package com.maritz.culturenext.recognition.services;

import java.util.List;

import com.maritz.culturenext.recognition.dto.EngagementScore;

public interface EngagementScoreService {

    /**
     * Return a specific user's engagement score
     * 
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7798815/Engagement+Score+Example
     */
    EngagementScore getEngagementScoreByPaxId(Long paxId);
    
    /**
     * Return the engagement score for the logged in pax ID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/7798815/Engagement+Score+Example
     */
    EngagementScore getEngagementScore();
    
    /**
     * TODO javadocs
     * 
     * @param paxIdList
     * @return
     */
    Integer bulkFetchEngagementScoreTotal(List<Long> paxIdList);
}
