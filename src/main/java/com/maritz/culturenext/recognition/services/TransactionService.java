package com.maritz.culturenext.recognition.services;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.recognition.dto.TransactionDetailsDTO;
import com.maritz.culturenext.recognition.dto.TransactionReversalDTO;

public interface TransactionService {

    /**
     * Search for transactions
     * 
     * @param programIdCommaSeparated - comma separated list of program ids to filter on
     * @param awardCodeCommaSeparated - comma separated list of award code numbers to return
     * @param statusCommaSeparated - comma separated list of statuses to filter on
     * @param recipientPaxIdCommaSeparated - comma separated list of pax_ids to filter by recipient
     * @param submitterPaxIdCommaSeparated - comma separated list of pax_Ids to fileter by submitter
     * @param programTypeCommaSeparated - comma separated list of program types to filter on
     *         valid program types are ECARD_ONLY,MANAGER_DISCRETIONARY,POINT_LOAD,PEER_TO_PEER,AWARD_CODE
     * @param startDate - begin date range filter on recognition submittal date
     * @param endDate - end date range filter on recognition submittal date
     * @param pageNumber
     * @param pageSize
     * @return
     * https://maritz.atlassian.net/wiki/display/M365/Transaction+Search
     */
    PaginatedResponseObject<TransactionDetailsDTO> getTransactionsSearch(
                PaginationRequestDetails requestDetails, 
                String programIdCommaSeparated,
                String awardCodeCommaSeparated,
                String statusCommaSeparated,
                String recipientPaxIdCommaSeparated,
                String submitterPaxIdCommaSeparated,
                String programTypeCommaSeparated,
                String startDate,
                String endDate,
                Integer pageNumber,
                Integer pageSize
            );
    
    /**
     * Reverses the transactions specified along with any raises tied to those transactions
     * One reversal comment will be applied to every transaction ID passed in.
     * 
     * This will update the STATUS_TYPE_CODE to REVERSED in these tables:
     * RECOGNITION, TRANSACTION_HEADER, and COMMENT
     * NOMINATION and NEWSFEED_ITEM - only if all recipients are reversed
     * ALERT - alerts of any kind tied to the transaction. Will check if all recipients are reversed for manager alerts.
     * APPROVAL_HISTORY - create a record here saving the pax that performed the reversal
     * 
     * @param TransactionReversalDTO  reversals - Transaction IDs to be reversed along with a reason for reversing
     * https://maritz.atlassian.net/wiki/display/M365/Update+Transaction
     */
    void reverseTransactions(TransactionReversalDTO reversals);
}
