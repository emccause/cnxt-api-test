package com.maritz.culturenext.recognition.services;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RecognitionStatsDTO;

public interface NominationService {
    
    /**
     * Validates DTO and then creates nomination and related entries
     * 
     * @param submitterPaxId id of pax submitting nomination
     * @param nominationDto request data to be created
     * @return details of completed nomination
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/10518636/Create+Nomination
     */
    NominationDetailsDTO createStandardNomination(Long submitterPaxId, NominationRequestDTO nominationDto, Boolean validate);

    /**
     * gets count of nominations submitted by pax
     * 
     * @param paxId id of pax to fetch info for
     * @return int count of recognitions given
     */
    int getNominationCountBySubmitter(Long paxId);

    /**
     * gets submittal date of latest approved nomination submitted by pax
     * 
     * @param paxId id of pax to fetch info for
     * @return UTC date time string representation of date of last recognition given
     */
    String getLastRecognitionGiven(Long paxId);

    /**
     * gets nomination info for the provided nomination based on the view of the pax id provided
     * 
     * @param nominationId string value of nomination id
     * @param paxId id of pax to return data for
     * @return NominationDetailsDTO populated with details on the nomination
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/11468936/Get+Nomination+by+Id+Example
     */
    NominationDetailsDTO getNominationById(String nominationId, Long paxId);

    /**
     * Gets nomination info for the provided nomination ids
     * 
     * @param nominationIdList - comma separated list of ids to get data for
     * @param paxId id of pax to return data for
     * @return List<NominationDetailsDTO> populated with details on the nominations
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47841284/GET+multiple+nominations+by+ID
     */
    List<NominationDetailsDTO> getNominationDetails(String nominationIdList, Long paxId);

    /**
     * Runs the DTO / recognized members through the business logic to determine if the nomination is valid or not.
     *
     * @param paxId id of the pax submitting the nomination
     * @param nominationDto information provided in the request
     * @param paxIds list of individual recipients
     * @param groupIds list of group recipients
     * @param validationType what type of recognition is the request (standard RECOGNTION or a RAISE)
     * @param isBatchProcess a flag stating if this is being called for Advanced Recognition batch
     * @return list of errors (if any)
     */
    List<ErrorMessage> validateNomination(Long paxId,
                                          NominationRequestDTO nominationDto,
                                          List<Long> paxIds,
                                          List<Long> groupIds,
                                          String validationType,
                                          Boolean isBatchProcess);

    /**
     * this method creates the discretionary and
     * transaction header entries to actually give points to the recognition (if necessary)
     * also creates a batch entry to simplify the process of creating
     * all the entries that tie points to the recognitions
     * 
     * @param nominationDto info provided in request
     * @param nomination nomination object built by the system
     */
    void insertTransactionData(NominationRequestDTO nominationDto, Nomination nomination);

    /**
     * Fetches stats related to recognitions given and received
     * 
     * @param paxId pax to get info for
     * @return RecognitionStatsDTO containing count of recognitions given / received and date of last recognition given
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/6914154/Recognition+Given+and+Received+Example
     */
    RecognitionStatsDTO getRecognitionStats(Long paxId);

    /**
     * Determines what status recognitions should be created in, based on configured approvals
     * Possible statuses are PENDING and AUTO_APPROVED
     * 
     * @param submitterPaxId
     * @param programId
     * @param awardAmount
     * @param paxIdsFromNomination
     * @param nominationId
     * @return a map of recognition id : status code
     */
    Map<Long, String> determineRecognitionStatusOnCreation(Long submitterPaxId, Long programId, 
            Integer awardAmount, List<Long> paxIds, Long nominationId);
    
}
