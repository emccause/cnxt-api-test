package com.maritz.culturenext.recognition.services;

import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.program.dto.TransactionDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;

import java.util.List;

public interface RecognitionService {

    /**
     * returns count of recognitions received by pax
     * 
     * @param paxId id of pax to get count for 
     * @return count of recognitions
     */
    Integer getRecognitionCountByRecipient(Long paxId);

    /**
     * Return a recognition details list corresponding to recognitionIds
     * 
     * @param recognitionIdList
     * @param returnApproverRecPoints : if it is true, pointsIssued property is populated for approvers.
     * @return recognition details for id list passed
     */
    List<RecognitionDetailsDTO> getRecognitionDetailsByIds (List<Long> recognitionIdList);
    
    /**
     * Return a recognitions details list
     * 
     * @param nominationId id of nomination to get list for
     * @param excludePrivate whether recipients with "private" share rec preference should be included
     * @param managerPaxId not actually used, but will throw an error if this doesn't match the logged in pax
     * @param delimitedStatusList comma delimited list of statuses to limit results to
     * @param returnApproverRecPoints : if it is true, pointsIssued property is populated for approvers.
     * @return list of recognition details.
     */
    List<RecognitionDetailsDTO> getRecognitionDetails(Long nominationId, Boolean excludePrivate, Long managerPaxId,
            String delimitedStatusList, Boolean returnApproverRecPoints);
     
    /**
     * Return a recognitions details list
     * 
     * @param nominationId id of nomination to get list for
     * @param excludePrivate whether recipients with "private" share rec preference should be included
     * @param managerPaxId not actually used, but will throw an error if this doesn't match the logged in pax
     * @param delimitedStatusList comma delimited list of statuses to limit results to
     * @param page for pagination - what page of results to return (default is 1)
     * @param size for pagination - size of pages (default is 50)
     * @param returnApproverRecPoints : if it is true, pointsIssued property is populated for approvers.
     * @return list of recognition details.
     * 
     * TODO See if the UI is using this version anywhere. If not, remove it.
     */
    @Deprecated
    List<RecognitionDetailsDTO> getRecognitionDetailsPagination(Long nominationId, Boolean excludePrivate,
            Long managerPaxId, String delimitedStatusList, Integer page, Integer size, Boolean returnApproverRecPoints);

    /**
     * Return a recognitions details list
     * 
     * @param requestDetails - details from request to be passed through to the PaginatedResponseObject
     * @param nominationId -  id of nomination to get list for
     * @param overrideVisibilty -  whether we should ignore visibility preferences
     * @param showPoints - whether we should override default point logic and always show - validated against MANAGER / ADMIN roles
     * @param paxId -  logged in pax
     * @param commaSeparatedStatusTypeCodeList -  comma delimited list of statuses to limit results to
     * @param notificationId - id of notification, if it's tied to this nomination then show approver view of points
     * @param pageNumber - what page of results to return
     * @param pageSize - number of results on page to return
     * @param returnApproverRecPoints - if it is true, pointsIssued property is populated for approvers.
     * 
     * @return list of RecognitionDetailsDTO sorted by recipient name (first > last)
     * https://maritz.atlassian.net/wiki/display/M365/Get+Recognitions+for+Nomination
     */
    
    PaginatedResponseObject<RecognitionDetailsDTO> getRecognitionDetails(
            PaginationRequestDetails requestDetails, Long nominationId, Boolean overrideVisibilty, Boolean showPoints,
            Long paxId, String commaSeparatedStatusTypeCodeList, Long notificationId, Integer pageNumber, Integer pageSize
        );
    
    /**
     * Return TransactionDTO with details by program type 
     * and transaction header id or nomination id
     * if programType = POINT_LOAD, then the ID is the TRANSACTION_HEADER_ID
     * and return TransactionDTO
     * 
     * if programType is MANAGER_DISCRETIONARY, PEER_TO_PEER, or AWARD_CODE, then the transactionId is the NOMINATION_ID.
     * and return List<RecognitionDetailsDTO>
     * 
     * @param programType - the program type to get list for
     * @param id - transaction id or nomination id depending of the program type
     * 
     * @return Return a TransactionDTO
     *
     * https://maritz.atlassian.net/wiki/display/M365/GET+Transaction+by+ID
     * https://maritz.atlassian.net/wiki/display/M365/Get+Recognitions+for+Nomination    
     */
    TransactionDTO getNominationOrTransactionByID (
            String programType, Long id);
}
