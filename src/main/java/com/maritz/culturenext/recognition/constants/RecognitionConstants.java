package com.maritz.culturenext.recognition.constants;

public class RecognitionConstants {
    
    public static final String APPROVAL_PENDING_PAX_ID = "approvalPendingPaxId";
    public static final String APPROVAL_HISTORY_PAX_ID = "approvalHistoryPaxId";
    public static final String PROGRAM = "program";
    public static final String APPROVAL_COMMENT = "approvalComment";
    public static final String NOMINATION_COMMENT = "nominationComment";
    public static final String RAISE_AMOUNT = "raiseAmount";
    public static final String RECOGNITIONS_URI_BASE ="recognitions";
}
