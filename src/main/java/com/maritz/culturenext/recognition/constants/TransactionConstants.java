package com.maritz.culturenext.recognition.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class TransactionConstants {
    
    // Errors
    public static final String ERROR_MISSING_TRANSACTION_IDS = "MISSING_TRANSACTION_IDS";
    public static final String ERROR_MISSING_TRANSACTION_IDS_MSG = "You must specify at least one transaction ID to reverse";
    public static final String ERROR_MISSING_REVERSAL_COMMENT = "MISSING_REVERSAL_COMMENT";
    public static final String ERROR_MISSING_REVERSAL_COMMENT_MSG = "You must specify a reason for reversing";
    
    public static final ErrorMessage MISSING_TRANSACTION_IDS_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_TRANSACTION_IDS)
            .setField(ProjectConstants.TRANSACTION_ID)
            .setMessage(ERROR_MISSING_TRANSACTION_IDS_MSG);
    
    public static final ErrorMessage MISSING_REVERSAL_COMMENT_MESSAGE = new ErrorMessage()
            .setCode(ERROR_MISSING_REVERSAL_COMMENT)
            .setField(ProjectConstants.REVERSAL_COMMENT)
            .setMessage(ERROR_MISSING_REVERSAL_COMMENT_MSG);
}
