package com.maritz.culturenext.recognition.constants;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.permission.constants.PermissionConstants;

public class NominationConstants {
    
    public static final String NOMINATION_URI_BASE = "nominations";
    
    // Pagination Related Constants
    public static final String RAISE_GET_ENDPOINT = "/nominations/%s/raises";

    public static final String[] STATUSES_THAT_CANT_GIVE_REC = { StatusTypeCode.INACTIVE.name(),
            StatusTypeCode.RESTRICTED.name() };

    //String Constants
    public static final String DISPLAY_NAME = "displayName";
    public static final String ID = "id";
    public static final String STATUS = "status";
    public static final String DESCRIPTION = "description";
    public static final String RECEIVERS_PAX = ProjectConstants.RECEIVERS + ProjectConstants.SLASH + ProjectConstants.PAX + ProjectConstants.SLASH;
    public static final String RECEIVERS_GROUP = ProjectConstants.RECEIVERS + ProjectConstants.SLASH + ProjectConstants.GROUP + ProjectConstants.SLASH;
    
    // Error message strings
    public static final String ERROR_PROGRAM_ID_MISSING = "MISSING_PROGRAM_ID";
    public static final String ERROR_PROGRAM_ID_MISSING_MSG = "Program Id is missing.";
    public static final String ERROR_INVALID_PROGRAM = "INVALID_PROGRAM";
    public static final String ERROR_INVALID_PROGRAM_MSG = "Program does not exist.";
    public static final String UNEXPECTED_ERROR_OCCURRED_MESSAGE = "An unexpected error occurred.";
    public static final String MISSING_RECEIVERS = "MISSING_RECEIVERS";
    public static final String MISSING_RECEIVERS_MSG = "At least one receiver must be provided to create a nomination";
    public static final String ERROR_CANNOT_RECOGNIZE_SELF = "CANNOT_RECOGNIZE_SELF";
    public static final String ERROR_CANNOT_RECOGNIZE_SELF_MSG = "Cannot recognize self";
    public static final String ERROR_NO_ELIGIBLE_RECEIVERS = "NO_ELIGIBLE_RECEIVERS";
    public static final String ERROR_NO_ELIGIBLE_RECEIVERS_MSG = 
            "At least one receiver must be eligible to receive recognition";
    public static final String ERROR_INELIGIBLE_SUBMITTER = "SUBMITTER_CANNOT_GIVE";
    public static final String ERROR_INELIGIBLE_SUBMITTER_MSG = "You cannot give from this program";
    public static final String ERROR_INELIGIBLE_GROUP_RECEIVER = "GROUP_CANNOT_RECEIVE";
    public static final String ERROR_INELIGIBLE_GROUP_RECEIVER_MSG =
        "This group cannot receive from the selected program";
    public static final String ERROR_INELIGIBLE_PAX_RECEIVER = "PAX_CANNOT_RECEIVE";
    public static final String ERROR_INELIGIBLE_PAX_RECEIVER_MSG = "This pax cannot receive from the selected program";
    public static final String ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION = "PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION";
    public static final String ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION_MSG = 
            "This is not a recognition program type";
    public static final String ERROR_SUBMITTER_CANNOT_RECOGNIZE = "SUBMITTER_CANNOT_RECOGNIZE";
    public static final String ERROR_SUBMITTER_CANNOT_RECOGNIZE_MSG = "Submitter is ineligible to send recognitions";
    public static final String ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT = "BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT";
    public static final String ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT_MSG = 
            "The budget Id needs to be provided for award amount for point-based nomination";
    public static final String ERROR_BUDGET_NOT_ACTIVE = "BUDGET_NOT_ACTIVE";
    public static final String ERROR_BUDGET_NOT_ACTIVE_MSG = "That budget is not currently active";
    public static final String ERROR_INVALID_BUDGET = "INVALID_BUDGET";
    public static final String ERROR_INVALID_BUDGET_MSG = "You cannot give from that budget for this program";
    public static final String ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET = "AWARD_AMOUNT_REQUIRED_FOR_BUDGET";
    public static final String ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET_MSG = 
            "The award amount needs to be provided for budget for point-based nomination";
    public static final String ERROR_AWARD_AMOUNT_HIGER_THAN_BUDGET = "AWARD_AMOUNT_HIGER_THAN_BUDGET";
    public static final String ERROR_AWARD_AMOUNT_HIGER_THAN_BUDGET_MSG = 
            "Award amount is higher than the budget total";
    public static final String ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT = "AWARD_AMOUNT_HIGHER_THAN_LIMIT";
    public static final String ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT_MSG = 
            "Award amount is higher than the available amount for individual";
    public static final String ERROR_NO_AWARD_TIERS_CONFIGURED = "NO_AWARD_TIERS_CONFIGURED";
    public static final String ERROR_NO_AWARD_TIERS_CONFIGURED_MSG = "No award tiers configured for this program";
    public static final String ERROR_AWARD_AMOUNT_NOT_IN_AWARD_TIERS_MSG = 
            "Award amount is not valid for any award tier configured for this program";
    public static final String ERROR_ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS = "ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS";
    public static final String ERROR_ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS_MSG = 
            "Original amount is not valid for any award tier configured for this program";
    public static final String ERROR_HEADLINE_MISSING = "HEADLINE_MISSING";
    public static final String ERROR_HEADLINE_MISSING_MSG = "Headline is missing";
    public static final String ERROR_HEADLINE_TOO_LONG = "HEADLINE_TOO_LONG";
    public static final String ERROR_HEADLINE_TOO_LONG_MSG = "Headline is too long.  Max length is ";
    public static final Integer MAX_HEADLINE_CHAR_LENGTH = 140;
    public static final String ERROR_COMMENT_TOO_LONG = "COMMENT_TOO_LONG";
    public static final String ERROR_COMMENT_TOO_LONG_MSG = "Comment is too long.  Max length is ";
    public static final Integer MAX_COMMENT_CHAR_LENGTH = 2000;
    public static final String ERROR_ECARD_INVALID = "ECARD_INVALID";
    public static final String ERROR_ECARD_INVALID_MSG = "Ecard is not valid for the selected program";
    public static final String ERROR_ECARD_NOT_SET_UP = "ECARD_NOT_SET_UP";
    public static final String ERROR_ECARD_NOT_SET_UP_MSG = "No ecards are configured for this program";
    public static final String ERROR_CRITERIA_REQUIRED = "CRITERIA_REQUIRED";
    public static final String ERROR_CRITERIA_REQUIRED_MSG = 
            "At least one recognition criteria is required to be selected";
    public static final String ERROR_CRITERIA_INVALID = "CRITERIA_INVALID";
    public static final String ERROR_CRITERIA_INVALID_MSG = 
            "One or more recognition criterias selected is not valid for the selected program";
    public static final String ERROR_CRITERIA_NOT_SET_UP = "CRITERIA_NOT_SET_UP";
    public static final String ERROR_CRITERIA_NOT_SET_UP_MSG = 
            "No recognition criterias are configured for this program";
    public static final String ERROR_INVALID_NOMINATION = "INVALID_NOMINATION_ID";
    public static final String ERROR_INVALID_NOMINATION_MSG = "Nomination ID must be a number.";
    public static final String ERROR_NOMINATION_NOT_EXIST = "NOMINATION_NOT_EXIST";
    public static final String ERROR_NOMINATION_NOT_EXIST_MSG = "No nomination exist for id provided";
    public static final String ERROR_APPROVAL_PAX_MISMATCH = "ERROR_APPROVAL_PAX_MISMATCH";
    public static final String ERROR_APPROVAL_PAX_MISMATCH_MSG = "The approval paxIds do not match";
    public static final String ERROR_ALREADY_APPROVED_OR_REJECTED = "ERROR_ALREADY_APPROVED_OR_REJECTED";
    public static final String ERROR_ALREADY_APPROVED_OR_REJECTED_MSG = "Raise has already been approved or denied";
    public static final String ERROR_INVALID_RAISE_ID = "ERROR_INVALID_RAISE_ID";
    public static final String ERROR_INVALID_RAISE_ID_MSG = "The passed in raise id is null or not valid";
    public static final String ERROR_AWARD_TIER_MISSING = "AWARD_TIER_ID_MISSING";
    public static final String ERROR_AWARD_TIER_MISSING_MSG = 
            "The awardTierId is missing (required if awardAmount exists)";
    public static final String ERROR_AWARD_AMOUNT_MISSING = "AWARD_AMOUNT_MISSING";
    public static final String ERROR_AWARD_AMOUNT_MISSING_MSG = 
            "The awardAmount is missing (required if awardTierId exists)";
    public static final String ERROR_INVALID_MANAGER_PAX_ID = "INVALID_MANAGER_PAX_ID";
    public static final String ERROR_INVALID_MANAGER_PAX_ID_MSG = "Please enter a valid manager pax id";
    public static final String ERROR_RECOGNITION_NOT_APPROVED = "RECOGNITION_NOT_APPROVED";
    public static final String ERROR_RECOGNITION_NOT_APPROVED_MSG = "Recognition has not been approved";
    public static final String ERROR_RECEIVER_DUPLICATED = "ERROR_RECEIVER_DUPLICATED";
    public static final String ERROR_RECEIVER_DUPLICATED_MSG = "Receivers are duplicated";
    public static final String ERROR_GROUPS_NOT_ALLOWED = "ERROR_GROUPS_NOT_ALLOWED";
    public static final String ERROR_GROUPS_NOT_ALLOWED_MSG = "Groups are not allowed on advanced recognition.";
    public static final String ERROR_RECEIVER_REQUIRED = "ERROR_RECEIVER_REQUIRED";
    public static final String ERROR_RECEIVER_REQUIRED_MSG = "PaxId or GroupId is required for each receiver";
    public static final String ERROR_ADVANCED_RECOGNITION_NOT_ALLOWED = "ERROR_ADVANCED_RECOGNITION_NOT_ALLOWED";
    public static final String ERROR_ADVANCED_RECOGNITION_NOT_ALLOWED_MSG = 
            "Submitter doesn't have advanced recognition permission";
    private static String ERROR_INVALID_STATUS = "INVALID_STATUS";
    private static String ERROR_INVALID_STATUS_MSG = "Invalid status passed into service";
    private static String ERROR_CRITERIA_EXISTS = "ERROR_CRITERIA_EXISTS";
    private static String ERROR_CRITERIA_EXISTS_MSG = "Display name already exists in database";
    private static String ERROR_CRITERIA_VALUE_IN_USE = "CRITERIA_VALUE_IN_USE";
    private static String ERROR_CRITERIA_VALUE_IN_USE_MSG = "The criteria value is being used";
    private static String ERROR_CRITERIA_NAME_NULL = "CRITERIA_NAME_NULL";
    private static String ERROR_CRITERIA_NAME_NULL_MSG = "Criteria value not provided";
    private static String ERROR_CRITERIA_NAME_TOO_LONG = "CRITERIA_NAME_TOO_LONG";
    private static String ERROR_CRITERIA_NAME_TOO_LONG_MSG = "Criteria value name, max length is 100";
    private static String ERROR_CRITERIA_DESCRIPTION_TOO_LONG = "CRITERIA_DESCRIPTION_TOO_LONG";
    private static String ERROR_CRITERIA_DESCRIPTION_TOO_LONG_MSG = "Criteria value description, max length is 255";
    private static String ERROR_ID_MISMATCHED = "ID_MISMATCHED";
    private static String ERROR_ID_MISMATCHED_MSG = "ID in URI does not match request";
    private static String ERROR_ID_NULL = "ID_NULL";
    private static String ERROR_ID_NULL_MSG = "id value not provided";
    private static String ERROR_NULL_STATUS = "NULL_STATUS";
    private static String ERROR_NULL_STATUS_MSG = "Status not provided";

    public static final String GROUP_ID_COLUMN_NAME = "GROUP_ID";
    public static final String PAX_ID_COLUMN_NAME = "PAX_ID";
    public static final String MANAGER_PAX_ID_COLUMN_NAME = "MANAGER_PAX_ID";
    public static final String RECOGNITION_NOMINATION_TYPE = "RECG";
    public static final String PROGRAM_POINT_LOAD_TYPE = "PNTU";
    public static final String NEWSFEED_VISIBILITY_PROGRAM_MISC_NAME = "NEWSFEED_VISIBILITY";
    public static final String NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME = "NOTIFICATION_RECIPIENT";
    public static final String NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME = "NOTIFICATION_RECIPIENT_MGR";
    public static final String RAISE = "RAISE";

    public static final String RECOGNITION_GIVEN_TYPE_CODE = "RECG";

    public static final String MISSING_SUBMITTERS = "MISSING_PAX_ID";
    public static final String MISSING_SUBMITTERS_MSG = "At least one submitter must be provided to create a raise";
    public static final String ERROR_CANNOT_RAISE_AGAIN = "ERROR_CANNOT_RAISE_AGAIN";
    public static final String ERROR_CANNOT_RAISE_AGAIN_MSG = "Cannot raise a nomination again";
    public static final String ERROR_CANNOT_RAISE_OWN_RECOGNITION = "ERROR_CANNOT_RAISE_OWN_RECOGNITION";
    public static final String ERROR_CANNOT_RAISE_OWN_RECOGNITION_MSG = "Cannot raise your own recognition";
    public static final String ERROR_INVALID_RECOGNITION = "ERROR_INVALID_RECOGNITION";
    public static final String ERROR_INVALID_RECOGNITION_MSG = "Recognition cannot be raised due to status";

    public static final String RAISE_RETURN_RECEIVER_PAX_ID = "receiverPaxId";
    public static final String ERROR_MISSING_RAISES = "ERROR_MISSING_RAISES";
    public static final String ERROR_MISSING_RAISES_MSG = "Must pass a list of raises to update";

    public static final String ERROR_PAX_ID_INVALID = "PAX_ID_INVALID";
    public static final String ERROR_PAX_ID_INVALID_MSG = "Pax id does not exist.";

    // Award code constants
    public static final String AWARD_CODE_RECOGNITION_TYPE = "AWARD_CODE";
    public static final String AWARD_CODE_RECOGNITION_PRINT_VALIDATION_TYPE = "AWARD_CODE_PRINT";
    public static final String AWARD_CODE_FILE_NAME = "award_codes.csv";
    public static final Integer AWARD_CODE_MAX_CERTIFICATE_QUANTITY = 1000;
    public static final String AWARD_CODE = "AWARD_CODE";

    public static final String ERROR_MULTIPLE_RECEIVERS_AWARD_CODE = "ERROR_MULTIPLE_RECEIVERS";
    public static final String ERROR_MULTIPLE_RECEIVERS_AWARD_CODE_MESSAGE = 
            "Can only have one receiver entry for award codes.";
    public static final String QUANTITY_FIELD = "awardCode.quantity";
    public static final String ERROR_INVALID_QUANTITY = "ERROR_INVALID_QUANTITY";
    public static final String ERROR_INVALID_QUANTITY_MESSAGE = 
            "Certificate quantity must be a valid positive (non zero) number.";
    public static final String ERROR_TOO_MANY_CODES = "ERROR_TOO_MANY_CODES";
    public static final String ERROR_TOO_MANY_CODES_MESSAGE = 
            "Maximum number of certificates that can be generated is %d.";
    public static final String ERROR_NOT_PRINTABLE = "ERROR_NOT_PRINTABLE";
    public static final String ERROR_NOT_PRINTABLE_MESSAGE = "Can't print certificates for that award code program.";
    public static final String CERT_FIELD = "awardCode.certId";
    public static final String ERROR_MISSING_CERT = "ERROR_MISSING_CERT";
    public static final String ERROR_MISSING_CERT_MESSAGE = "Cert id must be provided.";
    public static final String ERROR_INVALID_CERT = "ERROR_INVALID_CERT";
    public static final String ERROR_INVALID_CERT_MESSAGE = "Certificate id isn't an existing certificate.";
    public static final String ERROR_CERT_NOT_CONFIGURED = "ERROR_CERT_NOT_CONFIGURED";
    public static final String ERROR_CERT_NOT_CONFIGURED_MESSAGE = 
            "Certificate isn't configured for the chosen program.";
    public static final String GIVE_ANONYMOUS_FIELD = "awardCode.giveAnonymous";
    public static final String ERROR_MISSING_GIVE_ANONYMOUS = "ERROR_MISSING_GIVE_ANONYMOUS";
    public static final String ERROR_MISSING_GIVE_ANONYMOUS_MESSAGE = "giveAnonymous must be provided.";
    public static final String AWARD_CODE_FIELD = "awardCode";
    public static final String ERROR_MISSING_AWARD_CODE_ATTRIBUTE = "ERROR_MISSING_AWARD_CODE_ATTRIBUTE";
    public static final String ERROR_MISSING_AWARD_CODE_ATTRIBUTE_MESSAGE = "awardCode attribute must be provided.";

    public static final String ERROR_AWARD_CODE_INVALID = "ERROR_AWARD_CODE_INVALID";
    public static final String ERROR_AWARD_CODE_ALREADY_BEEN_CLAIMED = "ERROR_AWARD_CODE_ALREADY_BEEN_CLAIMED";
    public static final String ERROR_AWARD_CODE_INELIGIBLE_TO_CLAIM = "ERROR_AWARD_CODE_INELIGIBLE_TO_CLAIM";
    public static final String ERROR_AWARD_CODE_EXPIRED = "ERROR_AWARD_CODE_EXPIRED";
    public static final String ERROR_AWARD_CODE_MATCH = "ERROR_AWARD_CODE_MATCH";
    public static final String ERROR_AWARD_CODE_MISSING = "ERROR_AWARD_CODE_MISSING";
    public static final String ERROR_AWARD_CODE_STATUS_INVALID = "ERROR_AWARD_CODE_STATUS_INVALID";
    public static final String ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM = "ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM";
    public static final String ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE = "ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE";
    

    public static final String ERROR_AWARD_CODE_INVALID_MSG = "This code is invalid.";
    public static final String ERROR_AWARD_CODE_ALREADY_BEEN_CLAIMED_MSG = "This code has already been claimed.";
    public static final String ERROR_AWARD_CODE_INELIGIBLE_TO_CLAIM_MSG = 
            "Sorry! It seems you're ineligible to claim this code.";
    public static final String ERROR_AWARD_CODE_EXPIRED_MSG = "The code is expired.";
    public static final String ERROR_AWARD_CODE_MATCH_MSG = "Award code in path doesn´t match award code in json";
    public static final String ERROR_AWARD_CODE_MISSING_MSG = "Missing award code(s) in path and/or json).";
    public static final String ERROR_AWARD_CODE_STATUS_INVALID_MSG = "Invalid status for award code.";
    public static final String ERROR_OTHER_ERROR_STATUS_MSG = "Incorrect status: %s";
    public static final String ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG = "Not able to redeem.";
    public static final String ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG = "Not able to revoke.";

    public static final String ERROR_AWARD_CODE_NOMINATION_NOTEXIST_MSG = "Nomination does not exist for id provided";
    public static final String ERROR_AWARD_CODE_PROGRAM_NOTEXIST_MSG = "Program does not exist for id provided";

    public static final String ERROR_AWARD_CODE_TRANSACTION_HEADER_NOTEXIST_MSG = 
            "Transaction Header does not exist for id provided";
    public static final String ERROR_AWARD_CODE_DISCRETIONARY_NOTEXIST_MSG = 
            "Discretionary does not exist for recognition id provided";
    public static final String ERROR_AWARD_CODE_NOMINATION_DETAILS_NOTEXIST_MSG = 
            "Nomination details does not exist for id provided";
    public static final String ERROR_AWARD_CODE_RECOGNITION_NOTEXIST_MSG = "Recognition does not exist for id provided";
    public static final String ERROR_AWARD_CODE_TO_REVOKE_MISSING_MSG = "Missing award code list to revoke.";
    public static final String ERROR_AWARD_CODE_TO_REVOKE_STATUS_NOT_ENOUGH_PRIVILEGES = "Not enough privileges to revoke award code. Must be admin or submitter.";
    
    public static final String ERROR_MISSING_NOMINATION_ID = "MISSING_NOMINATION_ID";
    public static final String ERROR_MISSING_NOMINATION_ID_MSG = "At least one nomination ID must be supplied.";
    public static final String ERROR_AWARD_CODE_NOT_PENDING_MSG = "Award Code not in Pending status:";
    public static final String ERROR_AWARD_CODE_CAN_NOT_BE_CLAIMED_BY_SUBMITTER = "ERROR_AWARD_CODE_CAN_NOT_BE_CLAIMED_BY_SUBMITTER";
    public static final String ERROR_AWARD_CODE_CAN_NOT_BE_CLAIMED_BY_SUBMITTER_MSG = "Award code cannot be claimed by submitter.";
    
    public static final String ERROR_MISSING_PAX_GROUP_ID_MSG = "Pax Group Id does not exist for Pax Id provided";
    
    public static final String ERROR_PAYOUT_TYPE_INVALID = "PAYOUT_TYPE_INVALID";
    public static final String ERROR_PAYOUT_TYPE_INVALID_MSG = "Invalid Payout type";
    
    //Error Messages
    public static final ErrorMessage PROGRAM_ID_MISSING = new ErrorMessage()
            .setCode(NominationConstants.ERROR_PROGRAM_ID_MISSING)
            .setField(ProjectConstants.PROGRAM_ID)
            .setMessage(NominationConstants.ERROR_PROGRAM_ID_MISSING_MSG);
    
    public static final ErrorMessage INVALID_PROGRAM = new ErrorMessage()
            .setCode(NominationConstants.ERROR_INVALID_PROGRAM)
            .setField(ProjectConstants.PROGRAM_ID)
            .setMessage(NominationConstants.ERROR_INVALID_PROGRAM_MSG);
    
    public static final ErrorMessage PAX_ID_INVALID = new ErrorMessage()
        .setCode(NominationConstants.ERROR_PAX_ID_INVALID)
        .setField(ProjectConstants.INVOLVED_PAX_ID)
        .setMessage(NominationConstants.ERROR_PAX_ID_INVALID_MSG);
    
    public static final ErrorMessage APPROVAL_PAX_MISMATCH = new ErrorMessage()
        .setField(NominationConstants.RAISE)
        .setCode(NominationConstants.ERROR_APPROVAL_PAX_MISMATCH)
        .setMessage(NominationConstants.ERROR_APPROVAL_PAX_MISMATCH_MSG);
    
    public static final ErrorMessage ALREADY_APPROVED_OR_REJECTED_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.STATUS)
        .setCode(NominationConstants.ERROR_ALREADY_APPROVED_OR_REJECTED)
        .setMessage(NominationConstants.ERROR_ALREADY_APPROVED_OR_REJECTED_MSG);
    
    public static final ErrorMessage CANNOT_RAISE_AGAIN_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CANNOT_RAISE_AGAIN)
        .setMessage(NominationConstants.ERROR_CANNOT_RAISE_AGAIN_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage CANNOT_RAISE_OWN_RECOGNITION_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CANNOT_RAISE_OWN_RECOGNITION)
        .setMessage(NominationConstants.ERROR_CANNOT_RAISE_OWN_RECOGNITION_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage INVALID_RECOGNITION_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_INVALID_RECOGNITION)
        .setMessage(NominationConstants.ERROR_INVALID_RECOGNITION_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage CRITERIA_NAME_NULL_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CRITERIA_NAME_NULL)
        .setField(DISPLAY_NAME)
        .setMessage(ERROR_CRITERIA_NAME_NULL_MSG);
    
    public static final ErrorMessage CRITERIA_NAME_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CRITERIA_NAME_TOO_LONG)
        .setField(DISPLAY_NAME)
        .setMessage(ERROR_CRITERIA_NAME_TOO_LONG_MSG);
    
    public static final ErrorMessage ERROR_ID_NULL_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ID_NULL)
        .setField(ID)
        .setMessage(ERROR_ID_NULL_MSG);
    
    public static final ErrorMessage ERROR_ID_MISMATCHED_MESSAGE = new ErrorMessage()
        .setCode(ERROR_ID_MISMATCHED)
        .setField(ID)
        .setMessage(ERROR_ID_MISMATCHED_MSG);
    
    public static final ErrorMessage CRITERIA_EXISTS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CRITERIA_EXISTS)
        .setField(DISPLAY_NAME)
        .setMessage(ERROR_CRITERIA_EXISTS_MSG);
    
    public static final ErrorMessage NULL_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_NULL_STATUS)
        .setField(STATUS)
        .setMessage(ERROR_NULL_STATUS_MSG);
    
    public static final ErrorMessage CRITERIA_VALUE_IN_USE_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CRITERIA_VALUE_IN_USE)
        .setField(ID)
        .setMessage(ERROR_CRITERIA_VALUE_IN_USE_MSG);
    
    public static final ErrorMessage INVALID_STATUS_MESSAGE = new ErrorMessage()
        .setCode(ERROR_INVALID_STATUS)
        .setField(STATUS)
        .setMessage(ERROR_INVALID_STATUS_MSG);
    
    public static final ErrorMessage CRITERIA_DESCRIPTION_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(ERROR_CRITERIA_DESCRIPTION_TOO_LONG)
        .setField(DESCRIPTION)
        .setMessage(ERROR_CRITERIA_DESCRIPTION_TOO_LONG_MSG);
    
    public static final ErrorMessage PROXY_NOT_ALLOWED_ERROR_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PROXY)
        .setCode(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_CODE)
        .setMessage(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_RECIEVERS_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.MISSING_RECEIVERS)
        .setMessage(NominationConstants.MISSING_RECEIVERS_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION)
        .setMessage(NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION_MSG)
        .setField(ProjectConstants.PROGRAM_ID);

    public static final ErrorMessage INELIGIBLE_SUBMITTER = new ErrorMessage()
        .setCode(ERROR_INELIGIBLE_SUBMITTER)
        .setMessage(ERROR_INELIGIBLE_SUBMITTER_MSG)
        .setField(ProjectConstants.RECEIVERS);

    public static final ErrorMessage ERROR_SUBMITTER_CANNOT_RECOGNIZE_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_SUBMITTER_CANNOT_RECOGNIZE)
        .setMessage(NominationConstants.ERROR_SUBMITTER_CANNOT_RECOGNIZE_MSG)
        .setField(ProjectConstants.SUBMITTER);
    
    public static final ErrorMessage ERROR_CANNOT_RECOGNIZE_SELF_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CANNOT_RECOGNIZE_SELF)
        .setMessage(NominationConstants.ERROR_CANNOT_RECOGNIZE_SELF_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage ERROR_RECEIVER_DUPLICATED_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_RECEIVER_DUPLICATED)
        .setMessage(NominationConstants.ERROR_RECEIVER_DUPLICATED_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage MULTIPLE_RECEIVERS_AWARD_CODE_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_MULTIPLE_RECEIVERS_AWARD_CODE)
        .setMessage(NominationConstants.ERROR_MULTIPLE_RECEIVERS_AWARD_CODE_MESSAGE)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage INVALID_QUANTITY_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_INVALID_QUANTITY)
        .setMessage(NominationConstants.ERROR_INVALID_QUANTITY_MESSAGE)
        .setField(NominationConstants.QUANTITY_FIELD);
    
    public static final ErrorMessage ERROR_TOO_MANY_CODES_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_TOO_MANY_CODES)
        .setMessage(String.format(NominationConstants.ERROR_TOO_MANY_CODES_MESSAGE, 
                NominationConstants.AWARD_CODE_MAX_CERTIFICATE_QUANTITY))
        .setField(NominationConstants.QUANTITY_FIELD);
    
    public static final ErrorMessage NOT_PRINTABLE_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_NOT_PRINTABLE)
        .setMessage(NominationConstants.ERROR_NOT_PRINTABLE_MESSAGE)
        .setField(ProjectConstants.PROGRAM_ID);
    
    public static final ErrorMessage MISSING_CERT_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_MISSING_CERT)
        .setMessage(NominationConstants.ERROR_MISSING_CERT_MESSAGE)
        .setField(NominationConstants.CERT_FIELD);
    
    public static final ErrorMessage INVALID_CERT_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_INVALID_CERT)
        .setMessage(NominationConstants.ERROR_INVALID_CERT_MESSAGE)
        .setField(NominationConstants.CERT_FIELD);
    
    public static final ErrorMessage CERT_NOT_CONFIGURED_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CERT_NOT_CONFIGURED)
        .setMessage(NominationConstants.ERROR_CERT_NOT_CONFIGURED_MESSAGE)
        .setField(NominationConstants.CERT_FIELD);
    
    public static final ErrorMessage MISSING_GIVE_ANONYMOUS_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_MISSING_GIVE_ANONYMOUS)
        .setMessage(NominationConstants.ERROR_MISSING_GIVE_ANONYMOUS_MESSAGE)
        .setField(NominationConstants.GIVE_ANONYMOUS_FIELD);
    
    public static final ErrorMessage MISSING_AWARD_CODE_ATTRIBUTE_ERROR_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_MISSING_AWARD_CODE_ATTRIBUTE)
        .setMessage(NominationConstants.ERROR_MISSING_AWARD_CODE_ATTRIBUTE_MESSAGE)
        .setField(NominationConstants.AWARD_CODE_FIELD);
    
    public static final ErrorMessage ADVANCED_RECOGNITION_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_ADVANCED_RECOGNITION_NOT_ALLOWED)
        .setMessage(NominationConstants.ERROR_ADVANCED_RECOGNITION_NOT_ALLOWED_MSG)
        .setField(ProjectConstants.NOMINATION);
    
    public static final ErrorMessage RECEIVER_REQUIRED_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_RECEIVER_REQUIRED)
        .setMessage(NominationConstants.ERROR_RECEIVER_REQUIRED_MSG)
        .setField(ProjectConstants.RECEIVERS);
    
    public static final ErrorMessage AWARD_TIER_MISSING_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_AWARD_TIER_MISSING)
        .setMessage(NominationConstants.ERROR_AWARD_TIER_MISSING_MSG)
        .setField(ProjectConstants.AWARD_TIER);
    
    public static final ErrorMessage ERROR_AWARD_AMOUNT_MISSING_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_AWARD_AMOUNT_MISSING)
        .setMessage(NominationConstants.ERROR_AWARD_AMOUNT_MISSING_MSG)
        .setField(ProjectConstants.AWARD_AMOUNT);
    
    public static final ErrorMessage ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT)
        .setMessage(NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT_MSG)
        .setField(ProjectConstants.BUDGET_ID);
    
    public static final ErrorMessage AWARD_AMOUNT_REQUIRED_FOR_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET)
        .setMessage(NominationConstants.ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET_MSG)
        .setField(ProjectConstants.AWARD_AMOUNT);
    
    public static final ErrorMessage GROUPS_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_GROUPS_NOT_ALLOWED)
        .setMessage(NominationConstants.ERROR_GROUPS_NOT_ALLOWED_MSG)
        .setField(ProjectConstants.GROUP_ID);
    
    public static final ErrorMessage INVALID_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_INVALID_BUDGET)
        .setMessage(NominationConstants.ERROR_INVALID_BUDGET_MSG)
        .setField(ProjectConstants.BUDGET_ID);
    
    public static final ErrorMessage HEADLINE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_HEADLINE_MISSING)
        .setMessage(NominationConstants.ERROR_HEADLINE_MISSING_MSG)
        .setField(ProjectConstants.HEADLINE);
    
    public static final ErrorMessage HEADLINE_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_HEADLINE_TOO_LONG)
        .setMessage(NominationConstants.ERROR_HEADLINE_TOO_LONG_MSG +
                NominationConstants.MAX_HEADLINE_CHAR_LENGTH)
        .setField(ProjectConstants.HEADLINE);
    
    public static final ErrorMessage COMMENT_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_COMMENT_TOO_LONG)
        .setMessage(NominationConstants.ERROR_COMMENT_TOO_LONG_MSG + 
                NominationConstants.MAX_COMMENT_CHAR_LENGTH)
        .setField(ProjectConstants.COMMENT);
    
    public static final ErrorMessage ECARD_INVALID_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_ECARD_INVALID)
        .setMessage(NominationConstants.ERROR_ECARD_INVALID_MSG)
        .setField(ProjectConstants.ECARD_ID);
    
    public static final ErrorMessage ECARD_NOT_SET_UP_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_ECARD_NOT_SET_UP)
        .setMessage(NominationConstants.ERROR_ECARD_NOT_SET_UP_MSG)
        .setField(ProjectConstants.ECARD_ID);
    
    public static final ErrorMessage CRITERIA_REQUIRED_MESSSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CRITERIA_REQUIRED)
        .setMessage(NominationConstants.ERROR_CRITERIA_REQUIRED_MSG)
        .setField(ProjectConstants.RECOGNITION_CRITERIA_IDS);
    
    public static final ErrorMessage CRITERIA_INVALID_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CRITERIA_INVALID)
        .setMessage(NominationConstants.ERROR_CRITERIA_INVALID_MSG)
        .setField(ProjectConstants.RECOGNITION_CRITERIA_IDS);
    
    public static final ErrorMessage CRITERIA_NOT_SET_UP_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_CRITERIA_NOT_SET_UP)
        .setMessage(NominationConstants.ERROR_CRITERIA_NOT_SET_UP_MSG)
        .setField(ProjectConstants.RECOGNITION_CRITERIA_IDS);
    
    public static final ErrorMessage BUDGET_NOT_ACTIVE_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_BUDGET_NOT_ACTIVE)
        .setMessage(NominationConstants.ERROR_BUDGET_NOT_ACTIVE_MSG)
        .setField(ProjectConstants.BUDGET_ID);
    
    public static final ErrorMessage AWARD_AMOUNT_HIGER_THAN_BUDGET_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_AWARD_AMOUNT_HIGER_THAN_BUDGET)
        .setMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGER_THAN_BUDGET_MSG)
        .setField(ProjectConstants.AWARD_AMOUNT);
    
    public static final ErrorMessage AWARD_AMOUNT_HIGHER_THAN_LIMIT_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT)
        .setMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT_MSG)
        .setField(ProjectConstants.AWARD_AMOUNT);
    
    public static final ErrorMessage NO_AWARD_TIERS_CONFIGURED_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_NO_AWARD_TIERS_CONFIGURED)
        .setMessage(NominationConstants.ERROR_NO_AWARD_TIERS_CONFIGURED_MSG)
        .setField(ProjectConstants.AWARD_AMOUNT);
    
    public static final ErrorMessage ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS)
        .setMessage(NominationConstants.ERROR_ORIGINAL_AMOUNT_NOT_IN_AWARD_TIERS_MSG)
        .setField(ProjectConstants.ORIGINAL_AMOUNT);

    public static final ErrorMessage MISSING_NOMINATION_ID_MESSAGE = new ErrorMessage()
        .setCode(NominationConstants.ERROR_MISSING_NOMINATION_ID)
        .setMessage(NominationConstants.ERROR_MISSING_NOMINATION_ID_MSG)
        .setField(ProjectConstants.NOMINATION_IDS);

    public static final ErrorMessage PAYOUT_TYPE_INVALID_MESSAGE = new ErrorMessage()
        .setCode(ERROR_PAYOUT_TYPE_INVALID)
        .setMessage(ERROR_PAYOUT_TYPE_INVALID_MSG)
        .setField(ProjectConstants.PAYOUT_TYPE);

    public static final ErrorMessage INELIGIBLE_PAX_RECEIVER = new ErrorMessage()
        .setCode(ERROR_INELIGIBLE_PAX_RECEIVER)
        .setMessage(ERROR_INELIGIBLE_PAX_RECEIVER_MSG)
        .setField(ProjectConstants.RECEIVERS);

    public static final ErrorMessage INELIGIBLE_GROUP_RECEIVER = new ErrorMessage()
        .setCode(ERROR_INELIGIBLE_GROUP_RECEIVER)
        .setMessage(ERROR_INELIGIBLE_GROUP_RECEIVER_MSG)
        .setField(ProjectConstants.RECEIVERS);

    public static final ErrorMessage NO_ELIGIBLE_RECEIVERS = new ErrorMessage()
        .setCode(ERROR_NO_ELIGIBLE_RECEIVERS)
        .setMessage(ERROR_NO_ELIGIBLE_RECEIVERS_MSG)
        .setField(ProjectConstants.RECEIVERS);
}
