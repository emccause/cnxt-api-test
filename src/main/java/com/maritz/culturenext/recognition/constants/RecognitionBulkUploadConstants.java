package com.maritz.culturenext.recognition.constants;

public class RecognitionBulkUploadConstants {

    // The length of the buffer that will be used when reading a file
    public static final int APPEND_CHAR_BUFFER_LENGTH = 655360;
    public static final String UTF_8 = "UTF-8";
    public static final String XLS = ".xls";
    public static final String XLSX = ".xlsx";
    public static final char DELIMITER = ',';
    public static final String FIELD_AWARD_AMOUNT = "Award Amount";
    public static final String FIELD_RECIPIENT_FIRST_NAME = "Recipient First Name";
    public static final String FIELD_RECIPIENT_LAST_NAME = "Recipient Last Name";
    public static final String FIELD_RECIPIENT_ID = "Recipient ID (Participant ID)";
    public static final String FIELD_PUBLIC_HEADLINE = "Public Headline";
    public static final String FIELD_PRIVATE_MESSAGE = "Private Message";
    public static final String FIELD_PROGRAM_NAME = "Program Name";
    public static final String FIELD_PROGRAM_ID = "Program ID";
    public static final String FIELD_BUDGET_NAME = "Budget Name";
    public static final String FIELD_BUDGET_ID = "Budget ID";
    public static final String FIELD_AWARD_TYPE = "Award Type (Payout Type)";
    public static final String FIELD_SUBMITTER_FIRST_NAME = "Submitter First Name";
    public static final String FIELD_SUBMITTER_LAST_NAME = "Submitter Last Name";
    public static final String FIELD_SUBMITTER_ID = "Submitter ID";
    public static final String FIELD_ECARD_ID = "Ecard ID";
    public static final String FIELD_VALUE = "Value";
    public static final String FIELD_VALUE_ID = "Value ID";
    public static final String FIELD_MAKE_RECOGNITION_PRIVATE = "Make recognition private";
    public static final String FIELD_ERROR_MESSAGE = "Errors:";

    public static final String PUBLIC_HEADLINE = "publicHeadline";
    public static final String AWARD_TYPE = "awardType";
    public static final String VALUE ="value";
    public static final String VALUE_ID ="valueId";
    public static final String MAKE_RECOGNITION_PRIVATE = "makeRecognitionPrivate";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String DUPLICATE_FILE_CONTENTS = "duplicateFileContents";
    public static final String DUPLICATE_FILE_NAME = "duplicateFileName";
    public static final String NOMINATION_COUNT = "nominationCount";
    public static final String TOTAL_AWARD_AMOUNT = "totalAwardAmount";
    public static final String TOTAL_ROWS = "totalRows";

    public static final int START_POSITION = 0;
    public static final int PUBLIC_HEADLINE_MAX_LENGHT = 140;
    public static final int PRIVATE_MESSAGE_MAX_LENGHT = 2000;
}
