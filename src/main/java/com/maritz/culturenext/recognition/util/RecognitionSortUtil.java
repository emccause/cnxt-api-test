package com.maritz.culturenext.recognition.util;

import java.util.Comparator;

import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;

public class RecognitionSortUtil {
    /**
     * Provides a comparator for sorting a collection of EmployeeDTOs
     * by First name and Last name
     */
    public static Comparator<RecognitionDetailsDTO> firstAndLastNameComparator
            = new Comparator<RecognitionDetailsDTO>() {
        @Override
        public int compare(RecognitionDetailsDTO recognitionDetailsDTO1 , 
                RecognitionDetailsDTO recognitionDetailsDTO2) {
            int firstNameOrder = nullSafeStringCompareTo(recognitionDetailsDTO1.getToPax().getFirstName(), 
                    recognitionDetailsDTO2.getToPax().getFirstName()),
                    lastNameOrder = nullSafeStringCompareTo(recognitionDetailsDTO1.getToPax().getLastName(),
                            recognitionDetailsDTO2.getToPax().getLastName());

            return firstNameOrder != 0 ? firstNameOrder : lastNameOrder;
        }
    };

    /**
     * Null safe compareTo for string values
     * @param string1 - First string value to compare
     * @param string2 - Second string value to compare
     * @return int
     */
    private static int nullSafeStringCompareTo(String string1, String string2) {
        if (string1 == null && string2 == null) {
            return 0;
        } else if (string2 == null) {
            return 1;
        } else if (string1 == null) {
            return -1;
        } else {
            return string1.compareToIgnoreCase(string2);
        }
    }
}
