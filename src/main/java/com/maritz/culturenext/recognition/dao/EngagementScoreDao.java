package com.maritz.culturenext.recognition.dao;

import java.util.List;
import java.util.Map;

public interface EngagementScoreDao {
    
    List<Map<String, Object>> getEngagementScoreDetails(List<Long> paxIdList);

}
