package com.maritz.culturenext.recognition.dao;

import java.util.List;
import java.util.Map;

public interface RaiseDao {
    List<Map<String, Object>> getListOfRaisesOnNomination(Long activityId, Long fromPaxId, 
            List<String> parentStatusList, List<String> raiseStatusList, Long involvedPaxId);
    List<Map<String, Object>> getListOfRaisesOnNomination(Integer pageNumber, Integer pageSize, Long activityId
            , Long fromPaxId, List<String> parentStatusList, List<String> raiseStatusList, Long involvedPaxId);
    List<Map<String, Object>> getListOfPaxOnActivity(Long managerId, Long activityId, 
            boolean excludePrivate, List<String> statusList);
    List<Map<String, Object>> getListOfPaxOnNomination(Long paxId, Long nominationId, boolean excludePrivate);
    Long getRecognitionParentId(Long activityId, Long toPaxId);
    Boolean isAlreadyRaised(Long fromPaxId, Long toPaxId, Long activityId);
    Boolean isValidRecognition(Long fromPaxId, Long toPaxId, Long activityId);
    Long getPointsForRaise(Long paxId, Long activityId);
    Double getRaisePointsForManager(Long paxId, Long nominationId);

}
