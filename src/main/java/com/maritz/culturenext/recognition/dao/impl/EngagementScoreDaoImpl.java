package com.maritz.culturenext.recognition.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.recognition.dao.EngagementScoreDao;

@Component
public class EngagementScoreDaoImpl extends AbstractDaoImpl implements EngagementScoreDao {
    
    private static final String QUERY = "SELECT ESD.* " + 
                "FROM component.UF_LIST_TO_TABLE_ID(:" + ProgramConstants.PAX_ID_LIST_SQL_PARAM + ", ',') AUDIENCE " + 
                "LEFT JOIN component.VW_ENGAGEMENT_SCORE_DETAILS ESD " + 
                    "ON AUDIENCE.ITEMS = ESD.PAX_ID";

    @Override
    public List<Map<String, Object>> getEngagementScoreDetails(List<Long> paxIdList) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        String paxIdListString = StringUtils.join(paxIdList, ProjectConstants.COMMA_DELIM);
        params.addValue(ProgramConstants.PAX_ID_LIST_SQL_PARAM, paxIdListString);
        
        return namedParameterJdbcTemplate.query(QUERY, params, new CamelCaseMapRowMapper());
    }

}
