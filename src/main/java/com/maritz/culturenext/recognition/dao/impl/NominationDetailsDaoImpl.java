package com.maritz.culturenext.recognition.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.recognition.dao.NominationDetailsDao;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class NominationDetailsDaoImpl extends AbstractDaoImpl implements NominationDetailsDao {
    
    private static final String NOMINATION_COMMON_INFO_QUERY = 
            "SELECT n.ID AS 'NOMINATION_ID', n.STATUS_TYPE_CODE, n.SUBMITTER_PAX_ID, " +
            "n.PROGRAM_ID, n.HEADLINE_COMMENT, n.COMMENT, n.SUBMITTAL_DATE, d.BUDGET_ID, " + 
            "p.PROGRAM_TYPE_CODE, p.PROGRAM_NAME, pm.MISC_DATA AS 'PAYOUT_TYPE', ni.ID AS 'NEWSFEED_ITEM_ID' " +
            "FROM component.NOMINATION n " +
            //--Only grab one DISCRETIONARY/RECOGNITION per Nomination b/c we only need the budget id with this query
            "CROSS APPLY ( " +
                "SELECT TOP 1 d.BUDGET_ID_DEBIT AS 'BUDGET_ID' FROM component.RECOGNITION r " +
                "LEFT JOIN component.DISCRETIONARY d ON d.TARGET_ID = r.ID AND d.TARGET_TABLE = 'RECOGNITION' " +
                "WHERE r.NOMINATION_ID = n.ID " +
            ") d " +
            "JOIN component.PROGRAM p ON p.PROGRAM_ID = n.PROGRAM_ID " +
            "LEFT JOIN component.PROGRAM_MISC pm ON pm.PROGRAM_ID = p.PROGRAM_ID AND pm.VF_NAME = 'SELECTED_AWARD_TYPE' " +
            "LEFT JOIN component.NEWSFEED_ITEM ni ON ni.TARGET_ID = n.ID AND ni.TARGET_TABLE = 'NOMINATION' " +
            "where n.ID in (:nominationIds)";

    @Override
    public List<Map<String, Object>> getNominationCommonInfo(
            List<Long> nominationIds) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.NOMINATION_IDS, nominationIds);

        List<Map<String,Object>> nodes = 
                namedParameterJdbcTemplate.query(NOMINATION_COMMON_INFO_QUERY, params, new CamelCaseMapRowMapper());

        // need to convert all Date objects to String.
        for(Map<String, Object> node: nodes){
            if (node.get(ProjectConstants.SUBMITTAL_DATE) != null) {
                node.put(ProjectConstants.SUBMITTAL_DATE, 
                        DateUtil.convertToUTCDate((Date) node.get(ProjectConstants.SUBMITTAL_DATE)));
            }
        }

        return nodes;
    }
}
