package com.maritz.culturenext.recognition.dao;

import java.util.List;
import java.util.Map;

public interface RecognitionBulkUploadDao {
    
    /**
     * Return the complete CSV file with errors
     * 
     * @param batchId - batchId of the file with errors.
     * @return a list with the content of the CSV file and the errors
     */
    public List<Map<String, Object>> getStageDataByBatchId(Long batchId);
    
    /**
     * Executes the validation of the CSV file
     * 
     * @param batchId - batch id to be assigned
     * @param userId - user id
     * @param fileCrc - file crc of the file to be processed
     * @param fileName - the name of the file to be processed
     * @return results of the validation
     */
    public List<Map<String, Object>> validateRecognitionBulkUpload(Long batchId, 
            String userId, String fileCrc, String fileName);
    
    /**
     * Returns sum of points grouped by submitter / budget combination
     * 
     * @param batchId - id of batch to check
     * @return result set containing pax id and control num of the submitter, budget id, total points and giving limit of the budget.
     */
    public List<Map<String, Object>> getPaxBudgetUsageDetails (Long batchId);
    
    /**
     * Returns sum of points grouped by budget as well as budget total remaining
     * 
     * @param batchId - id of batch to check
     * @return result set containing budget id, total amount in file, and budget remaining total
     */
    public List<Map<String, Object>> getBudgetUsageDetails (Long batchId);
    
    /**
     * Return the data required for bulk upload history tables
     * 
     * @param statusList - List of statuses to be searched on
     * @return info(batchId, filename, row counts) for bulk upload history tables
     */
    public List<Map<String, Object>> getBulkUploadHistory(String statusList, Integer pageNumber, Integer pageSize);
}
