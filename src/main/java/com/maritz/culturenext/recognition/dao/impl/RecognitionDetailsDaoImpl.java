package com.maritz.culturenext.recognition.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.recognition.dao.RecognitionDetailsDao;

@Repository
public class RecognitionDetailsDaoImpl extends PaginatingDaoImpl implements RecognitionDetailsDao {

    private static final String NOMINATION_ID_PLACEHOLDER = "NOMINATION_ID";
    private static final String OVERRIDE_VISIBILITY_PLACEHOLDER = "OVERRIDE_VISIBILITY";
    private static final String PAX_ID_PLACEHOLDER = "PAX_ID";
    private static final String STATUS_TYPE_CODE_LIST_PLACEHOLDER = "STATUS_TYPE_CODE_LIST";

    private static final String QUERY =
      "SELECT " +
      "  RECOGNITION.ID, " +
      "  NOMINATION.SUBMITTER_PAX_ID, " +
      "  RECOGNITION.RECEIVER_PAX_ID, " +
      "  RECOGNITION.STATUS_TYPE_CODE, " +
      "  RECOGNITION.NOMINATION_ID, " +
      "  APPROVAL_PENDING.PAX_ID AS APPROVAL_PENDING_PAX_ID, " +
      "  CASE WHEN RECOGNITION.STATUS_TYPE_CODE = 'REVERSED' THEN AH_REVERSED.PAX_ID  " +
      "       ELSE APPROVAL_HISTORY.PAX_ID END AS APPROVAL_HISTORY_PAX_ID, " +
      "  APPROVAL_PROCESS.LAST_APPROVAL_DATE, " +
      "  RECOGNITION.CREATE_DATE," +
      "  NOMINATION.BUDGET_ID, " +
      "  PROGRAM.PROGRAM_NAME AS PROGRAM, " +
      "  CASE WHEN RECOGNITION.STATUS_TYPE_CODE = 'REVERSED' THEN AH_REVERSED.APPROVAL_NOTES " +
      "       ELSE APPROVAL_HISTORY.APPROVAL_NOTES END AS APPROVAL_COMMENT,  " +
      "  RECOGNITION.COMMENT, " +
      "  NOMINATION.COMMENT AS NOMINATION_COMMENT, " +
      "  NOMINATION.HEADLINE_COMMENT, " +
      "  BUDGET.BUDGET_CODE AS BUDGET_NAME, " +
      "  NOMINATION.SUBMITTAL_DATE, " +
      "  RECOGNITION.AMOUNT, " +
      "  RAISE_DETAILS.RAISE_AMOUNT, " +
      "  DISCRETIONARY.TRANSACTION_HEADER_ID AS TRANSACTION_HEADER_ID " +
      "FROM component.NOMINATION " +
      "LEFT JOIN component.RECOGNITION " +
      "  ON RECOGNITION.NOMINATION_ID = NOMINATION.ID " +
      "LEFT JOIN component.DISCRETIONARY " +
      "  ON DISCRETIONARY.TARGET_ID = RECOGNITION.ID AND DISCRETIONARY.TARGET_TABLE = 'RECOGNITION' " +
      "  AND DISCRETIONARY.amount >= 0 " +
      "LEFT JOIN component.BUDGET BUDGET " +
      "  ON NOMINATION.BUDGET_ID = BUDGET.ID " +
      "LEFT JOIN component.PROGRAM " +
      "  ON NOMINATION.PROGRAM_ID = PROGRAM.PROGRAM_ID " +
      "LEFT JOIN component.APPROVAL_PROCESS " +
      "  ON RECOGNITION.ID = APPROVAL_PROCESS.TARGET_ID " +
      "LEFT JOIN component.APPROVAL_PENDING " +
      "  ON RECOGNITION.ID = APPROVAL_PENDING.TARGET_ID AND APPROVAL_PENDING.TARGET_TABLE = 'RECOGNITION' " +
      "LEFT JOIN component.APPROVAL_HISTORY " +
      "  ON RECOGNITION.ID = APPROVAL_HISTORY.TARGET_ID AND APPROVAL_HISTORY.TARGET_TABLE = 'RECOGNITION' AND APPROVAL_HISTORY.STATUS_TYPE_CODE <> 'REVERSED' " +
      "LEFT JOIN component.APPROVAL_HISTORY AH_REVERSED  " +
      "  ON RECOGNITION.ID = AH_REVERSED.TARGET_ID AND AH_REVERSED.TARGET_TABLE = 'RECOGNITION' AND AH_REVERSED.STATUS_TYPE_CODE = 'REVERSED' " +
      "LEFT JOIN component.PAX_MISC" +
      "  ON PAX_MISC.PAX_ID = RECOGNITION.RECEIVER_PAX_ID " +
      "  AND PAX_MISC.VF_NAME = 'SHARE_REC' " +
      "LEFT JOIN component.RELATIONSHIP " +
      "  ON RELATIONSHIP.PAX_ID_1 = RECOGNITION.RECEIVER_PAX_ID " +
      "  AND RELATIONSHIP.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
      "LEFT JOIN component.PAX RECEIVER_PAX " +
      "  ON RECEIVER_PAX.PAX_ID = RECOGNITION.RECEIVER_PAX_ID " +
      "LEFT JOIN ( " +
      "  SELECT " +
      "    PARENT_ID AS RECOGNITION_ID, " +
      "    SUM(AMOUNT) AS RAISE_AMOUNT " +
      "  FROM component.RECOGNITION " +
      "  WHERE PARENT_ID IS NOT NULL " +
      "    AND STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED') " +
      "    and NOMINATION_ID = :" + NOMINATION_ID_PLACEHOLDER +
      "  GROUP BY PARENT_ID " +
      "  ) RAISE_DETAILS " +
      "  ON RECOGNITION.ID = RAISE_DETAILS.RECOGNITION_ID " +
      "WHERE NOMINATION.ID = :" + NOMINATION_ID_PLACEHOLDER + " " +
      "AND ( " +
      "  :" + STATUS_TYPE_CODE_LIST_PLACEHOLDER + " IS NULL " +
      "  OR RECOGNITION.STATUS_TYPE_CODE IN ( " +
      "    SELECT * FROM component.UF_LIST_TO_TABLE(:" + STATUS_TYPE_CODE_LIST_PLACEHOLDER + ", ',') " +
      "  )" +
      ")" +
      "AND (" +
      "  coalesce(PAX_MISC.MISC_DATA, 'FALSE') = 'TRUE'" +
      "  OR 1 = :" + OVERRIDE_VISIBILITY_PLACEHOLDER +
      "  OR RELATIONSHIP.PAX_ID_2 = :" + PAX_ID_PLACEHOLDER +
      "  OR APPROVAL_HISTORY.PAX_ID = :" + PAX_ID_PLACEHOLDER +
      "  OR APPROVAL_PENDING.PAX_ID = :" + PAX_ID_PLACEHOLDER +
      "  OR NOMINATION.SUBMITTER_PAX_ID = :" + PAX_ID_PLACEHOLDER +
      "  OR RECOGNITION.RECEIVER_PAX_ID = :" + PAX_ID_PLACEHOLDER +
      ")";

    private static final String ORDER_BY = "RECEIVER_PAX.FIRST_NAME, RECEIVER_PAX.LAST_NAME";

    @Override
    public List<Map<String, Object>> getRecognitionsForNomination(Long paxId, Long nominationId,
            Boolean overrideVisibility, String commaSeparatedStatusTypeCodeList, Integer pageNumber, Integer pageSize) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_PLACEHOLDER, paxId);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);
        params.addValue(OVERRIDE_VISIBILITY_PLACEHOLDER, overrideVisibility);
        params.addValue(STATUS_TYPE_CODE_LIST_PLACEHOLDER, commaSeparatedStatusTypeCodeList);

        return getPageWithTotalRows(QUERY, params, new CamelCaseMapRowMapper(), ORDER_BY, pageNumber, pageSize);
    }

}
