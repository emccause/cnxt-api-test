package com.maritz.culturenext.recognition.dao;

import java.util.List;

import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.Recognition;

public interface NominationDataBulkSaveDao {
    
    /**
     * TODO JavaDocs
     * @param nominationCriteriaList
     */
    void saveNominationCriteriaList(List<NominationCriteria> nominationCriteriaList);
    
    /**
     * TODO JavaDocs
     * @param recognitionList
     */
    void saveRecognitionList(List<Recognition> recognitionList);
    
    /**
     * TODO JavaDocs
     * @param alertList
     */
    void saveAlertList(List<Alert> alertList);
    
    /**
     * Writes to the db Transaction Header and Discretionary tables
     * @param nomination - Nomination used as a reference for the db insert statements (id and status)
     */
    void createTransactionHeaderAndDiscretionaryEntries(Nomination nomination);

}
