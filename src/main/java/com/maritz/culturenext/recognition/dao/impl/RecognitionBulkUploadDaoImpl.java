package com.maritz.culturenext.recognition.dao.impl;

import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.recognition.dao.RecognitionBulkUploadDao;

@Repository
public class RecognitionBulkUploadDaoImpl extends PaginatingDaoImpl implements RecognitionBulkUploadDao {
    
    private static final String BATCH_ID_SQL_PARAM = "BATCH_ID";
    private static final String USER_ID_SQL_PARAM = "USER_ID";
    private static final String FILE_CRC_SQL_PARAM = "FILE_CRC";
    private static final String FILE_NAME_SQL_PARAM = "FILE_NAME";
    
    private static final String STATUS_LIST_PLACEHOLDER = "STATUS_LIST";
    private static final String PARSED_STATUS_LIST_PLACEHOLDER = "PARSED_STATUS_LIST";
    
    private static final String GET_FILE_WITH_ERRORS_QUERY = 
            "SELECT * FROM STAGE_ADVANCED_REC WHERE BATCH_ID = :" + BATCH_ID_SQL_PARAM;
    
    private static final String BULK_UPLOAD_ADVANCED_REC_QUERY = 
            "EXEC component.UP_BULK_UPLOAD_ADVANCED_REC " 
                    + ":" + BATCH_ID_SQL_PARAM 
                    + ", :" + USER_ID_SQL_PARAM
                    + ", :" + FILE_CRC_SQL_PARAM
                    + ", :" + FILE_NAME_SQL_PARAM;
    
    private static final String PAX_BUDGET_USAGE_DETAILS_QUERY = 
            "SELECT " +
                "PAX.PAX_ID, " +
                "PAX.CONTROL_NUM, " +
                "BUDGET_ID, " +
                "SUM(AWARD_AMOUNT) AS TOTAL_AMOUNT, " +
                "MIN(BUDGET.INDIVIDUAL_GIVING_LIMIT) AS INDIVIDUAL_GIVING_LIMIT " +
            "FROM component.STAGE_ADVANCED_REC SAR " +
            "LEFT JOIN component.PAX " +
                "ON SAR.SUBMITTER_ID = PAX.CONTROL_NUM " +
            "LEFT JOIN component.BUDGET " +
                "ON SAR.BUDGET_ID = BUDGET.ID " +
            "WHERE BATCH_ID = :" + BATCH_ID_SQL_PARAM + " " +
            "GROUP BY PAX.PAX_ID, PAX.CONTROL_NUM, BUDGET_ID";
    
    private static final String BUDGET_USAGE_DETAILS_QUERY = 
            "SELECT " +
                "RESULTS.*, " +
                "(VBT.TOTAL_AMOUNT - VBT.TOTAL_USED) AS BUDGET_TOTAL " +
            "FROM ( " +
                "SELECT " +
                    "BUDGET_ID, " +
                    "SUM(AWARD_AMOUNT) AS TOTAL_AMOUNT " +
                "FROM component.STAGE_ADVANCED_REC " +
                "WHERE BATCH_ID = :" + BATCH_ID_SQL_PARAM + " " +
                "AND BUDGET_ID IS NOT NULL " + 
                "GROUP BY BUDGET_ID " +
            ") RESULTS " +
            "LEFT JOIN component.VW_BUDGET_TOTALS_FLAT VBT " +
                "ON RESULTS.BUDGET_ID = VBT.BUDGET_ID";

    private static final String GET_BULK_UPLOAD_HISTORY = 
            "SELECT " 
                + "BATCH.BATCH_ID, "
                + "BATCH_FILE.FILENAME AS FILE_NAME, "
                + "BATCH.UPDATE_DATE AS PROCESS_DATE, "
                + "ISNULL(TOTAL_ROWS.MESSAGE, 0) AS TOTAL_ROWS, "
                + "ISNULL(TOTAL_AWARDS.MESSAGE, 0) AS TOTAL_AWARDS, "
                + "ISNULL(NOMINATIONS.MESSAGE, 0) AS NOMINATIONS, "
                + "BATCH.STATUS_TYPE_CODE AS STATUS, "
                + "ISNULL(PPPX_APPLIED.MESSAGE, 'FALSE') AS PPP_INDEX_APPLIED "
            + "FROM component.BATCH "
            + "LEFT JOIN component.BATCH_EVENT TOTAL_ROWS "
                + "ON BATCH.BATCH_ID = TOTAL_ROWS.BATCH_ID "
                + "AND TOTAL_ROWS.BATCH_EVENT_TYPE_CODE = 'RECS' "
            + "LEFT JOIN component.BATCH_EVENT TOTAL_AWARDS "
                + "ON BATCH.BATCH_ID = TOTAL_AWARDS.BATCH_ID "
                + "AND TOTAL_AWARDS.BATCH_EVENT_TYPE_CODE = 'TOTAL_AWARD_AMOUNT' "
            + "LEFT JOIN component.BATCH_EVENT NOMINATIONS "
                + "ON BATCH.BATCH_ID = NOMINATIONS.BATCH_ID "
                + "AND NOMINATIONS.BATCH_EVENT_TYPE_CODE = 'NOMINATION_COUNT' "
            + "LEFT JOIN COMPONENT.BATCH_FILE "
                + "ON BATCH.BATCH_ID = BATCH_FILE.BATCH_ID "
            + "LEFT JOIN component.BATCH_EVENT PPPX_APPLIED "
                + "ON BATCH.BATCH_ID = PPPX_APPLIED.BATCH_ID "
                + "AND PPPX_APPLIED.BATCH_EVENT_TYPE_CODE = 'PPP_INDEX_APPLIED' "
            + "WHERE ( :" + STATUS_LIST_PLACEHOLDER + " IS NULL "
                + "OR BATCH.STATUS_TYPE_CODE IN ( :" + PARSED_STATUS_LIST_PLACEHOLDER + ")) "
                + "AND BATCH.BATCH_TYPE_CODE = 'ADV_REC_BULK_UPLOAD' ";
    
    private static final String GET_BULK_UPLOAD_HISTORY_ORDER_BY = "PROCESS_DATE DESC";
    
    public List<Map<String, Object>> validateRecognitionBulkUpload(Long batchId, 
            String userId, String fileCrc, String fileName) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);
        params.addValue(USER_ID_SQL_PARAM, userId);
        params.addValue(FILE_CRC_SQL_PARAM, fileCrc);
        params.addValue(FILE_NAME_SQL_PARAM, fileName);
        
        return namedParameterJdbcTemplate.query(BULK_UPLOAD_ADVANCED_REC_QUERY, params,  new CamelCaseMapRowMapper());
    }

    public List<Map<String, Object>> getStageDataByBatchId(Long batchId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);

        return namedParameterJdbcTemplate.query(GET_FILE_WITH_ERRORS_QUERY, params,  new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getPaxBudgetUsageDetails(Long batchId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);

        return namedParameterJdbcTemplate.query(PAX_BUDGET_USAGE_DETAILS_QUERY, params,  new CamelCaseMapRowMapper());
    }

    @Override
    public List<Map<String, Object>> getBudgetUsageDetails(Long batchId) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_SQL_PARAM, batchId);

        return namedParameterJdbcTemplate.query(BUDGET_USAGE_DETAILS_QUERY, params,  new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getBulkUploadHistory(String statusList, Integer pageNumber, Integer pageSize) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        params.addValue(PARSED_STATUS_LIST_PLACEHOLDER, StringUtils.parseDelimitedData(statusList));

        return getPageWithTotalRows(GET_BULK_UPLOAD_HISTORY, params, new CamelCaseMapRowMapper(), GET_BULK_UPLOAD_HISTORY_ORDER_BY, pageNumber, pageSize);
    }
}
