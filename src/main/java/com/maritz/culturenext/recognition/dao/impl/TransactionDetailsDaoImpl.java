package com.maritz.culturenext.recognition.dao.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.recognition.dao.TransactionDetailsDao;

@Repository
public class TransactionDetailsDaoImpl extends PaginatingDaoImpl implements TransactionDetailsDao {

    private static final String AWARD_CODES_SQL_PARAM = "awardCodes";
    private static final String END_DATE_SQL_PARAM = "endDate";
    private static final String PAGE_NUMBER_SQL_PARAM = "pageNumber";
    private static final String PAGE_SIZE_SQL_PARAM = "pageSize";
    private static final String PAX_ID_SQL_PARAM = "paxId";
    private static final String PROGRAM_IDS_SQL_PARAM = "programIds";
    private static final String PROGRAM_TYPES_SQL_PARAM = "programTypes";
    private static final String RECIPIENT_IDS_SQL_PARAM = "recipientPaxIds";
    private static final String REVERSAL_COMMENT_SQL_PARAM = "reversalComment";
    private static final String START_DATE_SQL_PARAM = "startDate";
    private static final String STATUS_LIST_SQL_PARAM = "statusList";
    private static final String SUBMITTER_IDS_SQL_PARAM = "submitterPaxIds";
    private static final String TRANSACTION_IDS_SQL_PARAM = "transactionIds";

    private static final String TRANSACTION_SEARCH_QUERY = "EXEC component.UP_TRANSACTION_SEARCH "
            + ":" + PAX_ID_SQL_PARAM
            + ", :" + START_DATE_SQL_PARAM
            + ", :" + END_DATE_SQL_PARAM
            + ", :" + AWARD_CODES_SQL_PARAM
            + ", :" + PROGRAM_IDS_SQL_PARAM
            + ", :" + PROGRAM_TYPES_SQL_PARAM
            + ", :" + STATUS_LIST_SQL_PARAM
            + ", :" + RECIPIENT_IDS_SQL_PARAM
            + ", :" + SUBMITTER_IDS_SQL_PARAM
            + ", :" + PAGE_NUMBER_SQL_PARAM
            + ", :" + PAGE_SIZE_SQL_PARAM;

    private static final String GET_TRANSACTIONS_TO_REVERSE_QUERY =
            "SELECT th_raise.* " +
            "FROM component.TRANSACTION_HEADER th " +
            "JOIN component.DISCRETIONARY disc ON disc.TRANSACTION_HEADER_ID = th.ID " +
            "JOIN component.RECOGNITION recg ON recg.PARENT_ID = disc.TARGET_ID AND disc.TARGET_TABLE = 'RECOGNITION' " +
            "JOIN component.DISCRETIONARY disc_raise ON disc_raise.TARGET_ID = recg.ID AND disc_raise.TARGET_TABLE = 'RECOGNITION' " +
            "JOIN component.TRANSACTION_HEADER th_raise ON th_raise.ID = disc_raise.TRANSACTION_HEADER_ID " +
            "AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE <> 'FUND' AND th_raise.STATUS_TYPE_CODE = 'APPROVED' " +
            "WHERE th.ID in (:" + TRANSACTION_IDS_SQL_PARAM + ") " +
            "UNION ALL " +
            "SELECT th.*" +
            "FROM component.TRANSACTION_HEADER th " +
            "WHERE th.ID in (:" + TRANSACTION_IDS_SQL_PARAM + ") " +
            "AND th.TYPE_CODE = 'DISC' AND th.SUB_TYPE_CODE <> 'FUND' AND th.STATUS_TYPE_CODE = 'APPROVED'";

    private static final String REVERSE_TRANSACTIONS_QUERY =
            "EXEC component.UP_REVERSE_TRANSACTIONS :" +
            TRANSACTION_IDS_SQL_PARAM + ", :" + REVERSAL_COMMENT_SQL_PARAM + ", :" + PAX_ID_SQL_PARAM;

    @Override
    public List<Map<String, Object>> getTransactionSearch(Long loggedPax, List<String> programId, List<String> awardCode,
            List<String> status, List<String> recipientPaxId, List<String> submitterPaxId, List<String> programType,
            Date startDate, Date endDate, Integer pageNumber, Integer pageSize) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(PAX_ID_SQL_PARAM, loggedPax);
        params.addValue(START_DATE_SQL_PARAM, startDate);
        params.addValue(END_DATE_SQL_PARAM, endDate);
        params.addValue(AWARD_CODES_SQL_PARAM, awardCode);
        params.addValue(PROGRAM_IDS_SQL_PARAM, programId);
        params.addValue(PROGRAM_TYPES_SQL_PARAM, programType);
        params.addValue(STATUS_LIST_SQL_PARAM, status);
        params.addValue(RECIPIENT_IDS_SQL_PARAM, recipientPaxId);
        params.addValue(SUBMITTER_IDS_SQL_PARAM, submitterPaxId);
        params.addValue(PAGE_NUMBER_SQL_PARAM,(pageNumber - 1) * pageSize);
        params.addValue(PAGE_SIZE_SQL_PARAM, pageSize);

        return namedParameterJdbcTemplate.query(TRANSACTION_SEARCH_QUERY, params,  new CamelCaseMapRowMapper());
    }

    @Override
    public List<TransactionHeader> getTransactionsToReverse(List<Long> transactionIds) {
        return namedParameterJdbcTemplate.query(
            GET_TRANSACTIONS_TO_REVERSE_QUERY
            ,new MapSqlParameterSource()
                .addValue(TRANSACTION_IDS_SQL_PARAM, transactionIds)
            ,new ConcentrixBeanRowMapper<TransactionHeader>()
                .setBeanClass(TransactionHeader.class)
                .build()
        );
    }

    @Override
    public void reverseTransactions(Collection<Long> transactionIds, String reversalComment, Long paxId) {
        if (transactionIds == null || transactionIds.isEmpty()) return;

        namedParameterJdbcTemplate.update(
            REVERSE_TRANSACTIONS_QUERY
            ,new MapSqlParameterSource()
                .addValue(TRANSACTION_IDS_SQL_PARAM, buildDelimitedIdString(transactionIds))
                .addValue(REVERSAL_COMMENT_SQL_PARAM, reversalComment)
                .addValue(PAX_ID_SQL_PARAM, paxId)
        );
    }

    protected String buildDelimitedIdString(Collection<Long> ids) {
        StringBuilder sb = new StringBuilder(1024);

        for (Long id : ids) {
            sb.append(id).append(",");
        }
        if (sb.length() > 0) sb.setLength(sb.length() - 1);    //remove trailing comma

        return sb.toString();
    }
}
