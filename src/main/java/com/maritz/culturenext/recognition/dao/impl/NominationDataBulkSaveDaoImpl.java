package com.maritz.culturenext.recognition.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.List;

@Repository
public class NominationDataBulkSaveDaoImpl extends AbstractDaoImpl implements NominationDataBulkSaveDao {
    
    private static final String NOMINATION_ID_SQL_PARAM = "NOMINATION_ID";
    private static final String PROXY_PAX_ID_PARAM = "PROXY_PAX_ID";
    
    private static final String NOMINATION_CRITERIA_INSERT_BASE = 
            "INSERT INTO component.NOMINATION_CRITERIA (RECOGNITION_CRITERIA_ID, NOMINATION_ID) "
            + "VALUES (?, ?)";
    private static final String RECOGNITION_INSERT_BASE = 
            "INSERT INTO component.RECOGNITION (NOMINATION_ID, RECEIVER_PAX_ID, GROUP_ID, VIEWED, STATUS_TYPE_CODE, AMOUNT, PROGRAM_AWARD_TIER_ID) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String ALERT_INSERT_BASE = 
            "INSERT INTO component.ALERT (ALERT_TYPE_CODE, ALERT_SUB_TYPE_CODE, PROGRAM_ID, TARGET_ID, PAX_ID, STATUS_TYPE_CODE, STATUS_DATE, TARGET_TABLE) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        
    private static final String CREATE_TRANSACTION_HEADER_ENTRIES =
            "INSERT INTO component.TRANSACTION_HEADER " +
            "(PAX_GROUP_ID, BATCH_ID, TYPE_CODE, SUB_TYPE_CODE, ACTIVITY_DATE, STATUS_TYPE_CODE, PROGRAM_ID, PERFORMANCE_VALUE, PROXY_PAX_ID) " +
            "SELECT " +
                "PG.PAX_GROUP_ID, " +
                "BE.BATCH_ID,  " +
                "'DISC' AS TYPE_CODE, " +
                "'RECG' AS SUB_TYPE_CODE, " +
                "NOM.CREATE_DATE AS ACTIVITY_DATE, " +
                "CASE " +
                    "WHEN REC.STATUS_TYPE_CODE = 'AUTO_APPROVED' THEN 'APPROVED' " +
                    "WHEN REC.STATUS_TYPE_CODE IN ('REJECTED_RESTRICTED', 'REJECTED_INACTIVE') THEN 'REJECTED' " +
                    "ELSE REC.STATUS_TYPE_CODE " + 
                "END AS 'STATUS_TYPE_CODE', " +
                "NOM.PROGRAM_ID, " +
                "REC.AMOUNT AS PERFORMANCE_VALUE,  " +
                ":" + PROXY_PAX_ID_PARAM +  " AS 'PROXY_PAX_ID'" +
            "FROM component.NOMINATION NOM " +
                "INNER JOIN component.RECOGNITION REC " +
                    "ON NOM.ID = REC.NOMINATION_ID " +
                "INNER JOIN component.PAX_GROUP PG " +
                    "ON REC.RECEIVER_PAX_ID = PG.PAX_ID " +
                "INNER JOIN component.BATCH_EVENT BE " +
                    "ON BE.TARGET_ID = NOM.ID " +
                    "AND BE.TARGET_TABLE = 'NOMINATION' " +
                    "AND BE.BATCH_EVENT_TYPE_CODE = 'NTHLKE' " +
                "WHERE NOM.ID = :" + NOMINATION_ID_SQL_PARAM;
    
    private static final String CREATE_DISCRETIONARY_ENTRIES = 
            "INSERT INTO component.DISCRETIONARY " +
            "(TRANSACTION_HEADER_ID, AMOUNT, PERFORMANCE_DATE, DESCRIPTION, BUDGET_ID_DEBIT, TARGET_TABLE, TARGET_ID) " +
            "SELECT " +
                "TH.ID AS TRANSACTION_HEADER_ID, " +
                "TH.PERFORMANCE_VALUE AS AMOUNT, " +
                "TH.ACTIVITY_DATE AS PERFORMANCE_DATE, " +
                "PROGRAM.PROGRAM_NAME AS COMMENT, " +
                "NOM.BUDGET_ID AS BUDGET_ID_DEBIT, " +
                "'RECOGNITION' AS TARGET_TABLE, " +
                "REC.ID AS TARGET_ID " +
            "FROM component.NOMINATION NOM " +
                "INNER JOIN component.RECOGNITION REC " +
                    "ON NOM.ID = REC.NOMINATION_ID " +
                "INNER JOIN component.PAX_GROUP PG " +
                    "ON REC.RECEIVER_PAX_ID = PG.PAX_ID " +
                "INNER JOIN component.BATCH_EVENT BE " +
                    "ON BE.TARGET_ID = NOM.ID " +
                    "AND BE.TARGET_TABLE = 'NOMINATION' " +
                    "AND BE.BATCH_EVENT_TYPE_CODE = 'NTHLKE' " +
                "INNER JOIN component.TRANSACTION_HEADER TH " +
                    "ON TH.PAX_GROUP_ID = PG.PAX_GROUP_ID " +
                    "AND TH.BATCH_ID = BE.BATCH_ID " +
                "INNER JOIN component.PROGRAM " +
                    "ON NOM.PROGRAM_ID = PROGRAM.PROGRAM_ID " +
                "WHERE NOM.ID = :" + NOMINATION_ID_SQL_PARAM;

    @Override
    public void saveNominationCriteriaList(final List<NominationCriteria> nominationCriteriaList) {
        
        jdbcTemplate.batchUpdate(NOMINATION_CRITERIA_INSERT_BASE, new BatchPreparedStatementSetter() {

            @Override
            public int getBatchSize() {
                return nominationCriteriaList.size();
            }

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                NominationCriteria nc = nominationCriteriaList.get(i);
                setLong(ps, 1, nc.getRecognitionCriteriaId());
                setLong(ps, 2, nc.getNominationId());
            }
        });
    }

    @Override
    public void saveRecognitionList(final List<Recognition> recognitionList) {
        
        jdbcTemplate.batchUpdate(RECOGNITION_INSERT_BASE, new BatchPreparedStatementSetter() {

            @Override
            public int getBatchSize() {
                return recognitionList.size();
            }

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Recognition rec = recognitionList.get(i);
                setLong(ps, 1, rec.getNominationId());
                setLong(ps, 2, rec.getReceiverPaxId());
                setLong(ps, 3, rec.getGroupId());
                setString(ps, 4, rec.getViewed());
                setString(ps, 5, rec.getStatusTypeCode());
                setDouble(ps, 6, rec.getAmount());
                setLong(ps, 7, rec.getProgramAwardTierId());
            }
        });
    }

    @Override
    public void saveAlertList(final List<Alert> alertList) {
        
        jdbcTemplate.batchUpdate(ALERT_INSERT_BASE, new BatchPreparedStatementSetter() {

            @Override
            public int getBatchSize() {
                return alertList.size();
            }

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Alert alert = alertList.get(i);
                setString(ps, 1, alert.getAlertTypeCode());
                setString(ps, 2, alert.getAlertSubTypeCode());
                setLong(ps, 3, alert.getProgramId());
                setLong(ps, 4, alert.getTargetId());
                setLong(ps, 5, alert.getPaxId());
                setString(ps, 6, alert.getStatusTypeCode());
                setDate(ps, 7, alert.getStatusDate());
                setString(ps, 8, alert.getTargetTable());
            }
        });
    }

    @Override
    public void createTransactionHeaderAndDiscretionaryEntries(Nomination nomination) {        
        MapSqlParameterSource params = new MapSqlParameterSource();
        Long proxyPaxId = nomination.getProxyPaxId();
        params.addValue(NOMINATION_ID_SQL_PARAM, nomination.getId());
        params.addValue(PROXY_PAX_ID_PARAM, proxyPaxId);
        
        namedParameterJdbcTemplate.update(CREATE_TRANSACTION_HEADER_ENTRIES, params);

        namedParameterJdbcTemplate.update(CREATE_DISCRETIONARY_ENTRIES, params);
    }

    /**
     * Helper methods for handling prepared statement null checks.
     */
    private void setString(PreparedStatement ps,Integer index, String value) throws SQLException {
        if (value != null) {
            ps.setString(index, value);    
        } else {
            ps.setNull(index, Types.NVARCHAR);
        }
    }
    
    private void setLong(PreparedStatement ps,Integer index, Long value) throws SQLException {
        if (value != null) {
            ps.setLong(index, value);    
        } else {
            ps.setNull(index, Types.BIGINT);
        }
    }
    
    private void setDouble(PreparedStatement ps,Integer index, Double value) throws SQLException {
        if (value != null) {
            ps.setDouble(index, value);    
        } else {
            ps.setNull(index, Types.DOUBLE);
        }
    }
    
    private void setDate(PreparedStatement ps,Integer index, Date value) throws SQLException {
        if (value != null) {
            ps.setTimestamp(index, new Timestamp(value.getTime()));
        } else {
            ps.setNull(index, Types.TIMESTAMP);
        }
    }
}