package com.maritz.culturenext.recognition.dao;

import java.util.List;
import java.util.Map;

public interface RecognitionDetailsDao {
    
    List<Map<String, Object>> getRecognitionsForNomination(
            Long paxId, Long nominationId, Boolean overrideVisibility,
            String commaSeparatedStatusTypeCodeList, Integer pageNumber, Integer pageSize
        );
}
