package com.maritz.culturenext.recognition.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.jdbc.dao.impl.PaginatingDaoImpl;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.constants.ProjectConstants;

@Repository
public class RaiseDaoImpl extends PaginatingDaoImpl implements RaiseDao {

    /*
     * Whats needed for the Query:
     * - fromPaxId
     * - Status List
     * - Activity Id
     * - Parent Id logic
     */
    
    private final String ACTIVITY_ID_PLACEHOLDER = "ACTIVITY_ID_PLACEHOLDER";
    private final String EXCLUDE_FLAG_PLACEHOLDER = "EXCLUDE_FLAG_PLACEHOLDER";
    private final String INVOLVED_PAX_PLACEHOLDER = "INVOLVED_PAX_PLACEHOLDER";
    private final String MANAGER_ID_PLACEHOLDER = "MANAGER_ID_PLACEHOLDER";
    private final String NOMINATION_ID_PLACEHOLDER = "NOMINATION_ID_PLACEHOLDER";
    private final String ORDER_BY_PLACEHOLDER = "ORDER_BY_PLACEHOLDER";
    private final String RAISE_STATUS_LIST_PLACEHOLDER = "RAISE_STATUS_LIST_PLACEHOLDER";
    private final String RECEIVER_PAX_ID_PLACEHOLDER = "RECEIVER_PAX_ID_PLACEHOLDER";
    private final String RECOGNITION_STATUS_QUERY_PLACEHOLDER = "RELATIONSHIP_QUERY_PLACEHOLDER";
    private final String RELATIONSHIP_QUERY_PLACEHOLDER = "RELATIONSHIP_QUERY_PLACEHOLDER";
    private final String STATUS_LIST_PLACEHOLDER = "STATUS_LIST_PLACEHOLDER";
    private final String SUBMITTER_PAX_ID_PLACEHOLDER = "SUBMITTER_PAX_ID_PLACEHOLDER";
    private final String SUBMITTER_PAX_PLACEHOLDER = "SUBMITTER_PAX_PLACEHOLDER";

    private final String RELATIONSHIP_LEFT_JOIN_QUERY = "LEFT JOIN RELATIONSHIP REL " +
                        "ON RE.RECEIVER_PAX_ID = REL.PAX_ID_1 " +
                            "AND REL.RELATIONSHIP_TYPE_CODE = 'REPORT_TO' ";
    
    private final String INVOLVED_PAX_FILTER_QUERY = " AND ( NOM.SUBMITTER_PAX_ID = :" + SUBMITTER_PAX_ID_PLACEHOLDER +
                                        " OR RE.RECEIVER_PAX_ID = :" + RECEIVER_PAX_ID_PLACEHOLDER +
                                        " OR REL.PAX_ID_2 = :" + MANAGER_ID_PLACEHOLDER +")";

    private final String LIST_OF_RAISES_QUERY =
            "SELECT re.ID, re.UPDATE_DATE, re.AMOUNT, re.PROGRAM_AWARD_TIER_ID, " +
                    "re.STATUS_TYPE_CODE, nom.SUBMITTER_PAX_ID, " +
                    "re.RECEIVER_PAX_ID, re.COMMENT  " +
                    "FROM RECOGNITION re " +
                    "INNER JOIN NOMINATION nom ON re.NOMINATION_ID = nom.ID " +
                    RELATIONSHIP_QUERY_PLACEHOLDER +
                    "WHERE re.PARENT_ID IN " +
                        "(SELECT RE.ID FROM RECOGNITION RE " +
                            "INNER JOIN NOMINATION NOM ON RE.NOMINATION_ID = NOM.ID " +
                            "WHERE NOM.ID = :" + NOMINATION_ID_PLACEHOLDER + " " +
                            "AND RE.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " )) " +
                    SUBMITTER_PAX_PLACEHOLDER   + " " + 
                    RAISE_STATUS_LIST_PLACEHOLDER + " "  +
                    INVOLVED_PAX_PLACEHOLDER + " " +
                    ORDER_BY_PLACEHOLDER ;
    
    private final String RAISES_ORDER_BY_CLAUSE = "ORDER BY re.UPDATE_DATE DESC";
    
    private final String RAISES_PAGINATION_ORDER_BY = "UPDATE_DATE DESC";

    private final String LIST_OF_PAX_ON_NOMINATION_EXCLUDE_PRIVATE_QUERY =
            "SELECT DISTINCT RECEIVER_PAX_ID FROM RECOGNITION re " +
                    "JOIN NOMINATION nom ON nom.ID = re.NOMINATION_ID " +
                    "JOIN SYS_USER su ON re.RECEIVER_PAX_ID = su.PAX_ID " +
                    "LEFT JOIN PAX_MISC pm ON re.RECEIVER_PAX_ID = pm.PAX_ID " +
                    "LEFT JOIN RELATIONSHIP rel ON re.RECEIVER_PAX_ID = PAX_ID_1 " +
                    "AND RELATIONSHIP_TYPE_CODE = 'REPORT_TO' " +
                    "LEFT JOIN APPROVAL_PENDING ap ON re.ID = ap.TARGET_ID AND ap.TARGET_TABLE = 'RECOGNITION' " +
                    "LEFT JOIN APPROVAL_HISTORY ah ON re.ID = ah.TARGET_ID AND ah.TARGET_TABLE = 'RECOGNITION' " +
                    "WHERE nom.ID = :" + NOMINATION_ID_PLACEHOLDER + " " +
                    "AND (" +
                        "(" +
                            "(" +
                                "(VF_NAME = 'SHARE_REC' AND MISC_DATA = 'TRUE') " +
                                "OR rel.PAX_ID_2 = :" + MANAGER_ID_PLACEHOLDER + " OR 0=:" + EXCLUDE_FLAG_PLACEHOLDER +
                            ") " +
                            "AND su.STATUS_TYPE_CODE NOT IN ('RESTRICTED','INACTIVE')" +
                        ") " +
                        "OR (" +
                            ":" + MANAGER_ID_PLACEHOLDER + " = ah.PAX_ID " + 
                            "OR :" + MANAGER_ID_PLACEHOLDER + " = ap.PAX_ID " +
                            "OR :" + MANAGER_ID_PLACEHOLDER + " = nom.SUBMITTER_PAX_ID " +
                            "OR :" + MANAGER_ID_PLACEHOLDER + " = re.RECEIVER_PAX_ID" +
                        ")" +
                    ")";
    
    private final String RECOGNITION_STATUS_FILTER_QUERY = " AND re.STATUS_TYPE_CODE IN ( :" + STATUS_LIST_PLACEHOLDER + " )";
    
    private final String LIST_OF_PAX_ON_ACTIVITY_EXCLUDE_PRIVATE_QUERY =
            "SELECT re.RECEIVER_PAX_ID " +
                    "FROM NEWSFEED_ITEM ni " +
                    "JOIN RECOGNITION re on re.nomination_id = ni.TARGET_ID AND NI.TARGET_TABLE = 'NOMINATION' " +
                    "JOIN SYS_USER su ON re.RECEIVER_PAX_ID = su.PAX_ID " +
                        " AND su.STATUS_TYPE_CODE NOT IN ('RESTRICTED','INACTIVE') " +
                    "LEFT JOIN PAX_MISC pm ON re.RECEIVER_PAX_ID = pm.PAX_ID and VF_NAME = 'SHARE_REC' " +
                    "WHERE ni.ID = :" + ACTIVITY_ID_PLACEHOLDER + 
                        " AND (pm.MISC_DATA = 'TRUE' OR 0=:" + EXCLUDE_FLAG_PLACEHOLDER+" ) " +
                    RECOGNITION_STATUS_QUERY_PLACEHOLDER;

    private final String GET_RECOGNITION_PARENT_ID =
            "SELECT TOP 1 RE.ID FROM NOMINATION NOM " +
                    "JOIN RECOGNITION RE ON NOM.ID = RE.NOMINATION_ID " +
                    "WHERE NOM.ID = :" + NOMINATION_ID_PLACEHOLDER +" AND " +
                    "RE.RECEIVER_PAX_ID = :" + RECEIVER_PAX_ID_PLACEHOLDER;

    private final String GET_RECEIVER_PAX_FROM_RAISE =
            "SELECT RE.ID " +
                    "FROM RECOGNITION RE " +
                    "JOIN NOMINATION NOM ON NOM.ID=RE.NOMINATION_ID " +
                    "WHERE NOM.PARENT_ID= :" + NOMINATION_ID_PLACEHOLDER + " " +
                    "AND NOM.SUBMITTER_PAX_ID=:" + SUBMITTER_PAX_PLACEHOLDER + " " +
                    "AND RE.RECEIVER_PAX_ID=:" + RECEIVER_PAX_ID_PLACEHOLDER;

    private final String CHECK_VALIDITY_OF_RECOGNITION =
            "SELECT RE.RECEIVER_PAX_ID FROM RECOGNITION RE " +
                    "LEFT JOIN NOMINATION NOM ON RE.NOMINATION_ID = NOM.ID " +
                    "WHERE NOM.ID = :" + NOMINATION_ID_PLACEHOLDER + " " +
                    "AND re.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED','PENDING') " +
                    "AND re.PARENT_ID IS NULL " +
                    "AND ( " +
                        "re.AMOUNT IS NULL " +
                        "OR ( " +
                            "re.AMOUNT IS NOT NULL " +
                            "AND nom.SUBMITTER_PAX_ID <> :" + SUBMITTER_PAX_PLACEHOLDER + " " +
                        ") " +
                    ") " +
                    "AND re.RECEIVER_PAX_ID = :" + RECEIVER_PAX_ID_PLACEHOLDER;

    private final String GET_POINTS_FOR_RAISE_RECEIVED_QUERY =
            "SELECT ISNULL(SUM(RE.AMOUNT),0) AS AMOUNT FROM RECOGNITION RE " +
                    "JOIN NOMINATION NOM ON RE.NOMINATION_ID = NOM.ID " +
                    "WHERE NOM.ID = :" + NOMINATION_ID_PLACEHOLDER  +
                    " AND RE.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED') " +
                    "AND RE.PARENT_ID IS NOT NULL " +
                    "AND RE.RECEIVER_PAX_ID = :" + RECEIVER_PAX_ID_PLACEHOLDER;

    private final String GET_POINTS_FOR_MANAGER_RAISE_RECEIVED_QUERY =
            "SELECT DISTINCT RE.ID, RE.AMOUNT FROM RECOGNITION RE " +
                    "JOIN NOMINATION NOM ON RE.NOMINATION_ID = NOM.ID " +
                    "JOIN RELATIONSHIP REL ON RE.RECEIVER_PAX_ID = REL.PAX_ID_1 " +
                    "WHERE RE.STATUS_TYPE_CODE IN ('APPROVED','AUTO_APPROVED') " +
                    "AND REL.PAX_ID_2 = :" + MANAGER_ID_PLACEHOLDER + 
                    " AND NOM.ID = :" + NOMINATION_ID_PLACEHOLDER;

    /**
     * Returns list of raises associated with given nomination id
     * @param nominationId - Nomination id of raises to get
     * @param fromPaxId - Pax id which submitted raise (Optional)
     * @param parentStatusList - Status type list of parent recognitions
     * @param raiseStatusList - Status type list of raises (Optional)
     * @return queryResult
     */
    @Override
    public List<Map<String, Object>> getListOfRaisesOnNomination(Long nominationId, Long fromPaxId, 
            List<String> parentStatusList, List<String> raiseStatusList, Long involvedPaxId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        String raisesQuery = LIST_OF_RAISES_QUERY;

        params.addValue(STATUS_LIST_PLACEHOLDER, parentStatusList);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        // Remove fromPaxId line if fromPaxId == null
        if (fromPaxId != null) {
            raisesQuery = raisesQuery.replace(SUBMITTER_PAX_PLACEHOLDER, "AND NOM.SUBMITTER_PAX_ID = " + fromPaxId);
        } else {
            raisesQuery = raisesQuery.replace(SUBMITTER_PAX_PLACEHOLDER, "");
        }

        if (raiseStatusList != null && !raiseStatusList.isEmpty()) {
            raisesQuery = raisesQuery.replace(RAISE_STATUS_LIST_PLACEHOLDER, 
                    "AND re.STATUS_TYPE_CODE IN (:RAISE_STATUS_LIST_PLACEHOLDER)");
            params.addValue(RAISE_STATUS_LIST_PLACEHOLDER, raiseStatusList);
        } else {
            raisesQuery = raisesQuery.replace(RAISE_STATUS_LIST_PLACEHOLDER, "");
        }
        
        if (involvedPaxId != null) {
            raisesQuery = raisesQuery.replace(RELATIONSHIP_QUERY_PLACEHOLDER, RELATIONSHIP_LEFT_JOIN_QUERY);
            raisesQuery = raisesQuery.replace(INVOLVED_PAX_PLACEHOLDER, INVOLVED_PAX_FILTER_QUERY);
            params.addValue(SUBMITTER_PAX_ID_PLACEHOLDER, involvedPaxId);
            params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, involvedPaxId);
            params.addValue(MANAGER_ID_PLACEHOLDER, involvedPaxId);
            
        } else {
            raisesQuery = raisesQuery.replace(RELATIONSHIP_QUERY_PLACEHOLDER, "");
            raisesQuery = raisesQuery.replace(INVOLVED_PAX_PLACEHOLDER, "");
        }

        raisesQuery = raisesQuery.replace(ORDER_BY_PLACEHOLDER, RAISES_ORDER_BY_CLAUSE);
        
        return namedParameterJdbcTemplate.query(raisesQuery, params, new CamelCaseMapRowMapper());
    }
    
    /**
     * Returns a paginated list of raises associated with given nomination id
     * @param nominationId - Nomination id of raises to get
     * @param fromPaxId - Pax id which submitted raise (Optional)
     * @param parentStatusList - Status type list of parent recognitions
     * @param raiseStatusList - Status type list of raises (Optional)
     * @return queryResult
     */
    @Override
    public List<Map<String, Object>> getListOfRaisesOnNomination(Integer pageNumber, Integer pageSize, Long nominationId, 
            Long fromPaxId, List<String> parentStatusList, List<String> raiseStatusList, Long involvedPaxId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        String raisesQuery = LIST_OF_RAISES_QUERY;

        params.addValue(STATUS_LIST_PLACEHOLDER, parentStatusList);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        // Remove fromPaxId line if fromPaxId == null
        if (fromPaxId != null) {
            raisesQuery = raisesQuery.replace(SUBMITTER_PAX_PLACEHOLDER, "AND NOM.SUBMITTER_PAX_ID = " + fromPaxId);
        } else {
            raisesQuery = raisesQuery.replace(SUBMITTER_PAX_PLACEHOLDER, "");
        }

        if (raiseStatusList != null && !raiseStatusList.isEmpty()) {
            raisesQuery = raisesQuery.replace(RAISE_STATUS_LIST_PLACEHOLDER, 
                    "AND re.STATUS_TYPE_CODE IN (:RAISE_STATUS_LIST_PLACEHOLDER)");
            params.addValue(RAISE_STATUS_LIST_PLACEHOLDER, raiseStatusList);
        } else {
            raisesQuery = raisesQuery.replace(RAISE_STATUS_LIST_PLACEHOLDER, "");
        }
        
        if (involvedPaxId != null) {
            raisesQuery = raisesQuery.replace(RELATIONSHIP_QUERY_PLACEHOLDER, RELATIONSHIP_LEFT_JOIN_QUERY);
            raisesQuery = raisesQuery.replace(INVOLVED_PAX_PLACEHOLDER, INVOLVED_PAX_FILTER_QUERY);
            params.addValue(SUBMITTER_PAX_ID_PLACEHOLDER, involvedPaxId);
            params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, involvedPaxId);
            params.addValue(MANAGER_ID_PLACEHOLDER, involvedPaxId);
            
        } else {
            raisesQuery = raisesQuery.replace(RELATIONSHIP_QUERY_PLACEHOLDER, "");
            raisesQuery = raisesQuery.replace(INVOLVED_PAX_PLACEHOLDER, "");
        }
        
        // Removing order by placeholder, it will be handled by getPageWithTotalRows method.
        raisesQuery = raisesQuery.replace(ORDER_BY_PLACEHOLDER, "");

        return getPageWithTotalRowsCTE(raisesQuery, params, new CamelCaseMapRowMapper(), RAISES_PAGINATION_ORDER_BY, pageNumber, pageSize);
    }

    @Override
    public List<Map<String, Object>> getListOfPaxOnActivity(Long paxId, Long activityId, boolean excludePrivate, 
            List<String> statusList) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String raiseRecipientsQuery = LIST_OF_PAX_ON_ACTIVITY_EXCLUDE_PRIVATE_QUERY;
        
        int applyFilter = 0;
        //If set to true, apply filter for recognition preferences.
        if (excludePrivate) {
            applyFilter = 1;
        }

        // Add activity id
        params.addValue(ACTIVITY_ID_PLACEHOLDER, activityId);
        //if excludPrivate flag is true, retrieve manager's direct reports participants
        params.addValue(MANAGER_ID_PLACEHOLDER, paxId);
        params.addValue(EXCLUDE_FLAG_PLACEHOLDER, applyFilter);
        
        // Add status filter
        if (!CollectionUtils.isEmpty(statusList)) {
            raiseRecipientsQuery =
                    raiseRecipientsQuery.replace(RECOGNITION_STATUS_QUERY_PLACEHOLDER, RECOGNITION_STATUS_FILTER_QUERY);
            params.addValue(STATUS_LIST_PLACEHOLDER, statusList);
        } else {
            raiseRecipientsQuery = 
                    raiseRecipientsQuery.replace(RECOGNITION_STATUS_QUERY_PLACEHOLDER, ProjectConstants.EMPTY_STRING);
        }

        return namedParameterJdbcTemplate.query(raiseRecipientsQuery, params, new CamelCaseMapRowMapper());
    }
    
    @Override
    public List<Map<String, Object>> getListOfPaxOnNomination(Long paxId, Long nominationId, boolean excludePrivate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        String raiseRecipientsQuery = LIST_OF_PAX_ON_NOMINATION_EXCLUDE_PRIVATE_QUERY;
        
        int applyFilter = 0;
        //If set to true, apply filter for recognition preferences.
        if (excludePrivate) {
            applyFilter = 1;
        }

        // Add activity id
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);
        //if excludPrivate flag is true, retrieve manager's direct reports participants
        params.addValue(MANAGER_ID_PLACEHOLDER, paxId);
        params.addValue(EXCLUDE_FLAG_PLACEHOLDER, applyFilter);

        return namedParameterJdbcTemplate.query(raiseRecipientsQuery, params, new CamelCaseMapRowMapper());
    }

    @Override
    public Long getRecognitionParentId(Long nominationId, Long toPaxId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = GET_RECOGNITION_PARENT_ID;
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);
        params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, toPaxId);

        List<Map<String, Object>> node = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());
        return node.size() == 0 ? null : (Long) node.get(0).get("id");
    }

    @Override
    public Boolean isAlreadyRaised(Long fromPaxId, Long toPaxId, Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = GET_RECEIVER_PAX_FROM_RAISE;
        params.addValue(SUBMITTER_PAX_PLACEHOLDER, fromPaxId);
        params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, toPaxId);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        List<Map<String, Object>> node = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());
        return node.size() != 0;
    }

    @Override
    public Boolean isValidRecognition(Long fromPaxId, Long toPaxId, Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = CHECK_VALIDITY_OF_RECOGNITION;
        params.addValue(SUBMITTER_PAX_PLACEHOLDER, fromPaxId);
        params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, toPaxId);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        List<Map<String, Object>> node = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());
        return node.size() != 0;
    }

    @Override
    public Long getPointsForRaise(Long paxId, Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = GET_POINTS_FOR_RAISE_RECEIVED_QUERY;
        params.addValue(RECEIVER_PAX_ID_PLACEHOLDER, paxId);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        List<Map<String, Object>> node = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());
        return node.size() == 0 ? null : ((Double) node.get(0).get("amount")).longValue();
    }

    @Override
    public Double getRaisePointsForManager(Long managerId, Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = GET_POINTS_FOR_MANAGER_RAISE_RECEIVED_QUERY;
        params.addValue(MANAGER_ID_PLACEHOLDER, managerId);
        params.addValue(NOMINATION_ID_PLACEHOLDER, nominationId);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());
        if (nodes == null || nodes.size() == 0) {
            return 0.0;
        }
        
        Double points = 0.0;
        for (Map<String, Object> node : nodes) {
            points += (Double) node.get("amount");
        }
        return points;
    }

}
