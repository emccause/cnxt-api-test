package com.maritz.culturenext.recognition.dao;

import java.util.List;
import java.util.Map;

public interface NominationDetailsDao {

    List<Map<String, Object>> getNominationCommonInfo(List<Long> nominationIds);
    
}
