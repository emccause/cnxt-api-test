package com.maritz.culturenext.recognition.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.TransactionHeader;

public interface TransactionDetailsDao {

    List<Map<String, Object>> getTransactionSearch(Long loggedPax, List<String> programId, List<String> awardCode
            , List<String> status, List<String> recipientPaxId, List<String> submitterPaxId, List<String> programType
            , Date startDate, Date endDate, Integer pageNumber, Integer pageSize);

    /**
     * Returns a list of all TRANSACTION_HEADER ids along with any associated raises that are APPROVED
     *
     * @param transactionIds - List of transaction header ids tied to recognitions about to be reversed.
     * @return List<TransactionHeader> - TransactionHeader records for the passed in list plus any raises, filtered down to APPROVED statuses only
     */
    List<TransactionHeader> getTransactionsToReverse(List<Long> transactionIds);

    /**
     * Reverses existing transactions by updating the STATUS_TYPE_CODE to REVERSED in these tables:
     * RECOGNITION and TRANSACTION_HEADER
     * NOMINATION and NEWSFEED_ITEM - only if all recipients are reversed
     * ALERT - alerts of any kind tied to the transaction. Will check if all recipients are reversed for manager alerts.
     * APPROVAL_HISTORY - create a record here saving the pax that performed the reversal
     *
     * The reversal comment will be applied to all transactions. It is saved as RECOGNITION.COMMENT
     * @param transactionIds - Comma separated list of transaction header ids to be reversed. This list includes raises
     * @param reversalComment - Reason for reversing these transactions
     * @param paxId - paxId of the person performing the reversal
     */
    void reverseTransactions(Collection<Long> transactionIds, String reversalComment, Long paxId);
}
