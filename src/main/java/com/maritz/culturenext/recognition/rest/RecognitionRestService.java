package com.maritz.culturenext.recognition.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping(NominationConstants.NOMINATION_URI_BASE)
public class RecognitionRestService {
    
    public static final String RECOGNITIONS_BY_NOMINATION_ID_PATH = "{" + NOMINATION_ID_REST_PARAM + "}" + "/recognitions";

    @Inject private ApprovalService approvalService;
    @Inject private RecognitionService recognitionService;
    @Inject private Security security;

    //rest/nominations/~/recognitions
    @Deprecated
    @RequestMapping(method = RequestMethod.GET, value = RECOGNITIONS_BY_NOMINATION_ID_PATH, headers = CURRENT_VERSION_HEADER)
    @ApiOperation(value="Returns the recognition details for the given nominationId", 
        notes="This method allows a user to fetch recognition details for a given nominationId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of recognition details"),
            @ApiResponse(code = 401, message = "Requesting user not authenticated")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getRecognitionsForNominationIdCurrent(
            @ApiParam(value = "Id of nomination ") @PathVariable(NOMINATION_ID_REST_PARAM) Long nominationId,
            @ApiParam(value = "ExcludePrivate, if set to true, private recipients will not be included in the list")
            @RequestParam(value = EXCLUDE_PRIVATE_REST_PARAM, required = false) Boolean excludePrivate,
            @ApiParam(value = "The Manager Pax Id is only checked when the exclude private flag is set to false, "
                    + "in which case we return all private records") 
            @RequestParam(value = MANAGER_PAX_ID_REST_PARAM, required = false) Long managerPaxId,
            @ApiParam(value = "The comma-delimited list of statuses, "
                    + "the recognition records to be returned for these statuses")
            @RequestParam(value = STATUS_REST_PARAM, required = false) String status,
            @ApiParam(value = "The number of people per page used to Paginate the request") 
            @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @ApiParam(value = "Page of the people raise feed used to Paginate the request")
            @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page
    ){
        Long paxId = security.getPaxId();
        if(paxId == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            return new ResponseEntity<Object>(recognitionService.getRecognitionDetailsPagination(nominationId, 
                    excludePrivate, managerPaxId, status, page, size, null), HttpStatus.OK);
        } catch (ErrorMessageException ex) {
            for (Iterator<ErrorMessage> iterator = ex.getErrorMessages().iterator(); iterator.hasNext(); ) {
                ErrorMessage errorMessage = iterator.next();
                if (errorMessage.getCode() != null && 
                        NominationConstants.ERROR_INVALID_MANAGER_PAX_ID.equals(errorMessage.getCode())) {
                    return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.FORBIDDEN);
                }
            }
            return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.BAD_REQUEST);
        }

    }

    //rest/nominations/~/recognitions
    @RequestMapping(method = RequestMethod.GET, value = RECOGNITIONS_BY_NOMINATION_ID_PATH, headers = FUTURE_VERSION_HEADER)
    @ApiOperation(value="Returns the recognition details for the given nominationId", 
        notes="This method allows a user to fetch recognition details for a given nominationId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of recognition details")
    })
    @Permission("PUBLIC")
    public PaginatedResponseObject<RecognitionDetailsDTO> getRecognitionsForNominationIdFuture(
            @ApiParam(value = "Id of nomination ") @PathVariable(NOMINATION_ID_REST_PARAM) String nominationIdString,
            @ApiParam(value = "Whether we should override recipient recognition preferences when populating recipients")
                @RequestParam(value = OVERRIDE_VISIBILITY_REST_PARAM, required = false) Boolean overrideVisibility,
            @ApiParam(value = "Whether we should show all points recieved (validated against user roles)")
                @RequestParam(value = SHOW_POINTS_REST_PARAM, required = false) Boolean showPoints,
            @ApiParam(value = "Comma separated filer list of statuses to limit results to")
                @RequestParam(value = STATUS_REST_PARAM, required = false) String commaSeparatedStatusTypeCodeList,
            @ApiParam(value = "ID of notification to pull back data for - only used if it is for the nomination provided")
                @RequestParam(value = NOTIFICATION_ID_REST_PARAM, required = false) String notificationIdString,
            @ApiParam(value="page of activity feed used to paginate request.",required = false)
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
            @ApiParam(value="The number of records per page.",required = false)
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) throws Throwable {
        
        Long nominationId = security.getId(nominationIdString, null);
        if(nominationId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        Long notificationId = security.getId(notificationIdString, null);

        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> pathParamMap = new HashMap<>();
        pathParamMap.put(NOMINATION_ID_REST_PARAM, nominationIdString);
        
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(OVERRIDE_VISIBILITY_REST_PARAM, overrideVisibility);
        requestParamMap.put(SHOW_POINTS_REST_PARAM, showPoints);
        requestParamMap.put(STATUS_REST_PARAM, commaSeparatedStatusTypeCodeList);
        requestParamMap.put(NOTIFICATION_ID_REST_PARAM, notificationIdString);
        
        PaginationRequestDetails requestDetails = new PaginationRequestDetails().setPathParamMap(pathParamMap)
                .setRequestPath(NominationConstants.NOMINATION_URI_BASE + "/" + RECOGNITIONS_BY_NOMINATION_ID_PATH)
                .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);

        
        return recognitionService.getRecognitionDetails(
                requestDetails, nominationId, overrideVisibility, showPoints, security.getPaxId(),
                commaSeparatedStatusTypeCodeList, notificationId, pageNumber, pageSize
            );
    }
    
    //rest/nominations/~/recognitions
    @RequestMapping(value = RECOGNITIONS_BY_NOMINATION_ID_PATH, method = RequestMethod.PUT)
    @ApiOperation(value="Handles approval and / or rejection of a list of recognition objects")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful approval / rejection of recognitions"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public ResponseEntity<List<RecognitionDetailsDTO>> approveOrRejectRecognitions(
            @ApiParam(value = "Id of nomination", required = true) 
            @PathVariable(NOMINATION_ID_REST_PARAM) String nominationIdString,
            @ApiParam(value = "List of recognition details", required = true) 
            @RequestBody List<RecognitionDetailsDTO> recognitionDetailList
            ) {
        
        Long nominationId = security.getId(nominationIdString, null);
        if (nominationId == null) {
            return new ResponseEntity<List<RecognitionDetailsDTO>>(HttpStatus.NOT_FOUND);
        }
        
        return new ResponseEntity<List<RecognitionDetailsDTO>>(approvalService
                .approveOrRejectRecognitions(security.getPaxId(), nominationId, recognitionDetailList),HttpStatus.OK);
    }
}
