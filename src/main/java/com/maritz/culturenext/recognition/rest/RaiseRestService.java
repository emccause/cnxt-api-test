package com.maritz.culturenext.recognition.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.services.RaiseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/recognition", description = "all calls dealing with raises")
@RequestMapping("nominations")
public class RaiseRestService {

    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private RaiseService raiseService;

    //rest/nominations/~/raises
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{" + NOMINATION_ID_REST_PARAM + "}/raises"
        , method = RequestMethod.GET, headers = {ProjectConstants.VERSION_REQUEST_HEADER + "!=future"})
    @ApiOperation(value = "Returns the list of Raises for a specific Activity")
    @Permission("PUBLIC")
    public List<RaiseDTO> getRaisesForActivity(
        @ApiParam(value = "nomination id of raises you want") 
            @PathVariable(NOMINATION_ID_REST_PARAM) String nominationIdString,
        @ApiParam(value = "The number of records per page used to Paginate the request") 
            @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
        @ApiParam(value = "Page of the Raise raise feed used to Paginate the request") 
            @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page,
        @ApiParam(value = "The list of possible statuses that you want returned") 
            @RequestParam(value = STATUS_REST_PARAM, required = false) List<String> statusList,
        @ApiParam(value = "If used will return the list of raises initiated by a given user") 
            @RequestParam(value = FROM_PAX_ID_REST_PARAM, required = false) Long fromPaxId,
        @ApiParam(value = "If set, return only raises for which the involvedPaxId is "
                + "either the raise giver, receiver, or manager of the receiver") 
            @RequestParam(value = INVOLVED_PAX_ID_REST_PARAM, required = false) Long involvedPaxId
    ) throws NoHandlerFoundException {
        Long nominationId = convertPathVariablesUtil.getId(nominationIdString);
        
        return raiseService.getRaisesForNominationCurrent(nominationId, size, page,
                statusList, fromPaxId, involvedPaxId);
    }
    
    //rest/nominations/~/raises
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{" + NOMINATION_ID_REST_PARAM + "}/raises"
        , method = RequestMethod.GET, headers = {ProjectConstants.VERSION_REQUEST_HEADER + "=future"})
    @ApiOperation(value = "Returns the list of Raises for a specific Activity")
    @Permission("PUBLIC")
    public PaginatedResponseObject<RaiseDTO> getRaisesForActivityPagination(
        @ApiParam(value = "nomination id of raises you want") 
            @PathVariable(NOMINATION_ID_REST_PARAM) String nominationIdString,
        @ApiParam(value = "The list of possible statuses that you want returned") 
            @RequestParam(value = STATUS_REST_PARAM, required = false) List<String> statusList,
        @ApiParam(value = "If used will return the list of raises initiated by a given user") 
            @RequestParam(value = FROM_PAX_ID_REST_PARAM, required = false) Long fromPaxId,
        @ApiParam(value = "If set, return only raises for which the involvedPaxId is "
                + "either the raise giver, receiver, or manager of the receiver") 
            @RequestParam(value = INVOLVED_PAX_ID_REST_PARAM, required = false) Long involvedPaxId,
        @ApiParam(value="Page of the Raise raise feed used to Paginate the request", required = false) 
            @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
        @ApiParam(value="The number of records per page used to Paginate the request", required = false) 
            @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) throws NoHandlerFoundException {
        Long nominationId = convertPathVariablesUtil.getId(nominationIdString);
        
        return raiseService.getRaisesForNomination(nominationId, pageSize, pageNumber,
                statusList, fromPaxId, involvedPaxId);
    }

    //rest/nominations/~/raises
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{nominationId}/raises", method = RequestMethod.POST)
    @ApiOperation(value = "Endpoint for the action of creating a raise on a recognition."
            + " Returns the saved raise records")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully created Raise record(s)", response = RaiseDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<RaiseDTO> createRaises(
            @PathVariable(NOMINATION_ID_REST_PARAM) String nominationIdString,
            @RequestBody List<RaiseDTO> raiseDTOs) throws Throwable {
        
        Long nominationId = convertPathVariablesUtil.getId(nominationIdString);
        
        return raiseService.createRaises(nominationId, raiseDTOs);
    }

    //rest/nominations/~/raises
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{nominationId}/raises", method = RequestMethod.PUT)
    @ApiOperation(value = "Endpoint for the action of updating a raise on a recognition. "
            + "Returns the saved raise records")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully updated Raise record(s)", response = RaiseDTO.class),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public List<RaiseDTO> updateRaises(
            @RequestBody List<RaiseDTO> raiseDTOs
    ) throws Throwable {

        return raiseService.updateRaises(raiseDTOs);
    }

}
