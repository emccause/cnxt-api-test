package com.maritz.culturenext.recognition.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.PAX_ID_REST_PARAM;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RecognitionStatsDTO;
import com.maritz.culturenext.recognition.services.NominationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping("participants")
public class NominationRestService {

    @Inject private NominationService nominationService;
    @Inject private Security security;

    //rest/participants/~/nominations
    @PostMapping(path = "{paxId}/nominations")
    @ApiOperation(value="Creates a nomination", notes="This method allows a user to create a nomination")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful creation of nomination", 
    response = NominationRequestDTO.class), @ApiResponse(code = 404, message = "Participant not found") })
    @Permission("PUBLIC")
    public NominationDetailsDTO createNomination(
            @ApiParam(value = "PaxId of the requesting user")@PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @RequestBody NominationRequestDTO nominationDto
        ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        return nominationService.createStandardNomination(paxId, nominationDto, Boolean.TRUE);
    }
    
    //rest/participants/~/nominations/~
    @RequestMapping(method = RequestMethod.GET, value = "{paxId}/nominations/{nominationId}")
    @ApiOperation(value="Returns the details of the specified nomination")
    @Permission("PUBLIC")
    public NominationDetailsDTO getNominationById(
        @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM)  String paxIdString,
        @ApiParam(value = "Nomination id of the nomination you want to retrieve")
        @PathVariable("nominationId")  String nominationIdString
    ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return nominationService.getNominationById(nominationIdString, paxId);
    }
    
    //rest/participants/~/nominations
    @RequestMapping(method = RequestMethod.GET, value = "{paxId}/nominations")
    @ApiOperation(value="Returns nomination details in a list. Includes award code information.")
    @Permission("PUBLIC")
    public List<NominationDetailsDTO> getNominationDetails(
        @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM)  String paxIdString,
        @ApiParam(value = "Comma separated list of Nomination ids for which to fetch data") 
        @RequestParam(value="nominationIds", required=false) String nominationIdsString
    ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return nominationService.getNominationDetails(nominationIdsString, paxId);
    }

    //rest/participants/~/nominations/stats
    @RequestMapping(method = RequestMethod.GET, value = "{paxId}/nominations/stats", 
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returns the users recognitions for the given paxId")
    @Permission("PUBLIC")
    public @ResponseBody RecognitionStatsDTO getRecognitionGivenReceived(
            @ApiParam(value ="Participant Id of the user you want the recognitions of")
            @PathVariable(PAX_ID_REST_PARAM)  String paxIdString) {
        Long paxId = security.getPaxId(paxIdString);
        return nominationService.getRecognitionStats(paxId);
    }
    
}
