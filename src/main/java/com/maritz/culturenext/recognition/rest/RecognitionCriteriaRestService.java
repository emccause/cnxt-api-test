package com.maritz.culturenext.recognition.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.recognition.dto.RecognitionCriteriaDTO;
import com.maritz.culturenext.recognition.services.RecognitionCriteriaService;
import com.maritz.culturenext.util.ConvertPathVariablesUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping("recognition-criteria")
public class RecognitionCriteriaRestService {
    
    @Inject private ConvertPathVariablesUtil convertPathVariablesUtil;
    @Inject private RecognitionCriteriaService recognitionCriteriaService;

    //rest/recognition-criteria
    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Returns list of all custom values (Recognition criteria) configured for a project. Can be filtered by ID.")
    @ApiResponse(code = 200, message = "Successful retrieval of recognition criteria values", 
    response = RecognitionCriteriaDTO.class, responseContainer = "List")
    @Permission("PUBLIC")
    public List<RecognitionCriteriaDTO> getRecognitionCriteriaByStatus(
            @ApiParam(value = "values for status are 'ACTIVE', 'INACTIVE'<br> default returns 'ACTIVE'")
            @RequestParam(value = STATUS_REST_PARAM, required = false) String status,
            @ApiParam(value = "Language Code for translation of recognition criteria")
            @RequestParam(value = LANGUAGE_CODE_REST_PARAM, required = false) String languageCode,
            @ApiParam(value = "comma separated list of ids to filter on")
            @RequestParam(value = IDS_REST_PARAM, required = false) String idList) {
        return recognitionCriteriaService.getRecognitionCriteria(status, languageCode, idList);
    }

    //rest/recognition-criteria
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.POST) 
    @ApiOperation(value = "Add/configure new values (Recognition Criteria) for a project.")
    @Permission("PUBLIC")
    public List<RecognitionCriteriaDTO> insertRecognitionCriteria(
            @ApiParam(value="Body containing the details of the new criteria")
            @RequestBody List<RecognitionCriteriaDTO> recognitionCriteriaDTOList) {
        return recognitionCriteriaService.insertRecognitionCriteria(recognitionCriteriaDTOList);
    }

    //rest/recognition-criteria/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(value = "{id}", method = RequestMethod.PUT) 
    @ApiOperation(value = "Update custom value (Recognition criteria) for a project.")
    @Permission("PUBLIC")
    public RecognitionCriteriaDTO updateRecognitionCriteria(
            @ApiParam(value = "Id of the custom value you want to update"
            )@PathVariable(ID_REST_PARAM) String recognitionCriteriaIdString, 
            @ApiParam(value = "The body containing the updated custom value")
            @RequestBody RecognitionCriteriaDTO recognitionCriteriaDTO) throws NoHandlerFoundException {
           
        Long recognitionCriteriaId = convertPathVariablesUtil.getId(recognitionCriteriaIdString);
        
        return recognitionCriteriaService.updateRecognitionCriteria(recognitionCriteriaDTO,    recognitionCriteriaId);
    }
}
