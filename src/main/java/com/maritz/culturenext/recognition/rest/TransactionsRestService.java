package com.maritz.culturenext.recognition.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.recognition.dto.TransactionDetailsDTO;
import com.maritz.culturenext.recognition.dto.TransactionReversalDTO;
import com.maritz.culturenext.recognition.services.TransactionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping("transactions")
public class TransactionsRestService {
    
    public static final String TRANSACTION_SEARCH_REST_ENDPOINT = "/rest/transactions/search";
    
    @Inject private TransactionService transactionsService;
    
    //rest/transactions/search
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.GET, value = "search")
    @ApiOperation(value = "search data for transactions")
    @Permission("PUBLIC")
    public @ResponseBody PaginatedResponseObject<TransactionDetailsDTO> getTransactionsSearch(
            @ApiParam(value="ids of program to filter by", required = false) 
                @RequestParam(value = PROGRAM_ID_REST_PARAM, required = false) 
                    String programIdCommaSeparated, 
            @ApiParam(value="Start date - returns data after this date",required = false) 
                @RequestParam(value = START_DATE_REST_PARAM, required = false) 
                    String startDate, 
            @ApiParam(value="End date - returns data before this date",required = false) 
                @RequestParam(value = END_DATE_REST_PARAM, required = false) 
                    String endDate, 
            @ApiParam(value="Award code - will find award codes containing this value", required = false) 
                @RequestParam(value = AWARD_CODE_REST_PARAM, required = false) 
                    String awardCodeCommaSeparated, 
            @ApiParam(value="status - award code nomination status to filter by", required = false) 
                @RequestParam(value = STATUS_REST_PARAM, required = false) 
                    String statusCommaSeparated, 
            @ApiParam(value="ids of pax who claimed award code nomination", required = false) 
                @RequestParam(value = RECIPIENT_PAX_REST_PARAM, required = false) 
                    String recipientPaxIdCommaSeparated, 
            @ApiParam(value="ids of pax who issued award code nomination", required = false) 
                @RequestParam(value = ISSUED_BY_PAX_REST_PARAM, required = false) 
                    String issuedByPaxIdCommaSeparated,
            @ApiParam(value="Types of program to search on", required = false) 
                @RequestParam(value = PROGRAM_TYPE_REST_PARAM, required = false) 
                    String programTypeCommaSeparated,
            @ApiParam(value="page number to fetch", required = false) 
                @RequestParam(value = PAGE_NUMBER_REST_PARAM, required = false) 
                    Integer pageNumber, 
            @ApiParam(value="size of page to fetch", required = false) 
                @RequestParam(value = PAGE_SIZE_REST_PARAM, required = false) 
                    Integer pageSize
            ) {
        
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        // Handle other parameters
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(RestParameterConstants.PROGRAM_ID_REST_PARAM, programIdCommaSeparated);
        requestParamMap.put(RestParameterConstants.START_DATE_REST_PARAM, startDate);
        requestParamMap.put(RestParameterConstants.END_DATE_REST_PARAM, endDate);
        requestParamMap.put(RestParameterConstants.AWARD_CODE_REST_PARAM, awardCodeCommaSeparated);
        requestParamMap.put(RestParameterConstants.STATUS_REST_PARAM, statusCommaSeparated);
        requestParamMap.put(RestParameterConstants.RECIPIENT_PAX_REST_PARAM, recipientPaxIdCommaSeparated);
        requestParamMap.put(RestParameterConstants.ISSUED_BY_PAX_REST_PARAM, issuedByPaxIdCommaSeparated);
        requestParamMap.put(RestParameterConstants.PROGRAM_TYPE_REST_PARAM, programTypeCommaSeparated);
        
        PaginationRequestDetails requestDetails =
                new PaginationRequestDetails().setRequestPath(TRANSACTION_SEARCH_REST_ENDPOINT)
                    .setRequestParamMap(requestParamMap).setPageNumber(pageNumber).setPageSize(pageSize);
        
        return transactionsService.getTransactionsSearch(requestDetails, programIdCommaSeparated, awardCodeCommaSeparated
                        , statusCommaSeparated, recipientPaxIdCommaSeparated, issuedByPaxIdCommaSeparated
                        , programTypeCommaSeparated, startDate, endDate, pageNumber, pageSize);
    }
    
    //rest/transactions
    @PreAuthorize("@security.hasRole('" + PermissionConstants.ADMIN_ROLE_NAME + "') and !@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.PUT)
    @ApiOperation(value = "Reverse the transactions specified along with any raises tied to those transactions")
    @Permission("PUBLIC")
    public void reverseTransactions(@RequestBody TransactionReversalDTO reversals) {        
        transactionsService.reverseTransactions(reversals);
    }

}
