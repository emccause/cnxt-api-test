package com.maritz.culturenext.recognition.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.recognition.dto.EngagementScore;
import com.maritz.culturenext.recognition.services.EngagementScoreService;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping("participants")
public class EngagementScoreRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private EngagementScoreService engagementScoreService;
    @Inject private Security security;    

    //rest/participants/~/recognition/engagement_score
    @PreAuthorize("!@security.isImpersonated() and @permissionUtil.hasPermission('" + PermissionConstants.ENGAGEMENT_SCORE_TYPE + "')")
    @RequestMapping(value = "{paxId}/recognition/engagement_score", method = RequestMethod.GET)
    @ApiOperation(value ="Returns basic profile information for the logged in user")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful retrieval of recognition criteria stats",
    response = EngagementScore.class), @ApiResponse(code = 403, message = "Forbidden") })
    @Permission("PUBLIC")
    public EngagementScore getEngagementScoreByPaxId(
            @ApiParam(value = "Particiapnt Id of the user you want") 
                @PathVariable(PAX_ID_REST_PARAM) String paxIdString
            ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null){
            throw new NoHandlerFoundException(null, null, null);
        }
        
        return engagementScoreService.getEngagementScoreByPaxId(paxId);        
    }
}
