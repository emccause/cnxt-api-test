package com.maritz.culturenext.recognition.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.FILE_REST_PARAM;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.recognition.constants.RecognitionConstants;
import com.maritz.culturenext.recognition.dto.RecognitionBulkResponseDTO;
import com.maritz.culturenext.recognition.services.RecognitionBulkUploadService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/recognitions", description = "all calls dealing with advanced recognitions bulk upload")
@RequestMapping(RecognitionConstants.RECOGNITIONS_URI_BASE)
public class RecognitionBulkRestService {
    public static final String RECOGNITIONS_BULK_PATH = "/bulk";
    public static final String BULK_UPLOAD_HISTORY_PATH = "/bulk/history";
    
    @Inject private RecognitionBulkUploadService recognitionsBulkUploadService;
    @Inject private Security security;

    //rest/recognitions/bulk
    @RequestMapping(method = RequestMethod.POST, value = RECOGNITIONS_BULK_PATH)
    @ApiOperation(value="Returns the recognition details for the given nominationId", 
        notes="This method allows a user to fetch recognition details for a given nominationId")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of recognition details")
    })
    @Permission("PUBLIC")
    public RecognitionBulkResponseDTO recognitionsBulkUpload(
            @ApiParam(value = "Target file the user wishes to upload") 
            @RequestParam(value = FILE_REST_PARAM) MultipartFile fileUpload)
            throws Exception {
        if (fileUpload == null
                || !ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.contains(fileUpload.getContentType().toLowerCase())) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return recognitionsBulkUploadService.recognitionsBulkUpload(security.getPaxId(),fileUpload);
    }
    
    //rest/recognitions/bulk/history
    @RequestMapping(value = BULK_UPLOAD_HISTORY_PATH, method = RequestMethod.GET)
    @ApiOperation(value = "Get a paginated list of advanced recognition bulk uploads")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned bulk upload history")
    })
    @Permission("PUBLIC")
    public PaginatedResponseObject<RecognitionBulkResponseDTO> getBulkUploadHistory(
        @ApiParam(value = "list of statuses to search on.", required = false)
            @RequestParam(value = RestParameterConstants.STATUS_REST_PARAM, required = false) String statusList,
        @ApiParam(value = "page of bulk upload history used to paginate request.", required = false)
            @RequestParam(value = RestParameterConstants.PAGE_NUMBER_REST_PARAM, required = false) Integer pageNumber, 
        @ApiParam(value = "The number of records per page.", required = false)
            @RequestParam(value = RestParameterConstants.PAGE_SIZE_REST_PARAM, required = false) Integer pageSize
    ) {
    
        if (pageNumber == null || pageNumber < 1) {
            pageNumber = ProjectConstants.DEFAULT_PAGE_NUMBER;
        }
        
        if (pageSize == null || pageSize < 1) {
            if (pageNumber > 1) {
                pageSize = ProjectConstants.DEFAULT_PAGE_SIZE; // All results doesn't make sense for multiple pages
            }
            else {
                pageSize = ProjectConstants.MAX_PAGE_SIZE; // Godspeed
            }
        }
        
        Map<String, Object> requestParamMap = new HashMap<>();
        requestParamMap.put(RestParameterConstants.STATUS_REST_PARAM, statusList);
    
        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(ProjectConstants.REPORTS_URI_BASE + BULK_UPLOAD_HISTORY_PATH)
                .setRequestParamMap(requestParamMap)
                .setPageNumber(pageNumber)
                .setPageSize(pageSize);
        
        return recognitionsBulkUploadService.getBulkUploadHistory(requestDetails, statusList, pageNumber, pageSize);
    }
}
