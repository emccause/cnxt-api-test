package com.maritz.culturenext.recognition.awardcode.dao.impl;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.recognition.awardcode.dao.AwardCodeGenerationDao;

@Repository
public class AwardCodeGenerationDaoImpl extends AbstractDaoImpl implements AwardCodeGenerationDao {
    
    private static final String BATCH_ID_PLACEHOLDER = "BATCH_ID";
    
    private static final String QUERY = "EXEC component.UP_GENERATE_AWARD_CODES :" + BATCH_ID_PLACEHOLDER;

    @Override
    public void generateAwardCodeData(Long batchId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(BATCH_ID_PLACEHOLDER, batchId);
        
        namedParameterJdbcTemplate.update(QUERY, params);
    }
}
