package com.maritz.culturenext.recognition.awardcode.services;

import java.util.List;

import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeResponseDTO;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;

public interface AwardCodeService {

    /**
     * Generate award code certificates. This will create one nomination/recognition set for each award code certificate
     * 
     * @param submitterPaxId
     * @param awardCodeRequestObject 
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/42663970/Recognition+-+Generate+Award+Code
     */
    AwardCodeResponseDTO generateAwardCodes(Long submitterPaxId, AwardCodeRequestDTO awardCodeRequestObject);
    
    
    /**
     * Redeem an award code. This will set the RECIPIENT_PAX_ID and update the status to APPROVED
     * 
     * @param awardCode
     * @param awardCodeDtoList
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/42663976/Recognition+-+Redeem+Award+Code
     */
    NominationDetailsDTO redeemAwardCode(String awardCode, AwardCodeDTO awardCodeDtoList, Long paxId);
    
    /**
     * Update the status of multiple award codes to "revoke" them.
     * This just means updating them to REJECTED status in the database
     * 
     * @param awardCodeDtoList
     * @param paxId
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/47251514/Revoke+Award+Code
     */
    List<NominationDetailsDTO> revokeAwardCode(List<AwardCodeDTO> awardCodeDtoList, Long paxId);
}