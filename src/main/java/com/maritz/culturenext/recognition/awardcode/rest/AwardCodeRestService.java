package com.maritz.culturenext.recognition.awardcode.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeResponseDTO;
import com.maritz.culturenext.recognition.awardcode.services.AwardCodeService;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;

@RestController
@Api(value = "/recognition", description = "all calls dealing with recognitions")
@RequestMapping("participants")
public class AwardCodeRestService {

    @Inject private AwardCodeService awardCodeService;
    @Inject private Security security;

    //rest/participants/~/nominations/award-code
    @RequestMapping(method = RequestMethod.POST, value = "{paxId}/nominations/award-code")
    @ApiOperation(value="Creates a nomination", notes="This method allows a user to create an award code batch")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successful creation of award codes", 
    response = NominationRequestDTO.class), @ApiResponse(code = 404, message = "Participant not found") })
    @Permission("PUBLIC")
    public AwardCodeResponseDTO createAwardCodeBatch(
            @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @RequestBody AwardCodeRequestDTO awardCodeRequestObject
        ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        return awardCodeService.generateAwardCodes(paxId, awardCodeRequestObject);
    }
    
    //rest/participants/~/nominations/award-codes/~
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(method = RequestMethod.PATCH, value = "{paxId}/nominations/award-codes/{awardCode}")
    @ApiOperation(value = "Update recognition for redeeming award code")
    @Permission("PUBLIC")
    public @ResponseBody NominationDetailsDTO reedemAwardCode(
            @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM)  String paxIdString,
            @ApiParam(value = "Award code to be reedemed")@PathVariable(AWARD_CODE_REST_PARAM)  String pathAwardCode,
            @RequestBody AwardCodeDTO awardCodeDto ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        if(paxId == null || pathAwardCode == null ){
            throw new NoHandlerFoundException(null, null, null);
        }

        return awardCodeService.redeemAwardCode(pathAwardCode, awardCodeDto, paxId);
    }
    
    //rest/participants/~/nominations/award-codes
    @PreAuthorize("!@security.isImpersonated()")
    @RequestMapping(method = { RequestMethod.PUT, RequestMethod.PATCH }, value = "{paxId}/nominations/award-codes")
    @ApiOperation(value = "Update recognition for revoke award code")
    @Permission("PUBLIC")
    public @ResponseBody
    List<NominationDetailsDTO> revokeAwardCode(
            @ApiParam(value = "PaxId of the requesting user") @PathVariable(PAX_ID_REST_PARAM)  String paxIdString,
            @RequestBody List<AwardCodeDTO> awardCodeDtoList ) throws Throwable {
        
        Long paxId = security.getPaxId(paxIdString);
        //security.
        if(paxId == null || awardCodeDtoList == null){
            throw new NoHandlerFoundException(null, null, null);
        }

        return awardCodeService.revokeAwardCode(awardCodeDtoList, paxId);
    }
    
}