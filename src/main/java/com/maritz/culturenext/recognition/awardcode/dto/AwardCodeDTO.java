package com.maritz.culturenext.recognition.awardcode.dto;

public class AwardCodeDTO {

    private String awardCode;

    private String status;

    public String getAwardCode() {
        return awardCode;
    }

    public void setAwardCode(String awardCode) {
        this.awardCode = awardCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}