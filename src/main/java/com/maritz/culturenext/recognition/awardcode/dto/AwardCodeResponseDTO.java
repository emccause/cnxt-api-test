package com.maritz.culturenext.recognition.awardcode.dto;

import java.util.List;

public class AwardCodeResponseDTO {
    
    private List<String> awardCodes;

    public List<String> getAwardCodes() {
        return awardCodes;
    }

    public AwardCodeResponseDTO setAwardCodes(List<String> awardCodes) {
        this.awardCodes = awardCodes;
        return this;
    }

}
