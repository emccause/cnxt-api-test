package com.maritz.culturenext.recognition.awardcode.dto;

public class AwardCodeInfoDTO {
    
    private Integer quantity;
    private Long certId;
    private Boolean giveAnonymous;
    
    public Integer getQuantity() {
        return quantity;
    }
    
    public AwardCodeInfoDTO setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }
    
    public Long getCertId() {
        return certId;
    }
    
    public AwardCodeInfoDTO setCertId(Long certId) {
        this.certId = certId;
        return this;
    }

    public Boolean getGiveAnonymous() {
        return giveAnonymous;
    }

    public AwardCodeInfoDTO setGiveAnonymous(Boolean giveAnonymous) {
        this.giveAnonymous = giveAnonymous;
        return this;
    }

}
