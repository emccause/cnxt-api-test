package com.maritz.culturenext.recognition.awardcode.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalProcess;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.PaxGroup;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.RecognitionMisc;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.repository.BatchEventRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.util.StatusTypeUtil;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.services.NewsfeedService;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.recognition.awardcode.dao.AwardCodeGenerationDao;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeResponseDTO;
import com.maritz.culturenext.recognition.awardcode.services.AwardCodeService;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;
import com.maritz.notification.ecard.ECardUtils;
import com.maritz.culturenext.alert.util.AlertUtil;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class AwardCodeServiceImpl implements AwardCodeService {
    
    final Logger logger = LoggerFactory.getLogger(AwardCodeServiceImpl.class.getName());
    
    @Inject private AlertUtil alertUtil;
    @Inject private ApprovalService approvalService;
    @Inject private AwardCodeGenerationDao awardCodeGenerationDao;
    @Inject @Named("batchService") private BatchService batchService;
    @Inject private BatchEventRepository batchEventRepository;
    @Inject private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Inject private ConcentrixDao<ApprovalProcess> approvalProcessDao;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;
    @Inject private ConcentrixDao<Discretionary> discretionaryDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<PaxGroup> paxGroupDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<RecognitionMisc> recognitionMiscDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private NewsfeedService newsfeedService;
    @Inject private NominationService nominationService;
    @Inject private ProgramEligibilityQueryDao programEligibilityQueryDao;
    @Inject private Security security;
    @Inject private ConcentrixDao<BudgetMisc> budgetMiscDao;
    @Inject private TransactionHeaderMiscDao transactionHeaderMiscDao;

    private static final String ADMIN_ROLE = "ADMIN";    
    
    /**
     * Generates award codes based on provided data.
     * 
     * @param submitterPaxId id of the pax who is submitting the award codes.
     * @param AwardCodeRequestDTO request containing data required
     * @return AwardCodeResponseDTO DTO containing list of award codes generated
     */
    @Override
    public AwardCodeResponseDTO generateAwardCodes(Long submitterPaxId, AwardCodeRequestDTO awardCodeRequestObject) {
        
        awardCodeRequestObject.getReceivers().get(0).setOriginalAmount(awardCodeRequestObject.getReceivers().get(0).getAwardAmount());
        
        ErrorMessageException.throwIfHasErrors(nominationService.validateNomination(submitterPaxId,
                awardCodeRequestObject, null, null, NominationConstants.AWARD_CODE_RECOGNITION_PRINT_VALIDATION_TYPE, Boolean.FALSE));
        
        Batch batch = batchService.createBatch(NominationConstants.AWARD_CODE_RECOGNITION_TYPE, 
                StatusTypeCode.COMPLETE.toString());
        StringBuilder awardCodeStringBuilder = new StringBuilder();
        int loopCount = 0;
        List<String> awardCodeList = new ArrayList<>();
        while (loopCount < awardCodeRequestObject.getAwardCode().getQuantity()) {
            String awardCode = batch.getBatchId() + "-" + ECardUtils.generateCertificateNumber(new Long(loopCount));
            awardCodeStringBuilder.append(awardCode);
            awardCodeList.add(awardCode);
            
            loopCount ++;
            // don't append a new row if we're done
            if (!awardCodeRequestObject.getAwardCode().getQuantity().equals(loopCount)) { 
                awardCodeStringBuilder.append(",");
            }
        }
        
        List<BatchEvent> batchEventList = new ArrayList<>();
        
        // count
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_COUNT.getCode(),
                        awardCodeRequestObject.getAwardCode().getQuantity().toString()
                        )
                );
        
        // submitter
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_SUBMITTER_PAX_ID.getCode(),
                        submitterPaxId.toString()
                        )
                );
        
        // budget
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_BUDGET_ID.getCode(),
                        awardCodeRequestObject.getBudgetId().toString()
                        )
                );
        
        // program
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_PROGRAM_ID.getCode(),
                        awardCodeRequestObject.getProgramId().toString()
                        )
                );
        
        // award amount
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_AMOUNT.getCode(),
                        awardCodeRequestObject.getReceivers().get(0).getAwardAmount().toString()
                        )
                );
        
        // award tier id
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_AWARD_TIER_ID.getCode(),
                        awardCodeRequestObject.getReceivers().get(0).getAwardTierId().toString()
                        )
                );
        
        // recognition criteria
        StringBuilder criteriaBuilder = new StringBuilder();
        for (Long criteriaId : awardCodeRequestObject.getRecognitionCriteriaIds()) {
            criteriaBuilder.append(criteriaId.toString());
            criteriaBuilder.append(ProjectConstants.COMMA_DELIM);
        }
        criteriaBuilder.setLength(criteriaBuilder.length() - 1); // strip off last comma
        
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_CRITERIA_IDS.getCode(),
                        criteriaBuilder.toString()
                        )
                );
        
        // headline
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_HEADLINE.getCode(),
                        awardCodeRequestObject.getHeadline()
                        )
                );
        
        // comment
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_COMMENT.getCode(),
                        awardCodeRequestObject.getComment()
                        )
                );
        
        // cert id
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_CERT_ID.getCode(),
                        awardCodeRequestObject.getAwardCode().getCertId().toString()
                        )
                );
        
        // giveAnonymous
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_GIVE_ANONYMOUS.getCode(),
                        awardCodeRequestObject.getAwardCode().getGiveAnonymous().toString()
                        )
                );
        
        // payoutType
        batchEventList.add(
                generateBatchEvent(
                        batch.getBatchId(),
                        BatchEventType.AWARD_CODE_PAYOUT_TYPE.getCode(),
                        awardCodeRequestObject.getPayoutType()
                        )
                );
        
        batchEventRepository.save(batchEventList);
        
        BatchFile batchFile = new BatchFile();
        batchFile.setBatchId(batch.getBatchId());
        batchFile.setFilename(NominationConstants.AWARD_CODE_FILE_NAME);
        batchFile.setFileUpload(awardCodeStringBuilder.toString());
        batchFileDao.save(batchFile);
        
        awardCodeGenerationDao.generateAwardCodeData(batch.getBatchId());
        
        return new AwardCodeResponseDTO().setAwardCodes(awardCodeList);
    }
    
    /**
     * generates a batch event entry based on the provided information
     * 
     * @param batchId id of the batch to link to
     * @param batchEventTypeCode type code to use
     * @param message value of the event
     * @return BatchEvent entity
     */
    private BatchEvent generateBatchEvent(Long batchId, String batchEventTypeCode, String message) {
        BatchEvent batchEvent = new BatchEvent();
        batchEvent.setBatchId(batchId);
        batchEvent.setBatchEventTypeCode(batchEventTypeCode);
        batchEvent.setMessage(message);
        batchEvent.setStatusTypeCode(StatusTypeCode.COMPLETE.toString());
        return batchEvent;
        
    }

    /**
     * Reedem Award Code
     * Possible statuses are PENDING
     * @param awardCode
     * @param awardCodeDto
     * @param paxId
     * @return NominationDetailsDTO populated with details on the nomination with the award code
     */
    @Override
    public NominationDetailsDTO redeemAwardCode(String awardCode, AwardCodeDTO awardCodeDto, Long paxId) {

        // check that award code(s) exists (in path and json) - throw AWARD_CODE_MISSING error
        if (awardCode == null || awardCodeDto == null || awardCodeDto.getAwardCode() == null) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_AWARD_CODE_MISSING)
                    .setField(ProjectConstants.AWARD_CODE)
                    .setMessage(NominationConstants.ERROR_AWARD_CODE_MISSING_MSG));
        }else if (!awardCode.equals(awardCodeDto.getAwardCode())) {// check that award code in path matches 
                                                                //award code in json - throw AWARD_CODE_MATCH error
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(NominationConstants.ERROR_AWARD_CODE_MATCH)
                        .setField(ProjectConstants.AWARD_CODE)
                        .setMessage(NominationConstants.ERROR_AWARD_CODE_MATCH_MSG));
                // check that status is valid - throw STATUS_INVALID error
            } else if (!StatusTypeCode.APPROVED.name().equals(awardCodeDto.getStatus())) {
                throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(NominationConstants.ERROR_AWARD_CODE_STATUS_INVALID)
                        .setField(ProjectConstants.AWARD_CODE)
                        .setMessage(NominationConstants.ERROR_AWARD_CODE_STATUS_INVALID_MSG));
            }

        Nomination nomination = null;
        Long recognitionId = recognitionMiscDao.findBy()
                .where(ProjectConstants.VF_NAME).eq(NominationConstants.AWARD_CODE)
                .and(ProjectConstants.MISC_DATA).eq(awardCodeDto.getAwardCode())
                .findOne(ProjectConstants.RECOGNITION_ID, Long.class);

        if (recognitionId != null) {
            Recognition recognition = recognitionDao.findBy()
                    .where(ProjectConstants.ID).eq(recognitionId)
                    .findOne();

            if (recognition != null) {
                // check the status of the award code PENDING (ready to be redeemed), APPROVED (already claimed
                switch (StatusTypeCode.valueOf(recognition.getStatusTypeCode())) {
                    case PENDING:
                        nomination = nominationDao.findById(recognition.getNominationId());
                        if(nomination == null){
                            logger.error(NominationConstants.ERROR_AWARD_CODE_NOMINATION_NOTEXIST_MSG +" [" + 
                                    recognition.getNominationId() + "] " );
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                                    .setField(ProjectConstants.AWARD_CODE)
                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
                        }
                        
                        Program program = programDao.findBy()
                                .where(ProjectConstants.PROGRAM_ID).eq(nomination.getProgramId())
                                .findOne();
                        if(program == null){
                            logger.error(NominationConstants.ERROR_AWARD_CODE_PROGRAM_NOTEXIST_MSG +" [" + 
                                    nomination.getProgramId() + "] " );
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                                    .setField(ProjectConstants.AWARD_CODE)
                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
                        }
                        // the program given from has ended or is inactive.
                        String programStatus = StatusTypeUtil.getImpliedStatus(program.getStatusTypeCode(),
                                program.getFromDate(), program.getThruDate());
    
                        if (!StatusTypeCode.ACTIVE.name().equalsIgnoreCase(programStatus)) {
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_AWARD_CODE_EXPIRED)
                                    .setField(ProjectConstants.AWARD_CODE)
                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_EXPIRED_MSG));
                        }
                        
                        if(nomination.getSubmitterPaxId().equals(paxId)){
                            throw new ErrorMessageException().addErrorMessage(
                                    new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_AWARD_CODE_CAN_NOT_BE_CLAIMED_BY_SUBMITTER)
                                    .setField(ProjectConstants.AWARD_CODE)
                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_CAN_NOT_BE_CLAIMED_BY_SUBMITTER_MSG));
                        }
                        
                        // check if the pax can receive from this program
                        List<Long> receiversList = 
                                programEligibilityQueryDao.getEligibleReceiversForProgram(nomination.getProgramId());
                        if ((!CollectionUtils.isEmpty(receiversList)) && receiversList.contains(paxId)) {
    
                            // update nomination
                            nomination.setStatusTypeCode(StatusTypeCode.APPROVED.name());
                            nominationDao.update(nomination);
    
                            // update recognition
                            recognition.setStatusTypeCode(StatusTypeCode.AUTO_APPROVED.name());
                            recognition.setReceiverPaxId(paxId);
                            recognitionDao.update(recognition);
                            
                         // Global or US overrides for project/subproject
                            BudgetMisc bm = budgetMiscDao.findBy()
                                    .where(ProjectConstants.BUDGET_ID).eq(nomination.getBudgetId())
                                    .and(ProjectConstants.MISC_TYPE_CODE).eq(VfName.SUB_PROJECT_OVERRIDE.name()).findOne();

                            if (bm != null) {
                                if (VfName.US_OVERRIDE.name().equals(bm.getVfName())) {
                                    transactionHeaderMiscDao.createUsOverrideProjectSubproject();
                                } else if (VfName.NON_US_OVERRIDE.name().equals(bm.getVfName())) {
                                    transactionHeaderMiscDao.createNonUsOverrideProjectSubproject();
                                }
                            }
    
                            //update the Transaction_Header status to Approved. 
                            Long transactionHeaderId = discretionaryDao.findBy()
                                    .where(ProjectConstants.TARGET_ID).eq(recognition.getId())
                                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                                    .findOne(ProjectConstants.TRANSACTION_HEADER_ID,Long.class);
                            if (transactionHeaderId != null) {
                                TransactionHeader transactionHeader = transactionHeaderDao.findBy()
                                        .where(ProjectConstants.ID).eq(transactionHeaderId)
                                        .findOne();
                                if (transactionHeader != null) {
                                    Long paxGroupId = paxGroupDao.findBy()
                                            .where(ProjectConstants.PAX_ID).eq(paxId)
                                            .findOne(ProjectConstants.PAX_GROUP_ID,Long.class);
                                    if(paxGroupId == null){
                                        logger.error(NominationConstants.ERROR_MISSING_PAX_GROUP_ID_MSG +
                                                " ["+paxId+"] " );
                                        throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                                    .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                                                    .setField(ProjectConstants.AWARD_CODE)
                                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
                                    }
                                    transactionHeader.setStatusTypeCode(StatusTypeCode.APPROVED.name());
                                    transactionHeader.setPaxGroupId(paxGroupId);
                                    transactionHeaderDao.update(transactionHeader);
                                } else {
                                    logger.error(NominationConstants.ERROR_AWARD_CODE_TRANSACTION_HEADER_NOTEXIST_MSG +
                                            " [" + transactionHeaderId + "] " );
                                    throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                            .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                                            .setField(ProjectConstants.AWARD_CODE)
                                            .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
                                }
                            } else {
                                logger.error(NominationConstants.ERROR_AWARD_CODE_DISCRETIONARY_NOTEXIST_MSG +
                                        " ["+recognition.getId()+"] " );
                                throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                            .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                                            .setField(ProjectConstants.AWARD_CODE)
                                            .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
                            }
    
                            List<Recognition> recognitionList = new ArrayList<Recognition>();
                            recognitionList.add(recognition);
    
                            // That'll create approval_process and approval_history
                            approvalService.createAutoApprovalHistory(recognitionList, program.getProgramId());
                            
                            // Create activity feed records.
                            newsfeedService.createNewsfeedItemsForNomination(nomination, NewsfeedItemTypeCode.AWARD_CODE);
                            
                            // Generate manager alert.
                            Long managerId = relationshipDao.findBy()
                                    .where(ProjectConstants.PAX_ID_1).eq(paxId)
                                    .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.name())
                                    .findOne(ProjectConstants.PAX_ID_2, Long.class);
                            
                            if (managerId != null) {
                                alertUtil.generateRecognitionAlerts(nomination, null, Arrays.asList(managerId), null, null);
                            }
    
                        } else {
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                        .setCode(NominationConstants.ERROR_AWARD_CODE_INELIGIBLE_TO_CLAIM)
                                        .setField(ProjectConstants.AWARD_CODE)
                                        .setMessage(NominationConstants.ERROR_AWARD_CODE_INELIGIBLE_TO_CLAIM_MSG));
                        }
                        break;
                    case AUTO_APPROVED:
                        // already claimed
                        throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                .setCode(NominationConstants.ERROR_AWARD_CODE_ALREADY_BEEN_CLAIMED)
                                .setField(ProjectConstants.AWARD_CODE)
                                .setMessage(NominationConstants.ERROR_AWARD_CODE_ALREADY_BEEN_CLAIMED_MSG));
                    default:
                        // other status.. ERROR
                        throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                .setCode(NominationConstants.ERROR_AWARD_CODE_INVALID)
                                .setField(ProjectConstants.AWARD_CODE)
                                .setMessage(String.format(NominationConstants.ERROR_OTHER_ERROR_STATUS_MSG,
                                        recognition.getStatusTypeCode())));
                }// switch

            } else {
                throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                        .setCode(NominationConstants.ERROR_AWARD_CODE_INVALID)
                        .setField(ProjectConstants.AWARD_CODE)
                        .setMessage(NominationConstants.ERROR_AWARD_CODE_INVALID_MSG));
            }
        } else { // award code not found
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_AWARD_CODE_INVALID)
                    .setField(ProjectConstants.AWARD_CODE)
                    .setMessage(NominationConstants.ERROR_AWARD_CODE_INVALID_MSG));
        }

        NominationDetailsDTO nominationDetailsDto = 
                nominationService.getNominationById(nomination.getId().toString(),nomination.getSubmitterPaxId());
        if( nominationDetailsDto == null){
            logger.error(NominationConstants.ERROR_AWARD_CODE_NOMINATION_DETAILS_NOTEXIST_MSG +" [" +
                    nomination.getId() + "] " );
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM)
                    .setField(ProjectConstants.AWARD_CODE)
                    .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REDEEM_MSG));
        }
        
        return nominationDetailsDto;
    }

    
    /**
     * Revoke Award Code
     * Possible statuses are PENDING
     * @param awardCodeDtoList
     * @param paxId
     * @return NominationDetailsDTO populated with details on the nomination with the award code
     */
    @Override
    public List<NominationDetailsDTO> revokeAwardCode(List<AwardCodeDTO> awardCodeDtoList, Long paxId) {
        
        List<NominationDetailsDTO> rejectedAwardCodeList = new ArrayList<NominationDetailsDTO>();
        
        if (CollectionUtils.isEmpty(awardCodeDtoList)) {
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                    .setCode(NominationConstants.ERROR_AWARD_CODE_MISSING)
                    .setField(ProjectConstants.AWARD_CODE)
                    .setMessage(NominationConstants.ERROR_AWARD_CODE_TO_REVOKE_MISSING_MSG));
        }
        for (AwardCodeDTO awardCodeItem : awardCodeDtoList) {
            
                if (StatusTypeCode.REJECTED.name().equals(awardCodeItem.getStatus())) {
                    Long recognitionId = recognitionMiscDao.findBy()
                            .where(ProjectConstants.VF_NAME).eq(NominationConstants.AWARD_CODE)
                            .and(ProjectConstants.MISC_DATA).eq(awardCodeItem.getAwardCode())
                            .findOne(ProjectConstants.RECOGNITION_ID, Long.class);

                    if (recognitionId == null) {
                        logger.error(NominationConstants.ERROR_AWARD_CODE_RECOGNITION_NOTEXIST_MSG + " [" +
                                    awardCodeItem.getAwardCode() + "] ");
                        throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                            .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                            .setField(ProjectConstants.AWARD_CODE)
                            .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG));
                    }
                    Recognition recognition = recognitionDao.findBy()
                                .where(ProjectConstants.ID).eq(recognitionId)
                                .findOne();
                        
                    if(recognition == null){
                            logger.error(NominationConstants.ERROR_AWARD_CODE_RECOGNITION_NOTEXIST_MSG + " [" + 
                                        recognitionId + "] ");
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                        .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                        .setField(ProjectConstants.AWARD_CODE)
                                        .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG));
                    }
                    
                    if (!StatusTypeCode.PENDING.name().equals(recognition.getStatusTypeCode())) {
                        throw new ErrorMessageException().addErrorMessage(
                                new ErrorMessage()
                                    .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                    .setField(ProjectConstants.AWARD_CODE)
                                    .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_PENDING_MSG + awardCodeItem.getAwardCode()));
                    }
                        
                    Nomination nomination = nominationDao.findById(recognition.getNominationId());

                    if (nomination == null) {
                            logger.error(NominationConstants.ERROR_AWARD_CODE_NOMINATION_NOTEXIST_MSG + " [" +
                                        recognition.getNominationId() + "] ");
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                        .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                        .setField(ProjectConstants.AWARD_CODE)
                                        .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG));
                    }
                
                    if(nomination.getSubmitterPaxId().equals(paxId) || (security.hasRole(ADMIN_ROLE))){

                        // Create REJECT_HISTORY entry
                        ApprovalHistory approvalHistory = new ApprovalHistory();
                        approvalHistory.setPaxId(paxId);
                        approvalHistory.setTargetId(recognitionId);
                        approvalHistory.setTargetTable(TableName.RECOGNITION.name());
                        approvalHistory.setApprovalDate(new Date());
                        approvalHistory.setStatusTypeCode(StatusTypeCode.REJECTED.name());
                        approvalHistory.setApprovalNotes(awardCodeItem.getAwardCode());
                        approvalHistory.setProxyPaxId(paxId);
                        
                        ApprovalProcess approvalProcess = new ApprovalProcess();
                        approvalProcess.setProgramId(nomination.getProgramId());
                        approvalProcess.setTargetId(recognition.getId());
                        approvalProcess.setTargetTable(TableName.RECOGNITION.name());
                        approvalProcess.setLastApprovalDate(new Date());
                        approvalProcess.setLastApprovalId(paxId);
                        approvalProcess.setApprovalProcessTypeCode(NominationConstants.RECOGNITION_GIVEN_TYPE_CODE);
                        approvalProcess.setStatusTypeCode(StatusTypeCode.REJECTED.toString());

                        nomination.setStatusTypeCode(StatusTypeCode.REJECTED.name());
                        recognition.setStatusTypeCode(StatusTypeCode.REJECTED.name());

                        nominationDao.update(nomination);
                        recognitionDao.update(recognition);
                        approvalProcessDao.save(approvalProcess);
                        approvalHistory.setApprovalProcessId(approvalProcess.getId()); // set process id
                        approvalHistoryDao.save(approvalHistory);

                        // update the Transaction_Header status to Rejected.
                        Long transactionHeaderId = discretionaryDao.findBy()
                                    .where(ProjectConstants.TARGET_ID).eq(recognition.getId())
                                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                                    .findOne(ProjectConstants.TRANSACTION_HEADER_ID, Long.class);
                        if (transactionHeaderId == null) {
                            logger.error(NominationConstants.ERROR_AWARD_CODE_DISCRETIONARY_NOTEXIST_MSG + " [ " +
                                        recognition.getId() + "] ");
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                        .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                        .setField(ProjectConstants.AWARD_CODE)
                                        .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG));
                        }
                        
                        TransactionHeader transactionHeader = transactionHeaderDao.findBy()
                                        .where(ProjectConstants.ID).eq(transactionHeaderId)
                                        .findOne();
                                
                        if (transactionHeader == null) {
                            logger.error(NominationConstants.ERROR_AWARD_CODE_TRANSACTION_HEADER_NOTEXIST_MSG + " [" + 
                                        transactionHeaderId + "] ");
                            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                            .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                            .setField(ProjectConstants.AWARD_CODE)
                                            .setMessage(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE_MSG));
                            }
                        transactionHeader.setStatusTypeCode(StatusTypeCode.REJECTED.name());
                        transactionHeaderDao.update(transactionHeader);
                                
                        NominationDetailsDTO nominationDetailsDto = nominationService.getNominationById(
                                nomination.getId().toString(),nomination.getSubmitterPaxId());
                                
                        rejectedAwardCodeList.add(nominationDetailsDto);
                    }else{
                        throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                                .setCode(NominationConstants.ERROR_AWARD_CODE_NOT_ABLE_TO_REVOKE)
                                .setField(ProjectConstants.AWARD_CODE)
                                .setMessage(NominationConstants.ERROR_AWARD_CODE_TO_REVOKE_STATUS_NOT_ENOUGH_PRIVILEGES));
                    }
                }//status rejected
            }// for
    return rejectedAwardCodeList;
}

}
