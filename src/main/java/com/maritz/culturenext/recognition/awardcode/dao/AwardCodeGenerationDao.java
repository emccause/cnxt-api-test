package com.maritz.culturenext.recognition.awardcode.dao;

public interface AwardCodeGenerationDao {
    
    public void generateAwardCodeData(Long batchId);

}
