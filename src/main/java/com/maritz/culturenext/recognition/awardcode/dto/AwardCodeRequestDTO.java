package com.maritz.culturenext.recognition.awardcode.dto;

import com.maritz.culturenext.recognition.dto.NominationRequestDTO;

public class AwardCodeRequestDTO extends NominationRequestDTO {
    
    private AwardCodeInfoDTO awardCode;

    public AwardCodeInfoDTO getAwardCode() {
        return awardCode;
    }

    public AwardCodeRequestDTO setAwardCode(AwardCodeInfoDTO awardCode) {
        this.awardCode = awardCode;
        return this;
    }

}
