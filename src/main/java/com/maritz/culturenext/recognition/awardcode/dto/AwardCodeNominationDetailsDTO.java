package com.maritz.culturenext.recognition.awardcode.dto;

public class AwardCodeNominationDetailsDTO {

    private String awardCode;
    private String claimingInstructions;
    private String fileName;
    private Long certId;
    
    public String getAwardCode() {
        return awardCode;
    }
    public void setAwardCode(String awardCode) {
        this.awardCode = awardCode;
    }
    public String getClaimingInstructions() {
        return claimingInstructions;
    }
    public void setClaimingInstructions(String claimingInstructions) {
        this.claimingInstructions = claimingInstructions;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public Long getCertId() {
        return certId;
    }
    public void setCertId(Long certId) {
        this.certId = certId;
    }
    
}
