package com.maritz.culturenext.recognition.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class NominationRequestDTO {

    private List<GroupMemberAwardDTO> receivers;
    private Long programId;
    private Long imageId;
    private List<Long> recognitionCriteriaIds;
    private String headline;
    private String comment;
    private Long budgetId;
    private Boolean isPrivate;
    private String payoutType;
    private List<EmployeeDTO> notifyOthers;
    private Long batchId;
    private Boolean giveAnonymous;
    private String programIdentifierByNomination;    

    public List<GroupMemberAwardDTO> getReceivers() {
        return receivers;
    }

    public NominationRequestDTO setReceivers(List<GroupMemberAwardDTO> receivers) {
        this.receivers = receivers;
        return this;
    }

    @ApiModelProperty(position = 1, required = true, value = "programID of the desired program")
    public Long getProgramId() {
        return programId;
    }

    public NominationRequestDTO setProgramId(Long programId) {
        this.programId = programId;
        return this;
    }

    @ApiModelProperty(position = 2, required = false, value = "imageId for the recognition")
    public Long getImageId() {
        return imageId;
    }

    public NominationRequestDTO setImageId(Long imageId) {
        this.imageId = imageId;
        return this;
    }

    @ApiModelProperty(position = 3, required = false, value = "list of criteriaIds for the recognition")
    public List<Long> getRecognitionCriteriaIds() {
        return recognitionCriteriaIds;
    }

    public NominationRequestDTO setRecognitionCriteriaIds(List<Long> recognitionCriteriaIds) {
        this.recognitionCriteriaIds = recognitionCriteriaIds;
        return this;
    }

    @ApiModelProperty(position = 4, required = true, value = "headline comment for the recognition")
    public String getHeadline() {
        return headline;
    }

    public NominationRequestDTO setHeadline(String headline) {
        this.headline = headline;
        return this;
    }

    @ApiModelProperty(position = 5, required = true, value = "private comment for the recognition")
    public String getComment() {
        return comment;
    }

    public NominationRequestDTO setComment(String comment) {
        this.comment = comment;
        return this;
    }

    @ApiModelProperty(position = 6, required = false, value = "desired budget for the recognition")
    public Long getBudgetId() {
        return budgetId;
    }

    public NominationRequestDTO setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }

    @ApiModelProperty(position = 7, required = false, value = "if recognition will be treated as private on the activity feed")
    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public NominationRequestDTO setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
        return this;
    }
    
    @ApiModelProperty(position = 8, required = false, value = " if there is a budget / award Amount, there MUST be a payoutType")
    public String getPayoutType() {
        return payoutType;
    }

    public NominationRequestDTO setPayoutType(String payoutType) {
        this.payoutType = payoutType;
        return this;
    }

    @ApiModelProperty(position = 9, required = false, value = " a list of pax to notify of the recognition")
    public List<EmployeeDTO> getNotifyOthers() {
        return notifyOthers;
    }

    public void setNotifyOthers(List<EmployeeDTO> notifyOthers) {
        this.notifyOthers = notifyOthers;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public Boolean getGiveAnonymous() {
        return giveAnonymous;
    }

    public void setGiveAnonymous(Boolean giveAnonymous) {
        this.giveAnonymous = giveAnonymous;
    }
    
    public String getProgramIdentifierByNomination() {
		return programIdentifierByNomination;
	}

	public void setProgramIdentifierByNomination(String programIdentifierByNomination) {
		this.programIdentifierByNomination = programIdentifierByNomination;
	}

}
