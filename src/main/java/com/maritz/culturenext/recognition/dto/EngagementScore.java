package com.maritz.culturenext.recognition.dto;


public class EngagementScore {
    
    private ScoreDTO total;
    private ScoreDTO lastLogin;
    private ScoreDTO lastRecGiven;
    private ScoreDTO lastRecReceived;
    private ScoreDTO lastComment;
    private ScoreDTO profilePic;
    
    public ScoreDTO getTotal() {
        return total;
    }
    public void setTotal(ScoreDTO total) {
        this.total = total;
    }
    public ScoreDTO getLastLogin() {
        return lastLogin;
    }
    public void setLastLogin(ScoreDTO lastLogin) {
        this.lastLogin = lastLogin;
    }
    public ScoreDTO getLastRecGiven() {
        return lastRecGiven;
    }
    public void setLastRecGiven(ScoreDTO lastRecGiven) {
        this.lastRecGiven = lastRecGiven;
    }
    public ScoreDTO getLastRecReceived() {
        return lastRecReceived;
    }
    public void setLastRecReceived(ScoreDTO lastRecReceived) {
        this.lastRecReceived = lastRecReceived;
    }
    public ScoreDTO getLastComment() {
        return lastComment;
    }
    public void setLastComment(ScoreDTO lastComment) {
        this.lastComment = lastComment;
    }
    public ScoreDTO getProfilePic() {
        return profilePic;
    }
    public void setProfilePic(ScoreDTO profilePic) {
        this.profilePic = profilePic;
    }
}
