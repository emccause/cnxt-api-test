package com.maritz.culturenext.recognition.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class AdvancedRecognitionFileContentDTO {
    
    @JsonIgnore
    private String firstName;
    
    @JsonIgnore
    private String lastName;
    
    private String controlNum;
    private Long awardAmount;
    private EmployeeDTO pax;
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getControlNum() {
        return controlNum;
    }
    public void setControlNum(String controlNum) {
        this.controlNum = controlNum;
    }
    public Long getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Long awardAmount) {
        this.awardAmount = awardAmount;
    }
    public EmployeeDTO getPax() {
        return pax;
    }
    public void setPax(EmployeeDTO pax) {
        this.pax = pax;
    }
}
