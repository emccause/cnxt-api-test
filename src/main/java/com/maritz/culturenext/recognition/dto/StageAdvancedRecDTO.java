package com.maritz.culturenext.recognition.dto;


public class StageAdvancedRecDTO {
    
    private Long id;
    private Long batchId;
    private Double awardAmount;
    private String recipientFirstName;
    private String recipientLastName;
    private String recipientId;
    private String publicHeadline;
    private String privateMessage;
    private String programName;
    private Long programId;
    private String budgetName;
    private Long budgetId;
    private String awardType;
    private String submitterFirstName;
    private String submitterLastName;
    private String submitterId;
    private Long ecardId;
    private String value;
    private Long valueId;
    private String makeRecognitionPrivate;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getBatchId() {
        return batchId;
    }
    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }
    public String getRecipientFirstName() {
        return recipientFirstName;
    }
    public void setRecipientFirstName(String recipientFirstName) {
        this.recipientFirstName = recipientFirstName;
    }
    public String getRecipientLastName() {
        return recipientLastName;
    }
    public void setRecipientLastName(String recipientLastName) {
        this.recipientLastName = recipientLastName;
    }
    public String getRecipientId() {
        return recipientId;
    }
    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }
    public String getPublicHeadline() {
        return publicHeadline;
    }
    public void setPublicHeadline(String publicHeadline) {
        this.publicHeadline = publicHeadline;
    }
    public String getPrivateMessage() {
        return privateMessage;
    }
    public void setPrivateMessage(String privateMessage) {
        this.privateMessage = privateMessage;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public Long getProgramId() {
        return programId;
    }
    public void setProgramId(Long programId) {
        this.programId = programId;
    }
    public String getBudgetName() {
        return budgetName;
    }
    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }
    public Long getBudgetId() {
        return budgetId;
    }
    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
    public String getAwardType() {
        return awardType;
    }
    public void setAwardType(String awardType) {
        this.awardType = awardType;
    }
    public String getSubmitterFirstName() {
        return submitterFirstName;
    }
    public void setSubmitterFirstName(String submitterFirstName) {
        this.submitterFirstName = submitterFirstName;
    }
    public String getSubmitterLastName() {
        return submitterLastName;
    }
    public void setSubmitterLastName(String submitterLastName) {
        this.submitterLastName = submitterLastName;
    }
    public String getSubmitterId() {
        return submitterId;
    }
    public void setSubmitterId(String submitterId) {
        this.submitterId = submitterId;
    }
    public Long getEcardId() {
        return ecardId;
    }
    public void setEcardId(Long ecardId) {
        this.ecardId = ecardId;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public Long getValueId() {
        return valueId;
    }
    public void setValueId(Long valueId) {
        this.valueId = valueId;
    }
    public String getMakeRecognitionPrivate() {
        return makeRecognitionPrivate;
    }
    public void setMakeRecognitionPrivate(String makeRecognitionPrivate) {
        this.makeRecognitionPrivate = makeRecognitionPrivate;
    }

}
