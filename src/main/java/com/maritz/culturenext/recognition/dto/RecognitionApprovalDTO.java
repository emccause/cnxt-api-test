package com.maritz.culturenext.recognition.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.maritz.culturenext.newsfeed.dto.NewsfeedNominationDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecognitionApprovalDTO {
    private Double totalPoints;
    private Integer recipientCount;
    private EmployeeDTO toPax;
    private NewsfeedNominationDTO nomination;
    private EmployeeDTO approvalPax;
    
    public Double getTotalPoints() {
        return totalPoints;
    }
    public void setTotalPoints(Double totalPoints) {
        this.totalPoints = totalPoints;
    }
    public Integer getRecipientCount() {
        return recipientCount;
    }
    public void setRecipientCount(Integer recipientCount) {
        this.recipientCount = recipientCount;
    }
    public EmployeeDTO getToPax() {
        return toPax;
    }
    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }
    public NewsfeedNominationDTO getNomination() {
        return nomination;
    }
    public void setNomination(NewsfeedNominationDTO nomination) {
        this.nomination = nomination;
    }
    public EmployeeDTO getApprovalPax() {
        return approvalPax;
    }
    public void setApprovalPax(EmployeeDTO approvalPax) {
        this.approvalPax = approvalPax;
    }
}
