package com.maritz.culturenext.recognition.dto;

import java.util.List;

public class TransactionReversalDTO {
    
    private List<Long> transactionIds;
    private String reversalComment;
    
    public List<Long> getTransactionIds() {
        return transactionIds;
    }
    public void setTransactionIds(List<Long> transactionIds) {
        this.transactionIds = transactionIds;
    }
    public String getReversalComment() {
        return reversalComment;
    }
    public void setReversalComment(String reversalComment) {
        this.reversalComment = reversalComment;
    }    
}
