package com.maritz.culturenext.recognition.dto;

import java.util.List;
import java.util.Map;

import com.maritz.culturenext.jpa.entity.StageAdvancedRec;

public class BulkAdvancedRecNominationDTO {
    
    private NominationRequestDTO nomination;
    private Long submitterPaxId;
    private Map<String, Long> recipientPaxIdControlNumMap;
    private List<StageAdvancedRec> recordList;
    private Boolean duplicatesFound;
    
    public NominationRequestDTO getNomination() {
        return nomination;
    }
    
    public BulkAdvancedRecNominationDTO setNomination(NominationRequestDTO nomination) {
        this.nomination = nomination;
        return this;
    }

    public Long getSubmitterPaxId() {
        return submitterPaxId;
    }

    public BulkAdvancedRecNominationDTO setSubmitterPaxId(Long submitterPaxId) {
        this.submitterPaxId = submitterPaxId;
        return this;
    }

    public Map<String, Long> getRecipientPaxIdControlNumMap() {
        return recipientPaxIdControlNumMap;
    }

    public BulkAdvancedRecNominationDTO setRecipientPaxIdControlNumMap(Map<String, Long> recipientPaxIdControlNumMap) {
        this.recipientPaxIdControlNumMap = recipientPaxIdControlNumMap;
        return this;
    }

    public List<StageAdvancedRec> getRecordList() {
        return recordList;
    }

    public BulkAdvancedRecNominationDTO setRecordList(List<StageAdvancedRec> recordList) {
        this.recordList = recordList;
        return this;
    }

    public Boolean getDuplicatesFound() {
        return duplicatesFound;
    }

    public BulkAdvancedRecNominationDTO setDuplicatesFound(Boolean duplicatesFound) {
        this.duplicatesFound = duplicatesFound;
        return this;
    }

}
