package com.maritz.culturenext.recognition.dto;

public class ScoreDTO {
    
    private int score;
    private int max;
    
    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
    public int getMax() {
        return max;
    }
    public void setMax(int max) {
        this.max = max;
    }
}
