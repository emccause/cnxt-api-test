package com.maritz.culturenext.recognition.dto;

public class RecognitionStatsDTO {
    private Integer countSubmitted;
    private Integer countReceived;
    private String lastRecognitionGiven;

    public Integer getCountSubmitted() {
        return countSubmitted;
    }

    public void setCountSubmitted(Integer countSubmitted) {
        this.countSubmitted = countSubmitted;
    }

    public Integer getCountReceived() {
        return countReceived;
    }

    public void setCountReceived(Integer countReceived) {
        this.countReceived = countReceived;
    }

    public String getLastRecognitionGiven() {
        return lastRecognitionGiven;
    }

    public void setLastRecognitionGiven(String lastRecognitionGiven) {
        this.lastRecognitionGiven = lastRecognitionGiven;
    }
}
