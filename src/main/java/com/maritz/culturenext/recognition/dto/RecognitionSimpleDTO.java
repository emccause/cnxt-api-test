package com.maritz.culturenext.recognition.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RecognitionSimpleDTO {
    private Long receiverPaxId;
    private Double awardAmount;
    private Long awardTierId;
    private String status;
    
    public Long getReceiverPaxId() {
        return receiverPaxId;
    }
    public void setReceiverPaxId(Long receiverPaxId) {
        this.receiverPaxId = receiverPaxId;
    }
    public Double getAwardAmount() {
        return awardAmount;
    }
    public void setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
    }
    public Long getAwardTierId() {
        return awardTierId;
    }
    public void setAwardTierId(Long awardTierId) {
        this.awardTierId = awardTierId;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
