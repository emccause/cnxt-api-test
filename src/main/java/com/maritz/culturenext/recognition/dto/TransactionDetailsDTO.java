package com.maritz.culturenext.recognition.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class TransactionDetailsDTO {
    
    private Long transactionId;
    private String awardCode;
    private String dateCreated;
    private String status;
    private Long nominationId;
    private Double awardAmount;
    private EmployeeDTO recipientPax;
    private EmployeeDTO issuedByPax;
    private Integer recipientCount;
    private String payoutType;
    private String programName;
    private String programType;
    
    public String getAwardCode() {
        return awardCode;
    }
    
    public TransactionDetailsDTO setAwardCode(String awardCode) {
        this.awardCode = awardCode;
        return this;
    }
    
    public String getDateCreated() {
        return dateCreated;
    }
    
    public TransactionDetailsDTO setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }
    
    public String getStatus() {
        return status;
    }
    
    public TransactionDetailsDTO setStatus(String status) {
        this.status = status;
        return this;
    }
    
    public Long getNominationId() {
        return nominationId;
    }

    public TransactionDetailsDTO setNominationId(Long nominationId) {
        this.nominationId = nominationId;
        return this;
    }

    public Double getAwardAmount() {
        return awardAmount;
    }

    public TransactionDetailsDTO setAwardAmount(Double awardAmount) {
        this.awardAmount = awardAmount;
        return this;
    }

    public EmployeeDTO getRecipientPax() {
        return recipientPax;
    }
    
    public TransactionDetailsDTO setRecipientPax(EmployeeDTO recipientPax) {
        this.recipientPax = recipientPax;
        return this;
    }
    
    public EmployeeDTO getIssuedByPax() {
        return issuedByPax;
    }
    
    public TransactionDetailsDTO setIssuedByPax(EmployeeDTO issuedByPax) {
        this.issuedByPax = issuedByPax;
        return this;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public TransactionDetailsDTO setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public Integer getRecipientCount() {
        return recipientCount;
    }

    public TransactionDetailsDTO setRecipientCount(Integer recipientCount) {
        this.recipientCount = recipientCount;
        return this;
    }

    public String getPayoutType() {
        return payoutType;
    }

    public TransactionDetailsDTO setPayoutType(String payoutType) {
        this.payoutType = payoutType;
        return this;
    }

    public String getProgramName() {
        return programName;
    }

    public TransactionDetailsDTO setProgramName(String programName) {
        this.programName = programName;
        return this;
    }

    public String getProgramType() {
        return programType;
    }

    public TransactionDetailsDTO setProgramType(String programType) {
        this.programType = programType;
        return this;
    }
    

}
