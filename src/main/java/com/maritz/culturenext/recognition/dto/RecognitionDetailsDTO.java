package com.maritz.culturenext.recognition.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import java.util.Date;

public class RecognitionDetailsDTO {
    private Long id;
    private Double AwardAmount;
    private EmployeeDTO fromPax;
    private EmployeeDTO toPax;
    private String status;
    private Long budgetId;
    private Long nominationId;
    private String comment;
    private String approvalComment;
    private String date;
    private EmployeeDTO approvalPax;
    private String nominationComment;
    private String headlineComment;
    private String budgetName;
    private String program;
    private Date approvalTimestamp;
    private Double pointsIssued;
    private Double raiseAmount;
    private Long transactionHeaderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonIgnore
    public Double getAwardAmount() {
        return AwardAmount;
    }

    public void setAwardAmount(Double awardAmount) {
        AwardAmount = awardAmount;
    }

    public EmployeeDTO getFromPax() {
        return fromPax;
    }

    public void setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getBudgetId() {
        return budgetId;
    }

    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }

    public Long getNominationId() {
        return nominationId;
    }

    public void setNominationId(Long nominationId) {
        this.nominationId = nominationId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public EmployeeDTO getApprovalPax() {
        return approvalPax;
    }

    public void setApprovalPax(EmployeeDTO approvalPax) {
        this.approvalPax = approvalPax;
    }

    public String getNominationComment() {
        return nominationComment;
    }

    public RecognitionDetailsDTO setNominationComment(String nominationComment) {
        this.nominationComment = nominationComment;
        return this;
    }

    public String getHeadlineComment() {
        return headlineComment;
    }

    public RecognitionDetailsDTO setHeadlineComment(String headlineComment) {
        this.headlineComment = headlineComment;
        return this;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public RecognitionDetailsDTO setBudgetName(String budgetName) {
        this.budgetName = budgetName;
        return this;
    }

    public String getApprovalComment() {
        return approvalComment;
    }

    public RecognitionDetailsDTO setApprovalComment(String approvalComment) {
        this.approvalComment = approvalComment;
        return this;
    }

    public String getProgram() {
        return program;
    }

    public RecognitionDetailsDTO setProgram(String program) {
        this.program = program;
        return this;
    }

    public Date getApprovalTimestamp() {
        return approvalTimestamp;
    }

    public void setApprovalTimestamp(Date approvalTimestamp) {
        this.approvalTimestamp = approvalTimestamp;
    }

    public Double getPointsIssued() {
        return pointsIssued;
    }

    public void setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
    }

    public Double getRaiseAmount() {
        return raiseAmount;
    }

    public void setRaiseAmount(Double raiseAmount) {
        this.raiseAmount = raiseAmount;
    }

    public Long getTransactionHeaderId() {
        return transactionHeaderId;
    }

    public void setTransactionHeaderId(Long transactionHeaderId) {
        this.transactionHeaderId = transactionHeaderId;
    }
}
