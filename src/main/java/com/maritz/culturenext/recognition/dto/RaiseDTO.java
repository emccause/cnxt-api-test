package com.maritz.culturenext.recognition.dto;


import java.util.Date;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class RaiseDTO {
    private Long id;
    private Long budgetId;
    private String time;
    private Long awardTierId;
    private Double awardAmount;
    private String status;
    private EmployeeDTO fromPax;
    private EmployeeDTO toPax;
    private EmployeeDTO approvalPax;
    private String comment;
    private Date approvalTimestamp;

    public Long getBudgetId() {
        return budgetId;
    }
    public void setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getAwardAmount() {
        return awardAmount;
    }

    public void setAwardAmount(Double points) {
        this.awardAmount = points;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public EmployeeDTO getFromPax() {
        return fromPax;
    }

    public void setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }

    public EmployeeDTO getApprovalPax() {
        return approvalPax;
    }

    public void setApprovalPax(EmployeeDTO approvalPax) {
        this.approvalPax = approvalPax;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getAwardTierId() {
        return awardTierId;
    }

    public void setAwardTierId(Long awardTierId) {
        this.awardTierId = awardTierId;
    }

    public Date getApprovalTimestamp() {
        return approvalTimestamp;
    }

    public void setApprovalTimestamp(Date approvalTimestamp) {
        this.approvalTimestamp = approvalTimestamp;
    }
}
