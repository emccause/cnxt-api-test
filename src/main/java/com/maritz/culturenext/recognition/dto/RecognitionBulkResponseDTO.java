package com.maritz.culturenext.recognition.dto;

public class RecognitionBulkResponseDTO {

    private Long batchId;
    private String status;
    private String fileName;
    private Integer totalRows;
    private Integer totalAwardAmount;
    private Integer nominationCount;
    private Boolean duplicateFileName;
    private Boolean duplicateFileContents;
    private String processDate;
    private Boolean pppIndexApplied;
    
    public Long getBatchId() {
        return batchId;
    }
    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    public Integer getTotalRows() {
        return totalRows;
    }
    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }
    public Integer getTotalAwardAmount() {
        return totalAwardAmount;
    }
    public void setTotalAwardAmount(Integer totalAwardAmount) {
        this.totalAwardAmount = totalAwardAmount;
    }
    public Integer getNominationCount() {
        return nominationCount;
    }
    public void setNominationCount(Integer nominationCount) {
        this.nominationCount = nominationCount;
    }
    public Boolean getDuplicateFileName() {
        return duplicateFileName;
    }
    public void setDuplicateFileName(Boolean duplicateFileName) {
        this.duplicateFileName = duplicateFileName;
    }
    public Boolean getDuplicateFileContents() {
        return duplicateFileContents;
    }
    public void setDuplicateFileContents(Boolean duplicateFileContents) {
        this.duplicateFileContents = duplicateFileContents;
    }
    public String getProcessDate() {
        return processDate;
    }
    public void setProcessDate(String processDate) {
        this.processDate = processDate;
    }
    public Boolean isPppIndexApplied() {
        return pppIndexApplied;
    }
    public void setPppIndexApplied(Boolean pppIndexApplied) {
        this.pppIndexApplied = pppIndexApplied;
    }
}
