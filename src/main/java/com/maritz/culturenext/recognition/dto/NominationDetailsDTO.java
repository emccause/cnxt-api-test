package com.maritz.culturenext.recognition.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.maritz.culturenext.newsfeed.dto.RecognitionCriteriaSimpleDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeNominationDetailsDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NominationDetailsDTO extends NominationDTO {
    private List<RecognitionSimpleDTO> recognitions;
    private List<RecognitionCriteriaSimpleDTO> recognitionCriteria;
    private Long newsFeedItemId;
    private List<Long>alertIds;
    private AwardCodeNominationDetailsDTO awardCodeDetail;
    private String createDate;
    private Boolean isPrivate;
    private String payoutType;
    private String payoutDisplayName;
    private List<EmployeeDTO> notifyOthers;
        
    public List<RecognitionSimpleDTO> getRecognitions() {
        return recognitions;
    }
    public void setRecognitions(List<RecognitionSimpleDTO> recognitions) {
        this.recognitions = recognitions;
    }
    public List<RecognitionCriteriaSimpleDTO> getRecognitionCriteria() {
        return recognitionCriteria;
    }
    public void setRecognitionCriteria(List<RecognitionCriteriaSimpleDTO> recognitionCriteria) {
        this.recognitionCriteria = recognitionCriteria;
    }
    public Long getNewsFeedItemId() {
        return newsFeedItemId;
    }
    public void setNewsFeedItemId(Long newsFeedItemId) {
        this.newsFeedItemId = newsFeedItemId;
    }
    public List<Long> getAlertIds() {
        return alertIds;
    }
    public void setAlertIds(List<Long> alertIds) {
        this.alertIds = alertIds;
    }
    public AwardCodeNominationDetailsDTO getAwardCodeDetail() {
        return awardCodeDetail;
    }
    public void setAwardCodeDetail(AwardCodeNominationDetailsDTO awardCodeDetail) {
        this.awardCodeDetail = awardCodeDetail;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public Boolean getIsPrivate() {
        return isPrivate;
    }
    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }
    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
    public List<EmployeeDTO> getNotifyOthers() {
        return notifyOthers;
    }
    public void setNotifyOthers(List<EmployeeDTO> notifyOthers) {
        this.notifyOthers = notifyOthers;
    }
    
}
