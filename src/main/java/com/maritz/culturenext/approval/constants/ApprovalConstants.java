package com.maritz.culturenext.approval.constants;

public class ApprovalConstants {
    // Error messages
    public static final String ERROR_NOMINATION_NOTEXIST = "NOMINATION_NOTEXIST";
    public static final String ERROR_NOMINATION_NOTEXIST_MSG = "Nomination does not exist for id provided";
    public static final String ERROR_RECOGNITION_NOTEXIST = "RECOGNITION_NOTEXIST";
    public static final String ERROR_RECOGNITION_NOTEXISTT_MSG = "Recognition does not exist for id provided";

    // validation error codes
    public static final String INVALID_NOMINATION_ID_CODE = "INVALID_NOMINATION_ID";
    public static final String INVALID_NOMINATION_ID_MESSAGE = 
            "Recognition nomination id does not match path nomination id for recognition id ";

    public static final String INVALID_STATUS_CODE = "INVALID_STATUS";
    public static final String INVALID_STATUS_MESSAGE = "Please enter a valid status for recognition id ";

    public static final String REJECT_COMMENT_REQUIRED_CODE = "REJECTION_COMMENT_REQUIRED";
    public static final String REJECT_COMMENT_REQUIRED_MESSAGE = 
            "Please add a comment for the rejection reason for recognition id ";

    public static final String RECOGNITION_DOES_NOT_EXIST_CODE = "RECOGNITION_DOES_NOT_EXIST";
    public static final String RECOGNITION_DOES_NOT_EXIST_MESSAGE = "The following recognition does not exist: ";

    public static final String RECOGNITION_NOT_APPROVEABLE_CODE = "RECOGNITION_NOT_APPROVEABLE";
    public static final String RECOGNITION_NOT_APPROVEABLE_MESSAGE = 
            "The following recognition is not able to be approved by the logged in user: ";

    public static final String MISSING_FROM_PAX_CODE = "MISSING_FROM_PAX";
    public static final String MISSING_FROM_PAX_MESSAGE = "Missing from pax on recognition id ";

    public static final String MISSING_TO_PAX_CODE = "MISSING_TO_PAX";
    public static final String MISSING_TO_PAX_MESSAGE = "Missing to pax on recognition id ";

    public static final String MISSING_APPROVAL_PAX_CODE = "MISSING_APPROVAL_PAX";
    public static final String MISSING_APPROVAL_PAX_MESSAGE = "Missing approval pax on recognition id ";

    public static final String INVALID_APPROVAL_PAX_CODE = "INVALID_APPROVAL_PAX";
    public static final String INVALID_APPROVAL_PAX_MESSAGE = 
            "Approval pax is not the logged in user on recognition id ";

    public static final String NOMINATION_ID_PLACEHOLDER = "NOMINATION_ID_PLACEHOLDER";
    public static final String APPROVAL_PAX_ID_PLACEHOLDER = "APPROVAL_PAX_ID_PLACEHOLDER";
    public static final String RECOGNITION_ID_PLACEHOLDER = "RECOGNITION_ID_PLACEHOLDER";

    public static final String ERROR_APPROVAL_INFO = "ERROR_ALERT_INFO";
    public static final String ERROR_APPROVAL_INFO_MSG = "The query for alert common info has failed";

    public static final String TOTAL_POINTS = "totalPoints";
    public static final String RECIPIENT_COUNT = "recipientCount";
}
