package com.maritz.culturenext.approval.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.maritz.culturenext.recognition.dto.RecognitionApprovalDTO;

public interface ApprovalDao {
    List<Long> getRecognitionIdsPendingApproval(Long nominationId);
    List<Long> getRaiseRecognitionIdsPendingApproval(Long nominationId, Long approvalPax);
    Map<Long, Date> getApprovedRecognitionCreateDates(List<Long> recognitionIds);
    RecognitionApprovalDTO getNominationCountAndAmountForPendingApproval(Long nominationId, Long approvalPax);
    Long getBudgetOwnerPaxId(Long nominationId);
}
