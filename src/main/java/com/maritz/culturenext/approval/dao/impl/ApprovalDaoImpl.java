package com.maritz.culturenext.approval.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.approval.constants.ApprovalConstants;
import com.maritz.culturenext.approval.dao.ApprovalDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.RecognitionApprovalDTO;

@Component
public class ApprovalDaoImpl extends AbstractDaoImpl implements ApprovalDao {
    
    private String SELECT_RECOGNITION_IDS_PENDING_APPROVAL = "select re.id " +
            "from recognition re " +
            "left join approval_pending ap " +
            "on re.id = ap.target_id and ap.target_table = 'RECOGNITION' " +
            "left join approval_history ah " +
            "on re.id = ah.target_id and ah.target_table = 'RECOGNITION' " +
            "where re.nomination_id = :" + ApprovalConstants.NOMINATION_ID_PLACEHOLDER;

    private final String SELECT_PAST_OR_PRESENT_RAISE_RECOGNITION_IDS_PENDING_APPROVAL =
            "SELECT re.ID " +
                    "FROM RECOGNITION re " +
                    "LEFT JOIN APPROVAL_PENDING AP " +
                    "ON re.ID = AP.TARGET_ID AND AP.TARGET_TABLE = 'RECOGNITION' " +
                    "LEFT JOIN APPROVAL_HISTORY AH " +
                    "ON re.ID = AH.TARGET_ID AND AH.TARGET_TABLE = 'RECOGNITION' " +
                    "WHERE re.PARENT_ID IN " +
                    "(SELECT parent.ID FROM RECOGNITION parent " +
                    "LEFT JOIN NOMINATION NOM ON parent.NOMINATION_ID = NOM.ID " +
                    "WHERE NOM.ID = :" + ApprovalConstants.NOMINATION_ID_PLACEHOLDER + " " +
                    "AND parent.STATUS_TYPE_CODE IN ( 'APPROVED', 'AUTO_APPROVED', 'PENDING' )) " +
                    "AND re.STATUS_TYPE_CODE IN ( 'PENDING' ) " +
                    "AND (AP.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + " " +
                    "OR AH.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + ") " +
                    "ORDER BY re.UPDATE_DATE DESC";

    private static final String SELECT_RECOGNITION_CREATE_DATES = "SELECT ID, CREATE_DATE " +
            "FROM RECOGNITION " +
            "WHERE (STATUS_TYPE_CODE = '" + StatusTypeCode.APPROVED.toString() + "' " +
            " OR STATUS_TYPE_CODE = '" + StatusTypeCode.AUTO_APPROVED.toString() + "' " +
            " OR STATUS_TYPE_CODE = '" + StatusTypeCode.COMPLETE.toString() + "') " +
            "AND ID IN (:" + ApprovalConstants.RECOGNITION_ID_PLACEHOLDER + ")";
    
    private static final String SELECT_PENDING_APPROVAL_FOR_NOMINATION_AND_APPROVER =
            "SELECT * FROM ( " +
                    "SELECT " +
                        "COUNT(*) AS RECIPIENT_COUNT, " +
                        "NOMINATION_ID " +
                    "FROM RECOGNITION RECG " +
                    "GROUP BY NOMINATION_ID " +
                    ") AS RECIPIENT_COUNT " + 
                "INNER JOIN ( " +
                     "SELECT " +  
                         "SUM(AMOUNT) AS TOTAL_POINTS, " + 
                         "NOMINATION_ID " + 
                     "FROM RECOGNITION RECG " + 
                     "LEFT JOIN APPROVAL_PENDING AP " + 
                        "ON RECG.ID = AP.TARGET_ID AND AP.TARGET_TABLE = 'RECOGNITION' " +
                    "LEFT JOIN APPROVAL_HISTORY AH " + 
                        "ON RECG.ID = AH.TARGET_ID AND AH.TARGET_TABLE = 'RECOGNITION' " +
                    "WHERE NOMINATION_ID = :" + ApprovalConstants.NOMINATION_ID_PLACEHOLDER + " " + 
                        "AND (AP.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + " " +
                        " OR AH.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + ") " +
                     "GROUP BY NOMINATION_ID " + 
                     ") AS TOTAL_POINTS ON TOTAL_POINTS.NOMINATION_ID = RECIPIENT_COUNT.NOMINATION_ID " + 
                     "INNER JOIN ( " + 
                         "SELECT TOP 1 " +
                             "RECG.NOMINATION_ID, " +
                             "VW.* " +
                         "FROM VW_USER_GROUPS_SEARCH_DATA VW " + 
                         "INNER JOIN RECOGNITION RECG " + 
                             "ON VW.PAX_ID = RECG.RECEIVER_PAX_ID " + 
                         "LEFT JOIN APPROVAL_PENDING AP " + 
                             "ON RECG.ID = AP.TARGET_ID AND AP.TARGET_TABLE = 'RECOGNITION' " +
                         "LEFT JOIN APPROVAL_HISTORY AH " + 
                             "ON RECG.ID = AH.TARGET_ID AND AH.TARGET_TABLE = 'RECOGNITION' " +
                         "WHERE NOMINATION_ID = :" + ApprovalConstants.NOMINATION_ID_PLACEHOLDER + " " + 
                             "AND (AP.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + " " +
                             " OR AH.PAX_ID = :" + ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER + ") " +
                         "ORDER BY RECG.ID " + 
                     ") AS RECEIVER_DATA " + 
                     "ON TOTAL_POINTS.NOMINATION_ID = RECEIVER_DATA.NOMINATION_ID";

    //Query for handling budget owner as approver
    private static final String BUDGET_OWNER = "SELECT ACL.SUBJECT_ID " +
            "FROM NOMINATION " +
            "INNER JOIN VW_BUDGET_TREE " +
            "ON NOMINATION.BUDGET_ID = VW_BUDGET_TREE.BUDGET_ID " +
            "INNER JOIN BUDGET " +
            "ON VW_BUDGET_TREE.ROOT = BUDGET.ID " +
            "AND BUDGET.PARENT_BUDGET_ID IS NULL " +
            "INNER JOIN ACL " +
            "ON BUDGET.ID = ACL.TARGET_ID " +
            "AND ACL.TARGET_TABLE = 'BUDGET' " +
            "AND ACL.ROLE_ID = (SELECT ROLE_ID FROM ROLE WHERE ROLE_CODE = 'BUDGET_OWNER') " +
            "AND ACL.SUBJECT_TABLE = 'PAX' " +
            "WHERE NOMINATION.ID = :NOMINATION_ID";
    private static final String NOMINATION_ID = "NOMINATION_ID";

    /**
     * Retrieves list of recognition ids which currently are pending approval or have been pending approval in the past
     * 
     * @param nominationId - Nomination id of recognitions to search for
     * @return recognitionIds
     */
    public List<Long> getRecognitionIdsPendingApproval(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ApprovalConstants.NOMINATION_ID_PLACEHOLDER, nominationId);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(SELECT_RECOGNITION_IDS_PENDING_APPROVAL,params, new CamelCaseMapRowMapper());

        List<Long> recognitionIds = new ArrayList<>();
        for (Map<String, Object> node : nodes) {
            recognitionIds.add((Long)node.get(ProjectConstants.ID));
        }

        return recognitionIds;
    }

    /**
     * Retrieves list of raise recognition ids which currently are pending approval or
     * have been pending approval in the past
     * @param nominationId - Nomination id of recognitions to search for
     * @return raiseRecognitionIds
     */
    public List<Long> getRaiseRecognitionIdsPendingApproval(Long nominationId, Long approvalPax) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ApprovalConstants.NOMINATION_ID_PLACEHOLDER, nominationId);
        params.addValue(ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER, approvalPax);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(SELECT_PAST_OR_PRESENT_RAISE_RECOGNITION_IDS_PENDING_APPROVAL, params, new CamelCaseMapRowMapper());

        List<Long> raiseRecognitionIds = new ArrayList<>();
        for (Map<String, Object> node : nodes) {
            raiseRecognitionIds.add((Long)node.get(ProjectConstants.ID));
        }

        return raiseRecognitionIds;
    }

    /**
     * Retrieves all approved and completed recognition create dates
     * @param recognitionIds - List of recognition ids
     * @return recognitionCreateDates
     */
    public Map<Long, Date> getApprovedRecognitionCreateDates(List<Long> recognitionIds) {
        if (CollectionUtils.isEmpty(recognitionIds)) {
            return new HashMap<>();
        }

        MapSqlParameterSource params = new MapSqlParameterSource();

        String recognitionParentIdQuery = SELECT_RECOGNITION_CREATE_DATES;
        params.addValue(ApprovalConstants.RECOGNITION_ID_PLACEHOLDER, recognitionIds);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(recognitionParentIdQuery, params, new CamelCaseMapRowMapper());

        Map<Long, Date> recognitionCreateDates = new HashMap<>();
        for (Map<String, Object> node : nodes) {
            recognitionCreateDates.put((Long) node.get(ProjectConstants.ID), (Date) node.get(ProjectConstants.CREATE_DATE));
        }

        return recognitionCreateDates;
    }

    /**
     * Retrieves a RecoingitionApprovalDTO for currently pending approval for a nomination and approver
     * @param nominationId - Nomination id of recognitions to search for
     * @param approvalPax - Pax id of person that would be approving.   
     * @return approvalDTO
     */
    public RecognitionApprovalDTO getNominationCountAndAmountForPendingApproval(Long nominationId, Long approvalPax) {
        RecognitionApprovalDTO approvalDTO = new RecognitionApprovalDTO();
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(ApprovalConstants.NOMINATION_ID_PLACEHOLDER, nominationId);
        params.addValue(ApprovalConstants.APPROVAL_PAX_ID_PLACEHOLDER, approvalPax);

        List<Map<String, Object>> nodes = 
                namedParameterJdbcTemplate.query(SELECT_PENDING_APPROVAL_FOR_NOMINATION_AND_APPROVER, params, new CamelCaseMapRowMapper());

        if (nodes != null && !nodes.isEmpty()) {
            Map<String, Object> node = nodes.get(0);
            Object totalPoints = node.get(ApprovalConstants.TOTAL_POINTS);
            if (totalPoints != null) {
                approvalDTO.setTotalPoints((Double) totalPoints);
            }
            Object recipientCount = node.get(ApprovalConstants.RECIPIENT_COUNT);
            if (recipientCount != null) {
                approvalDTO.setRecipientCount((Integer) recipientCount);
            }
            approvalDTO.setToPax(new EmployeeDTO(node, ProjectConstants.EMPTY_STRING));
        }
        
        return approvalDTO;
    }

    @Override
    public Long getBudgetOwnerPaxId(Long nominationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(NOMINATION_ID, nominationId);
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(BUDGET_OWNER, params, new CamelCaseMapRowMapper());

        if (!CollectionUtils.isEmpty(nodes)) {
            return (Long) nodes.get(0).get(ProjectConstants.SUBJECT_ID);
        }
        return null;
    }
}
