package com.maritz.culturenext.approval.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.ApprovalProcess;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.AuxiliaryNotification;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.dto.RaiseDetailsDTO;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.alert.util.AlertUtil;
import com.maritz.culturenext.approval.constants.ApprovalConstants;
import com.maritz.culturenext.approval.dao.ApprovalDao;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.ApprovalType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.jpa.repository.CnxtRelationshipRepository;
import com.maritz.culturenext.jpa.repository.CnxtTransactionHeaderRepository;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.ErrorConstants;
import com.maritz.culturenext.email.event.RecognitionEvent;


@Component
public class ApprovalServiceImpl implements ApprovalService {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private AlertService alertService;
    @Inject private AlertUtil alertUtil;
    @Inject private ApplicationEventPublisher applicationEventPublisher;
    @Inject private ApprovalDao approvalDao;
    @Inject private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    @Inject private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Inject private CnxtRelationshipRepository cnxtRelationshipRepository;
    @Inject private CnxtTransactionHeaderRepository cnxtTransactionHeaderRepository;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Inject private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Inject private ConcentrixDao<ApprovalProcess> approvalProcessDao;
    @Inject private ConcentrixDao<ApprovalProcessConfig> approvalProcessConfigDao;
    @Inject private ConcentrixDao<AuxiliaryNotification> auxiliaryNotificationDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;    
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<SysUser> sysUserDao;
    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private Environment environment;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private PaxCountDao paxCountDao;
    @Inject private ProxyService proxyService;
    @Inject private Security security;

    @Override
    public List<RecognitionDetailsDTO> approveOrRejectRecognitions(Long paxId, Long nominationId,
            List<RecognitionDetailsDTO> recognitionDetailList) {

        // Handle proxy first - this happens here instead of in validateRecognitionDetailList because we need the
        // proxyPaxId later
        Long proxyPaxId = null;
        if (security.isImpersonated() == true) {
            // make sure they have permission - either they've got the proxy permission for the pax they're approving on
            // behalf of
            // or they're an admin with the PROXY_APPROVE_REC_ANYONE permission
            if (proxyService.doesPaxHaveProxyPermission(PermissionConstants.PROXY_APPROVE_REC, paxId, security.getImpersonatorPaxId())
                    || security.impersonatorHasPermission(PermissionManagementTypes.PROXY_APPROVE_REC_ANYONE.toString())) {
                proxyPaxId = security.getImpersonatorPaxId();
            } else {
                List<ErrorMessage> errors = new ArrayList<>();
                errors.add(PermissionConstants.PROXY_NOT_ALLOWED_MESSAGE);
                ErrorMessageException.throwIfHasErrors(errors);
            }
        }

        List<Long> recognitionIdList = new ArrayList<>();
        for (RecognitionDetailsDTO recognitionDetail : recognitionDetailList) {
            if (recognitionDetail.getId() != null) { // null check, skip if null
                recognitionIdList.add(recognitionDetail.getId());
            }
        }

        List<ApprovalPending> approvalPendingList = approvalPendingDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                .and(ProjectConstants.TARGET_ID).in(recognitionIdList)
                .find();
        List<Recognition> recognitionList = recognitionDao.findById(recognitionIdList);
        Nomination nomination = nominationDao.findById(nominationId);

        ErrorMessageException.throwIfHasErrors(validateRecognitionDetailList(paxId, nominationId, recognitionDetailList,
                recognitionList, approvalPendingList));

        List<Long> paxIdsToResolveList = new ArrayList<>();
        List<Long> managerPaxIdList = new ArrayList<>();
        List<Recognition> approvedRecognitionList = new ArrayList<Recognition>();

        for (RecognitionDetailsDTO recognitionDetail : recognitionDetailList) {
            ApprovalPending approvalPendingEntry = null;
            for (ApprovalPending approvalPending : approvalPendingList) { // find specific APPROVAL_PENDING object
                if (approvalPending.getTargetId().equals(recognitionDetail.getId())) {
                    approvalPendingEntry = approvalPending;
                    break;
                }
            }
            if (approvalPendingEntry == null) {
                // Should have failed in validation - SHOULD NEVER GET HERE
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.RECOGNITION_ID);
                error.setCode(ApprovalConstants.RECOGNITION_NOT_APPROVEABLE_CODE);
                error.setMessage(ApprovalConstants.RECOGNITION_NOT_APPROVEABLE_MESSAGE + recognitionDetail.getId());
                throw new ErrorMessageException().addErrorMessage(error);
            }

            Recognition recognitionEntry = null;
            for (Recognition recognition : recognitionList) { // find specific RECOGNITION object
                if (recognition.getId().equals(recognitionDetail.getId())) {
                    recognitionEntry = recognition;
                    break;
                }
            }
            if (recognitionEntry == null) {
                // Should have failed in validation - SHOULD NEVER GET HERE
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.RECOGNITION_ID);
                error.setCode(ApprovalConstants.RECOGNITION_DOES_NOT_EXIST_CODE);
                error.setMessage(ApprovalConstants.RECOGNITION_DOES_NOT_EXIST_MESSAGE + recognitionDetail.getId());
                throw new ErrorMessageException().addErrorMessage(error);
            }

            ApprovalProcess approvalProcess = approvalProcessDao.findById(approvalPendingEntry.getApprovalProcessId());
            Integer currentApprovalLevel = approvalProcess.getPendingLevel();

            // Create APPROVAL_HISTORY entry
            ApprovalHistory approvalHistory = new ApprovalHistory();
            approvalHistory.setPaxId(paxId);
            approvalHistory.setApprovalProcessId(approvalPendingEntry.getApprovalProcessId());
            approvalHistory.setTargetId(approvalPendingEntry.getTargetId());
            approvalHistory.setTargetTable(TableName.RECOGNITION.name());
            approvalHistory.setApprovalDate(new Date());
            approvalHistory.setApprovalLevel(currentApprovalLevel);
            approvalHistory.setStatusTypeCode(recognitionDetail.getStatus().toUpperCase());
            approvalHistory.setApprovalNotes(recognitionDetail.getComment());
            approvalHistory.setProxyPaxId(proxyPaxId);

            // Update APPROVAL_PROCESS entry
            approvalProcess.setLastApprovalDate(new Date());
            approvalProcess.setLastApprovalId(paxId);
            approvalProcess.setStatusTypeCode(recognitionDetail.getStatus().toUpperCase());

            // update recognition if no additional approval is needed.
            recognitionEntry.setStatusTypeCode(recognitionDetail.getStatus().toUpperCase()); // APPROVED or REJECTED
            if (StatusTypeCode.REJECTED.toString().equalsIgnoreCase(recognitionEntry.getStatusTypeCode())) {
                recognitionEntry.setComment(recognitionDetail.getComment()); // set comment for REJECTED recognition
            } else {
                approvedRecognitionList.add(recognitionEntry);
            }

            recognitionDao.update(recognitionEntry); // update the RECOGNITION ENTRY
            approvalHistoryDao.save(approvalHistory); // create APPROVAL_HISTORY entry
            approvalProcessDao.update(approvalProcess); // update APPROVAL_PROCESS entry
            approvalPendingDao.delete(approvalPendingEntry); // delete the APPROVAL_PENDING entry

            // update TRANSACTION_HEADER
            TransactionHeader transactionHeader = cnxtTransactionHeaderRepository
                    .getTransactionHeaderByNominationIdAndPaxId(nominationId, recognitionDetail.getToPax().getPaxId());
            transactionHeader.setStatusTypeCode(recognitionDetail.getStatus().toUpperCase());
            transactionHeaderDao.update(transactionHeader);

            paxIdsToResolveList.add(recognitionEntry.getReceiverPaxId());

            // Set Award amount, it is needed for alerts.
            recognitionDetail.setAwardAmount(recognitionEntry.getAmount());
        }

        // Notify recipients of approved nomination
        List<Long> paxIds = new ArrayList<Long>();
        List<Long> groupIds = new ArrayList<Long>();
        boolean approvedRecognition = false;

        for (Recognition recognition : recognitionList) {
            // Only get the groups/pax for APPROVED recognitions
            if (ProjectConstants.APPROVED_STATUSES.contains(recognition.getStatusTypeCode())) {
                approvedRecognition = true;
                Long groupId = recognition.getGroupId();
                Long recipientPaxId = recognition.getReceiverPaxId();
                if (groupId != null) {
                    groupIds.add(groupId);
                }
                if (recipientPaxId != null) {
                    paxIds.add(recipientPaxId);
                }
            }
        }
        List<Map<String, Object>> allPax = paxCountDao.getDistinctPax(nomination.getSubmitterPaxId(), groupIds, paxIds);

        // This error is only for APPROVING recognitions
        if (approvedRecognition && (allPax == null || allPax.isEmpty())) { // All approved receivers are inactive restricted
            throw new ErrorMessageException().addErrorMessage(new ErrorMessage()
                            .setCode(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS)
                            .setMessage(NominationConstants.ERROR_NO_ELIGIBLE_RECEIVERS_MSG)
                            .setField(ProjectConstants.RECEIVERS));
        }

        for (Map<String, Object> pax : allPax) {
            Long managerPaxId = (Long) pax.get(NominationConstants.MANAGER_PAX_ID_COLUMN_NAME);
            if (!managerPaxIdList.contains(managerPaxId)) {
                managerPaxIdList.add(managerPaxId);
            }
        }
        
        // Grab all of the Notify other pax IDs
        List<Long> notifyOtherList = auxiliaryNotificationDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                .find(ProjectConstants.PAX_ID, Long.class);

        // Create recognition notification only for approved recognitions.
        alertUtil.generateRecognitionAlerts(nomination, approvedRecognitionList, managerPaxIdList, paxId, notifyOtherList);

        alertService.addApprovalActionTakenNotification(recognitionDetailList, nominationId); // create the approval action taken alert

        // take action if recognition has been completely evaluated.
        takeActionOnCompletedNominationAndNotification(paxId, nominationId,
                AlertSubType.RECOGNITION_APPROVAL.toString());

        // Add nomination from pax and approver pax (logged in user) to list of pax to resolve full pax objects for
        paxIdsToResolveList.add(recognitionDetailList.get(0).getFromPax().getPaxId());
        paxIdsToResolveList.add(paxId);

        List<EntityDTO> resolvedPaxObjects = participantInfoDao.getInfo(paxIdsToResolveList, null);

        // Determine approval timestamp after approval action has been taken
        Map<Long, Date> recognitionIdToApprovalDate = determineApprovalTimestampByRecognitionId(recognitionIdList);

        // Populate pax objects and set determined approval timestamp on response
        for (RecognitionDetailsDTO recognitionDetail : recognitionDetailList) {
            EmployeeDTO fromPax = recognitionDetail.getFromPax();
            EmployeeDTO toPax = recognitionDetail.getToPax();
            EmployeeDTO approvalPax = recognitionDetail.getApprovalPax();
            for (EntityDTO entity : resolvedPaxObjects) {
                EmployeeDTO paxEntity = null;
                try {
                    paxEntity = (EmployeeDTO) entity;
                    if (paxEntity.getPaxId().equals(fromPax.getPaxId())) {
                        recognitionDetail.setFromPax(paxEntity);
                    } else if (paxEntity.getPaxId().equals(toPax.getPaxId())) {
                        recognitionDetail.setToPax(paxEntity);
                    } else if (paxEntity.getPaxId().equals(approvalPax.getPaxId())) {
                        recognitionDetail.setApprovalPax(paxEntity);
                    }
                } catch (ClassCastException e) {
                    // Should never happen, all results should be pax
                    continue;
                }
            }

            recognitionDetail.setApprovalTimestamp(recognitionIdToApprovalDate.get(recognitionDetail.getId()));
        }

        return recognitionDetailList;
    }

    @Override
    public boolean createPendingApprovals(List<Recognition> recognitionList, Long submitterPaxId, Long programId,
            Integer awardAmount, Long approvalLevel, Long nominationId) {

        List<ApprovalProcessConfig> configs = fetchApprovalProcessConfigsForAwardTier(programId, awardAmount);
        
        Map<Long,Boolean> routeNextLevelManagerAwardTierMap = null;
        boolean routeNextLevelManager = false;
        //If configs is not null means the program contains an Award Tier
        if (configs != null) {        	
        	routeNextLevelManagerAwardTierMap = fetchRouteNextLevelManagerStatus(programId);
        }
        
        if (approvalLevel == null || programId == null || configs == null || configs.size() == 0) {
            // Either malformed data or no program settings, return
            return Boolean.FALSE;
        }

        Long nextApprovalLevel = approvalLevel + 1;

        List<Long> receiverPaxIds = new ArrayList<>();
        for (Recognition recognition : recognitionList) {
            receiverPaxIds.add(recognition.getReceiverPaxId());
        }

        Map<Long, Long> approvalMap = new HashMap<>();
        Map<Long, Long> nextApprovalMap = new HashMap<>();

        for (ApprovalProcessConfig config : configs) {
        	
        	if (!routeNextLevelManagerAwardTierMap.isEmpty()) {
        		if (routeNextLevelManagerAwardTierMap.containsKey(config.getTargetId())) {
        			routeNextLevelManager = routeNextLevelManagerAwardTierMap.get(config.getTargetId());
        		}
        	}        	
            if (approvalLevel.equals(config.getApprovalLevel())) {
                approvalMap = bulkFetchApprovers(receiverPaxIds, submitterPaxId, config.getApprovalTypeCode(),
                        config.getApproverPaxId(), nominationId,routeNextLevelManager);
            }
            if (nextApprovalLevel.equals(config.getApprovalLevel())) {
                nextApprovalMap = bulkFetchApprovers(receiverPaxIds, submitterPaxId, config.getApprovalTypeCode(),
                        config.getApproverPaxId(), nominationId,routeNextLevelManager);
            }
        }
        
        // collect recognition ids, create APPROVAL_PENDING objects
        List<Long> tempRecognitionIdList = new ArrayList<>();
        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        for (Recognition recognition : recognitionList) {
            if (StatusTypeCode.PENDING.toString().equals(recognition.getStatusTypeCode())) {
                tempRecognitionIdList.add(recognition.getId());
                
                ApprovalPending approvalPending = new ApprovalPending();
                approvalPending.setPaxId(approvalMap.get(recognition.getReceiverPaxId()));
                // We'll set the Approval Process Id later
                approvalPending.setTargetId(recognition.getId());
                approvalPending.setTargetTable(TableName.RECOGNITION.name());
                approvalPending.setNextApproverPaxId(nextApprovalMap.get(recognition.getReceiverPaxId()));
                approvalPendingList.add(approvalPending);
            }
        }
        
        if (tempRecognitionIdList.isEmpty()) {
            // no pending recognitions
            return Boolean.FALSE;
        }
        
        // Copy this before we mess with the contents of the temp list - will need this later
        List<Long> recognitionIdList = new ArrayList<>(tempRecognitionIdList);

        // Grab existing APPROVAL_PROCESS entries
        List<List<Long>> recognitionIdIterable = Lists.partition(recognitionIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        List<ApprovalProcess> approvalProcessList = new ArrayList<>();
        for (List<Long> recognitionIdSet : recognitionIdIterable) {
            approvalProcessList.addAll(approvalProcessDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.TARGET_ID).in(recognitionIdSet).find());
        }
        
        
        List<Long> approvalProcessIdList = new ArrayList<>();
        // loop through existing APPROVAL_PROCESS records to update status and create APPROVAL_HISTORY records
        for (ApprovalProcess approvalProcess : approvalProcessList) {
            // update pending level
            approvalProcess.setPendingLevel(approvalLevel.intValue());
            
            // keep for later - deleting old APPROVAL_PENDING records
            approvalProcessIdList.add(approvalProcess.getId());
            
            // remove recognition id from list (so we know what still needs to be created)
            tempRecognitionIdList.remove(approvalProcess.getTargetId());
        }
        
        approvalProcessDao.findBy().update(approvalProcessList);
        approvalProcessList = new ArrayList<>(); // reset variable
        
        // create missing APPROVAL_PROCESS records
        for (Long recognitionId : tempRecognitionIdList) {
            
            // create APPROVAL_PROCESS record            
            ApprovalProcess approvalProcess = new ApprovalProcess();
            approvalProcess.setApprovalProcessTypeCode(NominationConstants.RECOGNITION_GIVEN_TYPE_CODE);
            approvalProcess.setProgramId(programId);
            approvalProcess.setTargetId(recognitionId);
            approvalProcess.setTargetTable(TableName.RECOGNITION.name());
            approvalProcess.setStatusTypeCode(StatusTypeCode.PENDING.toString());
            approvalProcessList.add(approvalProcess);
        }

        // save records - we'll need to re-fetch them later to get the ID field populated on new records.
        approvalProcessDao.findBy().insert(approvalProcessList);
        
        // Delete old APPROVAL_PENDING records
        Iterable<List<Long>> approvalProcessIdIterable = Iterables.partition(approvalProcessIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> approvalProcessIdSet : approvalProcessIdIterable) {
                    approvalPendingDao.findBy()
                    .where(ProjectConstants.APPROVAL_PROCESS_ID).in(approvalProcessIdSet)
                    .delete();
        }
        
        // fetch APPROVAL_PROCESS records to get populated ID fields.
        approvalProcessList = new ArrayList<>(); // reset variable
        for (List<Long> recognitionIdSet : recognitionIdIterable) {
            approvalProcessList.addAll(approvalProcessDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.TARGET_ID).in(recognitionIdSet).find());
        }
        
        // loop through data and link up APPROVAL_PROCESS and APPROVAL_PENDING records
        for (ApprovalPending approvalPending : approvalPendingList) {
            Long recognitionId = approvalPending.getTargetId();
            for (ApprovalProcess approvalProcess : approvalProcessList) {
                if (recognitionId.equals(approvalProcess.getTargetId())) {
                    approvalPending.setApprovalProcessId(approvalProcess.getId());
                    break;
                }
             }
        }
        
        // save records
        approvalPendingDao.findBy().insert(approvalPendingList);
        
        // we need to send pending approval emails.
        return Boolean.TRUE;
    }

    @Async
    @Override
    public void createAutoApprovalHistory(List<Recognition> recognitionList, Long programId) {
        
        // collect recognition ids
        List<Long> recognitionIdList = new ArrayList<>();
        for (Recognition recognition : recognitionList) {
            if(ProjectConstants.APPROVED_STATUSES.contains(recognition.getStatusTypeCode())) {
                recognitionIdList.add(recognition.getId());
            }
        }

        // Grab existing APPROVAL_PROCESS entries
        List<List<Long>> recognitionIdIterable = Lists.partition(recognitionIdList, ProjectConstants.SQL_PARAM_COUNT_MAX);
        List<ApprovalProcess> approvalProcessList = new ArrayList<>();
        for (List<Long> recognitionIdSet : recognitionIdIterable) {
            approvalProcessList.addAll(approvalProcessDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.TARGET_ID).in(recognitionIdSet).find());
        }

        List<ApprovalHistory> approvalHistoryList = new ArrayList<>();
        
        // loop through existing APPROVAL_PROCESS records to update status and create APPROVAL_HISTORY records
        for (ApprovalProcess approvalProcess : approvalProcessList) {
            // mark approved
            approvalProcess.setStatusTypeCode(StatusTypeCode.APPROVED.name());
            
            // remove recognition id from list (so we know what still needs to be created)
            recognitionIdList.remove(approvalProcess.getTargetId());
            
            // create APPROVAL_HISTORY record
            approvalHistoryList.add(
                    createAutoApprovalHistoryRecord(approvalProcess.getId(), approvalProcess.getTargetId())
                );
        }
        approvalProcessDao.findBy().update(approvalProcessList);
        approvalProcessList = new ArrayList<>(); // reset variable
        
        // create missing APPROVAL_PROCESS records
        for (Long recognitionId : recognitionIdList) {
            
            // create APPROVAL_PROCESS record            
            ApprovalProcess approvalProcess = new ApprovalProcess();
            approvalProcess.setApprovalProcessTypeCode(NominationConstants.RECOGNITION_GIVEN_TYPE_CODE);
            approvalProcess.setProgramId(programId);
            approvalProcess.setTargetId(recognitionId);
            approvalProcess.setTargetTable(TableName.RECOGNITION.name());
            approvalProcess.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
            approvalProcessList.add(approvalProcess);
            
            // create APPROVAL_HISTORY record
            approvalHistoryList.add(
                    createAutoApprovalHistoryRecord(approvalProcess.getId(), approvalProcess.getTargetId())
                );
        }
        
        // save new APPROVAL_PROCESS records
        approvalProcessDao.findBy().insert(approvalProcessList);
                
        // fetch APPROVAL_PROCESS records to get populated ID fields.
        approvalProcessList = new ArrayList<>(); // reset variable
        for (List<Long> recognitionIdSet : recognitionIdIterable) {
            approvalProcessList.addAll(approvalProcessDao.findBy()
                    .where(ProjectConstants.PROGRAM_ID).eq(programId)
                    .and(ProjectConstants.TARGET_ID).in(recognitionIdSet).find());
        }
        
        // loop through data and link up APPROVAL_PROCESS and APPROVAL_PENDING records
        for (ApprovalHistory approvalHistory : approvalHistoryList) {
            Long recognitionId = approvalHistory.getTargetId();
            for (ApprovalProcess approvalProcess : approvalProcessList) {
                if (recognitionId.equals(approvalProcess.getTargetId())) {
                    approvalHistory.setApprovalProcessId(approvalProcess.getId());
                    break;
                }
             }
        }
        // Save records
        approvalHistoryDao.findBy().insert(approvalHistoryList);
    }

    @Override
    public Map<Long, Long> bulkFetchApprovers(List<Long> receiverPaxIds, Long submitterPaxId, String approvalTypeCode,
            Long configApproverPaxId, Long nominationId, boolean routeNextLevelManager) {
        Map<Long, Long> approvalMap = new HashMap<>();
        Map<Long, Long> relationshipMap = new HashMap<>();
        List<Long> paxIds = new ArrayList<>(receiverPaxIds);
        paxIds.add(submitterPaxId);

        Iterable<List<Long>> paxIdIterable = Iterables.partition(paxIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
        List<Relationship> firstRelationships = new ArrayList<>();
        for (List<Long> set : paxIdIterable) {
            firstRelationships.addAll(cnxtRelationshipRepository.getActiveManagerRelationships(set));    
        }

        List<Long> secondLevelPaxIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(firstRelationships)) {
            for (Relationship relationship : firstRelationships) {
                relationshipMap.put(relationship.getPaxId1(), relationship.getPaxId2());
                secondLevelPaxIds.add(relationship.getPaxId2());
            }
        }

        if (!CollectionUtils.isEmpty(secondLevelPaxIds)) {
            Iterable<List<Long>> secondLevelPaxIdIterable = Iterables.partition(secondLevelPaxIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
            List<Relationship> secondRelationships = new ArrayList<>();
            for (List<Long> set : secondLevelPaxIdIterable) {
                secondRelationships.addAll(cnxtRelationshipRepository.getActiveManagerRelationships(set));    
            }
            if (!CollectionUtils.isEmpty(secondRelationships)) {
                for (Relationship relationship : secondRelationships) {
                    relationshipMap.put(relationship.getPaxId1(), relationship.getPaxId2());
                }
            }
        }

        Long unassignedApprovalPaxId = null;
        try {
            String unassignedApprovalPaxString = 
                    environment.getProperty(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER);
            unassignedApprovalPaxId = unassignedApprovalPaxString == null ? null : 
                                            Long.parseLong(unassignedApprovalPaxString);
        } catch (NumberFormatException ex) {
            logger.error(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER + 
                    " value field contains an invalid numerical value");
        }

        for (Long receiverPaxId : receiverPaxIds) {
            Long approvalPaxId = null;
            ApprovalType approvalType = ApprovalType.valueOf(approvalTypeCode);
            switch (approvalType) {
                case NONE:
                    approvalPaxId = null;
                    break;
                case PERSON:
                    if (configApproverPaxId != null) {
                        SysUser approverSysUser = sysUserDao.findBy()
                            .where(ProjectConstants.PAX_ID).eq(configApproverPaxId)
                            .findOne();
                        if (StatusTypeCode.INACTIVE.name().equalsIgnoreCase(approverSysUser.getStatusTypeCode())) {
                            approvalPaxId = unassignedApprovalPaxId;
                        } else {
                            approvalPaxId = configApproverPaxId;
                        }
                    } else {
                        approvalPaxId = unassignedApprovalPaxId;
                    }
                    break;
                case BUDGET_OWNER:
                    configApproverPaxId = approvalDao.getBudgetOwnerPaxId(nominationId);
                    if (configApproverPaxId != null) {
                        SysUser approverSysUser = sysUserDao.findBy()
                                .where(ProjectConstants.PAX_ID).eq(configApproverPaxId)
                                .findOne();
                        if (StatusTypeCode.INACTIVE.name().equalsIgnoreCase(approverSysUser.getStatusTypeCode())) {
                            approvalPaxId = unassignedApprovalPaxId;
                        } else {
                            approvalPaxId = configApproverPaxId;
                        }
                    } else {
                        approvalPaxId = unassignedApprovalPaxId;
                    }
                    break;
                case SUBMITTER_FIRST:
                	approvalPaxId = relationshipMap.get(submitterPaxId) != null ? relationshipMap.get(submitterPaxId) : unassignedApprovalPaxId;

                	//If option of routeNextLevelManager is enabled (true) Manager can’t approve his own recognition that came from person that reports to him.
                	//Instead it should be rerouted to Manager's Manager. This is when approval is set to hierarchy: Submitter’s 1st Level manager
                	if (receiverPaxId.equals(approvalPaxId)&& routeNextLevelManager == true){
                		Long relationshipId = relationshipMap.get(receiverPaxId);
                    	approvalPaxId = relationshipId != null ? relationshipId : unassignedApprovalPaxId;                    	
                    }                    
                    break;
                case SUBMITTER_SECOND:
                    if (relationshipMap.get(submitterPaxId) != null
                        && relationshipMap.get(relationshipMap.get(submitterPaxId)) != null) {
                        approvalPaxId = relationshipMap.get(relationshipMap.get(submitterPaxId));
                    }
                    approvalPaxId = approvalPaxId != null ? approvalPaxId : unassignedApprovalPaxId;
                    break;
                case RECEIVER_FIRST:
                    approvalPaxId = relationshipMap.get(receiverPaxId) != null ? relationshipMap.get(receiverPaxId) : unassignedApprovalPaxId;
                    
                    //If option of routeNextLevelManager is enabled (true) Manager can't approve a recognition that he made to the person who reports to him.
                	//Instead it should be rerouted to Manager's Manager. This is when approval is set to hierarchy: Receivers's 1st Level manager
                    if (submitterPaxId.equals(approvalPaxId) && routeNextLevelManager == true){
                		Long relationshipId = relationshipMap.get(submitterPaxId);
                    	approvalPaxId = relationshipId != null ? relationshipId : unassignedApprovalPaxId;                    	
                    }     
                    
                    break;
                case RECEIVER_SECOND:
                    if (relationshipMap.get(receiverPaxId) != null
                        && relationshipMap.get(relationshipMap.get(receiverPaxId)) != null) {
                        approvalPaxId = relationshipMap.get(relationshipMap.get(receiverPaxId));
                    }
                    approvalPaxId = approvalPaxId != null ? approvalPaxId : unassignedApprovalPaxId;
                    break;
                default:
                    // We should never get here
                    approvalPaxId = null;
                    break;
            }

            // If the approver is the submitter or the approver is the receiver, auto-approve
            if (submitterPaxId.equals(approvalPaxId)||receiverPaxId.equals(approvalPaxId)){
                approvalPaxId = null;
            }
            approvalMap.put(receiverPaxId, approvalPaxId);
        }

        return approvalMap;
    }

    @Override
    public List<ApprovalProcessConfig> fetchApprovalProcessConfigsForAwardTier(Long programId, Integer awardAmount) {
        ProgramAwardTier programAwardTier = programAwardTierDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .and(ProjectConstants.MIN_AMOUNT).le(awardAmount)
                .and(ProjectConstants.MAX_AMOUNT).ge(awardAmount)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.ACTIVE.name())
                .findOne();

        if (programAwardTier == null) {
            return null;
        }

        return approvalProcessConfigDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(programAwardTier.getId())
                .find();
    }
    
    @Override
    public Map<Long, Boolean> fetchRouteNextLevelManagerStatus(Long programId) {
        
    	Map<Long, Boolean> routeNextLevelManagerAwardTierMap = new HashMap<Long, Boolean>();
    	
    	List<ProgramMisc>  programMisc = programMiscDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(programId)
                .and(ProjectConstants.VF_NAME).eq(VfName.ROUTE_NEXT_LEVEL_MANAGER.name())                
                .find();
        
    	for (ProgramMisc awardTierData : programMisc) {
    	
    			try {
    				Long awardTierId =  Long.parseLong(awardTierData.getMiscData());
    				routeNextLevelManagerAwardTierMap.put(awardTierId, Boolean.TRUE);
    				
    			}catch(NumberFormatException e) {
    				logger.error(ErrorConstants.KEY_NAME_ROUTE_NEXT_LEVEL_MANAGER_INVALID_AWARD_TIER_ID + 
    	                    " value field contains an invalid numerical value");    				
    			}catch(Exception e) {
    				logger.error(e.toString());    				
    			}
    	}
    	return routeNextLevelManagerAwardTierMap;  
    }
    
    @Override
    public void approveOrRejectRaises(List<RaiseDTO> raiseList, Long paxId) {
        Map<Long, RaiseDTO> raiseRecognitionMap = new HashMap<>();
        for (RaiseDTO raise : raiseList) {
            raiseRecognitionMap.put(raise.getId(), raise);
        }

        List<ApprovalPending> approvalPendingList = approvalPendingDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                .and(ProjectConstants.TARGET_ID).in(raiseRecognitionMap.keySet())
                .find();

        for (ApprovalPending approvalPending : approvalPendingList) {
            RaiseDTO pendingRaise = raiseRecognitionMap.get(approvalPending.getTargetId());

            if (pendingRaise != null && pendingRaise.getStatus() != null) {
                ApprovalProcess approvalProcess = approvalProcessDao.findById(approvalPending.getApprovalProcessId());

                // Create approval history record
                ApprovalHistory approvalHistory = new ApprovalHistory();
                approvalHistory.setPaxId(approvalPending.getPaxId());
                approvalHistory.setApprovalProcessId(approvalPending.getApprovalProcessId());
                approvalHistory.setTargetId(approvalPending.getTargetId());
                approvalHistory.setTargetTable(TableName.RECOGNITION.name());
                approvalHistory.setApprovalDate(new Date());
                approvalHistory.setApprovalLevel(approvalProcess.getPendingLevel());
                approvalHistory.setStatusTypeCode(pendingRaise.getStatus());
                approvalHistory.setApprovalNotes(pendingRaise.getComment());
                approvalHistoryDao.save(approvalHistory);

                // Update current approval process
                approvalProcess.setLastApprovalDate(new Date());
                approvalProcess.setLastApprovalId(approvalPending.getPaxId());
                approvalProcess.setStatusTypeCode(pendingRaise.getStatus());
                approvalProcessDao.update(approvalProcess);

                // Delete processed approval pending record
                approvalPendingDao.delete(approvalPending);
            }
        }

        List<Long> nominationIdList = recognitionDao.findBy()
                .where(ProjectConstants.ID).in(raiseRecognitionMap.keySet())
                .find(ProjectConstants.NOMINATION_ID, Long.class);

        // take action on nomination and notification if raises have been completely evaluated.
        for (Long nominationId : nominationIdList) {
            takeActionOnCompletedNominationAndNotification(paxId, nominationId, AlertSubType.RAISE_APPROVAL.toString());
        }
    }

    @Override
    public RaiseDetailsDTO getRaiseApprovalByNomination(Long nominationId, Long approvalPax) {
        RaiseDetailsDTO raiseDetailsDTO;

        List<Long> raiseRecogntionIds = approvalDao.getRaiseRecognitionIdsPendingApproval(nominationId, approvalPax);

        List<Recognition> raiseRecognitionList = recognitionDao.findBy()
                .where(ProjectConstants.ID).in(raiseRecogntionIds)
                .desc(ProjectConstants.CREATE_DATE)
                .find();

        if (!CollectionUtils.isEmpty(raiseRecognitionList)) {
            raiseDetailsDTO = new RaiseDetailsDTO();
            raiseDetailsDTO.setRecipientCount(raiseRecognitionList.size());
            raiseDetailsDTO.setToPax(participantInfoDao.getEmployeeDTO(raiseRecognitionList.get(0).getReceiverPaxId()));

            Nomination raiseNomination = nominationDao.findById(raiseRecognitionList.get(0).getNominationId());
            if (raiseNomination != null) {
                raiseDetailsDTO.setFromPax(participantInfoDao.getEmployeeDTO(raiseNomination.getSubmitterPaxId()));
            } else {
                throw new ErrorMessageException(ProjectConstants.NOMINATION,
                        ApprovalConstants.ERROR_NOMINATION_NOTEXIST, ApprovalConstants.ERROR_NOMINATION_NOTEXIST_MSG);
            }

            long raiseTotal = 0;
            for (Recognition raise : raiseRecognitionList) {
                raiseTotal += raise.getAmount();
            }
            raiseDetailsDTO.setTotalPoints(raiseTotal);
        } else {
            throw new ErrorMessageException(ProjectConstants.RECOGNITION_ID,
                    ApprovalConstants.ERROR_RECOGNITION_NOTEXIST, ApprovalConstants.ERROR_RECOGNITION_NOTEXISTT_MSG);
        }

        return raiseDetailsDTO;
    }

    @Override
    public Date determineApprovalTimestampByRecognitionId(Long recognitionId) {
        return determineApprovalTimestampByRecognitionId(Arrays.asList(recognitionId)).get(recognitionId);
    }

    @Override
    public Map<Long, Date> determineApprovalTimestampByRecognitionId(List<Long> recognitionIds) {
        Map<Long, Date> recognitionIdToApprovalDateMap = new HashMap<>();

        List<ApprovalProcess> approvalProcessList = approvalProcessDao.findBy()
                .where(ProjectConstants.TARGET_ID).in(recognitionIds)
                .and(ProjectConstants.STATUS_TYPE_CODE).not().eq(StatusTypeCode.PENDING.toString())
                .find();

        for (ApprovalProcess approvalProcess : approvalProcessList) {
            recognitionIdToApprovalDateMap.put(approvalProcess.getTargetId(), approvalProcess.getLastApprovalDate());
        }

        // Check for any missing approval dates not populated from APPROVAL_PROCESS table.
        List<Long> remainingRecognitionIds = new ArrayList<>();
        for (Long recognitionId : recognitionIds) {
            if (recognitionIdToApprovalDateMap.get(recognitionId) == null) {
                remainingRecognitionIds.add(recognitionId);
            }
        }

        // Auto-approved recognitions will be caught here and given approval date of its creation date
        if (!CollectionUtils.isEmpty(remainingRecognitionIds)) {
            Map<Long, Date> autoApprovedRecognitions = approvalDao
                    .getApprovedRecognitionCreateDates(remainingRecognitionIds);
            for (Long recognitionId : autoApprovedRecognitions.keySet()) {
                recognitionIdToApprovalDateMap.put(recognitionId, autoApprovedRecognitions.get(recognitionId));
            }
        }

        return recognitionIdToApprovalDateMap;
    }

    /**
     * Runs through all required validation for the request being made - make sure everything we need is present and in
     * the right status
     * 
     * @param paxId id of pax to handle data as
     * @param nominationId id of nomination being processed
     * @param recognitionDetailList list of recognition information provided in request
     * @param recognitionList list of recognition table objects
     * @param approvalPendingList list of pending approval table objects
     * @return list of errors, if any
     */
    private List<ErrorMessage> validateRecognitionDetailList(Long paxId, Long nominationId,
            List<RecognitionDetailsDTO> recognitionDetailList, List<Recognition> recognitionList,
            List<ApprovalPending> approvalPendingList) {

        List<ErrorMessage> errors = new ArrayList<>();
        for (RecognitionDetailsDTO recognitionDetail : recognitionDetailList) {

            // Recognition nomination id isn't the one in the request path
            if (!nominationId.equals(recognitionDetail.getNominationId())) { 
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.NOMINATION_ID);
                error.setCode(ApprovalConstants.INVALID_NOMINATION_ID_CODE);
                error.setMessage(ApprovalConstants.INVALID_NOMINATION_ID_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            if (!ProjectConstants.APPROVED_STATUSES.contains(recognitionDetail.getStatus())
                    // status provided is not "APPROVED" or "AUTO_APPROVED" or "REJECTED"
                    && !StatusTypeCode.REJECTED.toString().equalsIgnoreCase(recognitionDetail.getStatus())) { 
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.STATUS);
                error.setCode(ApprovalConstants.INVALID_STATUS_CODE);
                error.setMessage(ApprovalConstants.INVALID_STATUS_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            String rejectionComment = recognitionDetail.getComment();
            // status provided is "REJECTED" but no comment provided
            if (StatusTypeCode.REJECTED.toString().equalsIgnoreCase(recognitionDetail.getStatus())
                    && (rejectionComment == null || rejectionComment.trim().isEmpty())) { 
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.COMMENT);
                error.setCode(ApprovalConstants.REJECT_COMMENT_REQUIRED_CODE);
                error.setMessage(ApprovalConstants.REJECT_COMMENT_REQUIRED_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            // validate recognition ids first, check if the recognition exists
            boolean foundRecognition = false;
            for (Recognition recognition : recognitionList) {
                if (recognition.getId().equals(recognitionDetail.getId())) {
                    foundRecognition = true;
                    break;
                }
            }
            // no need to check for approval if recognition doesn't exist
            //check to see if there's an approval pending for the recognition that's assigned to the logged in pax
            if (foundRecognition == true) { 
                boolean foundApproval = false;
                for (ApprovalPending approvalPending : approvalPendingList) {
                    if (approvalPending.getTargetId().equals(recognitionDetail.getId())) {
                        foundApproval = true;
                        break;
                    }
                }
                if (foundApproval == false) {
                    // logged in pax isn't the approver for this pax
                    ErrorMessage error = new ErrorMessage();
                    error.setField(ProjectConstants.RECOGNITION_ID);
                    error.setCode(ApprovalConstants.RECOGNITION_NOT_APPROVEABLE_CODE);
                    error.setMessage(ApprovalConstants.RECOGNITION_NOT_APPROVEABLE_MESSAGE + recognitionDetail.getId());
                    errors.add(error);
                }
            } else {
                // rec doesn't exist
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.RECOGNITION_ID);
                error.setCode(ApprovalConstants.RECOGNITION_DOES_NOT_EXIST_CODE);
                error.setMessage(ApprovalConstants.RECOGNITION_DOES_NOT_EXIST_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            // validate pax data is present
            if (recognitionDetail.getFromPax() == null || recognitionDetail.getFromPax().getPaxId() == null) {
                // missing from pax
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.FROM_PAX);
                error.setCode(ApprovalConstants.MISSING_FROM_PAX_CODE);
                error.setMessage(ApprovalConstants.MISSING_FROM_PAX_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            if (recognitionDetail.getToPax() == null || recognitionDetail.getToPax().getPaxId() == null) {
                // missing to pax
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.TO_PAX);
                error.setCode(ApprovalConstants.MISSING_TO_PAX_CODE);
                error.setMessage(ApprovalConstants.MISSING_TO_PAX_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }

            if (recognitionDetail.getApprovalPax() == null || recognitionDetail.getApprovalPax().getPaxId() == null) {
                // missing approver pax
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.APPROVAL_PAX);
                error.setCode(ApprovalConstants.MISSING_APPROVAL_PAX_CODE);
                error.setMessage(ApprovalConstants.MISSING_APPROVAL_PAX_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            } else if (!recognitionDetail.getApprovalPax().getPaxId().equals(paxId)) {
                // approver passed in is not logged in pax
                ErrorMessage error = new ErrorMessage();
                error.setField(ProjectConstants.APPROVAL_PAX);
                error.setCode(ApprovalConstants.INVALID_APPROVAL_PAX_CODE);
                error.setMessage(ApprovalConstants.INVALID_APPROVAL_PAX_MESSAGE + recognitionDetail.getId());
                errors.add(error);
            }
        }

        return errors;
    }

    /**
     * Verify if nomination is completely evaluated. Change nomination status to APPROVED or REJECTED, depending on the
     * recognitions' status. Change notification status to ARCHIVED.
     * 
     * @param nominationId, alertType
     */
    private void takeActionOnCompletedNominationAndNotification(Long paxId, Long nominationId, String alertType) {
        int recognitionsApprovedCount = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .and(ProjectConstants.STATUS_TYPE_CODE).in(ProjectConstants.APPROVED_STATUSES)
                .count();
        if (recognitionsApprovedCount > 0) {
            NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(nominationId)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                    .and(ProjectConstants.NEWSFEED_ITEM_TYPE_CODE).eq(AlertSubType.RECOGNITION.toString())
                    .findOne();
            if (newsfeedItem != null) {
                // If at least one recognition is approved, newsfeed item is approved.
                newsfeedItem.setStatusTypeCode(StatusTypeCode.APPROVED.toString()); 

                // Verify to-pax is approved.
                List<Long> approvedReceivers = cnxtRecognitionRepository
                        .getApprovedReceiverPaxIdsWithPublicSharePermission(newsfeedItem.getTargetId());
                if (approvedReceivers != null && !approvedReceivers.isEmpty()) {
                    if (!approvedReceivers.contains(newsfeedItem.getToPaxId())) { // to pax is not among approved receivers
                        newsfeedItem.setToPaxId(approvedReceivers.get(0)); // set to first approved receiver
                    }
                } else {
                    newsfeedItem.setToPaxId(null); // no approved receivers with public share rec preferences
                }

                newsfeedItemDao.update(newsfeedItem);
            }

            List<ProgramMisc> programFlags = cnxtProgramMiscRepository.findByNominationIdAndVfNameList(nominationId,
                    Arrays.asList(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME,
                            NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME));

            Boolean notifyRecipient = Boolean.TRUE;
            Boolean notifyRecipientManager = Boolean.TRUE;
            for (ProgramMisc flag : programFlags) {
                if (flag.getVfName().equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME)) {
                    notifyRecipient = Boolean.parseBoolean(flag.getMiscData()); // should be "TRUE" or "FALSE"
                } else if (flag.getVfName()
                        .equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME)) {
                    notifyRecipientManager = Boolean.parseBoolean(flag.getMiscData()); // should be "TRUE" or "FALSE"
                }
            }
            
            int notifyOtherCount = auxiliaryNotificationDao.findBy()
                    .where(ProjectConstants.NOMINATION_ID).eq(nominationId).count();

            // send emails
            if (notifyRecipient.equals(Boolean.TRUE)) {
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.RECOGNITION_PARTICIPANT.getCode(), nominationId));
            }
            if (notifyRecipientManager.equals(Boolean.TRUE)) {
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.RECOGNITION_MANAGER.getCode(), nominationId));
            }
            if (notifyOtherCount > 0) {
                applicationEventPublisher.publishEvent(new RecognitionEvent(EventType.NOTIFY_OTHER.getCode(), nominationId));
            }

        }

        int recognitionsPendingApprovalCount = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.PENDING.toString())
                .count();
        if (recognitionsPendingApprovalCount == 0) { // No more recognitions pending approval.

            // Update nomination status
            Nomination nomination = nominationDao.findById(nominationId);

            NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(nominationId)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                    .and(ProjectConstants.NEWSFEED_ITEM_TYPE_CODE).eq(AlertSubType.RECOGNITION.toString())
                    .findOne();
            archiveAlertsByType(AlertSubType.RECOGNITION_PENDING_APPROVAL.toString(), 
                    newsfeedItem.getId(), newsfeedItem.getFromPaxId());

            // if at least one recognition is approved, we need to mark the nomination as approved
            if (recognitionsApprovedCount > 0) { 
                nomination.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
            } else { // no recognitions for the nomination were approved
                nomination.setStatusTypeCode(StatusTypeCode.REJECTED.toString());
                // newsfeed item will still be in PENDING status because there were no approvals, so update to REJECTED
                newsfeedItem.setStatusTypeCode(StatusTypeCode.REJECTED.toString());
                newsfeedItemDao.update(newsfeedItem);
            }
            nominationDao.update(nomination);

            // for recognition, alert target id equal to nominationId
            Long alertTargetId = nominationId;
            if (AlertSubType.RECOGNITION_APPROVAL.toString().equals(alertType)) {
                archiveAlertsByType(alertType, alertTargetId, paxId);
            } else if (AlertSubType.RAISE_APPROVAL.toString().equals(alertType)) {

                // for raise, alert target id equal to nomination parent id
                Nomination nominationEntity = nominationDao.findById(nominationId);
                if (nominationEntity != null && nominationEntity.getParentId() != null) {
                    alertTargetId = nominationEntity.getParentId();
                }

                List<Long> pendingRaises = approvalDao.getRaiseRecognitionIdsPendingApproval(alertTargetId, paxId);
                if (CollectionUtils.isEmpty(pendingRaises)) {
                    archiveAlertsByType(alertType, alertTargetId, paxId);
                }
            }
        }
    }

    /**
     * Archive approval pending alerts
     * 
     * @param alertType type of alert to archive
     * @param targetId target id to archive
     * @param paxId pax id to archive alert for
     */
    private void archiveAlertsByType(String alertType, Long targetId, Long paxId) {

        // archive approval pending alerts for this nomination
        List<Alert> approvalAlertList = alertDao.findBy()
                .where(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(alertType)
                .and(ProjectConstants.TARGET_ID).eq(targetId)
                .and(ProjectConstants.PAX_ID).eq(paxId)
                .find();

        // Null-check the list
        if (approvalAlertList != null) {
            for (Alert approvalAlert : approvalAlertList) { // set alerts to archived status
                approvalAlert.setStatusTypeCode(StatusTypeCode.ARCHIVED.toString());
                alertDao.update(approvalAlert);
            }
        }
    }
    
    /**
     * Creates an APPROVAL_HISTORY object in APPROVED status
     * 
     * @param approvalProcessId - APPROVAL_PROCESS.ID to tie record to
     * @param recognitionId - RECOGNITION.ID to tie record to
     * 
     * @return populated ApprovalHistory object
     */
    private ApprovalHistory createAutoApprovalHistoryRecord(Long approvalProcessId, Long recognitionId) {

        ApprovalHistory approvalHistory = new ApprovalHistory();
        approvalHistory.setApprovalProcessId(approvalProcessId);
        approvalHistory.setTargetId(recognitionId);
        approvalHistory.setTargetTable(TableName.RECOGNITION.name());
        approvalHistory.setApprovalDate(new Date());
        approvalHistory.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        approvalHistory.setApprovalNotes(ProjectConstants.AUTO_APPROVED);
        return approvalHistory;
    }

}
