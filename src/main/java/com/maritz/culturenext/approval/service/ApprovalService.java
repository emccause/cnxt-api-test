package com.maritz.culturenext.approval.service;

import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.culturenext.alert.dto.RaiseDetailsDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ApprovalService {

    /**
     * This method is where the actual approval / rejection happens - validates that the user can approve or reject the
     * recognition and then updates all the relevant database entries to their final status
     * 
     * @param paxId - this is the pax who is doing the approval
     * @param nominationId - this is the nomination for which recognitions are being approved
     * @param recognitionDetailList - contains all required information about the recognitions to finish the approval process
     * 
     * https://maritz.atlassian.net/wiki/display/M365/Update+Recognitions+for+Nomination
     */
    List<RecognitionDetailsDTO> approveOrRejectRecognitions(Long paxId, Long nominationId,
            List<RecognitionDetailsDTO> recognitionDetailList);

    /**
     * This method takes a list of recognitions and creates records in the pending approvals table
     * 
     * @param recognitionList - List of recognitions for which approvals will be created
     * @param submitterPaxId - Submitter pax id of the nomination
     * @param programId - The program id associated to the nomination
     * @param awardAmount - The award amount for the recognitions
     * @param approvalLevel - The approval level of the nomination
     * @param nominationId - Nomination id used to find budget owner
     * @return Boolean - value defining if emails and alerts are necessary
     */
    boolean createPendingApprovals(List<Recognition> recognitionList, Long submitterPaxId, Long programId,
            Integer awardAmount, Long approvalLevel, Long nominationId);

    /**
     * This method creates a map that puts together a map of receiver pax id : approver pax id to aid with the creation
     * of approval pending records or setting the recognition to auto approved
     * 
     * @param receiverPaxIds - list of pax ids tied to recipients 
     * @param submitterPaxId - pax ID of the submitter
     * @param approvalTypeCode - type of approval. Valid types are NONE,PERSON,BUDGET_OWNER,RECEIVER_FIRST,RECEIVER_SECOND,SUBMITTER_FIRST,SUBMITTER_SECOND
     * @param configApproverPaxId - if approvalTypeCode = PERSON, this is the PAX_ID for that person designated as the approver
     *                            - if approvalTypeCode = BUDGET_OWNER, this is the PAX_ID for the BUDGET_OWNER
     * @param nominationId - nomination id used to find budget owner
     * @param routeNextLevelManagerAwardTierList - List of AwardTierIds that has selected the option of "Route to the Next Level Manager" 
     */
    Map<Long, Long> bulkFetchApprovers(List<Long> receiverPaxIds, Long submitterPaxId, String approvalTypeCode,
            Long configApproverPaxId, Long nominationId, boolean routeNextLevelManagerStatus);

    /**
     * based on a program and award amount, this method finds the award tier that the award amount fits into and then
     * finds all approval process config entries for that tier.
     * 
     * @param programId - program ID to find award tiers for
     * @param awardAmount - the award amount to filter the award tier on
     */
    List<ApprovalProcessConfig> fetchApprovalProcessConfigsForAwardTier(Long programId, Integer awardAmount);

    /**
     * based on a program, this method finds if the option of ROUTE_NEXT_LEVEL_MANAGER award is selected for an Specific Award Tier 
     * 
     * @param programId - program ID to find value of routeNextLevelManager option
     * 
     */
     Map<Long, Boolean> fetchRouteNextLevelManagerStatus(Long programId);
    
    /**
     * Handles approved and rejected actions on pending raise approvals
     * 
     * @param raiseList - List of raises to update status
     * @param paxId - Pax id of whoever took approval action
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/22773800/Update+Raise
     */
    void approveOrRejectRaises(List<RaiseDTO> raiseList, Long paxId);

    /**
     * Retrieves detailed raise approval info by nomination id
     * 
     * @param nominationId - Nomination id tied to desired raise approval
     * @param approvalPax - Approval participant
     * 
     * Note: This is not implemented yet
     */
    RaiseDetailsDTO getRaiseApprovalByNomination(Long nominationId, Long approvalPax);

    /**
     * Retrieves approval date for given recognition id
     * 
     * @param recognitionId - Recognition id to check for
     */
    Date determineApprovalTimestampByRecognitionId(Long recognitionId);

    /**
     * Maps determined approval dates to given list of recognition ids
     * 
     * @param recognitionIds - List of recognition ids to map dates to
     */
    Map<Long, Date> determineApprovalTimestampByRecognitionId(List<Long> recognitionIds);

    /**
     * Creating approval history for auto-approved recognitions
     * 
     * @param recognitionList list of recognitions to process
     * @param programId program that the recognitions are created for
     */
    void createAutoApprovalHistory(List<Recognition> recognitionList, Long programId);
}
