package com.maritz.culturenext.jdbc.dao.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;

public class PaginatingDaoImpl extends AbstractDaoImpl {
    
    // errors
    private static final String ORDER_BY_FIELD = "orderBy";
    private static final String ORDER_BY_CODE = "ORDER_BY_INVALID";
    private static final String ORDER_BY_MESSAGE = "Order by clause is missing or invalid - bad dev!";
    
    private static final String PAGE_NUMBER_FIELD = "pageNumber";
    private static final String PAGE_NUMBER_CODE = "PAGE_NUMBER_INVALID";
    private static final String PAGE_NUMBER_MESSAGE = "Page number is missing or invalid - bad dev!";

    private static final String PAGE_SIZE_FIELD = "pageSize";
    private static final String PAGE_SIZE_CODE = "PAGE_SIZE_INVALID";
    private static final String PAGE_SIZE_MESSAGE = "Page size is missing or invalid - bad dev!";

    private static final String SELECT_FIELD = "select";
    private static final String SELECT_CODE = "SELECT_INVALID";
    private static final String SELECT_MESSAGE = "Query doesn't contain a select - bad dev!";
    
    // query wrapping
    private static final String SELECT = "SELECT";
    private static final String START_PLACEHOLDER = "START";
    private static final String PAGE_SIZE_PLACEHOLDER = "PAGE_SIZE";
    private static final String ORDER_BY_PLACEHOLDER = "{ORDER_BY_PLACEHOLDER_PAGINATION}";
    private static final String WRAPPER_END = "ORDER BY " + ORDER_BY_PLACEHOLDER + " " +
            "OFFSET :" + START_PLACEHOLDER + " ROWS FETCH NEXT :" + PAGE_SIZE_PLACEHOLDER + " ROWS ONLY";
    private static final String SELECT_ROW_COUNT = "SELECT COUNT(*) OVER() AS TOTAL_RESULTS, ";
    
    // CTE specific
    private static final String COMMON_TABLE_EXPRESSION_START = "WITH ";
    private static final String CTE_WRAPPER_QUERY_START = "QUERY AS ( ";
    private static final String CTE_WRAPPER_END = ") SELECT * FROM QUERY " +
            "CROSS JOIN (SELECT COUNT(*) AS TOTAL_RESULTS FROM QUERY) AS TOTAL_RESULTS " + WRAPPER_END;

    private String customCTEQuery = "";
    
    /**
     * Returns a page of data, manipulating the query to inject totalResults after the first instance of SELECT
     * 
     * @param query - provided query to paginate over.  Expected to begin with a SELECT statement.
     * @param params - required params - optional, will be created if not provided
     * @param rowMapper - row mapper to use to return the data - required
     * @param orderBy - order by clause - required
     * @param pageNumber - number of pages to offset - required
     * @param pageSize - size of pages - required.
     * 
     * @return list of results
     */
    public <T> List<T> getPageWithTotalRows(String query, MapSqlParameterSource params, RowMapper<T> rowMapper, 
            String orderBy, Integer pageNumber, Integer pageSize) {
        
        if (query.startsWith(SELECT)) {
            query = query.replaceFirst(SELECT, SELECT_ROW_COUNT); // include row count
        } else { // need to be populated.
            ErrorMessage errorMessage = new ErrorMessage()
                .setField(SELECT_FIELD)
                .setCode(SELECT_CODE)
                .setMessage(SELECT_MESSAGE);
            ErrorMessageException.throwIfHasErrors(Arrays.asList(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        query = query + WRAPPER_END.replace(ORDER_BY_PLACEHOLDER, orderBy);
        
        return process(query, params, rowMapper, orderBy, pageNumber, pageSize);
        
    }
    
    /**
     * Returns a page of data, wrapping the provided query as a common table expression.
     * Main use for this method is when your query needs to use a SELECT DISTINCT and we need to get an accurate result count.
     * 
     * @param query - provided query to paginate over.
     * @param params - required params - optional, will be created if not provided
     * @param rowMapper - row mapper to use to return the data - required
     * @param orderBy - order by clause - required
     * @param pageNumber - number of pages to offset - required
     * @param pageSize - size of pages - required.
     * 
     * @return list of results
     */
    public <T> List<T> getPageWithTotalRowsCTE(String query, MapSqlParameterSource params, RowMapper<T> rowMapper, 
            String orderBy, Integer pageNumber, Integer pageSize) {
        
        query = COMMON_TABLE_EXPRESSION_START + customCTEQuery + CTE_WRAPPER_QUERY_START + query + CTE_WRAPPER_END.replace(ORDER_BY_PLACEHOLDER, orderBy);
        
        return process(query, params, rowMapper, orderBy, pageNumber, pageSize);
    }
    
    /*
     * does the lower level processing after we've built the query.  See public methods for details.
     */
    private <T> List<T> process(String query, MapSqlParameterSource params, RowMapper<T> rowMapper, 
            String orderBy, Integer pageNumber, Integer pageSize) {
        if (orderBy == null || orderBy.trim().isEmpty()) { // need to be populated.
            ErrorMessage errorMessage = new ErrorMessage()
                .setField(ORDER_BY_FIELD)
                .setCode(ORDER_BY_CODE)
                .setMessage(ORDER_BY_MESSAGE);
            ErrorMessageException.throwIfHasErrors(Arrays.asList(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        if (pageNumber == null || pageNumber < 1) { // need to be populated.
            ErrorMessage errorMessage = new ErrorMessage()
                .setField(PAGE_NUMBER_FIELD)
                .setCode(PAGE_NUMBER_CODE)
                .setMessage(PAGE_NUMBER_MESSAGE);
            ErrorMessageException.throwIfHasErrors(Arrays.asList(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        if (pageSize == null || pageSize < 1) { // need to be populated.
            ErrorMessage errorMessage = new ErrorMessage()
                .setField(PAGE_SIZE_FIELD)
                .setCode(PAGE_SIZE_CODE)
                .setMessage(PAGE_SIZE_MESSAGE);
            ErrorMessageException.throwIfHasErrors(Arrays.asList(errorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
        if (params == null) {
            params = new MapSqlParameterSource();
        }
        
        params.addValue(START_PLACEHOLDER, (pageNumber - 1) * pageSize);
        params.addValue(PAGE_SIZE_PLACEHOLDER, pageSize);
        
        return namedParameterJdbcTemplate.query(query, params, rowMapper);
    }

    /**
     * Used to add more Common Table Expressions (CTE) to the query
     * @param customCTEQuery
     */
    protected void setCustomCTEQuery(String customCTEQuery) {
        this.customCTEQuery = customCTEQuery;
    }
}
