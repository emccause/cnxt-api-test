package com.maritz.culturenext.jdbc.tomcat.interceptor;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.ConnectionPool;
import org.apache.tomcat.jdbc.pool.JdbcInterceptor;
import org.apache.tomcat.jdbc.pool.PooledConnection;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class ContextInfoUserNameInterceptor extends JdbcInterceptor {

    private static final String SQL_SET_CONTEXT_INFO =
        "DECLARE @contextInfo VARBINARY(100) = CONVERT(VARBINARY(100), ?);" +
        "SET CONTEXT_INFO @contextInfo";

    private PooledConnection pooledConnection;
    private String connectionUserName;

    @Override
    public void reset(ConnectionPool parent, PooledConnection con) {
        if (parent == null) {
            this.pooledConnection = null;
            this.connectionUserName = null;
        }
        else {
            //NOTE: the user name cannot be set here because this happens before the user is added to the security context
            this.pooledConnection = con;
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (pooledConnection != null) {
            final String methodName = method.getName();
            if (compare("prepareStatement", methodName) || compare("createStatement", methodName) || compare("prepareCall", methodName)) {
                //set the connection user name if it has not been set yet
                setConnectionUserName();
            }
            else if (compare(CLOSE_VAL, methodName)) {
                //clear the connection user name on close()
                clearConnectionUserName();
            }
        }
        return super.invoke(proxy,method,args);
    }

    private void setConnectionUserName() throws SQLException {
        String authenticatedUserName = getAuthenticatedUserName();
        if (authenticatedUserName != null) {
            if (connectionUserName == null || !connectionUserName.equalsIgnoreCase(authenticatedUserName)) {
                connectionUserName = authenticatedUserName;
                setContextInfo(connectionUserName);
            }
        }
        else {
            clearConnectionUserName();
        }
    }

    private void clearConnectionUserName() throws SQLException {
        if (connectionUserName != null) {
            connectionUserName = null;
            setContextInfo("");
        }
    }

    private String getAuthenticatedUserName() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && authentication.isAuthenticated()
            ? authentication.getName()
            : null
        ;
    }

    private void setContextInfo(String contextInfo) throws SQLException {
        Connection conn = pooledConnection.getConnection();
        PreparedStatement ps = conn.prepareStatement(SQL_SET_CONTEXT_INFO);
        try {
            ps.setNString(1, contextInfo);
            ps.executeUpdate();
        }
        finally {
            ps.close();
        }
    }

}


