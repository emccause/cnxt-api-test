package com.maritz.culturenext.dto;

import com.maritz.culturenext.search.constants.SearchConstants;

import java.util.Date;
import java.util.Map;

public class EmailDTO {

    private Date sentDate;
    private String subject;
    private String emailType;


    /**
     * Constructor utilizing given field mapping for default values.
     *
     * @param node field mapping with default values
     */
    public EmailDTO(Map<String, Object> node) {
        if (node.get(SearchConstants.SENT_DATE) != null) {
            this.sentDate = (Date) node.get(SearchConstants.SENT_DATE);
        }
        if (node.get(SearchConstants.SUBJECT) != null) {
            this.subject = (String) node.get(SearchConstants.SUBJECT);
        }
        if (node.get(SearchConstants.EVENT_TYPE_DESC) != null) {
            this.emailType = (String) node.get(SearchConstants.EVENT_TYPE_DESC);
        }

    }

    public Date getSentDate() {
        return sentDate;
    }

    public EmailDTO setSentDate(Date sentDate) {
        this.sentDate = sentDate;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public EmailDTO setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getEmailType() {
        return emailType;
    }

    public EmailDTO setEmailType(String emailType) {
        this.emailType = emailType;
        return this;
    }

}
