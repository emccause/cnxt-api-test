package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@ApiModel("Group")
@Data
@NoArgsConstructor
@Accessors(chain = true)
@AllArgsConstructor
public class GroupDTO {
    @ApiModelProperty(example = "1", value = "id generated.")
    private Long id;
    @ApiModelProperty(example = "Sandoz Limited")
    private String name;
}
