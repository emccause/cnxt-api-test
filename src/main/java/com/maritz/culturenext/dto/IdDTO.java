package com.maritz.culturenext.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class IdDTO {

    public IdDTO(Long id) {
        super();
        this.id = id;
    }

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
