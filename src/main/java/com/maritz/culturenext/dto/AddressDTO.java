package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@ApiModel("Address")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddressDTO {
    @ApiModelProperty(example = "123ABC", value = "controlNumber")
    private String controlNumber;
    @ApiModelProperty(example = "123", value = "addressId")
    private Long addressId;
    @ApiModelProperty(example = "US", value = "Country code.")
    private String countryCode;
    @ApiModelProperty(example = "[\"Florida Business Park\",\"2065 Blue Island Drive\",\"Suite 1200\"]", value = "List of the parts of the address.")
    private List<String> addressLines;
    @ApiModelProperty(example = "33129", value = "Postal code.")
    private String postalCode;
    @ApiModelProperty(example = "Miami", value = "City name.")
    private String city;
    @ApiModelProperty(example = "FL", value = "State code.")
    private String state;
}
