package com.maritz.culturenext.dto;

import java.util.List;

public class ActivityFeedFilterDTO {
    private List<FilterDetailsDTO> filterOptions;
    private Boolean searchEnabled;
    private String defaultFilter;

    public ActivityFeedFilterDTO() {}

    public List<FilterDetailsDTO> getFilterOptions() {
        return filterOptions;
    }

    public void setFilterOptions(List<FilterDetailsDTO> filterOptions) {
        this.filterOptions = filterOptions;
    }

    public Boolean getSearchEnabled() {
        return searchEnabled;
    }

    public void setSearchEnabled(Boolean searchEnabled) {
        this.searchEnabled = searchEnabled;
    }

    public String getDefaultFilter() {
        return defaultFilter;
    }

    public void setDefaultFilter(String defaultFilter) {
        this.defaultFilter = defaultFilter;
    }
}
