package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@ApiModel("Participant")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ParticipantDTO {
    //Participant Control Number will be used as id to retrieve the Participant.
    @ApiModelProperty(required = true, example = "1889820IA", value = "Participant Control Number.")
    private String controlNumber;
    @ApiModelProperty(example = "John", value = "Participant first name.")
    private String firstName;
    @ApiModelProperty(example = "Doe", value = "Participant last name.")
    private String lastName;
    @ApiModelProperty(example = "doej", value = "Participant user name.")
    private String username;
    @ApiModelProperty(example = "ACTIVE, ACCEPTED, INACTIVE, etc.", value = "Status of login account")
    private String status;
    @ApiModelProperty(example = "joe.doe@email.com", value = "Email address.")
    private String emailAddress;
    // Manager's Control Number
    @ApiModelProperty(example = "1887820I", value = "Manager Control Number.")
    private String managerControlNumber;
    @ApiModelProperty(example = "Business Coordinator")
    private String jobTitle;
    @ApiModelProperty(example = "ACME")
    private String companyName;
    @ApiModelProperty(example = "2010-10-21")
    private LocalDate hireDate;
    @ApiModelProperty(example = "0000832785")
    private String costCenter;
    @ApiModelProperty(example = "GD")
    private String department;
    @ApiModelProperty(example = "C028")
    private String area;
    @ApiModelProperty(example = "Basel")
    private String function;
    @ApiModelProperty(example = "C046_B7")
    private String grade;
    @ApiModelProperty(example = "1818425465")
    private String phoneNumber;
    @ApiModelProperty(example = "02010809, REFS NBS, Global Management SZ, etc.")
    private List<String> customFields;
    private List<GroupDTO> groups;
    private List<RoleDTO> roles;
    private AddressDTO address;
}
