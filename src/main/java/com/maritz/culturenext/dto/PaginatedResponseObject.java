package com.maritz.culturenext.dto;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;

public class PaginatedResponseObject<T> {
    
    private static final String NO_DATA_ON_PAGE_ERROR = "NO_DATA_ON_PAGE";
    private static final String NO_DATA_ON_PAGE_ERROR_MESSAGE = "No data found";
    
    private static final String FIRST_PAGE_NUMBER = "1";
    private static final String EQUALS_SIGN = "=";
    private static final String AMPERSAND = "&";
    private static final String QUESTION_MARK = "?";
    private static final String PATH_PARAM_KEY_FORMAT = "{%s}";
    
    private static final String META_TOTAL_PAGES_KEY = "totalPages";
    private static final String META_TOTAL_RESULTS_KEY = "totalResults";
    public static final String META_PAGE_SIZE_KEY = "page[size]";
    public static final String META_PAGE_NUMBER_KEY = "page[number]";
    
    private static final String LINKS_SELF_KEY = "self";
    private static final String LINKS_FIRST_KEY = "first";
    private static final String LINKS_LAST_KEY = "last";
    private static final String LINKS_PREV_KEY = "prev";
    private static final String LINKS_NEXT_KEY = "next";
    
    private static final String PAGE_NUMBER_PLACEHOLDER = "{PAGE_NUMBER}";
    private static final String PAGE_SIZE_PLACEHOLDER = "{PAGE_SIZE}";
    
    private static final String PAGE_PARAMS = META_PAGE_NUMBER_KEY + EQUALS_SIGN + PAGE_NUMBER_PLACEHOLDER + 
            AMPERSAND + META_PAGE_SIZE_KEY + EQUALS_SIGN + PAGE_SIZE_PLACEHOLDER;
    
    private Map<String, Object> meta;
    private List<T> data;
    private Map<String, String> links;
    
    /**
     * empty constructor - not usable, constructor needed for mocking
     */
    public PaginatedResponseObject(){
    }
    
    /**
     * Constructor that takes info about the request and builds a response object
     * 
     * Use this if you have placeholders in your request path
     * 
     * Should be replaced with the constructor using PaginationRequestDetails passed in from rest service layer
     * 
     * @param data - the objects to be returned to the user
     * @param requestParamMap - a map of parameters on the request, used for building links
     * @param requestPath - the path of the request, used for building links
     * @param pathParamMap - map of path parameters for the request, used to clean up the requestPath if there are placeholders
     * @param totalResults - total number of results, provided by service
     * @param pageNumber - what page of results is being returned
     * @param pageSize - size of page being returned
     */
    @Deprecated
    public PaginatedResponseObject(
            List<T> data, Map<String, Object> requestParamMap, String requestPath, Map<String, Object> pathParamMap,
            Integer totalResults, Integer pageNumber, Integer pageSize
        ) {
        
        this(new PaginationRequestDetails(requestParamMap, requestPath, pathParamMap, pageNumber, pageSize), data, totalResults);
    }
    
    /**
     * Constructor that takes info about the request and builds a response object
     * 
     * Use this if you don't have placeholders in your request path
     * 
     * Should be replaced with the constructor using PaginationRequestDetails passed in from rest service layer
     * 
     * @param data - the objects to be returned to the user
     * @param requestParamMap - a map of parameters on the request, used for building links
     * @param requestPath - the path of the request, used for building links
     * @param totalResults - total number of results, provided by service
     * @param pageNumber - what page of results is being returned
     * @param pageSize - size of page being returned
     */
    @Deprecated
    public PaginatedResponseObject(
                List<T> data, Map<String, Object> requestParamMap, String requestPath,
                Integer totalResults, Integer pageNumber, Integer pageSize
            ) {

        this(new PaginationRequestDetails(requestParamMap, requestPath, null, pageNumber, pageSize), data, totalResults);
    }
    
    /**
     * Constructor that takes a PaginationRequestDetails object (should be constructed in rest layer) containing info about the request
     * 
     * @param requestDetails - PaginationRequestDetails object containing info on the request
     * @param data - the objects to be returned to the user
     * @param totalResults - total number of results, provided by service
     */
    public PaginatedResponseObject(PaginationRequestDetails requestDetails, List<T> data, Integer totalResults) {

        this.data = data;
        
        if (requestDetails == null) { // assume we're not paginating and get out
            return;
        }
        
        String path = requestDetails.getRequestPath();
        
        if (path == null) { // we need a path
            path = ProjectConstants.EMPTY_STRING;
        }
        
        Map<String, Object> pathParamMap = requestDetails.getPathParamMap();
        if (pathParamMap != null && !pathParamMap.isEmpty()) {
            for (String key : pathParamMap.keySet()) {
                Object value = pathParamMap.get(key);
                
                if (!key.contains("{")) {
                    // Assumes that path is "/path/{something}/more_path"
                    // and key is "something"
                    key = String.format(PATH_PARAM_KEY_FORMAT, key);    
                }
                
                if (value != null) { // we don't care about nulls.
                    path = path.replace(key, value.toString());
                }
                else {
                    path = path.replace(key, ProjectConstants.EMPTY_STRING);
                }
                
            }
        }
        
        if (data.isEmpty() && requestDetails.getPageNumber() > 1) {
            // Throw an error because we're after the max page of the result set and weird things happen here
            // no data would be returned anyway.
            
            ErrorMessage errorMessage = new ErrorMessage()
                .setField(ProjectConstants.REQUEST)
                .setCode(NO_DATA_ON_PAGE_ERROR)
                .setMessage(NO_DATA_ON_PAGE_ERROR_MESSAGE);
            ErrorMessageException.throwIfHasErrors(Arrays.asList(errorMessage), HttpStatus.BAD_REQUEST);
        }
        
        Integer totalPages = (int) Math.ceil((double) totalResults / (double) requestDetails.getPageSize());
        
        this.meta = new HashMap<String, Object>();
        this.meta.put(META_TOTAL_RESULTS_KEY, totalResults);
        this.meta.put(META_TOTAL_PAGES_KEY, totalPages);
        this.meta.put(META_PAGE_SIZE_KEY, requestDetails.getPageSize());
        this.meta.put(META_PAGE_NUMBER_KEY, requestDetails.getPageNumber());
        
        StringBuilder linkBuilder = new StringBuilder(path);
        
        boolean useAmpersand = path.contains(QUESTION_MARK);
        
        // handle parameters - add them to meta and links. 
        // page parameters have special logic so they're handled separately.
        Map<String, Object> requestParamMap = requestDetails.getRequestParamMap();
        if (requestParamMap != null && !requestParamMap.isEmpty()) {
            for (String key : requestParamMap.keySet()) {
                Object value = requestParamMap.get(key);
                if (value != null) { // we don't care about nulls.
                    this.meta.put(key, value); // add param to meta
                    
                    if (Boolean.TRUE.equals(useAmpersand)) { // add param to request for links
                        linkBuilder.append(AMPERSAND);
                    }
                    else {
                        linkBuilder.append(QUESTION_MARK);
                        useAmpersand = Boolean.TRUE;
                    }
                    
                    linkBuilder.append(key);
                    linkBuilder.append(EQUALS_SIGN);
                    linkBuilder.append(value.toString());
                    
                }
            }
        }
        
        // set up page parameters for links
        linkBuilder.append((Boolean.TRUE.equals(useAmpersand) ? AMPERSAND: QUESTION_MARK));
        linkBuilder.append(PAGE_PARAMS.replace(PAGE_SIZE_PLACEHOLDER, Integer.toString(requestDetails.getPageSize())));
        
        String linkBase = linkBuilder.toString();
        
        // populate links section - each link has slightly different logic for page number.
        this.links = new HashMap<String, String>();
        
        links.put(LINKS_SELF_KEY, linkBase.replace(PAGE_NUMBER_PLACEHOLDER, Integer.toString(requestDetails.getPageNumber())));
        
        if (!data.isEmpty()) { // only populate self link if there's no data
            links.put(LINKS_FIRST_KEY, linkBase.replace(PAGE_NUMBER_PLACEHOLDER, FIRST_PAGE_NUMBER));
            links.put(LINKS_LAST_KEY, linkBase.replace(PAGE_NUMBER_PLACEHOLDER, Integer.toString(totalPages)));
            
            if (requestDetails.getPageNumber().equals(1)) { // don't populate previous page if it's the first page
                links.put(LINKS_PREV_KEY, null);
            }
            else {
                links.put(LINKS_PREV_KEY, linkBase.replace(PAGE_NUMBER_PLACEHOLDER, Integer.toString((requestDetails.getPageNumber() - 1))));
            }

            if (requestDetails.getPageNumber().equals(totalPages)) { // don't populate next page if it's the last page
                links.put(LINKS_NEXT_KEY, null);
            }
            else {
                links.put(LINKS_NEXT_KEY, linkBase.replace(PAGE_NUMBER_PLACEHOLDER, Integer.toString((requestDetails.getPageNumber() + 1))));
            }
        }
    }
    
    public Map<String, Object> getMeta() {
        return meta;
    }
    
    public PaginatedResponseObject<T> setMeta(Map<String, Object> meta) {
        this.meta = meta;
        return this;
    }
    
    public List<T> getData() {
        return data;
    }
    
    public PaginatedResponseObject<T> setData(List<T> data) {
        this.data = data;
        return this;
    }
    
    public Map<String, String> getLinks() {
        return links;
    }
    
    public PaginatedResponseObject<T> setLinks(Map<String, String> links) {
        this.links = links;
        return this;
    }
    
    // DTO class used to pass this data in bulk so we can handle these in the rest layer instead of in the services layer
    public static class PaginationRequestDetails {
        
        private Map<String, Object> requestParamMap;
        private String requestPath;
        private Map<String, Object> pathParamMap;
        private Integer pageNumber;
        private Integer pageSize;
        
        /**
         * empty constructor
         */
        public PaginationRequestDetails() {
            
        }
        
        /**
         * Constructs a PaginationRequestDetails object holding the provided data (to be passed to PaginatedResponseObject)
         * 
         * @param requestParamMap - a map of parameters on the request, used for building links
         * @param requestPath - the path of the request, used for building links
         * @param pathParamMap - map of path parameters for the request, used to clean up the requestPath if there are placeholders
         * @param pageNumber - what page of results is being returned
         * @param pageSize - size of page being returned
         */
        public PaginationRequestDetails(
                Map<String, Object> requestParamMap, String requestPath,
                Map<String, Object> pathParamMap, Integer pageNumber, Integer pageSize
            ) {
            
            this.requestParamMap = requestParamMap;
            this.requestPath = requestPath;
            this.pathParamMap = pathParamMap;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            
        }
        
        public Map<String, Object> getRequestParamMap() {
            return requestParamMap;
        }
        
        public PaginationRequestDetails setRequestParamMap(Map<String, Object> requestParamMap) {
            this.requestParamMap = requestParamMap;
            return this;
        }
        
        public String getRequestPath() {
            return requestPath;
        }
        
        public PaginationRequestDetails setRequestPath(String requestUri) {
            this.requestPath = requestUri;
            return this;
        }
        
        public Map<String, Object> getPathParamMap() {
            return pathParamMap;
        }
        
        public PaginationRequestDetails setPathParamMap(Map<String, Object> pathParamMap) {
            this.pathParamMap = pathParamMap;
            return this;
        }
        
        public Integer getPageNumber() {
            return pageNumber;
        }
        
        public PaginationRequestDetails setPageNumber(Integer pageNumber) {
            this.pageNumber = pageNumber;
            return this;
        }
        
        public Integer getPageSize() {
            return pageSize;
        }
        
        public PaginationRequestDetails setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
            return this;
        }
    }
}
