package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.ZonedDateTime;

@ApiModel("Enrollment History")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class EnrollmentHistoryDTO {

    @ApiModelProperty(example = "2020-10-13 18:28:34")
    private ZonedDateTime changeDate;
    @ApiModelProperty(example = "2019-05-24")
    private ZonedDateTime fromDate;
    @ApiModelProperty(example = "2020-10-01")
    private ZonedDateTime thruDate;
    @ApiModelProperty(example = "Active, Inactive", value = "Status of enrollment")
    private String status;
}
