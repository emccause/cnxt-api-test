package com.maritz.culturenext.dto;

public class FilterDetailsDTO {
    private Boolean enabled;
    private String name;

    public FilterDetailsDTO() {}

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
