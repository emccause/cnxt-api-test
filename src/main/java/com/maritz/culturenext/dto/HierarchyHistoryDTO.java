package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.ZonedDateTime;

@ApiModel("Hierarchy History")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class HierarchyHistoryDTO {
    private ZonedDateTime fromDate;
    private ZonedDateTime thruDate;
    private String controlNumber;
}
