package com.maritz.culturenext.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@ApiModel("Role")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RoleDTO {
    @ApiModelProperty(required = true, example = "2")
    private Long id;

    @ApiModelProperty(example = "CADM")
    private String code;

    @ApiModelProperty(example = "Client Admin")
    private String name;
}
