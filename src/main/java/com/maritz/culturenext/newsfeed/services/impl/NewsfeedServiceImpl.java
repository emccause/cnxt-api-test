package com.maritz.culturenext.newsfeed.services.impl;

import java.util.List;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.services.NewsfeedService;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.constants.ProjectConstants;

@Component
public class NewsfeedServiceImpl implements NewsfeedService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<ProgramMisc> programMiscDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;

    @Override
    @Transactional
    public NewsfeedItem createNewsfeedItemsForNomination(@NotNull Nomination nomination
            , @NotNull NewsfeedItemTypeCode newsfeedType) {
        
        NewsfeedItem newsfeedItem = new NewsfeedItem();
        newsfeedItem.setFromPaxId(nomination.getSubmitterPaxId());
        if (nomination.getHeadlineComment() == null) { // save an empty string if it's null, just in case
            newsfeedItem.setMessage(ProjectConstants.EMPTY_STRING);
        }
        else {
            newsfeedItem.setMessage(nomination.getHeadlineComment());    
        }
        newsfeedItem.setNewsfeedItemTypeCode(newsfeedType.name());
        newsfeedItem.setStatusTypeCode(nomination.getStatusTypeCode());
        newsfeedItem.setTargetId(nomination.getId());
        newsfeedItem.setTargetTable(TableName.NOMINATION.name());
        
        // Newsfeed visibility preference for program
        String programNewsfeedVisibility = programMiscDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(nomination.getProgramId())
                .and(ProjectConstants.VF_NAME).eq(NominationConstants.NEWSFEED_VISIBILITY_PROGRAM_MISC_NAME)
                .findOne(ProjectConstants.MISC_DATA, String.class);
        
        // set default program visibility as recipient preferences.
        programNewsfeedVisibility = programNewsfeedVisibility == null ? 
                NewsfeedVisibility.RECIPIENT_PREF.name() : programNewsfeedVisibility;
        
        newsfeedItem.setNewsfeedVisibility(programNewsfeedVisibility);
        
        // save newsfeedItem to get an id.
        newsfeedItemDao.save(newsfeedItem);
        
        List<Recognition> recognitionsList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                .find();
        if (!CollectionUtils.isEmpty(recognitionsList)) {
    
            Boolean recognitionApproved = false;
            for (Recognition recognition : recognitionsList ) {
                recognitionApproved |= ProjectConstants.APPROVED_STATUSES.contains(recognition.getStatusTypeCode());
            }
            
            // If at least one recognition is approved, newsfeed item is approved.
            if (recognitionApproved && 
                    StatusTypeCode.PENDING.name().equalsIgnoreCase(newsfeedItem.getStatusTypeCode())) {
                newsfeedItem.setStatusTypeCode(StatusTypeCode.APPROVED.name());
            }
            
            newsfeedItemDao.update(newsfeedItem);
        }
        else {
            logger.error("Not able to create newsfeedItemPax records, there is not a recognition for nomination " + 
                    nomination.getId());
        }
        
        return newsfeedItem;
    }

}
