package com.maritz.culturenext.newsfeed.services;


import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;

public interface NewsfeedService {

    /**
     *  Create newsfeedItem and newsfeedItemPax for nomination.
     *  @param nomination
     *  @param newsfeedType
     *  ToPax field will be determined according to program and pax preferences.
     *  @return newsfeedItem - created object.
     */
    NewsfeedItem createNewsfeedItemsForNomination(Nomination nomination, NewsfeedItemTypeCode newsfeedType);
}
