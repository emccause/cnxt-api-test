package com.maritz.culturenext.newsfeed.rest;

import java.util.List;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.services.RaiseService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Api(value = "/participants", description = "all calls dealing with raise recipients")
@RequestMapping("participants")
public class RecognitionRecipientsRestService {

    @Inject private RaiseService raiseService;
    @Inject private Security security;

    //rest/participants/~/activity/~/recipients
    @RequestMapping(method = RequestMethod.GET, value = "{paxId}/activity/{activityId}/recipients")
    @ApiOperation(value = "Returns the list of participants for a specific Activity")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully returned the list of possible recipients for a raise", 
                    response = RaiseDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "No content")
    })
    @Permission("PUBLIC")
    public List<EntityDTO> getRecognitionRecipients(
            @ApiParam(value = "PaxId for security purposes") @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @ApiParam(value = "ActivityId for the Activity you want") 
                @PathVariable(ACTIVITY_ID_REST_PARAM) String activityIdString,
            @ApiParam(value = "ExcludePrivate, if set to true, private recipients will not be included in the list") 
                @RequestParam(value = EXCLUDE_PRIVATE_REST_PARAM, required = false) Boolean excludePrivate,
                // TODO Pagination Techdebt: MP-7507 & MP-7524
            @ApiParam(value = "The number of people per page used to Paginate the request") 
                @RequestParam(value = SIZE_REST_PARAM, required = false) Integer size,
            @ApiParam(value = "Page of the people raise feed used to Paginate the request") 
                @RequestParam(value = PAGE_REST_PARAM, required = false) Integer page,
            @ApiParam(value = "The results will be filtered based on the approval statuses sent in") 
                @RequestParam(value = APPROVAL_STATUS_REST_PARAM, required = false) String approvalStatus
    ) throws Throwable {
        Long paxId = security.getPaxId(paxIdString);
        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        Long activityId = security.getId(activityIdString, null);
        if (activityId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }
        
        if(excludePrivate == null){
            excludePrivate = false; // default value false
        }
        
        return raiseService.getRecipientsForRaise(activityId, excludePrivate, approvalStatus, 
                size, page);
    }

}
