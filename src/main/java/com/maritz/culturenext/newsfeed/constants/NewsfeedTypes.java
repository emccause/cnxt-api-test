package com.maritz.culturenext.newsfeed.constants;

public enum NewsfeedTypes {
    DEFAULT,
    MANAGER,
    NETWORK,
    PUBLIC,
    RECEIVED,
    SELF
}
