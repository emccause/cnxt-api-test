package com.maritz.culturenext.newsfeed.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class NewsfeedConstants {
    
    public static final Integer DEFAULT_PAGE_NUMBER = 1;
    public static final Integer DEFAULT_PAGE_SIZE = 50;

    //Error Handling
    public static final String ERROR_INVALID_TYPE = "INVALID_TYPE";
    public static final String ERROR_INVALID_TYPE_MSG = "Please enter a valid type";
    public static final String ERROR_INVALID_ACTIVITY_TYPE = "INVALID_ACTIVITY_TYPE";
    public static final String ERROR_INVALID_ACTIVITY_TYPE_MSG = "Please enter a valid activityType";
    public static final String ERROR_ACTIVITY_NOTEXIST = "ACTIVITY_NOTEXIST";
    public static final String ERROR_ACTIVITY_NOTEXIST_MSG = "Activity feed does not exist for id provided";
    public static final String ERROR_INVALID_PAX_ID = "INVALID_PAX_ID";
    public static final String ERROR_INVALID_PAX_ID_MESSAGE = "Pax ID in path is invalid";
    public static final String ERROR_INVALID_LOGGED_IN_PAX_ID = "INVALID_LOGGED_IN_PAX_ID";
    public static final String ERROR_INVALID_LOGGED_IN_PAX_ID_MESSAGE = "Pax ID for user is invalid";

    public static final String RAISED_STATUS_NOT_ALLOWED = "NOT_ALLOWED";
    public static final String RAISED_STATUS_INELIGIBLE = "INELIGIBLE";
    public static final String RAISED_STATUS_ELIGIBLE = "ELIGIBLE";
    public static final String RAISED_STATUS_GIVEN = "GIVEN";

    public static final String LIST = "LIST";
    public static final String DETAIL = "DETAIL";
    
    public static final String GIVE = "GIVE";
    
    //Error Messages
    public static final ErrorMessage INVALID_LOGGED_IN_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(NewsfeedConstants.ERROR_INVALID_LOGGED_IN_PAX_ID)
        .setField(ProjectConstants.LOGGED_IN_PAX_ID)
        .setMessage(NewsfeedConstants.ERROR_INVALID_LOGGED_IN_PAX_ID_MESSAGE);
    
    public static final ErrorMessage INVALID_PAX_ID_MESSAGE = new ErrorMessage()
        .setCode(NewsfeedConstants.ERROR_INVALID_PAX_ID)
        .setField(ProjectConstants.PAX_ID)
        .setMessage(NewsfeedConstants.ERROR_INVALID_PAX_ID_MESSAGE);
    
    public static final ErrorMessage INVALID_TYPE_MESSAGE = new ErrorMessage()
        .setCode(NewsfeedConstants.ERROR_INVALID_TYPE)
        .setField(ProjectConstants.TYPE)
        .setMessage(NewsfeedConstants.ERROR_INVALID_TYPE_MSG);
    
    public static final ErrorMessage INVALID_ACTIVITY_TYPE_MESSAGE = new ErrorMessage()
        .setCode(NewsfeedConstants.ERROR_INVALID_ACTIVITY_TYPE)
        .setField(ProjectConstants.ACTIVITY_TYPE)
        .setMessage(NewsfeedConstants.ERROR_INVALID_ACTIVITY_TYPE_MSG);
    
    public static final ErrorMessage ACTIVITY_NOTEXIST_MESSAGE = new ErrorMessage()
        .setCode(NewsfeedConstants.ERROR_ACTIVITY_NOTEXIST)
        .setField(ProjectConstants.ACTIVITY_ID)
        .setMessage(NewsfeedConstants.ERROR_ACTIVITY_NOTEXIST_MSG);
}
