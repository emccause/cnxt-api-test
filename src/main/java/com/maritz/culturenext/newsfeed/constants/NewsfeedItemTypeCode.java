package com.maritz.culturenext.newsfeed.constants;

import java.util.ArrayList;
import java.util.List;

public enum NewsfeedItemTypeCode {
    AWARD_CODE,
    RECOGNITION
    ;
    
    public static List<String> allTypes() {
        List<String> typesAsString =  new ArrayList<String>();
        for (NewsfeedItemTypeCode type: values()) {
            typesAsString.add(type.name());
        }
        return typesAsString;
    }
}
