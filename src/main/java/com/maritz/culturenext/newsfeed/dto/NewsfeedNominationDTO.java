package com.maritz.culturenext.newsfeed.dto;

import java.util.List;

public class NewsfeedNominationDTO {
    private Long nominationId;
    private Long programId;
    private String programName;
    private Long imageId;
    private List<RecognitionCriteriaSimpleDTO> recognitionCriteria;
    private String headline;
    private String comment;
    private String awardCode;
    private String payoutType;
    private String payoutDisplayName;
    private String payoutDescription;
    
    public Long getNominationId() {
        return nominationId;
    }
    public void setNominationId(Long nominationId) {
        this.nominationId = nominationId;
    }
    public Long getProgramId() {
        return programId;
    }
    public void setProgramId(Long programId) {
        this.programId = programId;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public Long getImageId() {
        return imageId;
    }
    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }
    public List<RecognitionCriteriaSimpleDTO> getRecognitionCriteria() {
        return recognitionCriteria;
    }
    public void setRecognitionCriteria(
            List<RecognitionCriteriaSimpleDTO> recognitionCriteria) {
        this.recognitionCriteria = recognitionCriteria;
    }
    public String getHeadline() {
        return headline;
    }
    public void setHeadline(String headline) {
        this.headline = headline;
    }
    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }
    public String getAwardCode() {
        return awardCode;
    }
    public void setAwardCode(String awardCode) {
        this.awardCode = awardCode;
    }
    public String getPayoutType() {
        return payoutType;
    }
    public void setPayoutType(String payoutType) {
        this.payoutType = payoutType;
    }
    public String getPayoutDisplayName() {
        return payoutDisplayName;
    }
    public void setPayoutDisplayName(String payoutDisplayName) {
        this.payoutDisplayName = payoutDisplayName;
    }
    public String getPayoutDescription() {
        return payoutDescription;
    }
    public void setPayoutDescription(String payoutDescription) {
        this.payoutDescription = payoutDescription;
    }
}
