package com.maritz.culturenext.newsfeed.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class PersonalActivityDTO {
    private Integer directReportCount;
    private EmployeeDTO likePax;
    private Long myCommentCount;
    private Long myRaisedCount;
    private Double pointsIssued;
    private Double raisedPointTotal;
    private String raiseStatus;
    private EmployeeDTO raisePax;
    private EmployeeDTO toPax;

    public Integer getDirectReportCount() {
        return directReportCount;
    }

    public void setDirectReportCount(Integer directReportCount) {
        this.directReportCount = directReportCount;
    }

    public EmployeeDTO getLikePax() {
        return likePax;
    }

    public void setLikePax(EmployeeDTO likePax) {
        this.likePax = likePax;
    }

    public Long getMyCommentCount() {
        return myCommentCount;
    }

    public void setMyCommentCount(Long myCommentCount) {
        this.myCommentCount = myCommentCount;
    }

    public Long getMyRaisedCount() {
        return myRaisedCount;
    }

    public void setMyRaisedCount(Long myRaisedCount) {
        this.myRaisedCount = myRaisedCount;
    }

    public Double getPointsIssued() {
        return pointsIssued;
    }

    public void setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
    }

    public Double getRaisedPointTotal() {
        return raisedPointTotal;
    }

    public void setRaisedPointTotal(Double raisedPointTotal) {
        this.raisedPointTotal = raisedPointTotal;
    }

    public String getRaiseStatus() {
        return raiseStatus;
    }

    public void setRaiseStatus(String raisedStatus) {
        this.raiseStatus = raisedStatus;
    }

    public EmployeeDTO getRaisePax() {
        return raisePax;
    }

    public void setRaisePax(EmployeeDTO raisedPax) {
        this.raisePax = raisedPax;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }
}
