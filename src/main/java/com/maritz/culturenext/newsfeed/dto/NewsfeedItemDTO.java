package com.maritz.culturenext.newsfeed.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

public class NewsfeedItemDTO {
    private Long id;
    private String activityType;
    private Integer recipientCount;
    private Integer approvedRecipientCount;
    private String createDate; // "2015-05-05T03:14:55.223Z"
    private EmployeeDTO fromPax;
    private NewsfeedNominationDTO nomination;
    private Long likeCount;
    private Long commenterCount;
    private Long commentCount;
    private Long raisedCount;
    private CommentDTO comment;
    private PersonalActivityDTO personalActivity;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getActivityType() {
        return activityType;
    }
    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }
    public Integer getRecipientCount() {
        return recipientCount;
    }
    public void setRecipientCount(Integer recipientCount) {
        this.recipientCount = recipientCount;
    }
    public Integer getApprovedRecipientCount() {
        return approvedRecipientCount;
    }
    public void setApprovedRecipientCount(Integer approvedRecipientCount) {
        this.approvedRecipientCount = approvedRecipientCount;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public EmployeeDTO getFromPax() {
        return fromPax;
    }
    public void setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
    }
    public NewsfeedNominationDTO getNomination() {
        return nomination;
    }
    public void setNomination(NewsfeedNominationDTO nomination) {
        this.nomination = nomination;
    }

    public Long getLikeCount() {
        return likeCount;
    }
    public void setLikeCount(Long likeCount) {
        this.likeCount = likeCount;
    }

    public Long getRaisedCount() {
        return raisedCount;
    }

    public void setRaisedCount(Long raisedCount) {
        this.raisedCount = raisedCount;
    }

    public Long getCommenterCount() {
        return commenterCount;
    }
    public void setCommenterCount(Long commenterCount) {
        this.commenterCount = commenterCount;
    }
    
    public Long getCommentCount() { return commentCount; }
    public void setCommentCount(Long commentCount) { this.commentCount = commentCount; }
    public CommentDTO getComment() { return comment; }
    public void setComment(CommentDTO comment) { this.comment = comment; }

    public PersonalActivityDTO getPersonalActivity() {
        return personalActivity;
    }
    public void setPersonalActivity(PersonalActivityDTO personalActivity) {
        this.personalActivity = personalActivity;
    }
}
