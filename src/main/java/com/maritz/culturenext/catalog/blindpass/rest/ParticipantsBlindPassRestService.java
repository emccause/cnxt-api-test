package com.maritz.culturenext.catalog.blindpass.rest;

import javax.inject.Inject;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.catalog.blindpass.dto.CatalogBlindPassCardTypeDTO;
import com.maritz.culturenext.catalog.blindpass.service.CatalogBlindPassService;

@RestController
@RequestMapping("/participants")
public class ParticipantsBlindPassRestService {
    final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Inject private CatalogBlindPassService catalogService;
    
    //rest/participants/~/card-types/~
    @RequestMapping(value = "{"+ PAX_ID_REST_PARAM +"}/card-types/{"+ CARD_TYPE_CODE_REST_PARAM +"}"
            , method = RequestMethod.GET)
    @Permission(isPublic = true)
    @PreAuthorize("@security.isMyPax(#paxId) and !@security.isImpersonated()")
    public CatalogBlindPassCardTypeDTO getCatalogBlindPass(
            @PathVariable(PAX_ID_REST_PARAM) String paxId, 
            @PathVariable(CARD_TYPE_CODE_REST_PARAM) String cardTypeCode) throws Exception {
        
        return catalogService.getCatalogBlindPass(cardTypeCode);
    }

}
