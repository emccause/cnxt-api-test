package com.maritz.culturenext.catalog.blindpass.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.RestParameterConstants;

public class CatalogBlindPassConstants {
    
    public static final String PARAM_NAME_USERNAME = "USERNAME";
    public static final String PARAM_NAME_PASSWORD = "PASSWORD";
    public static final String PARAM_NAME_JWT = "jwt";
    public static final String PARAM_NAME_REDIRECT_LINK = "redirectLink";
    public static final String REDIRECT_POST = "FORM_POST";
    public static final String REDIRECT_SIMPLE = "SIMPLE";
    
    // Error Messages
    public static final String ERROR_CATALOG_DATA_MISSED_CODE = "ERROR_CATALOG_DATA_MISSED";
    public static final String ERROR_CATALOG_DATA_MISSED_MSG = 
            "Catalog login information for card type code is missed. Check the logs for more information";
    
    public static final ErrorMessage ERROR_CATALOG_DATA_MISSED =  new ErrorMessage()
            .setCode(ERROR_CATALOG_DATA_MISSED_CODE)
            .setField(RestParameterConstants.CARD_TYPE_CODE_REST_PARAM)
            .setMessage(ERROR_CATALOG_DATA_MISSED_MSG);

}
