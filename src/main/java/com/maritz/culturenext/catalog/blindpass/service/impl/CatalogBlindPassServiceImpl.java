package com.maritz.culturenext.catalog.blindpass.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.CardType;
import com.maritz.core.jpa.entity.PaxAccount;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.CardTypeRepository;
import com.maritz.core.jpa.repository.PaxAccountRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.catalog.blindpass.constants.CatalogBlindPassConstants;
import com.maritz.culturenext.catalog.blindpass.dto.CatalogBlindPassCardTypeDTO;
import com.maritz.culturenext.catalog.blindpass.service.CatalogBlindPassService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.wallet.dto.CatalogBlindPassDTO;
import com.maritz.wallet.dto.ParamDTO;
import com.maritz.wallet.services.WalletService;

import io.jsonwebtoken.Claims;

@Service
public class CatalogBlindPassServiceImpl implements CatalogBlindPassService {
    final Logger logger = LoggerFactory.getLogger(getClass());
    
	protected static final String DESTINATION_KEY_AHQ = "AHQ";
	protected static final String DESTINATION_KEY_GC = "GLOBALCATALOG";
	
	protected static final String COUNTRY_CODE_US = "US";
	protected static final String COUNTRY_CODE_PR = "PR";
    
    @Inject private AddressRepository addressRepository;
    @Inject private ApplicationDataService applicationDataService;
    @Inject private CardTypeRepository cardTypeRespository;
    @Inject private PaxAccountRepository paxAccountRepository;
    @Inject private Security security;
    @Inject private WalletService walletService;
    @Inject private PaxRepository paxRepository;
    
    @Inject @Named("rideauTokenService") private TokenService rideauTokenService;
    
    @Override
    public CatalogBlindPassCardTypeDTO getCatalogBlindPass(String cardTypeCode) {
        
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();
        
        CatalogBlindPassCardTypeDTO blindPass = new CatalogBlindPassCardTypeDTO();
        
        // ----- Adding card description value -----
        CardType cardTypeEntity = cardTypeRespository.findByCardTypeCode(cardTypeCode);
        if (cardTypeEntity != null) {
            blindPass.setCardDescription(cardTypeEntity.getCardTypeDesc());
        }

        if (ProjectConstants.RIDEAU_CARD_TYPE.equals(cardTypeCode)) {
            
            blindPass
                .setCardTypeCode(cardTypeCode)
                .setRedirectMethod(CatalogBlindPassConstants.REDIRECT_POST);
            
            // ----- Set country code -----
            Address paxAddress = addressRepository.findPreferredByPaxId(security.getPaxId());
            if (paxAddress != null) {
                blindPass.setCurrentCountryCode(paxAddress.getCountryCode());
            }

            // ----- Pax account card name. -----
            PaxAccount paxAccount = paxAccountRepository.findIssuedByPaxIdAndCardTypeCode(
                    security.getPaxId(), cardTypeCode);
            if (paxAccount != null) {
                blindPass.setCardName(paxAccount.getCardName());
            }
            else {
                logger.error("Pax account does not exist for paxId " + security.getPaxId() 
                        + " and card type "+ cardTypeCode);
            }
            
            // ----- Set service anniversary base url. -----
            String baseUrl = 
                    applicationDataService
                    .getApplicationData(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL)
                    .getValue();
            if (baseUrl != null) {
                blindPass.setCatalogBaseURL(baseUrl);
            }
            else {
                logger.error("Service anniversary base Url has not been set");
            }
            
            // ----- Generate JWT token for rideau catalog -----
            String token = null;
            String rideauIssuer = 
                    applicationDataService
                        .getApplicationData(ApplicationDataConstants.KEY_NAME_RIDEAU_TOKEN_ISSUER)
                        .getValue();
            
            if (rideauIssuer != null) {
        
                Claims claims = rideauTokenService.createClaims();
                claims.setIssuer(rideauIssuer);
                claims.setSubject(security.getUserName());
                token = rideauTokenService.createToken(claims);
                
                if (token != null) {
                    blindPass.addParam(new ParamDTO()
                            .setName(CatalogBlindPassConstants.PARAM_NAME_JWT)
                            .setValue(token));
                }
                else {
                    logger.error("JWT token is not generated.");
                }
            }
            else {
                logger.error("RIDEAU issuer has not been set.");
            }
            // ----- Adding redirect link param -----
            String redirectLink = 
                    applicationDataService
                    .getApplicationData(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_REDIRECT_LINK)
                    .getValue();
            
            if (redirectLink != null) {
                blindPass.addParam(new ParamDTO()
                        .setName(CatalogBlindPassConstants.PARAM_NAME_REDIRECT_LINK)
                        .setValue(redirectLink));
            }
            
            if (paxAccount == null || baseUrl == null || token == null) {
                errors.add(CatalogBlindPassConstants.ERROR_CATALOG_DATA_MISSED);
            }
            
            // ----- Handle any errors -----
            ErrorMessageException.throwIfHasErrors(errors);

        }
        else {
        	Pax pax = paxRepository.findOne(security.getPaxId()); 
        	
        	String countryCode = null;  
        	String destinationType = null; 
            Address paxAddress = addressRepository.findPreferredByPaxId(security.getPaxId()); 
            if (paxAddress != null) {
            	countryCode =paxAddress.getCountryCode();
            	if (countryCode != null || countryCode.equalsIgnoreCase(COUNTRY_CODE_US)
            	    			|| countryCode.equalsIgnoreCase(COUNTRY_CODE_PR)) {
            		destinationType = DESTINATION_KEY_AHQ;
            	} else {
            		destinationType = DESTINATION_KEY_GC;
            	}
            }
        	
            // Using WalletService for other card types but RIDEAU.
        	///(replaced)  convertBlindPassObj(blindPass, walletService.getCatalogBlindPass(cardTypeCode));
        	convertBlindPassObj(blindPass, walletService.getCatalogBlindPass(pax,cardTypeCode, destinationType,""));
        }
        
        return blindPass;
    }

    /**
     * Convert wallet service's dto object to catalogBlindPass dto. 
     * @param blindPass
     * @param walletCatalogBlindPass
     */
    private void convertBlindPassObj(CatalogBlindPassCardTypeDTO blindPass, CatalogBlindPassDTO walletCatalogBlindPass) {
        blindPass.setCardName(walletCatalogBlindPass.getCardName());
        blindPass.setCardTypeCode(walletCatalogBlindPass.getCardType());
        blindPass.setCatalogBaseURL(walletCatalogBlindPass.getCatalogBaseURL());
        blindPass.setCurrentCountryCode(walletCatalogBlindPass.getCurrentCountryCode());
        blindPass.setParams(walletCatalogBlindPass.getParams());
        blindPass.setRedirectMethod(walletCatalogBlindPass.getRedirectMethod());
    }
}
