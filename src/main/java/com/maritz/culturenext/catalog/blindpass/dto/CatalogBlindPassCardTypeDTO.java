package com.maritz.culturenext.catalog.blindpass.dto;

import java.util.ArrayList;
import java.util.Collection;

import com.maritz.wallet.dto.ParamDTO;

public class CatalogBlindPassCardTypeDTO {
    
    private String cardTypeCode;
    private String cardName; 
    private String cardDescription;
    private String currentCountryCode = "US";
    private String redirectMethod;
    private String catalogBaseURL;
    private Collection<ParamDTO> params = new ArrayList<>();
    
    public String getCardTypeCode() {
        return cardTypeCode;
    }
    
    public CatalogBlindPassCardTypeDTO setCardTypeCode(String cardTypeCode) {
        this.cardTypeCode = cardTypeCode;
        return this;
    }
    
    public String getCardName() {
        return cardName;
    }
    
    public CatalogBlindPassCardTypeDTO setCardName(String cardName) {
        this.cardName = cardName;
        return this;
    }
    
    public String getCurrentCountryCode() {
        return currentCountryCode;
    }
    
    public CatalogBlindPassCardTypeDTO setCurrentCountryCode(String currentCountryCode) {
        this.currentCountryCode = currentCountryCode;
        return this;
    }
    
    public String getRedirectMethod() {
        return redirectMethod;
    }
    
    public CatalogBlindPassCardTypeDTO setRedirectMethod(String redirectMethod) {
        this.redirectMethod = redirectMethod;
        return this;
    }
    
    public String getCatalogBaseURL() {
        return catalogBaseURL;
    }
    
    public CatalogBlindPassCardTypeDTO setCatalogBaseURL(String catalogBaseURL) {
        this.catalogBaseURL = catalogBaseURL;
        return this;
    }
    
    public Collection<ParamDTO> getParams() {
        return params;
    }
    
    public CatalogBlindPassCardTypeDTO setParams(Collection<ParamDTO> params) {
        this.params.clear();
        if (params != null) this.params.addAll(params);
        return this;
    }
    
    public CatalogBlindPassCardTypeDTO addParam(ParamDTO param) {
        if (param != null) params.add(param);
        return this;
    }

    public String getCardDescription() {
        return cardDescription;
    }

    public CatalogBlindPassCardTypeDTO setCardDescription(String cardDescription) {
        this.cardDescription = cardDescription;
        return this;
    }

}

