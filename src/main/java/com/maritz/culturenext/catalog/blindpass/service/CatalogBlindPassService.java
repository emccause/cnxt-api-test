package com.maritz.culturenext.catalog.blindpass.service;

import com.maritz.culturenext.catalog.blindpass.dto.CatalogBlindPassCardTypeDTO;

public interface CatalogBlindPassService {
    
    /**
     * Retrieve the details to blind pass the user to the appropriate catalog site
     * @param cardTypeCode
     * 
     * TODO Confluence Documentation
     */
    CatalogBlindPassCardTypeDTO getCatalogBlindPass(String cardTypeCode);
}
