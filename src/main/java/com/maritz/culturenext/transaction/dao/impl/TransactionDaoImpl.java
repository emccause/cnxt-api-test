package com.maritz.culturenext.transaction.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.transaction.dao.TransactionDao;


@Repository
public class TransactionDaoImpl extends AbstractDaoImpl implements TransactionDao{
    
    private static final String TRANSACTION_HEADER_ID_PLACEHOLDER = "TRANSACTION_HEADER_ID";
    
    private static final String QUERY =
    "SELECT DISTINCT " +
    "th.ID AS 'TRANSACTION_ID',  " +
    "th.CREATE_DATE, " +
    "d_pl.AMOUNT AS 'AWARD_AMOUNT', " +
    "THM.misc_data as 'PAYOUT_TYPE', " +
    "prgm.PROGRAM_NAME, " +
    "th.STATUS_TYPE_CODE AS 'STATUS' , " +
    "N.comment as 'COMMENT', " +
    "CASE WHEN TH.STATUS_TYPE_CODE = 'REVERSED'  " +
        "THEN AH_REVERSED.APPROVAL_NOTES " +
        "ELSE '' " +
    "END AS 'REVERSAL_COMMENT', " + 
    "CASE " +
        "WHEN pt.PROGRAM_TYPE_NAME = 'POINT_LOAD' THEN d_pl.DESCRIPTION " +
        "ELSE n.HEADLINE_COMMENT " +
    "END AS 'HEADLINE', " +
    "r_pax.PAX_ID AS 'TO_PAX', " + 
    "s_pax.PAX_ID AS 'SUBMITTER_PAX_ID', " +
    "a_pax.PAX_ID AS 'APPROVER_PAX_ID' " +
    "FROM PROGRAM prgm " +
    "JOIN PROGRAM_TYPE pt ON pt.PROGRAM_TYPE_CODE = prgm.PROGRAM_TYPE_CODE " +
    "LEFT JOIN NOMINATION n ON n.PROGRAM_ID = prgm.PROGRAM_ID " +
    "LEFT JOIN TRANSACTION_HEADER th_pl ON th_pl.PROGRAM_ID = prgm.PROGRAM_ID " +
        "AND th_pl.TYPE_CODE = 'DISC' AND th_pl.SUB_TYPE_CODE = 'DISC' " +
    "LEFT JOIN DISCRETIONARY d_pl ON d_pl.TRANSACTION_HEADER_ID = th_pl.ID  " +
    "LEFT JOIN TRANSACTION_HEADER th  " +
        "ON th.ID = d_pl.TRANSACTION_HEADER_ID  " +
        "AND th.TYPE_CODE = 'DISC' " +
    "LEFT JOIN TRANSACTION_HEADER_MISC THM ON th.ID = THM.TRANSACTION_HEADER_ID AND THM.VF_NAME='PAYOUT_TYPE' " +
    "LEFT JOIN TRANSACTION_HEADER_EARNINGS the ON the.TRANSACTION_HEADER_ID = th.ID " +
    "LEFT JOIN PAX_GROUP rec_pg ON rec_pg.PAX_GROUP_ID = th.PAX_GROUP_ID " +
    "LEFT JOIN PAX r_pax ON r_pax.PAX_ID = rec_pg.PAX_ID " +
    "LEFT JOIN PAX s_pax ON s_pax.PAX_ID = n.SUBMITTER_PAX_ID " +
    "LEFT JOIN APPROVAL_HISTORY ah ON ah.TARGET_ID = d_pl.TARGET_ID AND ah.TARGET_TABLE = 'RECOGNITION' AND d_pl.TARGET_TABLE = 'RECOGNITION' " +
    "LEFT JOIN APPROVAL_PENDING ap ON ap.TARGET_ID = d_pl.TARGET_ID AND ap.TARGET_TABLE = 'RECOGNITION' AND d_pl.TARGET_TABLE = 'RECOGNITION' " +
    "LEFT JOIN PAX a_pax ON a_pax.PAX_ID = ISNULL(ah.PAX_ID, ap.PAX_ID) " +
    "LEFT JOIN APPROVAL_HISTORY AH_REVERSED  " +
        "ON AH_REVERSED.TARGET_ID  = th.ID AND ah_reversed.TARGET_TABLE = 'TRANSACTION_HEADER' " +
        "AND AH_REVERSED.STATUS_TYPE_CODE = 'REVERSED' " +
    "WHERE th.TYPE_CODE = 'DISC' AND pt.PROGRAM_TYPE_NAME = 'POINT_LOAD' " +
    "AND th.ID = :" + TRANSACTION_HEADER_ID_PLACEHOLDER;

    @Override
    public List<Map<String, Object>> getTransactionById(Long transactionId) {
        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue(TRANSACTION_HEADER_ID_PLACEHOLDER, transactionId);
            
        return namedParameterJdbcTemplate.query(QUERY, params, new CamelCaseMapRowMapper());
    }
    

}
