package com.maritz.culturenext.transaction.dao.impl;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;

@Repository
public class TransactionHeaderMiscDaoImpl extends AbstractDaoImpl implements TransactionHeaderMiscDao {
    
    private static final String CREATE_THM_PAYOUT_TYPE =
            "EXEC component.UP_CREATE_THM_PAYOUT_TYPE";
    
    private static final String CREATE_THM_DEFAULT_PROJECT_NUMBER = 
            "EXEC component.UP_CREATE_THM_DEFAULT_PROJECT_NUMBER";
            
    private static final String CREATE_THM_DEFAULT_SUB_PROJECT_NUMBER = 
            "EXEC component.UP_CREATE_THM_DEFAULT_SUB_PROJECT_NUMBER";
            
    private static final String CREATE_THM_US_OVERRIDE_PROJECT_NUMBER =
            "EXEC component.UP_CREATE_THM_US_OVERRIDE_PROJECT_NUMBER";
    
    private static final String CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER =
            "EXEC component.UP_CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER";
            
    private static final String CREATE_THM_NON_US_OVERRIDE_PROJECT_NUMBER =
            "EXEC component.UP_CREATE_THM_NON_US_OVERRIDE_PROJECT_NUMBER";

    private static final String CREATE_THM_NON_US_OVERRIDE_SUB_PROJECT_NUMBER =
            "EXEC component.UP_CREATE_THM_NON_US_OVERRIDE_SUB_PROJECT_NUMBER";

    private static final String CREATE_THM_CONFIGURABLE_SUB_PROJECT_NUMBER = 
            "EXEC component.UP_CALCULATE_SUB_PROJECT_OVERRIDE";
    
    @Override
    public void createPayoutType() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(CREATE_THM_PAYOUT_TYPE, params);
    }

    @Override
    public void createDefaultProjectSubproject() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(CREATE_THM_DEFAULT_PROJECT_NUMBER, params);
        namedParameterJdbcTemplate.update(CREATE_THM_DEFAULT_SUB_PROJECT_NUMBER, params);
    }

    @Override
    public void createUsOverrideProjectSubproject() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(CREATE_THM_US_OVERRIDE_PROJECT_NUMBER, params);
        namedParameterJdbcTemplate.update(CREATE_THM_US_OVERRIDE_SUB_PROJECT_NUMBER, params);        
    }

    @Override
    public void createNonUsOverrideProjectSubproject() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(CREATE_THM_NON_US_OVERRIDE_PROJECT_NUMBER, params);
        namedParameterJdbcTemplate.update(CREATE_THM_NON_US_OVERRIDE_SUB_PROJECT_NUMBER, params);        
    }

    @Override
    public void createConfigurableSubprojectOverride() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        namedParameterJdbcTemplate.update(CREATE_THM_CONFIGURABLE_SUB_PROJECT_NUMBER, params);    
    }
}
