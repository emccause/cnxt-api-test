package com.maritz.culturenext.transaction.dao;

public interface TransactionHeaderMiscDao {
    
    /**
     * Creates TRANSACTION_HEADER_MISC records for any TRANSACTION_HEADER_IDs that are missing them:
     * PAYOUT_TYPE - Pulled from the associated PROGRAM_MISC
     */
    void createPayoutType();

    /**
     * Creates TRANSACTION_HEADER_MISC records for any TRANSACTION_HEADER_IDs that are missing them:
     * DEFAULT_PROJECT_NUMBER and DEFAULT_SUB_PROJECT_NUMBER 
     * These values are pulled from the associated BUDGET
     */
    void createDefaultProjectSubproject();
    
    /**
     * Creates TRANSACTION_HEADER_MISC records for any TRANSACTION_HEADER_IDs that are missing them:
     * BUDGET_OVERRIDE_PROJECT_NUMBER and BUDGET_OVERRIDE_SUB_PROJECT_NUMBER (for US overrides)
     * These values are pulled from BUDGET_MISC if the associated budget has been set up with a US accounting override
     */
    void createUsOverrideProjectSubproject();
    
    /**
     * Creates TRANSACTION_HEADER_MISC records for any TRANSACTION_HEADER_IDs that are missing them:
     * BUDGET_OVERRIDE_PROJECT_NUMBER and BUDGET_OVERRIDE_SUB_PROJECT_NUMBER (for non-US overrides)
     * These values are pulled from BUDGET_MISC if the associated budget has been set up with a non-US accounting override
     */
    void createNonUsOverrideProjectSubproject();
    
    /**
     * Creates TRANSACTION_HEADER_MISC records for any TRANSACTION_HEADER_IDs that are missing them:
     * CONFIGURABLE_SUB_PROJECT_NUMBER
     * This uses the value in APPLICATION_DATA (vendorprocessing.subproject.override) to determine how to calculate the sub project number
     */
    void createConfigurableSubprojectOverride();
}
