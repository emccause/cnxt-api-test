package com.maritz.culturenext.transaction.dao;

import java.util.List;
import java.util.Map;

public interface TransactionDao {
    
    /**
     * Return TransactionDTO with details by transaction id 
     * 
     * @param transactionId - transaction id to get transaction details
     * 
     * @return Return a TransactionDTO
     *
     * https://maritz.atlassian.net/wiki/display/M365/GET+Transaction+by+ID
     */
    List<Map<String, Object>> getTransactionById(Long transactionId);
    }
