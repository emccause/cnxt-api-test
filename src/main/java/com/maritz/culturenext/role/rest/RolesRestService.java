package com.maritz.culturenext.role.rest;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.role.services.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Set;

import static com.maritz.culturenext.permission.constants.PermissionConstants.MARITZ_ADMIN_ROLE_NAME;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@RestController
@Api(value="/culturenext-roles")
@Slf4j
public class RolesRestService {

    public static final String ADMINISTRABLE_ROLE_FIELD = "ADMINISTRABLE";
    public static final String ADMINISTRABLE_VALUE_NOT_SUPPORTED_CODE = "ADMINISTRABLE_VALUE_NOT_SUPPORTED";
    public static final String ADMINISTRABLE_VARIABLE_VALUE_NOT_SUPPORTED_MESSAGE = "Administrable variable value not supported.";
    private final String ROLES_URI_BASE = "/culturenext-roles";
    @Inject
    private RoleService roleService;

    // /rest/culturenext-roles?administrable=
    @PreAuthorize("@security.hasRole('" + MARITZ_ADMIN_ROLE_NAME + "')")
    @GetMapping(value = ROLES_URI_BASE, produces = APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get roles")
    public Set<RoleDTO> getAdministeredRoles(
            @ApiParam(value = "type of roles the user is interested in", required = false, defaultValue = "false")
            boolean administrable) throws Throwable {
        if (!administrable){
            ArrayList<ErrorMessage> errors = new ArrayList<ErrorMessage>();
            errors.add(new ErrorMessage(ADMINISTRABLE_ROLE_FIELD, ADMINISTRABLE_VALUE_NOT_SUPPORTED_CODE,
                    ADMINISTRABLE_VARIABLE_VALUE_NOT_SUPPORTED_MESSAGE));
            ErrorMessageException.throwIfHasErrors(errors);
        }
        Set<RoleDTO> result = roleService.getRoles(administrable);
        return result;
    }


}
