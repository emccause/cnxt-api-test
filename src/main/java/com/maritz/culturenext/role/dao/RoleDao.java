package com.maritz.culturenext.role.dao;

import com.maritz.culturenext.dto.RoleDTO;

import java.util.List;
import java.util.Set;

public interface RoleDao {
    /**
     * get roles
     * @param administrable true:administrable roles
     * @return
     */
    Set<RoleDTO> getRoles(boolean administrable);

    Set<Long> getAdministrableRoleIds();

    List<RoleDTO> getRolesForControlNumber(String controlNumber);
}
