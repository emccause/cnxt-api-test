package com.maritz.culturenext.role.dao.impl;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jpa.repository.RoleRepository;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.role.dao.RoleDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class RoleDaoImpl extends AbstractDaoImpl implements RoleDao {

    private static final String CONTROL_NUMBER_PARAM = "CONTROL_NUMBER";

    private static final String PARTICIPANT_OVERVIEW_ROLE_QUERY =
            "SELECT DISTINCT r.ROLE_ID AS ID, " +
                    "r.ROLE_CODE AS CODE, " +
                    "r.ROLE_NAME AS NAME " +
                    "FROM PAX p " +
                    "JOIN VW_PAX_ROLE vpr ON " +
                    "p.PAX_ID = vpr.PAX_ID " +
                    "JOIN [ROLE] r ON " +
                    "vpr.ROLE_ID = r.ROLE_ID " +
                    "WHERE (vpr.SUBJECT_TABLE IN ('PAX','GROUPS') OR vpr.SUBJECT_TABLE IS NULL) " +
                    // granted directly to a person or through a group or a pax group (e.g. Maritz admin)
                    "AND vpr.TARGET_TABLE IS NULL " +
                    // general roles (i.e. not targeted to a specific entity)
                    "AND p.CONTROL_NUM = :" + CONTROL_NUMBER_PARAM;
    @Inject
    private RoleRepository roleRepository;

    @Value("${roles.administrable}")
    private String[] administrableRoles;

    private Set<RoleDTO> administrableRolesDTOList;

    public Set<Long> getAdministrableRoleIds() {
        return adminstrableRoleIds;
    }

    private Set<Long> adminstrableRoleIds;

    @PostConstruct
    private void postConstruct(){
        administrableRolesDTOList = roleRepository.findBy().where("roleCode").in(this.administrableRoles).findAll()
          .stream().map(role ->
            new RoleDTO()
                    .setCode(role.getRoleCode())
                    .setId(role.getRoleId())
                    .setName(role.getRoleName())).collect(Collectors.toSet());
        adminstrableRoleIds = administrableRolesDTOList.stream().map(RoleDTO::getId).collect(Collectors.toSet());

    }

    @Override
    public Set<RoleDTO> getRoles(boolean isAdministrable) {
        return administrableRolesDTOList;
    }



    @Override
    public List<RoleDTO> getRolesForControlNumber(String controlNumber){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(CONTROL_NUMBER_PARAM, controlNumber);
        return (List<RoleDTO>)namedParameterJdbcTemplate.query(PARTICIPANT_OVERVIEW_ROLE_QUERY, params,
                new BeanPropertyRowMapper(RoleDTO.class));
    }
}
