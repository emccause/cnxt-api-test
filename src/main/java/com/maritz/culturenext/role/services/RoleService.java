package com.maritz.culturenext.role.services;

import com.maritz.culturenext.dto.RoleDTO;

import java.util.List;
import java.util.Set;

public interface RoleService {
    Set<RoleDTO> getRoles(boolean isAdministrable);

    List<RoleDTO> getRolesForControlNumber(String controlNumber);

    Set<Long> getAdministrableRoleIds();
}
