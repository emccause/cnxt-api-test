package com.maritz.culturenext.role.services.impl;

import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.role.dao.RoleDao;
import com.maritz.culturenext.role.services.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
public class RoleServiceImpl implements RoleService {
    @Inject
    private RoleDao roleDao;
    @Override
    public Set<RoleDTO> getRoles(boolean isAdministrable) {
        return roleDao.getRoles(isAdministrable);
    }

    @Override
    public List<RoleDTO> getRolesForControlNumber(String controlNumber) {
        return roleDao.getRolesForControlNumber(controlNumber);
    }

    @Override
    public Set<Long> getAdministrableRoleIds() {
        return roleDao.getAdministrableRoleIds();
    }
}
