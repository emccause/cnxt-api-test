package com.maritz.culturenext.caching.dto;

public class BudgetEligibilityCacheDTO {
    
    private Long programId;
    private Long budgetId;
    
    public Long getProgramId() {
        return programId;
    }
    
    public BudgetEligibilityCacheDTO setProgramId(Long programId) {
        this.programId = programId;
        return this;
    }
    
    public Long getBudgetId() {
        return budgetId;
    }
    
    public BudgetEligibilityCacheDTO setBudgetId(Long budgetId) {
        this.budgetId = budgetId;
        return this;
    }
}
