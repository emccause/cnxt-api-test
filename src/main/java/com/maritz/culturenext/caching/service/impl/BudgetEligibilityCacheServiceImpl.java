package com.maritz.culturenext.caching.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.jpa.entity.VwPaxProgramBudget;

@Component
public class BudgetEligibilityCacheServiceImpl implements BudgetEligibilityCacheService {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject @Named("budgetEligibilityCache") private Cache cache;
    @Inject ConcentrixDao<VwPaxProgramBudget> vwPaxProgramBudgetDao;

    CountDownLatch buildLatch = new CountDownLatch(0);

    @Override
    @PostConstruct
    public void buildCache() {

        buildLatch = new CountDownLatch(1);

        cache.clear();

        List<VwPaxProgramBudget> eligibilityList = vwPaxProgramBudgetDao.findBy().find();

        Map<Long, List<BudgetEligibilityCacheDTO>> cacheMap = new HashMap<>();

        for (VwPaxProgramBudget entry : eligibilityList) { // Parse list into map
            List<BudgetEligibilityCacheDTO> cacheList = cacheMap.get(entry.getPaxId());
            if (cacheList == null) {
                cacheList = new ArrayList<>();
            }

            Boolean found = false;
            for (BudgetEligibilityCacheDTO cacheEntry : cacheList) {
                if (cacheEntry.getProgramId().equals(entry.getProgramId())
                        && cacheEntry.getBudgetId().equals(entry.getBudgetId())
                    ) {
                    // Already exists, skip
                    found = true;
                    break;
                }
            }
            if (Boolean.FALSE.equals(found)) {
                // not found, create and add to cache list
                cacheList.add(
                        new BudgetEligibilityCacheDTO().setProgramId(entry.getProgramId()).setBudgetId(entry.getBudgetId())
                    );
            }
            cacheMap.put(entry.getPaxId(), cacheList); // store!
        }

        // put map in cache
        for (Long key : cacheMap.keySet()) {
            cache.put(key, cacheMap.get(key));
        }
        buildLatch.countDown();
    }

    @Override
    public List<BudgetEligibilityCacheDTO> getEligibilityForPax(Long paxId) {

        try {
            buildLatch.await();
        } catch (InterruptedException e) {
            logger.error("error with build latch", e);
        }

        ValueWrapper wrappedObject = cache.get(paxId);
        if (wrappedObject != null) {
            return (List<BudgetEligibilityCacheDTO>) wrappedObject.get();
        }
        // Nothing found in cache
        return null;
    }

}
