package com.maritz.culturenext.caching.service;

import java.util.List;

import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;

public interface BudgetEligibilityCacheService {
    
    /**
     * TODO javadocs
     */
    void buildCache();
    
    /**
     * TODO javadocs
     * 
     * @param paxId
     * @return
     */
    List<BudgetEligibilityCacheDTO> getEligibilityForPax(Long paxId);

}
