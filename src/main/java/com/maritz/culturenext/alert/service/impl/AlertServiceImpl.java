package com.maritz.culturenext.alert.service.impl;

import com.google.common.collect.Iterables;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.*;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.constants.AlertConstants;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.*;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.social.comment.constants.CommentConstants;
import com.maritz.culturenext.social.comment.services.CommentService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.*;

@Service
public class AlertServiceImpl implements AlertService {

    final Logger logger = LoggerFactory.getLogger(AlertServiceImpl.class.getName());

    private static final String MISSING_NOTIFICATION_WARN_MSG = "Could not find notification corresponding";

    @Inject private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Inject private CommentService commentService;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<AlertMisc> alertMiscDao;
    @Inject private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ProxyService proxyService;
    @Inject private RaiseDao raiseDao;
    @Inject private Security security;
    @Inject private AwardTypeService awardTypeService;

    @Nonnull
    @Override
    public List<AlertUpdateDTO> updateAlerts(Long paxId, List<AlertUpdateDTO> notificationDTOList) {

        if (security.isImpersonated() == true
                && !proxyService.doesPaxHaveProxyPermission(PermissionConstants.PROXY_APPROVE_REC, paxId, security.getImpersonatorPaxId())
                && !security.impersonatorHasPermission(PermissionManagementTypes.PROXY_APPROVE_REC_ANYONE.name())) {
            // make sure they have permission - either they've got the proxy
            // permission for the pax they're approving on behalf of or they're
            // an admin with the PROXY_APPROVE_REC_ANYONE permission
            List<ErrorMessage> errors = new ArrayList<>();
            errors.add(AlertConstants.PROXY_NOT_ALLOWED_MESSAGE);
            ErrorMessageException.throwIfHasErrors(errors);
        }

        // check if there are notifications to update
        if (notificationDTOList == null) {
            return new ArrayList<>();
        }

        for (AlertUpdateDTO notificationUpdateDTO : notificationDTOList) {
            // validate id and status
            List<ErrorMessage> errors = new ArrayList<>();

            Alert alert = null;

            // verify notification id
            Long id = notificationUpdateDTO.getId();

            if (id == null) {
                errors.add(AlertConstants.MISSING_ID_MESSAGE);
            } else {
                alert = retrieveAlertEntity(id, paxId);
                if (alert == null) {
                    errors.add(AlertConstants.INVALID_ID_MESSAGE);
                }
            }

            // verify status
            String updateStatus = notificationUpdateDTO.getStatus();
            List<ErrorMessage> statusErrors = verifyStatus(updateStatus);
            errors.addAll(statusErrors);

            // verify status transition (only if new status is valid and there exists a notification to update)
            if (statusErrors.isEmpty() && alert != null) {
                String existingAlertStatus = alert.getStatusTypeCode();
                errors.addAll(verifyStatusTransition(existingAlertStatus, updateStatus));

                // verify status transition for reported comment notifications.
                if (AlertSubType.REPORT_COMMENT_ABUSE.name().equals(alert.getAlertSubTypeCode())) {
                    errors.addAll(verifyReportAbuseTransition(alert, updateStatus));
                }
            }

            // handle any errors before updating the notification
            ErrorMessageException.throwIfHasErrors(errors);

            // update the notification and save (at this point, status and id are valid)
            alert.setStatusTypeCode(notificationUpdateDTO.getStatus());
            alert.setStatusDate(new Date());
            alertDao.save(alert);
        }

        // no additional information needed to be returned so just return what was passed in
        return notificationDTOList;
    }

    /**
     * verifies if a status transfer is valid
     *
     * @param previousStatus current status
     * @param updateStatus status to be updated to
     * @return List<ErrorMessage> any errors
     */
    @Nonnull
    private List<ErrorMessage> verifyStatusTransition(String previousStatus, @Nonnull String updateStatus) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        // check if status was already defined (may not have been defined)
        if (previousStatus == null) {
            errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);

        } else {
            StatusTypeCode statusTypeCode = StatusTypeCode.valueOf(previousStatus);
            switch (statusTypeCode) { // verify correct transition or no transition at all
                case NEW:
                    if (!StatusTypeCode.VIEWED.name().equals(updateStatus)
                        && !StatusTypeCode.NEW.name().equals(updateStatus)) {

                        errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);
                    }
                    break;
                case VIEWED:
                    if (!StatusTypeCode.ARCHIVED.name().equals(updateStatus)
                        && !StatusTypeCode.VIEWED.name().equals(updateStatus)) {

                        errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);
                    }
                    break;
                case ARCHIVED:
                if (!StatusTypeCode.ARCHIVED.name().equals(updateStatus)) {
                    errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);
                    }
                    break;
                default: // should never hit this
                    errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);
            }
        }

        return errorMessages;
    }

    /**
     * gets an alert entry from the database
     *
     * @param id of alert to fetch
     * @return Alert object
     */
    @CheckForNull
    private Alert retrieveAlertEntity(Long id, Long paxId) {
        // verify that notification is valid
        Alert notification = null;

        if (id != null) {
            try {
                notification = alertDao.findBy().where(ProjectConstants.ID).eq(id)
                .and(ProjectConstants.PAX_ID).eq(paxId).findOne();
            } catch (Exception e) {
                logger.warn(MISSING_NOTIFICATION_WARN_MSG, e);
            }
        }

        return notification;
    }

    /**
     * Verifies the status.
     *
     * @param status status being evaluated
     * @return List<ErrorMessage> list of errors
     */
    @Nonnull
    private List<ErrorMessage> verifyStatus(String status) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (status == null) {
            errorMessages.add(AlertConstants.STATUS_EMPTY_MESSAGE);
        } else if (!AlertConstants.VALID_STATUS_TYPES.contains(status)) {
            errorMessages.add(new ErrorMessage()
                    .setCode(AlertConstants.ERROR_STATUS_NOT_VALID)
                    .setField(ProjectConstants.STATUS)
                    .setMessage(String.format(AlertConstants.ERROR_STATUS_NOT_VALID_MSG, status)));
        }

        return errorMessages;
    }

    /**
     * Verifies the alert type value is valid.
     *
     * @param type type being evaluated
     * @return List<ErrorMessage> list of errors
     */
    @Nonnull
    private List<ErrorMessage> verifyType(String type) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (type == null) {
            errorMessages.add(AlertConstants.TYPE_EMPTY_MESSAGE);
        } else if (!AlertSubType.DEFAULT_SUB_TYPES_LIST.contains(type)) {
            errorMessages.add(new ErrorMessage()
                    .setCode(AlertConstants.ERROR_TYPE_NOT_VALID)
                    .setField(ProjectConstants.TYPE)
                    .setMessage(String.format(AlertConstants.ERROR_TYPE_NOT_VALID_MSG, type)));
        }

        return errorMessages;
    }

    /**
     * Verifies report abuse notifications transition.
     *
     * @param alert alert being processed
     * @param updateStatus status to be updated to
     * @return List<ErrorMessage> any errors
     */
    @Nonnull
    private List<ErrorMessage> verifyReportAbuseTransition(Alert alert, String updateStatus) {

        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (alert != null) {
            Comment comment = commentService.findCommentById(alert.getTargetId());

            // If reported abuse comment hasn't been confirmed, notification shouldn't been archived.
            if (comment != null && CommentConstants.REPORTED_STATUS_CODE.equals(comment.getStatusTypeCode())
                    && StatusTypeCode.ARCHIVED.name().equals(updateStatus)) {
                errorMessages.add(AlertConstants.INVALID_STATUS_TRANSITION_MESSAGE);
            }

        }
        return errorMessages;
    }

    @Override
    public void addRaiseReceiverAlert(Long nominationId) {

        Nomination nomination = nominationDao.findById(nominationId);

        if (nomination != null) {

            List<Recognition> recognitionList = recognitionDao.findBy()
                    .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                    .find();

            List<Alert> receiverAlerts = new ArrayList<>();
            Alert alert = null;

            for (Recognition recognition : recognitionList) {
                if (recognition.getReceiverPaxId().equals(security.getPaxId())) {
                    continue; // Don't create an alert for the raise submitter.
                }
                alert = new Alert();
                alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                alert.setAlertSubTypeCode(AlertSubType.RAISE_RECEIVER.name());
                alert.setProgramId(nomination.getProgramId());
                alert.setTargetId(nomination.getId());
                alert.setPaxId(recognition.getReceiverPaxId());
                alert.setStatusTypeCode(StatusTypeCode.NEW.name());
                alert.setStatusDate(new Date());
                alert.setTargetTable(TableName.NOMINATION.name());
                alertDao.save(alert);

                receiverAlerts.add(alert);
            }

            addManagerRaiseReceiverAlert(nomination, receiverAlerts);
        } else {
            List<ErrorMessage> errorMessages = new ArrayList<>();

            errorMessages.add(AlertConstants.NOMINATION_NOT_FOUND_MESSAGE);

            ErrorMessageException.throwIfHasErrors(errorMessages);
        }
    }

    /**
     * Creates raise notification alerts for each of the receivers managers
     *
     * @param nomination - nomination
     * @param receiverAlerts - List of receiver alerts to be processed
     */
    private void addManagerRaiseReceiverAlert(Nomination nomination, List<Alert> receiverAlerts) {

        // Create set of receiver paxIds
        Set<Long> receiverPaxIds = new HashSet<>();
        for (Alert receiverAlert : receiverAlerts) {
            receiverPaxIds.add(receiverAlert.getPaxId());
        }

        // Get relationship to manager paxId1 == receiver, paxId2 == manager
        List<Relationship> relationships = relationshipDao.findBy()
                .where(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.name())
                .and(ProjectConstants.PAX_ID_1).in(receiverPaxIds)
                .find();

        // Get set of manager paxIds
        Set<Long> managerPaxIds = new HashSet<>();
        for (Relationship relationship : relationships) {
            if (relationship != null && relationship.getPaxId2() != null) {
                managerPaxIds.add(relationship.getPaxId2());
            }
        }

        Alert alert = null;

        for (Long managerPaxId : managerPaxIds) {
            if (managerPaxId.equals(security.getPaxId())) {
                continue; // Don't create alert for logged in pax
            }

            alert = new Alert();
            alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
            alert.setAlertSubTypeCode(AlertSubType.MANAGER_RAISE_RECEIVER.name());
            alert.setProgramId(nomination.getProgramId());
            alert.setTargetId(nomination.getId());
            alert.setPaxId(managerPaxId);
            alert.setStatusTypeCode(StatusTypeCode.NEW.name());
            alert.setStatusDate(new Date());
            alert.setTargetTable(TableName.NOMINATION.name());
            alertDao.save(alert);
        }
    }

    @Override
    public void addNotificationForRecognition(Long activityId, Long notificationCreatorPaxId,
            String receiverAlertTypeCode, String submitterAlertTypeCode) {

        NewsfeedItem newsfeedItem = newsfeedItemDao.findById(activityId);

        Nomination nomination = nominationDao.findById(newsfeedItem.getTargetId());

        // Add LIKE notification for receivers.
        List<Recognition> recognitionList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                .and(ProjectConstants.PARENT_ID).isNull() // Parent Id check
                // only create notifications for recipients who are approved
                .and(ProjectConstants.STATUS_TYPE_CODE).in(StatusTypeCode.APPROVED.name(), StatusTypeCode.AUTO_APPROVED.name())
                .find();

        Set<Long> receiversPaxIds = new HashSet<>();
        // get list of receivers' paxId.
        for (Recognition recognition : recognitionList) {
            Long receiverPaxId = recognition.getReceiverPaxId();

            // do not create / update an alert for the pax that caused the alert
            if (!receiverPaxId.equals(notificationCreatorPaxId)) {
                receiversPaxIds.add(receiverPaxId);
            }
        }

        if (!receiversPaxIds.isEmpty()) {
            // get like recognition alerts for receivers.
            List<Alert> receiverAlerts = new ArrayList<>();
            Iterable<List<Long>> activityIdIterable = Iterables.partition(receiversPaxIds, ProjectConstants.SQL_PARAM_COUNT_MAX);

            for (List<Long> set : activityIdIterable) {
                List<Alert> alList = alertDao.findBy().where(ProjectConstants.TARGET_ID).eq(newsfeedItem.getId())
                    .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(receiverAlertTypeCode)
                        .and(ProjectConstants.PAX_ID).in(set)
                    .find();
                if (!CollectionUtils.isEmpty(alList)) {
                    receiverAlerts.addAll(alList);
                }
            }

            // updating existed alerts, status = NEW
            for (Alert alert : receiverAlerts) {
                alert.setStatusTypeCode(StatusTypeCode.NEW.name());
                alert.setStatusDate(new Date());

                alertDao.save(alert);
                // remove updated paxIds from receivers list.
                receiversPaxIds.remove(alert.getPaxId());

            }
            // if alert not exist, create new one.
            for (Long paxId : receiversPaxIds) {
                Alert alert = new Alert();
                alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                alert.setAlertSubTypeCode(receiverAlertTypeCode);
                alert.setProgramId(nomination.getProgramId());
                alert.setTargetId(newsfeedItem.getId());
                alert.setPaxId(paxId);
                alert.setStatusTypeCode(StatusTypeCode.NEW.name());
                alert.setStatusDate(new Date());
                alert.setTargetTable(TableName.NEWSFEED_ITEM.name());

                alertDao.save(alert);
            }
        }

        // do not create / update an alert for the pax that caused the alert
        if (!nomination.getSubmitterPaxId().equals(notificationCreatorPaxId)) {
            // Add LIKE notification for Submitter.
            // verify if notification for LIKE activity already exists.
            Alert alert = alertDao.findBy()
                    .where(ProjectConstants.TARGET_ID).eq(newsfeedItem.getId())
                    .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(submitterAlertTypeCode)
                    .and(ProjectConstants.PAX_ID).eq(nomination.getSubmitterPaxId())
                    .findOne();
            // not exist create new Alert, otherwise change Status to NEW and update statusDate.
            if (alert == null) {
                alert = new Alert();
                alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                alert.setAlertSubTypeCode(submitterAlertTypeCode);
                alert.setProgramId(nomination.getProgramId());
                alert.setTargetId(newsfeedItem.getId());
                alert.setPaxId(nomination.getSubmitterPaxId());
                alert.setTargetTable(TableName.NEWSFEED_ITEM.name());
            }
            alert.setStatusTypeCode(StatusTypeCode.NEW.name());
            alert.setStatusDate(new Date());

            alertDao.save(alert);
        }

    }

    @Override
    public void addRecognitionApprovalAlert(Long nominationId) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (nominationId != null) {
            // validate nomination exists.
            Nomination nomination = nominationDao.findById(nominationId);

            if (nomination != null) {
                // get recognitions with pending status.
                List<Recognition> pendingRecognitions = recognitionDao.findBy()
                        .where(ProjectConstants.NOMINATION_ID).eq(nomination.getId())
                        .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.PENDING.name())
                        .find();

                if (!CollectionUtils.isEmpty(pendingRecognitions)) {
                    List<Long> recognitionIds = new ArrayList<>();
                    for (Recognition recognition : pendingRecognitions) {
                        recognitionIds.add(recognition.getId());
                    }

                    createApprovalRecognitionAlerts(recognitionIds, nomination,
                            AlertSubType.RECOGNITION_APPROVAL.name());
                } else {
                    logger.warn(String.format(AlertConstants.RECOGNITION_NOT_EXIST_WARN_MSG, nomination.getId()));
                }
            } else {
                errorMessages.add(AlertConstants.NOMINATION_DOES_NOT_EXIST_MESSAGE);
            }
        } else {
            errorMessages.add(AlertConstants.NOMINAITON_ID_INVALID_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errorMessages);
    }

    @Override
    public void addRaiseApprovalAlert(Long nominationId) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        Nomination nomination = nominationDao.findById(nominationId);

        if (nomination != null) {
            List<String> parentStatusList = new ArrayList<>();
            parentStatusList.add(StatusTypeCode.APPROVED.name());
            parentStatusList.add(StatusTypeCode.PENDING.name());
            List<String> raiseStatus = new ArrayList<>();
            raiseStatus.add(StatusTypeCode.PENDING.name());

            List<Long> raiseRecognitionIds = new ArrayList<>();

            List<Map<String, Object>> raiseList = raiseDao.getListOfRaisesOnNomination(nomination.getId(), null,
                    parentStatusList, raiseStatus, null);

            for (Map<String, Object> raise : raiseList) {
                raiseRecognitionIds.add((Long) raise.get(ProjectConstants.ID));
            }

            createApprovalRecognitionAlerts(raiseRecognitionIds, nomination, AlertSubType.RAISE_APPROVAL.name());
        } else {
            errorMessages.add(AlertConstants.NOMINATION_DOES_NOT_EXIST_MESSAGE);
        }

        ErrorMessageException.throwIfHasErrors(errorMessages);
    }

    /**
     * Creates non-existent alerts for approval recognition alerts
     *
     * @param recognitionIds - List of recognition ids to generate approval alerts for
     * @param nomination - Nomination entity tied to given list of recognitions
     * @param approvalSubTypeCode - Alert sub type code of approval
     */
    private void createApprovalRecognitionAlerts(List<Long> recognitionIds, Nomination nomination, String approvalSubTypeCode) {

        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        Iterable<List<Long>> recognitionIdIterable = Iterables.partition(recognitionIds, ProjectConstants.SQL_PARAM_COUNT_MAX);
        for (List<Long> set : recognitionIdIterable) {
            List<ApprovalPending> apList = approvalPendingDao.findBy()
                    .where(ProjectConstants.TARGET_ID).in(set)
                    .and(ProjectConstants.TARGET_TABLE).eq(TableName.RECOGNITION.name())
                    .find();
            if (!CollectionUtils.isEmpty(apList)) {
                approvalPendingList.addAll(apList);
            }
        }

        if (!CollectionUtils.isEmpty(approvalPendingList)) {
            for (ApprovalPending approvalPending : approvalPendingList) {
                Alert alertExists = alertDao.findBy()
                        .where(ProjectConstants.PAX_ID).eq(approvalPending.getPaxId())
                        .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(approvalSubTypeCode)
                        .and(ProjectConstants.TARGET_ID).eq(nomination.getId())
                        .findOne();

                // Create new alert if non-existent
                if (alertExists == null) {
                    Alert alert = new Alert();
                    alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                    alert.setAlertSubTypeCode(approvalSubTypeCode);
                    alert.setProgramId(nomination.getProgramId());
                    alert.setTargetId(nomination.getId());
                    alert.setPaxId(approvalPending.getPaxId());
                    alert.setStatusTypeCode(StatusTypeCode.NEW.name());
                    alert.setStatusDate(new Date());
                    if(approvalSubTypeCode.equals(AlertSubType.RECOGNITION_PENDING_APPROVAL.name())) {
                        alert.setTargetTable(TableName.NEWSFEED_ITEM.name());
                    } else {
                        alert.setTargetTable(TableName.NOMINATION.name());
                    }

                    alertDao.save(alert);
                } else {
                    logger.warn(String.format(AlertConstants.NOTIFICATION_EXIST_WARN_MSG, alertExists.getPaxId(),
                            alertExists.getAlertSubTypeCode(), alertExists.getTargetId()));
                }
            }
        } else {
            logger.warn(String.format(AlertConstants.APPROVAL_PENDING_NOTEXIST_WARN_MSG, nomination.getId()));
        }
    }

    @Override
    public void addReportedCommentAdminAlert(Long activityId, Comment comment) {

        NewsfeedItem newsfeedItem = newsfeedItemDao.findById(activityId);

        // get program id.
        Long programId = nominationDao.findBy()
                .where(ProjectConstants.ID).eq(newsfeedItem.getTargetId())
                .and(ProjectConstants.PARENT_ID).isNull() // Parent Id check Nomination
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);

        Alert alert = alertDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(comment.getId())
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.REPORT_COMMENT_ABUSE.name())
                .findOne();

        if (alert == null) {

            Alert newAlert = new Alert();
            newAlert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
            newAlert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.name());
            newAlert.setProgramId(programId);
            newAlert.setTargetId(comment.getId());
            newAlert.setPaxId(commentService.getReportAbuseAdminPaxId());
            newAlert.setStatusTypeCode(StatusTypeCode.NEW.name());
            newAlert.setStatusDate(new Date());
            newAlert.setTargetTable(TableName.COMMENT.name());

            alertDao.save(newAlert);

            AlertMisc alertMisc = new AlertMisc();
            alertMisc.setAlertId(newAlert.getId());
            alertMisc.setVfName(VfName.REPORTER_PAX.name());
            alertMisc.setMiscData(String.valueOf(security.getPaxId()));
            alertMisc.setMiscDate(new Date());
            alertMiscDao.save(alertMisc);

        } else {
            alert.setStatusTypeCode(StatusTypeCode.NEW.name());
            alert.setStatusDate(new Date());
            alertDao.save(alert);

            AlertMisc alertMisc = alertMiscDao.findBy()
                    .where(ProjectConstants.ALERT_ID).eq(alert.getId())
                    .and(ProjectConstants.VF_NAME).eq(VfName.REPORTER_PAX.name())
                    .findOne();

            if (alertMisc != null){
                alertMisc.setMiscData(String.valueOf(security.getPaxId()));
                alertMisc.setMiscDate(new Date());
                alertMiscDao.save(alertMisc);
            }else{
                alertMisc = new AlertMisc();
                alertMisc.setAlertId(alert.getId());
                alertMisc.setVfName(VfName.REPORTER_PAX.name());
                alertMisc.setMiscData(String.valueOf(security.getPaxId()));
                alertMisc.setMiscDate(new Date());
                alertMiscDao.save(alertMisc);
            }
        }

    }

    @Override
    public void addPendingRecognitionSubmitterAlert(Long nominationId, Long submitterPaxId) {

        Long programId = nominationDao.findBy()
                .where(ProjectConstants.ID).eq(nominationId)
                .and(ProjectConstants.PARENT_ID).isNull() // Parent Id check Nomination
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);

        Alert alert = alertDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationId)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.RECOGNITION_PENDING_APPROVAL.name())
                .findOne();

        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationId)
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();

        if (alert == null) {
            Alert newAlert = new Alert();
            newAlert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
            newAlert.setAlertSubTypeCode(AlertSubType.RECOGNITION_PENDING_APPROVAL.name());
            newAlert.setProgramId(programId);
            newAlert.setTargetId(newsfeedItem.getId());
            newAlert.setPaxId(submitterPaxId);
            newAlert.setStatusTypeCode(StatusTypeCode.NEW.name());
            newAlert.setStatusDate(new Date());
            newAlert.setTargetTable(TableName.NEWSFEED_ITEM.name());

            alertDao.save(newAlert);
        } else {
            alert.setStatusTypeCode(StatusTypeCode.NEW.name());
            alert.setStatusDate(new Date());
            alert.setCreateId(String.valueOf(security.getPaxId()));

            alertDao.save(alert);
        }

    }

    @Override
    public void addApprovalActionTakenNotification(List<RecognitionDetailsDTO> recognitionDetailsDTOList, Long nominationId) {
        List<RecognitionDetailsDTO> approvedList = new ArrayList<>();
        List<RecognitionDetailsDTO> deniedList = new ArrayList<>();
        double totalPoints = 0;

        // Get the list of approved and denied Recognitions
        for (RecognitionDetailsDTO recognitionDetailsDTO : recognitionDetailsDTOList) {
            if (ProjectConstants.APPROVED_STATUSES.contains(recognitionDetailsDTO.getStatus())) {
                approvedList.add(recognitionDetailsDTO);
            } else {
                deniedList.add(recognitionDetailsDTO);
            }
        }

        // Get the total points on the entire recognition (not just for those being approved/denied)
        // We want the points regardless of status since this is only used for the submitter view
        totalPoints = cnxtRecognitionRepository.getTotalPointsForNomination(nominationId);

        if (approvedList.size() > 0) {
            createNewAlertApprovalActionTaken(approvedList.get(0), approvedList.size(), totalPoints);
        }
        if (deniedList.size() > 0) {
            createNewAlertApprovalActionTaken(deniedList.get(0), deniedList.size(), totalPoints);
        }
    }

    /**
     * saves an alert for users who have had recognitions approved / rejected
     *
     * @param recognitionDetailsDTO recognition being processed
     * @param participantsActioned number of participants handled here
     * @param totalPoints total points on nomination
     */
    private void createNewAlertApprovalActionTaken(RecognitionDetailsDTO recognitionDetailsDTO,
            Integer participantsActioned, double totalPoints) {

        Long programId = nominationDao.findBy()
                .where(ProjectConstants.ID).eq(recognitionDetailsDTO.getNominationId())
                .and(ProjectConstants.PARENT_ID).isNull() // Parent Id check Nomination
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);

        Alert newAlert = new Alert();
        if (ProjectConstants.APPROVED_STATUSES.contains(recognitionDetailsDTO.getStatus())) {
            newAlert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
            newAlert.setAlertSubTypeCode(AlertSubType.RECOGNITION_APPROVED.name());
        } else if (recognitionDetailsDTO.getStatus().equals(StatusTypeCode.REJECTED.name())) {
            newAlert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
            newAlert.setAlertSubTypeCode(AlertSubType.RECOGNITION_DENIED.name());
        }
        newAlert.setPaxId(recognitionDetailsDTO.getFromPax().getPaxId());
        newAlert.setTargetId(recognitionDetailsDTO.getNominationId());
        newAlert.setProgramId(programId);
        newAlert.setStatusDate(new Date());
        newAlert.setStatusTypeCode(StatusTypeCode.NEW.name());
        newAlert.setTargetTable(TableName.NOMINATION.name());

        alertDao.save(newAlert);

        // Alert Misc for approval pax
        String alertMiscVfName = VfName.APPROVAL_PAX.name();
        AlertMisc alertMiscApprovalPax = new AlertMisc();
        alertMiscApprovalPax.setAlertId(newAlert.getId());
        alertMiscApprovalPax.setVfName(alertMiscVfName);
        alertMiscApprovalPax.setMiscData(String.valueOf(recognitionDetailsDTO.getApprovalPax().getPaxId()));
        alertMiscApprovalPax.setMiscDate(new Date());

        alertMiscDao.save(alertMiscApprovalPax);

        // Alert Misc for participants actioned
        alertMiscVfName = VfName.PARTICIPANTS_ACTIONED.name();
        AlertMisc alertMiscParticipantsActioned = new AlertMisc();
        alertMiscParticipantsActioned.setAlertId(newAlert.getId());
        alertMiscParticipantsActioned.setVfName(alertMiscVfName);
        alertMiscParticipantsActioned.setMiscData(String.valueOf(participantsActioned));
        alertMiscParticipantsActioned.setMiscDate(new Date());

        alertMiscDao.save(alertMiscParticipantsActioned);

        // Alert Misc for total points
        alertMiscVfName = VfName.TOTAL_POINTS.name();
        AlertMisc alertMiscTotalPoints = new AlertMisc();
        alertMiscTotalPoints.setAlertId(newAlert.getId());
        alertMiscTotalPoints.setVfName(alertMiscVfName);
        alertMiscTotalPoints.setMiscData(String.valueOf(totalPoints));
        alertMiscTotalPoints.setMiscDate(new Date());

        alertMiscDao.save(alertMiscTotalPoints);

        if (newAlert.getAlertSubTypeCode().equalsIgnoreCase(AlertSubType.RECOGNITION_DENIED.name())) {
            // Alert Misc for rejected pax
            alertMiscVfName = VfName.REJECTED_PAX.name();
            AlertMisc alertMiscRejectedPax = new AlertMisc();
            alertMiscRejectedPax.setAlertId(newAlert.getId());
            alertMiscRejectedPax.setVfName(alertMiscVfName);
            alertMiscRejectedPax.setMiscData(String.valueOf(recognitionDetailsDTO.getToPax().getPaxId()));
            alertMiscRejectedPax.setMiscDate(new Date());

            alertMiscDao.save(alertMiscRejectedPax);
        } else if (newAlert.getAlertSubTypeCode().equalsIgnoreCase(AlertSubType.RECOGNITION_APPROVED.name())) {

            // Alert Misc for approved pax
            alertMiscVfName = VfName.APPROVED_PAX.name();
            AlertMisc alertMiscApprovedPax = new AlertMisc();
            alertMiscApprovedPax.setAlertId(newAlert.getId());
            alertMiscApprovedPax.setVfName(alertMiscVfName);
            alertMiscApprovedPax.setMiscData(String.valueOf(recognitionDetailsDTO.getToPax().getPaxId()));
            alertMiscApprovedPax.setMiscDate(new Date());

            alertMiscDao.save(alertMiscApprovedPax);
        }
    }

    @Override
    public void addMilestoneReminderAlert(PaxMilestoneReminderDTO paxMilestoneReminderDTO) {

        Alert alert = new Alert();
        alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
        alert.setAlertSubTypeCode(AlertSubType.MILESTONE_REMINDER.name());
        alert.setProgramId(paxMilestoneReminderDTO.getProgramId());
        alert.setPaxId(paxMilestoneReminderDTO.getManagerPaxId());
        alert.setStatusTypeCode(StatusTypeCode.NEW.name());
        alert.setStatusDate(new Date());
        alertDao.save(alert);

        AlertMisc alertMisc = new AlertMisc();
        alertMisc.setAlertId(alert.getId());
        alertMisc.setVfName(VfName.MILESTONE_PAX.name());
        alertMisc.setMiscData(paxMilestoneReminderDTO.getPaxId().toString());
        alertMisc.setMiscDate(new Date());
        alertMiscDao.save(alertMisc);

        alertMisc = new AlertMisc();
        alertMisc.setAlertId(alert.getId());
        alertMisc.setVfName(VfName.MILESTONE_DATE.name());
        alertMisc.setMiscData(paxMilestoneReminderDTO.getMilestoneDate());
        alertMisc.setMiscDate(new Date());
        alertMiscDao.save(alertMisc);

        if (paxMilestoneReminderDTO.getEventType().equals(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE)) {
            alertMisc = new AlertMisc();
            alertMisc.setAlertId(alert.getId());
            alertMisc.setVfName(VfName.YEARS_OF_SERVICE.name());
            alertMisc.setMiscData(paxMilestoneReminderDTO.getYearsOfService().toString());
            alertMisc.setMiscDate(new Date());
            alertMiscDao.save(alertMisc);
        }

        alertMisc = new AlertMisc();
        alertMisc.setAlertId(alert.getId());
        alertMisc.setVfName(VfName.MILESTONE_TYPE.name());
        alertMisc.setMiscData(paxMilestoneReminderDTO.getEventType());
        alertMisc.setMiscDate(new Date());
        alertMiscDao.save(alertMisc);

        if(paxMilestoneReminderDTO.getAwardAmount() != null && paxMilestoneReminderDTO.getAwardAmount() > 0) {
            alertMisc = new AlertMisc();
            alertMisc.setAlertId(alert.getId());
            alertMisc.setVfName(VfName.AWARD_AMOUNT.name());
            alertMisc.setMiscData(paxMilestoneReminderDTO.getAwardAmount().toString());
            alertMisc.setMiscDate(new Date());
            alertMiscDao.save(alertMisc);
        }

        String payoutType = paxMilestoneReminderDTO.getPayoutType();
        if (payoutType != null) {
            alertMisc = new AlertMisc();
            alertMisc.setAlertId(alert.getId());
            alertMisc.setVfName(VfName.PAYOUT_TYPE.name());
            alertMisc.setMiscData(paxMilestoneReminderDTO.getPayoutType());
            alertMisc.setMiscDate(new Date());
            alertMiscDao.save(alertMisc);

            AwardTypeDTO awardType = awardTypeService.getAwardTypeByPayoutType(payoutType);
            alertMisc = new AlertMisc();
            alertMisc.setAlertId(alert.getId());
            alertMisc.setVfName(VfName.PAYOUT_DISPLAY_NAME.name());
            alertMisc.setMiscData(awardType.getDisplayName());
            alertMisc.setMiscDate(new Date());
            alertMiscDao.save(alertMisc);
        }
    }
}
