package com.maritz.culturenext.alert.service;

import java.util.List;

import com.maritz.core.jpa.entity.Comment;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;

public interface AlertService {

    /**
     * updates the status of a collection of notifications
     * 
     * @param paxId id of pax to update notifications for
     * @param notificationDTOList list of notifications to update
     * @return List<AlertUpdateDTO> updated list
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/23560208/Bulk+Update+Notifications
     */
    List<AlertUpdateDTO> updateAlerts(Long paxId, List<AlertUpdateDTO> notificationDTOList);

    /**
     * Adds notification alert for given raise nomination id
     * 
     * @param nominationId - Raise nomination id
     */
    void addRaiseReceiverAlert(Long nominationId);

    /**
     * Create notification for recognition receivers and submitter
     * 
     * @param activityId - ID of the activity tile to create a notification for
     * @param notificationCreatorPaxId - used to supress notifications if the 
     *         submitter/recipient was also the submitter of the nomination
     * @param receiverAlertTypeCode - type of alert to create for the receiver
     * @param submitterAlertTypeCode - type of alert to create for the submitter
     */
    void addNotificationForRecognition(Long activityId, Long notificationCreatorPaxId, String receiverAlertTypeCode,
            String submitterAlertTypeCode);

    /**
     * creates an alert for an admin to handle a reported comment
     * 
     * @param activityId id of activity that has the reported comment
     * @param comment comment object that was reported
     */
    void addReportedCommentAdminAlert(Long activityId, Comment comment);

    /**
     * creates an alert to inform submitter that their nomination is pending
     * approval
     * 
     * @param nominationId id of nomination to be approved
     * @param submitterPaxId id of pax who submitted recognition
     */
    void addPendingRecognitionSubmitterAlert(Long nominationId, Long submitterPaxId);

    /**
     * Adds notification alert for recognition approvals. Get approvers from
     * approval pending table.
     * 
     * @param nominationId
     */
    void addRecognitionApprovalAlert(Long nominationId);

    /**
     * creates alerts for users who have had recognitions approved / rejected
     * 
     * @param recognitionDetailsDTOList list of recognitions that have been processed
     * @param nominationId ID of the Nomination tied to the recognitions being processed
     */
    void addApprovalActionTakenNotification(List<RecognitionDetailsDTO> recognitionDetailsDTO, Long nominationId);

    /**
     * Adds notification alert for raise approvals
     * 
     * @param nominationId - Id of nomination to generate raise recognition alerts from
     */
    void addRaiseApprovalAlert(Long nominationId);

    /**
     * creates alerts for users who have upcoming milestones
     *
     * @param paxMilestoneReminderDTO DTO used to create alerts
     */
    void addMilestoneReminderAlert(PaxMilestoneReminderDTO paxMilestoneReminderDTO);
}
