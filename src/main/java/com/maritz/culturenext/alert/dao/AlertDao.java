package com.maritz.culturenext.alert.dao;

import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Nomination;

public interface AlertDao {

    List<Map<String, Object>> getAlertsInfo(Long paxId, List<String> alertStatus, List<String> alertTypes);

    Long getNominationByAlertId(Long alertId);

    Nomination getNominationObjectByAlertId(Long alertId);
}
