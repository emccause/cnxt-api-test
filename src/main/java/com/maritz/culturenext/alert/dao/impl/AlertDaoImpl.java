package com.maritz.culturenext.alert.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.maritz.core.jdbc.dao.impl.AbstractDaoImpl;
import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.core.jdbc.util.ConcentrixBeanRowMapper;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.AlertTargetType;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.alert.dao.AlertDao;
import com.maritz.culturenext.constants.ProjectConstants;

@Repository
public class AlertDaoImpl extends AbstractDaoImpl implements AlertDao {
    
    private static final String ALERT_INFO_BY_PAX_SP_QUERY = 
            "EXEC UP_ALERT_INFO_BY_PAX :paxId, :status, :types";


    private static final String RECOGNITION_ALERT_SUB_TYPES_PLACEHOLDER = "RECOGNITION_ALERT_SUB_TYPES";
    private static final String NOMINATION_ALERT_SUB_TYPES_PLACEHOLDER = "NOMINATION_ALERT_SUB_TYPES";
    private static final String NEWSFEED_ALERT_SUB_TYPES_PLACEHOLDER = "NEWSFEED_ALERT_SUB_TYPES";
    private static final String COMMENT_ALERT_SUB_TYPES_PLACEHOLDER = "COMMENT_ALERT_SUB_TYPES";
    
    private static final String NOMINATION_BY_ALERT_ID_QUERY = 
            "SELECT TOP 1 R.NOMINATION_ID"
            + " FROM ALERT A"
            + " INNER JOIN RECOGNITION R ON"
                + " ( A.ALERT_SUB_TYPE_CODE IN ( :"+ RECOGNITION_ALERT_SUB_TYPES_PLACEHOLDER +") AND "
                    + "A.TARGET_ID = R.ID ) OR "
                + " ( A.ALERT_SUB_TYPE_CODE IN ( :"+ NOMINATION_ALERT_SUB_TYPES_PLACEHOLDER +") AND "
                    + " A.TARGET_ID = R.NOMINATION_ID ) OR "
                + " ( A.ALERT_SUB_TYPE_CODE IN ( :"+ NEWSFEED_ALERT_SUB_TYPES_PLACEHOLDER +") AND "
                    + " R.NOMINATION_ID IN ( SELECT NI.TARGET_ID FROM NEWSFEED_ITEM NI WHERE A.TARGET_ID = NI.ID AND NI.TARGET_TABLE = 'NOMINATION') ) OR "
                + "( A.ALERT_SUB_TYPE_CODE IN ( :"+ COMMENT_ALERT_SUB_TYPES_PLACEHOLDER +") AND "
                    + "R.NOMINATION_ID IN ( "
                        + " SELECT NIC.TARGET_ID FROM COMMENT C, NEWSFEED_ITEM NIC "
                        + "WHERE A.TARGET_ID = C.ID AND C.TARGET_TABLE = 'NEWSFEED_ITEM' AND C.TARGET_ID = NIC.ID AND NIC.TARGET_TABLE = 'NOMINATION'))"
            + "WHERE A.ID = :"  + ProjectConstants.ID;
    
    private static final String NOMINATION_OBJECT_BY_ALERT_ID_QUERY = 
            "SELECT NOM.* FROM NOMINATION NOM " + 
                    "INNER JOIN NEWSFEED_ITEM NFI " + 
                        "ON NOM.ID = NFI.TARGET_ID AND NFI.TARGET_TABLE = 'NOMINATION' " + 
                    "INNER JOIN ALERT ALERT " + 
                        "ON NFI.ID = ALERT.TARGET_ID " + 
                    "WHERE ALERT.ID = :"  + ProjectConstants.ID;
    
    /**
     * Return SQL StoreProc's result of Alerts info
     * @param paxId
     * @param alertStatus : query filter - List of alert status .
     * @param alertTypes : query filter - List of alert types.
     * @return
     */
    @Override
    public List<Map<String, Object>> getAlertsInfo(Long paxId,
            List<String> alertStatus, List<String> alertTypes) {
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.PAX_ID, paxId);
        //Convert list to string delimited by comma
        
        params.addValue(ProjectConstants.STATUS, StringUtils.join(alertStatus, ProjectConstants.COMMA_DELIM));
        params.addValue(ProjectConstants.TYPES, StringUtils.join(alertTypes, ProjectConstants.COMMA_DELIM));
        
        List<Map<String, Object>> nodes = namedParameterJdbcTemplate.query(ALERT_INFO_BY_PAX_SP_QUERY, params, new CamelCaseMapRowMapper());                    
            
        // need to convert all Date objects to String.
        for(Map<String, Object> node: nodes) {
            if (node.get(ProjectConstants.ALERT_CREATE_DATE) != null) {
                node.put(ProjectConstants.ALERT_CREATE_DATE_UTC, 
                        DateUtil.convertToUTCDateTime((Date) node.get(ProjectConstants.ALERT_CREATE_DATE)));
            }
            if (node.get(ProjectConstants.ALERT_STATUS_DATE) != null) {
                node.put(ProjectConstants.ALERT_STATUS_DATE_UTC, 
                        DateUtil.convertToUTCDateTime((Date) node.get(ProjectConstants.ALERT_STATUS_DATE)));
            }
        }
        return nodes;
    }

    /**
     * Get nomination id by alert id.
     */
    @Override
    public Long getNominationByAlertId(Long alertId) {
        
        Long nominationId;
        String query = NOMINATION_BY_ALERT_ID_QUERY;
        
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.ID, alertId);
        params.addValue(RECOGNITION_ALERT_SUB_TYPES_PLACEHOLDER, AlertSubType.typeListByTargetType(AlertTargetType.RECOGNITION));
        params.addValue(NOMINATION_ALERT_SUB_TYPES_PLACEHOLDER, AlertSubType.typeListByTargetType(AlertTargetType.NOMINATION));
        params.addValue(NEWSFEED_ALERT_SUB_TYPES_PLACEHOLDER, AlertSubType.typeListByTargetType(AlertTargetType.NEWSFEED_ITEM));
        params.addValue(COMMENT_ALERT_SUB_TYPES_PLACEHOLDER, AlertSubType.typeListByTargetType(AlertTargetType.COMMENT));
        
        try {
            nominationId =  namedParameterJdbcTemplate.queryForObject(query, params, Long.class);
        } catch (EmptyResultDataAccessException emptyResultException) {
            nominationId = null;
        }
        
        return nominationId;
    }
    
    @Override
    public Nomination getNominationObjectByAlertId(Long alertId) {

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue(ProjectConstants.ID, alertId);
        
        List<Nomination> nominationList = namedParameterJdbcTemplate.query(NOMINATION_OBJECT_BY_ALERT_ID_QUERY, params, new ConcentrixBeanRowMapper<Nomination>()
                .setBeanClass(Nomination.class)
                .setTable(jitBuilder.getTable(Nomination.class))
                .build());
            
        if (nominationList != null && !nominationList.isEmpty()) {
            return nominationList.get(0);
        }
        
        return null;
    }
}
