package com.maritz.culturenext.alert.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;

@Component
public class AlertUtil {

    private final CnxtRecognitionRepository cnxtRecognitionRepository;
    private final ConcentrixDao<Program> programDao;
    private final ConcentrixDao<ProgramMisc> programMiscDao;
    private final ConcentrixDao<SysUser> sysUserDao;
    private final NominationDataBulkSaveDao nominationDataBulkSaveDao;

    public AlertUtil(
        CnxtRecognitionRepository cnxtRecognitionRepository,
        ConcentrixDao<Program> programDao,
        ConcentrixDao<ProgramMisc> programMiscDao,
        ConcentrixDao<SysUser> sysUserDao,
        NominationDataBulkSaveDao nominationDataBulkSaveDao
    ) {
        this.cnxtRecognitionRepository = cnxtRecognitionRepository;
        this.programDao = programDao;
        this.programMiscDao = programMiscDao;
        this.sysUserDao  =sysUserDao;
        this.nominationDataBulkSaveDao = nominationDataBulkSaveDao;
    }

    /**
     * Validates DTO and then creates nomination and related entries
     *
     * @param nomination - The nomination of the users whose recognition alerts are being generated for
     * @param recognitionList - List of recognitions for the given nomination
     * @param managerPaxIdList - list of managers for the recipients of the recognitions
     * @return void
     */
    public void generateRecognitionAlerts(Nomination nomination, List<Recognition> recognitionList, List<Long> managerPaxIdList, Long approverPaxId, List<Long> notifyOtherList) {

        List<ProgramMisc> programFlags = programMiscDao.findBy()
            .where(ProjectConstants.VF_NAME).in(
                NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME,
                NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME
            )
            .and(ProjectConstants.PROGRAM_ID).eq(nomination.getProgramId())
            .active()
            .find();

        boolean notifyRecipient = true;
        boolean notifyRecipientManager = true;
        if (!CollectionUtils.isEmpty(programFlags)) {
            for (ProgramMisc flag : programFlags) {
                if (flag.getVfName().equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME)) {
                    notifyRecipient = Boolean.parseBoolean(flag.getMiscData()); // should be "TRUE" or "FALSE"
                }
                else if (flag.getVfName().equalsIgnoreCase(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME)) {
                    notifyRecipientManager = Boolean.parseBoolean(flag.getMiscData()); // should be "TRUE" or "FALSE"
                }
            }
        }

        // recipient notifications
        ArrayList<Alert> alertList = new ArrayList<>();

        //determine the status of the alerts based on whether or not the nomination has points
        String status = StatusTypeCode.NEW.name();
        if (nomination != null) {
            Long points = cnxtRecognitionRepository.getTotalPointsForNomination(nomination.getId());
            if (points != null && points > 0) status = StatusTypeCode.HOLD.name();
        }

        if (notifyRecipient && recognitionList != null) {
            for (Recognition recognition : recognitionList) {
                Alert alert = new Alert();
                alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                alert.setAlertSubTypeCode(AlertSubType.RECOGNITION.name());
                alert.setProgramId(nomination.getProgramId());
                alert.setTargetId(recognition.getId());
                alert.setPaxId(recognition.getReceiverPaxId());
                alert.setStatusTypeCode(status);
                alert.setStatusDate(new Date());
                alert.setTargetTable(TableName.RECOGNITION.name());
                alertList.add(alert);
            }
        }

        // Defining alert type for manager based on program type.
        Program program = programDao.findById(nomination.getProgramId());
        String alertManagerType = ProgramTypeEnum.AWARD_CODE.getCode().equals(program.getProgramTypeCode())
            ? AlertSubType.MANAGER_AWARD_CODE.name()
            : AlertSubType.MANAGER_RECOGNITION.name()
        ;

        // Manager notifications
        Long submitterPaxId = nomination.getSubmitterPaxId();
        if (notifyRecipientManager) {
            for (Long managerPaxId : managerPaxIdList) {
                SysUser manager = sysUserDao.findBy()
                    .where(ProjectConstants.PAX_ID).eq(managerPaxId)
                    .findOne();

                if (manager != null && !submitterPaxId.equals(managerPaxId)
                // Recognizing direct reports should not appear under Direct Reports section
                        && !manager.getStatusTypeCode().equals(StatusTypeCode.INACTIVE.name())
                        && !managerPaxId.equals(approverPaxId)) { // Do not create notification if manager = approver
                    Alert alert = new Alert();
                    alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                    alert.setAlertSubTypeCode(alertManagerType);
                    alert.setProgramId(nomination.getProgramId());
                    alert.setTargetId(nomination.getId());
                    alert.setPaxId(managerPaxId);
                    alert.setStatusTypeCode(status);
                    alert.setStatusDate(new Date());
                    alert.setTargetTable(TableName.NOMINATION.name());
                    alertList.add(alert);
                }
            }
        }

        // Notify Other notifications
        if (notifyOtherList != null && !notifyOtherList.isEmpty()) {
            for (Long notifyOtherPaxId : notifyOtherList) {
                Alert alert = new Alert();
                alert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
                alert.setAlertSubTypeCode(AlertSubType.NOTIFY_OTHER.name());
                alert.setProgramId(nomination.getProgramId());
                alert.setTargetId(nomination.getId());
                alert.setPaxId(notifyOtherPaxId);
                alert.setStatusTypeCode(status);
                alert.setStatusDate(new Date());
                alert.setTargetTable(TableName.NOMINATION.name());
                alertList.add(alert);
            }
        }

        // Save the alerts
        if (!alertList.isEmpty()) {
            nominationDataBulkSaveDao.saveAlertList(alertList);
        }
    }
}
