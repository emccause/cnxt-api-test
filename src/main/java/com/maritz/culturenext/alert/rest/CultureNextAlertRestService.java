package com.maritz.culturenext.alert.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.PAX_ID_REST_PARAM;

import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.SecurityPolicy;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.alert.service.AlertService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("participants")
@Api(value = "/notifications", description = "Endpoint for alert notifications")
public class CultureNextAlertRestService {

    @Inject private AlertService alertService;

    //rest/participants/~/notifications
    @RequestMapping(value = "{paxId}/notifications", method = RequestMethod.PUT)
    @SecurityPolicy(permission=Security.PERMISSION_PUBLIC, authorization="@security.isMyPax(#pax.getPaxId())")
    @ApiOperation(
        value = "Bulk update of notifications - includes only the fields that are eligible for update (i.e. status)",
        notes = "Changes the requests JSON Array of notification objects to the requested status."
    )
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "successfully updated the notifications", response = List.class)
    })
    public List<AlertUpdateDTO> updateAlerts(
        @ApiParam(value = "Participant Id", required = true) @PathVariable(PAX_ID_REST_PARAM) Pax pax,
        @RequestBody List<AlertUpdateDTO> notificationUpdateDTOs
    ) {
        return alertService.updateAlerts(pax.getPaxId(), notificationUpdateDTOs);
    }

}
