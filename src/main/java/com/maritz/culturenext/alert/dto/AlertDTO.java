package com.maritz.culturenext.alert.dto;

public class AlertDTO {
    private Long id;
    private String alertType;
    private String programName;
    private String status;
    private String statusDate;
    private String createDate;
    private AlertDetailDTO detail;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getAlertType() {
        return alertType;
    }
    public void setAlertType(String alertType) {
        this.alertType = alertType;
    }
    public String getProgramName() {
        return programName;
    }
    public void setProgramName(String programName) {
        this.programName = programName;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatusDate() {
        return statusDate;
    }
    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }
    public String getCreateDate() {
        return createDate;
    }
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public AlertDetailDTO getDetail() {
        return detail;
    }
    public void setDetail(AlertDetailDTO detail) {
        this.detail = detail;
    }
}
