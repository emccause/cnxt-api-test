package com.maritz.culturenext.alert.dto;

import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.recognition.dto.RecognitionApprovalDTO;

public class AlertDetailDTO {
    private PointUploadAlertDTO pointUpload;
    private NewsfeedItemDTO activity;
    private RaiseDetailsDTO raiseDetails;
    private AbuseReportDTO abuseReport;
    private RecognitionApprovalDTO recognitionApproval;
    
    public PointUploadAlertDTO getPointUpload() {
        return pointUpload;
    }
    public void setPointUpload(PointUploadAlertDTO pointUpload) {
        this.pointUpload = pointUpload;
    }
    public NewsfeedItemDTO getActivity() {
        return activity;
    }
    public void setActivity(NewsfeedItemDTO activity) {
        this.activity = activity;
    }
    public RaiseDetailsDTO getRaiseDetails() {
        return raiseDetails;
    }
    public void setRaiseDetails(RaiseDetailsDTO raiseDetails) {
        this.raiseDetails = raiseDetails;
    }
    public AbuseReportDTO getAbuseReport() {
        return abuseReport;
    }
    public void setAbuseReport(AbuseReportDTO abuseReport) {
        this.abuseReport = abuseReport;
    }
    public RecognitionApprovalDTO getRecognitionApproval() {
        return recognitionApproval;
    }
    public void setRecognitionApproval(RecognitionApprovalDTO recognitionApproval) {
        this.recognitionApproval = recognitionApproval;
    }

    
}
