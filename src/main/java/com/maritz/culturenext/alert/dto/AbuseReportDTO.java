package com.maritz.culturenext.alert.dto;


import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

public class AbuseReportDTO {
    CommentDTO comment;
    EmployeeDTO reporterPax;
    String time;

    public CommentDTO getComment() {
        return comment;
    }

    public void setComment(CommentDTO comment) {
        this.comment = comment;
    }

    public EmployeeDTO getReporterPax() {
        return reporterPax;
    }

    public void setReporterPax(EmployeeDTO reporterPax) {
        this.reporterPax = reporterPax;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
