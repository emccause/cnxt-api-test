package com.maritz.culturenext.alert.dto;

public class AlertUpdateDTO {
    private Long id;
    private String status;

    public AlertUpdateDTO() {}

    public AlertUpdateDTO(Long id, String status) {
        this.id = id;
        this.status = status;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
