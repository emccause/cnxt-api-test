package com.maritz.culturenext.alert.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class PointUploadAlertDTO{
    private Long id;
    private EmployeeDTO toPax;
    private Double pointsIssued;
    private String budgetName;
    private String description;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }

    public Double getPointsIssued() {
        return pointsIssued;
    }

    public void setPointsIssued(Double pointsIssued) {
        this.pointsIssued = pointsIssued;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    

}
