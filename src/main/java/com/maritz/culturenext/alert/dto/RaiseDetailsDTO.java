package com.maritz.culturenext.alert.dto;

import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

public class RaiseDetailsDTO {
    private EmployeeDTO toPax;
    private EmployeeDTO fromPax;
    private Integer recipientCount;
    private Long totalPoints;

    public Long getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Long totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Integer getRecipientCount() {
        return recipientCount;
    }

    public void setRecipientCount(Integer recipientCount) {
        this.recipientCount = recipientCount;
    }

    public EmployeeDTO getToPax() {
        return toPax;
    }

    public void setToPax(EmployeeDTO toPax) {
        this.toPax = toPax;
    }

    public EmployeeDTO getFromPax() {
        return fromPax;
    }

    public void setFromPax(EmployeeDTO fromPax) {
        this.fromPax = fromPax;
    }
}
