package com.maritz.culturenext.alert.constants;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.permission.constants.PermissionConstants;

import java.util.Arrays;
import java.util.List;

public class AlertConstants {
    // Error messages.
    public static final String ERROR_STATUS_EMPTY = "ERROR_STATUS_EMPTY";
    public static final String ERROR_STATUS_EMPTY_MSG = "Status value is empty";
    public static final String ERROR_STATUS_NOT_VALID = "ERROR_STATUS_NOT_VALID";
    public static final String ERROR_STATUS_NOT_VALID_MSG = "Status %s is not valid.";

    public static final String ERROR_TYPE_EMPTY = "ERROR_TYPE_EMPTY";
    public static final String ERROR_TYPE_EMPTY_MSG = "Type value is empty";
    public static final String ERROR_TYPE_NOT_VALID = "ERROR_TYPE_NOT_VALID";
    public static final String ERROR_TYPE_NOT_VALID_MSG = "Type %s is not valid.";

    public static final String ERROR_MISSING_ID = "MISSING_ID";
    public static final String ERROR_MISSING_ID_MSG = "ID value is missing";
    public static final String ERROR_INVALID_ID = "INVALID_ID";
    public static final String ERROR_INVALID_ID_MSG = "ID value is not valid";
    public static final String ERROR_INVALID_STATUS_TRANSITION = "INVALID_STATUS_TRANSITION";
    public static final String ERROR_INVALID_STATUS_TRANSITION_MSG = 
            "Status can only be updated from NEW to VIEWED or VIEWED to ARCHIVED";

    public static final String ERROR_ALERT_INFO = "ERROR_ALERT_INFO";
    public static final String ERROR_ALERT_INFO_MSG = "The query for alert common info has failed";

    public static final String ERROR_NOMINATION_ID = "NOMINATION_NOT_EXIST";
    public static final String ERROR_NOMINATION_NOT_EXIST_MSG = "Nomination not exists";
    public static final String ERROR_NOMINATION_ID_INVALID_MSG = "Nomination id is not valid.";

    public static final String NOTIFICATION_FIELD = "Notification";
    public static final String ERROR_NOTIFICATION_BELONGS_TO_ANOTHER_PAX = "NOTIFICATION_BELONGS_TO_ANOTHER_PAX";
    public static final String ERROR_NOTIFICATION_BELONGS_TO_ANOTHER_PAX_MSG = 
            "Notification exists but belongs to different participant";
    public static final String ERROR_NOTIFICATION_NOT_FOUND = "NOTIFICATION_NOT_FOUND";
    public static final String ERROR_NOTIFICATION_NOT_FOUND_MSG = "Notification not found";

    public static final String ERROR_NOMINATION_NOT_FOUND = "NOMINATION_NOT_FOUND";
    public static final String ERROR_NOMINATION_NOT_FOUND_MSG = "Nomination id does not exist. ";

    // Warning messages
    public static final String NOTIFICATION_EXIST_WARN_MSG = 
            "A notification for pax ID %d, type %s, target ID %d already exists.";
    public static final String RECOGNITION_NOT_EXIST_WARN_MSG = 
            "There isn't any recognition associated with the nomination ID %d.";
    public static final String APPROVAL_PENDING_NOTEXIST_WARN_MSG = "Approval Pendings not exists for nomination ID %d";

    public static final List<String> VALID_STATUS_TYPES = Arrays.asList(StatusTypeCode.NEW.name(),
            StatusTypeCode.VIEWED.name(), StatusTypeCode.ARCHIVED.name());
    
    // Error Messages
    public static final ErrorMessage PROXY_NOT_ALLOWED_MESSAGE = new ErrorMessage()
        .setField(ProjectConstants.PROXY)
        .setCode(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_CODE)
        .setMessage(PermissionConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE);
    
    public static final ErrorMessage MISSING_ID_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_MISSING_ID)
        .setField(ProjectConstants.ID)
        .setMessage(AlertConstants.ERROR_MISSING_ID_MSG);
    
    public static final ErrorMessage INVALID_ID_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_INVALID_ID)
        .setField(ProjectConstants.ID)
        .setMessage(AlertConstants.ERROR_INVALID_ID_MSG);
    
    public static final ErrorMessage INVALID_STATUS_TRANSITION_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_INVALID_STATUS_TRANSITION)
        .setField(ProjectConstants.STATUS)
        .setMessage(AlertConstants.ERROR_INVALID_STATUS_TRANSITION_MSG);
    
    public static final ErrorMessage STATUS_EMPTY_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_STATUS_EMPTY)
        .setField(ProjectConstants.STATUS)
        .setMessage(AlertConstants.ERROR_STATUS_EMPTY_MSG);
    
    public static final ErrorMessage TYPE_EMPTY_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_TYPE_EMPTY)
        .setField(ProjectConstants.TYPE)
        .setMessage(AlertConstants.ERROR_TYPE_EMPTY_MSG);
    
    public static final ErrorMessage NOMINATION_NOT_FOUND_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_NOMINATION_NOT_FOUND)
        .setField(ProjectConstants.NOMINATION_ID)
        .setMessage(AlertConstants.ERROR_NOMINATION_NOT_FOUND_MSG);

    public static final ErrorMessage NOMINATION_DOES_NOT_EXIST_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_NOMINATION_ID)
        .setField(ProjectConstants.NOMINATION_ID)
        .setMessage(AlertConstants.ERROR_NOMINATION_NOT_EXIST_MSG);
    
    public static final ErrorMessage NOMINAITON_ID_INVALID_MESSAGE = new ErrorMessage()
        .setCode(AlertConstants.ERROR_NOMINATION_ID)
        .setField(ProjectConstants.NOMINATION_ID)
        .setMessage(AlertConstants.ERROR_NOMINATION_ID_INVALID_MSG);
}
