package com.maritz.culturenext.enums;

public enum FileItemTypeEnum {
    ECARD,
    GROUPS,
    PAX
}
