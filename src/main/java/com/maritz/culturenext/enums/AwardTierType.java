package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

public enum AwardTierType {
    RANGE,
    DISCRETE;
    
    public static final List<String> getList() {
        return Arrays.asList(
                    RANGE.toString(),
                    DISCRETE.toString()
                );
    }
}
