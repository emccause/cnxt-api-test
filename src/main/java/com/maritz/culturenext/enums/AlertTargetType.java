package com.maritz.culturenext.enums;

public enum AlertTargetType {
    RECOGNITION,
    NOMINATION,
    NEWSFEED_ITEM,
    COMMENT;
}
