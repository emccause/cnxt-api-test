package com.maritz.culturenext.enums;

public enum LookupCodeEnum {
    ABS,
    AWARD_VALUE,
    CATALOG_DEEP_LINK,
    CATALOG_URL,
    DISPLAY_NAME,
    LONG_DESC,
    PAYOUT_TYPE,
    PROJECT_NUMBER,
    SHORT_DESC,
    SUB_CLIENT,
    SUB_PROJECT_NUMBER,
	LOOKUP_DISPLAY_NAME_ID;
}
