package com.maritz.culturenext.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum AlertSubType {
    APPROVAL,
    COMMENT_RECOGNITION_RECEIVER,
    COMMENT_RECOGNITION_SUBMITTER,
    LIKE_RECOGNITION_RECEIVER,
    LIKE_RECOGNITION_SUBMITTER,
    MANAGER_AWARD_CODE,
    MANAGER_RAISE_RECEIVER,
    MANAGER_RECOGNITION,
    MILESTONE_REMINDER,
    NOTIFY_OTHER,
    POINT_LOAD,
    RAISE_APPROVAL,
    RAISE_RECEIVER,
    RECOGNITION,
    RECOGNITION_APPROVAL,
    RECOGNITION_APPROVED,
    RECOGNITION_DENIED,
    RECOGNITION_PENDING_APPROVAL,
    REPORT_COMMENT_ABUSE;

    public static List<String> DEFAULT_SUB_TYPES_LIST =
            Arrays.asList(
                    APPROVAL.name(),
                    COMMENT_RECOGNITION_RECEIVER.name(),
                    COMMENT_RECOGNITION_SUBMITTER.name(),
                    LIKE_RECOGNITION_RECEIVER.name(),
                    LIKE_RECOGNITION_SUBMITTER.name(),
                    MANAGER_AWARD_CODE.name(),
                    MANAGER_RAISE_RECEIVER.name(),
                    MANAGER_RECOGNITION.name(),
                    MILESTONE_REMINDER.name(),
                    NOTIFY_OTHER.name(),
                    POINT_LOAD.name(),
                    RAISE_APPROVAL.name(),
                    RAISE_RECEIVER.name(),
                    RECOGNITION.name(),
                    RECOGNITION_APPROVAL.name(),
                    RECOGNITION_APPROVED.name(),
                    RECOGNITION_DENIED.name(),
                    RECOGNITION_PENDING_APPROVAL.name(),
                    REPORT_COMMENT_ABUSE.name()
            );

    public static List<String> COMMENTS_SUB_TYPE_LIST =
            Arrays.asList(
                    COMMENT_RECOGNITION_RECEIVER.name(),
                    COMMENT_RECOGNITION_SUBMITTER.name(),
                    REPORT_COMMENT_ABUSE.name()
            );

    public static List<String> LIKES_SUB_TYPE_LIST =
            Arrays.asList(
                    LIKE_RECOGNITION_RECEIVER.name(),
                    LIKE_RECOGNITION_SUBMITTER.name()
            );

    public static List<String> RAISES_SUB_TYPE_LIST =
            Arrays.asList(
                    MANAGER_RAISE_RECEIVER.name(),
                    RAISE_RECEIVER.name()
            );

    public static List<String> RECOGNITIONS_SUB_TYPE_LIST =
            Arrays.asList(
                    MANAGER_RECOGNITION.name(),
                    RECOGNITION.name(),
                    NOTIFY_OTHER.name()
            );

    public static List<String> APPROVALS_SUB_TYPE_LIST =
            Arrays.asList(
                    RECOGNITION_APPROVAL.name(),
                    RECOGNITION_PENDING_APPROVAL.name(),
                    RECOGNITION_DENIED.name(),
                    RECOGNITION_APPROVED.name()
            );

    public static List<String> EXCLUDE_LIST =
            Arrays.asList(
                    MILESTONE_REMINDER.name(),
                    POINT_LOAD.name()
            );

    public static List<String> typeListByTargetType(AlertTargetType alertTargetType){
        
        List<String> alertSubTypes = new ArrayList<String>();
        
        switch (alertTargetType){
            case RECOGNITION : 
                alertSubTypes = Arrays.asList(RECOGNITION.name());
                break;
            case NOMINATION : 
                alertSubTypes = Arrays.asList(MANAGER_RECOGNITION.name(),
                                RAISE_RECEIVER.name(),
                                RAISE_APPROVAL.name(),
                                MANAGER_RAISE_RECEIVER.name(),
                                RECOGNITION_APPROVAL.name(),
                                RECOGNITION_APPROVED.name(),
                                RECOGNITION_DENIED.name(),
                                MANAGER_AWARD_CODE.name());
                break;
            case NEWSFEED_ITEM : 
                alertSubTypes = Arrays.asList(COMMENT_RECOGNITION_RECEIVER.name(),
                        COMMENT_RECOGNITION_SUBMITTER.name(),
                        RECOGNITION_PENDING_APPROVAL.name(),
                        LIKE_RECOGNITION_RECEIVER.name(),
                        LIKE_RECOGNITION_SUBMITTER.name());
                break;
            case COMMENT : 
                alertSubTypes = Arrays.asList(REPORT_COMMENT_ABUSE.name());
                break;
        }
        
        return alertSubTypes;
    }

}


