package com.maritz.culturenext.enums;

public enum LogicalOperatorEnum {
    AND,
    OR
}
