package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum ProgramActivityTypeCode {
    SERVICE_ANNIVERSARY, BIRTHDAY;

    public static List<String> getNames() {
        return Arrays.asList(ProgramActivityTypeCode.values()).stream()
            .map(e -> e.name())
            .collect(Collectors.toList());
    }

    public static boolean contains(String activityName) {
        return getNames().contains(activityName);
    }
}
