package com.maritz.culturenext.enums;

import java.util.HashMap;

public enum EmailType {
    BUSINESS("BUSINESS"),
    HOME("HOME")
    ;
    
    private String emailTypeCode;
    private static HashMap<String, EventType> emailTypeMap = new HashMap<String, EventType>();

    private EmailType(String emailTypeCode) {
        this.emailTypeCode = emailTypeCode;
    }
    
    public String getCode() {
        return emailTypeCode;
    }

    static
    {
        for (EventType type : EventType.values())
        {
            emailTypeMap.put(type.getCode(), type);
        }
    }

    public static EventType getTypeFromCodeValue(String eventTypeCode)
    {
        return emailTypeMap.get(eventTypeCode);
    }
}