package com.maritz.culturenext.enums;

public enum CountryCodeEnum {
    DE,        //Germany
    US,        //United States
    ES,        //Spain
    CA,        //Canada
    FR,        //France
    IT,        //Italy
    JP,        //Japan
    BR,        //Brazil
    RU,        //Russian Federation
    CN,        //China
    TW        //Taiwan
    ;
}