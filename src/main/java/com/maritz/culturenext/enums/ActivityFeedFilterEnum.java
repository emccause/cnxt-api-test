package com.maritz.culturenext.enums;

import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ActivityFeedFilterEnum {
    AREA,
    CITY,
    COMPANY_NAME,
    COST_CENTER,
    COUNTRY_CODE,
    DEPARTMENT,
    FUNCTION,
    GRADE,
    NETWORK,
    PUBLIC,
    STATE;

    public static boolean contains(String value) {
        for (ActivityFeedFilterEnum activityFeedFilterEnum : ActivityFeedFilterEnum.values()) {
            if (activityFeedFilterEnum.name().equals(value)) {
                return true;
            }
        }

        return false;
    }

    public static List<String> getKeyNameList() {
        List<String> keyNameList = new ArrayList<>();
        for (ActivityFeedFilterEnum activityFeedFilterEnum : ActivityFeedFilterEnum.values()) {
            keyNameList.add(
                ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER + ProjectConstants.DOT_DELIM +
                    activityFeedFilterEnum.name());
        }

        return keyNameList;
    }

    public static List<String> getStaticFilterList() {
        return Arrays.asList(NETWORK.name(), PUBLIC.name());
    }
}
