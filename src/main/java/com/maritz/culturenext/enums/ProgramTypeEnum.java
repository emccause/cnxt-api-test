package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

public enum ProgramTypeEnum {
    AWARD_CODE("AWARD_CODE"),
    ECARD_ONLY("ECRD"),
    MANAGER_DISCRETIONARY("MGRD"),
    POINT_LOAD("PNTU"),
    PEER_TO_PEER("PTP"),
    MILESTONE("MILESTONE"),
    APPLICATION("APPLICATION"),
    PINNACLE("PINNACLE")
    ;

    private String programTypeCode;

    private ProgramTypeEnum(String programTypeCode) {
        this.programTypeCode = programTypeCode;
    }
    
    private static final BidiMap<String, ProgramTypeEnum> programTypeCodes;
    static {
        programTypeCodes = new DualHashBidiMap<String, ProgramTypeEnum>();
        for (ProgramTypeEnum programType : ProgramTypeEnum.values()) {
            programTypeCodes.put(programType.getCode(), programType);
        }
    }
    
    public String getCode() {
        return programTypeCode;
    }

    public static ProgramTypeEnum programTypeByCode(String code) {
        return programTypeCodes.get(code);
    }

    public static String getCodeByProgramTypeName(String programType) {
        String strKey = null;
        strKey = programTypeCodes.getKey(programType);
        return strKey;
    }

    /**
     * Gets a ProgramTypeEnum element using the program Type name as locator. The search is case sensitive.
     * @param programType the name of the program type to look for.
     * @return programTypeEnum if found, null otherwise.
     */
    public static ProgramTypeEnum getProgramTypeByName(String programType){
        return programTypeCodes.get(programTypeCodes.getKey(programType));
    }

    public static List<String> getAllProgramTypes() {
        return Arrays.asList(
                AWARD_CODE.name(),
                PEER_TO_PEER.name(),
                ECARD_ONLY.name(),
                MANAGER_DISCRETIONARY.name(),
                POINT_LOAD.name(),
                MILESTONE.name(),
                PINNACLE.name()
                );

    }

    public static List<String> getRecognitionCategoryTypes() {
        return Arrays.asList(
                PEER_TO_PEER.name(),
                ECARD_ONLY.name(),
                MANAGER_DISCRETIONARY.name()
                );
    }

    public static List<String> getPointLoadCategoryTypes() {
        return Arrays.asList(
                POINT_LOAD.name()
                );
    }
}