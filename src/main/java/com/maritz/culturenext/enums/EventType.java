package com.maritz.culturenext.enums;

import java.util.HashMap;

public enum EventType {
    RECOGNITION_PARTICIPANT("RECGPX"),
    RECOGNITION_MANAGER("RECGMN"),
    APPROVAL("APR"),
    RESET_PASSWORD("RESET_PASSWORD"),
    CHANGE_PASSWORD("CHANGE_PASSWORD"),
    POINT_LOAD("POINT_LOAD"),
    NOTIFY_OTHER("NOTIFY_OTHER"),
    SERVICE_ANNIVERSARY_TYPE("SERVICE_ANNIVERSARY"),
    BIRTHDAY_TYPE("BIRTH_DAY"),
    SERVICE_ANNIVERSARY_REMINDER_TYPE("SERVICE_ANNIVERSARY_REMINDER"),
    BIRTHDAY_REMINDER_TYPE("BIRTHDAY_REMINDER"),
    APPROVAL_REMINDER("APR_REMINDER");
    
    private String eventTypeCode;
    private static HashMap<String, EventType> eventTypeMap = new HashMap<String, EventType>();

    private EventType(String eventTypeCode) {
        this.eventTypeCode = eventTypeCode;
    }
    
    public String getCode() {
        return eventTypeCode;
    }

    static
    {
        for (EventType type : EventType.values())
        {
            eventTypeMap.put(type.getCode(), type);
        }
    }

    public static EventType getTypeFromCodeValue(String eventTypeCode)
    {
        return eventTypeMap.get(eventTypeCode);
    }
}