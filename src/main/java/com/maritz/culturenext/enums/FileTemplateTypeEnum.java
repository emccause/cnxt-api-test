package com.maritz.culturenext.enums;

public enum FileTemplateTypeEnum {
    ADV_REC_BULK_UPLOAD,
    CPTD,
    ADV_REC_UPLOAD
}