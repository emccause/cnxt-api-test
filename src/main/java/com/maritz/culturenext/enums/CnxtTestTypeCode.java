package com.maritz.culturenext.enums;

public enum CnxtTestTypeCode {
    QUIZ,
    SURVEY,
    PINNACLE
}
