package com.maritz.culturenext.enums;

public enum ApprovalType {
    NONE,
    PERSON,
    BUDGET_OWNER,
    RECEIVER_FIRST,
    RECEIVER_SECOND,
    SUBMITTER_FIRST,
    SUBMITTER_SECOND,
    ;
}