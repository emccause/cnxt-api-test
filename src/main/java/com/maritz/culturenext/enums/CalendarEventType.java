package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

public enum CalendarEventType {
    HOLIDAY,
    SERVICE_ANNIVERSARY,
    BIRTHDAY,
    SUB_TYPES;

    CalendarEventType () {}

    public List<String> getListOfCalendarEventSubTypes () {
        return Arrays.asList(BIRTHDAY.toString(), SERVICE_ANNIVERSARY.toString());
    }
}
