package com.maritz.culturenext.enums;

public enum ApprovalTypeEnum {
    NONE("NONE"),
    PERSON("PERSON"),
    BUDGET_OWNER("BUDGET_OWNER"),
    SUBMITTER_FIRST("SUBMITTER_FIRST"), 
    SUBMITTER_SECOND("SUBMITTER_SECOND"), 
    RECEIVER_FIRST("RECEIVER_FIRST"), 
    RECEIVER_SECOND("RECEIVER_SECOND");
    
    private String type;

    
    private ApprovalTypeEnum(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
    
}
