package com.maritz.culturenext.enums;

public enum AudienceEnum {
    ALL,
    NETWORK,
    ME
}
