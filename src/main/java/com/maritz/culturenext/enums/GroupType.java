package com.maritz.culturenext.enums;

public enum GroupType {
    CUSTOM("ADHC"),
    ENROLLMENT("ENRL"),
    HIERARCHY("HIER"),
    PERSONAL("REPG"),
    ROLE_BASED("ROLE_BASED");
        
    private String groupTypeCode;
    
    private GroupType(String groupTypeCode) {
        this.groupTypeCode = groupTypeCode;
    }
    
    public String getCode() {
        return groupTypeCode;
    }
}
