package com.maritz.culturenext.enums;

public enum NewsfeedVisibility {
    NONE,
    PUBLIC,
    PRIVATE,
    RECIPIENT_PREF
    ;
}