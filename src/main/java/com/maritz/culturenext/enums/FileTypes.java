package com.maritz.culturenext.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public enum FileTypes {
    ECARD_THUMBNAIL,
    ECARD_DEFAULT,
    GROUP_HERO_IMAGE,
    GROUP_LOGO,
    PROGRAM_IMAGE_SMALL,
    PROGRAM_IMAGE_DETAILED,
    PROGRAM_IMAGE,
    AWARD_CODE_CERT,
    AWARD_CODE_ICON,
    CAROUSEL,
    JUMBOTRON,
    PROJECT_BACKGROUND,
    PROJECT_LOGO_LIGHT,
    PROJECT_LOGO,
    PRFT,
    PRFS,
    PRFD, 
    TERMS_AND_CONDITIONS, 
    HERO_IMAGE;
    
    public static boolean containsImageFileType(String s) {
        return getImageFileTypes().contains(s);
    }


    public static List<String> getImageFileTypes() {
        List<String> types = new ArrayList<>();
        for (FileTypes fileType : values()) {
            types.add(fileType.name());
        }
        
        return types;
    }

    public static  List<String> getImageFileTypeCode(){
        return Arrays.asList(
            AWARD_CODE_CERT.name(),
            AWARD_CODE_ICON.name(),
            JUMBOTRON.name(),
            CAROUSEL.name(),
            GROUP_HERO_IMAGE.name(),
            GROUP_LOGO.name());
    }
}
