package com.maritz.culturenext.enums;

public enum RelationshipType {
    REPORT_TO,
    FRIENDS,
    SECONDARY_REPORT
    ;
}