package com.maritz.culturenext.enums;

public enum ProgramEligibilityEnum {
    GIVE("Program Giver"),
    RECEIVE("Program Receiver");
    
    private String eligibilityRoleName;
    
    private ProgramEligibilityEnum(String eligibilityRoleName) {
        this.eligibilityRoleName = eligibilityRoleName;
    }
    
    public String getEligibilityRoleName() {
        return eligibilityRoleName;
    }
}
