package com.maritz.culturenext.enums;

public enum Visibility {
    PUBLIC,
    ADMIN;
}
