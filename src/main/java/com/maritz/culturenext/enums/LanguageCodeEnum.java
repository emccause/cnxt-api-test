package com.maritz.culturenext.enums;

public enum LanguageCodeEnum {
    de,        //German
    en,        //English
    es,        //Spanish
    fr,        //French
    it,        //Italian
    ja,        //Japanese
    pt,        //Portuguese
    ru,        //Russian
    zh        //Chinese
    ;
}