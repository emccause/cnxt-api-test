package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

public enum VfName {
    ALLOCATED_IN,
    ALLOCATED_OUT,
    ALLOW_TEMPLATE_UPLOAD,
    APPROVAL_PAX,
    APPROVED_PAX,
    AREA,
    AUDIENCE,
    AWARD_AMOUNT,
    AWARD_CODE,
    BIRTH_DAY,
    BUDGET_OVERRIDE_PROJECT_NUMBER,
    BUDGET_OVERRIDE_SUB_PROJECT_NUMBER,
    CERT_ID,
    CLAIM_INSTRUCTIONS,
    COMPANY_NAME,
    CONFIGURABLE_SUB_PROJECT_NUMBER,
    COST_CENTER,
    CUSTOM_1,
    CUSTOM_2,
    CUSTOM_3,
    CUSTOM_4,
    CUSTOM_5,
    DEFAULT_LOCALE,
    DEFAULT_PROJECT_NUMBER,
    DEFAULT_SUB_PROJECT_NUMBER,
    DEPARTMENT,
    ECARD_ID,
    FULL_BIRTH_DATE,
    FUNCTION,
    GIVE_ANONYMOUS,
    GRADE,
    HIRE_DATE,
    IS_DOWNLOADABLE,
    IS_PRINTABLE,
    IS_USABLE,
    JOB_TITLE,
    MILESTONE_DATE,
    MILESTONE_REMINDER,
    MILESTONE_TYPE,
    MILESTONE_PAX,
    NEWSFEED_VISIBILITY,
    NEWSFEED_VISIBILITY_APPROVED,
    NEWSFEED_VISIBILITY_NOMINATED,
    NON_US_OVERRIDE,
    NOTIFICATION_RECIPIENT,
    NOTIFICATION_RECIPIENT_2ND_MGR,
    NOTIFICATION_RECIPIENT_MGR,
    NOTIFICATION_SUBMITTER,
    ORIGINAL_AMOUNT,
    PARENT_ENTITY_NAME,
    PARTICIPANTS_ACTIONED,
    PAYOUT_DISPLAY_NAME,
    PAYOUT_TYPE,
    PINNACLE_INDIVIDUAL_AWARD_AMOUNT,
    PINNACLE_NOTIFICATION_RECIPIENT_MGR,
    PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR,
    PINNACLE_NOTIFICATION_RECIPIENT,
    PINNACLE_QUESTION,
    PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON,
    PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM,
    PPP_INDEX_ENABLED,
    ROUTE_NEXT_LEVEL_MANAGER,
    PRIVATE_MESSAGE,
    PROJECT_NUMBER,
    PUBLIC_HEADLINE,
    RAISE_PAX,
    RECEIVE_EMAIL_APPR,
    RECEIVE_EMAIL_DIGEST,
    RECEIVE_EMAIL_MILESTONE_REMINDER,
    RECEIVE_EMAIL_MGR_REC,
    RECEIVE_EMAIL_REC,
    RECEIVE_EMAIL_NOTIFY_OTHERS,
    RESET_PASSWORD_TOKEN,
    REJECTED_PAX,
    REPEATING,
    REPORTER_PAX,
    SELECTED_AWARD_TYPE,
    SHARE_BDAY,
    SHARE_REC,
    SHARE_SA,
    SUB_PROJECT_NUMBER,
    SUB_PROJECT_OVERRIDE,
    TIED_ENTITY_NAME,
    TOP_LEVEL_BUDGET_ID,
    TOTAL_POINTS,
    UNSUBSCRIBED,
    US_OVERRIDE,
    VERBAL_REC_1,
    VERBAL_REC_2,
    VERBAL_REC_3,
    WRITTEN_REC_1,
    WRITTEN_REC_2,
    YEARS_OF_SERVICE,
    REMINDER_DAYS;

    public static List<String> getApprovalMiscVfNameList() {
        return Arrays.asList(
            APPROVAL_PAX.name(),
            PARTICIPANTS_ACTIONED.name(),
            TOTAL_POINTS.name(),
            APPROVED_PAX.name(),
            REJECTED_PAX.name()
        );

    }

    public static List<String> getProgramMiscVfNameList() {
        return Arrays.asList(
            AUDIENCE.name(),
            NEWSFEED_VISIBILITY.name(),
            NEWSFEED_VISIBILITY_APPROVED.name(),
            NEWSFEED_VISIBILITY_NOMINATED.name(),
            NOTIFICATION_RECIPIENT.name(),
            NOTIFICATION_RECIPIENT_MGR.name(),
            NOTIFICATION_RECIPIENT_2ND_MGR.name(),
            NOTIFICATION_SUBMITTER.name(),
            PINNACLE_NOTIFICATION_RECIPIENT_MGR.name(),
            PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.name(),
            PINNACLE_NOTIFICATION_RECIPIENT.name(),
            PINNACLE_QUESTION.name(),
            ALLOW_TEMPLATE_UPLOAD.name(),
            IS_PRINTABLE.name(),
            IS_DOWNLOADABLE.name(),
            CLAIM_INSTRUCTIONS.name(),
            PINNACLE_INDIVIDUAL_AWARD_AMOUNT.name(),
            PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON.name(),
            PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM.name(),
            ROUTE_NEXT_LEVEL_MANAGER.name()
        );
    }

    public static List<String> getPaxGroupMiscVfNameList() {
        return Arrays.asList(
            AREA.name(),
            COMPANY_NAME.name(),
            COST_CENTER.name(),
            CUSTOM_1.name(),
            CUSTOM_2.name(),
            CUSTOM_3.name(),
            CUSTOM_4.name(),
            CUSTOM_5.name(),
            DEPARTMENT.name(),
            FUNCTION.name(),
            GRADE.name(),
            JOB_TITLE.name()
        );
    }


    public static List<String> getPaxMiscVfNameList() {
        return Arrays.asList(
            BIRTH_DAY.name(),
            HIRE_DATE.name(),
            RECEIVE_EMAIL_APPR.name(),
            RECEIVE_EMAIL_DIGEST.name(),
            RECEIVE_EMAIL_MILESTONE_REMINDER.name(),
            RECEIVE_EMAIL_MGR_REC.name(),
            RECEIVE_EMAIL_REC.name(),
            RECEIVE_EMAIL_NOTIFY_OTHERS.name(),
            SHARE_BDAY.name(),
            SHARE_SA.name(),
            SHARE_REC.name(),
            WRITTEN_REC_1.name(),
            WRITTEN_REC_2.name(),
            VERBAL_REC_1.name(),
            VERBAL_REC_2.name(),
            VERBAL_REC_3.name()
        );
    }

    public static List<String> getBudgetMiscVfNameList() {
        return Arrays.asList(
            IS_USABLE.name(),
            TIED_ENTITY_NAME.name(),
            PARENT_ENTITY_NAME.name(),
            TOP_LEVEL_BUDGET_ID.name(),
            ALLOCATED_IN.name(),
            ALLOCATED_OUT.name()
        );
    }
}
