package com.maritz.culturenext.enums;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public enum BudgetTypeEnum {
    ALLOCATED,
    DISTRIBUTED,
    SIMPLE
    ;
    
    private static Map<String, BudgetTypeEnum> budgetTypeMap = new HashMap<String, BudgetTypeEnum>();

    static {
        for (BudgetTypeEnum type : BudgetTypeEnum.values())
        {
            budgetTypeMap.put(type.name(), type);
        }
    }

    public static BudgetTypeEnum getType(String budgetTypeCode) {
        return budgetTypeMap.get(budgetTypeCode);
    }
    
    public static Set<String> getAllTypes() {
        return budgetTypeMap.keySet();
    }
}
