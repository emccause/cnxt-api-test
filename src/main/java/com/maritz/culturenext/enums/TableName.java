package com.maritz.culturenext.enums;

import com.maritz.culturenext.constants.ProjectConstants;

/**
 * Table names used for TARGET_TABLE, SUBJECT_TABLE columns
 */
public enum TableName {
    APPLICATION_DATA (ProjectConstants.APPLICATION_DATA_ID),
    BUDGET (ProjectConstants.ID),
    CALENDAR_ENTRY (ProjectConstants.ID),
    COMMENT (ProjectConstants.ID),
    ECARD (ProjectConstants.ID),
    FILES (ProjectConstants.ID),
    GROUPS (ProjectConstants.GROUP_ID),
    GROUP_CONFIG (ProjectConstants.ID),
    LOOKUP (ProjectConstants.ID),
    NEWSFEED_ITEM (ProjectConstants.ID),
    NOMINATION (ProjectConstants.ID),
    PAX (ProjectConstants.PAX_ID),
    PAX_TYPE (ProjectConstants.ID),
    PROGRAM (ProjectConstants.PROGRAM_ID),
    PROGRAM_ACTIVITY (ProjectConstants.PROGRAM_ACTIVITY_ID),
    PROGRAM_MISC (ProjectConstants.PROGRAM_MISC_ID),
    RECOGNITION (ProjectConstants.ID),
    RECOGNITION_CRITERIA (ProjectConstants.ID),
    ROLE (ProjectConstants.ROLE_ID),
    TEST (ProjectConstants.TEST),
    TRANSACTION_HEADER (ProjectConstants.ID);
    
    private String primaryKey;
    
    private TableName (String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }    
}
