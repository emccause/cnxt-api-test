package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

public enum PermissionManagementTypes {
    ASSIGN_PROXY_SELF,
    ASSIGN_PROXY_OTHERS,
    PROXY_GIVE_REC_ANYONE,
    PROXY_APPROVE_REC_ANYONE,
    PROXY_VIEW_REPORTS_ANYONE,
    ENABLE_ADVANCED_REC,
    ENABLE_COMMENTING,
    DISABLE_COMMENTING,
    ENABLE_LIKING,
    DISABLE_LIKING,
    ENABLE_ENGAGE_SCORE,
    DISABLE_ENGAGE_SCORE,
    ENABLE_NETWORK,
    DISABLE_NETWORK,
    ENABLE_CALENDAR,
    DISABLE_CALENDAR,
    ENABLE_SERVICE_ANNIVERSARY,
    DISABLE_SERVICE_ANNIVERSARY,
    PROXY_BUDGETS_ANYONE,
    PROXY_SETTINGS_ANYONE;
    
    public static List<String> getAllPermissions(){
        return Arrays.asList(
                ASSIGN_PROXY_SELF.name(),
                ASSIGN_PROXY_OTHERS.name(),
                PROXY_GIVE_REC_ANYONE.name(),
                PROXY_APPROVE_REC_ANYONE.name(),
                PROXY_VIEW_REPORTS_ANYONE.name(),
                ENABLE_ADVANCED_REC.name(),
                ENABLE_COMMENTING.name(),
                DISABLE_COMMENTING.name(),
                ENABLE_LIKING.name(),
                DISABLE_LIKING.name(),
                ENABLE_ENGAGE_SCORE.name(),
                DISABLE_ENGAGE_SCORE.name(),
                ENABLE_NETWORK.name(),
                DISABLE_NETWORK.name(),
                ENABLE_CALENDAR.name(),
                DISABLE_CALENDAR.name(),
                ENABLE_SERVICE_ANNIVERSARY.name(),
                DISABLE_SERVICE_ANNIVERSARY.name(),
                PROXY_BUDGETS_ANYONE.name(),
                PROXY_SETTINGS_ANYONE.name()
            );
    }

    public static List<String> getValidAdminPermissions() {
        return Arrays.asList(
                ASSIGN_PROXY_SELF.name(),
                ASSIGN_PROXY_OTHERS.name(),
                PROXY_GIVE_REC_ANYONE.name(),
                PROXY_APPROVE_REC_ANYONE.name(),
                PROXY_VIEW_REPORTS_ANYONE.name(),
                ENABLE_ADVANCED_REC.name(),
                PROXY_BUDGETS_ANYONE.name(),
                PROXY_SETTINGS_ANYONE.name()
            );
    }

    public static List<String> getValidManagerPermissions() {
        return Arrays.asList(
                ASSIGN_PROXY_SELF.name(),
                ENABLE_ADVANCED_REC.name()
            );
    }

    public static List<String> getValidParticipantPermissions() {
        return Arrays.asList(
                ASSIGN_PROXY_SELF.name(),
                ENABLE_ADVANCED_REC.name()
            );
    }

    public static List<String> getValidGroupPermissions() {
        return Arrays.asList(
                ASSIGN_PROXY_SELF.name(),
                ENABLE_COMMENTING.name(),
                DISABLE_COMMENTING.name(),
                ENABLE_LIKING.name(),
                DISABLE_LIKING.name(),
                ENABLE_ENGAGE_SCORE.name(),
                DISABLE_ENGAGE_SCORE.name(),
                ENABLE_NETWORK.name(),
                DISABLE_NETWORK.name(),
                ENABLE_CALENDAR.name(),
                DISABLE_CALENDAR.name(),
                ENABLE_SERVICE_ANNIVERSARY.name(),
                DISABLE_SERVICE_ANNIVERSARY.name()
            );
    }
    
    public static List<String> getAudiencePermissions(){
        return Arrays.asList(
                ENABLE_COMMENTING.name(),
                DISABLE_COMMENTING.name(),
                ENABLE_LIKING.name(),
                DISABLE_LIKING.name(),
                ENABLE_ENGAGE_SCORE.name(),
                DISABLE_ENGAGE_SCORE.name(),
                ENABLE_NETWORK.name(),
                DISABLE_NETWORK.name(),
                ENABLE_CALENDAR.name(),
                DISABLE_CALENDAR.name(),
                ENABLE_SERVICE_ANNIVERSARY.name(),
                DISABLE_SERVICE_ANNIVERSARY.name()
            );
    }
            
}
