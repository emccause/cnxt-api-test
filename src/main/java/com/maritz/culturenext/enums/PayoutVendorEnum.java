package com.maritz.culturenext.enums;

public enum PayoutVendorEnum {
    ABS("ABS"),
    THIRD_PARTY("3rd Party Items"),
    MARS("MARS");
        
    private String payoutVendorName;
    
    private PayoutVendorEnum(String payoutVendorName) {
        this.payoutVendorName = payoutVendorName;
    }
    
    public String getName() {
        return payoutVendorName;
    }
}
