package com.maritz.culturenext.enums;

public enum BudgetConstraintEnum {
    HARD_BUDGET,
    SOFT_BUDGET;
    
    public static BudgetConstraintEnum getByStringValue(String value) {
        if (HARD_BUDGET.name().equalsIgnoreCase(value)) {
            return HARD_BUDGET;
        }
        else if (SOFT_BUDGET.name().equalsIgnoreCase(value)) {
            return SOFT_BUDGET;
        }
        else {
            return null;
        }
    }

}
