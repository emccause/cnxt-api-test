package com.maritz.culturenext.enums;

public enum CalendarSubTypeCode {
    US_OBSERVED,
    RELIGIOUS,
    COMMON,
    COMPANY,
    CUSTOM,
    PROGRAM,
    BIRTHDAY,
    SERVICE_ANNIVERSARY
    ;
}