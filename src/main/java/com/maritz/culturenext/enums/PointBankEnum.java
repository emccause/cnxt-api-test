package com.maritz.culturenext.enums;

public enum PointBankEnum {
    ABS("ABS"),
    EY("EYD"),
    NA("NA"), // another value for N/A.
    NOT_APPLICABLE("N/A");

    private String bank;

    private PointBankEnum(String bank){
        this.bank = bank;
    }

    public String getBank() {
        return bank;
    }

    public static String getTypeByBank(String bank) {
        for (int i = 0; i < PointBankEnum.values().length; i++) {
            if (bank.equals(PointBankEnum.values()[i].bank.toUpperCase()))
                return PointBankEnum.values()[i].name();
        }
        return null;
    }
}