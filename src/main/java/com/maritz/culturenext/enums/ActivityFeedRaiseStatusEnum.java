package com.maritz.culturenext.enums;

public enum ActivityFeedRaiseStatusEnum {
    NOT_ALLOWED,
    INELIGIBLE,
    ELIGIBLE,
    GIVEN;

}
