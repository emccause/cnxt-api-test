package com.maritz.culturenext.enums;

import java.util.Arrays;
import java.util.List;

public enum SiteContentTypeEnum {
    TERMS_AND_CONDITIONS
    ;
    
    public static List<String> getSiteContentTypeList() {
        return Arrays.asList(
                TERMS_AND_CONDITIONS.name()
                );
                
    }
    
}
