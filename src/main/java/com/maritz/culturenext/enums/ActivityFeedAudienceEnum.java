package com.maritz.culturenext.enums;

public enum ActivityFeedAudienceEnum {

    DIRECT_REPORTS,
    NETWORK,
    PARTICIPANT,
    PUBLIC,
    GROUP
    
}
