package com.maritz.culturenext.enums;

public enum LocaleCodeEnum {
    de_DE,     //German
    en_US,    //English
    en_GB,  //English (Great Britian)
    es_ES,    //Spanish (Spain)
    es_MX,  //Spanish (Latin America)
    fr_CA,    //French (Canadian)
    fr_FR,    //French (Europe)
    hi_IN,  //Hindi (India)
    it_IT,    //Italian
    ja_JP,    //Japanese
    nl_NL,  //Dutch
    pt_BR,    //Portuguese (Brazilian)
    pt_PT,  //Portugese
    ru_RU,    //Russian
    zh_CN,    //Chinese (Simplified)
    zh_TW    //Chinese (Traditional)
    ;
}