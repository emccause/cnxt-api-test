package com.maritz.culturenext.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.maritz.culturenext.constants.ProjectConstants;

public enum MediaTypesEnum {
    // Image types
    IMAGE_JPEG("image/jpeg", ProjectConstants.JPEG),
    IMAGE_PNG("image/png", ProjectConstants.PNG),
    IMAGE_JPG("image/jpg", ProjectConstants.JPG),
    IMAGE_SVG("image/svg", ProjectConstants.SVG),
    IMAGE_SVG_XML("image/svg+xml", ProjectConstants.SVG),

    // Other types
    TEXT_CSV("text/csv", ProjectConstants.CSV),
    MS_EXCEL("application/vnd.ms-excel", ProjectConstants.MS_EXCEL),
    APPLICATION_OPEN_FORMAT_SPREADSHEET("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", 
            ProjectConstants.OFFICE_DOCUMENT_SPREADSHEET);


    private final String value;
    private final String fileExtension;
    
    private static final List<String> imageMediaTypes = Arrays.asList(IMAGE_JPEG.value(), IMAGE_PNG.value(), 
            IMAGE_JPG.value());

    MediaTypesEnum(String value, String fileExtension) {
        this.value = value;
        this.fileExtension = fileExtension;
    }

    public String value() {
        return this.value;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public static boolean containsImageMediaType(String s) {
        return imageMediaTypes.contains(s);
    }
    
    public static List<String> getMediaTypes() {
        List<String> types = new ArrayList<>();
        for (MediaTypesEnum mediaType : values()) {
            types.add(mediaType.value());
        }
        return types;
    }
    
    public static MediaTypesEnum fromValue(String value) {
        for(MediaTypesEnum mediaType: values()) {
            if (mediaType.value().equalsIgnoreCase(value)) {
                return mediaType;
            }
        }
        
        throw new IllegalArgumentException("No MediaTypesEnum matching value \"" + value + "\" exists.");
    }
    
    public static List<String> getCSVMediaTypes() {
        return Arrays.asList(TEXT_CSV.value(), MS_EXCEL.value());
    }
}
