package com.maritz.culturenext.cdn.service.impl;

import com.maritz.culturenext.cdn.service.CdnService;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class CdnServiceImpl implements CdnService {

    @Inject
    private Environment environment;

    @Override
    public String getDefaultUrl() {
        return environment.getProperty("cdn.local.url");

    }
}
