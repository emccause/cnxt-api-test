package com.maritz.culturenext.cdn.service;

public interface CdnService {

    /**
     * Return all CDN urls
     */
    String getDefaultUrl();
}
