package com.maritz.culturenext.cdn.rest;

import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.cdn.service.CdnService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import javax.inject.Inject;

@RestController
@Api(value = "cdn", description = "get the CDN URLs that has been stored in the application_data table")
public class CdnRestService {

    @Inject private CdnService cdnService;


    //rest/cdn
    @RequestMapping(value = "cdn", method = RequestMethod.GET)
    @ApiOperation(value = "Get CDN URL")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful retrieval of cdn url",
                    response = String.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Request error")
    })
    @Permission("PUBLIC")
    public @ResponseBody
    String getSiteContent() {
        return cdnService.getDefaultUrl();
    }

}