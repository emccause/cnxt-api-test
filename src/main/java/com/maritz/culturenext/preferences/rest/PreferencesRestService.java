package com.maritz.culturenext.preferences.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.annotation.Permission;
import com.maritz.culturenext.preferences.constants.PreferenceConstants;
import com.maritz.culturenext.preferences.dto.PreferenceDTO;
import com.maritz.culturenext.preferences.services.PreferencesService;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.inject.Inject;

import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("participants")
@Api(value="/preferences", description="All end points dealing with user preferences")
public class PreferencesRestService {

    @Inject private PreferencesService preferencesService;
    @Inject private Security security;

    //rest/participants/~/preferences
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = "{paxId}/preferences", method = RequestMethod.POST)
    @ApiOperation(value = "Creates key/value pairs for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created preferences", response = PreferenceDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found")
    })
    @Permission("PUBLIC")
    public List<PreferenceDTO> createPreferencesForUser(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @RequestBody List<PreferenceDTO> userPreferences) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            throw new NoHandlerFoundException(null, null, null);
        }

        return preferencesService.createPreferencesForUser(paxId, userPreferences);
    }

    //rest/participants/~/preferences/~
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = "{paxId}/preferences/{key}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates the value for the key/value pair for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated preference for user", 
                    response = PreferenceDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> updatePreferenceForUser(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(KEY_REST_PARAM) String key,
            @RequestBody PreferenceDTO userPreference) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (key != null && !key.isEmpty()) {
            userPreference.setKey(key);
        }

        try {
            return new ResponseEntity<Object>(preferencesService
                    .updatePreferenceForUser(paxId, userPreference), HttpStatus.OK);
        } catch (ErrorMessageException ex) {
            for (Iterator<ErrorMessage> iterator = ex.getErrorMessages().iterator(); iterator.hasNext(); ) {
                ErrorMessage errorMessage = iterator.next();
                if (errorMessage.getCode() != null && 
                        PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST.equalsIgnoreCase(errorMessage.getCode())) {
                    return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.NOT_FOUND);
                }
            }
            return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.BAD_REQUEST);
        }
    }

    //rest/participants/~/preferences/~
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/preferences/{key}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Deletes preference key/value pair for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted preference", response = PreferenceDTO.class),
            @ApiResponse(code = 404, message = "Not found")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> deletePreferenceForUser(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(KEY_REST_PARAM) String key) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            preferencesService.deletePreferenceForUser(paxId, key);

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (ErrorMessageException ex) {
            return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.NOT_FOUND);
        }
    }

    //rest/participants/~/preferences/~
    @PreAuthorize("@security.isMyPax(#paxIdString)")
    @RequestMapping(value = "{paxId}/preferences/{key}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the key/value pair for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved preference", response = PreferenceDTO.class),
            @ApiResponse(code = 404, message = "Not found")
    })
    @Permission("PUBLIC")
    public ResponseEntity<Object> getPreferenceForUser(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString,
            @PathVariable(KEY_REST_PARAM) String key) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        try {
            return new ResponseEntity<Object>(preferencesService.getPreferenceForUserByKey(paxId, key), HttpStatus.OK);
        } catch (ErrorMessageException ex) {
            return new ResponseEntity<Object>(ex.getErrorMessages(), HttpStatus.NOT_FOUND);
        }
    }

    //rest/participants/~/preferences
    @PreAuthorize("@security.isMyPax(#paxIdString) and !@security.isImpersonated()")
    @RequestMapping(value = "{paxId}/preferences", method = RequestMethod.GET)
    @ApiOperation(value = "Gets the key/value pairs for the user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved preferences", response = PreferenceDTO.class),
            @ApiResponse(code = 400, message = "Request error"),
            @ApiResponse(code = 404, message = "Not found")
    })
    @Permission("PUBLIC")
    public ResponseEntity<List<PreferenceDTO>> getAllPreferencesForUser(
            @PathVariable(PAX_ID_REST_PARAM) String paxIdString) throws Throwable {

        Long paxId = security.getPaxId(paxIdString);

        if (paxId == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(preferencesService.getAllPreferencesForUser(paxId), HttpStatus.OK);
    }

}
