package com.maritz.culturenext.preferences.constants;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.ProjectConstants;

public class PreferenceConstants {
    
    public static final int MAX_VALUE_SIZE = 4000;
    public static final int MAX_KEY_SIZE = 244;
    
    public static final String ERROR_INVALID_PREFERENCES_MISSING = "PREFERENCES_MISSING";
    public static final String ERROR_INVALID_PREFERENCES_MISSING_MSG = "List of preferences does not exist or is empty";

    public static final String ERROR_PREFERENCE_KEY_EXIST = "PREFERENCE_KEY_EXIST";
    public static final String ERROR_PREFERENCE_KEY_EXIST_MSG = "Preference with provided key already exists";

    public static final String ERROR_PREFERENCE_VALUE_MISSING = "MISSING_VALUE";
    public static final String ERROR_PREFERENCE_VALUE_MISSING_MSG = "A value is required";

    public static final String ERROR_PREFERENCE_KEY_MISSING = "MISSING_KEY";
    public static final String ERROR_PREFERENCE_KEY_MISSING_MSG = "A key is required";

    public static final String ERROR_PREFERENCE_KEY_NOT_EXIST = "KEY_NOT_EXIST";
    public static final String ERROR_PREFERENCE_KEY_NOT_EXIST_MSG = "Key does not exist for pax";
    
    public static final String ERROR_PREFERENCE_KEY_TOO_LONG = "KEY_TOO_LONG";
    public static final String ERROR_PREFERENCE_KEY_TOO_LONG_MSG = "A key is too long.";

    public static final String PREFERENCE_KEY_PREFIX = "preference.";
    
    //Error Messages
    public static final ErrorMessage INVALID_PREFERENCES_MISSING_MESSAGE = new ErrorMessage()
        .setCode(PreferenceConstants.ERROR_INVALID_PREFERENCES_MISSING)
        .setField(ProjectConstants.PREFERENCES)
        .setMessage(PreferenceConstants.ERROR_INVALID_PREFERENCES_MISSING_MSG);
    
    public static final ErrorMessage PREFERENCE_KEY_EXIST = new ErrorMessage()
        .setCode(PreferenceConstants.ERROR_PREFERENCE_KEY_EXIST)
        .setField(ProjectConstants.KEY)
        .setMessage(PreferenceConstants.ERROR_PREFERENCE_KEY_EXIST_MSG);
    
    public static final ErrorMessage PREFERENCE_KEY_MISSING = new ErrorMessage()
        .setCode(PreferenceConstants.ERROR_PREFERENCE_KEY_MISSING)
        .setField(ProjectConstants.KEY)
        .setMessage(PreferenceConstants.ERROR_PREFERENCE_KEY_MISSING_MSG);
    
    public static final ErrorMessage PREFERENCE_KEY_TOO_LONG_MESSAGE = new ErrorMessage()
        .setCode(PreferenceConstants.ERROR_PREFERENCE_KEY_TOO_LONG)
        .setField(ProjectConstants.KEY)
        .setMessage(PreferenceConstants.ERROR_PREFERENCE_KEY_TOO_LONG_MSG);
    
    public static final ErrorMessage PREFERENCE_VALUE_MISSING_MESSAGE = new ErrorMessage()
        .setCode(PreferenceConstants.ERROR_PREFERENCE_VALUE_MISSING)
        .setField(ProjectConstants.VALUE)
        .setMessage(PreferenceConstants.ERROR_PREFERENCE_VALUE_MISSING_MSG);
}
