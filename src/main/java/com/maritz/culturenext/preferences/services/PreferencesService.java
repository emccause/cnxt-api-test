package com.maritz.culturenext.preferences.services;

import java.util.List;

import com.maritz.culturenext.preferences.dto.PreferenceDTO;

public interface PreferencesService {

    /**
     * Creates key/value pairs to be stored for a participant in the PAX_DATA table
     * 
     * @param paxID - pax ID to create preferences for
     * @param preferences - Array of key/value pairs
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497109/Create+Preferences+for+Participant
     */
    List<PreferenceDTO> createPreferencesForUser(Long paxID, List<PreferenceDTO> preferences);

    /**
     * Update a participant's preference by key
     * 
     * @param paxID
     * @param preference
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497111/Update+Preference+for+Participant+by+Key
     */
    PreferenceDTO updatePreferenceForUser(Long paxID, PreferenceDTO preference);

    /**
     * Delete a specific key/value pair for a participant
     * 
     * @param PaxID
     * @param key
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497113/Delete+Preference+for+Participant+by+Key
     */
    void deletePreferenceForUser(Long PaxID, String key);

    /**
     * Return a list of all of the participant's PAX_DATA entries.
     * 
     * @param paxID
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497105/Get+List+of+Preferences+for+Participant
     */
    List<PreferenceDTO> getAllPreferencesForUser(Long paxID);

    /**
     * Look up a participant's preference by key
     * 
     * @param paxID
     * @param key
     * 
     * https://maritz.atlassian.net/wiki/spaces/M365/pages/19497107/Get+Preference+for+Participant+by+Key
     */
    PreferenceDTO getPreferenceForUserByKey(Long paxID, String key);


}
