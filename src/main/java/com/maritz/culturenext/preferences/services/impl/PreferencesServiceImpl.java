package com.maritz.culturenext.preferences.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.PaxData;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.preferences.constants.PreferenceConstants;
import com.maritz.culturenext.preferences.dto.PreferenceDTO;
import com.maritz.culturenext.preferences.services.PreferencesService;

@Component
public class PreferencesServiceImpl implements PreferencesService {

    private static final String PUBLIC_DEFAULT_OVERRIDE = "public.default.override";
    private static final String ACTIVITY_FEED_FILTER = "ACTIVITY_FEED_FILTER";
    @Inject private ConcentrixDao<PaxData> paxDataDao;
    @Inject private Environment environment;

    @Override
    public List<PreferenceDTO> createPreferencesForUser(Long paxId, List<PreferenceDTO> preferences) {
        List<PreferenceDTO> preferenceDTOs = new ArrayList<>();
        List<ErrorMessage> errors = new ArrayList<>();

        if (preferences == null || preferences.isEmpty()) {
            throw new ErrorMessageException(PreferenceConstants.INVALID_PREFERENCES_MISSING_MESSAGE);
        }

        for (PreferenceDTO preference : preferences) {
            validatePreferenceMissingKeyOrValue(preference, errors);

            if (preferenceExists(paxId, preference.getKey())) {
                errors.add(PreferenceConstants.PREFERENCE_KEY_EXIST);
            }

            if (errors.isEmpty()) {
                savePreference(paxId, preference);

                preferenceDTOs.add(preference);
            } else {
                throw new ErrorMessageException().addErrorMessages(errors);
            }
        }

        return preferenceDTOs;
    }

    @Override
    public PreferenceDTO updatePreferenceForUser(Long paxId, PreferenceDTO preference) {
        List<ErrorMessage> errors = new ArrayList<>();

        validatePreferenceMissingKeyOrValue(preference, errors);

        if (errors.isEmpty()) {
            PaxData paxDataToUpdate = getExistingPaxDataByKey(paxId, preference.getKey());

            paxDataToUpdate.setValue(preference.getValue());

            paxDataDao.update(paxDataToUpdate);
        } else {
            throw new ErrorMessageException().addErrorMessages(errors);
        }

        return preference;
    }

    @Override
    public void deletePreferenceForUser(Long paxId, String key) {
        paxDataDao.delete(getExistingPaxDataByKey(paxId, key));
    }

    @Override
    public List<PreferenceDTO> getAllPreferencesForUser(Long paxID) {
        return getPreferencesList(paxID);
    }

    @Override
    public PreferenceDTO getPreferenceForUserByKey(Long paxID, String key) {
        return getExistingPreferenceByKey(paxID, key);
    }

    /**
     * Checks for existing user preference by key
     * @param paxId - Participant Id
     * @param key - Preference key name
     * @return boolean
     */
    private boolean preferenceExists(Long paxId, String key) {
        return paxDataDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.KEY_NAME).eq(formatUserPreferenceKey(key))
                .findOne() != null;
    }

    /**
     * Gets existing pax preference from PaxData entity and builds preference dto
     * Throws KEY_NOT_EXIST error if not found.
     * @param paxId - Participant Id
     * @param key - User preference key
     * @return PreferenceDTO
     */
    private PreferenceDTO getExistingPreferenceByKey(Long paxId, String key) {
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        boolean shouldUsePublicDefaultOverride =
                (ACTIVITY_FEED_FILTER.equals(key))? Boolean.valueOf(environment.getProperty(PUBLIC_DEFAULT_OVERRIDE)):
                false;
        if (! shouldUsePublicDefaultOverride) {
            PaxData paxData = getExistingPaxDataByKey(paxId, key);
            preferenceDTO.setKey(unformatUserPreferenceKey(paxData.getKeyName()));
            preferenceDTO.setValue(paxData.getValue());
        }
        else {
            preferenceDTO.setKey(key);
            preferenceDTO.setValue("\"PUBLIC\"");
        }

        return preferenceDTO;
    }

    /**
     * Gets most recent existing key/value pair of PaxData by key name.
     * Throws KEY_NOT_EXIST error if not found.
     * @param paxId - Participant Id
     * @param key - User preference key
     * @return PaxMisc
     */
    private PaxData getExistingPaxDataByKey(Long paxId, String key) {
        PaxData paxData = paxDataDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.KEY_NAME).eq(formatUserPreferenceKey(key))
                .findOne();

        if (paxData == null) {
            throw new ErrorMessageException(new ErrorMessage()
                    .setCode(PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST)
                    .setField(ProjectConstants.KEY)
                    .setMessage(PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST_MSG));
        }

        return paxData;
    }

    /**
     * Gets and builds list of pax preferences from PaxMisc entities
     * @param paxId - Participant Id
     * @return List<PreferenceDTO>
     */
    private List<PreferenceDTO> getPreferencesList(Long paxId) {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();

        List<PaxData> paxDataList = paxDataDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(paxId)
                .and(ProjectConstants.KEY_NAME).contains(PreferenceConstants.PREFERENCE_KEY_PREFIX)
                .find();

        for (PaxData paxData : paxDataList) {
            PreferenceDTO preferenceDTO = new PreferenceDTO();
            preferenceDTO.setKey(unformatUserPreferenceKey(paxData.getKeyName()));
            preferenceDTO.setValue(paxData.getValue());

            preferenceDTOList.add(preferenceDTO);
        }

        return preferenceDTOList;
    }

    /**
     * Saves given preference for participant
     * @param paxId - Participant Id
     * @param preferenceDTO - Preference to save
     */
    private void savePreference(Long paxId, PreferenceDTO preferenceDTO) {
        PaxData paxData = new PaxData();
        paxData.setPaxId(paxId);
        paxData.setKeyName(formatUserPreferenceKey(preferenceDTO.getKey()));
        paxData.setValue(preferenceDTO.getValue());

        paxDataDao.save(paxData);
    }

    /**
     * Formats user preference key for use with PaxMisc entity
     * @param key - User preference key
     * @return String
     */
    private String formatUserPreferenceKey(String key) {
        return key != null ? new StringBuilder()
                .append(PreferenceConstants.PREFERENCE_KEY_PREFIX).append(key.trim()).toString() : key;
    }

    /**
     * Unformats PaxMisc key for user preference
     * @param key - User preference key
     * @return String
     */
    private String unformatUserPreferenceKey(String key) {
        return key != null ? key.replace(PreferenceConstants.PREFERENCE_KEY_PREFIX, "") : key;
    }

    /**
     * Checks for missing key and/or value for given user preference
     * @param preferenceDTO - Preference object to check
     * @param errors - List of ErrorMessages to append potential errors
     */
    private void validatePreferenceMissingKeyOrValue(PreferenceDTO preferenceDTO, List<ErrorMessage> errors) {
        if (preferenceDTO == null || preferenceDTO.getKey() == null || preferenceDTO.getKey().isEmpty()) {
            errors.add(PreferenceConstants.PREFERENCE_KEY_MISSING);
        }else if(preferenceDTO.getKey().length() > PreferenceConstants.MAX_KEY_SIZE){
            errors.add(PreferenceConstants.PREFERENCE_KEY_TOO_LONG_MESSAGE);
        }

        if (preferenceDTO == null || preferenceDTO.getValue() == null || preferenceDTO.getValue().isEmpty()) {
            errors.add(PreferenceConstants.PREFERENCE_VALUE_MISSING_MESSAGE);
        }
        else if(preferenceDTO.getValue().length() > PreferenceConstants.MAX_VALUE_SIZE){
            errors.add(PreferenceConstants.PREFERENCE_KEY_TOO_LONG_MESSAGE);
        }
    }
}
