package com.maritz.core.util;

import java.util.function.Predicate;

import com.maritz.core.dto.FileDTO;

/**
* COPIED FROM CORE 7.1.0 - HARWELLM
* Brought locally to remove .toLowerCase() from the fileNameStartsWith method.
* This is to fix our enrollment job - files must begin with enrollmentLoad (capital L)

* MODIFICATIONS:
* (2017-01-02): Allow fileNameStartsWith() to be case sensitive
*     
*/

public class FileDTOPredicates {

    public static Predicate<FileDTO> fileNameEndsWith(String suffix) {
        return f -> f.getName().toLowerCase().endsWith(suffix);
    }

    public static Predicate<FileDTO> fileNameStartsWith(String prefix) {
        return f -> f.getName().startsWith(prefix);
    }

}
