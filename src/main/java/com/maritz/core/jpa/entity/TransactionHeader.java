package com.maritz.core.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;
import com.maritz.core.jpa.support.audit.Auditable;
import com.maritz.core.jpa.support.audit.AuditableEntityListener;

@ConcentrixDao
@Entity
@EntityListeners(AuditableEntityListener.class)
@Table(name = "TRANSACTION_HEADER")
public class TransactionHeader implements Auditable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID", insertable = false, updatable = false)
    private Long id;
    @Column(name="PAX_GROUP_ID")
    private Long paxGroupId;
    @Column(name="TRANSACTION_CLAIM_ID")
    private Long transactionClaimId;
    @Column(name="BATCH_ID")
    private Long batchId;
    @Column(name="PERIOD_ID")
    private Long periodId;
    @Column(name="PROGRAM_ID")
    private Long programId;
    @NotNull
    @Column(name="TYPE_CODE")
    private String typeCode;
    @NotNull
    @Column(name="SUB_TYPE_CODE")
    private String subTypeCode;
    @NotNull
    @Column(name="ACTIVITY_DATE")
    private java.util.Date activityDate;
    @NotNull
    @Column(name="STATUS_TYPE_CODE")
    private String statusTypeCode;
    @Column(name="STATUS_CHANGE_DETAIL")
    private String statusChangeDetail;
    @Column(name="ACCOUNT_NUMBER")
    private String accountNumber;
    @Column(name="PERFORMANCE_METRIC")
    private String performanceMetric;
    @Column(name="PERFORMANCE_VALUE")
    private Double performanceValue;
    @Column(name="COUNTRY_CODE")
    private String countryCode;
    @Column(name="PROXY_PAX_ID")
    private Long proxyPaxId;
    @NotNull
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @NotNull
    @CreatedBy
    @Column(name="CREATE_ID", updatable = false)
    private String createId;
    @NotNull
    @LastModifiedDate
    @Column(name="UPDATE_DATE")
    private java.util.Date updateDate;
    @NotNull
    @LastModifiedBy
    @Column(name="UPDATE_ID")
    private String updateId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public TransactionHeader() {
    }
    
    public Long getId() {
        return id;
    }
        
    public TransactionHeader setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getPaxGroupId() {
        return paxGroupId;
    }
        
    public TransactionHeader setPaxGroupId(Long paxGroupId) {
        this.paxGroupId = paxGroupId;
        return this;
    }

    public Long getTransactionClaimId() {
        return transactionClaimId;
    }
        
    public TransactionHeader setTransactionClaimId(Long transactionClaimId) {
        this.transactionClaimId = transactionClaimId;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }
        
    public TransactionHeader setBatchId(Long batchId) {
        this.batchId = batchId;
        return this;
    }

    public Long getPeriodId() {
        return periodId;
    }
        
    public TransactionHeader setPeriodId(Long periodId) {
        this.periodId = periodId;
        return this;
    }

    public Long getProgramId() {
        return programId;
    }
        
    public TransactionHeader setProgramId(Long programId) {
        this.programId = programId;
        return this;
    }

    public String getTypeCode() {
        return typeCode;
    }
        
    public TransactionHeader setTypeCode(String typeCode) {
        this.typeCode = typeCode;
        return this;
    }

    public String getSubTypeCode() {
        return subTypeCode;
    }
        
    public TransactionHeader setSubTypeCode(String subTypeCode) {
        this.subTypeCode = subTypeCode;
        return this;
    }

    public java.util.Date getActivityDate() {
        return activityDate;
    }
        
    public TransactionHeader setActivityDate(java.util.Date activityDate) {
        this.activityDate = activityDate;
        return this;
    }

    public String getStatusTypeCode() {
        return statusTypeCode;
    }
        
    public TransactionHeader setStatusTypeCode(String statusTypeCode) {
        this.statusTypeCode = statusTypeCode;
        return this;
    }

    public String getStatusChangeDetail() {
        return statusChangeDetail;
    }
        
    public TransactionHeader setStatusChangeDetail(String statusChangeDetail) {
        this.statusChangeDetail = statusChangeDetail;
        return this;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
        
    public TransactionHeader setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public String getPerformanceMetric() {
        return performanceMetric;
    }
        
    public TransactionHeader setPerformanceMetric(String performanceMetric) {
        this.performanceMetric = performanceMetric;
        return this;
    }

    public Double getPerformanceValue() {
        return performanceValue;
    }
        
    public TransactionHeader setPerformanceValue(Double performanceValue) {
        this.performanceValue = performanceValue;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }
        
    public TransactionHeader setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public Long getProxyPaxId() {
        return proxyPaxId;
    }

    public void setProxyPaxId(Long proxyPaxId) {
        this.proxyPaxId = proxyPaxId;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }
        
    public TransactionHeader setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getCreateId() {
        return createId;
    }
        
    public TransactionHeader setCreateId(String createId) {
        this.createId = createId;
        return this;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }
        
    public TransactionHeader setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getUpdateId() {
        return updateId;
    }
        
    public TransactionHeader setUpdateId(String updateId) {
        this.updateId = updateId;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public TransactionHeader setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public TransactionHeader setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
