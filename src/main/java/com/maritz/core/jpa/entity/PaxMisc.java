package com.maritz.core.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;
import com.maritz.core.jpa.support.audit.Auditable;
import com.maritz.core.jpa.support.audit.AuditableEntityListener;

@ConcentrixDao
@Entity
@EntityListeners(AuditableEntityListener.class)
@Table(name = "PAX_MISC")
public class PaxMisc implements Auditable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="PAX_MISC_ID", insertable = false, updatable = false)
    private Long paxMiscId;
    @NotNull
    @Column(name="PAX_ID")
    private Long paxId;
    @NotNull
    @Column(name="VF_NAME")
    private String vfName;
    @Column(name="MISC_DATA")
    private String miscData;
    @Column(name="MISC_DATE")
    private java.util.Date miscDate;
    @Column(name="MISC_TYPE_CODE")
    private String miscTypeCode;
    @NotNull
    @CreatedDate
    @Column(name="CREATE_DATE", updatable = false)
    private java.util.Date createDate;
    @NotNull
    @CreatedBy
    @Column(name="CREATE_ID", updatable = false)
    private String createId;
    @NotNull
    @LastModifiedDate
    @Column(name="UPDATE_DATE")
    private java.util.Date updateDate;
    @NotNull
    @LastModifiedBy
    @Column(name="UPDATE_ID")
    private String updateId;
    @Column(name="PROXY_PAX_ID")
    private Long proxyPaxId;
    @Transient
    private long crc;
    @Transient
    private boolean markForDelete;
    
    public PaxMisc() {
    }
    
    public Long getPaxMiscId() {
        return paxMiscId;
    }
        
    public PaxMisc setPaxMiscId(Long paxMiscId) {
        this.paxMiscId = paxMiscId;
        return this;
    }

    public Long getPaxId() {
        return paxId;
    }
        
    public PaxMisc setPaxId(Long paxId) {
        this.paxId = paxId;
        return this;
    }

    public String getVfName() {
        return vfName;
    }
        
    public PaxMisc setVfName(String vfName) {
        this.vfName = vfName;
        return this;
    }

    public String getMiscData() {
        return miscData;
    }
        
    public PaxMisc setMiscData(String miscData) {
        this.miscData = miscData;
        return this;
    }

    public java.util.Date getMiscDate() {
        return miscDate;
    }
        
    public PaxMisc setMiscDate(java.util.Date miscDate) {
        this.miscDate = miscDate;
        return this;
    }

    public String getMiscTypeCode() {
        return miscTypeCode;
    }
        
    public PaxMisc setMiscTypeCode(String miscTypeCode) {
        this.miscTypeCode = miscTypeCode;
        return this;
    }

    public java.util.Date getCreateDate() {
        return createDate;
    }
        
    public PaxMisc setCreateDate(java.util.Date createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getCreateId() {
        return createId;
    }
        
    public PaxMisc setCreateId(String createId) {
        this.createId = createId;
        return this;
    }

    public java.util.Date getUpdateDate() {
        return updateDate;
    }
        
    public PaxMisc setUpdateDate(java.util.Date updateDate) {
        this.updateDate = updateDate;
        return this;
    }

    public String getUpdateId() {
        return updateId;
    }
        
    public PaxMisc setUpdateId(String updateId) {
        this.updateId = updateId;
        return this;
    }

    public Long getProxyPaxId() {
        return proxyPaxId;
    }
        
    public PaxMisc setProxyPaxId(Long proxyPaxId) {
        this.proxyPaxId = proxyPaxId;
        return this;
    }

    public long getCrc() {
        return crc;
    }
    
    public PaxMisc setCrc(long crc) {
        this.crc = crc;
        return this;
    }
    
    public boolean getMarkForDelete() {
        return markForDelete;
    }
    
    public PaxMisc setMarkForDelete(boolean markForDelete) {
        this.markForDelete = markForDelete;
        return this;
    }
    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
