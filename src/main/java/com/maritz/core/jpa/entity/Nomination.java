package com.maritz.core.jpa.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.maritz.core.jdbc.annotation.ConcentrixDao;
import com.maritz.core.jpa.support.audit.Auditable;
import com.maritz.core.jpa.support.audit.AuditableEntityListener;

@ConcentrixDao
@Entity
@EntityListeners(AuditableEntityListener.class)
@Table(name = "NOMINATION")
public class Nomination implements Auditable {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID", insertable = false, updatable = false)
	private Long id;
	@NotNull
	@Column(name="NOMINATION_TYPE_CODE")
	private String nominationTypeCode;
	@NotNull
	@Column(name="PROGRAM_ID")
	private Long programId;
	@NotNull
	@Column(name="SUBMITTER_PAX_ID")
	private Long submitterPaxId;
	@Column(name="BUDGET_ID")
	private Long budgetId;
	@Column(name="ECARD_ID")
	private Long ecardId;
	@Column(name="PROXY_PAX_ID")
	private Long proxyPaxId;
	@Column(name="PARENT_ID")
	private Long parentId;
	@Column(name="NOMINATION_NAME")
	private String nominationName;
	@Column(name="SUBMITTAL_DATE")
	private java.util.Date submittalDate;
	@Column(name="WORKFLOW_STATE")
	private String workflowState;
	@Column(name="PENDING_LEVEL")
	private Integer pendingLevel;
	@Column(name="BATCH_ID")
	private Long batchId;
	@Column(name="COMMENT_JUSTIFICATION")
	private String commentJustification;
	@Column(name="COMMENT")
	private String comment;
	@Column(name="NEWSFEED_VISIBILITY")
	private String newsfeedVisibility;
	@Column(name="HEADLINE_COMMENT")
	private String headlineComment;
	@NotNull
	@Column(name="STATUS_TYPE_CODE")
	private String statusTypeCode;
	@NotNull
	@CreatedDate
	@Column(name="CREATE_DATE", updatable = false)
	private java.util.Date createDate;
	@NotNull
	@CreatedBy
	@Column(name="CREATE_ID", updatable = false)
	private String createId;
	@NotNull
	@LastModifiedDate
	@Column(name="UPDATE_DATE")
	private java.util.Date updateDate;
	@NotNull
	@LastModifiedBy
	@Column(name="UPDATE_ID")
	private String updateId;
	@Column(name="IDENTIFIER")
	private String identifier;
	@Transient
	private long crc;
	@Transient
	private boolean markForDelete;
	
	public Nomination() {
	}
	
	public Long getId() {
		return id;
	}
		
	public Nomination setId(Long id) {
		this.id = id;
		return this;
	}

	public String getNominationTypeCode() {
		return nominationTypeCode;
	}
		
	public Nomination setNominationTypeCode(String nominationTypeCode) {
		this.nominationTypeCode = nominationTypeCode;
		return this;
	}

	public Long getProgramId() {
		return programId;
	}
		
	public Nomination setProgramId(Long programId) {
		this.programId = programId;
		return this;
	}

	public Long getSubmitterPaxId() {
		return submitterPaxId;
	}
		
	public Nomination setSubmitterPaxId(Long submitterPaxId) {
		this.submitterPaxId = submitterPaxId;
		return this;
	}

	public Long getBudgetId() {
		return budgetId;
	}
		
	public Nomination setBudgetId(Long budgetId) {
		this.budgetId = budgetId;
		return this;
	}

	public Long getEcardId() {
		return ecardId;
	}
		
	public Nomination setEcardId(Long ecardId) {
		this.ecardId = ecardId;
		return this;
	}

	public Long getProxyPaxId() {
		return proxyPaxId;
	}
		
	public Nomination setProxyPaxId(Long proxyPaxId) {
		this.proxyPaxId = proxyPaxId;
		return this;
	}

	public Long getParentId() {
		return parentId;
	}
		
	public Nomination setParentId(Long parentId) {
		this.parentId = parentId;
		return this;
	}

	public String getNominationName() {
		return nominationName;
	}
		
	public Nomination setNominationName(String nominationName) {
		this.nominationName = nominationName;
		return this;
	}

	public java.util.Date getSubmittalDate() {
		return submittalDate;
	}
		
	public Nomination setSubmittalDate(java.util.Date submittalDate) {
		this.submittalDate = submittalDate;
		return this;
	}

	public String getWorkflowState() {
		return workflowState;
	}
		
	public Nomination setWorkflowState(String workflowState) {
		this.workflowState = workflowState;
		return this;
	}

	public Integer getPendingLevel() {
		return pendingLevel;
	}
		
	public Nomination setPendingLevel(Integer pendingLevel) {
		this.pendingLevel = pendingLevel;
		return this;
	}

	public Long getBatchId() {
		return batchId;
	}
		
	public Nomination setBatchId(Long batchId) {
		this.batchId = batchId;
		return this;
	}

	public String getCommentJustification() {
		return commentJustification;
	}
		
	public Nomination setCommentJustification(String commentJustification) {
		this.commentJustification = commentJustification;
		return this;
	}

	public String getComment() {
		return comment;
	}
		
	public Nomination setComment(String comment) {
		this.comment = comment;
		return this;
	}

	public String getNewsfeedVisibility() {
		return newsfeedVisibility;
	}
		
	public Nomination setNewsfeedVisibility(String newsfeedVisibility) {
		this.newsfeedVisibility = newsfeedVisibility;
		return this;
	}

	public String getHeadlineComment() {
		return headlineComment;
	}
		
	public Nomination setHeadlineComment(String headlineComment) {
		this.headlineComment = headlineComment;
		return this;
	}

	public String getStatusTypeCode() {
		return statusTypeCode;
	}
		
	public Nomination setStatusTypeCode(String statusTypeCode) {
		this.statusTypeCode = statusTypeCode;
		return this;
	}

	public java.util.Date getCreateDate() {
		return createDate;
	}
		
	public Nomination setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
		return this;
	}

	public String getCreateId() {
		return createId;
	}
		
	public Nomination setCreateId(String createId) {
		this.createId = createId;
		return this;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}
		
	public Nomination setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public String getUpdateId() {
		return updateId;
	}
		
	public Nomination setUpdateId(String updateId) {
		this.updateId = updateId;
		return this;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public long getCrc() {
		return crc;
	}
	
	public Nomination setCrc(long crc) {
		this.crc = crc;
		return this;
	}
	
	public boolean getMarkForDelete() {
		return markForDelete;
	}
	
	public Nomination setMarkForDelete(boolean markForDelete) {
		this.markForDelete = markForDelete;
		return this;
	}
	
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
