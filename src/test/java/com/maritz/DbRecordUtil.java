package com.maritz;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Handles records within the DB for testing.
 */
public class DbRecordUtil {

    // Batch Type Codes
    private static final String POINT_LOAD_BATCH_TYPE_CODE = "CPTD";
    private static final String RECOGNITION_UPLOAD_BATCH_TYPE_CODE = "RECG";

    // Status Type Codes
    private static final String SUCCESS_STATUS_TYPE_CODE = "SUCCESS";
    // Batch Event Type Codes
    private static final String USER_BATCH_EVENT_TYPE_CODE = "USERID";
    private static final String START_TIME_BATCH_EVENT_TYPE_CODE = "STRTIM";
    private static final String END_TIME_BATCH_EVENT_TYPE_CODE = "ENDTIM";
    private static final String FILE_NAME_BATCH_EVENT_TYPE_CODE = "FILE";
    private static final String TOTAL_RECORDS_BATCH_EVENT_TYPE_CODE = "RECS";
    private static final String GOOD_RECORDS_BATCH_EVENT_TYPE_CODE = "OKRECS";
    private static final String ERROR_RECORDS_BATCH_EVENT_TYPE_CODE = "ERRS";

    public static Batch generateRecognitionActivityRecords(ConcentrixDao<Batch> batchDao,
                                                          ConcentrixDao<BatchEvent> batchEventDao,
                                                          String fileName,
                                                          String statusTypeCode,
                                                          Long totalRecords,
                                                          Long goodRecords,
                                                          Long errorRecords,
                                                          String startTime,
                                                          String endTime)
    {
        return generateActivityRecords(batchDao, batchEventDao, RECOGNITION_UPLOAD_BATCH_TYPE_CODE, fileName, 
                statusTypeCode, totalRecords, goodRecords, errorRecords, startTime, endTime);
    }

    public static Batch generatePointLoadActivityRecords(ConcentrixDao<Batch> batchDao,
                                                        ConcentrixDao<BatchEvent> batchEventDao,
                                                        String fileName,
                                                        String statusTypeCode,
                                                        Long totalRecords,
                                                        Long goodRecords,
                                                        Long errorRecords,
                                                        String startTime,
                                                        String endTime)
    {
        return generateActivityRecords(batchDao, batchEventDao, POINT_LOAD_BATCH_TYPE_CODE, fileName,
                statusTypeCode, totalRecords, goodRecords, errorRecords, startTime, endTime);
    }

    private static Batch generateActivityRecords(ConcentrixDao<Batch> batchDao,
                                                ConcentrixDao<BatchEvent> batchEventDao,
                                                String batchTypeCode,
                                                String fileName,
                                                String statusTypeCode,
                                                Long totalRecords,
                                                Long goodRecords,
                                                Long errorRecords,
                                                String startTime,
                                                String endTime)
    {
        // create a batch record
        Batch batch = generateBatchRecord(batchDao, batchTypeCode, statusTypeCode);

        // create corresponding batch_event records
        generatePointLoadBatchEvents(batchEventDao, batch.getBatchId(), fileName, totalRecords,
                goodRecords, errorRecords, startTime, endTime);

        return batch;
    }

    private static Batch generateBatchRecord(ConcentrixDao<Batch> batchDao,
                                            String batchTypeCode,
                                            String statusTypeCode) {
        // setup of entity
        Batch batch = new Batch();
        batch.setBatchTypeCode(batchTypeCode);
        batch.setStatusTypeCode(statusTypeCode);

        batchDao.create(batch);

        // verify actual creation
        if(batch.getBatchId() == null) {
            throw new RuntimeException("Could not create Batch record!!");
        }

        return batch;
    }

    public static void generatePointLoadTransactions(ConcentrixDao<TransactionHeader> transactionHeaderDao,
                                                      ConcentrixDao<Discretionary> discretionaryDao,
                                                      Long batchId,
                                                      Long budgetId,
                                                      Long totalPoints,
                                                      String transactionStatusCode,
                                                      String typeCode,
                                                      String subTypeCode) {
        // if specified, create transaction_header and discretionary records
        if(totalPoints != null && totalPoints > 0L) {
            Date activityDate = new Date();

            // create transaction header and budget allocation records (# of records = total points / MAX_POINT_LIMIT)
            while(totalPoints > 0L) {
                Long transactionPoints = (totalPoints < TestConstants.ID_1) ? totalPoints : TestConstants.ID_1;

                TransactionHeader transactionHeader = new TransactionHeader();
                transactionHeader.setBatchId(batchId);
                transactionHeader.setActivityDate(activityDate);
                transactionHeader.setStatusTypeCode(transactionStatusCode);
                transactionHeader.setTypeCode(typeCode);
                transactionHeader.setSubTypeCode(subTypeCode);
                transactionHeader.setPerformanceValue(transactionPoints.doubleValue());
                transactionHeader.setCountryCode("US");
                transactionHeader.setPerformanceMetric("Test Batch Record Generation");
                transactionHeaderDao.create(transactionHeader);

                Long transactionHeaderId = transactionHeader.getId();
                if(transactionHeaderId == null){
                    throw new RuntimeException("Could not create Transaction_Header record!!");
                }

                Discretionary budgetAllocation = new Discretionary();
                budgetAllocation.setAmount(transactionPoints.doubleValue());
                budgetAllocation.setTransactionHeaderId(transactionHeaderId);
                budgetAllocation.setBudgetIdCredit(budgetId);
                budgetAllocation.setDescription("Test Batch Record Generation");

                discretionaryDao.create(budgetAllocation);

                totalPoints -= transactionPoints;
            }
        }
    }

    private static void generatePointLoadBatchEvents(ConcentrixDao<BatchEvent> batchEventDao,
                                                     Long batchId,
                                                     String filename,
                                                     Long totalRecords,
                                                     Long goodRecords,
                                                     Long errorRecords,
                                                     String startTime,
                                                     String endTime) {
        // generate batchEvents for userId, total records, error records, filename, and good records
        batchEventDao.create(generateBatchEvent(batchId, USER_BATCH_EVENT_TYPE_CODE, "auto"));

        if(filename != null) {
            batchEventDao.create(generateBatchEvent(batchId, FILE_NAME_BATCH_EVENT_TYPE_CODE, filename));
        }

        if(totalRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, TOTAL_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    totalRecords.toString()));
        }

        if(goodRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, GOOD_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    goodRecords.toString()));
        }

        if(errorRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, ERROR_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    errorRecords.toString()));
        }

        if(startTime != null){
            batchEventDao.create(generateBatchEvent(batchId, START_TIME_BATCH_EVENT_TYPE_CODE, startTime));
        }

        if(endTime != null){
            batchEventDao.create(generateBatchEvent(batchId, END_TIME_BATCH_EVENT_TYPE_CODE, endTime));
        }
    }

    private static BatchEvent generateBatchEvent(Long batchId,
                                                 String batchEventTypeCode,
                                                 String message) {
        return generateBatchEvent(batchId, batchEventTypeCode, message, SUCCESS_STATUS_TYPE_CODE);
    }

    private static BatchEvent generateBatchEvent(Long batchId,
                                                 String batchEventTypeCode,
                                                 String message,
                                                 String batchEventStatusTypeCode) {
        BatchEvent batchEvent = new BatchEvent();

        batchEvent.setBatchId(batchId);
        batchEvent.setMessage(message);
        batchEvent.setBatchEventTypeCode(batchEventTypeCode);
        batchEvent.setStatusTypeCode(batchEventStatusTypeCode);

        return batchEvent;
    }


    /**
     * Verifies the statuses of the transaction_header and discretionary records associated with
     * the specified batch ID.
     *
     * @param batchId batch ID
     * @param expectedTransactionHeaderStatus expected transaction_header status
     * @param expectedBudgetAllocationStatus expected discretionary status
     */
    public static void verifyTransactionStatusChanges(ConcentrixDao<TransactionHeader> transactionHeaderDao,
                                                       Long batchId,
                                                       String expectedTransactionHeaderStatus) {
        List<Long> transactionHeaderIds = new ArrayList<Long>();

        List<TransactionHeader> transactionHeaderRecords = transactionHeaderDao.findBy()
                .where(ProjectConstants.BATCH_ID).eq(batchId)
                .find();

        for(TransactionHeader transactionHeader : transactionHeaderRecords) {
            assertEquals(expectedTransactionHeaderStatus, transactionHeader.getStatusTypeCode());

            transactionHeaderIds.add(transactionHeader.getId());
        }
    }
}
