package com.maritz;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;

/**
 * Generates records within the DB for testing.
 */
public class DbRecordGenerator {

    // Batch Type Codes
    private static final String POINT_LOAD_BATCH_TYPE_CODE = "CPTD";
    private static final String RECOGNITION_UPLOAD_BATCH_TYPE_CODE = "RECG";

    // Status Type Codes
    private static final String SUCCESS_STATUS_TYPE_CODE = "SUCCESS";

    // Batch Event Type Codes
    private static final String USER_BATCH_EVENT_TYPE_CODE = "USERID";
    private static final String START_TIME_BATCH_EVENT_TYPE_CODE = "STRTIM";
    private static final String END_TIME_BATCH_EVENT_TYPE_CODE = "ENDTIM";
    private static final String FILE_NAME_BATCH_EVENT_TYPE_CODE = "FILE";
    private static final String TOTAL_RECORDS_BATCH_EVENT_TYPE_CODE = "RECS";
    private static final String GOOD_RECORDS_BATCH_EVENT_TYPE_CODE = "OKRECS";
    private static final String ERROR_RECORDS_BATCH_EVENT_TYPE_CODE = "ERRS";

    public static void generateRecognitionActivityRecords(ConcentrixDao<Batch> batchDao,
                                                          ConcentrixDao<BatchEvent> batchEventDao,
                                                          String fileName,
                                                          String statusTypeCode,
                                                          Long totalRecords,
                                                          Long goodRecords,
                                                          Long errorRecords,
                                                          String startTime,
                                                          String endTime)
    {
        generateActivityRecords(batchDao, batchEventDao, RECOGNITION_UPLOAD_BATCH_TYPE_CODE, fileName, statusTypeCode,
                totalRecords, goodRecords, errorRecords, startTime, endTime);
    }

    public static void generatePointLoadActivityRecords(ConcentrixDao<Batch> batchDao,
                                                        ConcentrixDao<BatchEvent> batchEventDao,
                                                        String fileName,
                                                        String statusTypeCode,
                                                        Long totalRecords,
                                                        Long goodRecords,
                                                        Long errorRecords,
                                                        String startTime,
                                                        String endTime)
    {
        generateActivityRecords(batchDao, batchEventDao, POINT_LOAD_BATCH_TYPE_CODE, fileName, statusTypeCode,
                                totalRecords, goodRecords, errorRecords, startTime, endTime);
    }

    private static void generateActivityRecords(ConcentrixDao<Batch> batchDao,
                                                ConcentrixDao<BatchEvent> batchEventDao,
                                                String batchTypeCode,
                                                String fileName,
                                                String statusTypeCode,
                                                Long totalRecords,
                                                Long goodRecords,
                                                Long errorRecords,
                                                String startTime,
                                                String endTime)
    {
        // create a batch record
        Batch batch = generateBatchRecord(batchDao, batchTypeCode, statusTypeCode);

        // create corresponding batch_event records
        generatePointLoadBatchEvents(batchEventDao, batch.getBatchId(), fileName, totalRecords,
                goodRecords, errorRecords, startTime, endTime);
    }

    private static Batch generateBatchRecord(ConcentrixDao<Batch> batchDao,
                                            String batchTypeCode,
                                            String statusTypeCode) {
        // setup of entity
        Batch batch = new Batch();
        batch.setBatchTypeCode(batchTypeCode);
        batch.setStatusTypeCode(statusTypeCode);

        batchDao.create(batch);

        // verify actual creation
        if(batch.getBatchId() == null) {
            throw new RuntimeException("Could not create Batch record!!");
        }

        return batch;
    }

    private static void generatePointLoadBatchEvents(ConcentrixDao<BatchEvent> batchEventDao,
                                                     Long batchId,
                                                     String filename,
                                                     Long totalRecords,
                                                     Long goodRecords,
                                                     Long errorRecords,
                                                     String startTime,
                                                     String endTime) {
        // generate batchEvents for userId, total records, error records, filename, and good records
        batchEventDao.create(generateBatchEvent(batchId, USER_BATCH_EVENT_TYPE_CODE, "auto"));

        if(filename != null) {
            batchEventDao.create(generateBatchEvent(batchId, FILE_NAME_BATCH_EVENT_TYPE_CODE, filename));
        }

        if(totalRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, TOTAL_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    totalRecords.toString()));
        }

        if(goodRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, GOOD_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    goodRecords.toString()));
        }

        if(errorRecords != null){
            batchEventDao.create(generateBatchEvent(batchId, ERROR_RECORDS_BATCH_EVENT_TYPE_CODE, 
                    errorRecords.toString()));
        }

        if(startTime != null){
            batchEventDao.create(generateBatchEvent(batchId, START_TIME_BATCH_EVENT_TYPE_CODE, startTime));
        }

        if(endTime != null){
            batchEventDao.create(generateBatchEvent(batchId, END_TIME_BATCH_EVENT_TYPE_CODE, endTime));
        }
    }

    private static BatchEvent generateBatchEvent(Long batchId,
                                                 String batchEventTypeCode,
                                                 String message) {
        return generateBatchEvent(batchId, batchEventTypeCode, message, SUCCESS_STATUS_TYPE_CODE);
    }

    private static BatchEvent generateBatchEvent(Long batchId,
                                                 String batchEventTypeCode,
                                                 String message,
                                                 String batchEventStatusTypeCode) {
        BatchEvent batchEvent = new BatchEvent();

        batchEvent.setBatchId(batchId);
        batchEvent.setMessage(message);
        batchEvent.setBatchEventTypeCode(batchEventTypeCode);
        batchEvent.setStatusTypeCode(batchEventStatusTypeCode);

        return batchEvent;
    }
}
