package com.maritz;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;

import org.apache.commons.codec.binary.StringUtils;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Basic util class for testing. Includes common functionality for testing and verification.
 */
public class TestUtil {
    /**
     * Checks that validation errors are of specified error. Also asserts expected number of errors.
     * Only validates the error code
     *
     * @param expectedErrors    expected errors
     * @param errorMsgException actual error exception with errors
     */
    public static void assertValidationErrors(@Nonnull List<String> expectedErrors,
            @Nonnull ErrorMessageException errorMsgException) {
        assertNotNull(errorMsgException.getErrorMessages());
        assertEquals(expectedErrors.size(), errorMsgException.getErrorMessages().size());

        List<String> actualErrorCodes = new ArrayList<String>();
        for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
            actualErrorCodes.add(errorMessage.getCode());
        }

        // sort and verify equality of error codes
        Collections.sort(expectedErrors);
        Collections.sort(actualErrorCodes);

        assertEquals(expectedErrors, actualErrorCodes);
    }
    
    /**
     * Checks that validation errors are of specified error. Also asserts expected number of errors.
     *
     * @param expectedNumberOfErrors expected number of errors
     * @param expectedError          expected error
     * @param errorMsgException      actual error exception with errors
     */
    public static void assertValidationErrors(int expectedNumberOfErrors, String expectedError,
            ErrorMessageException errorMsgException) {
        assertEquals(expectedNumberOfErrors, errorMsgException.getErrorMessages().size());
        for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
            assertEquals(expectedError, errorMessage.getCode());
        }
    }
    
    /**
     * Checks that validation errors are of specified error. Also asserts expected number of errors.
     *
     * @param expectedNumberOfErrors expected number of errors
     * @param expectedErrorException the expected error object
     * @param errorMsgException      actual error object exception with errors
     */
    public static void assertValidationErrors(int expectedNumberOfErrors, ErrorMessage expectedError,
            ErrorMessageException errorMsgException) {
        assertEquals(expectedNumberOfErrors, errorMsgException.getErrorMessages().size());
        for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
            assertEquals(expectedError, errorMessage);
        }
    }

    /**
     * Comparator for sorting collections of ProgramRecognitionCriteriaStatsDTOs.
     */
    public static Comparator<ProgramRecognitionCriteriaStatsDTO> ProgramRecognitionCriteriaStatsDTOComparator = 
            new Comparator<ProgramRecognitionCriteriaStatsDTO>() {
        @Override
        public int compare(ProgramRecognitionCriteriaStatsDTO recognitionCriteriaStatsDTO1,
                           ProgramRecognitionCriteriaStatsDTO recognitionCriteriaStatsDTO2) {

            return recognitionCriteriaStatsDTO2.getRecognitionCriteriaId().intValue() -
                    recognitionCriteriaStatsDTO1.getRecognitionCriteriaId().intValue();
        }
    };

    /**
     * Comparator for sorting collections of ProgramRecognitionCriteriaStatsDTOs.
     */
    public static Comparator<PointLoadBatchFileInfoDTO> PointLoadBatchFileInfoDTOComparator = 
            new Comparator<PointLoadBatchFileInfoDTO>() {
        @Override
        public int compare(PointLoadBatchFileInfoDTO pointLoadBatchFileInfoDTO1,
                           PointLoadBatchFileInfoDTO pointLoadBatchFileInfoDTO2) {
            if(pointLoadBatchFileInfoDTO1 == null || pointLoadBatchFileInfoDTO2 == null) {
                return pointLoadBatchFileInfoDTO2 == null ? 1 : 0;
            }

            String fileName1 = pointLoadBatchFileInfoDTO1.getFileName();
            String fileName2 = pointLoadBatchFileInfoDTO2.getFileName();

            // compare filenames
            if(!StringUtils.equals(fileName1, fileName2)){
                return compareToString(fileName1, fileName2);
            } else {
                // if filenames are equal, then compare statuses
                String status1 = pointLoadBatchFileInfoDTO1.getStatus();
                String status2 = pointLoadBatchFileInfoDTO2.getStatus();
                return compareToString(status1, status2);
            }
        }
    };

    private static int compareToString(String string1, String string2) {
        if(string1 == null && string2 == null) {
            return 0;
        } else if(string1 == null || string2 == null) {
            return string2 == null ? 1 : -1;
        } else {
            return string1.compareTo(string2);
        }
    }

    /**
     * Generates some old date.
     *
     * @return some old date
     */
    public static Date generateOldDate() {
        return new Date(1334448000000L); //  Tue, 15 Apr 2012 00:00:00 GMT
    }

    /**
     * Generates some future date.
     *
     * @return some future date
     */
    public static Date generateFutureDate() {
        long currentTime = new Date().getTime();
        return new Date(currentTime + 1000000000L); //  Current time + ~11.57 days
    }

}