package com.maritz.authentication.services;

import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.security.authentication.login.LoginInfo;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class ProfileIncompleteServiceTest extends AbstractDatabaseTest {

    private static final String PROFILE_INCOMPLETE_EVENT = "PROFILE_INCOMPLETE";
    
    @Inject private SysUserRepository sysUserRepository;
    @Inject private AuthenticationService authenticationService;

    
    @Test
    public void test_login_complete_profile() throws Exception {
        //This user has both an email address and a valid password
        //They should not have the PROFILE_INCOMPLETE event
        
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_HARWELLM);
        
        LoginInfo loginInfo = authenticationService.login();

        assertThat(loginInfo.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(loginInfo.getEvents().contains(PROFILE_INCOMPLETE_EVENT), is(false));
    }

    @Test
    public void test_login_no_email() throws Exception {
        //This user should have the PROFILE_INCOMPLETE event because they don't have an email address
        //Password is not currently checked since the ssoEnabled flag is set to TRUE
        
        authenticationService.authenticate(TestConstants.USER_ADKINSJL);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_ADKINSJL);
        
        LoginInfo loginInfo = authenticationService.login();

        assertThat(loginInfo.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(loginInfo.getEvents().contains(PROFILE_INCOMPLETE_EVENT), is(true));
    }

    @Test
    public void test_profile_incomplete_flag_email() throws Exception {
        //This user should NOT have the PROFILE_INCOMPLETE event because they do have an email address
        //Password is not currently checked since the ssoEnabled flag is set to TRUE
        
        //This user should have the PROFILE_INCOMPLETE event because they don't have an email address
        //Password is not currently checked since the ssoEnabled flag is set to TRUE

        authenticationService.authenticate(TestConstants.USER_ADKINSJL);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_ADKINSJL);

        LoginInfo loginInfo = authenticationService.login();

        assertThat(loginInfo.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(loginInfo.getEvents().contains(PROFILE_INCOMPLETE_EVENT), is(true));
    }
    
    //Password is not currently checked since the ssoEnabled flag is set to TRUE
    @Test 
    public void test_profile_incomplete_flag_no_password_date() throws Exception {
        //This user has an email, but does not have a SYS_USER_PASSWORD_DATE
        //Therefore they should have the PROFILE_INCOMPLETE event
        
        authenticationService.authenticate(TestConstants.USER_ISMAILJ);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_ISMAILJ);
        
        LoginInfo loginInfo = authenticationService.login();

        assertThat(loginInfo.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(loginInfo.getEvents().contains(PROFILE_INCOMPLETE_EVENT), is(true));
    }
    
    //Password is not currently checked since the ssoEnabled flag is set to TRUE
    @Test
    public void test_profile_incomplete_flag_new_user() throws Exception {
        //This user has an email, but is in the NEW user status
        //Therefore they should have have the PROFILE_INCOMPLETE event
    
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_KUMARSJ);
        
        LoginInfo loginInfo = authenticationService.login();

        assertThat(loginInfo.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(loginInfo.getEvents().contains(PROFILE_INCOMPLETE_EVENT), is(true));
    }

}

