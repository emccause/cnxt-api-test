package com.maritz.culturenext.email.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ChangePasswordRestTest extends AbstractRestTest {
    
    @Test
    public void post_password_reset() throws Exception {
        mockMvc.perform(
                post("/rest/users/password-reset")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        "{ "
                            + "\"userName\": \"" + TestConstants.USER_HARWELLM + "\", "
                            + "\"email\": \"" + TestConstants.EMAIL_DEFAULT + "\" "
                        + "} "
                        )
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void post_password_reset_invalid_email_format() throws Exception {
        mockMvc.perform(
                post("/rest/users/password-reset")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        "{ "
                            + "\"userName\": \"" + TestConstants.USER_HARWELLM + "\", "
                            + "\"email\": \"" + TestConstants.EMAIL_DEFAULT_INVALID + "\" "
                        + "} "
                        )
            )
            .andExpect(status().isBadRequest())
        ;
    }
}
