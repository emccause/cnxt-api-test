package com.maritz.culturenext.email.dao;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.jpa.repository.CnxtNominationRepository;
import com.maritz.test.AbstractDatabaseTest;

public class EmailInfoDaoTest extends AbstractDatabaseTest {

    @Inject private EmailInfoDao emailInfoDao;
    @Inject private CnxtNominationRepository cnxtNominationRepository;

    private static Long nominationId = 70786L; //Two recipients, same manager. Created to test emails
    private static Long batchId = 23537L; //Three recipients - all DPP (Cant mix points/cash any more)
    private static final String payoutTypePoints = "DPP";

    @Test
    public void test_get_recipient_recognition_info() throws Exception {

        List<Map<String,Object>> nodes = emailInfoDao.getRecognitionReceivedRecipientEmailInfo(nominationId);

        assertThat(nodes.size(), is(2));

        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            //Make sure all columns are populated with data
            assertNotNull(node.get(ProjectConstants.EMAIL_TYPE));
            assertNotNull(node.get(ProjectConstants.NOMINATION_ID));
            assertNotNull(node.get(ProjectConstants.RECOGNITION_ID));
            assertNotNull(node.get(ProjectConstants.NOTIFICATION_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_EMAIL));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_PAX_ID));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.PROGRAM_NAME));
            assertNotNull(node.get(ProjectConstants.PROGRAM_TYPE_CODE));
            assertNotNull(node.get(ProjectConstants.POINT_AMOUNT));
            assertNotNull(node.get(ProjectConstants.ECARD_ID));
            assertNotNull(node.get(ProjectConstants.COMMENT));
            assertNotNull(node.get(ProjectConstants.HEADLINE));
            assertNotNull(node.get(ProjectConstants.PRIMARY_COLOR));
            assertNotNull(node.get(ProjectConstants.SECONDARY_COLOR));
            assertNotNull(node.get(ProjectConstants.TEXT_ON_PRIMARY_COLOR));
            assertNull(node.get(ProjectConstants.PAYOUT_TYPE));
            //Email Type is correct
            assertEquals((node.get(ProjectConstants.EMAIL_TYPE)), EventType.RECOGNITION_PARTICIPANT.getCode());
        }
    }

    @Test
    public void test_get_recipient_recognition_info_give_anonymous() throws Exception {

        Long nominationId = cnxtNominationRepository.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq("Milestone Email Test")
                .findOne(ProjectConstants.ID, Long.class);

        List<Map<String,Object>> nodes = emailInfoDao.getRecognitionReceivedRecipientEmailInfo(nominationId);

        assertThat(nodes.size(), is(1));

        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            //Make sure all columns are populated with data
            assertNotNull(node.get(ProjectConstants.EMAIL_TYPE));
            assertNotNull(node.get(ProjectConstants.NOMINATION_ID));
            assertNotNull(node.get(ProjectConstants.RECOGNITION_ID));
            assertNotNull(node.get(ProjectConstants.NOTIFICATION_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_EMAIL));
            assertNull(node.get(ProjectConstants.SUBMITTER_PAX_ID));
            assertNull(node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
            assertNull(node.get(ProjectConstants.SUBMITTER_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.PROGRAM_NAME));
            assertNotNull(node.get(ProjectConstants.PROGRAM_TYPE_CODE));
            assertNotNull(node.get(ProjectConstants.POINT_AMOUNT));
            assertNull(node.get(ProjectConstants.ECARD_ID));
            assertNotNull(node.get(ProjectConstants.COMMENT));
            assertNotNull(node.get(ProjectConstants.HEADLINE));
            assertNotNull(node.get(ProjectConstants.PRIMARY_COLOR));
            assertNotNull(node.get(ProjectConstants.SECONDARY_COLOR));
            assertNotNull(node.get(ProjectConstants.TEXT_ON_PRIMARY_COLOR));
            assertNull(node.get(ProjectConstants.PAYOUT_TYPE));
            //Email Type is correct
            assertEquals((node.get(ProjectConstants.EMAIL_TYPE)), EventType.RECOGNITION_PARTICIPANT.getCode());
        }
    }

    @Test
    public void test_get_manager_recognition_info() throws Exception {

        List<Map<String, Object>> nodes = emailInfoDao.getRecognitionReceivedManagerEmailInfo(nominationId);

        assertThat(nodes.size(), is(1));

        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            //Make sure all columns are populated with data
            assertNotNull(node.get(ProjectConstants.EMAIL_TYPE));
            assertNotNull(node.get(ProjectConstants.NOMINATION_ID));
            assertNotNull(node.get(ProjectConstants.NOTIFICATION_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_COUNT));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.POINT_AMOUNT));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_EMAIL));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_FIRST));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_LAST));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_PAX_ID));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.SUBMITTER_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.PROGRAM_NAME));
            assertNotNull(node.get(ProjectConstants.PROGRAM_TYPE_CODE));
            assertNotNull(node.get(ProjectConstants.ECARD_ID));
            assertNotNull(node.get(ProjectConstants.COMMENT));
            assertNotNull(node.get(ProjectConstants.HEADLINE));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_COUNT));
            assertNotNull(node.get(ProjectConstants.PRIMARY_COLOR));
            assertNotNull(node.get(ProjectConstants.SECONDARY_COLOR));
            assertNotNull(node.get(ProjectConstants.TEXT_ON_PRIMARY_COLOR));
            assertNull(node.get(ProjectConstants.PAYOUT_TYPE));
            //Email Type is correct
            assertEquals((node.get(ProjectConstants.EMAIL_TYPE)), EventType.RECOGNITION_MANAGER.getCode());
        }
    }

    @Test
    public void test_get_manager_recognition_info_give_anonymous() throws Exception {

        Long nominationId = cnxtNominationRepository.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq("Milestone Email Test")
                .findOne(ProjectConstants.ID, Long.class);

        List<Map<String, Object>> nodes = emailInfoDao.getRecognitionReceivedManagerEmailInfo(nominationId);

        assertThat(nodes.size(), is(1));

        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            //Make sure all columns are populated with data
            assertNotNull(node.get(ProjectConstants.EMAIL_TYPE));
            assertNotNull(node.get(ProjectConstants.NOMINATION_ID));
            assertNotNull(node.get(ProjectConstants.NOTIFICATION_ID));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_COUNT));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_PAX_ID));
            assertNotNull(node.get(ProjectConstants.POINT_AMOUNT));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_FIRST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_EMAIL));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_FIRST));
            assertNotNull(node.get(ProjectConstants.DIRECT_REPORT_LAST));
            assertNull(node.get(ProjectConstants.SUBMITTER_PAX_ID));
            assertNull(node.get(ProjectConstants.SUBMITTER_NAME_FIRST));
            assertNull(node.get(ProjectConstants.SUBMITTER_NAME_LAST));
            assertNotNull(node.get(ProjectConstants.PROGRAM_NAME));
            assertNotNull(node.get(ProjectConstants.PROGRAM_TYPE_CODE));
            assertNull(node.get(ProjectConstants.ECARD_ID));
            assertNotNull(node.get(ProjectConstants.COMMENT));
            assertNotNull(node.get(ProjectConstants.HEADLINE));
            assertNotNull(node.get(ProjectConstants.RECIPIENT_COUNT));
            assertNotNull(node.get(ProjectConstants.PRIMARY_COLOR));
            assertNotNull(node.get(ProjectConstants.SECONDARY_COLOR));
            assertNotNull(node.get(ProjectConstants.TEXT_ON_PRIMARY_COLOR));
            assertNull(node.get(ProjectConstants.PAYOUT_TYPE));
            //Email Type is correct
            assertEquals((node.get(ProjectConstants.EMAIL_TYPE)), EventType.RECOGNITION_MANAGER.getCode());
        }
    }

    @Test
    public void test_get_email_record_success() throws Exception {

        Map<String, Object> node = emailInfoDao.getEmailRecord(TestConstants.EMAIL_GLYNNKM);
        assertNotNull(node);
        assertEquals((node.get(ProjectConstants.EMAIL_ADDRESS)), TestConstants.EMAIL_GLYNNKM);

    }

    @Test
    public void test_get_email_record_not_found() throws Exception {

        Map<String, Object> node = emailInfoDao.getEmailRecord(TestConstants.EMAIL_HARWELLM);
        assertNull(node);
    }

    @Test
    public void test_get_point_load_email_info() throws Exception {

        List<Map<String, Object>> nodes = emailInfoDao.getPointLoadEmailInfo(batchId);

        assertThat(nodes.size(), is(3));

        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            //Make sure all columns are populated with data
            assertNotNull(node.get(ProjectConstants.PAX_ID));
            assertNotNull(node.get(ProjectConstants.PROGRAM_NAME));
            assertNotNull(node.get(ProjectConstants.AMOUNT));
            assertNotNull(node.get(ProjectConstants.HEADLINE));
            assertNotNull(node.get(ProjectConstants.PAYOUT_TYPE));

            //Program name will be the same for each record
            assertEquals((node.get(ProjectConstants.PROGRAM_NAME)), "Point Upload Validation Testing");
        }

        //Record 1
        assertEquals(nodes.get(0).get(ProjectConstants.PAX_ID), TestConstants.PAX_PORTERGA);
        assertEquals(((Double) nodes.get(0).get(ProjectConstants.AMOUNT)).intValue(), 10);
        assertEquals(nodes.get(0).get(ProjectConstants.HEADLINE), "Greg's point load email test");
        assertEquals(nodes.get(0).get(ProjectConstants.PAYOUT_TYPE), payoutTypePoints);
        //Record 2
        assertEquals(nodes.get(1).get(ProjectConstants.PAX_ID), TestConstants.PAX_HARWELLM);
        assertEquals(((Double) nodes.get(1).get(ProjectConstants.AMOUNT)).intValue(), 25);
        assertEquals(nodes.get(1).get(ProjectConstants.HEADLINE), "Leah's point load email test");
        assertEquals(nodes.get(1).get(ProjectConstants.PAYOUT_TYPE), payoutTypePoints);
        //Record 3
        assertEquals(nodes.get(2).get(ProjectConstants.PAX_ID), TestConstants.PAX_MUDDAM);
        assertEquals(((Double) nodes.get(2).get(ProjectConstants.AMOUNT)).intValue(), 50);
        assertEquals(nodes.get(2).get(ProjectConstants.HEADLINE), "Alex's point load email test");
        assertEquals(nodes.get(2).get(ProjectConstants.PAYOUT_TYPE), payoutTypePoints);
    }

    @Test
    public void releaseNominationEmails() throws Exception {
        //there are no recognitions in the test data set that have batch links and payouts.
        emailInfoDao.releaseNominationEmails(1L);
    }
}
