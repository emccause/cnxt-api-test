package com.maritz.culturenext.email.listener;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.MonthDay;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.program.ecard.services.EcardService;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.core.env.Environment;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationMisc;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeDetailsDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.email.dao.EmailInfoDao;
import com.maritz.culturenext.email.event.EmailEvent;
import com.maritz.culturenext.email.event.PointLoadEmailEvent;
import com.maritz.culturenext.email.event.RecognitionEvent;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.notification.service.NotificationRequest;
import com.maritz.notification.service.NotificationService;
import com.maritz.test.AbstractMockTest;

import freemarker.template.Configuration;

public class EmailListenerUnitTest extends AbstractMockTest {

    @Mock private ConcentrixDao<ApplicationData> applicationDataDao;
    @Mock private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock private EmailInfoDao emailInfoDao;
    @Mock private ConcentrixDao<NominationMisc> nominationMiscDao;
    @Mock private NotificationService notificationService;
    @Mock private Environment environment;
    @Mock private PaxRepository paxRepository;
    @Mock private TranslationUtil translationUtil;
    @Mock private ConcentrixDao<Alert> alertDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private RecognitionService recognitionService;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private AwardTypeService awardTypeService;
    @Mock private BatchService batchService;
    @Mock private EnvironmentUtil environmentUtil;
    @Mock private FileImageService fileImageService;
    @Mock private EcardService eCardService;
    @Mock private GcpUtil gcpUtil;

    @Captor ArgumentCaptor<List<NotificationRequest>> notificationRequestListCaptor;

    @InjectMocks EmailEventListener emailEventListener;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private FindBy<ApplicationData> applicationDataFindBy = mock(FindBy.class);
    private FindBy<Recognition> recognitionFindBy = mock(FindBy.class);
    private FindBy<Recognition> recipientListFindBy = mock(FindBy.class);
    private FindBy<Nomination> nominationFindBy = mock(FindBy.class);
    private FindBy<Alert> alertFindBy = mock(FindBy.class);
    private FindBy<Program> programFindBy = mock(FindBy.class);
    private FindBy<Pax> paxFindBy = mock(FindBy.class);
    private FindBy<Pax> submitterPaxFindBy = mock(FindBy.class);
    private FindBy<Pax> directReportPaxFindBy = mock(FindBy.class);
    private FindBy<Relationship> relationshipFindBy = mock(FindBy.class);
    private FindBy<ApprovalPending> approvalPendingFindBy = mock(FindBy.class);
    private FindBy<NominationMisc> nominationMiscFindBy = mock(FindBy.class);

    private static Long batchId = 23537L; //Three recipients - one CASH. Created to test emails
    private static Long managerPaxId = 5L;
    private static Long milestonePaxId = 6L;
    private static Long yearsOfService = 4L;
    private static String programName = "MILESTONE_TEST_PROGRAM";
    private static Long programId = 10L;
    private static String milestoneDate = "2015-01-01";
    private static String payoutType = "FREE_LUNCH";
    private static Double awardAmount = 20D;
    private static String pointLoadProgram = "Point Upload Validation Testing";
    private static Long testNominationId = 64989L;
    private static Long testRecognitionId = 64990L;
    private static Long testProgramId = 646901L;
    private static Long testReceiverPaxId = 5166L;
    private static final String payoutTypeDpp = "DPP";
    private static final String payoutTypeCash = "CASH";

    @Before
    public void setup() throws Exception {

        //Point Load Email Info
        List<Map<String, Object>> pointLoadInfoMap = new ArrayList<>();
        Map<String, Object> record1 = new HashMap<>();
        record1.put(ProjectConstants.PAX_ID, TestConstants.PAX_PORTERGA);
        record1.put(ProjectConstants.PROGRAM_NAME, pointLoadProgram);
        record1.put(ProjectConstants.AMOUNT, 10.0);
        record1.put(ProjectConstants.HEADLINE, "Greg's point load email test");
        record1.put(ProjectConstants.PAYOUT_TYPE, payoutTypeDpp);
        Map<String, Object> record2 = new HashMap<>();
        record2.put(ProjectConstants.PAX_ID, TestConstants.PAX_HARWELLM);
        record2.put(ProjectConstants.PROGRAM_NAME, pointLoadProgram);
        record2.put(ProjectConstants.AMOUNT, 25.0);
        record2.put(ProjectConstants.HEADLINE, "Leah's point load email test");
        record2.put(ProjectConstants.PAYOUT_TYPE, payoutTypeCash);
        Map<String, Object> record3 = new HashMap<>();
        record3.put(ProjectConstants.PAX_ID, TestConstants.PAX_MUDDAM);
        record3.put(ProjectConstants.PROGRAM_NAME, pointLoadProgram);
        record3.put(ProjectConstants.AMOUNT, 50.0);
        record3.put(ProjectConstants.HEADLINE, "Alex's point load email test");
        record3.put(ProjectConstants.PAYOUT_TYPE, payoutTypeDpp);
        pointLoadInfoMap.add(record1);
        pointLoadInfoMap.add(record2);
        pointLoadInfoMap.add(record3);
        //TODO make this better
        when(emailInfoDao.getPointLoadEmailInfo(batchId)).thenReturn(pointLoadInfoMap);

        //Pax
        Pax pax = new Pax();
        pax.setPaxId(TestConstants.PAX_HARWELLM);
        pax.setFirstName("Leah");
        pax.setLastName("Harwell");
        pax.setPreferredLocale(LocaleCodeEnum.en_US.name());
        when(paxRepository.findOne(anyLong())).thenReturn(pax);

        //Application Data
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR)).thenReturn("#235ba8");
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR)).thenReturn("#ffffff");
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL)).thenReturn("http://q-future-google.cnxtcloud.com");

        List<ApplicationData> applicationDataList = new ArrayList<>();
        ApplicationData header = new ApplicationData();
        header.setApplicationDataId(3L);
        header.setKeyName(ApplicationDataConstants.KEY_NAME_POINT_LOAD_HEADER);
        header.setValue("You've Received Points!");
        ApplicationData buttonText = new ApplicationData();
        buttonText.setApplicationDataId(6L);
        buttonText.setKeyName(ApplicationDataConstants.KEY_NAME_APR_BUTTON);
        buttonText.setValue("Button");
        applicationDataList.add(header);
        applicationDataList.add(buttonText);
        when(applicationDataDao.findBy()).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.where(anyString())).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.in(anyListOf(String.class))).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.find()).thenReturn(applicationDataList);

        //Translations
        Map<Long, String> translations = new HashMap<>();
        translations.put(TestConstants.ID_1, "Translated Email Header!");
        when(translationUtil.getTranslationsForListOfIds(
                anyString(), anyString(), anyString(), anyListOf(Long.class), anyMapOf(Long.class, String.class)))
                .thenReturn(translations);

        //Approval Email Info
        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<>();
        RecognitionDetailsDTO recognitionDto = new RecognitionDetailsDTO();
        recognitionDto.setApprovalPax(new EmployeeDTO().setPaxId(TestConstants.PAX_PORTERGA));
        recognitionDto.setId(286990L); //Just a random recognitionId
        recognitionDetailsList.add(recognitionDto);
        PaginatedResponseObject<RecognitionDetailsDTO> test = new PaginatedResponseObject<>(null, recognitionDetailsList, recognitionDetailsList.size());
        when(recognitionService.getRecognitionDetails(
                any(PaginationRequestDetails.class), anyLong(), anyBoolean(), anyBoolean(), anyLong(), anyString(), anyLong(), anyInt(), anyInt()))
                .thenReturn(test);

        Nomination testNomination = new Nomination();
        testNomination.setProgramId(testProgramId);
        testNomination.setSubmitterPaxId(TestConstants.PAX_KUSEY);
        testNomination.setId(testNominationId);
        testNomination.setEcardId(10101L);
        testNomination.setComment("Hello Friendo");
        testNomination.setHeadlineComment("This is a headline");
        when(nominationDao.findBy()).thenReturn(nominationFindBy);
        when(nominationFindBy.where(ProjectConstants.ID)).thenReturn(nominationFindBy);
        when(nominationFindBy.eq(anyLong())).thenReturn(nominationFindBy);
        when(nominationFindBy.findOne()).thenReturn(testNomination);

        Recognition testRecognition = new Recognition();
        testRecognition.setNominationId(testNominationId);
        testRecognition.setId(testRecognitionId);
        testRecognition.setReceiverPaxId(testReceiverPaxId);
        testRecognition.setAmount(30.00);
        List<Recognition> testRecipientList = new ArrayList<>();
        testRecipientList.add(testRecognition);
        when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        when(recognitionFindBy.where(ProjectConstants.NOMINATION_ID)).thenReturn(recognitionFindBy);
        when(recognitionFindBy.eq(anyLong())).thenReturn(recognitionFindBy);
        when(recognitionFindBy.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(recognitionFindBy);
        when(recognitionFindBy.eq(anyString())).thenReturn(recognitionFindBy);
        when(recognitionFindBy.and(ProjectConstants.PARENT_ID)).thenReturn(recipientListFindBy);
        when(recipientListFindBy.isNull()).thenReturn(recipientListFindBy);
        when(recognitionFindBy.findOne()).thenReturn(testRecognition);
        when(recipientListFindBy.find()).thenReturn(testRecipientList);

        when(alertDao.findBy()).thenReturn(alertFindBy);
        when(alertFindBy.where(ProjectConstants.TARGET_ID)).thenReturn(alertFindBy);
        when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        when(alertFindBy.and(ProjectConstants.ALERT_SUB_TYPE_CODE)).thenReturn(alertFindBy);
        when(alertFindBy.eq(AlertSubType.RECOGNITION_APPROVAL.toString())).thenReturn(alertFindBy);
        when(alertFindBy.and(ProjectConstants.PAX_ID)).thenReturn(alertFindBy);
        when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        when(alertFindBy.findOne()).thenReturn(new Alert());

        Program testProgram = new Program();
        testProgram.setProgramName("Test Program Name");
        testProgram.setProgramTypeCode("ACTIVE");
        when(programDao.findBy()).thenReturn(programFindBy);
        when(programFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programFindBy);
        when(programFindBy.eq(anyLong())).thenReturn(programFindBy);
        when(programFindBy.findOne()).thenReturn(testProgram);

        Pax testReceiverPax = new Pax();
        testReceiverPax.setPaxId(TestConstants.PAX_ADAM_SMITH_ADMIN);
        testReceiverPax.setFirstName("Test Rec First");
        testReceiverPax.setLastName("Test Rec Last");
        testReceiverPax.setPreferredLocale("en_US");
        Pax testSubmitterPax = new Pax();
        testSubmitterPax.setPaxId(TestConstants.PAX_KUSEY);
        testSubmitterPax.setFirstName("Test Rec First");
        testSubmitterPax.setLastName("Test Rec Last");
        Pax testDirectReportPax = new Pax();
        testDirectReportPax.setPaxId(TestConstants.PAX_ADAM_SMITH);
        testDirectReportPax.setFirstName("Test Rec First");
        testDirectReportPax.setLastName("Test Rec Last");
        Pax testApproverPax = new Pax();
        testApproverPax.setPaxId(TestConstants.PAX_PORTERGA);
        testApproverPax.setFirstName("Test Rec First");
        testApproverPax.setLastName("Test Rec Last");
        testApproverPax.setPreferredLocale("en_US");
        when(paxDao.findBy()).thenReturn(paxFindBy);
        when(paxDao.findById(anyLong())).thenReturn(testApproverPax);
        when(paxFindBy.where(ProjectConstants.PAX_ID)).thenReturn(paxFindBy);
        when(paxFindBy.eq(TestConstants.PAX_ADAM_SMITH_ADMIN)).thenReturn(paxFindBy);
        when(paxFindBy.eq(TestConstants.PAX_KUSEY)).thenReturn(submitterPaxFindBy);
        when(paxFindBy.eq(TestConstants.PAX_ADAM_SMITH)).thenReturn(directReportPaxFindBy);
        when(paxFindBy.findOne()).thenReturn(testReceiverPax);
        when(submitterPaxFindBy.findOne()).thenReturn(testSubmitterPax);
        when(directReportPaxFindBy.findOne()).thenReturn(testDirectReportPax);

        List<Long> testApprovalPendingList = new ArrayList<>();
        testApprovalPendingList.add(testRecognitionId);
        when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.where(ProjectConstants.TARGET_ID))
                .thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.and(ProjectConstants.TARGET_TABLE))
        .thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.in(Arrays.asList(anyLong())))
                .thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.and(ProjectConstants.PAX_ID)).thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.eq(anyLong())).thenReturn(approvalPendingFindBy);
        when(approvalPendingFindBy.find(ProjectConstants.TARGET_ID, Long.class))
                .thenReturn(testApprovalPendingList);

        List<Relationship> testRelationshipList = new ArrayList<>();
        Relationship testRelationship = new Relationship();
        testRelationship.setPaxId1(TestConstants.PAX_ADAM_SMITH);
        testRelationshipList.add(testRelationship);
        when(relationshipDao.findBy()).thenReturn(relationshipFindBy);
        when(relationshipFindBy.where(ProjectConstants.RELATIONSHIP_TYPE_CODE))
                .thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(anyString())).thenReturn(relationshipFindBy);
        when(relationshipFindBy.and(ProjectConstants.PAX_ID_2)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(anyLong())).thenReturn(relationshipFindBy);
        when(relationshipFindBy.and(ProjectConstants.PAX_ID_1)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.in(Arrays.asList(anyLong()))).thenReturn(relationshipFindBy);
        when(relationshipFindBy.find()).thenReturn(testRelationshipList);

        NominationMisc testNominationMisc = new NominationMisc();
        testNominationMisc.setVfName(ProjectConstants.PAYOUT_TYPE);
        testNominationMisc.setMiscData(payoutTypeDpp);
        when(nominationMiscDao.findBy()).thenReturn(nominationMiscFindBy);
        when(nominationMiscFindBy.where(ProjectConstants.VF_NAME)).thenReturn(nominationMiscFindBy);
        when(nominationMiscFindBy.eq(ProjectConstants.PAYOUT_TYPE)).thenReturn(nominationMiscFindBy);
        when(nominationMiscFindBy.and(ProjectConstants.NOMINATION_ID)).thenReturn(nominationMiscFindBy);
        when(nominationMiscFindBy.eq(anyLong())).thenReturn(nominationMiscFindBy);
        when(nominationMiscFindBy.findOne()).thenReturn(testNominationMisc);

        AwardTypeDTO awardType = new AwardTypeDTO();
        AwardTypeDetailsDTO awardTypeDetails = new AwardTypeDetailsDTO();
        awardTypeDetails.setPayoutType(payoutTypeDpp);
        awardType.setDetails(awardTypeDetails);
        awardType.setDisplayName("Points");
        awardType.setLongDesc("Long points description");
        awardType.setAbs(true);
        when(awardTypeService.getAwardTypeByPayoutType(anyString())).thenReturn(awardType);

        //Notify Others Email Info
        List<Map<String, Object>> notifyOtherInfoMap = new ArrayList<>();
        Map<String, Object> notifyOther = new HashMap<>();
        notifyOther.put(ProjectConstants.PAX_ID, TestConstants.PAX_PORTERGA);
        notifyOther.put(ProjectConstants.RECIPIENT_LOCALE_CODE, LocaleCodeEnum.en_US.name());
        notifyOther.put(ProjectConstants.PAYOUT_TYPE, payoutTypeDpp);
        notifyOther.put(ProjectConstants.PROGRAM_NAME, "Leah Unit Test");
        notifyOther.put(ProjectConstants.HEADLINE, "This is a unit test email for notify others");
        notifyOther.put(ProjectConstants.NOTIFICATION_ID, TestConstants.ID_1);
        notifyOther.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.PAX_PORTERGA);
        notifyOther.put(ProjectConstants.SUBMITTER_NAME_FIRST, "Greg");
        notifyOther.put(ProjectConstants.SUBMITTER_NAME_LAST, "Porter");
        notifyOther.put(ProjectConstants.ECARD_ID, "10131");
        notifyOther.put(ProjectConstants.RECIPIENT_NAME_FIRST, "Alex");
        notifyOther.put(ProjectConstants.RECIPIENT_NAME_LAST, "Mudd");
        notifyOther.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_2);
        notifyOther.put(ProjectConstants.POINT_AMOUNT, 10.00);
        notifyOther.put(ProjectConstants.RECIPIENT_COUNT, 1);
        notifyOther.put(ProjectConstants.DIRECT_REPORT_COUNT, 0);
        notifyOtherInfoMap.add(notifyOther);
        when(emailInfoDao.getNotifyOtherEmailInfo(TestConstants.ID_1)).thenReturn(notifyOtherInfoMap);
    }

    @Test
    public void test_email_point_upload_event() throws Exception {

        emailEventListener.handlePointLoadEmailEvent(new PointLoadEmailEvent(EventType.POINT_LOAD.getCode(), batchId));

        verify(emailInfoDao, times(1)).getPointLoadEmailInfo(anyLong());
        verify(paxRepository, times(3)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(9)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(3));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.POINT_LOAD.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), lessThanOrEqualTo(System.currentTimeMillis()));
    }

    @Test
    public void test_email_approval_event() throws Exception {

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.APPROVAL.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), lessThanOrEqualTo(System.currentTimeMillis()));
    }

    @Test
    public void approvalRecognition_cdnImageUrls() throws Exception {
        when(environmentUtil.readFromCdn()).thenReturn(true);
        when(environmentUtil.getLongProperty("url.signing.duration.email", 3153600000L)).thenReturn(3153600000L);

        Files logoFile = new Files().setUrl("logo.jpg");
        when(fileImageService.searchForImage(FileImageTypes.PROJECT_LOGO)).thenReturn(logoFile);
        when(gcpUtil.signUrl(logoFile, 3153600000L)).thenReturn("signedLogo");

        Files eCardFile = new Files().setUrl("ecard.jpg");
        when(eCardService.getECardImage(10101)).thenReturn(eCardFile);
        when(gcpUtil.signUrl(eCardFile, 3153600000L)).thenReturn("signedECard");

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        // Image URLs
        verify(environmentUtil, times(2)).readFromCdn();
        verify(environmentUtil, times(2)).getLongProperty("url.signing.duration.email", 3153600000L);

        verify(fileImageService, times(1)).searchForImage(FileImageTypes.PROJECT_LOGO);
        verify(gcpUtil, times(1)).signUrl(logoFile, 3153600000L);

        verify(eCardService, times(1)).getECardImage(10101);
        verify(gcpUtil, times(1)).signUrl(eCardFile, 3153600000L);


        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.APPROVAL.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), lessThanOrEqualTo(System.currentTimeMillis()));

        // Image URLs
        assertThat(notificationRequest.getVariable("clientLogo"), is("signedLogo"));
        assertThat(notificationRequest.getVariable("ecardUrl"), is("signedECard"));
    }

    @Test
    public void test_email_approval_reminder_event() throws Exception {

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL_REMINDER.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.APPROVAL_REMINDER.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), lessThanOrEqualTo(System.currentTimeMillis()));
    }

    @Test
    public void test_email_recognition_participant_event_with_points() throws Exception {

        Map<String, Object> node = getDefaultMap(15);
        when(emailInfoDao.getRecognitionReceivedRecipientEmailInfo(testNominationId)).thenReturn(Arrays.asList(node));

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.RECOGNITION_PARTICIPANT.getCode(), testNominationId));

        verify(emailInfoDao, times(1)).getRecognitionReceivedRecipientEmailInfo(testNominationId);
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.RECOGNITION_PARTICIPANT.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), is(DateUtils.parseDate("12/31/9999").getTime()));
    }

    @Test
    public void test_email_recognition_participant_event_without_points() throws Exception {

        Map<String, Object> node = getDefaultMap(0);
        node.remove("pointAmount");
        when(emailInfoDao.getRecognitionReceivedRecipientEmailInfo(testNominationId)).thenReturn(Arrays.asList(node));

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.RECOGNITION_PARTICIPANT.getCode(), testNominationId));

        verify(emailInfoDao, times(1)).getRecognitionReceivedRecipientEmailInfo(testNominationId);
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.RECOGNITION_PARTICIPANT.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), lessThanOrEqualTo(System.currentTimeMillis()));
    }

    @Test
    public void test_email_recognition_manager_event() throws Exception {
        Map<String, Object> node = getDefaultMap(15);
        when(emailInfoDao.getRecognitionReceivedManagerEmailInfo(testNominationId)).thenReturn(Arrays.asList(node));

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.RECOGNITION_MANAGER.getCode(), testNominationId));

        verify(emailInfoDao, times(1)).getRecognitionReceivedManagerEmailInfo(anyLong());
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.RECOGNITION_MANAGER.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), is(DateUtils.parseDate("12/31/9999").getTime()));
    }

    @Test
    public void test_email_change_password_event() throws Exception {
        emailEventListener.handleSendEmailEvent(new EmailEvent(EventType.CHANGE_PASSWORD.getCode(), TestConstants.PAX_HARWELLM));

        verify(paxRepository, times(1)).findOne(anyLong());
        verify(environment, times(3)).getProperty(anyString());
        verify(notificationService, times(1)).sendNotification(any(NotificationRequest.class));
    }

    @Test
    public void test_email_reset_password_event() throws Exception {
        emailEventListener.handleSendEmailEvent(new EmailEvent(EventType.RESET_PASSWORD.getCode(), TestConstants.PAX_HARWELLM));

        verify(paxRepository, times(1)).findOne(anyLong());
        verify(environment, times(3)).getProperty(anyString());
        verify(notificationService, times(1)).sendNotification(any(NotificationRequest.class));
    }

    @Test
    public void test_email_notify_others_event() throws Exception {
        Map<String, Object> node = getDefaultMap(15);
        when(emailInfoDao.getNotifyOtherEmailInfo(testNominationId)).thenReturn(Arrays.asList(node));

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.NOTIFY_OTHER.getCode(), TestConstants.ID_1));

        verify(emailInfoDao, times(1)).getNotifyOtherEmailInfo(anyLong());
        verify(paxRepository, times(1)).findOne(anyLong());
        verify(environment, times(3)).getProperty(anyString());
        verify(notificationService, times(1)).sendNotification(notificationRequestListCaptor.capture());

        List<NotificationRequest> notificationRequests = notificationRequestListCaptor.getValue();
        assertThat(notificationRequests.size(), is(1));

        NotificationRequest notificationRequest = notificationRequests.get(0);
        assertThat(notificationRequest.getEventType(), is(EventType.NOTIFY_OTHER.getCode()));
        assertThat(notificationRequest.getSendDate().getTime(), is(DateUtils.parseDate("12/31/9999").getTime()));
    }

    @Test
    public void test_email_milestone_reminder_event() throws Exception {
        Batch batch = new Batch();
        batch.setBatchId(5L);

        when(batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_REMINDER_EMAIL.name())).thenReturn(batch);
        when(batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_REMINDER_EMAIL.name())).thenReturn(batch);

        List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList = new ArrayList<>();
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name(), 15L, MonthDay.now(), yearsOfService, programId, Year.of(2018), programName, 15L, "2015-01-01", awardAmount, payoutType);
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);

        emailEventListener.handleMilestoneReminderEvent(new MilestoneReminderEvent(paxMilestoneReminderDTOList, EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode()));

        verify(paxRepository, times(2)).findOne(anyLong());
        verify(environment, times(3)).getProperty(anyString());
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }


    @Test
    public void test_writes_batch_summary_data() {
        ArgumentCaptor<BatchEventDTO> batchEventDTOCaptor = ArgumentCaptor.forClass(BatchEventDTO.class);

        Batch batch = new Batch();
        batch.setBatchId(5L);

        when(batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_REMINDER_EMAIL.name())).thenReturn(batch);
        when(batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_REMINDER_EMAIL.name())).thenReturn(batch);

        List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList = new ArrayList<>();
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = createPaxMilestoneReminder(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name(), 20L, MonthDay.now(), 15L, 50L, Year.of(2015), "Natalie's Program", 15L, "2015-06-17", 20.0, "DPP");
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);

        Map<String, Object> programData = new HashMap<>();
        programData.put(ProjectConstants.PAX_ID, 20L);
        programData.put(ProjectConstants.PROGRAM_NAME, "Natalie's Program");
        programData.put(ProjectConstants.MILESTONE_DATE, "2015-06-17");
        programData.put(ProjectConstants.PAYOUT_TYPE, "DPP");
        programData.put(ProjectConstants.POINT_AMOUNT, 20.0);

        MilestoneReminderEvent milestoneReminderEmailEvent = new MilestoneReminderEvent(paxMilestoneReminderDTOList, EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode());

        when(emailEventListener.setupEmail(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.getCode(), 20L, programData)).thenReturn(any(NotificationRequest.class));

        emailEventListener.handleMilestoneReminderEvent(milestoneReminderEmailEvent);

        verify(batchService, times(1)).endBatch(batch);
        verify(batchService, times(3)).createBatchEvent(eq(batch), batchEventDTOCaptor.capture());
        List<BatchEventDTO> batchEvents = batchEventDTOCaptor.getAllValues();

        BatchEventDTO batchEvent = batchEvents.get(0);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERR.name(), "PAX", 20L, null);

        batchEvent = batchEvents.get(1);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.RECS.name(), null, null, "1");

        batchEvent = batchEvents.get(2);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERRS.name(), null, null, "1");
    }

    @Test
    public void test_french_as_primary_language_header_approval_email() {

        Map<String, Object> node = getDefaultMap(15);

        node.put(ProjectConstants.RECIPIENT_LOCALE_CODE, LocaleCodeEnum.fr_CA);

        List<ApplicationData> applicationDataList = new ArrayList<>();
        ApplicationData andText = new ApplicationData();
        andText.setApplicationDataId(2L);
        andText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_AND);
        andText.setValue("et");
        ApplicationData otherText = new ApplicationData();
        otherText.setApplicationDataId(3L);
        otherText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHER);
        otherText.setValue("autre");
        ApplicationData othersText = new ApplicationData();
        othersText.setApplicationDataId(4L);
        othersText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHERS);
        othersText.setValue("autres");
        ApplicationData directReportText = new ApplicationData();
        directReportText.setApplicationDataId(5L);
        directReportText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS);
        directReportText.setValue("de vos rapports directs");
        applicationDataList.add(andText);
        applicationDataList.add(otherText);
        applicationDataList.add(othersText);
        applicationDataList.add(directReportText);
        when(applicationDataDao.findBy()).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.where(anyString())).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.in(anyListOf(String.class))).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.find()).thenReturn(applicationDataList);

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.APPROVAL.getCode()), Matchers.is("Adam Smith"));
    }

    @Test
    public void test_default_maps_recognition_email() {

        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.EMAIL_TYPE, EventType.RECOGNITION_MANAGER.getCode());

        mockTranslationApplicationDataList();

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.RECOGNITION_MANAGER.getCode()), Matchers.is("Adam Smith_Admin"));
    }

    @Test
    public void test_zero_direct_reports_approval_email() {

        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.RECIPIENT_COUNT, 2);
        node.put(ProjectConstants.DIRECT_REPORT_COUNT, 0);

        mockTranslationApplicationDataList();

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.APPROVAL.getCode()), Matchers.is("Adam Smith and 1 other"));
    }

    @Test
    public void test_equal_counts_approval_email() {

        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.RECIPIENT_COUNT, 2);
        node.put(ProjectConstants.DIRECT_REPORT_COUNT, 2);

        mockTranslationApplicationDataList();

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.APPROVAL.getCode()), Matchers.is("2 of your direct reports"));
    }

    @Test
    public void test_one_direct_multiple_recipient_approval_email() {

        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.RECIPIENT_COUNT, 3);
        node.put(ProjectConstants.DIRECT_REPORT_COUNT, 1);

        mockTranslationApplicationDataList();

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.APPROVAL.getCode()), Matchers.is("Adam Smith_Admin and 2 others"));
    }

    @Test
    public void test_two_direct_multiple_recipient_approval_email() {

        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.RECIPIENT_COUNT, 3);
        node.put(ProjectConstants.DIRECT_REPORT_COUNT, 2);

        mockTranslationApplicationDataList();

        assertThat("Manager text does not match expected",
                emailEventListener.getManagerText(node, EventType.APPROVAL.getCode()), Matchers.is("2 of your direct reports and 1 other"));
    }

    @Test
    public void test_alert_id_not_null() throws Exception {

        Alert alert = new Alert();
        alert.setId(23L);

        when(alertFindBy.findOne()).thenReturn(alert);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_alert_null() throws Exception {

        when(alertFindBy.findOne()).thenReturn(null);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_nomination_misc_null() throws Exception {

        when(nominationMiscFindBy.findOne()).thenReturn(null);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_approval_pending_list_empty() {

        when(approvalPendingFindBy.find(ProjectConstants.TARGET_ID, Long.class))
            .thenReturn(new ArrayList<Long>());

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_approval_pending_list_not_empty_not_contain_value() {

        List<Long> approvalPendingList = new ArrayList<>();
        approvalPendingList.add(999999999999L);

        when(approvalPendingFindBy.find(ProjectConstants.TARGET_ID, Long.class))
            .thenReturn(approvalPendingList);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_relationship_list_empty() {

        when(relationshipFindBy.find()).thenReturn(new ArrayList<Relationship>());

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_no_nomination_ecard_id() {

        Nomination testNomination = new Nomination();
        testNomination.setProgramId(testProgramId);
        testNomination.setSubmitterPaxId(TestConstants.PAX_KUSEY);
        testNomination.setId(testNominationId);
        testNomination.setComment("Hello Friendo");
        testNomination.setHeadlineComment("This is a headline");

        when(nominationFindBy.findOne()).thenReturn(testNomination);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.APPROVAL.getCode(), testNominationId));

        verify(paxRepository, times(1)).findOne(anyLong()); //Once for every email sent
        verify(environment, times(4)).getProperty(anyString()); //Three times for every email sent
        verify(notificationService, times(1)).sendNotification(anyListOf(NotificationRequest.class));
    }

    @Test
    public void test_bad_template_name() throws Exception {

        Configuration mockConfig = mock(Configuration.class);

        when(mockConfig.getTemplate(anyString())).thenThrow(new IOException());
    }

    @Test
    public void test_ecard_not_null_manager() throws Exception {

        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.ECARD_ID, 10128L);
        node.put(ProjectConstants.HEADLINE, "A headline");
        node.put(ProjectConstants.PAYOUT_TYPE, payoutTypeDpp);
        nodes.add(node);

        when(emailInfoDao.getRecognitionReceivedManagerEmailInfo(anyLong())).thenReturn(nodes);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.RECOGNITION_MANAGER.getCode(), TestConstants.ID_1));
    }

    @Test
    public void test_notification_id_and_ecard_not_null_participant() throws Exception {

        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = getDefaultMap(15);
        node.put(ProjectConstants.ECARD_ID, 10128L);
        node.put(ProjectConstants.HEADLINE, "A headline");
        node.put(ProjectConstants.PAYOUT_TYPE, payoutTypeDpp);
        node.put(ProjectConstants.NOTIFICATION_ID, 23L);
        nodes.add(node);

        when(emailInfoDao.getRecognitionReceivedRecipientEmailInfo(anyLong())).thenReturn(nodes);

        emailEventListener.handleRecognitionEvent(new RecognitionEvent(EventType.RECOGNITION_PARTICIPANT.getCode(), TestConstants.ID_1));
    }

    //Helper Methods

    private Map<String, Object> getDefaultMap(int points) {

        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.EMAIL_TYPE, EventType.APPROVAL.getCode());
        node.put(ProjectConstants.RECIPIENT_PAX_ID, TestConstants.ID_2);
        node.put(ProjectConstants.RECIPIENT_NAME_FIRST, "Adam");
        node.put(ProjectConstants.RECIPIENT_NAME_LAST, "Smith");
        node.put(ProjectConstants.RECIPIENT_LOCALE_CODE, LocaleCodeEnum.en_US);
        node.put(ProjectConstants.RECIPIENT_COUNT, 1);
        node.put(ProjectConstants.DIRECT_REPORT_COUNT, 1);
        node.put(ProjectConstants.DIRECT_REPORT_FIRST, "Adam");
        node.put(ProjectConstants.DIRECT_REPORT_LAST, "Smith_Admin");
        node.put(ProjectConstants.POINT_AMOUNT, points);
        node.put(ProjectConstants.PROGRAM_NAME, "Testing");
        node.put(ProjectConstants.COMMENT, "Private comment");
        node.put(ProjectConstants.SUBMITTER_PAX_ID, 3L);
        node.put(ProjectConstants.SUBMITTER_NAME_FIRST, "Leah");
        node.put(ProjectConstants.SUBMITTER_NAME_LAST, "Harwell");

        return node;
    }

    private void mockTranslationApplicationDataList() {

        List<ApplicationData> applicationDataList = new ArrayList<>();
        ApplicationData andText = new ApplicationData();
        andText.setApplicationDataId(2L);
        andText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_AND);
        andText.setValue("and");
        ApplicationData otherText = new ApplicationData();
        otherText.setApplicationDataId(3L);
        otherText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHER);
        otherText.setValue("other");
        ApplicationData othersText = new ApplicationData();
        othersText.setApplicationDataId(4L);
        othersText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_OTHERS);
        othersText.setValue("others");
        ApplicationData directReportText = new ApplicationData();
        directReportText.setApplicationDataId(5L);
        directReportText.setKeyName(ApplicationDataConstants.KEY_NAME_DYNAMIC_STRING_DIRECT_REPORTS);
        directReportText.setValue("of your direct reports");
        applicationDataList.add(andText);
        applicationDataList.add(otherText);
        applicationDataList.add(othersText);
        applicationDataList.add(directReportText);
        when(applicationDataDao.findBy()).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.where(anyString())).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.in(anyListOf(String.class))).thenReturn(applicationDataFindBy);
        when(applicationDataFindBy.find()).thenReturn(applicationDataList);
    }

    private void assertBatchEvent(BatchEventDTO batchEvent, Long batchId, String batchType, String targetTable, Long targetId, String message) {
        assertThat(batchEvent.getBatchId(), is(batchId));
        assertThat(batchEvent.getType(), is(batchType));
        assertThat(batchEvent.getTarget().getTable(), is(targetTable));
        assertThat(batchEvent.getTarget().getId(), is(targetId));
        assertThat(batchEvent.getMessage(), is(message));
    }

    private PaxMilestoneReminderDTO createPaxMilestoneReminder(String type, Long paxId, MonthDay monthDay, Long programActivityId, Long programId, Year hireYear, String programName, Long managerPaxId, String milestoneDate, Double awardAmount, String payoutType) {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(type, paxId, monthDay, programActivityId, programId, hireYear, programName, managerPaxId, milestoneDate, awardAmount, payoutType);
        return paxMilestoneReminderDTO;
    }

    @Test
    public void toId_null() {
        assertThat(emailEventListener.toId(null), nullValue());
    }

    @Test
    public void toId_long() {
        assertThat(emailEventListener.toId(101L), is(101L));
    }

    @Test
    public void toId_string() {
        assertThat(emailEventListener.toId("101"), is(101L));
    }

    @Test
    public void toId_invalidValue() {
        IllegalArgumentException illegalArgumentException = null;
        try {
            emailEventListener.toId(Boolean.FALSE);
        } catch (IllegalArgumentException e) {
            illegalArgumentException = e;
        }
        assertThat(illegalArgumentException, notNullValue());
        assertThat(illegalArgumentException.getMessage(), is("Unexpected class of ID encountered: java.lang.Boolean; false"));
    }
}
