package com.maritz.culturenext.email.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class EmailListenerHeavyQueryUnitTest extends AbstractDatabaseTest{
    private static final int CARDINALITY_RECORDS_IN_DB = 4423;
    private static final long NOMINATION_WITH_3K_RECORDS = 59252L;
    
    @Inject private EmailEventListener emailEventListener;
    @Inject private ConcentrixDao<Recognition> recognitionDao;

    @Test
    public void getApprovalPendingListTest () {
        List<Long> approvalPendingList = null;
        Long nominationId = NOMINATION_WITH_3K_RECORDS;
        assertNotNull("recognitionDAO was null and should not!", recognitionDao);
        List<Recognition> recognitionsList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .and(ProjectConstants.PARENT_ID).isNull()
                .find();
        assertNotNull ("recipientsList was null and should not!", recognitionsList);
        assertEquals("Records found were less than expected",CARDINALITY_RECORDS_IN_DB,recognitionsList.size());
        List<Long> recipientIds = new ArrayList<Long>();
        List<Long> recognitionIds = new ArrayList<Long>();
        Integer pointAmount = 0;
        Long approverPaxId = TestConstants.PAX_PORTERGA;
        int recipientCount = recognitionsList.size();
        Map<String, Object> approvalEmailMap = new HashMap<String, Object>();
        approvalEmailMap.put(ProjectConstants.RECIPIENT_COUNT, recipientCount);
        
        for (Recognition recg : recognitionsList) {
            recipientIds.add(recg.getReceiverPaxId());
            recognitionIds.add(recg.getId());
        }
        assertNotNull("RecipientsIds was null and should not!",recipientIds);
        assertNotNull("RecognitionIds was null and should not!", recognitionIds);
        
        this.emailEventListener.populateApprovalPendingList(recognitionIds,
                recognitionsList, approverPaxId, approvalPendingList, pointAmount);
        assertNull("Approval Pending List was null and should not", approvalPendingList);
    }

    @Test
    public void getRelationshipListTest () {
        Long nominationId = NOMINATION_WITH_3K_RECORDS;
        Long approverPaxId = TestConstants.PAX_PORTERGA;
        assertNotNull("recognitionDAO was null and should not!", recognitionDao);
        List<Recognition> recognitionsList = recognitionDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationId)
                .and(ProjectConstants.PARENT_ID).isNull()
                .find();
        assertNotNull ("recipientsList was null and should not!", recognitionsList);
        List<Long> recipientIds = new ArrayList<Long>();
        
        for (Recognition recg : recognitionsList) {
            recipientIds.add(recg.getReceiverPaxId());
        }
        assertNotNull("RecipientsIds was null and should not!",recipientIds);
        
        List<Relationship> relationshipList = this.emailEventListener.getRelationshipList(recipientIds, approverPaxId);
        assertNotNull("Relationship list was null and should not", relationshipList);
    }

}
