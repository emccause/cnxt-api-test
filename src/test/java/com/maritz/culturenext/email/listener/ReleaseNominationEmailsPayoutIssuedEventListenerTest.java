package com.maritz.culturenext.email.listener;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.maritz.core.event.PayoutIssuedEvent;
import com.maritz.core.jpa.entity.Payout;
import com.maritz.culturenext.email.dao.EmailInfoDao;
import com.maritz.test.AbstractMockTest;

public class ReleaseNominationEmailsPayoutIssuedEventListenerTest extends AbstractMockTest {

    @Mock EmailInfoDao emailInfoDao;

    ReleaseNominationEmailsPayoutIssuedEventListener listener;

    @Before
    public void setUp() throws Exception {
        listener = new ReleaseNominationEmailsPayoutIssuedEventListener(emailInfoDao);
    }

    @Test
    public void handlePayoutIssued() {
        PayoutIssuedEvent event = new PayoutIssuedEvent(new Payout().setPayoutId(1L));

        listener.handlePayoutIssued(event);

        verify(emailInfoDao, times(1)).releaseNominationEmails(1L);
    }

}
