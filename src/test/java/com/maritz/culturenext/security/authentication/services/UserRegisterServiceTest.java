package com.maritz.culturenext.security.authentication.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;

import javax.inject.Named;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.cache.Cache;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Email;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.security.authentication.dto.UserRegisterRequestDTO;
import com.maritz.culturenext.security.authentication.dto.UserRegisterTokenResponseDTO;
import com.maritz.culturenext.security.authentication.services.impl.UserRegisterServiceImpl;
import com.maritz.culturenext.security.user.status.constants.UserStatusConstants;
import com.maritz.profile.dto.PasswordResetDTO;
import com.maritz.profile.services.PasswordService;
import com.maritz.test.AbstractMockTest;

public class UserRegisterServiceTest extends AbstractMockTest {

    @InjectMocks UserRegisterServiceImpl userRegisterService;
    @Mock @Named("passwordResetAttemptsCache") private Cache passwordResetAttemptsCache;
    @Mock @Named("resetPasswordTokenService") private TokenService resetPasswordTokenService;
    @Mock private ConcentrixDao<SysUser> sysUserDao;
    @Mock private EmailRepository emailRepository;
    @Mock private PasswordEncoder passwordEncoder;
    @Mock private PasswordService passwordService;
    @Mock private PaxRepository paxRepository;
    @Mock private PaxMiscRepository paxMiscRepository;
    @Mock private SysUserRepository sysUserRepository;

    private static final String BIRTH_DATE_ENCRYPTED_MISC_TEST = "$2a$10$RHReD15pPDe9P4RlKZjph.fff63pp/vJoU3IBiKGW1oO1k8SVahu.";

    private static final String RESET_PASSWORD_TOKEN_TEST = "test.eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlcHBsaW5qbCIsImV4cCI6"
            + "MTQ4NDA5MTAxNCwiaWF0IjoxNDg0MDA0NjE0LCJpc3MiOiJtYXJpdHoifQ.KcO5RbqYCgJD8ZR2UPZQtAI96hadGD-prb6DTFdxtlo";

    private static final String SYS_USER_NAME_TEST = "test.username";
    private static final String EMPLOYEE_ID_TEST = "123456";
    private static final String BIRTH_DATE_TEST = "2000-01-01";
    private static final Integer MAX_BAD_ATTEMPTS = 5;

    private UserRegisterRequestDTO userRegisterRequest;

    FindBy<SysUser> sysUserFindBy = PowerMockito.mock(FindBy.class);

    @Rule public ExpectedException thrown= ExpectedException.none();

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        userRegisterRequest = new UserRegisterRequestDTO();

        Pax paxMock = new Pax();
        paxMock.setPaxId(TestConstants.ID_1);
        PowerMockito.when(paxRepository.findByControlNum(anyString())).thenReturn(paxMock);
        PowerMockito.when(paxRepository.findPaxIdByControlNum(anyString())).thenReturn(paxMock.getPaxId());

        SysUser sysUserMock = new SysUser();
        sysUserMock.setSysUserName(SYS_USER_NAME_TEST);
        sysUserMock.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        PowerMockito.when(sysUserRepository.findOneByPaxId(anyLong())).thenReturn(sysUserMock);

        List<SysUser> suList = Arrays.asList(sysUserMock);
        PowerMockito.when( sysUserRepository.findByPaxId(anyLong())).thenReturn(suList);

        PowerMockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.where(anyString())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(anyLong())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.findOne()).thenReturn(sysUserMock);

        PowerMockito.when(emailRepository.findPreferredByPaxId(anyLong())).thenReturn(null);

        PaxMisc paxMiscMock = new PaxMisc();
        paxMiscMock.setVfName(VfName.FULL_BIRTH_DATE.name());
        paxMiscMock.setMiscData(BIRTH_DATE_ENCRYPTED_MISC_TEST);
        PowerMockito.when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(paxMiscMock);

        PowerMockito.when(passwordResetAttemptsCache.get(anyString(), any(Class.class))).thenReturn(null);

        PowerMockito.when(passwordEncoder.matches(anyString(),anyString())).thenReturn(Boolean.TRUE);

        PowerMockito.when(passwordService.createResetPasswordToken(any(PasswordResetDTO.class))).thenReturn(RESET_PASSWORD_TOKEN_TEST);

        ReflectionTestUtils.setField(userRegisterService, "resetAttempts", MAX_BAD_ATTEMPTS);

    }

    @Test
    public final void test_register_success() {
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        UserRegisterTokenResponseDTO response = userRegisterService.register(userRegisterRequest);

        // Verify result
        Mockito.verify(passwordService, times(1)).createResetPasswordToken(any(PasswordResetDTO.class));

        assertNotNull(response);
        assertEquals(SYS_USER_NAME_TEST, response.getUserName());
        assertEquals(RESET_PASSWORD_TOKEN_TEST, response.getRegisterToken());
    }

    @Test
    public final void test_register_success_different_format() {
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate("01/01/2000");

        UserRegisterTokenResponseDTO response = userRegisterService.register(userRegisterRequest);

        // Verify result
        Mockito.verify(passwordService, times(1)).createResetPasswordToken(any(PasswordResetDTO.class));

        assertNotNull(response);
        assertEquals(SYS_USER_NAME_TEST, response.getUserName());
        assertEquals(RESET_PASSWORD_TOKEN_TEST, response.getRegisterToken());
    }

    @Test
    public final void test_register_fail_empty_fields() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_EMPLOYEE_ID_REQUIRED.getCode()));

        userRegisterRequest.setEmployeeId(null);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_BIRTH_DATE_REQUIRED.getCode()));

        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(null);

        userRegisterService.register(userRegisterRequest);
    }

    @Test
    public final void test_register_fail_participant_not_exist() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED.getCode()));

        // Mock participant not exists.
        PowerMockito.when(paxRepository.findByControlNum(anyString())).thenReturn(null);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

    }

    @Test
    public final void test_register_fail_participant_account_inactive() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED.getCode()));

        // Mock participant account inactive.
        SysUser sysUserMock = new SysUser();
        sysUserMock.setStatusTypeCode(SYS_USER_NAME_TEST);
        sysUserMock.setStatusTypeCode(StatusTypeCode.INACTIVE.name());

        PowerMockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.where(anyString())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(anyLong())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.findOne()).thenReturn(sysUserMock);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

    }

    @Test
    public final void test_register_fail_email_registered() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_EMAIL_REGISTERED.getCode()));

        // Mock existing email
        Email emailMock = new Email();
        emailMock.setPaxId(TestConstants.ID_1);
        PowerMockito.when(emailRepository.findPreferredByPaxId(anyLong())).thenReturn(emailMock);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

    }

    @Test
    public final void test_register_fail_birthdate_not_configured() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_NOT_ALLOWED.getCode()));

        // Mock no birthdate registered
        PowerMockito.when(paxMiscRepository.findOne(TestConstants.ID_1 , VfName.FULL_BIRTH_DATE.name())).thenReturn(null);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

    }

    @Test
    public final void test_register_fail_too_many_bad_attemtpts() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_TOO_MANY_BAD_ATTEMPTS.getCode()));

        // Mock too many attempts
        PowerMockito.when(passwordResetAttemptsCache.get(EMPLOYEE_ID_TEST, Integer.class)).thenReturn(MAX_BAD_ATTEMPTS);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

    }

    @Test
    public final void test_register_fail_first_bad_attemtpts() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_BAD_ATTEMPT.getCode()));

        // Mock bad attempt
        PowerMockito.when(passwordEncoder.matches(anyString(),anyString())).thenReturn(Boolean.FALSE);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

        // Verify result
        Mockito.verify(passwordResetAttemptsCache, times(1)).put(EMPLOYEE_ID_TEST,1);

    }

    @Test
    public final void test_register_fail_second_bad_attemtpts() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(UserStatusConstants.ERROR_USER_REGISTER_BAD_ATTEMPT.getCode()));

        // Mock bad attempt
        PowerMockito.when(passwordResetAttemptsCache.get(EMPLOYEE_ID_TEST, Integer.class)).thenReturn(1);
        PowerMockito.when(passwordEncoder.matches(anyString(),anyString())).thenReturn(Boolean.FALSE);

        // Request body.
        userRegisterRequest.setEmployeeId(EMPLOYEE_ID_TEST);
        userRegisterRequest.setBirthDate(BIRTH_DATE_TEST);

        userRegisterService.register(userRegisterRequest);

        // Verify result
        Mockito.verify(passwordResetAttemptsCache, times(1)).put(EMPLOYEE_ID_TEST,2);

    }



}
