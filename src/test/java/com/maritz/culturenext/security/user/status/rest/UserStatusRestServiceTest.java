package com.maritz.culturenext.security.user.status.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class UserStatusRestServiceTest extends AbstractRestTest{

    public static final Long ID_INVALID = 9999L;
    public static final Long ID_UNLOCKED = 5211L;
    public static final Long ID_LOCKED = 11186L;
    public static final Long ID = 9328L;    
    
    @Test
    public void unlock_user_bad_sysUser_id() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID_INVALID +"/unlock")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
            )
            .andExpect(status().isBadRequest());        
    }
    
    @Test
    public void unlock_user_unlocked_sysUser_id() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID_UNLOCKED +"/unlock")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
            )
            .andExpect(status().isBadRequest());        
    }
    
    @Test
    public void unlock_user_good() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID_LOCKED +"/unlock")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
            )
            .andExpect(status().isOk());        
    }
    
    @Test
    public void unlock_user_not_an_admin() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID_LOCKED +"/unlock")
                .with(authToken(TestConstants.USER_HAGOPIWL))
            )
            .andExpect(status().isForbidden());        
    }
    
    @Test
    public void update_user_status_not_an_admin() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID +"/status")
                .with(authToken(TestConstants.USER_HAGOPIWL))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        + "\"status\":\"SUSPENDED\""
                        + "}")
            )
            .andExpect(status().isForbidden());        
    }
    
    @Test
    public void update_user_status_already_this_status() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID +"/status")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        + "\"status\":\"INACTIVE\""
                        + "}")
            )
            .andExpect(status().isBadRequest());        
    }
    
    @Test
    public void update_user_status_all_good() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID +"/status")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        + "\"status\":\"SUSPENDED\""
                        + "}")
            )
            .andExpect(status().isOk());        
    }
    
    @Test
    public void date_user_status_not_valid_status() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + ID +"/status")
                .with(authToken(TestConstants.USER_WRIGHTMA_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        + "\"status\":\"FAKESTATUS\""
                        + "}")
            )
            .andExpect(status().isBadRequest());        
    }
    
    @Test
    public void update_user_status_not_admin_from_new_to_active() throws Exception {
        mockMvc.perform(
                put("/rest/users/" + TestConstants.PAX_KUMARSJ +"/status")
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        + "\"status\":\"ACTIVE\""
                        + "}")
            )
            .andExpect(status().isOk());        
    }
}
