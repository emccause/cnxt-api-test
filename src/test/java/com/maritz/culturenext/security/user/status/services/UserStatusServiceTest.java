package com.maritz.culturenext.security.user.status.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.security.user.status.constants.UserStatusConstants;
import com.maritz.test.AbstractDatabaseTest;

public class UserStatusServiceTest extends AbstractDatabaseTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private UserStatusService userStatusService;    
    
    public static final Long ID_INVALID = 99999L;
    public static final Long ID_UNLOCKED = 5211L;
    
    @Test
    public void unlock_user_by_sysUser_id_invalid_sysUser() throws Exception {
        
        authenticationService.authenticate(TestConstants.USER_WRIGHTMA_ADMIN);
        try {
            userStatusService.unlockUserBySysUserId(ID_INVALID);
        }
        catch(ErrorMessageException em) {
            for(ErrorMessage message: em.getErrorMessages())
                assertThat(message.getMessage(), is(UserStatusConstants.ERROR_USER_INVALID_MSG));
        }  
    }
    
    @Test
    public void unlock_user_by_sysUser_id_unlocked_sysUser() throws Exception {
        
        authenticationService.authenticate(TestConstants.USER_WRIGHTMA_ADMIN);
        try {
            userStatusService.unlockUserBySysUserId(ID_UNLOCKED);
        }
        catch(ErrorMessageException em) {
            for(ErrorMessage message: em.getErrorMessages())
                assertThat(message.getMessage(), is(UserStatusConstants.ERROR_USER_NOT_LOCKED_MSG));
        }  
    }
}
