package com.maritz.culturenext.groups.jpa.repository;

import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.jpa.repository.CnxtGroupsRepository;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

public class GroupsRepositoryTest extends AbstractDatabaseTest {

    @Inject private CnxtGroupsRepository cnxtGroupsRepository;


    @Test
    public void get_active_groups() throws Exception {
        List<Groups> groups = cnxtGroupsRepository.findByStatusTypeCode(StatusTypeCode.ACTIVE.name());

        assertThat(groups.size(), greaterThan(0));
    }


}




