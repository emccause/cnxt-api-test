package com.maritz.culturenext.groups.config.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.IMAGE_REST_PARAM;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.project.dto.ColorsProfileDTO;
import com.maritz.test.AbstractRestTest;


public class GroupsConfigRestServiceTest extends AbstractRestTest {

    private static final Long GROUP_ID_4 = 4L;
    private static final Long GROUP_ID_6 = 6L;
    
    private static final String URL_GET_ALL_GROUP_CONFIGS = "/rest/groups/config";
    private static final String URL_POST_GROUP_CONFIGS = "/rest/groups/"+TestConstants.ID_2+"/config";
    private static final String URL_PUT_PATCH_GROUP_CONFIGS = "/rest/groups/"+TestConstants.ID_1+"/config";
    private static final String URL_POST_GROUP_CONFIGS_INVALID = "/rest/groups/"+GROUP_ID_4+"/config";
    private static final String URL_POST_GROUP_CONFIGS_MISMATCH = "/rest/groups/"+GROUP_ID_6+"/config";
    
    private static final String GROUPS_CONFIG_LOGO_URL = "/rest/groups/1002/config/logo";
    private static final String NEW_GROUPS_CONFIG_LOGO_URL = "/rest/groups/1010/config/logo";
    private static final String GROUPS_CONFIG_LOGO_WRONG_GROUP_URL = "/rest/groups/5000/config/logo";
    private static final String GROUP_SPECIFIC_LOGO_URL_NO_GROUP_ID = "/rest/groups//config/logo";
    private static final String GROUP_SPECIFIC_LOGO_URL_INACTIVE_GROUP = "/rest/groups/27/config/logo";
    private static final String GROUP_SPECIFIC_LOGO_URL_JPG_GROUP = "/rest/groups/5/config/logo";
    private static final String GROUP_SPECIFIC_LOGO_URL_PNG_GROUP = "/rest/groups/6/config/logo";
    private static final String GROUP_SPECIFIC_LOGO_URL_SVG_GROUP = "/rest/groups/7/config/logo";

    private static final String URL_GET_LOGO_FOR_PAX_BASED_ON_GROUP_CONFIG = "/rest/participants/4/groups/logo";
    private static final String URL_GET_NO_LOGO_FOR_PAX_BASED_ON_GROUP_CONFIG = "/rest/participants/2/groups/logo";
    private static final String URL_GET_ERROR_MANY_LOGOS_FOR_PAX_BASED_ON_GROUP_CONFIG = "/rest/participants/24/groups/logo";

    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL = "/rest/groups/4/config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_WRONG_GROUP = "/rest/groups/5000/config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_NO_GROUP_ID = "/rest/groups//config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_INACTIVE_GROUP = "/rest/groups/27/config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_JPG_GROUP = "/rest/groups/5/config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_PNG_GROUP = "/rest/groups/6/config/hero";
    private static final String GROUP_SPECIFIC_HERO_IMAGE_URL_SVG_GROUP = "/rest/groups/7/config/hero";

    private static final String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private static final String LOGO_FILE = "cycle.jpg";
    private static final String LOGO_FILE_2 = "highway.jpg";
    private static final String FILE = "file";
    
    private static final String COLOR_BLACK = "#000000";
    private static final String COLOR_GRAY = "#454545";
    private static final String COLOR_GREEN = "#00FF00";
    private static final String COLOR_LIGHT_GRAY = "#999999";
    private static final String COLOR_PURPLE = "#800080";
    private static final String COLOR_RED = "#FF0000";
    
    FileInputStream image = null; 

    @Test
    public void get_all_group_configs() throws Exception {
        mockMvc.perform(
                get(URL_GET_ALL_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_get_group_logo_jpg() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_LOGO_URL_JPG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_group_logo_png() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_LOGO_URL_PNG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_group_logo_svg() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_LOGO_URL_SVG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_group_hero_image_jpg() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL_JPG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_group_hero_image_png() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL_PNG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_group_hero_image_svg() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL_SVG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_delete_group_specific_logo() throws Exception {
        mockMvc.perform(
                delete(GROUPS_CONFIG_LOGO_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
        
        mockMvc.perform(
                get(GROUPS_CONFIG_LOGO_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void test_delete_group_specific_hero_image() throws Exception {
        mockMvc.perform(
                delete(GROUP_SPECIFIC_HERO_IMAGE_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
        
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void post_group_config() throws Exception {

        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new ColorsProfileDTO();
        groupConfigDTO.setGroupId(TestConstants.ID_2);
        ColorsProfileDTO.setPrimaryColor(COLOR_BLACK);
        ColorsProfileDTO.setSecondaryColor(COLOR_GRAY);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_GREEN);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
        
        mockMvc.perform(
                post(URL_POST_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void put_group_config() throws Exception {    
        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new ColorsProfileDTO();
        groupConfigDTO.setGroupId(TestConstants.ID_1);
        ColorsProfileDTO.setPrimaryColor(COLOR_LIGHT_GRAY);
        ColorsProfileDTO.setSecondaryColor(COLOR_PURPLE);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_RED);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                put(URL_PUT_PATCH_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_group_config() throws Exception {        
        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new ColorsProfileDTO();
        groupConfigDTO.setGroupId(TestConstants.ID_1);
        ColorsProfileDTO.setPrimaryColor(COLOR_LIGHT_GRAY);
        ColorsProfileDTO.setSecondaryColor(COLOR_PURPLE);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_RED);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                patch(URL_PUT_PATCH_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void post_group_config_group_id_mismatch() throws Exception {        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new ColorsProfileDTO();
        groupConfigDTO.setGroupId(GROUP_ID_4);
        ColorsProfileDTO.setPrimaryColor(COLOR_BLACK);
        ColorsProfileDTO.setSecondaryColor(COLOR_GRAY);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_GREEN);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                post(URL_POST_GROUP_CONFIGS_MISMATCH)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void post_group_config_group_missing_colors() throws Exception {        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        groupConfigDTO.setGroupId(GROUP_ID_6);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                post(URL_POST_GROUP_CONFIGS_INVALID)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void post_group_config_missing_group_id() throws Exception {        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new ColorsProfileDTO();
        ColorsProfileDTO.setPrimaryColor(COLOR_BLACK);
        ColorsProfileDTO.setSecondaryColor(COLOR_GRAY);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_GREEN);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                post(URL_POST_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void post_group_config_colors_incomplete() throws Exception {        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        ColorsProfileDTO ColorsProfileDTO = new com.maritz.culturenext.project.dto.ColorsProfileDTO();
        groupConfigDTO.setGroupId(GROUP_ID_4);
        ColorsProfileDTO.setSecondaryColor(COLOR_GRAY);
        ColorsProfileDTO.setTextOnPrimaryColor(COLOR_GREEN);
        groupConfigDTO.setColors(ColorsProfileDTO);
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                post(URL_PUT_PATCH_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void get_group_specific_logo_ok() throws Exception {
        mockMvc.perform(
                get(GROUPS_CONFIG_LOGO_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_group_specific_logo_wrong_group() throws Exception {
        mockMvc.perform(
                get(GROUPS_CONFIG_LOGO_WRONG_GROUP_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }

    
    @Test
    public void post_new_group_specific_logo() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_JPG.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(NEW_GROUPS_CONFIG_LOGO_URL))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());

    }
    
    @Test
    public void post_update_group_specific_logo() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE_2);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                            LOGO_FILE_2 ,
                            MediaTypesEnum.IMAGE_JPG.name(), 
                            image);
        
        mockMvc.perform(
                fileUpload(String.format(NEW_GROUPS_CONFIG_LOGO_URL))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());

    }
        
    @Test
    public void get_logo_image_happy_path() throws Exception {
        mockMvc.perform(
                get(URL_GET_LOGO_FOR_PAX_BASED_ON_GROUP_CONFIG)
                    .with(authToken(TestConstants.USER_FOLEYJM))
        )
                .andExpect(status().isOk())
                .andReturn();
    }
    
    @Test
    public void get_error_no_logo() throws Exception {
        mockMvc.perform(
                get(URL_GET_NO_LOGO_FOR_PAX_BASED_ON_GROUP_CONFIG)
                    .with(authToken(TestConstants.USER_ADMIN))
        )
                .andExpect(status().isNotFound())
                .andReturn();
    }
    
    @Test
    public void get_pax_with_two_logos_return_null() throws Exception {
        mockMvc.perform(
                get(URL_GET_ERROR_MANY_LOGOS_FOR_PAX_BASED_ON_GROUP_CONFIG)
                    .with(authToken(TestConstants.USER_SELLSAA))
        )
                .andExpect(status().isNotFound())
                .andReturn();
    }
    
    @Test
    public void get_group_specific_hero_image_happy_path() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_group_specific_hero_image_wrong_group() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL_WRONG_GROUP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }

    
    @Test
    public void post_new_group_specific_hero_image() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_JPG.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_HERO_IMAGE_URL))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());

    }
    
    @Test
    public void post_update_group_specific_hero_image() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE_2);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                            LOGO_FILE_2 ,
                            MediaTypesEnum.IMAGE_JPG.name(), 
                            image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_HERO_IMAGE_URL))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());

    }
    
    @Test
    public void test_get_group_specific_logo_no_group_id() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_LOGO_URL_NO_GROUP_ID)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void test_get_group_specific_hero_image_no_group_id() throws Exception {
        mockMvc.perform(
                get(GROUP_SPECIFIC_HERO_IMAGE_URL_NO_GROUP_ID)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_create_update_group_config_dto_null() throws Exception {
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        
        String json = ObjectUtils.objectToJson(groupConfigDTO);
            
        mockMvc.perform(
                post(URL_POST_GROUP_CONFIGS)
                .with(authToken(TestConstants.USER_ADMIN)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_create_group_logo_inactive_group() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_JPG.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_LOGO_URL_INACTIVE_GROUP))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_create_group_hero_image_inactive_group() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_JPG.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_HERO_IMAGE_URL_INACTIVE_GROUP))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_create_group_logo_nonimage_file() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.TEXT_CSV.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_LOGO_URL_INACTIVE_GROUP))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_create_group_hero_image_nonimage_file() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE_REST_PARAM , 
                        LOGO_FILE ,
                        MediaTypesEnum.TEXT_CSV.name(), 
                        image);
        
        mockMvc.perform(
                fileUpload(String.format(GROUP_SPECIFIC_HERO_IMAGE_URL_INACTIVE_GROUP))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk());
    }
}

