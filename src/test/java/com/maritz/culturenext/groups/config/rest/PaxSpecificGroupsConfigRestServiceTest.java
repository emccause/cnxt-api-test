package com.maritz.culturenext.groups.config.rest;

import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class PaxSpecificGroupsConfigRestServiceTest extends AbstractRestTest {
    
    public static final Long TEST_PAX_ID = 4L;
    public static final Long TEST_PAX_ID_2 = 10L;
    public static final String COLORS = "colors";
    public static final String LOGO_ID = "logoId";
    public static final String HERO_IMAGE_ID = "heroImageId";
    
    @Test
    public void get_pax_specific_groups_config_single_group_color() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/config")
                    .with(authToken(TestConstants.USER_FOLEYJM))
        )
                .andExpect(status().isOk())
                .andReturn();
        
        assertThat(result.getResponse().getContentAsString(), notNullValue());
        assertThat(result.getResponse().getContentAsString().contains(COLORS), is(true));
        assertThat(result.getResponse().getContentAsString().contains(LOGO_ID), is(true));
        assertThat(result.getResponse().getContentAsString().contains(HERO_IMAGE_ID), is(true));
    }
    
    @Test
    public void get_pax_specific_groups_config_multiple_group_colors() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/groups/config")
                    .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk())
                .andReturn();
        
        assertThat(result.getResponse().getContentAsString(), notNullValue());
        assertThat(result.getResponse().getContentAsString().contains(COLORS), is(true));
        assertThat(result.getResponse().getContentAsString().contains(LOGO_ID), is(true));
        assertThat(result.getResponse().getContentAsString().contains(HERO_IMAGE_ID), is(true));
    }
    
    @Test
    public void get_pax_specific_groups_config_no_group_colors() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/groups/config")
                    .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().isOk())
                .andReturn();
        
        assertThat(result.getResponse().getContentAsString(), notNullValue());
        assertThat(result.getResponse().getContentAsString().contains(COLORS), is(true));
        assertThat(result.getResponse().getContentAsString().contains(LOGO_ID), is(true));
        assertThat(result.getResponse().getContentAsString().contains(HERO_IMAGE_ID), is(true));
    }
    
    @Test
    public void get_pax_specific_groups_config_not_own_pax_id() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/config")
                    .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().isForbidden())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
        assertThat(result.getResponse().getContentAsString().contains(COLORS), is(false));
        assertThat(result.getResponse().getContentAsString().contains(LOGO_ID), is(false));
        assertThat(result.getResponse().getContentAsString().contains(HERO_IMAGE_ID), is(false));
    }

    @Test
    public void get_pax_specific_groups_logo_one_logo() throws Exception{
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/logo")
                    .with(authToken(TestConstants.USER_FOLEYJM))
        )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_logo_multiple_logos() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/groups/logo")
                        .with(authToken(TestConstants.USER_WILSONEA))
        )
                .andExpect(status().is4xxClientError())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_logo_no_logos() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/groups/logo")
                        .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().is4xxClientError())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_logo_not_own_pax_id() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/logo")
                        .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().isForbidden())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_hero_image_one_image() throws Exception{
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/hero-image")
                        .with(authToken(TestConstants.USER_FOLEYJM))
        )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_hero_image_multiple_images() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TestConstants.PAX_KILINSKIS + "/groups/hero-image")
                        .with(authToken(TestConstants.USER_KILINSKIS_ADMIN))
        )
                .andExpect(status().is4xxClientError())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_hero_image_no_images() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/groups/hero-image")
                        .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().is4xxClientError())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }

    @Test
    public void get_pax_specific_groups_hero_image_not_own_pax_id() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/" + TEST_PAX_ID + "/groups/hero-image")
                        .with(authToken(TestConstants.USER_HAENNICJ))
        )
                .andExpect(status().isForbidden())
                .andReturn();

        assertThat(result.getResponse().getContentAsString(), notNullValue());
    }
}
