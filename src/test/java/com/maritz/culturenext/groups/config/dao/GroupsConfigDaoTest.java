package com.maritz.culturenext.groups.config.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.groups.config.dto.GroupConfigDTO;
import com.maritz.culturenext.project.dto.ColorsProfileDTO;
import com.maritz.test.AbstractDatabaseTest;

public class GroupsConfigDaoTest extends AbstractDatabaseTest {

    @Inject private GroupConfigServiceDao groupConfigServiceDao;
    
    @Test
    public void test_delete_group_logo() {
        groupConfigServiceDao.deleteGroupSpecificFile(TestConstants.ID_4, FileTypes.GROUP_LOGO.name());
        
        assertNull(groupConfigServiceDao.getGroupSpecificFile(TestConstants.ID_4, FileTypes.GROUP_LOGO.name()));
    }
    
    @Test
    public void test_delete_group_hero_image() {
        groupConfigServiceDao.deleteGroupSpecificFile(TestConstants.ID_4, FileTypes.GROUP_HERO_IMAGE.name());
        
        assertNull(groupConfigServiceDao.getGroupSpecificFile(TestConstants.ID_4, FileTypes.GROUP_HERO_IMAGE.name()));

    }
    
    @Test
    public void test_create_update_group_config_happy_path() {
        ColorsProfileDTO colors = new ColorsProfileDTO();
        colors.setPrimaryColor(TestConstants.PRIMARY_COLOR_VALUE);        
        colors.setSecondaryColor(TestConstants.SECONDARY_COLOR_VALUE);
        colors.setTextOnPrimaryColor(TestConstants.TEXT_ON_PRIMARY_COLOR_VALUE);
        
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        groupConfigDTO.setGroupId(TestConstants.ID_4);
        groupConfigDTO.setColors(colors);
        
        groupConfigServiceDao.createUpdateGroupConfig(groupConfigDTO);
    }
    
    @Test
    public void test_create_update_group_config_no_color() {
        GroupConfigDTO groupConfigDTO = new GroupConfigDTO();
        groupConfigDTO.setGroupId(TestConstants.ID_4);
        
        groupConfigServiceDao.createUpdateGroupConfig(groupConfigDTO);
    }
    
    @Test
    public void test_get_image_file() {
        Files file = groupConfigServiceDao.getGroupSpecificFile(TestConstants.ID_4, FileTypes.GROUP_HERO_IMAGE.name());
        
        assertNotNull(file);
    }
    
    @Test
    public void test_get_group_hero_image_no_results() {
        Files file = groupConfigServiceDao.getGroupSpecificFile(TestConstants.ID_MAX_VALUE, FileTypes.GROUP_HERO_IMAGE.name());
        
        assertNull(file);
    }
}
