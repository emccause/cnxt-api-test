package com.maritz.culturenext.groups.config.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.enums.MediaTypesEnum;

import com.maritz.culturenext.images.service.FileImageService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.groups.config.constants.PaxSpecificGroupsConfigConstants;
import com.maritz.culturenext.groups.config.dao.PaxSpecificGroupsConfigDao;
import com.maritz.culturenext.groups.config.dto.PaxSpecificGroupsConfigDTO;
import com.maritz.culturenext.groups.config.service.impl.PaxSpecificGroupsConfigServiceImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;

@PrepareForTest(StreamUtils.class)
@RunWith(PowerMockRunner.class)
public class PaxSpecificGroupsConfigServiceTest {
    
    @InjectMocks private PaxSpecificGroupsConfigServiceImpl paxSpecificGroupsConfigService;
    @Mock private PaxSpecificGroupsConfigDao paxSpecificGroupsConfigDao;
    @Mock private FileImageService fileImageService;
    
    private final Long TEST_PAX_ID_NO_COLORS = 142L;
    private final Long TEST_PAX_ID_ONE_COLOR = 4L;
    private final Long TEST_PAX_ID_TWO_COLORS = 328L;
    private final Long TEST_FILE_ID = 12097L;
    private final Long TEST_PAX_ID_NO_LOGOS = 2100L;
    private final Long TEST_PAX_ID_ONE_LOGO =  6L;
    private final Long TEST_PAX_ID_TWO_LOGOS = 7L;
    private final Long TEST_PAX_ID_NO_HERO_IMAGES = 3L;
    private final Long TEST_PAX_ID_ONE_HERO_IMAGE = 5L;
    private final Long TEST_PAX_ID_TWO_HERO_IMAGES = 141L;
    public static final String PRIMARY_COLOR_FOLEYJM = "#AAAAAA";
    public static final String SECONDARY_COLOR_FOLEYJM = "#BBBBBB";
    public static final String TEXT_ON_PRIMARY_COLOR_FOLEYJM = "#CCCCCC";
    
    @Before
    public void setup() {

        PowerMockito.mockStatic(StreamUtils.class);
    }
    
    @Test
    public void test_getPaxSpecificGroupsConfig_no_groups_with_colors() {
        List<Map<String, Object>> testList = new ArrayList<Map<String, Object>>();
        
        PowerMockito.when(paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_NO_COLORS, ProjectConstants.GROUP_SPECIFIC_KEY_LIST)).thenReturn(testList);
        
        PaxSpecificGroupsConfigDTO paxSpecificGroupsConfigDTO = paxSpecificGroupsConfigService.getPaxSpecificGroupsConfig(TEST_PAX_ID_NO_COLORS);
        
        assertNull(paxSpecificGroupsConfigDTO.getColors());
        assertNull(paxSpecificGroupsConfigDTO.getLogoId());
        assertNull(paxSpecificGroupsConfigDTO.getHeroImageId());
    }
    
    @Test
    public void test_getPaxSpecificGroupsConfig_one_group_with_colors() {
        Map<String, Object> testMap1 = new HashMap<String, Object>();
        Map<String, Object> testMap2 = new HashMap<String, Object>();
        Map<String, Object> testMap3 = new HashMap<String, Object>();
        
        testMap1.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.PRIMARY_COLOR_KEY);
        testMap1.put(PaxSpecificGroupsConfigConstants.VALUE, PRIMARY_COLOR_FOLEYJM);
        testMap1.put(PaxSpecificGroupsConfigConstants.NUMBER, 1);
        
        testMap2.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.SECONDARY_COLOR_KEY);
        testMap2.put(PaxSpecificGroupsConfigConstants.VALUE, SECONDARY_COLOR_FOLEYJM);
        testMap2.put(PaxSpecificGroupsConfigConstants.NUMBER, 1);

        testMap3.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.TEXT_ON_PRIMARY_COLOR_KEY);
        testMap3.put(PaxSpecificGroupsConfigConstants.VALUE, TEXT_ON_PRIMARY_COLOR_FOLEYJM);
        testMap3.put(PaxSpecificGroupsConfigConstants.NUMBER, 1);
        
        List<Map<String, Object>> testList = new ArrayList<Map<String, Object>>();
        
        testList.add(testMap1);
        testList.add(testMap2);
        testList.add(testMap3);
        
        Files files = new Files();
        files.setId(TEST_FILE_ID);
        
        PowerMockito.when(paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_ONE_COLOR, ProjectConstants.GROUP_SPECIFIC_KEY_LIST)).thenReturn(testList);
        
        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_ONE_COLOR)).thenReturn(files);
        PaxSpecificGroupsConfigDTO paxSpecificGroupsConfigDTO = paxSpecificGroupsConfigService.getPaxSpecificGroupsConfig(TEST_PAX_ID_ONE_COLOR);

        assertEquals(paxSpecificGroupsConfigDTO.getColors().getPrimaryColor(), PRIMARY_COLOR_FOLEYJM);
        assertEquals(paxSpecificGroupsConfigDTO.getColors().getSecondaryColor(), SECONDARY_COLOR_FOLEYJM);
        assertEquals(paxSpecificGroupsConfigDTO.getColors().getTextOnPrimaryColor(), TEXT_ON_PRIMARY_COLOR_FOLEYJM);
        assertEquals(TEST_FILE_ID, paxSpecificGroupsConfigDTO.getLogoId());
    }
    
    @Test
    public void test_getPaxSpecificGroupsConfig_two_groups_with_colors() {
        Map<String, Object> testMap1 = new HashMap<String, Object>();
        Map<String, Object> testMap2 = new HashMap<String, Object>();
        Map<String, Object> testMap3 = new HashMap<String, Object>();
        
        testMap1.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.PRIMARY_COLOR_KEY);
        testMap1.put(PaxSpecificGroupsConfigConstants.VALUE, "#AAAAAA");
        testMap1.put(PaxSpecificGroupsConfigConstants.NUMBER, 2);
        
        testMap2.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.SECONDARY_COLOR_KEY);
        testMap2.put(PaxSpecificGroupsConfigConstants.VALUE, "#BBBBBB");
        testMap2.put(PaxSpecificGroupsConfigConstants.NUMBER, 2);

        testMap3.put(PaxSpecificGroupsConfigConstants.KEYNAME, ProjectConstants.TEXT_ON_PRIMARY_COLOR_KEY);
        testMap3.put(PaxSpecificGroupsConfigConstants.VALUE, "#CCCCCC");
        testMap3.put(PaxSpecificGroupsConfigConstants.NUMBER, 2);
        
        List<Map<String, Object>> testList = new ArrayList<Map<String, Object>>();
        
        testList.add(testMap1);
        testList.add(testMap2);
        testList.add(testMap3);
        
        PowerMockito.when(paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_TWO_COLORS, ProjectConstants.GROUP_SPECIFIC_KEY_LIST)).thenReturn(testList);
        
        PaxSpecificGroupsConfigDTO paxSpecificGroupsConfigDTO = paxSpecificGroupsConfigService.getPaxSpecificGroupsConfig(TEST_PAX_ID_TWO_COLORS);
        
        assertNull(paxSpecificGroupsConfigDTO.getColors());
        assertNull(paxSpecificGroupsConfigDTO.getLogoId()); //is part of 2 groups and returns null
        assertNull(paxSpecificGroupsConfigDTO.getHeroImageId());
    }

    @Test
    public void test_getPaxSpecificGroupsLogo_no_groups_with_logos() {
        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_NO_LOGOS)).thenReturn(null);

        ResponseEntity<Object> paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(TEST_PAX_ID_NO_LOGOS);
        ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);

        assertEquals(paxSpecificGroupsLogo, responseEntity);
    }

    @Test
    public void test_getPaxSpecificGroupsLogo_one_group_with_logo() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_JPEG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_ONE_LOGO)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));
        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(paxSpecificGroupsLogo);
    }

    @Test
    public void test_getPaxSpecificGroupsLogo_two_groups_with_logo() {
        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_TWO_LOGOS)).thenReturn(null);

        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(TEST_PAX_ID_TWO_LOGOS);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));
        ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);

        assertEquals(paxSpecificGroupsLogo, responseEntity);
    }

    @Test
    public void test_getPaxSpecificGroupsLogo_JPG() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_JPG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_ONE_LOGO)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));
        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(paxSpecificGroupsLogo);
    }

    @Test
    public void test_getPaxSpecificGroupsLogo_PNG() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_PNG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_ONE_LOGO)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));
        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsLogo(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(paxSpecificGroupsLogo);
    }

    @Test
    public void test_getPaxSpecificGroupsConfig_no_groups_with_hero_image() {
        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_NO_HERO_IMAGES)).thenReturn(null);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));

        ResponseEntity paxSpecificGroupsHeroImage = paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(TEST_PAX_ID_NO_HERO_IMAGES);

        ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);

        assertEquals(paxSpecificGroupsHeroImage, responseEntity);
    }

    @Test
    public void test_getPaxSpecificGroupsConfig_one_group_with_hero_image() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_JPEG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_ONE_HERO_IMAGE)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));
        ResponseEntity paxSpecificGroupsHeroImage = paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(TEST_PAX_ID_ONE_HERO_IMAGE);

        assertNotNull(paxSpecificGroupsHeroImage);
    }

    @Test
    public void test_getPaxSpecificGroupsConfig_two_groups_with_hero_image() {
        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_TWO_HERO_IMAGES)).thenReturn(null);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(new ResponseEntity<Object>(HttpStatus.NOT_FOUND));

        ResponseEntity paxSpecificGroupsHeroImage = paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(TEST_PAX_ID_TWO_HERO_IMAGES);
        ResponseEntity<Object> responseEntity = new ResponseEntity<Object>(HttpStatus.NOT_FOUND);

        assertEquals(paxSpecificGroupsHeroImage, responseEntity);
    }
    @Test
    public void test_getPaxSpecificGroupsHeroImage_JPG() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_JPG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_ONE_LOGO)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));

        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(paxSpecificGroupsLogo);
    }

    @Test
    public void test_getPaxSpecificGroupsHeroImage_PNG() {
        Files file = new Files();
        file.setId(TEST_FILE_ID);
        file.setMediaType(MediaTypesEnum.IMAGE_PNG.value());

        PowerMockito.when(paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_ONE_LOGO)).thenReturn(file);
        PowerMockito.when( fileImageService.prepareImageForResponse(Mockito.anyLong(),Mockito.any(Files.class)))
                .thenReturn(Mockito.mock(ResponseEntity.class));

        ResponseEntity paxSpecificGroupsLogo = paxSpecificGroupsConfigService.getPaxSpecificGroupsHeroImage(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(paxSpecificGroupsLogo);
    }
}
