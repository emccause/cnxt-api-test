package com.maritz.culturenext.groups.config.dao;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.maritz.core.jpa.entity.Files;
import org.junit.Test;

import com.maritz.culturenext.groups.config.constants.PaxSpecificGroupsConfigConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.test.AbstractDatabaseTest;

public class PaxSpecificGroupsConfigDaoTest extends AbstractDatabaseTest {
    
    public static final Long TEST_PAX_ID_NO_COLOR = 142L;
    public static final Long TEST_PAX_ID_ONE_COLOR = 4L;
    public static final Long TEST_PAX_ID_TWO_COLORS = 328L;
    public static final Long TEST_PAX_ID_NO_LOGO = 2100L;
    public static final Long TEST_PAX_ID_ONE_LOGO = 6L;
    public static final Long TEST_PAX_ID_TWO_LOGOS = 141L;
    public static final Long TEST_PAX_ID_NO_HERO_IMAGES = 3L;
    public static final Long TEST_PAX_ID_ONE_HERO_IMAGE = 50L;
    public static final Long TEST_PAX_ID_TWO_HERO_IMAGES = 100L;
    public static final String TEST_HERO_IMAGE_FILE_NAME = "hit-the-road";
    public static final String TEST_LOGO_FILE_NAME = "MP-8769";
    public static final String PRIMARY_COLOR_FOLEYJM = "#DDDDDD";
    public static final String SECONDARY_COLOR_FOLEYJM = "#EEEEEE";
    public static final String TEXT_ON_PRIMARY_COLOR_FOLEYJM = "#FFFFFF";

    public static final List<String> TEST_KEY_LIST = Arrays.asList(
            "PRIMARY_COLOR",
            "SECONDARY_COLOR",
            "TEXT_ON_PRIMARY_COLOR");
    
    @Inject private PaxSpecificGroupsConfigDao paxSpecificGroupsConfigDao;
    
    @Test
    public void test_get_group_based_config_no_group_colors() {
        List<Map<String, Object>> nodes = paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_NO_COLOR, TEST_KEY_LIST);
        
        assertEquals(nodes.size(), 0);
    }
    
    @Test
    public void test_get_group_based_config_one_group_color() {
        List<Map<String, Object>> nodes = paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_ONE_COLOR, TEST_KEY_LIST);
        
        assertEquals(nodes.size(), 3);
        
        for (Map<String, Object> node : nodes) {
            assertEquals(node.get(PaxSpecificGroupsConfigConstants.NUMBER), 1);
            if (node.get(ProjectConstants.KEY_NAME) == ProjectConstants.PRIMARY_COLOR_KEY) {
                assertEquals(node.get(ProjectConstants.VALUE), PRIMARY_COLOR_FOLEYJM);
            } else if (node.get(ProjectConstants.KEY_NAME) == ProjectConstants.SECONDARY_COLOR_KEY) {
                assertEquals(node.get(ProjectConstants.VALUE), SECONDARY_COLOR_FOLEYJM);
            } else if (node.get(ProjectConstants.KEY_NAME) == ProjectConstants.TEXT_ON_PRIMARY_COLOR_KEY) {
                assertEquals(node.get(ProjectConstants.VALUE), TEXT_ON_PRIMARY_COLOR_FOLEYJM);
            }
        }
    }
    
    @Test
    public void test_get_group_based_config_two_group_colors() {
        List<Map<String, Object>> nodes = paxSpecificGroupsConfigDao.getGroupBasedConfiguration(TEST_PAX_ID_TWO_COLORS, TEST_KEY_LIST);
        
        assertEquals(nodes.size(), 3);
        
        for (Map<String, Object> node : nodes) {
            assertEquals(node.get(PaxSpecificGroupsConfigConstants.NUMBER), 2);
        }
    }

    @Test
    public void test_get_group_based_config_no_group_logo() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_NO_LOGO);

        assertNull(file);
    }

    @Test
    public void test_get_group_based_config_one_group_logo() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_ONE_LOGO);

        assertNotNull(file);
        assertTrue(file.getName().equals(TEST_LOGO_FILE_NAME));
    }

    @Test
    public void test_get_group_based_config_two_group_logos() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificLogo(TEST_PAX_ID_TWO_LOGOS);

        assertNull(file);
    }

    @Test
    public void test_get_group_based_config_no_group_hero_image() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_NO_HERO_IMAGES);

        assertNull(file);
    }

    @Test
    public void test_get_group_based_config_one_group_hero_image() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_ONE_HERO_IMAGE);

        assertNotNull(file);
        assertTrue(file.getName().equals(TEST_HERO_IMAGE_FILE_NAME));
    }

    @Test
    public void test_get_group_based_config_two_group_hero_images() {
        Files file = paxSpecificGroupsConfigDao.getPaxSpecificHeroImage(TEST_PAX_ID_TWO_HERO_IMAGES);

        assertNull(file);
    }
}
