package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.test.AbstractRestTest;

public class NominationRestServiceTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private Security security;

    private static final String NOMINATION_REST_URL = "/rest/participants/~/nominations";
    private static final String TEST_PAYOUT_TYPE = "DPP";

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
    }

    // no groups or pax - fail
    @Test
    public void post_no_groups_or_pax() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // groups only - pass
    @Test
    public void post_groups_only() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // pax only - pass
    @Test
    public void post_pax_only() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // no valid pax to recieve rec (all inactive)
    @Test
    public void post_no_valid_receivers() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(9329L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(10728L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(1146L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // Submitter cannot recognize - fail
    @Test
    public void post_submitter_cannot_recognize() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken("barraek"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // recognizing self only - fail
    @Test
    public void post_self_rec_only() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(security.getPaxId()));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // recognizing self with other pax - fail
    @Test
    public void post_self_rec_with_pax() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(security.getPaxId()));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // recognizing self with other groups - fail
    @Test
    public void post_self_rec_with_groups() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(security.getPaxId()));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // recognizing group that recognizer is a member of - pass
    @Test
    public void post_rec_group_submitter_is_member() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // missing program id - fail
    @Test
    public void post_missing_program_id() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        dto.setReceivers(receivers);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // invalid program for submitter - fail
    @Test
    public void post_invalid_program_for_submitter() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // invalid program for receivers - fail
    @Test
    public void post_invalid_program_for_receiver() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(753L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // criteria present when not configured - fail
    @Test
    public void post_criteria_not_configured_but_present() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(5L);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // criteria missing when not configured - pass
    @Test
    public void post_criteria_not_configured_not_present() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(350L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // missing criteria - fail
    @Test
    public void post_missing_criteria() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // invalid criteria - fail
    @Test
    public void post_invalid_criteria() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(3L);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // multiple valid criteria - pass
    @Test
    public void post_multiple_valid_criteria() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_1);
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // mix valid / invalid criteria - fail
    @Test
    public void post_mix_valid_and_invalid_criteria() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_1);
        criteria.add(3L);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // ecard present when not configured - fail
    @Test
    public void post_ecard_not_configured_but_present() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(29L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // ecard missing when not configured - pass
    @Test
    public void post_ecard_not_configured_not_present() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(350L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // invalid ecard - fail
    @Test
    public void post_invalid_ecard() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(5L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // missing headline - fail
    @Test
    public void post_missing_headline() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // empty headline - fail
    @Test
    public void post_empty_headline() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("");
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // headline > 140 characters - fail
    @Test
    public void post_too_long_headline() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("A headline that is really long and is in fact longer than 140 characters "
                + "so the test should fail.  This is really unreasonably long and you're pretty silly for "
                + "wanting to type this long of a message, if you want to write a novel write it in "
                + "the comment section.");
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // missing comment - pass
    @Test
    public void post_missing_comment() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // empty comment - pass
    @Test
    public void post_empty_comment() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment("");
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    // comment > 2000 characters - fail
    @Test
    public void post_too_long_comment() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        StringBuilder longCommentBuilder = new StringBuilder();
        dto.setIsPrivate(true);

        longCommentBuilder.append("Goodness this comment is going to be long. Are you really expecting me "
                + "to type out over 2000 characters? That's absurd. I'm going to make it easier on myself"
                + " by using a for loop to add words to this or something, because that's a huge waste of time...");
        for(int i = 0; i < 50; i++){
            longCommentBuilder.append(" The quickest brown fox jumps over the lazier dog.");
        }
        dto.setComment(longCommentBuilder.toString());
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    // all expected data - pass
    @Test
    public void post_good_data() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(710L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(8571L));
        dto.setReceivers(receivers);
        dto.setProgramId(343L);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    
    // budget but no award - fail
    @Test
    public void post_budget_no_award() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // award but no budget - fail
    @Test
    public void post_award_no_budget() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        
        GroupMemberAwardDTO receiverRequest = new GroupMemberAwardDTO();
        receiverRequest.setPaxId(969L);
        receiverRequest.setAwardAmount(31);
        receiverRequest.setAwardTierId(TestConstants.ID_1);
        receivers.add(receiverRequest);
        
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // can't give from budget - fail
    @Test
    public void post_cannot_give_from_budget() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardAmount(31));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(TestConstants.USER_BOHMJL_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // award higher than budget - fail
    @Test
    public void post_award_higher_than_budget() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardAmount(1000000));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // award higher than give eligibility - fail
    @Test
    public void post_award_higher_than_personal_limit() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardAmount(1000000));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // award outside tier - fail
    @Test
    public void post_award_outside_award_tier() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardAmount(1));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // award doesn't match increment - fail
    @Test
    public void post_award_doesnt_match_increment() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardAmount(11));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    // everything good - pass
    @Test
    public void post_award_with_points_good() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardTierId(2004L).setAwardAmount(15).setOriginalAmount(15));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE);
        
        List<Long> recognitionCriteriaIds = new ArrayList<Long>();
        recognitionCriteriaIds.add(TestConstants.ID_2);
        recognitionCriteriaIds.add(3L);
        dto.setRecognitionCriteriaIds(recognitionCriteriaIds);
        dto.setIsPrivate(true);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    
    // point load program  - fail
    @Test
    public void post_point_load_program() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setGroupId(1534L));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_2);
        dto.setImageId(29L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void post_advanced_recognition_success() throws Exception{
        budgetEligibilityCacheService.buildCache();
        authenticationService.authenticate(TestConstants.USER_PORTERGA);
        
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardTierId(2004L).setAwardAmount(15).setOriginalAmount(15));
        receivers.add(new GroupMemberAwardDTO().setPaxId(8571L).setAwardTierId(2003L).setAwardAmount(10).setOriginalAmount(10));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE);
        
        List<Long> recognitionCriteriaIds = new ArrayList<Long>();
        recognitionCriteriaIds.add(TestConstants.ID_2);
        recognitionCriteriaIds.add(3L);
        dto.setRecognitionCriteriaIds(recognitionCriteriaIds);
        dto.setIsPrivate(true);

        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void post_advanced_recognition_fail_no_permission() throws Exception{
        budgetEligibilityCacheService.buildCache();    
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(969L).setAwardTierId(2004L).setAwardAmount(15));
        receivers.add(new GroupMemberAwardDTO().setPaxId(8571L).setAwardTierId(2003L).setAwardAmount(10));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        dto.setImageId(40L);
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        
        List<Long> recognitionCriteriaIds = new ArrayList<Long>();
        recognitionCriteriaIds.add(TestConstants.ID_2);
        recognitionCriteriaIds.add(3L);
        dto.setRecognitionCriteriaIds(recognitionCriteriaIds);
        
        mockMvc.perform(
                post(NOMINATION_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isBadRequest())
        ;
    }

}