package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;


public class EngagementScoreRestTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private Security security;

    public static final String BASE_URI = "/rest/participants/~/recognition/engagement_score";
        
    @Before
    public void setup() {        
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
    }

    @Test
    public void get_engagement_score_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/" + security.getPaxId() + "/recognition/engagement_score")
                .with(authToken(security.getUserName()))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_engagement_score_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get(BASE_URI)
                .with(authToken(security.getUserName()))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_engagement_score_for_logged_in_user_with_permission() throws Exception {
        mockMvc.perform(
                get(BASE_URI)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_engagement_score_for_logged_in_user_with_no_permission() throws Exception {
        mockMvc.perform(
                get(BASE_URI)
                .with(authToken(TestConstants.USER_HAGOPIWL))
            )
            .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void get_engagement_score_for_invalid_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/x/recognition/engagement_score")
                .with(authToken(security.getUserName()))
            )
            .andExpect(status().isNotFound())
        ;
    }
}
