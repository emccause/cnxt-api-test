package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.rest.RecognitionRestService;
import com.maritz.test.AbstractRestTest;

public class RecognitionRestServiceTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private Security security;
    
    private static final String TEST_URI = "/rest/" + NominationConstants.NOMINATION_URI_BASE
            + "/" + RecognitionRestService.RECOGNITIONS_BY_NOMINATION_ID_PATH.replace("{nominationId}", "62456");
    
    @Test
    public void test_get_recognitions_by_id() throws Exception {
        
        // just make sure we can hit this, more detailed tests will be in service layer test

        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        mockMvc.perform(
                get(TEST_URI)
                .with(authToken(security.getUserName()))
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                )
        .andExpect(status().isOk());
        
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        mockMvc.perform(
                get(TEST_URI + "?" + RestParameterConstants.OVERRIDE_VISIBILITY_REST_PARAM + "=true")
                .with(authToken(security.getUserName()))
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                )
        .andExpect(status().isOk());
        
    }
}
