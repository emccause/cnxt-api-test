package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class RaiseRestServiceTest extends AbstractRestTest{

    @Inject private AuthenticationService authenticationService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private NominationService nominationService;

    private static final String TEST_PAYOUT_TYPE = "DPP";

    @Test
    public void post_new_raise_happy_path() throws Exception {
        budgetEligibilityCacheService.buildCache();

        NominationRequestDTO nominationRequestDTO = new NominationRequestDTO();
        final GroupMemberAwardDTO groupMemberStubDTO = new GroupMemberAwardDTO();
        groupMemberStubDTO.setPaxId(7213L);
        groupMemberStubDTO.setAwardTierId(2003L);
        groupMemberStubDTO.setAwardAmount(10);
        groupMemberStubDTO.setOriginalAmount(10);
        nominationRequestDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>() {{
            add(groupMemberStubDTO);
        }});
        nominationRequestDTO.setProgramId(TestConstants.ID_1);
        nominationRequestDTO.setBudgetId(TestConstants.ID_1);
        nominationRequestDTO.setRecognitionCriteriaIds(new ArrayList<Long>() {{
            add(3L);}});
        nominationRequestDTO.setHeadline("Test Headline");
        nominationRequestDTO.setComment("Test Comment");
        nominationRequestDTO.setIsPrivate(true);
        nominationRequestDTO.setPayoutType(TEST_PAYOUT_TYPE);

        authenticationService.authenticate(TestConstants.USER_DAGARFIN);
        NominationDetailsDTO nominationDetail = 
                nominationService.createStandardNomination(TestConstants.PAX_DAGARFIN, nominationRequestDTO, Boolean.TRUE);

        Long nominationId = nominationDetail.getNominationId();

        mockMvc.perform(
                post("/rest/nominations/" + nominationId + "/raises")
                        .with(authToken(TestConstants.USER_KILINSKIS_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(
                            "[ {\"budgetId\": " + nominationRequestDTO.getBudgetId() +",\n" +
                                    "    \"awardTierId\": 2003,\n" +
                                    "    \"awardAmount\": 10,\n" +
                                    "    \"fromPax\": {\n" +
                                    "   \t\t\"paxId\": 141\n" +
                                    " \t},\n" +
                                    " \t\"toPax\": {\n" +
                                    " \t\t\"paxId\": 7213\n" +
                                    "\t}\n" +
                                    "}]"
                        )
        )
                .andExpect(status().isOk())
        ;

        mockMvc.perform(
                get("/rest/nominations/" + nominationId + "/raises?fromPaxId=141")
                        .with(authToken(TestConstants.USER_KILINSKIS_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void post_new_raise_happy_path_version_future() throws Exception {
        budgetEligibilityCacheService.buildCache();

        NominationRequestDTO nominationRequestDTO = new NominationRequestDTO();
        final GroupMemberAwardDTO groupMemberStubDTO = new GroupMemberAwardDTO();
        groupMemberStubDTO.setPaxId(7213L);
        groupMemberStubDTO.setAwardTierId(2003L);
        groupMemberStubDTO.setAwardAmount(10);
        groupMemberStubDTO.setOriginalAmount(10);
        nominationRequestDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>() {{
            add(groupMemberStubDTO);
        }});
        nominationRequestDTO.setProgramId(TestConstants.ID_1);
        nominationRequestDTO.setBudgetId(TestConstants.ID_1);
        nominationRequestDTO.setRecognitionCriteriaIds(new ArrayList<Long>() {{
            add(3L);}});
        nominationRequestDTO.setHeadline("Test Headline");
        nominationRequestDTO.setComment("Test Comment");
        nominationRequestDTO.setIsPrivate(true);
        nominationRequestDTO.setPayoutType(TEST_PAYOUT_TYPE);

        authenticationService.authenticate(TestConstants.USER_DAGARFIN);
        NominationDetailsDTO nominationDetail = 
                nominationService.createStandardNomination(TestConstants.PAX_DAGARFIN, nominationRequestDTO, Boolean.TRUE);

        Long nominationId = nominationDetail.getNominationId();

        mockMvc.perform(
                post("/rest/nominations/" + nominationId + "/raises")
                        .with(authToken(TestConstants.USER_KILINSKIS_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(
                            "[ {\"budgetId\": " + nominationRequestDTO.getBudgetId() +",\n" +
                                    "    \"awardTierId\": 2003,\n" +
                                    "    \"awardAmount\": 10,\n" +
                                    "    \"fromPax\": {\n" +
                                    "   \t\t\"paxId\": 141\n" +
                                    " \t},\n" +
                                    " \t\"toPax\": {\n" +
                                    " \t\t\"paxId\": 7213\n" +
                                    "\t}\n" +
                                    "}]"
                        )
        )
                .andExpect(status().isOk())
        ;

        mockMvc.perform(
                get("/rest/nominations/" + nominationId + "/raises?fromPaxId=141")
                        .with(authToken(TestConstants.USER_KILINSKIS_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void post_new_raise_happy_path_no_initial_points() throws Exception {
        budgetEligibilityCacheService.buildCache();

        NominationRequestDTO nominationRequestDTO = new NominationRequestDTO();
        final GroupMemberAwardDTO groupMemberStubDTO = new GroupMemberAwardDTO();
        groupMemberStubDTO.setPaxId(7213L);
        nominationRequestDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>() {{
            add(groupMemberStubDTO);
        }});
        nominationRequestDTO.setProgramId(TestConstants.ID_1);
        nominationRequestDTO.setRecognitionCriteriaIds(new ArrayList<Long>() {{
            add(3L);}});
        nominationRequestDTO.setHeadline("Test Headline");
        nominationRequestDTO.setComment("Test Comment");
        nominationRequestDTO.setIsPrivate(true);
        nominationRequestDTO.setPayoutType(TEST_PAYOUT_TYPE);

        authenticationService.authenticate(TestConstants.USER_KILINSKIS_ADMIN);
        NominationDetailsDTO nominationDetail = 
                nominationService.createStandardNomination(TestConstants.PAX_KILINSKIS, nominationRequestDTO, Boolean.TRUE);

        Long nominationId = nominationDetail.getNominationId();

        mockMvc.perform(
                post("/rest/nominations/" + nominationId + "/raises")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(
                            "[ {\"budgetId\": 1,\n" +
                                    "    \"awardTierId\": 2003,\n" +
                                    "    \"awardAmount\": 10,\n" +
                                    "    \"fromPax\": {\n" +
                                    "   \t\t\"paxId\": 12698\n" +
                                    " \t},\n" +
                                    " \t\"toPax\": {\n" +
                                    " \t\t\"paxId\": 7213\n" +
                                    "\t}\n" +
                                    "}]"
                        )
        )
                .andExpect(status().isOk())
        ;

        mockMvc.perform(
                get("/rest/nominations/" + nominationId + "/raises?fromPaxId=12698")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void post_new_raise_fail_nominationId_as_string() throws Exception {
        budgetEligibilityCacheService.buildCache();

        String nominationId = "nominationIdString";

        mockMvc.perform(
                post("/rest/nominations/" + nominationId + "/raises")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(
                            "[ {\"budgetId\": 1,\n" +
                                    "    \"awardTierId\": 2003,\n" +
                                    "    \"awardAmount\": 10,\n" +
                                    "    \"fromPax\": {\n" +
                                    "   \t\t\"paxId\": 12698\n" +
                                    " \t},\n" +
                                    " \t\"toPax\": {\n" +
                                    " \t\t\"paxId\": 7213\n" +
                                    "\t}\n" +
                                    "}]"
                        )
        )
                .andExpect(status().isNotFound())
        ;

        mockMvc.perform(
                get("/rest/nominations/" + nominationId + "/raises?fromPaxId=12698")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isNotFound())
        ;
    }

}
