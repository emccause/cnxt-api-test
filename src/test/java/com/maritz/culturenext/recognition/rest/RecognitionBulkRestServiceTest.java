package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.test.AbstractRestTest;

public class RecognitionBulkRestServiceTest extends AbstractRestTest {
    
    @Inject ConcentrixDao<Batch> batchDao;
    @Inject ConcentrixDao<BatchFile> batchFileDao;
    
    private static final String FILE_PARAMETER = "file";
    private static final String FILE_NAME = "BULK_UPLOAD.csv";
    private static final String FILE_NAME_WITH_ERRORS = "BULK_UPLOAD_WITH_ERRORS.csv";
    private static final String FILE_NAME_EXTRA_COLUMN = "ERROR_FILE.csv";
    private static final String FILE_NAME_PUBLIC_HEADLINE_ERROR = "PUBLIC_HEADLINE_ERROR.csv";
    private static final String FILE_NAME_PRIVATE_MESSAGE_ERROR = "PRIVATE_MESSAGE_ERROR.csv";
    private static final String BULK_UPLOAD_ADVANCED_REC_URL =  "/rest/recognitions/bulk";
    private static final String BULK_UPLOAD_HISTORY_URL = "/rest/recognitions/bulk/history";
    private static final String UPDATE_BATCH_STATUS_URL = "/rest/batch/";
    
    private static final String FILE_WITH_VERY_LONG_HEADLINE = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706,publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline,Message private 1,programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n" +
    "246,Francisco,Perez,1346,publicHeadline,,programName,40,budgetName,78,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n" +
    "246,Pedro,Ontiveros,8326,publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline publicHeadline,Message private 2,programName,40,budgetName,78,    submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";

    private static final String VERY_LONG_PRIVATE_MESSAGE = "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message private message " +
    "private message private message private message private message private message private message private message private message ";
    
    private static final String FILE_WITH_VERY_LONG_PRIVATE_MESSAGE = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706,publicHeadline," + VERY_LONG_PRIVATE_MESSAGE + ",programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n" +
    "246,Francisco,Perez,1346,publicHeadline,,programName,40,budgetName,78,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n" +
    "246,Pedro,Ontiveros,8326,publicHeadline," + VERY_LONG_PRIVATE_MESSAGE + ",programName,40,budgetName,78,submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";


    private static final String FILE_HAPPY_PATH = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706,publicHeadline,Message private 1,programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n" +
    "246,Francisco,Perez,1346,publicHeadline,,programName,40,budgetName,78,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n" +
    "246,Pedro,Ontiveros,8326,publicHeadline,Message private 2,programName,40,budgetName,78,submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";

    private static final String FILE_WITH_ERRORS = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706,,Message private 1,programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n" +
    "246,Francisco,Perez,,publicHeadline,,programName,40,budgetName,78,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n" +
    "246,Pedro,Ontiveros,8326,publicHeadline,Message private 2,programName,40,budgetName,78,submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";
    
    private static final String FILE_EXTRA_COLUMN = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private,Errors\n" +
    "246,,,1706,,Message private 1,programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE,Error Column!\n";
    
    private static final String FILE_INVALID_FORMAT = 
    "Award Amount,Recipient First Name,Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706,,Message private 1,programName,40,budgetName,76,submitterFirstName,submitterLastName,3482,30,Client Focus,2a,TRUE!\n";
    
    private static final MockMultipartFile multipartfile = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_HAPPY_PATH.getBytes());

    private static final MockMultipartFile multipartfileWithErrors = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME_WITH_ERRORS,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_WITH_ERRORS.getBytes());
    
    private static final MockMultipartFile multipartfileExtraColumn = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME_EXTRA_COLUMN,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_EXTRA_COLUMN.getBytes());
    
    private static final MockMultipartFile multipartfileBadFormat = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME_EXTRA_COLUMN,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_INVALID_FORMAT.getBytes());
    
    private static final MockMultipartFile multipartfileVeryLongPublicHeadLine = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME_PUBLIC_HEADLINE_ERROR,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_WITH_VERY_LONG_HEADLINE.getBytes());

    private static final MockMultipartFile multipartfileVeryLongPrivateMessage = new MockMultipartFile(
            FILE_PARAMETER, 
            FILE_NAME_PRIVATE_MESSAGE_ERROR,
            MediaTypesEnum.TEXT_CSV.value(), 
            FILE_WITH_VERY_LONG_PRIVATE_MESSAGE.getBytes());

    private ObjectWriter ow;
    
    @Before
    public void setup() {
        ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    }

    @Test
    public void test_postUploadBulkUploadAdvanceRecFile_success() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.PENDING_RELEASE.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME));
        
    }
    
    @Test
    public void test_postUploadBulkUploadAdvanceRecFileWithErrors_success() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfileWithErrors)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.FAILED.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME_WITH_ERRORS));
    }
    
    @Test
    public void test_post_UploadBulkUploadAdvanceRecFile_verylong_public_headline() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfileVeryLongPublicHeadLine)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.FAILED.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME_PUBLIC_HEADLINE_ERROR));
    }

    @Test
    public void test_post_UploadBulkUploadAdvanceRecFile_verylong_private_message() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfileVeryLongPrivateMessage)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.FAILED.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME_PRIVATE_MESSAGE_ERROR));
    }
    
    @Test
    public void test_post_invalid_file_format_columns() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfileExtraColumn)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.FAILED.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME_EXTRA_COLUMN));
        
        //Make sure the Error file was saved properly
        BatchFile batchFile = batchFileDao.findBy().desc(ProjectConstants.CREATE_DATE).findOne();
        assert(batchFile.getFileUploadError().equals("Invalid file format"));
    }
    
    @Test
    public void test_post_invalid_file_format_id() throws Exception {
        
        MvcResult result = mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .file(multipartfileBadFormat)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.FAILED.toString()));
        assert(result.getResponse().getContentAsString().contains(FILE_NAME_EXTRA_COLUMN));
        
        //Make sure the Error file was saved properly
        BatchFile batchFile = batchFileDao.findBy().desc(ProjectConstants.CREATE_DATE).findOne();
        assert(batchFile.getFileUploadError().equals("Invalid file format"));
    }
    
    @Test
    public void test_postWithoutFile_fail_null_file() throws Exception {
        
        mockMvc.perform(
                fileUpload(String.format(BULK_UPLOAD_ADVANCED_REC_URL))
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_updateBatchStatus_happyPath() throws Exception {
        
        //Insert a PENDING_RELEASE batch for this test only (so I don't break the integration tests)
        Batch batchToCreate = new Batch();
        batchToCreate.setBatchTypeCode(BatchType.ADV_REC_BULK_UPLOAD.getCode());
        batchToCreate.setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name());
        batchDao.create(batchToCreate);
        
        //Grab the newly created batchId
        Batch pendingBatch = batchDao.findBy()
                .where(ProjectConstants.BATCH_TYPE_CODE).eq(BatchType.ADV_REC_BULK_UPLOAD.getCode())
                .desc(ProjectConstants.CREATE_DATE).findOne();
        
        BatchStatusDTO batchDto = new BatchStatusDTO();
        batchDto.setBatchId(pendingBatch.getBatchId());
        batchDto.setStatus(StatusTypeCode.QUEUED.name());
                
        String json = ow.writeValueAsString(batchDto);
        
        MvcResult result = mockMvc.perform(
                put(UPDATE_BATCH_STATUS_URL + pendingBatch.getBatchId())
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isOk()).andReturn();
    
        assert(result.getResponse().getContentAsString().contains(StatusTypeCode.QUEUED.name()));
        assert(result.getResponse().getContentAsString().contains(pendingBatch.getBatchId().toString()));
    }
    
    @Test
    public void test_updateBatchStatus_invalid_batch_id() throws Exception {
        
        BatchStatusDTO batchDto = new BatchStatusDTO();
        batchDto.setBatchId(TestConstants.ID_1);
        batchDto.setStatus(StatusTypeCode.QUEUED.name());
                
        String json = ow.writeValueAsString(batchDto);
        
        mockMvc.perform(
                put(UPDATE_BATCH_STATUS_URL + "x")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isNotFound()).andReturn();
    }
    
    @Test
    public void test_get_bulk_upload_history_success() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[number]=1&page[size]=1")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_bulk_upload_history_null_page_number() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[size]=1")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_bulk_upload_history_negative_page_number() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[number]=-1&page[size]=1")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_bulk_upload_history_null_page_size() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[number]=1")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_bulk_upload_history_negative_page_size() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[number]=1&page[size]=-1")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_bulk_upload_history_null_page_size_and_multiple_pages() throws Exception {
        
        mockMvc.perform(
                get(BULK_UPLOAD_HISTORY_URL + "?status=PENDING_RELEASE&page[number]=2")
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isBadRequest());
    }
}
