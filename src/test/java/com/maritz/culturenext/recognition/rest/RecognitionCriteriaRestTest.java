package com.maritz.culturenext.recognition.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.recognition.dto.RecognitionCriteriaDTO;
import com.maritz.test.AbstractRestTest;

public class RecognitionCriteriaRestTest extends AbstractRestTest {
    
    @Test
    public void get_rec_criteria_by_status_default() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_rec_criteria_by_status_inactive() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?status=inactive")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    @Test
    public void get_rec_criteria_by_status_active() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?status=active")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_rec_criteria_by_status_multiple() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?status=active,inactive")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_rec_criteria_by_invalid_status() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?status=badstatus")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isBadRequest());
        ;
    }

    @Test
    public void get_rec_criteria_by_status_language_code() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?languageCode=en_US")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_rec_criteria_by_status_language_code_french() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?languageCode=fr_CA")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_rec_criteria_by_ids() throws Exception {
        mockMvc.perform(
                get("/rest/recognition-criteria?ids=1,2,3,4,5")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void put_rec_criteria_fail_recognitionCriteriaId_as_string() throws Exception {
        
        RecognitionCriteriaDTO recognitionCriteriaDTO = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setId(TestConstants.ID_1);
        String json = ObjectUtils.objectToJson(recognitionCriteriaDTO);
        
        mockMvc.perform(
                put("/rest/recognition-criteria/recognitionCriteriaIdString")
                        .with(authToken(TestConstants.USER_KUMARSJ))
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isNotFound());
    }
    
}
