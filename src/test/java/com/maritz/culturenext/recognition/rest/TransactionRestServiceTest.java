package com.maritz.culturenext.recognition.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.recognition.dto.TransactionReversalDTO;
import com.maritz.culturenext.recognition.rest.TransactionsRestService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.test.AbstractRestTest;

public class TransactionRestServiceTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private Security security;
    
    private Map<String, Object> testDataMap;
    private StringBuilder stringBuilder;
    private ObjectWriter ow;
    
    private static final String REVERSE_TRANSACTIONS_ENDPOINT = "/rest/transactions";
    
    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_ADMIN);

        // dummy test data
        testDataMap = new HashMap<>();
        testDataMap.put(RestParameterConstants.PROGRAM_ID_REST_PARAM, "2");
        testDataMap.put(RestParameterConstants.START_DATE_REST_PARAM, "2015-05-01");
        testDataMap.put(RestParameterConstants.END_DATE_REST_PARAM, "2015-05-01");
        testDataMap.put(RestParameterConstants.AWARD_CODE_REST_PARAM, "25");
        testDataMap.put(RestParameterConstants.STATUS_REST_PARAM, "TESTING");
        testDataMap.put(RestParameterConstants.RECIPIENT_PAX_REST_PARAM, "5");
        testDataMap.put(RestParameterConstants.ISSUED_BY_PAX_REST_PARAM, "5");
        testDataMap.put(RestParameterConstants.PROGRAM_TYPE_REST_PARAM, "AWARD_CODE");
        
        stringBuilder = new StringBuilder(TransactionsRestService.TRANSACTION_SEARCH_REST_ENDPOINT); // build params
        Boolean useAmpersand = Boolean.FALSE;
        for (String key : testDataMap.keySet()) {
            if (Boolean.TRUE.equals(useAmpersand)) {
                stringBuilder.append("&");
            }
            else {
                stringBuilder.append("?");
                useAmpersand = Boolean.TRUE;
            }
            stringBuilder.append(key);
            stringBuilder.append("=");
            stringBuilder.append(testDataMap.get(key).toString());
        }
        
        ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    }
    
    // these tests are mostly verifying pagination logic works
    @Test
    public void get_no_data() throws Exception {
        
        PaginatedResponseObject response = ObjectUtils.jsonToObject(mockMvc.perform(
                get(stringBuilder.toString())
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk()).andReturn().getResponse().getContentAsString()
        , PaginatedResponseObject.class);

        List<?> data = response.getData();
        assertNotNull("data should exist", data);
        assertTrue("Should be no results.", data.isEmpty());
        
        Map<String, String> links = response.getLinks();
        assertNotNull("links should exist", links);
        assertFalse("links should be populated", links.isEmpty());
        assertEquals("should be only one link", 1, links.size());
        
        Map<String, Object> meta = response.getMeta();
        assertNotNull("meta should exist", meta);
        assertFalse("meta should be populated", meta.isEmpty());
        
        for (String key : testDataMap.keySet()) {
            assertTrue("key should be present", meta.containsKey(key));
            assertEquals("meta value should match test data value", testDataMap.get(key), meta.get(key));
        }
        
        assertTrue("pageNumber should have been added to meta", meta.containsKey("page[number]"));
        assertEquals("pageNumber should be the default value", ProjectConstants.DEFAULT_PAGE_NUMBER, 
                meta.get("page[number]"));
        
        assertTrue("pageSize should have been added to meta", meta.containsKey("page[size]"));
        assertEquals("pageSize should be the max value", ProjectConstants.MAX_PAGE_SIZE, 
                meta.get("page[size]"));
        
    }
    
    @Test
    public void get_page_two_error() throws Exception {
        
        stringBuilder.append("&page[number]=2"); // Error should be returned
        
        mockMvc.perform(
                get(stringBuilder.toString())
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isBadRequest());

    }
    
    @Test
    public void test_reverse_transactions_success() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        
        reversal.setTransactionIds(Arrays.asList(45198L));
        reversal.setReversalComment("Unit Test");
        
        String json = ow.writeValueAsString(reversal);
        mockMvc.perform(
                put(REVERSE_TRANSACTIONS_ENDPOINT)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_reverse_transactions_null_ids() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setReversalComment("Unit Test");
        
        String json = ow.writeValueAsString(reversal);
        mockMvc.perform(
                put(REVERSE_TRANSACTIONS_ENDPOINT)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_reverse_transactions_null_comment() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(Arrays.asList(45198L));        
        
        String json = ow.writeValueAsString(reversal);
        mockMvc.perform(
                put(REVERSE_TRANSACTIONS_ENDPOINT)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_reverse_transactions_forbidden() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        
        reversal.setTransactionIds(Arrays.asList(45198L));
        reversal.setReversalComment("Unit Test");
        
        String json = ow.writeValueAsString(reversal);
        mockMvc.perform(
                put(REVERSE_TRANSACTIONS_ENDPOINT)
                .with(authToken(TestConstants.USER_PORTERGA))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isForbidden());
    }
}
