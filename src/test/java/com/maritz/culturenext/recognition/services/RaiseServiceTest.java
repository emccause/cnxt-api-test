package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyCollectionOf;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.newsfeed.services.NewsfeedService;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.services.impl.RaiseServiceImpl;
import com.maritz.test.AbstractMockTest;

public class RaiseServiceTest extends AbstractMockTest {

    private static final String DPP = "DPP";
    // test constants
    private static final Long MANAGER_PAX_ID = 123L;
    private static final Long OTHER_PAX_ID = 141L;
    private static final Long PAX_ID_2 = 123L;
    private static final Long TEST_ACTIVITY_ID = 59009L;
    private static final Long TEST_NOMINATION_PARENT_ID = 64654L;
    private static final Long TEST_FROM_MANAGER_PAX_ID = 456L;
    private static final Long TEST_TO_PAX_ID = 171L;
    private static final Long TEST_TO_PAX_MANAGER_PAX_ID = 789L;
    private static final Long TEST_RAISE_PARENT_ID = 202098L;
    private static final Long TEST_BUDGET_ID = 12345L;
    private static final Integer ITERATIONS = 51;
    private static final Double TEST_AWARD_AMOUNT = 50.00;

    // mock objects
    private List<String> statusList = new ArrayList<>();
    private List<String> otherStatusList = new ArrayList<>();
    private List<Map<String, Object>> testRaiseList = new ArrayList<>();
    private List<RaiseDTO> testRaises = new ArrayList<>();
    private RaiseDTO raise = new RaiseDTO();
    private List <Long> groupIds = new ArrayList<>();
    private List<Long> paxIds = new ArrayList<>();
    private Recognition recognition;

    // mocked services
    @Mock private AlertService alertService;
    @Mock private ApprovalService approvalService;
    @Mock private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Mock private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private Environment environment;
    @Mock private NewsfeedService newsfeedService;
    @Mock private NominationService nominationService;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxCountDao paxCountDao;
    @Mock private PaxRepository paxRepository;
    @Mock private RaiseDao raiseDao;
    @Mock private Security security;
    @Mock private ConcentrixDao<ProgramMisc> programMiscDao;

    // mocked findBys
    private FindBy<ApprovalHistory> approvalHistoryFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalPending> approvalPendingFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> programMiscFindBy = PowerMockito.mock(FindBy.class);

    @Rule public ExpectedException thrown = ExpectedException.none();
    @InjectMocks private RaiseServiceImpl raiseService;

    @Before
    public void setup() {
        //Mock out user
        EmployeeDTO submitterPax = new EmployeeDTO();

        String languageCode = "en";
        String preferredLocale = "en_US";
        String controlNum = "s345";
        String firstName = "Kramer";
        String middleName = "M";
        String lastName = "Martin";
        Integer profilePictureVersion = 1;
        String status = "ACTIVE";
        String jobTitle = "Developer II";

        submitterPax.setPaxId(PAX_ID_2);
        submitterPax.setLanguageCode(languageCode);
        submitterPax.setPreferredLocale(preferredLocale);
        submitterPax.setControlNum(controlNum);
        submitterPax.setFirstName(firstName);
        submitterPax.setMiddleName(middleName);
        submitterPax.setLastName(lastName);
        submitterPax.setProfilePictureVersion(profilePictureVersion);
        submitterPax.setStatus(status);
        submitterPax.setJobTitle(jobTitle);
        submitterPax.setManagerPaxId(MANAGER_PAX_ID);

        PowerMockito.when(participantInfoDao.getEmployeeDTO(PAX_ID_2)).thenReturn(submitterPax);

        statusList.add(StatusTypeCode.APPROVED.toString());
        statusList.add(StatusTypeCode.AUTO_APPROVED.toString());
        statusList.add(StatusTypeCode.PENDING.toString());
        otherStatusList.add(StatusTypeCode.APPROVED.toString());
        otherStatusList.add(StatusTypeCode.AUTO_APPROVED.toString());
        for (int i = 0; i < 10; i ++) {
            Map<String, Object> node = new HashMap<>();
            node.put("id", TEST_ACTIVITY_ID);
            node.put("updateDate", new Date());
            node.put("programAwardTierId", TestConstants.ID_1);
            node.put("amount", 25.00);
            if (i % 2 == 0) {
                node.put("statusTypeCode", StatusTypeCode.APPROVED.toString());
            } else {
                node.put("statusTypeCode", StatusTypeCode.PENDING.toString());
            }
            node.put("submitterPaxId", TestConstants.PAX_DAGARFIN);
            node.put("receiverPaxId", OTHER_PAX_ID);
            node.put(ProjectConstants.TOTAL_RESULTS_KEY,10);
            testRaiseList.add(node);
        }

        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(TEST_NOMINATION_PARENT_ID,
                TestConstants.PAX_DAGARFIN, statusList, null, null)).thenReturn(testRaiseList);
        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(TEST_NOMINATION_PARENT_ID,
                null, otherStatusList, null, TestConstants.PAX_DAGARFIN)).thenReturn(testRaiseList);
        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(TEST_NOMINATION_PARENT_ID,
                TestConstants.PAX_DAGARFIN, otherStatusList, null, null)).thenReturn(testRaiseList.subList(0,5));

        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(anyInt(), anyInt(), anyLong()
                , anyLong(), anyListOf(String.class), anyListOf(String.class), anyLong()))
            .thenReturn(testRaiseList);

        List<Recognition> raiseItemList = new ArrayList<>();
        Long id = 0L;
        for (int i = 0; i < ITERATIONS; i++) {
            Recognition currentNode = new Recognition();
            currentNode.setId(id);

            // Setting in Node
            raiseItemList.add(currentNode);
            id += 1;
        }

        // Recognition Recipients for Raise setup

        List<Map<String, Object>> recognitionRecipientsFromDAO = new ArrayList<>();
        List<Long> paxList = new ArrayList<>();
        List<EntityDTO> employeeDTOList = new ArrayList<>();

        for (int i = 0; i < ITERATIONS; i++) {
            EmployeeDTO current = new EmployeeDTO();
            Map<String, Object> node = new HashMap<>();
            node.put("receiverPaxId", TestConstants.PAX_DAGARFIN);
            recognitionRecipientsFromDAO.add(node);
            paxList.add(TestConstants.PAX_DAGARFIN);
            employeeDTOList.add(current);
        }
        PowerMockito.when(raiseDao.getListOfPaxOnActivity(anyLong(),anyLong(),anyBoolean(),
                anyListOf(String.class))).thenReturn(recognitionRecipientsFromDAO);
        PowerMockito.when(raiseDao.getListOfPaxOnNomination(anyLong(),anyLong(),anyBoolean()))
                .thenReturn(recognitionRecipientsFromDAO);

        PowerMockito.when(participantInfoDao.getInfo(paxList, null)).thenReturn(employeeDTOList);

        // Create raise setup

        // Create Mock data
        raise.setBudgetId(TEST_BUDGET_ID);
        raise.setAwardTierId(TestConstants.ID_1);
        raise.setAwardAmount(TEST_AWARD_AMOUNT);
        EmployeeDTO fromPax = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        fromPax.setManagerPaxId(TEST_FROM_MANAGER_PAX_ID);
        raise.setFromPax(fromPax);
        EmployeeDTO toPax = new EmployeeDTO();
        toPax.setPaxId(TEST_TO_PAX_ID);
        toPax.setManagerPaxId(TEST_TO_PAX_MANAGER_PAX_ID);
        raise.setToPax(toPax);

        testRaises.add(raise);

        Nomination nominationEntity = new Nomination();
        Recognition recognitionEntity = new Recognition();

        NominationRequestDTO validateNominationRequestDTO = new NominationRequestDTO();
        List<GroupMemberAwardDTO> receivers = new ArrayList<>();
        GroupMemberAwardDTO receiver = new GroupMemberAwardDTO();
        receiver.setPaxId(TEST_TO_PAX_ID);
        receivers.add(receiver);
        validateNominationRequestDTO.setReceivers(receivers);

        paxIds.add(TEST_TO_PAX_ID);

        groupIds.add(0L);

        List<ErrorMessage> errors = new ArrayList<>();

        List<Map<String, Object>> allPax = new ArrayList<>();
        Map<String, Object> pax = new HashMap<>();
        pax.put("PAX_ID", TEST_TO_PAX_ID);
        pax.put("SHARE_REC", true);
        pax.put("MANAGER_PAX_ID", 1074L);
        allPax.add(pax);

        // Set up Mocks
        PowerMockito.when(nominationService.validateNomination(raise.getFromPax().getPaxId(),
                    validateNominationRequestDTO, paxIds, new ArrayList<Long>(), NominationConstants.RAISE, Boolean.FALSE))
                .thenReturn(errors);
        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyString())).thenReturn(programMiscFindBy);
        ProgramMisc programMisc = new ProgramMisc();
        programMisc.setMiscData(DPP);
        PowerMockito.when(programMiscFindBy.findOne()).thenReturn(programMisc);

        PowerMockito.when(raiseDao.isAlreadyRaised(TestConstants.PAX_DAGARFIN, TEST_TO_PAX_ID, TEST_NOMINATION_PARENT_ID))
                .thenReturn(false);
        PowerMockito.when(raiseDao.isValidRecognition(TestConstants.PAX_DAGARFIN, TEST_TO_PAX_ID, TEST_NOMINATION_PARENT_ID))
                .thenReturn(true);
        PowerMockito.when(paxCountDao.getDistinctPax(raise.getFromPax().getPaxId(), groupIds, paxIds))
                .thenReturn(allPax);
        Mockito.doNothing().when(nominationDao).save(nominationEntity);
        Mockito.doNothing().when(recognitionDao).save(recognitionEntity);
        PowerMockito.when(raiseDao.getRecognitionParentId(TEST_NOMINATION_PARENT_ID,
                raise.getToPax().getPaxId())).thenReturn(TEST_RAISE_PARENT_ID);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(nominationEntity.getSubmitterPaxId()))
                .thenReturn(new EmployeeDTO());
        PowerMockito.when(participantInfoDao.getEmployeeDTO(recognitionEntity.getReceiverPaxId()))
                .thenReturn(new EmployeeDTO());

        PowerMockito.doNothing()
                .when(nominationService)
                .insertTransactionData(validateNominationRequestDTO, nominationEntity);

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_DAGARFIN);



        // The recognition from the DB
        recognition = new Recognition();
        recognition.setId(284045L);
        recognition.setNominationId(71116L);
        recognition.setReceiverPaxId(7213L);
        recognition.setParentId(284044L);
        recognition.setProgramAwardTierId(TestConstants.ID_1);
        recognition.setAmount(50.00);
        recognition.setViewed("N");
        recognition.setStatusTypeCode(StatusTypeCode.PENDING.toString());

        // The nomination from the DB
        Nomination nomination;
        nomination = new Nomination();
        nomination.setId(71116L);
        nomination.setNominationTypeCode("ECAW");
        nomination.setProgramId(13567L);
        nomination.setSubmitterPaxId(141L);
        nomination.setBudgetId(12345L);
        nomination.setParentId(71115L);
        nomination.setSubmittalDate(new Date());
        nomination.setStatusTypeCode(StatusTypeCode.PENDING.toString());

        EmployeeDTO fromAndToPax = new EmployeeDTO();
        fromAndToPax.setPaxId(165L);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(fromAndToPax);

        EmployeeDTO approvalPax = new EmployeeDTO();
        approvalPax.setPaxId(11L);
        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);
        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nomination);

        ApprovalPending approvalPending = new ApprovalPending();
        approvalPending.setPaxId(TestConstants.ID_1);
        PowerMockito.when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.where(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(anyLong())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.and(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.findOne()).thenReturn(approvalPending);

        PowerMockito.when(approvalHistoryDao.findBy()).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.where(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.eq(anyLong())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.and(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.eq(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.findOne()).thenReturn(new ApprovalHistory());

        PowerMockito.when(approvalService.createPendingApprovals(anyListOf(Recognition.class), anyLong(),
                anyLong(), anyInt(), anyLong(), anyLong())).thenReturn(false);

        Map<Long, String> statusMap = new HashMap<>();
        statusMap.put(171L, StatusTypeCode.PENDING.toString());
        PowerMockito.when(nominationService.determineRecognitionStatusOnCreation(anyLong(), anyLong(),
                anyInt(), anyListOf(Long.class), anyLong())).thenReturn(statusMap);

        List<Recognition> recognitionList = new ArrayList<>();
        recognitionList.add(recognition);
        PowerMockito.when(recognitionDao.findById(anyCollectionOf(Long.class))).thenReturn(recognitionList);

        List<Nomination> nominationList = new ArrayList<>();
        nominationList.add(nomination);
        PowerMockito.when(nominationDao.findById(anyCollectionOf(Long.class))).thenReturn(nominationList);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_RAISE_APPROVAL_ENABLED))
                .thenReturn("false");

        PowerMockito.when(paxRepository.exists(anyLong())).thenReturn(Boolean.TRUE);
    }

    // Pagination Testing
    @Test
    public void test_pagination_page_and_size_null() {
        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(10));
    }
    @Test
    public void test_pagination_page_1_size_5() {
        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, 5, 1,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(5));
    }
    @Test
    public void test_pagination_page_5_size_1() {
        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, 1, 5,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(1));
    }
    @Test
    public void test_pagination_page_4_size_20() {
        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, 4, 20,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(0));
    }

    // Pagination test using meta data.
    @Test
    public void test_getRaisesForNomination_meta_page_and_size_null() {
        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_meta_page_1_size_10() {
        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_fromPaxId_null() {
        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                statusList, null, null);
        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_status_null() {
        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                null, TestConstants.PAX_DAGARFIN, null);
        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_fromPaxId_not_loggedPax() {

        PowerMockito.when(security.getPaxId()).thenReturn(1298L);

        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                null, TestConstants.PAX_DAGARFIN, null);

        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_status_not_contain_approved() {

        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.PENDING.toString());

        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                statusList, TestConstants.PAX_DAGARFIN, null);

        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    @Test
    public void test_getRaisesForNomination_involvedPaxId_not_null() {

        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.PENDING.toString());

        PaginatedResponseObject<RaiseDTO> paginatedResult = raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, 1, 10,
                statusList, TestConstants.PAX_DAGARFIN, TestConstants.PAX_DAGARFIN);

        assertThat(paginatedResult.getData().size(), equalTo(10));
    }

    // From Pax Id testing
    @Test
    public void test_from_pax_id_equal_happy() {
        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(10));
    }
    @Test
    public void test_from_pax_id_not_equal() {
        PowerMockito.when(security.getPaxId()).thenReturn(1298L);

        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, TestConstants.PAX_DAGARFIN, null);
        assertThat(raiseList.size(), equalTo(5));
    }
    @Test
    public void test_populate_raise_dto_output() {
        PowerMockito.when(security.getPaxId()).thenReturn(165L);

        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, TestConstants.PAX_DAGARFIN, null);
        RaiseDTO testRaise = raiseList.get(0);
        assertThat(testRaise.getId(), equalTo(TEST_ACTIVITY_ID));
        assertThat(testRaise.getAwardAmount(), equalTo(25.00));
        assertThat(testRaise.getStatus(), equalTo(StatusTypeCode.APPROVED.toString()));
    }

    // Testing fetch recipients
    @Test
    public void test_pagination_page_and_size_null_recognition_recipients() {
        List<EntityDTO> recognitionRecipientList = raiseService.getRecipientsForRaise(TEST_ACTIVITY_ID,
                false, null,null, null);
        assertThat(recognitionRecipientList.size(), equalTo(50));
    }
    @Test
    public void test_pagination_page_1_size_5_recognition_recipients() {
        List<EntityDTO> recognitionRecipientList = raiseService.getRecipientsForRaise(TEST_ACTIVITY_ID,
                false, null, 5, 1);
        assertThat(recognitionRecipientList.size(), equalTo(5));
    }
    @Test
    public void test_pagination_page_5_size_1_recognition_recipients() {
        List<EntityDTO> recognitionRecipientList = raiseService.getRecipientsForRaise(TEST_ACTIVITY_ID,
                false, null, 1, 5);
        assertThat(recognitionRecipientList.size(), equalTo(1));
    }

    // Involved Pax Id testing
    @Test
    public void test_involved_pax_id_happy() {

        List<RaiseDTO> raiseList = raiseService.getRaisesForNominationCurrent(TEST_NOMINATION_PARENT_ID, null, null,
                statusList, null, TestConstants.PAX_DAGARFIN);
        assertThat(raiseList.size(), equalTo(10));
    }
    @Test
    public void test_involved_pax_id_not_valid() {
        PowerMockito.when(paxRepository.exists(anyLong())).thenReturn(Boolean.FALSE);

        try{
            raiseService.getRaisesForNomination(TEST_NOMINATION_PARENT_ID, null, null, statusList, null, TestConstants.PAX_DAGARFIN);
            fail();
        }catch(ErrorMessageException e){
            ErrorMessage error =  e.getErrorMessages().iterator().next();
            assertThat(error.getField(), equalTo(ProjectConstants.INVOLVED_PAX_ID));
            assertThat(error.getCode(), equalTo(NominationConstants.ERROR_PAX_ID_INVALID));
        }


    }

    @Test
    public void create_raise_happy_path() {
        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("PAX_ID", TestConstants.PAX_DAGARFIN);
        user1.put("MANAGER_PAX_ID", MANAGER_PAX_ID);

        Map<String, Object> user2 = new HashMap<>();
        user2.put("PAX_ID", TEST_TO_PAX_ID);
        user2.put("MANAGER_PAX_ID", TEST_TO_PAX_MANAGER_PAX_ID);
        results.add(user2);

        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(TEST_TO_PAX_ID);
        PowerMockito.when(paxCountDao.getDistinctPax(TestConstants.PAX_DAGARFIN,null,paxIdList)).thenReturn(results);

        recognition = new Recognition();
        recognition.setId(284045L);
        recognition.setNominationId(71116L);
        recognition.setReceiverPaxId(7213L);
        recognition.setParentId(284044L);
        recognition.setProgramAwardTierId(TestConstants.ID_1);
        recognition.setAmount(50.00);
        recognition.setViewed("N");
        recognition.setStatusTypeCode(StatusTypeCode.APPROVED.toString());

        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);


        List<RaiseDTO> createdRaises = raiseService.createRaises(TEST_NOMINATION_PARENT_ID, testRaises);
        assertThat(createdRaises.size(), equalTo(1));
        assertThat(createdRaises.get(0).getBudgetId(), equalTo(TEST_BUDGET_ID));
        assertThat(createdRaises.get(0).getAwardAmount(), equalTo(TEST_AWARD_AMOUNT));
        assertNotNull(createdRaises.get(0).getTime());
    }

    @Test
    public void test_null_all_pax() {
        PowerMockito.when(paxCountDao.getDistinctPax(raise.getFromPax().getPaxId(), groupIds, paxIds)).thenReturn(null);
        try {
            raiseService.createRaises(TEST_NOMINATION_PARENT_ID, testRaises);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_null_to_pax() {
        raise.setBudgetId(TEST_BUDGET_ID);
        raise.setAwardAmount(TEST_AWARD_AMOUNT);
        EmployeeDTO fromPax = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        fromPax.setManagerPaxId(TEST_FROM_MANAGER_PAX_ID);
        raise.setFromPax(fromPax);
        EmployeeDTO toPax = new EmployeeDTO();
        toPax.setPaxId(TEST_TO_PAX_ID);
        toPax.setManagerPaxId(TEST_TO_PAX_MANAGER_PAX_ID);
        raise.setToPax(null);
        testRaises.add(raise);

        try {
            raiseService.createRaises(TEST_ACTIVITY_ID, testRaises);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
            assertTrue(e.getMessage().endsWith(NominationConstants.MISSING_RECEIVERS_MSG));
        }
    }

    @Test
    public void test_update_raise_happy_path_approved() {

        raise.setId(284045L);
        raise.setStatus(StatusTypeCode.APPROVED.toString());
        raise.setComment("Test Comment");

        PowerMockito.when(security.getPaxId()).thenReturn(165L);

        List<RaiseDTO> updatedRaises = raiseService.updateRaises(testRaises);

        //Confirm that the non-updatable fields have not changed
        assertThat(updatedRaises.get(0).getId(), is(284045L));
        assertThat(updatedRaises.get(0).getBudgetId(), is(12345L));
        assertThat(updatedRaises.get(0).getAwardAmount(), is(50.0));
        assertThat(updatedRaises.get(0).getApprovalPax().getPaxId(), is(165L));
        assertThat(updatedRaises.get(0).getFromPax().getPaxId(), is(165L));
        assertThat(updatedRaises.get(0).getToPax().getPaxId(), is(165L));


        //Confirm that the updatable fields have been modified
        assertThat(updatedRaises.get(0).getStatus(), is(StatusTypeCode.APPROVED.toString()));
        assertThat(updatedRaises.get(0).getComment(), is("Test Comment"));

    }

    @Test
    public void test_update_raise_approved_extra_fields() {

        // Make raise an existing raise
        raise.setId(284045L);
        raise.setStatus(StatusTypeCode.APPROVED.toString());
        raise.setComment("Test Comment");

        //Set fields that are not required and will actually get ignored
        raise.getToPax().setPaxId(16L);
        raise.getFromPax().setPaxId(16L);
        raise.setAwardAmount(10.0);
        raise.setBudgetId(31245L);

        PowerMockito.when(security.getPaxId()).thenReturn(165L);

        List<RaiseDTO> updatedRaises = raiseService.updateRaises(testRaises);

        //Confirm that the non-updatable fields have not changed
        assertThat(updatedRaises.get(0).getId(), is(284045L));
        assertThat(updatedRaises.get(0).getBudgetId(), is(12345L));
        assertThat(updatedRaises.get(0).getAwardAmount(), is(50.0));
        assertThat(updatedRaises.get(0).getApprovalPax().getPaxId(), is(165L));
        assertThat(updatedRaises.get(0).getFromPax().getPaxId(), is(165L));
        assertThat(updatedRaises.get(0).getToPax().getPaxId(), is(165L));

        //Confirm that the updatable fields have been modified
        assertThat(updatedRaises.get(0).getStatus(), is(StatusTypeCode.APPROVED.toString()));
        assertThat(updatedRaises.get(0).getComment(), is("Test Comment"));

    }

    @Test
    public void test_update_raise_null_id() {

        // Make raise an existing raise
        EmployeeDTO requestApprovalPax = new EmployeeDTO();
        raise.setId(null);
        requestApprovalPax.setPaxId(11L);
        raise.getToPax().setPaxId(165L);
        raise.getFromPax().setPaxId(165L);
        raise.setApprovalPax(requestApprovalPax);

        try {
            raiseService.updateRaises(testRaises);
            fail("No Exception thrown");
        } catch (Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(NominationConstants.ERROR_INVALID_RAISE_ID_MSG));
        }

    }


    @Test
    public void test_update_raise_mismatched_approval_pax() {

        // Make raise an existing raise
        raise.setId(284045L);
        raise.setAwardAmount(10.00);
        raise.getToPax().setPaxId(165L);
        raise.getFromPax().setPaxId(165L);

        PowerMockito.when(security.getPaxId()).thenReturn(16L);

        try {
            raiseService.updateRaises(testRaises);
            fail("No Exception thrown");
        } catch (Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(NominationConstants.ERROR_APPROVAL_PAX_MISMATCH_MSG));
        }

    }

    @Test
    public void test_update_raise_stored_raise_is_already_approved() {

        // Make raise an existing raise
        EmployeeDTO requestApprovalPax = new EmployeeDTO();
        raise.setId(284045L);
        raise.setAwardAmount(10.00);
        requestApprovalPax.setPaxId(11L);
        raise.getToPax().setPaxId(165L);
        raise.getFromPax().setPaxId(165L);
        raise.setApprovalPax(requestApprovalPax);

        recognition.setStatusTypeCode(StatusTypeCode.APPROVED.toString());

        try {
            raiseService.updateRaises(testRaises);
            fail("No Exception thrown");
        } catch (Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(NominationConstants.ERROR_ALREADY_APPROVED_OR_REJECTED_MSG));
        }

    }

    @Test
    public void create_raise_with_approvals_enabled() {
        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("PAX_ID", TestConstants.PAX_DAGARFIN);
        user1.put("MANAGER_PAX_ID", MANAGER_PAX_ID);

        Map<String, Object> user2 = new HashMap<>();
        user2.put("PAX_ID", TEST_TO_PAX_ID);
        user2.put("MANAGER_PAX_ID", TEST_TO_PAX_MANAGER_PAX_ID);
        results.add(user2);

        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(TEST_TO_PAX_ID);
        PowerMockito.when(paxCountDao.getDistinctPax(TestConstants.PAX_DAGARFIN,null,paxIdList)).thenReturn(results);

        recognition = new Recognition();
        recognition.setId(284045L);
        recognition.setNominationId(71116L);
        recognition.setReceiverPaxId(7213L);
        recognition.setParentId(284044L);
        recognition.setProgramAwardTierId(TestConstants.ID_1);
        recognition.setAmount(50.00);
        recognition.setViewed("N");
        recognition.setStatusTypeCode(StatusTypeCode.APPROVED.toString());

        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_RAISE_APPROVAL_ENABLED))
                .thenReturn("true");

        raiseService.createRaises(TEST_NOMINATION_PARENT_ID, testRaises);

        Mockito.verify(alertService, times(1)).addRaiseApprovalAlert(TEST_NOMINATION_PARENT_ID);
    }

    @Test
    public void create_raise_with_approvals_disabled() {
        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("PAX_ID", TestConstants.PAX_DAGARFIN);
        user1.put("MANAGER_PAX_ID", MANAGER_PAX_ID);

        Map<String, Object> user2 = new HashMap<>();
        user2.put("PAX_ID", TEST_TO_PAX_ID);
        user2.put("MANAGER_PAX_ID", TEST_TO_PAX_MANAGER_PAX_ID);
        results.add(user2);

        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(TEST_TO_PAX_ID);
        PowerMockito.when(paxCountDao.getDistinctPax(TestConstants.PAX_DAGARFIN,null,paxIdList)).thenReturn(results);

        recognition = new Recognition();
        recognition.setId(284045L);
        recognition.setNominationId(71116L);
        recognition.setReceiverPaxId(7213L);
        recognition.setParentId(284044L);
        recognition.setProgramAwardTierId(TestConstants.ID_1);
        recognition.setAmount(50.00);
        recognition.setViewed("N");
        recognition.setStatusTypeCode(StatusTypeCode.APPROVED.toString());

        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);

        raiseService.createRaises(TEST_NOMINATION_PARENT_ID, testRaises);

        Mockito.verify(alertService, times(0)).addRaiseApprovalAlert(TEST_NOMINATION_PARENT_ID);
    }

    @Test(expected = ErrorMessageException.class)
    public void create_raise_fail_when_rec_is_not_approved () {

        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("PAX_ID", TestConstants.PAX_DAGARFIN);
        user1.put("MANAGER_PAX_ID", MANAGER_PAX_ID);

        Map<String, Object> user2 = new HashMap<>();
        user2.put("PAX_ID", TEST_TO_PAX_ID);
        user2.put("MANAGER_PAX_ID", TEST_TO_PAX_MANAGER_PAX_ID);
        results.add(user2);

        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(TEST_TO_PAX_ID);
        PowerMockito.when(paxCountDao.getDistinctPax(TestConstants.PAX_DAGARFIN,null,paxIdList)).thenReturn(results);

        recognition = new Recognition();
        recognition.setId(284045L);
        recognition.setNominationId(71116L);
        recognition.setReceiverPaxId(7213L);
        recognition.setParentId(284044L);
        recognition.setProgramAwardTierId(TestConstants.ID_1);
        recognition.setAmount(50.00);
        recognition.setViewed("N");
        recognition.setStatusTypeCode(StatusTypeCode.PENDING.toString());

        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);

        raiseService.createRaises(TEST_NOMINATION_PARENT_ID, testRaises);


    }

    @Test
    public void test_getRecipientsForRaise_delimited_status_list() {
        List<String> statusLists = Arrays.asList(StatusTypeCode.APPROVED.toString(),
                StatusTypeCode.AUTO_APPROVED.toString());

        String delimitedStatusList = StringUtils.formatDelimitedData(statusLists);

        List<EntityDTO> recognitionRecipientList = raiseService.getRecipientsForRaise(
                TEST_ACTIVITY_ID, false, delimitedStatusList, null, null);

        assertTrue(recognitionRecipientList.size() > 0);

        Mockito.verify(raiseDao,times(1)).getListOfPaxOnActivity(TestConstants.PAX_DAGARFIN,TEST_ACTIVITY_ID, false, statusLists);
    }

    @Test
    public void test_getRecipientsForRaise_only_one_status() {
        List<String> statusLists = Arrays.asList(StatusTypeCode.PENDING.toString());

        String delimitedStatusList = StatusTypeCode.PENDING.toString();

        List<EntityDTO> recognitionRecipientList = raiseService.getRecipientsForRaise(
                TEST_ACTIVITY_ID, false, delimitedStatusList, null, null);

        assertTrue(recognitionRecipientList.size() > 0);

        Mockito.verify(raiseDao,times(1)).getListOfPaxOnActivity(TestConstants.PAX_DAGARFIN,TEST_ACTIVITY_ID, false, statusLists);
    }


}

