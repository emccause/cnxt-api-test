package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.security.access.AccessDeniedException;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.ApprovalProcess;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.AuxiliaryNotification;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.NominationMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramCriteria;
import com.maritz.core.jpa.entity.ProgramEcard;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.entity.RecognitionMisc;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.LookupRepository;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.core.jpa.support.util.AclTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.alert.util.AlertUtil;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ApprovalType;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtNominationRepository;
import com.maritz.culturenext.jpa.repository.CnxtRelationshipRepository;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.PaxDistinctDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.services.GroupsService;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.PinnacleDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeInfoDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;
import com.maritz.culturenext.recognition.dao.NominationDetailsDao;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.impl.NominationServiceImpl;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class NominationServiceUnitTest {

    // testing constants
    private static final String PAX_ID_COLUMN_NAME = "PAX_ID";
    private static final String MANAGER_PAX_ID_COLUMN_NAME = "MANAGER_PAX_ID";
    private static final String TEST_PROGRAM_NAME = "Batman's Program";
    private static final String TEST_PAYOUT_TYPE_PARKING_SPOTS = "PARKING_SPOTS";
    private static final String TEST_PAYOUT_TYPE_POINTS = "DPP";
    private static final Integer testAwardAmount = 50;
    private static final Integer testAwardAmount2 = 100;
    private static final Integer testOriginalAmount = 50;
    private static final Integer testOriginalAmount2 = 100;
    private static final Long testPaxId = TestConstants.PAX_DAGARFIN;
    private static final Long testPersonalGroup1 = 551L;
    private static final Long testPersonalGroup2 = 1264L;
    private static final Long testReceiverPaxId = 55555L;
    private static final Long testManagerPaxId = 170L;
    private static final Long testBudgetId = 12345L;
    private static final Long testProgramPTPId = 100L;
    private static final Long testProgramAwardCodeId = 200L;
    private static final Long testEcardId = 300L;
    private static final Long testFileAwardCodeIconId = 400L;

    // mocked objects
    Nomination testNominationEntity;
    NominationRequestDTO testNominationDTO;
    List<Map<String, Object>> testDistinctPax = new ArrayList<>();
    List<Map<String, Object>> testBudgetConfigList = new ArrayList<>();
    List<PaxDistinctDTO> testDistinctPaxDto = new ArrayList<>();
    List<ProgramMisc> programMiscList = new ArrayList<>();
    List<Long> testPaxIds = new ArrayList<>();
    List<Long> testGroupIds = new ArrayList<>();
    List<Long> testMyGroups = new ArrayList<>();
    List<Long> testSimpleAndParentBudgetIds = new ArrayList<>();
    ApprovalProcess testApprovalProcess = new ApprovalProcess();
    List<AwardTypeDTO> awardTypeList = new ArrayList<>();

    // mocked services
    @Mock AlertService alertService;
    @Mock AlertUtil alertUtil;
    @Mock ApplicationEventPublisher applicationEventPublisher;
    @Mock ApprovalService approvalService;
    @Mock BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Mock BudgetInfoDao budgetInfoDao;
    @Mock CnxtRelationshipRepository cnxtRelationshipRepository;
    @Mock CnxtNominationRepository cnxtNominationRepository;
    @Mock ConcentrixDao<Alert> alertDao;
    @Mock ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock ConcentrixDao<ApprovalProcess> approvalProcessDao;
    @Mock ConcentrixDao<ApprovalProcessConfig> approvalProcessConfigDao;
    @Mock ConcentrixDao<AuxiliaryNotification> auxiliaryNotificationDao;
    @Mock ConcentrixDao<Batch> batchDao;
    @Mock ConcentrixDao<BatchEvent> batchEventDao;
    @Mock ConcentrixDao<BudgetMisc> budgetMiscDao;
    @Mock ConcentrixDao<Groups> groupsDao;
    @Mock ConcentrixDao<Nomination> nominationDao;
    @Mock ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock ConcentrixDao<NominationCriteria> nominationCriteriaDao;
    @Mock ConcentrixDao<NominationMisc> nominationMiscDao;
    @Mock ConcentrixDao<Program> programDao;
    @Mock ConcentrixDao<ProgramEcard> programEcardDao;
    @Mock ConcentrixDao<ProgramMisc> programMiscDao;
    @Mock ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Mock ConcentrixDao<ProgramCriteria> programCriteriaDao;
    @Mock ConcentrixDao<Recognition> recognitionDao;
    @Mock ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Mock ConcentrixDao<RecognitionMisc> recognitionMiscDao;
    @Mock ConcentrixDao<Relationship> relationshipDao;
    @Mock ConcentrixDao<SysUser> sysUserDao;
    @Mock ConcentrixDao<FileItem> fileItemDao;
    @Mock ConcentrixDao<Files> filesDao;
    @Mock Environment environment;
    @Mock GroupsService groupsService;
    @Mock LookupRepository lookupRepository;
    @Mock NominationDataBulkSaveDao nominationDataBulkSaveDao;
    @Mock NominationDetailsDao nominationDetailsDao;
    @Mock ParticipantInfoDao participantInfoDao;
    @Mock ParticipantsPppIndexService participantsPppIndexService;
    @Mock PaxCountDao paxCountDao;
    @Mock ProgramEligibilityQueryDao programEligibilityQueryDao;
    @Mock ProgramMiscRepository programMiscRepository;
    @Mock ProxyService proxyService;
    @Mock RecognitionService recognitionService;
    @Mock Security security;
    @Mock TransactionHeaderMiscDao transactionHeaderMiscDao;
    @Mock ProgramService programService;
    @Mock com.maritz.core.jpa.support.findby.FindBy<Lookup> lookupFindBy;

    // mocked findBys
    private FindBy<Alert> alertFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalPending> approvalPendingFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalProcess> approvalProcessFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalProcessConfig> approvalProcessConfigFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<AuxiliaryNotification> auxiliaryNotificationFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<BudgetMisc> budgetMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<FileItem> fileItemFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Files> filesFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<NewsfeedItem> newsfeedItemFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Nomination> nominationFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<NominationCriteria> nominationCriteriaFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<NominationMisc> nominationMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Program> programFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramAwardTier> programAwardTierFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramCriteria> programCriteriaFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramEcard> programEcardFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> programMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Recognition> recognitionFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<RecognitionCriteria> recognitionCriteriaFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<RecognitionMisc> recognitionMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Relationship> relationshipFindBy = PowerMockito.mock(FindBy.class);
    private com.maritz.core.jpa.support.findby.FindBy<ProgramMisc> programMiscRepositoryFindBy = PowerMockito.mock(com.maritz.core.jpa.support.findby.FindBy.class);

    private FindBy<SysUser> sysUserFindBy = PowerMockito.mock(FindBy.class);

    @Rule ExpectedException thrown = ExpectedException.none();
    @InjectMocks private NominationServiceImpl nominationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        Map<String, Object> budgetTotals = new HashMap<>();
        budgetTotals.put("totalUsed", 733.0);
        budgetTotals.put("totalAmount", 3000.0);
        List<Map<String, Object>> budgetTotalsList = new ArrayList<>();
        budgetTotalsList.add(budgetTotals);
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(anyListOf(Long.class))).thenReturn(budgetTotalsList);

        testNominationDTO = new NominationRequestDTO();
        testNominationDTO.setHeadline("Not null");
        testNominationDTO.setBudgetId(testBudgetId);
        testNominationDTO.setProgramId(TestConstants.ID_1);
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(
                new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(
                new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.setIsPrivate(Boolean.TRUE);
        testNominationDTO.setPayoutType(TEST_PAYOUT_TYPE_POINTS);

        int testPaxCount = 1;
        double testIndividualUsedAmount = 0.00;

        testApprovalProcess.setId(100L);

        Program testProgram = new Program();
        testProgram.setProgramTypeCode("PTP");
        testProgram.setProgramName(TEST_PROGRAM_NAME);

        SysUser testSysUser = new SysUser();
        Map<String, Object> testBudgetConfig = new HashMap<>();
        testBudgetConfig.put("budgetId", "1");
        testBudgetConfig.put("config", "{\"givingMembers\":[{\"paxId\": 8987},{\"paxId\": 12698},"
                + "{\"groupId\": 1534}]}");
        testBudgetConfigList.add(testBudgetConfig);

        Map<String, Object> testBudgetInfo = new HashMap<>();
        testBudgetInfo.put("id", 1);
        testBudgetInfo.put("budgetTypeCode", "SIMPLE");
        testBudgetInfo.put("budgetName", "Dan's test budget");
        testBudgetInfo.put("budgetDisplayName", "Leah's Super Awesome Budget");
        testBudgetInfo.put("budgetDesc", "Initial budget created for testing the new budget structure");
        testBudgetInfo.put("individualGivingLimit", 1000.0);
        testBudgetInfo.put("fromDate", "2015-01-01");
        testBudgetInfo.put("thruDate", "2016-12-31");
        testBudgetInfo.put("statusTypeCode", "ACTIVE");
        testBudgetInfo.put("subProjectId", "551");
        testBudgetInfo.put("budgetConstraint", "HARD_BUDGET");
        testBudgetInfo.put("projectNumber", "S01234");
        testBudgetInfo.put("subProjectNumber", "000001");
        testBudgetInfo.put("createDate", "2015-05-08");
        testBudgetInfo.put("alertAmount", 1500.0);
        testBudgetConfigList.add(testBudgetInfo);

        List<ProgramAwardTier> testProgramAwardTierList = new ArrayList<>();
        ProgramAwardTier testProgramAwardTier = new ProgramAwardTier();
        testProgramAwardTier.setId(2002L);
        testProgramAwardTier.setProgramId(TestConstants.ID_1);
        testProgramAwardTier.setMinAmount(5.0);
        testProgramAwardTier.setMaxAmount(5.0);
        testProgramAwardTier.setIncrement(0.0);
        testProgramAwardTier.setRaisingStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testProgramAwardTierList.add(testProgramAwardTier);

        List<ApprovalProcessConfig> testApprovalProcessConfigList = new ArrayList<>();
        ApprovalProcessConfig testConfig1 = new ApprovalProcessConfig();
        testConfig1.setTargetId(2002L);
        testConfig1.setApprovalLevel(TestConstants.ID_1);
        testConfig1.setApprovalTypeCode(ApprovalType.SUBMITTER_FIRST.toString());
        testApprovalProcessConfigList.add(testConfig1);
        ApprovalProcessConfig testConfig2 = new ApprovalProcessConfig();
        testConfig2.setTargetId(2002L);
        testConfig2.setApprovalLevel(TestConstants.ID_2);
        testConfig2.setApprovalTypeCode(ApprovalType.SUBMITTER_SECOND.toString());
        testApprovalProcessConfigList.add(testConfig2);

        testSimpleAndParentBudgetIds.add(testBudgetId);

        Map<String, Object> distinctPax1 = new HashMap<>();
        distinctPax1.put(PAX_ID_COLUMN_NAME, 165L);
        distinctPax1.put(VfName.SHARE_REC.toString(), ProjectConstants.EMPTY_STRING);
        distinctPax1.put(MANAGER_PAX_ID_COLUMN_NAME, 170L);
        testDistinctPax.add(distinctPax1);
        Map<String, Object> distinctPax2 = new HashMap<>();
        distinctPax2.put(PAX_ID_COLUMN_NAME, 141L);
        distinctPax2.put(VfName.SHARE_REC.toString(), ProjectConstants.EMPTY_STRING);
        distinctPax2.put(MANAGER_PAX_ID_COLUMN_NAME, 170L);
        testDistinctPax.add(distinctPax2);

        PaxDistinctDTO paxDistinct = new PaxDistinctDTO();
        paxDistinct.setPaxId( 165L);
        paxDistinct.setShareRec(ProjectConstants.EMPTY_STRING);
        paxDistinct.setManagerPaxId(170L);
        testDistinctPaxDto.add(paxDistinct);
        paxDistinct = new PaxDistinctDTO();
        paxDistinct.setPaxId( 141L);
        paxDistinct.setShareRec(ProjectConstants.EMPTY_STRING);
        paxDistinct.setManagerPaxId(170L);
        testDistinctPaxDto.add(paxDistinct);

        List<Relationship> relationshipList = new ArrayList<>();
        Relationship testRelationship1 = new Relationship();
        testRelationship1.setPaxId1(141L);
        testRelationship1.setPaxId2(170L);
        testRelationship1.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship1);
        Relationship testRelationship2 = new Relationship();
        testRelationship2.setPaxId1(165L);
        testRelationship2.setPaxId2(170L);
        testRelationship2.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship2);
        Relationship testRelationship3 = new Relationship();
        testRelationship3.setPaxId1(TestConstants.PAX_DAGARFIN);
        testRelationship3.setPaxId2(170L);
        testRelationship3.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship3);
        Relationship testRelationship4 = new Relationship();
        testRelationship4.setPaxId1(170L);
        testRelationship4.setPaxId2(250L);
        testRelationship4.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship4);

        List<Recognition> recognitionList = new ArrayList<>();
        Recognition testRecognition1 = new Recognition();
        testRecognition1.setId(TestConstants.ID_1);
        testRecognition1.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition1.setAmount(50.0);
        testRecognition1.setReceiverPaxId(165L);
        testRecognition1.setStatusTypeCode(StatusTypeCode.PENDING.toString());
        recognitionList.add(testRecognition1);
        Recognition testRecognition2 = new Recognition();
        testRecognition2.setId(TestConstants.ID_2);
        testRecognition2.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition2.setAmount(50.0);
        testRecognition2.setReceiverPaxId(141L);
        testRecognition2.setStatusTypeCode(StatusTypeCode.PENDING.toString());
        recognitionList.add(testRecognition2);

        PowerMockito.when(lookupRepository.findBy()).thenReturn(lookupFindBy);
        PowerMockito.when(lookupFindBy.where(Matchers.anyString())).thenReturn(lookupFindBy);
        PowerMockito.when(lookupFindBy.eq(Matchers.anyString())).thenReturn(lookupFindBy);
        PowerMockito.when(lookupFindBy.and(Matchers.anyString())).thenReturn(lookupFindBy);
        PowerMockito.when(lookupFindBy.findOne()).thenReturn(new Lookup());

        PowerMockito.when(programDao.findById(anyLong())).thenReturn(testProgram);
        PowerMockito.when(programDao.findBy()).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.eq(testNominationDTO.getProgramId())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.findOne()).thenReturn(testProgram);

        PowerMockito.when(programEligibilityQueryDao.validateProgram(testNominationDTO.getProgramId(),
                testPaxId, testPaxIds, testGroupIds)).thenReturn(Boolean.TRUE);

        PowerMockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.where(ProjectConstants.PAX_ID)).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(testPaxId)).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(testManagerPaxId)).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(TestConstants.ID_1)).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.notIn(NominationConstants.STATUSES_THAT_CANT_GIVE_REC))
                .thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.findOne()).thenReturn(testSysUser);

        PowerMockito.when(paxCountDao.getCountDistinctPax(testGroupIds, testPaxIds, false)).thenReturn(testPaxCount);
        PowerMockito.when(paxCountDao.getDistinctPax(eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class)))
                .thenReturn(testDistinctPax);
        PowerMockito.when(paxCountDao.getDistinctPax(eq(testManagerPaxId), anyListOf(Long.class),
                anyListOf(Long.class))).thenReturn(testDistinctPax);

        PowerMockito.when(paxCountDao.getDistinctPaxObject(eq(testPaxId), anyListOf(Long.class),
                anyListOf(Long.class))).thenReturn(testDistinctPaxDto);
        PowerMockito.when(paxCountDao.getDistinctPaxObject(eq(testManagerPaxId), anyListOf(Long.class),
                anyListOf(Long.class))).thenReturn(testDistinctPaxDto);

        PowerMockito.when(budgetInfoDao.getIndividualUsedAmount(testPaxId, testNominationDTO.getBudgetId()))
                .thenReturn(testIndividualUsedAmount);
        PowerMockito.when(budgetInfoDao.getIndividualUsedAmount(testManagerPaxId, testNominationDTO.getBudgetId()))
                .thenReturn(testIndividualUsedAmount);

        List<BudgetEligibilityCacheDTO> budgetEligibilityCacheList =  new ArrayList<>();
        budgetEligibilityCacheList.add(new BudgetEligibilityCacheDTO().setBudgetId(testBudgetId).setProgramId(TestConstants.ID_1));
        PowerMockito.when(budgetEligibilityCacheService.getEligibilityForPax(anyLong())).thenReturn(budgetEligibilityCacheList);

        PowerMockito.when(budgetInfoDao.getEligibleBudgets(anyLong(), anyListOf(Long.class), anyBoolean()))
                .thenReturn(Arrays.asList(mockedRowResponseEligibleBudget()));

        PowerMockito.when(programEcardDao.findBy()).thenReturn(programEcardFindBy);
        PowerMockito.when(programEcardFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programEcardFindBy);
        PowerMockito.when(programEcardFindBy.eq(testNominationDTO.getProgramId())).thenReturn(programEcardFindBy);
        PowerMockito.when(programEcardFindBy.find(ProjectConstants.ECARD_ID, Long.class)).thenReturn(null);

        PowerMockito.when(programCriteriaDao.findBy()).thenReturn(programCriteriaFindBy);
        PowerMockito.when(programCriteriaFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programCriteriaFindBy);
        PowerMockito.when(programCriteriaFindBy.eq(testNominationDTO.getProgramId())).thenReturn(programCriteriaFindBy);
        PowerMockito.when(programCriteriaFindBy.find(ProjectConstants.RECOGNITION_CRITERIA_ID, Long.class))
                .thenReturn(null);

        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.in(Matchers.<String>anyVararg())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyLong())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.active()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.find()).thenReturn(programMiscList);
        PowerMockito.when(programMiscFindBy.findOne()).thenReturn(new ProgramMisc().setMiscData("TRUE"));

        PowerMockito.when(approvalService.fetchApprovalProcessConfigsForAwardTier(anyLong(), anyInt()))
                .thenReturn(testApprovalProcessConfigList);

        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.where(ProjectConstants.PAX_ID_1)).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.in(anyListOf(Long.class))).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.and(ProjectConstants.RELATIONSHIP_TYPE_CODE))
                .thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.eq(RelationshipType.REPORT_TO.toString()))
                .thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.find()).thenReturn(relationshipList);

        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(ProjectConstants.NOMINATION_ID)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(any(Long.class))).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.and(ProjectConstants.PARENT_ID)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.isNull()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);
        PowerMockito.when(recognitionFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);

        PowerMockito.when(approvalProcessDao.findBy()).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.eq(any(Long.class))).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.and(ProjectConstants.TARGET_ID)).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.findOne()).thenReturn(testApprovalProcess);

        PowerMockito.doNothing().when(approvalProcessDao).save(any(ApprovalProcess.class));
        PowerMockito.doNothing().when(approvalProcessDao).update(testApprovalProcess);

        PowerMockito.when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.where(ProjectConstants.APPROVAL_PROCESS_ID))
                .thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(any(Long.class))).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.findOne()).thenReturn(null);

        PowerMockito.doNothing().when(approvalPendingDao).delete(any(ApprovalPending.class));
        PowerMockito.doNothing().when(approvalPendingDao).save(any(ApprovalPending.class));

        PowerMockito.when(cnxtRelationshipRepository.getActiveManagerRelationships(anyListOf(Long.class)))
                .thenReturn(relationshipList);

        PowerMockito.doNothing().when(alertUtil).generateRecognitionAlerts(any(Nomination.class),
                anyListOf(Recognition.class),anyListOf(Long.class),anyLong(),anyListOf(Long.class));

        Map<Long, Long> approvalMap = new HashMap<>();
        approvalMap.put(165L, TestConstants.ID_1);
        approvalMap.put(141L, TestConstants.ID_1);
        PowerMockito.when(approvalService.bulkFetchApprovers(anyListOf(Long.class), anyLong(), anyString(),
                anyLong(), anyLong(),anyBoolean())).thenReturn(approvalMap);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
              Nomination nom = (Nomination) invocation.getArguments()[0];
              nom.setId(TestConstants.ID_1);
              return null;
            }
        }).when(nominationDao).save(any(Nomination.class));

        testNominationEntity = new Nomination();
        testNominationEntity.setId(TestConstants.ID_1);
        testNominationEntity.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        testNominationEntity.setSubmitterPaxId(testPaxId);
        testNominationEntity.setBudgetId(testBudgetId);
        testNominationEntity.setProgramId(TestConstants.ID_1);
        testNominationEntity.setEcardId(testEcardId);
        testNominationEntity.setSubmittalDate(new Date());
        PowerMockito.when(cnxtNominationRepository.findOne(anyLong())).thenReturn(testNominationEntity);

        PowerMockito.when(alertDao.findBy()).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.where(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);

        PowerMockito.when(nominationCriteriaDao.findBy()).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.where(anyString())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.eq(anyLong())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.find(ProjectConstants.RECOGNITION_CRITERIA_ID, Long.class))
                .thenReturn(Arrays.asList(TestConstants.ID_1,TestConstants.ID_2,3L));

        PowerMockito.when(newsfeedItemDao.findBy()).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.where(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(any())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.in(anyListOf(String.class))).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.and(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.findOne(ProjectConstants.ID,Long.class)).thenReturn(TestConstants.ID_1);

        // files
        PowerMockito.when(filesDao.findById(anyLong()))
                .thenReturn(new Files().setFileTypeCode(FileTypes.AWARD_CODE_CERT.name()));
        PowerMockito.when(filesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.in(anyListOf(Long.class))).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.and(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);

        // File Item
        PowerMockito.when(fileItemDao.findBy()).thenReturn(fileItemFindBy);
        PowerMockito.when(fileItemFindBy.where(anyString())).thenReturn(fileItemFindBy);
        PowerMockito.when(fileItemFindBy.eq(anyLong())).thenReturn(fileItemFindBy);
        PowerMockito.when(fileItemFindBy.and(anyString())).thenReturn(fileItemFindBy);
        PowerMockito.when(fileItemFindBy.findOne()).thenReturn(new FileItem());
        PowerMockito.when(fileItemFindBy.find(ProjectConstants.FILES_ID, Long.class))
                .thenReturn(Arrays.asList(TestConstants.ID_1,TestConstants.ID_2,3L));

        //Ecard Id
        PowerMockito.when(nominationDao.findBy()).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.where(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.eq(anyLong())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.and(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.isNull()).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.findOne(anyString(), Matchers.<Class<Long>>any())).thenReturn(300L);

        PowerMockito.when(nominationMiscDao.findBy()).thenReturn(nominationMiscFindBy);
        PowerMockito.when(nominationMiscFindBy.where(ProjectConstants.NOMINATION_ID)).thenReturn(nominationMiscFindBy);
        PowerMockito.when(nominationMiscFindBy.eq(anyLong())).thenReturn(nominationMiscFindBy);
        PowerMockito.when(nominationMiscFindBy.and(ProjectConstants.VF_NAME)).thenReturn(nominationMiscFindBy);
        PowerMockito.when(nominationMiscFindBy.eq(VfName.NEWSFEED_VISIBILITY.name())).thenReturn(nominationMiscFindBy);
        PowerMockito.when(nominationMiscFindBy.findOne(anyString(), Matchers.<Class<String>>any()))
                .thenReturn(ProjectConstants.TRUE);

        //Recognition Criteria
        List<RecognitionCriteria> recCriteriaList = new ArrayList<>();
        RecognitionCriteria recCriteria1 = new RecognitionCriteria();
        recCriteria1.setId(TestConstants.ID_1);
        recCriteria1.setRecognitionCriteriaName("Fun!");
        recCriteriaList.add(recCriteria1);
        RecognitionCriteria recCriteria2 = new RecognitionCriteria();
        recCriteria2.setId(TestConstants.ID_2);
        recCriteria2.setRecognitionCriteriaName("Stuff!");
        recCriteriaList.add(recCriteria2);

        PowerMockito.when(recognitionCriteriaDao.findBy()).thenReturn(recognitionCriteriaFindBy);
        PowerMockito.when(recognitionCriteriaFindBy.where(anyString())).thenReturn(recognitionCriteriaFindBy);
        PowerMockito.when(recognitionCriteriaFindBy.in(anyListOf(Long.class))).thenReturn(recognitionCriteriaFindBy);
        PowerMockito.when(recognitionCriteriaFindBy.find()).thenReturn(recCriteriaList);

        PowerMockito.when(budgetInfoDao.getIndividualUsedAmount(anyLong(), anyLong())).thenReturn(new Double(0));
        PowerMockito.when(programAwardTierDao.findBy()).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.where(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.eq(anyLong())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.and(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.eq(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.find()).thenReturn(generateAwardTierList());

        //Notify Others
        PowerMockito.when(auxiliaryNotificationDao.findBy()).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.where(anyString())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.eq(anyLong())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.find(anyString(), Matchers.<Class<Long>>any())).thenReturn(null);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());

        Map<String, Object> recipientMap2 = new HashMap<>();
        recipientMap2.put(ProjectConstants.SUBJECT_ID, testPaxId);
        recipientMap2.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());

        Map<String, Object> recipientMap3 = new HashMap<>();
        recipientMap3.put(ProjectConstants.SUBJECT_ID, 141L);
        recipientMap3.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());

        Map<String, Object> recipientMap4 = new HashMap<>();
        recipientMap4.put(ProjectConstants.SUBJECT_ID, 165L);
        recipientMap4.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());

        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        recipientMapList.add(recipientMap2);
        recipientMapList.add(recipientMap3);
        recipientMapList.add(recipientMap4);

        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(anyLong(),anyListOf(Long.class),anyListOf(Long.class))).thenReturn(recipientMapList);

        //Budget Misc for SUB_PROJECT_OVERRIDE
        PowerMockito.when(budgetMiscDao.findBy()).thenReturn(budgetMiscFindBy);
        PowerMockito.when(budgetMiscFindBy.where(anyString())).thenReturn(budgetMiscFindBy);
        PowerMockito.when(budgetMiscFindBy.eq(anyLong())).thenReturn(budgetMiscFindBy);
        PowerMockito.when(budgetMiscFindBy.and(anyString())).thenReturn(budgetMiscFindBy);
        PowerMockito.when(budgetMiscFindBy.eq(anyString())).thenReturn(budgetMiscFindBy);
        PowerMockito.when(budgetMiscFindBy.findOne()).thenReturn(null);

        //APPLICATION_DATA setting for vendorprocessing.subproject.override
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE)).thenReturn(null);

        //Program Giver check
        PowerMockito.when(programEligibilityQueryDao.canPaxGiveFromProgram(anyLong(), anyLong())).thenReturn(Boolean.TRUE);

        //PPP Index Flag
        PowerMockito.when(programMiscRepository.findBy()).thenReturn(programMiscRepositoryFindBy);
        PowerMockito.when(programMiscRepositoryFindBy.where(anyString())).thenReturn(programMiscRepositoryFindBy);
        PowerMockito.when(programMiscRepositoryFindBy.eq(anyString())).thenReturn(programMiscRepositoryFindBy);
        PowerMockito.when(programMiscRepositoryFindBy.and(anyString())).thenReturn(programMiscRepositoryFindBy);
        PowerMockito.when(programMiscRepositoryFindBy.findOne(anyString(), Matchers.<Class<String>>any())).thenReturn("TRUE");

        List<ParticipantsPppIndexDTO> partPppxList = Arrays.asList(new ParticipantsPppIndexDTO());

        PowerMockito.when(participantsPppIndexService.getParticipantsPppxByPaxIds(anyString(), anyString())).thenReturn(partPppxList);

        //Groups
        Groups group = new Groups();
        group.setGroupTypeCode(GroupType.CUSTOM.getCode());
        PowerMockito.when(groupsDao.findById(anyLong())).thenReturn(group);

        List<EntityDTO> members = new ArrayList<>();
        EmployeeDTO member1 = new EmployeeDTO();
        member1.setPaxId(165L);
        members.add(member1);
        EmployeeDTO member2 = new EmployeeDTO();
        member2.setPaxId(141L);
        members.add(member2);
        PowerMockito.when(groupsService.getGroupMembers(anyLong(), anyBoolean(), anyInt(), anyInt())).thenReturn(members);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_recognition_pending_happy_path() {
        // Set up test-specific mocks
        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);

        // Set up assertions on the pre-saved Recognition list to ensure the status is PENDING for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in pending status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        // Set up assertions on created ApprovalPending entries
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ApprovalPending approvalPending = (ApprovalPending)args[0];
                assertThat(approvalPending.getApprovalProcessId(), equalTo(100L));
                assertThat(approvalPending.getPaxId(), equalTo(170L));
                assertThat(approvalPending.getNextApproverPaxId(), equalTo(250L));
                return null;
            }
        }).when(approvalPendingDao).save(any(ApprovalPending.class));

        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_recognition_pending_unassigned_approver() {
        // Set up test-specific mocks
        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);
        PowerMockito.when(cnxtRelationshipRepository.getActiveManagerRelationships(anyListOf(Long.class)))
                .thenReturn(new ArrayList<Relationship>());

        // Set up assertions on the pre-saved Recognition list to ensure the status is PENDING for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in pending status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        // Set up assertions on created ApprovalPending entries
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ApprovalPending approvalPending = (ApprovalPending)args[0];
                assertThat(approvalPending.getApprovalProcessId(), equalTo(100L));
                assertThat(approvalPending.getPaxId(), equalTo(350L));
                assertThat(approvalPending.getNextApproverPaxId(), equalTo(350L));
                return null;
            }
        }).when(approvalPendingDao).save(any(ApprovalPending.class));

        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_recognition_auto_approved_happy_path() {
        // Set up test-specific mocks
        List<ApprovalProcessConfig> testApprovalProcessConfigList = new ArrayList<>();
        ApprovalProcessConfig testConfig = new ApprovalProcessConfig();
        testConfig.setTargetId(2002L);
        testConfig.setApprovalLevel(TestConstants.ID_1);
        testConfig.setApprovalTypeCode(ApprovalType.NONE.toString());
        testApprovalProcessConfigList.add(testConfig);

        List<Recognition> recognitionList = new ArrayList<>();
        Recognition testRecognition1 = new Recognition();
        testRecognition1.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition1.setAmount(50.0);
        testRecognition1.setReceiverPaxId(165L);
        testRecognition1.setStatusTypeCode(StatusTypeCode.AUTO_APPROVED.toString());
        recognitionList.add(testRecognition1);
        Recognition testRecognition2 = new Recognition();
        testRecognition2.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition2.setAmount(50.0);
        testRecognition2.setReceiverPaxId(141L);
        testRecognition2.setStatusTypeCode(StatusTypeCode.AUTO_APPROVED.toString());
        recognitionList.add(testRecognition2);

        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);
        PowerMockito.when(approvalProcessConfigFindBy.find()).thenReturn(testApprovalProcessConfigList);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);
        PowerMockito.when(approvalService.bulkFetchApprovers(anyListOf(Long.class), anyLong(), anyString(), anyLong(), anyLong(),anyBoolean()))
                .thenReturn(new HashMap<Long, Long>());

        //Notify Others
        List<Long> notifyOtherPaxIds = Arrays.asList(TestConstants.PAX_MUDDAM, TestConstants.PAX_HAGOPIWL);
        PowerMockito.when(auxiliaryNotificationDao.findBy()).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.where(anyString())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.eq(anyLong())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.find(anyString(), Matchers.<Class<Long>>any())).thenReturn(notifyOtherPaxIds);

        List<EmployeeDTO> notifyOthers = new ArrayList<>();
        EmployeeDTO alex = new EmployeeDTO().setPaxId(TestConstants.PAX_MUDDAM);
        notifyOthers.add(alex);
        EmployeeDTO wayne = new EmployeeDTO().setPaxId(TestConstants.PAX_HAGOPIWL);
        notifyOthers.add(wayne);
        testNominationDTO.setNotifyOthers(notifyOthers);

        // Set up assertions on the pre-saved Recognition list to ensure the status is APPROVED for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(),
                        equalTo(StatusTypeCode.AUTO_APPROVED.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(),
                        equalTo(StatusTypeCode.AUTO_APPROVED.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in approved status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.APPROVED.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));
        Mockito.verify(auxiliaryNotificationDao, times(2))
                .save(any(AuxiliaryNotification.class));

        // Ensure the DAO methods in createPendingApprovals aren't being called
        Mockito.verify(approvalProcessDao, times(0)).findBy();
        Mockito.verify(approvalProcessDao, times(0)).save(any(ApprovalProcess.class));
        Mockito.verify(approvalProcessDao, times(0)).update(any(ApprovalProcess.class));
        Mockito.verify(approvalProcessDao, times(0)).update(testApprovalProcess);
        Mockito.verify(approvalPendingDao, times(0)).delete(any(ApprovalPending.class));
        Mockito.verify(approvalPendingDao, times(0)).save(any(ApprovalPending.class));

        Mockito.verify(approvalService, times(1)).createAutoApprovalHistory(anyListOf(Recognition.class), anyLong());

        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_recognition_auto_approved_approver_as_manager() {
        // Set up test-specific mocks
        List<ApprovalProcessConfig> testApprovalProcessConfigList = new ArrayList<>();
        ApprovalProcessConfig testConfig = new ApprovalProcessConfig();
        testConfig.setTargetId(2002L);
        testConfig.setApprovalLevel(TestConstants.ID_1);
        testConfig.setApprovalTypeCode(ApprovalType.RECEIVER_FIRST.toString());
        testApprovalProcessConfigList.add(testConfig);
        ApprovalProcessConfig testConfig2 = new ApprovalProcessConfig();
        testConfig2.setTargetId(2002L);
        testConfig2.setApprovalLevel(TestConstants.ID_2);
        testConfig2.setApprovalTypeCode(ApprovalType.NONE.toString());
        testApprovalProcessConfigList.add(testConfig2);

        List<Recognition> recognitionList = new ArrayList<>();
        Recognition testRecognition1 = new Recognition();
        testRecognition1.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition1.setAmount(50.0);
        testRecognition1.setReceiverPaxId(165L);
        testRecognition1.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        recognitionList.add(testRecognition1);
        Recognition testRecognition2 = new Recognition();
        testRecognition2.setProgramAwardTierId(TestConstants.ID_1);
        testRecognition2.setAmount(50.0);
        testRecognition2.setReceiverPaxId(141L);
        testRecognition2.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        recognitionList.add(testRecognition2);

        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testManagerPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);
        PowerMockito.when(approvalProcessConfigFindBy.find()).thenReturn(testApprovalProcessConfigList);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);
        PowerMockito.when(sysUserFindBy.eq(testManagerPaxId)).thenReturn(sysUserFindBy);

        PowerMockito.when(approvalService.bulkFetchApprovers(anyListOf(Long.class), anyLong(), anyString(), anyLong(), anyLong(),anyBoolean()))
                .thenReturn(new HashMap<Long, Long>());

        // Set up assertions on the pre-saved Recognition list to ensure the status is PENDING for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(),
                        equalTo(StatusTypeCode.AUTO_APPROVED.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(),
                        equalTo(StatusTypeCode.AUTO_APPROVED.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in approved status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.APPROVED.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        PowerMockito.when(security.getPaxId()).thenReturn(testManagerPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        NominationDetailsDTO response = nominationService.createStandardNomination(testManagerPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        // Ensure the DAO methods in createPendingApprovals aren't being called
        Mockito.verify(approvalProcessDao, times(0)).findBy();
        Mockito.verify(approvalProcessDao, times(0)).save(any(ApprovalProcess.class));
        Mockito.verify(approvalProcessDao, times(0)).update(any(ApprovalProcess.class));
        Mockito.verify(approvalProcessDao, times(0)).update(testApprovalProcess);
        Mockito.verify(approvalPendingDao, times(0)).delete(any(ApprovalPending.class));
        Mockito.verify(approvalPendingDao, times(0)).save(any(ApprovalPending.class));

        Mockito.verify(approvalService, times(1)).createAutoApprovalHistory(anyListOf(Recognition.class), anyLong());

        assertNotNull(response);
    }


    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_recognition_pending_newsfeed_never_display_happy_path() {
        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(5).setOriginalAmount(5));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(5).setOriginalAmount(5));

        testNominationDTO.setProgramId(TestConstants.ID_1);

        testNominationDTO.setComment("great work");
        testNominationDTO.setHeadline("great work");

        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);
        java.util.Date date= new java.util.Date();

        programMiscList.clear();
        ProgramMisc programMisc = new ProgramMisc();
        programMisc.setProgramMiscId(TestConstants.ID_1);
        programMisc.setProgramId(TestConstants.ID_1);
        programMisc.setVfName(NominationConstants.NEWSFEED_VISIBILITY_PROGRAM_MISC_NAME);
        programMisc.setMiscData("NONE");
        programMisc.setMiscDate(date);
        programMiscList.add(programMisc);
        ProgramMisc programMisc1 = new ProgramMisc();
        programMisc1.setProgramMiscId(TestConstants.ID_2);
        programMisc1.setProgramId(TestConstants.ID_1);
        programMisc1.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMisc1.setMiscData("TRUE");
        programMisc1.setMiscDate(date);
        programMiscList.add(programMisc1);
        ProgramMisc programMisc2 = new ProgramMisc();
        programMisc2.setProgramMiscId(3L);
        programMisc2.setProgramId(TestConstants.ID_1);
        programMisc2.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMisc2.setMiscData("TRUE");
        programMisc2.setMiscDate(date);
        programMiscList.add(programMisc2);

        // Set up assertions on the pre-saved Recognition list to ensure the status is PENDING for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in pending status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        // Set up assertions on created ApprovalPending entries
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ApprovalPending approvalPending = (ApprovalPending)args[0];
                assertThat(approvalPending.getApprovalProcessId(), equalTo(100L));
                assertThat(approvalPending.getPaxId(), equalTo(170L));
                assertThat(approvalPending.getNextApproverPaxId(), equalTo(250L));
                return null;
            }
        }).when(approvalPendingDao).save(any(ApprovalPending.class));

        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_happy_path() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));
        assertNotNull(response);
    }


    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_diff_orig_award_amount() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount2));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        assertNotNull(response);
    }


    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_diff_orig_award_amount_2() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(12).setOriginalAmount(5));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(30).setOriginalAmount(15));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));
        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_simple_recognition_for_group() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setGroupId(1534L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, 1534L);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.GROUPS.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(1));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));
        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_simple_recognition_for_personal_group() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setGroupId(testPersonalGroup1)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));

        Groups group = new Groups();
        group.setGroupTypeCode(GroupType.PERSONAL.getCode());
        PowerMockito.when(groupsDao.findById(anyLong())).thenReturn(group);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, 165L);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        Map<String, Object> recipient2Map = new HashMap<>();
        recipient2Map.put(ProjectConstants.SUBJECT_ID, 141L);
        recipient2Map.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        recipientMapList.add(recipient2Map);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(1));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));
        assertNotNull(response);
    }

    @Test
    public void test_createNomination_P2P_with_PPP_and_no_Amount() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L));

        Program programPTP = new Program();
        programPTP.setProgramId(TestConstants.ID_1);
        programPTP.setProgramTypeCode(ProgramTypeEnum.PEER_TO_PEER.getCode());
        programPTP.setProgramName(TEST_PROGRAM_NAME);
        //PPP Index Flag is set by this above
//        PowerMockito.when(programMiscRepositoryFindBy.findOne(anyString(), Matchers.<Class<String>>any())).thenReturn("TRUE");

        List<Recognition> recognitionList = new ArrayList<>();
        Recognition testRecognition1 = new Recognition();
        testRecognition1.setId(TestConstants.ID_1);
        testRecognition1.setReceiverPaxId(165L);
        testRecognition1.setStatusTypeCode(StatusTypeCode.PENDING.toString());
        recognitionList.add(testRecognition1);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);

        testNominationDTO.setProgramId(programPTP.getProgramId());
        testNominationDTO.setBudgetId(null);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, 165L);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(1));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        assertNotNull(response);

    }


    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_simple_recognition_for_invalid_personal_group() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setGroupId(testPersonalGroup2)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));

        Groups group = new Groups();
        group.setGroupTypeCode(GroupType.PERSONAL.getCode());
        PowerMockito.when(groupsDao.findById(anyLong())).thenReturn(group);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, 5961L);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_send_email_notification_approval() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        PowerMockito.when(approvalService.createPendingApprovals(anyListOf(Recognition.class), anyLong(), anyLong()
                , anyInt(), anyLong(), anyLong()))
            .thenReturn(Boolean.TRUE);
        PinnacleDTO pinnacleDto = new PinnacleDTO();
        pinnacleDto.setTestId(123L);
        ProgramFullDTO programFullDto = new ProgramFullDTO();
        programFullDto.setPinnacle(pinnacleDto);
        when(programService.getProgramByID(testNominationDTO.getProgramId(), "en")).thenReturn(programFullDto);
        NominationDetailsDTO response = nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);

        // Ensure our save method (mocked) gets called and the above assertions have occured
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .saveRecognitionList(anyListOf(Recognition.class));
        Mockito.verify(nominationDataBulkSaveDao, times(1))
                .createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        assertNotNull(response);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_no_permission() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        // No advanced recognition permission.
        PowerMockito.when(security.hasPermission(ProjectConstants.ENABLE_ADVANCED_REC_PERMISSION))
                .thenReturn(Boolean.FALSE);

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_no_budget() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setBudgetId(null);
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_no_awardTier() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(null).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_no_awardAmount() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(null).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_no_originalAmount() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(null));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_originalAmount_not_in_awardTier() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(300));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_groups_as_receiver() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setGroupId(141L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_advanced_recognition_fail_duplicated_receivers() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount).setOriginalAmount(testOriginalAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2).setOriginalAmount(testOriginalAmount2));

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = AccessDeniedException.class)
    public void test_createNomination_fail_no_admin_permission() {

        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2));

        PowerMockito.when(security.getPaxId()).thenReturn(testManagerPaxId);
        PowerMockito.when(security.hasPermission(PermissionManagementTypes.PROXY_GIVE_REC_ANYONE.toString()))
                .thenReturn(Boolean.FALSE);

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNomination_fail_no_proxy_permission() {
        setupCreateNominationMocks();

        // Set up test-specific mocks
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_1).setAwardAmount(testAwardAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                .setAwardTierId(TestConstants.ID_2).setAwardAmount(testAwardAmount2));

        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getImpersonatorPaxId()).thenReturn(testManagerPaxId);
        PowerMockito.when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.FALSE);
        PowerMockito.when(security.impersonatorHasPermission(anyString())).thenReturn(Boolean.FALSE);

        nominationService.createStandardNomination(testPaxId, testNominationDTO, Boolean.TRUE);
    }

    private void setupCreateNominationMocks(){
        PowerMockito.when(programEligibilityQueryDao.validateProgram(eq(testNominationDTO.getProgramId()),
                eq(testPaxId), anyListOf(Long.class), anyListOf(Long.class))).thenReturn(Boolean.TRUE);

        // Set up assertions on the pre-saved Recognition list to ensure the status is PENDING for all receivers
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                List<Recognition> recognitionList = (List<Recognition>)args[0];
                assertThat(recognitionList.size(), equalTo(2));
                assertThat(recognitionList.get(0).getReceiverPaxId(), equalTo(165L));
                assertThat(recognitionList.get(0).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                assertThat(recognitionList.get(1).getReceiverPaxId(), equalTo(141L));
                assertThat(recognitionList.get(1).getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).saveRecognitionList(anyListOf(Recognition.class));

        // Verify transactions will be created in pending status
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Nomination nominationProvided = (Nomination)args[0];
                assertThat(nominationProvided.getStatusTypeCode(), equalTo(StatusTypeCode.PENDING.toString()));
                return null;
            }
        }).when(nominationDataBulkSaveDao).createTransactionHeaderAndDiscretionaryEntries(any(Nomination.class));

        // Set up assertions on created ApprovalPending entries
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ApprovalPending approvalPending = (ApprovalPending)args[0];
                assertThat(approvalPending.getApprovalProcessId(), equalTo(100L));
                assertThat(approvalPending.getPaxId(), equalTo(170L));
                assertThat(approvalPending.getNextApproverPaxId(), equalTo(250L));
                return null;
            }
        }).when(approvalPendingDao).save(any(ApprovalPending.class));

        List<PaxDistinctDTO> testDistinctPaxDto1 = new ArrayList<>();
        PaxDistinctDTO paxDistinct = new PaxDistinctDTO();
        paxDistinct.setPaxId( 165L);
        paxDistinct.setShareRec(ProjectConstants.EMPTY_STRING);
        paxDistinct.setManagerPaxId(170L);
        testDistinctPaxDto1.add(paxDistinct);
        List<PaxDistinctDTO> testDistinctPaxDto2 = new ArrayList<>();
        paxDistinct = new PaxDistinctDTO();
        paxDistinct.setPaxId( 141L);
        paxDistinct.setShareRec(ProjectConstants.EMPTY_STRING);
        paxDistinct.setManagerPaxId(170L);
        testDistinctPaxDto2.add(paxDistinct);
        PowerMockito.when(paxCountDao.getDistinctPaxObject(anyLong(),anyListOf(Long.class),anyListOf(Long.class)))
                .thenReturn(testDistinctPaxDto1).thenReturn(testDistinctPaxDto2);

        // Enable advanced recognition permission.
        PowerMockito.when(security.hasPermission(ProjectConstants.ENABLE_ADVANCED_REC_PERMISSION))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);
    }

    private List<ProgramAwardTier> generateAwardTierList() {
        List<ProgramAwardTier> programAwardTierList = new ArrayList<>();

        ProgramAwardTier programAwardTier = new ProgramAwardTier();
        programAwardTier.setId(TestConstants.ID_1);
        programAwardTier.setMinAmount(1D);
        programAwardTier.setMaxAmount(100D);
        programAwardTier.setIncrement(1D);
        programAwardTierList.add(programAwardTier);

        programAwardTier = new ProgramAwardTier();
        programAwardTier.setId(TestConstants.ID_2);
        programAwardTier.setMinAmount(1D);
        programAwardTier.setMaxAmount(100D);
        programAwardTier.setIncrement(1D);
        programAwardTierList.add(programAwardTier);

        return programAwardTierList;
    }

    private AwardCodeRequestDTO generateAwardCodeRequestObject() {

        AwardCodeRequestDTO awardCodeRequestObject = new AwardCodeRequestDTO();
        awardCodeRequestObject.setHeadline("something");
        awardCodeRequestObject.setBudgetId(TestConstants.ID_1);
        awardCodeRequestObject.setProgramId(TestConstants.ID_1);
        awardCodeRequestObject.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        awardCodeRequestObject.getReceivers().add(new GroupMemberAwardDTO().setAwardTierId(TestConstants.ID_1).setAwardAmount(5));

        AwardCodeInfoDTO awardCodeInfoObject = new AwardCodeInfoDTO();
        awardCodeInfoObject.setCertId(TestConstants.ID_1);
        awardCodeInfoObject.setGiveAnonymous(Boolean.FALSE);
        awardCodeInfoObject.setQuantity(10);

        awardCodeRequestObject.setAwardCode(awardCodeInfoObject);

        return awardCodeRequestObject;
    }

    @Test
    public void test_getNominationById_program_peer_to_peer() {
        Program programPTP = new Program();
        programPTP.setProgramId(testProgramPTPId);
        programPTP.setProgramTypeCode(ProgramTypeEnum.PEER_TO_PEER.getCode());
        programPTP.setProgramName(TEST_PROGRAM_NAME);

        testNominationEntity.setProgramId(programPTP.getProgramId());

        PowerMockito.when(cnxtNominationRepository.findOne(anyLong())).thenReturn(testNominationEntity);
        PowerMockito.when(programDao.findById(programPTP.getProgramId())).thenReturn(programPTP);

        // test method
        NominationDetailsDTO nominationDetails =
                nominationService.getNominationById(String.valueOf(testNominationEntity.getId()), testPaxId);

        Mockito.verify(fileItemFindBy,times(0)).find(ProjectConstants.FILES_ID, Long.class);
        assertNotNull(nominationDetails);
        assertEquals(programPTP.getProgramId(), nominationDetails.getProgramId());
        assertEquals(ProgramTypeEnum.PEER_TO_PEER.name(), nominationDetails.getProgramType());
        assertEquals(testEcardId, nominationDetails.getImageId());

        //isPrivate flag should be populated
        assertThat(nominationDetails.getIsPrivate(), is(true));
    }

    @Test
    public void test_getNominationById_program_award_code() {
        Program programAwardCode = new Program();
        programAwardCode.setProgramId(testProgramAwardCodeId);
        programAwardCode.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        programAwardCode.setProgramName(TEST_PROGRAM_NAME);

        testNominationEntity.setProgramId(programAwardCode.getProgramId());

        PowerMockito.when(cnxtNominationRepository.findOne(anyLong())).thenReturn(testNominationEntity);
        PowerMockito.when(programDao.findById(programAwardCode.getProgramId())).thenReturn(programAwardCode);
        PowerMockito.when(fileItemFindBy.find(ProjectConstants.FILES_ID, Long.class))
            .thenReturn(Arrays.asList(testFileAwardCodeIconId));
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.ID, Long.class))
            .thenReturn(testFileAwardCodeIconId);

        //Recognition Misc - Award Code and Cert ID
        RecognitionMisc recognitionMiscAwardCode = new RecognitionMisc();
        recognitionMiscAwardCode.setMiscData("123-456-789");
        RecognitionMisc recognitionMiscCertId = new RecognitionMisc();
        recognitionMiscCertId.setMiscData("3");

        PowerMockito.when(recognitionMiscDao.findBy()).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.where(anyString())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.eq(anyLong())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.and(anyString())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.eq(VfName.AWARD_CODE)).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.findOne()).thenReturn(recognitionMiscAwardCode);
        PowerMockito.when(recognitionMiscFindBy.eq(VfName.CERT_ID)).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.findOne()).thenReturn(recognitionMiscCertId);

        //Program Misc - Claiming Instructions
        ProgramMisc programMiscClaimingInstructions = new ProgramMisc();
        programMiscClaimingInstructions.setMiscData("These are the claiming instructions!");

        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyLong())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.findOne()).thenReturn(programMiscClaimingInstructions);

        //FileName
        PowerMockito.when(filesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(anyLong())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.NAME, String.class)).thenReturn("filename.jpg");


        // test method
        NominationDetailsDTO nominationDetails =
                nominationService.getNominationById(String.valueOf(testNominationEntity.getId()), testPaxId);

        Mockito.verify(fileItemFindBy,times(1)).find(ProjectConstants.FILES_ID, Long.class);
        Mockito.verify(filesFindBy,times(1)).findOne(ProjectConstants.ID, Long.class);
        assertNotNull(nominationDetails);
        assertEquals(programAwardCode.getProgramId(), nominationDetails.getProgramId());
        assertEquals(TEST_PROGRAM_NAME, nominationDetails.getProgramName());
        assertEquals(ProgramTypeEnum.AWARD_CODE.name(), nominationDetails.getProgramType());
        assertEquals(testFileAwardCodeIconId, nominationDetails.getImageId());
    }

    @Test
    public void test_getNominationDetails_missing_nomination_ids() {

        ErrorMessage testError = new ErrorMessage()
            .setCode(NominationConstants.ERROR_MISSING_NOMINATION_ID)
            .setMessage(NominationConstants.ERROR_MISSING_NOMINATION_ID_MSG)
            .setField(ProjectConstants.NOMINATION_IDS);

        // test method
        try {
            nominationService.getNominationDetails(null, 8571L);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
            assertThat(e.getMessage(), is(testError.toString()));
        }
        // test method
        try {
            nominationService.getNominationDetails("", 8571L);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
            assertThat(e.getMessage(), is(testError.toString()));
        }
    }

    @Test
    public void test_getNominationDetails_invalid_nomination_ids() {

        ErrorMessage testError = new ErrorMessage()
            .setCode(NominationConstants.ERROR_INVALID_NOMINATION)
            .setMessage(NominationConstants.ERROR_INVALID_NOMINATION_MSG)
            .setField(ProjectConstants.NOMINATION_IDS);

        // test method
        try {
            nominationService.getNominationDetails("1234,QWERT", 8571L);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
            assertThat(e.getMessage(), is(testError.toString()));
        }
    }

    @Test
    public void test_getNominationDetails_recognition_and_award_code() {

        //NominationDetailsDao
        Map<String,Object> node1 = new HashMap<>();
        node1.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        node1.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.APPROVED.toString());
        node1.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.ID_2);
        node1.put(ProjectConstants.PROGRAM_ID, 3L);
        node1.put(ProjectConstants.PROGRAM_NAME, TEST_PROGRAM_NAME);
        node1.put(ProjectConstants.HEADLINE_COMMENT, "Headline Comment - PTP");
        node1.put(ProjectConstants.COMMENT, "Private Comment - PTP");
        node1.put(ProjectConstants.BUDGET_ID, 4L);
        node1.put(ProjectConstants.PROGRAM_TYPE_CODE, ProgramTypeEnum.PEER_TO_PEER.toString());
        node1.put(ProjectConstants.NEWSFEED_ITEM_ID, 5L);
        node1.put(ProjectConstants.PAYOUT_TYPE, "CASH");

        Map<String,Object> node2 = new HashMap<>();
        node2.put(ProjectConstants.NOMINATION_ID, 6L);
        node2.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.APPROVED.toString());
        node2.put(ProjectConstants.SUBMITTER_PAX_ID, 7L);
        node2.put(ProjectConstants.PROGRAM_ID, 8L);
        node2.put(ProjectConstants.PROGRAM_NAME, TEST_PROGRAM_NAME);
        node2.put(ProjectConstants.HEADLINE_COMMENT, "Headline Comment - Award Code");
        node2.put(ProjectConstants.COMMENT, "Private Comment - Award Code");
        node2.put(ProjectConstants.BUDGET_ID, 9L);
        node2.put(ProjectConstants.PROGRAM_TYPE_CODE, ProgramTypeEnum.AWARD_CODE.toString());
        node2.put(ProjectConstants.NEWSFEED_ITEM_ID, 10L);
        node2.put(ProjectConstants.PAYOUT_TYPE, "CASH");

        List<Map<String, Object>> testNodes = new ArrayList<>();
        testNodes.add(node1);
        testNodes.add(node2);

        List<Long> testNominationIds = Arrays.asList(TestConstants.ID_1, 6L);

        PowerMockito.when(nominationDetailsDao.getNominationCommonInfo(testNominationIds)).thenReturn(testNodes);

        //Recognition Misc - Award Code and Cert ID
        RecognitionMisc recognitionMiscAwardCode = new RecognitionMisc();
        recognitionMiscAwardCode.setMiscData("123-456-789");
        RecognitionMisc recognitionMiscCertId = new RecognitionMisc();
        recognitionMiscCertId.setMiscData("3");

        PowerMockito.when(recognitionMiscDao.findBy()).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.where(anyString())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.eq(anyLong())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.and(anyString())).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.eq(VfName.AWARD_CODE)).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.findOne()).thenReturn(recognitionMiscAwardCode);
        PowerMockito.when(recognitionMiscFindBy.eq(VfName.CERT_ID)).thenReturn(recognitionMiscFindBy);
        PowerMockito.when(recognitionMiscFindBy.findOne()).thenReturn(recognitionMiscCertId);

        //Program Misc - Claiming Instructions
        ProgramMisc programMiscClaimingInstructions = new ProgramMisc();
        programMiscClaimingInstructions.setMiscData("These are the claiming instructions!");

        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyLong())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.findOne()).thenReturn(programMiscClaimingInstructions);

        //FileName
        PowerMockito.when(filesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(anyLong())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.NAME, String.class)).thenReturn("filename.jpg");

        // test method
        List<NominationDetailsDTO> nominationDetailList = nominationService.getNominationDetails("1,6", 8571L);

        assertThat(nominationDetailList.size(), is(2));

        //Test ecardID vs imageID
        assertThat(nominationDetailList.get(0).getImageId(), is(testEcardId));
        assertThat(nominationDetailList.get(1).getImageId(), is(TestConstants.ID_1));

        //AwardCodeDetails should only be populated for award code programs
        assertThat(nominationDetailList.get(0).getAwardCodeDetail(), is(IsNull.nullValue()));
        assertThat(nominationDetailList.get(0).getProgramName(), is(TEST_PROGRAM_NAME));
        assertThat(nominationDetailList.get(1).getAwardCodeDetail().getAwardCode(), is("3"));
        assertThat(nominationDetailList.get(1).getAwardCodeDetail().getCertId(), is(3L));
        assertThat(nominationDetailList.get(1).getProgramName(), is(TEST_PROGRAM_NAME));
        assertThat(nominationDetailList.get(1).getAwardCodeDetail().getClaimingInstructions(),
                is("These are the claiming instructions!"));
        assertThat(nominationDetailList.get(1).getAwardCodeDetail().getFileName(), is("filename.jpg"));
    }

    @Test
    public void test_validation_happy_path() {

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);

        assertThat(errors.size(), equalTo(1));

    }

    @Test
    public void test_null_program() {
        PowerMockito.when(programDao.findBy()).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.eq(testNominationDTO.getProgramId())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.findOne()).thenReturn(null);

        PowerMockito.when(programEligibilityQueryDao.validateProgram(testNominationDTO.getProgramId(), testPaxId,
                                                                     testPaxIds, testGroupIds)).thenReturn(Boolean.FALSE);

        // Eligible budgets not found.
        PowerMockito.when(budgetEligibilityCacheService.getEligibilityForPax(anyLong())).thenReturn(null);
        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertThat(errors.size(), equalTo(1));
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_INVALID_PROGRAM);
    }

    @Test
    public void test_cannot_give_raise_to_self() {
        // Eligible budgets not found.
        PowerMockito.when(budgetEligibilityCacheService.getEligibilityForPax(anyLong())).thenReturn(null);

        List<Long> testPaxIdList = new ArrayList<>(testPaxIds);
        testPaxIdList.add(testPaxId);
        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIdList,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertThat(errors.size(), equalTo(2)); // CANNOT_RECOGNIZE_SELF, INVALID_BUDGET
    }

    @Test
    public void test_null_budget_id() {
        testNominationDTO.setBudgetId(null);
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertThat(errors.size(), equalTo(1));
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT);
    }

    @Test
    public void test_null_award_amount() {
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                                                 .setAwardTierId(TestConstants.ID_1).setAwardAmount(null));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                                                 .setAwardTierId(TestConstants.ID_1).setAwardAmount(null));
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 3);
        StringBuilder errorCodes = new StringBuilder().append(errors.get(0).getCode()).append(errors.get(1).getCode());
        assertTrue(errorCodes.toString().contains(NominationConstants.ERROR_AWARD_AMOUNT_MISSING));
        assertTrue(errorCodes.toString().contains(NominationConstants.ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET));
    }

    @Test
    public void test_null_award_tier_id() {
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                                                 .setAwardTierId(null).setAwardAmount(testAwardAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                                                 .setAwardTierId(null).setAwardAmount(testAwardAmount));
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 2);
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_AWARD_TIER_MISSING);
    }

    @Test
    public void test_invalid_budget_id() {
        testNominationDTO.setBudgetId(0L);
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertThat(errors.size(), equalTo(1));
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT);
    }

    @Test
    public void test_invalid_award_amount() {
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                                                 .setAwardTierId(TestConstants.ID_1).setAwardAmount(0));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                                                 .setAwardTierId(TestConstants.ID_1).setAwardAmount(0));
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 3);
        StringBuilder errorCodes = new StringBuilder().append(errors.get(0).getCode()).append(errors.get(1).getCode());
        assertTrue(errorCodes.toString().contains(NominationConstants.ERROR_AWARD_AMOUNT_MISSING));
        assertTrue(errorCodes.toString().contains(NominationConstants.ERROR_AWARD_AMOUNT_REQUIRED_FOR_BUDGET));
    }

    @Test
    public void test_invalid_award_tier_id() {
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L)
                                                 .setAwardTierId(0L).setAwardAmount(testAwardAmount));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L)
                                                 .setAwardTierId(0L).setAwardAmount(testAwardAmount));
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 2);
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_AWARD_TIER_MISSING);
    }

    @Test
    public void test_null_payout_type() {
        testNominationDTO.setPayoutType(null);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);

        PowerMockito.when(
                programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                        any(List.class),
                        any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                testNominationDTO,
                testPaxIds,
                testGroupIds,
                TableName.RECOGNITION.name(),
                Boolean.TRUE);

        assertEquals(errors.size(), 2);
        assertEquals(errors.get(0).getMessage(), NominationConstants.PAYOUT_TYPE_INVALID_MESSAGE.getMessage());
    }

    @Test
    public void test_valid_nomination() {
        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);
        PowerMockito.when(
            programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                                                      any(List.class),
                                                                      any(List.class))).thenReturn(recipientMapList);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(1,errors.size());
    }

    @Test
    public void test_unable_to_give_from_program() {
        PowerMockito.when(programEligibilityQueryDao.canPaxGiveFromProgram(anyLong(), anyLong())).thenReturn(Boolean.FALSE);
        testAwardCodes(generateAwardCodeRequestObject(), Arrays.asList(NominationConstants.ERROR_INELIGIBLE_SUBMITTER));
    }

    @Test
    public void test_missing_receivers() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.setReceivers(new ArrayList<GroupMemberAwardDTO>());

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.MISSING_RECEIVERS));
    }

    @Test
    public void test_more_than_one_receiver() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getReceivers().add(new GroupMemberAwardDTO().setAwardTierId(TestConstants.ID_1).setAwardAmount(5));

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_MULTIPLE_RECEIVERS_AWARD_CODE));
    }

    @Test
    public void test_missing_budget() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.setBudgetId(null);

        testAwardCodes(awardCodeRequestObject,
                       Arrays.asList(NominationConstants.ERROR_BUDGET_ID_REQUIRED_FOR_AWARD_AMOUNT));
    }

    @Test
    public void test_missing_award_code_object() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.setAwardCode(null);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_MISSING_AWARD_CODE_ATTRIBUTE));
    }

    @Test
    public void test_quantity_zero() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getAwardCode().setQuantity(0);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_INVALID_QUANTITY));
    }

    @Test
    public void test_quantity_null() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getAwardCode().setQuantity(null);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_INVALID_QUANTITY));
    }

    @Test
    public void test_quantity_greater_than_max() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getAwardCode().setQuantity(NominationConstants.AWARD_CODE_MAX_CERTIFICATE_QUANTITY + 1);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_TOO_MANY_CODES));
    }

    @Test
    public void test_cert_id_missing() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getAwardCode().setCertId(null);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_MISSING_CERT));
    }

    @Test
    public void test_cert_id_doesnt_exist() {
        PowerMockito.when(filesDao.findById(anyLong())).thenReturn(null);

        testAwardCodes(generateAwardCodeRequestObject(), Arrays.asList(NominationConstants.ERROR_INVALID_CERT));
    }

    @Test
    public void test_cert_id_invalid_type() {
        PowerMockito.when(filesDao.findById(anyLong()))
            .thenReturn(new Files().setFileTypeCode(FileTypes.JUMBOTRON.name()));

        testAwardCodes(generateAwardCodeRequestObject(), Arrays.asList(NominationConstants.ERROR_INVALID_CERT));
    }

    @Test
    public void test_cert_id_not_linked() {
        PowerMockito.when(fileItemFindBy.findOne()).thenReturn(null);

        testAwardCodes(generateAwardCodeRequestObject(), Arrays.asList(NominationConstants.ERROR_CERT_NOT_CONFIGURED));
    }

    @Test
    public void test_give_anonymous_missing() {
        AwardCodeRequestDTO awardCodeRequestObject = generateAwardCodeRequestObject();
        awardCodeRequestObject.getAwardCode().setGiveAnonymous(null);

        testAwardCodes(awardCodeRequestObject, Arrays.asList(NominationConstants.ERROR_MISSING_GIVE_ANONYMOUS));
    }

    public void testAwardCodes(AwardCodeRequestDTO requestObject, List<String> expectedErrorList) {

        PowerMockito.when(budgetInfoDao.getIndividualUsedAmount(anyLong(), anyLong())).thenReturn(new Double(0));
        PowerMockito.when(programAwardTierDao.findBy()).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.where(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.eq(anyLong())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.and(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.eq(anyString())).thenReturn(programAwardTierFindBy);
        PowerMockito.when(programAwardTierFindBy.find()).thenReturn(generateAwardTierList());

        List<ErrorMessage> errorMessageList =
            nominationService.validateNomination(TestConstants.ID_1,
                                                 requestObject,
                                                 null,
                                                 null,
                                                 NominationConstants.AWARD_CODE_RECOGNITION_PRINT_VALIDATION_TYPE,
                                                 Boolean.TRUE);
        assertFalse("should have errors", errorMessageList.isEmpty());
        assertTrue(String.format("should be %d error(s)",
                                 expectedErrorList.size()), expectedErrorList.size() == errorMessageList.size());

        for (String expectedError : expectedErrorList) {
            assertTrue(expectedError + " should have been thrown.",
                       expectedError.equals(errorMessageList.get(0).getCode()));
        }
    }

    @Test
    public void test_validateNomination_isAdvancedRec_false() {
        nominationService.validateNomination(testPaxId,
                                             testNominationDTO,
                                             testPaxIds,
                                             testGroupIds,
                                             TableName.RECOGNITION.name(),
                                             Boolean.FALSE);
        Mockito.verify(security, times(1)).hasPermission(any(String.class));
    }

    @Test
    public void test_validateNomination_isAdvancedRec_true() {
        nominationService.validateNomination(testPaxId,
                                             testNominationDTO,
                                             testPaxIds,
                                             testGroupIds,
                                             TableName.RECOGNITION.name(),
                                             Boolean.TRUE);
        Mockito.verify(security, times(0)).hasPermission(any(String.class));
    }

    @Test
    public void test_giverec_point_load() {

        PowerMockito.when(programFindBy.findOne()).thenReturn(new Program().setProgramTypeCode(ProgramTypeEnum.POINT_LOAD.getCode()));

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 1);
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION);
    }

    @Test
    public void test_giverec_award_code() {

        PowerMockito.when(programFindBy.findOne()).thenReturn(new Program().setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode()));

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                                                                         testNominationDTO,
                                                                         testPaxIds,
                                                                         testGroupIds,
                                                                         TableName.RECOGNITION.name(),
                                                                         Boolean.TRUE);
        assertEquals(errors.size(), 1);
        assertEquals(errors.get(0).getCode(), NominationConstants.ERROR_PROGRAM_NOT_ELIGIBLE_FOR_RECOGNITION);
    }

    @Test
    public void test_invalid_payout_type() {
        testNominationDTO.setPayoutType(TEST_PAYOUT_TYPE_PARKING_SPOTS);

        Map<String, Object> recipientMap = new HashMap<>();
        recipientMap.put(ProjectConstants.SUBJECT_ID, testReceiverPaxId);
        recipientMap.put(ProjectConstants.SUBJECT_TABLE, AclTypeCode.PAX.name());
        List<Map<String, Object>> recipientMapList = new ArrayList<>();
        recipientMapList.add(recipientMap);

        PowerMockito.when(
                        programEligibilityQueryDao.getEligibleReceiversForProgram(any(Long.class),
                                        any(List.class),
                                        any(List.class))).thenReturn(recipientMapList);

        PowerMockito.when(lookupFindBy.findOne()).thenReturn(null);

        List<ErrorMessage> errors = nominationService.validateNomination(testPaxId,
                        testNominationDTO,
                        testPaxIds,
                        testGroupIds,
                        TableName.RECOGNITION.name(),
                        Boolean.TRUE);

        assertEquals(2, errors.size());
        assertEquals(errors.get(0).getMessage(), NominationConstants.PAYOUT_TYPE_INVALID_MESSAGE.getMessage());
    }

    private Map<String, Object> mockedRowResponseEligibleBudget() {
        Map<String, Object> rowData = new HashMap<>();
        rowData.put(ProjectConstants.ID, testBudgetId);
        rowData.put(ProjectConstants.FROM_DATE,new Date());
        rowData.put(ProjectConstants.THRU_DATE,null);
        rowData.put(ProjectConstants.STATUS_TYPE_CODE,StatusTypeCode.ACTIVE.name());
        rowData.put(ProjectConstants.TOTAL_AMOUNT, 1000d);
        rowData.put(ProjectConstants.TOTAL_USED,10d);
        rowData.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT,200d);

        return rowData;
    }

    @Test(expected = ErrorMessageException.class)
    public void test_create_short_form_nomination_happy_path() {
        PowerMockito.when(security.getPaxId()).thenReturn(testPaxId);
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        nominationService.createStandardNomination(testPaxId, testNominationDTO, true);

        ArgumentCaptor<Nomination> nominationCaptor = ArgumentCaptor.forClass(Nomination.class);
        verify(nominationDao, times(1)).save(nominationCaptor.capture());
        Nomination savedNomination = nominationCaptor.getValue();
        assertThat(savedNomination.getNominationTypeCode(), is(NominationConstants.RECOGNITION_NOMINATION_TYPE));
        assertThat(savedNomination.getProgramId(), is(testNominationDTO.getProgramId()));
        assertThat(savedNomination.getSubmitterPaxId(), is(testPaxId));
        assertThat(savedNomination.getEcardId(), is(testNominationDTO.getImageId()));
        assertThat(savedNomination.getHeadlineComment(), is(testNominationDTO.getHeadline()));
        assertThat(savedNomination.getBatchId(), is(testNominationDTO.getBatchId()));
        assertThat(savedNomination.getComment(), is(testNominationDTO.getComment()));
        assertThat(savedNomination.getBudgetId(), is(testNominationDTO.getBudgetId()));
        assertThat(savedNomination.getSubmittalDate(), is(notNullValue()));
        assertThat(savedNomination.getProxyPaxId(), is(nullValue()));
        assertThat(savedNomination.getStatusTypeCode(), is(StatusTypeCode.PENDING.name()));
    }

}
