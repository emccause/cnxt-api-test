package com.maritz.culturenext.recognition.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.culturenext.batch.service.BatchService;
import com.maritz.culturenext.batch.util.TemplateUtil;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.RecognitionBulkUploadDao;
import com.maritz.culturenext.recognition.services.AdvancedRecognitionThreadService;
import com.maritz.culturenext.recognition.services.impl.RecognitionBulkUploadServiceImpl;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.util.CsvFileUtil;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({CsvFileUtil.class, TemplateUtil.class})
public class RecognitionBulkUploadServiceTest {
    @InjectMocks RecognitionBulkUploadServiceImpl recognitionBulkUploadService;
    @Rule ExpectedException thrown = ExpectedException.none();

    @Mock AdvancedRecognitionThreadService advancedRecognitionThreadService;
    @Mock BatchService batchService;
    @Mock BudgetInfoDao budgetInfoDao;
    @Mock ConcentrixDao<Batch> batchDao;
    @Mock ConcentrixDao<BatchEvent> batchEventDao;
    @Mock ConcentrixDao<BatchFile> batchFileDao;
    @Mock ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;
    @Mock FindBy<Batch> batchFindBy;
    @Mock FindBy<BatchFile> batchFileFindBy;
    @Mock FindBy<StageAdvancedRec> stageAdvancedRecFindBy;
    @Mock Future<Boolean> future;
    @Mock RecognitionBulkUploadDao recognitionBulkUploadDao;
    
    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        
        Batch batch = new Batch();
        batch.setBatchId(1L);
        
        PowerMockito.when(batchDao.findBy()).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.where(Matchers.anyString())).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.eq(Matchers.anyLong())).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.and(Matchers.anyString())).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.eq(Matchers.anyString())).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.asc(Matchers.anyString())).thenReturn(batchFindBy);
        PowerMockito.when(batchFindBy.findOne()).thenReturn(batch);
        
        PowerMockito.when(advancedRecognitionThreadService.validateNominationAsync(Matchers.anyLong(), Matchers.anyLong())).thenReturn(future);
        
        PowerMockito.when(future.get()).thenReturn(Boolean.FALSE);
        
        Map<String, Object> paxBudgetDetailsMap = new HashMap<>();
        paxBudgetDetailsMap.put(ProjectConstants.CONTROL_NUM, TestConstants.USER_MUDDAM);
        paxBudgetDetailsMap.put(ProjectConstants.PAX_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        paxBudgetDetailsMap.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, null);
        
        Map<String, Object> paxBudgetDetailsMap2 = new HashMap<>();
        paxBudgetDetailsMap2.put(ProjectConstants.CONTROL_NUM, TestConstants.USER_MUDDAM);
        paxBudgetDetailsMap2.put(ProjectConstants.PAX_ID, 1L);
        paxBudgetDetailsMap2.put(ProjectConstants.BUDGET_ID, 2L);
        paxBudgetDetailsMap2.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        paxBudgetDetailsMap2.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1D);
        
        List<Map<String, Object>> paxBudgetDetailsMapList = new ArrayList<>();
        paxBudgetDetailsMapList.add(paxBudgetDetailsMap);
        paxBudgetDetailsMapList.add(paxBudgetDetailsMap2);
        PowerMockito.when(recognitionBulkUploadDao.getPaxBudgetUsageDetails(Matchers.anyLong())).thenReturn(paxBudgetDetailsMapList);
        
        Map<String, Object> budgetDetailsMap = new HashMap<>();
        budgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        budgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        budgetDetailsMap.put(ProjectConstants.BUDGET_TOTAL, 1D);
        
        List<Map<String, Object>> budgetDetailsMapList = new ArrayList<>();
        budgetDetailsMapList.add(budgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getBudgetUsageDetails(Matchers.anyLong())).thenReturn(budgetDetailsMapList);
        
        PowerMockito.when(budgetInfoDao.getIndividualUsedAmount(Matchers.anyLong(), Matchers.anyLong())).thenReturn(0D);
        
        PowerMockito.when(stageAdvancedRecDao.findBy()).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.where(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.eq(Matchers.anyLong())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.and(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.eq(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        
        PowerMockito.when(stageAdvancedRecFindBy.desc(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.findOne(Matchers.anyString(), Matchers.<Class<Long>>any())).thenReturn(1L);
        
        StageAdvancedRec stageAdvancedRec = new StageAdvancedRec();
        stageAdvancedRec.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec.setBudgetId(1L);
        
        List<StageAdvancedRec> stageAdvancedRecList = new ArrayList<>();
        stageAdvancedRecList.add(stageAdvancedRec);
        
        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(stageAdvancedRecList);
        
        List<Map<String, Object>> fileData = new ArrayList<>();
        
        PowerMockito.when(recognitionBulkUploadDao.getStageDataByBatchId(Matchers.anyLong())).thenReturn(fileData);
        
        PowerMockito.mockStatic(CsvFileUtil.class, TemplateUtil.class);
        PowerMockito.when(TemplateUtil.createStageAdvanceRecCsvColumnProps()).thenCallRealMethod();
        PowerMockito.when(CsvFileUtil.buildCSVFileFromMap(Matchers.<List<Map<String, Object>>>any(), Matchers.<List<CsvColumnPropertiesDTO>>any())).thenReturn("Hello World!");
        
        BatchFile batchFile = new BatchFile();
        batchFile.setBatchId(1L);
        PowerMockito.when(batchFileDao.findBy()).thenReturn(batchFileFindBy);
        PowerMockito.when(batchFileFindBy.where(Matchers.anyString())).thenReturn(batchFileFindBy);
        PowerMockito.when(batchFileFindBy.eq(Matchers.anyLong())).thenReturn(batchFileFindBy);
        PowerMockito.when(batchFileFindBy.findOne()).thenReturn(batchFile);
        
    }
    
    @Test
    public void test_batchJob_happy_path() {
        // Make sure things still work
        
        recognitionBulkUploadService.runBatchJob();
        
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(1)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
    }
    
    @Test
    public void test_batchJob_error_path_future() throws Exception {
        // Error returned from one of the async processes

        PowerMockito.when(future.get()).thenReturn(Boolean.TRUE);
        
        recognitionBulkUploadService.runBatchJob();
        
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
    }
    
    @Test
    public void test_batchJob_error_path_validation() throws Exception {
        // async process is fine, but we go over budget on the file
        
        List<Map<String, Object>> paxBudgetDetailsMapList = new ArrayList<>();
        Map<String, Object> paxBudgetDetailsMap = new HashMap<>();
        paxBudgetDetailsMap.put(ProjectConstants.CONTROL_NUM, TestConstants.USER_MUDDAM);
        paxBudgetDetailsMap.put(ProjectConstants.PAX_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 2D);
        paxBudgetDetailsMap.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1D);
        paxBudgetDetailsMapList.add(paxBudgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getPaxBudgetUsageDetails(Matchers.anyLong())).thenReturn(paxBudgetDetailsMapList);
        
        List<Map<String, Object>> budgetDetailsMapList = new ArrayList<>();
        Map<String, Object> budgetDetailsMap = new HashMap<>();
        budgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        budgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 2D);
        budgetDetailsMap.put(ProjectConstants.BUDGET_TOTAL, 1D);
        budgetDetailsMapList.add(budgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getBudgetUsageDetails(Matchers.anyLong())).thenReturn(budgetDetailsMapList);

        StageAdvancedRec stageAdvancedRec = new StageAdvancedRec();
        stageAdvancedRec.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec.setBudgetId(1L);
        
        StageAdvancedRec stageAdvancedRec2 = new StageAdvancedRec();
        stageAdvancedRec2.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec2.setBudgetId(1L);
        stageAdvancedRec2.setErrorMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT_MSG);
        List<StageAdvancedRec> stageAdvancedRecList = new ArrayList<>();
        stageAdvancedRecList.add(stageAdvancedRec);
        stageAdvancedRecList.add(stageAdvancedRec2);
        
        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(stageAdvancedRecList);
        
        recognitionBulkUploadService.runBatchJob();
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
    }
    
    @Test
    public void test_batchJob_error_path_both() throws Exception {
        // Async process returns errors AND we go over budget on the file 

        PowerMockito.when(future.get()).thenReturn(Boolean.TRUE);
        
        List<Map<String, Object>> paxBudgetDetailsMapList = new ArrayList<>();
        Map<String, Object> paxBudgetDetailsMap = new HashMap<>();
        paxBudgetDetailsMap.put(ProjectConstants.CONTROL_NUM, TestConstants.USER_MUDDAM);
        paxBudgetDetailsMap.put(ProjectConstants.PAX_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 2D);
        paxBudgetDetailsMap.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1D);
        paxBudgetDetailsMapList.add(paxBudgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getPaxBudgetUsageDetails(Matchers.anyLong())).thenReturn(paxBudgetDetailsMapList);
        
        List<Map<String, Object>> budgetDetailsMapList = new ArrayList<>();
        Map<String, Object> budgetDetailsMap = new HashMap<>();
        budgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        budgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, 2D);
        budgetDetailsMap.put(ProjectConstants.BUDGET_TOTAL, 1D);
        budgetDetailsMapList.add(budgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getBudgetUsageDetails(Matchers.anyLong())).thenReturn(budgetDetailsMapList);

        StageAdvancedRec stageAdvancedRec = new StageAdvancedRec();
        stageAdvancedRec.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec.setBudgetId(1L);
        
        StageAdvancedRec stageAdvancedRec2 = new StageAdvancedRec();
        stageAdvancedRec2.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec2.setBudgetId(1L);
        stageAdvancedRec2.setErrorMessage(NominationConstants.ERROR_AWARD_AMOUNT_HIGHER_THAN_LIMIT_MSG);
        List<StageAdvancedRec> stageAdvancedRecList = new ArrayList<>();
        stageAdvancedRecList.add(stageAdvancedRec);
        stageAdvancedRecList.add(stageAdvancedRec2);
        
        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(stageAdvancedRecList);
        
        recognitionBulkUploadService.runBatchJob();
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
    }
    
    @Test
    public void test_batchJob_fast_exit() {
        // No batch found = don't kick off anything
        
        PowerMockito.when(batchFindBy.findOne()).thenReturn(null);
        recognitionBulkUploadService.runBatchJob();
        
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).validateNominationAsync(Matchers.anyLong(), Matchers.anyLong());
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
    }
    
    @Test
    public void test_batchJob_future_error() throws Exception {
        // Make sure we handle unexpected errors from the async processes appropriately
        
        PowerMockito.when(future.get()).thenThrow(new InterruptedException());
        recognitionBulkUploadService.runBatchJob();
        
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
        
    }
    
    @Test
    public void test_code_coverage() throws Exception {
        // combined test - mixed results from async process and file process where we don't give points on the file
        
        PowerMockito.when(stageAdvancedRecFindBy.findOne(Matchers.anyString(), Matchers.<Class<Long>>any())).thenReturn(3L); // 3 results
        PowerMockito.when(future.get()).thenReturn(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);

        List<Map<String, Object>> paxBudgetDetailsMapList = new ArrayList<>();
        Map<String, Object> paxBudgetDetailsMap = new HashMap<>();
        paxBudgetDetailsMap.put(ProjectConstants.CONTROL_NUM, TestConstants.USER_MUDDAM);
        paxBudgetDetailsMap.put(ProjectConstants.PAX_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        paxBudgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, null);
        paxBudgetDetailsMap.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1D);
        paxBudgetDetailsMapList.add(paxBudgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getPaxBudgetUsageDetails(Matchers.anyLong())).thenReturn(paxBudgetDetailsMapList);
        
        List<Map<String, Object>> budgetDetailsMapList = new ArrayList<>();
        Map<String, Object> budgetDetailsMap = new HashMap<>();
        budgetDetailsMap.put(ProjectConstants.BUDGET_ID, 1L);
        budgetDetailsMap.put(ProjectConstants.TOTAL_AMOUNT, null);
        budgetDetailsMap.put(ProjectConstants.BUDGET_TOTAL, 1D);
        budgetDetailsMapList.add(budgetDetailsMap);
        PowerMockito.when(recognitionBulkUploadDao.getBudgetUsageDetails(Matchers.anyLong())).thenReturn(budgetDetailsMapList);
        
        recognitionBulkUploadService.runBatchJob();
        
        Mockito.verify(advancedRecognitionThreadService, Mockito.times(0)).createNominationAsync(Matchers.anyLong(), Matchers.anyLong());
        
    }
}
