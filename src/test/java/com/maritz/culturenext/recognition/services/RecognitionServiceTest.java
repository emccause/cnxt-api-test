package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.reflect.internal.WhiteboxImpl;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.nomination.dao.NominationDao;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.TransactionDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.RecognitionDetailsDao;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.impl.RecognitionServiceImpl;
import com.maritz.culturenext.transaction.dao.TransactionDao;
import com.maritz.culturenext.util.PaxUtil;
import com.maritz.test.AbstractMockTest;

public class RecognitionServiceTest extends AbstractMockTest {

    @InjectMocks private RecognitionServiceImpl recognitionService;
    @Mock private ApprovalService approvalService;
    @Mock private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Mock private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Mock private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock private ConcentrixDao<Budget> budgetDao;
    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private NominationDao nominationsDao;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxUtil paxUtil;
    @Mock private RaiseService raiseService;
    @Mock private RecognitionDetailsDao recognitionDetailsDao;
    @Mock private Security security;
    @Mock private TransactionDao transactionDao;

    private FindBy<Recognition> recognitionFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalPending> approvalPendingFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Program> programFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalHistory> approvalHistoryFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<NewsfeedItem> newsfeedItemFindBy = PowerMockito.mock(FindBy.class);

    private Long testNominationId = 100L;
    private Long testSubmitterManagerPaxId = 847L;
    private Long testManagerPaxId = 170L;
    private String budgetName = "Some Name";
    private Long programId = 23L;
    private String programName = "Some other name";
    private String testComment = "Test comment";
    private String testNominationComment = "Nomination comment";
    private String testHeadlineComment = "Headline comment";
    private Double testRecognitionAmount = 10d;
    private List<Long> listOfRecognitionIds = new ArrayList<>(Arrays.asList(TestConstants.ID_1, TestConstants.ID_2, 3l));
    private List<Long> recognitionRecipients = new ArrayList<>();

    @Before
    public void setup() {
        List<Nomination> testNominationList = new ArrayList<>();
        Nomination testNomination = new Nomination();
        testNomination.setId(testNominationId);
        testNomination.setSubmitterPaxId(TestConstants.PAX_DAGARFIN);
        testNomination.setBudgetId(TestConstants.ID_1);
        testNomination.setSubmittalDate(new Date());
        testNomination.setProgramId(programId);
        testNomination.setComment(testNominationComment);
        testNomination.setHeadlineComment(testHeadlineComment);
        testNominationList.add(testNomination);

        List<Recognition> testRecognitionList = new ArrayList<>();
        Recognition testRecognition1 = new Recognition();
        testRecognition1.setId(TestConstants.ID_1);
        testRecognition1.setReceiverPaxId(141L);
        testRecognition1.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        testRecognition1.setNominationId(testNominationId);
        testRecognition1.setComment(testComment + 1);
        testRecognition1.setAmount(testRecognitionAmount);
        testRecognitionList.add(testRecognition1);
        Recognition testRecognition2 = new Recognition();
        testRecognition2.setId(TestConstants.ID_2);
        testRecognition2.setReceiverPaxId(165L);
        testRecognition2.setStatusTypeCode(StatusTypeCode.PENDING.toString());
        testRecognition2.setNominationId(testNominationId);
        testRecognition2.setComment(testComment + 2);
        testRecognition2.setAmount(testRecognitionAmount);
        testRecognitionList.add(testRecognition2);
        Recognition testRecognition3 = new Recognition();
        testRecognition3.setId(3L);
        testRecognition3.setReceiverPaxId(125L);
        testRecognition3.setStatusTypeCode(StatusTypeCode.PENDING.toString());
        testRecognition3.setNominationId(testNominationId);
        testRecognition3.setComment(testComment + 3);
        testRecognition3.setAmount(testRecognitionAmount);
        testRecognitionList.add(testRecognition3);

        List<ApprovalPending> testApprovalPendingList = new ArrayList<>();
        ApprovalPending testApprovalPending = new ApprovalPending();
        testApprovalPending.setId(1000L);
        testApprovalPending.setPaxId(testManagerPaxId);
        testApprovalPending.setTargetId(TestConstants.ID_2);
        testApprovalPending.setTargetTable(TableName.RECOGNITION.name());
        testApprovalPendingList.add(testApprovalPending);

        List<EntityDTO> testParticipantInfoList = new ArrayList<>();
        EmployeeDTO testEmployee1 = new EmployeeDTO();
        testEmployee1.setPaxId(TestConstants.PAX_DAGARFIN);
        testEmployee1.setManagerPaxId(testSubmitterManagerPaxId);
        testParticipantInfoList.add(testEmployee1);
        EmployeeDTO testEmployee2 = new EmployeeDTO();
        testEmployee2.setPaxId(141L);
        testEmployee2.setManagerPaxId(1074L);
        testEmployee2.setFirstName("Gomez");
        testEmployee2.setLastName("Akdams");
        testParticipantInfoList.add(testEmployee2);
        EmployeeDTO testEmployee3 = new EmployeeDTO();
        testEmployee3.setPaxId(165L);
        testEmployee3.setManagerPaxId(969L);
        testEmployee3.setFirstName("Gomez");
        testEmployee3.setLastName("Adams");
        testParticipantInfoList.add(testEmployee3);
        EmployeeDTO testEmployee4 = new EmployeeDTO();
        testEmployee4.setPaxId(testManagerPaxId);
        testEmployee4.setManagerPaxId(171L);
        testParticipantInfoList.add(testEmployee4);
        EmployeeDTO testEmployee5 = new EmployeeDTO();
        testEmployee5.setPaxId(125L);
        testEmployee5.setManagerPaxId(969L);
        testEmployee5.setFirstName("Wayne");
        testEmployee5.setLastName("Rogers");
        testParticipantInfoList.add(testEmployee5);

        Map<Long, EmployeeDTO> employeeDTOMap = new HashMap<>();
        employeeDTOMap.put(TestConstants.PAX_DAGARFIN, testEmployee1);
        employeeDTOMap.put(141l, testEmployee2);
        employeeDTOMap.put(165l, testEmployee3);
        employeeDTOMap.put(125l, testEmployee5);
        employeeDTOMap.put(testManagerPaxId, testEmployee4);

        List<Budget> testBudgetList = new ArrayList<>();
        Budget budget = new Budget();
        budget.setId(TestConstants.ID_1);
        budget.setBudgetName(budgetName);
        testBudgetList.add(budget);

        List<Program> testProgramList = new ArrayList<>();
        Program program = new Program();
        program.setProgramId(programId);
        program.setProgramName(programName);
        testProgramList.add(program);

        recognitionRecipients = new ArrayList<>();
        recognitionRecipients.add(141L);
        recognitionRecipients.add(165L);
        recognitionRecipients.add(125L);

        PowerMockito.when(cnxtRecognitionRepository.getRecognitionCountByRecipient(any(Long.class))).thenReturn(5);


        PowerMockito.when(nominationDao.findById(testNominationId)).thenReturn(testNomination);

        PowerMockito.when(nominationDao.findById(any(List.class))).thenReturn(testNominationList);

        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(ProjectConstants.NOMINATION_ID)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(testNominationId)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.ne(StatusTypeCode.REVERSED.name())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(testRecognitionList);

        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(ProjectConstants.ID)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.in(any(List.class))).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(StatusTypeCode.APPROVED.toString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(testRecognitionList);

        PowerMockito.when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.where(ProjectConstants.TARGET_ID)).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.and(ProjectConstants.TARGET_TABLE)).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.in(any(List.class))).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(testApprovalPendingList);

        PowerMockito.when(budgetDao.findById(any(List.class))).thenReturn(testBudgetList);
        PowerMockito.when(budgetDao.findById(TestConstants.ID_1)).thenReturn(budget);

        PowerMockito.when(programDao.findBy()).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.in(any(List.class))).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.find()).thenReturn(testProgramList);
        PowerMockito.when(programDao.findById(anyLong())).thenReturn(program);

        PowerMockito.when(approvalHistoryDao.findBy()).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.where(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.and(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.eq(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.in(anyList())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.find()).thenReturn(null);

        PowerMockito.when(newsfeedItemDao.findBy()).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.where(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(anyLong())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_DAGARFIN);

        PowerMockito.when(participantInfoDao.getInfo(any(List.class), any(List.class)))
                .thenReturn(testParticipantInfoList);

        PowerMockito.when(paxUtil.getPaxIdEmployeeDTOMap(any(List.class))).thenReturn(employeeDTOMap);

        List<Map<String, Object>> resultList = new ArrayList<>();

        Map<String, Object> result = new HashMap<>();
        // Least required fields possible, this is only for code coverage and fields that won't ever be null.
        result.put(ProjectConstants.TOTAL_RESULTS_KEY, 1); // one result, so this needs to be populated
        result.put(ProjectConstants.LAST_APPROVAL_DATE, new Date()); // not populated in other tests

        resultList.add(result);

        PowerMockito.when(
                recognitionDetailsDao.getRecognitionsForNomination(anyLong(), anyLong(), anyBoolean(), anyString(), anyInt(), anyInt())
            ).thenReturn(resultList);
    }

    @Test
    public void test_getRecognitionDetails_happy_path() {
        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);

        assertThat(response.size(), equalTo(3));
        assertThat(response.get(0).getId(), equalTo(TestConstants.ID_2));
        assertThat(response.get(0).getApprovalPax().getPaxId(), equalTo(testManagerPaxId));
        assertThat(response.get(0).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(0).getToPax().getPaxId(), equalTo(165L));
        assertThat(response.get(0).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(0).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(0).getToPax().getManagerPaxId(), equalTo(969L));
        assertEquals(response.get(0).getComment(), testComment+2);
        assertEquals(response.get(0).getNominationComment(), testNominationComment);
        assertEquals(response.get(0).getHeadlineComment(), testHeadlineComment);
        assertEquals(response.get(0).getApprovalTimestamp(), approvalDate);
        assertEquals(response.get(0).getBudgetName(), budgetName);
        assertThat(response.get(1).getId(), equalTo(TestConstants.ID_1));
        assertNull(response.get(1).getApprovalPax());
        assertThat(response.get(1).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(1).getToPax().getPaxId(), equalTo(141L));
        assertThat(response.get(1).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(1).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(1).getToPax().getManagerPaxId(), equalTo(1074L));
        assertEquals(response.get(1).getComment(), testComment+1);
        assertEquals(response.get(1).getNominationComment(), testNominationComment);
        assertEquals(response.get(1).getHeadlineComment(), testHeadlineComment);
        assertEquals(response.get(1).getApprovalTimestamp(), approvalDate);
        assertEquals(response.get(1).getBudgetName(), budgetName);
        assertThat(response.get(2).getId(), equalTo(3L));
        assertNull(response.get(2).getApprovalPax());
        assertThat(response.get(2).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(2).getToPax().getPaxId(), equalTo(125L));
        assertThat(response.get(2).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(2).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(2).getToPax().getManagerPaxId(), equalTo(969L));
        assertEquals(response.get(2).getComment(), testComment+3);
        assertEquals(response.get(2).getNominationComment(), testNominationComment);
        assertEquals(response.get(2).getHeadlineComment(), testHeadlineComment);
        assertEquals(response.get(2).getApprovalTimestamp(), approvalDate);
        assertEquals(response.get(2).getBudgetName(), budgetName);
    }

    @Test
    public void test_getRecognitionDetailsById_happy_path() {
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response = recognitionService.getRecognitionDetailsByIds(listOfRecognitionIds);

        assertThat(response.size(), equalTo(3));
        assertThat(response.get(0).getId(), equalTo(TestConstants.ID_1));
        assertNull(response.get(0).getApprovalPax());
        assertThat(response.get(0).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(0).getToPax().getPaxId(), equalTo(141L));
        assertThat(response.get(0).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(1).getId(), equalTo(TestConstants.ID_2));
        assertThat(response.get(0).getBudgetName(), equalTo(budgetName));
        assertThat(response.get(0).getProgram(), equalTo(programName));
        assertThat(response.get(0).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(0).getToPax().getManagerPaxId(), equalTo(1074L));

    }

    @Test
    public void test_getRecognitionDetails_pagination_for_rest() {
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetailsPagination(testNominationId, false, null, null, 1, 1, null);

        assertThat(response.size(), equalTo(1));
        assertThat(response.get(0).getId(), equalTo(TestConstants.ID_2));
        assertThat(response.get(0).getApprovalPax().getPaxId(), equalTo(testManagerPaxId));
        assertThat(response.get(0).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(0).getToPax().getPaxId(), equalTo(165L));
        assertThat(response.get(0).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(0).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(0).getToPax().getManagerPaxId(), equalTo(969L));

        response = recognitionService.getRecognitionDetailsPagination(testNominationId, false, null, null,2,1, null);
        assertThat(response.get(0).getId(), equalTo(TestConstants.ID_1));
        assertNull(response.get(0).getApprovalPax());
        assertThat(response.get(0).getFromPax().getPaxId(), equalTo(TestConstants.PAX_DAGARFIN));
        assertThat(response.get(0).getToPax().getPaxId(), equalTo(141L));
        assertThat(response.get(0).getNominationId(), equalTo(testNominationId));
        assertThat(response.get(0).getFromPax().getManagerPaxId(), equalTo(testSubmitterManagerPaxId));
        assertThat(response.get(0).getToPax().getManagerPaxId(), equalTo(1074L));

    }

    @Test
    public void test_getRecognitionDetails_test_managerPax() {
        PowerMockito.when(raiseService.getRecipientsOnNomination(testNominationId, false))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnNomination(testNominationId, true))
                .thenReturn(new ArrayList<Long>());

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_DAGARFIN);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetailsPagination(testNominationId, false, TestConstants.PAX_DAGARFIN, null, 1, 10, null);
        assertThat(response.size(), equalTo(3));

    }

    @Test
    public void test_getRecognitionDetails_invalid_managerPax() {
        PowerMockito.when(raiseService.getRecipientsOnActivity(TestConstants.ID_1, false, null))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(TestConstants.ID_1, true, null))
                .thenReturn(new ArrayList<Long>());

        PowerMockito.when(security.getPaxId()).thenReturn(12L);

        try {
            recognitionService.getRecognitionDetailsPagination(testNominationId, false, TestConstants.ID_1, null, 1, 10, null);
            fail();
        } catch(ErrorMessageException e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().contains(NominationConstants.ERROR_INVALID_MANAGER_PAX_ID));
        }
    }

    @Test
    public void test_getRecognitionDetails_test_status() {
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetailsPagination(testNominationId, false, TestConstants.PAX_DAGARFIN,
                        StatusTypeCode.APPROVED.toString(), 1, 10, null);

        assertThat(response.size(), equalTo(3));
        assertThat(response.get(1).getStatus(), equalTo(StatusTypeCode.APPROVED.toString()));
    }

    @Test
    public void test_getRecognitionDetails_multiple_status() {
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetailsPagination(testNominationId, false, TestConstants.PAX_DAGARFIN,
                        StatusTypeCode.APPROVED.toString()+","+StatusTypeCode.AUTO_APPROVED.toString(), 1, 10, null);

        assertThat(response.size(), equalTo(3));
        assertThat(response.get(1).getStatus(), equalTo(StatusTypeCode.APPROVED.toString()));
    }

    @Test
    public void test_getRecognitionDetails_nomination_not_exist() {
        PowerMockito.when(nominationDao.findById(testNominationId)).thenReturn(null);

        try {
            recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);
            fail("No Exception thrown");
        } catch(Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(NominationConstants.ERROR_NOMINATION_NOT_EXIST_MSG));
        }
    }

    @Test
    public void test_getRecognitionDetails_nomination_id_null() {
        try {
            recognitionService.getRecognitionDetails(null, false, null, null, null);
            fail("No Exception thrown");
        } catch(Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(NominationConstants.ERROR_INVALID_NOMINATION_MSG));
        }
    }

    @Test
    public void test_get_recognition_details_rejected_recognition() {

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(null);

        String approvalNotes = "TESTING123";
        List<ApprovalHistory> testApprovalHistoryList = new ArrayList<>();
        ApprovalHistory testApprovalHistory = new ApprovalHistory();
        testApprovalHistory.setId(1000L);
        testApprovalHistory.setPaxId(testManagerPaxId);
        testApprovalHistory.setTargetId(TestConstants.ID_2);
        testApprovalHistory.setTargetTable(TableName.RECOGNITION.name());
        testApprovalHistory.setApprovalNotes(approvalNotes);
        testApprovalHistoryList.add(testApprovalHistory);
        PowerMockito.when(approvalHistoryFindBy.find()).thenReturn(testApprovalHistoryList);

        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);

        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);
        assertTrue("Approval Comment should not be null",
                approvalNotes.equalsIgnoreCase(response.get(0).getApprovalComment()));
    }

    @Test
    public void test_getRecognitionDetails_pointsIssued_giver() {

        // logged pax is the nomination submitter.
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_DAGARFIN);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);

        assertThat(response.get(0).getPointsIssued(), equalTo(testRecognitionAmount));

    }

    @Test
    public void test_getRecognitionDetails_pointsIssued_manager() {

        // logged pax is one of the receiver's manager.
        PowerMockito.when(security.getPaxId()).thenReturn(1074L);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);

        assertThat(response.get(0).getPointsIssued(), equalTo(null));
        assertThat(response.get(1).getPointsIssued(), equalTo(testRecognitionAmount));
        assertThat(response.get(2).getPointsIssued(), equalTo(null));

    }

    @Test
    public void test_getRecognitionDetails_pointsIssued_receiver() {

        // logged pax is one of the recognition's receiver.
        PowerMockito.when(security.getPaxId()).thenReturn(125L);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);

        assertThat(response.get(0).getPointsIssued(), equalTo(null));
        assertThat(response.get(1).getPointsIssued(), equalTo(null));
        assertThat(response.get(2).getPointsIssued(), equalTo(testRecognitionAmount));

    }

    @Test
    public void test_getRecognitionDetailsEnableApproverPoints_pointsIssued_approver() {
        Long testApproverPax = 170L;

        // logged pax is one of the recognition's approver.
        PowerMockito.when(security.getPaxId()).thenReturn(testApproverPax);

        List<ApprovalPending> testApprovalPendingList = new ArrayList<>();
        ApprovalPending testApprovalPending = new ApprovalPending();
        testApprovalPending.setId(1000L);
        testApprovalPending.setPaxId(testApproverPax);
        testApprovalPending.setTargetId(TestConstants.ID_2);
        testApprovalPending.setTargetTable(TableName.RECOGNITION.name());
        testApprovalPendingList.add(testApprovalPending);
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(testApprovalPendingList);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);

        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, null, null, null, true);

        assertThat(response.get(0).getPointsIssued(), equalTo(testRecognitionAmount));
        assertThat(response.get(1).getPointsIssued(), equalTo(null));
        assertThat(response.get(2).getPointsIssued(), equalTo(null));

    }

    @Test
    public void test_getRecognitionDetailsEnableApproverPoints_pointsIssued_not_approver() {

        // logged pax is one of the recognition's approver.
        PowerMockito.when(security.getPaxId()).thenReturn(testManagerPaxId);

        List<ApprovalPending> testApprovalPendingList = new ArrayList<>();
        ApprovalPending testApprovalPending = new ApprovalPending();
        testApprovalPending.setId(1000L);
        testApprovalPending.setPaxId(TestConstants.ID_1);
        testApprovalPending.setTargetId(TestConstants.ID_2);
        testApprovalPending.setTargetTable(TableName.RECOGNITION.name());
        testApprovalPendingList.add(testApprovalPending);
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(testApprovalPendingList);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);


        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, null, null, null, true);

        assertThat(response.get(0).getPointsIssued(), equalTo(null));
        assertThat(response.get(1).getPointsIssued(), equalTo(null));
        assertThat(response.get(2).getPointsIssued(), equalTo(null));

    }

    @Test
    public void test_getRecognitionDetails_pointsIssued_unreleated() {

        // logged pax is the nomination submitter.
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        Date approvalDate = new Date();
        Map<Long, Date> approvalDateMap = new HashMap<>();
        approvalDateMap.put(TestConstants.ID_1, approvalDate);
        approvalDateMap.put(TestConstants.ID_2, approvalDate);
        approvalDateMap.put(3L, approvalDate);
        PowerMockito.when(approvalService.determineApprovalTimestampByRecognitionId(anyListOf(Long.class)))
                .thenReturn(approvalDateMap);
        PowerMockito.when(raiseService.getRecipientsOnNomination(anyLong(), anyBoolean()))
                .thenReturn(recognitionRecipients);
        PowerMockito.when(raiseService.getRecipientsOnActivity(anyLong(), anyBoolean(), anyListOf(String.class)))
                .thenReturn(recognitionRecipients);
        List<RecognitionDetailsDTO> response =
                recognitionService.getRecognitionDetails(testNominationId, false, null, null, null);

        assertThat(response.get(0).getPointsIssued(), equalTo(null));
        assertThat(response.get(1).getPointsIssued(), equalTo(null));
        assertThat(response.get(2).getPointsIssued(), equalTo(null));

    }

    @Test
    public void test_get_recognitions_by_nomination_id() {
        PowerMockito.when(security.hasRole(anyString(), anyString())).thenReturn(Boolean.TRUE);
        recognitionService.getRecognitionDetails(null, TestConstants.ID_1, null, Boolean.TRUE, TestConstants.PAX_MUDDAM, null, null, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.DEFAULT_PAGE_SIZE);
    }

    @Test
    public void test_getRecognitionCountByRecipient(){
        Mockito.when(cnxtRecognitionRepository.getRecognitionCountByRecipient(Mockito.any(Long.class))).thenReturn(new Integer(666));
        Integer result = this.recognitionService.getRecognitionCountByRecipient(1L);
        assertEquals(new Integer(666), result);
    }

    @Test(expected=ErrorMessageException.class)
    public void test_getNominationOrTransactionByID_ProgramTypeNull_(){
        this.recognitionService.getNominationOrTransactionByID(null, new Long(666));
    }

    @Test(expected=ErrorMessageException.class)
    public void test_getNominationOrTransactionByID_ProgramTypeDoesNotExist(){
        this.recognitionService.getNominationOrTransactionByID("PROGRAM_TYPE_DOES_NOT_EXIST", new Long(666));
    }

    @Test
    public void test_getNominationOrTransactionByID_ProgramTypeIsPointLoad(){
        List<Map<String, Object>> result = new ArrayList<>();

        Mockito.when(transactionDao.getTransactionById(any(Long.class))).thenReturn(result);

        TransactionDTO transactionDTO = this.recognitionService.getNominationOrTransactionByID(ProgramTypeEnum.POINT_LOAD.toString(),
                1L);

        Assert.assertNull(transactionDTO);
    }

    @Test
    public void test_getNominationOrTransactionByID_ProgramTypeIsOtherThanPointLoad(){
        List<Map<String, Object>> result = new ArrayList<>();

        Mockito.when(nominationsDao.getNominationById(any(Long.class))).thenReturn(result);

        TransactionDTO transactionDTO =
                this.recognitionService.getNominationOrTransactionByID(ProgramTypeEnum.AWARD_CODE.toString(),
                1L);

        Assert.assertNull(transactionDTO);
    }

    @Test
    public void test_mapListToTransactionDTO_PointLoadProgramType() throws Exception{
        List<Map<String, Object>> transactionListData = new ArrayList<>();
        Map<String, Object> content = new HashMap<>();
        content.put(ProjectConstants.TRANSACTION_ID, new Long("666"));
        content.put(ProjectConstants.CREATE_DATE, new Date());
        content.put(ProjectConstants.TO_PAX, new Long("666"));
        transactionListData.add(content);
        TransactionDTO result = WhiteboxImpl.invokeMethod(this.recognitionService, "mapListToTransactionDTO",
                ProgramTypeEnum.POINT_LOAD.toString(), transactionListData);
        assertNotNull(result);
        assertNotNull(result.getTransactionId());
        assertEquals(result.getTransactionId(),new Long("666"));
        assertNotNull(result.getCreateDate());
        assertNull(result.getToPax());
    }

    @Test
    public void test_mapListToTransactionDTO_NonPointLoadProgramType() throws Exception{
        List<Map<String, Object>> transactionListData = new ArrayList<>();
        Map<String, Object> content = new HashMap<>();
        content.put(ProjectConstants.NOMINATION_ID, new Long("666"));
        content.put(ProjectConstants.CREATE_DATE, null);
        content.put(ProjectConstants.TO_PAX, new Long("666"));
        transactionListData.add(content);
        TransactionDTO result = WhiteboxImpl.invokeMethod(this.recognitionService, "mapListToTransactionDTO",
                ProgramTypeEnum.AWARD_CODE.toString(), transactionListData);
        assertNotNull(result);
        assertNull(result.getTransactionId());
        assertNotNull(result.getNominationId());
        assertEquals(result.getNominationId(),new Long("666"));
        assertNull(result.getCreateDate());
        assertNull(result.getFromPax());
    }
}
