package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.times;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Comment;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.PaxFiles;
import com.maritz.core.jpa.repository.*;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.security.Security;
import com.maritz.culturenext.util.PermissionUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.jpa.repository.CnxtNominationRepository;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.recognition.dao.EngagementScoreDao;
import com.maritz.culturenext.recognition.dto.EngagementScore;
import com.maritz.culturenext.recognition.services.impl.EngagementScoreServiceImpl;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.powermock.api.mockito.PowerMockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EngagementScoreServiceTest {

    @InjectMocks private EngagementScoreServiceImpl service;
    @Mock private CnxtNominationRepository cnxtNominationRepository;
    @Mock private ConcentrixDao<Comment> commentDao;
    @Mock private ConcentrixDao<PaxFiles> paxFilesDao;
    @Mock private EngagementScoreDao engagementScoreDao;
    @Mock private LoginHistoryRepository loginHistoryRepository;
    @Mock private PaxRepository paxRepository;
    @Mock private PermissionUtil permissionUtil;
    @Mock private RecognitionRepository recognitionRepository;
    @Mock private Security security;
    @Mock private SysUserRepository sysUserRepository;

    private FindBy<PaxFiles> filesFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Comment> commentFindBy = PowerMockito.mock(FindBy.class);

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        // Slightly convoluted, due to the need to mock each call in a call chain, boils down to mocking:
        // PaxFiles paxFiles = paxFilesDao.findBy().where("paxId").eq(paxId).findOne();
        PowerMockito.when(paxFilesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(TestConstants.PAX_KUMARSJ)).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(TestConstants.PAX_HAGOPIWL)).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.findOne()).thenReturn(null);

        PowerMockito.when(commentDao.findBy()).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.where(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(TestConstants.PAX_KUMARSJ)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(TestConstants.PAX_HAGOPIWL)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.and(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.desc(ProjectConstants.COMMENT_DATE)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(null);
        
        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)).thenReturn(true);
        
        List<Map<String, Object>> engagementScoreDetailsList = new ArrayList<>();
        Map<String, Object> engagementScoreDetailsMapOne = new HashMap<>();
        engagementScoreDetailsMapOne.put(ProjectConstants.LOGIN_DATE, new Date());
        engagementScoreDetailsMapOne.put(ProjectConstants.NOMINATION_SUBMITTAL_DATE, new Date());
        engagementScoreDetailsMapOne.put(ProjectConstants.RECOGNITION_RECEIVED_DATE, new Date());
        engagementScoreDetailsMapOne.put(ProjectConstants.COMMENT_DATE, new Date());
        engagementScoreDetailsMapOne.put(ProjectConstants.PIC_ID, 1L);
        
        engagementScoreDetailsList.add(engagementScoreDetailsMapOne);
        
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -15);
        Date fifteenDaysAgo = calendar.getTime();
        
        Map<String, Object> engagementScoreDetailsMapTwo = new HashMap<>();
        engagementScoreDetailsMapTwo.put(ProjectConstants.LOGIN_DATE, fifteenDaysAgo);
        engagementScoreDetailsMapTwo.put(ProjectConstants.NOMINATION_SUBMITTAL_DATE, fifteenDaysAgo);
        engagementScoreDetailsMapTwo.put(ProjectConstants.RECOGNITION_RECEIVED_DATE, fifteenDaysAgo);
        engagementScoreDetailsMapTwo.put(ProjectConstants.COMMENT_DATE, fifteenDaysAgo);
        engagementScoreDetailsMapTwo.put(ProjectConstants.PIC_ID, 1L);
        
        engagementScoreDetailsList.add(engagementScoreDetailsMapTwo);
        
        Calendar calendarTwo = Calendar.getInstance();
        calendarTwo.add(Calendar.MONTH, -2);
        Date twoMonthsAgo = calendarTwo.getTime();
        
        Map<String, Object> engagementScoreDetailsMapThree = new HashMap<>();
        engagementScoreDetailsMapThree.put(ProjectConstants.LOGIN_DATE, twoMonthsAgo);
        engagementScoreDetailsMapThree.put(ProjectConstants.NOMINATION_SUBMITTAL_DATE, twoMonthsAgo);
        engagementScoreDetailsMapThree.put(ProjectConstants.RECOGNITION_RECEIVED_DATE, twoMonthsAgo);
        engagementScoreDetailsMapThree.put(ProjectConstants.COMMENT_DATE, twoMonthsAgo);
        engagementScoreDetailsMapThree.put(ProjectConstants.PIC_ID, 1L);
        
        engagementScoreDetailsList.add(engagementScoreDetailsMapThree);
        
        Map<String, Object> engagementScoreDetailsMapFour = new HashMap<>();
        engagementScoreDetailsMapFour.put(ProjectConstants.LOGIN_DATE, null);
        engagementScoreDetailsMapFour.put(ProjectConstants.NOMINATION_SUBMITTAL_DATE, null);
        engagementScoreDetailsMapFour.put(ProjectConstants.RECOGNITION_RECEIVED_DATE, null);
        engagementScoreDetailsMapFour.put(ProjectConstants.COMMENT_DATE, null);
        engagementScoreDetailsMapFour.put(ProjectConstants.PIC_ID, null);
        
        engagementScoreDetailsList.add(engagementScoreDetailsMapFour);
        
        PowerMockito.when(engagementScoreDao.getEngagementScoreDetails(anyListOf(Long.class))).thenReturn(engagementScoreDetailsList);
    }

    @Test
    public void test_max_limits() {
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(engagementScore.getLastLogin().getMax(), equalTo(20));
        assertThat(engagementScore.getLastRecGiven().getMax(), equalTo(20));
        assertThat(engagementScore.getLastRecReceived().getMax(), equalTo(20));
        assertThat(engagementScore.getLastComment().getMax(), equalTo(20));
        assertThat(engagementScore.getProfilePic().getMax(), equalTo(20));
        assertThat(engagementScore.getTotal().getMax(), equalTo(100));
    }

    @Test
    public void test_null_pax_id() {
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(null);
        assertThat(engagementScore, equalTo(null));
    }

    @Test
    public void test_no_participation() {
        // Setup mocks
        PowerMockito.when(loginHistoryRepository.findMostRecentLoginDateByPaxId(TestConstants.PAX_KUMARSJ)).thenReturn(null);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_KUMARSJ, StatusTypeCode.APPROVED.toString())).thenReturn(null);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_KUMARSJ)).thenReturn(null);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(0));
        assertThat(engagementScore.getLastComment().getScore(), equalTo(0));
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(0));
        assertThat(engagementScore.getTotal().getScore(), equalTo(0));
    }

    @Test
    public void test_happy_path() {
        // Initialize test dates
        DateTime now = new DateTime();
        Date fiveDaysAgo = new Date(now.minusDays(5).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(fiveDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(fiveDaysAgo);
        PaxFiles testPaxFiles = new PaxFiles();

        // Setup mocks
        PowerMockito.when(loginHistoryRepository
                .findMostRecentLoginDateByPaxId(TestConstants.PAX_KUMARSJ)).thenReturn(fiveDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_KUMARSJ, StatusTypeCode.APPROVED.toString())).thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_KUMARSJ)).thenReturn(fiveDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);
        PowerMockito.when(filesFindBy.findOne()).thenReturn(testPaxFiles);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(20));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(20));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(20));
        assertThat(engagementScore.getLastComment().getScore(), equalTo(20));
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(20));
        assertThat(engagementScore.getTotal().getScore(), equalTo(100));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }

    @Test
    public void test_no_participation_for_20_days() {
        // Initialize test dates
        DateTime now = new DateTime();
        Date twentyDaysAgo = new Date(now.minusDays(20).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(twentyDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(twentyDaysAgo);

        // Setup mocks
        PowerMockito.when(loginHistoryRepository.findMostRecentLoginDateByPaxId(TestConstants.PAX_KUMARSJ)).thenReturn(twentyDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_KUMARSJ, StatusTypeCode.APPROVED.toString())).thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_KUMARSJ)).thenReturn(twentyDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(10));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(10));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(10));
        assertThat(engagementScore.getLastComment().getScore(), equalTo(10));
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(0));
        assertThat(engagementScore.getTotal().getScore(), equalTo(40));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }

    @Test
    public void test_no_participation_for_45_days() {
        // Initialize test dates
        DateTime now = new DateTime();
        Date fortyFiveDaysAgo = new Date(now.minusDays(45).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(fortyFiveDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(fortyFiveDaysAgo);

        // Setup mocks
        PowerMockito.when(loginHistoryRepository
                .findMostRecentLoginDateByPaxId(TestConstants.PAX_KUMARSJ)).thenReturn(fortyFiveDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_KUMARSJ, StatusTypeCode.APPROVED.toString())).thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_KUMARSJ)).thenReturn(fortyFiveDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(0));
        assertThat(engagementScore.getLastComment().getScore(), equalTo(0));
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(0));
        assertThat(engagementScore.getTotal().getScore(), equalTo(0));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }
    
    @Test
    public void test_happy_path_commenting_disabled() {
        // Initialize test dates
        DateTime now = new DateTime();
        Date fiveDaysAgo = new Date(now.minusDays(5).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(fiveDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(fiveDaysAgo);
        PaxFiles testPaxFiles = new PaxFiles();

        // Setup mocks
        PowerMockito.when(loginHistoryRepository
                .findMostRecentLoginDateByPaxId(TestConstants.PAX_HAGOPIWL)).thenReturn(fiveDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_HAGOPIWL, StatusTypeCode.APPROVED.toString())).thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_HAGOPIWL)).thenReturn(fiveDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);
        PowerMockito.when(filesFindBy.findOne()).thenReturn(testPaxFiles);
        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)).thenReturn(false);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_HAGOPIWL);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(25));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(25));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(25));
        assertNull(engagementScore.getLastComment());
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(25));
        assertThat(engagementScore.getTotal().getScore(), equalTo(100));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }
    
    @Test
    public void test_no_participation_for_20_days_and_commenting_disabled() {        
        // Initialize test dates
        DateTime now = new DateTime();
        Date twentyDaysAgo = new Date(now.minusDays(20).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(twentyDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(twentyDaysAgo);

        // Setup mocks
        PowerMockito.when(loginHistoryRepository
                .findMostRecentLoginDateByPaxId(TestConstants.PAX_HAGOPIWL)).thenReturn(twentyDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_HAGOPIWL, StatusTypeCode.APPROVED.toString()))
                .thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_HAGOPIWL)).thenReturn(twentyDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);
        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)).thenReturn(false);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_HAGOPIWL);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(13));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(13));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(13));
        assertNull(engagementScore.getLastComment());
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(0));
        assertThat(engagementScore.getTotal().getScore(), equalTo(39));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }

    @Test
    public void test_no_participation_for_45_days_and_commenting_disabled() {
        // Initialize test dates
        DateTime now = new DateTime();
        Date fortyFiveDaysAgo = new Date(now.minusDays(45).getMillis());

        //Initialize mocked objects
        Nomination nomination = new Nomination();
        nomination.setSubmittalDate(fortyFiveDaysAgo);
        Comment comment = new Comment();
        comment.setCommentDate(fortyFiveDaysAgo);

        // Setup mocks
        PowerMockito.when(loginHistoryRepository
                .findMostRecentLoginDateByPaxId(TestConstants.PAX_HAGOPIWL)).thenReturn(fortyFiveDaysAgo);
        PowerMockito.when(cnxtNominationRepository
                .findTop1BySubmitterPaxIdAndStatusTypeCodeOrderBySubmittalDateDesc(
                        TestConstants.PAX_HAGOPIWL, StatusTypeCode.APPROVED.toString())).thenReturn(nomination);
        PowerMockito.when(cnxtNominationRepository.getLastRecReceived(TestConstants.PAX_HAGOPIWL)).thenReturn(fortyFiveDaysAgo);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);
        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)).thenReturn(false);

        // Call method and assert values
        EngagementScore engagementScore = service.getEngagementScoreByPaxId(TestConstants.PAX_HAGOPIWL);
        assertThat(engagementScore.getLastLogin().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecGiven().getScore(), equalTo(0));
        assertThat(engagementScore.getLastRecReceived().getScore(), equalTo(0));
        assertNull(engagementScore.getLastComment());
        assertThat(engagementScore.getProfilePic().getScore(), equalTo(0));
        assertThat(engagementScore.getTotal().getScore(), equalTo(0));
        Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
        Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
        Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
    }
    
    @Test
    public void test_bulkFetchEngagementScoreTotal() {
        
        Integer scoreCommentingEnabled = service.bulkFetchEngagementScoreTotal(Arrays.asList(1L, 2L, 3L, 4L));
        /* Engagement scores:
         * 100
         * 60
         * 20
         * 0
         */
        assertEquals("Should have 45% engagement", 45, scoreCommentingEnabled.intValue());
        
        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE)).thenReturn(Boolean.FALSE);

        Integer scoreCommentingDisabled = service.bulkFetchEngagementScoreTotal(Arrays.asList(1L, 2L, 3L, 4L));
        /* Engagement scores:
         * 100
         * 64 - comment date is ignored
         * 25
         * 0 
         */
        assertEquals("Should have 48% engagement", 48, scoreCommentingDisabled.intValue());
        
    }

}
