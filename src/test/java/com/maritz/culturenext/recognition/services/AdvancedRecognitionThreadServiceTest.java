package com.maritz.culturenext.recognition.services;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.impl.AdvancedRecognitionThreadServiceImpl;
import com.maritz.test.AbstractMockTest;

public class AdvancedRecognitionThreadServiceTest extends AbstractMockTest {

    @InjectMocks AdvancedRecognitionThreadServiceImpl advancedRecognitionThreadService;
    @Rule public ExpectedException thrown = ExpectedException.none();

    @Mock ConcentrixDao<Pax> paxDao;
    @Mock ConcentrixDao<ProgramAwardTier> programAwardTiersDao;
    @Mock ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;
    @Mock NominationService nominationService;

    @Mock FindBy<Pax> paxFindBy;
    @Mock FindBy<ProgramAwardTier> programAwardTiersFindBy;
    @Mock FindBy<StageAdvancedRec> stageAdvancedRecFindBy;

    private static final String TEST_PAYOUT_TYPE_CASH = "CASH";
    private static final String TEST_PAYOUT_TYPE_DPP = "DPP";

    @Before
    public void setup() throws Exception {
        StageAdvancedRec record = new StageAdvancedRec();

        record.setSubmitterId(TestConstants.USER_MUDDAM);
        record.setPublicHeadline("public headline");
        record.setProgramId(1L);
        record.setValueId(1L);
        record.setPrivateMessage("private message");
        record.setEcardId(1L);
        record.setBudgetId(1L);
        record.setAwardType(TEST_PAYOUT_TYPE_CASH);
        record.setAwardAmount(1D);
        record.setOriginalAmount(1D);
        record.setRecipientId(TestConstants.USER_HARWELLM);

        StageAdvancedRec record2 = new StageAdvancedRec();

        record2.setSubmitterId(TestConstants.USER_MUDDAM);
        record2.setPublicHeadline("public headline");
        record2.setProgramId(1L);
        record2.setValueId(1L);
        record2.setPrivateMessage("private message");
        record2.setEcardId(1L);
        record2.setBudgetId(1L);
        record2.setAwardType(TEST_PAYOUT_TYPE_CASH);
        record2.setAwardAmount(1D);
        record2.setRecipientId(TestConstants.USER_HAGOPIWL);

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        nominationRecordList.add(record);
        nominationRecordList.add(record2);

        PowerMockito.when(stageAdvancedRecDao.findBy()).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.where(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.eq(Matchers.anyLong())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.and(Matchers.anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        Pax submitterPax = new Pax();
        submitterPax.setControlNum(TestConstants.USER_MUDDAM);
        submitterPax.setPaxId(1L);

        PowerMockito.when(paxDao.findBy()).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.where(Matchers.anyString())).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.eq(Matchers.anyString())).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.findOne()).thenReturn(submitterPax);

        Pax receiverPax1 = new Pax();
        receiverPax1.setControlNum(TestConstants.USER_HARWELLM);
        receiverPax1.setPaxId(1L);

        Pax receiverPax2 = new Pax();
        receiverPax2.setControlNum(TestConstants.USER_HAGOPIWL);
        receiverPax2.setPaxId(2L);

        List<Pax> paxList = new ArrayList<>();
        paxList.add(receiverPax1);
        paxList.add(receiverPax2);

        PowerMockito.when(paxFindBy.in(Matchers.anyCollectionOf(String.class))).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.find()).thenReturn(paxList);

        ProgramAwardTier awardTier = new ProgramAwardTier();
        awardTier.setProgramId(1L);
        awardTier.setMinAmount(0D);
        awardTier.setMaxAmount(0D);
        awardTier.setId(1L);

        ProgramAwardTier awardTier2 = new ProgramAwardTier();
        awardTier2.setProgramId(1L);
        awardTier2.setMinAmount(2D);
        awardTier2.setMaxAmount(2D);
        awardTier2.setId(2L);

        ProgramAwardTier awardTier3 = new ProgramAwardTier();
        awardTier3.setProgramId(1L);
        awardTier3.setMinAmount(1D);
        awardTier3.setMaxAmount(1D);
        awardTier3.setId(3L);

        List<ProgramAwardTier> awardTierList = new ArrayList<>();
        awardTierList.add(awardTier);
        awardTierList.add(awardTier2);
        awardTierList.add(awardTier3);

        PowerMockito.when(programAwardTiersDao.findBy()).thenReturn(programAwardTiersFindBy);
        PowerMockito.when(programAwardTiersFindBy.where(Matchers.anyString())).thenReturn(programAwardTiersFindBy);
        PowerMockito.when(programAwardTiersFindBy.eq(Matchers.anyLong())).thenReturn(programAwardTiersFindBy);
        PowerMockito.when(programAwardTiersFindBy.and(Matchers.anyString())).thenReturn(programAwardTiersFindBy);
        PowerMockito.when(programAwardTiersFindBy.eq(Matchers.anyString())).thenReturn(programAwardTiersFindBy);
        PowerMockito.when(programAwardTiersFindBy.find()).thenReturn(awardTierList);

        List<ErrorMessage> errorMessageList = new ArrayList<>();

        PowerMockito.when(
                nominationService.validateNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyListOf(Long.class),
                        Matchers.anyListOf(Long.class), Matchers.anyString(), Matchers.anyBoolean()
                    )
            ).thenReturn(errorMessageList);
    }

    @Test
    public void test_happy_path_validation_cash() {
        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertFalse("Should be no errors.", result);
    }

    @Test
    public void test_happy_path_cash_validation_async() throws Exception {
        Future<Boolean> resultFuture = advancedRecognitionThreadService.validateNominationAsync(1L, 1L);
        Boolean result = resultFuture.get();
        Assert.assertFalse("Should be no errors.", result);
    }

    @Test
    public void test_happy_path_create_nomination_async() {
        // This is basically just "doesn't throw errors" / code coverage since the only logic is covered in validation
        PowerMockito.when(
                nominationService.createStandardNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyBoolean())
                    ).thenReturn(new NominationDetailsDTO()
            );

        advancedRecognitionThreadService.createNominationAsync(1L, 1L);

    }

    @Test
    public void test_happy_path_validation_pointless() {

        StageAdvancedRec record = new StageAdvancedRec();

        record.setSubmitterId(TestConstants.USER_MUDDAM);
        record.setPublicHeadline("public headline");
        record.setProgramId(1L);
        record.setValueId(1L);
        record.setPrivateMessage("private message");
        record.setEcardId(1L);
        record.setRecipientId(TestConstants.USER_HARWELLM);

        StageAdvancedRec record2 = new StageAdvancedRec();

        record2.setSubmitterId(TestConstants.USER_MUDDAM);
        record2.setPublicHeadline("public headline");
        record2.setProgramId(1L);
        record2.setValueId(1L);
        record2.setPrivateMessage("private message");
        record2.setEcardId(1L);
        record2.setRecipientId(TestConstants.USER_HAGOPIWL);

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        nominationRecordList.add(record);
        nominationRecordList.add(record2);

        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertFalse("Should be no errors.", result);
    }

    @Test
    public void test_fail_fast_validation_no_records() {

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - no records found.", result);

    }

    @Test
    public void test_error_validation_no_award_tier_found() {

        // Also, get code coverage on POINTS award type :)
        StageAdvancedRec record = new StageAdvancedRec();

        record.setSubmitterId(TestConstants.USER_MUDDAM);
        record.setPublicHeadline("public headline");
        record.setProgramId(1L);
        record.setValueId(1L);
        record.setPrivateMessage("private message");
        record.setEcardId(1L);
        record.setBudgetId(1L);
        record.setAwardType(TEST_PAYOUT_TYPE_DPP);
        record.setAwardAmount(1D);
        record.setOriginalAmount(1D);
        record.setRecipientId(TestConstants.USER_HARWELLM);

        StageAdvancedRec record2 = new StageAdvancedRec();

        record2.setSubmitterId(TestConstants.USER_MUDDAM);
        record2.setPublicHeadline("public headline");
        record2.setProgramId(1L);
        record2.setValueId(1L);
        record2.setPrivateMessage("private message");
        record2.setEcardId(1L);
        record2.setBudgetId(1L);
        record2.setAwardType(TEST_PAYOUT_TYPE_DPP);
        record2.setAwardAmount(1D);
        record2.setOriginalAmount(1D);
        record2.setRecipientId(TestConstants.USER_HAGOPIWL);

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        nominationRecordList.add(record);
        nominationRecordList.add(record2);

        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        List<ProgramAwardTier> awardTierList = new ArrayList<>();
        PowerMockito.when(programAwardTiersFindBy.find()).thenReturn(awardTierList);

        List<ErrorMessage> errorMessageList = new ArrayList<>();
        errorMessageList.add(NominationConstants.AWARD_TIER_MISSING_MESSAGE); // no award tier

        PowerMockito.when(
                nominationService.validateNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyListOf(Long.class),
                        Matchers.anyListOf(Long.class), Matchers.anyString(), Matchers.anyBoolean()
                    )
            ).thenReturn(errorMessageList);


        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - no award tiers found.", result);
    }

    @Test
    public void test_error_validation_duplicate_recipients() {

        StageAdvancedRec duplicateRecord1 = new StageAdvancedRec();

        duplicateRecord1.setSubmitterId(TestConstants.USER_MUDDAM);
        duplicateRecord1.setPublicHeadline("public headline");
        duplicateRecord1.setProgramId(1L);
        duplicateRecord1.setValueId(1L);
        duplicateRecord1.setPrivateMessage("private message");
        duplicateRecord1.setEcardId(1L);
        duplicateRecord1.setBudgetId(1L);
        duplicateRecord1.setAwardType(TEST_PAYOUT_TYPE_CASH);
        duplicateRecord1.setAwardAmount(1D);
        duplicateRecord1.setOriginalAmount(1D);
        duplicateRecord1.setRecipientId(TestConstants.USER_HAGOPIWL);

        StageAdvancedRec duplicateRecord2 = new StageAdvancedRec();

        duplicateRecord2.setSubmitterId(TestConstants.USER_MUDDAM);
        duplicateRecord2.setPublicHeadline("public headline");
        duplicateRecord2.setProgramId(1L);
        duplicateRecord2.setValueId(1L);
        duplicateRecord2.setPrivateMessage("private message");
        duplicateRecord2.setEcardId(1L);
        duplicateRecord2.setBudgetId(1L);
        duplicateRecord2.setAwardType(TEST_PAYOUT_TYPE_CASH);
        duplicateRecord2.setAwardAmount(1D);
        duplicateRecord2.setOriginalAmount(1D);
        duplicateRecord2.setRecipientId(TestConstants.USER_HAGOPIWL);

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        nominationRecordList.add(duplicateRecord1);
        nominationRecordList.add(duplicateRecord2);

        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - duplicate recipients.", result);
    }

    @Test
    public void test_error_validation_nomination_service_errors_specific() {

        StageAdvancedRec goodRecord = new StageAdvancedRec();

        goodRecord.setSubmitterId(TestConstants.USER_MUDDAM);
        goodRecord.setPublicHeadline("public headline");
        goodRecord.setProgramId(1L);
        goodRecord.setValueId(1L);
        goodRecord.setPrivateMessage("private message");
        goodRecord.setEcardId(1L);
        goodRecord.setBudgetId(1L);
        goodRecord.setAwardType(TEST_PAYOUT_TYPE_CASH);
        goodRecord.setAwardAmount(1D);
        goodRecord.setOriginalAmount(1D);
        goodRecord.setRecipientId(TestConstants.USER_KUMARSJ);

        StageAdvancedRec inelligibleRecord = new StageAdvancedRec();

        inelligibleRecord.setSubmitterId(TestConstants.USER_MUDDAM);
        inelligibleRecord.setPublicHeadline("public headline");
        inelligibleRecord.setProgramId(1L);
        inelligibleRecord.setValueId(1L);
        inelligibleRecord.setPrivateMessage("private message");
        inelligibleRecord.setEcardId(1L);
        inelligibleRecord.setBudgetId(1L);
        inelligibleRecord.setAwardType(TEST_PAYOUT_TYPE_CASH);
        inelligibleRecord.setAwardAmount(1D);
        inelligibleRecord.setOriginalAmount(1D);
        inelligibleRecord.setRecipientId(TestConstants.USER_HARWELLM);

        StageAdvancedRec selfRecRecord = new StageAdvancedRec();

        selfRecRecord.setSubmitterId(TestConstants.USER_MUDDAM);
        selfRecRecord.setPublicHeadline("public headline");
        selfRecRecord.setProgramId(1L);
        selfRecRecord.setValueId(1L);
        selfRecRecord.setPrivateMessage("private message");
        selfRecRecord.setEcardId(1L);
        selfRecRecord.setBudgetId(1L);
        selfRecRecord.setAwardType(TEST_PAYOUT_TYPE_CASH);
        selfRecRecord.setAwardAmount(1D);
        selfRecRecord.setOriginalAmount(1D);
        selfRecRecord.setRecipientId(TestConstants.USER_MUDDAM);

        List<StageAdvancedRec> nominationRecordList = new ArrayList<>();
        nominationRecordList.add(goodRecord);
        nominationRecordList.add(inelligibleRecord);
        nominationRecordList.add(selfRecRecord);

        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(nominationRecordList);

        List<ErrorMessage> errorMessageList = new ArrayList<>();
        errorMessageList.add(new ErrorMessage()
                .setCode(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER)
                .setMessage(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER_MSG)
                .setField(NominationConstants.RECEIVERS_PAX + 1L));
        errorMessageList.add(NominationConstants.ERROR_RECEIVER_DUPLICATED_MESSAGE);
        errorMessageList.add(NominationConstants.ERROR_CANNOT_RECOGNIZE_SELF_MESSAGE);

        PowerMockito.when(
                nominationService.validateNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyListOf(Long.class),
                        Matchers.anyListOf(Long.class), Matchers.anyString(), Matchers.anyBoolean()
                    )
            ).thenReturn(errorMessageList);


        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - only on certain rows.", result);
    }

    @Test
    public void test_error_validation_nomination_service_errors_general() {

        List<ErrorMessage> errorMessageList = new ArrayList<>();
        errorMessageList.add(NominationConstants.INELIGIBLE_SUBMITTER);

        PowerMockito.when(
                nominationService.validateNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyListOf(Long.class),
                        Matchers.anyListOf(Long.class), Matchers.anyString(), Matchers.anyBoolean()
                    )
            ).thenReturn(errorMessageList);

        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - general error applied to all rows.", result);
    }

    @Test
    public void test_error_validation_nomination_service_code_coverage() {

        List<ProgramAwardTier> awardTierList = new ArrayList<>();
        PowerMockito.when(programAwardTiersFindBy.find()).thenReturn(awardTierList);

        List<ErrorMessage> errorMessageList = new ArrayList<>();
        errorMessageList.add(NominationConstants.AWARD_TIER_MISSING_MESSAGE); // no award tier
        errorMessageList.add(new ErrorMessage()
                .setCode(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER)
                .setMessage(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER_MSG)
                .setField(NominationConstants.RECEIVERS_PAX + 3L));

        PowerMockito.when(
                nominationService.validateNomination(
                        Matchers.anyLong(), Matchers.any(NominationRequestDTO.class), Matchers.anyListOf(Long.class),
                        Matchers.anyListOf(Long.class), Matchers.anyString(), Matchers.anyBoolean()
                    )
            ).thenReturn(errorMessageList);

        Boolean result = advancedRecognitionThreadService.validateNomination(1L, 1L);
        Assert.assertTrue("Should have errors - no pax id match = apply everywhere (code coverage).", result);
    }
}
