package com.maritz.culturenext.recognition.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import com.maritz.test.AbstractDatabaseTest;

public class RecognitionServiceUnitTest extends AbstractDatabaseTest {
    
    @Inject private AuthenticationService authenticationService;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private RecognitionService recognitionService;

    private static final String RESTRICTED_SUSPENDED_PROGRAM_NAME = "RECOGNITION_LIST_TEST_PROGRAM";    
    private static final String PRIVATE_PROGRAM_NAME = "RECOGNITION_LIST_PRIVATE_TEST_PROGRAM";
    private static final String APPROVAL_NOMINATION_HEADLINE_COMMENT = "NEWSFEED_APPROVAL_STATUS_TEST";
    
    @Test
    public void testRestrictedUserStatus(){

        Long testingProgramId = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(RESTRICTED_SUSPENDED_PROGRAM_NAME)
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        assertNotNull("Test data was not created", testingProgramId);
        
        Nomination testingNomination = nominationDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(testingProgramId)
                .findOne();
        assertNotNull("Test data was not created", testingNomination);
        
        authenticationService.authenticate(TestConstants.USER_BELLWE);
        List<RecognitionDetailsDTO> receiverList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), null, null, null, null, null, null);
        
        assertFalse("Receiver should see themselves", receiverList.isEmpty());
        
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        List<RecognitionDetailsDTO> submitterList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), null, null, null, null, null, null);
        
        assertFalse("Submitter should see receiver", submitterList.isEmpty());
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        List<RecognitionDetailsDTO> otherList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), null, null, null, null, null, null);
        
        assertTrue("Unrelated pax should not see anything", otherList.isEmpty());
    }
    
    @Test
    public void testPrivatePreference(){

        Long testingProgramId = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(PRIVATE_PROGRAM_NAME)
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        assertNotNull("Test data was not created", testingProgramId);
        
        Nomination testingNomination = nominationDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(testingProgramId)
                .findOne();
        assertNotNull("Test data was not created", testingNomination);
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        List<RecognitionDetailsDTO> receiverList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), true, null, null, null, null, null);
        
        assertFalse("Receiver should see themselves", receiverList.isEmpty());
        
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        List<RecognitionDetailsDTO> submitterList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), true, null, null, null, null, null);
        
        assertFalse("Submitter should see receiver", submitterList.isEmpty());
        
        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        List<RecognitionDetailsDTO> approverList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), true, null, null, null, null, null);
        
        assertFalse("Approver should see receiver", approverList.isEmpty());
        
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        List<RecognitionDetailsDTO> otherList = recognitionService
                .getRecognitionDetailsPagination(testingNomination.getId(), true, null, null, null, null, null);
        
        assertTrue("Unrelated pax should not see anything", otherList.isEmpty());
    }
    
    @Test
    public void test_recognitions_by_nomination_id_future() {
        Long nominationId = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(APPROVAL_NOMINATION_HEADLINE_COMMENT)
                .findOne(ProjectConstants.ID, Long.class);

        assertNotNull("Test data was not created", nominationId);
        
        Long notificationId = alertDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationId)
                .and(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.RECOGNITION_APPROVAL)
                .findOne(ProjectConstants.ID, Long.class);

        assertNotNull("Test data was not created", notificationId);
        
        // first, test with good notification id
        recognitionService.getRecognitionDetails(null, nominationId, null, null, TestConstants.PAX_MUDDAM, null, notificationId, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.DEFAULT_PAGE_SIZE);
        
        // second, test with bad notification id (notification not for that nomination)
        recognitionService.getRecognitionDetails(null, nominationId, null, null, TestConstants.PAX_MUDDAM, null, TestConstants.ID_1, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.DEFAULT_PAGE_SIZE);
        
    }

}
