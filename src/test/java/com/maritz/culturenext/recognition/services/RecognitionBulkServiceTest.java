package com.maritz.culturenext.recognition.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.recognition.constants.RecognitionBulkUploadConstants;
import com.maritz.culturenext.recognition.constants.RecognitionConstants;
import com.maritz.culturenext.recognition.dao.RecognitionBulkUploadDao;
import com.maritz.culturenext.recognition.dto.RecognitionBulkResponseDTO;
import com.maritz.culturenext.recognition.services.impl.RecognitionBulkUploadServiceImpl;
import com.maritz.test.AbstractMockTest;

public class RecognitionBulkServiceTest extends AbstractMockTest {

    @Mock private RecognitionBulkUploadDao recognitionBulkResponseDao;
    @Mock private Environment environment;
    @Mock private Security security;

    @InjectMocks private RecognitionBulkUploadServiceImpl recognitionBulkUploadService;

    public static final String BULK_UPLOAD_HISTORY_PATH = "/bulk/history";

    @Before
    public void setup() {
        PowerMockito.when(environment.getProperty(anyString())).thenReturn(Boolean.TRUE.toString());
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        List<Map<String, Object>> bulkUploadHistoryList = new ArrayList<>();
        Map<String, Object> bulkUploadHistoryEntry = new HashMap<>();

        bulkUploadHistoryEntry.put(ProjectConstants.BATCH_ID, 23622L);
        bulkUploadHistoryEntry.put(ProjectConstants.STATUS, "PENDING_RELEASE");
        bulkUploadHistoryEntry.put(ProjectConstants.FILE_NAME, "test_bulk_upload_history.csv");
        bulkUploadHistoryEntry.put(ProjectConstants.PROCESS_DATE, new Date());
        bulkUploadHistoryEntry.put(RecognitionBulkUploadConstants.TOTAL_ROWS, "40");
        bulkUploadHistoryEntry.put(ProjectConstants.TOTAL_AWARDS, "2000");
        bulkUploadHistoryEntry.put(ProjectConstants.NOMINATIONS, "20");
        bulkUploadHistoryEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);
        bulkUploadHistoryEntry.put(ProjectConstants.PPP_INDEX_APPLIED, "true");

        bulkUploadHistoryList.add(bulkUploadHistoryEntry);

        PowerMockito.when(
                recognitionBulkResponseDao.getBulkUploadHistory(anyString(), anyInt(), anyInt()))
                .thenReturn(bulkUploadHistoryList);
    }

    @Test
    public void test_get_bulk_upload_history() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(RecognitionConstants.RECOGNITIONS_URI_BASE + BULK_UPLOAD_HISTORY_PATH)
                .setPageNumber(1)
                .setPageSize(1);

        PaginatedResponseObject<RecognitionBulkResponseDTO> response =
                recognitionBulkUploadService.getBulkUploadHistory(requestDetails, StatusTypeCode.FAILED.toString(), 1, 1);

        Mockito.verify(recognitionBulkResponseDao, times(1)).getBulkUploadHistory(anyString(), anyInt(), anyInt());

        assertNotNull("Should have a response", response);

        List<RecognitionBulkResponseDTO> data = response.getData();
        assertNotNull("Data should not be null", data);
        assertTrue("BulkUpload 'is PPP index applied' should be true but was not",data.get(0).isPppIndexApplied());
        assertFalse("Data should not be empty", data.isEmpty());
    }
}
