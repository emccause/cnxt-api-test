package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.*;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.AuxiliaryNotification;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.NominationMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtTransactionHeaderRepository;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dto.NominationDetailsDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.test.AbstractDatabaseTest;

public class NominationServiceTest extends AbstractDatabaseTest {

    // programs without notifications
    private static final Long PROGRAM_NO_NEWSFEED_NO_NOTIFICATION = 4487L;
    private static final Long PROGRAM_PRIVATE_NEWSFEED_NO_NOTIFICATION = 4490L;
    private static final Long PROGRAM_PUBLIC_NEWSFEED_NO_NOTIFICATION = 4491L;
    private static final Long PROGRAM_RECIPIENT_PREF_NEWSFEED_NO_NOTIFICATION = 4492L;

    // programs without newsfeed
    private static final Long PROGRAM_NO_NEWSFEED_RECIPIENT_NOTIFICATION = 4488L;
    private static final Long PROGRAM_NO_NEWSFEED_MANAGER_NOTIFICATION = 4489L;
    private static final Long PROGRAM_NO_NEWSFEED_BOTH_NOTIFICATION = 4493L;

    // bulk nomination data verification
    private static final String BULK_NOMINATION_TEST_GROUP_DESC = "BULK_RECOGNITION_TEST_GROUP";
    private static final String BULK_NOMINATION_TEST_PROGRAM_NAME = "BULK_RECOGNITION_TEST_PROGRAM";

    // Program that PAX_HARWELLM is not a submitter for
    private static final Long PROGRAM_INELIGIBLE_SUBMITTER = 6176L;

    private static final String TEST_VALIDATION_TYPE = "TEST_VALIDATION_TYPE";

    private static final String TEST_PAYOUT_TYPE_DPP = "DPP";

    private static final String PERSONAL_GROUP_DESC_1 = "My Peeps!";
    private static final String PERSONAL_GROUP_DESC_2 = "My great group";
    private static final String PERSONAL_GROUP_DESC_3 = "Capital One";

    // services for verifying
    @Inject private AuthenticationService authenticationService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private CnxtTransactionHeaderRepository cnxtTransactionHeaderRepository;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<AuxiliaryNotification> auxiliaryNotificationDao;
    @Inject private ConcentrixDao<Groups> groupsDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<NominationMisc> nominationMiscDao;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<Recognition> recognitionDao;
    @Inject private NominationService nominationService;

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
    }
    
    @Test
    public void test_no_newsfeed() throws Exception {
        NominationDetailsDTO nominationDetail = createDefaultNomination(PROGRAM_NO_NEWSFEED_NO_NOTIFICATION);

        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertThat(newsfeedItem.getNewsfeedVisibility().equalsIgnoreCase(NewsfeedVisibility.NONE.name()), is(true));
    }

    @Test
    public void test_private_newsfeed() throws Exception {
        NominationDetailsDTO nominationDetail = createDefaultNomination(PROGRAM_PRIVATE_NEWSFEED_NO_NOTIFICATION);

        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertEquals(NewsfeedVisibility.PRIVATE.name(), newsfeedItem.getNewsfeedVisibility());
    }

    @Test
    public void test_public_newsfeed() throws Exception {
        NominationDetailsDTO nominationDetail = createDefaultNomination(PROGRAM_PUBLIC_NEWSFEED_NO_NOTIFICATION);

        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertEquals(NewsfeedVisibility.PUBLIC.name(), newsfeedItem.getNewsfeedVisibility());
    }

    @Test
    public void test_recipient_pref_newsfeed() throws Exception {
        NominationDetailsDTO nominationDetail = 
                createDefaultNomination(PROGRAM_RECIPIENT_PREF_NEWSFEED_NO_NOTIFICATION);

        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertEquals(NewsfeedVisibility.RECIPIENT_PREF.name(), newsfeedItem.getNewsfeedVisibility());
    }

    // Email generation tests already exist, no need to test here

    @Test
    public void test_no_notification() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        createDefaultNomination(PROGRAM_NO_NEWSFEED_NO_NOTIFICATION);

        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        assertEquals(recipientAlertCountBefore, recipientAlertCountAfter);
        assertEquals(managerAlertCountBefore, managerAlertCountAfter);
    }

    @Test
    public void test_recipient_notification() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        createDefaultNomination(PROGRAM_NO_NEWSFEED_RECIPIENT_NOTIFICATION);

        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        assertThat(recipientAlertCountAfter - recipientAlertCountBefore, is(1));
        assertEquals(managerAlertCountBefore, managerAlertCountAfter);
    }

    @Test
    public void test_manager_notification() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        createDefaultNomination(PROGRAM_NO_NEWSFEED_MANAGER_NOTIFICATION);

        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        assertEquals(recipientAlertCountBefore, recipientAlertCountAfter);
        assertThat(managerAlertCountAfter - managerAlertCountBefore, is(1));
    }

    @Test
    public void test_both_notification() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        createDefaultNomination(PROGRAM_NO_NEWSFEED_BOTH_NOTIFICATION);

        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .count();
        int managerAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                .count();

        assertThat(recipientAlertCountAfter - recipientAlertCountBefore, is(1));
        assertThat(managerAlertCountAfter - managerAlertCountBefore, is(1));
    }

    @Test
    public void check_transaction_header_status_approved() throws Exception {
        NominationDetailsDTO nominationDetail = createDefaultNomination(PROGRAM_NO_NEWSFEED_BOTH_NOTIFICATION);
        List<TransactionHeader> transactionHeaders =
                cnxtTransactionHeaderRepository.getTransactionHeaderByNominationId(nominationDetail.getNominationId());
        for (TransactionHeader transactionHeader : transactionHeaders) {
            assertThat(transactionHeader
                    .getStatusTypeCode()
                    .equalsIgnoreCase(StatusTypeCode.APPROVED.toString()), is(true));
        }

        Assert.assertNull("Should not have created payout type misc", nominationMiscDao.findBy()
            .where("nominationId").eq(nominationDetail.getNominationId())
            .and("vfName").eq(VfName.PAYOUT_TYPE.name())
            .findOne());
    }
    
    @Test
    public void test_newsfeed_with_all_recognitions_pending() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(141L).setAwardAmount(5).setOriginalAmount(5).setAwardTierId(9566L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(1328L).setAwardAmount(5).setOriginalAmount(5).setAwardTierId(9566L));
        receivers.add(new GroupMemberAwardDTO().setPaxId(3265L).setAwardAmount(5).setOriginalAmount(5).setAwardTierId(9566L));
        dto.setReceivers(receivers);
        dto.setProgramId(13571L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("Test rec please ignore");
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setIsPrivate(true);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);

        authenticationService.authenticate(TestConstants.USER_ADAM_SMITH);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_ADAM_SMITH_ADMIN, dto, Boolean.TRUE);
        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertEquals(StatusTypeCode.PENDING.toString(), newsfeedItem.getStatusTypeCode());
    }
    
    @Test
    public void test_newsfeed_with_recognition_auto_approved() throws Exception {
        budgetEligibilityCacheService.buildCache();
        ProgramAwardTier programAwardTier = programAwardTierDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(1)
                .and(ProjectConstants.MIN_AMOUNT).eq(30.00)
                .and(ProjectConstants.MAX_AMOUNT).eq(30.00)
                .findOne();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(141L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(1328L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(3265L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("Test rec please ignore");
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setIsPrivate(true);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);

        authenticationService.authenticate(TestConstants.USER_PORTERGA);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_PORTERGA, dto, Boolean.TRUE);
        NewsfeedItem newsfeedItem = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(nominationDetail.getNominationId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne();
        assertNotNull(newsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.toString(), newsfeedItem.getStatusTypeCode());
    }

    @Test
    public void test_bulk_nomination() throws Exception {
        budgetEligibilityCacheService.buildCache();
        Groups testGroup = groupsDao.findBy()
                .where(ProjectConstants.GROUP_DESC).eq(BULK_NOMINATION_TEST_GROUP_DESC)
                .findOne();
        Program testProgram = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(BULK_NOMINATION_TEST_PROGRAM_NAME)
                .findOne();

        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setProgramId(testProgram.getProgramId());
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(testGroup.getGroupId())));
        dto.setHeadline("Test rec please ignore");
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setIsPrivate(true);

        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        nominationService.createStandardNomination(TestConstants.PAX_MUDDAM, dto, Boolean.TRUE);
        assertNotNull(nominationService);
    }

    @Test
    public void test_nomination_error_exception() throws Exception {
        budgetEligibilityCacheService.buildCache();
        Groups testGroup = groupsDao.findBy()
                .where(ProjectConstants.GROUP_DESC).eq(BULK_NOMINATION_TEST_GROUP_DESC)
                .findOne();
        Program testProgram = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(BULK_NOMINATION_TEST_PROGRAM_NAME)
                .findOne();

        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setProgramId(testProgram.getProgramId());
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(testGroup.getGroupId())));
        dto.setHeadline("Test rec please ignore");
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setIsPrivate(true);

        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        try {
            nominationService.createStandardNomination(1L, dto, Boolean.TRUE);
            fail("should have thrown an exception");
        } catch (Exception e) {
            String message = "Exception thrown";
        }


    }
    
    @Test
    public void test_recognition_with_notify_others() throws Exception {
        
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(TestConstants.PAX_MUDDAM));
        dto.setReceivers(receivers);
        dto.setProgramId(PROGRAM_PUBLIC_NEWSFEED_NO_NOTIFICATION);
        dto.setImageId(30L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("Test rec with notify others");
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(false);
        //Add Notify Others
        List<EmployeeDTO> notifyOthers = new ArrayList<EmployeeDTO>();
        EmployeeDTO alex = new EmployeeDTO().setPaxId(TestConstants.PAX_MUDDAM);
        EmployeeDTO wayne = new EmployeeDTO().setPaxId(TestConstants.PAX_HAGOPIWL);
        notifyOthers.add(alex);
        notifyOthers.add(wayne);        
        dto.setNotifyOthers(notifyOthers);

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_HARWELLM, dto, Boolean.TRUE);

        List<AuxiliaryNotification> notifyOtherList = auxiliaryNotificationDao.findBy()
                .where(ProjectConstants.NOMINATION_ID).eq(nominationDetail.getNominationId()).find();
        
        assertNotNull(notifyOtherList);
        assertEquals(notifyOtherList.size(), 2);
    }

    @Test
    public void test_validateNomination_receivers_noRecipients() {
        NominationRequestDTO dto = createDefaultNominationRequest(PROGRAM_NO_NEWSFEED_NO_NOTIFICATION);
        List<Long> paxIds = new ArrayList<>();
        paxIds.add(171L);
        List<Long> groupIds = new ArrayList<>();
        String validationType = TEST_VALIDATION_TYPE;
        List<ErrorMessage> errorMessages = nominationService.validateNomination(TestConstants.PAX_MUDDAM,
                                                                                dto,
                                                                                paxIds,
                                                                                groupIds,
                                                                                validationType,
                                                                                Boolean.TRUE);
        assertTrue(!errorMessages.isEmpty());
        assertEquals(NominationConstants.NO_ELIGIBLE_RECEIVERS, errorMessages.get(0));
    }

    @Test
    public void test_validateNomination_receivers_ineligibleSubmitter() {
        NominationRequestDTO dto = createDefaultNominationRequest(PROGRAM_INELIGIBLE_SUBMITTER);
        List<Long> paxIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_MUDDAM);
        List<Long> groupIds = new ArrayList<>();
        String validationType = TEST_VALIDATION_TYPE;
        List<ErrorMessage> errorMessages = nominationService.validateNomination(TestConstants.PAX_HARWELLM,
                                                                                dto,
                                                                                paxIds,
                                                                                groupIds,
                                                                                validationType,
                                                                                Boolean.TRUE);
        assertTrue(!errorMessages.isEmpty());
        assertEquals(NominationConstants.INELIGIBLE_SUBMITTER, errorMessages.get(0));
    }

    @Test
    public void test_validateNomination_receivers_happyPath() {
        NominationRequestDTO dto = createDefaultNominationRequest(TestConstants.PROGRAM_RECEIVERS_HAPPY_PATH);
        List<Long> paxIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_MUDDAM);
        List<Long> groupIds = new ArrayList<>();
        String validationType = TEST_VALIDATION_TYPE;
        List<ErrorMessage> errorMessages = nominationService.validateNomination(TestConstants.PAX_HAGOPIWL,
                                                                                dto,
                                                                                paxIds,
                                                                                groupIds,
                                                                                validationType,
                                                                                Boolean.TRUE);
        assertTrue(!errorMessages.isEmpty());
        if (!errorMessages.isEmpty()) {
            assertTrue(!errorMessages.contains(NominationConstants.INELIGIBLE_SUBMITTER));
            assertTrue(!errorMessages.contains(NominationConstants.INELIGIBLE_PAX_RECEIVER));
            assertTrue(!errorMessages.contains(NominationConstants.INELIGIBLE_GROUP_RECEIVER));
            assertTrue(!errorMessages.contains(NominationConstants.MISSING_RECIEVERS_MESSAGE));
        }
    }
    
    @Test
    public void test_create_nomination_misc_payout() {
        budgetEligibilityCacheService.buildCache();
        ProgramAwardTier programAwardTier = programAwardTierDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(1)
                .and(ProjectConstants.MIN_AMOUNT).eq(30.00)
                .and(ProjectConstants.MAX_AMOUNT).eq(30.00)
                .findOne();
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(141L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(1328L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        receivers.add(
                new GroupMemberAwardDTO().setPaxId(3265L).setAwardAmount(30).setOriginalAmount(30).setAwardTierId(programAwardTier.getId()));
        dto.setReceivers(receivers);
        dto.setProgramId(TestConstants.ID_1);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("Test rec please ignore");
        dto.setComment(TestConstants.COMMENT);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setIsPrivate(true);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);

        authenticationService.authenticate(TestConstants.USER_PORTERGA);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_PORTERGA, dto, Boolean.TRUE);
        Assert.assertNotNull("Should have created payout type misc", nominationMiscDao.findBy()
            .where("nominationId").eq(nominationDetail.getNominationId())
            .and("vfName").eq(VfName.PAYOUT_TYPE.name())
            .findOne());
        
    }

    @Test
    public void test_group_nomination() throws Exception {
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(1534L).setAwardAmount(10).setOriginalAmount(10).setAwardTierId(2003L)));
        dto.setProgramId(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);
        dto.setIsPrivate(true);
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_MUDDAM, dto, Boolean.TRUE);
        Assert.assertEquals("Should be 59 recognitions", recognitionDao.findBy()
                .where("nominationId").eq(nominationDetail.getNominationId()).find().size(), 59);
    }

    @Test
    public void test_personal_group_nomination() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        Groups personalGroup = groupsDao.findBy()
                .where(ProjectConstants.GROUP_DESC).eq(PERSONAL_GROUP_DESC_1)
                .and(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .findOne();
        
        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(personalGroup.getGroupId()).setAwardAmount(10).setOriginalAmount(10).setAwardTierId(2003L)));
        dto.setProgramId(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);
        dto.setIsPrivate(true);
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_MUDDAM, dto, Boolean.TRUE);
        Assert.assertEquals("Should be 1 recognition", recognitionDao.findBy()
                .where("nominationId").eq(nominationDetail.getNominationId()).find().size(), 1);
    }

    @Test
    public void test_personal_group_nomination_invalid() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        Groups personalGroup = groupsDao.findBy()
                .where(ProjectConstants.GROUP_DESC).eq(PERSONAL_GROUP_DESC_2)
                .and(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .findOne();
        
        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(personalGroup.getGroupId()).setAwardAmount(10).setOriginalAmount(10).setAwardTierId(2003L)));
        dto.setProgramId(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);
        dto.setIsPrivate(true);
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        try {
            nominationService.createStandardNomination(TestConstants.PAX_MUDDAM, dto, Boolean.TRUE);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
            assertThat(e.getMessage(), is(NominationConstants.ERROR_INELIGIBLE_PAX_RECEIVER_MSG));
        }
    }

    @Test
    public void test_personal_group_nomination_exclude_logged_user() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        Groups personalGroup = groupsDao.findBy()
                .where(ProjectConstants.GROUP_DESC).eq(PERSONAL_GROUP_DESC_3)
                .and(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MUDDAM)
                .findOne();
        
        NominationRequestDTO dto = new NominationRequestDTO();
        dto.setReceivers(Arrays.asList(new GroupMemberAwardDTO().setGroupId(personalGroup.getGroupId()).setAwardAmount(10).setOriginalAmount(10).setAwardTierId(2003L)));
        dto.setProgramId(TestConstants.ID_1);
        dto.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_1));
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setBudgetId(TestConstants.ID_1);
        dto.setPayoutType(TEST_PAYOUT_TYPE_DPP);
        dto.setIsPrivate(true);
        
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        NominationDetailsDTO nominationDetail = nominationService.createStandardNomination(TestConstants.PAX_MUDDAM, dto, Boolean.TRUE);
        List<Recognition> recognition = recognitionDao.findBy()
                .where("nominationId").eq(nominationDetail.getNominationId()).find();

        Assert.assertEquals("Should be 1 recognition", recognition.size(), 1);
        Assert.assertNotEquals("Should not be Logged user", recognition.get(0).getReceiverPaxId(), TestConstants.PAX_MUDDAM);
    }

    private NominationDetailsDTO createDefaultNomination(Long programId){
        budgetEligibilityCacheService.buildCache();
        NominationRequestDTO dto = createDefaultNominationRequest(programId);

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        return nominationService.createStandardNomination(TestConstants.PAX_HARWELLM, dto, Boolean.TRUE);
    }

    private NominationRequestDTO createDefaultNominationRequest(Long programId) {
        NominationRequestDTO dto = new NominationRequestDTO();
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<>();
        receivers.add(new GroupMemberAwardDTO().setPaxId(TestConstants.PAX_MUDDAM));
        dto.setReceivers(receivers);
        dto.setProgramId(programId);
        dto.setImageId(30L);
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(TestConstants.ID_2);
        dto.setRecognitionCriteriaIds(criteria);
        dto.setHeadline("Test rec please ignore");
        dto.setComment(TestConstants.COMMENT);
        dto.setIsPrivate(false);

        return dto;
    }
}
