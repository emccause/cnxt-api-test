package com.maritz.culturenext.recognition.services;

import javax.inject.Inject;

import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.batch.service.BatchService;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.recognition.dto.RecognitionBulkResponseDTO;
import com.maritz.culturenext.recognition.services.RecognitionBulkUploadService;
import com.maritz.test.AbstractDatabaseTest;

public class RecognitionBulkUploadIntegrationTest extends AbstractDatabaseTest {
    
    private static final String TEST_FILE_NAME = "integration_test_adv_rec_bulk.csv";
    private static final String TEST_XLSX_FILE_NAME = "integration_test_adv_rec_bulk.xlsx";
    private static final String TEST_CONTENT_TYPE = "text/csv";
    private static final String TEST_XLSX_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Inject private BatchService batchService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private RecognitionBulkUploadService recognitionBulkUploadService;
    @Inject private ConcentrixDao<Batch> batchDao;
    
    @Test
    public void testUploadProcess() throws Exception {
        
        budgetEligibilityCacheService.buildCache();
        
        MultipartFile file = new MockMultipartFile(TEST_FILE_NAME, TEST_FILE_NAME, TEST_CONTENT_TYPE, 
                new ClassPathResource(TEST_FILE_NAME).getInputStream());
        RecognitionBulkResponseDTO responseDto = 
                recognitionBulkUploadService.recognitionsBulkUpload(TestConstants.PAX_MUDDAM, file);
        
        BatchStatusDTO statusDto = new BatchStatusDTO();
        statusDto.setBatchId(responseDto.getBatchId());
        statusDto.setStatus(StatusTypeCode.QUEUED.name());
        batchService.updateBatchStatus(statusDto);

        // re-load batch to make sure it was updated successfully
        Batch batch = batchDao.findById(responseDto.getBatchId());

        Assert.assertEquals("Batch should have the new status.", StatusTypeCode.QUEUED.name(), batch.getStatusTypeCode());

        // Run batch job
        recognitionBulkUploadService.runBatchJob();
        
        // re-load batch to make sure it processed correctly
        batch = batchDao.findById(responseDto.getBatchId());
        
        Assert.assertEquals("Batch should be complete without errors.",  StatusTypeCode.COMPLETE.name(), batch.getStatusTypeCode());
    }

    @Test
    public void testUploadSuccessfullySavesAsCsv() throws Exception {
        budgetEligibilityCacheService.buildCache();

        MultipartFile file = new MockMultipartFile(TEST_XLSX_FILE_NAME, TEST_XLSX_FILE_NAME, TEST_XLSX_TYPE,
                new ClassPathResource(TEST_XLSX_FILE_NAME).getInputStream());
        RecognitionBulkResponseDTO responseDto =
                recognitionBulkUploadService.recognitionsBulkUpload(TestConstants.PAX_MUDDAM, file);

        Assert.assertTrue(responseDto.getFileName().endsWith(".csv"));
    }

}
