package com.maritz.culturenext.recognition.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.impl.RaiseServiceImpl;
import com.maritz.test.AbstractDatabaseTest;

public class RaiseServiceDataRetrievalTest extends AbstractDatabaseTest{

    private static final String DPP = "DPP";
    @Inject private RaiseServiceImpl raiseService;
    
    @Test
    public void testSetPayoutTypeAssociatedWithProgram() {
        assertNotNull("service was null but should not!", raiseService);
        NominationRequestDTO validateNominationRequestDTO = new NominationRequestDTO();
        validateNominationRequestDTO.setProgramId(3L);
        raiseService.setPayoutTypeAssociatedWithProgram(validateNominationRequestDTO);
        assertNotNull("payout type was null and should not!", validateNominationRequestDTO.getPayoutType());
        assertEquals("payout retrieved was differend from expected",DPP,validateNominationRequestDTO.getPayoutType());
    }

}
