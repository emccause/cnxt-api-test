package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import com.maritz.TestUtil;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.jpa.repository.TransactionHeaderRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.services.impl.TransactionBatch;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.recognition.constants.TransactionConstants;
import com.maritz.culturenext.recognition.dao.TransactionDetailsDao;
import com.maritz.culturenext.recognition.dto.TransactionDetailsDTO;
import com.maritz.culturenext.recognition.dto.TransactionReversalDTO;
import com.maritz.culturenext.recognition.services.impl.TransactionServiceImpl;
import com.maritz.test.AbstractMockTest;

public class TransactionServiceTest extends AbstractMockTest {

    @Mock Security security;
    @Mock TransactionDetailsDao transactionDetailsDao;
    @Mock TransactionHeaderRepository transactionHeaderRepository;
    @Mock com.maritz.core.services.TransactionService transactionService;

    private static final String TEST_PROGRAM_ID_REQUEST = "2";
    private static final String TEST_START_DATE_REQUEST = "2015-05-01";
    private static final String TEST_END_DATE_REQUEST = "2015-05-01";
    private static final String TEST_AWARD_CODE_REQUEST = "25";
    private static final String TEST_STATUS_REQUEST = "TESTING";
    private static final String TEST_RECIPIENT_PAX_REQUEST = "5";
    private static final String TEST_ISSUED_BY_PAX_REQUEST = "5";
    private static final String TEST_PROGRAM_TYPE_REQUEST = "AWARD_CODE";
    private static final Long TEST_NOMINATION_ID = 12345L;
    private static final Long TEST_TRANSACTION_ID = 98765L;
    private static final Long TEST_SUBMITTER_ID = 100L;
    private static final Long TEST_RECIPIENT_ID = 101L;
    private static final Double TEST_AWARD_AMOUNT = 5D;
    private static final Integer TEST_RECIPIENT_COUNT = 2;
    private static final Integer TEST_TOTAL_RESULTS = 1;

    TransactionServiceImpl transactionServiceImpl;

    @Before
    public void setup() {
        transactionServiceImpl = new TransactionServiceImpl()
            .setTransactionDetailsDao(transactionDetailsDao)
            .setSecurity(security)
            .setTransactionService(transactionService)
        ;

        when(transactionDetailsDao.getTransactionSearch(anyLong(), anyListOf(String.class), anyListOf(String.class)
                , anyListOf(String.class), anyListOf(String.class), anyListOf(String.class), anyListOf(String.class)
                , any(Date.class), any(Date.class), anyInt(), anyInt()))
        .thenReturn(transactionSearchMockResults());

        when(security.getPaxId()).thenReturn(TestConstants.ID_1);
    }

    @Test
    public void test_getTransactionsSearch_success() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 10);

        assertNotNull(result);
        assertNotNull(result.getData());

        TransactionDetailsDTO transaction = result.getData().get(0);
        assertEquals(TEST_AWARD_CODE_REQUEST, transaction.getAwardCode());
        assertEquals(TEST_STATUS_REQUEST, transaction.getStatus());
        assertEquals(TEST_NOMINATION_ID, transaction.getNominationId());
        assertEquals(TEST_AWARD_AMOUNT, transaction.getAwardAmount());
        assertEquals(TEST_SUBMITTER_ID, transaction.getIssuedByPax().getPaxId());
        assertEquals(TEST_RECIPIENT_ID, transaction.getRecipientPax().getPaxId());
        assertEquals(TEST_TRANSACTION_ID, transaction.getTransactionId());
        assertEquals(TEST_RECIPIENT_COUNT, transaction.getRecipientCount());
        assertEquals(TEST_PROGRAM_TYPE_REQUEST, transaction.getProgramType());
    }

    @Test
    public void test_getTransactionsSearch_empty_programId_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, null
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_empty_awardCode_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , null, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_empty_status_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, null, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_empty_recipientPaxId_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, null, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_empty_submitterPaxId_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, null
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_empty_programType_filter() {
        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , null, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());
    }

    @Test
    public void test_getTransactionsSearch_no_data() {
        // No data found
        when(transactionDetailsDao.getTransactionSearch(anyLong(), anyListOf(String.class), anyListOf(String.class)
                , anyListOf(String.class), anyListOf(String.class), anyListOf(String.class), anyListOf(String.class)
                , any(Date.class), any(Date.class), anyInt(), anyInt()))
        .thenReturn(new ArrayList<>());

        PaginatedResponseObject<TransactionDetailsDTO> result = transactionServiceImpl.getTransactionsSearch(null, TEST_PROGRAM_ID_REQUEST
                , TEST_AWARD_CODE_REQUEST, TEST_STATUS_REQUEST, TEST_RECIPIENT_PAX_REQUEST, TEST_ISSUED_BY_PAX_REQUEST
                , TEST_PROGRAM_TYPE_REQUEST, TEST_START_DATE_REQUEST, TEST_END_DATE_REQUEST, 1, 5);

        assertNotNull(result);
        assertNotNull(result.getData());

    }

    private List<Map<String, Object>> transactionSearchMockResults() {
        List<Map<String, Object>> result = new ArrayList<>();
        Map<String, Object> row = new HashMap<>();
        row.put(ProjectConstants.AWARD_CODE, TEST_AWARD_CODE_REQUEST);
        row.put(ProjectConstants.CREATE_DATE, new Date());
        row.put(ProjectConstants.STATUS, TEST_STATUS_REQUEST);
        row.put(ProjectConstants.SUBMITTER_ID, TEST_SUBMITTER_ID);
        row.put(ProjectConstants.RECIPIENT_ID, TEST_RECIPIENT_ID);
        row.put(ProjectConstants.NOMINATION_ID, TEST_NOMINATION_ID);
        row.put(ProjectConstants.TRANSACTION_ID, TEST_TRANSACTION_ID);
        row.put(ProjectConstants.PROGRAM_TYPE, TEST_PROGRAM_TYPE_REQUEST);
        row.put(ProjectConstants.AWARD_AMOUNT, TEST_AWARD_AMOUNT);
        row.put(ProjectConstants.RECIPIENT_COUNT, TEST_RECIPIENT_COUNT);
        row.put(ProjectConstants.TOTAL_RESULTS_KEY, TEST_TOTAL_RESULTS);
        result.add(row);

        return result;
    }

    /*
    @Test
    public void test_reverse_transactions_success() throws Exception {

        when(transactionDetailsDao.getTransactionsToReverse(anyListOf(Long.class))).thenReturn(Arrays.asList(45198L));

        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(Arrays.asList(45198L));
        reversal.setReversalComment("Unit Test");

        transactionServiceImpl.reverseTransactions(reversal);

        verify(transactionDetailsDao, times(1)).getTransactionsToReverse((anyListOf(Long.class)));
        verify(transactionDetailsDao, times(1)).reverseTransactions(eq("45198"), eq("Unit Test"), anyLong());
    }
    */

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Test
    public void reverseTransactions_success() throws Exception {
        List<Long> idsToReverse = Arrays.asList(45198L);
        List<TransactionHeader> headersToReverse = Arrays.asList(
            new TransactionHeader()
                .setId(45198L)
        );

        when(transactionDetailsDao.getTransactionsToReverse(idsToReverse)).thenReturn(headersToReverse);

        when(transactionService.findByHeaders(headersToReverse)).thenReturn(Arrays.asList(
            new TransactionHeaderDetail<>(
                headersToReverse.get(0)
                ,new Discretionary()
                    .setId(1L)
                    .setTransactionHeaderId(45198L)
                    .setBudgetIdCredit(10L)
                    .setBudgetIdDebit(11L)
                    .setAmount(2.0)
                ,Arrays.asList(
                    new TransactionHeaderMisc()
                        .setTransactionHeaderId(45198L)
                        .setVfName("VFNAME1")
                        .setMiscData("DATA1")
                )
            )
        ));

        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(idsToReverse);
        reversal.setReversalComment("Unit Test");

        transactionServiceImpl.reverseTransactions(reversal);

        verify(transactionDetailsDao, times(1)).getTransactionsToReverse((idsToReverse));
        verify(transactionService, times(1)).findByHeaders(headersToReverse);
        verify(transactionDetailsDao, times(1)).reverseTransactions(idsToReverse, reversal.getReversalComment(), security.getPaxId());

        ArgumentCaptor<TransactionBatch> transactionServiceCreateCapture = ArgumentCaptor.forClass(TransactionBatch.class);
        verify(transactionService, times(1)).create(transactionServiceCreateCapture.capture());

        TransactionBatch<Discretionary> tb = transactionServiceCreateCapture.getValue();
        TransactionHeader header = tb.getHeaders().get(0);
        assertThat(header.getId(), nullValue());
        assertThat(header.getStatusTypeCode(), is(StatusTypeCode.APPROVED.name()));

        Discretionary detail = tb.getDetails().get(0);
        assertThat(detail.getId(), nullValue());
        assertThat(detail.getTransactionHeaderId(), nullValue());
        assertThat(detail.getBudgetIdCredit(), is(10L));
        assertThat(detail.getBudgetIdDebit(), is(11L));
        assertThat(detail.getAmount(), is(-2.0));

        TransactionHeaderMisc misc = tb.getMiscs().get(0);
        assertThat(misc.getId(), nullValue());
        assertThat(misc.getVfName(), is("VFNAME1"));
        assertThat(misc.getMiscData(), is("DATA1"));
    }

    @Test
    public void reverseTransactions_pending_id() throws Exception {

        when(transactionDetailsDao.getTransactionsToReverse(anyListOf(Long.class))).thenReturn(null);

        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(Arrays.asList(48350L));
        reversal.setReversalComment("Unit Test");

        transactionServiceImpl.reverseTransactions(reversal);

        verify(transactionDetailsDao, times(1)).getTransactionsToReverse((anyListOf(Long.class)));
        verify(transactionDetailsDao, times(0)).reverseTransactions(anyListOf(Long.class), anyString(), anyLong());
    }

    @Test
    public void reverseTransactions_null_ids() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setReversalComment("Unit Test");

        try {
            transactionServiceImpl.reverseTransactions(reversal);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, TransactionConstants.ERROR_MISSING_TRANSACTION_IDS, errorMsgException);
        }
    }

    @Test
    public void reverseTransactions_empty_ids() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(new ArrayList<Long>());
        reversal.setReversalComment("Unit Test");

        try {
            transactionServiceImpl.reverseTransactions(reversal);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, TransactionConstants.ERROR_MISSING_TRANSACTION_IDS, errorMsgException);
        }
    }

    @Test
    public void reverseTransactions_null_comment() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(Arrays.asList(45198L));

        try {
            transactionServiceImpl.reverseTransactions(reversal);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, TransactionConstants.ERROR_MISSING_REVERSAL_COMMENT, errorMsgException);
        }
    }

    @Test
    public void reverseTransactions_empty_comment() throws Exception {
        TransactionReversalDTO reversal = new TransactionReversalDTO();
        reversal.setTransactionIds(Arrays.asList(45198L));
        reversal.setReversalComment("");

        try {
            transactionServiceImpl.reverseTransactions(reversal);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, TransactionConstants.ERROR_MISSING_REVERSAL_COMMENT, errorMsgException);
        }
    }
}



