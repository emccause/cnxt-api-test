package com.maritz.culturenext.recognition.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.recognition.dto.RecognitionCriteriaDTO;
import com.maritz.culturenext.recognition.services.RecognitionCriteriaService;
import com.maritz.test.AbstractDatabaseTest;

public class RecognitionCriteriaServiceTest extends AbstractDatabaseTest {

    @Inject private RecognitionCriteriaService recCriteriaService;
    
    @Test
    public void test_get_recognition_criteria_list_default_status() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList = recCriteriaService.getRecognitionCriteria(null, null, null);
        if (recCriteriaDTOList != null && recCriteriaDTOList.size() > 0) {
            for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {                    
                assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.ACTIVE.name())
                        || recCriteriaDTO.getStatus().equals(StatusTypeCode.INACTIVE.name()));
            }
        }
    }
    
    @Test
    public void test_get_recognition_criteria_list_multiple_valid_status() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList =
                recCriteriaService.getRecognitionCriteria(StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.INACTIVE.name(), null, null);
        if (recCriteriaDTOList != null && recCriteriaDTOList.size() > 0) {
            for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {                    
                assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.ACTIVE.name())
                        || recCriteriaDTO.getStatus().equals(StatusTypeCode.INACTIVE.name()));
            }
        }
    }
    
    @Test
    public void test_get_recognition_criteria_list_active_status() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList = recCriteriaService.getRecognitionCriteria(StatusTypeCode.ACTIVE.name(), null, null);
        if (recCriteriaDTOList != null && recCriteriaDTOList.size() > 0) {
            for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {                    
                assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()));
            }
        }
    }
    
    @Test
    public void test_get_recognition_criteria_list_inactive_status() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList = recCriteriaService.getRecognitionCriteria(StatusTypeCode.INACTIVE.name(), null, null);
        if (recCriteriaDTOList != null && recCriteriaDTOList.size() > 0) {
            for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {                    
                assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.INACTIVE.name()));
            }
        }
    }
    
    @Test
    public void test_get_recognition_criteria_list_invalid_status() throws Exception {
        try {
            recCriteriaService.getRecognitionCriteria("invalid", null, null);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("INVALID_STATUS"));                
            }
        }    
    }

    @Test
    public void test_get_recognition_criteria_list_french() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList = recCriteriaService.getRecognitionCriteria(null, LocaleCodeEnum.fr_CA.name(), null);
        if (recCriteriaDTOList != null && recCriteriaDTOList.size() > 0) {
            for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {
                assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.ACTIVE.name())
                        || recCriteriaDTO.getStatus().equals(StatusTypeCode.INACTIVE.name()));

                if (recCriteriaDTO.getId() == 1) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Exploit"));
                } else if (recCriteriaDTO.getId() == 2) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Priorité au client"));
                } else if (recCriteriaDTO.getId() == 3) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Amusant"));
                } else if (recCriteriaDTO.getId() == 4) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Personnes"));
                } else if (recCriteriaDTO.getId() == 5) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Persévérance"));
                } else if (recCriteriaDTO.getId() == 6) {
                    assertTrue(recCriteriaDTO.getDisplayName().equals("Travail d'équipe"));
                }
            }
        }
    }
    
    @Test
    public void test_get_recognition_criteria_list_by_ids() throws Exception {
        List<RecognitionCriteriaDTO> recCriteriaDTOList = recCriteriaService.getRecognitionCriteria(null, null, "1,2,3,4,5,6");
        List<Long> idsToFetch = Arrays.asList(1L,2L,3L,4L,5L,6L);
        assertThat(recCriteriaDTOList.size(), is(6));
        for (RecognitionCriteriaDTO recCriteriaDTO : recCriteriaDTOList) {                    
            assertTrue(recCriteriaDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()));
            assertTrue(idsToFetch.contains(recCriteriaDTO.getId()));
        }
    }
    
    @Test
    public void test_get_recognition_criteria_invalid_id() throws Exception {
        try {
            recCriteriaService.getRecognitionCriteria("", null, "NaN");
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("INVALID_IDS"));                
            }
        }    
    }
    
    @Test 
    public void test_insert_recognition_criteria() throws Exception {
        List <RecognitionCriteriaDTO> recognitionCriteriaDTOList = new ArrayList<>();
        RecognitionCriteriaDTO recognitionCriteriaDTO = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setDescription("junit test");
        recognitionCriteriaDTO.setDisplayName("junit test");
                
        recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
        recCriteriaService.insertRecognitionCriteria( recognitionCriteriaDTOList);
        
        List <RecognitionCriteriaDTO> returnedRecognitionCriteriaDTOList =
                recCriteriaService.getRecognitionCriteria(StatusTypeCode.ACTIVE.name(), null, null);
        boolean haveDTO = false;
        for (RecognitionCriteriaDTO returnedRecognitionCriteriaDTO : returnedRecognitionCriteriaDTOList ) {
            if (returnedRecognitionCriteriaDTO.getDisplayName().equals("junit test")) {
                haveDTO = true;
            }                        
        }
        assertTrue(haveDTO == true);
        
    }
    
    @Test 
    public void test_insert_recognition_criteria_name_null() throws Exception {
        List <RecognitionCriteriaDTO> recognitionCriteriaDTOList = new ArrayList<>();
        RecognitionCriteriaDTO recognitionCriteriaDTO = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setDescription("junit test");        
                
        recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
        
        try {
            recCriteriaService.insertRecognitionCriteria( recognitionCriteriaDTOList);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_NAME_NULL"));                
            }
        }        
    }
    
    @Test 
    public void test_insert_recognition_criteria_name_too_long() throws Exception {
        List <RecognitionCriteriaDTO> recognitionCriteriaDTOList = new ArrayList<>();
        RecognitionCriteriaDTO recognitionCriteriaDTO = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setDisplayName("longNamelongNamelongNamelongNamelongNamelongNamelongName"
                + "longNamelongNamelongNamelongNamelongNamelongNamelongNamelongNamelongNamelongNamelongName");
                
        recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
        
        try {
            recCriteriaService.insertRecognitionCriteria( recognitionCriteriaDTOList);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_NAME_TOO_LONG"));                
            }
        }        
    }        
    
    @Test 
    public void test_insert_recognition_description_too_long() throws Exception {
        List <RecognitionCriteriaDTO> recognitionCriteriaDTOList = new ArrayList<>();
        RecognitionCriteriaDTO recognitionCriteriaDTO = new RecognitionCriteriaDTO();
        recognitionCriteriaDTO.setDisplayName("junit test name");
        recognitionCriteriaDTO.setDescription("longDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescription1");        
                
        recognitionCriteriaDTOList.add(recognitionCriteriaDTO);
        
        try {
            recCriteriaService.insertRecognitionCriteria( recognitionCriteriaDTOList);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_DESCRIPTION_TOO_LONG"));                
            }
        }        
    }
    
    @Test 
    public void test_update_recognition_criteria() throws Exception {
        RecognitionCriteriaDTO recognitionCriteriaDTO = recCriteriaService.getRecognitionCriteriaById(TestConstants.ID_1, null);
        recognitionCriteriaDTO.setDescription("junit test update");
                
        
        RecognitionCriteriaDTO returnedRecognitionCriteriaDTO = 
                recCriteriaService.updateRecognitionCriteria( recognitionCriteriaDTO, TestConstants.ID_1);
        assertThat(returnedRecognitionCriteriaDTO.getDescription(), is("junit test update"));
    }
    
    @Test 
    public void test_update_recognition_criteria_name_null() throws Exception {
        RecognitionCriteriaDTO recognitionCriteriaDTO = recCriteriaService.getRecognitionCriteriaById(TestConstants.ID_1, null);
        recognitionCriteriaDTO.setDisplayName(null);
        recognitionCriteriaDTO.setDescription("junit test");
                
        try {
            recCriteriaService.updateRecognitionCriteria( recognitionCriteriaDTO, TestConstants.ID_1);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_NAME_NULL"));                
            }
        }
    }
    
    @Test 
    public void test_update_recognition_criteria_name_too_long() throws Exception {
        RecognitionCriteriaDTO recognitionCriteriaDTO = recCriteriaService.getRecognitionCriteriaById(TestConstants.ID_1, null);
        recognitionCriteriaDTO.setDisplayName("longNamelongNamelongNamelongNamelongNamelongNamelongName"
                + "longNamelongNamelongName");
        recognitionCriteriaDTO.setDescription("junit test");
                
        try {
            recCriteriaService.updateRecognitionCriteria( recognitionCriteriaDTO, TestConstants.ID_1);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_NAME_TOO_LONG"));                
            }
        }
    }
    
    @Test 
    public void test_update_recognition_criteria_description_too_long() throws Exception {
        RecognitionCriteriaDTO recognitionCriteriaDTO = recCriteriaService.getRecognitionCriteriaById(TestConstants.ID_1, null);
        recognitionCriteriaDTO.setDisplayName("junit test");
        recognitionCriteriaDTO.setDescription("longDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescriptionlongDescription"
                + "longDescription1");
                
        try {
            recCriteriaService.updateRecognitionCriteria( recognitionCriteriaDTO, TestConstants.ID_1);
        } catch (ErrorMessageException errorMsgException) {            
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_DESCRIPTION_TOO_LONG"));                
            }
        }
    }
}

