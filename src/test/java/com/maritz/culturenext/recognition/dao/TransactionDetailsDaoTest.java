package com.maritz.culturenext.recognition.dao;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isIn;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class TransactionDetailsDaoTest extends AbstractDatabaseTest {

    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private TransactionDetailsDao transactionDetailsDao;

    private static final Long transactionWithRaises = 43253L; //Has two raises tied to it

    private static final Long approvedTransaction = 45198L;
    private static final Long pendingTransaction = 48350L;
    private static final Long fundingTransaction = 48376L;

    @Test
    public void getTransactionsToReverse_with_raises() throws Exception {
        List<Long> raiseTransactionIdList = transactionHeaderDao.findBy()
            .where(ProjectConstants.PAX_GROUP_ID).eq(TestConstants.PAX_HAGOPIWL)
            .and(ProjectConstants.PROGRAM_ID).eq(3L)
            .and(ProjectConstants.BATCH_ID).isNull()
            .find(ProjectConstants.ID, Long.class)
        ;
        assertThat(raiseTransactionIdList.size(), is(2));

        List<TransactionHeader> headers = transactionDetailsDao.getTransactionsToReverse(Arrays.asList(transactionWithRaises));
        assertThat(headers.size(), is(3));

        List<Long> transactionsIdsToFind = new ArrayList<>();
        transactionsIdsToFind.add(transactionWithRaises);
        transactionsIdsToFind.addAll(raiseTransactionIdList);

        for (TransactionHeader header : headers) {
            assertThat(header.getId(), isIn(transactionsIdsToFind));
        }
    }

    @Test
    public void getTransactionsToReverse_only_approved() throws Exception {
        List<TransactionHeader> headers = transactionDetailsDao.getTransactionsToReverse(
            Arrays.asList(approvedTransaction, pendingTransaction, fundingTransaction)
        );

        assertThat(headers.size(), is(1));
        assertThat(headers.get(0).getId(), is(approvedTransaction));
    }
}