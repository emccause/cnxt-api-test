package com.maritz.culturenext.recognition.dao;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.recognition.dao.NominationDetailsDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class NominationDetailsDaoUnitTest extends AbstractDatabaseTest {

    private static Long TEST_PROGRAM_ID = 3L;
    private static String TEST_HEADLINE = "This is a headline comment!";
    private static String TEST_COMMENT = "This is a private comment!";
    private static String TEST_PAYOUT_TYPE = "CASH";
    private static Long TEST_BUDGET_ID = 4L;
    private static Long TEST_NEWSFEED_ITEM_ID = 5L;

    @Inject private NominationDetailsDao nominationDetailsDao;
    
    @Test
    public void test_get_nomination_common_info() throws Exception {
        List<Map<String, Object>> nodes = nominationDetailsDao.getNominationCommonInfo(Arrays.asList(TestConstants.ID_1));
        
        for (Map<String, Object> node : nodes) {
            assertThat((Long) node.get(ProjectConstants.NOMINATION_ID), is(TestConstants.ID_1));
            assertThat((String) node.get(ProjectConstants.STATUS_TYPE_CODE), is(StatusTypeCode.APPROVED.toString()));
            assertThat((Long) node.get(ProjectConstants.SUBMITTER_PAX_ID), is(TestConstants.ID_2));
            assertThat((Long) node.get(ProjectConstants.PROGRAM_ID), is(TEST_PROGRAM_ID));
            assertThat((String) node.get(ProjectConstants.HEADLINE_COMMENT), is(TEST_HEADLINE));
            assertThat((String) node.get(ProjectConstants.COMMENT), is(TEST_COMMENT));
            assertThat((String) node.get(ProjectConstants.PAYOUT_TYPE), is(TEST_PAYOUT_TYPE));
            assertThat((Long) node.get(ProjectConstants.BUDGET_ID), is(TEST_BUDGET_ID));
            assertThat((String) node.get(ProjectConstants.PROGRAM_TYPE_CODE), is(ProgramTypeEnum.PEER_TO_PEER.toString()));
            assertThat((Long) node.get(ProjectConstants.NEWSFEED_ITEM_ID), is(TEST_NEWSFEED_ITEM_ID));
        }
    }
}
