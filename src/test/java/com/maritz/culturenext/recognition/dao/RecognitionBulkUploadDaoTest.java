package com.maritz.culturenext.recognition.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.recognition.constants.RecognitionBulkUploadConstants;
import com.maritz.culturenext.recognition.dao.RecognitionBulkUploadDao;
import com.maritz.test.AbstractDatabaseTest;

public class RecognitionBulkUploadDaoTest extends AbstractDatabaseTest {
    
    @Inject private RecognitionBulkUploadDao recognitionBulkUploadDao;
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private ConcentrixDao<BatchEvent> batchEventDao;
    @Inject private ConcentrixDao<BatchFile> batchFileDao;

    private static final Long BATCH_WITH_REPEATED_BATCH_EVENT = 23538L;

    
    
    private static final String TEST_FILE_NAME = "test_adv_rec_bulk_upload_history.csv";
    private static final String STATUS_LIST = StatusTypeCode.FAILED.toString();
    private static final String FILENAME = "filename"; //See how different it is?
    
    private String batchEventTotalRows;
    private String batchEventTotalAwards;
    private String batchEventNominations;
    
    @Test
    public void test_adv_rec_bulk_upload_history() {
        
        BatchFile batchFile = batchFileDao.findBy()
                .where(FILENAME)
                .eq(TEST_FILE_NAME)
                .findOne();
        
        Batch batch = batchDao.findBy()
                .where(ProjectConstants.BATCH_ID)
                .eq(batchFile.getBatchId())
                .findOne();
        
        List<BatchEvent> batchEventList = batchEventDao.findBy()
                .where(ProjectConstants.BATCH_ID)
                .eq(batchFile.getBatchId())
                .find();
        
        assertNotNull("Should be a batch file record inserted for bulk upload history", batchFile);
        assertNotNull("Should be a batch record inserted for bulk upload history", batch);
        assertNotNull("Should be batch event records inserted for bulk upload history", batchEventList);
        assertTrue("Should be multiple bulk upload history records in batch event", batchEventList.size() > 1);
        
        for (BatchEvent batchEvent : batchEventList) {
            if (BatchEventType.TOTAL_AWARD_AMOUNT.toString().equals(batchEvent.getBatchEventTypeCode())) {
                batchEventTotalRows = batchEvent.getMessage();
            } else if (BatchEventType.NOMINATION_COUNT.toString().equals(batchEvent.getBatchEventTypeCode())) {
                batchEventTotalAwards = batchEvent.getMessage();
            } else if (BatchEventType.RECORDS.toString().equals(batchEvent.getBatchEventTypeCode())) {
                batchEventNominations = batchEvent.getMessage();
            }
        }
        
        List<Map<String, Object>> bulkUploadHistoryEntries = 
                recognitionBulkUploadDao.getBulkUploadHistory(STATUS_LIST, 
                        ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE);
        
        assertNotNull("Should get data back from bulk upload history query", bulkUploadHistoryEntries);
        assertFalse("Should have found data for a bulk upload", bulkUploadHistoryEntries.isEmpty());
        
        for (Map<String, Object> bulkUploadHistoryEntry : bulkUploadHistoryEntries) { 
            if (TEST_FILE_NAME.equals(bulkUploadHistoryEntry.get(FILENAME))) {
                assertEquals(batchFile.getBatchId(), (Long) bulkUploadHistoryEntry.get(ProjectConstants.BATCH_ID));
                assertEquals(batchEventTotalRows, (String) bulkUploadHistoryEntry.get(RecognitionBulkUploadConstants.TOTAL_ROWS));
                assertEquals(batchEventTotalAwards, (String) bulkUploadHistoryEntry.get(ProjectConstants.TOTAL_AWARDS));
                assertEquals(batchEventNominations, (String) bulkUploadHistoryEntry.get(ProjectConstants.NOMINATIONS));
                assertEquals(batch.getStatusTypeCode(), (String) bulkUploadHistoryEntry.get(ProjectConstants.STATUS_TYPE_CODE));
            }
        }
    }
    
    @Test
    public void test_bulk_upload_history_negative_page_number() {
        
        try {
            recognitionBulkUploadDao.getBulkUploadHistory(STATUS_LIST, -1, ProjectConstants.MAX_PAGE_SIZE);
            fail("Should have thrown an error");
        } catch (ErrorMessageException eme) {
            TestUtil.assertValidationErrors(1, "PAGE_NUMBER_INVALID", eme);
        }
    }
    
    @Test
    public void test_bulk_upload_history_with_repeated_end_time_event(){
        try {
            List<Map<String, Object>> bulkUploadHistoryEntries = 
                    recognitionBulkUploadDao.getBulkUploadHistory(StatusTypeCode.COMPLETE.toString(), 
                    ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE);
            assertNotNull("bulk Upload history was null and should not!", bulkUploadHistoryEntries);
            List<BatchEvent> batchEvents = batchEventDao.findBy().where(ProjectConstants.BATCH_ID).eq(BATCH_WITH_REPEATED_BATCH_EVENT).find();
            int countExistingEndTimEvents = 0;
            for (BatchEvent batchEvent: batchEvents){
                if (BatchEventType.END_TIME.getCode().equals(batchEvent.getBatchEventTypeCode().trim())) {
                    ++countExistingEndTimEvents;
                }
            }
            assertEquals("Number of End Time Batch Events were different from expectd", 2, countExistingEndTimEvents);
        } catch (ErrorMessageException eme) {
            fail("Exception was thrown and should have not!");
        }
        
    }
}
