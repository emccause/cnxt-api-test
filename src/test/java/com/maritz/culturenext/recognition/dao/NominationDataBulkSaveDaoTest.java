package com.maritz.culturenext.recognition.dao;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.PaxGroupMisc;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.PaxGroupMiscRepository;
import com.maritz.core.jpa.repository.TransactionHeaderMiscRepository;
import com.maritz.core.jpa.repository.TransactionHeaderRepository;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import javax.inject.Inject;

import java.util.List;

public class NominationDataBulkSaveDaoTest extends AbstractDatabaseTest {

    @Inject private AddressRepository addressRepository;
    @Inject private ApplicationDataService applicationDataService;
    @Inject private PaxGroupMiscRepository paxGroupMiscRepository;
    @Inject private TransactionHeaderRepository transactionHeaderRepository;
    @Inject private TransactionHeaderMiscDao transactionHeaderMiscDao;
    @Inject private TransactionHeaderMiscRepository transactionHeaderMiscRepository;
    
    /* Test the Maritz custom logic for subproject overrides: (Test that the query is pulling correct data)
     *         Submitter, PAX_GROUP_MISC and ADDRESS
     *         Recipient, PAX_GROUP_MISC and ADDRESS
     */
    @Test
    public void test_subproject_override_submitter_cost_center() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("COST_CENTER/SUBMITTER");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROEJCT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            PaxGroupMisc pgm = paxGroupMiscRepository.findBy()
                    .where(ProjectConstants.PAX_GROUP_ID).eq(TestConstants.PAX_GROUP_PORTERGA)
                    .and(ProjectConstants.VF_NAME).eq(VfName.COST_CENTER.name()).findOne();
            Assert.assertNotNull("Test data wasn't created!", pgm);

            //Sub Project Number should have been mapped to the submitter's (Greg) Cost Center.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(pgm.getMiscData(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_submitter_area() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("AREA/SUBMITTER");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            PaxGroupMisc pgm = paxGroupMiscRepository.findBy()
                    .where(ProjectConstants.PAX_GROUP_ID).eq(TestConstants.PAX_GROUP_PORTERGA)
                    .and(ProjectConstants.VF_NAME).eq(VfName.AREA.name()).findOne();
            Assert.assertNotNull("Test data wasn't created!", pgm);

            //Sub Project Number should have been mapped to the submitter's (Greg) Area.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(pgm.getMiscData(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_submitter_city() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("CITY/SUBMITTER");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());

        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            Address addr = addressRepository.findBy()
                    .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                    .and(ProjectConstants.PREFERRED).eq(ProjectConstants.YES_CHAR).findOne();
            Assert.assertNotNull("Test data wasn't created!", addr);

            //Sub Project Number should have been mapped to the submitter's (Greg) City.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(addr.getCity(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_submitter_country_code() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("COUNTRY_CODE/SUBMITTER");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            Address addr = addressRepository.findBy()
                    .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_PORTERGA)
                    .and(ProjectConstants.PREFERRED).eq(ProjectConstants.YES_CHAR).findOne();
            Assert.assertNotNull("Test data wasn't created!", addr);

            //Sub Project Number should have been mapped to the submitter's (Greg) Country Code.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(addr.getCountryCode(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_recipient_cost_center() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("COST_CENTER/RECIPIENT");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            PaxGroupMisc pgm = paxGroupMiscRepository.findBy()
                    .where(ProjectConstants.PAX_GROUP_ID).eq(TestConstants.PAX_GROUP_HARWELLM)
                    .and(ProjectConstants.VF_NAME).eq(VfName.COST_CENTER.name()).findOne();
            Assert.assertNotNull("Test data wasn't created!", pgm);

            //Sub Project Number should have been mapped to the recipient's (Leah) Cost Center.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(pgm.getMiscData(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_recipient_area() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("AREA/RECIPIENT");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            PaxGroupMisc pgm = paxGroupMiscRepository.findBy()
                    .where(ProjectConstants.PAX_GROUP_ID).eq(TestConstants.PAX_GROUP_HARWELLM)
                    .and(ProjectConstants.VF_NAME).eq(VfName.AREA.name()).findOne();
            Assert.assertNotNull("Test data wasn't created!", pgm);

            //Sub Project Number should have been mapped to the recipient's (Leah) Area.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(pgm.getMiscData(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_recipient_city() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("CITY/RECIPIENT");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());
        
        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            Address addr = addressRepository.findBy()
                    .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_HARWELLM)
                    .and(ProjectConstants.PREFERRED).eq(ProjectConstants.YES_CHAR).findOne();
            Assert.assertNotNull("Test data wasn't created!", addr);

            //Sub Project Number should have been mapped to the recipient's (Leah) City.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(addr.getCity(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
    
    @Test
    public void test_subproject_override_recipient_country_code() throws Exception {
        ApplicationDataDTO appData = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SUBPROJECT_OVERRIDE);
        appData.setValue("COUNTRY_CODE/RECIPIENT");
        applicationDataService.setApplicationData(appData);
        
        transactionHeaderMiscDao.createConfigurableSubprojectOverride();
        
        TransactionHeader th = transactionHeaderRepository.findBy()
                .where(ProjectConstants.PERFORMANCE_METRIC).eq("MP-11678").findOne();
        Assert.assertNotNull("Test data wasn't created!", th);
        
        List<TransactionHeaderMisc> thm = transactionHeaderMiscRepository.findBy()
                .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                .and(ProjectConstants.VF_NAME).eq(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())
                .findAll();
        Assert.assertNotNull("Transaction header miscs were not created!", th);
        Assert.assertEquals("Should have created one THM record for CONFIGURABLE_SUB_PROJECT_NUMBER", 1, thm.size());

        TransactionHeaderMisc misc = thm.get(0);
        if (misc.getVfName().equals(VfName.CONFIGURABLE_SUB_PROJECT_NUMBER.name())) {
            Address addr = addressRepository.findBy()
                    .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_HARWELLM)
                    .and(ProjectConstants.PREFERRED).eq(ProjectConstants.YES_CHAR).findOne();
            Assert.assertNotNull("Test data wasn't created!", addr);

            //Sub Project Number should have been mapped to the recipient's (Leah) Country Code.
            Assert.assertEquals("THM for CONFIGURABLE_SUB_PROJECT_NUMBER not created properly!", 
                    StringUtils.substring(addr.getCountryCode(), 0, 8), misc.getMiscData());
        } else {
            Assert.fail("TransactionHeaderMisc was created with the incorrect VF_NAME");
        }
    }
}