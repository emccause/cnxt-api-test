package com.maritz.culturenext.recognition.awardcode.rest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.program.dto.Certificates;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeInfoDTO;
import com.maritz.culturenext.recognition.awardcode.dto.AwardCodeRequestDTO;
import com.maritz.test.AbstractRestTest;

public class AwardCodeRestServiceTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ProgramService programService;
    @Inject private Security security;
    
    private static final String TEST_PROGRAM_NAME = "AWARD_CODE_SUBMIT_TEST_PROGRAM";
    private static final String AWARD_CODE_REST_URL = "/rest/participants/~/nominations/award-code"; 

    private static final String AWARD_CODE_URL   = 
            "/rest/participants/847/nominations/award-codes/12345-123456789-987654321";
    private static final String AWARD_CODE_URL_2 = 
            "/rest/participants/555/nominations/award-codes/12345-123456789-987654321";

    private static final String AWARD_CODE_1 = "12345-123456789-987654321";
    private static final String AWARD_CODE_2 = "12345-123456789-987654322";
    
    private static final String REVOKE_AWARD_CODE_URL   = "/rest/participants/2/nominations/award-codes";
    private static final String REVOKE_AWARD_CODE_URL_2   = "/rest/participants/555/nominations/award-codes";
    private static final String REVOKE_AWARD_CODE_1 = "12345-123456789-123456654";
    private static final String REVOKE_AWARD_CODE_2 = "12345-123456789-1234566542";
    private static final String REVOKE_AWARD_CODE_3 = "12345-123456789-1234566543";
    private static final String REVOKE_AWARD_CODE_4 = "12345-123456789-1234566544";
    private static final String OTHER = "OTHER";
    private static final String TEST_PAYOUT_CASH = "CASH";
    
    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_PORTERGA);
    }
    // groups only - pass
    @Test
    public void post_success() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        Long programId = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(TEST_PROGRAM_NAME)
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        assertNotNull("Program should exist", programId);
        
        ProgramFullDTO program = programService.getProgramByID(programId, null);
        assertNotNull("Program should exist", program);
        
        List<AwardTierDTO> awardTierList = program.getAwardTiers();
        assertFalse("should have award tiers", awardTierList.isEmpty());
        assertTrue("Should be one award tier", awardTierList.size() == 1);
        
        List<Certificates> certIdList = program.getAwardCode().getCertificates();
        assertFalse("should have cert ids", certIdList.isEmpty());
        assertTrue("Should be one cert id", certIdList.size() == 1);
        
        List<Long> criteriaIdList = program.getRecognitionCriteriaIds();
        assertFalse("should have criteria ids", criteriaIdList.isEmpty());
        assertTrue("Should be one criteria id", criteriaIdList.size() == 1);
        
        
        AwardCodeRequestDTO dto = new AwardCodeRequestDTO();
        
        ArrayList<GroupMemberAwardDTO> receivers = new ArrayList<GroupMemberAwardDTO>();
        receivers.add(new GroupMemberAwardDTO()
                .setAwardAmount(awardTierList.get(0).getAwardAmount().intValue())
                .setOriginalAmount(awardTierList.get(0).getAwardAmount().intValue())
                .setAwardTierId(awardTierList.get(0).getAwardTierId()));
        dto.setReceivers(receivers);
        
        dto.setProgramId(programId);
        dto.setImageId(null);
        
        dto.setBudgetId(TestConstants.ID_1);
        
        ArrayList<Long> criteria = new ArrayList<Long>();
        criteria.add(criteriaIdList.get(0));
        dto.setRecognitionCriteriaIds(criteria);
        
        dto.setHeadline(TestConstants.HEADLINE);
        dto.setComment(TestConstants.COMMENT);
        
        AwardCodeInfoDTO info = new AwardCodeInfoDTO();
        info.setQuantity(10);
        info.setCertId(certIdList.get(0).getId());
        info.setGiveAnonymous(Boolean.FALSE);
        dto.setAwardCode(info);
        dto.setPayoutType(TEST_PAYOUT_CASH);
        
        mockMvc.perform(
                post(AWARD_CODE_REST_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(dto))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_redeem_award_code_user_is_not_a_program_receiver() throws Exception {
        budgetEligibilityCacheService.buildCache();

        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(AWARD_CODE_1);
        awardCodeDto.setStatus(StatusTypeCode.APPROVED.name());

        mockMvc.perform(patch(AWARD_CODE_URL_2)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void patch_redeem_award_code_without_body_request() throws Exception {
        budgetEligibilityCacheService.buildCache();

        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void patch_redeem_award_code_incorrect_status() throws Exception {
        budgetEligibilityCacheService.buildCache();

        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(AWARD_CODE_1);
        awardCodeDto.setStatus(OTHER);

        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void patch_redeem_award_code_2_different_codes() throws Exception {
        budgetEligibilityCacheService.buildCache();

        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(AWARD_CODE_2);
        awardCodeDto.setStatus(StatusTypeCode.APPROVED.name());

        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void patch_redeem_award_code() throws Exception {
        budgetEligibilityCacheService.buildCache();
        authenticationService.authenticate("porterga");
        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(AWARD_CODE_1);
        awardCodeDto.setStatus(StatusTypeCode.APPROVED.name());

        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void patch_already_redeem_award_code() throws Exception {
        budgetEligibilityCacheService.buildCache();

        authenticationService.authenticate(TestConstants.USER_PORTERGA);
        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(AWARD_CODE_1);
        awardCodeDto.setStatus(StatusTypeCode.APPROVED.name() );

        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto))
                ).andExpect(status().isOk());

        authenticationService.authenticate("porterga");
        mockMvc.perform(patch(AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(awardCodeDto)))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void put_revoke_award_code() throws Exception {

        budgetEligibilityCacheService.buildCache();
        authenticationService.authenticate(TestConstants.USER_ADMIN);

        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(REVOKE_AWARD_CODE_1);
        awardCodeDto.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto);
        
        AwardCodeDTO awardCodeDto2 = new AwardCodeDTO();
        awardCodeDto2.setAwardCode(REVOKE_AWARD_CODE_2);
        awardCodeDto2.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto2);

        mockMvc.perform(put(REVOKE_AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isOk());
    }

    @Test
    public void put_revoke_award_code_without_permissions() throws Exception {
        budgetEligibilityCacheService.buildCache();

        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        AwardCodeDTO awardCodeDto = new AwardCodeDTO();
        awardCodeDto.setAwardCode(REVOKE_AWARD_CODE_1);
        awardCodeDto.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto);
        
        AwardCodeDTO awardCodeDto2 = new AwardCodeDTO();
        awardCodeDto2.setAwardCode(REVOKE_AWARD_CODE_2);
        awardCodeDto2.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto2);

        mockMvc.perform(put(REVOKE_AWARD_CODE_URL_2)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isBadRequest()); // throws exception
        
    }
    
    @Test
    public void put_revoke_award_code_empty_list() throws Exception {
        budgetEligibilityCacheService.buildCache();

        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        mockMvc.perform(put(REVOKE_AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isBadRequest());
        
    }

    @Test
    public void patch_revoke_award_code_without_permissions() throws Exception {
        budgetEligibilityCacheService.buildCache();

        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        AwardCodeDTO awardCodeDto3 = new AwardCodeDTO();
        awardCodeDto3.setAwardCode(REVOKE_AWARD_CODE_3);
        awardCodeDto3.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto3);
        
        AwardCodeDTO awardCodeDto4 = new AwardCodeDTO();
        awardCodeDto4.setAwardCode(REVOKE_AWARD_CODE_4);
        awardCodeDto4.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto4);

        mockMvc.perform(patch(REVOKE_AWARD_CODE_URL_2)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isBadRequest()); // throws exception
    }
    
    @Test
    public void patch_revoke_award_code_empty_list() throws Exception {
        budgetEligibilityCacheService.buildCache();

        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        mockMvc.perform(patch(REVOKE_AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isBadRequest());
        
    }
    
    @Test
    public void patch_revoke_award_code() throws Exception {
        budgetEligibilityCacheService.buildCache();

        authenticationService.authenticate(TestConstants.USER_ADMIN);
        List<AwardCodeDTO> listAwardCodeDTO =  new ArrayList<AwardCodeDTO>();
        
        AwardCodeDTO awardCodeDto3 = new AwardCodeDTO();
        awardCodeDto3.setAwardCode(REVOKE_AWARD_CODE_3);
        awardCodeDto3.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto3);
        
        AwardCodeDTO awardCodeDto4 = new AwardCodeDTO();
        awardCodeDto4.setAwardCode(REVOKE_AWARD_CODE_4);
        awardCodeDto4.setStatus(StatusTypeCode.REJECTED.name());
        listAwardCodeDTO.add(awardCodeDto4);
        
        mockMvc.perform(patch(REVOKE_AWARD_CODE_URL)
                .with(authToken(security.getUserName()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(listAwardCodeDTO)))
                .andExpect(status().isOk());

    }
}