package com.maritz.culturenext.cardtypes.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CardTypesRestServiceTest extends AbstractRestTest{
    
    public static final String CARD_TYPES_URI = "/rest/participants/~/card-types";

    @Test
    public void get_card_types_happy_path() throws  Exception {
        mockMvc.perform(
                get(CARD_TYPES_URI)
                .with(authToken(TestConstants.USER_GARCIAF2))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_card_types_rideau() throws  Exception {
        MvcResult result = mockMvc.perform(
                get(CARD_TYPES_URI)
                        .with(authToken(TestConstants.USER_GARCIAF2)))
        .andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains("RIDEAU"));
        
    }
    
    @Test
    public void get_card_types_saa() throws  Exception {
        MvcResult result = mockMvc.perform(
                get(CARD_TYPES_URI)
                        .with(authToken(TestConstants.USER_GARCIAF2)))
        .andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains("DPP"));
        
    }

    @Test 
    public void get_empty_card_types_list() throws  Exception {
        mockMvc.perform(
                get(CARD_TYPES_URI)
                        .with(authToken(TestConstants.USER_COOPERJX)))
                        .andExpect(status().isOk())
                        .andExpect(content().string("[]"));
    }
}