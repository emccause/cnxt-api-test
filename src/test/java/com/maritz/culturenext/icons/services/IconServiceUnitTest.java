package com.maritz.culturenext.icons.services;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.util.EnvironmentUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityFiles;
import com.maritz.core.security.Security;
import com.maritz.core.util.StreamUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.icons.dto.IconDTO;
import com.maritz.culturenext.icons.service.impl.IconServiceImpl;
import com.maritz.culturenext.program.constants.ProgramConstants;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StreamUtils.class)
@PowerMockIgnore("javax.management.*")
public class IconServiceUnitTest {

    @InjectMocks private IconServiceImpl iconService;
    @Mock private ConcentrixDao<Files> filesDao;
    @Mock private ConcentrixDao<ProgramActivityFiles> programActivityFilesDao;
    @Mock private ConcentrixDao<ProgramActivity> programActivityDao;
    @Mock private Security security;
    @Mock private FileImageService fileImageService;
    @Mock private EnvironmentUtil environmentUtil;

    private FindBy<Files> filesFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramActivityFiles> programActivityFilesFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramActivity> programActivityFindBy = PowerMockito.mock(FindBy.class);

    private final Long id = 789L;

    private final String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private final String TEST_ICON = "test-icon.svg";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ((Files) args[0]).setId(id);
                return null;
            }
        }).when(filesDao).save(Matchers.any(Files.class));
        
        List<Long> iconIds = new ArrayList<>();

        iconIds.add(TestConstants.ID_1);
        iconIds.add(TestConstants.ID_2);
        iconIds.add(3L);
        iconIds.add(4L);
        
        PowerMockito.when(filesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(iconIds);

        List<Long> programActivityIconIds = new ArrayList<>();
        List<Long> activeProgramActivityIconIds = new ArrayList<>();
        
        programActivityIconIds.add(TestConstants.ID_2);
        programActivityIconIds.add(6L);
        programActivityIconIds.add(4L);
        programActivityIconIds.add(8L);
        activeProgramActivityIconIds.add(5L);
        activeProgramActivityIconIds.add(6L);
        activeProgramActivityIconIds.add(7L);
        activeProgramActivityIconIds.add(8L);

        PowerMockito.when(programActivityFilesDao.findBy()).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.where(anyString())).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.in(activeProgramActivityIconIds))
                .thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.find(ProjectConstants.FILES_ID, Long.class))
                .thenReturn(programActivityIconIds);

        PowerMockito.when(programActivityDao.findBy()).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.where(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.gt(any(Date.class))).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.and(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.lt(any(Date.class))).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.find(ProjectConstants.PROGRAM_ACTIVITY_ID, Long.class))
                .thenReturn(activeProgramActivityIconIds);
        
        PowerMockito.mockStatic(StreamUtils.class);
        
        PowerMockito.when(StreamUtils.toString(Matchers.any(FindBy.class), Matchers.anyString())).thenReturn("");
    }

    @Test
    public void test_create_icon_happy_path() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        FileInputStream image = new FileInputStream(TEST_IMAGES_DIR + TEST_ICON);
        MockMultipartFile icon = new MockMultipartFile("test", "newTest", "image/svg+xml", image);

        IconDTO iconDTO = iconService.createIcon(icon);

        assertTrue(iconDTO.getId() != null);
        assertTrue(!iconDTO.getUsedInPromotion());
        Mockito.verify(filesDao, times(1)).save(Matchers.any(Files.class));

        try {
            image.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void test_create_icon_not_admin() {

        IconDTO iconDTO = null;
        FileInputStream image = null;
        try {
            image = new FileInputStream(TEST_IMAGES_DIR + TEST_ICON);
            MockMultipartFile icon = new MockMultipartFile("test", "newTest", "image/xml+svg", image);

            iconDTO = iconService.createIcon(icon);
        } catch (Exception e) {
            assertTrue(e.getMessage().length() > 0);
            assertTrue(e.getMessage().endsWith(ProgramConstants.ERROR_NOT_ADMIN_CODE_MESSAGE));
        }

        assertTrue(iconDTO == null);

        try {
            image.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void test_get_icon_by_id_happy_path() {

        ResponseEntity<Object> iconXML = null;

        Files testIcon = new Files();
        FileInputStream image = null;
        MockMultipartFile icon;

        try {
            image = new FileInputStream(TEST_IMAGES_DIR + TEST_ICON);
            icon = new MockMultipartFile("test", "newTest", "image/xml+svg", image);

            testIcon.setId(TestConstants.ID_1);

            PowerMockito.when( fileImageService.prepareIconForResponse(Mockito.any(Files.class)))
                    .thenReturn(Mockito.mock(ResponseEntity.class));

            iconXML = iconService.getIconById(TestConstants.ID_1);
        } catch (Exception e) {
            fail();
        }

        //assertTrue(iconXML.substring(0, 9).equals("<!DOCTYPE"));
        assertNotNull(iconXML);

        try {
            image.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Test
    public void test_get_icon_by_id_no_icon() {

        ResponseEntity<Object> iconXML = null;

        try {
            PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(null);
            PowerMockito.when( fileImageService.prepareIconForResponse(Mockito.any(Files.class)))
                    .thenReturn(Mockito.mock(ResponseEntity.class));

            iconXML = iconService.getIconById(TestConstants.ID_2);
        } catch (Exception e) {
            fail();
        }

        assertNotNull(iconXML);

    }

    @Test
    public void test_get_list_of_icons_happy_path() {

        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.JUMBOTRON.toString());

        assertThat(iconDTOList.size(), is(4));
        assertFalse(iconDTOList.get(0).getUsedInPromotion());
        assertThat(iconDTOList.get(0).getId(), is(TestConstants.ID_1));
        assertTrue(iconDTOList.get(1).getUsedInPromotion());
        assertThat(iconDTOList.get(1).getId(), is(TestConstants.ID_2));

    }

    @Test
    public void test_get_list_of_icons_happy_path_used_in_promotion_true() {

        List<IconDTO> iconDTOList = iconService.getListOfIcons(Boolean.TRUE, FileTypes.JUMBOTRON.toString());

        assertThat(iconDTOList.size(), is(2));
        assertTrue(iconDTOList.get(0).getUsedInPromotion());
        assertThat(iconDTOList.get(0).getId(), is(TestConstants.ID_2));
        assertTrue(iconDTOList.get(1).getUsedInPromotion());
        assertThat(iconDTOList.get(1).getId(), is(4L));

    }

    @Test
    public void test_get_list_of_icons_happy_path_used_in_promotion_false() {

        List<IconDTO> iconDTOList = iconService.getListOfIcons(Boolean.FALSE, FileTypes.JUMBOTRON.toString());

        assertThat(iconDTOList.size(), is(2));
        assertFalse(iconDTOList.get(0).getUsedInPromotion());
        assertThat(iconDTOList.get(0).getId(), is(TestConstants.ID_1));
        assertFalse(iconDTOList.get(1).getUsedInPromotion());
        assertThat(iconDTOList.get(1).getId(), is(3L));

    }

    @Test
    public void test_get_list_of_icons_no_icons_in_db() {
        List<Long> programActivityIconIds = new ArrayList<>();

        programActivityIconIds.add(TestConstants.ID_2);
        programActivityIconIds.add(3L);

        PowerMockito.when(programActivityFilesFindBy.find(ProjectConstants.FILES_ID, Long.class))
                .thenReturn(programActivityIconIds);
        
        PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(null);

        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.JUMBOTRON.toString());

        assertThat(iconDTOList.size(), is(0));

    }

    @Test
    public void test_get_list_of_icons_no_program_activities() {
        List<Long> iconIds = new ArrayList<>();
        iconIds.add(TestConstants.ID_1);
        iconIds.add(TestConstants.ID_2);
        PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(iconIds);
        
        PowerMockito.when(programActivityFilesFindBy.find(ProjectConstants.FILES_ID, Long.class)).thenReturn(null);
        PowerMockito.when(programActivityFilesFindBy.find(ProjectConstants.FILES_ID, Long.class)).thenReturn(null);

        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.JUMBOTRON.toString());

        assertThat(iconDTOList.size(), is(2));
        assertFalse(iconDTOList.get(0).getUsedInPromotion());
        assertThat(iconDTOList.get(0).getId(), is(TestConstants.ID_1));
        assertFalse(iconDTOList.get(1).getUsedInPromotion());
        assertThat(iconDTOList.get(1).getId(), is(TestConstants.ID_2));

    }


    public void test_get_list_of_icons_type_AWARD_CODE_CERT() {
        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.AWARD_CODE_CERT.toString());
        assertThat(iconDTOList.size(), is(2));
    }

    public void test_get_list_of_icons_type_AWARD_CODE_ICON() {
        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.AWARD_CODE_ICON.toString());
        assertThat(iconDTOList.size(), greaterThan(0));
    }

    public void test_get_list_of_icons_type_JUMBOTRON() {
        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.JUMBOTRON.toString());
        assertThat(iconDTOList.size(), greaterThan(0));
    }

    public void test_get_list_of_icons_type_CAROUSEL() {
        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, FileTypes.CAROUSEL.toString());
        assertThat(iconDTOList.size(), greaterThan(0));
    }

    public void test_get_list_of_icons_type_NONE() {
        List<IconDTO> iconDTOList = iconService.getListOfIcons(null, "");
        assertThat(iconDTOList.size(), greaterThan(0));
    }

}
