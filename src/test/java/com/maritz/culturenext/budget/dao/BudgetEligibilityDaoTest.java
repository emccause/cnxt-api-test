package com.maritz.culturenext.budget.dao;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ProgramBudget;
import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class BudgetEligibilityDaoTest extends AbstractDatabaseTest {
    
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private ConcentrixDao<ProgramBudget> programBudgetDao;

    Long programNoBudgets = 36L;
        
    @Test
    public void get_budget_eligibility_good() throws Exception {

        budgetEligibilityCacheService.buildCache();
        
        List<BudgetEligibilityCacheDTO> cacheObjectList = budgetEligibilityCacheService.getEligibilityForPax(TestConstants.PAX_HARWELLM);
        
        assertNotNull("Should have found data", cacheObjectList);
        
        List<BudgetEligibilityCacheDTO> filteredList = new ArrayList<>();
        for (BudgetEligibilityCacheDTO cacheObject : cacheObjectList) {
            if (cacheObject.getProgramId().equals(TestConstants.ID_1)) {
                filteredList.add(cacheObject);
            }
        }
                  
        List<ProgramBudget> programBudgetList = programBudgetDao.findBy()
                .where(ProjectConstants.PROGRAM_ID).eq(TestConstants.ID_1)
                .find();
        
        assertThat(filteredList.size(), is(programBudgetList.size()));
          
    }
      
    @Test
    public void get_budget_eligibility_no_budgets() throws Exception {    
        
        budgetEligibilityCacheService.buildCache();
        
        List<BudgetEligibilityCacheDTO> cacheObjectList = budgetEligibilityCacheService.getEligibilityForPax(TestConstants.PAX_HARWELLM);
        
        assertNotNull("Should have found data", cacheObjectList);
        
        List<BudgetEligibilityCacheDTO> filteredList = new ArrayList<>();
        for (BudgetEligibilityCacheDTO cacheObject : cacheObjectList) {
            if (cacheObject.getProgramId().equals(programNoBudgets)) {
                filteredList.add(cacheObject);
            }
        }
        
        assertThat(filteredList.size(), is(0));
    }
}
