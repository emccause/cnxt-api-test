package com.maritz.culturenext.budget.dao;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.constants.BudgetCsvReportConstants;
import com.maritz.test.AbstractDatabaseTest;

public class BudgetReportDaoTest extends AbstractDatabaseTest {

    public final String BUDGET_86 = "86";
    public final String BUDGET_812 = "812";
    public final String BUDGET_5081 = "5081";

    @Inject private BudgetReportDao budgetReportDao;

    @Test
    public void test_getBudgetGivingLimit_default_inputs() {
        List<Map<String, Object>> result = budgetReportDao.getBudgetGivingLimit(null, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        verifyResultRowContent(result.get(0));
    }

    @Test
    public void test_getBudgetGivingLimit_filteredBy_budgetId() {

        String filterBudgetId = "1";

        List<Map<String, Object>> result = budgetReportDao.getBudgetGivingLimit(filterBudgetId, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String,Object> row : result){
            if (!filterBudgetId.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID)))) {
                fail("Query filter by budgetId is not working as expected");
            }
        }

        verifyResultRowContent(result.get(0));
    }


    @Test
    public void test_getBudgetGivingLimit_filteredBy_status() {

        List<Map<String, Object>> result = budgetReportDao.getBudgetGivingLimit(null, StatusTypeCode.ACTIVE.name());

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        verifyResultRowContent(result.get(0));

    }

    @Test
    public void test_getBudgetGivingLimit_filteredBy_budgetId_and_status() {

        String filterBudgetId1 = "1";
        String filterBudgetId2= "76";
        String filterStatus = "ACTIVE,SCHEDULED";

        List<Map<String, Object>> result = budgetReportDao.getBudgetGivingLimit(
                filterBudgetId1+","+filterBudgetId2, filterStatus);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String,Object> row : result){
            if (!(filterBudgetId1.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID)))
                    || filterBudgetId2.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID))))) {
                fail("Query filter by budgetId and status is not working as expected");
            }
        }

        verifyResultRowContent(result.get(0));

    }

    private void verifyResultRowContent(Map<String,Object> row) {
        assertNotNull(row);
        assertNotNull(row.get(ProjectConstants.BUDGET_ID));
        assertThat(row.get(ProjectConstants.BUDGET_ID), instanceOf(Long.class));
        assertNotNull(row.get(ProjectConstants.BUDGET_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_NAME), instanceOf(String.class));
        assertNotNull(row.get(ProjectConstants.BUDGET_DISPLAY_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_DISPLAY_NAME), instanceOf(String.class));
        assertNotNull(row.get(BudgetCsvReportConstants.BUDGET_GIVING_LIMIT));
        assertThat(row.get(BudgetCsvReportConstants.BUDGET_GIVING_LIMIT), instanceOf(Double.class));
        assertNotNull(row.get(ProjectConstants.START_DATE));
        assertThat(row.get(ProjectConstants.START_DATE), instanceOf(Date.class));
        assertNotNull(row.get(BudgetCsvReportConstants.BUDGET_USED));
        assertThat(row.get(BudgetCsvReportConstants.BUDGET_USED), instanceOf(Double.class));
        assertNotNull(row.get(BudgetCsvReportConstants.BUDGET_REMAINING));
        assertThat(row.get(BudgetCsvReportConstants.BUDGET_REMAINING), instanceOf(Double.class));
        assertNotNull(row.get(ProjectConstants.CONTROL_NUM));
        assertThat(row.get(ProjectConstants.CONTROL_NUM), instanceOf(String.class));
        assertNotNull(row.get(ProjectConstants.FIRST_NAME));
        assertThat(row.get(ProjectConstants.FIRST_NAME), instanceOf(String.class));
        assertNotNull(row.get(ProjectConstants.LAST_NAME));
        assertThat(row.get(ProjectConstants.LAST_NAME), instanceOf(String.class));
        assertNotNull(row.get(BudgetCsvReportConstants.PAX_AMOUNT_USED));
        assertThat(row.get(BudgetCsvReportConstants.PAX_AMOUNT_USED), instanceOf(Double.class));
        assertNotNull(row.get(ProjectConstants.STATUS));
        assertThat(row.get(ProjectConstants.STATUS), instanceOf(String.class));

    }

    @Test
    public void test_getBudgetSummary_default_inputs() {
        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(null, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));
        
        boolean found = false;
        for (Map<String,Object> row : result) {
            String budgetName = (String)row.get("budgetName");
            if ("85 - Company Name - MGTS".equalsIgnoreCase(budgetName)) {
                found = true;
                verifyResultRowContentSummary(row);
                break;
            }
        }
        assertThat(found, is(true));

        //first budget that has a notNull parentBudgetId
        verifyResultRowContentSummary(result.stream().filter(budget -> (budget.get(ProjectConstants.PARENT_BUDGET_ID)!=null)).findFirst().orElse(null));

    }

    @Test
    public void test_getBudgetSummary_filteredBy_budgetId() {

        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(BUDGET_86, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String,Object> row : result){
            if (!BUDGET_86.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID)))) {
                fail("Query filter by budgetId is not working as expected");
            }
        }

        verifyResultRowContentSummary(result.get(0));
    }

    @Test
    public void test_getBudgetSummary_optional_fields_filteredBy_budgetId() {

        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(BUDGET_86, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String,Object> row : result){
            if (!BUDGET_86.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID)))) {
                fail("Query filter by budgetId is not working as expected");
            }
        }

        verifyOptionalResultRowContentSummary(result.get(0));
    }

    @Test
    public void test_getBudgetSummary_filteredBy_status() {

        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(null, StatusTypeCode.ACTIVE.name());

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        boolean found = false;
        for (Map<String,Object> row : result) {
            String budgetName = (String)row.get("budgetName");
            if ("85 - Company Name - MGTS".equalsIgnoreCase(budgetName)) {
                found = true;
                verifyResultRowContentSummary(row);
                break;
            }
        }
        assertThat(found, is(true));
    }

    @Test
    public void test_getBudgetSummary_filteredBy_budgetId_and_status() {

        String budgetsIds = BUDGET_86+","+BUDGET_812;
        String filterStatus = StatusTypeCode.ACTIVE.name() + "," + StatusTypeCode.INACTIVE.name();

        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(budgetsIds, filterStatus);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String,Object> row : result){
            if (!(BUDGET_86.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID)))
                    || BUDGET_812.equals(String.valueOf(row.get(ProjectConstants.BUDGET_ID))))) {
                fail("Query filter by budgetId and status is not working as expected");
            }
        }

        verifyResultRowContentSummary(result.get(0));

    }

    @Test
    public void test_getBudgetSummary_optional_fields_including_allocated() {

        List<Map<String, Object>> result = budgetReportDao.getBudgetSummary(BUDGET_5081, null);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        verifyOptionalAllocatedResultRowContentSummary(result.get(0));
    }

    private void verifyResultRowContentSummary(Map<String,Object> row) {
        assertNotNull(row);
        assertNotNull(row.get(ProjectConstants.BUDGET_ID));
        assertThat(row.get(ProjectConstants.BUDGET_ID), instanceOf(Long.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_TYPE_CODE));
        assertThat(row.get(ProjectConstants.BUDGET_TYPE_CODE), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.PARENT_BUDGET_ID));
        assertThat(row.get(ProjectConstants.PARENT_BUDGET_ID), instanceOf(Long.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_NAME), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_DISPLAY_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_DISPLAY_NAME), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.START_DATE));
        assertThat(row.get(ProjectConstants.START_DATE), instanceOf(Date.class));

        assertNotNull(row.get(ProjectConstants.STATUS));
        assertThat(row.get(ProjectConstants.STATUS), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.DISTRIBUTED_BUDGET_GROUP));
        assertThat(row.get(ProjectConstants.DISTRIBUTED_BUDGET_GROUP), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.GROUP_ID));
        assertThat(row.get(ProjectConstants.GROUP_ID), instanceOf(Long.class));

        assertNotNull(row.get(ProjectConstants.PROJECT_NUMBER));
        assertThat(row.get(ProjectConstants.PROJECT_NUMBER), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.ACCOUNTING_ID));
        assertThat(row.get(ProjectConstants.ACCOUNTING_ID), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.TOTAL_AMOUNT));
        assertThat(row.get(ProjectConstants.TOTAL_AMOUNT), instanceOf(Double.class));

        assertNotNull(row.get(BudgetCsvReportConstants.BUDGET_USED));
        assertThat(row.get(BudgetCsvReportConstants.BUDGET_USED), instanceOf(Double.class));

        assertNotNull(row.get(BudgetCsvReportConstants.BUDGET_REMAINING));
        assertThat(row.get(BudgetCsvReportConstants.BUDGET_REMAINING), instanceOf(Double.class));

        assertNotNull(row.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT));
        assertThat(row.get(ProjectConstants.INDIVIDUAL_GIVING_LIMIT), instanceOf(Double.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_PER_PARTICIPANT));
        assertThat(row.get(ProjectConstants.BUDGET_PER_PARTICIPANT), instanceOf(Double.class));
    }

    private void verifyOptionalResultRowContentSummary(Map<String,Object> row) {

        assertNotNull(row.get(ProjectConstants.GROUP_ID));
        assertThat(row.get(ProjectConstants.GROUP_ID), instanceOf(Long.class));

        assertNotNull(row.get(ProjectConstants.GROUP_NAME));
        assertThat(row.get(ProjectConstants.GROUP_NAME), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_OWNER_CONTROL_NUM));
        assertThat(row.get(ProjectConstants.BUDGET_OWNER_CONTROL_NUM), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_OWNER_FIRST_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_OWNER_FIRST_NAME), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.BUDGET_OWNER_LAST_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_OWNER_LAST_NAME), instanceOf(String.class));
    }

    private void verifyOptionalAllocatedResultRowContentSummary(Map<String,Object> row) {

        assertNotNull(row.get(ProjectConstants.ALLOCATED_IN));
        assertThat(row.get(ProjectConstants.ALLOCATED_IN), instanceOf(Double.class));

        assertNotNull(row.get(ProjectConstants.ALLOCATED_OUT));
        assertThat(row.get(ProjectConstants.ALLOCATED_OUT), instanceOf(Double.class));
    }
}

