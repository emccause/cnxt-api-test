package com.maritz.culturenext.budget.dao;

import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.dto.BudgetUtilizationStatsDTO;
import com.maritz.culturenext.budget.service.BudgetReportService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class BudgetReportStatsDaoTest extends AbstractDatabaseTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private BudgetReportService budgetReportService;
    @Inject private ReportsQueryService reportsQueryService;
    
    @Test
    public void get_manager_budget_report_stats() throws Exception {
        ReportsQueryRequestDto requestDto = ObjectUtils.jsonToObject(
                "{\"fromDate\": \"2015-05-01\","
                + "\"thruDate\":\"2015-07-01\","
                + "\"members\":[{\"paxId\":847}],"
                + "\"directReports\":[847],"
                + "\"orgReports\":[]}", ReportsQueryRequestDto.class);

        try {
            authenticationService.authenticate(TestConstants.USER_PORTERGA);
            QueryIdDto query = reportsQueryService.getQueryId(requestDto);

            BudgetUtilizationStatsDTO budgetUtilizationStatsDTO = 
                    budgetReportService.getManagerBudgetStatsByQueryId( query.getQueryId());
            assertTrue(budgetUtilizationStatsDTO.getBudgetAvailable() >= 0);
            assertTrue(budgetUtilizationStatsDTO.getBudgetUsed() >= 0);
            assertTrue(budgetUtilizationStatsDTO.getTotalBudget() > 0);
            assertTrue(budgetUtilizationStatsDTO.getBudgetExpired() >= 0);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax() throws Exception {
        ReportsQueryRequestDto requestDto = ObjectUtils.jsonToObject(
                "{\"fromDate\": \"2015-05-01\","
                + "\"thruDate\":\"2015-07-01\","
                + "\"members\":[{\"paxId\":847}],"
                + "\"directReports\":[847],"
                + "\"orgReports\":[]}", ReportsQueryRequestDto.class);

        try {
            authenticationService.authenticate(TestConstants.USER_PORTERGA);
            QueryIdDto query = reportsQueryService.getQueryId(requestDto);

            BudgetUtilizationStatsDTO budgetUtilizationStatsDTO = 
                    budgetReportService.getManagerBudgetStatsForPaxByQueryId(TestConstants.PAX_HARWELLM, query.getQueryId());
            assertTrue(budgetUtilizationStatsDTO.getBudgetAvailable() >= 0);
            assertTrue(budgetUtilizationStatsDTO.getBudgetUsed() == 282);
            assertTrue(budgetUtilizationStatsDTO.getTotalBudget() > 0);
            assertTrue(budgetUtilizationStatsDTO.getBudgetExpired() >= 0);
        } catch (Exception e) {
            fail();
        }
    }
    
}
