package com.maritz.culturenext.budget.service.impl;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.BudgetMisc;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Project;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.entity.SubProject;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.repository.GroupConfigRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.budget.dao.BudgetBulkDataManagementDao;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dao.BudgetSearchDao;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetDetailsRequestDTO;
import com.maritz.culturenext.budget.dto.BudgetRequestDTO;
import com.maritz.culturenext.caching.dto.BudgetEligibilityCacheDTO;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.BudgetConstraintEnum;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtBudgetRepository;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.StatusTypeUtil;
import com.maritz.test.AbstractMockTest;

public class BudgetServiceUnitTest extends AbstractMockTest {

    @InjectMocks private BudgetServiceImpl budgetService;
    @Mock private BudgetBulkDataManagementDao budgetBulkDataManagementDao;
    @Mock private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Mock private BudgetInfoDao budgetInfoDao;
    @Mock private BudgetSearchDao budgetSearchDao;
    @Mock private GroupConfigRepository groupConfigRepository;
    @Mock private CnxtBudgetRepository cnxtBudgetRepository;
    @Mock private ConcentrixDao<BudgetMisc> budgetMiscDao;
    @Mock private ConcentrixDao<Discretionary> discretionaryDao;
    @Mock private ConcentrixDao<Groups> groupsDao;
    @Mock private ConcentrixDao<GroupsPax> groupsPaxDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<Project> projectDao;
    @Mock private ConcentrixDao<Role> roleDao;
    @Mock private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Mock private ConcentrixDao<SysUser> sysUserDao;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxRepository paxRepository;
    @Mock private ProgramRepository programRepository;
    @Mock private ProxyService proxyService;
    @Mock private Security security;
    @Mock private StatusTypeUtil statusTypeUtil;

    @Rule public ExpectedException thrown = ExpectedException.none();

    private static final Long TEST_BUDGET_CHILD_1_ID = 11L;
    private static final Long TEST_BUDGET_CHILD_2_ID = 12L;
    private static final Double TEST_TOTAL_USED = 200d;
    private static final Double TEST_TOTAL_AMOUNT = 100d;
    private static final String TEST_BUDGET_NAME = "Testing budget";
    private static final String TEST_BUDGET_DISPLAY_NAME = "Display this budget name";
    private static final String TEST_BUDGET_DESC = "This is the description for this budget";
    private static final String TEST_SUB_PROJECT_NUMBER = "11110";
    private static final Double TEST_INDIVIDUAL_GIVING_LIMIT = 50d;
    private static final Double TEST_INDIVIDUAL_USED_AMOUNT = 25d;
    private static final Long TEST_BUDGET_OWNER_PAX_ID = 1L;
    private static final String TEST_IS_USED = "TRUE";
    private static final Long TEST_BUDGET_USER_PAX_ID = 4L;
    private static final Long TEST_BUDGET_ALLOCATOR_PAX_ID = 5L;
    private static final String TEST_ALLOCATED_IN = "10";
    private static final String TEST_ALLOCATED_OUT = "20";
    BudgetRequestDTO budgetDto = null;

    private Map<String, Object> mockedResponseTopBudget;

    @Mock FindBy<BudgetMisc> budgetMiscFindBy;
    @Mock FindBy<BudgetMisc> budgetMiscAllocatedInFindBy;
    @Mock FindBy<Groups> groupsFindBy;
    @Mock FindBy<GroupsPax> groupsPaxFindBy;
    @Mock FindBy<Pax> paxFindBy;
    @Mock FindBy<Project> projectFindBy;
    @Mock FindBy<Role> roleFindBy;
    @Mock FindBy<SysUser> sysUserFindBy;

    @Before
    public void setup() throws Exception {

        mockedResponseTopBudget = mockedRowResponseBudgetById();

        List<BudgetEligibilityCacheDTO> budgetEligibilityCacheList =  new ArrayList<>();
        budgetEligibilityCacheList.add(new BudgetEligibilityCacheDTO().setBudgetId(1L));

        Mockito.when(budgetEligibilityCacheService.getEligibilityForPax(Matchers.anyLong())).thenReturn(budgetEligibilityCacheList);

        // BudgetInfoDao
        Mockito.when(budgetInfoDao.getBudgetInfoById(Matchers.anyLong()))
            .thenReturn(Arrays.asList(mockedResponseTopBudget));
        Mockito.when(budgetInfoDao.getEligibleBudgets(Matchers.anyLong(), Matchers.anyListOf(Long.class), Matchers.anyBoolean()))
            .thenReturn(Arrays.asList(mockedResponseTopBudget));

        Mockito.when(security.hasRole(RoleCode.ADMIN)).thenReturn(Boolean.TRUE);
        Mockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        Mockito.when(security.isMyPax(Matchers.anyLong()))
            .thenReturn(Boolean.TRUE);

        //Project Number Check
        Project project = new Project().setProjectNumber("S01234");
        Mockito.when(projectDao.findBy()).thenReturn(projectFindBy);
        Mockito.when(projectFindBy.where(Matchers.anyString())).thenReturn(projectFindBy);
        Mockito.when(projectFindBy.eq(Matchers.anyString())).thenReturn(projectFindBy);
        Mockito.when(projectFindBy.exists()).thenReturn(Boolean.TRUE);
        Mockito.when(projectFindBy.findOne()).thenReturn(project);

        //Existing Budget (Error)
        Mockito.when(cnxtBudgetRepository.findOne(Matchers.anyLong())).thenReturn(null);

        //Budget to Edit
        Mockito.when(cnxtBudgetRepository.findOne(Matchers.anyLong())).thenReturn(new Budget());

        //The ID will not be populated when the budget is saved via Repository (so mock out the GET response)
        Mockito.when(cnxtBudgetRepository.save(Matchers.any(Budget.class))).thenAnswer(new Answer<Budget>() {
            @Override
            public Budget answer(InvocationOnMock invocation) throws Throwable {
                Budget b = invocation.getArgumentAt(0, Budget.class);
                b.setId(1L);
                return b;
            }
        });

        //Existing pax check
        Mockito.when(paxDao.findById(Matchers.anyLong())).thenReturn(new Pax());

        //Look up pax name for BUDGET_MISC inserts
        Mockito.when(paxDao.findBy()).thenReturn(paxFindBy);
        Mockito.when(paxFindBy.where(anyString())).thenReturn(paxFindBy);
        Mockito.when(paxFindBy.eq(anyLong())).thenReturn(paxFindBy);
        Mockito.when(paxFindBy.findOne()).thenReturn(new Pax().setFirstName("Leah").setLastName("O'Rourke"));

        //Look up group name for BUDGET_MISC inserts
        Mockito.when(groupsDao.findBy()).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.where(anyString())).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.eq(anyLong())).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.findOne()).thenReturn(new Groups().setGroupDesc("Group o' winners"));

        //SysUser Status Check
        SysUser sysUser = new SysUser();
        sysUser.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        Mockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.where(ProjectConstants.PAX_ID)).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.eq(Matchers.anyLong())).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.findOne()).thenReturn(sysUser);

        //Group Status Check
        Groups group = new Groups();
        group.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        Mockito.when(groupsDao.findById(Matchers.anyLong())).thenReturn(group);

        //Roles
        Role budgetAllocator = new Role().setRoleCode(RoleCode.BUDGET_ALLOCATOR.name());
        Role budgetOwner = new Role().setRoleCode(RoleCode.BUDGET_OWNER.name());
        Role budgetUser = new Role().setRoleCode(RoleCode.BUDGET_USER.name());
        Mockito.when(roleDao.findBy()).thenReturn(roleFindBy);
        Mockito.when(roleFindBy.where(Matchers.anyString())).thenReturn(roleFindBy);
        Mockito.when(roleFindBy.in(Arrays.asList(Matchers.anyString()))).thenReturn(roleFindBy);
        Mockito.when(roleFindBy.find()).thenReturn(Arrays.asList(budgetAllocator, budgetOwner, budgetUser));

        //Groups
        Groups budgetAllocators = new Groups().setGroupName(BudgetConstants.BUDGET_ALLOCATORS_GROUP_NAME);
        Groups budgetOwners = new Groups().setGroupName(BudgetConstants.BUDGET_OWNERS_GROUP_NAME);
        Mockito.when(groupsDao.findBy()).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.where(Matchers.anyString())).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.in(Arrays.asList(Matchers.anyString()))).thenReturn(groupsFindBy);
        Mockito.when(groupsFindBy.find()).thenReturn(Arrays.asList(budgetAllocators, budgetOwners));

        Mockito.when(groupsPaxDao.findBy()).thenReturn(groupsPaxFindBy);
        Mockito.when(groupsPaxFindBy.where(Matchers.anyString())).thenReturn(groupsPaxFindBy);
        Mockito.when(groupsPaxFindBy.eq(Matchers.anyLong())).thenReturn(groupsPaxFindBy);
        Mockito.when(groupsPaxFindBy.find(ProjectConstants.PAX_ID, Long.class)).thenReturn(Arrays.asList(8571L, 847L));

        Mockito.when(budgetMiscDao.findBy()).thenReturn(budgetMiscFindBy);
        Mockito.when(budgetMiscFindBy.where(Matchers.anyString())).thenReturn(budgetMiscFindBy);
        Mockito.when(budgetMiscFindBy.eq(Matchers.anyLong())).thenReturn(budgetMiscFindBy);
        Mockito.when(budgetMiscFindBy.and(Matchers.anyString())).thenReturn(budgetMiscFindBy);
        Mockito.when(budgetMiscFindBy.eq(Matchers.anyString())).thenReturn(budgetMiscFindBy);
        Mockito.when(budgetMiscFindBy.eq(VfName.ALLOCATED_IN.name())).thenReturn(budgetMiscAllocatedInFindBy); // bail out for allocated in find
        Mockito.when(budgetMiscFindBy.findOne()).thenReturn(new BudgetMisc().setMiscData("123"));
        Mockito.when(budgetMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class)).thenReturn("12345");
        Mockito.when(budgetMiscAllocatedInFindBy.and(Matchers.anyString())).thenReturn(budgetMiscAllocatedInFindBy);
        Mockito.when(budgetMiscAllocatedInFindBy.eq(Matchers.anyLong())).thenReturn(budgetMiscAllocatedInFindBy);
        Mockito.when(budgetMiscAllocatedInFindBy.findOne()).thenReturn(new BudgetMisc().setMiscData("500"));


        SubProject subproject = new SubProject().setSubProjectNumber("UnitTest");
        Mockito.when(budgetBulkDataManagementDao.findExistingSubProjects(Matchers.anyString())).thenReturn(Arrays.asList(subproject));

        Mockito.when(programRepository.findOne(Matchers.anyLong())).thenReturn(
                new Program().setStatusTypeCode(StatusTypeCode.ACTIVE.name()).setProgramId(TestConstants.ID_1));

        budgetDto = setUpBudgetForTests();

        // PaxRepository
        Mockito.when(paxRepository.exists(Matchers.anyLong())).thenReturn(Boolean.TRUE);

        Mockito.when(budgetSearchDao.getBudgetList(
                Matchers.anyString(), Matchers.anyString(), Matchers.anyString(), Matchers.anyBoolean(), Matchers.anyBoolean(), Matchers.anyBoolean(), Matchers.anyString(), Matchers.anyInt(), Matchers.anyInt(), Matchers.anyString())
            ).thenReturn(getBudgetListResults());

        Mockito.when(budgetInfoDao.getBudgetTotals(Matchers.anyListOf(Long.class))).thenReturn(getBudgetTotalsResults());
        Mockito.when(budgetInfoDao.getBudgetUsed(Matchers.anyListOf(Long.class))).thenReturn(getBudgetUsedResults());
        Mockito.when(budgetInfoDao.getBudgetUsers(Matchers.anyListOf(Long.class))).thenReturn(getBudgetUsersResults());
        Mockito.when(participantInfoDao.getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class))).thenReturn(getInfoResults());

        List<Map<String, Object>> budgetTotalsList = new ArrayList<>();
        Map<String, Object> budgetTotals = new HashMap<>();
        budgetTotals.put(ProjectConstants.BUDGET_ID, TestConstants.ID_1);
        budgetTotals.put(ProjectConstants.TOTAL_AMOUNT, TEST_TOTAL_AMOUNT);
        budgetTotals.put(ProjectConstants.TOTAL_USED, TEST_TOTAL_USED);
        budgetTotalsList.add(budgetTotals);
        Mockito.when(budgetInfoDao.getBudgetTotalsFlat(Matchers.anyListOf(Long.class))).thenReturn(budgetTotalsList);

    }

    //********************************* Helper Methods *****************************************


    private BudgetRequestDTO setUpBudgetForTests() {
        BudgetRequestDTO parentBudget = new BudgetRequestDTO();
        BudgetRequestDTO levelOneBudget = new BudgetRequestDTO();
        BudgetRequestDTO levelTwoBudget = new BudgetRequestDTO();

        parentBudget.setBudgetId(TestConstants.ID_1);
        parentBudget.setParentBudgetId(null);
        parentBudget.setName("Budget Testing!");
        parentBudget.setDisplayName(null);
        parentBudget.setDescription("Budget Description");
        parentBudget.setTotalAmount(300.0);
        parentBudget.setUsedAmount(0.0);
        parentBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        parentBudget.setAccountingId("UnitTest");
        parentBudget.setIndividualGivingLimit(null);
        parentBudget.setStatus(StatusTypeCode.ACTIVE.name());
        parentBudget.setFromDate("2016-11-24");
        parentBudget.setThruDate(null);
        parentBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        parentBudget.setProjectNumber("S01234");
        parentBudget.setBudgetOwner(new EmployeeDTO().setPaxId(847L));
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setBudgetUsers(new ArrayList<GroupMemberStubDTO>());
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDetails.setCanAllocate(false);
        parentBudget.setBudgetDetails(budgetDetails);

        levelOneBudget.setBudgetId(TestConstants.ID_2);
        levelOneBudget.setParentBudgetId(TestConstants.ID_1);
        levelOneBudget.setName("Budget Testing!");
        levelOneBudget.setDisplayName(null);
        levelOneBudget.setDescription("Budget Description");
        levelOneBudget.setTotalAmount(100.0);
        levelOneBudget.setUsedAmount(0.0);
        levelOneBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        levelOneBudget.setAccountingId("UnitTest");
        levelOneBudget.setIndividualGivingLimit(null);
        levelOneBudget.setStatus(StatusTypeCode.INACTIVE.name());
        levelOneBudget.setFromDate("2016-11-24");
        levelOneBudget.setThruDate(null);
        levelOneBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        levelOneBudget.setProjectNumber("S01234");
        levelOneBudget.setBudgetOwner(new EmployeeDTO().setPaxId(8571L));
        BudgetDetailsRequestDTO budgetDetailsOne = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO leah = new GroupMemberStubDTO();
        leah.setPaxId(8571L);
        budgetDetailsOne.setBudgetUsers(Arrays.asList(leah));
        budgetDetailsOne.setAllocatedIn(200.0);
        budgetDetailsOne.setAllocatedOut(100.0);
        budgetDetailsOne.setCanAllocate(true);
        levelOneBudget.setBudgetDetails(budgetDetailsOne);

        levelTwoBudget.setBudgetId(3L);
        levelTwoBudget.setParentBudgetId(TestConstants.ID_2);
        levelTwoBudget.setName("Budget Testing!");
        levelTwoBudget.setDisplayName(null);
        levelTwoBudget.setDescription("Budget Description");
        levelTwoBudget.setTotalAmount(100.0);
        levelTwoBudget.setUsedAmount(0.0);
        levelTwoBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        levelTwoBudget.setAccountingId("UnitTest");
        levelTwoBudget.setIndividualGivingLimit(null);
        levelTwoBudget.setStatus(StatusTypeCode.ACTIVE.name());
        levelTwoBudget.setFromDate("2016-11-24");
        levelTwoBudget.setThruDate(null);
        levelTwoBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        levelTwoBudget.setProjectNumber("S01234");
        levelTwoBudget.setBudgetOwner(new EmployeeDTO().setPaxId(8987L));
        BudgetDetailsRequestDTO budgetDetailsTwo = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO alex = new GroupMemberStubDTO();
        alex.setPaxId(8987L);
        budgetDetailsTwo.setBudgetUsers(Arrays.asList(alex));
        budgetDetailsTwo.setAllocatedIn(100.0);
        budgetDetailsTwo.setAllocatedOut(0.0);
        budgetDetailsTwo.setCanAllocate(false);
        levelTwoBudget.setBudgetDetails(budgetDetailsTwo);

        levelTwoBudget.setChildBudgets(new ArrayList<BudgetRequestDTO>()); //childBudgets: []  instead of childBudgets: null
        levelOneBudget.setChildBudgets(Arrays.asList(levelTwoBudget));
        parentBudget.setChildBudgets(Arrays.asList(levelOneBudget));

        return parentBudget;
    }

    private Map<String, Object> mockedRowResponseBudgetById() {
        Map<String, Object> rowData = new HashMap<>();
        rowData.put(ProjectConstants.ID,TestConstants.ID_1);
        rowData.put(ProjectConstants.PARENT_BUDGET_ID,TestConstants.ID_2);
        rowData.put(ProjectConstants.TOTAL_AMOUNT,TEST_TOTAL_AMOUNT);
        rowData.put(ProjectConstants.TOTAL_USED,TEST_TOTAL_USED);
        rowData.put(ProjectConstants.BUDGET_NAME,TEST_BUDGET_NAME);
        rowData.put(ProjectConstants.DISPLAY_NAME,TEST_BUDGET_DISPLAY_NAME);
        rowData.put(ProjectConstants.BUDGET_DESC,TEST_BUDGET_DESC);
        rowData.put(ProjectConstants.BUDGET_TYPE_CODE,BudgetTypeEnum.SIMPLE.name());
        rowData.put(ProjectConstants.SUB_PROJECT_NUMBER,TEST_SUB_PROJECT_NUMBER);
        rowData.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT,TEST_INDIVIDUAL_GIVING_LIMIT);
        rowData.put(ProjectConstants.INDIVIDUAL_USED_AMOUNT,TEST_INDIVIDUAL_USED_AMOUNT);
        rowData.put(ProjectConstants.BUDGET_CONSTRAINT, BudgetConstraintEnum.SOFT_BUDGET.name());
        rowData.put(ProjectConstants.PROJECT_NUMBER, TestConstants.PROJECT_NUMBER);
        rowData.put(ProjectConstants.FROM_DATE,new Date());
        rowData.put(ProjectConstants.THRU_DATE,new Date());
        rowData.put(ProjectConstants.CREATE_DATE,new Date());
        rowData.put(ProjectConstants.BUDGET_OWNER,TEST_BUDGET_OWNER_PAX_ID);
        rowData.put(ProjectConstants.STATUS_TYPE_CODE,StatusTypeCode.ACTIVE.name());
        rowData.put(ProjectConstants.IS_USED,TEST_IS_USED);
        rowData.put(ProjectConstants.BUDGET_USER,TEST_BUDGET_USER_PAX_ID);
        rowData.put(ProjectConstants.BUDGET_USER+"_ID",TEST_BUDGET_USER_PAX_ID);
        rowData.put(ProjectConstants.BUDGET_ALLOCATOR,TEST_BUDGET_ALLOCATOR_PAX_ID);
        rowData.put(ProjectConstants.ALLOCATED_IN,TEST_ALLOCATED_IN);
        rowData.put(ProjectConstants.ALLOCATED_OUT,TEST_ALLOCATED_OUT);
        return rowData;
    }

    //********************************* Create/Update Budget Error Messages **********************************

    @Test
    public void update_budget_forbidden_non_admin() {
        //Must be an admin or the allocator to edit a budget
        budgetDto.setParentBudgetId(5L);
        Mockito.when(security.hasRole(RoleCode.ADMIN)).thenReturn(Boolean.FALSE);

        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - must be admin or allocator to edit budget");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.ERROR_MUST_BE_ALLOCATOR_TO_EDIT, errorMsgException);
        }
    }

    @Test
    public void update_budget_forbidden_not_allocator() {
        //Must be an admin or the allocator to edit a budget
        budgetDto.setParentBudgetId(5L);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO greg = new GroupMemberStubDTO();
        greg.setPaxId(847L);
        budgetDetails.setBudgetUsers(Arrays.asList(greg));
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDetails.setCanAllocate(false);
        budgetDto.setBudgetDetails(budgetDetails);
        Mockito.when(security.hasRole(RoleCode.ADMIN)).thenReturn(Boolean.FALSE);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - must be admin or allocator to edit budget");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.ERROR_MUST_BE_ALLOCATOR_TO_EDIT, errorMsgException);
        }
    }

    @Test
    public void update_budget_forbidden_proxy_doesnt_have_permission() {
        budgetDto.setParentBudgetId(5L);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO greg = new GroupMemberStubDTO();
        greg.setPaxId(847L);
        budgetDetails.setBudgetUsers(Arrays.asList(greg));
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDetails.setCanAllocate(false);
        budgetDto.setBudgetDetails(budgetDetails);
        Mockito.when(security.isImpersonated()).thenReturn(true);
        Mockito.when(proxyService.doesPaxHaveProxyPermission(Matchers.anyString(), Matchers.anyLong(), Matchers.anyLong())).thenReturn(false);
        Mockito.when(security.impersonatorHasPermission(Matchers.anyString())).thenReturn(false);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - proxy does not have permission");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, NominationConstants.PROXY_NOT_ALLOWED_ERROR_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void update_budget_mismatched_ids() {
        //budgetId in URL does not match budgetId in JSON
        try {
            budgetService.updateBudgetById(TestConstants.ID_2, budgetDto);
            Assert.fail("Should have thrown error - budgetId in path does not match budgetId in JSON");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.ERROR_MISMATCHED_BUDGET_IDS, errorMsgException);
        }
    }

    @Test
    public void update_budget_invalid_budget_id() {
        //budgetId does not exist in the system
        budgetDto.setBudgetId(99999L);
        Mockito.when(cnxtBudgetRepository.findOne(99999L)).thenReturn(null);
        try {
            budgetService.updateBudgetById(99999L, budgetDto);
            Assert.fail("Should have thrown error - budgetId does not exist");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.ERROR_INVALID_BUDGET_ID, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_name() {
        //budgetName is null
        budgetDto.setName(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - null budget name");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_NAME_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_type() {
        //type is null
        budgetDto.setType(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - type is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_TYPE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_from_date() {
        //fromDate is null
        budgetDto.setFromDate(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - from date is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_DATE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_budget_constraint() {
        //budgetConstraint is null
        budgetDto.setBudgetConstraint(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - budget constraint is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_CONSTRAINT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_project_number() {
        //projectNumber is null
        budgetDto.setProjectNumber(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - project number is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_PROJECT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_total_amount_negative() {
        //totalAmount is less than or equal to 0 (this only applies to child budgets)
        budgetDto.setParentBudgetId(5L);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        GroupMemberStubDTO leah = new GroupMemberStubDTO();
        leah.setPaxId(8871L);
        budgetDetails.setBudgetUsers(Arrays.asList(leah));
        budgetDetails.setAllocatedIn(100D); // allocatedOut = 200, will be negative
        budgetDto.setBudgetDetails(budgetDetails);
        budgetDto.setUsedAmount(null);

        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - total amount is negative");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(
                    Arrays.asList(BudgetConstants.ERROR_AMOUNT_NEGATIVE, BudgetConstants.ERROR_UNDER_USED_AMOUNT), errorMsgException);
        }
    }

    @Test
    public void validate_budget_total_amount_less_than_used_amount() {
        //totalAmount is less than usedAmount
        Mockito.when(cnxtBudgetRepository.getBudgedUsed(Matchers.anyLong())).thenReturn(100);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setAllocatedIn(250D); // allocatedOut = 200, will be 50 total
        budgetDto.setBudgetDetails(budgetDetails);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - total amount less than used amount");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.TOTAL_AMOUNT_UNDER_USED_AMOUNT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_type() {
        //type is invalid
        budgetDto.setType("Invalid");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid type");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_TYPE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_accounting_id_exceeds_max_length() {
        //accountingId is longer than 8 characters
        budgetDto.setAccountingId("0123456789");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid accounting id");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_ACCOUNTING_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_accounting_id_invalid() {
        //accountingId is not alphanumeric
        budgetDto.setAccountingId("!@#$%");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - accounting id not alphanumeric");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.ACCOUNTING_ID_FORMAT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_negative_individual_giving_limit() {
        //individualGivingLimit is less than or equal to 0
        budgetDto.setIndividualGivingLimit(0.0);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - giving limit is negative");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.GIVING_LIMIT_NEGATIVE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_individual_giving_limit_over_total_amount() {
        //individualGivingLimit is more than the totalAmount
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setAllocatedIn(250D); // allocatedOut = 200, will be 50 total
        budgetDto.setBudgetDetails(budgetDetails);
        budgetDto.setIndividualGivingLimit(200.0);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - giving limit over total amount");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.GIVING_LIMIT_OVER_TOTAL_AMOUNT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_from_date() {
        //fromDate is not a valid date format
        budgetDto.setFromDate("Nov 24 2016");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid from date format");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_FROM_DATE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_thru_date() {
        //thruDate is not a valid date format
        budgetDto.setThruDate("Dec 31 2099");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid thru date format");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_THRU_DATE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_from_date_past_thru_date() {
        //fromDate is later than thruDate
        budgetDto.setFromDate("2016-12-31");
        budgetDto.setThruDate("2016-01-01");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - from date later than thru date");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_DATE_RANGE_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_budget_constraint() {
        //budgetConstraint is invalid
        budgetDto.setBudgetConstraint("Invalid");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid budget constraint");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_CONSTRAINT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_budget_owner() {
        //budgetOwner paxId does not exist
        budgetDto.setBudgetOwner(new EmployeeDTO().setPaxId(99999L));
        Mockito.when(paxDao.findById(99999L)).thenReturn(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid budget owner");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_PAX_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_inactive_budget_owner() {
        //budgetOwner cannot be INACTIVE
        budgetDto.setBudgetOwner(new EmployeeDTO().setPaxId(99999L));
        budgetDto.setBudgetId(null);
        SysUser sysUser = new SysUser();
        sysUser.setStatusTypeCode(StatusTypeCode.INACTIVE.name());
        Mockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.where(ProjectConstants.PAX_ID)).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.eq(99999L)).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.findOne()).thenReturn(sysUser);
        try {
            budgetService.createBudget(budgetDto);
            Assert.fail("Should have thrown error - budget owner is inactive");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INACTIVE_PAX_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_missing_budget_owner_pax() {
        //budgetOwner paxId is missing
        budgetDto.setBudgetOwner(new EmployeeDTO().setPaxId(null));
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
        } catch (ErrorMessageException errorMsgException) {
            Assert.fail("Should not have thrown error - budget owner pax id can be missing");
        }
    }

    @Test
    public void validate_budget_missing_budget_details() {
        //budgetDetails object is null (OK for simple budgets but the test budget is Al-located)
        budgetDto.setBudgetDetails(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - budget details is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.MISSING_BUDGET_DETAILS_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_missing_budget_users() {
        //This does not apply to simple budgets or top level allocated & distributed budgets
        //budgetUsers is empty
        budgetDto.setParentBudgetId(5L);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setBudgetUsers(null);
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDto.setBudgetDetails(budgetDetails);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - budget users is empty");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.MISSING_BUDGET_USERS_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_budget_user_pax() {
        //budgetUser paxId does not exist
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO pax = new GroupMemberStubDTO().setPaxId(99999L);
        budgetDetails.setBudgetUsers(Arrays.asList(pax));
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDto.setBudgetDetails(budgetDetails);
        Mockito.when(paxDao.findById(99999L)).thenReturn(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid budget user (pax)");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_PAX_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_inactive_budget_user_pax() {
        //budgetUser (pax) cannot be INACTIVE
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO pax = new GroupMemberStubDTO().setPaxId(99999L);
        budgetDetails.setBudgetUsers(Arrays.asList(pax));
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDto.setBudgetDetails(budgetDetails);
        budgetDto.setBudgetOwner(null);
        budgetDto.setBudgetId(null);

        SysUser sysUserInactive = new SysUser();
        sysUserInactive.setStatusTypeCode(StatusTypeCode.INACTIVE.name());
        Mockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.where(ProjectConstants.PAX_ID)).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.eq(99999L)).thenReturn(sysUserFindBy);
        Mockito.when(sysUserFindBy.findOne()).thenReturn(sysUserInactive);

        try {
            budgetService.createBudget(budgetDto);
            Assert.fail("Should have thrown error - budget user is inactive (pax)");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INACTIVE_PAX_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_budget_user_group() {
        //groupId does not exist or is INACTIVE
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO group = new GroupMemberStubDTO().setGroupId(99999L);
        budgetDetails.setBudgetUsers(Arrays.asList(group));
        budgetDetails.setAllocatedIn(500.0);
        budgetDetails.setAllocatedOut(200.0);
        budgetDto.setBudgetDetails(budgetDetails);
        budgetDto.setBudgetId(null);
        Mockito.when(groupsDao.findById(99999L)).thenReturn(null);
        try {
            budgetService.createBudget(budgetDto);
            Assert.fail("Should have thrown error - invalid budget user - group id");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_GROUP_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_missing_allocated_in() {
        //allocatedIn is null
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setAllocatedIn(null);
        budgetDto.setBudgetDetails(budgetDetails);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - allocated in is missing");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.MISSING_BUDGET_ALLOCATED_IN_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_invalid_parent_id() {
        //parentBudgetId does not exist in the system
        BudgetRequestDTO childBudget = budgetDto.getChildBudgets().get(0);
        childBudget.setParentBudgetId(99999L);
        budgetDto.setChildBudgets(Arrays.asList(childBudget));
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid parent budget id");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_PARENT_BUDGET_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_user_duplication_fine() {
        BudgetRequestDTO childBudget = budgetDto.getChildBudgets().get(0);
        BudgetRequestDTO secondBudget = ObjectUtils.jsonToObject(ObjectUtils.objectToJson(childBudget), BudgetRequestDTO.class);
        secondBudget.getBudgetDetails().setBudgetUsers(Arrays.asList(
                new GroupMemberStubDTO().setPaxId(1L),
                new GroupMemberStubDTO().setPaxId(2L)
            ));
        budgetDto.setChildBudgets(Arrays.asList(childBudget, secondBudget.setBudgetId(null)));

        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
        } catch (ErrorMessageException errorMsgException) {
            Assert.fail("Should not have thrown errors!");
        }
    }

    //********************************* Create/Update Budget **************************************

    @Test
    public void test_create_update_budget_assigned_to_pax() {
        //create and update call all the same methods so this test covers both
        Mockito.when(security.isImpersonated()).thenReturn(true);
        Mockito.when(proxyService.doesPaxHaveProxyPermission(Matchers.anyString(), Matchers.anyLong(), Matchers.anyLong())).thenReturn(true);
        Mockito.when(security.impersonatorHasPermission(Matchers.anyString())).thenReturn(true);

        List<EntityDTO> entityList = new ArrayList<>();
        EntityDTO greg = new EmployeeDTO().setPaxId(847L);
        EntityDTO leah = new EmployeeDTO().setPaxId(8571L);
        EntityDTO alex = new EmployeeDTO().setPaxId(8987L);
        entityList.add(greg);
        entityList.add(leah);
        entityList.add(alex);
        Mockito.when(participantInfoDao.getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class))).thenReturn(entityList);

        GroupMemberStubDTO paxStub = new GroupMemberStubDTO().setPaxId(8571L);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setBudgetUsers(Arrays.asList(paxStub));
        budgetDetails.setCanAllocate(Boolean.TRUE);
        budgetDto.setBudgetDetails(budgetDetails);
        budgetDto.setDescription(ProjectConstants.EMPTY_STRING);

        budgetDto.setProxyPaxId(TestConstants.ID_2);

        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
        Mockito.verify(proxyService, Mockito.times(1)).doesPaxHaveProxyPermission(Matchers.anyString(), Matchers.anyLong(), Matchers.anyLong());
    }

    @Test
    public void test_create_update_budget_assigned_to_group() {
        //create and update call all the same methods so this test covers both
        List<EntityDTO> entityList = new ArrayList<>();
        EntityDTO group = new GroupDTO().setGroupId(1534L);
        entityList.add(group);
        Mockito.when(participantInfoDao.getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class))).thenReturn(entityList);

        GroupMemberStubDTO groupStub = new GroupMemberStubDTO().setGroupId(1534L);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setBudgetUsers(Arrays.asList(groupStub));
        budgetDetails.setCanAllocate(Boolean.TRUE);
        budgetDto.setBudgetDetails(budgetDetails);

        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }

    @Test
    public void test_create_child_budget_top_level_id_exists() {
        // this test covers budgets created/updated via the Manage My Budgets page
        budgetDto = budgetDto.getChildBudgets().get(0).getChildBudgets().get(0);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setAllocatedOut(50D);
        budgetDetails.setCanAllocate(true);
        budgetDto.setBudgetDetails(budgetDetails);
        //Set up a new child budget to be created
        BudgetRequestDTO childBudget = new BudgetRequestDTO();
        childBudget.setParentBudgetId(budgetDto.getBudgetId());
        childBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        childBudget.setDescription("Budget Description");
        childBudget.setFromDate("2017-11-10");
        childBudget.setName("Budget Name");
        childBudget.setProjectNumber(TestConstants.PROJECT_NUMBER);
        childBudget.setStatus(StatusTypeCode.ACTIVE.name());
        childBudget.setTotalAmount(50D);
        childBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        childBudget.setAccountingId("UnitTest");
        budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setAllocatedIn(50D);
        budgetDetails.setAllocatedOut(0D);
        budgetDetails.setCanAllocate(false);
        GroupMemberStubDTO paxStub = new GroupMemberStubDTO().setPaxId(8571L);
        budgetDetails.setBudgetUsers(Arrays.asList(paxStub));
        childBudget.setBudgetDetails(budgetDetails);
        budgetDto.setChildBudgets(Arrays.asList(childBudget));

        List<EntityDTO> entityList = new ArrayList<>();
        EntityDTO greg = new EmployeeDTO().setPaxId(847L);
        EntityDTO leah = new EmployeeDTO().setPaxId(8571L);
        EntityDTO alex = new EmployeeDTO().setPaxId(8987L);
        entityList.add(greg);
        entityList.add(leah);
        entityList.add(alex);
        Mockito.when(participantInfoDao.getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class))).thenReturn(entityList);

        budgetService.updateBudgetById(TestConstants.ID_3, budgetDto);
        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(2)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }

    @Test
    public void test_create_child_budget_top_level_id_is_null() {
        // this test covers budgets created/updated via the Manage My Budgets page
        budgetDto = budgetDto.getChildBudgets().get(0).getChildBudgets().get(0);
        BudgetDetailsRequestDTO budgetDetails = budgetDto.getBudgetDetails();
        budgetDetails.setAllocatedOut(50D);
        budgetDetails.setCanAllocate(true);
        budgetDto.setBudgetDetails(budgetDetails);
        //Set up a new child budget to be created
        BudgetRequestDTO childBudget = new BudgetRequestDTO();
        childBudget.setParentBudgetId(budgetDto.getBudgetId());
        childBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        childBudget.setDescription("Budget Description");
        childBudget.setFromDate("2017-11-10");
        childBudget.setName("Budget Name");
        childBudget.setProjectNumber(TestConstants.PROJECT_NUMBER);
        childBudget.setStatus(StatusTypeCode.ACTIVE.name());
        childBudget.setTotalAmount(50D);
        childBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        childBudget.setAccountingId("UnitTest");
        budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setAllocatedIn(50D);
        budgetDetails.setAllocatedOut(0D);
        budgetDetails.setCanAllocate(false);
        GroupMemberStubDTO paxStub = new GroupMemberStubDTO().setPaxId(8571L);
        budgetDetails.setBudgetUsers(Arrays.asList(paxStub));
        childBudget.setBudgetDetails(budgetDetails);
        budgetDto.setChildBudgets(Arrays.asList(childBudget));

        List<EntityDTO> entityList = new ArrayList<>();
        EntityDTO greg = new EmployeeDTO().setPaxId(847L);
        EntityDTO leah = new EmployeeDTO().setPaxId(8571L);
        EntityDTO alex = new EmployeeDTO().setPaxId(8987L);
        entityList.add(greg);
        entityList.add(leah);
        entityList.add(alex);
        Mockito.when(participantInfoDao.getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class))).thenReturn(entityList);

        Mockito.when(budgetMiscDao.findBy()
                .where(ProjectConstants.BUDGET_ID).eq(TestConstants.ID_3)
                .and(ProjectConstants.VF_NAME).eq(VfName.TOP_LEVEL_BUDGET_ID.name())
                .findOne(ProjectConstants.MISC_DATA, String.class)).thenReturn(null);

        budgetService.updateBudgetById(TestConstants.ID_3, budgetDto);
        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(2)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }
    @Test
    public void test_allocation_logic_top_level_zero_budget() {
        Mockito.when(budgetMiscAllocatedInFindBy.findOne()).thenReturn(new BudgetMisc().setMiscData("0"));

        budgetDto.getBudgetDetails().setAllocatedOut(500D);
        BudgetRequestDTO childBudget = budgetDto.getChildBudgets().get(0);
        childBudget.getBudgetDetails().setAllocatedIn(500D);
        childBudget.getBudgetDetails().setAllocatedOut(0D);
        childBudget.setChildBudgets(null);

        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);

        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(2)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }

    @Test
    public void test_allocation_logic_decrease_child_budget_amount_with_parent_budget_having_zero_points() {
        Mockito.when(budgetMiscAllocatedInFindBy.findOne()).thenReturn(new BudgetMisc().setMiscData("1000"));

        budgetDto.getBudgetDetails().setAllocatedOut(500D);
        BudgetRequestDTO childBudget = budgetDto.getChildBudgets().get(0);
        childBudget.getBudgetDetails().setAllocatedIn(500D);

        //Decreasing the child budget total from 500 to 100 (removing it completely from the tree - NOT rolling it back up to the parent)
        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);

        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(3)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(3)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));

    }

    @Test
    public void test_allocation_logic_skipped_level_decrease_budget() {
        Mockito.when(cnxtBudgetRepository.getTotalBudget(TestConstants.ID_1)).thenReturn(null);

        //The middle budget has 0 total budget - special scenario when allocation needs to "skip" a level
        BudgetRequestDTO childBudget1 = budgetDto.getChildBudgets().get(0);
        childBudget1.getBudgetDetails().setAllocatedOut(200D);
        BudgetRequestDTO childBudget2 = childBudget1.getChildBudgets().get(0);
        childBudget2.getBudgetDetails().setAllocatedIn(200D);
        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);

        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }

    @Test
    public void test_allocation_logic_skipped_level_increase_budget() {
        Mockito.when(cnxtBudgetRepository.getTotalBudget(TestConstants.ID_1)).thenReturn(null);
        Mockito.when(budgetMiscAllocatedInFindBy.findOne()).thenReturn(new BudgetMisc().setMiscData("0"));

        //The middle budget has 0 total budget - special scenario when allocation needs to "skip" a level
        BudgetRequestDTO childBudget1 = budgetDto.getChildBudgets().get(0);
        childBudget1.getBudgetDetails().setAllocatedOut(200D);
        BudgetRequestDTO childBudget2 = childBudget1.getChildBudgets().get(0);
        childBudget2.getBudgetDetails().setAllocatedIn(200D);
        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);

        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(3)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(3)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }


    @Test
    public void test_allocation_logic_decrease_budget() {
        Mockito.when(cnxtBudgetRepository.getTotalBudget(TestConstants.ID_1)).thenReturn(500);
        budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);

        Mockito.verify(participantInfoDao, Mockito.times(3)).getInfo(Matchers.anyListOf(Long.class), Matchers.anyListOf(Long.class));
        Mockito.verify(budgetMiscAllocatedInFindBy, Mockito.times(3)).findOne();
        Mockito.verify(transactionHeaderDao, Mockito.times(2)).save(Matchers.any(TransactionHeader.class));
        Mockito.verify(discretionaryDao, Mockito.times(2)).save(Matchers.any(Discretionary.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveACLEntries(Matchers.anyCollectionOf(Acl.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveBudgetMiscs(Matchers.anyCollectionOf(BudgetMisc.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).saveGroupsPaxEntries(Matchers.anyCollectionOf(GroupsPax.class));
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetOwnerGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).cleanUpBudgetAllocatorGroupMembership();
        Mockito.verify(budgetBulkDataManagementDao, Mockito.times(1)).findExistingSubProjects(Matchers.anyString());
        Mockito.verify(cnxtBudgetRepository, Mockito.times(1)).save(Matchers.anyCollectionOf(Budget.class));
    }


    //********************************* Get Budget By ID *****************************************

    @Test
    public void test_getBudgetById_fail_missing_budgetId() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_MISSING_BUDGET_ID));

        budgetService.getBudgetById(null);
    }

    @Test
    public void test_getBudgetById_success_type_simple() {

        Map<String, Object> mockedSimpleBudget = mockedRowResponseBudgetById();
        Mockito.when(budgetInfoDao.getBudgetInfoById(Matchers.anyLong()))
            .thenReturn(Arrays.asList(mockedSimpleBudget));

        mockedSimpleBudget.remove(ProjectConstants.BUDGET_USER);
        mockedSimpleBudget.remove(ProjectConstants.BUDGET_USER+"_ID");
        mockedSimpleBudget.remove(ProjectConstants.BUDGET_ALLOCATOR);
        mockedSimpleBudget.remove(ProjectConstants.ALLOCATED_IN);
        mockedSimpleBudget.remove(ProjectConstants.ALLOCATED_OUT);
        BudgetDTO response = budgetService.getBudgetById(TestConstants.ID_1);

        Assert.assertNotNull(response);

        Assert.assertEquals(TestConstants.ID_1, response.getBudgetId());
        Assert.assertEquals(TestConstants.ID_2, response.getParentBudgetId());
        Assert.assertEquals(TEST_BUDGET_NAME, response.getName());
        Assert.assertEquals(TEST_BUDGET_DISPLAY_NAME, response.getDisplayName());
        Assert.assertEquals(TEST_BUDGET_DESC, response.getDescription());
        Assert.assertEquals(TEST_TOTAL_AMOUNT, response.getTotalAmount());
        Assert.assertEquals(TEST_TOTAL_USED, response.getUsedAmount());
        Assert.assertEquals(BudgetTypeEnum.SIMPLE.name(), response.getType());
        Assert.assertEquals(TEST_SUB_PROJECT_NUMBER, response.getAccountingId());
        Assert.assertEquals(TEST_INDIVIDUAL_GIVING_LIMIT, response.getIndividualGivingLimit());
        Assert.assertEquals(TEST_INDIVIDUAL_USED_AMOUNT, response.getIndividualUsedAmount());
        Assert.assertEquals(Boolean.parseBoolean(TEST_IS_USED), response.getUsedInProgram());
        Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.getStatus());
        Assert.assertEquals(TestConstants.PROJECT_NUMBER, response.getProjectNumber());
        Assert.assertEquals(TEST_BUDGET_OWNER_PAX_ID, response.getBudgetOwner().getPaxId());
    }

    @Test
    public void test_getBudgetById_success_type_distributed() {

        mockedResponseTopBudget.put(ProjectConstants.BUDGET_TYPE_CODE,BudgetTypeEnum.DISTRIBUTED.name());

        Map<String, Object> mockedChildBudget1 = mockedRowResponseBudgetById();
        mockedChildBudget1.put(ProjectConstants.ID,TEST_BUDGET_CHILD_1_ID);
        mockedChildBudget1.put(ProjectConstants.PARENT_BUDGET_ID,TestConstants.ID_1);

        Map<String, Object> mockedChildBudget2 = mockedRowResponseBudgetById();
        mockedChildBudget2.put(ProjectConstants.ID,TEST_BUDGET_CHILD_2_ID);
        mockedChildBudget2.put(ProjectConstants.PARENT_BUDGET_ID,TestConstants.ID_1);

        Mockito.when(budgetInfoDao.getBudgetInfoById(Matchers.anyLong()))
            .thenReturn(Arrays.asList(mockedResponseTopBudget,mockedChildBudget1,mockedChildBudget2));

        BudgetDTO response = budgetService.getBudgetById(TestConstants.ID_1);
        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.getBudgetId());
        Assert.assertEquals(BudgetTypeEnum.DISTRIBUTED.name(), response.getType());
        Assert.assertNotNull(response.getBudgetDetails());
        Assert.assertNotNull(response.getChildBudgets());
        Assert.assertEquals(2, response.getChildBudgets().size());
    }

    @Test
    public void test_getBudgetById_success_type_allocated() {

        mockedResponseTopBudget.put(ProjectConstants.BUDGET_TYPE_CODE,BudgetTypeEnum.ALLOCATED.name());

        Map<String, Object> mockedChildBudget1 = mockedRowResponseBudgetById();
        mockedChildBudget1.put(ProjectConstants.ID,TEST_BUDGET_CHILD_1_ID);
        mockedChildBudget1.put(ProjectConstants.PARENT_BUDGET_ID,TestConstants.ID_1);

        Map<String, Object> mockedChildBudget2 = mockedRowResponseBudgetById();
        mockedChildBudget2.put(ProjectConstants.ID,TEST_BUDGET_CHILD_2_ID);
        mockedChildBudget2.put(ProjectConstants.PARENT_BUDGET_ID,TEST_BUDGET_CHILD_1_ID);

        Mockito.when(budgetInfoDao.getBudgetInfoById(Matchers.anyLong()))
            .thenReturn(Arrays.asList(mockedResponseTopBudget,mockedChildBudget1,mockedChildBudget2));

        Map<String, Object> pax = new HashMap<>();
        pax.put(ProjectConstants.BUDGET_USER_TYPE, "PAX");
        pax.put(ProjectConstants.NAME, "Leah Harwell");
        pax.put(ProjectConstants.SEARCH_TYPE, "P");
        pax.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        pax.put(ProjectConstants.PAX_ID, 5L);
        pax.put(ProjectConstants.LANGUAGE_CODE, "en");
        pax.put(ProjectConstants.PREFERRED_LOCALE, "en_US");
        pax.put(ProjectConstants.LANGUAGE_LOCALE, "en_US");
        pax.put(ProjectConstants.CONTROL_NUM, "8571");
        pax.put(ProjectConstants.FIRST_NAME, "Leah");
        pax.put(ProjectConstants.MIDDLE_NAME, "M");
        pax.put(ProjectConstants.LAST_NAME, "Harwell");
        pax.put(ProjectConstants.COMPANY_NAME, "MM");
        pax.put(ProjectConstants.JOB_TITLE, "Services Engineer");
        pax.put(ProjectConstants.SYS_USER_ID, 8571L);
        pax.put(ProjectConstants.MANAGER_PAX_ID, 847L);
        pax.put(ProjectConstants.BUDGET_ID,TestConstants.ID_1);
        pax.put(ProjectConstants.BUDGET_USER_ID,5L);

        Map<String, Object> group = new HashMap<>();
        group.put(ProjectConstants.BUDGET_USER_TYPE, "GROUPS");
        group.put(ProjectConstants.NAME, "The Cool Area");
        group.put(ProjectConstants.SEARCH_TYPE, "G");
        group.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        group.put(ProjectConstants.GROUP_ID, 2911L);
        group.put(ProjectConstants.GROUP_CONFIG_ID, TestConstants.ID_2);
        group.put(ProjectConstants.FIELD, "AREA");
        group.put(ProjectConstants.FIELD_DISPLAY_NAME, "Area");
        group.put(ProjectConstants.GROUP_NAME,"The Cool Area");
        group.put(ProjectConstants.GROUP_TYPE, "ENROLLMENT");
        group.put(ProjectConstants.PAX_COUNT, 1);
        group.put(ProjectConstants.GROUP_COUNT, 0);
        group.put(ProjectConstants.TOTAL_PAX_COUNT, 1);
        group.put(ProjectConstants.BUDGET_ID,TestConstants.ID_1);
        group.put(ProjectConstants.BUDGET_USER_ID,2911L);


        Mockito.when(budgetInfoDao.getBudgetUsers(Matchers.anyListOf(Long.class))).thenReturn(Arrays.asList(pax, group));

        BudgetDTO response = budgetService.getBudgetById(TestConstants.ID_1);

        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.getBudgetId());
        Assert.assertEquals(BudgetTypeEnum.ALLOCATED.name(), response.getType());
        Assert.assertNotNull(response.getBudgetDetails());
        Assert.assertNotNull(response.getChildBudgets());
        Assert.assertEquals(1, response.getChildBudgets().size());
        Assert.assertEquals(1, response.getChildBudgets().get(0).getChildBudgets().size());
    }


    //********************************* Get Eligible Budgets for Individual *****************************************


    @Test
    public void test_getIndividualEligibleBudgets_invalid_pax() {
        // PaxRepository - participant not exists
        Mockito.when(paxRepository.exists(Matchers.anyLong())).thenReturn(Boolean.FALSE);

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_INVALID_PAX));

        budgetService.getIndividualEligibleBudgets(TestConstants.ID_1, true);
    }

    @Test
    public void test_getIndividualEligibleBudgets_not_allowed_pax() {
        // Security - participant trying to access budgets that are not their own
        Mockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.FALSE);
        Mockito.when(security.isMyPax(Matchers.anyLong())).thenReturn(Boolean.FALSE);


        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_NOT_ADMIN));

        budgetService.getIndividualEligibleBudgets(TestConstants.ID_1, true);
    }

    @Test
    public void test_getIndividualEligibleBudgets_success() {
        List<BudgetDTO> response = budgetService.getIndividualEligibleBudgets(TestConstants.ID_1, Boolean.TRUE);

        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.get(0).getBudgetId());
        Assert.assertEquals(TestConstants.ID_2, response.get(0).getParentBudgetId());
        Assert.assertEquals(TEST_BUDGET_NAME, response.get(0).getName());
        Assert.assertEquals(TEST_BUDGET_DISPLAY_NAME, response.get(0).getDisplayName());
        Assert.assertEquals(TEST_BUDGET_DESC, response.get(0).getDescription());
        Assert.assertEquals(TEST_TOTAL_AMOUNT, response.get(0).getTotalAmount());
        Assert.assertEquals(TEST_TOTAL_USED, response.get(0).getUsedAmount());
        Assert.assertEquals(BudgetTypeEnum.SIMPLE.name(), response.get(0).getType());
        Assert.assertEquals(TEST_SUB_PROJECT_NUMBER, response.get(0).getAccountingId());
        Assert.assertEquals(TEST_INDIVIDUAL_GIVING_LIMIT, response.get(0).getIndividualGivingLimit());
        Assert.assertEquals(TEST_INDIVIDUAL_USED_AMOUNT, response.get(0).getIndividualUsedAmount());
        Assert.assertEquals(Boolean.parseBoolean(TEST_IS_USED), response.get(0).getUsedInProgram());
        Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.get(0).getStatus());
        Assert.assertEquals(TestConstants.PROJECT_NUMBER, response.get(0).getProjectNumber());
        Assert.assertEquals(TEST_BUDGET_OWNER_PAX_ID, response.get(0).getBudgetOwner().getPaxId());
    }

    @Test
    public void test_getIndividualEligibleBudgets_success_other_budgets() {
        // Trying to access budgets that are not their own with ADMIN permissions.
        Mockito.when(security.isMyPax(Matchers.anyLong()))
        .thenReturn(Boolean.FALSE);

        List<BudgetDTO> response = budgetService.getIndividualEligibleBudgets(TestConstants.ID_1, Boolean.TRUE);

        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.get(0).getBudgetId());
        Assert.assertEquals(TestConstants.ID_2, response.get(0).getParentBudgetId());
        Assert.assertEquals(TEST_BUDGET_NAME, response.get(0).getName());
        Assert.assertEquals(TEST_BUDGET_DISPLAY_NAME, response.get(0).getDisplayName());
        Assert.assertEquals(TEST_BUDGET_DESC, response.get(0).getDescription());
        Assert.assertEquals(TEST_TOTAL_AMOUNT, response.get(0).getTotalAmount());
        Assert.assertEquals(TEST_TOTAL_USED, response.get(0).getUsedAmount());
        Assert.assertEquals(BudgetTypeEnum.SIMPLE.name(), response.get(0).getType());
        Assert.assertEquals(TEST_SUB_PROJECT_NUMBER, response.get(0).getAccountingId());
        Assert.assertEquals(TEST_INDIVIDUAL_GIVING_LIMIT, response.get(0).getIndividualGivingLimit());
        Assert.assertEquals(TEST_INDIVIDUAL_USED_AMOUNT, response.get(0).getIndividualUsedAmount());
        Assert.assertEquals(Boolean.parseBoolean(TEST_IS_USED), response.get(0).getUsedInProgram());
        Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.get(0).getStatus());
        Assert.assertEquals(TestConstants.PROJECT_NUMBER, response.get(0).getProjectNumber());
        Assert.assertEquals(TEST_BUDGET_OWNER_PAX_ID, response.get(0).getBudgetOwner().getPaxId());
    }


    //********************************* Get Eligible Budgets for Pax and Program *****************************************


    @Test
    public void test_getEligibleBudgetsForProgram_invalid_pax() {
        // PaxRepository - participant not exists
        Mockito.when(paxRepository.exists(Matchers.anyLong())).thenReturn(Boolean.FALSE);

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_INVALID_PAX));

        budgetService.getEligibleBudgetsForProgram(TestConstants.ID_1, TestConstants.ID_1);
    }

    @Test
    public void test_getEligibleBudgetsForProgram_not_allowed_pax() {
        // Security - participant trying to access budgets that are not their own
        Mockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.FALSE);
        Mockito.when(security.isMyPax(Matchers.anyLong())).thenReturn(Boolean.FALSE);

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_NOT_ADMIN));

        budgetService.getEligibleBudgetsForProgram(TestConstants.ID_1, TestConstants.ID_1);
    }

    @Test
    public void test_getEligibleBudgetsForProgram_missing_program() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_NULL_PROGRAM_ID_MSG));

        budgetService.getEligibleBudgetsForProgram(8571L, null);
    }

    @Test
    public void test_getEligibleBudgetsForProgram_invalid_program() {
        // Program Repository - program doesn't exist
        Mockito.when(programRepository.findOne(Matchers.anyLong())).thenReturn(null);

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_INVALID_PROGRAM_ID_MSG));

        budgetService.getEligibleBudgetsForProgram(8571L, TestConstants.ID_1);
    }

    @Test
    public void test_getEligibleBudgetsForProgram_inactive_program() {
        // StatusTypeUtil - program is not ACTIVE
        Mockito.when(programRepository.findOne(Matchers.anyLong())).thenReturn(
                new Program().setStatusTypeCode(StatusTypeCode.INACTIVE.name()));

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(org.hamcrest.Matchers.containsString(BudgetConstants.ERROR_INACTIVE_PROGRAM_MSG));

        budgetService.getEligibleBudgetsForProgram(8571L, TestConstants.ID_1);
    }

    @Test
    public void test_getEligibleBudgetsForProgram_success() {
        List<BudgetDTO> response = budgetService.getEligibleBudgetsForProgram(8571L, TestConstants.ID_1);

        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.get(0).getBudgetId());
        Assert.assertEquals(TestConstants.ID_2, response.get(0).getParentBudgetId());
        Assert.assertEquals(TEST_BUDGET_NAME, response.get(0).getName());
        Assert.assertEquals(TEST_BUDGET_DISPLAY_NAME, response.get(0).getDisplayName());
        Assert.assertEquals(TEST_BUDGET_DESC, response.get(0).getDescription());
        Assert.assertEquals(TEST_TOTAL_AMOUNT, response.get(0).getTotalAmount());
        Assert.assertEquals(TEST_TOTAL_USED, response.get(0).getUsedAmount());
        Assert.assertEquals(BudgetTypeEnum.SIMPLE.name(), response.get(0).getType());
        Assert.assertEquals(TEST_SUB_PROJECT_NUMBER, response.get(0).getAccountingId());
        Assert.assertEquals(TEST_INDIVIDUAL_GIVING_LIMIT, response.get(0).getIndividualGivingLimit());
        Assert.assertEquals(TEST_INDIVIDUAL_USED_AMOUNT, response.get(0).getIndividualUsedAmount());
        Assert.assertEquals(Boolean.parseBoolean(TEST_IS_USED), response.get(0).getUsedInProgram());
        Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.get(0).getStatus());
        Assert.assertEquals(TestConstants.PROJECT_NUMBER, response.get(0).getProjectNumber());
        Assert.assertEquals(TEST_BUDGET_OWNER_PAX_ID, response.get(0).getBudgetOwner().getPaxId());
    }

    @Test
    public void test_getEligibleBudgetsForProgram_success_other_budgets() {
        // Trying to access budgets that are not their own with ADMIN permissions.
        Mockito.when(security.isMyPax(Matchers.anyLong()))
        .thenReturn(Boolean.FALSE);

        List<BudgetDTO> response = budgetService.getEligibleBudgetsForProgram(8571L, TestConstants.ID_1);

        Assert.assertNotNull(response);
        Assert.assertEquals(TestConstants.ID_1, response.get(0).getBudgetId());
        Assert.assertEquals(TestConstants.ID_2, response.get(0).getParentBudgetId());
        Assert.assertEquals(TEST_BUDGET_NAME, response.get(0).getName());
        Assert.assertEquals(TEST_BUDGET_DISPLAY_NAME, response.get(0).getDisplayName());
        Assert.assertEquals(TEST_BUDGET_DESC, response.get(0).getDescription());
        Assert.assertEquals(TEST_TOTAL_AMOUNT, response.get(0).getTotalAmount());
        Assert.assertEquals(TEST_TOTAL_USED, response.get(0).getUsedAmount());
        Assert.assertEquals(BudgetTypeEnum.SIMPLE.name(), response.get(0).getType());
        Assert.assertEquals(TEST_SUB_PROJECT_NUMBER, response.get(0).getAccountingId());
        Assert.assertEquals(TEST_INDIVIDUAL_GIVING_LIMIT, response.get(0).getIndividualGivingLimit());
        Assert.assertEquals(TEST_INDIVIDUAL_USED_AMOUNT, response.get(0).getIndividualUsedAmount());
        Assert.assertEquals(Boolean.parseBoolean(TEST_IS_USED), response.get(0).getUsedInProgram());
        Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.get(0).getStatus());
        Assert.assertEquals(TestConstants.PROJECT_NUMBER, response.get(0).getProjectNumber());
        Assert.assertEquals(TEST_BUDGET_OWNER_PAX_ID, response.get(0).getBudgetOwner().getPaxId());
    }


    //********************************* Delete Budget By ID ***************************************

    @Test
    public void test_delete_budget_used() {
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.ID, TestConstants.ID_1);
        node.put(ProjectConstants.IS_USED, "true");
        nodes.add(node);
        Mockito.when(budgetInfoDao.getIsUsedInfo(Matchers.anyLong())).thenReturn(nodes);
        try {
            budgetService.deleteBudgetById(TestConstants.ID_1);
            Assert.fail("Should have thrown error - budget is used");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.BUDGET_USED_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void test_delete_budget_no_children() {
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.ID, 39L);
        node.put(ProjectConstants.IS_USED, null);
        nodes.add(node);
        Mockito.when(budgetInfoDao.getIsUsedInfo(Matchers.anyLong())).thenReturn(nodes);

        budgetService.deleteBudgetById(39L);

        Mockito.verify(budgetInfoDao, Mockito.times(1)).inactivateBudgets(Matchers.anyListOf(Long.class));
    }

    @Test
    public void test_delete_budget_has_children() {
        Map<String, Object> node1 = new HashMap<>();
        node1.put(ProjectConstants.ID, 493L);
        node1.put(ProjectConstants.IS_USED, null);
        Map<String, Object> node2 = new HashMap<>();
        node2.put(ProjectConstants.ID, 494L);
        node2.put(ProjectConstants.IS_USED, null);
        Map<String, Object> node3 = new HashMap<>();
        node3.put(ProjectConstants.ID, 495L);
        node3.put(ProjectConstants.IS_USED, null);
        Map<String, Object> node4 = new HashMap<>();
        node4.put(ProjectConstants.ID, 496L);
        node4.put(ProjectConstants.IS_USED, null);
        Mockito.when(budgetInfoDao.getIsUsedInfo(Matchers.anyLong())).thenReturn(Arrays.asList(node1, node2, node3, node4));

        budgetService.deleteBudgetById(493L);

        Mockito.verify(budgetInfoDao, Mockito.times(1)).inactivateBudgets(Matchers.anyListOf(Long.class));
    }

    //********************************* Get budget list ***************************************

    @Test
    public void test_budget_list() {
        PaginatedResponseObject<BudgetDTO> responseObject = budgetService.getBudgetList(
                null, null, null, null, null, null, null, null,ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.DEFAULT_PAGE_SIZE, null
            );

        List<BudgetDTO> budgetList = responseObject.getData();
        Assert.assertNotNull("Should have data.", budgetList);
        Assert.assertFalse("Should have data.", budgetList.isEmpty());
        Assert.assertTrue("Should have four results.", 4 == budgetList.size());

    }

    private List<Map<String, Object>> getBudgetListResults() {
        List<Map<String, Object>> budgetList = new ArrayList<>();

        Map<String, Object> firstEntry = new HashMap<>();

        firstEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 4);
        firstEntry.put(ProjectConstants.ID, 1L);
        firstEntry.put(ProjectConstants.PARENT_BUDGET_ID, null);
        firstEntry.put(ProjectConstants.BUDGET_NAME, "test");
        firstEntry.put(ProjectConstants.BUDGET_DESC, "test");
        firstEntry.put(ProjectConstants.BUDGET_TYPE_CODE, "test");
        firstEntry.put(ProjectConstants.SUB_PROJECT_NUMBER, "test");
        firstEntry.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 10D);
        firstEntry.put(ProjectConstants.INDIVIDUAL_USED_AMOUNT, 0D);
        firstEntry.put(ProjectConstants.BUDGET_CONSTRAINT, "test");
        firstEntry.put(ProjectConstants.PROJECT_NUMBER, "test");
        firstEntry.put(ProjectConstants.FROM_DATE, new Date());
        firstEntry.put(ProjectConstants.THRU_DATE, new Date());
        firstEntry.put(ProjectConstants.CREATE_DATE, new Date());
        firstEntry.put(ProjectConstants.BUDGET_OWNER, 1L);
        firstEntry.put(ProjectConstants.BUDGET_STATUS, StatusTypeCode.ACTIVE.name());
        firstEntry.put(ProjectConstants.BUDGET_ALLOCATOR, 1L);
        firstEntry.put(ProjectConstants.ALLOCATED_IN, "10");
        firstEntry.put(ProjectConstants.ALLOCATED_OUT, "0");
        firstEntry.put(ProjectConstants.IS_USABLE, "true");

        budgetList.add(firstEntry);

        Map<String, Object> secondEntry = new HashMap<>();

        secondEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 4);
        secondEntry.put(ProjectConstants.ID, 2L);
        secondEntry.put(ProjectConstants.PARENT_BUDGET_ID, null);
        secondEntry.put(ProjectConstants.BUDGET_NAME, "test");
        secondEntry.put(ProjectConstants.BUDGET_DESC, "test");
        secondEntry.put(ProjectConstants.BUDGET_TYPE_CODE, "test");
        secondEntry.put(ProjectConstants.SUB_PROJECT_NUMBER, "test");
        secondEntry.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 10D);
        secondEntry.put(ProjectConstants.INDIVIDUAL_USED_AMOUNT, 0D);
        secondEntry.put(ProjectConstants.BUDGET_CONSTRAINT, "test");
        secondEntry.put(ProjectConstants.PROJECT_NUMBER, "test");
        secondEntry.put(ProjectConstants.FROM_DATE, new Date());
        secondEntry.put(ProjectConstants.THRU_DATE, null);
        secondEntry.put(ProjectConstants.CREATE_DATE, new Date());
        secondEntry.put(ProjectConstants.BUDGET_OWNER, null);
        secondEntry.put(ProjectConstants.BUDGET_STATUS, StatusTypeCode.ACTIVE.name());
        secondEntry.put(ProjectConstants.BUDGET_ALLOCATOR, null);
        secondEntry.put(ProjectConstants.ALLOCATED_IN, null); // code coverage
        secondEntry.put(ProjectConstants.ALLOCATED_OUT, null); // code coverage
        secondEntry.put(ProjectConstants.IS_USABLE, "true");

        budgetList.add(secondEntry);

        Map<String, Object> thirdEntry = new HashMap<>();

        thirdEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 4);
        thirdEntry.put(ProjectConstants.ID, 3L);
        thirdEntry.put(ProjectConstants.PARENT_BUDGET_ID, null);
        thirdEntry.put(ProjectConstants.BUDGET_NAME, "test");
        thirdEntry.put(ProjectConstants.BUDGET_DESC, "test");
        thirdEntry.put(ProjectConstants.BUDGET_TYPE_CODE, "test");
        thirdEntry.put(ProjectConstants.SUB_PROJECT_NUMBER, "test");
        thirdEntry.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 10D);
        thirdEntry.put(ProjectConstants.INDIVIDUAL_USED_AMOUNT, 0D);
        thirdEntry.put(ProjectConstants.BUDGET_CONSTRAINT, "test");
        thirdEntry.put(ProjectConstants.PROJECT_NUMBER, "test");
        thirdEntry.put(ProjectConstants.FROM_DATE, new Date());
        thirdEntry.put(ProjectConstants.THRU_DATE, null);
        thirdEntry.put(ProjectConstants.CREATE_DATE, new Date());
        thirdEntry.put(ProjectConstants.BUDGET_OWNER, 1L);
        thirdEntry.put(ProjectConstants.BUDGET_STATUS, StatusTypeCode.ACTIVE.name());
        thirdEntry.put(ProjectConstants.BUDGET_ALLOCATOR, null);
        thirdEntry.put(ProjectConstants.ALLOCATED_IN, null); // code coverage
        thirdEntry.put(ProjectConstants.ALLOCATED_OUT, null); // code coverage
        thirdEntry.put(ProjectConstants.IS_USABLE, "true");

        budgetList.add(thirdEntry);

        Map<String, Object> fourthEntry = new HashMap<>();

        fourthEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 4);
        fourthEntry.put(ProjectConstants.ID, 4L);
        fourthEntry.put(ProjectConstants.PARENT_BUDGET_ID, null);
        fourthEntry.put(ProjectConstants.BUDGET_NAME, "test");
        fourthEntry.put(ProjectConstants.BUDGET_DESC, "test");
        fourthEntry.put(ProjectConstants.BUDGET_TYPE_CODE, "test");
        fourthEntry.put(ProjectConstants.SUB_PROJECT_NUMBER, "test");
        fourthEntry.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 10D);
        fourthEntry.put(ProjectConstants.INDIVIDUAL_USED_AMOUNT, 0D);
        fourthEntry.put(ProjectConstants.BUDGET_CONSTRAINT, "test");
        fourthEntry.put(ProjectConstants.PROJECT_NUMBER, "test");
        fourthEntry.put(ProjectConstants.FROM_DATE, new Date());
        fourthEntry.put(ProjectConstants.THRU_DATE, null);
        fourthEntry.put(ProjectConstants.CREATE_DATE, new Date());
        fourthEntry.put(ProjectConstants.BUDGET_OWNER, null);
        fourthEntry.put(ProjectConstants.BUDGET_STATUS, StatusTypeCode.ACTIVE.name());
        fourthEntry.put(ProjectConstants.BUDGET_ALLOCATOR, null);
        fourthEntry.put(ProjectConstants.ALLOCATED_IN, null); // code coverage
        fourthEntry.put(ProjectConstants.ALLOCATED_OUT, "10"); // code coverage
        fourthEntry.put(ProjectConstants.IS_USABLE, "true");

        budgetList.add(fourthEntry);

        return budgetList;
    }

    private List<Map<String, Object>> getBudgetTotalsResults() {
        List<Map<String, Object>> budgetList = new ArrayList<>();

        Map<String, Object> firstEntry = new HashMap<>();

        firstEntry.put(ProjectConstants.BUDGET_ID, 1L);
        firstEntry.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        firstEntry.put(ProjectConstants.TOTAL_USED, 1D);

        budgetList.add(firstEntry);

        Map<String, Object> secondEntry = new HashMap<>();

        secondEntry.put(ProjectConstants.BUDGET_ID, 2L);
        secondEntry.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        secondEntry.put(ProjectConstants.TOTAL_USED, 1D);

        budgetList.add(secondEntry);

        Map<String, Object> thirdEntry = new HashMap<>();

        thirdEntry.put(ProjectConstants.BUDGET_ID, 5L); // code coverage
        thirdEntry.put(ProjectConstants.TOTAL_AMOUNT, 1D);
        thirdEntry.put(ProjectConstants.TOTAL_USED, 1D);

        budgetList.add(thirdEntry);

        return budgetList;
    }

    private List<Map<String, Object>> getBudgetUsedResults() {
        List<Map<String, Object>> budgetList = new ArrayList<>();

        Map<String, Object> firstEntry = new HashMap<>();

        firstEntry.put(ProjectConstants.BUDGET_ID, 1L);
        firstEntry.put(ProjectConstants.IS_USED, Boolean.TRUE.toString());

        budgetList.add(firstEntry);

        Map<String, Object> secondEntry = new HashMap<>();

        secondEntry.put(ProjectConstants.BUDGET_ID, 2L);
        secondEntry.put(ProjectConstants.IS_USED, Boolean.FALSE.toString());

        budgetList.add(secondEntry);

        Map<String, Object> thirdEntry = new HashMap<>();

        thirdEntry.put(ProjectConstants.BUDGET_ID, 5L); // code coverage
        thirdEntry.put(ProjectConstants.IS_USED, Boolean.FALSE.toString());

        budgetList.add(thirdEntry);

        return budgetList;
    }

    private List<Map<String, Object>> getBudgetUsersResults() {
        List<Map<String, Object>> budgetList = new ArrayList<>();

        Map<String, Object> firstEntry = new HashMap<>();

        firstEntry.put(ProjectConstants.BUDGET_ID, 1L);
        firstEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.PAX.name());
        firstEntry.put(ProjectConstants.BUDGET_USER_ID, 1L);

        budgetList.add(firstEntry);

        Map<String, Object> secondEntry = new HashMap<>();

        secondEntry.put(ProjectConstants.BUDGET_ID, 1L);
        secondEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.PAX.name());
        secondEntry.put(ProjectConstants.BUDGET_USER_ID, 2L);

        budgetList.add(secondEntry);

        Map<String, Object> thirdEntry = new HashMap<>();

        thirdEntry.put(ProjectConstants.BUDGET_ID, 2L);
        thirdEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.PAX.name());
        thirdEntry.put(ProjectConstants.BUDGET_USER_ID, 1L);

        budgetList.add(thirdEntry);

        Map<String, Object> fourthEntry = new HashMap<>();

        fourthEntry.put(ProjectConstants.BUDGET_ID, 2L);
        fourthEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.GROUPS.name());
        fourthEntry.put(ProjectConstants.BUDGET_USER_ID, 1L);

        budgetList.add(fourthEntry);

        Map<String, Object> fifthEntry = new HashMap<>(); // code coverage

        fifthEntry.put(ProjectConstants.BUDGET_ID, 2L);
        fifthEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.GROUPS.name());
        fifthEntry.put(ProjectConstants.BUDGET_USER_ID, 1L);

        budgetList.add(fifthEntry);

        Map<String, Object> sixthEntry = new HashMap<>(); // code coverage

        sixthEntry.put(ProjectConstants.BUDGET_ID, 5L);
        sixthEntry.put(ProjectConstants.BUDGET_USER_TYPE, TableName.GROUPS.name());
        sixthEntry.put(ProjectConstants.BUDGET_USER_ID, 1L);

        budgetList.add(sixthEntry);

        return budgetList;
    }

    private List<EntityDTO> getInfoResults() {
        List<EntityDTO> entityList = new ArrayList<>();

        entityList.add(new EmployeeDTO().setPaxId(1L));
        entityList.add(new EmployeeDTO().setPaxId(2L));
        entityList.add(new GroupDTO().setGroupId(1L));
        entityList.add(new GroupDTO().setGroupId(2L));

        return entityList;
    }

    @Test
    public void validate_budget_override_accounting_id_exceeds_max_length() {
        //overrideAccountingId is longer than 8 characters
        budgetDto.setOverrideProjectNumber("S43210");
        budgetDto.setOverrideAccountingId("0123456789");
        budgetDto.setOverrideLocation("US_OVERRIDE");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - invalid override accounting id");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_OVERRIDE_ACCOUNTING_ID_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_override_accounting_id_invalid() {
        //overrideAccountingId is not alphanumeric
        budgetDto.setOverrideProjectNumber("S43210");
        budgetDto.setOverrideAccountingId("!@#$%");
        budgetDto.setOverrideLocation(VfName.US_OVERRIDE.name());
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - override accounting id not alphanumeric");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.OVERRIDE_ACCOUNTING_ID_FORMAT_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_null_override_location() {
        //overrideLocation is null
        budgetDto.setOverrideProjectNumber("S43210");
        budgetDto.setOverrideAccountingId("123456");
        budgetDto.setOverrideLocation(null);
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - override location is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.NULL_OVERRIDE_LOCATION_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_override_location_invalid() {
        //overrideLocation is not valid
        budgetDto.setOverrideProjectNumber("S43210");
        budgetDto.setOverrideAccountingId("123456");
        budgetDto.setOverrideLocation("ABC");
        try {
            budgetService.updateBudgetById(TestConstants.ID_1, budgetDto);
            Assert.fail("Should have thrown error - override location not valid");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, BudgetConstants.INVALID_OVERRIDE_LOCATION_MESSAGE, errorMsgException);
        }
    }

    @Test
    public void validate_budget_status() {
        try {

            BudgetRequestDTO budget = setUpBudgetForTests();
            if (budget.getStatus() == null) {
            budget.setStatus(StatusTypeCode.ACTIVE.name());
            }
            BudgetDTO response = budgetService.updateBudgetById(TestConstants.ID_1, budget);

            Assert.assertEquals(StatusTypeCode.ACTIVE.name(), response.getStatus());

        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, "Budget Status can't be verified", errorMsgException);
        }
    }

    @Test
    public void validate_budget_tree_status_good() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO().setBudgetId(2L),
                new BudgetRequestDTO().setBudgetId(3L)
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L),
                new BudgetDTO().setBudgetId(3L)
            ));
        try {
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
        }
        catch (Exception e) {
            Assert.fail("Should not have thrown errors");
        }
    }

    @Test
    public void validate_budget_tree_status_adding_children() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO(),
                new BudgetRequestDTO()
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        try {
            // no existing child budgets
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
        }
        catch (Exception e) {
            Assert.fail("Should not have thrown errors");
        }
    }

    @Test
    public void validate_budget_tree_status_adding_child() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO().setBudgetId(2L),
                new BudgetRequestDTO()
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L)
            ));
        try {
            // adding a new child budget, but existing budget exists
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
        }
        catch (Exception e) {
            Assert.fail("Should not have thrown errors");
        }
    }

    @Test
    public void validate_budget_tree_no_child_budgets() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L),
                new BudgetDTO().setBudgetId(3L)
            ));
        try {
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
            Assert.fail("Should have thrown errors");
        }
        catch (ErrorMessageException e) {
            Assert.assertEquals("Should have one error", 1, e.getErrorMessages().size());
            Assert.assertEquals("should be INCOMPLETE_BUDGET_TREE", "INCOMPLETE_BUDGET_TREE", e.getErrorMessages().iterator().next().getCode());
        }
    }

    @Test
    public void validate_budget_tree_no_budget_match() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO().setBudgetId(4L),
                new BudgetRequestDTO().setBudgetId(5L)
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L),
                new BudgetDTO().setBudgetId(3L)
            ));
        try {
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
            Assert.fail("Should have thrown errors");
        }
        catch (ErrorMessageException e) {
            Assert.assertEquals("Should have one error", 1, e.getErrorMessages().size());
            Assert.assertEquals("should be INCOMPLETE_BUDGET_TREE", "INCOMPLETE_BUDGET_TREE", e.getErrorMessages().iterator().next().getCode());
        }
    }

    @Test
    public void validate_budget_tree_same_count_but_new() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO(),
                new BudgetRequestDTO()
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L),
                new BudgetDTO().setBudgetId(3L)
            ));
        try {
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
            Assert.fail("Should have thrown errors");
        }
        catch (ErrorMessageException e) {
            Assert.assertEquals("Should have one error", 1, e.getErrorMessages().size());
            Assert.assertEquals("should be INCOMPLETE_BUDGET_TREE", "INCOMPLETE_BUDGET_TREE", e.getErrorMessages().iterator().next().getCode());
        }
    }

    @Test
    public void validate_budget_tree_no_budget_missing_budget() {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(1L);
        requestBudget.setChildBudgets(Arrays.asList(
                new BudgetRequestDTO().setBudgetId(2L)
            ));

        BudgetDTO existingBudget = new BudgetDTO();
        existingBudget.setBudgetId(1L);
        existingBudget.setChildBudgets(Arrays.asList(
                new BudgetDTO().setBudgetId(2L),
                new BudgetDTO().setBudgetId(3L)
            ));
        try {
            budgetService.validateBudgetTreeStructureRecursion(requestBudget, existingBudget);
            Assert.fail("Should have thrown errors");
        }
        catch (ErrorMessageException e) {
            Assert.assertEquals("Should have one error", 1, e.getErrorMessages().size());
            Assert.assertEquals("should be INCOMPLETE_BUDGET_TREE", "INCOMPLETE_BUDGET_TREE", e.getErrorMessages().iterator().next().getCode());
        }
    }
}
