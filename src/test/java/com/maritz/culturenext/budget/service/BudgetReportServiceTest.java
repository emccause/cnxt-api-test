package com.maritz.culturenext.budget.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dao.BudgetReportDao;
import com.maritz.culturenext.budget.dto.BudgetUtilizationStatsDTO;
import com.maritz.culturenext.budget.dto.ChildBudgetUtilizationStatsDTO;
import com.maritz.culturenext.budget.service.impl.BudgetReportServiceImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheHandler;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.test.AbstractMockTest;

public class BudgetReportServiceTest extends AbstractMockTest {
    @InjectMocks private BudgetReportServiceImpl budgetReportService;

    @Mock private BudgetInfoDao budgetInfoDao;
    @Mock private BudgetReportDao budgetReportDao;
    @Mock private ReportsQueryCacheHandler reportsQueryCacheHandler;

    @Rule public ExpectedException thrown= ExpectedException.none();

    private final Double TEST_BUDGET_AVAILABLE = 10d;
    private final Double TEST_BUDGET_EXPIRED = 20d;
    private final Double TEST_BUDGET_USED_IN_PERIOD = 30d;
    private final Double TEST_BUDGET_USED_OUT_PERIOD = 40d;
    private final Double TEST_BUDGET_TOTAL_BUDGET = 50d;

    private final String TEST_BUDGET_ID_LIST_PARAM = "1,2,3";
    private final String TEST_FROM_DATE_PARAM = "2016-01-01";
    private final String TEST_THRU_DATE_PARAM = "2016-12-01";

    @Before
    public void setup() throws Exception {
        PowerMockito.when(budgetReportDao.getBudgetOwnerStats(anyLong()))
            .thenReturn(createBudgetStatsMock());

        Map<String, Object> eligibleBudgetMap1 = new HashMap<>();
        eligibleBudgetMap1.put(ProjectConstants.ID, TestConstants.ID_1);
        eligibleBudgetMap1.put(ProjectConstants.PAX_COUNT, 1);
        eligibleBudgetMap1.put(ProjectConstants.BUDGET_TYPE_CODE, BudgetTypeEnum.SIMPLE.name());
        eligibleBudgetMap1.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1000D);
        eligibleBudgetMap1.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        Map<String, Object> eligibleBudgetMap2 = new HashMap<>();
        eligibleBudgetMap2.put(ProjectConstants.ID, TestConstants.ID_2);
        eligibleBudgetMap2.put(ProjectConstants.PAX_COUNT, 1);
        eligibleBudgetMap2.put(ProjectConstants.BUDGET_TYPE_CODE, BudgetTypeEnum.ALLOCATED.name());
        eligibleBudgetMap2.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        Map<String, Object> eligibleBudgetMap3 = new HashMap<>();
        eligibleBudgetMap3.put(ProjectConstants.ID, TestConstants.ID_3);
        eligibleBudgetMap3.put(ProjectConstants.PAX_COUNT, 1);
        eligibleBudgetMap3.put(ProjectConstants.BUDGET_TYPE_CODE, BudgetTypeEnum.DISTRIBUTED.name());
        eligibleBudgetMap3.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 50D);
        eligibleBudgetMap3.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        Map<String, Object> eligibleBudgetMap4 = new HashMap<>();
        eligibleBudgetMap4.put(ProjectConstants.ID, 4L);
        eligibleBudgetMap4.put(ProjectConstants.PAX_COUNT, 3);
        eligibleBudgetMap4.put(ProjectConstants.BUDGET_TYPE_CODE, BudgetTypeEnum.DISTRIBUTED.name());
        eligibleBudgetMap4.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 100D);
        eligibleBudgetMap4.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        Map<String, Object> eligibleBudgetMap5 = new HashMap<>();
        eligibleBudgetMap5.put(ProjectConstants.ID, 5L);
        eligibleBudgetMap5.put(ProjectConstants.PAX_COUNT, 2);
        eligibleBudgetMap5.put(ProjectConstants.BUDGET_TYPE_CODE, BudgetTypeEnum.SIMPLE.name());
        eligibleBudgetMap5.put(ProjectConstants.INDIVIDUAL_GIVING_LIMIT, 1000D);
        eligibleBudgetMap5.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        List<Map<String, Object>> eligibleBudgetList = new ArrayList<>();
        eligibleBudgetList.add(eligibleBudgetMap1);
        eligibleBudgetList.add(eligibleBudgetMap2);
        eligibleBudgetList.add(eligibleBudgetMap3);
        eligibleBudgetList.add(eligibleBudgetMap4);
        eligibleBudgetList.add(eligibleBudgetMap5);
        PowerMockito.when(budgetInfoDao.getEligibleBudgetsForPax(any(String.class), any(Boolean.class), any(Boolean.class)))
            .thenReturn(eligibleBudgetList);

        Map<String, Object> budgetTotalsMap1 = new HashMap<>();
        budgetTotalsMap1.put(ProjectConstants.BUDGET_ID, TestConstants.ID_1);
        budgetTotalsMap1.put(ProjectConstants.TOTAL_USED, 50D);
        budgetTotalsMap1.put(ProjectConstants.TOTAL_AMOUNT, 5000D);
        budgetTotalsMap1.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        Map<String, Object> budgetTotalsMap2 = new HashMap<>();
        budgetTotalsMap2.put(ProjectConstants.BUDGET_ID, TestConstants.ID_2);
        budgetTotalsMap2.put(ProjectConstants.TOTAL_USED, 50D);
        budgetTotalsMap2.put(ProjectConstants.TOTAL_AMOUNT, 500D);
        budgetTotalsMap2.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        Map<String, Object> budgetTotalsMap3 = new HashMap<>();
        budgetTotalsMap3.put(ProjectConstants.BUDGET_ID, TestConstants.ID_3);
        budgetTotalsMap3.put(ProjectConstants.TOTAL_USED, 0D);
        budgetTotalsMap3.put(ProjectConstants.TOTAL_AMOUNT, 100D);
        budgetTotalsMap3.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        Map<String, Object> budgetTotalsMap4 = new HashMap<>();
        budgetTotalsMap4.put(ProjectConstants.BUDGET_ID, 4L);
        budgetTotalsMap4.put(ProjectConstants.TOTAL_USED, 50D);
        budgetTotalsMap4.put(ProjectConstants.TOTAL_AMOUNT, 100D);
        budgetTotalsMap4.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        Map<String, Object> budgetTotalsMap5 = new HashMap<>();
        budgetTotalsMap5.put(ProjectConstants.BUDGET_ID, 5L);
        budgetTotalsMap5.put(ProjectConstants.TOTAL_USED, 905D);
        budgetTotalsMap5.put(ProjectConstants.TOTAL_AMOUNT, 1000D);
        budgetTotalsMap5.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        List<Map<String, Object>> budgetTotalsFlatList = new ArrayList<>();
        budgetTotalsFlatList.add(budgetTotalsMap1);
        budgetTotalsFlatList.add(budgetTotalsMap2);
        budgetTotalsFlatList.add(budgetTotalsMap3);
        budgetTotalsFlatList.add(budgetTotalsMap4);
        budgetTotalsFlatList.add(budgetTotalsMap5);
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(anyListOf(Long.class))).thenReturn(budgetTotalsFlatList);

        Map<String, Object> budgetUsedMap1 = new HashMap<>();
        budgetUsedMap1.put(ProjectConstants.BUDGET_ID, TestConstants.ID_1);
        budgetUsedMap1.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        budgetUsedMap1.put(ProjectConstants.USED_IN_PERIOD, 50D);
        budgetUsedMap1.put(ProjectConstants.USED_OUT_OF_PERIOD, 0D);
        Map<String, Object> budgetUsedMap2 = new HashMap<>();
        budgetUsedMap2.put(ProjectConstants.BUDGET_ID, TestConstants.ID_2);
        budgetUsedMap2.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        budgetUsedMap2.put(ProjectConstants.USED_IN_PERIOD, 10D);
        budgetUsedMap2.put(ProjectConstants.USED_OUT_OF_PERIOD, 40D);
        Map<String, Object> budgetUsedMap4 = new HashMap<>();
        budgetUsedMap4.put(ProjectConstants.BUDGET_ID, 4L);
        budgetUsedMap4.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        budgetUsedMap4.put(ProjectConstants.USED_IN_PERIOD, 25D);
        budgetUsedMap4.put(ProjectConstants.USED_OUT_OF_PERIOD, 25D);
        Map<String, Object> budgetUsedMap5 = new HashMap<>();
        budgetUsedMap5.put(ProjectConstants.BUDGET_ID, 5L);
        budgetUsedMap5.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        budgetUsedMap5.put(ProjectConstants.USED_IN_PERIOD, 5D);
        budgetUsedMap5.put(ProjectConstants.USED_OUT_OF_PERIOD, 0D);
        List<Map<String, Object>> budgetUsedList = new ArrayList<>();
        budgetUsedList.add(budgetUsedMap1);
        budgetUsedList.add(budgetUsedMap2);
        budgetUsedList.add(budgetUsedMap4);
        budgetUsedList.add(budgetUsedMap5);
        PowerMockito.when(budgetInfoDao.getBudgetUsedByPax(any(String.class), any(Date.class), any(Date.class)))
            .thenReturn(budgetUsedList);

        //Get budgetIdList from cached queryId
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(TestConstants.ID_1);
        budgetIdList.add(TestConstants.ID_2);
        budgetIdList.add(TestConstants.ID_3);
        PowerMockito.when(reportsQueryCacheHandler.getBudgetIdList(anyString())).thenReturn(budgetIdList);

        Map<String, Object> budgetTotalsMap = new HashMap<>();
        budgetTotalsMap.put(ProjectConstants.BUDGET_ID, TestConstants.ID_2);
        budgetTotalsMap.put(ProjectConstants.TOTAL_USED, TEST_BUDGET_USED_IN_PERIOD);
        budgetTotalsMap.put(ProjectConstants.TOTAL_AMOUNT, TEST_BUDGET_TOTAL_BUDGET);
        budgetTotalsMap.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        List<Map<String, Object>> budgetTotalsList = new ArrayList<>();
        budgetTotalsList.add(budgetTotalsMap);
        PowerMockito.when(budgetInfoDao.getBudgetTotals(anyListOf(Long.class))).thenReturn(budgetTotalsList);
    }


    // ************ Admin Budget Dashboard ***********

    @Test
    public void test_getBudgetStatsByQueryId_success() throws Exception {

        ChildBudgetUtilizationStatsDTO response = budgetReportService.getBudgetStatsByQueryId(null, "queryIdTest");

        assertThat(response.getBudgetAvailable(), is(5495D));
        assertThat(response.getBudgetExpired(), is(150D));
        assertThat(response.getBudgetUsed(), is(1055D));
        assertThat(response.getTotalBudget(), is(6700D));
        assertThat(response.getBudgetAllocated(), is (0D));
        assertThat(response.getChildBudgetAvailable(), is(0D));
        assertThat(response.getChildBudgetExpired(), is(0D));
        assertThat(response.getChildBudgetUsed(), is(0D));
    }

    @Test
    public void test_getBudgetStatsByQueryId_budgetId_success() throws Exception {

        //Active parent allocated budget
        Map<String, Object> budgetTotalsActive = new HashMap<>();
        budgetTotalsActive.put(ProjectConstants.BUDGET_ID, TestConstants.ID_1);
        budgetTotalsActive.put(ProjectConstants.TOTAL_USED, 0D);
        budgetTotalsActive.put(ProjectConstants.TOTAL_AMOUNT, 1000D);
        budgetTotalsActive.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(Arrays.asList(TestConstants.ID_1))).thenReturn(Arrays.asList(budgetTotalsActive));
        //Ended parent allocated budget
        Map<String, Object> budgetTotalsEnded = new HashMap<>();
        budgetTotalsEnded.put(ProjectConstants.BUDGET_ID, TestConstants.ID_2);
        budgetTotalsEnded.put(ProjectConstants.TOTAL_USED, 0D);
        budgetTotalsEnded.put(ProjectConstants.TOTAL_AMOUNT, 0D);
        budgetTotalsEnded.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(Arrays.asList(TestConstants.ID_2))).thenReturn(Arrays.asList(budgetTotalsEnded));
        //Simple budget
        Map<String, Object> simpleBudgetTotals = new HashMap<>();
        simpleBudgetTotals.put(ProjectConstants.BUDGET_ID, TestConstants.ID_3);
        simpleBudgetTotals.put(ProjectConstants.TOTAL_USED, 50D);
        simpleBudgetTotals.put(ProjectConstants.TOTAL_AMOUNT, 100D);
        simpleBudgetTotals.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(Arrays.asList(TestConstants.ID_3))).thenReturn(Arrays.asList(simpleBudgetTotals));
        //Child budget info - active and ended
        Map<String, Object> childBudgetActive = new HashMap<>();
        childBudgetActive.put(ProjectConstants.PARENT_BUDGET_ID, TestConstants.ID_1);
        childBudgetActive.put(ProjectConstants.TOTAL_USED, 2500D);
        childBudgetActive.put(ProjectConstants.TOTAL_AMOUNT, 4000D);
        PowerMockito.when(budgetInfoDao.getChildBudgetTotals(Arrays.asList(TestConstants.ID_1))).thenReturn(Arrays.asList(childBudgetActive));
        Map<String, Object> childBudgetEnded = new HashMap<>();
        childBudgetEnded.put(ProjectConstants.PARENT_BUDGET_ID, TestConstants.ID_2);
        childBudgetEnded.put(ProjectConstants.TOTAL_USED, 0D);
        childBudgetEnded.put(ProjectConstants.TOTAL_AMOUNT, 1000D);
        PowerMockito.when(budgetInfoDao.getChildBudgetTotals(Arrays.asList(TestConstants.ID_2))).thenReturn(Arrays.asList(childBudgetEnded));

        //Active test (allocated budget)
        ChildBudgetUtilizationStatsDTO response = budgetReportService.getBudgetStatsByQueryId(TestConstants.ID_1, "queryIdTest");
        assertThat(response.getBudgetAvailable(), is(1000D));
        assertThat(response.getBudgetExpired(), is(0D));
        assertThat(response.getBudgetUsed(), is(0D));
        assertThat(response.getTotalBudget(), is(1000D));
        assertThat(response.getBudgetAllocated(), is(4000D));
        assertThat(response.getChildBudgetAvailable(), is(1500D));
        assertThat(response.getChildBudgetExpired(), is(0D));
        assertThat(response.getChildBudgetUsed(), is(2500D));

        //Ended test (allocated budget)
        response = budgetReportService.getBudgetStatsByQueryId(TestConstants.ID_2, "queryIdTest");
        assertThat(response.getBudgetAvailable(), is(0D));
        assertThat(response.getBudgetExpired(), is(0D));
        assertThat(response.getBudgetUsed(), is(0D));
        assertThat(response.getTotalBudget(), is(0D));
        assertThat(response.getBudgetAllocated(), is(1000D));
        assertThat(response.getChildBudgetAvailable(), is(0D));
        assertThat(response.getChildBudgetExpired(), is(1000D));
        assertThat(response.getChildBudgetUsed(), is(0D));

        //Simple budget test
        response = budgetReportService.getBudgetStatsByQueryId(TestConstants.ID_3, "queryIdTest");
        assertThat(response.getBudgetAvailable(), is(50D));
        assertThat(response.getBudgetExpired(), is(0D));
        assertThat(response.getBudgetUsed(), is(50D));
        assertThat(response.getTotalBudget(), is(100D));
        assertThat(response.getBudgetAllocated(), is(0D));
        assertThat(response.getChildBudgetAvailable(), is(0D));
        assertThat(response.getChildBudgetExpired(), is(0D));
        assertThat(response.getChildBudgetUsed(), is(0D));
    }

    @Test
    public void test_getBudgetStatsByQueryId_empty_budgetTotals() throws Exception {

        List<Map<String, Object>> emptyBudgetTotals = new ArrayList<>();
        PowerMockito.when(budgetInfoDao.getBudgetTotalsFlat(anyListOf(Long.class))).thenReturn(emptyBudgetTotals);

        ChildBudgetUtilizationStatsDTO response = budgetReportService.getBudgetStatsByQueryId(null, "queryIdTest");

        assertThat(response.getBudgetAvailable(), is(0D));
        assertThat(response.getBudgetExpired(), is(0D));
        assertThat(response.getBudgetUsed(), is(0D));
        assertThat(response.getTotalBudget(), is(0D));
        assertThat(response.getBudgetAllocated(), is (0D));
        assertThat(response.getChildBudgetAvailable(), is(0D));
        assertThat(response.getChildBudgetExpired(), is(0D));
        assertThat(response.getChildBudgetUsed(), is(0D));
    }

    @Test
    public void test_getBudgetStatsByQueryId_null_queryId() throws Exception {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_MISSING_QUERY_ID_MSG));

        String queryId = null;
        budgetReportService.getBudgetStatsByQueryId(null, queryId);

    }

    @Test
    public void test_getBudgetStatsByQueryId_empty_queryId() throws Exception {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_MISSING_QUERY_ID_MSG));

        String queryId = "";
        budgetReportService.getBudgetStatsByQueryId(null, queryId);

    }

    @Test
    public void test_getBudgetStatsByQueryId_invalid_queryId() throws Exception {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(ReportConstants.ERROR_QUERYID_INVALID_MSG));

        String queryId = "invalidQueryId";

        List<Long> budgetIdList = new ArrayList<>();
        PowerMockito.when(reportsQueryCacheHandler.getBudgetIdList(queryId)).thenReturn(budgetIdList);

        budgetReportService.getBudgetStatsByQueryId(null, queryId);
    }


    // ************  Manager Budget Dashboard *************


    @Test
    public void test_getManagerBudgetStats_success() {
        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "2016-01-01";
        String thruDate = "2016-12-01";

        BudgetUtilizationStatsDTO response = budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);

        assertThat(response.getBudgetAvailable(), is(1495D));
        assertThat(response.getBudgetExpired(), is(100D));
        assertThat(response.getBudgetUsed(), is(155D));
        assertThat(response.getTotalBudget(), is(1750D));
    }

    @Test
    public void test_getManagerBudgetStats_success_empty() {
        //Mock stats result - empty
        PowerMockito.when(budgetInfoDao.getEligibleBudgetsForPax(any(String.class), any(Boolean.class), any(Boolean.class)))
            .thenReturn(new ArrayList<Map<String, Object>>());
        PowerMockito.when(budgetInfoDao.getBudgetUsedByPax(any(String.class), any(Date.class), any(Date.class)))
            .thenReturn(new ArrayList<Map<String, Object>>());

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "2016-01-01";
        String thruDate = "2016-12-01";

        BudgetUtilizationStatsDTO response = budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);

        assertThat(response.getBudgetAvailable(), is(0d));
        assertThat(response.getBudgetExpired(), is(0d));
        assertThat(response.getBudgetUsed(), is(0d));
        assertThat(response.getTotalBudget(), is(0d));
    }

    @Test
    public void test_getManagerBudgetStats_fail_missing_paxIds_null() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(ReportConstants.ERROR_MISSING_PAX_IDS_MSG));

        // Input
        String paxIdsList = null;
        String fromDate = "2016-01-01";
        String thruDate = "2016-12-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_missing_paxIds_empty() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(ReportConstants.ERROR_MISSING_PAX_IDS_MSG));

        // Input
        String paxIdsList = "";
        String fromDate = "2016-01-01";
        String thruDate = "2016-12-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_invalid_paxIds_non_digits() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(ReportConstants.PARAM_ONLY_SUPPORTS_DIGITS_ERROR_MESSAGE));

        // Input
        String paxIdsList = "INVALID,PAX,LIST";
        String fromDate = "2016-01-01";
        String thruDate = "2016-12-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_missing_fromDate() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(DateConstants.ERROR_FROM_DATE_INVALID_MSG));

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = null;
        String thruDate = "2016-12-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_empty_fromDate() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(DateConstants.ERROR_FROM_DATE_INVALID_MSG));

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "";
        String thruDate = "2016-12-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_missing_thruDate() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(DateConstants.ERROR_THRU_DATE_INVALID_MSG));

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "2016-01-01";
        String thruDate = null;

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_empty_thruDate() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(DateConstants.ERROR_THRU_DATE_INVALID_MSG));

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "2016-01-01";
        String thruDate = "";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    @Test
    public void test_getManagerBudgetStats_fail_fromDate_after_thruDate() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(DateConstants.ERROR_FROM_DATE_AFTER_THRU_DATE_MSG));

        // Input
        String paxIdsList = "141,843,8571";
        String fromDate = "2016-12-01";
        String thruDate = "2016-01-01";

        budgetReportService.getManagerBudgetDashboard(paxIdsList, fromDate, thruDate);
    }

    private List<Map<String, Object>> createBudgetStatsMock() {
        List<Map<String, Object>> resultList = new ArrayList<>();
        Map<String, Object> row1 = new HashMap<>();
        row1.put(ProjectConstants.BUDGET_AVAILABLE, TEST_BUDGET_AVAILABLE);
        row1.put(ProjectConstants.BUDGET_USED, TEST_BUDGET_USED_IN_PERIOD);
        row1.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        Map<String, Object> row2 = new HashMap<>();
        row2.put(ProjectConstants.BUDGET_AVAILABLE, TEST_BUDGET_EXPIRED);
        row2.put(ProjectConstants.BUDGET_USED, TEST_BUDGET_USED_OUT_PERIOD);
        row2.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ENDED.name());

        resultList.add(row1);
        resultList.add(row2);

        return resultList;
    }


    // ************  Budget Owner Dashboard *************

    @Test
    public void test_getBudgetOwnerStats_fail_fromDate_null() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_MISSING_FROM_DATE_MSG));

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, TEST_BUDGET_ID_LIST_PARAM, null, TEST_THRU_DATE_PARAM);
    }


    @Test
    public void test_getBudgetOwnerStats_fail_fromDate_invalid() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_INVALID_FROM_DATE_MSG));

        String invalidFromDate = "invalidDate";

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, TEST_BUDGET_ID_LIST_PARAM, invalidFromDate, TEST_THRU_DATE_PARAM);
    }


    @Test
    public void test_getBudgetOwnerStats_fail_thruDate_null() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_MISSING_THRU_DATE_MSG));

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, TEST_BUDGET_ID_LIST_PARAM, TEST_FROM_DATE_PARAM, null);
    }

    @Test
    public void test_getBudgetOwnerStats_fail_thruDate_invalid() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_INVALID_THRU_DATE_MSG));

        String invalidThruDate = "invalidDate";

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, TEST_BUDGET_ID_LIST_PARAM, TEST_FROM_DATE_PARAM, invalidThruDate);
    }


    @Test
    public void test_getBudgetOwnerStats_fail_budgetIdList_non_digits() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_BUDGET_ID_NON_DIGIT_MSG));

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, "NON,DIGITS", TEST_FROM_DATE_PARAM, TEST_THRU_DATE_PARAM);
    }

    @Test
    public void test_getBudgetOwnerStats_fail_invalid_date_range() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetConstants.ERROR_INVALID_DATE_RANGE_MSG));

        String invalidFromDate = TEST_THRU_DATE_PARAM;
        String invalidThruDate = TEST_FROM_DATE_PARAM;

        budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, TEST_BUDGET_ID_LIST_PARAM, invalidFromDate, invalidThruDate);
    }

    @Test
    public void test_getBudgetOwnerStats_success() {

        BudgetUtilizationStatsDTO response =
                budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, "2", TEST_FROM_DATE_PARAM, TEST_THRU_DATE_PARAM);

        assertThat(response.getBudgetAvailable(), is(20D));
        assertThat(response.getBudgetExpired(), is(0D));
        assertThat(response.getBudgetUsed(), is(30D));

        // Budget Id list null
        response = budgetReportService.getBudgetOwnerDashboard(TestConstants.ID_1, null, TEST_FROM_DATE_PARAM, TEST_THRU_DATE_PARAM);

        assertThat(response.getBudgetAvailable(), is(TEST_BUDGET_AVAILABLE));
        assertThat(response.getBudgetExpired(), is(TEST_BUDGET_EXPIRED));
        assertThat(response.getBudgetUsed(), is(TEST_BUDGET_USED_IN_PERIOD + TEST_BUDGET_USED_OUT_PERIOD));
    }

}
