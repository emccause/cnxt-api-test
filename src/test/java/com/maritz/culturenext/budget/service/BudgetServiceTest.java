package com.maritz.culturenext.budget.service;

import static com.maritz.culturenext.budget.rest.BudgetRestService.BUDGET_SEARCH_PATH;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import com.maritz.culturenext.constants.ProjectConstants;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.test.AbstractDatabaseTest;

public class BudgetServiceTest extends AbstractDatabaseTest {

    @Inject private BudgetService budgetService;

    private static final String TEST_NAME = "Demo Test Budget";
    private static final String BUDGET_ALLOCATOR_PAX_ID = "8571";
    private static final String BUDGET_OWNER_PAX_ID = "10721";
    
    @Test
    public void search_no_params_good() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);


        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, null, null, null, null, null, null, 1, 50, null);

        assertNotNull("Should have data.", response.getData());
        assertFalse("Should have data.", response.getData().isEmpty());
    }

    @Test
    public void search_no_duplicates_good() {

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(null, null, null, null, null, null, null, null, 1, ProjectConstants.MAX_PAGE_SIZE, null);

        List<BudgetDTO> data = response.getData();
        List<String> dataNoDuplicates = new ArrayList<>();

        for(BudgetDTO entry : data) {
            assertFalse(dataNoDuplicates.contains(entry.getName()));
            dataNoDuplicates.add(entry.getName());
        }
    }

    @Test
    public void search_name_good() {

        String testName = "Simple Budget Test";

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, testName, null, null, null, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());

        for (BudgetDTO entry : data) {
            assertTrue("Results should contain test name", entry.getDisplayName().contains(testName));
        }
    }

    @Test
    public void search_status_good() {
        String testStatus = "SCHEDULED,ACTIVE,ENDED,ARCHIVED";

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, testStatus, null, null, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());

        List<String> statusList = Arrays.asList(testStatus.split(","));

        for (BudgetDTO entry : data) {
            assertTrue("Results should contain test status", statusList.contains(entry.getStatus()));
        }
    }

    @Test
    public void search_name_no_data() {
        String testName = "Why would you give a budget this name? honestly...";

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, testName, null, null, null, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data field, but should be empty.", data);
        assertTrue("Should not have data.", data.isEmpty());
    }

    @Test
    public void search_status_no_data() {
        String teststatus = "INVALID_STATUS";

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, teststatus, null, null, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data field, but should be empty.", data);
        assertTrue("Should not have data.", data.isEmpty());
    }

    @Test
    public void search_has_giving_limit_is_false() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, TEST_NAME, StatusTypeCode.ACTIVE.name(), null, Boolean.FALSE, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();

        assertNotNull("Should have data field, and shouldn't be empty", data);
        assertFalse("Should have data.", data.isEmpty());
    }

    @Test
    public void search_has_giving_limit_is_true() {
        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, TEST_NAME, StatusTypeCode.ACTIVE.name(), null,Boolean.TRUE, null, null, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();

        assertNotNull("Should have data field, but should be empty.", data);
        assertTrue("Should not have data.", data.isEmpty());
    }

    @Test
    public void search_has_budget_owner() {
    PaginationRequestDetails requestDetails = new PaginationRequestDetails()
        .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
        budgetService.getBudgetList(requestDetails, null, StatusTypeCode.ACTIVE.name(), Boolean.FALSE, null, null, BUDGET_OWNER_PAX_ID, null, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());

    }

    @Test
    public void search_has_allocator_not_include_children() {
        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
        .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, StatusTypeCode.ACTIVE.name(), Boolean.FALSE, null, null, null, BUDGET_ALLOCATOR_PAX_ID, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());

    }

    @Test
    public void search_has_allocator_include_children() {
        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, StatusTypeCode.ACTIVE.name(), Boolean.TRUE, null, null, null, BUDGET_ALLOCATOR_PAX_ID, 1, 50, null);

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());

    }

    @Test
    public void search_has_allocated_budget_type() {
        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(BUDGET_SEARCH_PATH)
                        .setPageNumber(1).setPageSize(50);

        PaginatedResponseObject<BudgetDTO> response =
                budgetService.getBudgetList(requestDetails, null, StatusTypeCode.ACTIVE.name(), Boolean.TRUE, null, null, null, BUDGET_ALLOCATOR_PAX_ID, 1, 50, BudgetTypeEnum.ALLOCATED.name());

        List<BudgetDTO> data = response.getData();
        assertNotNull("Should have data.", data);
        assertFalse("Should have data.", data.isEmpty());
    }
}
