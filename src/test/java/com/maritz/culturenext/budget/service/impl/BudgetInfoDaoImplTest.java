package com.maritz.culturenext.budget.service.impl;

import com.maritz.culturenext.budget.dao.impl.BudgetInfoDaoImpl;
import com.maritz.test.AbstractDatabaseTest;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.Test;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class BudgetInfoDaoImplTest extends AbstractDatabaseTest {
    @Inject
    private BudgetInfoDaoImpl budgetInfoDao;

    @Test
    public void testGetBudgetUsed(){
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(1L);
        budgetIdList.add(39L);
        budgetIdList.add(5082L);
        List<Map<String, Object>> result = budgetInfoDao.getBudgetUsed(budgetIdList);
        assertNotNull(result);
        assertNotEquals(0, result.size());
        assertEquals(3, result.size());
    }

    @Test
    public void testGetBudgetUsedWithDuplicatedIds(){
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(1L);
        budgetIdList.add(39L);
        budgetIdList.add(5082L);
        budgetIdList.add(1L);
        List<Map<String, Object>> result = budgetInfoDao.getBudgetUsed(budgetIdList);
        assertNotNull(result);
        assertNotEquals(0, result.size());
        assertEquals(3, result.size());
    }

    @Test
    public void testGetBudgetUsers() {
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(86L);
        budgetIdList.add(90L);
        budgetIdList.add(89L);
        List<Map<String, Object>> result = budgetInfoDao.getBudgetUsers(budgetIdList);
        assertNotNull(result);
        assertNotEquals(0, result.size());
        assertEquals(3, result.size());
    }

    @Test
    public void testGetBudgetTotals(){
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(86L);
        budgetIdList.add(90L);
        budgetIdList.add(89L);
        List<Map<String, Object>> result = budgetInfoDao.getBudgetTotals(budgetIdList);
        assertNotNull(result);
        assertNotEquals(0, result.size());
        assertEquals(3, result.size());
    }

    @Test
    public void testGetBudgetTotalsFlatWithNullBudgetList(){
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = null;
        List<Map<String, Object>> result = budgetInfoDao.getBudgetTotalsFlat(budgetIdList);
        assertNotNull(result);
        assertEquals(2106, result.size());
    }

    @Test
    public void testGetBudgetTotalsFlatWithBudgetList(){
        assertNotNull(budgetInfoDao);
        List<Long> budgetIdList = new ArrayList<>();
        budgetIdList.add(39L);
        budgetIdList.add(76L);
        budgetIdList.add(78L);
        List<Map<String, Object>> result = budgetInfoDao.getBudgetTotalsFlat(budgetIdList);
        assertNotNull(result);
        assertEquals(3, result.size());
    }
}
