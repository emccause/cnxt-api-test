package com.maritz.culturenext.budget.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.constants.RestParameterConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;
import com.maritz.test.AbstractRestTest;

public class BudgetReportStatsRestTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private ReportsQueryService reportsQueryService;

    long noBudgetsPaxId = 10721L;
    long nullPaxId;
    
    private static final String REST_URL_BUDGET_OWNER_STATS = "/rest/budgets/budget-owner-dashboard";
    private static final String REST_URL_MANAGER_DASHBOARD_STATS = "/rest/budgets/manager-dashboard";
    
    // ***************************** ADMIN BUDGET REPORT TESTS *********************************

    @Test
    public void get_budget_report_stats() throws Exception {

        //Run this to cache the query values
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();
        reportsQueryRequestDto.setDirectReports(Arrays.asList(847L));
        reportsQueryRequestDto.setFromDate("2015-01-01");
        reportsQueryRequestDto.setThruDate("2015-07-01");
        reportsQueryRequestDto.setOrgReports(Arrays.asList(12671L,553L,12851L));

        //Setting members
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(8571L);
        reportsQueryRequestDto.setMembers(Arrays.asList(groupMemberStubDTO));

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);        

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats?queryId=" + queryIdDto.getQueryId())                
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }

    @Test
    public void get_budget_report_stats_invalid_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats?queryId=INVALID_QUERY_ID")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_INVALID)));
    }

    @Test
    public void get_budget_report_stats_null_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats?queryId=")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED)));
    }
    
    
    
    // ***************************** MANAGER BUDGET REPORT TESTS *********************************

    @Test
    public void get_manager_budget_report_stats_all() throws Exception {

        //Run this to cache the query values
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();
        reportsQueryRequestDto.setDirectReports(Arrays.asList(847L));
        reportsQueryRequestDto.setFromDate("2015-01-01");
        reportsQueryRequestDto.setThruDate("2015-07-01");
        GroupMemberStubDTO member = new GroupMemberStubDTO();
        member.setPaxId(847L);
        reportsQueryRequestDto.setMembers(Arrays.asList(member));

        //Setting members
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(8571L);
        reportsQueryRequestDto.setMembers(Arrays.asList(groupMemberStubDTO));

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);        

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit?queryId=" + queryIdDto.getQueryId())                
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax() throws Exception {

        //Run this to cache the query values
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();
        reportsQueryRequestDto.setDirectReports(Arrays.asList(847L));
        reportsQueryRequestDto.setFromDate("2015-01-01");
        reportsQueryRequestDto.setThruDate("2015-07-01");
        GroupMemberStubDTO member = new GroupMemberStubDTO();
        member.setPaxId(847L);
        reportsQueryRequestDto.setMembers(Arrays.asList(member));

        //Setting members
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(8571L);
        reportsQueryRequestDto.setMembers(Arrays.asList(groupMemberStubDTO));

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);        

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + TestConstants.PAX_HARWELLM + "?queryId=" + queryIdDto.getQueryId())                
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }

    @Test
    public void get_manager_budget_report_stats_all_invalid_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit?queryId=INVALID_QUERY_ID")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_INVALID)));
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax_invalid_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + TestConstants.PAX_HARWELLM + "?queryId=INVALID_QUERY_ID")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_INVALID)));
    }

    @Test
    public void get_manager_budget_report_stats_all_null_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit?queryId=")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED)));
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax_null_query_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + TestConstants.PAX_HARWELLM + "?queryId=")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED)));
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax_null_pax_id() throws Exception {

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + nullPaxId + "?queryId=")        
                .with(authToken(TestConstants.USER_HARWELLM))                
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(ReportConstants.ERROR_QUERYID_NOT_SUPPLIED)));
    }
    
    
    @Test
    public void get_manager_budget_report_stats_for_pax_non_report_pax() throws Exception {

        //Run this to cache the query values
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();
        reportsQueryRequestDto.setDirectReports(Arrays.asList(847L));
        reportsQueryRequestDto.setFromDate("2015-01-01");
        reportsQueryRequestDto.setThruDate("2015-07-01");
        GroupMemberStubDTO member = new GroupMemberStubDTO();
        member.setPaxId(847L);
        reportsQueryRequestDto.setMembers(Arrays.asList(member));

        //Setting members
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(8571L);
        reportsQueryRequestDto.setMembers(Arrays.asList(groupMemberStubDTO));

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);        

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + TestConstants.PAX_KUMARSJ + "?queryId=" + queryIdDto.getQueryId())                
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_NO_BUDGETS_FOR_PAX)));
    }
    
    @Test
    public void get_manager_budget_report_stats_for_pax_no_budgets_pax() throws Exception {

        //Run this to cache the query values
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();
        reportsQueryRequestDto.setDirectReports(Arrays.asList(847L));
        reportsQueryRequestDto.setFromDate("2015-01-01");
        reportsQueryRequestDto.setThruDate("2015-07-01");
        GroupMemberStubDTO member = new GroupMemberStubDTO();
        member.setPaxId(847L);
        reportsQueryRequestDto.setMembers(Arrays.asList(member));

        //Setting members
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(8571L);
        reportsQueryRequestDto.setMembers(Arrays.asList(groupMemberStubDTO));

        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);        

        //This is getting the stats based on the query
        mockMvc.perform(
                get("/rest/budgets/stats-by-giving-limit/" + noBudgetsPaxId + "?queryId=" + queryIdDto.getQueryId())                
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_NO_BUDGETS_FOR_PAX)));
    }

    @Test
    public void get_getBudgetOwnerStats_success() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HARWELLM);

        //This is getting the stats based on the query
        mockMvc.perform(
                get(REST_URL_BUDGET_OWNER_STATS)
                .param(RestParameterConstants.FROM_DATE_REST_PARAM, "2016-01-01")
                .param(RestParameterConstants.THRU_DATE_REST_PARAM, "2016-12-01")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void get_getManagerDashboard_success() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HARWELLM);

        mockMvc.perform(
                get(REST_URL_MANAGER_DASHBOARD_STATS)
                .param(RestParameterConstants.PAX_IDS_REST_PARAM, "8571")
                .param(RestParameterConstants.FROM_DATE_REST_PARAM, "2016-01-01")
                .param(RestParameterConstants.THRU_DATE_REST_PARAM, "2016-12-01")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk());
    }
}
