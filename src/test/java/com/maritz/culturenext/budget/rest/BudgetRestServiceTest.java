package com.maritz.culturenext.budget.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_NUMBER_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAGE_SIZE_REST_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.GIVING_LIMIT_ONLY_REST_PARAM;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetDetailsDTO;
import com.maritz.culturenext.budget.dto.BudgetDetailsRequestDTO;
import com.maritz.culturenext.budget.dto.BudgetRequestDTO;
import com.maritz.culturenext.budget.service.BudgetService;
import com.maritz.culturenext.caching.service.BudgetEligibilityCacheService;
import com.maritz.culturenext.enums.BudgetConstraintEnum;
import com.maritz.culturenext.enums.BudgetTypeEnum;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtBudgetRepository;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class BudgetRestServiceTest extends AbstractRestTest {

    private static final String URL_VALID_SEARCH = "/rest/budgets/search";
    private static final String URL_VALID_LIST = "/rest/budgets";
    private static final String URL_BUDGET_BY_ID = "/rest/budgets/%d";
    private static final String URL_BUDGET_ELIGIBLE = "/rest/participants/%s/budgets";

    private static final String SIMPLE_BUDGET_NAME = "junitTest";
    private static final String DISTRIBUTED_BUDGET_NAME = "Test Distributed Budget";
    private static final String ALLOCATED_BUDGET_NAME = "Top Level Budget";
    private static final String FROM_DATE = "2016-11-24";
    private static final String PROJECT_NUMBER = "S01234";
    private static final String OVERRIDE_PROJECT_NUMBER = "S43210";
    private static final String OVERRIDE_ACCOUNTING_ID = "123456";
    
    private static final Long MO_GROUP_ID = 3124L;
    private static final Long IL_GROUP_ID = 3126L;
    
    @Inject private BudgetService budgetService;
    @Inject private BudgetEligibilityCacheService budgetEligibilityCacheService;
    @Inject private CnxtBudgetRepository cnxtBudgetRepository;
    
    // *********************************** Budget List ***************************************
    
    @Test
    public void test_getBudgetList_valid_search() throws Exception {
        MvcResult result = mockMvc.perform(
                get(URL_VALID_LIST)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void test_getBudgetList_set_page_and_size() throws Exception {
        MvcResult result = mockMvc.perform(
                get(URL_VALID_LIST)
                .param(PAGE_NUMBER_REST_PARAM, "1")
                .param(PAGE_SIZE_REST_PARAM, "10")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
        
        result = mockMvc.perform(
                get(URL_VALID_LIST)
                .param(PAGE_NUMBER_REST_PARAM, "0")
                .param(PAGE_SIZE_REST_PARAM, "0")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
        
        result = mockMvc.perform(
                get(URL_VALID_LIST)
                .param(PAGE_NUMBER_REST_PARAM, "2")
                .param(PAGE_SIZE_REST_PARAM, "0")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    /**
     *  Helper method for update budget.
     *  The get budget by ID will return a BudgetDTO but the update requires a BudgetRequestDTO
     *  This method will take a BudgetDTO and map all fields to a BudgetRequestDTO
     *  @param BudgetDTO - Response from get budget by ID
     *  @param BudgetRequestDTO - The DTO to be used to update the budget
     *  @return BudgetRequestDTO
     */
    private BudgetRequestDTO mapBudgetForRequest(BudgetDTO budget) {
        BudgetRequestDTO requestBudget = new BudgetRequestDTO();
        requestBudget.setBudgetId(budget.getBudgetId());
        requestBudget.setParentBudgetId(budget.getParentBudgetId());
        requestBudget.setName(budget.getName());
        requestBudget.setDisplayName(budget.getDisplayName());
        requestBudget.setDescription(budget.getDescription());
        requestBudget.setTotalAmount(budget.getTotalAmount());
        requestBudget.setType(budget.getType());
        requestBudget.setAccountingId(budget.getAccountingId());
        requestBudget.setIndividualGivingLimit(budget.getIndividualGivingLimit());
        requestBudget.setFromDate(budget.getFromDate());
        requestBudget.setThruDate(budget.getThruDate());
        requestBudget.setBudgetConstraint(budget.getBudgetConstraint());
        requestBudget.setProjectNumber(budget.getProjectNumber());
        requestBudget.setIsUsable(budget.getIsUsable());
        requestBudget.setBudgetOwner(budget.getBudgetOwner());
        
        BudgetDetailsRequestDTO requestDetails = new BudgetDetailsRequestDTO();
        BudgetDetailsDTO budgetDetails = budget.getBudgetDetails();
        if (budgetDetails == null) {
            budgetDetails = new BudgetDetailsDTO();
        }
        requestDetails.setAllocatedIn(budgetDetails.getAllocatedIn());
        requestDetails.setAllocatedOut(budgetDetails.getAllocatedOut());
        requestDetails.setCanAllocate(budgetDetails.getCanAllocate());
        
        List<EntityDTO> budgetUsers = budgetDetails.getBudgetUsers();
        if (budgetUsers != null && !budgetUsers.isEmpty()) {
            GroupMemberStubDTO requestBudgetUser = new GroupMemberStubDTO();
            EntityDTO entity = budgetUsers.get(0);
            if (entity instanceof EmployeeDTO) {
                requestBudgetUser.setPaxId(((EmployeeDTO) entity).getPaxId());
            } else if (entity instanceof GroupDTO) {
                requestBudgetUser.setGroupId(((GroupDTO) entity).getGroupId());
            }
            requestDetails.setBudgetUsers(Arrays.asList(requestBudgetUser));
        }
        requestBudget.setBudgetDetails(requestDetails);
        
        List<BudgetRequestDTO> childBudgetList = new ArrayList<>();
        if (budget.getChildBudgets() != null && !budget.getChildBudgets().isEmpty()) {
            for (BudgetDTO childBudget : budget.getChildBudgets()) {
                BudgetRequestDTO childRequestBudget = mapBudgetForRequest(childBudget);
                childBudgetList.add(childRequestBudget);
            }
        }
        requestBudget.setChildBudgets(childBudgetList);
        
        return requestBudget;
    }

    // *********************************** Create/Update Simple Budget ***************************************
    
    private BudgetRequestDTO createSimpleBudget() {
        BudgetRequestDTO budget = new BudgetRequestDTO();
        budget.setName(SIMPLE_BUDGET_NAME);
        budget.setType(BudgetTypeEnum.SIMPLE.name());
        budget.setFromDate(FROM_DATE);
        budget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        budget.setProjectNumber(PROJECT_NUMBER);
        budget.setTotalAmount(100D);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setAllocatedIn(100D);
        budget.setBudgetDetails(budgetDetails);
        budget.setIsUsable(Boolean.TRUE);
        budget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        budget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        budget.setOverrideLocation(VfName.US_OVERRIDE.name());
        return budget;
    }

    @Test
    public void test_create_simple_budget_good() throws Exception {

        mockMvc.perform(
                post("/rest/budgets")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(createSimpleBudget()))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void test_update_simple_budget_good() throws Exception {
        BudgetRequestDTO budget = mapBudgetForRequest(budgetService.getBudgetById(755L));        
        
        //Change totalAmount and BudgetOwner
        budget.setBudgetOwner(new EmployeeDTO().setPaxId(8571L));
        budget.setTotalAmount(200D);
        BudgetDetailsRequestDTO budgetDetails = budget.getBudgetDetails();
        if (budgetDetails == null) {
            budgetDetails = new BudgetDetailsRequestDTO();
        }
        budgetDetails.setAllocatedIn(200D);
        budget.setBudgetDetails(budgetDetails);
        budget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        budget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        budget.setOverrideLocation(VfName.NON_US_OVERRIDE.name());
        if (budget.getStatus() == null) {
            budget.setStatus(StatusTypeCode.ACTIVE.name());    
        }
        
        mockMvc.perform(
                put("/rest/budgets/755")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(budget))
                )
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void test_update_simple_budget_ended_good() throws Exception {
        BudgetRequestDTO budget = mapBudgetForRequest(budgetService.getBudgetById(755L));        
        
        //Change totalAmount and BudgetOwner
        budget.setBudgetOwner(new EmployeeDTO().setPaxId(8571L));
        budget.setTotalAmount(200D);
        BudgetDetailsRequestDTO budgetDetails = budget.getBudgetDetails();
        if (budgetDetails == null) {
            budgetDetails = new BudgetDetailsRequestDTO();
        }
        budgetDetails.setAllocatedIn(200D);
        budget.setBudgetDetails(budgetDetails);
        budget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        budget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        budget.setOverrideLocation(VfName.NON_US_OVERRIDE.name());
        if (budget.getStatus() == null) {
            budget.setStatus(StatusTypeCode.ENDED.name());    
        }
        
        mockMvc.perform(
                put("/rest/budgets/755")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(budget))
                )
                .andExpect(status().isOk()).andReturn();
    }
    
    
    // *********************************** Create/Update Distributed Budget ***************************************
    
    private BudgetRequestDTO createDistributedBudget() {
        BudgetRequestDTO parentBudget = new BudgetRequestDTO();
        parentBudget.setName(DISTRIBUTED_BUDGET_NAME);
        parentBudget.setType(BudgetTypeEnum.DISTRIBUTED.name());
        parentBudget.setFromDate(FROM_DATE);
        parentBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        parentBudget.setProjectNumber(PROJECT_NUMBER);
        parentBudget.setTotalAmount(0D);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setAllocatedIn(1000D);
        budgetDetails.setAllocatedOut(1000D);
        parentBudget.setBudgetDetails(budgetDetails);
        parentBudget.setIsUsable(Boolean.FALSE);
        parentBudget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        parentBudget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        parentBudget.setOverrideLocation(VfName.US_OVERRIDE.name());
        
        BudgetRequestDTO childBudgetOne = new BudgetRequestDTO();
        childBudgetOne.setName(DISTRIBUTED_BUDGET_NAME);
        childBudgetOne.setType(BudgetTypeEnum.DISTRIBUTED.name());
        childBudgetOne.setFromDate(FROM_DATE);
        childBudgetOne.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        childBudgetOne.setProjectNumber(PROJECT_NUMBER);
        childBudgetOne.setTotalAmount(500D);
        BudgetDetailsRequestDTO budgetDetailsOne = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO groupOne = new GroupMemberStubDTO().setGroupId(MO_GROUP_ID);
        budgetDetailsOne.setBudgetUsers(Arrays.asList(groupOne));
        budgetDetailsOne.setAllocatedIn(500D);
        budgetDetailsOne.setAllocatedOut(0D);
        childBudgetOne.setBudgetDetails(budgetDetailsOne);
        childBudgetOne.setIsUsable(Boolean.TRUE);
        
        BudgetRequestDTO childBudgetTwo = new BudgetRequestDTO();
        childBudgetTwo.setName(DISTRIBUTED_BUDGET_NAME);
        childBudgetTwo.setType(BudgetTypeEnum.DISTRIBUTED.name());
        childBudgetTwo.setFromDate(FROM_DATE);
        childBudgetTwo.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        childBudgetTwo.setProjectNumber(PROJECT_NUMBER);
        childBudgetTwo.setTotalAmount(500D);
        BudgetDetailsRequestDTO budgetDetailsTwo = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO groupTwo = new GroupMemberStubDTO().setGroupId(IL_GROUP_ID);
        budgetDetailsTwo.setBudgetUsers(Arrays.asList(groupTwo));
        budgetDetailsTwo.setAllocatedIn(500D);
        budgetDetailsTwo.setAllocatedOut(0D);
        childBudgetTwo.setBudgetDetails(budgetDetailsTwo);
        childBudgetTwo.setIsUsable(Boolean.TRUE);
        
        parentBudget.setChildBudgets(Arrays.asList(childBudgetOne, childBudgetTwo));
        return parentBudget;
    }

    @Test
    public void test_create_distributed_budget_good() throws Exception {

        mockMvc.perform(
                post("/rest/budgets")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(createDistributedBudget()))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void test_update_distributed_budget_good() throws Exception {
        BudgetRequestDTO budget = mapBudgetForRequest(budgetService.getBudgetById(811L));        
        
        //Update giving limit for all child budgets
        List<BudgetRequestDTO> childBudgetList = budget.getChildBudgets();
        List<BudgetRequestDTO> updatedChildBudgets = new ArrayList<>();
        for (BudgetRequestDTO childBudget : childBudgetList) {
            childBudget.setIndividualGivingLimit(200D);
            childBudget.setStatus(StatusTypeCode.ACTIVE.name());
            updatedChildBudgets.add(childBudget);
        }
        budget.setChildBudgets(updatedChildBudgets);
        budget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        budget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        budget.setOverrideLocation(VfName.NON_US_OVERRIDE.name());
        
        if (budget.getStatus() == null) {
            budget.setStatus(StatusTypeCode.ACTIVE.name());    
        }
        
        mockMvc.perform(
                put("/rest/budgets/811")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(budget))
                )
                .andExpect(status().isOk());
    }
        
    
    // *********************************** Create/Update Allocated Budget ***************************************
    
    private BudgetRequestDTO createAllocatedBudget() {
        BudgetRequestDTO topBudget = new BudgetRequestDTO();
        topBudget.setName(ALLOCATED_BUDGET_NAME);
        topBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        topBudget.setFromDate(FROM_DATE);
        topBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        topBudget.setProjectNumber(PROJECT_NUMBER);
        topBudget.setTotalAmount(0D);
        BudgetDetailsRequestDTO budgetDetails = new BudgetDetailsRequestDTO();
        budgetDetails.setAllocatedIn(500D);
        budgetDetails.setAllocatedOut(500D);
        topBudget.setBudgetDetails(budgetDetails);
        topBudget.setIsUsable(Boolean.FALSE);
        topBudget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        topBudget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        topBudget.setOverrideLocation(VfName.US_OVERRIDE.name());
        
        BudgetRequestDTO levelOneBudget = new BudgetRequestDTO();
        levelOneBudget.setName(ALLOCATED_BUDGET_NAME);
        levelOneBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        levelOneBudget.setFromDate(FROM_DATE);
        levelOneBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        levelOneBudget.setProjectNumber(PROJECT_NUMBER);
        levelOneBudget.setTotalAmount(250D);
        BudgetDetailsRequestDTO budgetDetailsOne = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO paxOne = new GroupMemberStubDTO().setPaxId(8571L);
        budgetDetailsOne.setBudgetUsers(Arrays.asList(paxOne));
        budgetDetailsOne.setCanAllocate(Boolean.TRUE);
        budgetDetailsOne.setAllocatedIn(500D);
        budgetDetailsOne.setAllocatedOut(250D);
        levelOneBudget.setBudgetDetails(budgetDetailsOne);
        levelOneBudget.setIsUsable(Boolean.TRUE);
        
        BudgetRequestDTO levelTwoBudget = new BudgetRequestDTO();
        levelTwoBudget.setName(ALLOCATED_BUDGET_NAME);
        levelTwoBudget.setType(BudgetTypeEnum.ALLOCATED.name());
        levelTwoBudget.setFromDate(FROM_DATE);
        levelTwoBudget.setBudgetConstraint(BudgetConstraintEnum.HARD_BUDGET.name());
        levelTwoBudget.setProjectNumber(PROJECT_NUMBER);
        levelTwoBudget.setTotalAmount(250D);
        BudgetDetailsRequestDTO budgetDetailsTwo = new BudgetDetailsRequestDTO();
        GroupMemberStubDTO paxTwo = new GroupMemberStubDTO().setPaxId(847L);
        budgetDetailsTwo.setBudgetUsers(Arrays.asList(paxTwo));
        budgetDetailsTwo.setCanAllocate(Boolean.TRUE);
        budgetDetailsTwo.setAllocatedIn(250D);
        budgetDetailsTwo.setAllocatedOut(0D);
        levelTwoBudget.setBudgetDetails(budgetDetailsTwo);
        levelTwoBudget.setIsUsable(Boolean.TRUE);
        
        levelOneBudget.setChildBudgets(Arrays.asList(levelTwoBudget));
        topBudget.setChildBudgets(Arrays.asList(levelOneBudget));
        
        return topBudget;
    }

    @Test
    public void test_create_allocated_budget_good() throws Exception {
        mockMvc.perform(
                post("/rest/budgets")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(createAllocatedBudget()))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void test_update_allocated_budget_good() throws Exception {
        BudgetRequestDTO budget = mapBudgetForRequest(budgetService.getBudgetById(5086L));        
        
        //Update fromDate and AccountingId
        budget.setAccountingId("UnitTest");
        budget.setFromDate("2016-11-24");
        if (budget.getStatus() == null) {
            budget.setStatus(StatusTypeCode.ACTIVE.name());    
            }
        List<BudgetRequestDTO> levelOneChildBudgets = budget.getChildBudgets();
        for (BudgetRequestDTO levelOneChildBudget : levelOneChildBudgets) {
            levelOneChildBudget.setAccountingId("UnitTest");
            levelOneChildBudget.setFromDate("2016-11-24");
            levelOneChildBudget.setStatus(StatusTypeCode.ACTIVE.name());
            List<BudgetRequestDTO> levelTwoChildBudgets = levelOneChildBudget.getChildBudgets();
            for (BudgetRequestDTO levelTwoChildBudget : levelTwoChildBudgets) {
                levelTwoChildBudget.setAccountingId("UnitTest");
                levelTwoChildBudget.setFromDate("2016-11-24");
                levelTwoChildBudget.setStatus(StatusTypeCode.ACTIVE.name());
            }
        }
        budget.setOverrideProjectNumber(OVERRIDE_PROJECT_NUMBER);
        budget.setOverrideAccountingId(OVERRIDE_ACCOUNTING_ID);
        budget.setOverrideLocation(VfName.NON_US_OVERRIDE.name());
        
        mockMvc.perform(
                put("/rest/budgets/5086")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(budget))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void test_update_budget_forbidden() throws Exception {
        //Greg does not have the BUDGET_ALLOCATOR role so he gets a forbidden
        mockMvc.perform(
                put("/rest/budgets/5087")
                .with(authToken(TestConstants.USER_PORTERGA))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(mapBudgetForRequest(budgetService.getBudgetById(5087L))))
                )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_update_budget_not_allocator_for_this_budget() throws Exception {

        //Leah does have the BUDGET_ALLOCATOR role but she is not the allocator for this budget
        mockMvc.perform(
                put("/rest/budgets/5087")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                .content(ObjectUtils.objectToJson(mapBudgetForRequest(budgetService.getBudgetById(5087L))))
                )
                .andExpect(status().isBadRequest());
    }
    
    // *********************************** Delete Budget By ID ***************************************
    
    @Test
    public void test_delete_budget_good() throws Exception{
        mockMvc.perform(
                delete("/rest/budgets/39")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_delete_budget_used_in_program() throws Exception {
        mockMvc.perform(
                delete("/rest/budgets/78")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_delete_budget_used_in_transactions() throws Exception {
        mockMvc.perform(
                delete("/rest/budgets/1")
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isBadRequest());
    }
    
    
    // *********************************** Get Budget By ID ***************************************
    
    @Test
    public void test_getBudgetById_type_simple() throws Exception {
        
        Budget budget = cnxtBudgetRepository.findByBudgetCode(SIMPLE_BUDGET_NAME).get(0);
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_BY_ID, budget.getId()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        BudgetDTO budgetResponse = ObjectUtils.jsonToObject(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertThat(budgetResponse.getName(), is(SIMPLE_BUDGET_NAME));
        assertThat(budgetResponse.getType(), is(BudgetTypeEnum.SIMPLE.name()));
        assertNull(budgetResponse.getChildBudgets());
    }
    
    @Test
    public void test_getBudgetById_type_distributed() throws Exception {
        
        Budget budget = cnxtBudgetRepository.findByBudgetCode(DISTRIBUTED_BUDGET_NAME).get(0);
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_BY_ID, budget.getId()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        BudgetDTO budgetResponse = ObjectUtils.jsonToObject(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertThat(budgetResponse.getName(), is(DISTRIBUTED_BUDGET_NAME));
        assertThat(budgetResponse.getType(), is(BudgetTypeEnum.DISTRIBUTED.name()));
        assertNotNull(budgetResponse.getChildBudgets());
        assertThat(budgetResponse.getChildBudgets().size(), is(35));
        
        // childBudget is null on distributed children 
        for (BudgetDTO childBudget: budgetResponse.getChildBudgets()) {
            assertNull(childBudget.getChildBudgets());
        }
        
    }
    
    @Test
    public void test_getBudgetById_type_allocated() throws Exception {
        
        Budget budget = cnxtBudgetRepository.findByBudgetCode(ALLOCATED_BUDGET_NAME).get(0);
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_BY_ID, budget.getId()))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        BudgetDTO budgetResponse = ObjectUtils.jsonToObject(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertThat(budgetResponse.getName(), is(ALLOCATED_BUDGET_NAME));
        assertThat(budgetResponse.getType(), is(BudgetTypeEnum.ALLOCATED.name()));
        assertNotNull(budgetResponse.getChildBudgets());
        assertThat(budgetResponse.getChildBudgets().size(), is(2));
        assertNotNull(budgetResponse.getChildBudgets().get(0));
        assertThat(budgetResponse.getChildBudgets().get(0).getChildBudgets().size(), is(2));
        assertNull(budgetResponse.getChildBudgets().get(1).getChildBudgets());
    }    
    
    // *********************************** Get My Individual Remaining Budget ***************************************
    
    @Test
    public void test_getBudgetEligibility_givingLimitOnly_true() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_ELIGIBLE, TestConstants.PAX_HARWELLM))
                .param(GIVING_LIMIT_ONLY_REST_PARAM, Boolean.TRUE.toString())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        List<BudgetDTO> budgetResponse = ObjectUtils.jsonToList(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertNotNull(budgetResponse.get(0).getIndividualUsedAmount());
    }
    
    @Test
    public void test_getBudgetEligibility_givingLimitOnly_false() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_ELIGIBLE, TestConstants.PAX_HARWELLM))
                .param(GIVING_LIMIT_ONLY_REST_PARAM, Boolean.FALSE.toString())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        List<BudgetDTO> budgetResponse = ObjectUtils.jsonToList(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertNotNull(budgetResponse.get(0).getIndividualUsedAmount());
    }
    
    @Test
    public void test_getBudgetEligibility_missing_givingLimitOnly() throws Exception {
        budgetEligibilityCacheService.buildCache();
        
        MvcResult result = mockMvc.perform(
                get(String.format(URL_BUDGET_ELIGIBLE, TestConstants.PAX_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();
        
        assertNotNull(result.getResponse());
        
        List<BudgetDTO> budgetResponse = ObjectUtils.jsonToList(result.getResponse().getContentAsString(), BudgetDTO.class);
        assertNotNull(budgetResponse);
        assertNotNull(budgetResponse.get(0).getIndividualUsedAmount());
    }
    
    @Test
    public void test_getBudgetEligibility_invalid_participantId() throws Exception {
        
        mockMvc.perform(
                get(String.format(URL_BUDGET_ELIGIBLE, "invalid"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isNotFound());
    }
}
