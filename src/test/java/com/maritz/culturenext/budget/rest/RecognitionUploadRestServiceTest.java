package com.maritz.culturenext.budget.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class RecognitionUploadRestServiceTest extends AbstractRestTest{
    
    private static final String PROGRAM_ID_TEST = "13577";
    private static final String PROGRAM_ID_STRING = "programIdString";
    
    private static final String TEMPLATES_RECOGNITIONS_URL =  "/rest/programs/%s/recognitions/validation";
    
    private static final String TEMPLATES_FILE_CONTENT= 
            "Participant First Name,Participant Last Name,Participant ID - Mandatory,Award Amount\n"
            + "Andy,Kilinskis (Admin),141,5\n"
            + "Alex,Mudd,8987,10\n"
            + "Omar,MadridOntiveros,12289,5";
    
    private static final MockMultipartFile multipartfile= new MockMultipartFile("file", "Recognition_template.csv",
            ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.get(0), TEMPLATES_FILE_CONTENT.getBytes());
    

    @Test
    public void test_postUploadRecognitionFile_success() throws Exception {

        mockMvc.perform(
                fileUpload(String.format(TEMPLATES_RECOGNITIONS_URL,PROGRAM_ID_TEST))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_postUploadRecognitionFile_fail_unauthorized() throws Exception {
        

        mockMvc.perform(
                fileUpload(String.format(TEMPLATES_RECOGNITIONS_URL,PROGRAM_ID_TEST))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_INVALID))
                ).andExpect(status().isUnauthorized());
        
    }
    
    
    @Test
    public void test_postUploadRecognitionFile_fail_null_file() throws Exception {
        MockMultipartFile multipartfile= new MockMultipartFile("fileNotvalid", "Recognition_template.csv", 
                ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.get(0), TEMPLATES_FILE_CONTENT.getBytes());
        
        mockMvc.perform(
                fileUpload(String.format(TEMPLATES_RECOGNITIONS_URL,PROGRAM_ID_TEST))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isBadRequest());

    }

    
    @Test
    public void test_postUploadRecognitionFile_fail_program_id_as_string() throws Exception {
        
        mockMvc.perform(
                fileUpload(String.format(TEMPLATES_RECOGNITIONS_URL,PROGRAM_ID_STRING))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isNotFound());

    }
}
