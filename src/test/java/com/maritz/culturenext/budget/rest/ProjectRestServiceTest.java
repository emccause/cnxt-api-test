package com.maritz.culturenext.budget.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ProjectRestServiceTest extends AbstractRestTest {

    @Test
    public void test_get_project_numbers() throws Exception {
        mockMvc.perform(
                get("/rest/award-project-numbers")
                .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(status().isOk());
    }
}
