package com.maritz.culturenext.budget.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;

import com.maritz.culturenext.budget.constants.BudgetConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class BudgetEligibilityRestTest extends AbstractRestTest {
    
    @Test
    public void get_budget_eligibility_good() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/" + TestConstants.ID_1 + "/budgets")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }

    @Ignore("FIXME: getting a 200 response, expecting 400")
    @Test
    public void get_budget_eligibility_invalid_giver_pax() throws Exception {
        mockMvc.perform(
                get("/rest/participants/0/programs/" + TestConstants.ID_1 + "/budgets")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_INVALID_PAX)));
    } 
    
    @Test
    public void get_budget_eligibility_null_program_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/" + "" + "/budgets")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isNotFound())
       ;
    } 
    
    @Test
    public void get_budget_eligibility_invalid_program_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/9999/budgets")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_INVALID_PROGRAM_ID)));
    } 
    
    @Test
    public void get_budget_eligibility_inactive_program() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/" + TestConstants.PROGRAM_INACTIVE + "/budgets")
                .with(authToken(TestConstants.USER_ALGELD))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_INACTIVE_PROGRAM)));
    }
    
    @Test
    public void get_budget_eligibility_ended_program() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/" + TestConstants.PROGRAM_ENDED + "/budgets")
                .with(authToken(TestConstants.USER_ALGELD))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_INACTIVE_PROGRAM)));
    }
    
    @Test
    public void get_budget_eligibility_scheduled_program() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/programs/" + TestConstants.PROGRAM_SCHEDULED + "/budgets")
                .with(authToken(TestConstants.USER_ALGELD))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_INACTIVE_PROGRAM)));
    }
    
    @Ignore("FIXME: getting a 200 response, expecting 400")
    @Test
    public void get_budget_eligibility_not_admin() throws Exception {
        mockMvc.perform(
                get("/rest/participants/8571/programs/" + TestConstants.ID_1 + "/budgets")
                .with(authToken(TestConstants.USER_ALGELD))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetConstants.ERROR_NOT_ADMIN)));
    }  
}
