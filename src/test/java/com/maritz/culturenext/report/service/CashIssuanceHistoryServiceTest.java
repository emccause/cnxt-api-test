package com.maritz.culturenext.report.service;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.reports.dao.CashIssuanceHistoryDao;
import com.maritz.culturenext.reports.dto.CashIssuanceHistoryDTO;
import com.maritz.culturenext.reports.dto.PendingCashIssuanceDTO;
import com.maritz.culturenext.reports.service.impl.CashIssuanceHistoryServiceImpl;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;

public class CashIssuanceHistoryServiceTest extends AbstractMockTest {

    @Mock private CashIssuanceHistoryDao cashIssuanceHistoryDao;
    @Mock private Environment environment;
    @Mock private Security security;

    @InjectMocks private CashIssuanceHistoryServiceImpl cashIssuanceHistoryService;

    private static final String CASH_HISTORY_PATH = "/cash-issuance-history";
    private static final long TEST_BATCH_ID = 23554L;
    private static final double TEST_AWARD_AMOUNT = 40;

    @Before
    public void setup() {

        PowerMockito.when(environment.getProperty(anyString())).thenReturn(Boolean.TRUE.toString());
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        List<Map<String, Object>> cashIssuanceHistoryList = new ArrayList<>();
        List<Map<String, Object>> pendingCashIssuanceList = new ArrayList<>();
        Map<String, Object> cashEntry = new HashMap<>();

        //Mock out the Pending Cash Data
        cashEntry.put(ProjectConstants.TOTAL_RECORDS, 1);
        cashEntry.put(ProjectConstants.AWARD_AMOUNT, TEST_AWARD_AMOUNT);

        pendingCashIssuanceList.add(cashEntry);

        //Mock out the Cash History Data
        cashEntry.put(ProjectConstants.REPORT_CREATE_DATE, DateUtil.convertFromString("2017-03-28T20:46Z"));
        cashEntry.put(ProjectConstants.BATCH_ID, TEST_BATCH_ID);
        cashEntry.put(ProjectConstants.TOTAL_RECORDS, 1);
        cashEntry.put(ProjectConstants.AWARD_AMOUNT, TEST_AWARD_AMOUNT);
        cashEntry.put(ProjectConstants.REPORT_TYPE, "AdHoc");
        cashEntry.put(ProjectConstants.STATUS, "COMPLETE");
        cashEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);

        cashIssuanceHistoryList.add(cashEntry);

        PowerMockito.when(cashIssuanceHistoryDao.getPendingCashIssuance()).thenReturn(pendingCashIssuanceList);
        PowerMockito.when(cashIssuanceHistoryDao.getCashIssuanceHistory(anyInt(), anyInt())).thenReturn(cashIssuanceHistoryList);
    }

    @Test
    public void test_get_pending_cash_issuance() {

        PendingCashIssuanceDTO response = cashIssuanceHistoryService.getPendingCashIssuance();

        Mockito.verify(cashIssuanceHistoryDao, times(1)).getPendingCashIssuance();

        assertNotNull("should have a response", response);
    }

    @Test
    public void test_get_pending_cash_issuance_empty() {

        List<Map<String, Object>> emptyPendingList = new ArrayList<>();

        assertThat(emptyPendingList, empty());

        PowerMockito.when(cashIssuanceHistoryDao.getPendingCashIssuance()).thenReturn(emptyPendingList);

        PendingCashIssuanceDTO response = cashIssuanceHistoryService.getPendingCashIssuance();

        Mockito.verify(cashIssuanceHistoryDao, times(1)).getPendingCashIssuance();

        assertNotNull("should have a response", response);
        assertThat(response, instanceOf(PendingCashIssuanceDTO.class));
    }

    @Test
    public void test_get_pending_cash_issuance_null() {

        List<Map<String, Object>> nullPendingList = null;

        assertNull(nullPendingList);

        PowerMockito.when(cashIssuanceHistoryDao.getPendingCashIssuance()).thenReturn(nullPendingList);

        PendingCashIssuanceDTO response = cashIssuanceHistoryService.getPendingCashIssuance();

        Mockito.verify(cashIssuanceHistoryDao, times(1)).getPendingCashIssuance();

        assertNotNull("should have a response", response);
        assertThat(response, instanceOf(PendingCashIssuanceDTO.class));
    }

    @Test
    public void test_get_cash_issuance_history() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(ProjectConstants.REPORTS_URI_BASE + CASH_HISTORY_PATH)
                .setPageNumber(1)
                .setPageSize(1);

        PaginatedResponseObject<CashIssuanceHistoryDTO> response =
                cashIssuanceHistoryService.getCashIssuanceHistory(requestDetails, 1, 1);

        Mockito.verify(cashIssuanceHistoryDao, times(1)).getCashIssuanceHistory(anyInt(), anyInt());

        assertNotNull("should have a response", response);

        List<CashIssuanceHistoryDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }
}
