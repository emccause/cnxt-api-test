package com.maritz.culturenext.report.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockHttpServletResponse;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enrollment.config.services.impl.EnrollmentConfigServiceImpl;
import com.maritz.culturenext.reports.constants.CashIssuanceReportConstants;
import com.maritz.culturenext.reports.dao.impl.CashIssuanceReportsDaoImpl;
import com.maritz.culturenext.reports.dao.impl.ParticipantReportsDaoImpl;
import com.maritz.culturenext.reports.service.impl.CashIssuanceReportsServiceImpl;
import com.maritz.test.AbstractMockTest;

public class CashIssuanceReportServiceTest extends AbstractMockTest {

    @InjectMocks private CashIssuanceReportsServiceImpl cashIssuanceReportsService;
    @Mock private CashIssuanceReportsDaoImpl cashIssuanceReportsDao;
    @Mock private ConcentrixDao<Batch> batchDao;
    @Mock private ConcentrixDao<BatchFile> batchFileDao;
    @Mock private EnrollmentConfigServiceImpl enrollmentConfigService;
    @Mock private Environment environment;
    @Mock private ParticipantReportsDaoImpl participantReportsDao;
    @Mock private Security security;
    @Mock private BatchUtil batchUtil;

    @SuppressWarnings("unchecked")
    private FindBy<Batch> batchFindby = PowerMockito.mock(FindBy.class);
    @SuppressWarnings("unchecked")
    private FindBy<BatchFile> batchFileFindby = PowerMockito.mock(FindBy.class);

    private MockHttpServletResponse response = new MockHttpServletResponse();

    private List<Map<String, Object>> processResults = new ArrayList<>();
    private List<Map<String, Object>> reportData = new ArrayList<>();

    private String FILE_NAME = "filename.csv";

    @Before
    public void setup() {
        // Mock objects.
        PowerMockito.when(batchDao.findBy()).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.where(ProjectConstants.BATCH_ID)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(Long.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.in(anyListOf(String.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.and(ProjectConstants.BATCH_TYPE_CODE)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(String.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(String.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.exists()).thenReturn(true);

        BatchFile batchFile = new BatchFile();
        batchFile.setId(1L);
        batchFile.setFilename(FILE_NAME);

        String fileContent = "'Transaction ID,Nomination ID,Submitted Date,Recipient''s ID,Recipient''s First Name,Recipient''s Last Name,Submitter''s ID,Submitter''s First Name,Submitter''s Last Name,Approver''s ID,Approver''s First Name,Approver''s Last Name,Award Amount,Payout Type,Program Name,Value,Headline,Program Type,Budget Name,Recognition Status,Approval Date,Issuance Date,Award Code,Batch Id\n"
                + "40525,43116,2014-05-22 16:01:19,6718,Tony,Bergeron,8571,Léah,Harwell,,,,25.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537'Transaction ID,Nomination ID,Submitted Date,Recipient''s ID,Recipient''s First Name,Recipient''s Last Name,Submitter''s ID,Submitter''s First Name,Submitter''s Last Name,Approver''s ID,Approver''s First Name,Approver''s Last Name,Award Amount,Payout Type,Program Name,Value,Headline,Program Type,Budget Name,Recognition Status,Approval Date,Issuance Date,Award Code,Batch Id\n"
                + "40525,43116,2014-05-22 16:01:19,6718,Tony,Bergeron,8571,Léah,Harwell,,,,25.0,CASH,You Rock!,,,PEER_TO_PEER,Leah''s Super Awesome Budget,APPROVED,,2015-09-16T19:02Z,,23537\n";
        batchFile.setFileUpload(fileContent);

        PowerMockito.when(batchFileDao.findBy()).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.where(ProjectConstants.BATCH_ID)).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.eq(any(Long.class))).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.findOne()).thenReturn(batchFile);

        Batch batch = new Batch();
        batch.setBatchId(1L);
        batch.setBatchTypeCode(CashIssuanceReportConstants.CASH_REPORT);
        batch.setStatusTypeCode(StatusTypeCode.IN_PROGRESS.name());

        PowerMockito.when(batchDao.findBy()).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.where(ProjectConstants.BATCH_ID)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(Long.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.findOne()).thenReturn(batch);

        Long batchId = 1L;

        @SuppressWarnings({ "rawtypes", "unchecked" })
        Map<String, Object> map = new HashMap();
        map.put(ProjectConstants.BATCH_ID, batchId);
        processResults.add(map);

        @SuppressWarnings({ "rawtypes", "unchecked" })
        Map<String, Object> mapData = new HashMap();
        mapData.put(ProjectConstants.BATCH_ID, batchId);
        reportData.add(mapData);
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_happy_path() {
        Long batchId = 1L;
        try {
            cashIssuanceReportsService.getCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_null_batchId() {
        Long batchId = null;
        try {
            cashIssuanceReportsService.getCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_wrong_batchId() {
        Long batchId = -1L;
        try {
            cashIssuanceReportsService.getCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_wrong_batch_type_code() {
        Long batchId = 1L;
        PowerMockito.when(batchDao.findBy()).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.where(ProjectConstants.BATCH_ID)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(Long.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.in(anyListOf(String.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.and(ProjectConstants.BATCH_TYPE_CODE)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq("OTHER")).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.eq(any(String.class))).thenReturn(batchFindby);
        PowerMockito.when(batchFindby.exists()).thenReturn(false);
        try {
            cashIssuanceReportsService.getCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_createCashIssuanceReport_happy_path() {
        try {
            PowerMockito.when(cashIssuanceReportsDao.processCashPayouts()).thenReturn(processResults);
            cashIssuanceReportsService.createCashIssuanceReport(response);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_updateCashIssuanceReportById_happy_path() {
        Long batchId = 1L;
        try {
            cashIssuanceReportsService.updateCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_updateCashIssuanceReportById_null_batch_file() {
        Long batchId = 1L;
        PowerMockito.when(batchFileDao.findBy()).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.where(ProjectConstants.BATCH_ID)).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.eq(any(Long.class))).thenReturn(batchFileFindby);
        PowerMockito.when(batchFileFindby.findOne()).thenReturn(null);
        try {
            cashIssuanceReportsService.updateCashIssuanceReportById(response, batchId);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_createCashIssuanceReport_no_records_processed() {
        try {
            List<Map<String, Object>> emptyList = null;
            PowerMockito.when(cashIssuanceReportsDao.processCashPayouts()).thenReturn(emptyList);
            PowerMockito.when(cashIssuanceReportsDao.saveCashIssuanceReport(anyLong(), anyString(), anyString()))
                    .thenReturn(emptyList);
            cashIssuanceReportsService.createCashIssuanceReport(response);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_createCashIssuanceReport_no_file() {
        try {
            List<Map<String, Object>> emptyList = null;
            PowerMockito.when(cashIssuanceReportsDao.processCashPayouts()).thenReturn(processResults);
            PowerMockito.when(cashIssuanceReportsDao.saveCashIssuanceReport(anyLong(), anyString(), anyString()))
                    .thenReturn(emptyList);
            cashIssuanceReportsService.createCashIssuanceReport(response);

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_createCashIssuanceReport_no_batch_id() {
        try {
            List<Map<String, Object>> emptyList = new ArrayList<>();
            PowerMockito.when(cashIssuanceReportsDao.processCashPayouts()).thenReturn(emptyList);
            cashIssuanceReportsService.createCashIssuanceReport(response);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_createCashIssuanceReport_no_file_with_batch_id() {
        try {
            List<Map<String, Object>> emptyList = new ArrayList<>();
            PowerMockito.when(cashIssuanceReportsDao.processCashPayouts()).thenReturn(processResults);
            PowerMockito.when(cashIssuanceReportsDao.saveCashIssuanceReport(anyLong(), anyString(), anyString()))
                    .thenReturn(emptyList);
            cashIssuanceReportsService.createCashIssuanceReport(response);

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }
}
