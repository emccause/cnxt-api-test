package com.maritz.culturenext.report.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class PendingEmailsReportRestServiceTest extends AbstractRestTest {

    private final String PENDING_EMAILS_REPORT_URL = "/rest/reports/held-emails";

    @Test
    public void test_getPendingEmailsReport_success_no_param() throws Exception {
        mockMvc.perform(
                get(PENDING_EMAILS_REPORT_URL)
                        .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_getPendingEmailsReport_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(PENDING_EMAILS_REPORT_URL)
                        .with(authToken(TestConstants.USER_MADRIDO))
        )
                .andExpect(status().isForbidden())
        ;
    }

    @Test
    public void test_getPendingEmailsReport_success_status() throws Exception {
        mockMvc.perform(
                get(PENDING_EMAILS_REPORT_URL)
                        .param(ProjectConstants.STATUS, StatusTypeCode.PENDING.name() + "," + StatusTypeCode.ERROR.name())
                        .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk())
        ;
    }
}
