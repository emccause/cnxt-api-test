package com.maritz.culturenext.report.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.reports.constants.CashIssuanceReportConstants;
import com.maritz.test.AbstractRestTest;

public class CashIssuanceReportsRestServiceTest extends AbstractRestTest {
    
    @Inject private ConcentrixDao<Batch> batchDao;

    private static Long NEW_BATCH_ID;
    private static Long COMPLETE_BATCH_ID;
    
    private static final String CASH_ISSUANCE_BATCH_PATH = "/rest/reports/cash-issuance/batch-id/";
    private static final String CASH_ISSUANCE_PATH = "/rest/reports/cash-issuance";
    private static final String nullString = null;
    
    @Before
    public void setup() {
        NEW_BATCH_ID = batchDao.findBy()
        .where(ProjectConstants.BATCH_TYPE_CODE).eq(CashIssuanceReportConstants.CASH_REPORT)
        .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.NEW.name())
        .findOne(ProjectConstants.BATCH_ID, Long.class);
        
        COMPLETE_BATCH_ID = batchDao.findBy()
        .where(ProjectConstants.BATCH_TYPE_CODE).eq(CashIssuanceReportConstants.CASH_REPORT)
        .and(ProjectConstants.STATUS_TYPE_CODE).eq(StatusTypeCode.COMPLETE.name())
        .findOne(ProjectConstants.BATCH_ID, Long.class);        
    }
    
    @Test
    public void test_post_cash_issuance_report() throws Exception {
        mockMvc.perform(
                post(CASH_ISSUANCE_PATH)
                .with(authToken(TestConstants.USER_GARCIAF2)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_put_cash_issuance_report_incorrect_status() throws Exception {
        mockMvc.perform(
                put(CASH_ISSUANCE_BATCH_PATH + COMPLETE_BATCH_ID.toString())
                .with(authToken(TestConstants.USER_GARCIAF2)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void test_put_cash_issuance_report() throws Exception {
        mockMvc.perform(
                put(CASH_ISSUANCE_BATCH_PATH + NEW_BATCH_ID.toString())
                .with(authToken(TestConstants.USER_GARCIAF2))
            )
            .andExpect(status().isOk());
    }
    @Test
    public void test_put_cash_issuance_report_no_batch_id() throws Exception {
        mockMvc.perform(
                put(CASH_ISSUANCE_PATH)
                .param(ProjectConstants.BATCH_ID, nullString)
                .with(authToken(TestConstants.USER_GARCIAF2)))
                .andExpect(status().isMethodNotAllowed());
    }
    
    @Test
    public void test_get_cash_issuance_report() throws Exception {
        mockMvc.perform(
                get(CASH_ISSUANCE_BATCH_PATH + NEW_BATCH_ID.toString())
                .with(authToken(TestConstants.USER_GARCIAF2)))
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_cash_issuance_report_no_batch_id() throws Exception {
        String nullString = null;
        mockMvc.perform(
                get(CASH_ISSUANCE_PATH)
                .param(ProjectConstants.BATCH_ID, nullString)
                .with(authToken(TestConstants.USER_GARCIAF2)))
                .andExpect(status().isMethodNotAllowed());
    }
}