package com.maritz.culturenext.report.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.reports.constants.ParticipantSummaryReportConstants;
import com.maritz.test.AbstractRestTest;

public class ParticipantSummaryReportRestServiceTest extends AbstractRestTest {

    private static final String PARTICIPANT_SUMMARY_REPORT_URI = "/rest/reports/pax-summary";
    private static final String TEST_PAX_ID_PARAM_VALUE = "8987";
    @Test
    public void test_get_participant_summary_report_no_param_success() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_get_participant_summary_report_with_param_success() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(PAX_ID_REST_PARAM, TEST_PAX_ID_PARAM_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_get_participant_summary_report_with_issued_status_param_success() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(STATUS_REST_PARAM, StatusTypeCode.ISSUED.name())
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_get_participant_summary_report_non_admin() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(STATUS_REST_PARAM, StatusTypeCode.ISSUED.name())
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void test_get_participant_summary_report_with_program_type_param_success() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(PROGRAM_TYPE_REST_PARAM, ProgramTypeEnum.AWARD_CODE.name() + ","
                        + ProgramTypeEnum.ECARD_ONLY.name() + ","
                        + ProgramTypeEnum.PEER_TO_PEER.name() + ","
                        + ProgramTypeEnum.MANAGER_DISCRETIONARY.name() + ","
                        + ProgramTypeEnum.POINT_LOAD.name() + ","
                        + ProgramTypeEnum.MILESTONE.name()
                    )
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_get_participant_summary_report_with_milestone_program_type_param_success() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(PROGRAM_TYPE_REST_PARAM, ProgramTypeEnum.MILESTONE.name())
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_get_participant_summary_report_with_invalid_program_type_param() throws Exception {
        mockMvc.perform(
                get(PARTICIPANT_SUMMARY_REPORT_URI)
                .param(PROGRAM_TYPE_REST_PARAM, "BAD_$_PROGRAM_TYPE")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$[0].code", is(ParticipantSummaryReportConstants.PARAM_ONLY_SUPPORTS_LETTERS_ERROR)));
        ;
    }
}
