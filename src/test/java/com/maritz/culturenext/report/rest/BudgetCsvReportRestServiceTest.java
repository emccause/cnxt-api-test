package com.maritz.culturenext.report.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class BudgetCsvReportRestServiceTest extends AbstractRestTest{

    private final String REPORTS_GIVING_LIMIT_SUMMARY_URL = "/rest/reports/giving-limit-summary";
    private final String REPORTS_BUDGET_SUMMARY_URL = "/rest/reports/budget-summary";
    private final String REPORTS_BUDGET_TRANSACTION_DETAIL_URL = "/rest/reports/budget-transaction-detail";
    
    
    @Test
    public void test_getBudgetGivingLimitReport_success_no_param() throws Exception {
        mockMvc.perform(
                get(REPORTS_GIVING_LIMIT_SUMMARY_URL)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_getBudgetGivingLimitReport_success_status_and_budgetId() throws Exception {
        mockMvc.perform(
                get(REPORTS_GIVING_LIMIT_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.SCHEDULED.name())
                .param(ProjectConstants.BUDGET_ID, "1,2,10,200")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_getBudgetGivingLimitReport_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(REPORTS_GIVING_LIMIT_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name())
                .param(ProjectConstants.BUDGET_ID, "1")
                .with(authToken(TestConstants.USER_MADRIDO))
            )
            .andExpect(status().isForbidden())
        ;
    }
    
    
    @Test
    public void test_getBudgetGivingLimitReport_fail_budgetId_invalid() throws Exception {
        mockMvc.perform(
                get(REPORTS_GIVING_LIMIT_SUMMARY_URL)
                .param(ProjectConstants.BUDGET_ID, "NOT_VALID")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void test_getBudgetSummaryReport_success_no_param() throws Exception {
        mockMvc.perform(
                get(REPORTS_BUDGET_SUMMARY_URL)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_getBudgetSummaryReport_success_status_and_budgetId() throws Exception {
        mockMvc.perform(
                get(REPORTS_BUDGET_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.INACTIVE.name())
                .param(ProjectConstants.BUDGET_ID, "1,85,453")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_getBudgetSummaryReport_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(REPORTS_BUDGET_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name())
                .param(ProjectConstants.BUDGET_ID, "1")
                .with(authToken(TestConstants.USER_MADRIDO))
            )
            .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void test_getBudgetSummaryReport_success_budget_owner() throws Exception {
        mockMvc.perform(
                get(REPORTS_BUDGET_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.INACTIVE.name())
                .param(ProjectConstants.BUDGET_ID, "1,85,453")
                .with(authToken(TestConstants.USER_GARCIAF2))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_getBudgetTransactionDetailReport_success_budget_owner() throws Exception {
        mockMvc.perform(
                get(REPORTS_BUDGET_TRANSACTION_DETAIL_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.INACTIVE.name())
                .with(authToken(TestConstants.USER_GARCIAF2))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_getGivingLimitReport_success_budget_owner() throws Exception {
        mockMvc.perform(
                get(REPORTS_GIVING_LIMIT_SUMMARY_URL)
                .param(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name()+","+StatusTypeCode.SCHEDULED.name())
                .param(ProjectConstants.BUDGET_ID, "1,2,10,200")
                .with(authToken(TestConstants.USER_GARCIAF2))
            )
            .andExpect(status().isOk())
        ;
    }
}
