package com.maritz.culturenext.pointupload.writer;

import com.google.common.collect.Iterables;
import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.pointupload.dto.DiscretionaryExtended;
import com.maritz.culturenext.transaction.dao.TransactionHeaderMiscDao;
import com.maritz.test.AbstractMockTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;

public class PointUploadWriterTest extends AbstractMockTest {

    @Mock
    com.maritz.core.services.BatchService batchService;
    @Mock
    com.maritz.core.services.TransactionService transactionService;
    @Mock
    TransactionHeaderMiscDao transactionheaderMiscDao;
    @Mock
    Environment environment;

    @Captor
    private ArgumentCaptor<Collection<TransactionHeaderDetail<Discretionary>>> transactionHeaderDetailsCaptor;

    private PointUploadWriter writer;
    private TransactionHeader transactionHeader;
    private Discretionary discretionary;
    private DiscretionaryExtended item;

    @Before
    public void setup() throws Exception {
        writer = new PointUploadWriter();

        writer.setBatchService(batchService)
                .setTransactionHeaderMiscDao(transactionheaderMiscDao)
                .setTransactionService(transactionService)
                .setEnvironment(environment);

        transactionHeader = new TransactionHeader();
        transactionHeader.setBatchId(400L);
        discretionary = new Discretionary();

        item = new DiscretionaryExtended();
        item.setParticipantId("5L");
        item.setPointAmount(20.0);
        item.setBudgetName("DISC");
        item.setSubProjectNumber("3");
        item.setPayoutType("nat");
        item.setDescription("Arbitrary description");
        item.setOriginalAmount(10.0);
        item.setProgramName("TestProgram");
        item.setTransactHeader(transactionHeader);
        item.setProjectNumber("4");
        item.setDiscretionary(discretionary);

        List<DiscretionaryExtended> discretionaryExtendedList = new ArrayList<DiscretionaryExtended>();
        discretionaryExtendedList.add(item);
        List<List<DiscretionaryExtended>> listOfDiscretionaryExtendedLists = new ArrayList<List<DiscretionaryExtended>>();
        listOfDiscretionaryExtendedLists.add(discretionaryExtendedList);

        writer.write(listOfDiscretionaryExtendedLists);
    }

    @Test
    public void test_write_setsCorrectlyFormattedTime() {
        ArgumentCaptor<BatchEventDTO> argument = ArgumentCaptor.forClass(BatchEventDTO.class);

        Mockito.verify(batchService, times(1)).createBatchEvent(anyLong(), argument.capture());

        assertTrue(argument.getValue().getMessage().matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z"));
    }

    @Test
    public void test_write_setsPayoutTypeInUppercase() {
        Mockito.verify(transactionService, times(1)).create(transactionHeaderDetailsCaptor.capture());

        Collection<TransactionHeaderDetail<Discretionary>> transactionHeaderDetails = transactionHeaderDetailsCaptor.getValue();

        TransactionHeaderDetail<Discretionary> transactionHeaderDetail = Iterables.get(transactionHeaderDetails, 0); //only one exists
        TransactionHeaderMisc transactionHeaderMisc = transactionHeaderDetail.getMisc(VfName.PAYOUT_TYPE.name());
        Assert.assertEquals(1, transactionHeaderDetails.size());
        Assert.assertEquals( "NAT", transactionHeaderMisc.getMiscData());
        Assert.assertNotEquals( "nat", transactionHeaderMisc.getMiscData());
    }
}
