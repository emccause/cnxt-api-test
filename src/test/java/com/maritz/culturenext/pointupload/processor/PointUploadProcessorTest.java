package com.maritz.culturenext.pointupload.processor;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.inject.Inject;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.BudgetMiscRepository;
import com.maritz.core.jpa.repository.BudgetRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.ProjectRepository;
import com.maritz.core.jpa.repository.SubProjectRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.location.PaxGroupService;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.culturenext.pointupload.dao.PointUploadDao;
import com.maritz.culturenext.pointupload.dto.DiscretionaryExtended;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PointUploadProcessorTest extends AbstractDatabaseTest {
    
    private static final String TEST_FILE_NAME = "unit_test_point_load_complete.csv";
    private static final String FILENAME = "filename";
    
    @Inject private ConcentrixDao<BatchFile> batchFileDao;
    @Inject private AddressRepository addressRepository;
    @Inject private BatchFileService batchFileService;
    @Inject private BatchRepository batchRepository;
    @Inject private BatchService batchService;
    @Inject private BudgetMiscRepository budgetMiscRepository;
    @Inject private BudgetRepository budgetRepository;
    @Inject private ParticipantsPppIndexService participantsPppIndexService;
    @Inject private PaxGroupService paxGroupService;
    @Inject private PaxRepository paxRepository;
    @Inject private PointUploadDao pointUploadDao;
    @Inject private ProgramMiscRepository programMiscRepository;
    @Inject private ProgramRepository programRepository;
    @Inject private ProjectRepository projectRepository;
    @Inject private SubProjectRepository subProjectRepository;
    @Inject private SysUserRepository sysUserRepository;

    PointUploadProcessor pointUploadProcessor = new PointUploadProcessor();
    @Before
    public void setup() throws Exception {
        pointUploadProcessor
                .setAddressRepository(addressRepository)
                .setBatchFileService(batchFileService)
                .setBatchRepository(batchRepository)
                .setBatchService(batchService)
                .setBudgetMiscRepository(budgetMiscRepository)
                .setBudgetRepository(budgetRepository)
                .setParticipantsPppIndexService(participantsPppIndexService)
                .setPaxGroupService(paxGroupService)
                .setPaxRepository(paxRepository)
                .setPointUploadDao(pointUploadDao)
                .setProgramMiscRepository(programMiscRepository)
                .setProgramRepository(programRepository)
                .setProjectRepository(projectRepository)
                .setSubProjectRepository(subProjectRepository)
                .setSysUserRepository(sysUserRepository);
    }
    
    @Test
    public void test_process_happy_path() throws Exception  {
        assertNotNull("Batch file is empty.", batchFileDao);
        
        BatchFile batchFile = batchFileDao.findBy()
                .where(FILENAME)
                .eq(TEST_FILE_NAME)
                .findOne();
        List<DiscretionaryExtended> processTest = pointUploadProcessor.process(batchFile);
        Assert.assertEquals("Test data wasn't created!", 3, processTest.size());
        assertNotNull("should have DiscretionaryExtended list", processTest);

        //Validate the things that are the same for every record on the file
        for(DiscretionaryExtended disc : processTest) {
            Assert.assertEquals("Invalid program name", "Point Upload Validation Testing", disc.getProgramName());
            Assert.assertEquals("Invalid budget name", "Point Upload Validation Test Budget", disc.getBudgetName());
            Assert.assertEquals("Invalid payout type", "DPP", disc.getPayoutType());
            Assert.assertEquals("Invalid project number", "S01234", disc.getProjectNumber());
            Assert.assertEquals("Invalid subproject number", "000825", disc.getSubProjectNumber());
            Assert.assertEquals("Invalid budget ID", 825L, disc.getDiscretionary().getBudgetIdDebit().longValue());
        }
    }
}
