package com.maritz.culturenext.pointupload.processor;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.BatchRepository;
import com.maritz.core.jpa.repository.BudgetMiscRepository;
import com.maritz.core.jpa.repository.BudgetRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.ProjectRepository;
import com.maritz.core.jpa.repository.SubProjectRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.location.PaxGroupService;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.culturenext.pointupload.dao.PointUploadDao;
import com.maritz.culturenext.pppx.services.ParticipantsPppIndexService;
import com.maritz.test.AbstractMockTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;

public class PointUploadProcessorUnitTest extends AbstractMockTest {

    @Mock
    AddressRepository addressRepository;
    @Mock
    BatchFileService batchFileService;
    @Mock
    BatchRepository batchRepository;
    @Mock
    BatchService batchService;
    @Mock
    BudgetMiscRepository budgetMiscRepository;
    @Mock
    BudgetRepository budgetRepository;
    @Mock
    ParticipantsPppIndexService participantsPppIndexService;
    @Mock
    PaxGroupService paxGroupService;
    @Mock
    PaxRepository paxRepository;
    @Mock
    PointUploadDao pointUploadDao;
    @Mock
    ProgramMiscRepository programMiscRepository;
    @Mock
    ProgramRepository programRepository;
    @Mock
    ProjectRepository projectRepository;
    @Mock
    SubProjectRepository subProjectRepository;
    @Mock
    SysUserRepository sysUserRepository;

    PointUploadProcessor pointUploadProcessor = new PointUploadProcessor();

    @Before
    public void setup() throws Exception {
        pointUploadProcessor
                .setAddressRepository(addressRepository)
                .setBatchFileService(batchFileService)
                .setBatchRepository(batchRepository)
                .setBatchService(batchService)
                .setBudgetMiscRepository(budgetMiscRepository)
                .setBudgetRepository(budgetRepository)
                .setParticipantsPppIndexService(participantsPppIndexService)
                .setPaxGroupService(paxGroupService)
                .setPaxRepository(paxRepository)
                .setPointUploadDao(pointUploadDao)
                .setProgramMiscRepository(programMiscRepository)
                .setProgramRepository(programRepository)
                .setProjectRepository(projectRepository)
                .setSubProjectRepository(subProjectRepository)
                .setSysUserRepository(sysUserRepository);
    }

    @Test
    public void test_process_setsCorrectlyFormattedTime() throws Exception {
        Batch batch = new Batch();

        BatchFile batchFile = new BatchFile();
        batchFile.setBatchId(1L);
        batchFile.setFileUpload("File upload stuff");

        Mockito.when(batchRepository.findOne(batchFile.getBatchId())).thenReturn(batch);

        pointUploadProcessor.process(batchFile);

        ArgumentCaptor<BatchEventDTO> argument = ArgumentCaptor.forClass(BatchEventDTO.class);

        Mockito.verify(batchService, times(1)).createBatchEvent(any(Batch.class), argument.capture());

        assertTrue(argument.getValue().getMessage().matches("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z"));
    }
}
