package com.maritz.culturenext.newsfeed.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.NewsfeedItemPax;
import com.maritz.core.jpa.entity.NewsfeedItemType;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.networkconnections.services.NetworkConnectionService;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.services.impl.NewsfeedServiceImpl;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;

public class NewsfeedServiceTest extends AbstractMockTest {

    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private ConcentrixDao<NewsfeedItemType> newsfeedItemTypeDao;
    @Mock private ConcentrixDao<PaxMisc> paxMiscDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private ConcentrixDao<ProgramMisc> programMiscDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private Environment environment;
    @Mock private NetworkConnectionService networkConnectionService;
    @Mock private NominationDataBulkSaveDao nominationDataBulkSaveDao;
    @Mock private PaxRepository paxRepository;
    @Mock private ProgramService programService;
    @Mock private RaiseDao raiseDao;
    @Mock private Security security;

    @InjectMocks private final NewsfeedServiceImpl newsfeedService = new NewsfeedServiceImpl();

    private FindBy<NewsfeedItemType> newsfeedItemTypeFindBy = PowerMockito.mock(FindBy.class);
    // External concentrix findBy needed for relationshipRepository findBy
    private FindBy<Relationship> relationshipFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<PaxMisc> paxMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Program> programFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> programMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Recognition> recognitionFindBy = PowerMockito.mock(FindBy.class);

    @Captor private ArgumentCaptor<List<NewsfeedItemPax>> newsfeedItemPaxListCaptor;

    @Before
    public void setup() {
        Program program = new Program();
        program.setProgramId(2341L);
        program.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        program.setFromDate(DateUtil.convertFromString("2015-03-15T03:14:55.223Z"));

        // Common Program findby mocks
        PowerMockito.when(programDao.findBy()).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.where(anyString())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.eq(anyString())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.findOne()).thenReturn(program);

        // Common newsfeedItemType findby mocks
        PowerMockito.when(newsfeedItemTypeDao.findBy()).thenReturn(newsfeedItemTypeFindBy);
        PowerMockito.when(newsfeedItemTypeFindBy.where(anyString())).thenReturn(newsfeedItemTypeFindBy);
        PowerMockito.when(newsfeedItemTypeFindBy.in(anyListOf(String.class))).thenReturn(newsfeedItemTypeFindBy);
        PowerMockito.when(newsfeedItemTypeFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1));

        // Common relationshipRepository findby mocks
        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.where(anyString())).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.eq(anyString())).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.in(anyListOf(Long.class))).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.and(anyString())).thenReturn(relationshipFindBy);

        // Common paxRepository mocks
        PowerMockito.when(paxRepository.exists(7213L)).thenReturn(Boolean.TRUE);

        PowerMockito.when(security.getPaxId()).thenReturn(141L);


        // Recognition Recipients for Raise setup

        List<Map<String, Object>> recognitionRecipientsFromDAO = new ArrayList<>();

        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.RECEIVER_PAX_ID, TestConstants.PAX_DAGARFIN);
        recognitionRecipientsFromDAO.add(node);

        PowerMockito.when(raiseDao.getListOfPaxOnActivity(anyLong(), anyLong(), anyBoolean(), anyListOf(String.class)))
            .thenReturn(recognitionRecipientsFromDAO);

        PowerMockito.when(paxMiscDao.findBy()).thenReturn(paxMiscFindBy);
        PowerMockito.when(paxMiscFindBy.where(anyString())).thenReturn(paxMiscFindBy);
        PowerMockito.when(paxMiscFindBy.and(anyString())).thenReturn(paxMiscFindBy);
        PowerMockito.when(paxMiscFindBy.eq(any())).thenReturn(paxMiscFindBy);
        PowerMockito.when(paxMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(ProjectConstants.TRUE);

        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(any())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(NewsfeedVisibility.PUBLIC.name());

        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(any())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.find()).thenReturn(getRecognitionListTest());
    }

    @Test
    public void test_createNewsfeedItemsForNomination_type_recognition() {

        NewsfeedItem createdNewsfeedItem = newsfeedService.createNewsfeedItemsForNomination(getNominationTest(),
                NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_type_awardcode() {

        NewsfeedItem createdNewsfeedItem = newsfeedService.createNewsfeedItemsForNomination(getNominationTest(),
                NewsfeedItemTypeCode.AWARD_CODE);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_nomination_and_recognition_status_pending() {

        // Set nomination status PENDING
        Nomination nomination = getNominationTest().setStatusTypeCode(StatusTypeCode.PENDING.name());

        // Set recognition status PENDING
        List<Recognition> recognitionList = getRecognitionListTest();
        for(Recognition recognition: recognitionList){
            recognition.setStatusTypeCode(StatusTypeCode.PENDING.name());
        }
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);

        // Test method.
        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(nomination, NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.PENDING.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_nomination_pending_and_at_least_one_recognition_approved() {

        // Set nomination status PENDING
        Nomination nomination = getNominationTest().setStatusTypeCode(StatusTypeCode.PENDING.name());

        // Set recognition status PENDING
        List<Recognition> recognitionList = getRecognitionListTest();
        for(Recognition recognition: recognitionList){
            recognition.setStatusTypeCode(StatusTypeCode.PENDING.name());
        }

        // Only one recognition is approved.
        recognitionList.get(0).setStatusTypeCode(StatusTypeCode.APPROVED.name());

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitionList);

        // Test method.
        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(nomination, NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_program_newsfeed_visibility_NONE() {

        // Set program newsfeed visibility NONE
        PowerMockito.when(programMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
            .thenReturn(NewsfeedVisibility.NONE.name());

        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(getNominationTest(), NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(NewsfeedVisibility.NONE.name(), createdNewsfeedItem.getNewsfeedVisibility());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_recipients_newsfeed_visible() {

        // Set program newsfeed visibility as recipient preferences.
        PowerMockito.when(programMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(NewsfeedVisibility.RECIPIENT_PREF.name());

        // Set pax misc data share rec as TRUE.
        PowerMockito.when(paxMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(ProjectConstants.TRUE);

        // Test method
        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(getNominationTest(), NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_recipient_newsfeed_visibility_false() {

        // Set program newsfeed visibility as recipient preferences.
        PowerMockito.when(programMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(NewsfeedVisibility.RECIPIENT_PREF.name());

        // Set pax misc data share rec as FALSE.
        PowerMockito.when(paxMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(ProjectConstants.FALSE);

        // Test method
        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(getNominationTest(), NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    @Test
    public void test_createNewsfeedItemsForNomination_only_one_recipient_newsfeed_visibility_true() {

        // Set program newsfeed visibility as recipient preferences.
        PowerMockito.when(programMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(NewsfeedVisibility.RECIPIENT_PREF.name());

        // Set pax misc data share rec as FALSE.
        PowerMockito.when(paxMiscFindBy.findOne(ProjectConstants.MISC_DATA, String.class))
                .thenReturn(ProjectConstants.TRUE, ProjectConstants.FALSE);

        // Test method
        NewsfeedItem createdNewsfeedItem =
                newsfeedService.createNewsfeedItemsForNomination(getNominationTest(), NewsfeedItemTypeCode.RECOGNITION);

        assertNotNull(createdNewsfeedItem);
        assertEquals(StatusTypeCode.APPROVED.name(), createdNewsfeedItem.getStatusTypeCode());

        Mockito.verify(newsfeedItemDao, times(1)).save(any(NewsfeedItem.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
    }

    private Nomination getNominationTest() {
        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setSubmitterPaxId(TestConstants.ID_2);
        nomination.setProgramId(3L);
        nomination.setStatusTypeCode(StatusTypeCode.APPROVED.name());

        return nomination;
    }

    private List<Recognition> getRecognitionListTest() {

        Recognition recognitionTest1 = new Recognition();
        recognitionTest1.setId(TestConstants.ID_1);
        recognitionTest1.setReceiverPaxId(10L);
        recognitionTest1.setStatusTypeCode(StatusTypeCode.APPROVED.name());

        Recognition recognitionTest2 = new Recognition();
        recognitionTest2.setId(TestConstants.ID_2);
        recognitionTest2.setReceiverPaxId(20L);
        recognitionTest2.setStatusTypeCode(StatusTypeCode.APPROVED.name());

        return Arrays.asList(recognitionTest1, recognitionTest2);
    }
}
