package com.maritz.culturenext.weeklydigest.dao;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Ignore;
import org.junit.Test;
import javax.inject.Inject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class WeeklyDigestDaoImplTest extends AbstractDatabaseTest {
	
    @Inject
    private BigQueryPointBalanceDao bigQueryPointBalanceDao;
    private static final String env = "UAT";
    
    @Ignore
    public void test_get_points_balances() throws Exception {
        List<String> products = new ArrayList<String>();
        List<String> subclients = new ArrayList<String>();
        
        products.add("EYD");
        products.add("FCP");
        products.add("5PP");
        products.add("PTS"); 
        products.add("VSR");
        products.add("CCR");
        subclients.add("4247.5");
        subclients.add("30908.2");
        subclients.add("123");
        subclients.add("1494.13");
        subclients.add("1234.1");        
        
        Map<String, String> balances = bigQueryPointBalanceDao.getPointBalances(env, products, subclients);
        assertFalse(balances.isEmpty()); //Should return results for points balances
    }
    
    @Ignore
    public void test_bigquery_connection() throws Exception {
    	String project_id = "data-lake-pre-production";
    	
    	//Path where credentials JSON file is located
    	String filePath = System.getProperty("user.home") + File.separator + "gcp" + File.separator + "application.json";
    			
    	//Get credentials from JSON file
    	GoogleCredentials credentials;
    	try (FileInputStream serviceAccountStream = new FileInputStream(filePath)){
    		credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);		
    	}
    			
    	//Get client that will be used to send requests from file credentials
    	BigQuery bigQuery = BigQueryOptions.newBuilder()
    	.setCredentials(credentials).setProjectId(project_id).build().getService();
 
    	assertNotNull(bigQuery);
    }
    
    @Ignore
    public void test_bigquery_results() throws Exception {
    	
        List<String> productList = new ArrayList<String>();
        List<String> subclientList = new ArrayList<String>();
        
        productList.add("5PP");
        subclientList.add("4247.5");   
        
        String[] products = productList.toArray(new String[productList.size()]); 
		String[] clients = subclientList.toArray(new String[subclientList.size()]);
    	
    	TableResult results = bigQueryPointBalanceDao.runQuery(env, products, clients);
    	
    	assertThat(results.getSchema().getFields().get(0).getName()).isEqualTo("ACCOUNT_ID");
    	assertThat(results.getSchema().getFields().get(1).getName()).isEqualTo("PARTICIPANTNBR");
    	assertThat(results.getSchema().getFields().get(2).getName()).isEqualTo("FIRST_NAME");
    	assertThat(results.getSchema().getFields().get(3).getName()).isEqualTo("LAST_NAME");
    	assertThat(results.getSchema().getFields().get(4).getName()).isEqualTo("POINT_BALANCE");
    	assertThat(results.getSchema().getFields().get(5).getName()).isEqualTo("DOLLAR_BALANCE");
    	assertThat(results.getSchema().getFields().get(6).getName()).isEqualTo("Client_Number");
    	assertThat(results.getSchema().getFields().get(7).getName()).isEqualTo("Product_Nbr");
    	assertThat(results.getSchema().getFields().get(8).getName()).isEqualTo("Client_Src_Nm");
    }
}