package com.maritz.culturenext.program.stats.services;

import com.google.common.collect.ImmutableMap;
import com.maritz.TestUtil;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;
import com.maritz.culturenext.program.stats.services.StatsService;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;
import com.maritz.test.AbstractDatabaseTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class StatsServiceTest extends AbstractDatabaseTest {

    // error codes
    private static final String ERROR_QUERYID_INVALID = "QUERYID_INVALID";
    private static final String ERROR_QUERYID_NOT_SUPPLIED = "QUERYID_NOT_SUPPLIED";
    private static final String ERROR_REQUESTOR_NOT_MANAGER = "REQUESTOR_NOT_MANAGER";

    // date ranges
    private static final String DEFAULT_FROM_DATE = "2015-01-01";
    private static final String DEFAULT_THRU_DATE = "2015-12-19";

    // program ids
    private static final Long NO_BUDGET_TEST_PROGRAM_ID = 1680L;
    private static final Long BUDGET_TEST_PROGRAM_ID = 1691L;
    private static final Long BUDGET_CRITERIA_TEST_PROGRAM_ID = 1987L;

    // pax ids
    private static final Long INVALID_PAX_ID = 9876543L;
    private static final Long NO_BUDGET_GIVER_PAX_ID = 4610L;
    private static final Long NO_BUDGET_RECEIVER_PAX_ID = 9355L;
    private static final Long BUDGET_GIVER_PAX_ID = 4489L;
    private static final Long BUDGET_RECEIVER_PAX_ID = 6577L;
    private static final Long BUDGET_CRITERIA_GIVER_PAX_ID = 11141L;
    private static final Long BUDGET_CRITERIA_RECEIVER_PAX_ID = 11144L;
    private static final Long MANAGER_PAX_ID = 9618L;
    private static final Long PARTICIPANT_1_PAX_ID = 9449L;
    private static final Long PARTICIPANT_2_PAX_ID = 9405L;

    // group ids
    private static final Long NO_BUDGET_GROUP_ID = 1728L;
    private static final Long BUDGET_GROUP_ID = 1729L;
    private static final Long BUDGET_CRITERIA_GROUP_ID = 1770L;

    // recognition criteria information
    private static final Map<Long, String> RECOGNITION_CRITERIA_ID_DISPLAY_NAME_MAP = 
            ImmutableMap.<Long, String>builder()
            .put(TestConstants.ID_1, "Achievement")
            .put(TestConstants.ID_2, "Client Focus")
            .build();

    @Inject private AuthenticationService authenticationService;
    @Inject private ReportsQueryService reportsQueryService;
    @Inject private StatsService statsService;

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
    }
    
    // PROGRAM STATS TESTS

    // validation

    @Test
    public void testGetProgramStatsMissingQuery() throws Exception {
        // set up expected errors and missing query
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_NOT_SUPPLIED));
        verifyGetProgramStatsErrors(null, expectedErrorCodes);
    }

    @Test
    public void testGetProgramStatsInvalidQuery() throws Exception {
        // set up expected errors and invalid
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_INVALID));
        verifyGetProgramStatsErrors("dsvasdSh78vnd7&dj", expectedErrorCodes);
    }

    // zero stats (from no programs, limited date range, etc.)

    @Test
    public void testGetProgramStatsNoProgramsQueried() throws Exception {
        // set up the query with inactive and invalid users
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(INVALID_PAX_ID));

        verifyGetProgramStatsValues(queryId, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsPastDateRange() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID,
                        BUDGET_RECEIVER_PAX_ID));

        verifyGetProgramStatsValues(queryId, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsInvalidProgram() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID,
                        BUDGET_RECEIVER_PAX_ID));

        Long invalidProgramId = 912343L;

        verifyGetProgramStatsValues(queryId, invalidProgramId, 0L, 0L, 0L, 0L);
    }

    // tests with multiple programs returned from query

    @Test
    public void testGetProgramStatsAllProgramsWithAllUsers() throws Exception {
        // set up the query to get programs with any users
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                new ArrayList<Long>());

        ProgramStatsDTO programStats = statsService.getProgramStats(queryId);
        assertNotNull(programStats.getPercentEligibleUsersGiving());
        assertNotNull(programStats.getPercentEligibleUsersReceiving());
        assertNotNull(programStats.getPointsGiven());
    }

    @Test
    public void testGetProgramStatsMultipleProgramsQueried() throws Exception {
        // set up the query for this test
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID, NO_BUDGET_GIVER_PAX_ID,
                        NO_BUDGET_RECEIVER_PAX_ID));

        ProgramStatsDTO programStats = statsService.getProgramStats(queryId);

        // assert that some results were given back
        assertTrue(programStats.getRecognitionsGiven() >= 0L);
        assertTrue(programStats.getRecognitionsReceived() >= 0L);
        assertTrue(programStats.getPointsGiven() >= 0L);
    }

    @Test
    public void testGetProgramStatsMultipleProgramQueriedWithMultiplePaxIdSets() throws Exception {
        // set up the query for this test
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID, NO_BUDGET_GIVER_PAX_ID,
                    NO_BUDGET_RECEIVER_PAX_ID));

        verifyGetProgramStatsValues(queryId, 4L, 14L, 60L, 60L);
    }

    @Test
    public void testGetProgramStatsMultipleProgramQueriedWithOneGroupId() throws Exception {
        // set up the query with one group id (should isolate to one program)
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(NO_BUDGET_GROUP_ID),
                new ArrayList<Long>());

        verifyGetProgramStatsValues(queryId, 0L, 6L, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsMultipleProgramQueriedWithMultipleGroupIds() throws Exception {
        // set up the query with multiple group ids (should choose 2 programs)
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(NO_BUDGET_GROUP_ID, BUDGET_GROUP_ID),
                new ArrayList<Long>());

        verifyGetProgramStatsValues(queryId, 4L, 14L, 60L, 60L);
    }

    // test with single program provided within url

    @Test
    public void testGetProgramStatsSingleProgramWithNonMemberPaxIdSet() throws Exception {
        // set up the query with pax ids that don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID));

        // (no budget program with budget receiver = 0 points giving, 0 points receiving)
        verifyGetProgramStatsValues(queryId, NO_BUDGET_TEST_PROGRAM_ID, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithOneMemberPaxId() throws Exception {
        // set up the query with pax ids that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID));

        // (budget program with budget giver = 60 points giving, 0 points receiving)
        verifyGetProgramStatsValues(queryId, BUDGET_TEST_PROGRAM_ID, 4L, 0L, 60L, 0L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithMemberPaxIdSet() throws Exception {
        // set up the query with pax ids that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID));

        // (budget program with budget giver and receiver = 60 points giving, 60 points receiving)
        verifyGetProgramStatsValues(queryId, BUDGET_TEST_PROGRAM_ID, 4L, 4L, 60L, 60L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithMultiplePaxIdSets() throws Exception {
        // set up the query with multiple pax ids that don't belong and belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID, NO_BUDGET_GIVER_PAX_ID, 
                        NO_BUDGET_RECEIVER_PAX_ID));

        // (budget program with budget giver and receiver = 60 points giving & 
        //60 points receiving as no budget giver/receiver is not factored)
        verifyGetProgramStatsValues(queryId, BUDGET_TEST_PROGRAM_ID, 4L, 4L, 60L, 60L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithOneNonMatchingGroupId() throws Exception {
        // set up the query with group id that resolve to members that don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(BUDGET_GROUP_ID),
                new ArrayList<Long>());

        // (no budget program with budget giver and receiver = 0 points giving & 0 points receiving)
        verifyGetProgramStatsValues(queryId, NO_BUDGET_TEST_PROGRAM_ID, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithOneMatchingGroupId() throws Exception {
        // set up the query with group id that resolve to members that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(NO_BUDGET_GROUP_ID),
                new ArrayList<Long>());

        // (no budget program with budget giver and receiver = 0 points giving & 0 points receiving)
        verifyGetProgramStatsValues(queryId, NO_BUDGET_TEST_PROGRAM_ID, TestConstants.ID_2, TestConstants.ID_2, 0L, 0L);
    }

    @Test
    public void testGetProgramStatsSingleProgramWithMultipleGroupIds() throws Exception {
        // set up the query with group id that resolve to members that belong and don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(NO_BUDGET_GROUP_ID, BUDGET_GROUP_ID),
                new ArrayList<Long>());

        // (no budget program with budget giver and receiver = 0 points giving & 0 points receiving)
        verifyGetProgramStatsValues(queryId, NO_BUDGET_TEST_PROGRAM_ID, TestConstants.ID_2, TestConstants.ID_2, 0L, 0L);
    }

    // RECOGNITION CRITERIA STATS TESTS

    // validation

    @Test
    public void testGetProgramRecognitionCriteriaStatsMissingQuery() throws Exception {
        // set up expected errors and missing query
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_NOT_SUPPLIED));
        verifyGetProgramRecognitionCriteriaStatsErrors(null, expectedErrorCodes);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsInvalidQuery() throws Exception {
        // set up expected errors and invalid
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_INVALID));
        verifyGetProgramRecognitionCriteriaStatsErrors("dsvasdSh78vnd7&dj", expectedErrorCodes);
    }

    // zero stats (from no programs, limited date range, etc.)

    @Test
    public void testGetProgramRecognitionCriteriaStatsNoProgramsQueried() throws Exception {
        // set up the query with inactive and invalid users
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(INVALID_PAX_ID));

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsPastDateRange() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID,
                        BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsInvalidProgram() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID, 
                        BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        Long invalidProgramId = 912343L;

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, invalidProgramId, expectedStats);
    }

    // tests with multiple programs returned from query

    @Test
    public void testGetProgramRecognitionCriteriaStatsAllProgramsWithAllUsers() throws Exception {
        // set up the query to get programs with any users
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                new ArrayList<Long>());

        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsService.getProgramRecognitionCriteriaStats(queryId);
        assertTrue(recognitionCriteriaStatsDTOs.size() > 2);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsMultipleProgramsQueried() throws Exception {
        // set up the query for this test
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID, NO_BUDGET_GIVER_PAX_ID,
                        NO_BUDGET_RECEIVER_PAX_ID));

        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsService.getProgramRecognitionCriteriaStats(queryId);
        assertTrue(recognitionCriteriaStatsDTOs.size() >= 2);
    }

    // test with single program provided within url

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleNonCriteriaProgramWithNonMemberPaxIdSet(
            ) throws Exception {
        // set up the query with pax ids that don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should get nothing back as program has no criteria and paxIds are not members)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleNonCriteriaProgramWithMemberPaxIdSet() throws Exception {
        // set up the query with pax ids that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithNonMemberPaxIdSet() throws Exception {
        // set up the query with pax ids that don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithOneMemberPaxId() throws Exception {
        // set up the query with pax ids that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_CRITERIA_RECEIVER_PAX_ID));

        // set up expected values (should get stats for 2 criterion)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                Arrays.asList(generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_1, 1),
                        generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_2, 1));

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithMemberPaxIdSet() throws Exception {
        // set up the query with pax ids that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID));

        // set up expected values (should get stats for 2 criterion)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                Arrays.asList(generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_1, 1),
                        generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_2, 1));

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithMultiplePaxIdSets() throws Exception {
        // set up the query with multiple pax ids that don't belong and belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID,
                        NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID,
                        BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID));

        // set up expected values (should get stats for 2 criterion -- 
        //similar to above test but with more users defined)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                Arrays.asList(generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_1, 1),
                        generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_2, 1));

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithOneNonMatchingGroupId() 
            throws Exception {
        // set up the query with group id that resolve to members that don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(BUDGET_GROUP_ID),
                new ArrayList<Long>());

        // set up expected values (should get nothing back)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                new ArrayList<ProgramRecognitionCriteriaStatsDTO>();

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithOneMatchingGroupId() throws Exception {
        // set up the query with group id that resolve to members that belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(BUDGET_CRITERIA_GROUP_ID),
                new ArrayList<Long>());

        // set up expected values (should get stats for 2 criterion as group has both paxIds of members)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                Arrays.asList(generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_1, 1),
                        generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_2, 1));

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSingleCriteriaProgramWithMultipleGroupIds() throws Exception {
        // set up the query with group id that resolve to members that belong and don't belong in given program
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                Arrays.asList(NO_BUDGET_GROUP_ID, BUDGET_GROUP_ID, BUDGET_CRITERIA_GROUP_ID),
                new ArrayList<Long>());

        // set up expected values (should get stats for 2 criterion --
        //similar to above test but with more groups defined)
        List<ProgramRecognitionCriteriaStatsDTO> expectedStats =
                Arrays.asList(generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_1, 1),
                        generatePredefinedRecognitionCriteriaDTO(TestConstants.ID_2, 1));

        verifyGetProgramRecognitionCriteriaStatsValues(queryId, BUDGET_CRITERIA_TEST_PROGRAM_ID, expectedStats);
    }

    // PARTICIPANT ACTIVITY STATS TESTS

    // validation

    @Test
    public void testGetActivityStatsMissingQuery() throws Exception {
        // set up expected errors and missing query
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_NOT_SUPPLIED));
        verifyGetActivityStatsErrors(null, expectedErrorCodes);
    }

    @Test
    public void testGetActivityStatsInvalidQuery() throws Exception {
        // set up expected errors and invalid
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_QUERYID_INVALID));
        verifyGetActivityStatsErrors("dsvasdSh78vnd7&dj", expectedErrorCodes);
    }

    @Test
    public void testGetActivityStatsNullPaxId() throws Exception{
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_REQUESTOR_NOT_MANAGER));
        verifyGetActivityStatsErrorsWithNullPaxId("dsvasdSh78vnd7&dj", expectedErrorCodes);
    }

    @Test
    public void testGetActivityStatsRequesterNotManager() throws Exception {
        // set up expected errors
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_REQUESTOR_NOT_MANAGER));

        // authenticate as manager
        authenticationService.authenticate(TestConstants.USER_BECKETDK);

        // set up the query for this test with non-managerial hierarchy
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(MANAGER_PAX_ID, PARTICIPANT_1_PAX_ID, PARTICIPANT_2_PAX_ID));

        verifyGetActivityStatsErrorsWithParticipant(queryId, BUDGET_RECEIVER_PAX_ID, expectedErrorCodes);
    }

    // zero stats (from no programs, limited date range, etc.)

    @Test
    public void testGetActivityStatsNoProgramsQueried() throws Exception {
        // set up the query with inactive and invalid users
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(INVALID_PAX_ID));

        // set up expected values (should be zero throughout)
        verifyGetActivityStatsValuesWithNoProjectId(queryId, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetActivityStatsPastDateRange() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID, 
                        BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should be zero throughout)
        verifyGetActivityStatsValuesWithNoProjectId(queryId, 0L, 0L, 0L, 0L);
    }

    @Test
    public void testGetActivityStatsInvalidProgram() throws Exception {
        // set up the query with dates in the past
        String queryId = generateQueryId("2014-06-01",
                "2014-07-01",
                new ArrayList<Long>(),
                Arrays.asList(NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID, BUDGET_GIVER_PAX_ID,
                        BUDGET_RECEIVER_PAX_ID));

        // set up expected values (should be zero throughout)
        verifyGetActivityStatsValuesWithNoProjectId(queryId, 0L, 0L, 0L, 0L);
    }

    // multiple programs queries

    @Test
    public void testGetActivityStatsMultipleProgramsQueried() throws Exception {
        // set up the query for this test
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID,
                        NO_BUDGET_GIVER_PAX_ID, NO_BUDGET_RECEIVER_PAX_ID,
                        BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID));

        ParticipantActivityStatsDTO activityStatsDTO = statsService.getActivityStats(queryId);

        // assert that some results were given back
        assertTrue(activityStatsDTO.getRecognitionsGiven() >= 0L);
        assertTrue(activityStatsDTO.getRecognitionsReceived() >= 0L);
        assertTrue(activityStatsDTO.getPointsGiven() >= 0L);
    }

    @Test
    public void testGetActivityStatsGiverParticipant() throws Exception {

        // authenticate as manager
        authenticationService.authenticate(TestConstants.USER_BECKETDK);

        // set up the query for this test with non-managerial hierarchy
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(MANAGER_PAX_ID, PARTICIPANT_1_PAX_ID, PARTICIPANT_2_PAX_ID));

        // verify activity stats (should only have 1 recognition given/received, 15 points given, 
        //and 0 points receiving as paxId is giver)
        verifyGetActivityStatsValuesWithParticipant(queryId, PARTICIPANT_1_PAX_ID, TestConstants.ID_1, 0L, 15L, 0L);
    }

    @Test
    public void testGetActivityStatsReceiverParticipant() throws Exception {

        // authenticate as manager
        authenticationService.authenticate(TestConstants.USER_BECKETDK);

        // set up the query for this test with non-managerial hierarchy
        String queryId = generateQueryId(DEFAULT_FROM_DATE,
                DEFAULT_THRU_DATE,
                new ArrayList<Long>(),
                Arrays.asList(MANAGER_PAX_ID, PARTICIPANT_1_PAX_ID, PARTICIPANT_2_PAX_ID));

        // verify activity stats (should only have 1 recognition given/received, 0 points given, 
        //and 15 points receiving as paxId is receiver)
        verifyGetActivityStatsValuesWithParticipant(queryId, PARTICIPANT_2_PAX_ID, 0L, TestConstants.ID_1, 0L, 15L);
    }

    // helper methods

    /**
     * Generates queryId with the specified query parameters.
     *
     * @param fromDate fromDate (start date) of query
     * @param thruDate thruDate (end date) of query
     * @param groupIds IDs of groups to be represented in query
     * @param paxIds IDs of paxes to be represented in query
     * @return id of query
     */
    @Nonnull
    private String generateQueryId(String fromDate,
                                         String thruDate,
                                         @Nonnull List<Long> groupIds,
                                         @Nonnull List<Long> paxIds) {
        // generate the report query dto with the given parameters
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();

        reportsQueryRequestDto.setFromDate(fromDate);
        reportsQueryRequestDto.setThruDate(thruDate);

        reportsQueryRequestDto.setMembers(generateMembers(groupIds, paxIds));
        reportsQueryRequestDto.setDirectReports(new ArrayList<Long>());
        reportsQueryRequestDto.setOrgReports(new ArrayList<Long>());

        try {
            QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);

            assertNotNull(queryIdDto);

            String queryId = queryIdDto.getQueryId();
            assertNotNull(queryId);
            return queryId;
        } catch(Exception e) {
            throw new RuntimeException("Failed to generate queryId for testing!!");
        }
    }

    /**
     * Generates stub DTOs.
     *
     * @param groupIds IDs of groups to be represented
     * @param paxIds IDs of paxes to be represented
     * @return list of stub DTOs with represented groups and paxes
     */
    private List<GroupMemberStubDTO> generateMembers(@Nonnull List<Long> groupIds, @Nonnull List<Long> paxIds) {
        List<GroupMemberStubDTO> members = new ArrayList<GroupMemberStubDTO>();

        for(Long groupId : groupIds) {
            members.add(new GroupMemberStubDTO().setGroupId(groupId));
        }

        for(Long paxId : paxIds) {
            members.add(new GroupMemberStubDTO().setPaxId(paxId));
        }

        return members;
    }

    private void verifyGetProgramStatsValues(@Nonnull String queryId,
                                             @Nonnull Long expectedRecognitionsGiven,
                                             @Nonnull Long expectedRecognitionsReceived,
                                             @Nonnull Long expectedPointsGiven,
                                             @Nonnull Long expectedPointsReceived) throws Exception {
        ProgramStatsDTO programStats = statsService.getProgramStats(queryId);
        verifyProgramStats(programStats, expectedRecognitionsGiven, expectedRecognitionsReceived, expectedPointsGiven,
                expectedPointsReceived);
    }

    private void verifyGetProgramStatsValues(@Nonnull String queryId,
                                             @Nonnull Long programId,
                                             @Nonnull Long expectedRecognitionsGiven,
                                             @Nonnull Long expectedRecognitionsReceived,
                                             @Nonnull Long expectedPointsGiven,
                                             @Nonnull Long expectedPointsReceived) throws Exception {
        ProgramStatsDTO programStats = statsService.getProgramStats(queryId, programId);
        verifyProgramStats(programStats, expectedRecognitionsGiven, expectedRecognitionsReceived, expectedPointsGiven,
                expectedPointsReceived);
    }

    private void verifyProgramStats(@Nonnull ProgramStatsDTO programStats,
                                    @Nonnull Long expectedRecognitionsGiven,
                                    @Nonnull Long expectedRecognitionsReceived,
                                    @Nonnull Long expectedPointsGiven,
                                    @Nonnull Long expectedPointsReceived) {
        assertEquals(expectedRecognitionsGiven, programStats.getRecognitionsGiven());
        assertEquals(expectedRecognitionsReceived, programStats.getRecognitionsReceived());
        assertEquals(expectedPointsGiven, programStats.getPointsGiven());
        assertEquals(expectedPointsReceived, programStats.getPointsReceived());
    }

    private void verifyGetProgramStatsErrors(String queryId, @Nonnull List<String> expectedErrorCodes)
            throws Exception {
        try {
            statsService.getProgramStats(queryId);
            fail("Call to get program stats should have failed!");
        } catch(ErrorMessageException e) {
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    private void verifyGetProgramRecognitionCriteriaStatsValues(@Nonnull String queryId,
            @Nonnull List<ProgramRecognitionCriteriaStatsDTO> expectedRecognitionCriteriaStats) throws Exception {
        List<ProgramRecognitionCriteriaStatsDTO> programRecognitionCriteriaStats =
                statsService.getProgramRecognitionCriteriaStats(queryId);
        verifyProgramRecognitionCriteriaStats(expectedRecognitionCriteriaStats, programRecognitionCriteriaStats);
    }

    private void verifyGetProgramRecognitionCriteriaStatsValues(@Nonnull String queryId,
            @Nonnull Long programId, 
            @Nonnull List<ProgramRecognitionCriteriaStatsDTO> expectedRecognitionCriteriaStats) throws Exception {
        List<ProgramRecognitionCriteriaStatsDTO> programRecognitionCriteriaStats =
                statsService.getProgramRecognitionCriteriaStats(queryId, programId);
        verifyProgramRecognitionCriteriaStats(expectedRecognitionCriteriaStats, programRecognitionCriteriaStats);
    }

    private void verifyProgramRecognitionCriteriaStats(
            @Nonnull List<ProgramRecognitionCriteriaStatsDTO> expectedRecognitionCriteriaStats,
            @Nonnull List<ProgramRecognitionCriteriaStatsDTO> actualProgramRecognitionCriteriaStats) {
        assertEquals(expectedRecognitionCriteriaStats.size(), actualProgramRecognitionCriteriaStats.size());

        int recognitionCriteriaCount = expectedRecognitionCriteriaStats.size();

        // sort the given lists
        Collections.sort(actualProgramRecognitionCriteriaStats, TestUtil.ProgramRecognitionCriteriaStatsDTOComparator);
        Collections.sort(expectedRecognitionCriteriaStats, TestUtil.ProgramRecognitionCriteriaStatsDTOComparator);

        for(int i = 0; i < recognitionCriteriaCount; i++) {
            ProgramRecognitionCriteriaStatsDTO actualDTO = actualProgramRecognitionCriteriaStats.get(i);
            ProgramRecognitionCriteriaStatsDTO expectedDTO = expectedRecognitionCriteriaStats.get(i);

            assertEquals(expectedDTO.getDisplayName(), actualDTO.getDisplayName());
            assertEquals(expectedDTO.getRecognitionCriteriaId(), actualDTO.getRecognitionCriteriaId());
            assertEquals(expectedDTO.getRecognitionsReceived(), actualDTO.getRecognitionsReceived());
        }
    }

    private ProgramRecognitionCriteriaStatsDTO generatePredefinedRecognitionCriteriaDTO(
            @Nonnull Long recognitionCriteriaId, @Nonnull Integer expectedRecognitionReceived) {
        if(!RECOGNITION_CRITERIA_ID_DISPLAY_NAME_MAP.containsKey(recognitionCriteriaId)) {
            fail("Recognition criteria information is not available for that particular ID!");
        }

        return generateRecognitionCriteriaDTO(recognitionCriteriaId,
                RECOGNITION_CRITERIA_ID_DISPLAY_NAME_MAP.get(recognitionCriteriaId),
                expectedRecognitionReceived);
    }

    private ProgramRecognitionCriteriaStatsDTO generateRecognitionCriteriaDTO(Long recognitionCriteriaId,
                                                                              String displayName,
                                                                              Integer recognitionReceived) {
        ProgramRecognitionCriteriaStatsDTO programRecognitionCriteriaStatsDTO =
                new ProgramRecognitionCriteriaStatsDTO();

        programRecognitionCriteriaStatsDTO.setRecognitionCriteriaId(recognitionCriteriaId);
        programRecognitionCriteriaStatsDTO.setDisplayName(displayName);
        programRecognitionCriteriaStatsDTO.setRecognitionsReceived(recognitionReceived);

        return programRecognitionCriteriaStatsDTO;
    }

    /**
     * Verifies errors for getting program recognition criteria stats.
     *
     * @param queryId ID of stored query to use
     * @param expectedErrorCodes expected error codes to be returned in response
     * @throws Exception 
     */
    private void verifyGetProgramRecognitionCriteriaStatsErrors(String queryId,
            @Nonnull List<String> expectedErrorCodes) throws Exception {
        try {
            statsService.getProgramRecognitionCriteriaStats(queryId);
            fail("Call to get recognition criteria stats should have failed!");
        } catch(ErrorMessageException e) {
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    private void verifyGetActivityStatsValuesWithNoProjectId(@Nonnull String queryId,
                                                             @Nonnull Long expectedRecognitionsGiven,
                                                             @Nonnull Long expectedRecognitionsReceived,
                                                             @Nonnull Long expectedPointsIssued,
                                                             @Nonnull Long expectedPointsReceived) throws Exception {
        verifyGetActivityStatsValues(queryId, 
                expectedRecognitionsGiven, expectedRecognitionsReceived, expectedPointsIssued, expectedPointsReceived);
    }

    private void verifyGetActivityStatsValues(@Nonnull String queryId,
                                              @Nonnull Long expectedRecognitionsGiven,
                                              @Nonnull Long expectedRecognitionsReceived,
                                              @Nonnull Long expectedPointsIssued,
                                              @Nonnull Long expectedPointsReceived) throws Exception {
        ParticipantActivityStatsDTO activityStatsDTO = statsService.getActivityStats(queryId);
        verifyActivityStats(activityStatsDTO, expectedRecognitionsGiven, expectedRecognitionsReceived,
                expectedPointsIssued, expectedPointsReceived);
    }

    private void verifyGetActivityStatsValuesWithParticipant(@Nonnull String queryId,
                                                             @Nonnull Long participantId,
                                                             @Nonnull Long expectedRecognitionsGiven,
                                                             @Nonnull Long expectedRecognitionsReceived,
                                                             @Nonnull Long expectedPointsIssued,
                                                             @Nonnull Long expectedPointsReceived) throws Exception {
        ParticipantActivityStatsDTO activityStatsDTO = statsService.getActivityStats(queryId, participantId);
        verifyActivityStats(activityStatsDTO, expectedRecognitionsGiven, expectedRecognitionsReceived,
                expectedPointsIssued, expectedPointsReceived);
    }

    private void verifyActivityStats(@Nonnull ParticipantActivityStatsDTO activityStatsDTO,
                                     @Nonnull Long expectedRecognitionsGiven,
                                     @Nonnull Long expectedRecognitionsReceived,
                                     @Nonnull Long expectedPointsIssued,
                                     @Nonnull Long expectedPointsReceived) {
        assertEquals(expectedRecognitionsGiven, activityStatsDTO.getRecognitionsGiven());
        assertEquals(expectedRecognitionsReceived, activityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsIssued, activityStatsDTO.getPointsGiven());
        assertEquals(expectedPointsReceived, activityStatsDTO.getPointsReceived());

        // verify that login count and engagement score are not null
        assertTrue(activityStatsDTO.getLoginCount() >= 0);
        assertTrue(activityStatsDTO.getPercentEngagementScore() >= 0);
    }

   
    /**
     * Verifies errors for getting activity stats.
     *
     * @param queryId ID of stored query to use
     * @param expectedErrorCodes expected error codes to be returned in response
     * @throws Exception 
     */
    private void verifyGetActivityStatsErrors(String queryId, @Nonnull List<String> expectedErrorCodes
            ) throws Exception {
        try {
            statsService.getActivityStats(queryId);
            fail("Call to get activity stats should have failed!");
        } catch(ErrorMessageException e) {
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    /**
     * @see StatsServiceTest#verifyGetActivityStatsErrorsWithParticipant(String, Long, Long, List)
     *

     */
  

    /**
     * Verifies errors for getting activity stats with participant.
     *
     * @param queryId ID of stored query to use
     * @param participantId ID of participant
     * @param expectedErrorCodes expected error codes to be returned in response
     * @throws Exception 
     */
    private void verifyGetActivityStatsErrorsWithParticipant(String queryId, Long participantId, 
            @Nonnull List<String> expectedErrorCodes) throws Exception {
        try {
            statsService.getActivityStats(queryId, participantId);
            fail("Call to get activity stats should have failed!");
        } catch(ErrorMessageException e) {
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    /**
     * Verifies errors for getting activity stats with null pax id.
     *
     * @param queryId ID of stored query to use
     * @param expectedErrorCodes expected error codes to be returned in response
     * @throws Exception
     */
    private void verifyGetActivityStatsErrorsWithNullPaxId(String queryId,
                                                            @Nonnull List<String> expectedErrorCodes) throws Exception {
        try{
            statsService.getActivityStats(queryId, null);
            fail("Call to get activity stats should have failed!");
        } catch (ErrorMessageException e) {
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }
}
