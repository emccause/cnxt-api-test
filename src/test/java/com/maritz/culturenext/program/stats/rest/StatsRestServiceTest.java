package com.maritz.culturenext.program.stats.rest;

import static com.maritz.TestUtil.generateFutureDate;
import static com.maritz.TestUtil.generateOldDate;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractRestTest;

public class StatsRestServiceTest extends AbstractRestTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private ReportsQueryService reportsQueryService;

    String queryId = "";

    // pax ids
    private static final Long BUDGET_GIVER_PAX_ID = 4489L;
    private static final Long BUDGET_RECEIVER_PAX_ID = 6577L;
    private static final Long BUDGET_CRITERIA_GIVER_PAX_ID = 11141L;
    private static final Long BUDGET_CRITERIA_RECEIVER_PAX_ID = 11144L;
    private static final Long PARTICIPANT_1_PAX_ID = 9449L;

    @Before
    public void setUp() throws Exception {
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        queryId = generateValidQuery();
    }

    @Test
    public void testGetProgramStats() throws Exception {
        mockMvc.perform(
                get("/rest/programs/stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
        .andExpect(status().isOk());
    }

    @Test
    public void testGetProgramStatsWithProgram() throws Exception {
        Long programId = 1691L;
        mockMvc.perform(
                get("/rest/programs/" + programId + "/stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProgramStats_fail_programId_as_string() throws Exception {
        String programId = "programIdString";
        mockMvc.perform(
                get("/rest/programs/" + programId + "/stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void testGetProgramRecognitionCriteriaStats() throws Exception {
        mockMvc.perform(
                get("/rest/programs/values-stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsWithProgram() throws Exception {
        Long programId = 1691L;
        mockMvc.perform(
                get("/rest/programs/" + programId + "/values-stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk());
    }
    
    @Test
    public void testGetProgramRecognitionCriteriaStats_fail_program_id_as_string() throws Exception {
        String programId = "programIdString";
        mockMvc.perform(
                get("/rest/programs/" + programId + "/values-stats?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetLoginsForQuery() throws Exception {
        mockMvc.perform(
                get("/rest/logins-by-query-id?queryId=" + queryId)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void testGetActivityStatsAllParticipantsWithProject() throws Exception {
        mockMvc.perform(
            get("/rest/participants/activity-stats?queryId=" + queryId)
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk());
    }

    @Test
    public void testGetActivityStatsWithProject() throws Exception {
        
        authenticationService.authenticate("becketdk");
        queryId = generateValidQuery();
        
        Long participantId = PARTICIPANT_1_PAX_ID;

        mockMvc.perform(
            get("/rest/participants/" + participantId + "/activity-stats?queryId=" + queryId)
                .with(authToken("becketdk"))
            )
            .andExpect(status().isOk());
    }

    @Test
    public void testGetActivityStatsWithProject_null_paxId() throws Exception {
        try {
            mockMvc.perform(
                    get("/rest/participants/a/activity-stats")
                            .with(authToken(TestConstants.USER_MUDDAM))
            )
                    .andExpect(status().isOk());
        } catch (Throwable e) {
            assert(!e.getMessage().isEmpty());
        }
    }

    // helper methods

    /**
     * Generates a queryId with valid parameters.
     *
     * @return id of valid query
     */
    private String generateValidQuery() {
        return generateQueryId(DateUtil.convertToUTCDate(generateOldDate()),
                DateUtil.convertToUTCDate(generateFutureDate()),
                new ArrayList<Long>(),
                Arrays.asList(BUDGET_GIVER_PAX_ID, BUDGET_RECEIVER_PAX_ID,BUDGET_CRITERIA_GIVER_PAX_ID,
                        BUDGET_CRITERIA_RECEIVER_PAX_ID));
    }

    /**
     * Generates queryId with the specified query parameters.
     *
     * @param fromDate fromDate (start date) of query
     * @param thruDate thruDate (end date) of query
     * @param groupIds IDs of groups to be represented in query
     * @param paxIds IDs of paxes to be represented in query
     * @return id of query
     */
    @Nonnull
    private String generateQueryId(String fromDate,
                                   String thruDate,
                                   @Nonnull List<Long> groupIds,
                                   @Nonnull List<Long> paxIds) {
        // generate the report query dto with the given parameters
        ReportsQueryRequestDto reportsQueryRequestDto = new ReportsQueryRequestDto();

        reportsQueryRequestDto.setFromDate(fromDate);
        reportsQueryRequestDto.setThruDate(thruDate);

        reportsQueryRequestDto.setMembers(generateMembers(groupIds, paxIds));
        reportsQueryRequestDto.setDirectReports(new ArrayList<Long>());
        reportsQueryRequestDto.setOrgReports(new ArrayList<Long>());

        try {
            QueryIdDto queryIdDto = reportsQueryService.getQueryId(reportsQueryRequestDto);

            assertNotNull(queryIdDto);

            String queryId = queryIdDto.getQueryId();
            assertNotNull(queryId);
            return queryId;
        } catch(Exception e) {
            throw new RuntimeException("Failed to generate queryId for testing!!");
        }
    }

    /**
     * Generates stub DTOs.
     *
     * @param groupIds IDs of groups to be represented
     * @param paxIds IDs of paxes to be represented
     * @return list of stub DTOs with represented groups and paxes
     */
    private List<GroupMemberStubDTO> generateMembers(@Nonnull List<Long> groupIds, @Nonnull List<Long> paxIds) {
        List<GroupMemberStubDTO> members = new ArrayList<GroupMemberStubDTO>();

        for(Long groupId : groupIds) {
            members.add(new GroupMemberStubDTO().setGroupId(groupId));
        }

        for(Long paxId : paxIds) {
            members.add(new GroupMemberStubDTO().setPaxId(paxId));
        }

        return members;
    }
}