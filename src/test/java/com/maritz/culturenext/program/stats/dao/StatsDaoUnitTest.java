package com.maritz.culturenext.program.stats.dao;

import static com.maritz.TestUtil.generateFutureDate;
import static com.maritz.TestUtil.generateOldDate;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.stats.dao.impl.StatsDaoImpl;
import com.maritz.test.AbstractMockTest;

public class StatsDaoUnitTest extends AbstractMockTest {

    @InjectMocks private StatsDaoImpl statsDao;
    @Mock private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    // program IDs for stat calculation
    private static final Long NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID = 1675L;
    private static final Long NO_GIVERS_STATS_PROGRAM_ID = 1678L;
    private static final Long NO_RECEIVERS_STATS_PROGRAM_ID = 1679L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID = 1680L;
    private static final Long GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID = 1691L;
    private static final Long BUDGET_CRITERIA_TEST_PROGRAM_ID = 1987L;
    private static final Long MANAGER_PARTICIPANT_PROGRAM_ID = 2695L;

    // pax IDs
    private static final Long NO_GIVERS_PAX_ID = 4487L;
    private static final Long NO_RECEIVERS_PAX_ID = 4422L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID = 4610L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID = 9355L;
    private static final Long GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID = 4489L;
    private static final Long GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID = 6577L;
    private static final Long BUDGET_CRITERIA_GIVER_PAX_ID = 11141L;
    private static final Long BUDGET_CRITERIA_RECEIVER_PAX_ID = 11144L;
    private static final Long TWO_LOGINS_PAX_ID = 13356L;

    @Test
    public void testGetProgramStatsNullNode(){
        thrown.expect(RuntimeException.class);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(SqlParameterSource.class), Matchers.<RowMapper<Map<String, Object>>>any())).thenReturn(null);

        statsDao.getProgramStats(Arrays.asList(24L),
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        fail();
    }

    @Test
    public void testGetAllLoginsNullNode(){
        thrown.expect(RuntimeException.class);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(SqlParameterSource.class), Matchers.<RowMapper<Map<String, Object>>>any())).thenReturn(null);
        List<Long> userIds = new ArrayList<>();
        userIds.add(TWO_LOGINS_PAX_ID);

        statsDao.getAllLogins(userIds, generateOldDate(), generateFutureDate());

        fail();
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsAllUsersNullNode() {
        thrown.expect(RuntimeException.class);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(SqlParameterSource.class), Matchers.<RowMapper<Map<String, Object>>>any())).thenReturn(null);
        List<Long> programIds = generateValidProgramList();

        statsDao.getProgramRecognitionCriteriaStatsAllUsers(programIds,
                        generateOldDate(), generateFutureDate());

        fail();
    }

    private List<Long> generateValidProgramList() {
        return new ArrayList<>(Arrays.asList(
                TestConstants.ID_1,
                NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID,
                NO_GIVERS_STATS_PROGRAM_ID,
                NO_RECEIVERS_STATS_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID,
                BUDGET_CRITERIA_TEST_PROGRAM_ID,
                MANAGER_PARTICIPANT_PROGRAM_ID
        ));
    }

    private List<Long> generateValidPaxList() {
        return new ArrayList<>(Arrays.asList(
                NO_GIVERS_PAX_ID,
                NO_RECEIVERS_PAX_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID,
                BUDGET_CRITERIA_GIVER_PAX_ID,
                BUDGET_CRITERIA_RECEIVER_PAX_ID
        ));
    }

}