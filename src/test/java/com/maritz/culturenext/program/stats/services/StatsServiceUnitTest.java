package com.maritz.culturenext.program.stats.services;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.stats.dao.StatsDao;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.services.impl.StatsServiceImpl;
import com.maritz.culturenext.recognition.services.EngagementScoreService;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheHandler;
import com.maritz.culturenext.reports.caching.ReportsQueryCacheObject;
import com.maritz.culturenext.util.DateUtil;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({LoggerFactory.class, LogFactory.class, DateUtil.class})
public class StatsServiceUnitTest {
    @InjectMocks private StatsServiceImpl statsService;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private EngagementScoreService engagementScoreService;
    @Mock private ReportsQueryCacheHandler reportsQueryCacheHandler;
    @Mock private Security security;
    @Mock private StatsDao statsDao;

    private ReportsQueryCacheObject mockedQueryCacheObject = PowerMockito
            .mock(ReportsQueryCacheObject.class);
    
    private FindBy<Relationship> relationshipTypeFindBy = PowerMockito.mock(FindBy.class);
    
    private static final String ERROR_REQUESTOR_NOT_MANAGER = "REQUESTOR_NOT_MANAGER";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        // Mock logger
        PowerMockito.mockStatic(LoggerFactory.class);
        Logger logger = Mockito.mock(Logger.class);
        PowerMockito.when(LoggerFactory.getLogger(any(Class.class)))
                .thenReturn(logger);

        PowerMockito.mockStatic(LogFactory.class);
        Log log = Mockito.mock(Log.class);
        PowerMockito.when(LogFactory.getLog(any(Class.class))).thenReturn(log);
    }

    @Test
    public void test_getActivityStats_manager_participant_input() {

        // Mock security service default
        PowerMockito.when(security.isMyPax(anyLong())).thenReturn(
                true); // logged user

        verifyGetActivityStatsByParticipant();
    }
    
    @Test
    public void test_getActivityStats_requestor_not_manager() {
        // set up expected errors
        List<String> expectedErrorCodes = new ArrayList<String>(Arrays.asList(ERROR_REQUESTOR_NOT_MANAGER));

        // Mock security service default
        PowerMockito.when(security.isMyPax(anyLong())).thenReturn(false); // no logged user

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1); 
        //Requestor not manager direct report.
        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.where(anyString())).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.eq(anyString())).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.end()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.and()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.find()).thenReturn(null);

        try{
             statsService.getActivityStats("QueryID", TestConstants.ID_1);
             fail();
        }catch(ErrorMessageException e){
            assertNotNull(e.getErrorMessages());

            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }catch(Exception ex){
            fail("Error not expected: "+ ex.getMessage());
        }
    }
    @Test
    public void test_getActivityStats_requestor_direct_report_manager() {
        //Mock relationshipDao result
        List<Relationship> mockedRelationshipsList = Arrays.asList(new Relationship());
        // Mock security service default
        PowerMockito.when(security.isMyPax(anyLong())).thenReturn(false); // no logged user

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1); 
        //Direct report participant
        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.where(anyString())).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.eq(anyString())).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.end()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.and()).thenReturn(relationshipTypeFindBy);
        PowerMockito.when(relationshipTypeFindBy.find()).thenReturn(mockedRelationshipsList);

        verifyGetActivityStatsByParticipant();
    }


    private void verifyGetActivityStatsByParticipant(){
        final Long PARTICIPANT_INPUT = 847L;
        final String QUERY_INPUT = "ODQ3OjE0NDM4MDkyODk0NzQ=";
        
        ParticipantActivityStatsDTO participantActivityStatsResult = new ParticipantActivityStatsDTO();
        participantActivityStatsResult.setLoginCount(TestConstants.ID_1);
        participantActivityStatsResult.setRecognitionsGiven(TestConstants.ID_1);
        participantActivityStatsResult.setRecognitionsReceived(TestConstants.ID_1);
        participantActivityStatsResult.setPercentEngagementScore(1);
        participantActivityStatsResult.setPointsGiven(TestConstants.ID_1);
        participantActivityStatsResult.setPointsReceived(TestConstants.ID_1);
        
        try {
        
            PowerMockito.when(
                    reportsQueryCacheHandler.getCacheObject(anyString()))
                    .thenReturn(mockedQueryCacheObject);
            PowerMockito.when(
                    reportsQueryCacheHandler.getProgramIdList(anyString()))
                    .thenReturn(new ArrayList<Long>());
            PowerMockito.when(mockedQueryCacheObject.getStartDate())
                    .thenReturn(new Date());
            PowerMockito.when(mockedQueryCacheObject.getEndDate()).thenReturn(
                    new Date());

            PowerMockito.when(
                    statsDao.getActivityStats(anyListOf(Long.class), anyListOf(Long.class),
                            any(Date.class), any(Date.class))).thenReturn(
                    participantActivityStatsResult);

            PowerMockito
                    .when(engagementScoreService
                            .bulkFetchEngagementScoreTotal(anyListOf(Long.class))).thenReturn(
                                    10);

            
            ParticipantActivityStatsDTO result = statsService.getActivityStats(
                    QUERY_INPUT, PARTICIPANT_INPUT);
            
            assertThat(10, equalTo(result.getPercentEngagementScore()));
        } catch (Exception e) {
            fail();
        }

    }

}
