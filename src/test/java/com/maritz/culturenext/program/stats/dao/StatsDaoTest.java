package com.maritz.culturenext.program.stats.dao;

import static com.maritz.TestUtil.generateFutureDate;
import static com.maritz.TestUtil.generateOldDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.stats.dao.StatsDao;
import com.maritz.culturenext.program.stats.dto.LoginCountDTO;
import com.maritz.culturenext.program.stats.dto.ParticipantActivityStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramRecognitionCriteriaStatsDTO;
import com.maritz.culturenext.program.stats.dto.ProgramStatsDTO;
import com.maritz.test.AbstractDatabaseTest;

public class StatsDaoTest extends AbstractDatabaseTest {

    // program IDs for stat calculation
    private static final Long NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID = 1675L;
    private static final Long NO_GIVERS_STATS_PROGRAM_ID = 1678L;
    private static final Long NO_RECEIVERS_STATS_PROGRAM_ID = 1679L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID = 1680L;
    private static final Long GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID = 1691L;
    private static final Long BUDGET_CRITERIA_TEST_PROGRAM_ID = 1987L;
    private static final Long MANAGER_PARTICIPANT_PROGRAM_ID = 2695L;

    // pax IDs
    private static final Long NO_GIVERS_PAX_ID = 4487L;
    private static final Long NO_RECEIVERS_PAX_ID = 4422L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID = 4610L;
    private static final Long GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID = 9355L;
    private static final Long GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID = 4489L;
    private static final Long GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID = 6577L;
    private static final Long BUDGET_CRITERIA_GIVER_PAX_ID = 11141L;
    private static final Long BUDGET_CRITERIA_RECEIVER_PAX_ID = 11144L;
    private static final Long ZERO_LOGINS_PAX_ID = 13355L;
    private static final Long TWO_LOGINS_PAX_ID = 13356L;
    private static final Long MANAGER_PAX_ID = 9618L;
    private static final Long PARTICIPANT_1_PAX_ID = 9449L;
    private static final Long PARTICIPANT_2_PAX_ID = 9405L;

    @Inject private StatsDao statsDao;

    // test program queries (with valid date range and members)

    @Test
    public void testGetProgramStatsAllUsers() {
        // get stats for all programs with same valid users
        ProgramStatsDTO programStatsDTO = 
                statsDao.getProgramStatsAllUsers(generateValidProgramList(), generateOldDate(), generateFutureDate());

        // just verify that mapping is done correctly (no null values)
        assertNotNull(programStatsDTO.getRecognitionsGiven());
        assertNotNull(programStatsDTO.getRecognitionsReceived());
        assertNotNull(programStatsDTO.getPercentEligibleUsersGiving());
        assertNotNull(programStatsDTO.getPercentEligibleUsersReceiving());
        assertNotNull(programStatsDTO.getPointsGiven());
    }

    @Test
    public void testGetProgramStatsAnyProgram() {
        // get stats for all programs
        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(Arrays.asList(24L),
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // just verify that mapping is done correctly (no null values)
        assertNotNull(programStatsDTO.getRecognitionsGiven());
        assertNotNull(programStatsDTO.getRecognitionsReceived());
        assertNotNull(programStatsDTO.getPercentEligibleUsersGiving());
        assertNotNull(programStatsDTO.getPercentEligibleUsersReceiving());
        assertNotNull(programStatsDTO.getPointsGiven());
    }

    @Test
    public void testGetProgramStatsNoPrograms() {
        // get stats with no programs and verify no recognitions/points
        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                new ArrayList<Long>(),
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received, points given, and percentages
        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    @Test
    public void testGetProgramStatsNoUsers() {
        // get stats with no programs and verify no recognitions/points
        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateValidProgramList(),
                new ArrayList<Long>(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received, points given, and percentages
        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    @Test
    public void testGetProgramStatsNoGiversNoReceiversMultiplePrograms() {
        // set up programs to get stats for and verify stats
        List<Long> programIds = Arrays.asList(
                NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID,
                NO_GIVERS_STATS_PROGRAM_ID,
                NO_RECEIVERS_STATS_PROGRAM_ID
        );

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                programIds,
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received, points given, and percentages
        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    @Test
    public void testGetProgramStatsNoGiversNoReceivers() {
        // set up programs to get stats for and verify stats
        List<Long> programIdsToQuery = Arrays.asList(
                NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID,
                NO_GIVERS_STATS_PROGRAM_ID,
                NO_RECEIVERS_STATS_PROGRAM_ID
        );

        for(Long programId : programIdsToQuery) {
            List<Long> programIdSearch = Arrays.asList(programId);

            ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                    programIdSearch,
                    generateValidPaxList(),
                    generateOldDate(),
                    generateFutureDate());

            // verify correct recognitions given/received, points given, and percentages
            verifyZeroProgramStatsRecognitions(programStatsDTO);
        }
    }

    @Test
    public void testGetProgramStatsGiversAndReceiversNoPoints() {
        // set up programs to get stats for and get stats
        List<Long> programIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                programIds,
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 0L;
        verifyRecognitionsAndPoints(programStatsDTO, TestConstants.ID_2,
                TestConstants.ID_2, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsGiversAndReceiversWithPoints() {
        // set up programs to get stats for and get stats
        List<Long> programIds = Arrays.asList(GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                programIds,
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given
        Long expectedRecognitionsGiven = 4L;
        Long expectedRecognitionsReceived = 4L;
        Long expectedPointsGiven = 60L;
        Long expectedPointsReceived = 60L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsMultiplePrograms() {
        // set up programs to get stats for and get stats
        List<Long> programIds = Arrays.asList(
                NO_GIVERS_STATS_PROGRAM_ID,
                NO_RECEIVERS_STATS_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                programIds,
                generateValidPaxList(),
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given
        Long expectedRecognitionsGiven = 6L;
        Long expectedRecognitionsReceived = 6L;
        Long expectedPointsGiven = 60L;
        Long expectedPointsReceived = 60L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }

    // test date range queries (with valid programs and members)

    @Test
    public void testGetProgramStatsNoRecognitionsInDateRange() {
        // set up date range to get stats for and get stats
        Date wayFutureFromDate = new Date(29348006400000L); // 1/1/2900 00:00:00 GMT
        Date wayFutureThruDate = new Date(29348010000000L); // 1/1/2900 01:00:00 GMT

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                generateValidPaxList(),
                wayFutureFromDate,
                wayFutureThruDate);

        // verify no recognitions in date range
        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    @Test
    public void testGetProgramStatsRecognitionsInDateRange(){
        // set up programs to get stats for and get stats
        List<Long> programIds = generateProgramIdListWithRecognitions();

        Date fromDate = new Date(1434240000000L); // 6/14/2015 00:00:00 GMT
        Date thruDate = new Date(1434412800000L); // 6/16/2015 00:00:00 GMT

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                programIds,
                generateValidPaxList(),
                fromDate,
                thruDate);

        // verify correct recognitions given/received and points given
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 0L;
        verifyRecognitionsAndPoints(programStatsDTO, TestConstants.ID_1,
                TestConstants.ID_1, expectedPointsGiven, expectedPointsReceived);
    }

    // test user queries (with valid date range and programs)

    @Test
    public void testGetProgramStatsNoRecognitionsWithNoUsers() {
        // get stats with no users and verify no recognitions
        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                new ArrayList<Long>(),
                generateOldDate(),
                generateFutureDate());

        // verify zero recognitions given/received and points
        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    @Test
    public void testGetProgramStatsNoBudgetGiverPaxIdOnly() {
        // set up users to get stats for and get stats
        List<Long> testUserIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID);

        for(Long userId : testUserIds) {
            List<Long> userIds = Arrays.asList(userId);

            ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                    generateProgramIdListWithRecognitions(),
                    userIds,
                    generateOldDate(),
                    generateFutureDate());

            // verify correct recognitions given/received and points given

            // (giver only so should only get stats for given)
            Long expectedRecognitionsReceived = 0L;

            // (no budget giver = 0 points, no receiver = 0 points)
            Long expectedPointsGiven = 0L;
            Long expectedPointsReceived = 0L;
            verifyRecognitionsAndPoints(programStatsDTO, TestConstants.ID_2,
                    expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
        }
    }

    @Test
    public void testGetProgramStatsBudgetGiverPaxIdOnly() {
        // set up users to get stats for and get stats
        List<Long> testUserIds = Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID);

        for(Long userId : testUserIds) {
            List<Long> userIds = Arrays.asList(userId);

            ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                    generateProgramIdListWithRecognitions(),
                    userIds,
                    generateOldDate(),
                    generateFutureDate());

            // verify correct recognitions given/received and points given

            // (giver only so should only get stats for given)
            Long expectedRecognitionsGiven = 4L;
            Long expectedRecognitionsReceived = 0L;

            // (budget giver = 60 points, no receiver = 0 points)
            Long expectedPointsGiven = 60L;
            Long expectedPointsReceived = 0L;
            verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                    expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
        }
    }

    @Test
    public void testGetProgramStatsNoBudgetReceiverOnlyPaxId() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given

        // (receiver only so should only get stats for received)
        Long expectedRecognitionsGiven = 0L;

        // (no giver = 0 points, no budget receiver = 0 points)
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 0L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                TestConstants.ID_2, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsBudgetReceiverOnlyPaxIds() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given

        // (receiver only so should only get stats for received)
        Long expectedRecognitionsGiven = 0L;
        Long expectedRecognitionsReceived = 4L;

        // (no giver = 0 points, budget receiver = 60 points)
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 60L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsReceiversOnlyPaxIds() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given

        // (receiver only so should only get combined stats for received respective to two receivers)
        Long expectedRecognitionsGiven = 0L;
        Long expectedRecognitionsReceived = 6L;

        // (no giver = 0 points, budget receiver + no budget receiver = 60 + 0 = 60 points)
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 60L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsGiversOnlyPaxIds() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received

        // (givers only so should only get stats for given respective to two givers)
        Long expectedRecognitionsGiven = 6L;
        Long expectedRecognitionsReceived = 0L;

        // (budget giver + no budget giver = 60 + 0 = 60 points, no receivers = 0 points)
        Long expectedPointsGiven = 60L;
        Long expectedPointsReceived = 0L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsMismatchedGiverAndReceiver() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID, 
                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received (as long as giver or receiver matches, 
        //should be able to get recognitions)

        // (budget giver = 4 recognitions given, no budget receiver = 2 recognitions received)
        Long expectedRecognitionsGiven = 4L;

        // verify correct points issued (budget giver = 60 points, no budget receiver = zero points)
        Long expectedPointsGiven = 60L;
        Long expectedPointsReceived = 0L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                TestConstants.ID_2, expectedPointsGiven, expectedPointsReceived);
    }

    @Test
    public void testGetProgramStatsValidGiverAndReceiver() {
        // set up users to get stats for and get stats
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID);

        ProgramStatsDTO programStatsDTO = statsDao.getProgramStats(
                generateProgramIdListWithRecognitions(),
                userIds,
                generateOldDate(),
                generateFutureDate());

        // verify correct recognitions given/received and points given
        Long expectedRecognitionsGiven = 4L;
        Long expectedRecognitionsReceived = 4L;

        // (budget giver = 60 points, budget receiver = 60 points)
        Long expectedPointsGiven = 60L;
        Long expectedPointsReceived = 60L;
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
    }


    @Test
    public void testGetProgramStatsAllUsersNoProgramIds() {
        ProgramStatsDTO programStatsDTO = statsDao.getProgramStatsAllUsers(
                new ArrayList<Long>(),
                generateOldDate(),
                generateFutureDate()
        );

        verifyZeroProgramStatsRecognitions(programStatsDTO);
    }

    // recognition criteria stats tests

    @Test
    public void testGetProgramRecognitionCriteriaStatsAllUsers() {
        List<Long> programIds = generateValidProgramList();

        // get stats for all programs with same valid users
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStatsAllUsers(programIds, 
                        generateOldDate(), generateFutureDate());

        // verify that there were some criterias returned
        assertFalse(recognitionCriteriaStatsDTOs.isEmpty());

        for(ProgramRecognitionCriteriaStatsDTO recognitionCriteriaStatsDTO : recognitionCriteriaStatsDTOs) {
            // just verify that mapping is done correctly (no null values)
            assertNotNull(recognitionCriteriaStatsDTO.getRecognitionCriteriaId());
            assertNotNull(recognitionCriteriaStatsDTO.getDisplayName());
            assertNotNull(recognitionCriteriaStatsDTO.getRecognitionsReceived());
        }
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsNoPrograms() {
        // set up programs and users
        List<Long> programIds = new ArrayList<Long>();
        List<Long> userIds = generateValidPaxIdListWithCriteria();

        // get stats for all programs with same valid users but no programs
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStats(programIds, userIds, 
                        generateOldDate(), generateFutureDate());

        // verify that no criterias were returned
        assertTrue(recognitionCriteriaStatsDTOs.isEmpty());
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsNoUsers() {
        // set up programs and users
        List<Long> programIds = Arrays.asList(BUDGET_CRITERIA_TEST_PROGRAM_ID);
        List<Long> userIds = new ArrayList<Long>();

        // get stats for all programs with same valid programs but no users
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStats(programIds, userIds, 
                        generateOldDate(), generateFutureDate());

        // verify that no criterias were returned
        assertTrue(recognitionCriteriaStatsDTOs.isEmpty());
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsNoCriteriaWithinDateRange() {
        // set up programs and users
        List<Long> programIds = Arrays.asList(BUDGET_CRITERIA_TEST_PROGRAM_ID);
        List<Long> userIds = Arrays.asList(BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID);

        Date wayFutureFromDate = new Date(29348006400000L); // 1/1/2900 00:00:00 GMT
        Date wayFutureThruDate = new Date(29348010000000L); // 1/1/2900 01:00:00 GMT

        // get stats for all programs with same valid users
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStats(programIds, userIds, wayFutureFromDate, wayFutureThruDate);

        // verify that no criterion were returned
        assertTrue(recognitionCriteriaStatsDTOs.isEmpty());
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsNoCriteriaWithUsers() {
        // set up programs and users
        List<Long> programIds = Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID);
        List<Long> userIds = Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID, 
                GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID, GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID);

        // get stats for all programs with same valid users
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStats(programIds, userIds, 
                        generateOldDate(), generateFutureDate());

        // verify that no criterion were returned
        assertTrue(recognitionCriteriaStatsDTOs.isEmpty());
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsSomeCriteria() {
        // set up programs and users
        List<Long> programIds = Arrays.asList(BUDGET_CRITERIA_TEST_PROGRAM_ID);
        List<Long> userIds = Arrays.asList(BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID);

        // get stats for all programs with same valid users
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStats(programIds, userIds, 
                        generateOldDate(), generateFutureDate());

        // verify that set up criterias were returned
        assertFalse(recognitionCriteriaStatsDTOs.isEmpty());
        assertEquals(2, recognitionCriteriaStatsDTOs.size());

        // verify 2 criterion, each with 1 recognition received
        for(ProgramRecognitionCriteriaStatsDTO recognitionCriteriaStatsDTO : recognitionCriteriaStatsDTOs) {
            assertNotNull(recognitionCriteriaStatsDTO.getDisplayName());
            assertNotNull(recognitionCriteriaStatsDTO.getRecognitionCriteriaId());
            assertNotNull(recognitionCriteriaStatsDTO.getRecognitionsReceived());
            assertEquals(Integer.valueOf(1), recognitionCriteriaStatsDTO.getRecognitionsReceived());
        }
    }

    @Test
    public void testGetProgramRecognitionCriteriaStatsAllUsersNoPrograms() {
        // set up programs and users
        List<Long> programIds = new ArrayList<Long>();

        // get stats for all users with no programs
        List<ProgramRecognitionCriteriaStatsDTO> recognitionCriteriaStatsDTOs =
                statsDao.getProgramRecognitionCriteriaStatsAllUsers(programIds, 
                        generateOldDate(), generateFutureDate());

        // verify that no criterias were returned
        assertTrue(recognitionCriteriaStatsDTOs.isEmpty());
    }

    // test login queries

    @Test
    public void testGetAllLoginsInvalidUser() {
        // set up invalid user
        List<Long> userIds = Arrays.asList(23493483939L);

        // get login count with valid users and verify zero logins
        LoginCountDTO loginCountDTO = statsDao.getAllLogins(userIds, generateOldDate(), generateFutureDate());

        Long expectedLoginCount = 0L;
        assertEquals(expectedLoginCount, loginCountDTO.getLoginCount());
    }

    @Test
    public void testGetAllLoginsZeroLogins() {
        // set up user (who hasn't logged in yet)
        List<Long> userIds = Arrays.asList(ZERO_LOGINS_PAX_ID);

        // get login count with valid users and verify zero logins
        LoginCountDTO loginCountDTO = statsDao.getAllLogins(userIds, generateOldDate(), generateFutureDate());

        Long expectedLoginCount = 0L;
        assertEquals(expectedLoginCount, loginCountDTO.getLoginCount());
    }

    @Test
    public void testGetAllLoginsTwoLogins() {
        // set up user (who has logged in twice)
        List<Long> userIds = Arrays.asList(TWO_LOGINS_PAX_ID);

        // get login count with valid users and verify zero logins
        LoginCountDTO loginCountDTO = statsDao.getAllLogins(userIds, generateOldDate(), generateFutureDate());

        assertEquals(TestConstants.ID_2, loginCountDTO.getLoginCount());
    }

    // activity stats tests

    @Test
    public void testGetActivityStatsInvalidPrograms() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(39847232090909L, 23423424324234L),
                        Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID, 
                                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID, 
                                GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID, GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID),
                        generateOldDate(),
                        generateFutureDate());

        // verify zeroed stats (except for engagement score)
        verifyZeroActivityStats(participantActivityStatsDTO);
    }

    @Test
    public void testGetActivityStatsInvalidUsers() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                        GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID),
                        Arrays.asList(234234923747299795L, 23948239847498237L),
                        generateOldDate(),
                        generateFutureDate());

        // verify zeroed stats (except for engagement score)
        verifyZeroActivityStats(participantActivityStatsDTO);
    }

    @Test
    public void testGetActivityStatsFutureDateRange() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID, 
                        GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID), 
                        Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID, 
                                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID),
                        new Date(20684926398000L), // 6/24/2625  20:53:18
                        new Date(20684929998000L)); // 6/24/2625 21:53:18

        // verify zeroed stats (except for engagement score)
        verifyZeroActivityStats(participantActivityStatsDTO);
    }

    @Test
    public void testGetActivityStatsNoBudgetActivityStats() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID, 
                        GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID),
                        Arrays.asList(GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID, 
                                GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID),
                        generateOldDate(),
                        generateFutureDate());

        Long expectedPointsReceived = 0L;
        Long expectedPointsIssued = 0L;

        // should match up with recognitions and points given/received
        assertEquals(TestConstants.ID_2, participantActivityStatsDTO.getRecognitionsGiven());
        assertEquals(TestConstants.ID_2, participantActivityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsReceived, participantActivityStatsDTO.getPointsReceived());
        assertEquals(expectedPointsIssued, participantActivityStatsDTO.getPointsGiven());

        assertNull(participantActivityStatsDTO.getPercentEngagementScore());
    }

    @Test
    public void testGetActivityStatsBudgetActivityStats() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID),
                        Arrays.asList(GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID,
                                GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID),
                        generateOldDate(),
                        generateFutureDate());

        Long expectedRecognitionsGiven = 4L;
        Long expectedRecognitionsReceived = 4L;
        Long expectedPointsReceived = 60L;
        Long expectedPointsIssued = 60L;

        // should match up with recognitions and points given/received
        assertEquals(expectedRecognitionsGiven, participantActivityStatsDTO.getRecognitionsGiven());
        assertEquals(expectedRecognitionsReceived, participantActivityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsReceived, participantActivityStatsDTO.getPointsReceived());
        assertEquals(expectedPointsIssued, participantActivityStatsDTO.getPointsGiven());

        assertNull(participantActivityStatsDTO.getPercentEngagementScore());
    }

    @Test
    public void testGetActivityStatsBudgetRecognitionCriteriaActivityStats() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(BUDGET_CRITERIA_TEST_PROGRAM_ID),
                        Arrays.asList(BUDGET_CRITERIA_GIVER_PAX_ID, BUDGET_CRITERIA_RECEIVER_PAX_ID),
                        generateOldDate(),
                        generateFutureDate());

        Long expectedPointsReceived = 11L;
        Long expectedPointsIssued = 11L;

        // should match up with recognitions and points given/received
        assertEquals(TestConstants.ID_2, participantActivityStatsDTO.getRecognitionsGiven());
        assertEquals(TestConstants.ID_2, participantActivityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsReceived, participantActivityStatsDTO.getPointsReceived());
        assertEquals(expectedPointsIssued, participantActivityStatsDTO.getPointsGiven());

        assertNull(participantActivityStatsDTO.getPercentEngagementScore());
    }

    @Test
    public void testGetActivityStatsManagerParticipantActivityStats() {
        ParticipantActivityStatsDTO participantActivityStatsDTO =
                statsDao.getActivityStats(Arrays.asList(MANAGER_PARTICIPANT_PROGRAM_ID),
                        Arrays.asList(MANAGER_PAX_ID, PARTICIPANT_1_PAX_ID, PARTICIPANT_2_PAX_ID),
                        generateOldDate(),
                        generateFutureDate());

        Long expectedPointsReceived = 15L;
        Long expectedPointsIssued = 15L;

        // should match up with recognitions and points given/received
        assertEquals(TestConstants.ID_1, participantActivityStatsDTO.getRecognitionsGiven());
        assertEquals(TestConstants.ID_1, participantActivityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsReceived, participantActivityStatsDTO.getPointsReceived());
        assertEquals(expectedPointsIssued, participantActivityStatsDTO.getPointsGiven());

        assertNull(participantActivityStatsDTO.getPercentEngagementScore());
    }

    // helper methods

    private void verifyZeroActivityStats(ParticipantActivityStatsDTO participantActivityStatsDTO) {
        assertNotNull(participantActivityStatsDTO);

        Long expectedZeroStats = 0L;

        assertEquals(expectedZeroStats, participantActivityStatsDTO.getRecognitionsGiven());
        assertEquals(expectedZeroStats, participantActivityStatsDTO.getRecognitionsReceived());
        assertEquals(expectedZeroStats, participantActivityStatsDTO.getPointsReceived());
        assertEquals(expectedZeroStats, participantActivityStatsDTO.getPointsGiven());

        assertNull(participantActivityStatsDTO.getPercentEngagementScore());
    }

    // helper methods

    /**
     * Verifies the given stats to match the expected recognitions given/received and points issued.
     *
     * @param programStatsDTO stats to verify
     * @param expectedRecognitionsGiven expected recognitions given
     * @param expectedRecognitionsReceived expected recognitions received
     * @param expectedPointsGiven expected points given
     * @param expectedPointsReceived expected points received
     */
    private void verifyRecognitionsAndPoints(@Nonnull ProgramStatsDTO programStatsDTO,
                                             @Nonnull Long expectedRecognitionsGiven,
                                             @Nonnull Long expectedRecognitionsReceived,
                                             @Nonnull Long expectedPointsGiven,
                                             @Nonnull Long expectedPointsReceived) {
        // verify expected values (recognition given/received and points issued)
        assertEquals(expectedRecognitionsGiven, programStatsDTO.getRecognitionsGiven());
        assertEquals(expectedRecognitionsReceived, programStatsDTO.getRecognitionsReceived());
        assertEquals(expectedPointsGiven, programStatsDTO.getPointsGiven());
        assertEquals(expectedPointsReceived, programStatsDTO.getPointsReceived());

        // verify that percentages are at least defined
        assertNotNull(programStatsDTO.getPercentEligibleUsersGiving());
        assertNotNull(programStatsDTO.getPercentEligibleUsersReceiving());
    }

    /**
     * Verifies program stats for zero recognitions.
     *
     * @param programStatsDTO stats to verify
     */
    private void verifyZeroProgramStatsRecognitions(@Nonnull ProgramStatsDTO programStatsDTO) {
        Long expectedRecognitionsGiven = 0L;
        Long expectedRecognitionsReceived = 0L;
        Long expectedPointsGiven = 0L;
        Long expectedPointsReceived = 0L;
        BigDecimal expectedPercentage = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);

        // verify zero recognitions, points, and user percentage
        verifyRecognitionsAndPoints(programStatsDTO, expectedRecognitionsGiven,
                expectedRecognitionsReceived, expectedPointsGiven, expectedPointsReceived);
        assertEquals(expectedPercentage, programStatsDTO.getPercentEligibleUsersGiving());
        assertEquals(expectedPercentage, programStatsDTO.getPercentEligibleUsersReceiving());
    }

    /**
     * Generates a list with some valid paxIds.
     *
     * @return a list with some valid paxIds
     */
    private List<Long> generateValidPaxList() {
        return new ArrayList<Long>(Arrays.asList(
            NO_GIVERS_PAX_ID,
            NO_RECEIVERS_PAX_ID,
            GIVERS_AND_RECEIVERS_NO_BUDGET_GIVER_PAX_ID,
            GIVERS_AND_RECEIVERS_NO_BUDGET_RECEIVER_PAX_ID,
            GIVERS_AND_RECEIVERS_BUDGET_GIVER_PAX_ID,
            GIVERS_AND_RECEIVERS_BUDGET_RECEIVER_PAX_ID,
            BUDGET_CRITERIA_GIVER_PAX_ID,
            BUDGET_CRITERIA_RECEIVER_PAX_ID
        ));
    }

    /**
     * Generates a list with some valid programIds.
     *
     * @return a list with some valid programIds
     */
    private List<Long> generateValidProgramList() {
        return new ArrayList<Long>(Arrays.asList(
                TestConstants.ID_1,
                NO_GIVERS_NOR_RECEIVERS_STATS_PROGRAM_ID,
                NO_GIVERS_STATS_PROGRAM_ID,
                NO_RECEIVERS_STATS_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID,
                BUDGET_CRITERIA_TEST_PROGRAM_ID,
                MANAGER_PARTICIPANT_PROGRAM_ID
        ));
    }

    /**
     * Generates a list with some program activity IDs associated with some recognitions.
     *
     * @return a list with some program activity IDs associated with some recognitions
     */
    private List<Long> generateProgramIdListWithRecognitions() {
        return new ArrayList<Long>(Arrays.asList(
                GIVERS_AND_RECEIVERS_NO_BUDGET_PROGRAM_ID,
                GIVERS_AND_RECEIVERS_WITH_BUDGET_PROGRAM_ID));
    }

    /**
     * Generates a list of valid paxIds associated with criteria.
     *
     * @return a list of valid paxIds associated with criteria
     */
    private List<Long> generateValidPaxIdListWithCriteria() {
        return new ArrayList<Long>(Arrays.asList(
                BUDGET_CRITERIA_GIVER_PAX_ID,
                BUDGET_CRITERIA_RECEIVER_PAX_ID
        ));
    }



}