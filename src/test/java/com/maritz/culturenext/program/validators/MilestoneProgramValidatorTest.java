package com.maritz.culturenext.program.validators;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.budget.service.BudgetService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.*;
import com.maritz.test.AbstractMockTest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import com.maritz.culturenext.program.dao.EligibilityDao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;

public class MilestoneProgramValidatorTest extends AbstractMockTest {

    private MilestoneProgramValidator milestoneProgramValidator;
    @Mock private BudgetService budgetService;
    @Mock private ConcentrixDao<Budget> budgetDao;
    @Mock private EligibilityDao eligibilityDao;
    @Mock private PaxRepository paxRepository;

    private ProgramRequestDTO programRequestDTO;
    private List<ErrorMessage> errors;

    @Before
    public void setup() {
        milestoneProgramValidator = new MilestoneProgramValidator()
                .setBudgetService(budgetService)
                .setPaxRepository(paxRepository);
        milestoneProgramValidator
            .setBudgetDao(budgetDao)
            .setEligibilityDao(eligibilityDao);
        
        Mockito.when(budgetService.getBudgetById(anyLong())).thenReturn(new BudgetDTO().setType("SIMPLE"));
        Mockito.when(paxRepository.findPaxIdByControlNum(Matchers.anyString())).thenReturn(1L);
        Mockito.when(budgetDao.findById(anyLong())).thenReturn(new Budget().setId(1L).setStatusTypeCode("ACTIVE"));
        programRequestDTO = new ProgramRequestDTO();
    }

    @Test
    public void test_happy_path() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        List<BudgetAssignmentStubDTO> budgetAssignments = new ArrayList<>();
        BudgetEntityStubDTO budgetEntity = new BudgetEntityStubDTO();
        budgetEntity.setBudgetId(1L);
        budgetAssignment.setBudget(budgetEntity);
        budgetAssignments.add(budgetAssignment);
        programRequestDTO.setBudgetAssignments(budgetAssignments);
        validateNoErrorOccurred();
    }

    @Test
    public void test_validate_program_award_tiers_not_allowed() {
        List<AwardTierDTO> awardTiers = new ArrayList<>();
        AwardTierDTO awardTier = new AwardTierDTO();
        awardTier.setAllowRaising(true);
        awardTiers.add(awardTier);
        programRequestDTO.setAwardTiers(awardTiers);
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        validateErrorException(ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED);
    }

    @Test
    public void test_validate_program_giving_eligibility_good() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(Arrays.asList(1L), null, Arrays.asList(3L, 4L), null));
        validateNoErrorOccurred();
    }

    @Test
    public void test_validate_program_giving_eligibility_null() {
        programRequestDTO.setEligibility(null);
        validateErrorException(ProgramConstants.ERROR_MISSING_ELIGIBILITY_CODE);
    }

    @Test
    public void test_validate_program_giving_eligibility_not_allowed() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(Arrays.asList(2L), null, Arrays.asList(3L, 4L), null));
        validateErrorException(ProgramActivityConstants.ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED);
    }

    @Test
    public void test_validate_program_giving_eligibility_not_allowed_multiple() {
        // can't have admin + anyone else
        programRequestDTO.setEligibility(generateEligibilityStubDTO(Arrays.asList(1L, 2L), null, Arrays.asList(3L, 4L), null));
        validateErrorException(ProgramActivityConstants.ERROR_GIVING_ELIGIBILITY_NOT_ALLOWED);
    }

    @Test
    public void test_validate_program_receiving_eligibility_required() {
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramActivityConstants.ERROR_RECEIVING_ELIGIBILITY_REQUIRED);
    }

    @Test
    public void test_validate_program_budget_not_simple() {
        BudgetDTO budgetDTO = new BudgetDTO();
        budgetDTO.setType("DISTRIBUTED");
        Mockito.when(budgetService.getBudgetById(anyLong())).thenReturn(budgetDTO);
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        List<BudgetAssignmentStubDTO> budgetAssignments = new ArrayList<>();
        BudgetEntityStubDTO budgetEntity = new BudgetEntityStubDTO();
        budgetEntity.setBudgetId(1L);
        budgetAssignment.setBudget(budgetEntity);
        budgetAssignments.add(budgetAssignment);
        programRequestDTO.setBudgetAssignments(budgetAssignments);
        validateErrorException(ProgramConstants.ERROR_PROGRAM_NON_SIMPLE_BUDGET);
    }

    @Test
    public void test_validate_programs_multiple_budgets() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        List<BudgetAssignmentStubDTO> budgetAssignments = new ArrayList<>();
        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(5L);
        budgetAssignment.setBudget(budget);
        budgetAssignments.add(budgetAssignment);
        budget.setBudgetId(6L);
        budgetAssignment.setBudget(budget);
        budgetAssignments.add(budgetAssignment);
        programRequestDTO.setBudgetAssignments(budgetAssignments);
        validateErrorException(ProgramConstants.ERROR_PROGRAM_MORE_THAN_ONE_BUDGET);
    }

    @Test
    public void test_validate_programs_budget_giving_eligibility_good() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(1L);
        budgetAssignment.setBudget(budget);
        budgetAssignment.setGivingMembers(Arrays.asList(new BudgetEntityStubDTO().setPaxId(1L)));
        programRequestDTO.setBudgetAssignments(Arrays.asList(budgetAssignment));
        validateNoErrorOccurred();
    }

    @Test
    public void test_validate_programs_budget_giving_eligibility_giving_not_allowed() {
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(5L);
        budgetAssignment.setBudget(budget);
        budgetAssignment.setGivingMembers(Arrays.asList(new BudgetEntityStubDTO().setPaxId(2L)));
        programRequestDTO.setBudgetAssignments(Arrays.asList(budgetAssignment));
        validateErrorException(ProgramActivityConstants.BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED.getCode());
    }

    @Test
    public void test_validate_programs_budget_giving_eligibility_giving_not_allowed_multiple() {
        // can't have admin + anyone else
        programRequestDTO.setEligibility(generateEligibilityStubDTO(null, null, Arrays.asList(1L, 2L), null));
        BudgetAssignmentStubDTO budgetAssignment = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(5L);
        budgetAssignment.setBudget(budget);
        budgetAssignment.setGivingMembers(
                Arrays.asList(
                        new BudgetEntityStubDTO().setPaxId(1L),
                        new BudgetEntityStubDTO().setPaxId(2L)
                    )
            );
        programRequestDTO.setBudgetAssignments(Arrays.asList(budgetAssignment));
        validateErrorException(ProgramActivityConstants.BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED.getCode());
    }

    private void validateErrorException(String errorCode) {
        try {
            errors = milestoneProgramValidator.validateProgram(programRequestDTO, ProjectConstants.POST, TestConstants.ID_1);
            ErrorMessageException.throwIfHasErrors(errors);
            fail("no error caught!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }
    }

    private void validateNoErrorOccurred() {
        try {
            errors = milestoneProgramValidator.validateProgram(programRequestDTO, ProjectConstants.POST, TestConstants.ID_1);
            ErrorMessageException.throwIfHasErrors(errors);
        } catch (ErrorMessageException errorMsgException) {
            fail("caught error: " + errorMsgException.getMessage());
        }
    }

    /**
     * Generates a valid eligibility stub DTO with no ids for a ProgramRequest.
     *
     * @return a valid eligibility stub DTO with no ids
     */
    private EligibilityStubDTO generateEmptyEligibilityStubDTO() {
        return generateEligibilityStubDTO(new ArrayList<Long>(), new ArrayList<Long>(), new ArrayList<Long>(),
                new ArrayList<Long>());
    }

    /**
     * Generates the eligibility stub DTO with the specified paxIds and
     * groupIds.
     *
     * @param givePaxIds
     *            paxIds identified for giving eligibility
     * @param giveGroupIds
     *            groupIds identified for receiving eligibility
     * @param receivePaxIds
     *            paxIds identified for giving eligibility
     * @param receiveGroupIds
     *            groupIds identified for receiving eligibility
     * @return eligibility stub DTO with the specified paxIds and groupIds
     */
    private EligibilityStubDTO generateEligibilityStubDTO(List<Long> givePaxIds, List<Long> giveGroupIds,
                                                          List<Long> receivePaxIds, List<Long> receiveGroupIds) {
        EligibilityStubDTO eligibilityStubDTO = new EligibilityStubDTO();

        // generate list of eligibility entity stub DTOs for giving and receiving eligibility
        eligibilityStubDTO.setGive(generateEligibilityEntityStubDTOs(givePaxIds, giveGroupIds));
        eligibilityStubDTO.setReceive(generateEligibilityEntityStubDTOs(receivePaxIds, receiveGroupIds));

        return eligibilityStubDTO;
    }

    /**
     * Generates a list of eligibility entity stub DTOs with the specified
     * paxIds and/or groupId.
     *
     * @param paxIds
     *            paxIds to be included
     * @param groupIds
     *            groupIds to be included
     * @return a list of eligibility entity stub DTOs with the specified paxIds
     *         and/or groupIds
     */
    private List<EligibilityEntityStubDTO> generateEligibilityEntityStubDTOs(List<Long> paxIds, List<Long> groupIds) {
        List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs = new ArrayList<>();

        // add paxIds within the DTO
        if (paxIds != null) {
            for (Long paxId : paxIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setPaxId(paxId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        // add groupIds within the DTO
        if (groupIds != null) {
            for (Long groupId : groupIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setGroupId(groupId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        return eligibilityEntityStubDTOs;
    }
}