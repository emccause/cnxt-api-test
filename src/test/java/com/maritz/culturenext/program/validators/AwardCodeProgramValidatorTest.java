package com.maritz.culturenext.program.validators;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.EligibilityDao;
import com.maritz.culturenext.program.dto.AwardCodeProgramDetailsDTO;
import com.maritz.culturenext.program.dto.Certificates;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.validators.AwardCodeProgramValidator;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.test.AbstractMockTest;

public class AwardCodeProgramValidatorTest extends AbstractMockTest {
    
    private AwardCodeProgramValidator awardCodeProgramValidator;
    @Mock private ConcentrixDao<Files> filesDao;
    @Mock private EligibilityDao eligibilityDao;
    @Mock private FindBy<Files> filesFindBy;
    
    private AwardCodeProgramDetailsDTO awardCodeDetailsDTO;
    private ProgramRequestDTO programRequestDTO;
    private List<ErrorMessage> errors;
    
    private static final String TEST_CLAIMING_INSTRUCTIONS_MISC_DATA = "Claiming instructions text";
    private static final String TEST_FILE_NAME_1 = "test_file_1";
    private static final String TEST_FILE_NAME_2 = "test_file_2";
    private static final String TEST_FILE_NAME_3 = "test_file_3";
    
    @Before
    public void setup() {
        awardCodeProgramValidator = new AwardCodeProgramValidator()
                .setFilesDao(filesDao);
        awardCodeProgramValidator.setEligibilityDao(eligibilityDao);
        
        List<Certificates> certificatesList = new ArrayList<>();
        Certificates certificate_1 = new Certificates();
        certificate_1.setId(TestConstants.ID_1);
        certificate_1.setFileName(TEST_FILE_NAME_1);
        certificatesList.add(certificate_1);
        
        Certificates certificate_2 = new Certificates();
        certificate_2.setId(TestConstants.ID_2);
        certificate_2.setFileName(TEST_FILE_NAME_2);
        certificatesList.add(certificate_2);
        
        Certificates certificate_3 = new Certificates();
        certificate_3.setId(3L);
        certificate_3.setFileName(TEST_FILE_NAME_3);
        certificatesList.add(certificate_3);
        
        awardCodeDetailsDTO = new AwardCodeProgramDetailsDTO();
        awardCodeDetailsDTO.setCertificates(certificatesList);
        awardCodeDetailsDTO.setIconId(TestConstants.ID_1);
        awardCodeDetailsDTO.setClaimingInstructions(TEST_CLAIMING_INSTRUCTIONS_MISC_DATA);
        awardCodeDetailsDTO.setDownloadable(Boolean.TRUE);
        awardCodeDetailsDTO.setPrintable(Boolean.TRUE);
        
        programRequestDTO = new ProgramRequestDTO();
        programRequestDTO.setAwardCode(awardCodeDetailsDTO);
        programRequestDTO.setNotification(Arrays.asList(
                NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        
        PowerMockito.when(filesDao.findBy()).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.where(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.eq(any())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.and(anyString())).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.in(anyListOf(Long.class))).thenReturn(filesFindBy);
        PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1,TestConstants.ID_2, 3L));
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);
    }
    
    @Test
    public void test_validate_program_error_claiming_instructions_missing() {
        awardCodeDetailsDTO.setClaimingInstructions(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_MISSING_CLAIMING_INSTRUCTIONS_CODE);
    }
    
    @Test
    public void test_validate_program_success_inactive_claiming_instructions_missing() {
        awardCodeDetailsDTO.setClaimingInstructions(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setStatus(StatusTypeCode.INACTIVE.name());
        validateNoErrorOccurred();
    }
    
    @Test
    public void test_validate_program_error_iconId_missing() {
        awardCodeDetailsDTO.setIconId(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_MISSING_ICON_ID_CODE);
    }
    
    @Test
    public void test_validate_program_success_inactive_iconId_missing() {
        awardCodeDetailsDTO.setIconId(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setStatus(StatusTypeCode.INACTIVE.name());
        validateNoErrorOccurred();
    }
    
    @Test
    public void test_validate_program_error_iconId_invalid() {
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        PowerMockito.when(filesFindBy.findOne(ProjectConstants.ID, Long.class)).thenReturn(null);
        validateErrorException(ProgramConstants.ERROR_INVALID_ICON_ID_CODE);
    }
    
    @Test
    public void test_validate_program_error_award_code_neither_printable_nor_downloadable() {
        awardCodeDetailsDTO.setPrintable(Boolean.FALSE);
        awardCodeDetailsDTO.setDownloadable(Boolean.FALSE);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_AWARD_CODE_INVALID_MODE_CODE);
    }
    
    @Test
    public void test_validate_program_error_printable_missing_cert() {
        awardCodeDetailsDTO.setCertificates(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_MISSING_CERTIFICATES_CODE);
    }
    
    @Test
    public void test_validate_program_success_inactive_printable_missing_cert() {
        awardCodeDetailsDTO.setCertificates(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setStatus(StatusTypeCode.INACTIVE.name());
        validateNoErrorOccurred();
    }
    
    @Test
    public void test_validate_program_error_downloadable_with_cert() {
        awardCodeDetailsDTO.setPrintable(Boolean.FALSE);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_CERTIFICATES_NOT_REQUIRED_CODE);
    }
    
    @Test
    public void test_validate_program_error_certificate_invalid() {
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        PowerMockito.when(filesFindBy.find(ProjectConstants.ID, Long.class)).thenReturn(null);
        validateErrorException(ProgramConstants.ERROR_INVALID_CERTIFICATES_CODE);
    }
    
    @Test
    public void test_validate_program_error_award_code_missing() {
        programRequestDTO.setAwardCode(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        validateErrorException(ProgramConstants.ERROR_MISSING_AWARD_CODE_OBJECT_CODE);
    }
    
    @Test
    public void test_validate_program_success_inactive_award_code_missing() {
        programRequestDTO.setAwardCode(null);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setStatus(StatusTypeCode.INACTIVE.name());
        validateNoErrorOccurred();
    }
    
    @Test
    public void test_validate_program_error_notification_only_for_manager() {
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setNotification(Arrays.asList(
                NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME,
                NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME));
        validateErrorException(ProgramConstants.ERROR_NOTIFICATION_INVALID);
    }
    
    
    private void validateErrorException(String errorCode) {
        try {
            errors = awardCodeProgramValidator.validateProgram(programRequestDTO, ProjectConstants.POST, TestConstants.ID_1);
            ErrorMessageException.throwIfHasErrors(errors);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        } 
    }
    
    private void validateNoErrorOccurred() {
        try {
            errors = awardCodeProgramValidator.validateProgram(programRequestDTO, ProjectConstants.POST, TestConstants.ID_1);
            ErrorMessageException.throwIfHasErrors(errors);
        } catch (ErrorMessageException errorMsgException) {
            fail();
        } 
    }
    
    /**
     * Generates a valid eligibility stub DTO with no ids for a ProgramRequest.
     *
     * @return a valid eligibility stub DTO with no ids
     */
    private EligibilityStubDTO generateEmptyEligibilityStubDTO() {
        return generateEligibilityStubDTO(new ArrayList<Long>(), new ArrayList<Long>(), new ArrayList<Long>(),
                new ArrayList<Long>());
    }
    
    /**
     * Generates the eligibility stub DTO with the specified paxIds and
     * groupIds.
     *
     * @param givePaxIds
     *            paxIds identified for giving eligibility
     * @param giveGroupIds
     *            groupIds identified for receiving eligibility
     * @param receivePaxIds
     *            paxIds identified for giving eligibility
     * @param receiveGroupIds
     *            groupIds identified for receiving eligibility
     * @return eligibility stub DTO with the specified paxIds and groupIds
     */
    private EligibilityStubDTO generateEligibilityStubDTO(List<Long> givePaxIds, List<Long> giveGroupIds,
            List<Long> receivePaxIds, List<Long> receiveGroupIds) {
        EligibilityStubDTO eligibilityStubDTO = new EligibilityStubDTO();

        // generate list of eligibility entity stub DTOs for giving and receiving eligibility
        eligibilityStubDTO.setGive(generateEligibilityEntityStubDTOs(givePaxIds, giveGroupIds));
        eligibilityStubDTO.setReceive(generateEligibilityEntityStubDTOs(receivePaxIds, receiveGroupIds));

        return eligibilityStubDTO;
    }
    
    /**
     * Generates a list of eligibility entity stub DTOs with the specified
     * paxIds and/or groupId.
     *
     * @param paxIds
     *            paxIds to be included
     * @param groupIds
     *            groupIds to be included
     * @return a list of eligibility entity stub DTOs with the specified paxIds
     *         and/or groupIds
     */
    private List<EligibilityEntityStubDTO> generateEligibilityEntityStubDTOs(List<Long> paxIds, List<Long> groupIds) {
        List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs = new ArrayList<>();

        // add paxIds within the DTO
        if (paxIds != null) {
            for (Long paxId : paxIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setPaxId(paxId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        // add groupIds within the DTO
        if (groupIds != null) {
            for (Long groupId : groupIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setGroupId(groupId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        return eligibilityEntityStubDTOs;
    }
}
