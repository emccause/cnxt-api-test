package com.maritz.culturenext.program.validators;

import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.*;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import com.maritz.survey.dto.QuestionDTO;
import com.maritz.test.AbstractMockTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PinnacleProgramValidatorTest extends AbstractMockTest {
    private PinnacleProgramValidator pinnacleProgramValidator;

    private String nullAudience;
    private String emptyAudience;
    private String individualAudience;
    private String teamAudience;
    private String bothAudience;
    private AwardsDTO nullAwards;
    private AwardsDTO emptyAwards;
    private AwardsDTO individualAward;
    private AwardsDTO teamAwardPerIndividual;
    private AwardsDTO teamAwardPerTeam;
    private AwardsDTO individualAwardAndTeamPerIndividualAndTeamPerTeam;
    private String validNominationInstructions;
    private String nullNominationInstructions;
    private String emptyNominationInstructions;
    private String tooLongNominationInstructions;
    private List<QuestionDTO> nullQuestions;
    private List<QuestionDTO> emptyQuestions;
    private List<QuestionDTO> validQuestions;
    private List<ApprovalDTO> approvalMissingPax;
    private List<ApprovalDTO> nullApproval;
    private List<ApprovalDTO> emptyApproval;
    private List<ApprovalDTO> validApprovals;
    private List<NominationPeriodDTO> emptyNominationPeriods;
    private List<NominationPeriodDTO> nullNominationPeriods;
    private List<NominationPeriodDTO> nominationPeriodMissingFromDate;
    private List<NominationPeriodDTO> nominationPeriodsMissingThruDate;
    private List<NominationPeriodDTO> nominationPeriodsEmptyOptionalThruDate;
    private List<NominationPeriodDTO> overlappingNominationPeriods;
    private List<NominationPeriodDTO> validNominationPeriods;

    private static final String postRequestType = "POST";
    private static final Long programId = 1L;

    @Before
    public void setup() {
        // set up audiences
        nullAudience = null;
        emptyAudience = "";
        individualAudience = "INDIVIDUAL";
        teamAudience = "TEAM";
        bothAudience = "BOTH";

        // set up awards
        nullAwards = null;
        emptyAwards = new AwardsDTO();
        individualAward = createAwards(10D, 0D, 0D);
        teamAwardPerIndividual = createAwards(0D, 20D, 0D);
        teamAwardPerTeam = createAwards(0D, 0D, 30D);
        individualAwardAndTeamPerIndividualAndTeamPerTeam = createAwards(80D, 90D, 100D);

        // set up nomination instructions and questions
        validNominationInstructions = "These are your nomination instructions.";
        tooLongNominationInstructions = "These are your nomination instructions but they are way too long. " +
                "These are your nomination instructions but they are way too long. " +
                "These are your nomination instructions but they are way too long. " +
                "These are your nomination instructions but they are way too long. ";
        nullNominationInstructions = null;
        emptyNominationInstructions = "";
        QuestionDTO question1 = new QuestionDTO();
        QuestionDTO question2 = new QuestionDTO();
        question1.setText("This is your first question.");
        question2.setText("I'm Ron Burgundy?");
        validQuestions = new ArrayList<>();
        validQuestions.add(question1);
        validQuestions.add(question2);
        emptyQuestions = new ArrayList<>();
        nullQuestions = null;

        // set up approvals
        ApprovalDTO approval = createApproval(2L);
        validApprovals = new ArrayList<>();
        validApprovals.add(approval);

        emptyApproval = new ArrayList<>();
        nullApproval = null;
        approvalMissingPax = new ArrayList<>();
        approval.setPax(null);
        approvalMissingPax.add(approval);

        // set up nomination periods
        validNominationPeriods = new ArrayList<>();
        validNominationPeriods.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        validNominationPeriods.add(createNominationPeriod("2018-06-01", "2018-06-30"));

        emptyNominationPeriods = new ArrayList<>();
        nullNominationPeriods = null;
        nominationPeriodsMissingThruDate = new ArrayList<>();
        nominationPeriodsMissingThruDate.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        nominationPeriodsMissingThruDate.add(createNominationPeriod("2018-06-01", null));
        nominationPeriodsMissingThruDate.add(createNominationPeriod("2018-07-01", "2018-08-01"));
        nominationPeriodMissingFromDate = new ArrayList<>();
        nominationPeriodMissingFromDate.add(createNominationPeriod(null, "2018-05-31"));
        nominationPeriodsEmptyOptionalThruDate = new ArrayList<>();
        nominationPeriodsEmptyOptionalThruDate.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        nominationPeriodsEmptyOptionalThruDate.add(createNominationPeriod("2018-06-01", null));
        overlappingNominationPeriods = new ArrayList<>();
        overlappingNominationPeriods.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        overlappingNominationPeriods.add(createNominationPeriod("2018-05-02", "2018-05-09"));

        pinnacleProgramValidator = new PinnacleProgramValidator();
    }

    @Test
    public void null_approval_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, nullApproval, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_MISSING_APPROVAL_MSG.equalsIgnoreCase(message));
    }
;
    @Test
    public void empty_approval_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, emptyApproval, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_MISSING_APPROVAL_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void null_instructions_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nullNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_MISSING_INSTRUCTIONS_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void empty_instructions_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, emptyNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_MISSING_INSTRUCTIONS_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void too_long_instructions_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, tooLongNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_INSTRUCTIONS_TOO_LONG_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void null_questions_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                nullQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_NO_QUESTIONS_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void empty_questions_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                emptyQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_NO_QUESTIONS_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void null_audience_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(nullAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
    }

    @Test
    public void empty_audience_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(emptyAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_MISSING_AUDIENCE_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void null_awards_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, nullAwards, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_AWARDS_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void empty_awards_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, emptyAwards, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void individual_audience_team_award_per_person_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, teamAwardPerIndividual, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(2, errors.size());
        List<String> messages = errors.stream().map(m -> m.getMessage()).collect(Collectors.toList());
        List<String> expectedMessages = new ArrayList<>();
        expectedMessages.add(PinnacleConstants.ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG);
        expectedMessages.add(PinnacleConstants.ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED_MSG);
        Assert.assertTrue(messages.containsAll(expectedMessages));
    }

    @Test
    public void individual_audience_team_award_per_team_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, teamAwardPerTeam, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(2, errors.size());
        List<String> messages = errors.stream().map(m -> m.getMessage()).collect(Collectors.toList());
        List<String> expectedMessages = new ArrayList<>();
        expectedMessages.add(PinnacleConstants.ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG);
        expectedMessages.add(PinnacleConstants.ERROR_TEAM_AWARD_AMOUNT_NOT_ALLOWED_MSG);
        Assert.assertTrue(messages.containsAll(expectedMessages));
    }

    @Test
    public void team_audience_individual_award_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(teamAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(2, errors.size());
        List<String> messages = errors.stream().map(m -> m.getMessage()).collect(Collectors.toList());
        List<String> expectedMessages = new ArrayList<>();
        expectedMessages.add(PinnacleConstants.ERROR_TEAM_AWARD_AMOUNT_REQUIRED_MSG);
        expectedMessages.add(PinnacleConstants.ERROR_INDIVIDUAL_AWARD_AMOUNT_NOT_ALLOWED_MSG);
        Assert.assertTrue(messages.containsAll(expectedMessages));
    }

    @Test
    public void both_audience_individual_award_only_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(bothAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_TEAM_AWARD_AMOUNT_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void both_audience_team_awards_only_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(bothAudience, validNominationInstructions,
                validQuestions, teamAwardPerIndividual, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_INDIVIDUAL_AWARD_AMOUNT_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void both_audience_all_awards_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(bothAudience, validNominationInstructions,
                validQuestions, individualAwardAndTeamPerIndividualAndTeamPerTeam, validApprovals, validNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.ERROR_TWO_TEAM_AWARD_AMOUNTS_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void empty_nomination_period_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, emptyNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.NOMINATION_PERIOD_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void null_nomination_period_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, nullNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.NOMINATION_PERIOD_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void multiple_nomination_periods_missing_thru_date_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, nominationPeriodsMissingThruDate);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.NOMINATION_PERIOD_THRU_DATE_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void multiple_nomination_periods_empty_optional_thru_date() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, nominationPeriodsEmptyOptionalThruDate);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(0, errors.size());
    }

    @Test
    public void nomination_periods_missing_from_date_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, nominationPeriodMissingFromDate);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.NOMINATION_PERIOD_FROM_DATE_REQUIRED_MSG.equalsIgnoreCase(message));
    }

    @Test
    public void overlapping_nomination_periods_fails() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, validNominationInstructions,
                validQuestions, individualAward, validApprovals, overlappingNominationPeriods);
        List<ErrorMessage> errors = pinnacleProgramValidator.validateProgram(program, postRequestType, programId);
        Assert.assertEquals(1, errors.size());
        String message = errors.get(0).getMessage();
        Assert.assertTrue(PinnacleConstants.NOMINATION_PERIODS_MUST_NOT_OVERLAP_MSG.equalsIgnoreCase(message));
    }

    private AwardsDTO createAwards(Double individualAward, Double teamAwardPerPerson, Double teamAwardPerTeam) {
        AwardsDTO awards = new AwardsDTO();

        awards.setIndividualAmount(individualAward);
        awards.setTeamAmountPerPerson(teamAwardPerPerson);
        awards.setTeamAmountPerTeam(teamAwardPerTeam);

        return awards;
    }

    private ApprovalDTO createApproval(Long paxId) {
        ApprovalDTO approval = new ApprovalDTO();
        EmployeeDTO employee = new EmployeeDTO();
        employee.setPaxId(paxId);

        approval.setLevel(1L);
        approval.setType("PERSON");
        approval.setPax(employee);
        return approval;
    }

    private NominationPeriodDTO createNominationPeriod(String fromDate, String thruDate) {
        NominationPeriodDTO nominationPeriod = new NominationPeriodDTO();
        if (fromDate != null && !fromDate.isEmpty()) {
            nominationPeriod.setFromDate(fromDate);
        }
        if (thruDate != null && !thruDate.isEmpty()) {
            nominationPeriod.setThruDate(thruDate);
        }
        return nominationPeriod;
    }

    private ProgramRequestDTO setupProgramRequestDTO(String audience, String nominationInstructions, List<QuestionDTO> questions,
                                                     AwardsDTO awards, List<ApprovalDTO> approvals, List<NominationPeriodDTO> nominationPeriods) {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        List<BudgetAssignmentStubDTO> budgetAssignments = new ArrayList<>();
        budgetAssignments.add(new BudgetAssignmentStubDTO());
        programRequestDTO.setBudgetAssignments(budgetAssignments);
        PinnacleDTO pinnacleDTO = new PinnacleDTO();
        pinnacleDTO.setAudience(audience);
        pinnacleDTO.setNominationInstructions(nominationInstructions);
        pinnacleDTO.setQuestions(questions);
        pinnacleDTO.setAwards(awards);
        pinnacleDTO.setApprovals(approvals);
        pinnacleDTO.setNominationPeriods(nominationPeriods);

        programRequestDTO.setPinnacle(pinnacleDTO);

        return programRequestDTO;
    }
}
