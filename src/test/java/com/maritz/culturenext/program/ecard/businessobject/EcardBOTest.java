package com.maritz.culturenext.program.ecard.businessobject;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import com.maritz.culturenext.program.ecard.dto.EcardImagesDictDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.util.MapUtils;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EcardBOTest extends AbstractDatabaseTest {

    @Inject
    private EcardBO ecardBO;
    @Inject private FilesRepository filesRepository;

    @Test(expected = IllegalArgumentException.class)
    public void validateEmptyMetadataAndImages(){
        assertNotNull(ecardBO);
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        MultipartFile[] images = new MultipartFile[0];
        EcardImageDTO[] imagesMetadata = new EcardImageDTO[0];
        EcardImagesDictDTO result = ecardBO.validateMetadataAndImages(imagesMetadata, images, errors);
        assertTrue(result.getImagesDict().isEmpty() && result.getMetadataDict().isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateDifferentMetadataAndImages(){
        assertNotNull(ecardBO);
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        MultipartFile[] images = new MultipartFile[2];
        EcardImageDTO[] imagesMetadata = new EcardImageDTO[3];
        EcardImagesDictDTO result = ecardBO.validateMetadataAndImages(imagesMetadata, images, errors);
        assertTrue(result.getImagesDict().isEmpty() && result.getMetadataDict().isEmpty());    }

    @Test
    public void validateSameSizeDifferentData() throws IOException {
        assertNotNull(ecardBO);
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        MultipartFile[] images = new MultipartFile[3];
        MockMultipartFile firstMultipartFile = new MockMultipartFile("example 1",
                "example 1",
                "application/sql",
                (InputStream) null);

        MockMultipartFile secondMultipartFile = new MockMultipartFile("example 2",
                "example 2",
                "application/sql",
                (InputStream) null);

        MockMultipartFile thirdMultipartFile = new MockMultipartFile("example 3",
                "example 3",
                "application/sql",
                (InputStream) null);
        images[0] = thirdMultipartFile;
        images[1] = secondMultipartFile;
        images[2] = firstMultipartFile;

        EcardImageDTO[] imagesMetadata = new EcardImageDTO[3];
        EcardImageDTO firstElement = new EcardImageDTO();
        firstElement.setFileName("example 1");
        imagesMetadata[0] = firstElement;
        EcardImageDTO secondElement = new EcardImageDTO();
        secondElement.setFileName("example 5");
        imagesMetadata[1] = secondElement;
        EcardImageDTO thirdElement = new EcardImageDTO();
        thirdElement.setFileName("example 3");
        imagesMetadata[2] = thirdElement;
        EcardImagesDictDTO result = ecardBO.validateMetadataAndImages(imagesMetadata, images, errors);
        assertTrue(MapUtils.isEmpty(result.getImagesDict()) && MapUtils.isEmpty(result.getMetadataDict()));
    }

    @Test
    public void validateSameSizeSameData() throws IOException {
        assertNotNull(ecardBO);
        ArrayList<ErrorMessage> errors = new ArrayList<>();
        MultipartFile[] images = new MultipartFile[3];
        MockMultipartFile firstMultipartFile = new MockMultipartFile("example 1",
                "example 1",
                "application/sql",
                (InputStream) null);

        MockMultipartFile secondMultipartFile = new MockMultipartFile("example 2",
                "example 2",
                "application/sql",
                (InputStream) null);

        MockMultipartFile thirdMultipartFile = new MockMultipartFile("example 3",
                "example 3",
                "application/sql",
                (InputStream) null);
        images[0] = thirdMultipartFile;
        images[1] = secondMultipartFile;
        images[2] = firstMultipartFile;

        EcardImageDTO[] imagesMetadata = new EcardImageDTO[3];
        EcardImageDTO firstElement = new EcardImageDTO();
        firstElement.setFileName("example 1");
        imagesMetadata[0] = firstElement;
        EcardImageDTO secondElement = new EcardImageDTO();
        secondElement.setFileName("example 2");
        imagesMetadata[1] = secondElement;
        EcardImageDTO thirdElement = new EcardImageDTO();
        thirdElement.setFileName("example 3");
        imagesMetadata[2] = thirdElement;

        EcardImagesDictDTO result = ecardBO.validateMetadataAndImages(imagesMetadata, images, errors);
        assertTrue(!MapUtils.isEmpty(result.getImagesDict()) && !MapUtils.isEmpty(result.getMetadataDict()));
    }

    @Test
    public void testUpdateImage (){
        assertNotNull(filesRepository);
        List<Files> filesfound = filesRepository.findByEcardIdFileTypeCode(10128L, "ECARD_DEFAULT");
        assertNotNull(filesfound);
        assertEquals(1,filesfound.size());
        Files filefound = filesfound.get(0);
        filefound.setUrl("SET BY CODE!");
        filesRepository.saveAndFlush(filefound);
        List<Files> filesfoundSnd = filesRepository.findByEcardIdFileTypeCode(10128L, "ECARD_DEFAULT");
        assertEquals("SET BY CODE!", filesfoundSnd.get(0).getUrl());
        System.out.println(ReflectionToStringBuilder.toString(filesfoundSnd.get(0)));
    }

}