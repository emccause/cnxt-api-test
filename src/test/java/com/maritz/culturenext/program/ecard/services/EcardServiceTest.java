package com.maritz.culturenext.program.ecard.services;

import com.maritz.TestUtil;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.images.service.impl.FileImageServiceImpl;
import com.maritz.culturenext.program.ecard.dto.EcardInfoDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class EcardServiceTest extends AbstractDatabaseTest {

    private static final Logger log = LoggerFactory.getLogger(EcardServiceTest.class);

    public static final String SAMPLE_IMAGE_PNG = "sample-image.png";
    public static final String TEST_DESCRIPTION = "testDescription";
    public static final String THIS_TEST_CASE_MUST_FAIL = "This test case must fail";
    public static final String IMAGE_JPEG = "image/jpeg";
    @Inject
    private EcardService ecardService;
    @Inject
    private AuthenticationService authenticationService;

    private static final int MAX_ECARD_NAME_LENGTH = 50;
    private static final int MAX_ECARD_DESCRIPTION_LENGTH = 100;

    private static final String EXISTING_ECARD_NAME = "Great work!";
    private static final String DEFAULT_SIZE = "default";
    private static final String THUMBNAIL_SIZE = "thumbnail";

    // error codes
    private static final String ERROR_ECARD_INFO_MISSING = "ECARD_INFO_MISSING";
    private static final String ERROR_ECARD_MISSING = "ECARD_MISSING";
    private static final String ERROR_CONTENT_TYPE = "CONTENT_TYPE";
    private static final String ERROR_ECARD_NAME_MISSING = "ECARD_NAME_MISSING";
    private static final String ERROR_ECARD_NAME_LENGTH = "ECARD_NAME_LENGTH";
    private static final String ERROR_DUPLICATE_ECARD_NAME = "DUPLICATE_ECARD_NAME";
    private static final String ERROR_ECARD_DESCRIPTION_MISSING = "ECARD_DESCRIPTION_MISSING";
    private static final String ERROR_ECARD_DESCRIPTION_LENGTH = "ECARD_DESCRIPTION_LENGTH";
    private static final String ERROR_ECARD_TOO_SMALL = "ECARD_TOO_SMALL";
    private static final String ERROR_ECARD_NOT_EXIST = "ERROR_ECARD_NOT_EXIST";
    private static final String ERROR_ECARD_TYPE = "ECARD_TYPE";
    private static final String ERROR_ECARD_ASSIGNED_TO_PROGRAM = "ECARD_ASSIGNED_TO_PROGRAM";

    // validation tests

    @Test
    public void testUploadEcardMissingImage() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_MISSING);
        verifyUploadEcardErrors(null, generateValidUploadEcardInfoDTO(), expectedErrors);
    }

    @Test
    public void testUploadEcardEmptyImage() throws Exception {
        assertNotNull(ecardService);
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_MISSING);
        MultipartFile[] image = generateMultiPartFile("test", null, new byte[] {});
        verifyUploadEcardErrors(image, generateValidUploadEcardInfoDTO(), expectedErrors);
    }

    @Test
    public void testUploadEcardMissingImageContentType() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_CONTENT_TYPE);
        MultipartFile[] image = generateMultiPartFile("test", null, SAMPLE_IMAGE_PNG);
        verifyUploadEcardErrors(image, generateValidUploadEcardInfoDTO(), expectedErrors);
    }

    @Test
    public void testUploadEcardUnsupportedImageContentType() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_CONTENT_TYPE);
        MultipartFile[] image = generateMultiPartFile("test", "invalid/content-type", SAMPLE_IMAGE_PNG);
        verifyUploadEcardErrors(image, generateValidUploadEcardInfoDTO(), expectedErrors);
    }

    // TODO MP-7588 Determine tests to test out process error and multiple image
    // uploads

    @Test
    public void testUploadEcardMissingEcardName() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_NAME_MISSING);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO(null, TEST_DESCRIPTION);
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardMissingEcardInfo() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_INFO_MISSING);
        MultipartFile[] image = generateValidMultiPartFile();
        verifyUploadEcardErrors(image, null, expectedErrors);
    }

    @Test
    public void testUploadEcardEmptyEcardName() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_NAME_MISSING);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO("", TEST_DESCRIPTION);
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardTooLongEcardName() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_NAME_LENGTH);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO(
                RandomStringUtils.randomAlphanumeric(MAX_ECARD_NAME_LENGTH + 150), TEST_DESCRIPTION);
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardExistingEcardName() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_DUPLICATE_ECARD_NAME);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO(EXISTING_ECARD_NAME, TEST_DESCRIPTION);
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardMissingDescription() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_DESCRIPTION_MISSING);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO("test", null);
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardEmptyDescription() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_DESCRIPTION_MISSING);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO("test", "");
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardTooLongDescription() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_DESCRIPTION_LENGTH);
        MultipartFile[] image = generateValidMultiPartFile();
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO("test",
                RandomStringUtils.randomAlphanumeric(MAX_ECARD_DESCRIPTION_LENGTH + 150));
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testUploadEcardTooSmall() throws Exception {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_TOO_SMALL);
        MultipartFile[] image = generateMultiPartFile(RandomStringUtils.randomAlphanumeric(16), "image/png",
                SAMPLE_IMAGE_PNG);
        EcardInfoDTO ecardInfoDTO = generateUploadEcardInfoDTO("test",
                RandomStringUtils.randomAlphanumeric(MAX_ECARD_DESCRIPTION_LENGTH + 150));
        verifyUploadEcardErrors(image, ecardInfoDTO, expectedErrors);
    }

    @Test
    public void testInactiveCustomEcard_success()  {
        // admin user
        authenticationService.authenticate(TestConstants.USER_KUMARSJ_ADMIN);
        // ecardId: custom type and NOT assigned to program.
        Long ecardId = 81L;

        try {
            ecardService.inactiveCustomEcard(ecardId);
        } catch (ErrorMessageException e) {
            fail("ERROR: not able to inactive a valid ecard");
        }
    }

    @Test
    public void testInactiveCustomEcard_fail_ecardId_not_found() {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_NOT_EXIST);
        // admin user
        authenticationService.authenticate(TestConstants.USER_KUMARSJ_ADMIN);
        // ecardId: not exist.
        Long ecardId = 8000L;
        try {
            ecardService.inactiveCustomEcard(ecardId);
            fail(THIS_TEST_CASE_MUST_FAIL);
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrors, e);
        }
    }

    @Test
    public void testInactiveCustomEcard_fail_not_custom_type() {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_TYPE);
        // admin user
        authenticationService.authenticate(TestConstants.USER_KUMARSJ_ADMIN);
        // ecardId: NOT custom type, NOT assigned to program.
        Long ecardId = 82L;
        try {
            ecardService.inactiveCustomEcard(ecardId);
            fail(THIS_TEST_CASE_MUST_FAIL);
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrors, e);
        }
    }

    @Test
    public void testInactiveCustomEcard_fail_assigned_to_program() {
        List<String> expectedErrors = Arrays.asList(ERROR_ECARD_ASSIGNED_TO_PROGRAM);
        // admin user
        authenticationService.authenticate(TestConstants.USER_KUMARSJ_ADMIN);
        // ecardId: custom type, but assigned to program.
        Long ecardId = 83L;
        try {
            ecardService.inactiveCustomEcard(ecardId);
            fail(THIS_TEST_CASE_MUST_FAIL);
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrors, e);
        }
    }

    @Test
    public void test_getImage_default_size() {
        Files fileD = new Files();
        fileD.setFileTypeCode("Default");
        fileD.setMediaType(IMAGE_JPEG);

        Files fileT = new Files();
        fileT.setFileTypeCode("Thumbnail");
        fileT.setMediaType(IMAGE_JPEG);
        List<Files> filesList = new ArrayList<>();
        filesList.add(fileD);
        filesList.add(fileT);

        ResponseEntity<Object> image;
        try {
            image = ecardService.getImage(83L, DEFAULT_SIZE);
            assertNotNull(image);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void test_getImage_thumbnail_size() {
        Files fileD = new Files();
        fileD.setFileTypeCode("Default");
        fileD.setMediaType(IMAGE_JPEG);

        Files fileT = new Files();
        fileT.setFileTypeCode("Thumbnail");
        fileT.setMediaType(IMAGE_JPEG);

        List<Files> filesList = new ArrayList<>();
        filesList.add(fileD);
        filesList.add(fileT);

        ResponseEntity<Object> image = null;
        try {
            image = ecardService.getImage(83L, THUMBNAIL_SIZE);
            assertNotNull (image);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    // helper methods
    
    private MultipartFile[] generateMultiPartFile(String fileName, String contentType, byte[] data
            ) {
        MockMultipartFile[] images = new MockMultipartFile[1];
        images[0] = new MockMultipartFile(fileName, fileName, contentType, data);
        return images;
    }

    private MultipartFile[] generateValidMultiPartFile() throws IOException {
        return generateMultiPartFile(RandomStringUtils.randomAlphanumeric(16), IMAGE_JPEG, "earth-2014-free-15.jpg");
    }

    private MultipartFile[] generateMultiPartFile(String fileName, String contentType, String resourceName
            ) throws IOException {
        MockMultipartFile[] images = new MockMultipartFile[1];
        images[0] = new MockMultipartFile(fileName, fileName, contentType, 
                new ClassPathResource(resourceName).getInputStream());
        return images;
    }


    private EcardInfoDTO generateValidUploadEcardInfoDTO() {
        // use randomly generated values for display name and description
        String displayName = RandomStringUtils.randomAlphanumeric(16);
        String description = RandomStringUtils.randomAlphanumeric(16);

        return generateUploadEcardInfoDTO(displayName, description);
    }

    private EcardInfoDTO generateUploadEcardInfoDTO(String displayName, String description) {
        EcardInfoDTO ecardInfoDTO = new EcardInfoDTO();
        ecardInfoDTO.setDisplayName(displayName);
        ecardInfoDTO.setDescription(description);
        ecardInfoDTO.setDisplayType("CUSTOM");
        return ecardInfoDTO;
    }

    private void verifyUploadEcardErrors(MultipartFile[] images,
                                         EcardInfoDTO info,
                                         @Nonnull List<String> expectedErrorCodes) throws Exception {

        try {
            Map<String, Object> ecardInfo = ecardService.uploadEcard(images);
            ecardService.updateEcardInfo((Long) ecardInfo.get(ProjectConstants.ID), info);
        } catch (ErrorMessageException e) {
            //TODO MP-7588 replace with matchers
            //log.error(e.getMessage());
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }
}