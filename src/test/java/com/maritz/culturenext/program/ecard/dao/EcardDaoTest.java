package com.maritz.culturenext.program.ecard.dao;

import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.ecard.dao.EcardDao;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class EcardDaoTest extends AbstractDatabaseTest {

    @Inject EcardDao ecardDao;
    @Inject

    @Test
    public void test_getInfoForEcardId() {
        Map<String, Object> infoForEcardId = ecardDao.getInfoForEcardId(TestConstants.ID_1);
        assertNotNull(infoForEcardId);
    }

    @Test
    public void test_getInfoForEcardId_null(){
        Map<String, Object> infoForEcardId = ecardDao.getInfoForEcardId(null);
        assertNull(infoForEcardId);
    }

    @Test
    public void test_getEcard(){
        Files ecard = ecardDao.getEcard(TestConstants.ID_1, "full");
        assertNull(ecard);
    }

    @Test
    public void test_getEcard_null_id(){
        Files ecard = ecardDao.getEcard(null, null);
        assertNull(ecard);
    }

}
