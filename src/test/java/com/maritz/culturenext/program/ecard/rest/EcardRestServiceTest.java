package com.maritz.culturenext.program.ecard.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.program.ecard.dto.EcardImageDTO;
import com.maritz.culturenext.program.ecard.dto.EcardInfoDTO;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.maritz.culturenext.constants.ProjectConstants.IMAGE;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EcardRestServiceTest extends AbstractRestTest {

    private final static String URI_ECARD = "/rest/ecards?type="+ FileImageTypes.PRFD.name()+"&targetId=24";
    private final static String URI_MULTIPLE_FILE_ECARDS_UPLOAD = "/rest/ecards/upload";
    private final static String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private final static String TEST_SMALL_IMAGE = "blindmouse.png";
    private final static String IMAGE_FILE = "carousel-image-large.png";
    private final static String IMAGE_FILE_SQUARE = "carousel-image-square.png";
    private final static Long ECARD_ID = 83L;

    @Test
    public void test_upload_ecard() throws Exception {
        FileInputStream image = new FileInputStream(TEST_IMAGES_DIR + IMAGE_FILE);


        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE,
                IMAGE_FILE,
                MediaTypesEnum.IMAGE_PNG.value(),
                image);

        mockMvc.perform(
                fileUpload(String.format(URI_ECARD))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_MUDDAM))
                        .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_upload_ecard_cnxt_778() throws Exception {
        FileInputStream image = new FileInputStream(TEST_IMAGES_DIR + TEST_SMALL_IMAGE);


        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE,
                IMAGE_FILE,
                MediaTypesEnum.IMAGE_PNG.value(),
                image);

        MvcResult response = mockMvc.perform(
                fileUpload(String.format(URI_ECARD))
                        .file(multipartfile)
                        .with(authToken(TestConstants.USER_MUDDAM))
                        .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
        )
                .andExpect(status().isBadRequest()).andReturn();
        assertTrue(response.getResponse().getContentAsString().contains("ECARD_TOO_SMALL"));
    }

    @Test
    public void test_upload_Multiple_Ecards_Images () throws Exception {
        FileInputStream image = new FileInputStream(TEST_IMAGES_DIR + IMAGE_FILE);
        FileInputStream imageTwo = new FileInputStream(TEST_IMAGES_DIR + IMAGE_FILE_SQUARE);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.fileUpload(URI_MULTIPLE_FILE_ECARDS_UPLOAD);
        builder.with(new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setMethod("PUT");
                return request;
            }
        });

        MockMultipartFile fileImage = new MockMultipartFile(IMAGE, IMAGE_FILE,
                MediaTypesEnum.IMAGE_PNG.value(), imageTwo);

        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE,
                IMAGE_FILE,
                MediaTypesEnum.IMAGE_PNG.value(),
                image);

        EcardImageDTO data = new EcardImageDTO();
        data.setFileName(IMAGE_FILE);
        data.setId(24L);

        ObjectMapper objectMapper = new ObjectMapper();
        List<EcardImageDTO> ecardstoUpdateLst = new ArrayList<EcardImageDTO>();
        ecardstoUpdateLst.add(data);

        MockMultipartFile metadata =
                new MockMultipartFile(
                        "request",
                        "request",
                        MediaType.APPLICATION_JSON_VALUE,
                        objectMapper.writeValueAsString(ecardstoUpdateLst).getBytes(StandardCharsets.UTF_8));
        mockMvc.perform(
                builder .file(fileImage)
                        .file(multipartfile)
                        .file(metadata)
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                        .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void test_upload_Multiple_Ecards_Images_same_size () throws Exception {
        FileInputStream image = new FileInputStream(TEST_IMAGES_DIR + IMAGE_FILE);
        FileInputStream imageTwo = new FileInputStream(TEST_IMAGES_DIR + IMAGE_FILE_SQUARE);

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.fileUpload(URI_MULTIPLE_FILE_ECARDS_UPLOAD);
        builder.with(new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setMethod("PUT");
                return request;
            }
        });

        MockMultipartFile fileImage = new MockMultipartFile(IMAGE, IMAGE_FILE_SQUARE,
                MediaTypesEnum.IMAGE_PNG.value(), imageTwo);

        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE,
                IMAGE_FILE,
                MediaTypesEnum.IMAGE_PNG.value(),
                image);

        List<EcardImageDTO> ecardstoUpdateLst = new ArrayList<EcardImageDTO>();

        EcardImageDTO data = new EcardImageDTO();
        data.setFileName(IMAGE_FILE);
        data.setId(10129L);
        ecardstoUpdateLst.add(data);

        data = new EcardImageDTO();
        data.setFileName(IMAGE_FILE_SQUARE);
        data.setId(10128L);
        ecardstoUpdateLst.add(data);

        data = new EcardImageDTO();
        data.setFileName(IMAGE_FILE_SQUARE);
        data.setId(89L);
        ecardstoUpdateLst.add(data);

        ObjectMapper objectMapper = new ObjectMapper();

        MockMultipartFile metadata =
                new MockMultipartFile(
                        "request",
                        "request",
                        MediaType.APPLICATION_JSON_VALUE,
                        objectMapper.writeValueAsString(ecardstoUpdateLst).getBytes(StandardCharsets.UTF_8));
        mockMvc.perform(
                builder .file(fileImage)
                        .file(multipartfile)
                        .file(multipartfile)
                        .file(metadata)
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                        .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_get_all_active_ecards() throws Exception {
        mockMvc.perform(
                get("/rest/ecards")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_get_all_active_ecards_unauthorized() throws Exception {
        mockMvc.perform(
                get("/rest/ecards")
                )
                .andExpect(status().isUnauthorized())
            ;
    }

    @Test
    public void test_get_ecard_image() throws Exception {
        mockMvc.perform(
                get("/rest/ecards/75")
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_get_ecard_null_id() throws Exception {
            mockMvc.perform(
                    get("/rest/ecards/a")
                            .with(authToken(TestConstants.USER_MUDDAM))
            )
                    .andExpect(status().isNotFound());
    }

    @Test
    public void test_delete_ecard() throws Exception {
        mockMvc.perform(
                delete("/rest/ecards/81")
                    .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                )
                .andExpect(status().isOk())
            ;
    }
    
    @Test
    public void test_delete_ecard_ecardId_not_found() throws Exception {
        mockMvc.perform(
                delete("/rest/ecards/8000")
                    .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                )
                .andExpect(status().isNotFound())
            ;
    }
    
    @Test
    public void test_delete_ecard_not_admin() throws Exception {
        mockMvc.perform(
                delete("/rest/ecards/81")
                    .with(authToken(TestConstants.USER_MADRIDO))
                )
                .andExpect(status().isForbidden())
            ;
    }
    
    @Test
    public void test_delete_ecard_not_custom_type() throws Exception {
        mockMvc.perform(
                delete("/rest/ecards/82")
                 .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                )
                .andExpect(status().isBadRequest())
            ;
    }
    
    @Test
    public void test_delete_ecard_assigned_to_program() throws Exception {
        mockMvc.perform(
                delete("/rest/ecards/83")
                    .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_delete_ecard_null_id() throws Exception {
        try {
            mockMvc.perform(
                    delete("/rest/ecards/a")
                            .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
                    .andExpect(status().is4xxClientError());
        } catch (Throwable e) {
            assert(!e.getMessage().isEmpty());
        }
    }

    @Test
    public void test_update_ecard() throws Exception {
        EcardInfoDTO ecardInfoDTO = new EcardInfoDTO();
        ecardInfoDTO.setId(83L);
        ecardInfoDTO.setDescription("New ecard description");
        ecardInfoDTO.setDisplayName("New display name");
        ecardInfoDTO.setDisplayType("STOCK");
        String json = ObjectUtils.objectToJson(ecardInfoDTO);

        mockMvc.perform(
                put("/rest/ecards/83/info")
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .with(authToken(TestConstants.USER_MUDDAM))
                        .contentType(MediaType.APPLICATION_JSON).content(json)
                ).andExpect(status().isOk());
    }

    @Test
    public void test_update_ecard_null_id() throws Exception {
        EcardInfoDTO ecardInfoDTO = new EcardInfoDTO();
        ecardInfoDTO.setId(83L);
        ecardInfoDTO.setDescription("New ecard description");
        ecardInfoDTO.setDisplayName("New display name");
        ecardInfoDTO.setDisplayType("STOCK");
        String json = ObjectUtils.objectToJson(ecardInfoDTO);

        try {
            mockMvc.perform(
                    put("/rest/ecards/a/info")
                            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .with(authToken(TestConstants.USER_MUDDAM))
                            .contentType(MediaType.APPLICATION_JSON).content(json)
            ).andExpect(status().isOk());
        } catch (Throwable e) {
            assert(!e.getMessage().isEmpty());
        }
    }
}