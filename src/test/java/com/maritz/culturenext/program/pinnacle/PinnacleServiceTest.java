package com.maritz.culturenext.program.pinnacle;

import com.maritz.core.jpa.support.findby.FindBy;
import com.maritz.core.jpa.entity.*;
import com.maritz.core.jpa.repository.*;
import com.maritz.core.jpa.support.util.PaxTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.LanguageCodeEnum;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.culturenext.notifications.constants.NotificationConstants;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.dto.*;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import com.maritz.culturenext.program.pinnacle.service.PinnacleService;
import com.maritz.culturenext.program.pinnacle.service.impl.PinnacleServiceImpl;
import com.maritz.survey.dto.AnswerDTO;
import com.maritz.survey.dto.QuestionDTO;
import com.maritz.survey.dto.SectionDTO;
import com.maritz.survey.dto.TestDTO;
import com.maritz.survey.services.TestAdminService;
import com.maritz.survey.services.TestService;
import com.maritz.test.AbstractMockTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PinnacleServiceTest extends AbstractMockTest {
    private PinnacleService pinnacleService;

    @Mock private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    @Mock private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    @Mock private ApprovalProcessConfigRepository approvalProcessConfigRepository;
    @Mock private TestRepository testRepository;
    @Mock private TestSectionRepository testSectionRepository;
    @Mock private TestQuestionRepository testQuestionRepository;
    @Mock private TestAdminService testAdminService;
    @Mock private TestService testService;
    @Mock private AclRepository aclRepository;
    @Mock private PaxRepository paxRepository;

    @Mock private FindBy<ApprovalProcessConfig> approvalProcessConfigFindBy;
    @Mock private FindBy<Acl> aclFindBy;
    @Mock private FindBy<com.maritz.core.jpa.entity.Test> testFindBy;
    @Mock private FindBy<TestSection> testSectionFindBy;
    @Mock private FindBy<ProgramMisc> programMiscFindBy;
    @Mock private FindBy<Pax> paxFindBy;

    @Captor ArgumentCaptor<ProgramMisc> programMiscArgumentCaptor;
    @Captor ArgumentCaptor<ApprovalProcessConfig> approvalProcessConfigArgumentCaptor;
    @Captor ArgumentCaptor<ProgramActivity> programActivityArgumentCaptor;
    @Captor ArgumentCaptor<TestDTO> testArgumentCaptor;
    @Captor ArgumentCaptor<Acl> aclArgumentCaptor;

    private String individualAudience;
    private String teamAudience;
    private String bothAudience;
    private String nominationInstructions;
    private List<QuestionDTO> questions;
    private List<QuestionDTO> singleQuestion;
    private TestDTO testOneQuestion;
    private TestDTO testOneQuestionToReturn;
    private TestDTO testTwoQuestions;
    private TestDTO testTwoQuestionsToReturn;
    private AnswerDTO answer;
    private List<AnswerDTO> answers;

    private AwardsDTO individualAward;
    private AwardsDTO teamAwardPerIndividual;
    private AwardsDTO awardPerIndividualAndTeamPerTeam;

    private List<ApprovalDTO> approvals;
    private Pax pax;

    private List<NominationPeriodDTO> twoNominationPeriods;
    private List<NominationPeriodDTO> singleNominationPeriod;

    private List<String> notifications;

    private String newsfeedVisibilityNominated;
    private String newsfeedVisibilityApproved;

    private Acl aclToReturn;
    private List<TestQuestion> testQuestions;

    ProgramMisc audienceProgramMisc;
    ProgramMisc newsfeedVisNominatedProgramMisc;
    ProgramMisc newsfeedVisApprovedProgramMisc;
    ProgramMisc pinnacleNotificationRecipientProgramMisc;
    ProgramMisc pinnacleNotificationMgr;
    ProgramMisc pinnacleNotification2ndMgr;
    ProgramMisc individualAwardAmountProgramMisc;
    ProgramMisc teamAwardAmountPerPersonProgramMisc;
    ProgramMisc teamAwardAmountPerTeamProgramMisc;
    List<ProgramMisc> dummyProgramMiscs;
    List<String> dummyNotifications;

    ProgramActivity nomPeriod1ProgramActivity;
    ProgramActivity nomPeriod2ProgramActivity;
    List<ProgramActivity> dummyProgramActivities;

    private static final Long programId = 11L;
    private static final Long testId = 50L;
    private static final Long testSectionId = 51L;
    private static final String postRequest = "POST";
    private static final String putRequest = "PUT";
    private static final Long approvalProcessConfigId = 20L;
    private static final Long paxId = 13L;

    @Before
    public void setup() {
        pinnacleService = new PinnacleServiceImpl()
            .setCnxtProgramMiscRepository(cnxtProgramMiscRepository)
            .setCnxtProgramActivityRepository(cnxtProgramActivityRepository)
            .setApprovalProcessConfigRepository(approvalProcessConfigRepository)
            .setTestAdminService(testAdminService)
            .setTestService(testService)
            .setAclRepository(aclRepository)
            .setPaxRepository(paxRepository);

        // set up audiences
        individualAudience = PinnacleConstants.INDIVIDUAL_AUDIENCE;
        teamAudience = PinnacleConstants.TEAM_AUDIENCE;
        bothAudience = PinnacleConstants.BOTH_AUDIENCE;

        // set up nomination questions and instructions
        QuestionDTO question1 = new QuestionDTO();
        QuestionDTO question2 = new QuestionDTO();
        question1.setText("Why did the chicken cross the road?");
        question2.setText("I'm Ron Burgundy?");

        questions = new ArrayList<>();
        questions.add(question1);
        questions.add(question2);

        singleQuestion = new ArrayList<>();
        singleQuestion.add(question1);

        nominationInstructions = "Fill out this form.";

        testOneQuestion = createTest(nominationInstructions, singleQuestion);
        testTwoQuestions = createTest(nominationInstructions, questions);

        answer = new AnswerDTO();
        answer.setUserProvided(true);
        answers = new ArrayList<>();
        answers.add(answer);

        // set up awards
        individualAward = createAwards(10D, null, 0D);
        teamAwardPerIndividual = createAwards(0D, 20D, 0D);
        awardPerIndividualAndTeamPerTeam = createAwards(60D, 0D, 70D);

        // set up approval
        approvals = new ArrayList<>();
        ApprovalDTO approval = createApproval(3L);
        approvals.add(approval);

        // set up nomination periods
        singleNominationPeriod = new ArrayList<>();
        twoNominationPeriods = new ArrayList<>();

        singleNominationPeriod.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        twoNominationPeriods.add(createNominationPeriod("2018-05-01", "2018-05-31"));
        twoNominationPeriods.add(createNominationPeriod("2018-06-01", "2018-06-30"));

        // set up notifications
        notifications = new ArrayList<>();
        notifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT_MGR.toString());
        notifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.toString());
        notifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT.toString());

        // set up newsfeed visibility
        newsfeedVisibilityNominated = "RECIPIENT_PREF";
        newsfeedVisibilityApproved = "RECIPIENT_PREF";

        // set up test service return data
        testOneQuestionToReturn = testOneQuestion;
        testOneQuestionToReturn.setId(testId);
        testTwoQuestionsToReturn = testTwoQuestions;
        testTwoQuestionsToReturn.setId(testId);

        // set up test repositories and return data
        com.maritz.core.jpa.entity.Test test = new com.maritz.core.jpa.entity.Test();
        test.setName(PinnacleConstants.PINNACLE_TEST_DESCRIPTION);
        test.setDescription(nominationInstructions);
        test.setStatusTypeCode(StatusTypeCode.ACTIVE.name());

        com.maritz.core.jpa.entity.Test testPopulatedWithId = test;
        testPopulatedWithId.setId(testId);

        when(testRepository.save(Matchers.any(com.maritz.core.jpa.entity.Test.class))).thenReturn(testPopulatedWithId);

        TestSection testSection = new TestSection();
        testSection.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testSection.setTestId(testId);
        testSection.setNumberOfQuestions(questions.size());

        TestSection testSectionPopulatedWithId = testSection;
        testSectionPopulatedWithId.setId(testSectionId);

        when(testSectionRepository.save(Matchers.any(TestSection.class))).thenReturn(testSectionPopulatedWithId);

        when(testRepository.findBy()).thenReturn(testFindBy);
        when(testFindBy.where(Matchers.anyString())).thenReturn(testFindBy);
        when(testFindBy.and(Matchers.anyString())).thenReturn(testFindBy);
        when(testFindBy.eq(Matchers.anyString())).thenReturn(testFindBy);
        when(testFindBy.eq(Matchers.anyLong())).thenReturn(testFindBy);
        when(testFindBy.findOne()).thenReturn(test);

        when(testSectionRepository.findBy()).thenReturn(testSectionFindBy);
        when(testSectionFindBy.where(Matchers.anyString())).thenReturn(testSectionFindBy);
        when(testSectionFindBy.eq(Matchers.anyLong())).thenReturn(testSectionFindBy);
        when(testSectionFindBy.findOne()).thenReturn(testSectionPopulatedWithId);

        testQuestions = new ArrayList<>();
        TestQuestion testQuestion1 = new TestQuestion();
        testQuestion1.setQuestionText(questions.get(0).getText());
        testQuestion1.setTestSectionId(testSectionId);
        testQuestions.add(testQuestion1);

        when(testQuestionRepository.findByTestId(Matchers.anyLong())).thenReturn(testQuestions);

        // set up ACL repository and return data
        Acl testAcl = new Acl();
        testAcl.setTargetId(programId);
        testAcl.setTargetTable(PinnacleConstants.PROGRAM_TABLE);
        testAcl.setSubjectId(testId);
        testAcl.setSubjectTable(PinnacleConstants.TEST_TABLE);

        aclToReturn = testAcl;
        aclToReturn.setId(10L);

        when(aclRepository.findBy()).thenReturn(aclFindBy);
        when(aclFindBy.where(Matchers.anyString())).thenReturn(aclFindBy);
        when(aclFindBy.eq(Matchers.anyString())).thenReturn(aclFindBy);
        when(aclFindBy.and(Matchers.anyString())).thenReturn(aclFindBy);
        when(aclFindBy.eq(Matchers.anyLong())).thenReturn(aclFindBy);

        when(aclFindBy.findOne()).thenReturn(aclToReturn);

        when(testService.getTest(Matchers.anyLong(), Matchers.anyBoolean())).thenReturn(testTwoQuestionsToReturn);

        // set up approval process config and repository
        ApprovalProcessConfig approvalProcessConfigToReturn = new ApprovalProcessConfig();
        List<ApprovalProcessConfig> approvalProcessConfigsToReturn = new ArrayList<>();
        approvalProcessConfigsToReturn.add(approvalProcessConfigToReturn);
        approvalProcessConfigToReturn.setId(approvalProcessConfigId);
        approvalProcessConfigToReturn.setApprovalLevel(1L);
        approvalProcessConfigToReturn.setTargetId(programId);
        approvalProcessConfigToReturn.setTargetTable(PinnacleConstants.PROGRAM_TABLE);
        approvalProcessConfigToReturn.setApprovalProcessTypeCode("APPR");
        approvalProcessConfigToReturn.setApprovalTypeCode("PERSON");
        approvalProcessConfigToReturn.setApproverPaxId(paxId);
        when(approvalProcessConfigRepository.findBy()).thenReturn(approvalProcessConfigFindBy);
        when(approvalProcessConfigFindBy.where(Matchers.anyString())).thenReturn(approvalProcessConfigFindBy);
        when(approvalProcessConfigFindBy.and(Matchers.anyString())).thenReturn(approvalProcessConfigFindBy);
        when(approvalProcessConfigFindBy.eq(Matchers.anyString())).thenReturn(approvalProcessConfigFindBy);
        when(approvalProcessConfigFindBy.eq(Matchers.anyLong())).thenReturn(approvalProcessConfigFindBy);
        when(approvalProcessConfigFindBy.findAll()).thenReturn(approvalProcessConfigsToReturn);
        when(approvalProcessConfigFindBy.findOne()).thenReturn(approvalProcessConfigToReturn);

        pax = new Pax();
        pax.setPaxId(paxId);
        pax.setLanguageCode(LanguageCodeEnum.en.name());
        pax.setPreferredLocale(LocaleCodeEnum.en_US.name());
        pax.setControlNum("102950");
        pax.setFirstName("Nedd");
        pax.setLastName("Stark");
        pax.setPaxTypeCode(PaxTypeCode.PERSON.name());
        when(paxRepository.findBy()).thenReturn(paxFindBy);
        when(paxFindBy.where(ProjectConstants.PAX_ID)).thenReturn(paxFindBy);
        when(paxFindBy.eq(Matchers.anyLong())).thenReturn(paxFindBy);
        when(paxFindBy.findOne()).thenReturn(pax);

        // set up program misc repository and return data
        ProgramMisc programMiscToReturn = new ProgramMisc();
        programMiscToReturn.setProgramId(programId);
        when(cnxtProgramMiscRepository.findBy()).thenReturn(programMiscFindBy);
        when(programMiscFindBy.where(Matchers.anyString())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.and(Matchers.anyString())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.eq(Matchers.anyString())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.eq(Matchers.anyLong())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.findOne()).thenReturn(programMiscToReturn);

        dummyProgramMiscs = new ArrayList<>();
        audienceProgramMisc = createProgramMisc(programId, VfName.AUDIENCE.name(), PinnacleConstants.INDIVIDUAL_AUDIENCE);
        dummyProgramMiscs.add(audienceProgramMisc);
        newsfeedVisNominatedProgramMisc = createProgramMisc(programId, VfName.NEWSFEED_VISIBILITY_NOMINATED.name(), "USER_PREF");
        dummyProgramMiscs.add(newsfeedVisNominatedProgramMisc);
        newsfeedVisApprovedProgramMisc = createProgramMisc(programId, VfName.NEWSFEED_VISIBILITY_APPROVED.name(), "USER_PREF");
        dummyProgramMiscs.add(newsfeedVisApprovedProgramMisc);
        pinnacleNotificationRecipientProgramMisc = createProgramMisc(programId, VfName.PINNACLE_NOTIFICATION_RECIPIENT.name(), "TRUE");
        dummyProgramMiscs.add(pinnacleNotificationRecipientProgramMisc);
        pinnacleNotificationMgr = createProgramMisc(programId, VfName.PINNACLE_NOTIFICATION_RECIPIENT_MGR.name(), "TRUE");
        dummyProgramMiscs.add(pinnacleNotificationMgr);
        pinnacleNotification2ndMgr = createProgramMisc(programId, VfName.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.name(), "TRUE");
        dummyProgramMiscs.add(pinnacleNotification2ndMgr);
        individualAwardAmountProgramMisc = createProgramMisc(programId, VfName.PINNACLE_INDIVIDUAL_AWARD_AMOUNT.name(), "10.0");
        dummyProgramMiscs.add(individualAwardAmountProgramMisc);
        teamAwardAmountPerPersonProgramMisc = createProgramMisc(programId, VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON.name(), "20.0");
        teamAwardAmountPerTeamProgramMisc = createProgramMisc(programId, VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM.name(), "30.0");
        dummyNotifications = new ArrayList<>();
        dummyNotifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT.name());
        dummyNotifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT_MGR.name());
        dummyNotifications.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.name());

        dummyProgramActivities = new ArrayList<>();
        nomPeriod1ProgramActivity = createProgramActivity(programId, "2018-05-01", "2018-05-31");
        nomPeriod2ProgramActivity = createProgramActivity(programId, "2018-06-01", "2018-06-30");
        dummyProgramActivities.add(nomPeriod1ProgramActivity);
        dummyProgramActivities.add(nomPeriod2ProgramActivity);

        when(cnxtProgramMiscRepository.findByProgramId(programId)).thenReturn(dummyProgramMiscs);
        when(cnxtProgramActivityRepository.findByProgramId(programId)).thenReturn(dummyProgramActivities);
    }

    @Test
    public void save_program_misc_data_for_team_audience() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testTwoQuestionsToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(teamAudience, nominationInstructions, questions, notifications, teamAwardPerIndividual, approvals,
                twoNominationPeriods, newsfeedVisibilityNominated, newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);
        verify(cnxtProgramMiscRepository, Mockito.times(7)).save(programMiscArgumentCaptor.capture());
        List<ProgramMisc> arguments = programMiscArgumentCaptor.getAllValues();
        ProgramMisc saveMgrNotification = arguments.get(0);
        ProgramMisc save2ndMgrNotification = arguments.get(1);
        ProgramMisc saveRecipientNotification = arguments.get(2);
        ProgramMisc saveAudience = arguments.get(3);
        ProgramMisc saveNewsfeedVisibilityNominated = arguments.get(4);
        ProgramMisc saveNewsfeedVisibilityApproved = arguments.get(5);
        ProgramMisc saveTeamAwardPerPerson = arguments.get(6);

        Assert.assertTrue(VfName.AUDIENCE.name().equalsIgnoreCase(saveAudience.getVfName()));
        Assert.assertTrue(teamAudience.equalsIgnoreCase(saveAudience.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT_MGR.name().equalsIgnoreCase(saveMgrNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(saveMgrNotification.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.name().equalsIgnoreCase(save2ndMgrNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(save2ndMgrNotification.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT.name().equalsIgnoreCase(saveRecipientNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(saveRecipientNotification.getMiscData()));
        Assert.assertTrue(VfName.NEWSFEED_VISIBILITY_NOMINATED.name().equalsIgnoreCase(saveNewsfeedVisibilityNominated.getVfName()));
        Assert.assertTrue(program.getPinnacle().getNewsfeedVisibilityNominated().equalsIgnoreCase(saveNewsfeedVisibilityNominated.getMiscData()));
        Assert.assertTrue(VfName.NEWSFEED_VISIBILITY_APPROVED.name().equalsIgnoreCase(saveNewsfeedVisibilityApproved.getVfName()));
        Assert.assertTrue(program.getPinnacle().getNewsfeedVisibilityNominated().equalsIgnoreCase(saveNewsfeedVisibilityApproved.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON.name().equalsIgnoreCase(saveTeamAwardPerPerson.getVfName()));
        Assert.assertTrue(program.getPinnacle().getAwards().getTeamAmountPerPerson().toString().equalsIgnoreCase(saveTeamAwardPerPerson.getMiscData()));
    }

    @Test
    public void save_program_misc_data_for_both_audience() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testTwoQuestionsToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(bothAudience, nominationInstructions, questions, notifications, awardPerIndividualAndTeamPerTeam, approvals,
                twoNominationPeriods, newsfeedVisibilityNominated, newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);
        verify(cnxtProgramMiscRepository, times(8)).save(programMiscArgumentCaptor.capture());
        List<ProgramMisc> arguments = programMiscArgumentCaptor.getAllValues();
        ProgramMisc saveMgrNotification = arguments.get(0);
        ProgramMisc save2ndMgrNotification = arguments.get(1);
        ProgramMisc saveRecipientNotification = arguments.get(2);
        ProgramMisc saveAudience = arguments.get(3);
        ProgramMisc saveNewsfeedVisibilityNominated = arguments.get(4);
        ProgramMisc saveNewsfeedVisibilityApproved = arguments.get(5);
        ProgramMisc saveIndividualAward = arguments.get(6);
        ProgramMisc saveTeamAwardPerTeam = arguments.get(7);

        Assert.assertTrue(VfName.AUDIENCE.name().equalsIgnoreCase(saveAudience.getVfName()));
        Assert.assertTrue(bothAudience.equalsIgnoreCase(saveAudience.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT_MGR.name().equalsIgnoreCase(saveMgrNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(saveMgrNotification.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR.name().equalsIgnoreCase(save2ndMgrNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(save2ndMgrNotification.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_NOTIFICATION_RECIPIENT.name().equalsIgnoreCase(saveRecipientNotification.getVfName()));
        Assert.assertTrue(Boolean.parseBoolean(saveRecipientNotification.getMiscData()));
        Assert.assertTrue(VfName.NEWSFEED_VISIBILITY_NOMINATED.name().equalsIgnoreCase(saveNewsfeedVisibilityNominated.getVfName()));
        Assert.assertTrue(program.getPinnacle().getNewsfeedVisibilityNominated().equalsIgnoreCase(saveNewsfeedVisibilityNominated.getMiscData()));
        Assert.assertTrue(VfName.NEWSFEED_VISIBILITY_APPROVED.name().equalsIgnoreCase(saveNewsfeedVisibilityApproved.getVfName()));
        Assert.assertTrue(program.getPinnacle().getNewsfeedVisibilityNominated().equalsIgnoreCase(saveNewsfeedVisibilityApproved.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_INDIVIDUAL_AWARD_AMOUNT.name().equalsIgnoreCase(saveIndividualAward.getVfName()));
        Assert.assertTrue(program.getPinnacle().getAwards().getIndividualAmount().toString().equalsIgnoreCase(saveIndividualAward.getMiscData()));
        Assert.assertTrue(VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM.name().equalsIgnoreCase(saveTeamAwardPerTeam.getVfName()));
        Assert.assertTrue(program.getPinnacle().getAwards().getTeamAmountPerTeam().toString().equalsIgnoreCase(saveTeamAwardPerTeam.getMiscData()));
    }

    @Test
    public void save_approval_process_config_data() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testTwoQuestionsToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, questions, notifications, individualAward, approvals,
                twoNominationPeriods, newsfeedVisibilityNominated, newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);
        verify(cnxtProgramMiscRepository, times(7)).save(Matchers.any(ProgramMisc.class));
        verify(approvalProcessConfigRepository, times(1)).save(approvalProcessConfigArgumentCaptor.capture());
        List<ApprovalProcessConfig> arguments = approvalProcessConfigArgumentCaptor.getAllValues();
        ApprovalProcessConfig saveApprover = arguments.get(0);

        Assert.assertEquals(programId, saveApprover.getTargetId());
        Assert.assertTrue(saveApprover.getTargetTable().equalsIgnoreCase("PROGRAM"));
        Assert.assertEquals(approvals.get(0).getPax().getPaxId(), saveApprover.getApproverPaxId());
        Assert.assertEquals(approvals.get(0).getLevel(), saveApprover.getApprovalLevel());
    }

    @Test
    public void save_one_nomination_period() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testTwoQuestionsToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, questions, notifications, individualAward, approvals,
                singleNominationPeriod, newsfeedVisibilityNominated, newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);
        verify(cnxtProgramMiscRepository, times(7)).save(Matchers.any(ProgramMisc.class));
        verify(cnxtProgramActivityRepository, times(1)).save(programActivityArgumentCaptor.capture());
        List<ProgramActivity> arguments = programActivityArgumentCaptor.getAllValues();
        ProgramActivity saveNominationPeriod = arguments.get(0);

        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(0).getFromDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod.getFromDate().toString()));
        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(0).getThruDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod.getThruDate().toString()));
    }

    @Test
    public void save_multiple_nomination_periods() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testTwoQuestionsToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, questions, notifications, individualAward, approvals,
                twoNominationPeriods, newsfeedVisibilityNominated, newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);
        verify(cnxtProgramMiscRepository, times(7)).save(Matchers.any(ProgramMisc.class));
        verify(cnxtProgramActivityRepository, times(2)).save(programActivityArgumentCaptor.capture());

        List<ProgramActivity> arguments = programActivityArgumentCaptor.getAllValues();
        ProgramActivity saveNominationPeriod1 = arguments.get(0);
        ProgramActivity saveNominationPeriod2 = arguments.get(1);

        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(0).getFromDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod1.getFromDate().toString()));
        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(0).getThruDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod1.getThruDate().toString()));
        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(1).getFromDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod2.getFromDate().toString()));
        Assert.assertTrue(program.getPinnacle().getNominationPeriods().get(1).getThruDateLocalDate().toString().equalsIgnoreCase(saveNominationPeriod2.getThruDate().toString()));
    }


    @Test
    public void save_nomination_instructions_and_one_question() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testOneQuestionToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, singleQuestion,
                notifications, individualAward, approvals, singleNominationPeriod, newsfeedVisibilityNominated,
                newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);

        verify(testAdminService, times(1)).createTest(testArgumentCaptor.capture());
        verify(aclRepository, times(1)).save(aclArgumentCaptor.capture());

        List<TestDTO> saveTestArguments = testArgumentCaptor.getAllValues();
        List<Acl> saveAclArguments = aclArgumentCaptor.getAllValues();
        Acl saveAcl = saveAclArguments.get(0);
        TestDTO saveTest = saveTestArguments.get(0);

        QuestionDTO question = saveTest.getSections().get(0).getQuestions().get(0);

        Assert.assertTrue(nominationInstructions.equalsIgnoreCase(saveTest.getDescription()));
        Assert.assertTrue(saveAcl.getSubjectTable().equalsIgnoreCase(PinnacleConstants.TEST_TABLE));
        Assert.assertTrue(saveAcl.getTargetTable().equalsIgnoreCase(PinnacleConstants.PROGRAM_TABLE));
        Assert.assertEquals(testId, saveAcl.getSubjectId());
        Assert.assertEquals(programId, saveAcl.getTargetId());
        Assert.assertTrue(singleQuestion.get(0).getText().equalsIgnoreCase(question.getText()));
        Assert.assertTrue(PinnacleConstants.PINNACLE.equalsIgnoreCase(question.getQuestionTypeCode()));
        Assert.assertTrue(singleQuestion.get(0).getAnswers().get(0).getUserProvided());
        assertThat(saveTestArguments.get(0).canResume(), is(true));
    }

    @Test
    public void save_nomination_instructions_and_multiple_questions() {
        when(testAdminService.createTest(Matchers.any(TestDTO.class))).thenReturn(testOneQuestionToReturn);
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, questions,
                notifications, individualAward, approvals, singleNominationPeriod, newsfeedVisibilityNominated,
                newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, postRequest);

        verify(testAdminService, times(1)).createTest(testArgumentCaptor.capture());
        verify(aclRepository, times(1)).save(aclArgumentCaptor.capture());

        List<TestDTO> saveTestArguments = testArgumentCaptor.getAllValues();
        List<Acl> saveAclArguments = aclArgumentCaptor.getAllValues();
        Acl saveAcl = saveAclArguments.get(0);
        TestDTO saveTest = saveTestArguments.get(0);

        QuestionDTO question1 = saveTest.getSections().get(0).getQuestions().get(0);
        QuestionDTO question2 = saveTest.getSections().get(0).getQuestions().get(1);

        Assert.assertTrue(nominationInstructions.equalsIgnoreCase(saveTest.getDescription()));
        Assert.assertTrue(saveAcl.getSubjectTable().equalsIgnoreCase(PinnacleConstants.TEST_TABLE));
        Assert.assertTrue(saveAcl.getTargetTable().equalsIgnoreCase(PinnacleConstants.PROGRAM_TABLE));
        Assert.assertEquals(testId, saveAcl.getSubjectId());
        Assert.assertEquals(programId, saveAcl.getTargetId());
        Assert.assertTrue(questions.get(0).getText().equalsIgnoreCase(question1.getText()));
        Assert.assertTrue(PinnacleConstants.PINNACLE.equalsIgnoreCase(question1.getQuestionTypeCode()));
        Assert.assertTrue(questions.get(0).getAnswers().get(0).getUserProvided());
        Assert.assertTrue(questions.get(1).getText().equalsIgnoreCase(question2.getText()));
        Assert.assertTrue(PinnacleConstants.PINNACLE.equalsIgnoreCase(question2.getQuestionTypeCode()));
        Assert.assertTrue(questions.get(1).getAnswers().get(0).getUserProvided());
        assertThat(saveTestArguments.get(0).canResume(), is(true));
    }

    @Test
    public void update_pinnacle() {
        ProgramRequestDTO program = setupProgramRequestDTO(individualAudience, nominationInstructions, questions,
                notifications, individualAward, approvals, singleNominationPeriod, newsfeedVisibilityNominated,
                newsfeedVisibilityApproved);
        pinnacleService.insertUpdatePinnacle(program.getProgramId(), program, putRequest);
        verify(testAdminService, times(1)).updateTest(Matchers.any(TestDTO.class));
        verify(cnxtProgramMiscRepository, times(7)).save(Matchers.any(ProgramMisc.class));
        verify(cnxtProgramActivityRepository, times(1)).deleteByProgramId(programId);
    }

    @Test
    public void get_pinnacle() {
        when(testService.getTest(testId, false)).thenReturn(testTwoQuestionsToReturn);
        PinnacleDTO savedPinnacle = pinnacleService.getPinnacle(programId);
        verify(cnxtProgramMiscRepository, times(1)).findByProgramId(programId);
        verify(cnxtProgramActivityRepository, times(1)).findByProgramId(programId);
        verify(aclFindBy, times(1)).findOne();
        verify(testService, times(1)).getTest(testId, false);
        verify(approvalProcessConfigFindBy, times(1)).findAll();
        verify(paxFindBy, times(1)).findOne();

        Assert.assertNotNull(savedPinnacle);
        Assert.assertTrue(PinnacleConstants.INDIVIDUAL_AUDIENCE.equalsIgnoreCase(savedPinnacle.getAudience()));
        Assert.assertTrue("USER_PREF".equalsIgnoreCase(savedPinnacle.getNewsfeedVisibilityNominated()));
        Assert.assertTrue("USER_PREF".equalsIgnoreCase(savedPinnacle.getNewsfeedVisibilityApproved()));
        Assert.assertTrue(savedPinnacle.getNotifications().containsAll(dummyNotifications));
        Assert.assertEquals(individualAward.getIndividualAmount(), savedPinnacle.getAwards().getIndividualAmount());
        Assert.assertTrue(convertStringToDate(savedPinnacle.getNominationPeriods().get(0).getFromDateLocalDate().toString()).equals(dummyProgramActivities.get(0).getFromDate()));
        Assert.assertTrue(convertStringToDate(savedPinnacle.getNominationPeriods().get(0).getThruDateLocalDate().toString()).equals(dummyProgramActivities.get(0).getThruDate()));
        Assert.assertTrue(convertStringToDate(savedPinnacle.getNominationPeriods().get(1).getFromDateLocalDate().toString()).equals(dummyProgramActivities.get(1).getFromDate()));
        Assert.assertTrue(convertStringToDate(savedPinnacle.getNominationPeriods().get(1).getThruDateLocalDate().toString()).equals(dummyProgramActivities.get(1).getThruDate()));
        Assert.assertTrue(testTwoQuestionsToReturn.getDescription().equalsIgnoreCase(savedPinnacle.getNominationInstructions()));
        Assert.assertTrue(testTwoQuestionsToReturn.getSections().get(0).getQuestions().get(0).getText()
                .equalsIgnoreCase(testTwoQuestions.getSections().get(0).getQuestions().get(0).getText()));
        Assert.assertTrue(testTwoQuestionsToReturn.getSections().get(0).getQuestions().get(1).getText()
                .equalsIgnoreCase(testTwoQuestions.getSections().get(0).getQuestions().get(1).getText()));
        Assert.assertEquals(paxId, savedPinnacle.getApprovals().get(0).getPax().getPaxId());
    }

    private List<String> getPinnacleVfNames() {
        List<String> vfNames = new ArrayList<>();
        vfNames.add(VfName.AUDIENCE.name());
        vfNames.add(VfName.NEWSFEED_VISIBILITY_NOMINATED.name());
        vfNames.add(VfName.NEWSFEED_VISIBILITY_APPROVED.name());
        vfNames.add(VfName.PINNACLE_NOTIFICATION_RECIPIENT.name());
        vfNames.add(VfName.NOTIFICATION_RECIPIENT.name());
        vfNames.add(VfName.NOTIFICATION_RECIPIENT_MGR.name());
        vfNames.add(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.name());
        vfNames.add(VfName.NOTIFICATION_SUBMITTER.name());
        vfNames.add(VfName.PINNACLE_INDIVIDUAL_AWARD_AMOUNT.name());
        vfNames.add(VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_PERSON.name());
        vfNames.add(VfName.PINNACLE_TEAM_AWARD_AMOUNT_PER_TEAM.name());
        return vfNames;
    }

    private ProgramRequestDTO setupProgramRequestDTO(String audience, String nominationInstructions, List<QuestionDTO> questions,
                                                     List<String> notifications, AwardsDTO awards, List<ApprovalDTO> approvals,
                                                     List<NominationPeriodDTO> nominationPeriods, String newsfeedVisibilityNominated,
                                                     String newsfeedVisibilityApproved) {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        programRequestDTO.setProgramId(programId);
        PinnacleDTO pinnacleDTO = new PinnacleDTO();
        pinnacleDTO.setAudience(audience);
        pinnacleDTO.setNominationInstructions(nominationInstructions);
        pinnacleDTO.setQuestions(questions);
        pinnacleDTO.setNotifications(notifications);
        pinnacleDTO.setAwards(awards);
        pinnacleDTO.setApprovals(approvals);
        pinnacleDTO.setNominationPeriods(nominationPeriods);
        pinnacleDTO.setNewsfeedVisibilityNominated(newsfeedVisibilityNominated);
        pinnacleDTO.setNewsfeedVisibilityApproved(newsfeedVisibilityApproved);

        programRequestDTO.setPinnacle(pinnacleDTO);

        return programRequestDTO;
    }

    private AwardsDTO createAwards(Double individualAward, Double teamAwardPerPerson, Double teamAwardPerTeam) {
        AwardsDTO awards = new AwardsDTO();

        awards.setIndividualAmount(individualAward);
        awards.setTeamAmountPerPerson(teamAwardPerPerson);
        awards.setTeamAmountPerTeam(teamAwardPerTeam);

        return awards;
    }

    private ApprovalDTO createApproval(Long paxId) {
        ApprovalDTO approval = new ApprovalDTO();
        EmployeeDTO pax = new EmployeeDTO();
        pax.setPaxId(paxId);
        approval.setLevel(1L);
        approval.setType("PERSON");
        approval.setPax(pax);
        return approval;
    }

    private NominationPeriodDTO createNominationPeriod(String fromDate, String thruDate) {
        NominationPeriodDTO nominationPeriod = new NominationPeriodDTO();
        if (fromDate != null && !fromDate.isEmpty()) {
            nominationPeriod.setFromDate(fromDate);
        }
        if (thruDate != null && !thruDate.isEmpty()) {
            nominationPeriod.setThruDate(thruDate);
        }
        return nominationPeriod;
    }

    private TestDTO createTest(String nominationInstructions, List<QuestionDTO> questions) {
        // question fields
        for (QuestionDTO question : questions) {
            question.setQuestionTypeCode(PinnacleConstants.PINNACLE);
            question.setMaxAnswers(PinnacleConstants.MAX_ANSWERS);
            question.setRequiredAnswers(PinnacleConstants.REQ_ANSWERS);
        }

        // section fields
        SectionDTO section = new SectionDTO();
        section.setQuestions(questions);
        section.setNumberOfQuestions(questions.size());
        List<SectionDTO> sections = new ArrayList<>();
        sections.add(section);

        // test fields
        TestDTO test = new TestDTO();
        test.setDescription(nominationInstructions);
        test.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        test.setSections(sections);
        test.setName(PinnacleConstants.PINNACLE);

        return test;
    }

    private ProgramMisc createProgramMisc(Long programId, String vfName, String miscData) {
        ProgramMisc programMisc = new ProgramMisc();

        programMisc.setProgramId(programId);
        programMisc.setVfName(vfName);
        programMisc.setMiscData(miscData);

        return programMisc;
    }

    private ProgramActivity createProgramActivity(Long programId, String fromDate, String thruDate) {
        ProgramActivity programActivity = new ProgramActivity();

        programActivity.setProgramId(programId);
        programActivity.setFromDate(convertStringToDate(fromDate));
        programActivity.setThruDate(convertStringToDate(thruDate));

        return programActivity;
    }

    private Date convertStringToDate(String date) {
        return Date.from(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
