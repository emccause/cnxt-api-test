package com.maritz.culturenext.program.services;

import java.util.*;

import javax.inject.Inject;

import com.google.common.collect.Lists;
import com.maritz.culturenext.enums.ProgramCategoryEnum;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramEligibilityDTO;
import com.maritz.culturenext.program.dto.ProgramEligibilityQueryDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.services.ProgramEligibilityQueryService;
import com.maritz.test.AbstractDatabaseTest;

public class ProgramEligibilityQueryServiceTest extends AbstractDatabaseTest {

    private static final Long TEST_PROGRAM_3 = 3L;

    @Inject private AuthenticationService authenticationService;
    @Inject private ProgramEligibilityQueryService programEligibilityQueryService;
    @Inject private ProgramEligibilityQueryDao programEligibilityQueryDao;
    @Inject private Security security;
    
    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
    }

    @Test
    public void test_get_eligible_programs_null_group_id() throws Exception {
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setPaxId(1L);
        List<GroupMemberStubDTO> groupMemberStubDTOList = new ArrayList<>();
        groupMemberStubDTOList.add(0, groupMemberStubDTO);
        groupMemberStubDTOList.add(1, groupMemberStubDTO);
        ProgramEligibilityQueryDTO programEligibilityQueryDTO = new ProgramEligibilityQueryDTO();
        programEligibilityQueryDTO.setReceivers(groupMemberStubDTOList);
        programEligibilityQueryDTO.setProgramCategory(Lists.newArrayList(ProgramCategoryEnum.RECOGNITION.name()));

        List<ProgramSummaryDTO> programSummaryDTOList =
                programEligibilityQueryService.getEligiblePrograms(security.getPaxId(), programEligibilityQueryDTO, "en_US");
        Assert.assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_get_eligible_programs_null_pax_id() throws Exception {
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        groupMemberStubDTO.setGroupId(1L);
        List<GroupMemberStubDTO> groupMemberStubDTOList = new ArrayList<>();
        groupMemberStubDTOList.add(0, groupMemberStubDTO);
        groupMemberStubDTOList.add(1, groupMemberStubDTO);
        ProgramEligibilityQueryDTO programEligibilityQueryDTO = new ProgramEligibilityQueryDTO();
        programEligibilityQueryDTO.setReceivers(groupMemberStubDTOList);
        programEligibilityQueryDTO.setProgramCategory(Lists.newArrayList(ProgramCategoryEnum.RECOGNITION.name()));

        List<ProgramSummaryDTO> programSummaryDTOList =
                programEligibilityQueryService.getEligiblePrograms(security.getPaxId(), programEligibilityQueryDTO, "en_US");
        Assert.assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_get_eligible_programs_null_group_and_pax() throws Exception {
        GroupMemberStubDTO groupMemberStubDTO = new GroupMemberStubDTO();
        List<GroupMemberStubDTO> groupMemberStubDTOList = new ArrayList<>();
        groupMemberStubDTOList.add(0, groupMemberStubDTO);
        groupMemberStubDTOList.add(1, groupMemberStubDTO);
        ProgramEligibilityQueryDTO programEligibilityQueryDTO = new ProgramEligibilityQueryDTO();
        programEligibilityQueryDTO.setReceivers(groupMemberStubDTOList);
        programEligibilityQueryDTO.setProgramCategory(Lists.newArrayList(ProgramCategoryEnum.RECOGNITION.name()));

        List<ProgramSummaryDTO> programSummaryDTOList =
                programEligibilityQueryService.getEligiblePrograms(security.getPaxId(), programEligibilityQueryDTO, "en_US");
        Assert.assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_get_all_eligible_programs_for_pax() throws Exception {
        String programTypeString = "ECARD_ONLY,MANAGER_DISCRETIONARY,POINT_LOAD,PEER_TO_PEER,AWARD_CODE";
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryService
                .getAllEligibleProgramsForPax(security.getPaxId(), null, programTypeString, null);
        Assert.assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_get_all_eligible_programs_for_pax_AWARD_CODE() throws Exception {
        String programTypeString = "AWARD_CODE";
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryService
                .getAllEligibleProgramsForPax(security.getPaxId(), null, programTypeString, null);
        for(ProgramSummaryDTO programSummaryDTO :  programSummaryDTOList){
            Assert.assertTrue(programSummaryDTO.getProgramType().equals("AWARD_CODE"));
        }
    }

    @Test
    public void test_get_all_eligible_programs_for_pax_MGRD_PTP() throws Exception {
        String programTypeString = "MANAGER_DISCRETIONARY,PEER_TO_PEER";
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryService
                .getAllEligibleProgramsForPax(security.getPaxId(), "en_US", programTypeString, "GIVE");
        for(ProgramSummaryDTO programSummaryDTO :  programSummaryDTOList){
            Assert.assertTrue(programSummaryDTO.getProgramType().equals("MANAGER_DISCRETIONARY") || programSummaryDTO.getProgramType().equals("PEER_TO_PEER"));
        }
    }

    @Test
    public void test_get_all_eligible_programs_for_pax_ECARD() throws Exception {
        String programTypeString = "ECARD_ONLY";
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryService
                .getAllEligibleProgramsForPax(security.getPaxId(), "fr_CA", programTypeString, "RECEIVE");
        for(ProgramSummaryDTO programSummaryDTO :  programSummaryDTOList){
            Assert.assertTrue(programSummaryDTO.getProgramType().equals("ECARD_ONLY"));
        }
    }

    @Test
    public void test_get_all_eligible_programs_for_pax_null_program_list() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryService
                .getAllEligibleProgramsForPax(security.getPaxId(), null, "", null);
        Assert.assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_visibility_should_not_affect_give_rec_process() throws Exception {
        authenticationService.authenticate(TestConstants.USER_MADRIDO);

        // Get a list of all ADMIN programs
        List<Long> adminIdList = programEligibilityQueryDao.getProgramsWithAdminVisibility();

        // Get my program list
        List<ProgramSummaryDTO> myProgramsList = programEligibilityQueryDao
                .getAllEligibleProgramsForPax(security.getPaxId(), ProgramTypeEnum.getAllProgramTypes(), null);

        // Make sure visibility should not affect give rec process.
        for (ProgramSummaryDTO programSummary : myProgramsList) {
            if (programSummary.getProgramId() == TestConstants.ID_1) { //admin
                Assert.assertFalse(adminIdList.contains(new Long(programSummary.getProgramId())));
            }
            if (programSummary.getProgramId() == TEST_PROGRAM_3) {//public
                Assert.assertFalse(!adminIdList.contains(new Long(programSummary.getProgramId())));
            }
        }
    }
    
    @Test
    public void test_pax_program_list_does_not_contain_un_wanted_programs() throws Exception {
        
        //Get my program list
        List<ProgramSummaryDTO> myProgramsList = 
                programEligibilityQueryDao.getAllEligibleProgramsForPax(security.getPaxId(), 
                        Arrays.asList(ProgramTypeEnum.MANAGER_DISCRETIONARY.name()), null);
        
        //Make sure no admin programs are in my list
        for (ProgramSummaryDTO programSummary : myProgramsList) {
            Assert.assertTrue(ProgramTypeEnum
                    .MANAGER_DISCRETIONARY.name().equalsIgnoreCase(programSummary.getProgramType()));
        }        
    }
    
    @Test
    public void test_recognition_eligibility() throws Exception {
        
        ProgramEligibilityDTO printableDTO = programEligibilityQueryService.getRecognitionEligibility(
                TestConstants.PAX_HARWELLM);
        Assert.assertTrue("Should be able to give from a rec program", printableDTO.getGive().getRecognition());
        Assert.assertTrue("Should be able to give from an award_code program that is printable",
                printableDTO.getGive().getAwardCodeCert());
        Assert.assertFalse("Should not be able to give from an award_code program that is downloadable", 
                printableDTO.getGive().getAwardCodeDownload());
        
        ProgramEligibilityDTO downloadableDTO = programEligibilityQueryService.getRecognitionEligibility(
                TestConstants.PAX_MUDDAM);
        Assert.assertTrue("Should be able to give from a rec program", downloadableDTO.getGive().getRecognition());
        Assert.assertFalse("Should not be able to give from an award_code program that is printable", 
                downloadableDTO.getGive().getAwardCodeCert());
        Assert.assertTrue("Should be able to give from an award_code program that is downloadable",
                downloadableDTO.getGive().getAwardCodeDownload());
        
        ProgramEligibilityDTO bothDTO = programEligibilityQueryService.getRecognitionEligibility(
                TestConstants.PAX_HAGOPIWL);
        Assert.assertTrue("Should be able to give from a rec program", bothDTO.getGive().getRecognition());
        Assert.assertTrue("Should be able to give from an award_code program that is printable",
                bothDTO.getGive().getAwardCodeCert());
        Assert.assertTrue("Should be able to give from an award_code program that is downloadable",
                bothDTO.getGive().getAwardCodeDownload());
        
    }
}

    
