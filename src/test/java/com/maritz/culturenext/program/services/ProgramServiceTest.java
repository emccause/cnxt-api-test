package com.maritz.culturenext.program.services;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.ProgramTypeCode;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;

import com.maritz.culturenext.enums.ApprovalTypeEnum;
import com.maritz.culturenext.enums.AwardTierType;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.entity.ProgramAwardTierReminder;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.EligibilityDTO;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;
import com.maritz.culturenext.program.dto.EligibilityStubDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.social.like.service.LikeService;
import com.maritz.culturenext.budget.dto.ApprovalDTO;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.test.AbstractDatabaseTest;

import org.apache.commons.collections4.ListUtils;
import org.junit.Test;
import org.springframework.jdbc.UncategorizedSQLException;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ProgramServiceTest extends AbstractDatabaseTest {
    
    @Inject private AuthenticationService authenticationService;
    @Inject private ConcentrixDao<Program> programDao;
    @Inject private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Inject private ConcentrixDao<ProgramAwardTierReminder> programAwardTierReminderDao;
    @Inject private LikeService likeService;
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ProgramService programService;
    @Inject private Security security;
    @Inject private SysUserRepository sysUserRepository;

    private static final String TEST_PROGRAM_NAME = "JUNIT Test Program Name";
    private static final String TEST_PROGRAM_NAME_TZ = "JUNIT Test Program Name Time Zone";
    private static final String TEST_SHORT_DESC = "JUNIT test short description";
    private static final String TEST_LONG_DESC = "JUNIT test long description";
    private static final String TEST_DUPLICATE_PROGRAM_NAME = "CITI Thank You";
    private static final String INVALID_DATE = "INVALID_DATE";

    private static final Long VALID_GROUP_ID_1 = 1414L;
    private static final Long VALID_GROUP_ID_2 = 1415L;
    private static final Long VALID_PAX_ID_1 = 4417L;
    private static final Long VALID_PAX_ID_2 = 5961L;

    private static final Long INVALID_PAX_ID_1 = 999999L;
    private static final Long INVALID_GROUP_ID_1 = 999999L;
    private static final Long INACTIVE_PAX_ID = 13997L;
    private static final Long INACTIVE_GROUP_ID = 27L;
    private static final Long HIERARCHY_GROUP_ID = 8L;
    private static final Long PERSONAL_GROUP_ID = 1446L;

    private static final Long NO_AWARD_TIER_PROGRAM_ID = 601L;
    private static final Long SEVERAL_AWARD_TIER_PROGRAM_ID = 599L;

    private static final Long TEST_PROGRAM_ID_EMPTY_ELIGIBILITY = 3534L;
    private static final Long TEST_PROGRAM_ID_VALID_ELIGIBILITY = 3506L;

    private static final Long PROGRAM_EMPTY_BUDGET_ASSIGNMENTS = 931L;
    private static final Long PROGRAM_VALID_BUDGET_ASSIGNMENTS = 932L;

    private static final Long ENDED_PROGRAM = 3323L;

    private static final Long NON_ASSOCIATED_BUDGET_ID = 738L;
    private static final Long SIMPLE_BUDGET_NO_GIVING_LIMIT = 862L;
    private static final Long INACTIVE_BUDGET_ID = 746L;
    private static final Long INVALID_BUDGET_ID = 999999L;

    private static final Long PROGRAM_NOT_USED = 777L;
    private static final Long PROGRAM_USED_IN_NOMINATION = 778L;
    private static final Long PROGRAM_USED_IN_DISCRETIONARY = 779L;
    private static final Long PROGRAM_BUDGET_ALLOCATED_DIFFERENT_TYPE_CODE = 780L;

    // award Tiers
    private static final Long TEST_APPROVAL_PERSON_PAX_ID_2 = 11830L;
    private static final String AWARD_TIER_DELETE_TEST_PROGRAM_NAME = "AWARD_TIER_DELETE_TEST_PROGRAM";

    // likes
    private static final Long LIKE_PROGRAM_ID = 5831L;

    private static final String ERROR_MISSING_BUDGET = "MISSING_BUDGET";
    private static final String ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED = "BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED";
    private static final String ERROR_RECEIVING_ELIGIBILITY_REQUIRED = "RECEIVING_ELIGIBILITY_REQUIRED";

    //days
    private static final int ONE_DAY = 1;
    private static final int THREE_DAY = 3;

    private static List<Integer> MILESTONE_REMINDERS = new ArrayList<>();

    @Test
    public void test_get_program_list_by_all_statuses() throws Exception {
        assertNotNull(programService);
    
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus("", null, ProgramTypeEnum.PEER_TO_PEER.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
    }

    @Test
    public void test_get_program_list_by_active_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = 
                programService.getProgramListByStatus(StatusTypeCode.ACTIVE.name(), null,
                        ProgramTypeEnum.PEER_TO_PEER.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        
        for (ProgramSummaryDTO programSummaryDTO : programSummaryDTOList) {
            assertTrue(programSummaryDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()));
            assertTrue(programSummaryDTO.getProgramType().equals(ProgramTypeEnum.PEER_TO_PEER.name()));
        }
    }

    @Test
    public void test_get_program_list_by_active_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ACTIVE.name(), LocaleCodeEnum.it_IT.name(), 
                        ProgramTypeEnum.PEER_TO_PEER.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()));
        }
        assertTrue(programSummaryDTOList.get(0).getProgramName().equals("Programma di riconoscimento dei premi 2015"));
    }

    @Test
    public void test_get_program_list_by_scheduled_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.SCHEDULED.name(), null, 
                        ProgramTypeEnum.PEER_TO_PEER.name());
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.SCHEDULED.toString()));
        }
    }

    @Test
    public void test_get_program_list_by_scheduled_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.SCHEDULED.name(), LocaleCodeEnum.de_DE.name(), 
                        ProgramTypeEnum.PEER_TO_PEER.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        
        logger.warn("programSummaryDTOList.size:"+programSummaryDTOList.size());
        for (ProgramSummaryDTO programSummaryDTO : programSummaryDTOList) {
            assertTrue(programSummaryDTO.getStatus().equals(StatusTypeCode.SCHEDULED.name()));
            logger.warn("program Name:"+ programSummaryDTO.getProgramName());
        }
        assertTrue(programSummaryDTOList.get(0).getProgramName().equals("Programm verwendet testen - Yukio"));
    }

    @Test
    public void test_get_program_list_by_ended_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ENDED.name(), null, ProgramTypeEnum.PEER_TO_PEER.name());
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ENDED.toString()));
        }
    }

    @Test
    public void test_get_program_list_by_ended_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ENDED.name(), LocaleCodeEnum.it_IT.name(), 
                        ProgramTypeEnum.PEER_TO_PEER.name());
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ENDED.name()));
        }
        assertTrue(programSummaryDTOList.get(0).getProgramName().equals("Programma di riconoscimento dei premi 2015 sviluppatore di prova 199911"));
    }

    @Test
    public void test_get_program_list_by_archived_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ARCHIVED.name(), null,
                        ProgramTypeEnum.MANAGER_DISCRETIONARY.name());
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ARCHIVED.toString()));
        }
    }

    @Test
    public void test_get_program_list_by_archived_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ARCHIVED.name(), LocaleCodeEnum.de_DE.name(),
                        ProgramTypeEnum.MANAGER_DISCRETIONARY.name());
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ARCHIVED.name()));
        }
        assertTrue(programSummaryDTOList.get(0).getProgramName().equals("Entwurf Hervorragende Leistung"));
    }

    @Test
    public void test_get_program_list_by_suspended_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.SUSPENDED.name(), null, 
                        ProgramTypeEnum.MANAGER_DISCRETIONARY.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
        
        for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
            assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.SUSPENDED.name()));
        }
    }

    @Test
    public void test_get_program_list_by_suspended_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.SUSPENDED.name(), LocaleCodeEnum.de_DE.name(), null);
        if (programSummaryDTOList != null && programSummaryDTOList.size() > 0) {
            for (ProgramSummaryDTO ProgramSummaryDTO : programSummaryDTOList) {
                assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.SUSPENDED.name()));
            }
            assertTrue(programSummaryDTOList.get(0).getProgramName().equals("3Cheers Führen Verweisung"));
        }
    }

    @Test
    public void test_get_program_list_by_else_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = new ArrayList<>();
        try {
            programSummaryDTOList =
                    programService.getProgramListByStatus(StatusTypeCode.FAIL.name(), null, null);
        } catch (UncategorizedSQLException e) {
            assertFalse(programSummaryDTOList.size() > 0);
        }
    }

    @Test
    public void test_get_program_list_by_else_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = new ArrayList<>();
        try {
            programSummaryDTOList =
                    programService.getProgramListByStatus(StatusTypeCode.FAIL.name(), LocaleCodeEnum.de_DE.name(), null);
        } catch (UncategorizedSQLException e) {
            assertFalse(programSummaryDTOList.size() > 0);
        }
    }

    @Test
    public void test_get_program_list_by_active_suspended_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ACTIVE + "," + StatusTypeCode.SUSPENDED, null, 
                        ProgramTypeEnum.MANAGER_DISCRETIONARY.name());
        
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size()>0);
        
        for (ProgramSummaryDTO programSummaryDTO : programSummaryDTOList) {
            assertTrue(programSummaryDTO.getStatus().equals(StatusTypeCode.SUSPENDED.name())
                    || programSummaryDTO.getStatus().equals(StatusTypeCode.ACTIVE.name()));
            assertTrue(ProgramTypeEnum.MANAGER_DISCRETIONARY.name().equals(programSummaryDTO.getProgramType()));
        }
    }

    @Test
    public void test_get_program_list_by_null_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(null, null, null);
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
    }

    @Test
    public void test_get_program_list_by_null_status_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList =
                programService.getProgramListByStatus(null, LocaleCodeEnum.de_DE.name(), null);
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
    }

    @Test
    public void test_get_program_list_by_all_statuses_translation() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = programService.getProgramListByStatus("", LocaleCodeEnum.it_IT.name(), null);
        assertNotNull(programSummaryDTOList);
        assertTrue(programSummaryDTOList.size() > 0);
    }
    
    @Test
    public void test_get_program_list_by_archived_status_peer_to_peer_program_type() throws Exception {
        List<ProgramSummaryDTO> ProgramSummaryDTOList =
                programService.getProgramListByStatus(StatusTypeCode.ARCHIVED.name(), null, 
                        ProgramTypeEnum.MANAGER_DISCRETIONARY.name());
        assertNotNull(ProgramSummaryDTOList);
        assertTrue(ProgramSummaryDTOList.size()>0);
        if (ProgramSummaryDTOList != null && ProgramSummaryDTOList.size() > 0) {
            for (ProgramSummaryDTO ProgramSummaryDTO : ProgramSummaryDTOList) {
                logger.warn("ProgramSummaryDTO:"+ProgramSummaryDTO.getProgramType());
                assertTrue(ProgramSummaryDTO.getStatus().equals(StatusTypeCode.ARCHIVED.toString()));
            }
        }
    }

    @Test
    public void test_insert_program() throws Exception {
        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        ProgramFullDTO ProgramFullDTO = programService
                .insertProgram(setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST));
        assertThat(ProgramFullDTO.getProgramName(), is(TEST_PROGRAM_NAME));
        assertThat(ProgramFullDTO.getProgramDescriptionShort(), is(TEST_SHORT_DESC));
        assertThat(ProgramFullDTO.getProgramDescriptionLong(), is(TEST_LONG_DESC));
        assertThat(ProgramFullDTO.getProgramType(), is(ProgramTypeEnum.PEER_TO_PEER.name()));
        // assertTrue(!ProgramFullDTO.getCreateDate().equals(null) &&
        // !ProgramFullDTO.getCreateDate().equals(""));
        assertTrue(!ProgramFullDTO.getThruDate().equals(null) && !ProgramFullDTO.getThruDate().equals(""));
        assertThat(ProgramFullDTO.getCreatorPax().getPaxId(), is(TestConstants.PAX_HAGOPIWL));
        assertThat(ProgramFullDTO.getCreatorPax().getManagerPaxId(), is(TestConstants.PAX_PORTERGA));
        assertThat(ProgramFullDTO.getStatus(), is(StatusTypeCode.SCHEDULED.name()));
        assertThat(ProgramFullDTO.getProgramVisibility(), is(NewsfeedVisibility.PUBLIC.toString()));
        assertThat(ProgramFullDTO.getNewsfeedVisibility(), is(NewsfeedVisibility.RECIPIENT_PREF.toString()));
        assertThat(ProgramFullDTO.getMilestoneReminders(), is(MILESTONE_REMINDERS));
        assertThat(ProgramFullDTO.getNotification().size(), is(2));
        assertThat(ProgramFullDTO.getNotification().get(0), is(VfName.NOTIFICATION_RECIPIENT.toString()));
        assertThat(ProgramFullDTO.getNotification().get(1), is(VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        assertThat(ProgramFullDTO.getPayoutType(), is(TestConstants.DPP));
    }

    public ProgramRequestDTO setupProgramRequestDTOForTesting(ProgramRequestDTO programRequestDTO,
            String responseType) {
        Calendar calendar = Calendar.getInstance();
        int year = Calendar.getInstance().get(Calendar.YEAR) + 1;
        DateFormat formatter = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);

        // setup the programRequestDTO object
        programRequestDTO.setCreateDate(DateUtil.getCurrentTimeInUTCDate());
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME);
        programRequestDTO.setProgramDescriptionShort(TEST_SHORT_DESC);
        programRequestDTO.setProgramDescriptionLong(TEST_LONG_DESC);
        programRequestDTO.setProgramType(ProgramTypeCode.PEER_TO_PEER.toString());
        programRequestDTO.setThruDate(getProgramEndDate(formatter, year, calendar));
        programRequestDTO.setFromDate(getProgramStartDate(formatter, year, calendar));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setProgramCategory(TableName.RECOGNITION.name());
        programRequestDTO.setProgramVisibility(NewsfeedVisibility.PUBLIC.toString());
        programRequestDTO.setNewsfeedVisibility(NewsfeedVisibility.RECIPIENT_PREF.toString());
        programRequestDTO.setNotification(Arrays.asList(VfName.NOTIFICATION_RECIPIENT.toString(),
                VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        programRequestDTO.setAllowTemplateUpload(Boolean.TRUE);
        programRequestDTO.setEcardIds(Arrays.asList(73L));
        programRequestDTO.setImageId(6L);
        programRequestDTO.setPayoutType(TestConstants.DPP);
        List<Integer> milestoneRemindersList = new ArrayList<>();
        milestoneRemindersList.add(1);
        milestoneRemindersList.add(2);
        milestoneRemindersList.add(4);
        MILESTONE_REMINDERS = milestoneRemindersList;
        programRequestDTO.setMilestoneReminders(milestoneRemindersList);

        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_HAGOPIWL);
        programRequestDTO.setCreatorPax(participantInfoDao.getEmployeeDTO(sysUser.getPaxId()));

        return programRequestDTO;
    }

    @Test
    public void test_insert_program_duplicate_program_name() throws Exception {

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramName(TEST_DUPLICATE_PROGRAM_NAME);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("DUPLICATE_PROGRAM_NAME"));
            }
        }
    }

    @Test
    public void test_insert_program_date_bad() throws Exception {

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setFromDate(INVALID_DATE);
        programRequestDTO.setThruDate(INVALID_DATE);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            assertThat(errorMsgException.getErrorMessages().size(), is(2));
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertTrue(errorMessage.getCode().equals("FROM_DATE_BAD_FORMAT")
                        || errorMessage.getCode().equals("THRU_DATE_BAD_FORMAT"));
            }
        }
    }

    @Test
    public void test_insert_program_2_time_zones_same_day() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        DateFormat formatter = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        Calendar calendar = Calendar.getInstance();
        Date dateBefore = calendar.getTime();
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME_TZ);
        programRequestDTO.setFromDate(formatter.format(dateBefore));
        // add recipients
        programRequestDTO.setEligibility(generateRecipientsEligibilityStubDTO());

        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);

        assertNotNull(programFullDTO.getProgramId());        
    }

    @Test
    public void test_insert_program_2_time_zones() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        DateFormat formatter = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, ONE_DAY);
        Date dateBefore = calendar.getTime();
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME_TZ);
        programRequestDTO.setFromDate(formatter.format(dateBefore));
        // add recipients
        programRequestDTO.setEligibility(generateRecipientsEligibilityStubDTO());

        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);

        assertNotNull(programFullDTO.getProgramId());
        
    }
    
    @Test
    public void test_insert_program_2_time_zones_fail() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        DateFormat formatter = new SimpleDateFormat(DateConstants.ISO_8601_DATE_ONLY_FORMAT);
        
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, THREE_DAY);
        Date dateBefore = calendar.getTime();
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME_TZ);
        programRequestDTO.setFromDate(formatter.format(dateBefore));

        // add recipients
        programRequestDTO.setEligibility(generateRecipientsEligibilityStubDTO());

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            assertThat(errorMsgException.getErrorMessages().size(), is(1));
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProgramConstants.ERROR_FROM_DATE_NOT_FUTURE));
            }
        }
        
    }

    @Test
    public void test_insert_program_invalid_program_type() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setAllowTemplateUpload(null);
        programRequestDTO.setProgramType("INVALID");

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            assertThat(errorMsgException.getErrorMessages().size(), is(1));
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PROGRAM_TYPE_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_invalid_recognition_criteria() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setRecognitionCriteriaIds(Arrays.asList(9000000L, 99999999L));

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_VALUE_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_invalid_ecard_ids() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setEcardIds(Arrays.asList(9000000L, 99999999L));

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("ECARD_VALUE_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_inactive_ecard_ids() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setEcardIds(Arrays.asList(TestConstants.ID_1, TestConstants.ID_2));
        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("ECARD_VALUE_INVALID"));

            }
        }
    }

    @Test
    public void test_insert_program_invalid_notification() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setNotification(Arrays.asList("NONE"));

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NOTIFICATION_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_newsfeed_visibility_null() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setNewsfeedVisibility(null);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NEWSFEED_VISIBLITY_NULL"));
            }
        }
    }

    @Test
    public void test_insert_program_invalid_newsfeed_visibility() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setNewsfeedVisibility("TEST");

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NEWSFEED_VISIBLITY_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_milestone_reminders() throws Exception {
        ProgramRequestDTO programRequestDTO =
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        List<Integer> milestoneReminders = new ArrayList<>();
        milestoneReminders.add(3);
        programRequestDTO.setMilestoneReminders(milestoneReminders);

        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);
        assertEquals(programFullDTO.getMilestoneReminders(), milestoneReminders);
    }

    @Test
    public void test_insert_program_null_milestone_reminders() throws Exception {
        ProgramRequestDTO programRequestDTO =
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        List<Integer> milestoneReminders = new ArrayList<>();
        milestoneReminders.add(null);
        milestoneReminders.add(5);
        programRequestDTO.setMilestoneReminders(milestoneReminders);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("MILESTONE_REMINDERS_NULL"));
            }
        }
    }

    @Test
    public void test_insert_program_invalid_program_visibility() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramVisibility("TEST");

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PROGRAM_VISIBILITY_INVALID"));
            }
        }
    }

    @Test
    public void test_insert_program_program_visibility_null() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramVisibility(null);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProgramConstants.ERROR_PROGRAM_VISIBILITY_NULL));
            }
        }
    }

    @Test
    public void test_update_program() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO, new Long(39));

        assertThat(ProgramFullDTO.getProgramName(), is(TEST_PROGRAM_NAME));
        assertThat(ProgramFullDTO.getProgramDescriptionShort(), is(TEST_SHORT_DESC));
        assertThat(ProgramFullDTO.getProgramDescriptionLong(), is(TEST_LONG_DESC));
        assertThat(ProgramFullDTO.getProgramType(), is("PEER_TO_PEER"));
        // assertTrue(!ProgramFullDTO.getCreateDate().equals(null) &&
        // !ProgramFullDTO.getCreateDate().equals(""));
        assertTrue(!ProgramFullDTO.getThruDate().equals(null) && !ProgramFullDTO.getThruDate().equals(""));
        assertThat(ProgramFullDTO.getCreatorPax().getPaxId(), is(TestConstants.PAX_HAGOPIWL));
        assertThat(ProgramFullDTO.getCreatorPax().getManagerPaxId(), is(TestConstants.PAX_PORTERGA));
        assertThat(ProgramFullDTO.getStatus(), is(StatusTypeCode.SCHEDULED.name()));
        assertThat(ProgramFullDTO.getProgramVisibility(), is(NewsfeedVisibility.PUBLIC.toString()));
        assertThat(ProgramFullDTO.getNewsfeedVisibility(), is(NewsfeedVisibility.RECIPIENT_PREF.toString()));
        assertThat(ProgramFullDTO.getNotification().size(), is(2));
        assertThat(ProgramFullDTO.getNotification().get(0), is(VfName.NOTIFICATION_RECIPIENT.toString()));
        assertThat(ProgramFullDTO.getNotification().get(1), is(VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        assertThat(ProgramFullDTO.getPayoutType(), is(TestConstants.DPP));
    }

    @Test
    public void test_update_program_duplicate_program_name() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramName(TEST_DUPLICATE_PROGRAM_NAME);

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("DUPLICATE_PROGRAM_NAME"));
            }
        }
    }

    @Test
    public void test_update_program_active_status_invalid_fields() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramDescriptionShort("");
        programRequestDTO.setProgramDescriptionLong("");
        programRequestDTO.setProgramName("");
        programRequestDTO.setProgramType("");
        programRequestDTO.setAllowTemplateUpload(null);
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            assertThat(errorMsgException.getErrorMessages().size(), is(4));
        }
    }

    @Test
    public void test_update_program_active_status_invalid_recognition_criteria() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setRecognitionCriteriaIds(Arrays.asList(9000000L, 99999999L));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("CRITERIA_VALUE_INVALID"));
            }
        }
    }

    @Test
    public void test_update_program_active_status_invalid_ecard_ids() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setEcardIds(Arrays.asList(9000000L, 99999999L));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("ECARD_VALUE_INVALID"));
            }
        }
    }

    @Test
    public void test_update_program_invalid_notification() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setNotification(Arrays.asList("NONE"));

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NOTIFICATION_INVALID"));
            }
        }
    }

    @Test
    public void test_update_program_newsfeed_visibility_null() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setNewsfeedVisibility(null);

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NEWSFEED_VISIBLITY_NULL"));
            }
        }
    }

    @Test
    public void test_update_program_invalid_newsfeed_visibility() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setNewsfeedVisibility("TEST");

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("NEWSFEED_VISIBLITY_INVALID"));
            }
        }
    }

    @Test
    public void test_update_program_invalid_program_visibility() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramVisibility("TEST");

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PROGRAM_VISIBILITY_INVALID"));
            }
        }
    }

    @Test
    public void test_update_program_program_visibility_null() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramVisibility(null);

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProgramConstants.ERROR_PROGRAM_VISIBILITY_NULL));
            }
        }
    }

    // eligibility tests

    @Test
    public void testGetProgramEligibilityEmptyEligibility() {
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_EMPTY_ELIGIBILITY);
        assertNotNull(ProgramFullDTO.getEligibility());
        assertTrue(ProgramFullDTO.getEligibility().getGive() != null
                && ProgramFullDTO.getEligibility().getGive().isEmpty());
        assertTrue(ProgramFullDTO.getEligibility().getReceive() != null
                && ProgramFullDTO.getEligibility().getReceive().isEmpty());
    }

    @Test
    public void testGetProgramEligibilityActualEligibility() {
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_VALID_ELIGIBILITY);
        assertNotNull(ProgramFullDTO.getEligibility());
        assertTrue(ProgramFullDTO.getEligibility().getGive() != null
                && !ProgramFullDTO.getEligibility().getGive().isEmpty());
        assertTrue(ProgramFullDTO.getEligibility().getReceive() != null
                && !ProgramFullDTO.getEligibility().getReceive().isEmpty());
    }

    @Test
    public void testInsertProgramEligibilityEmptyEligibility() {
        // setup with eligibility addition
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and eligibility
        assertNotNull(ProgramFullDTO);

        // verify empty eligibility
        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        assertTrue(eligibilityDTO.getGive().isEmpty());
        assertTrue(eligibilityDTO.getReceive().isEmpty());
    }

    @Test
    public void testInsertProgramEligibilityValidEntities() {
        List<Long> expectedGivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<Long> expectedGiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));
        List<Long> expectedReceivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_2));
        List<Long> expectedReceiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_2));

        // setup with eligibility entities corresponding with the expected ids
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setEligibility(generateEligibilityStubDTO(expectedGivePaxIds, expectedGiveGroupIds,
                expectedReceivePaxIds, expectedReceiveGroupIds));

        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and eligibility
        assertNotNull(ProgramFullDTO);

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifyEligibilityIds(eligibilityDTO.getGive(), expectedGivePaxIds, expectedGiveGroupIds);
        verifyEligibilityIds(eligibilityDTO.getReceive(), expectedReceivePaxIds, expectedReceiveGroupIds);
    }

    @Test
    public void testInsertProgramEligibilityWithDuplicateEntities() {
        List<Long> expectedGivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<Long> expectedGiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_2));
        List<Long> expectedReceivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_2));
        List<Long> expectedReceiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));

        EligibilityStubDTO eligibilityStubDTO = generateEligibilityStubDTO(expectedGivePaxIds, expectedGiveGroupIds,
                expectedReceivePaxIds, expectedReceiveGroupIds);
        eligibilityStubDTO = duplicateEligibilityStubDTOEntities(eligibilityStubDTO);

        // setup eligibility with duplicate entries
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setEligibility(eligibilityStubDTO);

        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and eligibility
        assertNotNull(ProgramFullDTO);

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifyEligibilityIds(eligibilityDTO.getGive(), expectedGivePaxIds, expectedGiveGroupIds);
        verifyEligibilityIds(eligibilityDTO.getReceive(), expectedReceivePaxIds, expectedReceiveGroupIds);
    }

    @Test
    public void testUpdateProgramEligibilityWithNonEmptyEligibility() {
        List<Long> expectedGivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2));
        List<Long> expectedGiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_2));
        List<Long> expectedReceivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_2));
        List<Long> expectedReceiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1, VALID_GROUP_ID_2));

        // setup a program activity request with eligibility definition to
        // update an existing program activity with no eligibility
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_EMPTY_ELIGIBILITY);
        assertNotNull(existingProgramFullDTO.getEligibility());
        assertTrue(existingProgramFullDTO.getEligibility().getGive().isEmpty());
        assertTrue(existingProgramFullDTO.getEligibility().getReceive().isEmpty());

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setEligibility(generateEligibilityStubDTO(expectedGivePaxIds, expectedGiveGroupIds,
                expectedReceivePaxIds, expectedReceiveGroupIds));

        String expectedUpdateProgramDescriptionLong = "UpdateNonEmptyEligibilityTest";
        programRequestDTO.setProgramDescriptionLong(expectedUpdateProgramDescriptionLong);
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify update and eligibility update changes
        assertNotNull(ProgramFullDTO);
        assertEquals(expectedUpdateProgramDescriptionLong, ProgramFullDTO.getProgramDescriptionLong());

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifyEligibilityIds(eligibilityDTO.getGive(), expectedGivePaxIds, expectedGiveGroupIds);
        verifyEligibilityIds(eligibilityDTO.getReceive(), expectedReceivePaxIds, expectedReceiveGroupIds);
    }

    @Test
    public void testUpdateProgramEligibilityWithEmptyEligibility() {
        // setup a program activity request with eligibility definition to
        // update an existing program activity with no eligibility
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_VALID_ELIGIBILITY);
        assertNotNull(existingProgramFullDTO.getEligibility());
        assertFalse(existingProgramFullDTO.getEligibility().getGive().isEmpty());
        assertFalse(existingProgramFullDTO.getEligibility().getReceive().isEmpty());

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());

        String expectedUpdateProgramDescriptionLong = "UpdateNonEmptyEligibilityTest";
        programRequestDTO.setProgramDescriptionLong(expectedUpdateProgramDescriptionLong);
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify update and eligibility update changes (should have deleted
        // existing eligibility)
        assertNotNull(ProgramFullDTO);
        assertEquals(expectedUpdateProgramDescriptionLong, ProgramFullDTO.getProgramDescriptionLong());

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        assertTrue(eligibilityDTO.getGive().isEmpty());
        assertTrue(eligibilityDTO.getReceive().isEmpty());
    }

    @Test
    public void testUpdateProgramEligibilityWithDifferentEligibility() {
        List<Long> expectedGivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2));
        List<Long> expectedGiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_2));
        List<Long> expectedReceivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_2));
        List<Long> expectedReceiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1, VALID_GROUP_ID_2));
        EligibilityStubDTO expectedEligibility = generateEligibilityStubDTO(expectedGivePaxIds, expectedGiveGroupIds,
                expectedReceivePaxIds, expectedReceiveGroupIds);

        // setup a program activity request with eligibility definition to
        // update an existing program activity with no eligibility
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_VALID_ELIGIBILITY);
        assertNotNull(existingProgramFullDTO.getEligibility());
        assertNotEquals(expectedEligibility.getGive().size(), existingProgramFullDTO.getEligibility().getGive().size());
        assertNotEquals(expectedEligibility.getReceive().size(),
                existingProgramFullDTO.getEligibility().getReceive().size());

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setEligibility(expectedEligibility);

        String expectedUpdateProgramDescriptionLong = "UpdateNonEmptyEligibilityTest";
        programRequestDTO.setProgramDescriptionLong(expectedUpdateProgramDescriptionLong);
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify update and eligibility update changes (should have deleted and
        // changed to match given eligibility)
        assertNotNull(ProgramFullDTO);
        assertEquals(expectedUpdateProgramDescriptionLong, ProgramFullDTO.getProgramDescriptionLong());

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifyEligibilityIds(eligibilityDTO.getGive(), expectedGivePaxIds, expectedGiveGroupIds);
        verifyEligibilityIds(eligibilityDTO.getReceive(), expectedReceivePaxIds, expectedReceiveGroupIds);
    }

    @Test
    public void testUpdateProgramEligibilitySameEligibility() {
        // setup a program activity request with eligibility definition to
        // update an existing program activity with no eligibility
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_VALID_ELIGIBILITY);
        assertNotNull(existingProgramFullDTO.getEligibility());

        EligibilityDTO expectedEligbilityDTO = existingProgramFullDTO.getEligibility();

        EligibilityStubDTO eligibilityStubDTO = convertEligibilityToStub(existingProgramFullDTO.getEligibility());

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setEligibility(eligibilityStubDTO);

        String expectedUpdateProgramDescriptionLong = "UpdateNonEmptyEligibilityTest";
        programRequestDTO.setProgramDescriptionLong(expectedUpdateProgramDescriptionLong);
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify update and eligibility update changes (should have deleted and
        // changed to match given eligibility)
        assertNotNull(ProgramFullDTO);
        assertEquals(expectedUpdateProgramDescriptionLong, ProgramFullDTO.getProgramDescriptionLong());

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifySameEligibilityDTO(expectedEligbilityDTO, eligibilityDTO);
    }

    @Test
    public void testUpdateProgramEligibilityWithDuplicateEntities() {
        // setup a program activity request with eligibility definition to
        // update program activity with existing eligibility
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(TEST_PROGRAM_ID_VALID_ELIGIBILITY);
        assertNotNull(existingProgramFullDTO.getEligibility());

        EligibilityDTO expectedEligbilityDTO = existingProgramFullDTO.getEligibility();
        EligibilityStubDTO eligibilityStubDTO = convertEligibilityToStub(existingProgramFullDTO.getEligibility());
        eligibilityStubDTO = duplicateEligibilityStubDTOEntities(eligibilityStubDTO);

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setEligibility(eligibilityStubDTO);

        String expectedUpdateProgramDescriptionLong = "UpdateNonEmptyEligibilityTest";
        programRequestDTO.setProgramDescriptionLong(expectedUpdateProgramDescriptionLong);
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify update and eligibility update changes (should have deleted and
        // changed to match given eligibility)
        assertNotNull(ProgramFullDTO);
        assertEquals(expectedUpdateProgramDescriptionLong, ProgramFullDTO.getProgramDescriptionLong());

        EligibilityDTO eligibilityDTO = ProgramFullDTO.getEligibility();
        assertNotNull(eligibilityDTO);
        verifySameEligibilityDTO(expectedEligbilityDTO, eligibilityDTO);
    }

     @Test
     public void testEligibilityValidationMissingEligibility() {
         ProgramRequestDTO programRequestDTO =
         setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
         programRequestDTO.setEligibility(null);
        
         int expectedNumberOfErrors = 1;
        
         // verify errors on insert/update
         verifyProgramInsertErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_ELIGIBILITY_CODE, expectedNumberOfErrors);
         verifyProgramUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_ELIGIBILITY_CODE, expectedNumberOfErrors);
     }

    // eligibility entity validation

    @Test
    public void testEligibilityValidationGivingEntityErrors() {
        List<Long> errorGivePaxIds = new ArrayList<>(Arrays.asList(INVALID_PAX_ID_1, INACTIVE_PAX_ID));
        List<Long> errorGiveGroupIds = new ArrayList<>(
                Arrays.asList(INVALID_GROUP_ID_1, INACTIVE_GROUP_ID, PERSONAL_GROUP_ID, HIERARCHY_GROUP_ID));

        List<Long> validPaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<Long> validGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));

        // setup with eligibility entities corresponding with the expected ids
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO(), generateNewPointLoadProgramRequestDTO());

        List<String> expectedErrors = new ArrayList<String>(
                Arrays.asList(ProgramConstants.ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE,
                        ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND, 
                        ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND,
                        ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE, 
                        ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE));

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            // verify giving errors
            programRequestDTO.setEligibility(
                    generateEligibilityStubDTO(errorGivePaxIds, errorGiveGroupIds, validPaxIds, validGroupIds));

            // program types to verify with and verify errors on insert/update
            verifyInsertUpdateErrors(programRequestDTO, expectedErrors);
        }
    }

    @Test
    public void testEligibilityValidationReceivingEntityErrors() {
        List<Long> errorGivePaxIds = new ArrayList<>(Arrays.asList(INVALID_PAX_ID_1, INACTIVE_PAX_ID));
        List<Long> errorGiveGroupIds = new ArrayList<>(
                Arrays.asList(INVALID_GROUP_ID_1, INACTIVE_GROUP_ID, PERSONAL_GROUP_ID, HIERARCHY_GROUP_ID));

        List<Long> validPaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<Long> validGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));

        // setup with eligibility entities corresponding with the expected ids
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO(), generateNewPointLoadProgramRequestDTO());

        List<String> expectedErrors = new ArrayList<String>(
                Arrays.asList(ProgramConstants.ERROR_GROUP_ELIGIBILITY_NOT_ACTIVE,
                        ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND, 
                        ProgramConstants.ERROR_ELIGIBILITY_ENTITY_NOT_FOUND,
                        ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE, 
                        ProgramConstants.ERROR_GROUP_ELIGIBILITY_INVALID_GROUP_TYPE));

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            // verify receiving errors
            programRequestDTO.setEligibility(
                    generateEligibilityStubDTO(validPaxIds, validGroupIds, errorGivePaxIds, errorGiveGroupIds));

            // program types to verify with and verify errors on insert/update
            verifyInsertUpdateErrors(programRequestDTO, expectedErrors);
        }
    }

    // eligibility stub validation

    @Test
    public void testEligibilityValidationInvalidEntityStub() {
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO(), generateNewPointLoadProgramRequestDTO());

        List<String> expectedErrors = new ArrayList<String>(
                Arrays.asList(ProgramConstants.ERROR_MISSING_ELIGIBILITY_ID, 
                        ProgramConstants.ERROR_MULTIPLE_ELIGIBILITY_ID));

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            // setup with eligibility stubs
            EligibilityStubDTO eligibility = generateEmptyEligibilityStubDTO();
            List<EligibilityEntityStubDTO> invalidEntityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();
            invalidEntityStubDTOs.add(generateEligibilityEntityStubDTO(null, null)); // no eligibility ids defined
            invalidEntityStubDTOs.add(generateEligibilityEntityStubDTO(TestConstants.ID_1, TestConstants.ID_1)); // multiple eligibility ids defined

            List<EligibilityEntityStubDTO> validEntityStubDTOs = Arrays
                    .asList(generateEligibilityEntityStubDTO(VALID_GROUP_ID_1, null));

            // verify give validation
            eligibility.setGive(invalidEntityStubDTOs);
            eligibility.setReceive(validEntityStubDTOs);
            programRequestDTO.setEligibility(eligibility);

            // program types to verify with and verify errors on insert/update
            verifyInsertUpdateErrors(programRequestDTO, expectedErrors);

            // verify receive validation
            eligibility.setGive(validEntityStubDTOs);
            eligibility.setReceive(invalidEntityStubDTOs);
            programRequestDTO.setEligibility(eligibility);

            // program types to verify with and verify errors on insert/update
            verifyInsertUpdateErrors(programRequestDTO, expectedErrors);
        }
    }

    // award tiers tests

    @Test
    public void testGetAwardTierEmptyTiers() {
        // get the program activity and verify that there are no award tiers
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(NO_AWARD_TIER_PROGRAM_ID);

        assertNotNull(ProgramFullDTO.getAwardTiers());
        assertTrue(ProgramFullDTO.getAwardTiers().isEmpty());
    }

    @Test
    public void testGetAwardTierSeveralTiers() {
        // get the program activity and verify that there are no award tiers
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);

        assertNotNull(ProgramFullDTO.getAwardTiers());
        assertFalse(ProgramFullDTO.getAwardTiers().isEmpty());

        int expectedAwardTierCount = 4;

        assertEquals(expectedAwardTierCount, ProgramFullDTO.getAwardTiers().size());

    }

    @Test
    public void testInsertAwardTierWithAwardTier_approval_NONE() {
        // setup approvals
        List<ApprovalDTO> expectedApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.NONE.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(Arrays.asList(this.generateAwardTier(25L,
                true, expectedApprovals, AwardTierType.DISCRETE.toString(),true)));

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(expectedAwardTier);
        programRequestDTO.setImageId(6L);

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and award tiers
        assertNotNull(ProgramFullDTO);

        // verify that award tiers matches expected award tiers
        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertEquals(expectedAwardTier, actualAwardTier);
    }

     @Test
    public void testInsertAwardTierWithAwardTier_approval_large_tier_amount() {
        // setup approvals
        List<ApprovalDTO> expectedApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.PERSON.name(), TestConstants.PAX_PORTERGA),
                        this.generateApproval(2, ApprovalTypeEnum.PERSON.name(), TEST_APPROVAL_PERSON_PAX_ID_2)));
        // setup with award tier addition
        List<AwardTierDTO> expectedAwardTier = 
                new ArrayList<AwardTierDTO>(Arrays.asList(this.generateAwardTier(9999999L, true, 
                        expectedApprovals, AwardTierType.DISCRETE.toString(),true)));

        ProgramRequestDTO programRequestDTO =
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(Arrays.asList(generateBudgetAssignmentStubDTOWithMembers(
                SIMPLE_BUDGET_NO_GIVING_LIMIT, Arrays.asList(VALID_GROUP_ID_1, VALID_GROUP_ID_2),
                Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2)), 
                generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,Arrays.asList(VALID_GROUP_ID_1,
                        VALID_GROUP_ID_2), Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2))));
        programRequestDTO.setAwardTiers(expectedAwardTier);

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and award tiers
        assertNotNull(ProgramFullDTO);

        // verify that award tiers matches expected award tiers
        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertEquals(actualAwardTier.size(), 1);
    }

    @Test
    public void testInsertAwardTierWithAwardTier_approval_two_approvers() {
        // setup approvals
        List<ApprovalDTO> expectedApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.PERSON.name(), TestConstants.PAX_PORTERGA),
                        this.generateApproval(2, ApprovalTypeEnum.PERSON.name(), TEST_APPROVAL_PERSON_PAX_ID_2)));
        // setup with award tier addition
        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(Arrays.asList(this.generateAwardTier(25L,
                true, expectedApprovals, AwardTierType.DISCRETE.toString(),true)));

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(expectedAwardTier);

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and award tiers
        assertNotNull(ProgramFullDTO);

        // verify that award tiers matches expected award tiers
        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertEquals(actualAwardTier.size(), 1);
    }

    @Test
    public void testAwardTierValidationMissingAwardTier() {
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationAwardTierDuplicated() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.PERSON.name(), TestConstants.PAX_PORTERGA),
                        this.generateApproval(2, ApprovalTypeEnum.SUBMITTER_FIRST.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true),
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));

        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_AWARD_AMOUNT_DUPLICATED, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testInsertAwardTier_ApprovalLevel_empty() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>();
        // setup with award tier addition
        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(expectedAwardTier);


        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and award tiers
        assertNotNull(ProgramFullDTO);

        // verify that award tiers matches expected award tiers
        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertEquals(actualAwardTier.size(), 1);
    }

    @Test
    public void testAwardTierValidationInvalidApprovalType() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, "INVALID_TYPE", null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_INVALID_APPROVAL_TYPE,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationMissingApproverForPersonType() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.PERSON.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_APPROVER_PAX,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationDuplicatedApprovalLevel() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.SUBMITTER_FIRST.name(), null),
                        this.generateApproval(1, ApprovalTypeEnum.SUBMITTER_SECOND.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_APPROVAL_LEVEL_DUPLICATED, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationInvalidApprovalLevel() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.SUBMITTER_FIRST.name(), null),
                        this.generateApproval(4, ApprovalTypeEnum.SUBMITTER_SECOND.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_INVALID_APPROVAL_LEVEL, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationDuplicatedApprovalType() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.SUBMITTER_FIRST.name(), null),
                        this.generateApproval(2, ApprovalTypeEnum.SUBMITTER_FIRST.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_APPROVAL_TYPE_DUPLICATED, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testAwardTierValidationDuplicatedApprovalPersonType() {
        // setup approvals
        List<ApprovalDTO> approvals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.PERSON.name(), TestConstants.PAX_PORTERGA),
                        this.generateApproval(2, ApprovalTypeEnum.PERSON.name(), TestConstants.PAX_PORTERGA)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(
                this.generateAwardTier(25L, true, approvals, AwardTierType.DISCRETE.toString(),true)));
        // setup without setting giving members at all in budget assignment
        // expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
            programRequestDTO.setAwardTiers(awardTier);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_APPROVAL_TYPE_DUPLICATED, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testUpdateAwardTierFromActualToEmptyAwardTier() {
        // get existing program activity and make sure award tiers are not
        // already empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertFalse(existingProgramFullDTO.getAwardTiers().isEmpty());

        List<AwardTierDTO> previousAwardTier = existingProgramFullDTO.getAwardTiers();

        // set up update to clear out award tiers
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setImageId(6L);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);

        // verify non-equality of previous award tiers
        assertFalse(ListUtils.isEqualList(previousAwardTier, actualAwardTier));

        // verify that award tiers are now empty
        assertTrue(actualAwardTier.isEmpty());
    }

    @Test
    public void testUpdateAwardTierFromEmptyToActualTiers() {
        // get existing program activity and make sure award tiers are already
        // empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(NO_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertTrue(existingProgramFullDTO.getAwardTiers().isEmpty());

        List<AwardTierDTO> previousAwardTier = existingProgramFullDTO.getAwardTiers();

        // set up update to add new award tiers
        List<AwardTierDTO> newAwardTier = this.generateValidAwardTier();
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(newAwardTier);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);

        // verify non-equality of previous award tiers
        assertFalse(ListUtils.isEqualList(previousAwardTier, actualAwardTier));

        // verify that award tiers are equal to the specified award tiers and
        // are in specified order
        assertTrue(ListUtils.isEqualList(newAwardTier, actualAwardTier));
    }

    @Test
    public void testUpdateAwardTierSameAwardTier() {
        // get existing program activity and make sure award tiers are already
        // empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertFalse(existingProgramFullDTO.getAwardTiers().isEmpty());

        List<AwardTierDTO> previousAwardTier = existingProgramFullDTO.getAwardTiers();

        for (AwardTierDTO awardTier : previousAwardTier) {
            awardTier.setType("DISCRETE");
            awardTier.setApprovalReminderEmail(true);
        }

        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(previousAwardTier);

        // set up update to add new award tiers
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(previousAwardTier);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);

        // verify that award tiers are equal to the specified award tiers
        assertTrue(ListUtils.isEqualList(expectedAwardTier, actualAwardTier));
    }

    @Test
    public void testUpdateAwardTierPartialAwardTierUpdate() {
        // setup approvals
        List<ApprovalDTO> validApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.NONE.name(), null)));

        // get existing program activity and make sure award tiers are already
        // empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertFalse(existingProgramFullDTO.getAwardTiers().isEmpty());

        // set up new award tiers to be partially different
        List<AwardTierDTO> previousAwardTier = existingProgramFullDTO.getAwardTiers();
        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(previousAwardTier);
        expectedAwardTier.remove(0);
        expectedAwardTier.add(this.generateAwardTier(25L, true, validApprovals,
                AwardTierType.DISCRETE.toString(),true));

        for (AwardTierDTO awardTierDTO : expectedAwardTier){
            awardTierDTO.setApprovalReminderEmail(true);
        }

        // set up update to add new award tiers
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(expectedAwardTier);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);

        // verify non-equality of previous award tiers
        assertFalse(ListUtils.isEqualList(previousAwardTier, actualAwardTier));

        // verify that award tiers are equal to the specified award tiers and is
        // in correct order
        assertTrue(ListUtils.isEqualList(expectedAwardTier, actualAwardTier));
    }

    @Test
    public void testUpdateAwardTierMissingAwardTier() {
        // get existing program activity and make sure award tiers are already
        // empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertFalse(existingProgramFullDTO.getAwardTiers().isEmpty());

        existingProgramFullDTO.getAwardTiers();

        // set up update without providing award tiers
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setImageId(6L);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers (should be cleared)
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertTrue(actualAwardTier.isEmpty());
    }

    @Test
    public void testUpdateAwardTier_ApprovalLevel_empty() {
        // get existing program activity and make sure award tiers are already
        // empty (for later verification)
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(SEVERAL_AWARD_TIER_PROGRAM_ID);
        assertNotNull(existingProgramFullDTO.getAwardTiers());
        assertFalse(existingProgramFullDTO.getAwardTiers().isEmpty());

        List<AwardTierDTO> previousAwardTier = existingProgramFullDTO.getAwardTiers();

        // empty approval levels
        previousAwardTier.get(0).setApprovals(null);
        previousAwardTier.get(1).setApprovals(new ArrayList<ApprovalDTO>());

        // set up update without providing award tiers
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        // setup with award tier addition
        programRequestDTO.setAwardTiers(previousAwardTier);

        for (AwardTierDTO awardTier : programRequestDTO.getAwardTiers()) {
            awardTier.setType("DISCRETE");
            awardTier.setApprovalReminderEmail(true);
        }

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verify award tiers (should be cleared)
        assertNotNull(ProgramFullDTO);

        List<AwardTierDTO> actualAwardTier = ProgramFullDTO.getAwardTiers();
        assertNotNull(actualAwardTier);
        assertTrue(actualAwardTier.get(0).getApprovals().isEmpty());
        assertTrue(actualAwardTier.get(1).getApprovals().isEmpty());
    }

    // program used tests

    @Test
    public void testGetProgramProgramUsedNotUsed() {
        // retrieve the program activity and assert that it is not used (flag is
        // set to false)
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_NOT_USED);

        assertFalse(ProgramFullDTO.isProgramUsed());
    }

    @Test
    public void testGetProgramProgramUsedWithNominations() {
        // retrieve the program activity and assert that it is used (used within
        // nominations)
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_NOMINATION);

        assertTrue(ProgramFullDTO.isProgramUsed());
    }

    @Test
    public void testGetProgramProgramUsedWithBudgetAllocations() {
        // retrieve the program activity and assert that it is used (used within
        // budget allocation associated with transaction)
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_DISCRETIONARY);

        assertTrue(ProgramFullDTO.isProgramUsed());
    }

    @Test
    public void testGetProgramProgramUsedWithBudgetAllocationAndDifferentType() {
        // retrieve the program activity and assert that it is not used (used in
        // budget allocation but not point load type)
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_BUDGET_ALLOCATED_DIFFERENT_TYPE_CODE);

        assertFalse(ProgramFullDTO.isProgramUsed());
    }

    @Test
    public void testUpdateProgramProgramNotUsedWithDifferentStartDate() {
        // retrieve any program activity that is currently in use
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_NOT_USED);
        assertFalse(ProgramFullDTO.isProgramUsed());

        String previousStartDate = ProgramFullDTO.getFromDate();

        // setup with eligibility entities corresponding with the expected ids
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, ProjectConstants.POST);
        
        programRequestDTO.setAllowTemplateUpload(null);

        // attempt to update the start date (fromDate) and set other fields
        String newStartDate = "2015-05-21";
        programRequestDTO.setFromDate(newStartDate);
        programRequestDTO.setProgramId(ProgramFullDTO.getProgramId());
        programRequestDTO.setImageId(6L);

        // verify no errors on update
        ProgramFullDTO updatedProgramFullDTO = programService.updateProgram(programRequestDTO,
                ProgramFullDTO.getProgramId());

        String updatedStartDate = updatedProgramFullDTO.getFromDate();

        // verify that update happened
        assertTrue(!updatedStartDate.equals(previousStartDate));
        assertTrue(updatedStartDate.equals(newStartDate));
    }

    @Test
    public void testUpdateProgramProgramUsedWithNominationWithDifferentStartDate() {
        // retrieve any program activity that is currently in use
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_NOMINATION);
        assertTrue(ProgramFullDTO.isProgramUsed());

        // setup with eligibility entities corresponding with the expected ids
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, ProjectConstants.POST);
            
            programRequestDTO.setAllowTemplateUpload(null);

            // attempt to update the start date (fromDate)
            programRequestDTO.setFromDate("2016-07-11");
            programRequestDTO.setThruDate("2016-08-12");
            programRequestDTO.setProgramId(ProgramFullDTO.getProgramId());

            int expectedNumberOfErrors = 1;

            // verify errors on update
            verifyProgramUpdateErrors(programRequestDTO, ProgramConstants.ERROR_PROGRAM_START_DATE_ALREADY_IN_USE,
                    ProgramFullDTO.getProgramId(), expectedNumberOfErrors);
        }
    }

    @Test
    public void testUpdateProgramProgramUsedWithNominationWithSameStartDate() {
        // retrieve any program activity that is currently in use
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_NOMINATION);
        assertTrue(ProgramFullDTO.isProgramUsed());

        String previousStartDate = ProgramFullDTO.getFromDate();

        // setup with eligibility entities corresponding with the expected ids
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, ProjectConstants.POST);
        
        programRequestDTO.setAllowTemplateUpload(null);

        // attempt to update other fields but leave start date the same
        String oldDescription = ProgramFullDTO.getProgramDescriptionLong();
        String newDescription = "NEW DESCRIPTION -- SAME START DATE";

        programRequestDTO.setFromDate(previousStartDate);
        programRequestDTO.setProgramDescriptionLong(newDescription);
        programRequestDTO.setProgramId(ProgramFullDTO.getProgramId());

        // verify no errors on update
        ProgramFullDTO updatedProgramFullDTO = programService.updateProgram(programRequestDTO,
                ProgramFullDTO.getProgramId());

        String updatedDescription = updatedProgramFullDTO.getProgramDescriptionLong();

        // verify that update happened
        assertTrue(!updatedDescription.equals(oldDescription));
        assertTrue(updatedDescription.equals(newDescription));
    }

    @Test
    public void testUpdateProgramProgramUsedWithBudgetAllocationWithDifferentStartDate() {
        // retrieve any program activity that is currently in use
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_DISCRETIONARY);
        assertTrue(ProgramFullDTO.isProgramUsed());

        // setup with eligibility entities corresponding with the expected ids
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, ProjectConstants.POST);
            
            programRequestDTO.setAllowTemplateUpload(null);
            // attempt to update the start date (fromDate) and set other fields
            programRequestDTO.setFromDate("2016-07-10");
            programRequestDTO.setThruDate("2016-08-10");
            programRequestDTO.setProgramId(ProgramFullDTO.getProgramId());

            int expectedNumberOfErrors = 1;

            // verify errors on update
            verifyProgramUpdateErrors(programRequestDTO, ProgramConstants.ERROR_PROGRAM_START_DATE_ALREADY_IN_USE,
                    ProgramFullDTO.getProgramId(), expectedNumberOfErrors);
        }
    }

    @Test
    public void testUpdateProgramProgramUsedBudgetAllocationWithSameStartDate() {
        // retrieve any program activity that is currently in use
        ProgramFullDTO ProgramFullDTO = getProgramByProgramId(PROGRAM_USED_IN_DISCRETIONARY);
        assertTrue(ProgramFullDTO.isProgramUsed());

        String previousStartDate = ProgramFullDTO.getFromDate();

        // setup with eligibility entities corresponding with the expected ids
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, ProjectConstants.POST);

        // attempt to update other fields but leave start date the same
        String oldDescription = ProgramFullDTO.getProgramDescriptionLong();
        String newDescription = "NEW DESCRIPTION -- SAME START DATE";

        programRequestDTO.setFromDate(previousStartDate);
        programRequestDTO.setProgramDescriptionLong(newDescription);
        programRequestDTO.setProgramId(ProgramFullDTO.getProgramId());

        // verify no errors on update
        ProgramFullDTO updatedProgramFullDTO = programService.updateProgram(programRequestDTO,
                ProgramFullDTO.getProgramId());

        String updatedDescription = updatedProgramFullDTO.getProgramDescriptionLong();

        // verify that update happened
        assertTrue(!updatedDescription.equals(oldDescription));
        assertTrue(updatedDescription.equals(newDescription));
    }

    // budget assignments tests

    @Test
    public void testGetBudgetAssignmentsEmptyBudgetAssignments() {
        // get program activity and verify that there are no associated budget
        // assignments
        List<BudgetAssignmentDTO> budgetAssignmentDTOList = getProgramBudgetsByProgramId(PROGRAM_EMPTY_BUDGET_ASSIGNMENTS);
        assertNotNull(budgetAssignmentDTOList);
        assertTrue(budgetAssignmentDTOList.isEmpty());
    }

    @Test
    public void testGetBudgetAssignmentsValidBudgetAssignments() {
        // get program activity and verify that there are associated budget
        // assignments
        List<BudgetAssignmentDTO> budgetAssignmentDTOList = getProgramBudgetsByProgramId(PROGRAM_VALID_BUDGET_ASSIGNMENTS);
        assertNotNull(budgetAssignmentDTOList);
        assertFalse(budgetAssignmentDTOList.isEmpty());
    }

    @Test
    public void testInsertBudgetAssignmentsEmptyBudgetAssignments() {
        // setup with empty budget assignments
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and empty budget assignments
        assertNotNull(ProgramFullDTO);

        List<BudgetAssignmentDTO> actualBudgetAssignments = ProgramFullDTO.getBudgetAssignments();
        assertNull(actualBudgetAssignments);
    }

    @Test
    public void testInsertBudgetAssignmentsEmptyAndAwarTiersNull() {
        // setup with empty budget assignments and award tiers null
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setAwardTiers(null);
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and empty budget assignments
        assertNotNull(ProgramFullDTO);

        List<BudgetAssignmentDTO> actualBudgetAssignments = ProgramFullDTO.getBudgetAssignments();
        assertNull(actualBudgetAssignments);
    }

    @Test
    public void testInsertBudgetAssignmentsNullAndAwarTiersNull() {
        // setup with empty budget assignments and award tiers null
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setAwardTiers(null);
        programRequestDTO.setBudgetAssignments(null);

        // insert program activity
        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and empty budget assignments
        assertNotNull(ProgramFullDTO);

        List<BudgetAssignmentDTO> actualBudgetAssignments = ProgramFullDTO.getBudgetAssignments();
        assertNull(actualBudgetAssignments);
    }

    @Test
    public void testInsertBudgetAssignmentsValidBudgetAssignments() {
        Long expectedBudgetId = NON_ASSOCIATED_BUDGET_ID;
        List<Long> expectedGivingMembersGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));
        List<Long> expectedGivingMembersPaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));

        // setup with budget assignments corresponding with the expected ids
        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
        budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(expectedBudgetId,
                expectedGivingMembersGroupIds, expectedGivingMembersPaxIds));

        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setAwardTiers(generateValidAwardTier());
        programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

        ProgramFullDTO ProgramFullDTO = programService.insertProgram(programRequestDTO);

        // verification of general insert and budget assignments
        assertNotNull(ProgramFullDTO);

        List<BudgetAssignmentDTO> budgetAssignmentDTOs = ProgramFullDTO.getBudgetAssignments();
        assertNull(budgetAssignmentDTOs);
    }

    @Test
    public void testUpdateBudgetAssignmentsWithEmptyBudgetAssignments() {
        // get a program activity with existing budget assignments
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(PROGRAM_VALID_BUDGET_ASSIGNMENTS);
        assertNull(existingProgramFullDTO.getBudgetAssignments());

        // set up update to clear budget assignments
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setImageId(6L);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verification of general insert and budget assignments
        assertNotNull(ProgramFullDTO);

        // verify that budget assignments are empty
        List<BudgetAssignmentDTO> budgetAssignmentDTOs = ProgramFullDTO.getBudgetAssignments();
        assertNull(budgetAssignmentDTOs);
    }

    @Test
    public void testUpdateBudgetAssignmentsWithEmptyBudgetAssignmentsAndAwardTierNull() {
        // get a program activity with existing budget assignments
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(PROGRAM_VALID_BUDGET_ASSIGNMENTS);
        assertNull(existingProgramFullDTO.getBudgetAssignments());

        // set up update to clear budget assignments
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setAwardTiers(null);
        programRequestDTO.setImageId(6L);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verification of general insert and budget assignments
        assertNotNull(ProgramFullDTO);

        // verify that budget assignments are empty
        List<BudgetAssignmentDTO> budgetAssignmentDTOs = ProgramFullDTO.getBudgetAssignments();
        assertNull(budgetAssignmentDTOs);
    }

    @Test
    public void testUpdateBudgetAssignmentsWithNullBudgetAssignmentsAndAwardTierNull() {
        // get a program activity with existing budget assignments
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(PROGRAM_VALID_BUDGET_ASSIGNMENTS);
        assertNull(existingProgramFullDTO.getBudgetAssignments());

        // set up update to clear budget assignments
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setBudgetAssignments(null);
        programRequestDTO.setAwardTiers(null);
        programRequestDTO.setImageId(6L);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verification of general insert and budget assignments
        assertNotNull(ProgramFullDTO);

        // verify that budget assignments are empty
        List<BudgetAssignmentDTO> budgetAssignmentDTOs = ProgramFullDTO.getBudgetAssignments();
        assertNull(budgetAssignmentDTOs);
    }

    @Test
    public void testUpdateBudgetAssignmentsWithNonEmptyBudgetAssignmentsAndAwardTier() {
        // get a program activity with existing budget assignments
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(PROGRAM_EMPTY_BUDGET_ASSIGNMENTS);
        assertNull(existingProgramFullDTO.getBudgetAssignments());

        // set up update to add new budget assignments
        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
        budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2)));

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setAwardTiers(generateValidAwardTier());
        programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

        // update program activity
        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO,
                programRequestDTO.getProgramId());

        // verification of budget assignments update
        assertNotNull(ProgramFullDTO);

        // verify that award tiers matches expected award tiers and is in right order
        List<BudgetAssignmentDTO> budgetAssignmentDTOs = ProgramFullDTO.getBudgetAssignments();
        assertNull(budgetAssignmentDTOs);
    }
    
    @Test
    public void testDeleteUsedAwardTier() {
        Long programId = programDao.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(AWARD_TIER_DELETE_TEST_PROGRAM_NAME)
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        assertNotNull("Program should exist.", programId);
        // get a program activity with existing budget assignments
        ProgramFullDTO existingProgramFullDTO = getProgramByProgramId(programId);
        assertNotNull("Program should exist", existingProgramFullDTO);
        assertNotNull("Program should have award tiers", existingProgramFullDTO.getAwardTiers());
        assertFalse("Program should have award tiers", existingProgramFullDTO.getAwardTiers().isEmpty());

        ProgramAwardTier programAwardTierBefore = 
                programAwardTierDao.findById(existingProgramFullDTO.getAwardTiers().get(0).getAwardTierId());

        assertNotNull("Award tier should exist before update", programAwardTierBefore);
        assertTrue("Tier status should be active before update",
                StatusTypeCode.ACTIVE.name().equalsIgnoreCase(programAwardTierBefore.getStatusTypeCode()));
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());

        // update program
        programService.updateProgram(programRequestDTO, programRequestDTO.getProgramId());
        
        ProgramAwardTier programAwardTierAfter = 
                programAwardTierDao.findById(existingProgramFullDTO.getAwardTiers().get(0).getAwardTierId());

        assertNotNull("Award tier should exist after update", programAwardTierAfter);
        assertTrue("Tier status should be inactive after update", 
                StatusTypeCode.INACTIVE.name().equalsIgnoreCase(programAwardTierAfter.getStatusTypeCode()));
        
    }

    @Test
    public void testBudgetAssignmentsValidationMissingBudget() {
        // setup with missing budget ID in budget assignment DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            BudgetAssignmentStubDTO budgetAssignmentStubDTO = generateBudgetAssignmentStubDTOWithMembers(null,
                    Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2));
            budgetAssignmentStubDTO.setBudget(null);
            budgetAssignmentStubDTOs.add(budgetAssignmentStubDTO);
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_BUDGET_ID,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationMissingBudgetId() {
        // setup with missing budget ID in budget assignment DTO\
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(null,
                    Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_BUDGET_ID, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationInvalidBudget() {
        // setup with invalid budget ID in budget assignment DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(INVALID_BUDGET_ID,
                    Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_INVALID_BUDGET_ID,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationNotActiveBudget() {
        // setup with inactive budget ID in budget assignment DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(INACTIVE_BUDGET_ID,
                    Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_BUDGET_NOT_ACTIVE,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationMissingGivingMembers() {
        // setup without setting giving members at all in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();

            BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
            BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
            budget.setBudgetId(NON_ASSOCIATED_BUDGET_ID);
            budgetAssignmentStubDTO.setBudget(budget);
            budgetAssignmentStubDTO.setGivingMembers(null);

            budgetAssignmentStubDTOs.add(budgetAssignmentStubDTO);
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_GIVING_MEMBERS,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationEmptyGivingMembers() {
        // setup using no giving members in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                    new ArrayList<Long>(), new ArrayList<Long>()));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_GIVING_MEMBERS_EMPTY, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationMissingGivingMemberId() {
        // setup using giving member with no id in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs
                    .add(generateBudgetAssignmentStubDTOWithOneMember(NON_ASSOCIATED_BUDGET_ID, null, null));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MISSING_GIVING_MEMBER_ID, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationMultipleGivingMemberId() {
        // setup using giving member with no id in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithOneMember(NON_ASSOCIATED_BUDGET_ID,
                    VALID_GROUP_ID_1, VALID_PAX_ID_1));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;

            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_MULTIPLE_GIVING_MEMBER_ID,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationInvalidGroup() {
        // setup using giving member with invalid group ID in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                    Arrays.asList(INVALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;
            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationInactiveGroup() {
        // setup using giving member with invalid group ID in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                    Arrays.asList(INACTIVE_GROUP_ID), Arrays.asList(VALID_PAX_ID_2)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;
            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationInvalidPax() {
        // setup using giving member with invalid pax ID in budget assignment expected DTO
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());

            List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
            budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                    Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(INVALID_PAX_ID_1)));
            programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

            int expectedNumberOfErrors = 1;
            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND, 
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationGivenBudgetAssignmentsMissingAwardTier() {
        // setup with missing award tiers but defined budget assignments
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
            programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());

            int expectedNumberOfErrors = 1;
            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY,
                    expectedNumberOfErrors);
        }
    }

    @Test
    public void testBudgetAssignmentsValidationGivenAwardTierMissingBudgetAssignments() {
        // setup with missing budget assignments but defined award tiers
        List<ProgramRequestDTO> programTypeDTOs = Arrays.asList(generateNewPeerToPeerProgramRequestDTO(),
                generateNewManagerDiscretionaryProgramRequestDTO());

        for (ProgramRequestDTO programRequestDTO : programTypeDTOs) {
            programRequestDTO.setAwardTiers(generateValidAwardTier());
            programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());

            int expectedNumberOfErrors = 1;
            verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_BUDGET_ASSIGNMENT_AWARD_TIER_DEPENDENCY,
                    expectedNumberOfErrors);
        }
    }

    // POINT_LOAD program type validation

    @Test
    public void testPointLoadValidationMissingReceiverEligibility() {
        // setup with missing receive eligibility
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);
        programRequestDTO.setEligibility(new EligibilityStubDTO());

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ERROR_RECEIVING_ELIGIBILITY_REQUIRED, expectedNumberOfErrors);
    }

    @Test
    public void testPointLoadValidationEmptyReceiverEligibility() {
        // setup with empty receive eligibility
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);

        // giving eligibility doesn't factor into receiver validation
        EligibilityStubDTO eligibilityStubDTO = generateEligibilityStubDTO(Arrays.asList(VALID_PAX_ID_1), 
                new ArrayList<Long>(), new ArrayList<Long>(), new ArrayList<Long>());
        programRequestDTO.setEligibility(eligibilityStubDTO);

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ERROR_RECEIVING_ELIGIBILITY_REQUIRED, expectedNumberOfErrors);
    }

    @Test
    public void testPointLoadValidationNonEmptyAwardTier() {
        // setup with defined award tiers
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);
        programRequestDTO.setAwardTiers(this.generateValidAwardTier());

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ProgramConstants.ERROR_AWARD_TIER_NOT_ALLOWED,
                expectedNumberOfErrors);
    }

    @Test
    public void testPointLoadMissingBudgetAssignments() {
        // setup with missing budget ID in budget assignment DTO
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(null);

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ERROR_MISSING_BUDGET, expectedNumberOfErrors);
    }

    @Test
    public void testPointLoadEmptyBudgetAssignments() {
        // setup with empty budget ID in budget assignment DTO
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);
        programRequestDTO.setBudgetAssignments(null);

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ERROR_MISSING_BUDGET, expectedNumberOfErrors);
    }

    @Test
    public void testPointLoadNonEmptyGivingMembersInBudgetAssignments() {
        // setup with non empty giving members in budget assignment DTO
        ProgramRequestDTO programRequestDTO = generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);

        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = generateBudgetAssignmentStubDTOWithMembers(
                NON_ASSOCIATED_BUDGET_ID, Arrays.asList(VALID_GROUP_ID_1), Arrays.asList(VALID_PAX_ID_2));
        budgetAssignmentStubDTOs.add(budgetAssignmentStubDTO);
        programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

        int expectedNumberOfErrors = 1;
        verifyInsertUpdateErrors(programRequestDTO, ERROR_BUDGET_GIVING_ELIGIBILITY_NOT_ALLOWED,
                expectedNumberOfErrors);
    }

    // update test with implied statuses (ENDED, SCHEDULED)

    @Test
    public void testUpdateProgramWithScheduledStatus() {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setFromDate(DateUtil.convertToUTCDateComponentsFormat(TestUtil.generateFutureDate()));
        programRequestDTO.setThruDate(null);
        programRequestDTO.setStatus(StatusTypeCode.SCHEDULED.toString());
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO, new Long(39));

        // verify some field values and that status is SCHEDULED
        assertThat(ProgramFullDTO.getProgramName(), is(TEST_PROGRAM_NAME));
        assertThat(ProgramFullDTO.getProgramType(), is("PEER_TO_PEER"));
        assertThat(ProgramFullDTO.getCreatorPax().getPaxId(), is(TestConstants.PAX_HAGOPIWL));
        assertThat(ProgramFullDTO.getCreatorPax().getManagerPaxId(), is(TestConstants.PAX_PORTERGA));
        assertThat(ProgramFullDTO.getStatus(), is(StatusTypeCode.SCHEDULED.toString()));
    }

    @Test
    public void testUpdateProgramWithEndedStatus() {
        String programName = "Ended Program Test - DO NOT CHANGE";
        Long creatorPaxId = 4417L;

        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(ENDED_PROGRAM, null);
        assertThat(existingProgramFullDTO.getStatus(), is(StatusTypeCode.ENDED.toString()));

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramId(ENDED_PROGRAM);
        programRequestDTO.setProgramName(existingProgramFullDTO.getProgramName());
        programRequestDTO.setFromDate(existingProgramFullDTO.getFromDate());
        programRequestDTO.setThruDate(DateUtil.convertToUTCDateComponentsFormat(TestUtil.generateFutureDate()));
        programRequestDTO.setStatus(StatusTypeCode.ENDED.toString());
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO, ENDED_PROGRAM);

        // verify some field values and that status is ACTIVE
        assertThat(ProgramFullDTO.getProgramName(), is(programName));
        assertThat(ProgramFullDTO.getProgramType(), is(ProgramTypeCode.PEER_TO_PEER.toString()));
        assertThat(ProgramFullDTO.getCreatorPax().getPaxId(), is(creatorPaxId));
        assertThat(ProgramFullDTO.getCreatorPax().getManagerPaxId(), is(TestConstants.PAX_PORTERGA));
        assertThat(ProgramFullDTO.getStatus(), is(StatusTypeCode.ACTIVE.name()));
    }

    @Test
    public void testUpdateProgramWithEndedStatusNull() {
        String programName = "Ended Program Test - DO NOT CHANGE";
        Long creatorPaxId = 4417L;

        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(ENDED_PROGRAM, null);
        assertThat(existingProgramFullDTO.getStatus(), is(StatusTypeCode.ENDED.toString()));

        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setProgramId(ENDED_PROGRAM);
        programRequestDTO.setProgramName(existingProgramFullDTO.getProgramName());
        programRequestDTO.setFromDate(existingProgramFullDTO.getFromDate());
        programRequestDTO.setThruDate(null);
        programRequestDTO.setStatus(StatusTypeCode.ENDED.toString());
        programRequestDTO.setImageId(6L);

        ProgramFullDTO ProgramFullDTO = programService.updateProgram(programRequestDTO, ENDED_PROGRAM);

        // verify some field values and that status is ACTIVE
        assertThat(ProgramFullDTO.getProgramName(), is(programName));
        assertThat(ProgramFullDTO.getProgramType(), is(ProgramTypeCode.PEER_TO_PEER.toString()));
        assertThat(ProgramFullDTO.getCreatorPax().getPaxId(), is(creatorPaxId));
        assertThat(ProgramFullDTO.getCreatorPax().getManagerPaxId(), is(TestConstants.PAX_PORTERGA));
        assertThat(ProgramFullDTO.getStatus(), is(StatusTypeCode.ACTIVE.name()));
    }
    
    // start payout type tests
    
    @Test
    public void test_insert_program_invalid_payout_type() throws Exception {
        // Null from date
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setPayoutType("NONE");

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PAYOUT_TYPE_INVALID"));
            }
        }
    }
    
    @Test
    public void test_update_program_invalid_payout_type() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        programRequestDTO.setPayoutType("NONE");

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PAYOUT_TYPE_INVALID"));
            }
        }
    }
    
    @Test
    public void test_update_program_missing_payout_type() throws Exception {
        ProgramFullDTO existingProgramFullDTO = programService.getProgramByID(new Long(39), null);
        ProgramRequestDTO programRequestDTO = transformProgramResponseToRequest(existingProgramFullDTO);
        programRequestDTO = setupProgramRequestDTOForTesting(programRequestDTO, ProjectConstants.PUT);
        
        // setup approvals
        List<ApprovalDTO> expectedApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.NONE.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> expectedAwardTier = new ArrayList<AwardTierDTO>(Arrays.asList(this.generateAwardTier(25L,
                true, expectedApprovals, AwardTierType.DISCRETE.toString(),true)));
        
        programRequestDTO.setBudgetAssignments(generateValidBudgetAssignmentStubDTOs());
        programRequestDTO.setAwardTiers(expectedAwardTier);
        programRequestDTO.setPayoutType(ProjectConstants.EMPTY_STRING);

        try {
            programService.updateProgram(programRequestDTO, new Long(39));
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("PAYOUT_TYPE_MISSING"));
            }
        }
    }
    
    // helper methods

    private ProgramRequestDTO setupValidPeerToPeerProgramRequestDTO(ProgramRequestDTO programRequestDTO,
            String responseType) {
        // setup only the fields related to PeerToPeer program type
        programRequestDTO.setProgramType(ProgramTypeCode.PEER_TO_PEER.toString());


        return programRequestDTO;
    }

    private ProgramRequestDTO setupValidPointLoadProgramRequestDTO(ProgramRequestDTO programRequestDTO,
            String responseType) {
        // setup only the fields related to PointLoad program type
        programRequestDTO.setProgramType(ProgramTypeCode.POINT_LOAD.toString());
        programRequestDTO.setEligibility(generateValidEligibilityStubDTO());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = Arrays
                .asList(generatePointLoadBudgetAssignment(NON_ASSOCIATED_BUDGET_ID));
        programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);
        programRequestDTO.setAllowTemplateUpload(null);

        return programRequestDTO;
    }

    private ProgramRequestDTO setupValidManagerDiscretionaryProgramRequestDTO(ProgramRequestDTO programRequestDTO,
            String responseType) {
        // setup only the fields related to ManagerDiscretionary program type
        programRequestDTO.setProgramType(ProgramTypeCode.MANAGER_DISCRETIONARY.toString());
        programRequestDTO.setProgramCategory(TableName.RECOGNITION.name());

        return programRequestDTO;
    }

    private ProgramRequestDTO generateNewProgramRequestDTO(String responseType) {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        Calendar calendar = Calendar.getInstance();
        int year = Calendar.getInstance().get(Calendar.YEAR) + 1;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        // setup the basic fields for programRequestDTO object
        programRequestDTO.setCreateDate(DateUtil.getCurrentTimeInUTCDate());
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME);
        programRequestDTO.setProgramDescriptionShort(TEST_SHORT_DESC);
        programRequestDTO.setProgramDescriptionLong(TEST_LONG_DESC);
        programRequestDTO.setProgramType(TableName.RECOGNITION.name());
        programRequestDTO.setThruDate(getProgramEndDate(formatter, year, calendar));
        programRequestDTO.setFromDate(getProgramStartDate(formatter, year, calendar));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setProgramCategory(TableName.RECOGNITION.name());
        programRequestDTO.setProgramVisibility(NewsfeedVisibility.PUBLIC.toString());
        programRequestDTO.setNewsfeedVisibility(NewsfeedVisibility.RECIPIENT_PREF.toString());
        programRequestDTO.setNotification(Arrays.asList(VfName.NOTIFICATION_RECIPIENT.toString(), 
                VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        programRequestDTO.setAllowTemplateUpload(Boolean.TRUE);
        programRequestDTO.setPayoutType(TestConstants.DPP);

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        programRequestDTO.setCreatorPax(participantInfoDao.getEmployeeDTO(security.getPaxId()));

        return programRequestDTO;
    }

    /**
     * @see ProgramServiceTest#generateNewPeerToPeerProgramRequestDTO
     *
     *      Just handles response type (any non-GET type)
     */
    private ProgramRequestDTO generateNewPeerToPeerProgramRequestDTO() {
        return generateNewPeerToPeerProgramRequestDTO(ProjectConstants.POST);
    }

    /**
     * Generates a valid Program Activity request with a PeerToPeer program
     * type.
     *
     * @param responseType
     *            response type to construct request with
     * @return valid Program Activity request with a PeerToPeer program type
     */
    private ProgramRequestDTO generateNewPeerToPeerProgramRequestDTO(String responseType) {
        ProgramRequestDTO programRequestDTO = generateNewProgramRequestDTO(responseType);
        programRequestDTO = setupValidPeerToPeerProgramRequestDTO(programRequestDTO, responseType);
        return programRequestDTO;
    }

    /**
     * @see ProgramServiceTest#generateNewManagerDiscretionaryProgramRequestDTO
     *
     *      Just handles response type (any non-GET type)
     */
    private ProgramRequestDTO generateNewManagerDiscretionaryProgramRequestDTO() {
        return generateNewManagerDiscretionaryProgramRequestDTO(ProjectConstants.POST);
    }

    /**
     * Generates a valid Program Activity request with a ManagerDiscretionary
     * program type.
     *
     * @param responseType
     *            response type to construct request with
     * @return valid Program Activity request with a ManagerDiscretionary
     *         program type
     */
    private ProgramRequestDTO generateNewManagerDiscretionaryProgramRequestDTO(String responseType) {
        ProgramRequestDTO programRequestDTO = generateNewProgramRequestDTO(responseType);
        programRequestDTO = setupValidManagerDiscretionaryProgramRequestDTO(programRequestDTO, responseType);
        return programRequestDTO;
    }

    /**
     * @see ProgramServiceTest#generateNewPointLoadProgramRequestDTO
     *
     *      Just handles response type (any non-GET type)
     */
    private ProgramRequestDTO generateNewPointLoadProgramRequestDTO() {
        return generateNewPointLoadProgramRequestDTO(ProjectConstants.POST);
    }

    /**
     * Generates a valid Program Activity request with a PointLoad program type.
     *
     * @param responseType
     *            response type to construct request with
     * @return valid Program Activity request with a PointLoad program type
     */
    private ProgramRequestDTO generateNewPointLoadProgramRequestDTO(String responseType) {
        ProgramRequestDTO programRequestDTO = generateNewProgramRequestDTO(responseType);
        programRequestDTO.setAllowTemplateUpload(null);
        programRequestDTO = setupValidPointLoadProgramRequestDTO(programRequestDTO, responseType);
        return programRequestDTO;
    }

    /**
     * Generates a Budget Assignment with only the specified Budget ID (for
     * Point Load programs).
     *
     * @param budgetId
     *            ID of budget
     * @return budget assignment with a budget of the specified ID
     */
    private BudgetAssignmentStubDTO generatePointLoadBudgetAssignment(Long budgetId) {
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();

        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(budgetId);

        budgetAssignmentStubDTO.setBudget(budget);

        return budgetAssignmentStubDTO;
    }

    /**
     * Generates eligibility stub DTO that contains duplicate entities.
     *
     * @param eligibilityStubDTO
     *            eligibility stub DTO with previous entities
     * @return eligibility stub DTO that contains duplicate entities
     */
    @Nonnull
    private EligibilityStubDTO duplicateEligibilityStubDTOEntities(@Nonnull EligibilityStubDTO eligibilityStubDTO) {
        EligibilityStubDTO duplicateEligibilityStubDTO = new EligibilityStubDTO();

        // duplicate entities in give and receive
        List<EligibilityEntityStubDTO> duplicateGive = new ArrayList<EligibilityEntityStubDTO>();
        List<EligibilityEntityStubDTO> duplicateReceive = new ArrayList<EligibilityEntityStubDTO>();

        if (eligibilityStubDTO.getGive() != null) {
            // add either 1 or 2 additional duplicate entities
            duplicateGive = duplicateEligibilityEntityStubDTOEntities(eligibilityStubDTO.getGive(),
                    new Random().nextInt(2) + 2);
        }

        if (eligibilityStubDTO.getReceive() != null) {
            // add either 1 or 2 additional duplicate entities
            duplicateReceive = duplicateEligibilityEntityStubDTOEntities(eligibilityStubDTO.getReceive(),
                    new Random().nextInt(2) + 2);
        }

        duplicateEligibilityStubDTO.setGive(duplicateGive);
        duplicateEligibilityStubDTO.setReceive(duplicateReceive);

        return duplicateEligibilityStubDTO;
    }

    /**
     * Duplicates entities defined in the given EligibilityEntityStub DTO list.
     *
     * @param eligibilityEntityStubDTOs
     *            ligibilityEntityStub DTO list to duplicate
     * @param repeatTimes
     *            number of times to duplicate entities
     * @return ligibilityEntityStub DTO list with duplicated entities
     */
    @Nonnull
    private List<EligibilityEntityStubDTO> duplicateEligibilityEntityStubDTOEntities(
            @Nonnull List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs, int repeatTimes) {
        List<EligibilityEntityStubDTO> duplicateEligibilityEntityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();

        for (EligibilityEntityStubDTO eligibilityEntityStubDTO : eligibilityEntityStubDTOs) {
            for (int i = 0; i < repeatTimes; i++) {
                // repeat adding same entity to specified number of times
                duplicateEligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        return duplicateEligibilityEntityStubDTOs;
    }

    /**
     * Generates an Eligibility Entity Stub DTO with the specified IDs.
     *
     * @param groupId
     *            group ID
     * @param paxId
     *            pax ID
     * @return Eligibility Entity Stub DTO with the specified IDs
     */
    private EligibilityEntityStubDTO generateEligibilityEntityStubDTO(Long groupId, Long paxId) {
        EligibilityEntityStubDTO invalidEligibilityEntityStubDTO = new EligibilityEntityStubDTO();
        invalidEligibilityEntityStubDTO.setGroupId(groupId);
        invalidEligibilityEntityStubDTO.setPaxId(paxId);
        return invalidEligibilityEntityStubDTO;
    }

    /**
     * Generates a valid eligibility stub DTO with no ids for a ProgramRequest.
     *
     * @return a valid eligibility stub DTO with no ids
     */
    private EligibilityStubDTO generateEmptyEligibilityStubDTO() {
        return generateEligibilityStubDTO(new ArrayList<Long>(), new ArrayList<Long>(), new ArrayList<Long>(),
                new ArrayList<Long>());
    }

    /**
     * Generates a valid eligibility stub DTO with valid recipients.
     *
     * @return a valid eligibility stub DTO with recipients
     */
    private EligibilityStubDTO generateRecipientsEligibilityStubDTO() {
        return generateEligibilityStubDTO(new ArrayList<Long>(), new ArrayList<Long>(), Arrays.asList(847L),
                Arrays.asList(3L));
    }

    /**
     * Generates a valid eligibility stub DTO with valid pax and group ids (for
     * giver and receiver eligibility) for a ProgramRequest.
     *
     * @return a valid eligibility stub DTO with valid pax and group ids (for
     *         giver and receiver eligibility)
     */
    private EligibilityStubDTO generateValidEligibilityStubDTO() {
        List<Long> expectedGivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2));
        List<Long> expectedGiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1, VALID_GROUP_ID_2));
        List<Long> expectedReceivePaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<Long> expectedReceiveGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_2));
        return generateEligibilityStubDTO(expectedGivePaxIds, expectedGiveGroupIds, expectedReceivePaxIds,
                expectedReceiveGroupIds);
    }

    /**
     * Generates a valid budget assignment stub DTO with valid pax, group, and
     * budget ids for a ProgramRequest.
     *
     * @return a valid budget assignment stub DTO with valid pax, group, and
     *         budget ids
     */
    private List<BudgetAssignmentStubDTO> generateValidBudgetAssignmentStubDTOs() {
        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();

        budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(NON_ASSOCIATED_BUDGET_ID,
                Arrays.asList(VALID_GROUP_ID_1, VALID_GROUP_ID_2), Arrays.asList(VALID_PAX_ID_1, VALID_PAX_ID_2)));

        return budgetAssignmentStubDTOs;
    }

    /**
     * Generates a budget assignment stub DTO with the specified budget ID and
     * giving members with the specified group/pax IDs.
     * 
     * @param budgetId
     *            ID of budget
     * @param groupIds
     *            IDs of groups identified as giving members
     * @param paxIds
     *            IDs of paxs identified as giving members
     * @return budget assignment stub DTO with the specified budget ID and
     *         giving members with the specified group/pax IDs
     */
    private BudgetAssignmentStubDTO generateBudgetAssignmentStubDTOWithMembers(Long budgetId, List<Long> groupIds,
            List<Long> paxIds) {
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();

        // generate budget with valid budget id
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(budgetId);

        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);

        budgetAssignmentStubDTO.setGivingMembers(generateGivingMembers(groupIds, paxIds));

        return budgetAssignmentStubDTO;
    }

    /**
     * Generates giving member stubs with the given group and pax ids.
     *
     * @param groupIds
     *            group IDs
     * @param paxIds
     *            pax IDs
     * @return giving members with the specified group and pax IDs
     */
    private List<BudgetEntityStubDTO> generateGivingMembers(@Nonnull List<Long> groupIds, @Nonnull List<Long> paxIds) {
        List<BudgetEntityStubDTO> givingMembers = new ArrayList<BudgetEntityStubDTO>();

        for (Long groupId : groupIds) {
            BudgetEntityStubDTO groupStubDTO = new BudgetEntityStubDTO();
            groupStubDTO.setGroupId(groupId);
            givingMembers.add(groupStubDTO);
        }

        for (Long paxId : paxIds) {
            BudgetEntityStubDTO paxStubDTO = new BudgetEntityStubDTO();
            paxStubDTO.setPaxId(paxId);
            givingMembers.add(paxStubDTO);
        }

        return givingMembers;
    }

    /**
     * Generates a BudgetAssignmentStubDTO with the specified budgetId and one
     * giving member with the specified pax and/or group id.
     *
     * @param budgetId
     *            budget id to use for budget
     * @param paxId
     *            pax id to use for giving member
     * @param groupId
     *            group id to use for giving member
     * @return BudgetAssignmentStubDTO with the specified budgetId and one
     *         giving member with the specified pax and/or group id
     */
    private BudgetAssignmentStubDTO generateBudgetAssignmentStubDTOWithOneMember(Long budgetId, Long groupId,
            Long paxId) {
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();

        // set up budget
        BudgetEntityStubDTO budget = new BudgetEntityStubDTO();
        budget.setBudgetId(budgetId);
        budgetAssignmentStubDTO.setBudget(budget);

        // set up one giving member
        BudgetEntityStubDTO givingMember = new BudgetEntityStubDTO();
        givingMember.setGroupId(groupId);
        givingMember.setPaxId(paxId);
        budgetAssignmentStubDTO.setGivingMembers(Arrays.asList(givingMember));

        return budgetAssignmentStubDTO;
    }

    /**
     * Generates a simple definition of award tiers.
     *
     * @return simple definition of award tiers
     */
    private List<AwardTierDTO> generateValidAwardTier() {
        // setup approvals
        List<ApprovalDTO> validApprovals = new ArrayList<ApprovalDTO>(
                Arrays.asList(this.generateApproval(1, ApprovalTypeEnum.NONE.name(), null)));
        // setup with award tier addition
        List<AwardTierDTO> awardTier = new ArrayList<AwardTierDTO>(Arrays.asList(this.generateAwardTier(25L, true,
                validApprovals, AwardTierType.DISCRETE.toString(),true)));

        return awardTier;
    }

    private ProgramRequestDTO transformProgramResponseToRequest(ProgramFullDTO programFullDTO) {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();

        programRequestDTO.setProgramId(programFullDTO.getProgramId());
        programRequestDTO.setProgramName(programFullDTO.getProgramName());
        programRequestDTO.setProgramDescriptionShort(programFullDTO.getProgramDescriptionShort());
        programRequestDTO.setProgramType(programFullDTO.getProgramType());
        programRequestDTO.setCreateDate(programFullDTO.getCreateDate());
        programRequestDTO.setFromDate(programFullDTO.getFromDate());
        programRequestDTO.setThruDate(programFullDTO.getThruDate());
        programRequestDTO.setStatus(programFullDTO.getStatus());
        programRequestDTO.setCreatorPax(programFullDTO.getCreatorPax());
        programRequestDTO.setProgramDescriptionLong(programFullDTO.getProgramDescriptionLong());
        programRequestDTO.setProgramVisibility(programFullDTO.getProgramVisibility());
        programRequestDTO.setNewsfeedVisibility(programFullDTO.getNewsfeedVisibility());
        programRequestDTO.setNotification(programFullDTO.getNotification());
        programRequestDTO.setAllowTemplateUpload(programFullDTO.getAllowTemplateUpload());
        programRequestDTO.setRecognitionCriteriaIds(new ArrayList<Long>(programFullDTO.getRecognitionCriteriaIds()));
        programRequestDTO.setEcardIds(new ArrayList<Long>(programFullDTO.getEcardIds()));
        programRequestDTO.setProgramCategory(programFullDTO.getProgramCategory());
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setEligibility(generateEmptyEligibilityStubDTO());
        programRequestDTO.setPayoutType(TestConstants.DPP);

        return programRequestDTO;
    }

    /**
     * Generates a list of eligibility entity stub DTOs with the specified
     * paxIds and/or groupId.
     *
     * @param paxIds
     *            paxIds to be included
     * @param groupIds
     *            groupIds to be included
     * @return a list of eligibility entity stub DTOs with the specified paxIds
     *         and/or groupIds
     */
    private List<EligibilityEntityStubDTO> generateEligibilityEntityStubDTOs(List<Long> paxIds, List<Long> groupIds) {
        List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs = new ArrayList<>();

        // add paxIds within the DTO
        if (paxIds != null) {
            for (Long paxId : paxIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setPaxId(paxId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        // add groupIds within the DTO
        if (groupIds != null) {
            for (Long groupId : groupIds) {
                EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                eligibilityEntityStubDTO.setGroupId(groupId);
                eligibilityEntityStubDTOs.add(eligibilityEntityStubDTO);
            }
        }

        return eligibilityEntityStubDTOs;
    }

    /**
     * Generates the eligibility stub DTO with the specified paxIds and
     * groupIds.
     *
     * @param givePaxIds
     *            paxIds identified for giving eligibility
     * @param giveGroupIds
     *            groupIds identified for receiving eligibility
     * @param receivePaxIds
     *            paxIds identified for giving eligibility
     * @param receiveGroupIds
     *            groupIds identified for receiving eligibility
     * @return eligibility stub DTO with the specified paxIds and groupIds
     */
    private EligibilityStubDTO generateEligibilityStubDTO(List<Long> givePaxIds, List<Long> giveGroupIds,
            List<Long> receivePaxIds, List<Long> receiveGroupIds) {
        EligibilityStubDTO eligibilityStubDTO = new EligibilityStubDTO();

        // generate list of eligibility entity stub DTOs for giving and receiving eligibility
        eligibilityStubDTO.setGive(generateEligibilityEntityStubDTOs(givePaxIds, giveGroupIds));
        eligibilityStubDTO.setReceive(generateEligibilityEntityStubDTOs(receivePaxIds, receiveGroupIds));

        return eligibilityStubDTO;
    }

    /**
     * Verification method that checks if all given paxIds and groupIds are
     * represented within the list of entity DTOs.
     *
     * @param entityDTOs
     *            entity DTOs to verify
     * @param paxIds
     *            expected paxIds
     * @param groupIds
     *            expected groupIds
     */
    private void verifyEligibilityIds(@Nonnull List<EntityDTO> entityDTOs, @Nonnull List<Long> paxIds,
            @Nonnull List<Long> groupIds) {
        // iterate through all the entities
        for (EntityDTO entityDTO : entityDTOs) {
            // check either paxId or groupId
            if (entityDTO instanceof EmployeeDTO) {
                Long paxId = ((EmployeeDTO) entityDTO).getPaxId();
                assertTrue(paxIds.contains(paxId));
                paxIds.remove(paxId);
            } else if (entityDTO instanceof GroupDTO) {
                Long groupId = ((GroupDTO) entityDTO).getGroupId();
                assertTrue(groupIds.contains(groupId));
                groupIds.remove(groupId);
            } else {
                fail("Current entity DTO is not a GroupDTO or an EmployeeDTO!!");
            }
        }

        // assert that all the ids were accounted for
        assertTrue(paxIds.isEmpty());
        assertTrue(groupIds.isEmpty());
    }

    /**
     * Verifies that the given eligibility DTOs are the same.
     *
     * @param expectedEligbilityDTO
     *            expected eligibility DTO
     * @param eligibilityDTO
     *            eligibility DTO to check
     */
    private void verifySameEligibilityDTO(EligibilityDTO expectedEligbilityDTO, EligibilityDTO eligibilityDTO) {
        // if one object is missing, then both should be missing
        if (expectedEligbilityDTO == null || eligibilityDTO == null) {
            assertNull(expectedEligbilityDTO);
            assertNull(eligibilityDTO);
        } else {
            // assert both objects are not null before trying to compare
            // entities
            assertNotNull(expectedEligbilityDTO);
            assertNotNull(eligibilityDTO);

            verifySameEntityIds(expectedEligbilityDTO.getGive(), eligibilityDTO.getGive());
            verifySameEntityIds(expectedEligbilityDTO.getReceive(), eligibilityDTO.getReceive());
        }
    }

    /**
     * Verifies if two lists of entity DTOs are the same. Only checks id.
     *
     * @param expectedEntities
     *            expected entity DTOs
     * @param actualEntities
     *            actual entity DTOs
     */
    private void verifySameEntityIds(List<EntityDTO> expectedEntities, List<EntityDTO> actualEntities) {
        // if one object is missing, then both should be missing
        if (expectedEntities == null || actualEntities == null) {
            assertNull(expectedEntities);
            assertNull(actualEntities);
        } else {
            // assert both objects are not null before trying to compare entities
            assertNotNull(expectedEntities);
            assertNotNull(actualEntities);

            assertEquals(expectedEntities.size(), actualEntities.size());

            // just compare by ids so filter ids from expected entities and see if actual contains those ids
            List<Long> groupIds = new ArrayList<Long>();
            List<Long> paxIds = new ArrayList<Long>();

            // filter group and pax ids by what is specified by the stub DTOs
            for (EntityDTO entityDTO : expectedEntities) {
                if (entityDTO instanceof EmployeeDTO) {
                    paxIds.add(((EmployeeDTO) entityDTO).getPaxId());
                } else if (entityDTO instanceof GroupDTO) {
                    groupIds.add(((GroupDTO) entityDTO).getGroupId());
                } else {
                    fail("Entity DTO is not a participant or group!");
                }
            }

            for (EntityDTO entityDTO : actualEntities) {
                if (entityDTO instanceof EmployeeDTO) {
                    Long paxId = ((EmployeeDTO) entityDTO).getPaxId();
                    assertTrue(paxIds.contains(paxId));
                    paxIds.remove(paxId);
                } else if (entityDTO instanceof GroupDTO) {
                    Long groupId = ((GroupDTO) entityDTO).getGroupId();
                    assertTrue(groupIds.contains(groupId));
                    groupIds.remove(groupId);
                } else {
                    fail("Entity DTO is not a participant or group!");
                }
            }

            assertTrue(paxIds.isEmpty());
            assertTrue(groupIds.isEmpty());
        }
    }

    /**
     * Verifies budget assignment DTOs against the expected budget assignment
     * stubs.
     *
     * NOTE: Expects list order to be the same.
     *
     * @param budgetAssignmentDTOs
     *            budget assignment DTOs to verify
     * @param expectedBudgetAssignmentStubs
     *            budget assignment stubs to verify against
     */
    private void verifyBudgetAssignmentDTOs(List<BudgetAssignmentDTO> budgetAssignmentDTOs,
            List<BudgetAssignmentStubDTO> expectedBudgetAssignmentStubs) {
        // if one is missing, then both should be missing
        if (budgetAssignmentDTOs == null || expectedBudgetAssignmentStubs == null) {
            assertNull(budgetAssignmentDTOs);
            assertNull(expectedBudgetAssignmentStubs);
        } else {
            // assert both objects are not null before trying to compare
            assertNotNull(budgetAssignmentDTOs);
            assertNotNull(expectedBudgetAssignmentStubs);

            // check if number of actual budget assignments matches expected budget assignments
            assertEquals(budgetAssignmentDTOs.size(), expectedBudgetAssignmentStubs.size());

            // iterate through the given lists and verify that the budget and giving members IDs match
            for (int i = 0; i < budgetAssignmentDTOs.size(); i++) {
                BudgetAssignmentDTO budgetAssignmentDTO = budgetAssignmentDTOs.get(i);
                BudgetAssignmentStubDTO budgetAssignmentStubDTO = expectedBudgetAssignmentStubs.get(i);

                // sanity check for budgets
                assertNotNull(budgetAssignmentDTO.getBudget());
                assertNotNull(budgetAssignmentStubDTO.getBudget());

                assertEquals(budgetAssignmentDTO.getBudget().getBudgetId(),
                        budgetAssignmentStubDTO.getBudget().getBudgetId());
                verifyGivingMembers(budgetAssignmentDTO.getGivingMembers(), budgetAssignmentStubDTO.getGivingMembers());
            }
        }
    }

    /**
     * Verifies actual giving members entities against the expected giving
     * members.
     *
     * @param givingMembers
     *            actual giving members entities
     * @param expectedGivingMembers
     *            expected giving members
     */
    private void verifyGivingMembers(List<EntityDTO> givingMembers, List<BudgetEntityStubDTO> expectedGivingMembers) {
        if (givingMembers == null || expectedGivingMembers == null) {
            assertNull(givingMembers);
            assertNull(expectedGivingMembers);
        } else {
            assertNotNull(givingMembers);
            assertNotNull(expectedGivingMembers);

            List<Long> expectedGroupIds = new ArrayList<Long>();
            List<Long> expectedPaxIds = new ArrayList<Long>();

            for (BudgetEntityStubDTO budgetEntityStubDTO : expectedGivingMembers) {
                if (budgetEntityStubDTO.getGroupId() != null && budgetEntityStubDTO.getPaxId() == null) {
                    expectedGroupIds.add(budgetEntityStubDTO.getGroupId());
                } else if (budgetEntityStubDTO.getPaxId() != null && budgetEntityStubDTO.getGroupId() == null) {
                    expectedPaxIds.add(budgetEntityStubDTO.getPaxId());
                } else {
                    fail("Got invalid Budget Entity Stub DTO!");
                }
            }

            for (EntityDTO givingMember : givingMembers) {
                if (givingMember instanceof EmployeeDTO) {
                    EmployeeDTO employeeDTO = (EmployeeDTO) givingMember;
                    Long paxId = employeeDTO.getPaxId();
                    assertTrue(expectedPaxIds.contains(paxId));
                    expectedPaxIds.remove(paxId);
                } else if (givingMember instanceof GroupDTO) {
                    GroupDTO groupDTO = (GroupDTO) givingMember;
                    Long groupId = groupDTO.getGroupId();
                    assertTrue(expectedGroupIds.contains(groupId));
                    expectedGroupIds.remove(groupId);
                } else {
                    fail("Got invalid Entity DTO!!");
                }
            }

            assertTrue(expectedGroupIds.isEmpty());
            assertTrue(expectedPaxIds.isEmpty());
        }
    }

    /**
     * Converts the given eligibility DTO to a corresponding eligibility stub
     * DTO.
     *
     * @param eligibility
     *            eligibility DTO
     * @return corresponding eligibility stub DTO
     */
    private EligibilityStubDTO convertEligibilityToStub(EligibilityDTO eligibility) {
        EligibilityStubDTO eligibilityStubDTO = new EligibilityStubDTO();

        // iterate through eligibility and extract just ids
        List<EligibilityEntityStubDTO> givers = convertEligibilityEntityDTOsToStubs(eligibility.getGive());
        List<EligibilityEntityStubDTO> receivers = convertEligibilityEntityDTOsToStubs(eligibility.getReceive());

        eligibilityStubDTO.setGive(givers);
        eligibilityStubDTO.setReceive(receivers);

        return eligibilityStubDTO;
    }

    /**
     * Converts the given list of entity DTOs to corresponding entity stub DTOs.
     *
     * @param entityDTOs
     *            entity DTOs to convert
     * @return entity stub DTOs that correspond to the given entity DTOs
     */
    private List<EligibilityEntityStubDTO> convertEligibilityEntityDTOsToStubs(List<EntityDTO> entityDTOs) {
        List<EligibilityEntityStubDTO> eligibilityEntityStubDTOs = new ArrayList<EligibilityEntityStubDTO>();

        if (entityDTOs != null && !entityDTOs.isEmpty()) {
            for (EntityDTO entityDTO : entityDTOs) {
                if (entityDTO instanceof EmployeeDTO) {
                    EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                    eligibilityEntityStubDTOs
                            .add(eligibilityEntityStubDTO.setPaxId(((EmployeeDTO) entityDTO).getPaxId()));
                } else if (entityDTO instanceof GroupDTO) {
                    EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
                    eligibilityEntityStubDTOs
                            .add(eligibilityEntityStubDTO.setGroupId(((GroupDTO) entityDTO).getGroupId()));
                } else {
                    fail("Entity DTO is not a participant or group!");
                }
            }
        }

        return eligibilityEntityStubDTOs;
    }

    /**
     * Retrieves the Program Activity DTO specified by the given project and
     * program activity IDs.
     * 
     * @param programId
     *            ID of the program activity
     * @return Program Activity DTO specified by the given project and program
     *         activity IDs
     */
    private ProgramFullDTO getProgramByProgramId(@Nonnull Long programId) {
        ProgramFullDTO ProgramFullDTO = programService.getProgramByID(programId, null);
        assertNotNull(ProgramFullDTO);

        return ProgramFullDTO;
    }

    /**
     * Retrieves a BudgetAssignmentDTO list specified by the given program ID.
     *
     * @param programId ID of the program
     *
     * @return List<BudgetAssignmentDTO> specified by the given program ID.
     */
    private List<BudgetAssignmentDTO> getProgramBudgetsByProgramId(@Nonnull Long programId) {
        List<BudgetAssignmentDTO> budgetAssignmentDTOList = programService.getProgramBudgetsByProgramID(programId);
        assertNotNull(budgetAssignmentDTOList);
        return budgetAssignmentDTOList;
    }

    /**
     * Verifies insert and update errors for specified Program Activity request.
     *
     * @param programRequestDTO
     *            Program Activity request to test
     * @param expectedError
     *            expected error
     * @param expectedNumberOfErrors
     *            expected number of errors
     */
    private void verifyInsertUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO, @Nonnull String expectedError,
            int expectedNumberOfErrors) {
        // verify errors on insert/update
        verifyProgramInsertErrors(programRequestDTO, expectedError, expectedNumberOfErrors);
        verifyProgramUpdateErrors(programRequestDTO, expectedError, expectedNumberOfErrors);
    }

    /**
     * Verifies insert and update errors for specified Program Activity request.
     *
     * @param programRequestDTO
     *            Program Activity request to test
     * @param expectedErrors
     *            expected errors
     */
    private void verifyInsertUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO,
            @Nonnull List<String> expectedErrors) {
        // verify errors on insert/update
        verifyProgramInsertErrors(programRequestDTO, expectedErrors);
        verifyProgramUpdateErrors(programRequestDTO, expectedErrors);
    }

    /**
     * Verifies that the creation of the specified Program Activity errors and
     * returns the specified errors.
     *
     * @param programRequestDTO
     *            request definition of the Program Activity to update
     * @param expectedError
     *            expected error to be shown when updating
     * @param expectedNumberOfErrors
     *            (expected number of errors, all of specified expected error)
     */
    private void verifyProgramInsertErrors(@Nonnull ProgramRequestDTO programRequestDTO, @Nonnull String expectedError,
            int expectedNumberOfErrors) {
        try {
            programService.insertProgram(programRequestDTO);
            fail("Creation of Program Activity should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedNumberOfErrors, expectedError, errorMsgException);
        }
    }

    /**
     * Verifies that the creation of the specified Program Activity errors and
     * returns the specified errors.
     *
     * @param programRequestDTO
     *            request definition of the Program Activity to update
     * @param expectedErrors
     *            expected errors to be shown when updating
     */
    private void verifyProgramInsertErrors(@Nonnull ProgramRequestDTO programRequestDTO,
            @Nonnull List<String> expectedErrors) {
        try {
            programService.insertProgram(programRequestDTO);
            fail("Creation of Program Activity should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedErrors, errorMsgException);
        }
    }

    /**
     * @see ProgramServiceTest#verifyProgramUpdateErrors(ProgramRequestDTO,
     *      String, Long, int)
     *
     *      Adds handling of program ID (just uses program with ID 1).
     */
    private void verifyProgramUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO, @Nonnull String expectedError,
            int expectedNumberOfErrors) {
        programRequestDTO.setProgramId(PROGRAM_NOT_USED);
        verifyProgramUpdateErrors(programRequestDTO, expectedError, PROGRAM_NOT_USED, expectedNumberOfErrors);
    }

    /**
     * Verifies that the update of the specified Program Activity errors and
     * returns the specified errors.
     *
     * @param programRequestDTO
     *            request definition of the Program Activity to update
     * @param expectedError
     *            expected error to be shown when updating
     * @param programId
     *            ID of the program (necessary for updates)
     * @param expectedNumberOfErrors
     *            (expected number of errors, all of specified expected error)
     */
    private void verifyProgramUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO, @Nonnull String expectedError,
            @Nonnull Long programId, int expectedNumberOfErrors) {
        try {
            programService.updateProgram(programRequestDTO, programId);
            fail("Update of Program Activity should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedNumberOfErrors, expectedError, errorMsgException);
        }
    }

    /**
     * @see ProgramServiceTest#verifyProgramUpdateErrors(ProgramRequestDTO,
     *      String, Long, int)
     *
     *      Adds handling of program ID (just uses program with ID 1).
     */
    private void verifyProgramUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO,
            @Nonnull List<String> expectedErrors) {
        programRequestDTO.setProgramId(PROGRAM_NOT_USED);
        verifyProgramUpdateErrors(programRequestDTO, expectedErrors, PROGRAM_NOT_USED);
    }

    /**
     * Verifies that the update of the specified Program Activity errors and
     * returns the specified errors.
     *
     * @param programRequestDTO
     *            request definition of the Program Activity to update
     * @param expectedErrors
     *            expected errors to be shown when updating
     * @param programId
     *            ID of the program (necessary for updates)
     */
    private void verifyProgramUpdateErrors(@Nonnull ProgramRequestDTO programRequestDTO,
            @Nonnull List<String> expectedErrors, @Nonnull Long programId) {
        try {
            programService.updateProgram(programRequestDTO, programId);
            fail("Update of Program Activity should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedErrors, errorMsgException);
        }
    }

    /**
     * Generates an AwardTier
     */
    private AwardTierDTO generateAwardTier(Long amount, Boolean allowRaising, List<ApprovalDTO> approvals,
            String type,Boolean enabled) {
        AwardTierDTO awardTier = new AwardTierDTO();
        awardTier.setAwardAmount(amount);
        awardTier.setAllowRaising(allowRaising);
        awardTier.setApprovals(approvals);
        awardTier.setType(type);
        awardTier.setApprovalReminderEmail(enabled);
        return awardTier;
    }

    /**
     * Generates an Approval
     */
    private ApprovalDTO generateApproval(int level, String type, Long paxId) {
        ApprovalDTO approval = new ApprovalDTO();
        approval.setLevel(level);
        approval.setType(type);
        approval.setPax(participantInfoDao.getEmployeeDTO(paxId));
        return approval;
    }

    @Test
    public void test_insert_program_ecard_only_no_ecard() throws Exception {

        ProgramRequestDTO programRequestDTO =
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramType(ProgramTypeCode.ECARD_ONLY.toString());
        programRequestDTO.setEcardIds(new ArrayList<Long>());

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("ECARD_REQUIRED"));
            }
        }
    }

    @Test
    public void test_insert_program_ecard_only_with_budget() throws Exception {
        Long expectedBudgetId = NON_ASSOCIATED_BUDGET_ID;
        List<Long> expectedGivingMembersGroupIds = new ArrayList<>(Arrays.asList(VALID_GROUP_ID_1));
        List<Long> expectedGivingMembersPaxIds = new ArrayList<>(Arrays.asList(VALID_PAX_ID_1));
        List<BudgetAssignmentStubDTO> budgetAssignmentStubDTOs = new ArrayList<BudgetAssignmentStubDTO>();

        ProgramRequestDTO programRequestDTO =
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramType(ProgramTypeCode.ECARD_ONLY.toString());
        budgetAssignmentStubDTOs.add(generateBudgetAssignmentStubDTOWithMembers(expectedBudgetId,
                expectedGivingMembersGroupIds, expectedGivingMembersPaxIds));
        programRequestDTO.setBudgetAssignments(budgetAssignmentStubDTOs);

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("BUDGET_NOT_ALLOWED"));
            }
        }
    }

    @Test
    public void test_insert_program_ecard_only_with_award_tier() throws Exception {
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramType(ProgramTypeCode.ECARD_ONLY.toString());
        programRequestDTO.setAwardTiers(generateValidAwardTier());

        try {
            programService.insertProgram(programRequestDTO);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is("AWARD_TIER_NOT_ALLOWED"));
            }
        }
    }

    @Test
    public void test_insert_program_ecard_only_all_good() throws Exception {
        ProgramRequestDTO programRequestDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), ProjectConstants.POST);
        programRequestDTO.setProgramType(ProgramTypeCode.ECARD_ONLY.toString());
    }

    @Test
    public void test_like_program() throws Exception {
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        ProgramFullDTO originalProgramDTO = programService.getProgramByID(LIKE_PROGRAM_ID, null);
        assertEquals(originalProgramDTO.getLikeCount(), 0L);
        ProgramFullDTO alexLikeDTO = likeService.likeProgram(TestConstants.PAX_MUDDAM, LIKE_PROGRAM_ID);
        assertEquals(alexLikeDTO.getLikeCount(), 1);
        assertEquals(alexLikeDTO.getLikePax().getPaxId(), TestConstants.PAX_MUDDAM);
        assertEquals(alexLikeDTO.getLikePax().getManagerPaxId(), TestConstants.PAX_PORTERGA);
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        ProgramFullDTO leahLikeDTO = likeService.likeProgram(TestConstants.PAX_HARWELLM, LIKE_PROGRAM_ID);
        assertEquals(leahLikeDTO.getLikeCount(), 2);
        assertEquals(leahLikeDTO.getLikePax().getPaxId(), TestConstants.PAX_HARWELLM);
        assertEquals(leahLikeDTO.getLikePax().getManagerPaxId(), TestConstants.PAX_PORTERGA);
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
        ProgramFullDTO finalProgramDTO = programService.getProgramByID(LIKE_PROGRAM_ID, null);
        assertEquals(finalProgramDTO.getLikeCount(), 2);
        assertEquals(finalProgramDTO.getLikePax().getPaxId(), TestConstants.PAX_MUDDAM);
        assertEquals(finalProgramDTO.getLikePax().getManagerPaxId(), TestConstants.PAX_PORTERGA);

    }

    private String getProgramStartDate(DateFormat formatter, int year, Calendar calendar) {
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date start = calendar.getTime();

        return formatter.format(start);
    }

    private String getProgramEndDate(DateFormat formatter, int year, Calendar calendar) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, 31);
        Date end = calendar.getTime();

        return formatter.format(end);
    }

}
