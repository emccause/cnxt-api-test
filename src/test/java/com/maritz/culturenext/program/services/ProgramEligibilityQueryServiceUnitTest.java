package com.maritz.culturenext.program.services;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramEligibilityDTO;
import com.maritz.culturenext.program.services.impl.ProgramEligibilityQueryServiceImpl;
import com.maritz.test.AbstractMockTest;

public class ProgramEligibilityQueryServiceUnitTest extends AbstractMockTest {

    @InjectMocks
    private ProgramEligibilityQueryServiceImpl programEligibilityQueryService;
    @Mock
    private ProgramEligibilityQueryDao programEligibilityResolverDao;

    @Test
    public void test_get_recognition_eligibility_empty_nodes() {
        PowerMockito.when(programEligibilityResolverDao.getRecognitionEligibility(TestConstants.PAX_ADAM_SMITH)).thenReturn(null);
        ProgramEligibilityDTO programEligibility = programEligibilityQueryService.getRecognitionEligibility(TestConstants.PAX_ADAM_SMITH);
        assertTrue(programEligibility.getGive().getAwardCodeCert() == null);
        assertTrue(programEligibility.getGive().getAwardCodeDownload() == null);
        assertTrue(programEligibility.getGive().getAwardCodeDownload() == null);
        assertTrue(programEligibility.getGive().getPointLoad() == null);
        assertTrue(programEligibility.getReceive().getRecognition() == null);
        assertTrue(programEligibility.getReceive().getAwardCodeCert() == null);
        assertTrue(programEligibility.getReceive().getAwardCodeDownload() == null);
        assertTrue(programEligibility.getReceive().getPointLoad() == null);
    }

    @Test
    public void test_get_recognition_eligibility_else_node() {
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.ROLE_CODE, "name");
        nodes.add(node);
        PowerMockito.when(programEligibilityResolverDao.getRecognitionEligibility(TestConstants.PAX_ADAM_SMITH)).thenReturn(nodes);
        ProgramEligibilityDTO programEligibility = programEligibilityQueryService.getRecognitionEligibility(TestConstants.PAX_ADAM_SMITH);
        assertTrue(programEligibility.getGive().getRecognition() == null);
        assertTrue(programEligibility.getGive().getAwardCodeCert() == null);
        assertTrue(programEligibility.getGive().getAwardCodeDownload() == null);
        assertTrue(programEligibility.getGive().getPointLoad() == null);
        assertTrue(programEligibility.getReceive().getRecognition() == null);
        assertTrue(programEligibility.getReceive().getAwardCodeCert() == null);
        assertTrue(programEligibility.getReceive().getAwardCodeDownload() == null);
        assertTrue(programEligibility.getReceive().getPointLoad() == null);
    }
}
