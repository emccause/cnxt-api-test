package com.maritz.culturenext.program.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.*;

import com.maritz.culturenext.jpa.entity.ProgramAwardTierReminder;
import com.maritz.culturenext.jpa.repository.CnxtProgramAwardTierRepository;
import com.maritz.culturenext.nomination.dao.NominationDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.context.ApplicationEventPublisher;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Ecard;
import com.maritz.core.jpa.entity.FileItem;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramAwardTierType;
import com.maritz.core.jpa.entity.ProgramBudget;
import com.maritz.core.jpa.entity.ProgramCriteria;
import com.maritz.core.jpa.entity.ProgramEcard;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.ProgramType;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.repository.AclRepository;
import com.maritz.core.jpa.repository.DiscretionaryRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.ProgramTypeRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.budget.dao.BudgetInfoDao;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentBudgetDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.budget.dto.BudgetEntityStubDTO;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.AwardTierType;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.enums.Visibility;
import com.maritz.culturenext.images.service.impl.FileImageServiceImpl;
import com.maritz.culturenext.jpa.repository.CnxtDiscretionaryRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramTypeRepository;
import com.maritz.culturenext.notifications.constants.NotificationConstants;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.EligibilityDao;
import com.maritz.culturenext.program.dao.ProgramBudgetsDao;
import com.maritz.culturenext.program.dao.ProgramDao;
import com.maritz.culturenext.program.dto.ApprovalDTO;
import com.maritz.culturenext.program.dto.AwardCodeProgramDetailsDTO;
import com.maritz.culturenext.program.dto.AwardsDTO;
import com.maritz.culturenext.program.dto.Certificates;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.culturenext.program.dto.NominationPeriodDTO;
import com.maritz.culturenext.program.dto.PinnacleDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.program.milestone.service.MilestoneReminderService;
import com.maritz.culturenext.program.milestone.service.MilestoneService;
import com.maritz.culturenext.program.pinnacle.constants.PinnacleConstants;
import com.maritz.culturenext.program.pinnacle.service.PinnacleService;
import com.maritz.culturenext.program.services.impl.ProgramServiceImpl;
import com.maritz.culturenext.program.util.ProgramTranslationUtil;
import com.maritz.culturenext.program.validators.DefaultProgramValidator;
import com.maritz.culturenext.program.validators.PinnacleProgramValidator;
import com.maritz.culturenext.program.validators.ProgramValidatorContext;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.util.AclUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.survey.dto.QuestionDTO;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class ProgramServiceUnitTest {
    @InjectMocks private ProgramServiceImpl programService;
    @Mock private ApplicationEventPublisher applicationEventPublisher;
    @Mock private AclRepository aclRepository;
    @Mock private AclUtil aclUtil;
    @Mock private BudgetInfoDao budgetInfoDao;
    @Mock private CnxtDiscretionaryRepository cnxtDiscretionaryRepository;
    @Mock private CnxtProgramTypeRepository cnxtProgramTypeRepository;
    @Mock private ConcentrixDao<Acl> aclDao;
    @Mock private ConcentrixDao<ApprovalProcessConfig> approvalProcessConfigDao;
    @Mock private ConcentrixDao<Budget> budgetDao;
    @Mock private ConcentrixDao<Ecard> ecardDao;
    @Mock private ConcentrixDao<Files> filesDao;
    @Mock private ConcentrixDao<FileItem> fileItemDao;
    @Mock private ConcentrixDao<Lookup> lookupDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Mock private ConcentrixDao<ProgramAwardTierReminder> programAwardTierReminderDao;
    @Mock private CnxtProgramAwardTierRepository programAwardTierRepository;
    @Mock private ConcentrixDao<ProgramAwardTierType> programAwardTierTypeDao;
    @Mock private ConcentrixDao<ProgramBudget> programBudgetDao;
    @Mock private ConcentrixDao<ProgramCriteria> programCriteriaDao;
    @Mock private ConcentrixDao<ProgramEcard> programEcardDao;
    @Mock private ConcentrixDao<ProgramMisc> programMiscDao;
    @Mock private ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Mock private ConcentrixDao<Role> roleDao;
    @Mock private DefaultProgramValidator defaultProgramValidator;
    @Mock private DiscretionaryRepository discretionaryRepository;
    @Mock private EligibilityDao eligibilityDao;
    @Mock private FileImageServiceImpl fileImageService;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxRepository paxRepository;
    @Mock private ProgramDao programDao;
    @Mock private ProgramRepository programRepository;
    @Mock private ProgramTranslationUtil programTranslationUtil;
    @Mock private ProgramTypeRepository programTypeRepository;
    @Mock private ProgramValidatorContext programValidatorContext;
    @Mock private Security security;
    @Mock private MilestoneService milestoneService;
    @Mock private MilestoneReminderService milestoneReminderService;
    @Mock private PinnacleService pinnacleService;
    @Mock private PinnacleProgramValidator pinnacleProgramValidator;
    @Mock private ProgramBudgetsDao programBudgetsDao;
    @Mock private NominationDao nominationDaoInterface;

    private com.maritz.core.jpa.support.findby.FindBy<Program> programFindBy = PowerMockito.mock(com.maritz.core.jpa.support.findby.FindBy.class);
    private FindBy<Acl> aclFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalProcessConfig> mockedFindByApprovalProcessConfig = PowerMockito.mock(FindBy.class);
    private FindBy<Budget> budgetFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Files> mockedFindByFiles = PowerMockito.mock(FindBy.class);
    private FindBy<FileItem> mockedFindByFileItem = PowerMockito.mock(FindBy.class);
    private FindBy<Lookup> mockedFindByLookup = PowerMockito.mock(FindBy.class);
    private FindBy<Nomination> mockedFindByNomination = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramAwardTier> mockedFindByProgramAwardTier = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramAwardTierType> programAwardTierTypeFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramBudget> mockedFindByProgramBudget = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramCriteria> mockedFindByProgramCriteria = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramEcard> mockedFindByProgramEcard = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> mockedFindByProgramMisc = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> mockedFindByProgramMiscClaimInstructions = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> mockedFindByProgramMiscIsPrintable = PowerMockito.mock(FindBy.class);
    private FindBy<ProgramMisc> mockedFindByProgramMiscIsDownloadable = PowerMockito.mock(FindBy.class);
    private FindBy<Role> roleFindBy = PowerMockito.mock(FindBy.class);

    private Long groupId;
    private Long programID;
    private Program program;
    private ProgramFullDTO programFullDTO;
    private ProgramRequestDTO programRequestDTO;

    private static final String TEST_CLAIMING_INSTRUCTIONS_MISC_DATA = "Claiming instructions text";
    private static final String TEST_IS_DOWNLOADABLE_MISC_DATA = "TRUE";
    private static final String TEST_IS_PRINTABLE_MISC_DATA = "TRUE";

    private static final String TEST_FILE_NAME_1 = "test_file_1";
    private static final String TEST_FILE_NAME_2 = "test_file_2";
    private static final String TEST_FILE_NAME_3 = "test_file_3";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        program = new Program();
        programFullDTO = new ProgramFullDTO();
        programRequestDTO = new ProgramRequestDTO();
        programID = 635L;
        groupId = 987L;

        program.setProgramId(programID);
        program.setProgramName("Program Award Tiers testing Dan's program");
        program.setProgramDesc("Program Award Tiers testing Dan's program");
        program.setFromDate(new Date());
        program.setProgramTypeCode(ProgramTypeEnum.PEER_TO_PEER.getCode());
        program.setCreateId("YUKIOM");
        program.setUpdateId("dbo");
        program.setCreatorPaxId(TestConstants.PAX_DAGARFIN);
        program.setProgramLongDesc("Program Award Tiers testing Dan's program");
        program.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        program.setProgramCategoryCode(AlertSubType.RECOGNITION.toString());

        programRequestDTO.setProgramId(programID);
        programRequestDTO.setProgramName("Program Award Tiers testing Dan's program");
        programRequestDTO.setProgramDescriptionShort("Program Award Tiers testing Dan's program");
        programRequestDTO.setProgramType(ProgramTypeEnum.PEER_TO_PEER.name());
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setFromDate(DateUtils.format(DateUtil.getCurrentDateAtMidnight(), DateUtils.ISO_8601_DATETIME_FORMAT));

        programRequestDTO.setNewsfeedVisibility(NewsfeedVisibility.RECIPIENT_PREF.toString());
        programRequestDTO.setProgramVisibility(Visibility.PUBLIC.toString());
        programRequestDTO.setNotification(Arrays.asList(VfName.NOTIFICATION_RECIPIENT.toString(),
                VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        programRequestDTO.setProgramDescriptionLong("A longer description");
        programRequestDTO.setAllowTemplateUpload(Boolean.TRUE);
        programRequestDTO.setPayoutType(TestConstants.DPP);

        ProgramType programType = new ProgramType();
        programType.setId(4L);
        programType.setProgramTypeCode(ProgramTypeEnum.PEER_TO_PEER.getCode());
        programType.setProgramTypeDesc(ProgramTypeEnum.PEER_TO_PEER.name());
        programType.setCreateId("ccx162client");
        programType.setUpdateId("ccx162client");

        ProgramType programTypeAwardCode = new ProgramType();
        programType.setId(TestConstants.ID_1);
        programType.setProgramTypeCode("AWRD");
        programType.setProgramTypeDesc("AWARD_CODE");

        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setPaxId(4417L);
        employeeDTO.setLanguageCode("en");
        employeeDTO.setPreferredLocale("en_US");
        employeeDTO.setControlNum("4417");
        employeeDTO.setFirstName("Dan");
        employeeDTO.setLastName("Garf");
        employeeDTO.setCompanyName("MR");
        employeeDTO.setJobTitle("culturenext Team Member");
        employeeDTO.setStatus(StatusTypeCode.ACTIVE.name());

        List<ProgramEcard> programEcardList = new ArrayList<ProgramEcard>();
        ProgramEcard programEcard = new ProgramEcard();
        programEcard.setId(TestConstants.ID_1);
        programEcardList.add(programEcard);

        List<ProgramCriteria> programCriteriaList = new ArrayList<ProgramCriteria>();
        ProgramCriteria programCriteria = new ProgramCriteria();
        programCriteria.setId(TestConstants.ID_1);
        programCriteriaList.add(programCriteria);

        // programEcardDao.findBy().where("programId").eq(programId).find()
        PowerMockito.when(programEcardDao.findBy()).thenReturn(mockedFindByProgramEcard);
        PowerMockito.when(mockedFindByProgramEcard.where(anyString())).thenReturn(mockedFindByProgramEcard);
        PowerMockito.when(mockedFindByProgramEcard.eq(anyLong())).thenReturn(mockedFindByProgramEcard);
        PowerMockito.when(mockedFindByProgramEcard.find()).thenReturn(programEcardList);

        // programCriteriaDao.findBy().where("programId").eq(programId).find()
        PowerMockito.when(programCriteriaDao.findBy()).thenReturn(mockedFindByProgramCriteria);
        PowerMockito.when(mockedFindByProgramCriteria.where(anyString())).thenReturn(mockedFindByProgramCriteria);
        PowerMockito.when(mockedFindByProgramCriteria.eq(anyLong())).thenReturn(mockedFindByProgramCriteria);
        PowerMockito.when(mockedFindByProgramCriteria.find()).thenReturn(programCriteriaList);

        //programMiscDao.findBy().where(ProgramConstants.PROGRAM_ID).eq(programBasicDTO.getProgramId())
        //.and("vfName").eq(vfName).findOne()
        PowerMockito.when(programMiscDao.findBy()).thenReturn(mockedFindByProgramMisc);
        PowerMockito.when(mockedFindByProgramMisc.where(anyString())).thenReturn(mockedFindByProgramMisc);
        PowerMockito.when(mockedFindByProgramMisc.eq(anyLong())).thenReturn(mockedFindByProgramMisc);
        PowerMockito.when(mockedFindByProgramMisc.and(anyString())).thenReturn(mockedFindByProgramMisc);
        PowerMockito.when(mockedFindByProgramMisc.eq(anyString())).thenReturn(mockedFindByProgramMisc);
        PowerMockito.when(mockedFindByProgramMisc.findOne()).thenReturn(null);

        PowerMockito.when(lookupDao.findBy()).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.where(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.eq(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.and(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.eq(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.and(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.eq(anyString())).thenReturn(mockedFindByLookup);
        PowerMockito.when(mockedFindByLookup.findOne()).thenReturn(new Lookup());

        // aclUtil.getAclEligibilityGivers(programID);
        Acl testGroupAcl = new Acl();
        testGroupAcl.setSubjectTable(TableName.GROUPS.toString());
        testGroupAcl.setSubjectId(groupId);

        List<Acl> aclUtilList = Arrays.asList(testGroupAcl);
        PowerMockito.when(aclUtil.getAclEligibilityGivers(anyLong())).thenReturn(aclUtilList);
        PowerMockito.when(aclUtil.getAclEligibilityReceivers(anyLong())).thenReturn(aclUtilList);

        // programBudgetDao.findBy().where("programId").eq(programId).find()
        List<ProgramBudget> programBudgetList = new ArrayList<>();
        ProgramBudget programBudget = new ProgramBudget();
        programBudget.setProgramId(programID);
        programBudgetList.add(programBudget);

        PowerMockito.when(programBudgetDao.findBy()).thenReturn(mockedFindByProgramBudget);
        PowerMockito.when(mockedFindByProgramBudget.where(anyString())).thenReturn(mockedFindByProgramBudget);
        PowerMockito.when(mockedFindByProgramBudget.eq(anyLong())).thenReturn(mockedFindByProgramBudget);
        PowerMockito.when(mockedFindByProgramBudget.find()).thenReturn(programBudgetList);

        //budgetInfoDao.getProgramBudgets(programBudgetIdList);
        List<Long> programBudgetIdList = new ArrayList<>();
        programBudgetIdList.add(TestConstants.ID_1);
        PowerMockito.when(budgetInfoDao.getProgramBudgets(programBudgetIdList)).thenReturn(null);

        //programAwardTierDao.findBy().where(AWARD_TIERS_PROGRAM_ID).eq(programId).find();
        List<ProgramAwardTier> programAwardTierList = new ArrayList<>();
        ProgramAwardTier programAwardTier = new ProgramAwardTier();
        programAwardTier.setId(635L);
        programAwardTier.setMinAmount(10.00);
        programAwardTier.setRaisingStatusTypeCode(StatusTypeCode.ACTIVE.name());
        programAwardTier.setProgramAwardTierTypeCode(AwardTierType.DISCRETE.toString());
        programAwardTierList.add(programAwardTier);
        ProgramAwardTier programAwardTier2 = new ProgramAwardTier();
        programAwardTier2.setId(675L);
        programAwardTier2.setMinAmount(10.0);
        programAwardTier2.setMaxAmount(20.0);
        programAwardTier2.setProgramAwardTierTypeCode(AwardTierType.RANGE.toString());
        programAwardTier2.setRaisingStatusTypeCode(StatusTypeCode.ACTIVE.name());
        programAwardTierList.add(programAwardTier2);

        PowerMockito.when(programAwardTierDao.findBy()).thenReturn(mockedFindByProgramAwardTier);
        PowerMockito.when(mockedFindByProgramAwardTier.where(anyString())).thenReturn(mockedFindByProgramAwardTier);
        PowerMockito.when(mockedFindByProgramAwardTier.eq(anyLong())).thenReturn(mockedFindByProgramAwardTier);
        PowerMockito.when(mockedFindByProgramAwardTier.and(anyString())).thenReturn(mockedFindByProgramAwardTier);
        PowerMockito.when(mockedFindByProgramAwardTier.eq(anyString())).thenReturn(mockedFindByProgramAwardTier);
        PowerMockito.when(mockedFindByProgramAwardTier.find()).thenReturn(programAwardTierList);

        // setup mock to populate id on programAwardTiers

        // nominationDao.findBy().where(NOMINATION_PROGRAM_ID).eq(programId).find();
        PowerMockito.when(nominationDao.findBy()).thenReturn(mockedFindByNomination);
        PowerMockito.when(mockedFindByNomination.where(anyString())).thenReturn(mockedFindByNomination);
        PowerMockito.when(mockedFindByNomination.eq(anyLong())).thenReturn(mockedFindByNomination);
        PowerMockito.when(mockedFindByNomination.and(anyString())).thenReturn(mockedFindByNomination);
        PowerMockito.when(mockedFindByNomination.isNull()).thenReturn(mockedFindByNomination);
        PowerMockito.when(mockedFindByNomination.find()).thenReturn(null);

        List<Discretionary> discretionaryList = new ArrayList<>();
        Discretionary discretionary = new Discretionary();
        discretionaryList.add(discretionary);
        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(discretionaryList);

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(discretionaryList);

        // approvalProcessConfigDao.findBy().where(APPROVAL_PROCESS_CONFIG_TARGETID)
        //.eq(programAwardTier.getId()).find();
        List<ApprovalProcessConfig> approvalProcessConfigList = new ArrayList<>();
        ApprovalProcessConfig approvalProcessConfig = new ApprovalProcessConfig();
        approvalProcessConfigList.add(approvalProcessConfig);

        PowerMockito.when(approvalProcessConfigDao.findBy()).thenReturn(mockedFindByApprovalProcessConfig);
        PowerMockito.when(mockedFindByApprovalProcessConfig.where(anyString()))
            .thenReturn(mockedFindByApprovalProcessConfig);
        PowerMockito.when(mockedFindByApprovalProcessConfig.eq(anyLong()))
            .thenReturn(mockedFindByApprovalProcessConfig);
        PowerMockito.when(mockedFindByApprovalProcessConfig.find()).thenReturn(approvalProcessConfigList);

        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);
        PowerMockito.when(programTypeRepository.findByProgramTypeCode(ProgramTypeEnum.PEER_TO_PEER.getCode()))
            .thenReturn(programType);
        PowerMockito.when(programTypeRepository.findByProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode()))
            .thenReturn(programTypeAwardCode);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(program.getCreatorPaxId())).thenReturn(employeeDTO);
        PowerMockito.when(programDao.getProgramImageId(program.getProgramId())).thenReturn(TestConstants.ID_1);
        PowerMockito.when(programDao.getProgramVisibility(programFullDTO.getProgramId())).thenReturn(null);

        Map<String, String> mapProgramDtoData = new HashMap();
        mapProgramDtoData.put(VfName.NEWSFEED_VISIBILITY.toString(),"RECIPIENT_PREF");
        mapProgramDtoData.put(VfName.NOTIFICATION_RECIPIENT.toString(),"TRUE");
        mapProgramDtoData.put(VfName.NOTIFICATION_RECIPIENT_MGR.toString(),"TRUE");
        mapProgramDtoData.put(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString(),"TRUE");
        mapProgramDtoData.put(VfName.NOTIFICATION_SUBMITTER.toString(),"FALSE");
        mapProgramDtoData.put(VfName.ALLOW_TEMPLATE_UPLOAD.toString(),"");
        mapProgramDtoData.put(VfName.SELECTED_AWARD_TYPE.toString(),"DPP");
        mapProgramDtoData.put(VfName.PPP_INDEX_ENABLED.toString(),"");
        mapProgramDtoData.put(VfName.REMINDER_DAYS.toString(),"");

        Mockito.when(programDao.getProgramDtoData(anyLong())).thenReturn(mapProgramDtoData);

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_DAGARFIN);
        Mockito.doNothing().when(programDao).setLikeInfo(anyLong(), any(ProgramFullDTO.class));

        PowerMockito.when(programRepository.findBy()).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.where(anyString())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.eq(anyString())).thenReturn(programFindBy);
        PowerMockito.when(programFindBy.findAll(anyString(), any(Class.class))).thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtProgramTypeRepository.findByProgramTypeName(anyString())).thenReturn(programType);

        PowerMockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ((Program)args[0]).setProgramId(programID);
                return null;
            }
        }).when(programRepository).save(Matchers.any(Program.class));

        PowerMockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ((ProgramAwardTier)args[0]).setId(1L);
                return null;
            }
        }).when(programAwardTierDao).save(Matchers.any(ProgramAwardTier.class));

        Role role = new Role();
        role.setRoleId(4417L);

        PowerMockito.when(roleDao.findBy()).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.where(anyString())).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.eq(anyString())).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.findOne()).thenReturn(role);

        List<ProgramAwardTierType> programAwardTierTypeList = new ArrayList<>();
        ProgramAwardTierType discreteType = new ProgramAwardTierType();
        discreteType.setProgramAwardTierTypeCode(AwardTierType.DISCRETE.toString());
        ProgramAwardTierType rangeType = new ProgramAwardTierType();
        rangeType.setProgramAwardTierTypeCode(AwardTierType.RANGE.toString());
        programAwardTierTypeList.add(discreteType);
        programAwardTierTypeList.add(rangeType);

        PowerMockito.when(programAwardTierTypeDao.findBy()).thenReturn(programAwardTierTypeFindBy);
        PowerMockito.when(programAwardTierTypeFindBy.find()).thenReturn(programAwardTierTypeList);

        // programTranslationUtil
        PowerMockito.doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ProgramSummaryDTO summaryDTO = (ProgramSummaryDTO)args[0];
                summaryDTO.setProgramName("Translated name");
                summaryDTO.setProgramDescriptionShort("Translated short desc");
                summaryDTO.setProgramDescriptionLong("Translated long desc");
                return null;
            }
        }).when(programTranslationUtil)
        .translateProgramFieldsOnProgramSummaryDTO(Matchers.any(ProgramSummaryDTO.class), Matchers.eq("de_DE"));

        // eligibilityDao.getEligibilityEntities(paxIds, groupsIds)
        EntityDTO testGroup = new GroupDTO();
        ((GroupDTO) testGroup).setGroupId(groupId);
        ((GroupDTO) testGroup).setField("Test group desc");
        ((GroupDTO) testGroup).setFieldDisplayName("Test group name");
        List<EntityDTO> testEntityDTOs = Arrays.asList(testGroup);

        PowerMockito.when(eligibilityDao.getEligibilityEntities(anyListOf(Long.class), anyListOf(Long.class)))
            .thenReturn(testEntityDTOs);


        //budgetDao.findBy().where(BudgetConstants.PARENT_BUDGET_ID).in(budgetIdList).find()

        PowerMockito.when(budgetDao.findBy()).thenReturn(budgetFindBy);
        PowerMockito.when(budgetFindBy.where(anyString())).thenReturn(budgetFindBy);
        PowerMockito.when(budgetFindBy.in(anyListOf(Long.class))).thenReturn(budgetFindBy);
        PowerMockito.when(budgetFindBy.find()).thenReturn(new ArrayList<Budget>());

        PowerMockito.when(filesDao.findBy()).thenReturn(mockedFindByFiles);
        PowerMockito.when(mockedFindByFiles.where(anyString())).thenReturn(mockedFindByFiles);
        PowerMockito.when(mockedFindByFiles.eq(any())).thenReturn(mockedFindByFiles);
        PowerMockito.when(mockedFindByFiles.and(anyString())).thenReturn(mockedFindByFiles);
        PowerMockito.when(mockedFindByFiles.in(anyListOf(Long.class))).thenReturn(mockedFindByFiles);
        PowerMockito.when(mockedFindByFiles.find(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1,TestConstants.ID_2, 3L));
        PowerMockito.when(mockedFindByFiles.findOne(ProjectConstants.ID, Long.class)).thenReturn(TestConstants.ID_1);
        PowerMockito.when(mockedFindByFiles.findOne(ProjectConstants.FILE_TYPE_CODE, String.class))
            .thenReturn(FileTypes.PROGRAM_IMAGE.name(), FileTypes.AWARD_CODE_ICON.name(),
                    FileTypes.AWARD_CODE_CERT.name());

        ProgramMisc programMiscClaimingInstructions = new ProgramMisc();
        programMiscClaimingInstructions.setVfName(VfName.CLAIM_INSTRUCTIONS.name());
        programMiscClaimingInstructions.setMiscData(TEST_CLAIMING_INSTRUCTIONS_MISC_DATA);

        PowerMockito.when(mockedFindByProgramMisc.eq(VfName.CLAIM_INSTRUCTIONS.name()))
            .thenReturn(mockedFindByProgramMiscClaimInstructions);
        PowerMockito.when(mockedFindByProgramMiscClaimInstructions.findOne())
            .thenReturn(programMiscClaimingInstructions);
        PowerMockito.when(mockedFindByProgramMiscClaimInstructions
                .findOne(ProjectConstants.PROGRAM_MISC_ID, Long.class))
            .thenReturn(TestConstants.ID_1);

        ProgramMisc programMiscIsDownloadable = new ProgramMisc();
        programMiscIsDownloadable.setVfName(VfName.IS_DOWNLOADABLE.name());
        programMiscIsDownloadable.setMiscData(TEST_IS_DOWNLOADABLE_MISC_DATA);

        PowerMockito.when(mockedFindByProgramMisc.eq(VfName.IS_DOWNLOADABLE.name()))
            .thenReturn(mockedFindByProgramMiscIsDownloadable);
        PowerMockito.when(mockedFindByProgramMiscIsDownloadable
                .findOne(ProjectConstants.PROGRAM_MISC_ID, Long.class))
            .thenReturn(TestConstants.ID_1);

        ProgramMisc programMiscIsPrintable = new ProgramMisc();
        programMiscIsPrintable.setVfName(VfName.IS_PRINTABLE.name());
        programMiscIsPrintable.setMiscData(TEST_IS_PRINTABLE_MISC_DATA);

        PowerMockito.when(mockedFindByProgramMisc.eq(VfName.IS_PRINTABLE.name()))
            .thenReturn(mockedFindByProgramMiscIsPrintable);
        PowerMockito.when(mockedFindByProgramMiscIsPrintable
                .findOne(ProjectConstants.PROGRAM_MISC_ID, Long.class))
            .thenReturn(TestConstants.ID_1);

        PowerMockito.when(aclDao.findBy()).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.where(anyString())).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.eq(anyLong())).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.and(anyString())).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.eq(anyString())).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.in(anyListOf(Long.class))).thenReturn(aclFindBy);
        PowerMockito.when(aclFindBy.find()).thenReturn(new ArrayList<Acl>());

        //FileImageService  saveFileItemsEntry(Long filesId, String targetTable , Long targetId)
        PowerMockito.doNothing().when(fileImageService).saveFileItemsEntry(anyLong(),anyString(),anyLong());

        FileItem programFilesImage = new FileItem();
        programFilesImage.setId(TestConstants.ID_1);
        programFilesImage.setFilesId(TestConstants.ID_1);

        FileItem programFilesAwardCodeIcon = new FileItem();
        programFilesAwardCodeIcon.setId(TestConstants.ID_2);
        programFilesAwardCodeIcon.setFilesId(TestConstants.ID_2);

        FileItem programFilesAwardCodeCert = new FileItem();
        programFilesAwardCodeCert.setId(3L);
        programFilesAwardCodeCert.setFilesId(3L);

        PowerMockito.when(fileItemDao.findBy()).thenReturn(mockedFindByFileItem);
        PowerMockito.when(mockedFindByFileItem.where(anyString())).thenReturn(mockedFindByFileItem);
        PowerMockito.when(mockedFindByFileItem.eq(anyLong())).thenReturn(mockedFindByFileItem);
        PowerMockito.when(mockedFindByFileItem.and(anyString())).thenReturn(mockedFindByFileItem);
        PowerMockito.when(mockedFindByFileItem.eq(anyString())).thenReturn(mockedFindByFileItem);
        PowerMockito.when(mockedFindByFileItem.find())
            .thenReturn(Arrays.asList(programFilesImage, programFilesAwardCodeIcon, programFilesAwardCodeCert));

        when(pinnacleProgramValidator.validateProgram(Matchers.any(ProgramRequestDTO.class), Matchers.anyString(), Matchers.anyLong())).thenReturn(new ArrayList<>());
    }

    @Test
    public void test_happy_path_get_program_by_id() {
        ProgramFullDTO programFullDTO = programService.getProgramByID(635L, null);
        assertTrue(programFullDTO.getImageId().equals(TestConstants.ID_1));
    }

    @Test
    public void test_get_program_by_id_null() {
        try {
            ProgramFullDTO programFullDTO = programService.getProgramByID(null, null);
        } catch (ErrorMessageException e){
            assertTrue(e.getErrorMessages().size() == 1);
        }
    }

    @Test
    public void test_get_program_image_id_null() {
        PowerMockito.when(mockedFindByFileItem.find()).thenReturn(null);
        ProgramFullDTO programFullDTO = programService.getProgramByID(635L, null);
        assertTrue(programFullDTO.getImageId() == null);
    }

    @Test
    public void test_get_program_by_id_german() {
        ProgramFullDTO programFullDTO = programService.getProgramByID(635L, "de_DE");
        assertNotNull(programFullDTO);
        assertEquals("Translated name", programFullDTO.getProgramName());
        assertEquals("Translated short desc", programFullDTO.getProgramDescriptionShort());
        assertEquals("Translated long desc", programFullDTO.getProgramDescriptionLong());
    }

    @Test
    public void test_happy_path_get_program_budgets_by_program_id() {
        List<BudgetAssignmentDTO> budgetAssignmentDTOList = programService.getProgramBudgetsByProgramID(635L);
        assertNotNull(budgetAssignmentDTOList);
    }

    @Test
    public void test_get_program_budgets_by_program_id_null() {
        try {
            List<BudgetAssignmentDTO> budgetAssignmentDTOList = programService.getProgramBudgetsByProgramID(null);
        }
        catch(ErrorMessageException e) {
            assertEquals(1, e.getErrorMessages().size());
        }
    }

    @Test
    public void test_simple_post_program_with_awardTier_happy () {
        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMin(10L);
        awardTierRange.setAwardMax(50L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentDTO budgetAssignment = new BudgetAssignmentDTO();
        BudgetAssignmentBudgetDTO budgetAssignmentBudget = new BudgetAssignmentBudgetDTO();
        budgetAssignmentBudget.setBudgetId(TestConstants.ID_1);
        budgetAssignment.setBudget(budgetAssignmentBudget);
        List<EntityDTO> givingMembers = new ArrayList<>();
        EntityDTO member = new EntityDTO();
        givingMembers.add(member);
        budgetAssignment.setGivingMembers(givingMembers);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        ProgramFullDTO response = programService.insertProgram(programRequestDTO);

        assertTrue(response != null);
        assertTrue(response.getAwardTiers().size() == 2);


    }

    @Test
    public void test_simple_put_program_with_missing_award_type_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setAwardMin(10L);
        awardTierRange.setAwardMax(50L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_range_award_missing_min_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMax(50L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_range_award_missing_max_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMin(10L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }


        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_discrete_missing_award_amount_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMin(10L);
        awardTierRange.setAwardMax(50L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_range_min_max_equal_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMin(10L);
        awardTierRange.setAwardMax(10L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_range_min_greater_than_max_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.RANGE.toString());
        awardTierRange.setAwardMin(50L);
        awardTierRange.setAwardMax(10L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_simple_put_program_with_range_min_less_than_mininum_fail () {

        PowerMockito.when(cnxtDiscretionaryRepository.getPointLoadBudgetAllocations(anyLong()))
            .thenReturn(new ArrayList<>());

        PowerMockito.when(cnxtDiscretionaryRepository.getTopPointLoadBudgetAllocations(anyLong()))
                .thenReturn(new ArrayList<>());

        ProgramFullDTO response = null;

        List<AwardTierDTO> awardTierList = new ArrayList<>();
        AwardTierDTO awardTierRange = new AwardTierDTO();
        awardTierRange.setType(AwardTierType.DISCRETE.toString());
        awardTierRange.setAwardMin(0L);
        awardTierRange.setAwardMax(10L);
        awardTierRange.setAllowRaising(true);
        awardTierRange.setApprovalReminderEmail(true);
        AwardTierDTO awardTierDiscrete = new AwardTierDTO();
        awardTierDiscrete.setType(AwardTierType.DISCRETE.toString());
        awardTierDiscrete.setAwardAmount(10L);
        awardTierDiscrete.setAllowRaising(true);
        awardTierDiscrete.setApprovalReminderEmail(true);
        awardTierList.add(awardTierRange);
        awardTierList.add(awardTierDiscrete);

        List<BudgetAssignmentStubDTO> budgetAssignmentBudgetList = new ArrayList<>();
        BudgetAssignmentStubDTO budgetAssignmentStubDTO = new BudgetAssignmentStubDTO();
        BudgetEntityStubDTO budgetEntityStubDTO = new BudgetEntityStubDTO();
        budgetEntityStubDTO.setBudgetId(TestConstants.ID_1);
        BudgetEntityStubDTO memberEntityStubDTO = new BudgetEntityStubDTO();
        memberEntityStubDTO.setPaxId(TestConstants.PAX_DAGARFIN);
        budgetAssignmentStubDTO.setBudget(budgetEntityStubDTO);
        List<BudgetEntityStubDTO> members = new ArrayList<>();
        members.add(memberEntityStubDTO);
        budgetAssignmentStubDTO.setGivingMembers(members);
        budgetAssignmentBudgetList.add(budgetAssignmentStubDTO);

        programRequestDTO.setAwardTiers(awardTierList);
        programRequestDTO.setBudgetAssignments(budgetAssignmentBudgetList);

        try {
            response = programService.updateProgram(programRequestDTO, programID);
        } catch (ErrorMessageException e) {
            assertTrue(e.getErrorMessages().size() == 1);
        }

        assertTrue(response == null);

    }

    @Test
    public void test_program_long_desc_length_validation () {
        char longDesc[] = new char[ProgramConstants.LONG_PROGRAM_DESCRIPTION_MAX_LENGTH + 1];
        programRequestDTO.setProgramDescriptionLong(new String(longDesc));

        try {
            programService.insertProgram(programRequestDTO);
            fail();
        } catch (ErrorMessageException ex) {
            assertTrue(ex.getMessage().contains(ProgramConstants.ERROR_LONG_PROGRAM_DESCRIPTION_TOO_LONG));
        }
    }

    @Test
    public void test_get_program_by_id_award_code_program_type() {

        // Award Code program type
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);

        ProgramFullDTO programFullDTO = programService.getProgramByID(635L, null);

        assertNotNull(programFullDTO.getAwardCode());
        assertNotNull(programFullDTO.getAwardCode().getClaimingInstructions());
        assertNotNull(programFullDTO.getAwardCode().getCertificates());
        assertNotNull(programFullDTO.getAwardCode().getIconId());
    }

    @Test
    public void test_insert_program_success_award_code_type_active() {

        // Award Code program type
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);

        addAwardCodeDefaultDetailsObject(programRequestDTO);

        ProgramFullDTO response = programService.insertProgram(programRequestDTO);

        assertTrue(response != null);
        assertNotNull(response.getAwardCode());
        assertNotNull(response.getAwardCode());
    }

    @Test
    public void test_insert_program_success_award_code_type_inactive() {

        // Award Code program type
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);

        addAwardCodeDefaultDetailsObject(programRequestDTO);
        programRequestDTO.setStatus(StatusTypeCode.INACTIVE.name());
        programRequestDTO.getAwardCode().setClaimingInstructions(null);
        programRequestDTO.getAwardCode().setCertificates(null);
        programRequestDTO.getAwardCode().setIconId(null);
        programRequestDTO.getAwardCode().setDownloadable(null);
        programRequestDTO.getAwardCode().setPrintable(null);

        ProgramFullDTO response = programService.insertProgram(programRequestDTO);

        assertTrue(response != null);
        assertNotNull(response.getAwardCode());
    }

    @Test
    public void test_insert_program_success_award_code_downloadable() {

        // Award Code program type
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);

        addAwardCodeDefaultDetailsObject(programRequestDTO);
        programRequestDTO.getAwardCode().setPrintable(Boolean.FALSE);
        programRequestDTO.getAwardCode().setCertificates(null);

        ProgramFullDTO response = programService.insertProgram(programRequestDTO);

        assertTrue(response != null);
        assertNotNull(response.getAwardCode());
    }

    @Test
    public void test_insert_program_success_award_code_printable() {

        // Award Code program type
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        PowerMockito.when(programRepository.findOne(programID)).thenReturn(program);

        addAwardCodeDefaultDetailsObject(programRequestDTO);
        programRequestDTO.getAwardCode().setDownloadable(Boolean.FALSE);

        ProgramFullDTO response = programService.insertProgram(programRequestDTO);

        assertTrue(response != null);
        assertNotNull(response.getAwardCode());
    }

    @Test
    public void test_populate_award_code_details_null(){
        ProgramSummaryDTO programDTO = new ProgramSummaryDTO();
        programDTO.setProgramId(1L);
        programService.populateAwardCodeDetails(null, null);
        assertNull(programDTO.getAwardCode());
    }

    @Test
    public void test_insert_update_milestone_reminders() {
        List<Integer> milestoneReminders = new ArrayList<Integer>();
        milestoneReminders.add(1);
        programRequestDTO.setMilestoneReminders(milestoneReminders);
        when(milestoneReminderService.getMilestoneReminders(programRequestDTO.getProgramId())).thenReturn(milestoneReminders);
        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);
        verify(milestoneReminderService, times(1)).insertUpdateMilestoneReminders(programRequestDTO.getProgramId(), programRequestDTO.getMilestoneReminders());
        assertThat(programFullDTO.getMilestoneReminders(), is(milestoneReminders));
    }

    @Test
    public void test_insert_update_milestones() {
        List<MilestoneDTO> milestones = new ArrayList<MilestoneDTO>();
        milestones.add(new MilestoneDTO());
        programRequestDTO.setMilestones(milestones);
        when(milestoneService.getMilestones(programRequestDTO.getProgramId())).thenReturn(milestones);
        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);
        verify(milestoneService, times(1)).insertUpdateMilestones(programRequestDTO);
        assertThat(programFullDTO.getMilestones(), is(milestones));
    }

    private void addAwardCodeDefaultDetailsObject(ProgramRequestDTO programRequestDTO) {

        List<Certificates> certificatesList = new ArrayList<>();
        Certificates certificate_1 = new Certificates();
        certificate_1.setId(TestConstants.ID_1);
        certificate_1.setFileName(TEST_FILE_NAME_1);
        certificatesList.add(certificate_1);

        Certificates certificate_2 = new Certificates();
        certificate_2.setId(TestConstants.ID_2);
        certificate_2.setFileName(TEST_FILE_NAME_2);
        certificatesList.add(certificate_2);

        Certificates certificate_3 = new Certificates();
        certificate_3.setId(3L);
        certificate_3.setFileName(TEST_FILE_NAME_3);
        certificatesList.add(certificate_3);

        AwardCodeProgramDetailsDTO awardCodeProgram = new AwardCodeProgramDetailsDTO();
        awardCodeProgram.setClaimingInstructions(TEST_CLAIMING_INSTRUCTIONS_MISC_DATA);
        awardCodeProgram.setCertificates(certificatesList);
        awardCodeProgram.setIconId(null);
        awardCodeProgram.setDownloadable(Boolean.TRUE);
        awardCodeProgram.setPrintable(Boolean.TRUE);

        programRequestDTO.setNotification(Arrays.asList(
                NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setProgramType(ProgramTypeEnum.AWARD_CODE.name());
        programRequestDTO.setAwardCode(awardCodeProgram);
        programRequestDTO.setAllowTemplateUpload(null);
    }

    @Test
    public void test_save_pinnacle_program() {
        String questionText1 = "Here is your first question.";
        String questionText2 = "And this other one?";
        QuestionDTO question1 = new QuestionDTO();
        QuestionDTO question2 = new QuestionDTO();
        question1.setText(questionText1);
        question2.setText(questionText2);
        List<QuestionDTO> questions = new ArrayList<>();
        questions.add(question1);
        questions.add(question2);

        AwardsDTO awards = new AwardsDTO();
        awards.setIndividualAmount(100D);

        ApprovalDTO approver = new ApprovalDTO();
        EmployeeDTO pax = new EmployeeDTO();
        pax.setPaxId(2L);
        approver.setLevel(1L);
        approver.setPax(pax);
        approver.setType("PERSON");
        List<ApprovalDTO> approvers = new ArrayList<>();
        approvers.add(approver);

        List<String> notifications = new ArrayList<>();
        notifications.add(NotificationConstants.PINNACLE_NOTIFICATION_RECIPIENT_MGR);
        notifications.add(NotificationConstants.PINNACLE_NOTIFICATION_RECIPIENT_2ND_MGR);
        notifications.add(NotificationConstants.PINNACLE_NOTIFICATION_RECIPIENT);

        NominationPeriodDTO nominationPeriod1 = new NominationPeriodDTO();
        nominationPeriod1.setFromDate(LocalDate.now().toString());
        nominationPeriod1.setThruDate(LocalDate.now().plusDays(30).toString());
        NominationPeriodDTO nominationPeriod2 = new NominationPeriodDTO();
        nominationPeriod2.setFromDate(LocalDate.now().plusDays(31).toString());
        nominationPeriod2.setThruDate(LocalDate.now().plusDays(61).toString());
        List<NominationPeriodDTO> nominationPeriods = new ArrayList<>();
        nominationPeriods.add(nominationPeriod1);
        nominationPeriods.add(nominationPeriod2);


        PinnacleDTO pinnacle = new PinnacleDTO();
        pinnacle.setAudience(PinnacleConstants.INDIVIDUAL_AUDIENCE);
        pinnacle.setNominationInstructions("Answer these questions.");
        pinnacle.setQuestions(questions);
        pinnacle.setAwards(awards);
        pinnacle.setApprovals(approvers);
        pinnacle.setNotifications(notifications);
        pinnacle.setNewsfeedVisibilityNominated("PUBLIC");
        pinnacle.setNewsfeedVisibilityApproved("PUBLIC");
        pinnacle.setNominationPeriods(nominationPeriods);

        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setProgramType(ProgramTypeEnum.PINNACLE.name());
        programRequestDTO.setPinnacle(pinnacle);
        programRequestDTO.setAwardTiers(null);

        ProgramFullDTO programFullDTO = programService.insertProgram(programRequestDTO);
        verify(pinnacleService, times(1)).insertUpdatePinnacle(programRequestDTO.getProgramId(), programRequestDTO, "POST");
    }
}
