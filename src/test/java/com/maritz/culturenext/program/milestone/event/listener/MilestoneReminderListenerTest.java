package com.maritz.culturenext.program.milestone.event.listener;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.enums.EventType;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.test.AbstractMockTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.MonthDay;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class MilestoneReminderListenerTest extends AbstractMockTest {

    @Mock
    private AlertService alertService;
    @Mock
    private BatchService batchService;

    @InjectMocks
    MilestoneReminderListener milestoneReminderListener;

    private ArgumentCaptor<BatchEventDTO> batchEventDTOCaptor;

    private Long paxId;
    private Long paxIdWithoutProgramData;
    private Year hireYear;
    private Long programId;
    private Long directManagerId;
    private Double awardAmount;
    private String programName;
    private String payoutType;
    private Long yearsOfService;
    private List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList;
    private Batch batch;
    private MonthDay monthDay;

    @Before
    public void setup() {
        paxId = 754L;
        paxMilestoneReminderDTOList = new ArrayList<>();
        paxIdWithoutProgramData = 222L;
        hireYear = Year.of(2015);
        programId = 20L;
        directManagerId = 45L;
        awardAmount = 200.00;
        programName = "NatalieProgram";
        payoutType = "NataliePayoutType";
        yearsOfService = 3L;
        monthDay = MonthDay.now();

        batchEventDTOCaptor = ArgumentCaptor.forClass(BatchEventDTO.class);

        batch = new Batch();
        batch.setBatchId(123L);
        when(batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_REMINDER_ALERT.name())).thenReturn(batch);
        when(batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_REMINDER_ALERT.name())).thenReturn(batch);
    }

    @Test
    public void test_handle_birthday_milestone_reminders() throws Exception {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO =  new PaxMilestoneReminderDTO(EventType.BIRTHDAY_REMINDER_TYPE.name(), paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);

        MilestoneReminderEvent birthdayMilestoneReminder = new MilestoneReminderEvent(paxMilestoneReminderDTOList, EventType.BIRTHDAY_REMINDER_TYPE.name());

        milestoneReminderListener.handleMilestoneReminders(birthdayMilestoneReminder);

        verify(alertService, times(1)).addMilestoneReminderAlert(paxMilestoneReminderDTO);
    }

   @Test
    public void test_handle_service_anniversary_milestone_reminders() throws Exception {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name(), paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);

        MilestoneReminderEvent serviceAnniversaryMilestoneReminder = new MilestoneReminderEvent(paxMilestoneReminderDTOList, EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name());

        milestoneReminderListener.handleMilestoneReminders(serviceAnniversaryMilestoneReminder);

        verify(alertService, times(1)).addMilestoneReminderAlert(paxMilestoneReminderDTO);
    }

    @Test
    public void test_writes_batch_summary_data() {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name(), paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);
        PaxMilestoneReminderDTO paxMilestoneReminderDTObadData = new PaxMilestoneReminderDTO(EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name(), paxIdWithoutProgramData, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTO);
        paxMilestoneReminderDTOList.add(paxMilestoneReminderDTObadData);

        MilestoneReminderEvent milestoneReminder = new MilestoneReminderEvent(paxMilestoneReminderDTOList, EventType.SERVICE_ANNIVERSARY_REMINDER_TYPE.name());

        doThrow(Exception.class)
                .when(alertService)
                .addMilestoneReminderAlert(paxMilestoneReminderDTObadData);

        milestoneReminderListener.handleMilestoneReminders(milestoneReminder);

        verify(batchService, times(1)).endBatch(batch);
        verify(batchService, times(3)).createBatchEvent(Mockito.eq(batch), batchEventDTOCaptor.capture());

        List<BatchEventDTO> batchEvents = batchEventDTOCaptor.getAllValues();
        BatchEventDTO batchEvent = batchEvents.get(0);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERR.name(), "PAX", paxIdWithoutProgramData, null);

        batchEvent = batchEvents.get(1);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.RECS.name(), null, null, "2");

        batchEvent = batchEvents.get(2);
        assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERRS.name(), null, null, "1");
    }

    private void assertBatchEvent(BatchEventDTO batchEvent, Long batchId, String batchType, String targetTable, Long targetId, String message) {
        assertThat(batchEvent.getBatchId(), is(batchId));
        assertThat(batchEvent.getType(), is(batchType));
        assertThat(batchEvent.getTarget().getTable(), is(targetTable));
        assertThat(batchEvent.getTarget().getId(), is(targetId));
        assertThat(batchEvent.getMessage(), is(message));
    }
}