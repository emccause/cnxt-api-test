package com.maritz.culturenext.program.milestone.event.listener;

import com.maritz.culturenext.email.event.RecognitionEvent;
import com.maritz.culturenext.jpa.repository.CnxtProgramAwardTierRepository;
import com.maritz.culturenext.program.milestone.service.impl.ProgramReminderServiceImpl;
import com.maritz.test.AbstractMockTest;
import org.junit.Test;
import org.mockito.*;
import org.springframework.context.ApplicationEventPublisher;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class ProgramReminderListenerTest extends AbstractMockTest {
    @Mock private ApplicationEventPublisher applicationEventPublisher;
    @Mock CnxtProgramAwardTierRepository cnxtProgramAwardTierReminderRepository;
    @InjectMocks private ProgramReminderServiceImpl programReminderService;

    @Test
    public void test_flag_turned_off() {
        programReminderService.sendPendingApprovalRecognitionEmail(); // reminderDaysEnabled=false should avoid to send an email.
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(RecognitionEvent.class));
    }

    @Test
    public void test_flag_turned_on_reminder_day_does_not_match() {
        List<Map<String, Object>> nominationReminderEntity = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("REMINDER_DAYS", 2);
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1); //for a nomination which its program has been configured to remind managers every 2 days, it should avoid to send an email.
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        map.put("CREATE_DATE", yesterday.format(formatter));
        map.put("ID", 5);
        nominationReminderEntity.add(map);
        Mockito.when(cnxtProgramAwardTierReminderRepository.getPendingApprovalProgramReminderDays()).thenReturn(nominationReminderEntity);

        programReminderService.sendPendingApprovalRecognitionEmail();
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(RecognitionEvent.class));
    }

    @Test
    public void test_flag_turned_on_reminder_day_equals_zero() {
        List<Map<String, Object>> nominationReminderEntity = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("REMINDER_DAYS", 0);  // 0 days should avoid to send an email
        LocalDate today = LocalDate.now();
        LocalDate yesterday = today.minusDays(1);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        map.put("CREATE_DATE", yesterday.format(formatter));
        map.put("ID", 5);
        nominationReminderEntity.add(map);
        Mockito.when(cnxtProgramAwardTierReminderRepository.getPendingApprovalProgramReminderDays()).thenReturn(nominationReminderEntity);

        programReminderService.sendPendingApprovalRecognitionEmail();
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(RecognitionEvent.class));
    }

    @Test
    public void test_flag_turned_on_reminder_day_does_match() {
        List<Map<String, Object>> nominationReminderEntity = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("REMINDER_DAYS", 10);
        LocalDate today = LocalDate.now();
        LocalDate tenDaysAgo = today.minusDays(10); //10 days from its creation and reminderDays=10, it should send an email.
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        map.put("CREATE_DATE", tenDaysAgo.format(formatter));
        map.put("ID", 5);
        nominationReminderEntity.add(map);

        Mockito.when(cnxtProgramAwardTierReminderRepository.getPendingApprovalProgramReminderDays()).thenReturn(nominationReminderEntity);

        programReminderService.sendPendingApprovalRecognitionEmail();
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(Matchers.any(RecognitionEvent.class));
    }

}
