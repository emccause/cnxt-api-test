package com.maritz.culturenext.program.milestone.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jpa.repository.ProgramActivityMiscRepository;
import com.maritz.core.jpa.repository.ProgramActivityRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.ProgramActivityTypeCode;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.test.AbstractDatabaseTest;

public class MilestoneServiceDataTest extends AbstractDatabaseTest {

    @Inject private MilestoneService milestoneService; // Way easier to get all the data for each milestone this way
    @Inject private ProgramRepository programRepository;
    @Inject private ProgramActivityRepository programActivityRepository;
    @Inject private ProgramActivityMiscRepository programActivityMiscRepository;
    private static final String MILESTONE_TEST_PROGRAM_NAME_SA = "MILESTONE_TEST_PROGRAM";
    private static final String MILESTONE_TEST_PROGRAM_NAME_BIRTHDAY = "MILESTONE_TEST_PROGRAM_BIRTHDAY";

    private Long TEST_PROGRAM_ID_SA = null;
    private Long TEST_PROGRAM_ID_BIRTHDAY = null;
    private MilestoneDTO birthdayMilestone = null;
    private MilestoneDTO serviceAnniversaryRepeatingMilestone = null;
    private MilestoneDTO serviceAnniversaryTwoYearMilestone = null;
    private MilestoneDTO serviceAnniversaryFiveYearMilestone = null;

    @Before
    public void setup() {
        TEST_PROGRAM_ID_SA = programRepository.findBy()
            .where(ProjectConstants.PROGRAM_NAME).eq(MILESTONE_TEST_PROGRAM_NAME_SA)
            .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        Assert.assertNotNull("test data wasn't created!", TEST_PROGRAM_ID_SA);

        TEST_PROGRAM_ID_BIRTHDAY = programRepository.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq(MILESTONE_TEST_PROGRAM_NAME_BIRTHDAY)
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
        Assert.assertNotNull("test data wasn't created!", TEST_PROGRAM_ID_BIRTHDAY);

        List<MilestoneDTO> serviceAnniversaryMilestones = milestoneService.getMilestones(TEST_PROGRAM_ID_SA);
        Assert.assertNotNull("test data wasn't created!", serviceAnniversaryMilestones);

        serviceAnniversaryRepeatingMilestone = serviceAnniversaryMilestones.stream()
                .filter(milestone -> ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name().equalsIgnoreCase(milestone.getType()))
                .filter(milestone -> Boolean.TRUE.equals(milestone.isRepeat()))
                .findAny()
                .map(v -> v)
                .orElse(null);
        Assert.assertNotNull("test data wasn't created!", serviceAnniversaryRepeatingMilestone);

        serviceAnniversaryTwoYearMilestone = serviceAnniversaryMilestones.stream()
                .filter(milestone -> ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name().equalsIgnoreCase(milestone.getType()))
                .filter(milestone -> Boolean.FALSE.equals(milestone.isRepeat()))
                .filter(milestone -> 2 == milestone.getYearsOfService())
                .findAny()
                .map(v -> v)
                .orElse(null);
        Assert.assertNotNull("test data wasn't created!", serviceAnniversaryTwoYearMilestone);

        serviceAnniversaryFiveYearMilestone = serviceAnniversaryMilestones.stream()
                .filter(milestone -> ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name().equalsIgnoreCase(milestone.getType()))
                .filter(milestone -> Boolean.FALSE.equals(milestone.isRepeat()))
                .filter(milestone -> 5 == milestone.getYearsOfService())
                .findAny()
                .map(v -> v)
                .orElse(null);
        Assert.assertNotNull("test data wasn't created!", serviceAnniversaryFiveYearMilestone);

        List<MilestoneDTO> birthdayMilestones = milestoneService.getMilestones(TEST_PROGRAM_ID_BIRTHDAY);
        Assert.assertNotNull("test data wasn't created!", birthdayMilestones);

        birthdayMilestone = birthdayMilestones.stream()
                .filter(milestone -> ProgramActivityTypeCode.BIRTHDAY.name().equalsIgnoreCase(milestone.getType()))
                .findAny()
                .map(v -> v)
                .orElse(null);
        Assert.assertNotNull("test data wasn't created!", birthdayMilestone);
    }

    @Test
    public void testDeleteBirthdayMilestone() {
        assertThat(programActivityMiscRepository.findAll(birthdayMilestone.getId()).size(), greaterThan(0));
        assertThat(programActivityRepository.findOne(birthdayMilestone.getId()), is(notNullValue()));

        milestoneService.deleteMilestoneById(TEST_PROGRAM_ID_BIRTHDAY, birthdayMilestone.getId());

        assertThat(programActivityMiscRepository.findAll(birthdayMilestone.getId()).size(), is(0));
        assertThat(programActivityRepository.findOne(birthdayMilestone.getId()), is(nullValue()));
    }

    @Test
    public void testDeleteServiceAnniversaryMilestone() {
        assertThat(programActivityMiscRepository.findAll(serviceAnniversaryTwoYearMilestone.getId()).size(),
                greaterThan(0));
        assertThat(programActivityRepository.findOne(serviceAnniversaryTwoYearMilestone.getId()), is(notNullValue()));

        milestoneService.deleteMilestoneById(TEST_PROGRAM_ID_SA, serviceAnniversaryTwoYearMilestone.getId());

        assertThat(programActivityMiscRepository.findAll(serviceAnniversaryTwoYearMilestone.getId()).size(), is(0));
        assertThat(programActivityRepository.findOne(serviceAnniversaryTwoYearMilestone.getId()), is(nullValue()));
    }
}
