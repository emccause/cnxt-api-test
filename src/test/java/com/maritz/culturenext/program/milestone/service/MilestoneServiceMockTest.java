package com.maritz.culturenext.program.milestone.service;

import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityMisc;
import com.maritz.core.jpa.repository.ProgramActivityMiscRepository;
import com.maritz.core.jpa.repository.ProgramActivityRepository;
import com.maritz.core.jpa.support.findby.FindBy;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.NotFoundException;
import com.maritz.culturenext.enums.ProgramActivityTypeCode;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityMiscRepository;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.program.dao.MilestoneDao;
import com.maritz.culturenext.program.dto.MilestoneDTO;
import com.maritz.culturenext.program.dto.ProgramRequestDTO;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneEvent;
import com.maritz.culturenext.program.milestone.event.MilestoneReminderEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneDTO;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.program.milestone.service.impl.MilestoneServiceImpl;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.springframework.context.ApplicationEventPublisher;
import org.thymeleaf.util.ListUtils;

import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class MilestoneServiceMockTest extends AbstractMockTest {

    private MilestoneServiceImpl milestoneService;

    @Mock private ApplicationEventPublisher applicationEventPublisher;
    @Mock private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    @Mock private CnxtProgramActivityMiscRepository cnxtProgramActivityMiscRepository;
    @Mock private MilestoneDao milestoneDao;
    @Mock private ProgramActivityRepository programActivityRepository;
    @Mock private ProgramActivityMiscRepository programActivityMiscRepository;
    @Mock private FindBy<ProgramActivity> programActivityFindBy;
    @Mock private FindBy<ProgramActivityMisc> programActivityMiscFindBy;

    @Captor
    private ArgumentCaptor<MilestoneEvent> milestoneEventArgumentCaptor;

    @Captor
    private ArgumentCaptor<MilestoneReminderEvent> milestoneReminderEventArgumentCaptor;

    @Captor
    private ArgumentCaptor<ArrayList<Long>> milestoneIdListCaptor;

    private Long paxId;
    private Long yearsAfterHireDateOrBirthday;

    @Before
    public void setup() {
        paxId = 15L;
        milestoneService = new MilestoneServiceImpl()
            .setCnxtProgramActivityRepository(cnxtProgramActivityRepository)
            .setCnxtProgramActivityMiscRepository(cnxtProgramActivityMiscRepository)
            .setProgramActivityRepository(programActivityRepository)
            .setProgramActivityMiscRepository(programActivityMiscRepository)
            .setMilestoneDao(milestoneDao)
            .setApplicationEventPublisher(applicationEventPublisher);

        Mockito.when(programActivityRepository.findBy()).thenReturn(programActivityFindBy);
        Mockito.when(programActivityFindBy.where(Matchers.anyString())).thenReturn(programActivityFindBy);
        Mockito.when(programActivityFindBy.eq(Matchers.anyLong())).thenReturn(programActivityFindBy);
        Mockito.when(programActivityFindBy.and(Matchers.anyString())).thenReturn(programActivityFindBy);
        Mockito.when(programActivityFindBy.in(Matchers.anyListOf(String.class))).thenReturn(programActivityFindBy);
        List<ProgramActivity> programActivityList = Arrays.asList(
                    new ProgramActivity()
                        .setProgramActivityId(1L)
                        .setProgramActivityName(ProgramActivityTypeCode.BIRTHDAY.name())
                        .setProgramActivityTypeCode(MilestoneConstants.MILESTONE),
                    new ProgramActivity()
                        .setProgramActivityId(2L)
                        .setProgramActivityName(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                        .setProgramActivityTypeCode(MilestoneConstants.MILESTONE),
                    new ProgramActivity()
                        .setProgramActivityId(3L)
                        .setProgramActivityName(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                        .setProgramActivityTypeCode(MilestoneConstants.MILESTONE)
                );

        Mockito.when(programActivityFindBy.findAll()).thenReturn(programActivityList);
        Mockito.when(programActivityFindBy.findAll(Matchers.anyString(), Matchers.<Class<Long>>any())).thenReturn(
                    programActivityList.stream() // Parse programActivityList to collect ID as a list
                    .map(pa -> {return pa.getProgramActivityId();})
                    .collect(Collectors.toList())
                );

        Mockito.when(programActivityMiscRepository.findBy()).thenReturn(programActivityMiscFindBy);
        Mockito.when(programActivityMiscFindBy.where(Matchers.anyString())).thenReturn(programActivityMiscFindBy);
        Mockito.when(programActivityMiscFindBy.in(Matchers.anyListOf(Long.class))).thenReturn(programActivityMiscFindBy);
        List<ProgramActivityMisc> programActivityMiscList = Arrays.asList(
                new ProgramActivityMisc()
                    .setProgramActivityId(1L)
                    .setVfName(VfName.REPEATING.name())
                    .setMiscData(Boolean.TRUE.toString()),
                new ProgramActivityMisc()
                    .setProgramActivityId(1L)
                    .setVfName(VfName.YEARS_OF_SERVICE.name())
                    .setMiscData("1"),
                new ProgramActivityMisc()
                    .setProgramActivityId(1L)
                    .setVfName(VfName.PUBLIC_HEADLINE.name())
                    .setMiscData("Happy Birthday!"),
                new ProgramActivityMisc()
                    .setProgramActivityId(1L)
                    .setVfName(VfName.PRIVATE_MESSAGE.name())
                    .setMiscData("Private Message 1"),
                new ProgramActivityMisc()
                    .setProgramActivityId(2L)
                    .setVfName(VfName.REPEATING.name())
                    .setMiscData(Boolean.TRUE.toString()),
                new ProgramActivityMisc()
                    .setProgramActivityId(2L)
                    .setVfName(VfName.YEARS_OF_SERVICE.name())
                    .setMiscData("1"),
                new ProgramActivityMisc()
                    .setProgramActivityId(2L)
                    .setVfName(VfName.PUBLIC_HEADLINE.name())
                    .setMiscData("Happy Service Anniversary!"),
                new ProgramActivityMisc()
                    .setProgramActivityId(2L)
                    .setVfName(VfName.PRIVATE_MESSAGE.name())
                    .setMiscData("Private Message 2"),
                new ProgramActivityMisc()
                    .setProgramActivityId(3L)
                    .setVfName(VfName.REPEATING.name())
                    .setMiscData(Boolean.FALSE.toString()),
                new ProgramActivityMisc()
                    .setProgramActivityId(3L)
                    .setVfName(VfName.YEARS_OF_SERVICE.name())
                    .setMiscData("1"),
                new ProgramActivityMisc()
                    .setProgramActivityId(3L)
                    .setVfName(VfName.PUBLIC_HEADLINE.name())
                    .setMiscData("Happy Service Anniversary!"),
                new ProgramActivityMisc()
                    .setProgramActivityId(3L)
                    .setVfName(VfName.AWARD_AMOUNT.name())
                    .setMiscData(new Double(1D).toString()),
                new ProgramActivityMisc()
                    .setProgramActivityId(3L)
                    .setVfName(VfName.ECARD_ID.name())
                    .setMiscData(new Long(1L).toString())
            );

        Mockito.when(programActivityMiscFindBy.findAll()).thenReturn(programActivityMiscList);

        Mockito.when(
                cnxtProgramActivityMiscRepository.findByProgramIdAndVfName(Matchers.anyLong(), Matchers.anyString())
            ).thenReturn(new ArrayList<>());

        Date date = DateUtil.convertFromUTCStringDate("2018-04-07");
        List<Map<String, Object>> upcomingBirthdayMilestones = Arrays.asList(createUpcomingMilestone(date), createUpcomingMilestone(date));

        Mockito.when(
                milestoneDao.findUpcomingMilestoneDates(Matchers.anyString())
            ).thenReturn(upcomingBirthdayMilestones);

        List<Map<String,Object>> paxBirthdays = Arrays.asList(createPaxMilestone(10L, "09-22"), createPaxMilestone(10L, "03-29"));

        MonthDay monthDay = MonthDay.of(4, 7);
        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, monthDay)
            ).thenReturn(paxBirthdays);

        monthDay = MonthDay.now();
        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, monthDay)
            ).thenReturn(paxBirthdays);

        List<Map<String, Object>> paxServiceAnniversaries = Arrays.asList(createPaxMilestone(10L, "2015-09-22"), createPaxMilestone(11L, "2017-03-29"));

        monthDay = MonthDay.of(4, 7);
        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, monthDay)
            ).thenReturn(paxServiceAnniversaries);

        monthDay = MonthDay.now();
        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, monthDay)
            ).thenReturn(paxServiceAnniversaries);

        yearsAfterHireDateOrBirthday = 2L;

        List<Map<String, Object>> enrolledPrograms = new ArrayList<>();
        enrolledPrograms.add(createEnrolledProgram(65417L, 215L, 86L, yearsAfterHireDateOrBirthday, true, 1));
        enrolledPrograms.add(createEnrolledProgram(65417L, 274L, 40L, 10L, true, 1));
        Mockito.when(
                milestoneDao.getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any())
            ).thenReturn(enrolledPrograms);
    }

    @Test
    public void test_validation_happy_path() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage(null)
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(2)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertTrue("Should not have errors.", errorMessageList.isEmpty());
    }

    @Test
    public void test_validation_invalid_type() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType("INVALID")
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Should be correct error.", MilestoneConstants.INVALID_MILESTONE_TYPE_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_duplicate_year_repeating() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.MULTIPLE_REPEATING_NOT_SUPPORTED_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_duplicate_year_non_repeating() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.DUPLICATE_MILESTONE_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_repeat_missing() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(null)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.REPEATING_MISSING_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_years_of_service_missing() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(null)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.YEARS_OF_SERVICE_MISSING_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_years_of_service_negative() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(-1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.YEARS_OF_SERVICE_NEGATIVE_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_public_headline_missing() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline(null)
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Shoult be correct error.", MilestoneConstants.PUBLIC_HEADLINE_MISSING_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_public_headline_too_long() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("This is a really long string. I mean, absurdly long.  Who would even want to receive this recognition? Happy birthday, I guess, if you're still reading this.")
                    .setPrivateMessage("private message")
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Should be correct error.", MilestoneConstants.PUBLIC_HEADLINE_TOO_LONG_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_private_message_too_long() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        String privateMessage = "private message with more than 2000 characters";
        for (int i = 0; i < 50; i++) {
            privateMessage += "..................................................";
        }

        program.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("public headline")
                    .setPrivateMessage(privateMessage)
                    .setEcardId(1L)
                    .setAwardAmount(1D)
            ));

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertFalse("Should have errors.", errorMessageList.isEmpty());
        Assert.assertEquals("Should be one error.", 1, errorMessageList.size());
        Assert.assertEquals("Should be correct error.", MilestoneConstants.PRIVATE_MESSAGE_TOO_LONG_CODE, errorMessageList.get(0).getCode());
    }

    @Test
    public void test_validation_empty() {
        ProgramRequestDTO program = new ProgramRequestDTO();
        program.setMilestones(new ArrayList<>());

        List<ErrorMessage> errorMessageList = milestoneService.validateMilestones(program);
        Assert.assertTrue("Should have no errors.", errorMessageList.isEmpty());
    }

    @Test
    public void test_get_milestones() {
        List<MilestoneDTO> milestoneList = milestoneService.getMilestones(1L);
        Assert.assertFalse("Should have returned data!", ListUtils.isEmpty(milestoneList));
        Assert.assertEquals("should have three results", 3, milestoneList.size());

        milestoneList.forEach(milestone -> {
            Assert.assertNotNull("Id should be populated!", milestone.getId());

            if (milestone.getId().equals(1L)) {
                Assert.assertEquals("Should be birthday type", ProgramActivityTypeCode.BIRTHDAY.name(), milestone.getType());
                Assert.assertEquals("Should be repeating", Boolean.TRUE, milestone.isRepeat());
                Assert.assertEquals("Should be one year of service", new Integer(1), milestone.getYearsOfService());
                Assert.assertEquals("public headline should be correct", "Happy Birthday!", milestone.getPublicHeadline());
                Assert.assertEquals("private message should be correct", "Private Message 1", milestone.getPrivateMessage());
                Assert.assertNull("Should have no award amount", milestone.getAwardAmount());
                Assert.assertNull("Should have no ecard id", milestone.getEcardId());
            }
            else if (milestone.getId().equals(2L)) {
                Assert.assertEquals("Should be Service anniversary type", ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name(), milestone.getType());
                Assert.assertEquals("Should be repeating", Boolean.TRUE, milestone.isRepeat());
                Assert.assertEquals("Should be one year of service", new Integer(1), milestone.getYearsOfService());
                Assert.assertEquals("public headline should be happy service anniversary", "Happy Service Anniversary!", milestone.getPublicHeadline());
                Assert.assertEquals("private message should be correct", "Private Message 2", milestone.getPrivateMessage());
                Assert.assertNull("Should have no award amount", milestone.getAwardAmount());
                Assert.assertNull("Should have no ecard id", milestone.getEcardId());
            }
            else if (milestone.getId().equals(3L)) {
                Assert.assertEquals("Should be Service anniversary type", ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name(), milestone.getType());
                Assert.assertEquals("Should not be repeating", Boolean.FALSE, milestone.isRepeat());
                Assert.assertEquals("Should be one year of service", new Integer(1), milestone.getYearsOfService());
                Assert.assertEquals("public headline should be happy service anniversary", "Happy Service Anniversary!", milestone.getPublicHeadline());
                Assert.assertNull("private message should be null", milestone.getPrivateMessage());
                Assert.assertEquals("Should have award amount 1", new Double(1D), milestone.getAwardAmount());
                Assert.assertEquals("Should have ecard id 1", new Long(1L), milestone.getEcardId());
            }
        });
    }

    @Test
    public void test_get_milestones_none() {
        Mockito.when(programActivityFindBy.findAll()).thenReturn(new ArrayList<>());
        List<MilestoneDTO> milestoneList = milestoneService.getMilestones(1L);
        Assert.assertTrue("Should have no results!", ListUtils.isEmpty(milestoneList));
    }

    @Test
    public void test_get_milestones_code_coverage() {
        // wanted coverage on that last case where VF_NAME wasn't ECARD_ID...
        Mockito.when(programActivityFindBy.findAll()).thenReturn(Arrays.asList(
                new ProgramActivity()
                    .setProgramActivityId(1L)
                    .setProgramActivityName(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setProgramActivityTypeCode(MilestoneConstants.MILESTONE)
            ));
        Mockito.when(programActivityFindBy.findAll(Matchers.anyString(), Matchers.<Class<Long>>any())).thenReturn(Arrays.asList(1L));
        Mockito.when(programActivityMiscFindBy.findAll()).thenReturn(Arrays.asList(
                        new ProgramActivityMisc()
                            .setProgramActivityId(1L)
                            .setVfName(VfName.REPEATING.name())
                            .setMiscData(Boolean.TRUE.toString()),
                        new ProgramActivityMisc()
                            .setProgramActivityId(1L)
                            .setVfName(VfName.YEARS_OF_SERVICE.name())
                            .setMiscData("1"),
                        new ProgramActivityMisc()
                            .setProgramActivityId(1L)
                            .setVfName(VfName.PUBLIC_HEADLINE.name())
                            .setMiscData("Happy Birthday!"),
                        new ProgramActivityMisc()
                            .setProgramActivityId(1L)
                            .setVfName(VfName.PRIVATE_MESSAGE.name())
                            .setMiscData("Private Message 1"),
                        new ProgramActivityMisc()
                            .setProgramActivityId(1L)
                            .setVfName("Something that will be ignored")
                            .setMiscData("You didn't see anything...")
                    ));

        List<MilestoneDTO> milestoneList = milestoneService.getMilestones(1L);
        Assert.assertFalse("Should have returned data!", ListUtils.isEmpty(milestoneList));
        Assert.assertEquals("should have one result", 1, milestoneList.size());

        MilestoneDTO milestone = milestoneList.get(0);
        Assert.assertEquals("Should be birthday type", ProgramActivityTypeCode.BIRTHDAY.name(), milestone.getType());
        Assert.assertEquals("Should be repeating", Boolean.TRUE, milestone.isRepeat());
        Assert.assertEquals("Should be one year of service", new Integer(1), milestone.getYearsOfService());
        Assert.assertEquals("public headline should be correct", "Happy Birthday!", milestone.getPublicHeadline());
        Assert.assertEquals("private message should be correct", "Private Message 1", milestone.getPrivateMessage());
        Assert.assertNull("Should have no award amount", milestone.getAwardAmount());
        Assert.assertNull("Should have no ecard id", milestone.getEcardId());
    }

    @Test
    public void test_insert_update_milestones() {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        programRequestDTO.setProgramId(1L);
        programRequestDTO.setFromDate(DateUtil.getCurrentTimeInUTCDateTime());
        programRequestDTO.getProgramType();
        programRequestDTO.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("Public Headline"),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("Public Headline")
                    .setPrivateMessage("Private Message")
                    .setEcardId(1L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(1)
                    .setPublicHeadline("Public Headline")
                    .setPrivateMessage("Private Message")
                    .setEcardId(2L)
                    .setAwardAmount(1D),
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.SERVICE_ANNIVERSARY.name())
                    .setRepeat(Boolean.FALSE)
                    .setYearsOfService(5)
                    .setPublicHeadline("Public Headline")
                    .setEcardId(2L)
            ));

        // make sure each tier has a different id
        Mockito.when(programActivityRepository.save(Matchers.any(ProgramActivity.class))).thenAnswer(invocation -> {
            return invocation.getArgumentAt(0, ProgramActivity.class).setProgramActivityId(1L);
        }).thenAnswer(invocation -> {
            return invocation.getArgumentAt(0, ProgramActivity.class).setProgramActivityId(2L);
        }).thenAnswer(invocation -> {
            return invocation.getArgumentAt(0, ProgramActivity.class).setProgramActivityId(3L);
        }).thenAnswer(invocation -> {
            return invocation.getArgumentAt(0, ProgramActivity.class).setProgramActivityId(4L);
        });

        Mockito.when(programActivityMiscRepository.save(Matchers.anyListOf(ProgramActivityMisc.class))).thenAnswer(invocation -> {
            @SuppressWarnings("unchecked")
            List<ProgramActivityMisc> miscList = (ArrayList<ProgramActivityMisc>) invocation.getArgumentAt(0, ArrayList.class);

            Map<Long, List<ProgramActivityMisc>> programActivityMiscMap = miscList.stream()
                    .collect(Collectors.groupingBy(pam -> pam.getProgramActivityId(), Collectors.toList()));
            Assert.assertEquals("Should have 4 milestones", 4, programActivityMiscMap.keySet().size());

            // first milestone validation
            List<ProgramActivityMisc> milestoneOneMiscList = programActivityMiscMap.get(1L);
            Assert.assertEquals("Should have repeat, years of service, and public headline miscs",
                    Arrays.asList(
                            VfName.REPEATING.name(), VfName.YEARS_OF_SERVICE.name(), VfName.PUBLIC_HEADLINE.name()
                        ),
                    milestoneOneMiscList.stream()
                        .map(pam -> pam.getVfName())
                        .collect(Collectors.toList())
                    );

            milestoneOneMiscList.forEach(pam -> {
                if (VfName.REPEATING.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be repeating", Boolean.TRUE.toString(), pam.getMiscData());
                }
                else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be one year of service", new Integer(1).toString(), pam.getMiscData());
                }
                else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("public headline should be 'Public Headline'", "Public Headline", pam.getMiscData());
                }
                else if (VfName.ECARD_ID.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have an ecard");
                }
                else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have an award amount");
                }
            });

            // second milestone validation
            List<ProgramActivityMisc> milestoneTwoMiscList = programActivityMiscMap.get(2L);
            Assert.assertEquals("Should have all miscs",
                    Arrays.asList(
                            VfName.REPEATING.name(), VfName.YEARS_OF_SERVICE.name(), VfName.PUBLIC_HEADLINE.name(),
                            VfName.AWARD_AMOUNT.name(), VfName.ECARD_ID.name(), VfName.PRIVATE_MESSAGE.name()
                        ),
                    milestoneTwoMiscList.stream()
                        .map(pam -> pam.getVfName())
                        .collect(Collectors.toList())
                );

            milestoneTwoMiscList.forEach(pam -> {
                if (VfName.REPEATING.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be repeating", Boolean.TRUE.toString(), pam.getMiscData());
                }
                else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be one year of service", Integer.toString(1), pam.getMiscData());
                }
                else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("public headline should be 'Public Headline'", "Public Headline", pam.getMiscData());
                }
                else if (VfName.ECARD_ID.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("ecard id should be 1", Long.toString(1L), pam.getMiscData());
                }
                else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("award amount should be one point", Double.toString(1D), pam.getMiscData());
                }
                else if (VfName.PRIVATE_MESSAGE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("private message should be 'Private Message'", "Private Message", pam.getMiscData());
                }
            });

            // third milestone validation
            List<ProgramActivityMisc> milestoneThreeMiscList = programActivityMiscMap.get(3L);
            Assert.assertEquals("Should have all miscs",
                    Arrays.asList(
                            VfName.REPEATING.name(), VfName.YEARS_OF_SERVICE.name(), VfName.PUBLIC_HEADLINE.name(),
                            VfName.AWARD_AMOUNT.name(), VfName.ECARD_ID.name(), VfName.PRIVATE_MESSAGE.name()
                        ),
                    milestoneThreeMiscList.stream()
                        .map(pam -> pam.getVfName())
                        .collect(Collectors.toList())
                );

            milestoneThreeMiscList.forEach(pam -> {
                if (VfName.REPEATING.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should not be repeating", Boolean.FALSE.toString(), pam.getMiscData());
                }
                else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be one year of service", Integer.toString(1), pam.getMiscData());
                }
                else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("public headline should be 'Public Headline'", "Public Headline", pam.getMiscData());
                }
                else if (VfName.ECARD_ID.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("ecard id should be 2", Long.toString(2L), pam.getMiscData());
                }
                else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("award amount should be one point", Double.toString(1D), pam.getMiscData());
                }
                else if (VfName.PRIVATE_MESSAGE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("private message should be 'Private Message'", "Private Message", pam.getMiscData());
                }
            });

            // fourth milestone validation
            List<ProgramActivityMisc> milestoneFourMiscList = programActivityMiscMap.get(4L);
            Assert.assertEquals("Should have repeating, years of service, public headline, and award amount miscs",
                    Arrays.asList(
                            VfName.REPEATING.name(), VfName.YEARS_OF_SERVICE.name(), VfName.PUBLIC_HEADLINE.name(),
                            VfName.ECARD_ID.name()
                        ),
                    milestoneFourMiscList.stream()
                        .map(pam -> pam.getVfName())
                        .collect(Collectors.toList())
                );

            milestoneFourMiscList.forEach(pam -> {
                if (VfName.REPEATING.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should not be repeating", Boolean.FALSE.toString(), pam.getMiscData());
                }
                else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be five years of service", Integer.toString(5), pam.getMiscData());
                }
                else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("public headline should be 'Public Headline'", "Public Headline", pam.getMiscData());
                }
                else if (VfName.ECARD_ID.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("ecard id should be 2", Long.toString(2L), pam.getMiscData());
                }
                else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have an award amount");
                }
            });

            return miscList;
        });

        milestoneService.insertUpdateMilestones(programRequestDTO);
    }

    @Test
    public void test_insert_update_milestones_no_ecards() {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        programRequestDTO.setProgramId(1L);
        programRequestDTO.setFromDate(DateUtil.getCurrentTimeInUTCDateTime());
        programRequestDTO.setMilestones(Arrays.asList(
                new MilestoneDTO()
                    .setType(ProgramActivityTypeCode.BIRTHDAY.name())
                    .setRepeat(Boolean.TRUE)
                    .setYearsOfService(1)
                    .setPublicHeadline("Public Headline")
            ));

        Mockito.when(programActivityRepository.save(Matchers.any(ProgramActivity.class))).thenAnswer(invocation -> {
            return invocation.getArgumentAt(0, ProgramActivity.class).setProgramActivityId(1L);
        });

        Mockito.when(programActivityMiscRepository.save(Matchers.anyListOf(ProgramActivityMisc.class))).thenAnswer(invocation -> {

            @SuppressWarnings("unchecked")
            List<ProgramActivityMisc> miscList = (ArrayList<ProgramActivityMisc>) invocation.getArgumentAt(0, ArrayList.class);

            Map<Long, List<ProgramActivityMisc>> programActivityMiscMap = miscList.stream()
                    .collect(Collectors.groupingBy(pam -> pam.getProgramActivityId(), Collectors.toList()));
            Assert.assertEquals("Should have 1 milestone", 1, programActivityMiscMap.keySet().size());

            List<ProgramActivityMisc> milestoneMiscList = programActivityMiscMap.get(1L);
            Assert.assertEquals("Should have repeat, years of service, and public headline miscs",
                    Arrays.asList(
                            VfName.REPEATING.name(), VfName.YEARS_OF_SERVICE.name(), VfName.PUBLIC_HEADLINE.name()
                        ),
                    milestoneMiscList.stream()
                        .map(pam -> pam.getVfName())
                        .collect(Collectors.toList())
                );

            milestoneMiscList.forEach(pam -> {
                if (VfName.REPEATING.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be repeating", Boolean.TRUE.toString(), pam.getMiscData());
                }
                else if (VfName.YEARS_OF_SERVICE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("Should be one year of service", new Integer(1).toString(), pam.getMiscData());
                }
                else if (VfName.PUBLIC_HEADLINE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.assertEquals("public headline should be 'Public Headline'", "Public Headline", pam.getMiscData());
                }
                else if (VfName.ECARD_ID.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have an ecard");
                }
                else if (VfName.AWARD_AMOUNT.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have an award amount");
                }
                else if (VfName.PRIVATE_MESSAGE.name().equalsIgnoreCase(pam.getVfName())) {
                    Assert.fail("should not have a private message");
                }
            });

            return miscList;
        });

        milestoneService.insertUpdateMilestones(programRequestDTO);
    }

    @Test
    public void test_insert_update_milestones_empty() {
        ProgramRequestDTO programRequestDTO = new ProgramRequestDTO();
        programRequestDTO.setProgramId(1L);
        programRequestDTO.setFromDate(DateUtil.getCurrentTimeInUTCDateTime());
        programRequestDTO.setMilestones(new ArrayList<>());

        try {
            milestoneService.insertUpdateMilestones(programRequestDTO);
        }
        catch (Exception e) {
            Assert.fail("Should not have thrown errors.");
        }
    }

    @Test
    public void test_delete_milestone_by_id() {
        Long programId = 100L;
        Long milestoneId = 200L;
        ProgramActivity programActivity = new ProgramActivity();
        programActivity.setProgramId(programId);
        when(programActivityRepository.findOne(milestoneId)).thenReturn(programActivity);

        milestoneService.deleteMilestoneById(programId, milestoneId);

        verify(cnxtProgramActivityMiscRepository, times(1))
                .deleteByProgramActivityIdIn(milestoneIdListCaptor.capture());
        List<Long> milestoneIdList = milestoneIdListCaptor.getValue();
        assertThat(milestoneIdList.size(), is(1));
        assertThat(milestoneIdList.get(0), is(milestoneId));

        verify(cnxtProgramActivityRepository, times(1)).deleteByProgramActivityIdIn(milestoneIdListCaptor.capture());
        milestoneIdList = milestoneIdListCaptor.getValue();
        assertThat(milestoneIdList.size(), is(1));
        assertThat(milestoneIdList.get(0), is(milestoneId));
    }

    @Test
    public void test_delete_milestone_by_id_milestone_not_found() {
        Long programId = 100L;
        Long milestoneId = 200L;
        ProgramActivity programActivity = new ProgramActivity();
        programActivity.setProgramId(300L);
        when(programActivityRepository.findOne(milestoneId)).thenReturn(programActivity);

        try {
            milestoneService.deleteMilestoneById(programId, milestoneId);
            fail("Should have thrown NotFoundException");
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is("field=milestoneId code=NOT_FOUND message=milestoneId not found"));
        }
    }

    @Test
    public void test_delete_milestone_by_id_milestone_not_tied_to_program() {
        Long programId = 100L;
        Long milestoneId = 200L;
        when(programActivityRepository.findOne(milestoneId)).thenReturn(null);

        try {
            milestoneService.deleteMilestoneById(programId, milestoneId);
            fail("Should have thrown NotFoundException");
        } catch (NotFoundException e) {
            assertThat(e.getMessage(), is("field=milestoneId code=NOT_FOUND message=milestoneId not found"));
        }
    }

    @Test(expected = NotFoundException.class)
    public void test_delete_milestones_by_id_null_program_activity() throws Exception {
        Mockito.when(programActivityRepository.findOne(Matchers.anyLong())).thenReturn(null);

        milestoneService.deleteMilestoneById(1L, 2L);
    }

    @Test(expected = NotFoundException.class)
    public void test_delete_milestones_by_id_milestone_not_tied_to_program() {
        ProgramActivity programActivity = new ProgramActivity();
        programActivity.setProgramId(5L);
        Mockito.when(programActivityRepository.findOne(Matchers.anyLong())).thenReturn(programActivity);

        milestoneService.deleteMilestoneById(1L, 2L);
    }

    @Test
    public void test_find_upcoming_birthday_reminders() {
        List<Map<String, Object>> upcomingBirthdayDates = createMultipleUpcomingMilestoneDates();

        Mockito.when(
                milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.BIRTHDAY_TYPE)
        ).thenReturn(upcomingBirthdayDates);

        List<Map<String, Object>> paxBirthdays = Arrays.asList(createPaxMilestone(paxId, "2018-05-17"));

        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, MonthDay.now())
        ).thenReturn(paxBirthdays);

        Map<String, Object> programData = createProgramData(10L, "Natalie!", 20.0,"DPP", null, 215L);

        Mockito.when(
                milestoneDao.getMilestoneProgramData(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong())
        ).thenReturn(programData);

        milestoneService.findUpcomingBirthdayMilestoneReminders();

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent((milestoneReminderEventArgumentCaptor.capture()));
        MilestoneReminderEvent milestoneReminderEvent = milestoneReminderEventArgumentCaptor.getValue();

        PaxMilestoneReminderDTO paxMilestoneReminderDTO = milestoneReminderEvent.getPaxMilestoneReminderList().get(0);
        assertThat(milestoneReminderEvent.getPaxMilestoneReminderList().size(), is(upcomingBirthdayDates.size()));
        assertThat(milestoneReminderEvent.getEventType(), is(MilestoneConstants.BIRTHDAY_REMINDER_TYPE));
        assertThat(paxMilestoneReminderDTO.getPaxId(), is(paxId));
        assertThat(paxMilestoneReminderDTO.getProgramName(), is("Natalie!"));
        assertThat(paxMilestoneReminderDTO.getAwardAmount(), is(20.0));
        assertThat(paxMilestoneReminderDTO.getPayoutType(), is ("DPP"));
        assertThat(paxMilestoneReminderDTO.getManagerPaxId(), is (10L));
        assertThat(paxMilestoneReminderDTO.getMilestoneDate(), is ("2018-05-17"));
    }

    @Test
    public void test_find_upcoming_birthday_reminders_null_values() {
        List<Map<String, Object>> upcomingBirthdays = new ArrayList<>();
        Map<String, Object> upcomingBirthday = new HashMap<>();
        upcomingBirthday.put("upcomingMilestone", new Date());
        upcomingBirthdays.add(upcomingBirthday);

        Mockito.when(
                milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.BIRTHDAY_TYPE)
        ).thenReturn(upcomingBirthdays);

        List<Map<String, Object>> paxServiceAnniversaries = Arrays.asList(createPaxMilestone(paxId, "2018-05-17"));

        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, MonthDay.now())
        ).thenReturn(paxServiceAnniversaries);

        Map<String, Object> programData = createProgramData(null, "Natalie!", null,"DPP", null, 215L);

        Mockito.when(
                milestoneDao.getMilestoneProgramData(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong())
        ).thenReturn(programData);

        milestoneService.findUpcomingBirthdayMilestoneReminders();

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent((milestoneReminderEventArgumentCaptor.capture()));
        MilestoneReminderEvent milestoneReminderEvent = milestoneReminderEventArgumentCaptor.getValue();

        PaxMilestoneReminderDTO paxMilestoneReminderDTO = milestoneReminderEvent.getPaxMilestoneReminderList().get(0);
        assertThat(milestoneReminderEvent.getEventType(), is(MilestoneConstants.BIRTHDAY_REMINDER_TYPE));
        assertThat(paxMilestoneReminderDTO.getPaxId(), is(paxId));
        assertThat(paxMilestoneReminderDTO.getProgramName(), is("Natalie!"));
        assertThat(paxMilestoneReminderDTO.getAwardAmount(), is(nullValue()));
        assertThat(paxMilestoneReminderDTO.getPayoutType(), is ("DPP"));
        assertThat(paxMilestoneReminderDTO.getManagerPaxId(), is (nullValue()));
        assertThat(paxMilestoneReminderDTO.getMilestoneDate(), is ("2018-05-17"));
    }

    @Test
    public void test_find_upcoming_birthday_reminders_no_upcoming_birthdays() {
        Mockito.when(
            milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.BIRTHDAY_TYPE)
        ).thenReturn(null);
        milestoneService.findUpcomingBirthdayMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(0)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_upcoming_birthday_reminders_no_participants() {
        Mockito.when(
            milestoneDao.findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class))
        ).thenReturn(null);
        milestoneService.findUpcomingBirthdayMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(2)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_upcoming_birthday_reminders_no_enrolled_programs() {
        Mockito.when(
            milestoneDao.getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any())
        ).thenReturn(null);
        milestoneService.findUpcomingBirthdayMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(2)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(4)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_upcoming_service_anniversary_reminders() {
        List<Map<String, Object>> upcomingServiceAnniversaryDates = createMultipleUpcomingMilestoneDates();

        Mockito.when(
                milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE)
        ).thenReturn(upcomingServiceAnniversaryDates);

        List<Map<String, Object>> paxServiceAnniversaries = Arrays.asList(createPaxMilestone(paxId, "2018-05-17"));

        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, MonthDay.now())
        ).thenReturn(paxServiceAnniversaries);

        Map<String, Object> programData = createProgramData(10L, "Natalie!", 20.0,"DPP", 2L, 215L);

        Mockito.when(
                milestoneDao.getMilestoneProgramData(Matchers.anyLong(), Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong())
        ).thenReturn(programData);

        milestoneService.findUpcomingServiceAnniversaryMilestoneReminders();

        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent((milestoneReminderEventArgumentCaptor.capture()));
        MilestoneReminderEvent milestoneReminderEvent = milestoneReminderEventArgumentCaptor.getValue();

        PaxMilestoneReminderDTO paxMilestoneReminderDTO = milestoneReminderEvent.getPaxMilestoneReminderList().get(0);
        assertThat(milestoneReminderEvent.getPaxMilestoneReminderList().size(), is(upcomingServiceAnniversaryDates.size()));
        assertThat(milestoneReminderEvent.getEventType(), is(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE));
        assertThat(paxMilestoneReminderDTO.getPaxId(), is(paxId));
        assertThat(paxMilestoneReminderDTO.getYearsOfService(), is(2L));
        assertThat(paxMilestoneReminderDTO.getProgramName(), is("Natalie!"));
        assertThat(paxMilestoneReminderDTO.getManagerPaxId(), is (10L));
        assertThat(paxMilestoneReminderDTO.getMilestoneDate(), is ("2018-05-17"));
    }

    @Test
    public void test_find_upcoming_service_anniversary_reminders_no_service_anniversaries() {
        Mockito.when(
            milestoneDao.findUpcomingMilestoneDates(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE)
        ).thenReturn(null);
        milestoneService.findUpcomingServiceAnniversaryMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(0)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_upcoming_service_anniversary_reminders_no_participants() {
        Mockito.when(
            milestoneDao.findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class))
        ).thenReturn(null);
        milestoneService.findUpcomingServiceAnniversaryMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(2)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_upcoming_service_anniversary_reminders_no_enrolled_programs() {
        Mockito.when(
            milestoneDao.getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any())
        ).thenReturn(null);
        milestoneService.findUpcomingServiceAnniversaryMilestoneReminders();
        Mockito.verify(milestoneDao, Mockito.times(1)).findUpcomingMilestoneDates(Matchers.anyString());
        Mockito.verify(milestoneDao, Mockito.times(2)).findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class));
        Mockito.verify(milestoneDao, Mockito.times(4)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneReminderEvent.class));
    }

    @Test
    public void test_find_birthday_milestones() {
        List<Map<String, Object>> paxBirthdayMilestones = Arrays.asList(createPaxMilestone(paxId, calculateDateRelativeToNow(2)));

        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, MonthDay.now())
        ).thenReturn(paxBirthdayMilestones);

        milestoneService.findBirthdayMilestones();
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent(milestoneEventArgumentCaptor.capture());

        MilestoneEvent milestoneEvent = milestoneEventArgumentCaptor.getValue();

        PaxMilestoneDTO paxMilestoneDTO = milestoneEvent.getPaxMilestoneList().get(0);
        assertThat(milestoneEvent.getMilestoneType(), is(MilestoneConstants.BIRTHDAY_TYPE));
        assertThat(paxMilestoneDTO.getPaxId(), is(paxId));
    }

    @Test
    public void test_find_birthday_milestones_no_participants() {
        Mockito.when(
            milestoneDao.findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class))
        ).thenReturn(null);
        milestoneService.findBirthdayMilestones();
        Mockito.verify(milestoneDao, Mockito.times(1)).findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, MonthDay.now());
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneEvent.class));
    }

    @Test
    public void test_find_birthday_milestones_no_enrolled_programs() {
        Mockito.when(
            milestoneDao.getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any())
        ).thenReturn(null);
        milestoneService.findBirthdayMilestones();
        Mockito.verify(milestoneDao, Mockito.times(1)).findParticipantsWithMilestones(MilestoneConstants.BIRTHDAY_TYPE, MonthDay.now());
        Mockito.verify(milestoneDao, Mockito.times(2)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneEvent.class));
    }


    @Test
    public void test_find_service_anniversary_milestones() {
        List<Map<String, Object>> paxServiceAnniversaries = Arrays.asList(createPaxMilestone(paxId, calculateDateRelativeToNow(yearsAfterHireDateOrBirthday.intValue())));
        Mockito.when(
                milestoneDao.findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, MonthDay.now())
        ).thenReturn(paxServiceAnniversaries);

        milestoneService.findServiceAnniversaryMilestones();
        Mockito.verify(applicationEventPublisher, Mockito.times(1)).publishEvent((milestoneEventArgumentCaptor.capture()));
        MilestoneEvent milestoneEvent = milestoneEventArgumentCaptor.getValue();

        PaxMilestoneDTO paxMilestoneDTO = milestoneEvent.getPaxMilestoneList().get(0);
        assertThat(milestoneEvent.getMilestoneType(), is(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE));
        assertThat(paxMilestoneDTO.getPaxId(), is(paxId));
        assertThat(paxMilestoneDTO.getYearsOfService(), is (yearsAfterHireDateOrBirthday));
    }

    @Test
    public void test_find_service_anniversary_milestones_no_participants() {
        Mockito.when(
            milestoneDao.findParticipantsWithMilestones(Matchers.anyString(), Matchers.any(MonthDay.class))
        ).thenReturn(null);
        milestoneService.findServiceAnniversaryMilestones();
        Mockito.verify(milestoneDao, Mockito.times(1)).findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, MonthDay.now());
        Mockito.verify(milestoneDao, Mockito.times(0)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneEvent.class));
    }

    @Test
    public void test_find_service_anniversary_milestones_no_enrolled_programs() {
        Mockito.when(
            milestoneDao.getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any())
        ).thenReturn(null);
        milestoneService.findServiceAnniversaryMilestones();
        Mockito.verify(milestoneDao, Mockito.times(1)).findParticipantsWithMilestones(MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, MonthDay.now());
        Mockito.verify(milestoneDao, Mockito.times(2)).getEnrolledProgramsForPax(Matchers.anyLong(), Matchers.anyString(), Matchers.anyLong(), Matchers.any());
        Mockito.verify(applicationEventPublisher, Mockito.times(0)).publishEvent(Matchers.any(MilestoneEvent.class));
    }

    private Map<String, Object> createEnrolledProgram(Long subjectId, Long programId, Long programActivityId, Long yearsOfService, Boolean repeating, Integer programRank) {
        Map<String, Object> enrolledProgram = new HashMap<>();
        enrolledProgram.put("subjectId", subjectId);
        enrolledProgram.put("programId", programId);
        enrolledProgram.put("programActivityId", programActivityId);
        enrolledProgram.put("yearsOfService", yearsOfService);
        enrolledProgram.put("repeating", repeating);
        enrolledProgram.put("programRank", programRank);
        return enrolledProgram;
    }

    private Map<String, Object> createPaxMilestone(Long paxId, String milestoneDate) {
        Map<String,Object> paxMilestone = new HashMap<>();
        paxMilestone.put("paxId", paxId);
        paxMilestone.put("milestoneDate", milestoneDate);
        return paxMilestone;
    }

    private Map<String, Object> createUpcomingMilestone(Date date) {
        Map<String,Object> upcomingMilestone = new HashMap<>();
        upcomingMilestone.put("upcomingMilestone", date);
        return upcomingMilestone;
    }

    private String calculateDateRelativeToNow(Integer years) {
        LocalDateTime todayDate = LocalDateTime.now();
        LocalDateTime relativeDate = todayDate.minusYears(years);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return relativeDate.format(formatter);
    }

    private Map<String, Object> createProgramData (Long directManagerId, String programName, Double awardAmount, String payoutType, Long yearsOfService, Long programId) {
        Map<String, Object> programData = new HashMap<String, Object>();
        programData.put("directManager", directManagerId);
        programData.put("programName", programName);
        programData.put("awardAmount", awardAmount);
        programData.put("payoutType", payoutType);
        programData.put("yearsOfService", yearsOfService);
        programData.put("programId", programId);
        return programData;
    }


    private List<Map<String, Object>> createMultipleUpcomingMilestoneDates() {
        Date dateTomorrow = calculateDateTomorrow();

        List<Map<String, Object>> upcomingMilestones = new ArrayList<>();

        Map<String, Object> upcomingMilestoneToday = createUpcomingMilestone(new Date());
        Map<String, Object> upcomingMilestoneTomorrow = createUpcomingMilestone(dateTomorrow);

        upcomingMilestones.add(upcomingMilestoneToday);
        upcomingMilestones.add(upcomingMilestoneTomorrow);

        return upcomingMilestones;
    }

    private Date calculateDateTomorrow() {
        Date dateToday = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateToday);
        calendar.add(Calendar.DATE, 1);
        Date relativeDate = calendar.getTime();
        return relativeDate;
    }
}
