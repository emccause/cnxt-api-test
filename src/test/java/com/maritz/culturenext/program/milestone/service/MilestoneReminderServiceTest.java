package com.maritz.culturenext.program.milestone.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.repository.ProgramMiscRepository;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.program.milestone.service.impl.MilestoneReminderServiceImpl;
import com.maritz.test.AbstractMockTest;

public class MilestoneReminderServiceTest extends AbstractMockTest {
    
    private MilestoneReminderService milestoneReminderService;
    
    @Mock private ProgramMiscRepository programMiscRepository;
    
    @Captor private ArgumentCaptor<List<ProgramMisc>> programMiscListCaptor;
    
    private List<String> vfNames;

    @Before
    public void setUp() throws Exception {
        milestoneReminderService = new MilestoneReminderServiceImpl()
                .setProgramMiscRepository(programMiscRepository);        

        vfNames = new ArrayList<String>();
        vfNames.add(VfName.MILESTONE_REMINDER.toString());
    }

    @Test
    public void test_get_no_existing_values() {
        when(programMiscRepository.findAll(1L, vfNames)).thenReturn(null);

        List<Integer> result = milestoneReminderService.getMilestoneReminders(1L);
        
        assertThat(result.size(), is(0));
    }
    
    @Test
    public void test_get_one_existing_value() {
        List<ProgramMisc> programMiscs = new ArrayList<ProgramMisc>();
        programMiscs.add(createMilestoneReminder("1"));
        when(programMiscRepository.findAll(1L, vfNames)).thenReturn(programMiscs);
        
        List<Integer> result = milestoneReminderService.getMilestoneReminders(1L);
        
        assertThat(result.size(), is(1));
        assertThat(result.get(0), is(1));
    }

    @Test
    public void test_get_values_from_more_than_one_existing_record() {
        List<ProgramMisc> programMiscs = new ArrayList<ProgramMisc>();
        programMiscs.add(createMilestoneReminder("1"));
        programMiscs.add(createMilestoneReminder("5"));
        programMiscs.add(createMilestoneReminder("10"));
        when(programMiscRepository.findAll(1L, vfNames)).thenReturn(programMiscs);
        
        List<Integer> result = milestoneReminderService.getMilestoneReminders(1L);
        
        assertThat(result.size(), is(3));
        assertThat(result.get(0), is(1));
        assertThat(result.get(1), is(5));
        assertThat(result.get(2), is(10));
    }
    
    @Test
    public void test_insert_one_new_milestone_reminder() {
        Long programId = 1L;
        Integer milestoneReminderValue = 1;
        List<Integer> milestoneReminderList = new ArrayList<Integer>();
        milestoneReminderList.add(milestoneReminderValue);
        
        milestoneReminderService.insertUpdateMilestoneReminders(programId, milestoneReminderList);
        
        ArgumentCaptor<ProgramMisc> programMiscCaptor = ArgumentCaptor.forClass(ProgramMisc.class);
        verify(programMiscRepository, times(1)).save(programMiscCaptor.capture());
        ProgramMisc programMisc = programMiscCaptor.getValue();
        assertThat(programMisc.getProgramId(), is(programId));
        assertThat(programMisc.getVfName(), is(VfName.MILESTONE_REMINDER.toString()));
        assertThat(programMisc.getMiscData(), is(milestoneReminderValue.toString()));
    }
    
    @Test
    public void test_delete_existing_milestone_reminders_before_inserting() {
        Long programId = 1L;
        List<ProgramMisc> programMiscList = new ArrayList<ProgramMisc>();
        when(programMiscRepository.findAll(programId, vfNames)).thenReturn(programMiscList);
        
        milestoneReminderService.insertUpdateMilestoneReminders(programId, new ArrayList<Integer>());

        verify(programMiscRepository, times(1)).delete(programMiscListCaptor.capture());
        assertThat(programMiscListCaptor.getValue(), is(programMiscList));
    }

    @Test
    public void test_insert_more_than_one_new_milestone_reminder() {
        Long programId = 1L;
        List<Integer> milestoneReminderList = new ArrayList<Integer>();
        milestoneReminderList.add(1);
        milestoneReminderList.add(5);
        milestoneReminderList.add(10);
        
        milestoneReminderService.insertUpdateMilestoneReminders(programId, milestoneReminderList);
        
        ArgumentCaptor<ProgramMisc> programMiscCaptor = ArgumentCaptor.forClass(ProgramMisc.class);
        verify(programMiscRepository, times(milestoneReminderList.size())).save(programMiscCaptor.capture());
        List<ProgramMisc> programMiscList = programMiscCaptor.getAllValues();
        for (int i=0; i<programMiscList.size(); i++) {
            assertThat(programMiscList.get(i).getProgramId(), is(programId));
            assertThat(programMiscList.get(i).getVfName(), is(VfName.MILESTONE_REMINDER.toString()));
            assertThat(programMiscList.get(i).getMiscData(), is(milestoneReminderList.get(i).toString()));
        }
    }

    private ProgramMisc createMilestoneReminder(String miscData) {
        ProgramMisc programMisc = new ProgramMisc();
        programMisc.setProgramId(1L);
        programMisc.setVfName(VfName.MILESTONE_REMINDER.toString());
        programMisc.setMiscData(miscData);
        return programMisc;
    }
}
