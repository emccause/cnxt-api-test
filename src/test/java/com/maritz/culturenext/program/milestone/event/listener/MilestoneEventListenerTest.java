package com.maritz.culturenext.program.milestone.event.listener;

import com.maritz.core.dto.BatchEventDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.BatchEventTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.services.BatchService;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dao.MilestoneDao;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.MilestoneEvent;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.services.NominationService;
import com.maritz.test.AbstractMockTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class MilestoneEventListenerTest extends AbstractMockTest {
  @Mock private MilestoneDao milestoneDao;
  @Mock private NominationService nominationService;
  @Mock private SysUserRepository sysUserRepository;
  @Mock private SysUser sysUser;
  @Mock private BatchService batchService;
  @Mock private BatchUtil batchUtil;

  @InjectMocks MilestoneEventListener milestoneEventListener;

  private ArgumentCaptor<NominationRequestDTO> nominationRequestDTOArgumentCaptor;
  private ArgumentCaptor<BatchEventDTO> batchEventDTOCaptor;

  private Long paxId;
  private Long sysUserId;
  private String sysUserName;
  private Long directManagerPaxId;
  private Long programId;
  private Long programActivityId;
  private Long budgetId;
  private String awardAmount;
  private String programName;
  private String payoutType;
  private Long batchId;
  private String publicHeadline;
  private Boolean giveAnonymous;
  private Map<String, Object> programData;
  private Long yearsOfService;
  private Batch batch;
  private List<PaxMilestoneDTO> paxMilestoneDTOList;

  private Long otherPaxId;
  private String otherAwardAmount;
  private Map<String, Object> otherProgramData;

  @Before
  public void setup() {
    paxId = 1L;
    sysUserId = 2L;
    directManagerPaxId = 3L;
    programId = 4L;
    programActivityId = 5L;
    budgetId = 6L;
    sysUserName = "admin";
    awardAmount = "100";
    programName = "Libby's awesome program";
    payoutType = "DPP";
    publicHeadline = "HEADLINE!";
    giveAnonymous = true;
    sysUser.setPaxId(sysUserId);
    yearsOfService = 1L;
    batchId = 123L;

    otherPaxId = 20L;
    otherAwardAmount = "25";

    programData = new HashMap<String, Object>();
    programData.put("directManager", directManagerPaxId);
    programData.put("programName", programName);
    programData.put("awardAmount", awardAmount);
    programData.put("payoutType", payoutType);
    programData.put("publicHeadline", publicHeadline);
    programData.put("giveAnonymous", giveAnonymous);
    programData.put("budgetId", budgetId);
    programData.put("programId", programId);

    otherProgramData = new HashMap<String,Object>();
    otherProgramData.put("directManager", directManagerPaxId);
    otherProgramData.put("programName", programName);
    otherProgramData.put("awardAmount", otherAwardAmount);
    otherProgramData.put("payoutType", payoutType);
    otherProgramData.put("publicHeadline", publicHeadline);
    otherProgramData.put("giveAnonymous", giveAnonymous);
    otherProgramData.put("budgetId", budgetId);
    otherProgramData.put("programId", programId);

    paxMilestoneDTOList = new ArrayList<PaxMilestoneDTO>();

    nominationRequestDTOArgumentCaptor = ArgumentCaptor.forClass(NominationRequestDTO.class);
    batchEventDTOCaptor = ArgumentCaptor.forClass(BatchEventDTO.class);

    when(sysUser.getSysUserId()).thenReturn(sysUserId);
    when(sysUserRepository.findBySysUserName(sysUserName)).thenReturn(sysUser);
    
    when(milestoneDao.getMilestoneProgramData(paxId, programId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService)).thenReturn(programData);
    when(milestoneDao.getMilestoneProgramData(paxId, programId, MilestoneConstants.BIRTHDAY_TYPE, null)).thenReturn(programData);
    when(milestoneDao.getMilestoneProgramData(otherPaxId, programId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService)).thenReturn(otherProgramData);
    
    batch = new Batch();
    batch.setBatchId(batchId);
    when(batchService.startBatch(BatchType.MILESTONE_SERVICE_ANNIVERSARY_EVENT.name())).thenReturn(batch);
    when(batchService.startBatch(BatchType.MILESTONE_BIRTHDAY_EVENT.name())).thenReturn(batch);
  }

  @Test
  public void test_handle_service_anniversary_milestones() {
    MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

    milestoneEventListener.handleMilestones(milestoneEvent);
    
    verify(nominationService, times(1)).createStandardNomination(Matchers.eq(sysUserId), nominationRequestDTOArgumentCaptor.capture(), Matchers.eq(true));
    assertNominationRequest(nominationRequestDTOArgumentCaptor.getValue(), publicHeadline, paxId, budgetId, Integer.parseInt(awardAmount), payoutType, giveAnonymous, batchId);
  }

  @Test
  public void test_handle_service_anniversary_milestones_handles_missing_program_data() {
    MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);
    when(milestoneDao.getMilestoneProgramData(paxId, programId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService)).thenReturn(null);

    milestoneEventListener.handleMilestones(milestoneEvent);

    assertErrorLogged("field=programId code=NOT_FOUND message=programId not found");
  }
  
  @Test
  public void test_handle_birthday_milestones() {
    MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

    milestoneEventListener.handleMilestones(milestoneEvent);

    verify(nominationService, times(1)).createStandardNomination(Matchers.eq(sysUserId), nominationRequestDTOArgumentCaptor.capture(), Matchers.eq(true));
    assertNominationRequest(nominationRequestDTOArgumentCaptor.getValue(), publicHeadline, paxId, budgetId, Integer.parseInt(awardAmount), payoutType, giveAnonymous, batchId);
  }

  @Test
  public void test_handle_birthday_milestones_handles_missing_program_data() {
    MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);
    when(milestoneDao.getMilestoneProgramData(paxId, programId, MilestoneConstants.BIRTHDAY_TYPE, null)).thenReturn(null);

    milestoneEventListener.handleMilestones(milestoneEvent);

    assertErrorLogged("field=programId code=NOT_FOUND message=programId not found");
  }

  @Test
  public void test_handle_service_anniversaries_missing_public_headline() {
    programData.put("publicHeadline", null);
    MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

    milestoneEventListener.handleMilestones(milestoneEvent);
      
    assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }

  @Test
  public void test_handle_service_anniversaries_empty_public_headline() {
    programData.put("publicHeadline", "");
    MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

    milestoneEventListener.handleMilestones(milestoneEvent);
    
    assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }

  @Test
  public void test_handle_service_anniversaries_missing_budget_id() {
    programData.put("budgetId", null);
    MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

    milestoneEventListener.handleMilestones(milestoneEvent);
        
    assertErrorLogged(getErrorMessageAsString(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_service_anniversaries_missing_payout_type() {
      programData.put("payoutType", null);
      MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_service_anniversaries_missing_headline_and_budget_id() {
      programData.put("publicHeadline", null);
      programData.put("budgetId", null);
      MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE) + "; " + getErrorMessageAsString(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_service_anniversaries_missing_headline() {
      programData.put("publicHeadline", null);
      MilestoneEvent milestoneEvent = createSimpleServiceAnniversaryMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }
  
  @Test
  public void test_handle_birthdays_missing_public_headline() {
      programData.put("publicHeadline", null);
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }

  @Test
  public void test_handle_birthdays_empty_public_headline() {
      programData.put("publicHeadline", "");
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }

  @Test
  public void test_handle_birthdays_missing_budget_id() {
      programData.put("budgetId", null);
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_birthdays_missing_payout_type() {
      programData.put("payoutType", null);
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
      
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.ERROR_MISSING_PAYOUT_TYPE_WITH_BUDGET_ID_AND_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_birthdays_missing_headline_and_budget_id() {
      programData.put("publicHeadline", null);
      programData.put("budgetId", null);
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE) + "; " + getErrorMessageAsString(ProgramConstants.ERROR_MISSING_BUDGET_ID_WITH_AWARD_AMOUNT_MESSAGE));
  }

  @Test
  public void test_handle_birthdays_missing_headline() {
      programData.put("publicHeadline", null);
      programData.put("awardAmount", null);
      MilestoneEvent milestoneEvent = createSimpleBirthdayMilestoneEvent(paxId);

      milestoneEventListener.handleMilestones(milestoneEvent);
          
      assertErrorLogged(getErrorMessageAsString(ProgramConstants.HEADLINE_MISSING_MESSAGE));
  }

  @Test
  public void test_empty_pax_milestone_list () {
      List<PaxMilestoneDTO> paxMilestoneDTOList = new ArrayList<PaxMilestoneDTO>();
      MilestoneEvent milestoneEvent = new MilestoneEvent(paxMilestoneDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
      milestoneEventListener.handleMilestones(milestoneEvent);

      verify(batchService, times(1)).endBatch(batch);
      verify(batchService, times(3)).createBatchEvent(Mockito.eq(batch), batchEventDTOCaptor.capture());

      List<BatchEventDTO> batchEvents = batchEventDTOCaptor.getAllValues();
      BatchEventDTO batchEvent = batchEvents.get(0);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.RECS.name(), null, null, "0");

      batchEvent = batchEvents.get(1);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERRS.name(), null, null, "0");

      batchEvent = batchEvents.get(2);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.TOTAL_POINTS.name(), null, null, "0.0");
  }
  
  @Test
  public void test_writes_batch_summary_data() {
      List<PaxMilestoneDTO> paxMilestoneDTOList = new ArrayList<PaxMilestoneDTO>();
      addPaxMilestoneToList(paxMilestoneDTOList, paxId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService, programActivityId, programId);
      addPaxMilestoneToList(paxMilestoneDTOList, 222L, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService, programActivityId, programId);
      addPaxMilestoneToList(paxMilestoneDTOList, otherPaxId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService, programActivityId, programId);
      MilestoneEvent milestoneEvent = new MilestoneEvent(paxMilestoneDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);

      Map<String, Object> errorProgramData = new HashMap<String, Object>();
      when(milestoneDao.getMilestoneProgramData(222L, programId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE, yearsOfService)).thenReturn(errorProgramData);

      milestoneEventListener.handleMilestones(milestoneEvent);

      verify(batchService, times(1)).endBatch(batch);
      verify(batchService, times(4)).createBatchEvent(Mockito.eq(batch), batchEventDTOCaptor.capture());
      List<BatchEventDTO> batchEvents = batchEventDTOCaptor.getAllValues();
      
      BatchEventDTO batchEvent = batchEvents.get(0);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERR.name(), "PAX", 222L, "field=programId code=NOT_FOUND message=programId not found");

      batchEvent = batchEvents.get(1);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.RECS.name(), null, null, "3");

      batchEvent = batchEvents.get(2);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.ERRS.name(), null, null, "1");

      batchEvent = batchEvents.get(3);
      assertBatchEvent(batchEvent, batch.getBatchId(), BatchEventTypeCode.TOTAL_POINTS.name(), null, null, "125.0");
  }

  private PaxMilestoneDTO createSimpleMilestone(Long paxId, String milestoneType) {
      return new PaxMilestoneDTO(paxId, milestoneType, yearsOfService, programActivityId, programId);
  }

  private MilestoneEvent createSimpleServiceAnniversaryMilestoneEvent(Long paxId) {
      paxMilestoneDTOList.add(createSimpleMilestone(paxId, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE));
      return new MilestoneEvent(paxMilestoneDTOList, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
  }

  private MilestoneEvent createSimpleBirthdayMilestoneEvent(Long paxId) {
      paxMilestoneDTOList.add(createSimpleMilestone(paxId, MilestoneConstants.BIRTHDAY_TYPE));
      return new MilestoneEvent(paxMilestoneDTOList, MilestoneConstants.BIRTHDAY_TYPE);
  }

  private void addPaxMilestoneToList(List<PaxMilestoneDTO> list, Long paxId, String type, Long yearsOfService, Long programActivityId, Long programId) {
      PaxMilestoneDTO paxMilestoneDTO = new PaxMilestoneDTO(paxId, type, yearsOfService, programActivityId, programId);
      list.add(paxMilestoneDTO);
  }
  
  private void assertBatchEvent(BatchEventDTO batchEvent, Long batchId, String batchType, String targetTable, Long targetId, String message) {
      assertThat(batchEvent.getBatchId(), is(batchId));
      assertThat(batchEvent.getType(), is(batchType));
      assertThat(batchEvent.getTarget().getTable(), is(targetTable));
      assertThat(batchEvent.getTarget().getId(), is(targetId));
      assertThat(batchEvent.getMessage(), is(message));
  }
  
  private void assertNominationRequest(NominationRequestDTO nominationRequestDTO, String publicHeadline, Long paxId, Long budgetId, Integer awardAmount, String payoutType, Boolean giveAnonymous, Long batchId) {
      assertThat(nominationRequestDTO.getHeadline(), is(publicHeadline));
      assertThat(nominationRequestDTO.getReceivers().get(0).getPaxId(), is(paxId));
      assertThat(nominationRequestDTO.getBudgetId(), is(budgetId));
      assertThat(nominationRequestDTO.getReceivers().get(0).getAwardAmount(), is(awardAmount));
      assertThat(nominationRequestDTO.getReceivers().get(0).getOriginalAmount(), is(awardAmount));
      assertThat(nominationRequestDTO.getPayoutType(), is(payoutType));
      assertThat(nominationRequestDTO.getGiveAnonymous(), is(giveAnonymous));
      assertThat(nominationRequestDTO.getBatchId(), is (batchId));
  }

  private String getErrorMessageAsString(ErrorMessage errorMessage) {
      return "field=" + errorMessage.getField() + 
              " code=" + errorMessage.getCode() + 
              " message=" + errorMessage.getMessage();
  }

  private void assertErrorLogged(String errorMessage) {
      verify(nominationService, times(0)).createStandardNomination(Matchers.any(), Matchers.any(), Matchers.any());
      verify(batchService, times(4)).createBatchEvent(Mockito.eq(batch), batchEventDTOCaptor.capture());
      List<BatchEventDTO> batchEvents = batchEventDTOCaptor.getAllValues();
      assertBatchEvent(batchEvents.get(0), batch.getBatchId(), BatchEventTypeCode.ERR.name(), "PAX", paxId, errorMessage);
  }
  
}
