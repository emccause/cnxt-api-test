package com.maritz.culturenext.program.activity.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.ProgramActivityFiles;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.images.service.impl.FileImageServiceImpl;
import com.maritz.culturenext.jpa.repository.CnxtProgramActivityRepository;
import com.maritz.culturenext.program.activity.constants.ProgramActivityConstants;
import com.maritz.culturenext.program.activity.dao.ProgramActivityDao;
import com.maritz.culturenext.program.activity.dto.ProgramActivityDTO;
import com.maritz.culturenext.program.activity.service.impl.ProgramActivityServiceImpl;
import com.maritz.culturenext.program.activity.util.ProgramActivityTypes;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;

public class ProgramActivityServiceTest extends AbstractMockTest {

    private final String TEST_PROGRAM_ACTIVITY_DESCRIPTION = "Testing program activity endpoints";
    private final String TEST_PROGRAM_ACTIVITY_TYPE = ProgramActivityTypes.JUMBOTRON.name();
    private final String TEST_STATUS = StatusTypeCode.ACTIVE.name();
    private final Date TEST_FROM_DATE_NEW =  DateUtils.addDays(new Date(),20);
    private final Date TEST_END_DATE_NEW = DateUtils.addDays(new Date(),30);
    private final Date TEST_FROM_DATE_EXPIRED = DateUtils.addDays(new Date(), -30);
    private final Date TEST_END_DATE_EXPIRED = DateUtils.addDays(new Date(), -20);
    private final Date TEST_FROM_DATE_ACTIVE = DateUtils.addDays(new Date(), -5);
    private final Date TEST_END_DATE_ACTIVE = DateUtils.addDays(new Date(), 5);
    private final Date TEST_FROM_DATE_SCHEDULED = DateUtils.addDays(new Date(), 10);
    private final Date TEST_END_DATE_SCHEDULED = DateUtils.addDays(new Date(), 15);
    private final Date TEST_FROM_DATE_OVERLAP =  DateUtils.addDays(new Date(),2);
    private final Date TEST_END_DATE_OVERLAP = DateUtils.addDays(new Date(),12);
    private final Long TEST_IMAGE_ID = 1L;

    private final String TEST_DESCRIPTION_TOO_LONG = "qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiop"
            + "asdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyu"
            + "iopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzxcvbnmqwertyuiopasdfghjklzx1";

    private final String FAIL_METHOD_PROGRAM_ACTIVITY_MSG =
            "Program activity should have failed and thrown an exception";
    private final String FAIL_GET_METHOD_MSG = "GET:"+FAIL_METHOD_PROGRAM_ACTIVITY_MSG;
    private final String FAIL_CREATE_METHOD_MSG = "CREATE:"+FAIL_METHOD_PROGRAM_ACTIVITY_MSG;
    private final String FAIL_UPDATE_METHOD_MSG = "UPDATE:"+FAIL_METHOD_PROGRAM_ACTIVITY_MSG;
    private final String FAIL_DELETE_METHOD_MSG = ":"+FAIL_METHOD_PROGRAM_ACTIVITY_MSG;

    @InjectMocks private ProgramActivityServiceImpl programActivityService;
    @Mock private CnxtProgramActivityRepository cnxtProgramActivityRepository;
    @Mock private ConcentrixDao<ProgramActivity> programActivityDao;
    @Mock private ConcentrixDao<ProgramActivityFiles> programActivityFilesDao;
    @Mock private ConcentrixDao<Files> filesDao;
    @Mock private FileImageServiceImpl fileImageService;
    @Mock private ProgramActivityDao programActivityDaoImpl;
    @Mock private ProgramRepository programRepository;
    @Mock private ProgramEligibilityQueryDao programEligibilityQueryDao;
    @Mock private Security security;

    FindBy<ProgramActivity> programActivityFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ProgramActivityFiles> programActivityFilesFindBy = PowerMockito.mock(FindBy.class);

    @Before
    public void setup() {
        // Default mocks
        PowerMockito.when(programActivityDao.findById(anyLong())).thenReturn(programActivityTestObject()) ;
        PowerMockito.when(programActivityDao.findBy()).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.where(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.and()).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.or()).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.end()).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.in(anyListOf(String.class))).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.and(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.ge(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.le(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.gt(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.lt(anyString())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.eq(any())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.between(any(),any())).thenReturn(programActivityFindBy);
        PowerMockito.when(programActivityFindBy.find()).thenReturn(null);

        Program program = new Program();
        program.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        PowerMockito.when(programRepository.findOne(anyLong())).thenReturn(program);

        PowerMockito.when(programEligibilityQueryDao.getAllEligibleProgramsForPax(anyLong(), anyListOf(String.class),
                anyString())).thenReturn(eligiblePrograms());

        PowerMockito.when(programActivityFilesDao.findBy()).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.where(anyString())).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.eq(any())).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.findOne()).thenReturn(null);
        PowerMockito.when(programActivityFilesFindBy.find()).thenReturn(new ArrayList<ProgramActivityFiles>());

        PowerMockito.when(filesDao.findById(anyLong())).thenReturn(new Files());

        PowerMockito.when(cnxtProgramActivityRepository.isOverlappedTime(anyLong(), anyLong(),anyString(),
                anyString(),anyString())).thenReturn(null);
    }

    @Test
    public void test_create_program_activity_array_success(){

        List<ProgramActivityDTO> programActivityDtoList = programActivityService
                .createProgramActivityList(Arrays.asList(generateProgramActivityDto(), generateProgramActivityDto()));

        Mockito.verify(programActivityDao, times(2)).save(any(ProgramActivity.class));
        ProgramActivityDTO programActivityDto = programActivityDtoList.get(0);

        assertThat(programActivityDto.getType(), equalTo(TEST_PROGRAM_ACTIVITY_TYPE));
        assertThat(programActivityDto.getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(programActivityDto.getStatus(), equalTo(StatusTypeCode.SCHEDULED.name()));
        assertEquals(DateUtil.convertToUTCDate(TEST_FROM_DATE_NEW),programActivityDto.getStartDate());
        assertEquals(DateUtil.convertToUTCDate(TEST_END_DATE_NEW), programActivityDto.getEndDate());
        assertThat(programActivityDto.getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));

    }

    @Test
    public void test_update_program_activity_success(){

        ProgramActivityDTO programActivityDto = programActivityService
                .updateProgramActivity(generateProgramActivityDto(), TestConstants.ID_1);

        Mockito.verify(cnxtProgramActivityRepository, times(1)).save(any(ProgramActivity.class));

        assertThat(programActivityDto.getType(), equalTo(TEST_PROGRAM_ACTIVITY_TYPE));
        assertThat(programActivityDto.getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(programActivityDto.getStatus(), equalTo(StatusTypeCode.SCHEDULED.name()));
        assertEquals(DateUtil.convertToUTCDate(TEST_FROM_DATE_NEW),programActivityDto.getStartDate());
        assertEquals(DateUtil.convertToUTCDate(TEST_END_DATE_NEW), programActivityDto.getEndDate());
        assertThat(programActivityDto.getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));

    }

    @Test
    public void test_delete_program_activity_success(){

        programActivityService.deleteProgramActivity(TestConstants.ID_1);

        Mockito.verify(cnxtProgramActivityRepository, times(1)).delete(anyLong());
    }

    @Test
    public void test_delete_program_activity_success_with_files(){

        PowerMockito.when(programActivityFilesFindBy.find()).thenReturn(listProgramActivityFiles());

        programActivityService.deleteProgramActivity( TestConstants.ID_1);

        Mockito.verify(cnxtProgramActivityRepository, times(1)).delete(anyLong());
        Mockito.verify(programActivityFilesFindBy, times(1)).delete();
    }

    @Test
    public void test_get_active_program_activity_success(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        programActivityService.getAllProgramActivities(StatusTypeCode.ACTIVE.name(),
                ProgramActivityTypes.JUMBOTRON.name(), 1, 1);

        Mockito.verify(programActivityFindBy, times(1)).ge(any());
        Mockito.verify(programActivityFindBy, times(1)).le(any());

    }

    @Test
    public void test_get_expired_program_activity_success(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        programActivityService.getAllProgramActivities(StatusTypeCode.ENDED.name(),
                ProgramActivityTypes.CAROUSEL.name(), 1, 1);

        Mockito.verify(programActivityFindBy, times(1)).lt(any());
    }

    @Test
    public void test_get_scheduled_program_activity_success(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        programActivityService.getAllProgramActivities(StatusTypeCode.SCHEDULED.name(),
                ProgramActivityTypes.JUMBOTRON.name(), 1, 1);

        Mockito.verify(programActivityFindBy, times(1)).gt(any());
    }

    @Test
    public void test_get_multiple_statuses_program_activity_success(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        programActivityService.getAllProgramActivities(StatusTypeCode.SCHEDULED.name() + "," +
                StatusTypeCode.ACTIVE.name() + "," +
                StatusTypeCode.ENDED.name(), ProgramActivityTypes.JUMBOTRON.name(), 1, 1);

        Mockito.verify(programActivityFindBy, times(1)).gt(any());
        Mockito.verify(programActivityFindBy, times(1)).ge(any());
        Mockito.verify(programActivityFindBy, times(1)).le(any());
        Mockito.verify(programActivityFindBy, times(1)).lt(any());
    }

    @Test
    public void test_get_program_activity_success_pagination_size_1(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        List<ProgramActivityDTO> resultList = programActivityService.getAllProgramActivities(
                StatusTypeCode.ACTIVE.name(), ProgramActivityTypes.JUMBOTRON.name(), 1, 1);

        assertThat(resultList.size(), equalTo(1));
        assertThat(resultList.get(0).getType(), equalTo(TEST_PROGRAM_ACTIVITY_TYPE));
        assertThat(resultList.get(0).getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(resultList.get(0).getStatus(), equalTo(StatusTypeCode.ACTIVE.name()));
        assertEquals(DateUtil.convertToUTCDate(TEST_FROM_DATE_ACTIVE),resultList.get(0).getStartDate());
        assertEquals(DateUtil.convertToUTCDate(TEST_END_DATE_ACTIVE), resultList.get(0).getEndDate());
        assertThat(resultList.get(0).getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));

    }

    @Test
    public void test_get_program_activity_success_pagination_size_default(){
        PowerMockito.when(programActivityFindBy.find()).thenReturn(listProgramActivityTestObject());

        List<ProgramActivityDTO> resultList = programActivityService.getAllProgramActivities(
                StatusTypeCode.ACTIVE.name(), ProgramActivityTypes.JUMBOTRON.name(), null, null);

        assertThat(resultList.size(), equalTo(3));
        assertThat(resultList.get(0).getType(), equalTo(TEST_PROGRAM_ACTIVITY_TYPE));
        assertThat(resultList.get(0).getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(resultList.get(0).getStatus(), equalTo(StatusTypeCode.ACTIVE.name()));
        assertEquals(DateUtil.convertToUTCDate(TEST_FROM_DATE_ACTIVE),resultList.get(0).getStartDate());
        assertEquals(DateUtil.convertToUTCDate(TEST_END_DATE_ACTIVE), resultList.get(0).getEndDate());
        assertThat(resultList.get(0).getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));

    }

    @Test
    public void test_create_update_programActivity_fail_missing_type(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setType(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_MISSING_TYPE);
    }

    @Test
    public void test_create_update_programActivity_fail_invalid_type(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setType("INVALID TYPE");
        request.setStatus("ACTIVE");

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INVALID_TYPE);
    }

    @Test
    public void test_create_update_programActivity_fail_missing_programId(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setProgramId(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_MISSING_PROGRAM_ID);
    }

    @Test
    public void test_create_update_programActivity_fail_invalid_programId(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setProgramId(1978L);

        PowerMockito.when(programRepository.findOne(anyLong())).thenReturn(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INVALID_PROGRAM_ID);
    }

    @Test
    public void test_create_update_programActivity_fail_invactive_programId(){
        Program program = new Program();
        program.setStatusTypeCode(StatusTypeCode.INACTIVE.name());


        ProgramActivityDTO request = generateProgramActivityDto();
        request.setProgramId(1978L);

        PowerMockito.when(programRepository.findOne(anyLong())).thenReturn(program);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INACTIVE_PROGRAM);
    }

    @Test
    public void test_create_update_programActivity_fail_missing_startDate(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setStartDate(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_MISSING_START_DATE);
    }

    @Test
    public void test_create_update_programActivity_fail_invalid_startDate(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setStartDate("12/12/15");

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_START_DATE_INVALID);
    }

    @Test
    public void test_create_update_programActivity_fail_missing_endDate(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setEndDate(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_MISSING_END_DATE);
    }

    @Test
    public void test_create_update_programActivity_fail_invalid_endtDate(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setEndDate("12/12/15");

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INVALID_END_DATE);
    }

    @Test
    public void test_create_update_programActivity_fail_startDate_after_endDate(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setStartDate("2016-01-01");
        request.setEndDate("2015-12-01");

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_START_DATE_IS_AFTER_END_DATE);
    }

    @Test
    public void test_create_update_programActivity_fail_invalid_timeframe(){

        // cause a timeframe error.
        PowerMockito.when(cnxtProgramActivityRepository.isOverlappedTime(anyLong(), anyLong(),anyString(),
                anyString(),anyString())).thenReturn(TestConstants.ID_1);

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setStartDate(DateUtil.convertToUTCDate(TEST_FROM_DATE_OVERLAP));
        request.setEndDate(DateUtil.convertToUTCDate(TEST_END_DATE_OVERLAP));

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INVALID_TIMEFRAME);
    }

    @Test
    public void test_create_update_programActivity_fail_missing_description(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setDescription(null);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_MISSING_DESCRIPTION);
    }

    @Test
    public void test_create_update_programActivity_carousel_missing_description(){
        ProgramActivityDTO request = generateProgramActivityDto();
        request.setType(ProgramActivityTypes.CAROUSEL.name());
        request.setDescription(null);

        programActivityService.createProgramActivityList(Arrays.asList(request));

        Mockito.verify(programActivityDao, times(1)).save(any(ProgramActivity.class));

    }

    @Test
    public void test_create_update_programActivity_fail_description_too_long(){

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setDescription(TEST_DESCRIPTION_TOO_LONG);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_DESCRIPTION_TOO_LONG);
    }

    @Test
    public void test_update_programActivity_fail_invalid_program_activity_id(){

        PowerMockito.when(programActivityDao.findById(anyLong())).thenReturn(null) ;

        testFailUpdateProgramActivity(ProgramActivityConstants.ERROR_INVALID_PROGRAM_ACTIVITY_ID);
    }

    @Test
    public void test_delete_programActivity_fail_missing_program_activity_id(){

        testFailDeleteProgramActivity(null, ProgramActivityConstants.ERROR_MISSING_PROGRAM_ACTIVITY_ID);
    }

    @Test
    public void test_delete_programActivity_fail_invalid_program_activity_id(){

        PowerMockito.when(programActivityDao.findById(anyLong())).thenReturn(null) ;

        testFailDeleteProgramActivity(TestConstants.ID_1,
                ProgramActivityConstants.ERROR_INVALID_PROGRAM_ACTIVITY_ID);
    }

    @Test
    public void test_get_programActivity_fail_missing_status(){

        testFailGetProgramActivity(null, TEST_PROGRAM_ACTIVITY_TYPE,
                ProgramActivityConstants.ERROR_MISSING_STATUS);
    }

    @Test
    public void test_get_programActivity_fail_invalid_status(){
        String statusListString = "INVALID STATUS";
        testFailGetProgramActivity(statusListString, TEST_PROGRAM_ACTIVITY_TYPE,
                ProgramActivityConstants.ERROR_INVALID_STATUS);
    }

    @Test
    public void test_get_programActivity_fail_invalid_status_multiple(){
        String statusListString = "ACTIVE,INVALID STATUS";
        testFailGetProgramActivity(statusListString, TEST_PROGRAM_ACTIVITY_TYPE,
                ProgramActivityConstants.ERROR_INVALID_STATUS);
    }

    @Test
    public void test_get_programActivity_fail_missing_type(){

        testFailGetProgramActivity(TEST_STATUS, null, ProgramActivityConstants.ERROR_MISSING_TYPE);
    }

    @Test
    public void test_get_programActivity_fail_invalid_type(){
        String type = "INVALID TYPE";
        testFailGetProgramActivity(TEST_STATUS, type, ProgramActivityConstants.ERROR_INVALID_TYPE);
    }

    @Test
    public void test_create_program_activity_success_with_image(){

        Files file = new Files().setId(TEST_IMAGE_ID);
        PowerMockito.when(filesDao.findById(anyLong())).thenReturn(file);

        PowerMockito.doNothing().when(fileImageService).saveFileItemsEntry(anyLong(),anyString(),anyLong());
        PowerMockito.doNothing().when(programActivityFilesDao).save((ProgramActivityFiles) anyObject());

        PowerMockito.when(programActivityFilesDao.findBy()).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.where(ProjectConstants.PROGRAM_ACTIVITY_ID)).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.eq(anyLong())).thenReturn(programActivityFilesFindBy);
        PowerMockito.when(programActivityFilesFindBy.findOne(ProjectConstants.FILES_ID, Long.class)).thenReturn(TEST_IMAGE_ID);

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setImageId(TestConstants.ID_1);

        List<ProgramActivityDTO> programActivityDtoList =
                programActivityService.createProgramActivityList(Arrays.asList(request));
        ProgramActivityDTO programActivityDto = programActivityDtoList.get(0);

        Mockito.verify(programActivityDao, times(1)).save(any(ProgramActivity.class));

        assertThat(programActivityDto.getImageId(), equalTo(TestConstants.ID_1));

    }

    @Test
    public void test_update_program_activity_success_with_image(){

        PowerMockito.when(programActivityFilesFindBy.findOne()).thenReturn(new ProgramActivityFiles());

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setImageId(TestConstants.ID_1);

        ProgramActivityDTO programActivityDto = programActivityService.updateProgramActivity(request, TestConstants.ID_1);

        Mockito.verify(cnxtProgramActivityRepository, times(1)).save(any(ProgramActivity.class));
        Mockito.verify(programActivityFilesDao, times(1)).save(any(ProgramActivityFiles.class));

        assertThat(programActivityDto.getImageId(), equalTo(TestConstants.ID_1));

    }

    @Test
    public void test_update_program_activity_array_success_with_image(){

        PowerMockito.when(programActivityFilesFindBy.findOne()).thenReturn(new ProgramActivityFiles());

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setImageId(TestConstants.ID_1);

        ProgramActivityDTO request2 = generateProgramActivityDto();
        request2.setImageId(TestConstants.ID_1);

        List<ProgramActivityDTO> programActivityDtoList =
                programActivityService.updateProgramActivityList(Arrays.asList(request, request2));

        Mockito.verify(cnxtProgramActivityRepository, times(2)).save(any(ProgramActivity.class));
        Mockito.verify(programActivityFilesDao, times(2)).save(any(ProgramActivityFiles.class));

        assertThat(programActivityDtoList.get(0).getImageId(), equalTo(TestConstants.ID_1));
        assertThat(programActivityDtoList.get(1).getImageId(), equalTo(TestConstants.ID_1));

    }

    @Test
    public void test_create_update_program_activity_fail_invalid_image(){

        // causing invalid image error
        PowerMockito.when(filesDao.findById(anyLong())).thenReturn(null);

        ProgramActivityDTO request = generateProgramActivityDto();
        request.setImageId(TestConstants.ID_1);

        testFailCreateAndUpdateProgramActivity(request,ProgramActivityConstants.ERROR_INVALID_IMAGE_ID);
    }

    @Test
    public void test_get_program_activities_for_pax_jumbotron_success() {
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.PROGRAM_ID, TestConstants.ID_1);
        node.put(ProjectConstants.PROGRAM_ACTIVITY_ID, TestConstants.ID_2);
        node.put(ProjectConstants.PROGRAM_ACTIVITY_TYPE_CODE, ProgramActivityTypes.JUMBOTRON.name());
        node.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        node.put(ProjectConstants.FROM_DATE, "2017-01-01");
        node.put(ProjectConstants.THRU_DATE, "2017-12-31");
        node.put(ProjectConstants.DESCRIPTION, "Testing program activity endpoints");
        node.put(ProjectConstants.FILES_ID, TestConstants.ID_1);
        PowerMockito.when(programActivityDaoImpl.getProgramActivitiesForPax(anyLong(), anyString())).thenReturn(Arrays.asList(node));

        List<ProgramActivityDTO> result =
                programActivityService.getProgramActivitiesForPax(TestConstants.PAX_HARWELLM, ProgramActivityTypes.JUMBOTRON.name());

        Mockito.verify(programActivityDaoImpl, times(1)).getProgramActivitiesForPax(anyLong(), anyString());
        assertThat(result.size(), equalTo(1));

        ProgramActivityDTO programActivityDto = result.get(0);
        assertThat(programActivityDto.getId(), equalTo(TestConstants.ID_2));
        assertThat(programActivityDto.getType(), equalTo(ProgramActivityTypes.JUMBOTRON.name()));
        assertThat(programActivityDto.getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(programActivityDto.getStatus(), equalTo(StatusTypeCode.ACTIVE.name()));
        assertEquals("2017-01-01", programActivityDto.getStartDate());
        assertEquals("2017-12-31", programActivityDto.getEndDate());
        assertThat(programActivityDto.getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));
        assertThat(programActivityDto.getImageId(), equalTo(TestConstants.ID_1));
    }

    @Test
    public void test_get_program_activities_for_pax_carousel_success() {
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.PROGRAM_ID, TestConstants.ID_1);
        node.put(ProjectConstants.PROGRAM_ACTIVITY_ID, TestConstants.ID_2);
        node.put(ProjectConstants.PROGRAM_ACTIVITY_TYPE_CODE, ProgramActivityTypes.CAROUSEL.name());
        node.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.ACTIVE.name());
        node.put(ProjectConstants.FROM_DATE, "2017-01-01");
        node.put(ProjectConstants.THRU_DATE, "2017-12-31");
        node.put(ProjectConstants.DESCRIPTION, "Testing program activity endpoints");
        node.put(ProjectConstants.FILES_ID, TestConstants.ID_1);
        PowerMockito.when(programActivityDaoImpl.getProgramActivitiesForPax(anyLong(), anyString())).thenReturn(Arrays.asList(node));

        List<ProgramActivityDTO> result =
                programActivityService.getProgramActivitiesForPax(TestConstants.PAX_HARWELLM, ProgramActivityTypes.CAROUSEL.name());

        Mockito.verify(programActivityDaoImpl, times(1)).getProgramActivitiesForPax(anyLong(), anyString());
        assertThat(result.size(), equalTo(1));

        ProgramActivityDTO programActivityDto = result.get(0);
        assertThat(programActivityDto.getId(), equalTo(TestConstants.ID_2));
        assertThat(programActivityDto.getType(), equalTo(ProgramActivityTypes.CAROUSEL.name()));
        assertThat(programActivityDto.getProgramId(), equalTo(TestConstants.ID_1));
        assertThat(programActivityDto.getStatus(), equalTo(StatusTypeCode.ACTIVE.name()));
        assertEquals("2017-01-01", programActivityDto.getStartDate());
        assertEquals("2017-12-31", programActivityDto.getEndDate());
        assertThat(programActivityDto.getDescription(), equalTo(TEST_PROGRAM_ACTIVITY_DESCRIPTION));
        assertThat(programActivityDto.getImageId(), equalTo(TestConstants.ID_1));
    }

    @Test
    public void test_get_program_activities_for_pax_carousel_null_nodes() {
        List<ProgramActivityDTO> result =
                programActivityService.getProgramActivitiesForPax(TestConstants.PAX_HARWELLM, ProgramActivityTypes.JUMBOTRON.name());

        try {
            ProgramActivityDTO programActivityDto = result.get(0);
        } catch (IndexOutOfBoundsException e){
            assertNotNull(result);
        }
    }

    private void testFailCreateAndUpdateProgramActivity(ProgramActivityDTO request ,String errorCode){
        try{
            programActivityService.createProgramActivityList(Arrays.asList(request));
            fail(FAIL_CREATE_METHOD_MSG);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }

        try{
            programActivityService.updateProgramActivity(request, TestConstants.ID_1);
            fail(FAIL_UPDATE_METHOD_MSG);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }
    }

    private void testFailUpdateProgramActivity(String errorCode){
        try{
            programActivityService.updateProgramActivity(generateProgramActivityDto(), TestConstants.ID_1);
            fail(FAIL_UPDATE_METHOD_MSG);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }
    }

    private void testFailDeleteProgramActivity(Long programActivityId, String errorCode){
        try{
            programActivityService.deleteProgramActivity(programActivityId);
            fail(FAIL_DELETE_METHOD_MSG);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }
    }

    private void testFailGetProgramActivity(String statusListString, String type ,String errorCode){
        try{
            programActivityService.getAllProgramActivities(statusListString, type, null, null);
            fail(FAIL_GET_METHOD_MSG);
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, errorCode, errorMsgException);
        }
    }

    private ProgramActivityDTO generateProgramActivityDto(){
        ProgramActivityDTO dto = new ProgramActivityDTO();
        dto.setId(TestConstants.ID_1);
        dto.setType(TEST_PROGRAM_ACTIVITY_TYPE);
        dto.setProgramId(TestConstants.ID_1);
        dto.setStartDate(DateUtil.convertToUTCDate(TEST_FROM_DATE_NEW));
        dto.setEndDate(DateUtil.convertToUTCDate(TEST_END_DATE_NEW));
        dto.setDescription(TEST_PROGRAM_ACTIVITY_DESCRIPTION);
        dto.setImageId(null);

        return dto;
    }

    private ProgramActivity programActivityTestObject(){
        ProgramActivity testObj = new ProgramActivity();

        testObj.setProgramActivityId(TestConstants.ID_1);
        testObj.setProgramActivityDesc(TEST_PROGRAM_ACTIVITY_DESCRIPTION);
        testObj.setProgramActivityTypeCode(TEST_PROGRAM_ACTIVITY_TYPE);
        testObj.setProgramId(TestConstants.ID_1);
        testObj.setStatusTypeCode(TEST_STATUS);
        testObj.setFromDate(TEST_FROM_DATE_ACTIVE);
        testObj.setThruDate(TEST_END_DATE_ACTIVE);

        return testObj;
    }

    private List<ProgramActivity> listProgramActivityTestObject(){
        List<ProgramActivity> list = new ArrayList<>();

        ProgramActivity testObj = programActivityTestObject();
        testObj.setProgramActivityId(TestConstants.ID_1);
        testObj.setFromDate(TEST_FROM_DATE_ACTIVE);
        testObj.setThruDate(TEST_END_DATE_ACTIVE);
        list.add(testObj);

        testObj = programActivityTestObject();
        testObj.setProgramActivityId(TestConstants.ID_2);
        testObj.setFromDate(TEST_FROM_DATE_EXPIRED);
        testObj.setThruDate(TEST_END_DATE_EXPIRED);
        list.add(testObj);

        testObj = programActivityTestObject();
        testObj.setProgramActivityId(3L);
        testObj.setFromDate(TEST_FROM_DATE_SCHEDULED);
        testObj.setThruDate(TEST_END_DATE_SCHEDULED);
        list.add(testObj);

        return list;
    }

    private List<ProgramSummaryDTO> eligiblePrograms(){
        List<ProgramSummaryDTO> programs = new ArrayList<>();

        ProgramSummaryDTO programSummaryDto = new ProgramSummaryDTO();
        programSummaryDto.setProgramId(TestConstants.ID_1);
        programs.add(programSummaryDto);

        programSummaryDto = new ProgramSummaryDTO();
        programSummaryDto.setProgramId(TestConstants.ID_2);
        programs.add(programSummaryDto);

        return programs;
    }

    private List<ProgramActivityFiles> listProgramActivityFiles(){
        List<ProgramActivityFiles> files = new ArrayList<>();

        ProgramActivityFiles programActivityFile = new ProgramActivityFiles();
        programActivityFile.setProgramActivityId(TestConstants.ID_1);
        programActivityFile.setFilesId(TestConstants.ID_1);
        files.add(programActivityFile);

        programActivityFile.setProgramActivityId(TestConstants.ID_1);
        programActivityFile.setFilesId(TestConstants.ID_2);
        files.add(programActivityFile);

        programActivityFile.setProgramActivityId(TestConstants.ID_1);
        programActivityFile.setFilesId(3L);
        files.add(programActivityFile);

        return files;
    }
}
