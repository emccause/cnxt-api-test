package com.maritz.culturenext.program.activity.rest;

import static com.maritz.culturenext.constants.RestParameterConstants.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.activity.dto.ProgramActivityDTO;
import com.maritz.culturenext.program.activity.util.ProgramActivityTypes;
import com.maritz.test.AbstractRestTest;

public class ProgramActivityRestTest  extends AbstractRestTest {

    private ObjectWriter ow;
    
    @Before
    public void setup() {
        ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    }
    
    @Test
    public void test_get_program_activities() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/program-activities")
                .param(TYPE_REST_PARAM, ProgramActivityTypes.JUMBOTRON.name())
                .param(STATUS_REST_PARAM, StatusTypeCode.ACTIVE.name())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void test_post_program_activity_list() throws Exception {
        List<ProgramActivityDTO> programActivityList = new ArrayList<>();
        programActivityList.add(createProgramActivity());
        String json = ow.writeValueAsString(programActivityList);

        mockMvc.perform(
                post("/rest/program-activities")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isOk());
    }
    
    private ProgramActivityDTO createProgramActivity() {
        ProgramActivityDTO programActivity = new ProgramActivityDTO();
        programActivity.setProgramId(TestConstants.ID_2);
        programActivity.setStartDate("2017-01-01");
        programActivity.setEndDate("2017-12-31");
        programActivity.setDescription("Unit Testing!");
        programActivity.setType(ProgramActivityTypes.JUMBOTRON.name());
        programActivity.setImageId(11987L);
        return programActivity;
    }
    
    private ProgramActivityDTO getExistingProgramActivity() {
        //This is the PROGRAM_ACTIVITY record in the test database
        ProgramActivityDTO programActivity = new ProgramActivityDTO();
        programActivity.setId(TestConstants.ID_1);
        programActivity.setProgramId(TestConstants.ID_1);
        programActivity.setStartDate("2017-01-01");
        programActivity.setEndDate("2099-12-31");
        programActivity.setDescription("Program Activity for Unit Tests");
        programActivity.setType(ProgramActivityTypes.JUMBOTRON.name());
        programActivity.setImageId(11987L);
        return programActivity;
    }
    
    @Test
    public void test_update_program_activity() throws Exception {
        ProgramActivityDTO existingActivity = getExistingProgramActivity();
        //Change description
        existingActivity.setDescription("Updating Description");        
        String json = ow.writeValueAsString(existingActivity);
        
        mockMvc.perform(
                put("/rest/program-activities/1")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void test_update_program_activity_list() throws Exception {
        ProgramActivityDTO existingActivity = getExistingProgramActivity();
        //Change description
        existingActivity.setDescription("Updating Description in a list");    
        String json = ow.writeValueAsString(Arrays.asList(existingActivity));
        
        mockMvc.perform(
                put("/rest/program-activities")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json)
                )
                .andExpect(status().isOk()).andReturn();
    }
    
    @Test
    public void test_delete_program_activity() throws Exception {
        mockMvc.perform(
                delete("/rest/program-activities/1")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void test_get_program_activities_for_pax() throws Exception {            
        MvcResult result = mockMvc.perform(
                get("/rest/participants/8571/program-activities")
                .param(TYPE_REST_PARAM, ProgramActivityTypes.JUMBOTRON.name())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void test_get_program_activities_for_pax_invalid_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/invalid/program-activities")
                .param(TYPE_REST_PARAM, ProgramActivityTypes.JUMBOTRON.name())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isNotFound()).andReturn();
    }
    
}
