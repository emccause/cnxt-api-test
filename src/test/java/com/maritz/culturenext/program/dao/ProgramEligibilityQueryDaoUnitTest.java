package com.maritz.culturenext.program.dao;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.dao.impl.ProgramEligibilityQueryDaoImpl;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProgramEligibilityQueryDaoUnitTest extends AbstractMockTest {

    @InjectMocks
    private ProgramEligibilityQueryDaoImpl programEligibilityQueryDao;

    @Mock
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Test
    public void test_getEligiblePrograms_nonnull_thruDate() {
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put("PROGRAM_ID", 1L);
        node.put(ProjectConstants.PROGRAM_NAME, "My Test Program");
        node.put(ProjectConstants.PROGRAM_DESCRIPTION_SHORT, "Test program description");
        node.put(ProjectConstants.PROGRAM_DESCRIPTION_LONG, "Longer description of the test program");
        node.put(ProjectConstants.PROGRAM_TYPE, "PEER_TO_PEER");
        node.put(ProjectConstants.PROGRAM_CATEGORY, "POINT_LOAD");
        node.put("TYPE", "");
        try {
            DateFormat createDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date result = createDate.parse("2017-01-01");
            node.put("CREATE_DATE", result);
            result = createDate.parse("2017-06-26");
            node.put("FROM_DATE", result);
            result = createDate.parse("2017-07-01");
            node.put("THRU_DATE", result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        node.put(ProjectConstants.LIKE_COUNT, TestConstants.PAX_ADAM_SMITH);
        nodes.add(node);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(MapSqlParameterSource.class), Matchers.any(ColumnMapRowMapper.class))).thenReturn(nodes);

        List<String> programTypeList = new ArrayList<>();
        programTypeList.add("");
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH_ADMIN);
        groupIds.add(TestConstants.ID_1);
        List<ProgramSummaryDTO> programs = programEligibilityQueryDao.getEligiblePrograms(programTypeList, TestConstants.ID_1, paxIds, groupIds);
        Date thruDate = (Date) node.get("THRU_DATE");
        Assert.assertTrue(programs.get(0).getThruDate().equals(DateUtil.convertToUTCDate(thruDate)));
    }

    @Test
    public void test_validateProgram_nonnull_nodes() {
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH_ADMIN);
        groupIds.add(TestConstants.ID_1);

        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put(ProjectConstants.PROGRAM_NAME, "My Test Program");
        node.put(ProjectConstants.PROGRAM_DESCRIPTION_SHORT, "Test program description");
        node.put("TYPE", "");
        nodes.add(node);

        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(MapSqlParameterSource.class), Matchers.any(ColumnMapRowMapper.class))).thenReturn(nodes);
        Boolean validateProgram = programEligibilityQueryDao.validateProgram(null, null, paxIds, groupIds);
        Assert.assertTrue(validateProgram);
    }
}