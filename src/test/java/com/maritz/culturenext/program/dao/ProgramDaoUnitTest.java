package com.maritz.culturenext.program.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.dao.impl.ProgramDaoImpl;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.test.AbstractMockTest;

public class ProgramDaoUnitTest extends AbstractMockTest {

    @InjectMocks
    private ProgramDaoImpl programDao;

    @Mock private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Test
    public void setLikeInfo_empty_nodes() throws Exception{
        List<Map<String, Object>> node = new ArrayList<>();
        ProgramSummaryDTO programSummaryDTO = new ProgramSummaryDTO();
        programSummaryDTO.setProgramId(TestConstants.ID_1);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(MapSqlParameterSource.class), Matchers.any(CamelCaseMapRowMapper.class))).thenReturn(node);
        programDao.setLikeInfo(TestConstants.PAX_ADAM_SMITH, programSummaryDTO);
    }

    @Test
    public void getProgramImageId() throws Exception{
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put("FILES_ID", 1L);
        nodes.add(node);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(MapSqlParameterSource.class), Matchers.any(ColumnMapRowMapper.class))).thenReturn(nodes);
        Long imageId = programDao.getProgramImageId(TestConstants.ID_1);
        assertTrue(imageId == 1L);
    }
}