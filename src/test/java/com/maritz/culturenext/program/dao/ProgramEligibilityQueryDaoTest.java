package com.maritz.culturenext.program.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.dao.ProgramEligibilityQueryDao;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.test.AbstractDatabaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProgramEligibilityQueryDaoTest extends AbstractDatabaseTest{

    private static final String GIVE = "GIVE";

    @Inject private ProgramEligibilityQueryDao programEligibilityQueryDao;

    @Test
    public void test_is_pax_id_eligible_to_give_from_program() {
        assertTrue(programEligibilityQueryDao.checkIfEligibleProgramsForPax(TestConstants.PAX_DAGARFIN, GIVE));
    }

    @Test
    public void test_is_pax_id_ineligible_to_give_from_program_invalid_pax() {
        assertFalse(programEligibilityQueryDao.checkIfEligibleProgramsForPax(100000L, GIVE));
    }

    @Test
    public void test_is_pax_id_ineligible_to_give_from_program_valid_pax() {
        assertFalse(programEligibilityQueryDao.checkIfEligibleProgramsForPax(142L, GIVE));
    }
    
    @Test
    public void test_eligible_to_receive_but_not_give() {
        assertFalse(programEligibilityQueryDao.checkIfEligibleProgramsForPax(12637L, GIVE));
    }
    
    @Test
    public void test_eligible_to_give_only_from_point_load() {
        assertFalse(programEligibilityQueryDao.checkIfEligibleProgramsForPax(12638L, GIVE));
    }

    @Test
    public void test_eligible_receivers_by_pax_ids() {
        List<Long> paxIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_MUDDAM);
        List<Long> groupIds = new ArrayList<>();
        List<Map<String, Object>> resultsMapList =
            programEligibilityQueryDao.getEligibleReceiversForProgram(TestConstants.PROGRAM_RECEIVERS_HAPPY_PATH,
                                                                      paxIds,
                                                                      groupIds);

        List<Long> recipientPaxIds = new ArrayList<>();
        List<Long> recipientGroupIds = new ArrayList<>();
        for (Map<String, Object> resultsMap : resultsMapList) {
            Long id = (Long) resultsMap.get("subjectId");
            if ("PAX".equals(resultsMap.get("subjectTable"))) {
                recipientPaxIds.add(id);
            }
            if ("GROUPS".equals(resultsMap.get("subjectTable"))) {
                recipientGroupIds.add(id);
            }
        }

        assertTrue(recipientPaxIds.contains(TestConstants.PAX_MUDDAM));
        assertTrue(recipientGroupIds.isEmpty());
    }

    @Test
    public void test_eligible_receivers_by_group_ids() {
        final Long GROUP_ID = 1534L;
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(GROUP_ID);
        List<Map<String, Object>> resultsMapList =
            programEligibilityQueryDao.getEligibleReceiversForProgram(4487L,
                                                                      paxIds,
                                                                      groupIds);

        List<Long> recipientPaxIds = new ArrayList<>();
        List<Long> recipientGroupIds = new ArrayList<>();
        for (Map<String, Object> resultsMap : resultsMapList) {
            Long id = (Long) resultsMap.get("subjectId");
            if ("PAX".equals(resultsMap.get("subjectTable"))) {
                recipientPaxIds.add(id);
            }
            if ("GROUPS".equals(resultsMap.get("subjectTable"))) {
                recipientGroupIds.add(id);
            }
        }

        assertTrue(recipientPaxIds.isEmpty());
        assertTrue(recipientGroupIds.contains(GROUP_ID));
    }

    @Test
    public void test_eligible_receivers_with_no_receivers() {
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        List<Map<String, Object>> resultsMapList =
            programEligibilityQueryDao.getEligibleReceiversForProgram(TestConstants.PROGRAM_RECEIVERS_HAPPY_PATH,
                                                                      paxIds,
                                                                      groupIds);

        assertTrue(resultsMapList.isEmpty());
    }

    @Test
    public void test_ineligible_receivers() {
        List<Long> paxIds = new ArrayList<>();
        paxIds.add(-1L);
        List<Long> groupIds = new ArrayList<>();
        List<Map<String, Object>> resultsMapList =
            programEligibilityQueryDao.getEligibleReceiversForProgram(TestConstants.PROGRAM_RECEIVERS_HAPPY_PATH,
                                                                      paxIds,
                                                                      groupIds);

        assertTrue(resultsMapList.isEmpty());
    }

    @Test
    public void test_eligible_receivers_by_bad_program_id() {
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(1534L);
        List<Map<String, Object>> resultsMapList =
            programEligibilityQueryDao.getEligibleReceiversForProgram(-1L,
                                                                      paxIds,
                                                                      groupIds);

        assertTrue(resultsMapList.isEmpty());
    }

    @Test
    public void test_validate_program() {
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH);
        groupIds.add(TestConstants.ID_1);
        Boolean validateProgram = programEligibilityQueryDao.validateProgram(TestConstants.ID_1, TestConstants.ID_1, paxIds, groupIds);
        assertFalse(validateProgram);
    }

    @Test
    public void test_validate_program_empty_paxIds(){
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(TestConstants.ID_1);
        Boolean validateProgram = programEligibilityQueryDao.validateProgram(TestConstants.ID_1, TestConstants.ID_1, paxIds, groupIds);
        assertFalse(validateProgram);
    }

    @Test
    public void test_validate_program_empty_groupIds(){
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH_ADMIN);
        Boolean validateProgram = programEligibilityQueryDao.validateProgram(TestConstants.ID_1, TestConstants.ID_1, paxIds, groupIds);
        assertFalse(validateProgram);
    }

    @Test
    public void test_resolve_programs(){
        List<String> programCategoryList = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH_ADMIN);
        groupIds.add(TestConstants.ID_1);
        programCategoryList.add("");
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryDao.getEligiblePrograms(programCategoryList, TestConstants.ID_1, paxIds, groupIds);
        assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_resolve_programs_empty_paxIds(){
        List<String> programCategoryList = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(TestConstants.ID_1);
        programCategoryList.add("");
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryDao.getEligiblePrograms(programCategoryList, TestConstants.ID_1, paxIds, groupIds);
        assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_resolve_programs_empty_groupIds(){
        List<String> programCategoryList = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();
        List<Long> groupIds = new ArrayList<>();
        paxIds.add(TestConstants.PAX_ADAM_SMITH_ADMIN);
        programCategoryList.add("");
        List<ProgramSummaryDTO> programSummaryDTOList = programEligibilityQueryDao.getEligiblePrograms(programCategoryList, TestConstants.ID_1, paxIds, groupIds);
        assertNotNull(programSummaryDTOList);
    }
}
