package com.maritz.culturenext.program.dao;

import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.program.dao.ProgramDao;
import com.maritz.culturenext.program.dto.ProgramSummaryDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

import java.util.*;

import static org.junit.Assert.*;

public class ProgramDaoTest extends AbstractDatabaseTest {
    final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject private ProgramDao programDao;
    @Inject private ProgramRepository programRepository;

    @Test
    public void test_getProgramByStatus_request_for_active() {
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.ACTIVE.name());

        List<ProgramSummaryDTO> programList = programDao.getProgramByStatus(true, statusList, true, false, false, null);
        
        assertNotNull("ProgramList should not be null but was.",programList);

        for(ProgramSummaryDTO program : programList) {
            Date fromDate = null;
            Date thruDate = null;
            Date today = new Date();

            if (program.getFromDate() != null) {
                fromDate = DateUtil.convertFromUTCStringDate(program.getFromDate());
            }

            if (program.getThruDate() != null) {
                thruDate = DateUtil.convertFromUTCStringDate(program.getThruDate());
            }

            boolean validScheduledStatus = (fromDate != null && fromDate.after(today));

            boolean validEndedStatus = (fromDate != null && fromDate.before(today))
                    && (thruDate != null && thruDate.before(today));
                        
            assertFalse("Application Program type was not expected but was present!.",
                    ProgramTypeEnum.APPLICATION.toString().equalsIgnoreCase(program.getProgramType()));
            
            assertTrue(validScheduledStatus && program.getStatus().equals(StatusTypeCode.SCHEDULED.name())
                    || validEndedStatus && program.getStatus().equals(StatusTypeCode.ENDED.name())
                    || program.getStatus().equals(StatusTypeCode.ACTIVE.name()));
        }
        
    }

    @Test
    public void test_getProgramByStatus_request_for_scheduled() {
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.SCHEDULED.name());

        List<ProgramSummaryDTO> programList = programDao.getProgramByStatus(true, statusList, false, true, false, null);

        for(ProgramSummaryDTO program : programList) {
            assertTrue(program.getStatus().equals(StatusTypeCode.SCHEDULED.name()));
            assertFalse("Application Program type was not expected but was present!.",
                    ProgramTypeEnum.APPLICATION.toString().equalsIgnoreCase(program.getProgramType()));
        }
    }

    @Test
    public void test_getProgramByStatus_request_for_ended() {
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.ENDED.name());

        List<ProgramSummaryDTO> programListList = programDao.getProgramByStatus(true, statusList, false, false, true, null);

        for(ProgramSummaryDTO program : programListList) {
            // INACTIVE programs can come back too, they're also "ended"
            assertTrue((program.getStatus().equals(StatusTypeCode.ENDED.name()) || program.getStatus().equals(StatusTypeCode.INACTIVE.name()))); 
            assertFalse("Application Program type was not expected but was present!.",
                    ProgramTypeEnum.APPLICATION.toString().equalsIgnoreCase(program.getProgramType()));
        }
    }
    
    @Test
    public void test_getProgramByStatus_request_for_active_by_peer_to_peer() {
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.ACTIVE.name());
        
        List<ProgramSummaryDTO> programList = programDao.getProgramByStatus(true, statusList, true, false, false, ProgramTypeEnum.PEER_TO_PEER.name());
        
        assertNotNull("ProgramList should not be null but was.",programList);

        for(ProgramSummaryDTO program : programList) {
            Date fromDate = null;
            Date thruDate = null;
            Date today = new Date();

            if (program.getFromDate() != null) {
                fromDate = DateUtil.convertFromUTCStringDate(program.getFromDate());
            }

            if (program.getThruDate() != null) {
                thruDate = DateUtil.convertFromUTCStringDate(program.getThruDate());
            }

            boolean validScheduledStatus = (fromDate != null && fromDate.after(today));

            boolean validEndedStatus = (fromDate != null && fromDate.before(today))
                    && (thruDate != null && thruDate.before(today));
            
            
            assertTrue("Other than Peer to peer Program type was not expected but was present!.",
                    ProgramTypeEnum.PEER_TO_PEER.name().equalsIgnoreCase(program.getProgramType()));
            
            assertTrue(validScheduledStatus && program.getStatus().equals(StatusTypeCode.SCHEDULED.name())
                    || validEndedStatus && program.getStatus().equals(StatusTypeCode.ENDED.name())
                    || program.getStatus().equals(StatusTypeCode.ACTIVE.name()));
        }
    }

    @Test
    public void test_getProgramByStatus_null_received_status(){
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.SCHEDULED.name());
        List<ProgramSummaryDTO> programSummaryDTOList = programDao.getProgramByStatus(false, statusList, true, false, false, null);
        assertNotNull(programSummaryDTOList);
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertTrue(programSummaryDTO.getStatus().equals("SCHEDULED"));
        }
    }

    @Test
    public void test_getProgramByStatus_empty_query_string(){
        List<String> statusList = new ArrayList<>();
        statusList.add(StatusTypeCode.SCHEDULED.name());
        List<ProgramSummaryDTO> programSummaryDTOList = programDao.getProgramByStatus(true, statusList, true, true, true, null);
        assertNotNull(programSummaryDTOList);
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertNotNull(programSummaryDTO.getStatus());
        }
    }

    @Test
    public void test_getProgramByStatus_null_status_list(){
        List<ProgramSummaryDTO> programSummaryDTOList = programDao.getProgramByStatus(true, null, true, false, false, null);
        assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_getProgramByStatus_empty_status_list(){
        List<String> statusList = new ArrayList<>();
        List<ProgramSummaryDTO> programSummaryDTOList = programDao.getProgramByStatus(true, statusList, true, false, false, null);
        assertNotNull(programSummaryDTOList);
    }

    @Test
    public void test_getProgramVisibility(){
        String programVisibility = programDao.getProgramVisibility(TestConstants.ID_1);
        assertNotNull(programVisibility);
    }

    @Test
    public void test_getProgramVisibility_null_id(){
        String programVisibility = programDao.getProgramVisibility(null);
        assertNotNull(programVisibility);
    }

    @Test
    public void test_getProgramImageId(){
        Long programImageId = programDao.getProgramImageId(TestConstants.ID_1);
        assertNull(programImageId);
    }

    @Test
    public void test_getProgramImageId_null_id(){
        Long programImageId = programDao.getProgramImageId(null);
        assertNull(programImageId);
    }

    @Test
    public void test_getProgramDtoData(){
        // getProgramDtoData should return 9 values, NEWSFEED_VISIBILITY, NOTIFICATION_RECIPIENT, NOTIFICATION_RECIPIENT_MGR,
        // NOTIFICATION_RECIPIENT_2ND_MGR, NOTIFICATION_SUBMITTER, ALLOW_TEMPLATE_UPLOAD, SELECTED_AWARD_TYPE, PPP_INDEX_ENABLED, REMINDER_DAYS.
        // No null values allowed. Empty strings might be present though.
        Map<String,String> mapProgramDtoData = programDao.getProgramDtoData(TestConstants.ID_1);
        assertTrue(!mapProgramDtoData.isEmpty());
        assertTrue(mapProgramDtoData.size() ==  9);
        assertTrue(mapProgramDtoData.get(
                VfName.NEWSFEED_VISIBILITY.toString()).equalsIgnoreCase("PUBLIC") ||
                        mapProgramDtoData.get(VfName.NEWSFEED_VISIBILITY.toString()).equalsIgnoreCase("PRIVATE") ||
                        mapProgramDtoData.get(VfName.NEWSFEED_VISIBILITY.toString()).equalsIgnoreCase("RECIPIENT_PREF") ||
                        mapProgramDtoData.get(VfName.NEWSFEED_VISIBILITY.toString()).equalsIgnoreCase("NONE")
                );
        assertNotNull(mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT.toString()));
        assertTrue(mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT.toString()).equalsIgnoreCase("TRUE") ||
                mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT.toString()).equalsIgnoreCase("FALSE"));
        assertNotNull(mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT_MGR.toString()));
        assertTrue(mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT_MGR.toString()).equalsIgnoreCase("TRUE") ||
                mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT_MGR.toString()).equalsIgnoreCase("FALSE"));
        assertNotNull(mapProgramDtoData.get(VfName.NOTIFICATION_RECIPIENT_2ND_MGR.toString()));
        assertNotNull(mapProgramDtoData.get(VfName.NOTIFICATION_SUBMITTER.toString()));
        assertNotNull(mapProgramDtoData.get(VfName.ALLOW_TEMPLATE_UPLOAD.toString()));
        assertNotNull(mapProgramDtoData.get(VfName.SELECTED_AWARD_TYPE.toString()));
        assertTrue(mapProgramDtoData.get(VfName.SELECTED_AWARD_TYPE.toString()).equalsIgnoreCase("DPP"));
        assertNotNull(mapProgramDtoData.get(VfName.PPP_INDEX_ENABLED.toString()));
        assertNull(mapProgramDtoData.get("NO_PRESENT_KEY"));
    }
}