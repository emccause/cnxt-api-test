package com.maritz.culturenext.program.dao;

import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.hamcrest.Matchers.is;

public class MilestoneDaoTest extends AbstractDatabaseTest {
    @Inject
    private MilestoneDao milestoneDao;

    @Inject
    private ProgramRepository programRepository;

    @Inject
    private SysUserRepository sysUserRepository;

    private static final String BIRTHDAY_EVENT = MilestoneConstants.BIRTHDAY_TYPE;
    private static final String SERVICE_ANNIVERSARY_EVENT = MilestoneConstants.SERVICE_ANNIVERSARY_TYPE;
    private static final String SERVICE_ANNIVERSARY_REMINDER_EVENT = MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE;

    private Long PAX_ID = null;
    private Long PAX_ID_MANAGER = null;
    private Long PROGRAM_ID_SA = null;
    private Long PROGRAM_ID_BIRTHDAY = null;

    @Before
    public void setup() {
        PAX_ID = sysUserRepository.findBy()
                .where(ProjectConstants.SYS_USER_NAME).eq(TestConstants.USER_MUDDAM)
                .findOne(ProjectConstants.PAX_ID, Long.class);

        PAX_ID_MANAGER = sysUserRepository.findBy()
                .where(ProjectConstants.SYS_USER_NAME).eq(TestConstants.USER_PORTERGA)
                .findOne(ProjectConstants.PAX_ID, Long.class);

        PROGRAM_ID_BIRTHDAY = programRepository.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq("MILESTONE_TEST_PROGRAM_BIRTHDAY")
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);

        PROGRAM_ID_SA = programRepository.findBy()
                .where(ProjectConstants.PROGRAM_NAME).eq("MILESTONE_TEST_PROGRAM")
                .findOne(ProjectConstants.PROGRAM_ID, Long.class);
    }

    @Test
    public void test_get_enrolled_programs_for_pax_birthday() {
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, BIRTHDAY_EVENT, null, null);
        assertEquals(1, enrolledPrograms.size());
        assertEquals(PROGRAM_ID_BIRTHDAY, enrolledPrograms.get(0).get("programId"));
        assertNotNull(enrolledPrograms.get(0).get("programActivityId"));
        assertEquals("1", enrolledPrograms.get(0).get("yearsOfService"));
        assertEquals("TRUE", enrolledPrograms.get(0).get("repeating"));
        assertEquals(1L, enrolledPrograms.get(0).get("programRank"));
    }

    @Test
    public void test_get_enrolled_programs_for_pax_birthday_reminders() {
        LocalDateTime fiveDaysInFuture = LocalDateTime.now().plusDays(5);
        int month = fiveDaysInFuture.getMonthValue();
        int day = fiveDaysInFuture.getDayOfMonth();
        MonthDay monthDay = MonthDay.of(month, day);
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, BIRTHDAY_EVENT, null, monthDay);
        assertEquals(1, enrolledPrograms.size());
        assertEquals(PROGRAM_ID_BIRTHDAY, enrolledPrograms.get(0).get("programId"));
        assertNotNull(enrolledPrograms.get(0).get("programActivityId"));
        assertEquals("1", enrolledPrograms.get(0).get("yearsOfService"));
        assertEquals("TRUE", enrolledPrograms.get(0).get("repeating"));
        assertEquals(1L, enrolledPrograms.get(0).get("programRank"));
    }

    @Test
    public void test_get_enrolled_programs_for_pax_birthday_reminders_no_programs() {
        LocalDateTime now = LocalDateTime.now();
        int month = now.getMonthValue();
        int day = now.getDayOfMonth();
        MonthDay monthDay = MonthDay.of(month, day);
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, BIRTHDAY_EVENT, null, monthDay);
        assertEquals(0, enrolledPrograms.size());
    }

    @Test
    public void test_get_enrolled_programs_for_pax_service_anniversary() {
        Long anniversaryYear = 1L;
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, SERVICE_ANNIVERSARY_EVENT, anniversaryYear, null);
        assertEquals(1, enrolledPrograms.size());
        assertEquals(PROGRAM_ID_SA, enrolledPrograms.get(0).get("programId"));
        assertNotNull(enrolledPrograms.get(0).get("programActivityId"));
        assertEquals("1", enrolledPrograms.get(0).get("yearsOfService"));
        assertEquals("TRUE", enrolledPrograms.get(0).get("repeating"));
        assertEquals(1L, enrolledPrograms.get(0).get("programRank"));
    }

    @Test
    public void test_get_enrolled_programs_for_pax_service_anniversary_reminders() {
        LocalDateTime fifteenDaysInFuture = LocalDateTime.now().plusDays(15);
        int month = fifteenDaysInFuture.getMonthValue();
        int day = fifteenDaysInFuture.getDayOfMonth();
        MonthDay monthDay = MonthDay.of(month, day);
        Long anniversaryYear = 1L;
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, SERVICE_ANNIVERSARY_EVENT, anniversaryYear, monthDay);
        assertEquals(1, enrolledPrograms.size());
        assertEquals(PROGRAM_ID_SA, enrolledPrograms.get(0).get("programId"));
        assertNotNull(enrolledPrograms.get(0).get("programActivityId"));
        assertEquals("1", enrolledPrograms.get(0).get("yearsOfService"));
        assertEquals("TRUE", enrolledPrograms.get(0).get("repeating"));
        assertEquals(1L, enrolledPrograms.get(0).get("programRank"));
    }

    @Test
    public void test_get_enrolled_programs_for_pax_service_anniversary_reminders_no_programs() {
        LocalDateTime now = LocalDateTime.now();
        int month = now.getMonthValue();
        int day = now.getDayOfMonth();
        MonthDay monthDay = MonthDay.of(month, day);
        Long anniversaryYear = 1L;
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, SERVICE_ANNIVERSARY_EVENT, anniversaryYear, monthDay);
        assertEquals(0, enrolledPrograms.size());
    }

    @Test
    public void test_get_enrolled_programs_for_pax_inactive_user() {
        Long anniversaryYear = 1L;
        PAX_ID = sysUserRepository.findBy()
                .where(ProjectConstants.SYS_USER_NAME).eq("jorgenmm")
                .findOne(ProjectConstants.PAX_ID, Long.class);
        List<Map<String, Object>> enrolledPrograms = milestoneDao.getEnrolledProgramsForPax(PAX_ID, SERVICE_ANNIVERSARY_EVENT, anniversaryYear, null);
        assertEquals(0, enrolledPrograms.size());
    }

    @Test
        public void test_find_participants_with_milestones_birthday() {
        MonthDay monthDay = MonthDay.of(1, 4);
        List<Map<String, Object>> participants = milestoneDao.findParticipantsWithMilestones(BIRTHDAY_EVENT, monthDay);
        assertEquals(1, participants.size());
        assertEquals(6L, participants.get(0).get("paxId"));
        assertEquals("01-04", participants.get(0).get("milestoneDate"));
    }

    @Test
        public void test_find_participants_with_milestones_service_anniversary() {
        MonthDay monthDay = MonthDay.of(7, 16);
        List<Map<String, Object>> participants = milestoneDao.findParticipantsWithMilestones(SERVICE_ANNIVERSARY_EVENT, monthDay);
        assertEquals(1, participants.size());
        assertEquals(6L, participants.get(0).get("paxId"));
        assertEquals("2014-07-16", participants.get(0).get("milestoneDate"));
    }

    @Test
    public void test_find_upcoming_milestone_dates_birthday() {
        List<Map<String, Object>> upcomingMilestoneDates = milestoneDao.findUpcomingMilestoneDates(BIRTHDAY_EVENT);
        assertEquals(1, upcomingMilestoneDates.size());
    }

    @Test
    public void test_find_upcoming_milestone_dates_service_anniversary() {
        List<Map<String, Object>> upcomingMilestoneDates = milestoneDao.findUpcomingMilestoneDates(SERVICE_ANNIVERSARY_EVENT);
        assertEquals(1, upcomingMilestoneDates.size());
    }

    @Test
    public void test_get_milestone_program_data_service_anniversary() {
        Long anniversaryYear = 5L;
        Map<String, Object> programData = milestoneDao.getMilestoneProgramData(PAX_ID, PROGRAM_ID_SA, SERVICE_ANNIVERSARY_EVENT, anniversaryYear);

        Long programId = (Long) programData.get("programId");
        String programName = (String) programData.get("programName");
        Long budgetId = (Long) programData.get("budgetId");
        Long directManager = (Long) programData.get("directManager");
        String payoutType = (String) programData.get("payoutType");
        String repeating = (String) programData.get("repeating");
        Double awardAmount = Double.parseDouble(programData.get("awardAmount").toString());
        Integer yearsOfService = Integer.parseInt(programData.get("yearsOfService").toString());
        Long eCardId = Long.parseLong(programData.get("ecardId").toString());
        String publicHeadline = (String) programData.get("publicHeadline");
        String privateMessage = (String) programData.get("privateMessage");

        assertThat(programId, is(PROGRAM_ID_SA));
        assertThat(programName, is("MILESTONE_TEST_PROGRAM"));
        assertNotNull(budgetId);
        assertThat(directManager, is(PAX_ID_MANAGER));
        assertThat(payoutType, is("DPP"));
        assertThat(repeating, is("FALSE"));
        assertThat(awardAmount, is(10D));
        assertThat(yearsOfService, is(5));
        assertNotNull(eCardId);
        assertThat(publicHeadline, is("HAPPY SERVICE ANNIVERSARY 2"));
        assertThat(privateMessage, is(nullValue()));
    }

    @Test
    public void test_get_milestone_program_data_service_anniversary_first_year() {
        Long anniversaryYear = 1L;
        Map<String, Object> programData = milestoneDao.getMilestoneProgramData(PAX_ID, PROGRAM_ID_SA, SERVICE_ANNIVERSARY_EVENT, anniversaryYear);

        Long programId = (Long) programData.get("programId");
        String programName = (String) programData.get("programName");
        Long budgetId = (Long) programData.get("budgetId");
        Long directManager = (Long) programData.get("directManager");
        String payoutType = (String) programData.get("payoutType");
        String repeating = (String) programData.get("repeating");
        Double awardAmount = Double.parseDouble(programData.get("awardAmount").toString());
        Integer yearsOfService = Integer.parseInt(programData.get("yearsOfService").toString());
        Long eCardId = Long.parseLong(programData.get("ecardId").toString());
        String publicHeadline = (String) programData.get("publicHeadline");
        String privateMessage = (String) programData.get("privateMessage");

        assertThat(programId, is(PROGRAM_ID_SA));
        assertThat(programName, is("MILESTONE_TEST_PROGRAM"));
        assertNotNull(budgetId);
        assertThat(directManager, is(PAX_ID_MANAGER));
        assertThat(payoutType, is("DPP"));
        assertThat(repeating, is("TRUE"));
        assertThat(awardAmount, is(1D));
        assertThat(yearsOfService, is(1));
        assertNotNull(eCardId);
        assertThat(publicHeadline, is("HAPPY SERVICE ANNIVERSARY"));
        assertThat(privateMessage, is(nullValue()));
    }

    @Test
    public void test_get_milestone_program_data_service_anniversary_reminder() {
        Long anniversaryYear = 5L;
        Map<String, Object> programData = milestoneDao.getMilestoneProgramData(PAX_ID, PROGRAM_ID_SA, SERVICE_ANNIVERSARY_REMINDER_EVENT, anniversaryYear);

        Long programId = (Long) programData.get("programId");
        String programName = (String) programData.get("programName");
        Long budgetId = (Long) programData.get("budgetId");
        Long directManager = (Long) programData.get("directManager");
        String payoutType = (String) programData.get("payoutType");
        String repeating = (String) programData.get("repeating");
        Double awardAmount = Double.parseDouble(programData.get("awardAmount").toString());
        Integer yearsOfService = Integer.parseInt(programData.get("yearsOfService").toString());
        Long eCardId = Long.parseLong(programData.get("ecardId").toString());
        String publicHeadline = (String) programData.get("publicHeadline");
        String privateMessage = (String) programData.get("privateMessage");

        assertThat(programId, is(PROGRAM_ID_SA));
        assertThat(programName, is("MILESTONE_TEST_PROGRAM"));
        assertNotNull(budgetId);
        assertThat(directManager, is(PAX_ID_MANAGER));
        assertThat(payoutType, is("DPP"));
        assertThat(repeating, is("FALSE"));
        assertThat(awardAmount, is(10D));
        assertThat(yearsOfService, is(5));
        assertNotNull(eCardId);
        assertThat(publicHeadline, is("HAPPY SERVICE ANNIVERSARY 2"));
        assertThat(privateMessage, is(nullValue()));
    }
}
