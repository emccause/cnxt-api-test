package com.maritz.culturenext.program.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.test.AbstractDatabaseTest;

public class EligibilityDaoTest extends AbstractDatabaseTest {

    // pax for testing
    private static final long TEST_PAX_ID_1 = 4417L;
    private static final long TEST_PAX_ID_2 = 5961L;

    // Personal groups for testing
    private static final long TEST_GROUP_1_ID = 1447L;
    private static final long TEST_GROUP_2_ID = 1446L;

    private static final long TEST_GROUP_ENROLLMENT_ID = 1243L;
    private static final long TEST_GROUP_ROLE_BASED_ID = 5L;
    private static final long TEST_GROUP_CUSTOM_ID = 1414L;
    private static final long TEST_GROUP_HIERARCHY_ID = 8L;

    private static final long INVALID_PAX_ID = 999999999L;
    private static final long INVALID_GROUP_ID = 999999999L;

    private static final String SEARCH_TYPE = "searchType";
    private static final String PAX_SEARCH_TYPE = "P";
    private static final String GROUP_SEARCH_TYPE = "G";
    private static final String STATUS = "status";
    private static final String DOES_NOT_EXIST_STATUS = "DOES_NOT_EXIST";
    private static final String INVALID_GROUP_TYPE_STATUS = "INVALID_GROUP_TYPE";
    private static final String INPUT_ID = "inputId";

    @Inject private EligibilityDao eligibilityDao;

    @Test
    public void testGetEligibilityNoDTOs() {
        // get eligibility objects with no specified ids
        List<EntityDTO> entityDTOs = 
                eligibilityDao.getEligibilityEntities(new ArrayList<Long>(), new ArrayList<Long>());

        assertTrue(entityDTOs.isEmpty());
    }
    
    @Test
    public void testGetEligibilityGroupEntityDTOs() {
        // set up groups to look up
        List<Long> groupIds = new ArrayList<Long>(Arrays.asList(TEST_GROUP_1_ID, TEST_GROUP_2_ID));

        // get Groups DTO entities using DAO
        List<EntityDTO> groupsDTOs = eligibilityDao.getEligibilityEntities(new ArrayList<Long>(), groupIds);

        // verify groups
        assertNotNull(groupsDTOs);
        assertEquals(groupIds.size(), groupsDTOs.size());

        verifyGroupsEntityDTOExistence(groupsDTOs, groupIds);
    }

    @Test
    public void testGetEligibilityPaxEntityDTOs() {
        // set up groups to look up
        List<Long> paxIds = new ArrayList<Long>(Arrays.asList(TEST_PAX_ID_1, TEST_PAX_ID_2));

        // get Groups DTO entities using DAO
        List<EntityDTO> paxDTOs = eligibilityDao.getEligibilityEntities(paxIds, new ArrayList<Long>());

        // verify paxs
        assertNotNull(paxDTOs);
        assertEquals(paxIds.size(), paxDTOs.size());

        verifyPaxsEntityDTOExistence(paxDTOs, paxIds);
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusNoIds() {
        // get object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                new ArrayList<Long>(), new ArrayList<Long>(), new ArrayList<String>());

        // verify result
        assertNotNull(resultMappings);
        assertTrue(resultMappings.isEmpty());
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusExistingPaxs() {
        List<Long> existingPaxs = new ArrayList<Long>(Arrays.asList(TEST_PAX_ID_1, TEST_PAX_ID_2));

        // get PAX object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                existingPaxs, new ArrayList<Long>(), new ArrayList<String>());

        // verify result
        assertNotNull(resultMappings);
        assertEquals(existingPaxs.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, existingPaxs, new ArrayList<Long>(), 
                new ArrayList<Long>(), new ArrayList<String>());
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusInvalidPax() {
        List<Long> invalidPaxIds = new ArrayList<Long>(Arrays.asList(INVALID_PAX_ID));

        // get Groups object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                new ArrayList<Long>(), invalidPaxIds, new ArrayList<String>());

        // verify result
        assertNotNull(resultMappings);
        assertEquals(invalidPaxIds.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, new ArrayList<Long>(), new ArrayList<Long>(), 
                invalidPaxIds, new ArrayList<String>());
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusExistingGroups() {
        List<Long> existingGroups = new ArrayList<Long>(Arrays.asList(TEST_GROUP_1_ID));

        // get Groups object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                new ArrayList<Long>(), existingGroups, new ArrayList<String>());

        // verify result
        assertNotNull(resultMappings);
        assertEquals(existingGroups.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, new ArrayList<Long>(), existingGroups, 
                new ArrayList<Long>(), new ArrayList<String>());
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusInvalidGroup() {
        List<Long> invalidGroups = new ArrayList<Long>(Arrays.asList(INVALID_GROUP_ID));

        // get Groups object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                new ArrayList<Long>(), invalidGroups, new ArrayList<String>());

        // verify result
        assertNotNull(resultMappings);
        assertEquals(invalidGroups.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, new ArrayList<Long>(), new ArrayList<Long>(),
                invalidGroups, new ArrayList<String>());
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusPersonalGroupsOnlyValid() {
        // setup groups to query eligibility
        List<Long> existingGroups = new ArrayList<Long>(Arrays.asList(
                TEST_GROUP_1_ID, TEST_GROUP_CUSTOM_ID, TEST_GROUP_ENROLLMENT_ID,
                TEST_GROUP_HIERARCHY_ID, TEST_GROUP_ROLE_BASED_ID));
        List<String> validGroupTypeNames = new ArrayList<String>(Arrays.asList("PERSONAL"));

        // get Groups object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                new ArrayList<Long>(), existingGroups, validGroupTypeNames);

        // verify result
        assertNotNull(resultMappings);
        assertEquals(existingGroups.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, new ArrayList<Long>(), existingGroups, 
                new ArrayList<Long>(), validGroupTypeNames);
    }

    @Test
    public void testGetEligibilityEntitiesAndStatus() {
        // setup groups and paxs to query eligibility
        List<Long> existingGroupIds = new ArrayList<Long>(Arrays.asList(
                TEST_GROUP_1_ID, TEST_GROUP_CUSTOM_ID, TEST_GROUP_ENROLLMENT_ID));
        List<Long> nonExistingGroupIds = new ArrayList<Long>(Arrays.asList(INVALID_GROUP_ID));
        List<String> validGroupTypeNames = new ArrayList<String>(Arrays.asList("ENROLLMENT", "CUSTOM", "ROLE BASED"));

        List<Long> existingPaxIds = new ArrayList<Long>(Arrays.asList(TEST_PAX_ID_1, TEST_PAX_ID_2));
        List<Long> nonExistingPaxIds = new ArrayList<Long>(Arrays.asList(INVALID_PAX_ID));

        List<Long> allGroupIds = new ArrayList<Long>();
        allGroupIds.addAll(existingGroupIds);
        allGroupIds.addAll(nonExistingGroupIds);

        List<Long> allPaxIds = new ArrayList<Long>();
        allPaxIds.addAll(existingPaxIds);
        allPaxIds.addAll(nonExistingPaxIds);

        List<Long> allInvalidIds = new ArrayList<Long>();
        allInvalidIds.addAll(nonExistingGroupIds);
        allInvalidIds.addAll(nonExistingPaxIds);

        // get object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                allPaxIds, allGroupIds, validGroupTypeNames);

        // verify result
        assertNotNull(resultMappings);
        assertEquals(allGroupIds.size() + allPaxIds.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, existingPaxIds, existingGroupIds, allInvalidIds, validGroupTypeNames);
    }

    @Test
    public void testGetEligibilityEntitiesAndStatusDuplicateIds() {
        // setup group and pax ids with duplicate entries for querying
        List<Long> existingPaxIds = new ArrayList<Long>(Arrays.asList(TEST_PAX_ID_1, TEST_PAX_ID_1));
        List<Long> existingGroupIds = new ArrayList<Long>(Arrays.asList(
                TEST_GROUP_CUSTOM_ID, TEST_GROUP_CUSTOM_ID, TEST_GROUP_CUSTOM_ID));
        List<String> validGroupTypeNames = new ArrayList<String>(Arrays.asList("ENROLLMENT", "CUSTOM", "ROLE BASED"));

        // get Groups object mappings using DAO
        List<Map<String, Object>> resultMappings = eligibilityDao.getEligibilityEntitiesAndStatus(
                existingPaxIds, existingGroupIds, validGroupTypeNames);

        // verify result
        List<Long> expectedPaxIds = new ArrayList<Long>(Arrays.asList(TEST_PAX_ID_1));
        List<Long> expectedGroupIds = new ArrayList<Long>(Arrays.asList(TEST_GROUP_CUSTOM_ID));
        assertNotNull(resultMappings);
        assertEquals(expectedGroupIds.size() + expectedPaxIds.size(), resultMappings.size());
        verifyObjectMapExistence(resultMappings, expectedPaxIds, expectedGroupIds, 
                new ArrayList<Long>(), validGroupTypeNames);
    }

    // helper methods

    private void verifyGroupsEntityDTOExistence(@Nonnull List<EntityDTO> groupsDTOs, 
            @Nonnull List<Long> expectedGroupsIds) {
        // copy expected ids (don't want to modify given input)
        List<Long> groupIds = new ArrayList<Long>(expectedGroupsIds);

        // verify DTO objects as GroupDTO objects and groupId
        for(EntityDTO entityDTO : groupsDTOs) {
            assertTrue(entityDTO instanceof GroupDTO);
            GroupDTO groupDTO = (GroupDTO) entityDTO;
            assertNotNull(groupDTO);

            Long groupId = groupDTO.getGroupId();
            assertNotNull(groupId);
            assertTrue(groupIds.contains(groupId));

            groupIds.remove(groupId);
        }

        assertTrue(groupIds.isEmpty());
    }

    private void verifyPaxsEntityDTOExistence(@Nonnull List<EntityDTO> paxDTOs, @Nonnull List<Long> expectedPaxIds) {
        // copy expected ids (don't want to modify given input)
        List<Long> paxIds = new ArrayList<Long>(expectedPaxIds);

        // verify DTO objects as EmployeeDTO objects and paxId
        for(EntityDTO entityDTO : paxDTOs) {
            assertTrue(entityDTO instanceof EmployeeDTO);
            EmployeeDTO employeeDTO = (EmployeeDTO) entityDTO;
            assertNotNull(employeeDTO);

            Long paxId = employeeDTO.getPaxId();
            assertNotNull(paxId);
            assertTrue(paxIds.contains(paxId));

            paxIds.remove(paxId);
        }

        assertTrue(paxIds.isEmpty());
    }

    private void verifyObjectMapExistence(@Nonnull List<Map<String, Object>> paxObjectMappings,
                                              @Nonnull List<Long> expectedValidPaxIds,
                                              @Nonnull List<Long> expectedValidGroupIds,
                                              @Nonnull List<Long> expectedNonValidIds,
                                              @Nonnull List<String> validGroupTypeNames) {
        // copy expected ids (don't want to modify given input)
        List<Long> validPaxIds = new ArrayList<Long>(expectedValidPaxIds);
        List<Long> validGroupIds = new ArrayList<Long>(expectedValidGroupIds);
        List<Long> nonValidIds = new ArrayList<Long>(expectedNonValidIds);

        // verify object mappings as EmployeeDTO objects and paxId
        for(Map<String, Object> node : paxObjectMappings) {
            // check if it should exist or not and handle accordingly
            if(DOES_NOT_EXIST_STATUS.equals(node.get(STATUS))) {
                Long invalidId = Long.valueOf((Integer) node.get(INPUT_ID));
                assertTrue(nonValidIds.contains(invalidId));
                nonValidIds.remove(invalidId);
                continue;
            }

            // get search type and check pax and group
            assertTrue(node.containsKey(SEARCH_TYPE));
            String searchType = (String) node.get(SEARCH_TYPE);

            if(PAX_SEARCH_TYPE.equalsIgnoreCase(searchType)) {
                EmployeeDTO employeeDTO = new EmployeeDTO(node, null);

                // check pax id
                Long paxId = employeeDTO.getPaxId();
                assertNotNull(paxId);
                assertTrue(validPaxIds.contains(paxId));

                validPaxIds.remove(paxId);
            } else if(GROUP_SEARCH_TYPE.equalsIgnoreCase(searchType)) {
                GroupDTO groupDTO = new GroupDTO(node);

                // check group id
                Long groupId = groupDTO.getGroupId();
                assertNotNull(groupId);
                assertTrue(validGroupIds.contains(groupId));

                // check status (only if we specified for valid groups)
                if(!validGroupTypeNames.isEmpty() && !validGroupTypeNames.contains(groupDTO.getType())) {
                    assertTrue(INVALID_GROUP_TYPE_STATUS.equals(node.get(STATUS)));
                }

                validGroupIds.remove(groupId);
            } else {
                fail("Got object mapping with unknown search type!");
            }
        }

        assertTrue(validGroupIds.isEmpty());
        assertTrue(validPaxIds.isEmpty());
        assertTrue(nonValidIds.isEmpty());
    }
}