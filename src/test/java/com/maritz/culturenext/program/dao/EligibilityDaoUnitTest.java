package com.maritz.culturenext.program.dao;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.program.dao.impl.EligibilityDaoImpl;
import com.maritz.test.AbstractMockTest;

public class EligibilityDaoUnitTest extends AbstractMockTest {

    @InjectMocks
    private EligibilityDaoImpl eligibilityDao;

    @Mock
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final long TEST_PAX_ID_1 = 4417L;
    private static final long TEST_PAX_ID_2 = 5961L;
    private static final String RS_SEARCH_TYPE = "searchType";

    @Test
    public void testGetEligibilityEntities_log_warning() {
        List<Long> paxIds = new ArrayList<>(Arrays.asList(TEST_PAX_ID_1, TEST_PAX_ID_2));

        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node = new HashMap<>();
        node.put(RS_SEARCH_TYPE, "a");
        nodes.add(node);
        PowerMockito.when(namedParameterJdbcTemplate.query(Matchers.anyString(), Matchers.any(MapSqlParameterSource.class), Matchers.any(CamelCaseMapRowMapper.class))).thenReturn(nodes);

        List<EntityDTO> paxDTOs = eligibilityDao.getEligibilityEntities(paxIds, new ArrayList<Long>());

        assertNotNull(paxDTOs);
        assertNotEquals(paxIds.size(), paxDTOs.size());
    }
}
