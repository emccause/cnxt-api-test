package com.maritz.culturenext.program.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramBudget;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.repository.AclRepository;
import com.maritz.core.jpa.repository.ProgramBudgetRepository;
import com.maritz.core.jpa.repository.ProgramRepository;
import com.maritz.core.jpa.repository.RoleRepository;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.test.AbstractDatabaseTest;

public class ProgramBudgetsDaoTest extends AbstractDatabaseTest {

    @Inject private ProgramRepository programRepository;
    @Inject private ProgramBudgetRepository programBudgetRepository;
    @Inject private AclRepository aclRepository;
    @Inject private RoleRepository roleRepository;
    
    @Inject private ProgramBudgetsDao programBudgetsDao;

    @Test
    public void getEligibleGivingMembersForProgramWithNoBudgets() {
        Program program = programRepository.findOneRsql("programName==\"2015 Awards Program budget test 2\"");
        assertThat(program, is(notNullValue()));

        Map<Long, List<EntityDTO>> result = programBudgetsDao.getEligibileGivingMembers(program.getProgramId());

        assertTrue(result.isEmpty());
    }

    @Test
    public void getEligibleGivingMembersForProgramWithOneBudgetHavingPaxGivingMember() {
        Program program = programRepository.findOneRsql("programName==\"2015 Awards Program budget test dev 1\"");
        List<ProgramBudget> programBudgetList = programBudgetRepository.findAllRsql("programId==" + program.getProgramId());
        Role role = roleRepository.findByRoleCode("PROGRAM_BUDGET_GIVER");
        List<Acl> budgetGiverList = aclRepository.findAllRsql("roleId==" + role.getRoleId() + ";targetTable==\"PROGRAM_BUDGET\";targetId==" + programBudgetList.get(0).getId() + ";subjectTable==\"PAX\"");
        assertThat(budgetGiverList.size(), is(1));
        
        Map<Long, List<EntityDTO>> result = programBudgetsDao.getEligibileGivingMembers(program.getProgramId());

        assertThat(result.size(), is(programBudgetList.size()));
        List<EntityDTO> givingMembers = result.get(programBudgetList.get(0).getBudgetId());
        assertThat(givingMembers.size(), is(budgetGiverList.size()));
        EmployeeDTO entity = (EmployeeDTO)givingMembers.get(0);
        assertThat(entity.getPaxId(), is(budgetGiverList.get(0).getSubjectId()));
    }

    @Test
    public void getEligibleGivingMembersForProgramWithOneBudgetHavingGroupGivingMember() {
        Program program = programRepository.findOneRsql("programName==\"Program Activity testing\"");
        List<ProgramBudget> programBudgetList = programBudgetRepository.findAllRsql("programId==" + program.getProgramId());
        Role role = roleRepository.findByRoleCode("PROGRAM_BUDGET_GIVER");
        List<Acl> budgetGiverList = aclRepository.findAllRsql("roleId==" + role.getRoleId() + ";targetTable==\"PROGRAM_BUDGET\";targetId==" + programBudgetList.get(0).getId() + ";subjectTable==\"GROUPS\"");
        assertThat(budgetGiverList.size(), is(1));

        Map<Long, List<EntityDTO>> result = programBudgetsDao.getEligibileGivingMembers(program.getProgramId());

        assertThat(result.size(), is(programBudgetList.size()));
        List<EntityDTO> givingMembers = result.get(programBudgetList.get(0).getBudgetId());
        assertThat(givingMembers.size(), is(budgetGiverList.size()));
        GroupDTO entity = (GroupDTO)givingMembers.get(0);
        assertThat(entity.getGroupId(), is(budgetGiverList.get(0).getSubjectId()));
    }

    @Test
    public void getEligibleGivingMembersForProgramWithMultipleBudgetsHavingGroupGivingMember() {
        Program program = programRepository.findOneRsql("programName==\"2015 Awards Program 2.1.5\"");
        List<ProgramBudget> programBudgetList = programBudgetRepository.findAllRsql("programId==" + program.getProgramId());
        assertThat(programBudgetList.size(), is(3));
        List<Long> expectedBudgetIds = new ArrayList<Long>();
        for (ProgramBudget programBudget : programBudgetList) {
            expectedBudgetIds.add(programBudget.getBudgetId());
        }
        
        Map<Long, List<EntityDTO>> result = programBudgetsDao.getEligibileGivingMembers(program.getProgramId());

        assertThat(result.size(), is(programBudgetList.size()));
        assertTrue("Result is missing expected Budget Ids", result.keySet().containsAll(expectedBudgetIds));
        assertTrue("Result contains Budget Ids that were not expected", expectedBudgetIds.containsAll(result.keySet()));
    }
}