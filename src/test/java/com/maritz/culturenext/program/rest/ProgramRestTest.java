package com.maritz.culturenext.program.rest;

import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.inject.Inject;
import static org.junit.Assert.assertThat;

import com.maritz.culturenext.budget.dto.BudgetAssignmentDTO;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.jpa.entity.ProgramActivity;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.ProgramActivityRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.dto.AwardTierDTO;
import com.maritz.culturenext.budget.dto.BudgetAssignmentStubDTO;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.NewsfeedVisibility;
import com.maritz.culturenext.enums.ProgramActivityTypeCode;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.enums.Visibility;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.program.dto.*;
import com.maritz.test.AbstractRestTest;

public class ProgramRestTest extends AbstractRestTest {

    private static final String MANAGER_DISCRETIONARY = "MANAGER_DISCRETIONARY";
    private static final String APPLICATION = "APPLICATION";
    private static final String PEER_TO_PEER = "PEER_TO_PEER";
    private static final String TEST_PROGRAM_NAME = "JUNIT Test Program Name";
    private static final String TEST_SHORT_DESC = "JUNIT test short description";
    private static final String TEST_LONG_DESC = "JUNIT test long description";
    
    private static final String GET_TRANSACTION_FOR_POINT_LOAD = 
            "/rest/programs/POINT_LOAD/transactions/44935";
    private static final String GET_TRANSACTION_FOR_PEER_TO_PEER = 
            "/rest/programs/PEER_TO_PEER/transactions/32784";
    private static final String GET_TRANSACTION_FOR_MANAGER_DISCRETIONARY  = 
            "/rest/programs/MANAGER_DISCRETIONARY/transactions/34368";
    private static final String GET_TRANSACTION_FOR_AWARD_CODE  = 
            "/rest/programs/AWARD_CODE/transactions/70771";
    private static final String DELETE_MILESTONE_BY_ID =
            "/rest/programs/1/milestones/3";
    private static final String DELETE_MILESTONE_BY_ID_NO_PROGRAM_ID =
            "/rest/programs//milestones/3";
    private static final String DELETE_MILESTONE_BY_ID_NO_MILESTONE_ID =
            "/rest/programs/1/milestones/";
    
    @Inject private ParticipantInfoDao participantInfoDao;
    @Inject private ProgramActivityRepository programActivityRepository;
    @Inject private SysUserRepository sysUserRepository;
    
    @Test
    public void get_program_list() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList) {
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
        }

    }
    
    @Test
    public void get_program_list_for_specific_status() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?status=ACTIVE")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
        }
    }
    
    @Test
    public void get_program_list_for_specific_status_filtered_by_peer_to_peer() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?status=ACTIVE&programType='PTP,MGRMD'")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
            assertThat(programSummaryDTO.getProgramType(), anyOf(is(PEER_TO_PEER),is(MANAGER_DISCRETIONARY)));
        }
    }
    
    @Test
    public void get_program_list_for_specific_status_filtered_by_peer_but_not_application() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?status=ACTIVE&programType=PTP")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
            assertThat(programSummaryDTO.getProgramType(), is(PEER_TO_PEER));
            assertThat(programSummaryDTO.getProgramType(), is(not(APPLICATION)));            
        }
    }
    
    @Test
    public void get_program_list_for_specific_status_filtered_out_not_existing_Program_type() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?status=ACTIVE&programType=PROGRAM_TYPE")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        assertThat(programSummaryDTOList.size(), is(0));
    }

    @Test
    public void get_program_list_for_multiple_statuses() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?status=ACTIVE,ENDED")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
        }
    }

    @Test
    public void get_program_list_language_code() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?languageCode=en_US")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
        }
    }

    @Test
    public void get_program_list_language_code_french() throws Exception {
        List<ProgramSummaryDTO> programSummaryDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs?languageCode=fr_CA")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramSummaryDTO.class
        );

        assertThat(programSummaryDTOList, notNullValue());
        for(ProgramSummaryDTO programSummaryDTO : programSummaryDTOList){
            assertThat(programSummaryDTO.getProgramId(), notNullValue());
        }
    }

    @Test
    public void get_program_by_id() throws Exception {
        ProgramFullDTO programFullDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get("/rest/program/" + TestConstants.ID_1)
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramFullDTO.class
        );

        assertThat(programFullDTO.getProgramId(), is(TestConstants.ID_1));
    }

    @Test
    public void get_program_by_id_language_code() throws Exception {
        ProgramFullDTO programFullDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get("/rest/program/20?languageCode=en_US")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramFullDTO.class
        );

        assertThat(programFullDTO.getProgramId(), notNullValue());
    }

    @Test
    public void get_program_by_id_language_code_french() throws Exception {
        ProgramFullDTO programFullDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get("/rest/program/" + TestConstants.ID_1 + "?languageCode=fr_CA")
                                .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramFullDTO.class
        );

        assertThat(programFullDTO.getProgramId(), is(TestConstants.ID_1));
    }
    
    @Test
    public void get_program_by_id_fail_programId_as_string() throws Exception {
        mockMvc.perform(
                get("/rest/program/programIdString?languageCode=fr_CA")
                        .with(authToken(TestConstants.USER_HAGOPIWL)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void get_program_budgets_by_program_id() throws Exception {
        List<BudgetAssignmentDTO> budgetAssignmentDTOList = ObjectUtils.jsonToList(
                mockMvc.perform(
                        get("/rest/programs/" + TestConstants.ID_1 + "/budgets")
                        .with(authToken(TestConstants.USER_HAGOPIWL)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                        ,BudgetAssignmentDTO.class
                );

        assertNotNull(budgetAssignmentDTOList);
        assertEquals(3, budgetAssignmentDTOList.size());
        assertTrue(budgetAssignmentDTOList.get(0).getBudget().getBudgetId().equals(1L));
    }

    @Test
    public void get_program_budgets_by_program_id_fail_programId_as_string() throws Exception {
        mockMvc.perform(get("/rest/program/programIdString/budgets")
                .with(authToken(TestConstants.USER_HAGOPIWL)))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void post_program() throws Exception {        
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.POST.toString());
        
        String json = ObjectUtils.objectToJson(programBasicDTO);

        ProgramFullDTO programFullDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        post("/rest/program")
                                .with(authToken(TestConstants.USER_HAGOPIWL))
                                .contentType(MediaType.APPLICATION_JSON).content(json)
                )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,ProgramFullDTO.class
        );

        assertThat(programFullDTO.getProgramName(), is(programBasicDTO.getProgramName()));
        assertThat(programFullDTO.getProgramDescriptionShort(), is(programBasicDTO.getProgramDescriptionShort()));
        assertThat(programFullDTO.getProgramDescriptionLong(), is(programBasicDTO.getProgramDescriptionLong()));
        assertThat(programFullDTO.getProgramType(), is(programBasicDTO.getProgramType()));
        assertThat(programFullDTO.getFromDate(), is(programBasicDTO.getFromDate()));
        assertThat(programFullDTO.getStatus(), anyOf(is(programBasicDTO.getStatus()), is("SCHEDULED"), is("ENDED")));
    }
    
    @Test
    public void post_program_from_date_in_past() throws Exception {        
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.POST.toString());
        programBasicDTO.setFromDate("01-01-2000");
        String json = ObjectUtils.objectToJson(programBasicDTO);

        mockMvc.perform(
                post("/rest/program")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    public void post_program_from_after_thru_date() throws Exception {        
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.POST.toString());
        programBasicDTO.setFromDate("01-01-2050");
        String json = ObjectUtils.objectToJson(programBasicDTO);
            
        mockMvc.perform(
                post("/rest/program")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
        ;
    }
    
    @Test
    public void post_program_recognition_criteria_invalid() throws Exception {        
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.POST.toString());
        programBasicDTO.setRecognitionCriteriaIds(Arrays.asList(9000000L, 99999999L));
        String json = ObjectUtils.objectToJson(programBasicDTO);
            
        mockMvc.perform(
                post("/rest/program")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
        ;
    }
    
    @Test
    public void post_program_ecard_id_invalid() throws Exception {        
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.POST.toString());
        programBasicDTO.setEcardIds(Arrays.asList(9000000L, 99999999L));
        String json = ObjectUtils.objectToJson(programBasicDTO);
            
        mockMvc.perform(
                post("/rest/program")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest());
        ;
    }
    
    @Test
    public void put_program() throws Exception {
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        String json = ObjectUtils.objectToJson(programBasicDTO);

        ProgramFullDTO programFullDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        put("/rest/program/39")
                                .with(authToken(TestConstants.USER_HAGOPIWL)).
                                contentType(MediaType.APPLICATION_JSON).content(json))
                        .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString()
                ,ProgramFullDTO.class
        );
        assertThat(programFullDTO.getProgramId(), notNullValue());
    }
    
    @Test
    public void put_program_null_short_desc() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setProgramDescriptionShort("");
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_null_long_desc() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setProgramDescriptionLong("");
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_duplicate_program_name() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setProgramName("CITI Thank You");
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_from_date_format_invalid() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setFromDate("JUNK");
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_thru_date_format_invalid() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setThruDate("JUNK");
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_mismatched_ids() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/40")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }        
    
    @Test
    public void put_program_invalid_recognition_criteria_id() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setRecognitionCriteriaIds(Arrays.asList(9000000L, 99999999L));
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_invalid_ecard_id() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), RequestMethod.PUT.toString());
        programBasicDTO.setEcardIds(Arrays.asList(9000000L, 99999999L));
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/39")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void put_program_fail_programId_as_string() throws Exception {    
        ProgramBasicDTO programBasicDTO = 
                setupProgramRequestDTOForTesting(new ProgramRequestDTO(), "PUT");
        programBasicDTO.setEcardIds(Arrays.asList(9000000L, 99999999L));
        String json = ObjectUtils.objectToJson(programBasicDTO);
        
        mockMvc.perform(
                put("/rest/program/programIdString")
                .with(authToken(TestConstants.USER_HAGOPIWL)).
                contentType(MediaType.APPLICATION_JSON).content(json))
            .andExpect(status().isNotFound())
        ;
    }
    
    private ProgramBasicDTO setupProgramRequestDTOForTesting(ProgramRequestDTO programRequestDTO, String responseType){

        Calendar calendar = Calendar.getInstance();
        int year = Calendar.getInstance().get(Calendar.YEAR) + 1;
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        //setup the programBasicDTO object
        programRequestDTO.setProgramName(TEST_PROGRAM_NAME);
        programRequestDTO.setProgramDescriptionShort(TEST_SHORT_DESC);
        programRequestDTO.setProgramDescriptionLong(TEST_LONG_DESC);
        programRequestDTO.setProgramType(ProgramTypeEnum.PEER_TO_PEER.name());
        programRequestDTO.setThruDate(getProgramEndDate(formatter, year, calendar));
        programRequestDTO.setFromDate(getProgramStartDate(formatter, year, calendar));
        programRequestDTO.setStatus(StatusTypeCode.ACTIVE.name());
        programRequestDTO.setRecognitionCriteriaIds(Arrays.asList(TestConstants.ID_2, TestConstants.ID_1));
        programRequestDTO.setEcardIds(Arrays.asList(74L, 75L));
        programRequestDTO.setAwardTiers(new ArrayList<AwardTierDTO>());
        programRequestDTO.setBudgetAssignments(new ArrayList<BudgetAssignmentStubDTO>());
        programRequestDTO.setNewsfeedVisibility(NewsfeedVisibility.NONE.toString());
        programRequestDTO.setNotification(Arrays.asList(VfName.NOTIFICATION_RECIPIENT.toString()));
        programRequestDTO.setProgramVisibility(Visibility.PUBLIC.toString());
        programRequestDTO.setAllowTemplateUpload(Boolean.TRUE);
        programRequestDTO.setPayoutType(TestConstants.DPP);
        programRequestDTO.setPppIndexEnabled(Boolean.TRUE);
        
        EligibilityStubDTO eligibilityStubDTO = new EligibilityStubDTO();
        eligibilityStubDTO.setGive(new ArrayList<EligibilityEntityStubDTO>());
        eligibilityStubDTO.setReceive(new ArrayList<EligibilityEntityStubDTO>());
        programRequestDTO.setEligibility(eligibilityStubDTO);

        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_HAGOPIWL);
        programRequestDTO.setCreatorPax(participantInfoDao.getEmployeeDTO(sysUser.getPaxId()));

        //setup the ProgramMiscDTO
        if (responseType.equals(RequestMethod.POST.toString())) {
            //do nothing
        } else {
            programRequestDTO.setProgramId(39);
        }
        return programRequestDTO;
        
    }

    private String getProgramStartDate(DateFormat formatter, int year, Calendar calendar) {
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        Date start = calendar.getTime();

        return formatter.format(start);
    }

    private String getProgramEndDate(DateFormat formatter, int year, Calendar calendar) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.DAY_OF_YEAR, 31);
        Date end = calendar.getTime();

        return formatter.format(end);
    }

    @Test
    public void test_getNominationOrTransactionByID_point_load() throws Exception {
        mockMvc.perform(
                get(GET_TRANSACTION_FOR_POINT_LOAD)
                        .with(authToken(TestConstants.USER_GARCIAF2))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_getNominationOrTransactionByID_peer_to_peer() throws Exception {
        TransactionDTO transactionDTO = ObjectUtils.jsonToObject(
            mockMvc.perform(
                    get(GET_TRANSACTION_FOR_PEER_TO_PEER)
                            .with(authToken(TestConstants.USER_GARCIAF2)))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString()
                ,TransactionDTO.class
        );

        assertThat(transactionDTO.getNominationId(), notNullValue());
    }
    
    @Test
    public void test_getNominationOrTransactionByID_manager_discretionary() throws Exception {
        TransactionDTO transactionDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get(GET_TRANSACTION_FOR_MANAGER_DISCRETIONARY)
                                .with(authToken(TestConstants.USER_GARCIAF2)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,TransactionDTO.class
        );

        assertThat(transactionDTO.getNominationId(), notNullValue());
    }
    
    @Test
    public void test_getNominationOrTransactionByID_award_code() throws Exception {
        TransactionDTO transactionDTO = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get(GET_TRANSACTION_FOR_AWARD_CODE)
                                .with(authToken(TestConstants.USER_GARCIAF2)))
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                ,TransactionDTO.class
        );

        assertThat(transactionDTO.getNominationId(), notNullValue());
    }

    @Test
    public void test_deleteMilestoneById_good() throws Exception {
        ProgramActivity programActivity = programActivityRepository.findBy()
                .where("programActivityName").in(ProgramActivityTypeCode.getNames())
                .findOne();
        assertNotNull("test data wasn't created!", programActivity);
        mockMvc.perform(
                delete(String.format("/rest/programs/%s/milestones/%s", programActivity.getProgramId(), programActivity.getProgramActivityId()))
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_deleteMilestoneById_no_match() throws Exception {
        mockMvc.perform(
                delete(DELETE_MILESTONE_BY_ID)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void test_deleteMilestoneById_no_program_id() throws Exception {
        mockMvc.perform(
                delete(DELETE_MILESTONE_BY_ID_NO_PROGRAM_ID)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void test_deleteMilestoneById_no_milestone_id() throws Exception {
        mockMvc.perform(
                delete(DELETE_MILESTONE_BY_ID_NO_MILESTONE_ID)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isNotFound());
    }
}