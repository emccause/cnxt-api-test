package com.maritz.culturenext.program.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ProgramEligibilityQueryRestServiceTest extends AbstractRestTest {

    @Test
    public void get_program_eligibility() throws Exception {
        MvcResult result = mockMvc.perform(
                post("/rest/participants/~/programs/query")
                .with(authToken(TestConstants.USER_PORTERGA))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"programCategory\":[\"POINT_LOAD\",\"RECOGNITION\"],\"receivers\":[{\"paxId\":969},"
                        + "{\"paxId\":4103},{\"paxId\":8571},{\"paxId\":8987}]}")
                //.content("{\"programCategory\":[\"POINT_LOAD\",\"RECOGNITION\"],\"receivers\":[{\"paxId\":969},
                //{\"paxId\":4103},{\"paxId\":8571},{\"paxId\":8987},{\"groupId\":132},{\"groupId\":331},
                //{\"groupId\":36}]}")
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void get_program_eligibility_language_code() throws Exception {
        MvcResult result = mockMvc.perform(
                post("/rest/participants/~/programs/query?languageCode=fr_CA")
                        .with(authToken(TestConstants.USER_PORTERGA))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\"programCategory\":[\"POINT_LOAD\",\"RECOGNITION\"],\"receivers\":[{\"paxId\":969},"
                                + "{\"paxId\":4103},{\"paxId\":8571},{\"paxId\":8987}]}")
                        //.content("{\"programCategory\":[\"POINT_LOAD\",\"RECOGNITION\"],
                        //\"receivers\":[{\"paxId\":969},{\"paxId\":4103},{\"paxId\":8571},{\"paxId\":8987},
                        //{\"groupId\":132},{\"groupId\":331},{\"groupId\":36}]}")
                )
                .andExpect(status().isOk())
                .andReturn();
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void get_all_eligible_programs_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }

    @Test
    public void get_all_eligible_programs_for_pax_language_code() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs?languageCode=fr_CA")
                        .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk())
                .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));

    }
    
    @Test
    public void get_all_eligible_programs_to_give_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs?eligibility=GIVE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
    
    @Test
    public void get_all_eligible_programs_to_receive_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs?eligibility=RECEIVE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
    
    @Test
    public void get_all_eligible_programs_specific_types_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs?programType=ECARD_ONLY,PEER_TO_PEER,MANAGER_DISCRETIONARY")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
    
    @Test
    public void get_all_eligible_programs_give_receive_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
    
    @Test
    public void get_give_recognition_eligibility_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs/eligibility?type=GIVE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
    
    @Test
    public void get_all_recognition_eligibility_for_pax() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants/~/programs/eligibility")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andReturn();
        assert(!"[]".equals(result.getResponse().getContentAsString()));
            
    }
}