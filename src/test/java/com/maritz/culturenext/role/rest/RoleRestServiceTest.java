package com.maritz.culturenext.role.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import static com.maritz.culturenext.role.rest.RolesRestService.ADMINISTRABLE_VALUE_NOT_SUPPORTED_CODE;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoleRestServiceTest  extends AbstractRestTest {
    private static final String ROLES_BASE_PATH = "/rest/culturenext-roles";

    @Test
    public void test_get_administered_roles() throws Exception  {
        MvcResult result = mockMvc.perform(
                get(ROLES_BASE_PATH +
                        "?administrable=true")
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        assertNotNull(content);
        assertFalse(content.isEmpty());
        assertTrue( content.contains("CLIENT_ADMINISTRATOR") && content.contains("CLIENT_SUPPORT"));
    }

    @Test
    public void test_get_administered_roles_withoutParameter() throws Exception  {
        MvcResult result = mockMvc.perform(
                get(ROLES_BASE_PATH )
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isBadRequest() ).andReturn();
        String response = result.getResponse().getContentAsString();
        assertTrue(response.contains(ADMINISTRABLE_VALUE_NOT_SUPPORTED_CODE));
    }

    @Test
    public void test_get_administered_roles_noadmin() throws Exception  {
        mockMvc.perform(
                get(ROLES_BASE_PATH)
                        .with(authToken(TestConstants.USER_VANLOOWA))
        ).andExpect(status().is4xxClientError());
    }
}
