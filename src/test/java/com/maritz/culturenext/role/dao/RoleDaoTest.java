package com.maritz.culturenext.role.dao;

import com.maritz.core.jpa.entity.Role;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

public class RoleDaoTest extends AbstractDatabaseTest {

    @Inject
    private RoleDao  roleDao;

    @Test
    public void test_getAdministrableRoles(){
        assertNotNull(roleDao);
        Set<RoleDTO> result = roleDao.getRoles(true);
        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void test_getAdministrableRolesIds(){
        assertNotNull(roleDao);
        Set<Long> result = roleDao.getAdministrableRoleIds();
        assertNotNull(result);
        assertEquals(2, result.size());
    }
}
