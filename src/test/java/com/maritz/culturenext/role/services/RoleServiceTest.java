package com.maritz.culturenext.role.services;

import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.role.dao.RoleDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.when;

public class RoleServiceTest {
    @Mock
    private RoleDao roleDao;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_getAdministrableRobles(){
        RoleDTO tmp = new RoleDTO().setId(54L).setCode("CLIENT_SUPPORT").setName("Client Support");
        Set<RoleDTO> tmpAdministrableRoles = new HashSet<>();
        tmpAdministrableRoles.add(tmp);
        tmp = new RoleDTO().setId(55L).setCode("CLIENT_ADMINISTRATOR").setName("Client Administrator");
        tmpAdministrableRoles.add(tmp);

        when(roleDao.getRoles(anyBoolean())).thenReturn(tmpAdministrableRoles);

        Set<RoleDTO> administrableRoles = roleDao.getRoles(true);
        assertNotNull(administrableRoles);
        assertEquals(2, administrableRoles.size());
    }

}
