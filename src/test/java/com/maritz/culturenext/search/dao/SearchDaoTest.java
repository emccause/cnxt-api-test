package com.maritz.culturenext.search.dao;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SearchDaoTest extends AbstractDatabaseTest {

    @Inject private SearchDao searchDao;

    private final Integer PAGE_NUMBER = 1;
    private final Integer PAGE_SIZE = 20;
    private final String SEARCH_STRING_1 = "adams";
    private final String SEARCH_STRING_2 = "mudd";
    private final String SEARCH_STRING_3 = "group";
    private final String STATUS_LIST = "ACTIVE,LOCKED,NEW,SUSPENDED";
    private final String PUBLIC_VISIBILITY_LIST = "PUBLIC";
    private final String GROUP_TYPE_LIST = "ENROLLMENT,ROLE_BASED,CUSTOM,PERSONAL";
    private final Integer BUDGET_ID = 5091;
    private final Integer PROGRAM_ID = null;
    private final String GROUP_DESC = "Department Code - 19932";
    private final Long GROUP_CONFIG_ID = 7L;

    //pax_admin_search values
    private final String FIRST_NAME = "Alex";
    private final String LAST_NAME = "Mudd";
    private final String CONTROL_NUMBER = "8987";
    private final String USER_NAME = "muddam";
    private final String MGR_CONTROL_NUMBER = "847";
    private final String EMAIL_ADDRESS = "a@b.c";

    private final String FIRST_NAME_1 = "Shawn";
    private final String LAST_NAME_1 = "M";
    private final String CONTROL_NUMBER_1 = null;
    private final String USER_NAME_1 = null;
    private final String MGR_CONTROL_NUMBER_1 = null;
    private final String EMAIL_ADDRESS_1 = null;


    @Test
    public void test_combinedSearch_no_matching_searchString(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, "BREAKING_EVERYTHING",
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    @Ignore
    public void test_combinedSearch_not_searchString(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, null,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(combinedSearch.size(), PAGE_SIZE.intValue());
    }

    @Test
    public void test_combinedSearch_status_list(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_1,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_single_status(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_1,
                "LOCKED", TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    @Ignore
    public void test_combinedSearch_no_status(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_1,
                null, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(combinedSearch.size(), PAGE_SIZE.intValue());
    }

    @Test
    public void test_combinedSearch_null_excludeSelf(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_2,
                STATUS_LIST, TestConstants.PAX_MUDDAM, null, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_not_excludeSelf(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_2,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.FALSE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_excludeSelf(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_2,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_public_visibility(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_no_visibility(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, null, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_invalid_visibility(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, "INVALID_VISIBILITY", GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_group_type_list(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, GROUP_TYPE_LIST, BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_single_group_type(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, "ENROLLMENT", BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_invalid_group_type(){
        assertNotNull(searchDao);
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE, SEARCH_STRING_3,
                STATUS_LIST, TestConstants.PAX_MUDDAM, Boolean.TRUE, PUBLIC_VISIBILITY_LIST, "INVALID_GROUP_TYPE", BUDGET_ID, PROGRAM_ID); //BUDGET_ID added
        assertEquals(0, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_With_Control_Num_not_Null() {
        assertNotNull(searchDao);
        final long startTime = System.nanoTime();
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE,"Sunitha" ,
                STATUS_LIST, 8987L , Boolean.FALSE, PUBLIC_VISIBILITY_LIST,
                null, null, null); //BUDGET_ID added
        final long endTime = System.nanoTime();
        logger.info("Total execution time:"+( endTime - startTime)/ 1000000);
        assertEquals( 2, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_With_Control_Num_not_Null_not_including_self() {
        assertNotNull(searchDao);
        final long startTime = System.nanoTime();
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE,"Sunitha" ,
                STATUS_LIST, 969L , Boolean.TRUE, PUBLIC_VISIBILITY_LIST,
                null, null, null); //BUDGET_ID added
        final long endTime = System.nanoTime();
        logger.info("Total execution time:"+( endTime - startTime)/ 1000000);
        assertEquals(1, combinedSearch.size());
    }

    @Test
    public void test_combinedSearch_single_group_type_with_results() {
        assertNotNull(searchDao);
        final long startTime = System.nanoTime();
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE,"INTERO" ,
                STATUS_LIST, 969L , Boolean.TRUE, PUBLIC_VISIBILITY_LIST,
                null, null, null); //BUDGET_ID added
        final long endTime = System.nanoTime();
        logger.info("Total execution time:"+( endTime - startTime)/ 1000000);
        assertEquals(5, combinedSearch.size());
    }

    @Test
    public void test_searchAllocatedQuery(){
        assertNotNull(searchDao);
        final long startTime = System.nanoTime();
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE,"Baker" ,
                STATUS_LIST, 8987L, Boolean.TRUE, PUBLIC_VISIBILITY_LIST,
                null, 1151, null); //BUDGET_ID added
        final long endTime = System.nanoTime();
        logger.info("Total execution time:"+( endTime - startTime)/ 1000000);
        assertEquals(1, combinedSearch.size());
    }

    @Test
    public void test_searchAllocatedQueryWithProgram(){
        assertNotNull(searchDao);
        final long startTime = System.nanoTime();
        List<Map<String, Object>> combinedSearch = searchDao.combinedSearch(PAGE_NUMBER, PAGE_SIZE,"Shawn" ,
                STATUS_LIST, 5997L, Boolean.FALSE, PUBLIC_VISIBILITY_LIST,
                null, null, 760); //BUDGET_ID added
        final long endTime = System.nanoTime();
        logger.info("Total execution time:"+( endTime - startTime)/ 1000000);
        assertEquals(3, combinedSearch.size());
    }

    @Test
    public void test_participant_search_all_params(){
        assertNotNull(searchDao);
        List<Map<String, Object>> participantSearch = searchDao.participantSearch(PAGE_NUMBER, PAGE_SIZE, FIRST_NAME, LAST_NAME,
                CONTROL_NUMBER, USER_NAME, MGR_CONTROL_NUMBER, EMAIL_ADDRESS);
        assertEquals(1, participantSearch.size());
    }

    @Test
    public void test_participant_search_firstName_lastInitial_param(){
        assertNotNull(searchDao);
        List<Map<String, Object>> participantSearch = searchDao.participantSearch(PAGE_NUMBER, PAGE_SIZE, FIRST_NAME_1, LAST_NAME_1,
                CONTROL_NUMBER_1, USER_NAME_1, MGR_CONTROL_NUMBER_1, EMAIL_ADDRESS_1);
        assertEquals(2, participantSearch.size());
    }

    @Test
    public void test_group_search_all_params(){
        assertNotNull(searchDao);
        List<Map<String, Object>> groupSearch = searchDao.groupSearch(PAGE_NUMBER, PAGE_SIZE, GROUP_DESC, GROUP_CONFIG_ID);
        assertEquals(1, groupSearch.size());
    }
}