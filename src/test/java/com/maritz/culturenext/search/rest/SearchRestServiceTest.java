package com.maritz.culturenext.search.rest;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.junit.Test;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

public class SearchRestServiceTest extends AbstractRestTest {
    
    // PAX SEARCH

    @Test
    public void getMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getMatchingSearchWithParams() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&page[number]=1&page[size]=20")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getMatchingSearchWithParamsTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&page[number]=100&page[size]=20")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void getNonMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=BREAKING_EVERYTHING")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk()).andExpect(jsonPath("$['data']", Matchers.empty()));
    }

    @Test
    public void getNoSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/participants?page[number]=1&page[size]=10")
                        .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getMatchingSearchWithSingleStatusParamGood() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=porter&status=ACTIVE")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }

    @Test
    public void getMatchingSearchWithListStatusParamGood() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=porter&status=ACTIVE,LOCKED")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }

    @Test
    public void getNonMatchingSearchWithStatusParamBad() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&status=ALEX")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.empty()));
    }
    
    @Test
    public void getMatchingSearchWithSysUserId() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=mudd")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(jsonPath("$['data'][0]['sysUserId']", Matchers.notNullValue()));
    }
    
    //programId test cases
    
    @Test
    public void getMatchingSearchWithParamsAndProg() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=baker&page[number]=1&page[size]=30&programid=3200")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getNonMatchingSearchWithParamsAndProgTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=garcia&page[number]=100&page[size]=20&programid=1")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void getMatchingSearchWithSingleStatusParamAndProgGood() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=baker&status=ACTIVE&programid=3200")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }

    @Test
    public void getNonMatchingSearchWithStatusParamBadAndProg() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&status=ALEX&programid=1")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(jsonPath("$['data']", Matchers.empty()));
    }
    
    @Test
    public void getMatchingSearchWithExcludeSelfFalse() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&excludeSelf=false")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getMatchingSearchWithExcludeSelfTrue() throws Exception {
         mockMvc.perform(
                get("/rest/participants?searchStr=harwell&excludeSelf=true")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
             .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getControlNumNoMatch() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=T1023")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.empty()));
    }
    
    @Test
    public void getControlNumMatch() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=T10234")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    // GROUP SEARCH
    
    @Test
    public void getGroupListWithPagination() throws Exception {
        mockMvc.perform(
                get("/rest/groups?searchStr=Comp&page=1&size=3&groupConfigId=6")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk());
    }
    
    @Test
    public void getGroupListWithOutPagination() throws Exception {
        mockMvc.perform(
                get("/rest/groups?searchStr=Comp&groupConfigId=6")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk());
    }

    // COMBINED SEARCH

    @Test
    public void getCombinedMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=adm")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getCombinedMatchingSearchWithParams() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=adm&page[number]=1&page[size]=20")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getCombinedMatchingSearchWithParamsTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=adm&page[number]=100&page[size]=20")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void getCombinedNonMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=BREAKING_EVERYTHING")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.empty()));
    }

    @Test
    public void getCombinedNoSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/search?page[number]=1&page[size]=10")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void getCombinedWithoutExcludeSelfParam() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=Mudd")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getCombinedNotExcludeSelf() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=Mudd&excludeSelf=false")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getCombinedExcludeSelf() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=Mudd&excludeSelf=true")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.empty()));
    }
    
    @Test
    public void getCombinedShortSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=M&page[number]=1&page[size]=10")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void getCombinedControlNumNoMatch() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=T1023")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.empty()));
    }
    
    @Test
    public void getCombinedControlNumMatch() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=T10234")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$['data']", Matchers.not(Matchers.empty())));
    }
    
    @Test
    public void getCombinedMatchingSearchWithVisibility() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=group&visibility=PUBLIC")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data'].length()", Matchers.is(15)));
    }
    
    @Test
    public void getCombinedMatchingSearchInvalidVisibility() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=group&visibility=INVALID_VISIBILITY")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data'].length()", Matchers.is(1)));
    }
    
    @Test
    public void getCombinedMatchingSearchWithoutVisibility() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=group")
                .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk())
            .andExpect(jsonPath("$['data'].length()", Matchers.is(17)));
    }


    @Test
    public void get_first_last_name_only() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?firstName=b&lastName=t&page[number]=1&page[size]=10")
                        .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                        .with(authToken(TestConstants.USER_WRIGHTMA))
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$['data'].length()", Matchers.is(10)))
        .andReturn();
        String content = result.getResponse().getContentAsString();
        System.out.println(content);
        assertTrue(content.contains("\"username\":\"tannehbe\""));
    }

    @Test
    public void get_no_search_params() throws Exception {
        mockMvc.perform(
                get("/rest/participants")
                        .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                        .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
        )
                .andExpect(status().is4xxClientError());
    }

}
