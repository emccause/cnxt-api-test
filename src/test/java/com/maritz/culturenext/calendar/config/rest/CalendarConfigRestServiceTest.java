package com.maritz.culturenext.calendar.config.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CalendarConfigRestServiceTest extends AbstractRestTest {

    private final String BASE_URL = "/rest/calendar-config";

    @Test
    public void get_calendar_event_types_config_happy () throws Exception {

        mockMvc.perform(
                get(BASE_URL)
                        .with(authToken(TestConstants.USER_DAGARFIN))
        ).andExpect(status().isOk());

    }

    @Test
    public void set_calendar_event_types_config_happy () throws Exception {

        mockMvc.perform(
                put(BASE_URL)
                        .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"eventTypes\": [\"SERVICE_ANNIVERSARY\", \"BIRTHDAY\"] \n" +
                                "}")
        ).andExpect(status().isOk());
    }

    @Test
     public void set_calendar_event_type_config_forbidden () throws Exception {

        mockMvc.perform(
                put(BASE_URL)
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"eventTypes\": [\"SERVICE_ANNIVERSARY\", \"BIRTHDAY\"] \n" +
                                "}")
        ).andExpect(status().isForbidden());
    }

    @Test
    public void set_calendar_event_type_config_bad_request () throws Exception {

        mockMvc.perform(
                put(BASE_URL)
                        .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"eventTypes\": [\"INVALID_TYPE\", \"BIRTHDAY\"] \n" +
                                "}")
        ).andExpect(status().isBadRequest());
    }


}
