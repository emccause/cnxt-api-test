package com.maritz.culturenext.calendar.config.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.CalendarSubType;
import com.maritz.core.jpa.entity.CalendarType;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.calendar.config.dto.CalendarConfigDTO;
import com.maritz.culturenext.calendar.config.services.impl.CalendarConfigServiceImpl;
import com.maritz.culturenext.calendar.events.services.CalendarEventService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.CalendarEventType;
import com.maritz.test.AbstractMockTest;

public class CalendarConfigServiceTest extends AbstractMockTest {

    @InjectMocks private CalendarConfigServiceImpl calendarConfigService;
    @Mock private CalendarEventService calendarEventService;
    @Mock private ConcentrixDao<CalendarType> calendarTypeDao;
    @Mock private ConcentrixDao<CalendarSubType> calendarSubTypeDao;
    @Mock private Security security;

    private FindBy<CalendarSubType> calendarSubTypeFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<CalendarType> calendarTypeFindBy = PowerMockito.mock(FindBy.class);


    @Before
    public void setup () {
        List<CalendarSubType> calendarSubTypeList = new ArrayList<>();
        CalendarSubType birthdayType = new CalendarSubType();
        birthdayType.setId(TestConstants.ID_1);
        birthdayType.setCalendarSubTypeName("Birthday");
        birthdayType.setCalendarSubTypeCode(CalendarEventType.BIRTHDAY.toString());
        birthdayType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        birthdayType.setCalendarTypeId(4l);
        calendarSubTypeList.add(birthdayType);
        CalendarSubType anniversaryType = new CalendarSubType();
        anniversaryType.setId(TestConstants.ID_2);
        anniversaryType.setCalendarSubTypeName("Service Anniversary");
        anniversaryType.setCalendarSubTypeCode(CalendarEventType.SERVICE_ANNIVERSARY.toString());
        anniversaryType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        anniversaryType.setCalendarTypeId(4l);
        calendarSubTypeList.add(anniversaryType);


        PowerMockito.when(calendarSubTypeDao.findBy()).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.where(ProjectConstants.CALENDAR_SUB_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.in(any(List.class))).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.and(ProjectConstants.DISPLAY_STATUS_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.find()).thenReturn(calendarSubTypeList);

        CalendarType holidayType = new CalendarType();
        holidayType.setId(TestConstants.ID_1);
        holidayType.setCalendarTypeCode(CalendarEventType.HOLIDAY.toString());
        holidayType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());

        PowerMockito.when(calendarTypeDao.findBy()).thenReturn(calendarTypeFindBy);
        PowerMockito.when(calendarTypeFindBy.where(ProjectConstants.CALENDAR_TYPE_CODE)).thenReturn(calendarTypeFindBy);
        PowerMockito.when(calendarTypeFindBy.eq(CalendarEventType.HOLIDAY.toString())).thenReturn(calendarTypeFindBy);
        PowerMockito.when(calendarTypeFindBy.findOne()).thenReturn(holidayType);

        PowerMockito.when(calendarSubTypeDao.findBy()).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.where(ProjectConstants.CALENDAR_SUB_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.in(any(List.class))).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.or(ProjectConstants.CALENDAR_TYPE_ID))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.eq(TestConstants.ID_1)).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.find()).thenReturn(calendarSubTypeList);

    }


    @Test
    public void test_happy_path_get_calendar_event_configs () {

        CalendarConfigDTO response = calendarConfigService.getConfigCalendarEventTypes();

        assertTrue(response != null);
        assertTrue(response.getEventTypes().size() == 3);

    }

    @Test
    public void test_happy_path_set_calendar_event_configs () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        CalendarConfigDTO request = new CalendarConfigDTO();
        List<String> types = new ArrayList<>();
        types.add(CalendarEventType.BIRTHDAY.toString());
        types.add(CalendarEventType.SERVICE_ANNIVERSARY.toString());
        types.add(CalendarEventType.HOLIDAY.toString());
        request.setEventTypes(types);

        CalendarConfigDTO response = calendarConfigService.setConfigCalendarEventTypes(request);

        assertTrue(response != null);
        assertTrue(response.getEventTypes().size() == 3);

    }

    @Test
     public void test_throw_expception_when_invalid_type_set_event_configs () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        CalendarConfigDTO response = new CalendarConfigDTO();

        CalendarConfigDTO request = new CalendarConfigDTO();
        List<String> types = new ArrayList<>();
        types.add("Some Invalid type");
        types.add(CalendarEventType.SERVICE_ANNIVERSARY.toString());
        request.setEventTypes(types);

        try {
            response = calendarConfigService.setConfigCalendarEventTypes(request);

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(response.getEventTypes() == null);

    }

    @Test
    public void test_throw_expception_when_no_type_set_event_configs () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        CalendarConfigDTO response = new CalendarConfigDTO();

        CalendarConfigDTO request = new CalendarConfigDTO();

        try {
            response = calendarConfigService.setConfigCalendarEventTypes(request);

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(response.getEventTypes() == null);

    }

    @Test
    public void test_when_empty_type_set_event_type_configs () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        CalendarConfigDTO response = new CalendarConfigDTO();

        CalendarConfigDTO request = new CalendarConfigDTO();
        List<String> configs = new ArrayList<>();
        request.setEventTypes(configs);

        try {
            response = calendarConfigService.setConfigCalendarEventTypes(request);

        } catch (ErrorMessageException e) {
            fail();
        }

        assertTrue(response.getEventTypes() != null);

    }

}
