package com.maritz.culturenext.calendar.events.dao;

import com.maritz.core.jpa.entity.CalendarEntry;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.CalendarEntryRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.util.DateUtils;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AudienceEnum;
import com.maritz.culturenext.enums.CalendarSubTypeCode;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CalendarEventsDaoTest extends AbstractDatabaseTest {
    @Inject
    CalendarEntryRepository calendarEntryRepository;

    @Inject
    PaxMiscRepository paxMiscRepository;

    @Inject
    CalendarEventsDao calendarEventsDao;

    @Inject
    SysUserRepository sysUserRepository;

    Date startDate = DateUtil.convertFromString("2015-01-01");
    Date endDate = DateUtil.convertFromString("9999-01-01");

    List<String> calendarSubTypes = Arrays.asList(CalendarSubTypeCode.BIRTHDAY.name(),
            CalendarSubTypeCode.SERVICE_ANNIVERSARY.name());

    @Test
    public void test_get_active_calendar_events_first_years_service_anniversary() throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.YEAR, -1);
        Date hireDate = calendar.getTime();
        String hireDateString = DateUtils.formatDate(hireDate);

        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_MUDDAM);
        PaxMisc paxMisc = new PaxMisc();
        paxMisc.setVfName(VfName.HIRE_DATE.name());
        paxMisc.setMiscData(hireDateString);
        paxMisc.setPaxId(sysUser.getPaxId());
        paxMiscRepository.save(paxMisc);

        List<CalendarEventDTO> calendarEvents = calendarEventsDao.getActiveCalendarEvents(sysUser.getPaxId(),
                DateUtils.parse("2000-01-01", "yyyy-MM-dd"),
                new Date(), Arrays.asList(CalendarSubTypeCode.SERVICE_ANNIVERSARY.name()),
                AudienceEnum.ALL.name(), 1, 10);
        CalendarEntry calendarEntry =
                calendarEntryRepository.findOneRsql("calendarEntryName==\"Alex Mudd's Service Anniversary\"");

        assertThat(calendarEvents.size(), is(1));

        CalendarEventDTO calendarEvent = calendarEvents.get(0);

        assertThat(calendarEvent.getId(), is(calendarEntry.getId()));
        assertThat(calendarEvent.getName(), is(calendarEntry.getCalendarEntryName()));
        assertThat(calendarEvent.getDescription(), is(calendarEntry.getCalendarEntryDesc()));
        assertThat(calendarEvent.getStartDate(), is(DateUtil.convertToUTCDate(calendarEntry.getFromDate())));
        assertThat(calendarEvent.getEndDate(), is(DateUtil.convertToUTCDate(calendarEntry.getThruDate())));
        assertThat(calendarEvent.getTargetId(), is(calendarEntry.getTargetId()));
        assertThat(calendarEvent.getType(), is(CalendarSubTypeCode.SERVICE_ANNIVERSARY.name()));
    }

    @Test
    public void test_get_calendar_events_audience_filter_ALL() throws Exception {
        
        List<String> emptyList = new ArrayList<>();
        
        List<CalendarEventDTO> nodes = calendarEventsDao.getActiveCalendarEvents(
                TestConstants.PAX_HAGOPIWL, startDate, endDate,
                emptyList, AudienceEnum.ALL.name(), 1, 500);

        assertEquals(nodes.size(), 118); 
    }
    
    @Test
    public void test_get_calendar_events_audience_filter_NETWORK() throws Exception {
        
        List<CalendarEventDTO> nodes = calendarEventsDao.getActiveCalendarEvents(
                TestConstants.PAX_HAGOPIWL, startDate, endDate, calendarSubTypes,
                AudienceEnum.NETWORK.name(), 1, 10);

        assertThat(nodes.size(), is(2)); 
    }
    
    @Test
    public void test_get_calendar_events_audience_filter_ME() throws Exception {
        
        List<CalendarEventDTO> nodes = calendarEventsDao.getActiveCalendarEvents(
                TestConstants.PAX_HAGOPIWL, startDate, endDate, calendarSubTypes,
                AudienceEnum.ME.name(), 1, 10);

        assertThat(nodes.size(), is(1)); 
    }
    
}