package com.maritz.culturenext.calendar.events.services;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.CalendarEntry;
import com.maritz.core.jpa.entity.CalendarSubType;
import com.maritz.core.jpa.entity.CalendarType;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.calendar.constants.CalendarConstants;
import com.maritz.culturenext.calendar.events.dao.CalendarEventsDao;
import com.maritz.culturenext.calendar.events.dto.CalendarEventDTO;
import com.maritz.culturenext.calendar.events.services.impl.CalendarEventServiceImpl;
import com.maritz.culturenext.calendar.util.CalendarTranslationUtil;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.AudienceEnum;
import com.maritz.culturenext.enums.CalendarSubTypeCode;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.entity.VwUserGroupsSearchData;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PaxUtil;
import com.maritz.culturenext.util.PermissionUtil;
import com.maritz.test.AbstractMockTest;

public class CalendarEventServiceTest extends AbstractMockTest {

    @InjectMocks private CalendarEventServiceImpl calendarEventService;
    @InjectMocks private PermissionUtil permissionUtil;
    @Mock private CalendarEventsDao calendarEventsDao;
    @Mock private CalendarTranslationUtil calendarTranslationUtil;
    @Mock private ConcentrixDao<Acl> aclDao;
    @Mock private ConcentrixDao<CalendarEntry> calendarEntryDao;
    @Mock private ConcentrixDao<CalendarSubType> calendarSubTypeDao;
    @Mock private ConcentrixDao<CalendarType> calendarTypeDao;
    @Mock private ConcentrixDao<Groups> groupsDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<Role> roleDao;
    @Mock private ConcentrixDao<VwUserGroupsSearchData> vwUserGroupsSearchDataDao;
    @Mock private Environment environment;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxUtil paxUtil;
    @Mock private Security security;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private final String NAME = "Event Name";
    private final String START_DATE = "2015-01-01";
    private final String END_DATE = "2015-01-03";
    private final String SOME_INVALID_DATE = "20-12-2013";
    private final String SOME_INVALID_CALENDAR_DATE = "2015-15-01";
    private final String DESCRIPTION = "Some event description";
    private final String TYPE = "CUSTOM";
    private final Long PAX_ID = 789l;
    private final Long GROUP_ID = 123l;
    private final List<GroupMemberStubDTO> AUDIENCE = new ArrayList<>();

    private FindBy<VwUserGroupsSearchData> vwUserGroupsSearchDataFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<CalendarSubType> calendarSubTypeFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Role> roleFindBy = PowerMockito.mock(FindBy.class);

    @Before
    public void setup () {
        GroupMemberStubDTO audience1 = new GroupMemberStubDTO();
        GroupMemberStubDTO audience2 = new GroupMemberStubDTO();

        audience1.setPaxId(PAX_ID);
        audience2.setGroupId(GROUP_ID);
        AUDIENCE.add(audience1);
        AUDIENCE.add(audience2);

        List<VwUserGroupsSearchData> paxList = new ArrayList<>();
        VwUserGroupsSearchData pax = new VwUserGroupsSearchData();
        pax.setPaxId(PAX_ID);
        pax.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        paxList.add(pax);

        List<VwUserGroupsSearchData> groupsList = new ArrayList<>();
        VwUserGroupsSearchData groups = new VwUserGroupsSearchData();
        groups.setGroupId(GROUP_ID);
        groups.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        groupsList.add(groups);

        List<Long> validPax = new ArrayList<>();
        validPax.add(PAX_ID);

        List<Long> validGroup = new ArrayList<>();
        validGroup.add(GROUP_ID);

        List<CalendarSubType> calendarSubTypeList = new ArrayList<>();
        CalendarSubType birthdayType = new CalendarSubType();
        birthdayType.setId(TestConstants.ID_2);
        birthdayType.setCalendarSubTypeName("Birthday");
        birthdayType.setCalendarSubTypeCode("BIRTHDAY");
        birthdayType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        calendarSubTypeList.add(birthdayType);
        CalendarSubType customType = new CalendarSubType();
        customType.setId(TestConstants.ID_1);
        customType.setCalendarSubTypeName("Custom");
        customType.setCalendarSubTypeCode("CUSTOM");
        customType.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        calendarSubTypeList.add(customType);
        CalendarSubType anniversaryType = new CalendarSubType();
        anniversaryType.setId(3l);
        anniversaryType.setCalendarSubTypeName("Service Anniversary");
        anniversaryType.setCalendarSubTypeCode("SERVICE_ANNIVERSARY");
        anniversaryType.setDisplayStatusTypeCode("ACTIVE");
        calendarSubTypeList.add(anniversaryType);

        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setPaxId(PAX_ID);
        employeeDTO.setFirstName("First");
        employeeDTO.setLastName("Last");

        Map<Long, EmployeeDTO> employeeDTOMap = new HashMap<>();
        employeeDTOMap.put(PAX_ID, employeeDTO);

        PowerMockito.when(paxUtil.getPaxIdEmployeeDTOMap(any(List.class))).thenReturn(employeeDTOMap);

        PowerMockito.when(participantInfoDao.getEmployeeDTO(PAX_ID)).thenReturn(new EmployeeDTO().setPaxId(PAX_ID));

        PowerMockito.when(vwUserGroupsSearchDataDao.findBy()).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.where(any(String.class)))
                .thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.in(any(List.class))).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.and(ProjectConstants.STATUS_TYPE_CODE))
                .thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.find()).thenReturn(paxList, groupsList);

        PowerMockito.when(roleDao.findBy()).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.where(anyString())).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.eq(anyString())).thenReturn(roleFindBy);
        PowerMockito.when(roleFindBy.findOne()).thenReturn(new Role());

        PowerMockito.when(calendarSubTypeDao.findBy()).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.where(ProjectConstants.CALENDAR_SUB_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.eq(anyString())).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.findOne()).thenReturn(new CalendarSubType());

        PowerMockito.when(calendarSubTypeDao.findBy()).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.find()).thenReturn(calendarSubTypeList);

        PowerMockito.when(calendarSubTypeDao.findBy()).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.where(ProjectConstants.CALENDAR_SUB_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.in(any(List.class))).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.and(ProjectConstants.DISPLAY_STATUS_TYPE_CODE))
                .thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(calendarSubTypeFindBy);
        PowerMockito.when(calendarSubTypeFindBy.find()).thenReturn(calendarSubTypeList);

        List<CalendarEventDTO> calendarEventDaoResult = activeCalendarEventsDaoTestResult();
        List<CalendarEventDTO> translateCalendarEntryList = translateCalendarEntryNamesTestResult(calendarEventDaoResult);

        PowerMockito.when(calendarEventsDao.getActiveCalendarEvents(anyLong(), any(Date.class), any(Date.class)
                , anyListOf(String.class), anyString(), anyInt(), anyInt())).thenReturn(calendarEventDaoResult);

        PowerMockito.when(calendarTranslationUtil.translateCalendarEntryNames(anyListOf(CalendarEventDTO.class),
                anyString())).thenReturn(translateCalendarEntryList);


        PowerMockito.doNothing().when(aclDao).save(any(Acl.class));
        PowerMockito.doNothing().when(calendarEntryDao).save(any(CalendarEntry.class));

        PowerMockito.when(security.hasRole(anyString(), anyString())).thenReturn(true);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        PowerMockito.when(vwUserGroupsSearchDataDao.findBy()).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.where(any(String.class)))
                .thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.in(any(List.class))).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.and(ProjectConstants.STATUS_TYPE_CODE))
                .thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.ne(StatusTypeCode.INACTIVE.name())).thenReturn(vwUserGroupsSearchDataFindBy);
        PowerMockito.when(vwUserGroupsSearchDataFindBy.find()).thenReturn(paxList, groupsList);
    }

    @Test
    public void test_exception_thrown_when_missing_name () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);


    }

    @Test
    public void test_exception_thrown_when_missing_startDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);
    }

    @Test
    public void test_when_missing_endDate_should_save_as_startDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);

        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            fail();
        }

        assertTrue(calendarEventResponse.getEndDate().equals(calendarEventResponse.getStartDate()));

    }

    @Test
    public void test_exception_thrown_when_missing_audience () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_exception_thrown_when_invalid_startDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(SOME_INVALID_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_exception_thrown_when_invalid_calendar_startDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(SOME_INVALID_CALENDAR_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_exception_thrown_when_invalid_endDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(SOME_INVALID_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }


    public void test_exception_thrown_when_invalid_calendar_endDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(SOME_INVALID_CALENDAR_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_exception_thrown_when_invalid_audience () {

        List<GroupMemberStubDTO> invalidAudience = new ArrayList<>();

        GroupMemberStubDTO audience = new GroupMemberStubDTO();

        audience.setPaxId(4687l);
        invalidAudience.add(audience);


        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(invalidAudience);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_exception_thrown_when_startDate_after_endDate () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(END_DATE);
        calendarEventRequest.setEndDate(START_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_create_calendar_event_and_acl() {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setAudience(AUDIENCE);

        calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);

        Mockito.verify(calendarEntryDao, times(1)).save(Matchers.any(CalendarEntry.class));
        Mockito.verify(aclDao, times(2)).save(Matchers.any(Acl.class));

        assertThat(calendarEventResponse.getName(), is(NAME));
        assertThat(calendarEventResponse.getStartDate(), is(START_DATE));
        assertThat(calendarEventResponse.getEndDate(), is(END_DATE));
        assertThat(calendarEventResponse.getAudience(), is(AUDIENCE));
        assertThat(calendarEventResponse.getDescription(), is(DESCRIPTION));

    }

    @Test
    public void test_fetch_calendar_events_for_admin_happy_path () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE
                        , null, null, null, null, null);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());
    }

    @Test
    public void test_fetch_calendar_events_with_just_startDate_happy_path () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID,START_DATE
                        , null, null, null, null, null, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());
    }

    @Test
    public void test_fetch_calendar_events_for_admin_happy_path_paginated () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID,START_DATE, END_DATE
                        , null, null, null, null, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());

    }

    @Test
    public void test_fetch_calendar_events_error_pagination_size_exceeds_max () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        int size = CalendarConstants.MAX_PAGINATION_SIZE + 1;

        try {
            calendarEventService.getCalendarEventsForParticipant(PAX_ID,START_DATE, END_DATE, null, null
                    , null, null, size);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

    }

    @Test
    public void test_fetch_calendar_event_for_logged_in_pax_happy_path () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.isMyPax(PAX_ID)).thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID,START_DATE, END_DATE
                        , null, null, null, null, null);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        for (CalendarEventDTO result : response) {
            if (CalendarSubTypeCode.CUSTOM.toString().equals(result.getType())) {
                assertNull(result.getPax());
            } else {
                assertNotNull(result.getPax());
            }
        }
    }

    @Test
    public void test_fetch_calendar_event_for_logged_in_pax_happy_path_paginated () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.isMyPax(PAX_ID)).thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE
                        , null, null, null, null, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());
    }

    @Test
    public void test_throw_exception_fetch_calendar_event_invalid_startDate () {

        List<CalendarEventDTO> response = new ArrayList<>();

        try {
            PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                    calendarEventService.getCalendarEventsForParticipant(PAX_ID, SOME_INVALID_DATE, END_DATE
                            , null, null, null, null, null);

            response = paginatedResponse.getData();

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(response.isEmpty());
    }

    @Test
    public void test_throw_exception_fetch_calendar_event_invalid_endDate () {

        List<CalendarEventDTO> response = new ArrayList<>();

        try {
            PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                    calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, SOME_INVALID_DATE
                            , null, null, null, null, null);

            response = paginatedResponse.getData();

        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(response.isEmpty());
    }

    @Test
    public void test_throw_exception_create_calendar_event_no_groupId_paxId () {

        CalendarEventDTO calendarEventRequest = new CalendarEventDTO();
        CalendarEventDTO calendarEventResponse = new CalendarEventDTO();

        calendarEventRequest.setName(NAME);
        calendarEventRequest.setStartDate(START_DATE);
        calendarEventRequest.setEndDate(END_DATE);
        calendarEventRequest.setDescription(DESCRIPTION);
        calendarEventRequest.setType(TYPE);

        List<GroupMemberStubDTO> groupMemberStubDTOList = new ArrayList<>();
        GroupMemberStubDTO audience = new GroupMemberStubDTO();
        groupMemberStubDTOList.add(audience);

        calendarEventRequest.setAudience(groupMemberStubDTOList);

        try {
            calendarEventResponse = calendarEventService.createCalendarEvent(calendarEventRequest);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        assertTrue(calendarEventResponse.getName() == null);

    }

    @Test
    public void test_fetch_translated_calendar_events_as_admin () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE,
                        LocaleCodeEnum.fr_CA.name(), null, null, 1, 5);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        for (CalendarEventDTO ce : response) {
            assertTrue(ce.getName().contains(LocaleCodeEnum.fr_CA.name()));
        }
    }

    @Test
    public void test_fetch_translated_calendar_events_non_admin () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.isMyPax(PAX_ID)).thenReturn(true);

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE,LocaleCodeEnum.fr_CA.name()
                        , null, null, 1, 5);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        for (CalendarEventDTO ce : response) {
            assertTrue(ce.getName().contains(LocaleCodeEnum.fr_CA.name()));
        }
    }

    @Test
    public void test_fetch_translated_calendar_events_properly_sorted () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.isMyPax(PAX_ID)).thenReturn(true);


        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE, LocaleCodeEnum.fr_CA.name()
                        , null, null, 1, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        for (CalendarEventDTO ce : response) {
            assertTrue(ce.getName().contains(LocaleCodeEnum.fr_CA.name()));
        }

        Date first = new Date(response.get(0).getStartDate());
        Date last = new Date(response.get(2).getStartDate());

        assertTrue(first.before(last));
    }

    @Test
    public void test_verifyCalendarEnabled_calendarNotEnabled(){
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

//        PowerMockito.when(permissionUtil.hasPermission(PermissionConstants.CALENDAR_TYPE)).thenReturn(false);
        assertFalse(permissionUtil.hasPermission(PermissionConstants.CALENDAR_TYPE));
    }

    @Test
    public void test_fetch_translated_calendar_events_filteredBy_audience_NETWORK () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.isMyPax(PAX_ID)).thenReturn(true);


        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
                calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE, LocaleCodeEnum.fr_CA.name()
                        , AudienceEnum.NETWORK.name(), null, 1, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();

        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());

    }

    @Test
    public void test_fetch_translated_calendar_events_filteredBy_invalid_audience () {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CalendarConstants.ERROR_INVALID_AUDIENCE_CODE));

        calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE, LocaleCodeEnum.fr_CA.name()
                , "INVALID_AUDIENCE", null, 1, 3);
    }

    @Test
    public void test_fetch_translated_calendar_events_filteredBy_calendarEventTypes () {

        PaginatedResponseObject<CalendarEventDTO> paginatedResponse =
        calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE, LocaleCodeEnum.fr_CA.name()
                , null, "CUSTOM,BIRTHDAY,SERVICE_ANNIVERSARY", 1, 3);

        List<CalendarEventDTO> response = paginatedResponse.getData();
        assertTrue(response.size() == 3);
        assertNull(response.get(0).getPax());
        assertNotNull(response.get(1).getPax());
        assertNotNull(response.get(2).getPax());
    }

    @Test
    public void test_fetch_translated_calendar_events_filteredBy_invalid_calendarEventTypes () {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CalendarConstants.ERROR_INVALID_EVENT_TYPE_CODE));

        calendarEventService.getCalendarEventsForParticipant(PAX_ID, START_DATE, END_DATE, LocaleCodeEnum.fr_CA.name()
                , null, "INVALID_EVENT_TYPE", 1, 3);
    }



    private Date addDays(Date date, int days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        return calendar.getTime();
    }

    private List<CalendarEventDTO> activeCalendarEventsDaoTestResult() {
        List<CalendarEventDTO> result = new ArrayList<>();

        CalendarEventDTO calendarEventDTO = new CalendarEventDTO();
        calendarEventDTO.setId(TestConstants.ID_1);
        calendarEventDTO.setName("Custom");
        calendarEventDTO.setDescription("Some description");
        calendarEventDTO.setType(CalendarSubTypeCode.CUSTOM.name());
        calendarEventDTO.setStartDate(new Date().toString());
        calendarEventDTO.setEndDate(addDays(new Date(), 1).toString());
        calendarEventDTO.setTotalResults(3);
        result.add(calendarEventDTO);

        calendarEventDTO = new CalendarEventDTO();
        calendarEventDTO.setId(TestConstants.ID_2);
        calendarEventDTO.setName("Birthday");
        calendarEventDTO.setDescription("Some description");
        calendarEventDTO.setType(CalendarSubTypeCode.BIRTHDAY.name());
        calendarEventDTO.setStartDate( addDays(new Date(), 1).toString());
        calendarEventDTO.setEndDate ( addDays(new Date(), 2).toString());
        calendarEventDTO.setTotalResults(3);
        calendarEventDTO.setTargetId(PAX_ID);
        calendarEventDTO.setPax(new EmployeeDTO().setPaxId(calendarEventDTO.getTargetId()));
        calendarEventDTO.setTargetTable(TableName.PAX.name());
        result.add(calendarEventDTO);

        calendarEventDTO = new CalendarEventDTO();
        calendarEventDTO.setId(3L);
        calendarEventDTO.setName( "Anniversary");
        calendarEventDTO.setDescription( "Some description");
        calendarEventDTO.setType(CalendarSubTypeCode.SERVICE_ANNIVERSARY.name());
        calendarEventDTO.setStartDate( addDays(new Date(), 2).toString());
        calendarEventDTO.setEndDate ( addDays(new Date(), 3).toString());
        calendarEventDTO.setTargetId( PAX_ID);
        calendarEventDTO.setPax(new EmployeeDTO().setPaxId(calendarEventDTO.getTargetId()));
        calendarEventDTO.setTargetTable( TableName.PAX.name());
        calendarEventDTO.setTotalResults(3);
        result.add(calendarEventDTO);

        return result;
    }

    private List<CalendarEventDTO> translateCalendarEntryNamesTestResult(List<CalendarEventDTO> queryResult) {
        List<CalendarEventDTO> translateResult = new ArrayList<>();
        CalendarEventDTO event = null;
        for (CalendarEventDTO row: queryResult){
            event = new CalendarEventDTO();
            event.setId(row.getId());
            event.setName(LocaleCodeEnum.fr_CA.name() + row.getName());
            event.setDescription(row.getDescription());
            event.setType(row.getType());
            event.setStartDate(row.getStartDate());
            event.setEndDate(row.getEndDate());
            event.setTotalResults( row.getTotalResults());
            if (row.getTargetId() != null) {
                event.setPax(new EmployeeDTO().setPaxId(row.getTargetId()));
            }
            translateResult.add(event);
        }
        return translateResult;
    }
}
