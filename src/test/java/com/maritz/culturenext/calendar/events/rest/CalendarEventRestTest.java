package com.maritz.culturenext.calendar.events.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CalendarEventRestTest extends AbstractRestTest{

    public static final String CREATE_URI = "/rest/calendar-events";

    private static long testPaxId = 12698;
    public static final String GET_URI = "/rest/participants/" + Long.toString(testPaxId) + "/calendar-events";

    @Test
    public void create_calendar_event_params_good_and_happy() throws Exception {
        mockMvc.perform(
                post(CREATE_URI)
                        .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"name\": \"Custom Event Dan Test\",\n" +
                                "    \"startDate\": \"2015-10-25\",\n" +
                                "    \"endDate\": \"2015-10-25\",\n" +
                                "    \"description\": \"Custom event description Dan test\",\n" +
                                "    \"audience\":[\n" +
                                "        {\n" +
                                "            \"paxId\": 12698\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"groupId\": 141\n" +
                                "        }\n" +
                                " \n" +
                                "    ]\n" +
                                "}")
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void create_calendar_event_params_with_permission() throws Exception {

        mockMvc.perform(
                post(CREATE_URI)
                        .with(authToken(TestConstants.USER_HARWELLM))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"name\": \"Custom Event Dan Test\",\n" +
                                "    \"startDate\": \"2015-10-25\",\n" +
                                "    \"endDate\": \"2015-10-25\",\n" +
                                "    \"description\": \"Custom event description Dan test\",\n" +
                                "    \"audience\":[\n" +
                                "        {\n" +
                                "            \"paxId\": 12698\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"groupId\": 141\n" +
                                "        }\n" +
                                " \n" +
                                "    ]\n" +
                                "}")
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void create_calendar_event_params_with_no_permission() throws Exception {

        mockMvc.perform(
                post(CREATE_URI)
                        .with(authToken(TestConstants.USER_HAGOPIWL))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\n" +
                                "    \"name\": \"Custom Event Dan Test\",\n" +
                                "    \"startDate\": \"2015-10-25\",\n" +
                                "    \"endDate\": \"2015-10-25\",\n" +
                                "    \"description\": \"Custom event description Dan test\",\n" +
                                "    \"audience\":[\n" +
                                "        {\n" +
                                "            \"paxId\": 12698\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"groupId\": 141\n" +
                                "        }\n" +
                                " \n" +
                                "    ]\n" +
                                "}")
        )
                .andExpect(status().isForbidden())
        ;
    }

    @Test
    public void get_calendar_event_happy_path() throws  Exception {
        mockMvc.perform(
                get(GET_URI+"?startDate=2015-01-01")
                .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_calendar_event_bad_request_no_startDate() throws  Exception {
        mockMvc.perform(
                get(GET_URI)
                        .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test //Multi day event Hanukkah is included
    public void get_calendar_event_multi_day_happy_path() throws  Exception {
        mockMvc.perform(
                get(GET_URI+"?startDate=2016-01-01&endDate=2016-01-31")
                        .with(authToken(TestConstants.USER_DAGARFIN_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_calendar_event_with_permission() throws  Exception {
        mockMvc.perform(
                get(GET_URI+"?startDate=2015-01-01")
                .with(authToken(TestConstants.USER_HARWELLM))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_calendar_event_with_no_permission() throws  Exception {
        mockMvc.perform(
                get(GET_URI)
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        ).andExpect(status().isBadRequest());
    }
}
