package com.maritz.culturenext.alert.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.TestUtil;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.repository.AlertRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.alert.constants.AlertConstants;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class AlertServiceTest extends AbstractDatabaseTest {

    @Inject private AlertRepository alertRepository; 
    @Inject private AlertService alertService;
    @Inject private AuthenticationService authenticationService;

    // valid alert entity test ids
    private static final Long NEW_ALERT_ID = 96539L;
    private static final Long VIEWED_ALERT_ID = 96540L;
    private static final Long ARCHIVED_ALERT_ID = 96541L;
    
    // pax who has the above alerts
    private static final Long ALERT_PAX_ID = 2765L;
    
    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_RUSKLA);
    }

    @Test
    public void testUpdateAlertsMissingDTOs() {
        // update with missing DTOs and check if result is empty
        List<AlertUpdateDTO> alertUpdateDTOList = updateAlerts(null);
        assertTrue(alertUpdateDTOList.isEmpty());
    }

    @Test
    public void testUpdateAlertsEmptyDTOs() {
        // update with empty DTOs and check if result is empty
        List<AlertUpdateDTO> alertUpdateDTOList
                = updateAlerts(new ArrayList<AlertUpdateDTO>());
        assertTrue(alertUpdateDTOList.isEmpty());
    }

    // status validation

    @Test
    public void testUpdateAlertsMissingStatus() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_STATUS_EMPTY);
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, null)));

        verifyUpdateAlertsErrors(updateDTOs, errors);
    }

    @Test
    public void testUpdateAlertsInvalidStatus() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_STATUS_NOT_VALID);
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, "BAD STATUS")));

        verifyUpdateAlertsErrors(updateDTOs, errors);
    }

    // id validation

    @Test
    public void testUpdateAlertsMissingId() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_MISSING_ID);
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(null, StatusTypeCode.VIEWED.name())));

        verifyUpdateAlertsErrors(updateDTOs, errors);
    }

    @Test
    public void testUpdateAlertsInvalidId() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_INVALID_ID);
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(93939394343439L, StatusTypeCode.VIEWED.name())));

        verifyUpdateAlertsErrors(updateDTOs, errors);
    }

    // status transition validation

    @Test
    public void testUpdateAlertsNewToNotViewed() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_INVALID_STATUS_TRANSITION);
        
        List<String> invalidTransitionStatusList = Arrays.asList(
                StatusTypeCode.NEW.name(),
                StatusTypeCode.ARCHIVED.name()
            );
        for (String invalidTransitionStatus : invalidTransitionStatusList) {
            List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                    Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, invalidTransitionStatus)));
            verifyUpdateAlertsErrors(updateDTOs, errors);
        }
    }

    @Test
    public void testUpdateAlertsViewedToNotArchived() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_INVALID_STATUS_TRANSITION);

        List<String> invalidTransitionStatusList = Arrays.asList(
                StatusTypeCode.NEW.name(),
                StatusTypeCode.VIEWED.name()
            );
        for (String invalidTransitionStatus : invalidTransitionStatusList) {
            List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                    Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, invalidTransitionStatus)));
            verifyUpdateAlertsErrors(updateDTOs, errors);
        }
    }

    @Test
    public void testUpdateAlertsArchivedToAnyStatus() {
        // set up expected errors and update request
        List<String> errors = Arrays.asList(AlertConstants.ERROR_INVALID_STATUS_TRANSITION);

        List<String> invalidTransitionStatusList = Arrays.asList(
                StatusTypeCode.NEW.name(),
                StatusTypeCode.VIEWED.name(),
                StatusTypeCode.ARCHIVED.name()
            );
        for (String invalidTransitionStatus : invalidTransitionStatusList) {
            List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                    Arrays.asList(generateAlertUpdateDTO(ARCHIVED_ALERT_ID, invalidTransitionStatus)));
            verifyUpdateAlertsErrors(updateDTOs, errors);
        }
    }

    // success

    @Test
    public void testUpdateAlertsNewToViewed() {
        // update with missing DTOs and check if result is empty
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, StatusTypeCode.VIEWED.name())));
        List<AlertUpdateDTO> alertUpdateDTOList = updateAlerts(updateDTOs);

        assertEquals(1, alertUpdateDTOList.size());

        AlertUpdateDTO updateDTO = alertUpdateDTOList.get(0);
        assertEquals(StatusTypeCode.VIEWED.name(), updateDTO.getStatus());

        // verify that the status on the entity has been updated
        Alert alert = alertRepository.findOne(updateDTO.getId());
        assertEquals(StatusTypeCode.VIEWED.name(), alert.getStatusTypeCode());
    }

    @Test
    public void testUpdateAlertsViewedToArchived() {
        // update with missing DTOs and check if result is empty
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(VIEWED_ALERT_ID, StatusTypeCode.ARCHIVED.name())));
        List<AlertUpdateDTO> alertUpdateDTOList = updateAlerts(updateDTOs);

        assertEquals(1, alertUpdateDTOList.size());

        AlertUpdateDTO updateDTO = alertUpdateDTOList.get(0);
        assertEquals(StatusTypeCode.ARCHIVED.name(), updateDTO.getStatus());

        // verify that the status on the entity has been updated
        Alert alert = alertRepository.findOne(updateDTO.getId());
        assertEquals(StatusTypeCode.ARCHIVED.name(), alert.getStatusTypeCode());
    }
    
    @Test
    public void testUpdateAlertNotMine() {
        // update with missing DTOs and check if result is empty
        List<AlertUpdateDTO> updateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(generateAlertUpdateDTO(NEW_ALERT_ID, StatusTypeCode.VIEWED.name())));

        authenticationService.authenticate(TestConstants.USER_PORTERGA);
        verifyUpdateAlertsErrors(updateDTOs, Arrays.asList(AlertConstants.ERROR_INVALID_ID));
        
    }
    
    // helper methods

    /**
     * Intiates the update with the given records.
     * Also does verification to check if the result is actually provided.
     *
     * @param alertUpdateDTOs records for update
     * @return result from update
     */
    private List<AlertUpdateDTO> updateAlerts(List<AlertUpdateDTO> alertUpdateDTOs) {
        List<AlertUpdateDTO> alertUpdateDTOList = alertService.updateAlerts(ALERT_PAX_ID, alertUpdateDTOs);
        assertNotNull(alertUpdateDTOList);

        return alertUpdateDTOList;
    }

    /**
     * Verifies errors that are expected to occur during alert update.
     *
     * @param errorCodes expected error codes
     */
    private void verifyUpdateAlertsErrors(List<AlertUpdateDTO> alertUpdateDTOs,
                                                 @Nonnull List<String> errorCodes) {
        try {
            alertService.updateAlerts(ALERT_PAX_ID, alertUpdateDTOs);
        } catch(ErrorMessageException e) {
            TestUtil.assertValidationErrors(errorCodes, e);
        }
    }

    /**
     * Generates a DTO for an alert update.
     *
     * @param status status to update alert with
     * @param id id of the alert to update
     * @return DTO for a alert update
     */
    private AlertUpdateDTO generateAlertUpdateDTO(Long id, String status) {
        return new AlertUpdateDTO(id, status);
    }
}