package com.maritz.culturenext.alert.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.times;

import java.time.MonthDay;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.AlertMisc;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.Comment;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.alert.service.impl.AlertServiceImpl;
import com.maritz.culturenext.approval.service.ApprovalService;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.newsfeed.services.NewsfeedService;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.program.milestone.event.PaxMilestoneReminderDTO;
import com.maritz.culturenext.recognition.dao.RaiseDao;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.social.comment.constants.CommentConstants;
import com.maritz.culturenext.social.comment.services.CommentService;
import com.maritz.test.AbstractMockTest;

public class AlertServiceUnitTest extends AbstractMockTest {

    @Mock private ApprovalService approvalService;
    @Mock private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Mock private CommentService commentService;
    @Mock private ConcentrixDao<Alert> alertDao;
    @Mock private ConcentrixDao<AlertMisc> alertMiscDao;
    @Mock private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock private ConcentrixDao<Comment> commentDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<PaxMisc> paxMiscDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private NewsfeedService newsfeedService;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private ProxyService proxyService;
    @Mock private RaiseDao raiseDao;
    @Mock private Security security;
    @Mock private AwardTypeService awardTypeService;

    private Long paxId;
    private Long paxIdWithoutProgramData;
    private Year hireYear;
    private Long programId;
    private Long directManagerId;
    private Double awardAmount;
    private String programName;
    private String payoutType;
    private Long yearsOfService;
    private List<PaxMilestoneReminderDTO> paxMilestoneReminderDTOList;
    private MonthDay monthDay;

    private FindBy<Alert> alertFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Alert> mockedFindByReceiver = PowerMockito.mock(FindBy.class);
    private FindBy<Alert> mockedFindBySubmitter = PowerMockito.mock(FindBy.class);
    private FindBy<AlertMisc> alertMiscFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<ApprovalPending> approvalPendingFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Comment> commentFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<NewsfeedItem> newsfeedItemFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Nomination> nominationFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Recognition> recognitionFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Relationship> relationshipFindBy = PowerMockito.mock(FindBy.class);

    @InjectMocks private final AlertServiceImpl alertService = new AlertServiceImpl();

    @Before
    public void setup() {
        paxId = 754L;
        paxMilestoneReminderDTOList = new ArrayList<>();
        paxIdWithoutProgramData = 222L;
        hireYear = Year.of(2015);
        programId = 20L;
        directManagerId = 45L;
        awardAmount = 200.00;
        programName = "NatalieProgram";
        payoutType = "NataliePayoutType";
        yearsOfService = 3L;
        monthDay = MonthDay.now();


        MockitoAnnotations.initMocks(this);

        // Common nomination findby mocks
        PowerMockito.when(nominationDao.findBy()).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.where(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.eq(anyLong())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.and(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.isNull()).thenReturn(nominationFindBy);

        // Common recognition findby mocks
        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(anyLong())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.and(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.in(Matchers.<String>anyVararg())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.isNull()).thenReturn(recognitionFindBy);

        // Common alert findby mocks
        PowerMockito.when(alertDao.findBy()).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.where(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.and(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.in(anyCollection())).thenReturn(alertFindBy);

        // Common relationship findby mocks
        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.where(anyString())).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.eq(anyLong())).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.and(anyString())).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.in(anyCollection())).thenReturn(relationshipFindBy);

        // Common approvalPending findby mocks
        PowerMockito.when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.where(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.in(anyList())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.and(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(anyString())).thenReturn(approvalPendingFindBy);

        // Common Nomination program findby mocks
        PowerMockito.when(nominationDao.findBy()).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.where(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.eq(anyLong())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.and(anyString())).thenReturn(nominationFindBy);
        PowerMockito.when(nominationFindBy.isNull()).thenReturn(nominationFindBy);

        PowerMockito.when(newsfeedItemDao.findBy()).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.where(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.and(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(anyLong())).thenReturn(newsfeedItemFindBy);

        // Comment Abuse Admin Additions
        PowerMockito.when(commentService.getReportAbuseAdminPaxId()).thenReturn(141L);

        // Raise Dao mocks
        PowerMockito.when(raiseDao.getPointsForRaise(anyLong(), anyLong())).thenReturn(10L);
        PowerMockito.when(raiseDao.getRaisePointsForManager(anyLong(), anyLong())).thenReturn(30.0);

        PowerMockito.when(security.getPaxId()).thenReturn(141L);

        Comment comment = new Comment();
        comment.setTargetId(TestConstants.ID_1);
        comment.setCommentDate(new Date());
        PowerMockito.when(commentDao.findBy()).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.where(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.findOne()).thenReturn(comment);

        Long programId = 3L;
        NewsfeedItem newsfeedItem = new NewsfeedItem();
        newsfeedItem.setId(57581L);
        Long abuseCommentAdmin = 141L;

        Alert newAlert = new Alert();
        newAlert.setAlertTypeCode(ProjectConstants.DEFAULT_ALERT_TYPE_CODE);
        newAlert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.toString());
        newAlert.setProgramId(programId);
        newAlert.setTargetId(newsfeedItem.getId());
        newAlert.setPaxId(abuseCommentAdmin);
        newAlert.setStatusTypeCode(StatusTypeCode.NEW.name());
        newAlert.setStatusDate(new Date());
        newAlert.setCreateId(String.valueOf(security.getPaxId()));

        PowerMockito.doNothing().when(alertDao).save(any(Alert.class));
        PowerMockito.doNothing().when(alertMiscDao).save(any(AlertMisc.class));

        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName("DPP Points");
        PowerMockito.when(awardTypeService.getAwardTypeByPayoutType(any(String.class))).thenReturn(awardTypeDTO);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_addRaiseReceiverAlert_should_not_create_notification() {

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(null);

        alertService.addRaiseReceiverAlert(TestConstants.ID_1);

        // Invalid newsfeedItem should prevent subsequent data calls from occurring
        PowerMockito.verifyZeroInteractions(alertDao, recognitionDao);
    }

    @Test
    public void test_addRaiseReceiverAlert_should_create_notification() {
        int receiverCount = 5;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(TestConstants.ID_1);
        nomination.setSubmitterPaxId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nomination);

        // Mockup list of recognitions
        List<Recognition> recognitions = new ArrayList<>();
        Recognition recognition = new Recognition();
        recognition.setReceiverPaxId(TestConstants.ID_1);
        recognitions.add(recognition);

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recognitions);

        // Mockup list of receiver alerts
        List<Alert> receiverAlerts = new ArrayList<>();
        for (int i = 1; i < receiverCount; i++) {
            Alert alert = new Alert();
            alert.setId(new Long(i));
            receiverAlerts.add(alert);
        }

        PowerMockito.when(alertFindBy.find()).thenReturn(receiverAlerts);

        List<Relationship> relationships = new ArrayList<>();
        relationships.add(new Relationship().setPaxId2(TestConstants.ID_2));

        PowerMockito.when(relationshipFindBy.find()).thenReturn(relationships);

        Alert mgrAlert = new Alert();
        mgrAlert.setId(TestConstants.ID_2);

        PowerMockito.when(alertFindBy.findOne()).thenReturn(mgrAlert);

        alertService.addRaiseReceiverAlert(TestConstants.ID_1);

        // Alerts for receiver and manager
        Mockito.verify(alertDao, times(2)).save(any(Alert.class));
    }

    @Test
    public void test_create_alert_for_comment_new(){
        NewsfeedItem nfi = new NewsfeedItem();
        nfi.setId(TestConstants.ID_1);
        nfi.setTargetId(TestConstants.ID_1);
        nfi.setFromPaxId(TestConstants.ID_1);
        nfi.setNewsfeedItemTypeCode("RECOGNITION");

        PowerMockito.when(newsfeedItemDao.findById(anyLong())).thenReturn(nfi);

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setSubmitterPaxId(TestConstants.ID_1);
        nomination.setParentId(TestConstants.ID_1);
        nomination.setProgramId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nomination);

        List<Recognition> recList = new ArrayList<>();
        for(long i = 2L; i < 4; i++){
            Recognition rec = new Recognition();
            rec.setReceiverPaxId(i);
            recList.add(rec);
        }

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);

        String receiverAlertTypeCode = "COMMENT_RECOGNITION_RECEIVER";

        PowerMockito.when(alertFindBy.eq(matches(receiverAlertTypeCode))).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.and(anyString())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.in(anyCollection())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.find()).thenReturn(new ArrayList<Alert>());
        String submitterAlertTypeCode = "COMMENT_RECOGNITION_SUBMITTER";
        PowerMockito.when(alertFindBy.eq(matches(submitterAlertTypeCode))).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.and(anyString())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.eq(anyLong())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.find()).thenReturn(new ArrayList<Alert>());

        // mockito - mock alertDao.save(Alert) to do nothing.

        alertService.addNotificationForRecognition(TestConstants.ID_1, 3L, receiverAlertTypeCode, submitterAlertTypeCode);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Alert alert = (Alert)args[0];
                Long alertId = alert.getId();
                assertTrue(alertId == null);
                assertThat(alert.getStatusTypeCode(), is(StatusTypeCode.NEW.name()));
                assertFalse(alert.getPaxId().equals(3L)); // Should not change alert for pax id 3
                                                            //since they caused the alert to happen
                alert.setId(TestConstants.ID_1);
                return null;
            }
        }).when(alertDao).save(Matchers.any(Alert.class));
    }

    @Test
    public void test_create_alert_for_comment_viewed(){
        NewsfeedItem nfi = new NewsfeedItem();
        nfi.setId(TestConstants.ID_1);
        nfi.setTargetId(TestConstants.ID_1);
        nfi.setFromPaxId(TestConstants.ID_1);
        nfi.setNewsfeedItemTypeCode("RECOGNITION");

        PowerMockito.when(newsfeedItemDao.findById(anyLong())).thenReturn(nfi);

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setSubmitterPaxId(TestConstants.ID_1);
        nomination.setParentId(TestConstants.ID_1);
        nomination.setProgramId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nomination);

        List<Recognition> recList = new ArrayList<>();
        List<Alert> receiverAlerts = new ArrayList<>();
        for(long i = 2L; i < 4; i++){
            Recognition rec = new Recognition();
            rec.setReceiverPaxId(i);
            recList.add(rec);

            Alert alert = new Alert();
            alert.setStatusTypeCode(StatusTypeCode.VIEWED.name());
            alert.setStatusDate(new Date());
            alert.setId(i);
            alert.setPaxId(i);
            alert.setTargetTable(TableName.NEWSFEED_ITEM.name());
            receiverAlerts.add(alert);
        }

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);

        String receiverAlertTypeCode = "COMMENT_RECOGNITION_RECEIVER";

        List<Alert> submitterAlerts = new ArrayList<>();

        Alert alert = new Alert();
        alert.setStatusTypeCode(StatusTypeCode.VIEWED.name());
        alert.setStatusDate(new Date());
        alert.setId(TestConstants.ID_1);
        alert.setPaxId(TestConstants.ID_1);
        alert.setTargetTable(TableName.NEWSFEED_ITEM.name());
        submitterAlerts.add(alert);

        PowerMockito.when(alertFindBy.eq(matches(receiverAlertTypeCode))).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.and(anyString())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.in(anyCollection())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.find()).thenReturn(receiverAlerts);
        String submitterAlertTypeCode = "COMMENT_RECOGNITION_SUBMITTER";
        PowerMockito.when(alertFindBy.eq(matches(submitterAlertTypeCode))).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.and(anyString())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.eq(anyLong())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.find()).thenReturn(submitterAlerts);

        alertService.addNotificationForRecognition(TestConstants.ID_1, 3L, receiverAlertTypeCode, submitterAlertTypeCode);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Alert alert = (Alert)args[0];
                Long alertId = alert.getId();
                assertTrue(alertId != null);
                assertThat(alert.getStatusTypeCode(), is(StatusTypeCode.NEW.name()));
                assertFalse(alert.getPaxId().equals(3L)); // Should not change alert for pax id 3
                                                            //since they caused the alert to happen
                return null;
            }
        }).when(alertDao).save(Matchers.any(Alert.class));
    }

    @Test
    public void test_addRecognitionApprovalAlert_success(){
        int receiverCount = 2;

        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);

        List<Recognition> recList = new ArrayList<>();
        for(long i = 1L; i < receiverCount+1; i++){
            Recognition rec = new Recognition();
            rec.setId(i);
            rec.setReceiverPaxId(i);
            recList.add(rec);
        }

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);

        List<ApprovalPending> apprPendingList = new ArrayList<>();
        for(long i = 1L; i <  receiverCount+1; i++){
            ApprovalPending apprPending = new ApprovalPending();
            apprPending.setPaxId(i);
            apprPendingList.add(apprPending);
        }

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(apprPendingList);

        //mock alert not created before.
        PowerMockito.when(alertFindBy.findOne()).thenReturn(null);

        alertService.addRecognitionApprovalAlert(TestConstants.ID_1);

        //verify create notification is called.
        Mockito.verify(alertDao, times( receiverCount)).save(any(Alert.class));
    }

    @Test
    public void test_addRecognitionApprovalAlert_notification_exist(){
        int receiverCount = 2;

        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);

        List<Recognition> recList = new ArrayList<>();
        for(long i = 1L; i < receiverCount+1; i++){
            Recognition rec = new Recognition();
            rec.setId(i);
            rec.setReceiverPaxId(i);
            recList.add(rec);
        }

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);

        List<ApprovalPending> apprPendingList = new ArrayList<>();
        for(long i = 1L; i <  receiverCount+1; i++){
            ApprovalPending apprPending = new ApprovalPending();
            apprPending.setPaxId(i);
            apprPendingList.add(apprPending);
        }

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(apprPendingList);

        //mock alert not created before.
        PowerMockito.when(alertFindBy.findOne()).thenReturn(PowerMockito.mock(Alert.class));

        alertService.addRecognitionApprovalAlert(TestConstants.ID_1);

        //verify create notification is not called.
        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test
    public void test_addRecognitionApprovalAlert_approvalPending_not_exist(){
        int receiverCount = 2;

        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);

        //Mock recognition result.
        List<Recognition> recList = new ArrayList<>();
        for(long i = 1L; i < receiverCount+1; i++){
            Recognition rec = new Recognition();
            rec.setId(i);
            rec.setReceiverPaxId(i);
            recList.add(rec);
        }
        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);
        //approval pending not exists.
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(null);

        alertService.addRecognitionApprovalAlert(TestConstants.ID_1);

        //verify create notification is not called.
        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test
    public void test_addRecognitionApprovalAlert_recognition_not_exist(){
        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);
        //recognition not exists for nomination entered.
        PowerMockito.when(recognitionFindBy.find()).thenReturn(null);

        alertService.addRecognitionApprovalAlert(TestConstants.ID_1);

        //verify create notification is not called.
        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_addRecognitionApprovalAlert_failed_nomination_not_found(){

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(null);

        alertService.addRecognitionApprovalAlert(TestConstants.ID_1);

        //verify create notification is not called.
        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_addRecognitionApprovalAlert_failed_nominationId_invalid(){

        alertService.addRecognitionApprovalAlert(null);

        //verify create notification is not called.
        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test
    public void test_create_alert_for_reported_comment(){
        NewsfeedItem nfi = new NewsfeedItem();
        nfi.setId(TestConstants.ID_1);
        nfi.setTargetId(TestConstants.ID_1);
        nfi.setFromPaxId(TestConstants.ID_1);
        nfi.setNewsfeedItemTypeCode("RECOGNITION");

        PowerMockito.when(newsfeedItemDao.findById(anyLong())).thenReturn(nfi);

        PowerMockito.when(nominationFindBy.findOne(ProjectConstants.PROGRAM_ID, Long.class)).thenReturn(100L);

        List<Recognition> recList = new ArrayList<>();
        List<Alert> receiverAlerts = new ArrayList<>();
        for(long i = 2L; i < 4; i++){
            Recognition rec = new Recognition();
            rec.setReceiverPaxId(i);
            recList.add(rec);

            Alert alert = new Alert();
            alert.setStatusTypeCode(StatusTypeCode.VIEWED.name());
            alert.setStatusDate(new Date());
            alert.setId(i);
            alert.setPaxId(i);
            alert.setTargetTable(TableName.COMMENT.name());
            receiverAlerts.add(alert);
        }

        PowerMockito.when(recognitionFindBy.find()).thenReturn(recList);

        String receiverAlertTypeCode = "COMMENT_RECOGNITION_RECEIVER";

        List<Alert> submitterAlerts = new ArrayList<>();

        Alert alert = new Alert();
        alert.setStatusTypeCode(StatusTypeCode.VIEWED.name());
        alert.setStatusDate(new Date());
        alert.setId(TestConstants.ID_1);
        alert.setPaxId(TestConstants.ID_1);
        alert.setTargetTable(TableName.COMMENT.name());
        submitterAlerts.add(alert);

        PowerMockito.when(alertFindBy.eq(matches(receiverAlertTypeCode))).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.and(anyString())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.in(anyCollection())).thenReturn(mockedFindByReceiver);
        PowerMockito.when(mockedFindByReceiver.find()).thenReturn(receiverAlerts);
        String submitterAlertTypeCode = "COMMENT_RECOGNITION_SUBMITTER";
        PowerMockito.when(alertFindBy.eq(matches(submitterAlertTypeCode))).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.and(anyString())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.eq(anyLong())).thenReturn(mockedFindBySubmitter);
        PowerMockito.when(mockedFindBySubmitter.find()).thenReturn(submitterAlerts);

        alertService.addReportedCommentAdminAlert(TestConstants.ID_1, new Comment());

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Alert alert = (Alert)args[0];
                Long alertId = alert.getId();
                assertTrue(alertId != null);
                assertThat(alert.getStatusTypeCode(), is(StatusTypeCode.NEW.name()));
                return null;
            }
        }).when(alertDao).save(Matchers.any(Alert.class));
    }

    @Test
    public void test_add_pending_recognition_submitter_alert() {
        Long testSubmitterPaxId = 3L;
        PowerMockito.when(nominationFindBy.findOne(ProjectConstants.PROGRAM_ID, Long.class)).thenReturn(TestConstants.ID_2);

        PowerMockito.when(alertDao.findBy()).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.where(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(TestConstants.ID_1)).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.and(ProjectConstants.ALERT_SUB_TYPE_CODE)).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(AlertSubType.RECOGNITION_PENDING_APPROVAL.toString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.findOne()).thenReturn(new Alert());

        PowerMockito.when(newsfeedItemDao.findBy()).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.where(ProjectConstants.TARGET_ID)).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.and(ProjectConstants.TARGET_TABLE)).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(TestConstants.ID_1)).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.findOne()).thenReturn(new NewsfeedItem());

        alertService.addPendingRecognitionSubmitterAlert(TestConstants.ID_1, testSubmitterPaxId);
        alertService.addPendingRecognitionSubmitterAlert(TestConstants.ID_1, 7L);
        alertService.addPendingRecognitionSubmitterAlert(TestConstants.ID_1, 8L);

        // Check the new alert record was saved
        Mockito.verify(alertDao, times(3)).save(any(Alert.class));

    }

    @Test
    public void test_add_approval_action_taken_notification_happy_path() {
        PowerMockito.when(nominationFindBy.findOne(ProjectConstants.PROGRAM_ID, Long.class)).thenReturn(TestConstants.ID_1);

        List<RecognitionDetailsDTO> recognitionDetailsDTOList = new ArrayList<>();

        Long nominationId = 65042L;

        RecognitionDetailsDTO recognitionDetailsDTO = new RecognitionDetailsDTO();
        recognitionDetailsDTO.setId(202818L);
        recognitionDetailsDTO.setStatus(StatusTypeCode.APPROVED.name());
        recognitionDetailsDTO.setNominationId(nominationId);
        EmployeeDTO fromPax = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        recognitionDetailsDTO.setFromPax(fromPax);
        EmployeeDTO toPax = new EmployeeDTO();
        toPax.setPaxId(141L);
        recognitionDetailsDTO.setToPax(toPax);
        EmployeeDTO approvalPax = new EmployeeDTO();
        approvalPax.setPaxId(847L);
        recognitionDetailsDTO.setApprovalPax(approvalPax);
        recognitionDetailsDTO.setAwardAmount(10.0);
        recognitionDetailsDTOList.add(recognitionDetailsDTO);

        RecognitionDetailsDTO recognitionDetailsDTO2 = new RecognitionDetailsDTO();
        recognitionDetailsDTO2.setId(202818L);
        recognitionDetailsDTO2.setStatus(StatusTypeCode.REJECTED.name());
        recognitionDetailsDTO2.setNominationId(nominationId);
        EmployeeDTO fromPax2 = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        recognitionDetailsDTO2.setFromPax(fromPax2);
        EmployeeDTO toPax2 = new EmployeeDTO();
        toPax.setPaxId(141L);
        recognitionDetailsDTO2.setToPax(toPax2);
        EmployeeDTO approvalPax2 = new EmployeeDTO();
        approvalPax.setPaxId(847L);
        recognitionDetailsDTO2.setApprovalPax(approvalPax2);
        recognitionDetailsDTO2.setAwardAmount(10.0);
        recognitionDetailsDTOList.add(recognitionDetailsDTO2);

        RecognitionDetailsDTO recognitionDetailsDTO3 = new RecognitionDetailsDTO();
        recognitionDetailsDTO3.setId(202818L);
        recognitionDetailsDTO3.setStatus(StatusTypeCode.APPROVED.name());
        recognitionDetailsDTO3.setNominationId(nominationId);
        EmployeeDTO fromPax3 = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        recognitionDetailsDTO3.setFromPax(fromPax3);
        EmployeeDTO toPax3 = new EmployeeDTO();
        toPax.setPaxId(141L);
        recognitionDetailsDTO3.setToPax(toPax3);
        EmployeeDTO approvalPax3 = new EmployeeDTO();
        approvalPax.setPaxId(847L);
        recognitionDetailsDTO3.setApprovalPax(approvalPax3);
        recognitionDetailsDTO3.setAwardAmount(10.0);
        recognitionDetailsDTOList.add(recognitionDetailsDTO3);

        RecognitionDetailsDTO recognitionDetailsDTO4 = new RecognitionDetailsDTO();
        recognitionDetailsDTO4.setId(202818L);
        recognitionDetailsDTO4.setStatus(StatusTypeCode.APPROVED.name());
        recognitionDetailsDTO4.setNominationId(nominationId);
        EmployeeDTO fromPax4 = new EmployeeDTO();
        fromPax.setPaxId(TestConstants.PAX_DAGARFIN);
        recognitionDetailsDTO4.setFromPax(fromPax4);
        EmployeeDTO toPax4 = new EmployeeDTO();
        toPax.setPaxId(141L);
        recognitionDetailsDTO4.setToPax(toPax4);
        EmployeeDTO approvalPax4 = new EmployeeDTO();
        approvalPax.setPaxId(847L);
        recognitionDetailsDTO4.setApprovalPax(approvalPax4);
        recognitionDetailsDTO4.setAwardAmount(10.0);
        recognitionDetailsDTOList.add(recognitionDetailsDTO4);

        PowerMockito.when(cnxtRecognitionRepository.getTotalPointsForNomination(anyLong())).thenReturn(5L);

        alertService.addApprovalActionTakenNotification(recognitionDetailsDTOList, nominationId);

        Mockito.verify(alertDao, times(2)).save(any(Alert.class));
        Mockito.verify(alertMiscDao, times(8)).save(any(AlertMisc.class));

    }

    @Test
    public void test_addRaiseApprovalAlert_happy_path(){
        int receiverCount = 2;

        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);

        List<Map<String, Object>> raiseList = new ArrayList<>();
        for (int i = 1; i < receiverCount+1; i++) {
            Map<String, Object> raise = new HashMap<>();
            raise.put(ProjectConstants.ID, new Long(i));
            raiseList.add(raise);
        }

        List<String> parentStatusList = new ArrayList<>();
        parentStatusList.add(StatusTypeCode.APPROVED.toString());
        parentStatusList.add(StatusTypeCode.PENDING.toString());
        List<String> raiseStatus = new ArrayList<>();
        raiseStatus.add(StatusTypeCode.PENDING.toString());

        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(TestConstants.ID_1, null, parentStatusList,
                raiseStatus,null)).thenReturn(raiseList);

        List<ApprovalPending> apprPendingList = new ArrayList<>();
        for(long i = 1L; i <  receiverCount+1; i++){
            ApprovalPending apprPending = new ApprovalPending();
            apprPending.setPaxId(i);
            apprPendingList.add(apprPending);
        }

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(apprPendingList);

        PowerMockito.when(alertFindBy.findOne()).thenReturn(null);

        alertService.addRaiseApprovalAlert(TestConstants.ID_1);

        Mockito.verify(alertDao, times(receiverCount)).save(any(Alert.class));
    }

    @Test
    public void test_addRaiseApprovalAlert_alert_exists(){
        int receiverCount = 2;

        //mock nomination result.
        Nomination nominationExp = new Nomination();
        nominationExp.setId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nominationExp);

        List<Map<String, Object>> raiseList = new ArrayList<>();
        for (int i = 1; i < receiverCount+1; i++) {
            Map<String, Object> raise = new HashMap<>();
            raise.put(ProjectConstants.ID, new Long(i));
            raiseList.add(raise);
        }

        List<String> parentStatusList = new ArrayList<>();
        parentStatusList.add(StatusTypeCode.APPROVED.toString());
        List<String> raiseStatus = new ArrayList<>();
        raiseStatus.add(StatusTypeCode.PENDING.toString());

        PowerMockito.when(raiseDao.getListOfRaisesOnNomination(TestConstants.ID_1, null, parentStatusList,
                raiseStatus,null)).thenReturn(raiseList);

        List<ApprovalPending> apprPendingList = new ArrayList<>();
        for(long i = 1L; i <  receiverCount+1; i++){
            ApprovalPending apprPending = new ApprovalPending();
            apprPending.setPaxId(i);
            apprPendingList.add(apprPending);
        }

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(apprPendingList);

        PowerMockito.when(alertFindBy.findOne()).thenReturn(PowerMockito.mock(Alert.class));

        alertService.addRaiseApprovalAlert(TestConstants.ID_1);

        Mockito.verify(alertDao, times(0)).save(any(Alert.class));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updateAlerts_reportedCommentAbuse_new_to_archived(){
        AlertUpdateDTO alertUpdate = new AlertUpdateDTO();
        alertUpdate.setId(TestConstants.ID_1);
        alertUpdate.setStatus(StatusTypeCode.ARCHIVED.toString());

        // Provide mock data for Alert lookup
        Alert alert = new Alert();
        alert.setId(TestConstants.ID_1);
        alert.setTargetId(3L);
        alert.setPaxId(170L);
        alert.setStatusTypeCode(StatusTypeCode.VIEWED.toString());
        alert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.toString());
        alert.setTargetTable(TableName.COMMENT.name());

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);

        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(CommentConstants.REPORTED_STATUS_CODE);

        PowerMockito.when(commentService.findCommentById(anyLong())).thenReturn(comment);

        alertService.updateAlerts(170L, Arrays.asList(alertUpdate));
    }

    public void test_updateAlerts_evaluated_reportCommentAbuse_viewed_to_archived(){
        AlertUpdateDTO alertUpdate = new AlertUpdateDTO();
        alertUpdate.setId(TestConstants.ID_1);
        alertUpdate.setStatus(StatusTypeCode.ARCHIVED.toString());

        // Provide mock data for Alert lookup
        Alert alert = new Alert();
        alert.setId(TestConstants.ID_1);
        alert.setTargetId(3L);
        alert.setPaxId(170L);
        alert.setStatusTypeCode(StatusTypeCode.VIEWED.toString());
        alert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.toString());
        alert.setTargetTable(TableName.COMMENT.name());

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);

        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE);

        PowerMockito.when(commentService.findCommentById(anyLong())).thenReturn(comment);

        List<AlertUpdateDTO> responseBody = alertService.updateAlerts(170L, Arrays.asList(alertUpdate));

        assertTrue(responseBody.get(0).getId().equals(TestConstants.ID_1));
        assertTrue(responseBody.get(0).getStatus().equals(StatusTypeCode.ARCHIVED.toString()));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updateAlerts_reportCommentAbuse_viewed_to_archived(){
        AlertUpdateDTO alertUpdate = new AlertUpdateDTO();
        alertUpdate.setId(TestConstants.ID_1);
        alertUpdate.setStatus(StatusTypeCode.ARCHIVED.toString());

        // Provide mock data for Alert lookup
        Alert alert = new Alert();
        alert.setId(TestConstants.ID_1);
        alert.setTargetId(3L);
        alert.setPaxId(170L);
        alert.setStatusTypeCode(StatusTypeCode.VIEWED.toString());
        alert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.toString());
        alert.setTargetTable(TableName.COMMENT.name());

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);

        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(CommentConstants.REPORTED_STATUS_CODE);

        PowerMockito.when(commentService.findCommentById(anyLong())).thenReturn(comment);

        alertService.updateAlerts(170L, Arrays.asList(alertUpdate));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updateAlerts_reportCommentAbuse_evaluated_new_to_archived(){
        AlertUpdateDTO alertUpdate = new AlertUpdateDTO();
        alertUpdate.setId(TestConstants.ID_1);
        alertUpdate.setStatus(StatusTypeCode.ARCHIVED.toString());

        // Provide mock data for Alert lookup
        Alert alert = new Alert();
        alert.setId(TestConstants.ID_1);
        alert.setTargetId(3L);
        alert.setPaxId(170L);
        alert.setStatusTypeCode(StatusTypeCode.NEW.name());
        alert.setAlertSubTypeCode(AlertSubType.REPORT_COMMENT_ABUSE.toString());
        alert.setTargetTable(TableName.COMMENT.name());

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);

        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(CommentConstants.REPORTED_STATUS_CODE);

        PowerMockito.when(commentService.findCommentById(anyLong())).thenReturn(comment);

        alertService.updateAlerts(170L, Arrays.asList(alertUpdate));
    }

    @Test
    public void test_add_milestone_reminder_alert() {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE, paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);

        alertService.addMilestoneReminderAlert(paxMilestoneReminderDTO);

        Mockito.verify(alertDao, times(1)).save(any(Alert.class));
        Mockito.verify(alertMiscDao, times(7)).save(any(AlertMisc.class));
    }

    @Test
    public void test_add_milestone_reminder_alert_birthday() {

        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(MilestoneConstants.BIRTHDAY_REMINDER_TYPE, paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", awardAmount, payoutType);

        alertService.addMilestoneReminderAlert(paxMilestoneReminderDTO);

        Mockito.verify(alertDao, times(1)).save(any(Alert.class));
        Mockito.verify(alertMiscDao, times(6)).save(any(AlertMisc.class));
    }

    @Test
    public void test_add_milestone_reminder_alert_no_award_amount() {
        PaxMilestoneReminderDTO paxMilestoneReminderDTO = new PaxMilestoneReminderDTO(MilestoneConstants.SERVICE_ANNIVERSARY_REMINDER_TYPE, paxId, monthDay, yearsOfService, programId, hireYear, programName, directManagerId, "2015-01-01", null, null);

        alertService.addMilestoneReminderAlert(paxMilestoneReminderDTO);

        Mockito.verify(alertDao, times(1)).save(any(Alert.class));
        Mockito.verify(alertMiscDao, times(4)).save(any(AlertMisc.class));
    }
}
