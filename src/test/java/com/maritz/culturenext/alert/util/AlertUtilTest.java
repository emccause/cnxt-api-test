package com.maritz.culturenext.alert.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.recognition.constants.NominationConstants;
import com.maritz.culturenext.recognition.dao.NominationDataBulkSaveDao;
import com.maritz.test.AbstractMockTest;


public class AlertUtilTest extends AbstractMockTest {

    @Mock CnxtRecognitionRepository cnxtRecognitionRepository;
    @Mock ConcentrixDao<Program> programDao;
    @Mock ConcentrixDao<ProgramMisc> programMiscDao;
    @Mock ConcentrixDao<SysUser> sysUserDao;
    @Mock NominationDataBulkSaveDao nominationDataBulkSaveDao;

    @SuppressWarnings("unchecked")
    FindBy<ProgramMisc> programMiscFindBy = mock(FindBy.class);
    @SuppressWarnings("unchecked")
    FindBy<SysUser> sysUserFindBy = mock(FindBy.class);

    @Captor ArgumentCaptor<List<Alert>> alertListCaptor;

    AlertUtil alertUtil;

    @Before
    public void setup() {
        when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        when(programMiscFindBy.where(anyString())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.in((String)anyVararg())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.and(anyString())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.eq(anyLong())).thenReturn(programMiscFindBy);
        when(programMiscFindBy.active()).thenReturn(programMiscFindBy);
        when(programMiscFindBy.find()).thenReturn(Arrays.asList(new ProgramMisc()));

        when(programDao.findById(anyLong())).thenReturn(new Program());

        when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        when(sysUserFindBy.where(anyString())).thenReturn(sysUserFindBy);
        when(sysUserFindBy.eq(anyLong())).thenReturn(sysUserFindBy);
        when(sysUserFindBy.findOne()).thenReturn(new SysUser());

        when(cnxtRecognitionRepository.getTotalPointsForNomination(anyLong())).thenReturn(0L);

        alertUtil = new AlertUtil(cnxtRecognitionRepository, programDao, programMiscDao, sysUserDao, nominationDataBulkSaveDao);
    }

    @Test
    public void generateRecognitionAlerts_manger_award_code_type() {

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.FALSE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.TRUE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Setup program type AWARD_CODE
        Program program = new Program();
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        when(programDao.findById(anyLong())).thenReturn(program);

        // Manager status (ACTIVE)
        SysUser manager = new SysUser().setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        when(sysUserFindBy.findOne()).thenReturn(manager);

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;
        Long testManagerId = 1000L;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        List<Long> managerPaxIdList =  Arrays.asList(testManagerId);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, null, managerPaxIdList, null, null);

        // Verify result
        verify(nominationDataBulkSaveDao, times(1)).saveAlertList(alertListCaptor.capture());
        assertEquals(1 , alertListCaptor.getValue().size());

        Alert createdAlert = alertListCaptor.getValue().get(0);
        assertEquals(ProjectConstants.DEFAULT_ALERT_TYPE_CODE , createdAlert.getAlertTypeCode());
        assertEquals(AlertSubType.MANAGER_AWARD_CODE.name() , createdAlert.getAlertSubTypeCode());
        assertEquals(testProgramId , createdAlert.getProgramId());
        assertEquals(TestConstants.ID_1 , createdAlert.getTargetId());
        assertEquals(testManagerId , createdAlert.getPaxId());
        assertEquals(StatusTypeCode.NEW.name() , createdAlert.getStatusTypeCode());
    }

    @Test
    public void generateRecognitionAlerts_manger_inactive() {

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.FALSE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.TRUE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Setup program type AWARD_CODE
        Program program = new Program();
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        when(programDao.findById(anyLong())).thenReturn(program);

        // Manager status (INACTIVE)
        SysUser manager = new SysUser().setStatusTypeCode(StatusTypeCode.INACTIVE.name());
        when(sysUserFindBy.findOne()).thenReturn(manager);

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;
        Long testManagerId = 1000L;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        List<Long> managerPaxIdList =  Arrays.asList(testManagerId);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, null, managerPaxIdList, null, null);

        // Verify result
        verify(nominationDataBulkSaveDao, times(0)).saveAlertList(alertListCaptor.capture());
    }

    @Test
    public void generateRecognitionAlerts_manger_same_as_submitter() {

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.FALSE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.TRUE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Setup program type AWARD_CODE
        Program program = new Program();
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        when(programDao.findById(anyLong())).thenReturn(program);

        // Manager status (ACTIVE)
        SysUser manager = new SysUser().setStatusTypeCode(StatusTypeCode.INACTIVE.name());
        when(sysUserFindBy.findOne()).thenReturn(manager);

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;
        Long testManagerId = testSubmitterPaxId;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        List<Long> managerPaxIdList =  Arrays.asList(testManagerId);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, null, managerPaxIdList, null, null);

        // Verify result
        verify(nominationDataBulkSaveDao, times(0)).saveAlertList(alertListCaptor.capture());
    }

    @Test
    public void generateRecognitionAlerts_program_misc_false_flags() {

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.FALSE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.FALSE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Setup program type AWARD_CODE
        Program program = new Program();
        program.setProgramTypeCode(ProgramTypeEnum.AWARD_CODE.getCode());
        when(programDao.findById(anyLong())).thenReturn(program);

        // Manager status (ACTIVE)
        SysUser manager = new SysUser().setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        when(sysUserFindBy.findOne()).thenReturn(manager);

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;
        Long testManagerId = 1000L;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        List<Long> managerPaxIdList =  Arrays.asList(testManagerId);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, null, managerPaxIdList, null, null);

        // Verify result
        verify(nominationDataBulkSaveDao, times(0)).saveAlertList(alertListCaptor.capture());
    }

    @Test
    public void generateRecognitionAlerts_notify_others_without_points() {

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.TRUE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.FALSE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        Recognition recg = new Recognition();
        recg.setNominationId(TestConstants.ID_1);
        recg.setId(TestConstants.ID_1);
        recg.setReceiverPaxId(TestConstants.PAX_HAGOPIWL);
        recg.setStatusTypeCode(StatusTypeCode.APPROVED.name());
        List<Recognition> recgList = Arrays.asList(recg);

        List<Long> notifyOtherList = new ArrayList<>();
        notifyOtherList.add(TestConstants.PAX_HARWELLM);
        notifyOtherList.add(TestConstants.PAX_MUDDAM);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, recgList, null, null, notifyOtherList);

        // Verify result
        verify(nominationDataBulkSaveDao, times(1)).saveAlertList(alertListCaptor.capture());
        assertEquals(3 , alertListCaptor.getValue().size());

        Alert createdAlert = alertListCaptor.getValue().get(1); //RECOGNITION alert will get created first
        assertEquals(ProjectConstants.DEFAULT_ALERT_TYPE_CODE , createdAlert.getAlertTypeCode());
        assertEquals(AlertSubType.NOTIFY_OTHER.name() , createdAlert.getAlertSubTypeCode());
        assertEquals(testProgramId , createdAlert.getProgramId());
        assertEquals(TestConstants.ID_1 , createdAlert.getTargetId());
        assertEquals(TestConstants.PAX_HARWELLM , createdAlert.getPaxId());
        assertEquals(StatusTypeCode.NEW.name() , createdAlert.getStatusTypeCode());
    }


    @Test
    public void generateRecognitionAlerts_notify_others_with_points() {

        when(cnxtRecognitionRepository.getTotalPointsForNomination(anyLong())).thenReturn(10L);

        // Setup programMisc flags, notify recipient's manager.
        ProgramMisc programMiscRecNotify = new ProgramMisc();
        programMiscRecNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_PROGRAM_MISC_NAME);
        programMiscRecNotify.setMiscData(ProjectConstants.TRUE);

        ProgramMisc programMiscMgrNotify = new ProgramMisc();
        programMiscMgrNotify.setVfName(NominationConstants.NOTIFICATION_RECIPIENT_MGR_PROGRAM_MISC_NAME);
        programMiscMgrNotify.setMiscData(ProjectConstants.FALSE);

        when(programMiscFindBy.find()).thenReturn(Arrays.asList(programMiscRecNotify, programMiscMgrNotify));

        // Defining arguments
        Long testProgramId = 10L;
        Long testSubmitterPaxId = 100L;

        Nomination nomination = new Nomination();
        nomination.setId(TestConstants.ID_1);
        nomination.setProgramId(testProgramId);
        nomination.setSubmitterPaxId(testSubmitterPaxId);

        Recognition recg = new Recognition();
        recg.setNominationId(TestConstants.ID_1);
        recg.setId(TestConstants.ID_1);
        recg.setReceiverPaxId(TestConstants.PAX_HAGOPIWL);
        recg.setStatusTypeCode(StatusTypeCode.APPROVED.name());
        List<Recognition> recgList = Arrays.asList(recg);

        List<Long> notifyOtherList = new ArrayList<>();
        notifyOtherList.add(TestConstants.PAX_HARWELLM);
        notifyOtherList.add(TestConstants.PAX_MUDDAM);

        // Test method
        alertUtil.generateRecognitionAlerts(nomination, recgList, null, null, notifyOtherList);

        // Verify result
        verify(nominationDataBulkSaveDao, times(1)).saveAlertList(alertListCaptor.capture());
        assertEquals(3 , alertListCaptor.getValue().size());

        Alert createdAlert = alertListCaptor.getValue().get(1); //RECOGNITION alert will get created first
        assertEquals(ProjectConstants.DEFAULT_ALERT_TYPE_CODE , createdAlert.getAlertTypeCode());
        assertEquals(AlertSubType.NOTIFY_OTHER.name() , createdAlert.getAlertSubTypeCode());
        assertEquals(testProgramId , createdAlert.getProgramId());
        assertEquals(TestConstants.ID_1 , createdAlert.getTargetId());
        assertEquals(TestConstants.PAX_HARWELLM , createdAlert.getPaxId());
        assertThat(createdAlert.getStatusTypeCode(), is(StatusTypeCode.HOLD.name()));
    }
}

