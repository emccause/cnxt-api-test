package com.maritz.culturenext.social.like.service;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Likes;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.program.dto.ProgramFullDTO;
import com.maritz.culturenext.program.services.ProgramService;
import com.maritz.culturenext.social.like.dao.PaginatedLikeListDao;
import com.maritz.culturenext.social.like.service.impl.LikeServiceImpl;
import com.maritz.test.AbstractMockTest;


public class LikeServiceUnitTest extends AbstractMockTest {

    @InjectMocks private LikeServiceImpl likeService;
    @Mock private ActivityFeedService activityFeedService;
    @Mock private AlertService alertService;
    @Mock private ConcentrixDao <Alert> alertDao;
    @Mock private ConcentrixDao<Likes> likesDao;
    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private Environment environment;
    @Mock private PaginatedLikeListDao paginatedLikeListDao;
    @Mock private ProgramService programService;
    @Mock private Security security;

    private FindBy<Likes> mockedLikesFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Alert> mockedAlertFindBy = PowerMockito.mock(FindBy.class);

    private final String TEST_TYPE_CODE = AlertSubType.RECOGNITION.toString();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup(){
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        PowerMockito.when(newsfeedItemDao.findById(anyLong())).thenReturn( createNewsfeedObject());
        PowerMockito.when(activityFeedService.getActivityById(anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean(), anyBoolean(), anyString()))
            .thenReturn(new NewsfeedItemDTO());

        PowerMockito.when(likesDao.findBy()).thenReturn(mockedLikesFindBy);
        PowerMockito.when(mockedLikesFindBy.where(anyString())).thenReturn(mockedLikesFindBy);
        PowerMockito.when(mockedLikesFindBy.eq(anyLong())).thenReturn(mockedLikesFindBy);
        PowerMockito.when(mockedLikesFindBy.and(anyString())).thenReturn(mockedLikesFindBy);
        PowerMockito.when(mockedLikesFindBy.findOne()).thenReturn(null);

        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        PowerMockito.when(programDao.findById(anyLong())).thenReturn(new Program());
        PowerMockito.when(programService.getProgramByID(anyLong(), isNull(String.class)))
                .thenReturn(new ProgramFullDTO());

        PowerMockito.when(alertDao.findBy()).thenReturn(mockedAlertFindBy);
        PowerMockito.when(mockedAlertFindBy.where()).thenReturn(mockedAlertFindBy);
        PowerMockito.when(mockedAlertFindBy.eq(anyLong())).thenReturn(mockedAlertFindBy);
        PowerMockito.when(mockedAlertFindBy.and()).thenReturn(mockedAlertFindBy);
        PowerMockito.when(mockedAlertFindBy.findOne()).thenReturn(null);

    }

    @Test
    public void test_likeActivity_success(){
        NewsfeedItemDTO newsfeedItemDTO = likeService.likeActivity(TestConstants.ID_1,TestConstants.ID_1);

        assertNotNull (newsfeedItemDTO);
        Mockito.verify(likesDao, times(1)).save(any(Likes.class));
    }

    @Test
    public void test_likeProgram_success(){
        ProgramFullDTO programDto = likeService.likeProgram(TestConstants.ID_1,TestConstants.ID_1);

        assertNotNull (programDto);
        Mockito.verify(likesDao, times(1)).save(any(Likes.class));
    }

    @Test
    public void test_unlikeActivity_fail_likingNotEnabled(){
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

        thrown.expect(ErrorMessageException.class);

        likeService.unlikeActivity(TestConstants.ID_1,TestConstants.ID_1);
    }

    @Test
    public void test_unlikeActivity_success(){
        PowerMockito.when(mockedLikesFindBy.findOne()).thenReturn(new Likes());

        NewsfeedItemDTO newsfeedItemDTO = likeService.unlikeActivity(TestConstants.ID_1,TestConstants.ID_1);

        assertNotNull (newsfeedItemDTO);
        Mockito.verify(likesDao, times(1)).delete(any(Likes.class));
    }

    @Test
    public void test_unlikeProgram_fail_likingNotEnabled(){
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

        thrown.expect(ErrorMessageException.class);

        likeService.unlikeProgram(TestConstants.ID_1,TestConstants.ID_1);
    }

    @Test
    public void test_unlikeProgram_success(){
        PowerMockito.when(mockedLikesFindBy.findOne()).thenReturn(new Likes());

        ProgramFullDTO programDto = likeService.unlikeProgram(TestConstants.ID_1,TestConstants.ID_1);

        assertNotNull (programDto);
        Mockito.verify(likesDao, times(1)).delete(any(Likes.class));
    }

    private NewsfeedItem createNewsfeedObject(){
        NewsfeedItem newsfeed = new NewsfeedItem();
        newsfeed.setId(TestConstants.ID_1);
        newsfeed.setToPaxId(TestConstants.ID_1);
        newsfeed.setNewsfeedItemTypeCode(TEST_TYPE_CODE);
        return newsfeed;
    }

}
