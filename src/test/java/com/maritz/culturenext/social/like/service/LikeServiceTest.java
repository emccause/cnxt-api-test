package com.maritz.culturenext.social.like.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.social.like.service.LikeService;
import com.maritz.culturenext.alert.dto.AlertUpdateDTO;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class LikeServiceTest  extends AbstractDatabaseTest {

    @Inject private AlertService alertService;
    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private LikeService likeService;
    
    private static final Long LIKER_PAX_ID_2 = 22L;
    private static final Long NEWSFEED_ACTIVITY_ID = 59088L;

    private static final String STATUS_DATE = "statusDate";
    
    @Test
    public void test_likeActivity_notification_single_like() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .count();
        int submitterAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .count();
        
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .count();
        int submitterAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .count();
        
        assertThat(recipientAlertCountAfter - recipientAlertCountBefore, is(1));
        assertThat(submitterAlertCountAfter - submitterAlertCountBefore, is(1));
    }
    
    @Test
    public void test_likeActivity_notification_multiple_likes() throws Exception {
        int recipientAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .count();
        int submitterAlertCountBefore = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .count();
        //FIRST LIKE
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        //get last notification created for receiver
        Alert lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        Date firstStatusDateRec = lastReceiverAlert.getStatusDate();
        //change alert status to viewed
        updateAlertsNewToViewed(TestConstants.PAX_MADRIDO, lastReceiverAlert.getId()); 
        
        //get last notification created for submitter
        Alert lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();
        Date firstStatusDateSub = lastSubmitterAlert.getStatusDate();
        //change alert status to viewed
        updateAlertsNewToViewed(TestConstants.PAX_OMARG, lastSubmitterAlert.getId());
        
        //SECOND LIKE
        likeService.likeActivity(LIKER_PAX_ID_2, NEWSFEED_ACTIVITY_ID);
        //get last notification created for receiver
        lastReceiverAlert = alertDao.findById(lastReceiverAlert.getId());
        Date secondStatusDateRec = lastReceiverAlert.getStatusDate();
        //get last notification created for submitter
        lastSubmitterAlert = alertDao.findById(lastSubmitterAlert.getId());
        Date secondStatusDateSub = lastSubmitterAlert.getStatusDate();
        
        int recipientAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .count();
        int submitterAlertCountAfter = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .count();
        
        assertThat(recipientAlertCountAfter - recipientAlertCountBefore, is(1));
        assertThat(submitterAlertCountAfter - submitterAlertCountBefore, is(1));
        assertEquals(lastReceiverAlert.getStatusTypeCode(),StatusTypeCode.NEW.name());
        assertTrue(firstStatusDateRec.compareTo(secondStatusDateRec)<0);
        assertEquals(lastSubmitterAlert.getStatusTypeCode(),StatusTypeCode.NEW.name());
        assertTrue(firstStatusDateSub.compareTo(secondStatusDateSub)<0);
    }
    
    @Test
    public void test_unlikeActivity_notification_single_like() throws Exception {
        //LIKE
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //get last notification created for receiver
        Alert lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        
        assertEquals(lastReceiverAlert.getStatusTypeCode(), StatusTypeCode.NEW.name());
        
        //get last notification created for submitter
        Alert lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();
        
        assertEquals(lastSubmitterAlert.getStatusTypeCode(), StatusTypeCode.NEW.name());
        
        //UNLIKE
        likeService.unlikeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //the notifications for submitter and receiver should have been deleted
        lastReceiverAlert = alertDao.findById(lastReceiverAlert.getId());
        lastSubmitterAlert = alertDao.findById(lastSubmitterAlert.getId());
        assertNull(lastReceiverAlert);
        assertNull(lastSubmitterAlert);
    }
    
    @Test
    public void test_unlikeActivity_notification_multiple_likes() throws Exception {
        //LIKE - 2 likes
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        likeService.likeActivity(LIKER_PAX_ID_2, NEWSFEED_ACTIVITY_ID);
        
        //UNLIKE - 1 like
        likeService.unlikeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //get last notification created for receiver
        Alert lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        
        assertEquals(lastReceiverAlert.getStatusTypeCode(), StatusTypeCode.NEW.name());
        
        //get last notification created for submitter
        Alert lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();

        assertEquals(lastSubmitterAlert.getStatusTypeCode(), StatusTypeCode.NEW.name());
    }
    
    @Test
    public void test_unlikeActivity_notification_single_like_viewed() throws Exception {
        //LIKE
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //get reciever notification.
        Alert lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        //change alert status to viewed
        updateAlertsNewToViewed(TestConstants.PAX_MADRIDO, lastReceiverAlert.getId()); 
        
        //get submitter notification.
        Alert lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();
        //change alert status to viewed
        updateAlertsNewToViewed(TestConstants.PAX_OMARG, lastSubmitterAlert.getId()); 
                
        //UNLIKE - 1 like
        likeService.unlikeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //Notifications should have been deleted
        lastReceiverAlert = alertDao.findById(lastReceiverAlert.getId());
        lastSubmitterAlert = alertDao.findById(lastSubmitterAlert.getId());
        assertNull(lastReceiverAlert);
        assertNull(lastSubmitterAlert);
    }
    
    @Test
    public void test_unlikeActivity_notification_single_like_cancelled() throws Exception {
        //LIKE
        likeService.likeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //get reciever notification.
        Alert lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        //get submitter notification.
        Alert lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();
        
        //Save the alert IDs because they will be deleted
        Long lastReceiverAlertId = lastReceiverAlert.getId();
        Long lastSubmitterAlertId = lastSubmitterAlert.getId();
        
        //UNLIKE
        likeService.unlikeActivity(TestConstants.PAX_PORTERGA, NEWSFEED_ACTIVITY_ID);
        
        //LIKE by another pax
        likeService.likeActivity(LIKER_PAX_ID_2, NEWSFEED_ACTIVITY_ID);
        
        //the first alerts should have been deleted, and new alerts created for the new likes
        lastReceiverAlert = alertDao.findById(lastReceiverAlert.getId());
        lastSubmitterAlert = alertDao.findById(lastSubmitterAlert.getId());
        assertNull(lastReceiverAlert);
        assertNull(lastSubmitterAlert);
        
        //Find newly created like alerts
        lastReceiverAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_MADRIDO)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_RECEIVER.toString())
                .desc(STATUS_DATE).findOne();
        lastSubmitterAlert = alertDao.findBy()
                .where(ProjectConstants.PAX_ID).eq(TestConstants.PAX_OMARG)
                .and(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.LIKE_RECOGNITION_SUBMITTER.toString())
                .desc(STATUS_DATE).findOne();
        
        assertNotEquals(lastReceiverAlert.getId(), lastReceiverAlertId);
        assertNotEquals(lastSubmitterAlert.getId(), lastSubmitterAlertId);
    }
    
    private void updateAlertsNewToViewed(Long paxId, Long alertId) {
        List<AlertUpdateDTO> alertUpdateDTOs = new ArrayList<AlertUpdateDTO>(
                Arrays.asList(new AlertUpdateDTO(alertId, StatusTypeCode.VIEWED.toString())));
        
        alertService.updateAlerts(paxId, alertUpdateDTOs);

    }
    
}
