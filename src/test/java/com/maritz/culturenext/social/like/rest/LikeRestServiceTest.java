package com.maritz.culturenext.social.like.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.google.common.net.HttpHeaders;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class LikeRestServiceTest extends AbstractRestTest {
    
    private static final String URI_LIKING_ACTIVITY_57796 = "/rest/participants/~/activity/57796/likes";
    private static final String URI_LIKING_ACTIVITY_999999999 = "/rest/participants/~/activity/99999999/likes";
    private static final String URI_LIKING_PROGRAM_1 = "/rest/participants/~/programs/1/likes";
    private static final String URI_LIKING_PROGRAM_2 = "/rest/participants/~/programs/2/likes";
    private static final String URI_LIKING_ACTIVITY_57583 = "/rest/participants/~/activity/57583/likes";
    private static final String URI_LIKING_ACTIVITY_57581 = "/rest/participants/~/activity/57581/likes";
    private static final String URI_LIKING_ACTIVITY_PAGED_57581 = "/rest/participants/~/activity/57581/likes?page=1";
    private static final String URI_LIKING_PROGRAM_PAGED_1 = "/rest/participants/~/programs/1/likes?page=1";

    @Test
    public void post_like_activity_good() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void post_like_activity_not_exist() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_999999999)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }
    
    @Test
    public void post_like_activity_duplicate() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void post_like_program_good() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_1)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void post_like_program_not_exist() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_999999999)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }
    
    @Test
    public void post_like_program_duplicate() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_2)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_2)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void delete_unlike_activity() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
        mockMvc.perform(
                delete(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
    }

    @Test
    public void delete_unlike_activity_not_liked() throws Exception {
        mockMvc.perform(
                delete(URI_LIKING_ACTIVITY_57796)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }
  
    @Test
    public void delete_unlike_program() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_2)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
        mockMvc.perform(
                delete(URI_LIKING_PROGRAM_2)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
       ;
    }

    @Test
    public void delete_unlike_program_not_liked() throws Exception {
        mockMvc.perform(
                delete(URI_LIKING_PROGRAM_2)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void get_like_activity_good() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57583)
                        .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk())
        ;

        MvcResult result = mockMvc.perform(
                get(URI_LIKING_ACTIVITY_57583)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
               .andReturn()
       ;
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void get_like_program_good() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_1)
                        .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk())
        ;

        MvcResult result = mockMvc.perform(
                get(URI_LIKING_PROGRAM_1)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isOk())
               .andReturn()
       ;
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void test_create_like_for_activity_permission_denied() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_ACTIVITY_57581)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_create_like_for_program_permission_denied() throws Exception {
        mockMvc.perform(
                post(URI_LIKING_PROGRAM_1)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_delete_like_for_activity_permission_denied() throws Exception {
        mockMvc.perform(
                delete(URI_LIKING_ACTIVITY_57581)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_delete_like_for_program_permission_denied() throws Exception {
        mockMvc.perform(
                delete(URI_LIKING_PROGRAM_1)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_get_likes_for_activity_permission_denied() throws Exception {
        mockMvc.perform(
                get(URI_LIKING_ACTIVITY_PAGED_57581)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
    @Test
    public void test_get_likes_for_program_permission_denied() throws Exception {
        mockMvc.perform(
                get(URI_LIKING_PROGRAM_PAGED_1)
                    .with(authToken(TestConstants.USER_HAGOPIWL))
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    )
                .andExpect(status().isForbidden());
    }
    
}
