package com.maritz.culturenext.social.comment.services;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.Comment;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.social.comment.constants.CommentConstants;
import com.maritz.culturenext.social.comment.dto.CommentDTO;
import com.maritz.culturenext.social.comment.services.impl.CommentServiceImpl;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.util.PermissionUtil;
import com.maritz.test.AbstractMockTest;

public class CommentServiceTest extends AbstractMockTest {

    @InjectMocks private CommentServiceImpl commentService;
    @InjectMocks private PermissionUtil permissionUtil;
    @Mock private AlertService alertService;
    @Mock private ConcentrixDao<Alert> alertDao;
    @Mock private ConcentrixDao<Comment> commentDao;
    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private Environment environment;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private Security security;

    private final Long PAXID = 123L;
    private final Long OTHERPAXID = 1234L;
    private static final int ITERATIONS = 51;
    private int intervals = 5000;
    private Long testActivityId = 57581L;
    private Long testReportAbuseAdmin = 141L;
    private FindBy<Comment> commentFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Comment> commentCheckFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Alert> alertFindBy = PowerMockito.mock(FindBy.class);

    private final Long PAX_ID = 123L;
    private final Long MANAGER_PAX_ID = 456L;
    private final String VALID_TEXT = "Good Job!";
    private final String INVALID_TEXT =
            "Lorem ipsum dolor sit amet, ipsum feugiat eleifend commodo vel magna pede. Turpis conubia lorem " +
                    "urna velit eget, egestas arcu elit sapien, non nec eros. Ullamcorper et dignissim ipsum " +
                    "sodales venenatis, justo integer porta, mauris sem wisi magna nunc a. Nec per metus eget " +
                    "lorem vel semper, phasellus eros, consequat justo auctor commodo esse vehicula, consequat " +
                    "massa congue nec arcu ante, turpis nascetur vulputate vitae volutpat. Eget vitae lorem " +
                    "eget iaculis vivamus, at rutrum lectus habitant risus gravida, curabitur lorem ac orci. " +
                    "Ullamcorper felis augue ipsum nisl ac mattis, integer vehicula dolore lectus diam metus, " +
                    "vitae per. Eu risus auctor, ullamcorper vestibulum donec luctus non at luctus, nisl quam " +
                    "odio, vestibulum ut suspendisse posuere. Vitae lorem, eu amet, amet vestibulum etiam erat " +
                    "feugiat lacinia. Amet nunc, semper blandit quis nec adipiscing, arcu blandit placerat, et " +
                    "leo consequat, aut non in orci. Nunc non.";
    private final String TARGET_TABLE = "NEWSFEED_ITEM";
    private final Long TARGET_ID = 567L;
    private final Long COMMENT_ID = 890L;
    private final Long REPORTED_COMMENT_ID = 891L;
    private final Long OTHERCOMMENTID = 8901L;
    private final Long ACTIVITY_ID = 456L;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        Date date = new Date();

        //Mock comment db object
        Comment comment = new Comment();
        comment.setId(COMMENT_ID);
        comment.setTargetId(TARGET_ID);
        comment.setTargetTable(TARGET_TABLE);
        comment.setComment(VALID_TEXT);
        comment.setPaxId(PAXID);
        comment.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        comment.setCommentDate(date);

        //Mock Reported Comment DB object
        Comment reportedComment = new Comment();
        reportedComment.setId(REPORTED_COMMENT_ID);
        reportedComment.setTargetId(TARGET_ID);
        reportedComment.setTargetTable(TARGET_TABLE);
        reportedComment.setComment(VALID_TEXT);
        reportedComment.setPaxId(PAXID);
        reportedComment.setStatusTypeCode(CommentConstants.REPORTED_STATUS_CODE);
        reportedComment.setCommentDate(date);

        //Mock out user
        EmployeeDTO userPax = new EmployeeDTO();

        String languageCode = "en";
        String preferredLocale = "en_US";
        String controlNum = "s345";
        String firstName = "Kramer";
        String middleName = "M";
        String lastName = "Martin";
        Integer profilePictureVersion = 1;
        String status = "ACTIVE";
        String jobTitle = "Developer II";

        userPax.setPaxId(PAX_ID);
        userPax.setLanguageCode(languageCode);
        userPax.setPreferredLocale(preferredLocale);
        userPax.setControlNum(controlNum);
        userPax.setFirstName(firstName);
        userPax.setMiddleName(middleName);
        userPax.setLastName(lastName);
        userPax.setProfilePictureVersion(profilePictureVersion);
        userPax.setStatus(status);
        userPax.setJobTitle(jobTitle);
        userPax.setManagerPaxId(MANAGER_PAX_ID);

        // Mock out ApplicationData entity
        ApplicationData testApplicationData = new ApplicationData();
        testApplicationData.setValue(testReportAbuseAdmin.toString());


        PowerMockito.when(participantInfoDao.getEmployeeDTO(PAX_ID)).thenReturn(userPax);
        PowerMockito.when(commentDao.findById(COMMENT_ID)).thenReturn(comment);
        PowerMockito.when(commentDao.findById(REPORTED_COMMENT_ID)).thenReturn(reportedComment);
        PowerMockito.when(!security.isMyPax(PAX_ID)).thenReturn(true);
        PowerMockito.when(security.getPaxId()).thenReturn(PAX_ID);
        Mockito.doNothing().when(commentDao).update(comment);
        Mockito.doNothing().when(commentDao).update(reportedComment);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ((Comment)args[0]).setId(COMMENT_ID);
                return null;
            }
        }).when(commentDao).save(Matchers.any(Comment.class));

        List<Comment> commentItemList = new ArrayList<>();
        Long id = 0L;
        for (int i = 0; i < ITERATIONS; i++) {
            Comment currentNode = new Comment();
            currentNode.setId(id);

            // Adding more time to DAO for testing
            Date today = new Date();
            Date later = new Date(today.getTime()+ intervals);
            intervals += 1000000000;

            // Setting in Node
            currentNode.setCommentDate(later);
            commentItemList.add(currentNode);
            id += 1;
        }
        // commentDao.findBy().where("targetId").eq(activityID).desc("commentDate").find();
        PowerMockito.when(commentDao.findBy()).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.where(ProjectConstants.TARGET_ID)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(testActivityId)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.and(ProjectConstants.STATUS_TYPE_CODE)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.desc(ProjectConstants.COMMENT_DATE)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.find()).thenReturn(commentItemList);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN))
                .thenReturn(String.valueOf(testReportAbuseAdmin));
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        NewsfeedItem nfi = new NewsfeedItem();
        nfi.setId(TestConstants.ID_1);
        nfi.setNewsfeedItemTypeCode(AlertSubType.RECOGNITION.toString());
        PowerMockito.when(newsfeedItemDao.findById(anyLong())).thenReturn(nfi);

        PowerMockito.when(commentFindBy.where(ProjectConstants.TARGET_TABLE)).thenReturn(commentCheckFindBy);
        PowerMockito.when(commentCheckFindBy.eq(anyString())).thenReturn(commentCheckFindBy);
        PowerMockito.when(commentCheckFindBy.eq(anyLong())).thenReturn(commentCheckFindBy);
        PowerMockito.when(commentCheckFindBy.and(anyString())).thenReturn(commentCheckFindBy);
        PowerMockito.when(commentCheckFindBy.eq(anyString())).thenReturn(commentCheckFindBy);
        PowerMockito.when(commentCheckFindBy.findOne()).thenReturn(new Comment());


        PowerMockito.when(alertDao.findBy()).thenReturn(alertFindBy);

        PowerMockito.when(alertFindBy.where(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.and(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.in(Matchers.<String>anyVararg())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.find()).thenReturn(new ArrayList<Alert>());

    }

    @Test
    public void test_create_comment_on_activity() {
        //Mock objects
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText(VALID_TEXT);

        CommentDTO commentResponse = commentService.createCommentOnActivity(PAX_ID, TARGET_ID, commentDTO);

        assertTrue(commentResponse.getId().equals(COMMENT_ID));
        assertTrue(commentResponse.getText().equals(commentDTO.getText()));
        assertTrue(commentResponse.getPax().getPaxId().equals(PAX_ID));
        assertTrue(commentResponse.getPax().getManagerPaxId().equals(MANAGER_PAX_ID));
    }

    @Test
    public void test_throw_exception_when_activity_comment_over_limit() {
        //Mock objects
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText(INVALID_TEXT);
        thrown.expect(ErrorMessageException.class);

        commentService.createCommentOnActivity(PAXID, TARGET_ID, commentDTO);
    }

    // Testing Update Comment
    @Test
    public void test_update_comment_happy_path() {
        // Initialize
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setText(VALID_TEXT);
        commentDTO.setId(COMMENT_ID);
        commentDTO.setStatus(StatusTypeCode.ACTIVE.name());

        CommentDTO commentResponse = commentService.updateComment(PAXID, COMMENT_ID, commentDTO);

        assertTrue(commentResponse.getText().equals(VALID_TEXT));
        assertTrue(commentResponse.getId().equals(COMMENT_ID));
        assertTrue(commentResponse.getPax().getPaxId().equals(PAXID));
        assertTrue(commentResponse.getPax().getManagerPaxId().equals(MANAGER_PAX_ID));

    }

    @Test
    public void test_update_comment_invalid_comment_id () {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(COMMENT_ID);
        commentDTO.setText(VALID_TEXT);

        try {
            commentService.updateComment(PAXID, OTHERCOMMENTID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_update_comment_null_comment_id () {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(COMMENT_ID);
        commentDTO.setText(VALID_TEXT);

        try {
            commentService.updateComment(PAXID, null, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_update_comment_pax_id_validity () {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(COMMENT_ID);
        commentDTO.setText(VALID_TEXT);

        try {
            commentService.updateComment(OTHERPAXID, COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    //User status Testing
    @Test
    public void test_update_comment_change_status_non_admin() {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(StatusTypeCode.ACTIVE.name());

        try {
            commentService.updateComment(OTHERPAXID, COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_update_comment_reported_to_abusive() {
        // First try as non-report abuse admin
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(REPORTED_COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE);

        try {
            commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        // Second, try as the report abuse admin
        PowerMockito.when(security.getPaxId()).thenReturn(testReportAbuseAdmin);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Comment comment = (Comment)args[0];
                assertThat(comment.getAdminPaxId(), is(testReportAbuseAdmin));
                assertNotNull(comment.getAdminUpdateTime());
                return null;
            }
        }).when(commentDao).update(Matchers.any(Comment.class));

        CommentDTO adminResponse = commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);

        assertThat(adminResponse.getStatus(), is(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE));
        Mockito.verify(commentDao, times(1)).update(Matchers.any(Comment.class));
    }

    @Test
    public void test_update_comment_reported_to_abusive_when_commenting_not_enabled() {
        // Commenting enabled turned off.
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

        // First try as non-report abuse admin
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(REPORTED_COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE);

        try {
            commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        // Second, try as the report abuse admin
        PowerMockito.when(security.getPaxId()).thenReturn(testReportAbuseAdmin);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Comment comment = (Comment)args[0];
                assertThat(comment.getAdminPaxId(), is(testReportAbuseAdmin));
                assertNotNull(comment.getAdminUpdateTime());
                return null;
            }
        }).when(commentDao).update(Matchers.any(Comment.class));

        CommentDTO adminResponse = commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);

        assertThat(adminResponse.getStatus(), is(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE));
        Mockito.verify(commentDao, times(1)).update(Matchers.any(Comment.class));
    }

    @Test
    public void test_update_comment_reported_to_active() {
        // First try as non-report abuse admin
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(REPORTED_COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(StatusTypeCode.ACTIVE.toString());

        List<Comment> commentList = new ArrayList<>();
        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        commentList.add(comment);

        // commentDao.findBy().where("targetTable").eq("NEWSFEED_ITEM").and("targetId").eq(activityID).find();
        PowerMockito.when(commentDao.findBy()).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.where(ProjectConstants.TARGET_TABLE)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.and(ProjectConstants.TARGET_ID)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(anyLong())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.find()).thenReturn(commentList);

        List<Alert> alertList = new ArrayList<>();
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_RECEIVER.toString()));
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_SUBMITTER.toString()));

        PowerMockito.when(alertFindBy.find()).thenReturn(alertList);
        PowerMockito.doNothing().when(alertDao).save(Matchers.any(Alert.class));

        try {
            commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        // Second, try as the report abuse admin
        PowerMockito.when(security.getPaxId()).thenReturn(testReportAbuseAdmin);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Comment comment = (Comment)args[0];
                assertThat(comment.getAdminPaxId(), is(testReportAbuseAdmin));
                assertNotNull(comment.getAdminUpdateTime());
                return null;
            }
        }).when(commentDao).update(Matchers.any(Comment.class));

        CommentDTO adminResponse = commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);

        assertThat(adminResponse.getStatus(), is(StatusTypeCode.ACTIVE.name()));
        Mockito.verify(commentDao, times(1)).update(Matchers.any(Comment.class));
        Mockito.verify(alertDao, times(2)).save(Matchers.any(Alert.class));
    }

    @Test
    public void test_update_comment_reported_to_active_when_commenting_not_enabled() {
        // Commenting enabled turned off.
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

        // First try as non-report abuse admin
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(REPORTED_COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(StatusTypeCode.ACTIVE.name());

        try {
            commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        // Second, try as the report abuse admin
        PowerMockito.when(security.getPaxId()).thenReturn(testReportAbuseAdmin);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Comment comment = (Comment)args[0];
                assertThat(comment.getAdminPaxId(), is(testReportAbuseAdmin));
                assertNotNull(comment.getAdminUpdateTime());
                return null;
            }
        }).when(commentDao).update(Matchers.any(Comment.class));

        CommentDTO adminResponse = commentService.updateComment(PAXID, REPORTED_COMMENT_ID, commentDTO);

        assertThat(adminResponse.getStatus(), is(StatusTypeCode.ACTIVE.name()));
        Mockito.verify(commentDao, times(1)).update(Matchers.any(Comment.class));
    }

    @Test
    public void test_update_comment_active_to_abuse_confirmed() {
        // First try as non-report abuse admin
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(COMMENT_ID);
        commentDTO.setText(VALID_TEXT);
        commentDTO.setStatus(CommentConstants.ABUSE_CONFIRMED_STATUS_CODE);

        try {
            commentService.updateComment(OTHERPAXID, COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

        // Second, try as the report abuse admin
        PowerMockito.when(security.getPaxId()).thenReturn(testReportAbuseAdmin);

        try {
            commentService.updateComment(PAXID, COMMENT_ID, commentDTO);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    // Get Comments For Activity Testing
     @Test
     public void test_pagination_page_3_size_25() {
         // With 51 records, the third page of size 25 should have 1 record left
         List<CommentDTO> commentDTOList = commentService.getCommentsForActivityCurrent(3,25,testActivityId,TestConstants.PAX_DAGARFIN);

         assertThat(commentDTOList.size(), equalTo(1));
         Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
         Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
         Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
     }
     @Test
     public void test_pagination_page_3_size_20() {
         // Third page of 51 should have 11 records after 40 have been returned
         List<CommentDTO> commentDTOList = commentService.getCommentsForActivityCurrent(3,20,testActivityId,TestConstants.PAX_DAGARFIN);

         assertThat(commentDTOList.size(), equalTo(11));
         Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
         Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
         Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
     }
     @Test
     public void test_pagination_page_4_size_20() {
         // Fourth page should have no records left
         List<CommentDTO> commentDTOList = commentService.getCommentsForActivityCurrent(4,20,testActivityId,TestConstants.PAX_DAGARFIN);

         assertThat(commentDTOList.size(), equalTo(0));
         Mockito.verify(commentFindBy, times(1)).and(ProjectConstants.STATUS_TYPE_CODE);
         Mockito.verify(commentFindBy, times(1)).eq(StatusTypeCode.ACTIVE.name());
         Mockito.verify(commentFindBy, times(1)).desc(ProjectConstants.COMMENT_DATE);
     }

    @Test
    public void test_delete_throw_exception_when_nonexistent_comment() {
        PowerMockito.when(commentDao.findById(COMMENT_ID)).thenReturn(null);

        try {
            commentService.deleteCommentOnActivity(PAX_ID, ACTIVITY_ID, COMMENT_ID);
            fail();
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_delete_happy_path() {
        Comment comment = new Comment();
        comment.setPaxId(PAX_ID);

        PowerMockito.when(commentDao.findById(COMMENT_ID)).thenReturn(comment);
        PowerMockito.doNothing().when(commentDao).delete(comment);

        commentService.deleteCommentOnActivity(PAX_ID, ACTIVITY_ID, COMMENT_ID);

        Mockito.verify(commentDao, times(1)).delete(Matchers.any(Comment.class));
    }

    @Test
    public void test_getReportAbuseAdminPaxId_null() {
        // Commenting enabled turned off.
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN))
                .thenReturn(null);

        Long response = commentService.getReportAbuseAdminPaxId();

        assertNull(response);
    }

    @Test
    public void test_getReportAbuseAdminPaxId_valid() {
        Long response = commentService.getReportAbuseAdminPaxId();

        assertThat(response, is(141L));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_comment_fails_commenting_not_enabled() {

        // Commenting enabled turned off.
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(Boolean.FALSE.toString());

        commentService.updateComment(PAXID, COMMENT_ID, new CommentDTO());

    }

    @Test
    public void test_deleteOnlyComment() {
        PowerMockito.when(commentCheckFindBy.findOne()).thenReturn(null);

        List<Alert> alertList = new ArrayList<>();
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_RECEIVER.toString()));
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_SUBMITTER.toString()));

        PowerMockito.when(alertFindBy.find()).thenReturn(alertList);
        PowerMockito.doNothing().when(alertDao).delete(Matchers.any(Alert.class));

        commentService.deleteCommentOnActivity(PAX_ID, ACTIVITY_ID, COMMENT_ID);
        Mockito.verify(alertDao, times(2)).delete(Matchers.any(Alert.class));

    }

    @Test
    public void test_reportOnlyComment() {
        PowerMockito.when(commentCheckFindBy.findOne()).thenReturn(null);

        List<Comment> commentList = new ArrayList<>();
        Comment comment = new Comment();
        comment.setId(TestConstants.ID_1);
        comment.setStatusTypeCode(StatusTypeCode.REPORTED.name());
        commentList.add(comment);

        // commentDao.findBy().where("targetTable").eq("NEWSFEED_ITEM").and("targetId").eq(activityID).find();
        PowerMockito.when(commentDao.findBy()).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.where(ProjectConstants.TARGET_TABLE)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(anyString())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.and(ProjectConstants.TARGET_ID)).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.eq(anyLong())).thenReturn(commentFindBy);
        PowerMockito.when(commentFindBy.find()).thenReturn(commentList);

        List<Alert> alertList = new ArrayList<>();
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_RECEIVER.toString()));
        alertList.add(new Alert().setAlertSubTypeCode(AlertSubType.COMMENT_RECOGNITION_SUBMITTER.toString()));

        PowerMockito.when(alertFindBy.find()).thenReturn(alertList);
        PowerMockito.doNothing().when(alertDao).save(Matchers.any(Alert.class));

        CommentDTO commentRequest = new CommentDTO();
        commentRequest.setPax(new EmployeeDTO().setPaxId(1L));
        commentRequest.setStatus(CommentConstants.REPORTED_STATUS_CODE);
        commentRequest.setTime(DateUtil.getCurrentTimeInUTCDateTime());

        commentService.reportComment(MANAGER_PAX_ID, ACTIVITY_ID, COMMENT_ID, commentRequest);
        Mockito.verify(alertDao, times(2)).save(Matchers.any(Alert.class));

    }

}
