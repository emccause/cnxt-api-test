package com.maritz.culturenext.social.comment.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class CommentRestServiceTest extends AbstractRestTest {

    @Inject private ApplicationDataService applicationDataService;
    
    private long testPaxId = 12698;
    private long activityId = 57581;
    private String prependedPath = "/rest/participants/";
    private String testReportAbuseAdmin = "141";

    @Test
    public void test_get_comment_list_all_params_good() throws Exception {
        mockMvc.perform(
                get(prependedPath + testPaxId + "/activity/" + activityId + "/comments" +
                        "?page[number]=1&page[size]=20")
                        .with(authToken(TestConstants.USER_DAGARFIN))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_get_comment_list_default_good() throws Exception {
        mockMvc.perform(
                get(prependedPath + testPaxId + "/activity/" + activityId + "/comments")
                        .with(authToken(TestConstants.USER_DAGARFIN))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_get_comment_list_default_page_good() throws Exception {
        mockMvc.perform(
                get(prependedPath + testPaxId + "/activity/" + activityId + "/comments" +
                        "?page[number]=1")
                        .with(authToken(TestConstants.USER_DAGARFIN))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_get_comment_list_empty_activity_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/12698/activity//comments?page=[number]1")
                        .with(authToken(TestConstants.USER_DAGARFIN))
        )
                .andExpect(status().isNotFound())
        ;
    }

    // Testing the rest service for update comment
    @Test
    public void test_update_comment_id_mismatch_type() throws Exception {
        mockMvc.perform(
                put("/rest/participants/12698/activity/57581/comments/5")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":5," + 
                                "\"text\":\"Great Work on project Test Put 5\"," +
                                "\"status\":\"ACTIVE\"" +
                                "}")
        )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void test_update_comment_id_invalid() throws Exception {
        mockMvc.perform(
                put("/rest/participants/12698/activity/57581/comments/3")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":5," +
                                "\"text\":\"Great Work on project Test Put 5\"," + 
                                "\"status\":\"ACTIVE\"" +
                                "}")
        )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void test_update_comment_from_reported_to_active_non_admin() throws Exception {
        mockMvc.perform(
                put("/rest/participants/11034/activity/57581/comments/78")
                        .with(authToken(TestConstants.USER_WRIGHTMA))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" + 
                                "\"id\":78," +
                                "\"text\":\"Test Comment\"," + 
                                "\"status\":\"ACTIVE\"" + 
                                "}")
        )
                .andExpect(status().isForbidden())
        ;
    }

    @Test
    public void test_update_comment_from_reported_to_active_abuse_admin() throws Exception {
        // Set kilinskis.admin as reportAbuseAdmin
        ApplicationDataDTO appData = 
                applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN);
        appData.setValue(testReportAbuseAdmin);
        applicationDataService.setApplicationData(appData);

        mockMvc.perform(
                put("/rest/participants/12696/activity/57581/comments/78")
                        .with(authToken("kilinskis.admin"))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" + 
                                "\"id\":78," +
                                "\"text\":\"Test Comment\"," + 
                                "\"status\":\"ACTIVE\"" + 
                                "}")
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_update_comment_from_reported_to_active_non_abuse_admin() throws Exception {
        mockMvc.perform(
                put("/rest/participants/12696/activity/1/comments/78")
                        .with(authToken("wrightma.admin"))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":78," + 
                                "\"text\":\"Test Comment\"," +
                                "\"status\":\"ACTIVE\"" + 
                                "}")
        )
                .andExpect(status().isForbidden())
        ;
    }

    @Test
    public void test_update_comment_from_active_to_reported() throws Exception {
        mockMvc.perform(
                put("/rest/participants/12698/activity/57581/comments/66")
                        .with(authToken(TestConstants.USER_DAGARFIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":66," +
                                "\"text\":\"Great work on project X REPORTED\"," +
                                "\"status\":\"REPORTED\"" +
                                "}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void test_update_comment_from_active_to_reported_self() throws Exception {
        mockMvc.perform(
                put("/rest/participants/11034/activity/57581/comments/66")
                        .with(authToken(TestConstants.USER_WRIGHTMA))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":66," +
                                "\"text\":\"Great work on project X REPORTED\"," + 
                                "\"status\":\"REPORTED\"" +
                                "}")
        )
                .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void test_get_comment_list_permission_denied() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/activity/57581/comments?page=1")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        )
                .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void test_update_comment_permission_denied() throws Exception {
        mockMvc.perform(
                put("/rest/participants/~/activity/57581/comments/4")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"id\":66," +
                                "\"text\":\"Updating a comment!\"," +
                                "\"status\":\"ACTIVE\"" +
                                "}")
        )
                .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void test_create_comment_permission_denied() throws Exception {
        mockMvc.perform(
                post("/rest/participants/~/activity/57581/comments")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{" +
                                "\"text\":\"Creating a comment\"" +
                                "}")
        )
                .andExpect(status().isForbidden())
        ;
    }
    
    @Test
    public void test_delete_comment_permission_denied() throws Exception {
        mockMvc.perform(
                delete("/rest/participants/~/activity/57581/comments/6")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        )
                .andExpect(status().isForbidden())
        ;
    }
}
