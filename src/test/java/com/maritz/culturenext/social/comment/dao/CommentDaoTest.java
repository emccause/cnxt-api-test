package com.maritz.culturenext.social.comment.dao;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.social.comment.dao.CommentDao;
import com.maritz.test.AbstractDatabaseTest;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CommentDaoTest extends AbstractDatabaseTest {
    
    @Inject private CommentDao actualCommentDao;

    //First row in the result set - pulled from test DB
    public static final Long COMMENT_ID = 77L;
    public static final String COMMENT_TEXT = "Good Job!";
    public static final Long PAX_ID = 165L;
    public static final String PAX_LANGUAGE_CODE = "en";
    public static final String CONTROL_NUM = "165";
    public static final String FIRST_NAME = "Dezmond";
    public static final String LAST_NAME = "Moore";
    public static final String COMPANY_NAME = "MM";
    public static final String JOB_TITLE = "m365 Team Member";
    public static final Long SYS_USER_ID = 165L;
    
    private Long testActivityId = 926L;
    private Long testActivityId2 = 57581L;
    
    // Get Comments For Activity Testing
    @Test
    public void get_comments_for_activity_test_good() throws Exception {

        List<Map<String, Object>> nodes = actualCommentDao.getCommentsForActivity(1, 50, testActivityId2, TestConstants.ID_2);

        assertTrue(nodes.size() > 0);

        Map<String, Object> result1 = nodes.get(0);
        
        //iterate to the required Comment
        for (Map<String, Object> result2 : nodes) {
            if (COMMENT_ID.equals(result2.get(ProjectConstants.COMMENT_ID))) { result1 = result2; break; }
        }
        
        //Only verify the first record of the result set
        assertNotNull(result1);
        assertEquals(COMMENT_ID, result1.get(ProjectConstants.COMMENT_ID));
        assertEquals(COMMENT_TEXT, result1.get(ProjectConstants.COMMENT_TEXT));
        assertEquals(StatusTypeCode.ACTIVE.name(), result1.get(ProjectConstants.COMMENT_PAX_STATUS));
        assertEquals(PAX_ID, result1.get(ProjectConstants.PAX_ID));
        assertEquals(PAX_LANGUAGE_CODE, result1.get(ProjectConstants.LANGUAGE_CODE));
        assertEquals(LocaleCodeEnum.en_US.name(), result1.get(ProjectConstants.PREFERRED_LOCALE));
        assertEquals(LocaleCodeEnum.en_US.name(), result1.get(ProjectConstants.LANGUAGE_LOCALE));
        assertEquals(CONTROL_NUM, result1.get(ProjectConstants.CONTROL_NUM));
        assertEquals(FIRST_NAME, result1.get(ProjectConstants.FIRST_NAME));
        assertEquals(LAST_NAME, result1.get(ProjectConstants.LAST_NAME));
        assertEquals(COMPANY_NAME, result1.get(ProjectConstants.COMPANY_NAME));
        assertEquals(JOB_TITLE, result1.get(ProjectConstants.JOB_TITLE));
        assertEquals(SYS_USER_ID, result1.get(ProjectConstants.SYS_USER_ID));
        assertEquals(StatusTypeCode.ACTIVE.name(), result1.get(ProjectConstants.STATUS_TYPE_CODE));
        assertEquals(TestConstants.PAX_PORTERGA, result1.get(ProjectConstants.MANAGER_PAX_ID));
    }
    
    @Test
    public void test_pagination_size_7_activity_id_926() {
        // With 27 records, the fourth page of size 7 should have 6 records
        List<Map<String, Object>> commentDTOList = actualCommentDao.getCommentsForActivity(4, 7, testActivityId, TestConstants.ID_2);
        
        assertThat(commentDTOList.size(), equalTo(6));
    }
    @Test
    public void test_pagination_size_20_activity_id_926() {
        // With 27 records, the second page of size 20 should have 7 records
        List<Map<String, Object>> commentDTOList = actualCommentDao.getCommentsForActivity(2, 20, testActivityId, TestConstants.ID_2);

        assertThat(commentDTOList.size(), equalTo(7));
    }
    @Test
    public void test_pagination_size_30_activity_id_926() {
        // With 27 records, the second page of size 30 should have no records
        List<Map<String, Object>> commentDTOList = actualCommentDao.getCommentsForActivity(3, 30, testActivityId, TestConstants.ID_2);

        assertThat(commentDTOList.size(), equalTo(0));
    }
    
    @Test
    public void get_comments_verify_comment_status() throws Exception {

        List<Map<String,Object>> nodes = actualCommentDao.getCommentsForActivity(1, 5, testActivityId, TestConstants.ID_2);

        assertTrue(nodes.size() > 0);

        //Verify that all returned records are ACTIVE
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.COMMENT_PAX_STATUS), StatusTypeCode.ACTIVE.name());
        }
    }
}