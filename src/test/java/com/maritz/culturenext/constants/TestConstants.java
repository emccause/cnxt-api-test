package com.maritz.culturenext.constants;

import java.util.Date;

public class TestConstants {

    //Regular Users
    public static final String USER_ABBOTTM = "abbottm";
    public static final String USER_ABRASSSL = "abrasssl";
    public static final String USER_KUSEY = "adam.kusey";
    public static final String USER_KUSEY_ADMIN = "adam.kusey.admin";
    public static final String USER_ADAM_SMITH = "adam.smith.admin";
    public static final String USER_ADMIN = "admin";
    public static final String USER_ALGELD = "algeld";
    public static final String USER_BECKETDK = "becketdk";
    public static final String USER_BELLWE = "bellwe";
    public static final String USER_BOHMJL_ADMIN = "bohmjl_admin";
    public static final String USER_CARRAM = "carram";
    public static final String USER_COOPERJX = "cooperjx";
    public static final String USER_DAGARFIN = "dagarfin";
    public static final String USER_DAGARFIN_ADMIN = "dagarfin.admin";
    public static final String USER_DAYSTB_ADMIN = "daystb.admin";
    public static final String USER_FOLEYJM = "foleyjm";
    public static final String USER_GARCIAF2 = "garciaf2";
    public static final String USER_HANDERJ = "handerj";
    public static final String USER_HAGOPIWL = "hagopiwl";
    public static final String USER_HAENNICJ = "haennicj";
    public static final String USER_HARWELLM = "harwellm";
    public static final String USER_KILINSKIS_ADMIN = "kilinskis.admin";
    public static final String USER_KUMARSJ = "kumarsj";
    public static final String USER_KUMARSJ_ADMIN = "kumarsj_admin";
    public static final String USER_MADRIDO = "madrido";
    public static final String USER_MENDELLL = "mendelll";
    public static final String USER_MUDDAM = "muddam";
    public static final String USER_PORTERGA = "porterga";
    public static final String USER_RIDERAC1 = "riderac1";
    public static final String USER_SELLSAA = "sellsaa";
    public static final String USER_VANLOOWA = "vanloowa";
    public static final String USER_WILSONEA = "wilsonea";
    public static final String USER_WRIGHTMA = "wrightma";
    public static final String USER_WRIGHTMA_ADMIN = "wrightma.admin";
    public static final String USER_MCVEYCC_ADMIN_ADMIN = "mcveycc_admin";

    //Invalid User
    public static final String USER_INVALID = "invalid";

    //Speetra Account
    public static final String USER_PULSEM = "pulseM";

    //Special Cases
    public static final String USER_ADKINSJL = "adkinsjl"; //User with no email
    public static final String USER_ISMAILJ = "ismailj"; //Gets CHANGE_PASSWORD event (no SYS_USER_PASSOWRD_DATE)
    public static final String USER_ROHLFFDA = "rohlffda"; //NEW user
    public static final String USER_RUSKLA = "ruskla"; // user for alert tests

    //Pax Ids
    public static final Long PAX_ABBOTTM = 5564L;
    public static final Long PAX_ADAM_SMITH = 5166L;
    public static final Long PAX_ADAM_SMITH_ADMIN = 5352L;
    public static final Long PAX_ADMIN = 2L;
    public static final Long PAX_DAGARFIN = 12698L;
    public static final Long PAX_HAGOPIWL = 4103L;
    public static final Long PAX_HARWELLM = 8571L;
    public static final Long PAX_KILINSKIS = 141L;
    public static final Long PAX_KUMARSJ = 969L;
    public static final Long PAX_KUSEY = 7213L;
    public static final Long PAX_KUSEY_ADMIN = 8259L;
    public static final Long PAX_MADRIDO = 12289L;
    public static final Long PAX_MUDDAM = 8987L;
    public static final Long PAX_OMARG = 2297L;
    public static final Long PAX_PORTERGA = 847L;
    public static final Long PAX_VANLOOWA = 6577L;

    //PAX Control Number
    public static final String PAX_KUSEY_ADMIN_CONTROL_NUM = "8259";

    //PaxGroup Ids
    public static final Long PAX_GROUP_PORTERGA = 847L;
    public static final Long PAX_GROUP_HARWELLM = 8572L;

    //Email Addresses
    public static final String EMAIL_DEFAULT = "a@b.c";
    public static final String EMAIL_DEFAULT_INVALID = "a @b.c";
    public static final String EMAIL_GLYNNKM = "kristina.glynn@maritz.com";
    public static final String EMAIL_HARWELLM = "leah.harwell@maritz.com";

    //Random Ids we use all the time..?
    public static final Long ID_1 = 1L;
    public static final Long ID_2 = 2L;
    public static final Long ID_3 = 3L;
    public static final Long ID_4 = 4L;
    public static final Long ID_9 = 9L;
    public static final Long ID_11 = 11L;
    public static final Long ID_14 = 14L;
    public static final Long ID_MAX_VALUE = Long.MAX_VALUE;

    //Programs
    public static final Long PROGRAM_ID_NO_BUDGETS = 36L;
    public static final Long PROGRAM_INACTIVE = 16L;
    public static final Long PROGRAM_ENDED = 4L;
    public static final Long PROGRAM_SCHEDULED = 10L;
    // Program that works with PAX_HARWELLM as a giver and PAX_MUDDAM as a receiver
    public static final Long PROGRAM_RECEIVERS_HAPPY_PATH = 13579L;

    //Other commonly used data
    public static final String COMMENT = "A comment";
    public static final String DEFAULT_PASSWORD = "abcd1234";
    public static final String FUTURE_VERSION = "future";
    public static final String HEADLINE = "A headline";
    public static final String PROJECT_NUMBER = "S01234";

    // System properties
    public static final String CATALINA_BASE = "catalina.base";
    public static final String USER_DIR = "user.dir";

    // Color values
    public static final String PRIMARY_COLOR_VALUE = "#33FDFF";
    public static final String SECONDARY_COLOR_VALUE = "#FFFFFF";
    public static final String TEXT_ON_PRIMARY_COLOR_VALUE = "#000000";

    //Award Types
    public static final String CASH = "CASH";
    public static final String DPP = "DPP";
    public static final String SAA = "SAA";

    //Program Misc
    public static final String VF_NAME = "ROUTE_NEXT_LEVEL_MANAGER";
    public static final String DUMMY = "DUMMY";
    public static final String MISC_DATA = "2002";

    //Addrress
    public static final String ADDRESS_TEST_CONTROL_NUMBER = "969";
    public static final String ADDRESS_TEST_BAD_CONTROL_NUMBER = "BAD";
    public static final String ADDRESS_TEST_FAIL_NO_CONTROL_NUMBER = "";
    public static final String ADDRESS_TEST_FAIL_NO_ADDRESS_ID = "";
    public static final Long ADDRESS_TEST_ADDRESS_ID = 873L;

    //Email
    public static final Integer TEST_PAGE_SIZE = 1;
    public static final Integer TEST_PAGE_NUMBER = 10;
    public static final String TEST_DATE =  "7/20/2021";
    public static final String TEST_SUBJECT = "Test Subject";
    public static final String TEST_EMAIL_TYPE = "BUS";

}
