package com.maritz.culturenext.permission.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.dto.PermissionRoleDTO;
import com.maritz.core.security.authorization.AuthorizationService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.ProxyDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.test.AbstractRestTest;

public class ProxyRestTest extends AbstractRestTest {

    @Inject private AuthorizationService authorizationService;

    @Before
    public void setup() {
        PermissionRoleDTO permissionRoleDto = new PermissionRoleDTO();
        permissionRoleDto.setRole("RECG");
        permissionRoleDto.setPermission("ASSIGN_PROXY_SELF");
        authorizationService.grantPermission(permissionRoleDto);
    }

    @Test
    public void get_proxies_no_type() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/proxies")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }

    @Test
    public void get_proxies_single_type() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/proxies?type=MY_PROXIES")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }

    @Test
    public void get_proxies_both_types() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/proxies?type=MY_PROXIES,PROXY_AS")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk())
                ;
    }

    @Test
    public void post_proxy() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8571L));
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }

        proxyDto.setPermissions(permissionList);

        mockMvc.perform(
                post("/rest/participants/~/proxies")
                .with(authToken("muddam"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(proxyDto))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void put_proxy() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8987L));
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }

        proxyDto.setPermissions(permissionList);

        mockMvc.perform(
                put("/rest/participants/~/proxies/8987")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(proxyDto))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void patch_proxy() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8987L));
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }

        proxyDto.setPermissions(permissionList);

        mockMvc.perform(
                patch("/rest/participants/~/proxies/8987")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(proxyDto))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void delete_proxy() throws Exception {
        mockMvc.perform(
                delete("/rest/participants/~/proxies/8987")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isOk());
    }

    @Test
    public void post_proxy_access_denied() throws Exception {
        PermissionRoleDTO permissionRoleDto = new PermissionRoleDTO();
        permissionRoleDto.setRole("RECG");
        permissionRoleDto.setPermission("ASSIGN_PROXY_SELF");
        authorizationService.revokePermission(permissionRoleDto);

        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8571L));
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }

        proxyDto.setPermissions(permissionList);

        mockMvc.perform(
                post("/rest/participants/~/proxies")
                .with(authToken("muddam"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(proxyDto))
                )
                .andExpect(status().isForbidden());
    }

    @Test
    public void get_proxy_for_specific_pax_not_impersonated() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/proxies/permissions")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isForbidden())
                ;
    }

    @Test
    public void get_proxy_for_specific_pax_invalid_type() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/proxies?type=INVALID_TYPE")
                .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isBadRequest())
                ;
    }

    @Test
    public void get_proxy_for_specific_pax_all_statuses() throws Exception {
        List<ProxyDTO> proxyList = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get("/rest/participants/~/proxies")
                        .with(authToken(TestConstants.USER_HARWELLM))
                        )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                        ,ArrayList.class
                );
        assertFalse("Should have results", proxyList.isEmpty());
    }

    @Test
    public void get_proxy_for_specific_pax_no_results() throws Exception {
        List<ProxyDTO> proxyList = ObjectUtils.jsonToObject(
                mockMvc.perform(
                        get("/rest/participants/~/proxies?status=INACTIVE")
                        .with(authToken(TestConstants.USER_HARWELLM))
                        )
                        .andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString()
                        ,ArrayList.class
                );
        assertTrue("Should not have results", proxyList.isEmpty());
    }
}
