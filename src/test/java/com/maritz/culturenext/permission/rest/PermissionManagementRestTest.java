package com.maritz.culturenext.permission.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.enums.RoleCode;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.PermissionWrapperDTO;
import com.maritz.test.AbstractRestTest;


public class PermissionManagementRestTest extends AbstractRestTest {

    public static final String PATH_PERMISSIONS_ROLE = "/rest/users/permissions/role";
    public static final String PATH_PERMISSIONS_ROLE_ADMIN = "/rest/users/permissions/role?roleName=ADMIN";

    @Test
    public void get_permissions_single_role() throws Exception {
        mockMvc.perform(
                get("/rest/users/permissions/role?roleName=ADMIN")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_permissions_multiple_roles() throws Exception {
        mockMvc.perform(
                get("/rest/users/permissions/role?roleName=ADMIN,MANAGER")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
        .andExpect(status().isOk())
         ;
    }
    
    @Test
    public void get_permissions_single_group() throws Exception {
        mockMvc.perform(
                get("/rest/users/permissions/group?groupId=1261")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
        .andExpect(status().isOk())
         ;
    }
    
    @Test
    public void get_permissions_multiple_groups() throws Exception {
        mockMvc.perform(
                get("/rest/users/permissions/group?groupId=1261,1272")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void put_permissions_role() throws Exception {
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionManagementTypes.getValidAdminPermissions()) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }
        permissionWrapper.setPermissions(permissionList);
        
        mockMvc.perform(
                put("/rest/users/permissions/role")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
            )
            .andExpect(status().isOk());
    }
    
    @Test
    public void patch_permissions_role() throws Exception {
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO()
                .setPermissionCode(PermissionManagementTypes.getValidAdminPermissions().get(0)).setEnabled(false));
        permissionWrapper.setPermissions(permissionList);
        
        mockMvc.perform(
                patch("/rest/users/permissions/role")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
            )
            .andExpect(status().isOk());
    }
    
    @Test
    public void put_permissions_group() throws Exception {
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(1261L);
        List<PermissionDTO> permissionList = new ArrayList<>();
        for (String permission : PermissionManagementTypes.getValidGroupPermissions()) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }
        permissionWrapper.setPermissions(permissionList);
        
        mockMvc.perform(
                put("/rest/users/permissions/group")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
            )
            .andExpect(status().isOk());
    }
    
    @Test
    public void patch_permissions_group() throws Exception {
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(1261L);
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO()
                .setPermissionCode(PermissionManagementTypes.getValidGroupPermissions().get(0)).setEnabled(false));
        permissionWrapper.setPermissions(permissionList);
        
        mockMvc.perform(
                patch("/rest/users/permissions/group")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
            )
            .andExpect(status().isOk());
    }
    

    @Test
    public void get_check_permissions_and_roles_proxy_settings_anyone() throws Exception {

        MvcResult result = mockMvc.perform(
                    get(PATH_PERMISSIONS_ROLE_ADMIN)
                    .with(authToken(TestConstants.USER_KUMARSJ_ADMIN)))
                    .andExpect(status().isOk())
                    .andReturn();

        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_BUDGETS_ANYONE.name()));
        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_SETTINGS_ANYONE.name()));

    }

    @Test
    public void put_permissions_role_proxy_settings_anyone() throws Exception {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole(RoleCode.ADMIN.name());

        List<PermissionDTO> permissionList = new ArrayList<>();

        for (String permission : PermissionManagementTypes.getValidAdminPermissions()) {
            permissionList.add(new PermissionDTO().setPermissionCode(permission).setEnabled(true));
        }
        permissionWrapper.setPermissions(permissionList);

        MvcResult result = mockMvc.perform(
                put(PATH_PERMISSIONS_ROLE)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN)))
                .andExpect(status().isOk()).andReturn();

        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_BUDGETS_ANYONE.name()));
        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_SETTINGS_ANYONE.name()));
    }

    @Test
    public void patch_permissions_role_proxy_settings_anyone() throws Exception {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole(RoleCode.ADMIN.name());

        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO()
                .setPermissionCode(PermissionManagementTypes.getValidAdminPermissions().get(0)).setEnabled(false));

        permissionWrapper.setPermissions(permissionList);

        MvcResult result = mockMvc.perform(
                patch(PATH_PERMISSIONS_ROLE)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(permissionWrapper))
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN)))
                .andExpect(status().isOk()).andReturn();

        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_BUDGETS_ANYONE.name()));
        assert(result.getResponse().getContentAsString().contains(PermissionManagementTypes.PROXY_SETTINGS_ANYONE.name()));
    }
}
