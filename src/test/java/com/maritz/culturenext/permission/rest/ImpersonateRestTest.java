package com.maritz.culturenext.permission.rest;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.core.dto.AuthTokenDTO;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.authentication.login.LoginInfo;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ImpersonateRestTest extends AbstractRestTest {
    
    @Inject private SysUserRepository sysUserRepository;

    @Test
    public void get_proxies_no_type() throws Exception {
        SysUser impersonator = sysUserRepository.findBySysUserName(TestConstants.USER_HARWELLM);
        SysUser impersonatee = sysUserRepository.findBySysUserName(TestConstants.USER_DAYSTB_ADMIN);

        AuthTokenDTO impersonateResult = ObjectUtils.jsonToObject(
            mockMvc.perform(
                post("/rest/authenticate/impersonate/id/" + impersonatee.getSysUserId())
                    .with(authToken(impersonator.getSysUserName()))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.authToken", notNullValue()))
                .andReturn().getResponse().getContentAsString()
            ,AuthTokenDTO.class
        );

        assertThat(impersonateResult, notNullValue());
        assertThat(impersonateResult.getAuthToken(), notNullValue());

        LoginInfo loginInfo = ObjectUtils.jsonToObject(
            mockMvc.perform(
                post("/rest/authenticate/login")
                    .with(token(impersonateResult.getAuthToken()))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.userName", is(impersonatee.getSysUserName())))
                .andExpect(jsonPath("$.pax.paxId", is(impersonatee.getPaxId().intValue())))
                .andExpect(jsonPath("$.impersonated", is(true)))
                .andExpect(jsonPath("$.attributes.AuthToken", notNullValue()))
                .andReturn().getResponse().getContentAsString()
            ,LoginInfo.class
        );

        mockMvc.perform(
                get("/rest/participants/~/recognition/engagement_score")
                .with(token((String)loginInfo.getAttributes().get("AuthToken")))
            )
               .andExpect(status().isForbidden())
       ;
    }
}
