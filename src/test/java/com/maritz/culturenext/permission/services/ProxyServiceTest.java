package com.maritz.culturenext.permission.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.ProxyDao;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.ProxyDTO;
import com.maritz.culturenext.permission.services.impl.ProxyServiceImpl;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class ProxyServiceTest {
    
    @InjectMocks private ProxyServiceImpl proxyService;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private ProxyDao proxyDao;
    @Mock private Security security;
    
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        
        Pax pax = new Pax();
        pax.setPaxId(TestConstants.ID_1);
        PowerMockito.when(paxDao.findById(anyLong())).thenReturn(pax);
    }
    
    @Test
    public void test_get_proxies_invalid_type() {
        try {
            proxyService.getProxiesForPax(TestConstants.ID_1, "BAD_TYPE", null);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid type", e.getErrorMessages().size() == 1);
            assertTrue("type is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_TYPE_ERROR_CODE));
        }
    }
    
    @Test
    public void test_patch_missing_pax() {
        try {
            proxyService.updateProxyForPax(TestConstants.ID_1, TestConstants.ID_1, RequestMethod.PATCH.toString(), new ProxyDTO());
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing pax", e.getErrorMessages().size() == 1);
            assertTrue("pax is missing", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.PAX_MISSING_ERROR_CODE));
        }
    }
    
    @Test
    public void test_patch_invalid_pax() {
        PowerMockito.when(paxDao.findById(anyLong())).thenReturn(null);
        try {
            proxyService.updateProxyForPax(TestConstants.ID_1, TestConstants.ID_1, RequestMethod.PATCH.toString(), 
                    new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1)));
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid pax", e.getErrorMessages().size() == 1);
            assertTrue("pax does not exist", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_CODE));
        }
    }
    
    @Test
    public void test_patch_pax_mismatch() {
        try {
            proxyService.updateProxyForPax(TestConstants.ID_1, TestConstants.ID_2, RequestMethod.PATCH.toString(),
                    new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(3L)));
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for pax mismatch", e.getErrorMessages().size() == 1);
            assertTrue("path pax doesn't match request body pax", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.PAX_ID_NOT_SAME_AS_PATH_ERROR_CODE));
        }
    }
    
    @Test
    public void test_patch_invalid_permission_code() {
        ProxyDTO proxyDto = new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO().setPermissionCode("BRIDGE_ACCESS").setEnabled(true));
        
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(TestConstants.ID_2, TestConstants.ID_1, RequestMethod.PATCH.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid permission code", e.getErrorMessages().size() == 1);
            assertTrue("permission is not valid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_PERMISSION_ERROR_CODE));
        }
    }
    
    @Test
    public void test_patch_missing_enabled_value() {
        ProxyDTO proxyDto = new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC));
        
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(TestConstants.ID_2, TestConstants.ID_1, RequestMethod.PATCH.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for no value for enabled", e.getErrorMessages().size() == 1);
            assertTrue("enabled is null", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_PERMISSION_ENABLED_ERROR_CODE));
        }
    }
    
    @Test
    public void test_post_values_already_exist() {

        PowerMockito.when(proxyDao.doesAclEntryExist(anyListOf(String.class), anyLong(), anyLong())).thenReturn(true);
        ProxyDTO proxyDto = new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO()
                .setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(true));
        
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(TestConstants.ID_2, TestConstants.ID_1, RequestMethod.POST.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for entry already exists", e.getErrorMessages().size() == 1);
            assertTrue("entry already exists", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.ENTRY_ALREADY_EXISTS_ERROR_CODE));
        }
    }
    
    @Test
    public void test_put_values_do_not_exist() {

        PowerMockito.when(proxyDao.doesAclEntryExist(anyListOf(String.class), anyLong(), anyLong())).thenReturn(false);
        ProxyDTO proxyDto = new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(true));
        
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(TestConstants.ID_2, TestConstants.ID_1, RequestMethod.PUT.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for does not exist", e.getErrorMessages().size() == 1);
            assertTrue("entry doesn't exist", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.ENTRY_DOES_NOT_EXIST_ERROR_CODE));
        }
    }
    
    @Test
    public void test_put_not_all_permissions_populated() {

        PowerMockito.when(proxyDao.doesAclEntryExist(anyListOf(String.class), anyLong(), anyLong())).thenReturn(true);
        ProxyDTO proxyDto = new ProxyDTO().setProxyPax(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(true));
        
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(TestConstants.ID_2, TestConstants.ID_1, RequestMethod.PUT.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("multiple errors for missing permissions for put / post", e.getErrorMessages().size() ==
                    PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST.size() - 1);
            Iterator<ErrorMessage> errorIterator = e.getErrorMessages().iterator();
            while(errorIterator.hasNext()) {
                ErrorMessage message = errorIterator.next();
                assertTrue("permission is missing",
                        message.getCode().equalsIgnoreCase(PermissionConstants.MISSING_PERMISSION_ERROR_CODE));
            }
        }
    }
    
    @Test
    public void test_delete_invalid_pax() {
        PowerMockito.when(paxDao.findById(anyLong())).thenReturn(null);
        
        try {
            proxyService.deleteProxyForPax(TestConstants.ID_1, TestConstants.ID_1);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for does not exist", e.getErrorMessages().size() == 1);
            assertTrue("entry doesn't exists", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.PAX_DOES_NOT_EXIST_ERROR_CODE));
        }
    }
    
    @Test
    public void test_delete_pax_not_proxy() {

        PowerMockito.when(proxyDao.doesAclEntryExist(anyListOf(String.class), anyLong(), anyLong())).thenReturn(false);
        
        try {
            proxyService.deleteProxyForPax(TestConstants.ID_1, TestConstants.ID_1);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for pax not proxy", e.getErrorMessages().size() == 1);
            assertTrue("pax isn't proxy", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.PAX_IS_NOT_A_PROXY_ERROR_CODE));
        }
    }
    
    @Test
    public void test_put_proxy_self_assign() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8571L));
        proxyDto.setType(PermissionConstants.MY_PROXIES);
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_GIVE_REC).setEnabled(true));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(true));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_VIEW_REPORTS).setEnabled(true));
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(8571L, 8571L, RequestMethod.PUT.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid type", e.getErrorMessages().size() == 1);
            assertTrue("type is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.ASSIGN_SELF_PROXY_ERROR_CODE));
        }
    }
    
    @Test
    public void test_put_proxy_duplicate_permissions() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8987L));
        proxyDto.setType(PermissionConstants.MY_PROXIES);
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_GIVE_REC).setEnabled(true));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_GIVE_REC).setEnabled(true));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(true));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_VIEW_REPORTS).setEnabled(true));
        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(8571L, 8987L, RequestMethod.PUT.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid type", e.getErrorMessages().size() == 1);
            assertTrue("type is invalid", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.DUPLICATE_PERMISSION_ERROR_CODE));
        }
    }
    
    @Test
    public void test_post_all_permissions_false() throws Exception {
        ProxyDTO proxyDto = new ProxyDTO();
        proxyDto.setProxyPax(new EmployeeDTO().setPaxId(8987L));
        proxyDto.setType(PermissionConstants.MY_PROXIES);
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_GIVE_REC).setEnabled(false));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(false));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_VIEW_REPORTS).setEnabled(false));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_BUDGETS).setEnabled(false));
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_SETTINGS).setEnabled(false));

        proxyDto.setPermissions(permissionList);
        
        try {
            proxyService.updateProxyForPax(8571L, 8987L, RequestMethod.POST.toString(), proxyDto);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid type", e.getErrorMessages().size() == 1);
            assertTrue("type is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.POST_ALL_FALSE_ERROR_CODE));
        }
    }
    
    @Test
    public void test_get_proxy_for_specific_pax_admin() {
        //Set up the mocks
        PowerMockito.when(proxyDao.getProxyForSpecificPax(anyListOf(String.class),
                anyLong(), anyLong(), anyString())).thenReturn(null);
        PowerMockito.when(security.getImpersonatorPaxId()).thenReturn(8571L);
        PowerMockito.when(security.impersonatorHasPermission(anyString())).thenReturn(Boolean.TRUE);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(new EmployeeDTO().setPaxId(158L));
        
        //Run the tests
        ProxyDTO proxyDTO = proxyService.getProxyPermissions(8571L);
        assertTrue(proxyDTO.getProxyPax().getPaxId().equals(158L));
        assertTrue(proxyDTO.getType().equalsIgnoreCase(PermissionConstants.PROXY_AS));
        for (PermissionDTO permissionDTO : proxyDTO.getPermissions()) {
            assertTrue(permissionDTO.getEnabled() == true);
            assertTrue(PermissionConstants.SUPPORTED_PROXY_PERMISSION_LIST.contains(permissionDTO.getPermissionCode()));
        }
    }
    
    @Test
    public void test_get_proxy_for_specific_pax_non_admin() {
        //Set up the mocks
        ProxyDTO proxyDtoProxyAs = new ProxyDTO();
        proxyDtoProxyAs.setProxyPax(new EmployeeDTO().setPaxId(8571L));
        proxyDtoProxyAs.setType(PermissionConstants.PROXY_AS);
        List<PermissionDTO> permissionList2 = new ArrayList<>();
        permissionList2.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_GIVE_REC).setEnabled(true));
        permissionList2.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_APPROVE_REC).setEnabled(false));
        permissionList2.add(
                new PermissionDTO().setPermissionCode(PermissionConstants.PROXY_VIEW_REPORTS).setEnabled(false));
        proxyDtoProxyAs.setPermissions(permissionList2);
        PowerMockito.when(proxyDao.getProxyForSpecificPax(anyListOf(String.class), anyLong(),
                anyLong(), anyString())).thenReturn(proxyDtoProxyAs);
        PowerMockito.when(security.getImpersonatorPaxId()).thenReturn(8571L);
        PowerMockito.when(security.impersonatorHasPermission(anyString())).thenReturn(Boolean.FALSE);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(new EmployeeDTO().setPaxId(8571L));
        
        //Run the tests
        ProxyDTO proxyDTO = proxyService.getProxyPermissions(158L);
        assertTrue(proxyDTO.getProxyPax().getPaxId().equals(8571L));
        assertTrue(proxyDTO.getType().equalsIgnoreCase(PermissionConstants.PROXY_AS));
        for (PermissionDTO permissionDTO : proxyDTO.getPermissions()) {
            if (permissionDTO.getPermissionCode().equals(PermissionConstants.PROXY_GIVE_REC)) {
                assertTrue(permissionDTO.getEnabled() == true);
            }
            else if (permissionDTO.getPermissionCode().equals(PermissionConstants.PROXY_APPROVE_REC)) {
                assertTrue(permissionDTO.getEnabled() == false);
            }
            else if (permissionDTO.getPermissionCode().equals(PermissionConstants.PROXY_VIEW_REPORTS)) {
                assertTrue(permissionDTO.getEnabled() == false);
            }
        }
    }

}
