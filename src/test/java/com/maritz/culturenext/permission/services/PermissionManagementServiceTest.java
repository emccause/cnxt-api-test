package com.maritz.culturenext.permission.services;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.jpa.entity.Role;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.enums.LogicalOperatorEnum;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.permission.dao.PermissionResolutionDao;
import com.maritz.culturenext.permission.dto.GroupPermissionsDTO;
import com.maritz.culturenext.permission.dto.PermissionDTO;
import com.maritz.culturenext.permission.dto.PermissionWrapperDTO;
import com.maritz.culturenext.permission.services.impl.PermissionManagementServiceImpl;
import com.maritz.culturenext.profile.constants.GroupConstants;
import com.maritz.test.AbstractMockTest;


public class PermissionManagementServiceTest extends AbstractMockTest {

    @InjectMocks private PermissionManagementServiceImpl permissionService;
    @Mock private ConcentrixDao<Role> roleDao;
    @Mock private ConcentrixDao<Groups> groupsDao;
    @Mock private GroupConfigServiceDao groupConfigServiceDao;
    @Mock private PermissionResolutionDao permissionResolutionDao;

    private FindBy<Groups> groupsFindBy = mock(FindBy.class);

    @Rule public ExpectedException thrown = ExpectedException.none();

    private static final String FIELD_TEST = "Field";
    private static final String FIELD_DISPLAY_NAME_TEST = "Display Name";
    private static final String GROUP_NAME_TEST = "ENABLE Permissions";
    private static final String GROUP_TYPE_TEST = "CUSTOM";
    private static final Integer PAX_COUNT_TEST = 5;
    private static final Integer GROUP_COUNT_TEST = 10;
    private static final Integer TOTAL_PAX_COUNT_TEST = 15;
    private static final Long GROUP_PAX_ID_TEST = 20L;
    private static final String STATUS_TYPE_TEST = StatusTypeCode.ACTIVE.name();
    private static final Date CREATE_DATE_TEST = new Date();
    private static final List<String> PERMISSION_CODES_LIST_TEST =
            Arrays.asList(PermissionManagementTypes.ENABLE_CALENDAR.name()
                    , PermissionManagementTypes.DISABLE_LIKING.name());
    private static final Integer SIZE_TEST = 1;
    private static final Integer PAGE_TEST = 1;

    @Before
    public void setup() {
        List<Groups> groupList = new ArrayList<>();
        Groups group = new Groups();
        group.setGroupId(TestConstants.ID_1);
        group.setGroupTypeCode(GroupType.CUSTOM.getCode());
        groupList.add(group);

        PowerMockito.when(groupsDao.findBy()).thenReturn(groupsFindBy);
        PowerMockito.when(groupsFindBy.where(anyString())).thenReturn(groupsFindBy);
        PowerMockito.when(groupsFindBy.in(anyListOf(Long.class))).thenReturn(groupsFindBy);
        PowerMockito.when(groupsFindBy.find(anyString(), Matchers.<Class<Long>>any())).thenReturn(Arrays.asList(TestConstants.ID_1, 4L, 5217L));
    }

    @Test
    public void test_get_individual_role_missing() {

        try {
            permissionService.getPermissionsForRoles("");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing role", e.getErrorMessages().size() == 1);
            assertTrue("Role is missing", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.MISSING_ROLE_NAME_ERROR_CODE));
        }
    }

    @Test
    public void test_get_individual_role_invalid() {
        try {
            permissionService.getPermissionsForRoles("FRED");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid role", e.getErrorMessages().size() == 1);
            assertTrue("Role is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_ROLE_NAME_ERROR_CODE));
        }
    }

    @Test
    public void test_get_multiple_roles_invalid() {

        try {
            permissionService.getPermissionsForRoles("ADMIN,STEVE");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error per invalid role", e.getErrorMessages().size() == 1);
            ErrorMessage steveError = e.getErrorMessages().iterator().next();
            assertTrue("error is for steve", steveError.getMessage().contains("STEVE"));
            assertTrue("Role is invalid",
                    steveError.getCode().equalsIgnoreCase(PermissionConstants.INVALID_ROLE_NAME_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_missing() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing role", e.getErrorMessages().size() == 1);
            assertTrue("Role is missing", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.ROLE_MISSING_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_invalid() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("FRED");
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid role", e.getErrorMessages().size() == 1);
            assertTrue("Role is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_ROLE_NAME_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_group_id_populated() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        permissionWrapper.setGroupId(TestConstants.ID_1);
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for group id populated", e.getErrorMessages().size() == 1);
            assertTrue("cannot pass group id", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.CANNOT_MODIFY_GROUPS_HERE_ERROR_CODE));
        }
    }

    @Test
    public void test_get_individual_group_missing() {

        try {
            permissionService.getPermissionsForGroups("");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing group", e.getErrorMessages().size() == 1);
            assertTrue("Group is missing", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.MISSING_GROUP_ID_ERROR_CODE));
        }
    }

    @Test
    public void test_get_individual_group_invalid() {

        try {
            permissionService.getPermissionsForGroups("FRED");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid group", e.getErrorMessages().size() == 1);
            assertTrue("Group is invalid", e.getErrorMessages()
                    .iterator().next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_GROUP_ID_ERROR_CODE));
        }
    }

    @Test
    public void test_get_multiple_groups_invalid() {

        try {
            permissionService.getPermissionsForGroups("1261,STEVE");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error per invalid group", e.getErrorMessages().size() == 1);
            ErrorMessage steveError = e.getErrorMessages().iterator().next();
            assertTrue("error is for steve", steveError.getMessage().contains("STEVE"));
            assertTrue("Group is invalid",
                    steveError.getCode().equalsIgnoreCase(PermissionConstants.INVALID_GROUP_ID_ERROR_CODE));
        }
    }

    @Test
    public void test_get_individual_group_does_not_exist() {

        try {
            permissionService.getPermissionsForGroups("5");
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for non-existant group", e.getErrorMessages().size() == 1);
            assertTrue("Group does not exist", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.GROUP_DOES_NOT_EXIST_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_group_missing() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();

        try {
            permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing group", e.getErrorMessages().size() == 1);
            assertTrue("Group is missing", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.MISSING_GROUP_ID_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_group_does_not_exist() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(5L);

        try {
            permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for non-existant group", e.getErrorMessages().size() == 1);
            assertTrue("Group does not exist", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.GROUP_DOES_NOT_EXIST_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_group_role_populated() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(TestConstants.ID_1);
        permissionWrapper.setRole("Something");

        try {
            permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for role populated", e.getErrorMessages().size() == 1);
            assertTrue("cannot pass role", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.CANNOT_MODIFY_ROLES_HERE_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_missing_permission_list() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PATCH.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing permission list", e.getErrorMessages().size() == 1);
            assertTrue("Permission list is missing", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.MISSING_PERMISSION_LIST_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_invalid_permission() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(new PermissionDTO().setPermissionCode("INVALID").setEnabled(Boolean.TRUE));
        permissionWrapper.setPermissions(permissionList);
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PATCH.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for invalid permission", e.getErrorMessages().size() == 1);
            assertTrue("Permission is invalid", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.INVALID_PERMISSION_ERROR_CODE));
        }
    }

    @Test
    public void test_update_individual_role_missing_permission() {

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setRole("ADMIN");
        List<PermissionDTO> permissionList = new ArrayList<>();
        permissionList.add(
                new PermissionDTO().setPermissionCode(PermissionManagementTypes
                        .getValidAdminPermissions().get(0)).setEnabled(Boolean.TRUE));
        permissionWrapper.setPermissions(permissionList);
        try {
            permissionService.updatePermissionsForRole(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("Multiple errors for missing permission on PUT", e.getErrorMessages().size() ==
                    PermissionManagementTypes.getValidAdminPermissions().size() - 1);
            Iterator<ErrorMessage> errors = e.getErrorMessages().iterator();
            while(errors.hasNext()){
                ErrorMessage message = errors.next();
                assertTrue("Permission is missing",
                        message.getCode().equalsIgnoreCase(PermissionConstants.MISSING_PERMISSION_ERROR_CODE));
            }
        }
    }

    @Test
    public void test_patch_group_permission_for_proxy() {

        List<PermissionDTO> permissionList = new ArrayList<>();
        PermissionDTO proxyPermission = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ASSIGN_PROXY_SELF.name())
            .setEnabled(true);
        permissionList.add(proxyPermission);
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(4L); //Participants group
        permissionWrapper.setPermissions(permissionList);

        List<PermissionWrapperDTO> returnList =
                permissionService.updatePermissionsForGroup(RequestMethod.PATCH.toString(), permissionWrapper);

        assertTrue(returnList.size() == 1);
        assertTrue(returnList.get(0).getGroupId().equals(4L));
        assertTrue(returnList.get(0).getPermissions().size() == PermissionManagementTypes.getValidGroupPermissions().size());
    }

    @Test
    public void test_patch_group_permission_for_audience() {
        List<PermissionDTO> permissionList = new ArrayList<>();
        PermissionDTO proxyPermission = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_COMMENTING.name())
            .setEnabled(true);
        permissionList.add(proxyPermission);
        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(5217L); //DISABLE Permissions group
        permissionWrapper.setPermissions(permissionList);

        List<PermissionWrapperDTO> returnList =
                permissionService.updatePermissionsForGroup(RequestMethod.PATCH.toString(), permissionWrapper);

        assertTrue(returnList.size() == 1);
        assertTrue(returnList.get(0).getGroupId().equals(5217L));
        assertTrue(returnList.get(0).getPermissions().size() == PermissionManagementTypes.getValidGroupPermissions().size());
    }

    @Test
    public void test_put_group_permissions() {
        List<PermissionDTO> permissionList = setupPutPermissions();

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(5217L); //DISABLE Permissions group
        permissionWrapper.setPermissions(permissionList);

        List<PermissionWrapperDTO> returnList =
                permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);

        assertTrue(returnList.size() == 1);
        assertTrue(returnList.get(0).getGroupId().equals(5217L));
        assertTrue(returnList.get(0).getPermissions().size() == PermissionManagementTypes.getValidGroupPermissions().size());
    }

    @Test
    public void test_put_group_permissions_missing_one() {
        List<PermissionDTO> permissionList = setupPutPermissions();
        permissionList.remove(0);

        PermissionWrapperDTO permissionWrapper = new PermissionWrapperDTO();
        permissionWrapper.setGroupId(5217L); //DISABLE Permissions group
        permissionWrapper.setPermissions(permissionList);

        try {
            permissionService.updatePermissionsForGroup(RequestMethod.PUT.toString(), permissionWrapper);
            fail("should have thrown error");
        }
        catch (ErrorMessageException e) {
            assertTrue("One error for missing permission", e.getErrorMessages().size() == 1);
            assertTrue("Missing a permission for PUT", e.getErrorMessages().iterator()
                    .next().getCode().equalsIgnoreCase(PermissionConstants.MISSING_PERMISSION_ERROR_CODE));
        }
    }

    private List<PermissionDTO> setupPutPermissions() {

        List<PermissionDTO> permissionList = new ArrayList<>();

        PermissionDTO proxyPermission = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ASSIGN_PROXY_SELF.name())
            .setEnabled(true);
        permissionList.add(proxyPermission);
        PermissionDTO enableCalendar = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_CALENDAR.name())
            .setEnabled(false);
        permissionList.add(enableCalendar);
        PermissionDTO disableCalendar = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_CALENDAR.name())
            .setEnabled(true);
        permissionList.add(disableCalendar);
        PermissionDTO enableNetwork = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_NETWORK.name())
            .setEnabled(false);
        permissionList.add(enableNetwork);
        PermissionDTO disableNetwork = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_NETWORK.name())
            .setEnabled(true);
        permissionList.add(disableNetwork);
        PermissionDTO enableCommenting = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_COMMENTING.name())
            .setEnabled(false);
        permissionList.add(enableCommenting);
        PermissionDTO disableCommenting = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_COMMENTING.name())
            .setEnabled(true);
        permissionList.add(disableCommenting);
        PermissionDTO enableLiking = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_LIKING.name())
            .setEnabled(false);
        permissionList.add(enableLiking);
        PermissionDTO disableLiking = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_LIKING.name())
            .setEnabled(true);
        permissionList.add(disableLiking);
        PermissionDTO enableEngageScore = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_ENGAGE_SCORE.name())
            .setEnabled(false);
        permissionList.add(enableEngageScore);
        PermissionDTO disableEngageScore = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_ENGAGE_SCORE.name())
            .setEnabled(true);
        permissionList.add(disableEngageScore);
        PermissionDTO enableServiceAnniversary = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.ENABLE_SERVICE_ANNIVERSARY.name())
            .setEnabled(false);
        permissionList.add(enableServiceAnniversary);
        PermissionDTO disableServiceAnniversary = new PermissionDTO()
            .setPermissionCode(PermissionManagementTypes.DISABLE_SERVICE_ANNIVERSARY.name())
            .setEnabled(true);
        permissionList.add(disableServiceAnniversary);

        return permissionList;
    }

    public void test_getPermissionsGroups_fail_missing_permission() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(PermissionConstants.MISSING_PERMISSIONS_CODE));

        permissionService.getGroupsByPermissions("", LogicalOperatorEnum.AND.name(), PAGE_TEST, SIZE_TEST);
    }

    @Test
    public void test_getPermissionsGroups_fail_invalid_permission() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(PermissionConstants.INVALID_PERMISSIONS_CODE));

        permissionService.getGroupsByPermissions("NOT_VALID_PERMISSION", LogicalOperatorEnum.AND.name()
                , PAGE_TEST, SIZE_TEST);
    }

    @Test
    public void test_getPermissionsGroups_fail_missing_filter() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(PermissionConstants.MISSING_PERMISSION_FILTER_CODE));

        permissionService.getGroupsByPermissions(PermissionManagementTypes.DISABLE_CALENDAR.name(), ""
                , PAGE_TEST, SIZE_TEST);
    }

    @Test
    public void test_getPermissionsGroups_fail_invalid_filter() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(PermissionConstants.INVALID_PERMISSION_FILTER_CODE));

        permissionService.getGroupsByPermissions(PermissionManagementTypes.DISABLE_CALENDAR.name(), "NOT_VALID"
                , PAGE_TEST, SIZE_TEST);
    }

    @Test
    public void test_getPermissionsGroups_success() {
        PowerMockito.when(permissionResolutionDao.getGroupsByPermissions(anyListOf(String.class), anyString(), anyInt(), anyInt()))
            .thenReturn(Arrays.asList(mockGroupsByPermissionsResponse()));

        PaginatedResponseObject<GroupPermissionsDTO> permissionsByGroupPaginated =
                permissionService.getGroupsByPermissions(PermissionManagementTypes.DISABLE_CALENDAR.name()
                        , LogicalOperatorEnum.AND.name(), PAGE_TEST, SIZE_TEST);

        List<GroupPermissionsDTO> permissionsByGroupList = permissionsByGroupPaginated.getData();

        assertNotNull(permissionsByGroupList);

        GroupPermissionsDTO permissionGroup = permissionsByGroupList.get(0);

        assertNotNull(permissionGroup);
        assertEquals(TestConstants.ID_1, permissionGroup.getGroup().getGroupId());
        assertEquals(TestConstants.ID_2, permissionGroup.getGroup().getGroupConfigId());
        assertEquals(FIELD_TEST, permissionGroup.getGroup().getField());
        assertEquals(FIELD_DISPLAY_NAME_TEST, permissionGroup.getGroup().getFieldDisplayName());
        assertEquals(GROUP_NAME_TEST, permissionGroup.getGroup().getGroupName());
        assertEquals(GROUP_TYPE_TEST, permissionGroup.getGroup().getType());
        assertEquals(STATUS_TYPE_TEST, permissionGroup.getGroup().getStatus());
        assertEquals(PAX_COUNT_TEST, permissionGroup.getGroup().getPaxCount());
        assertEquals(GROUP_COUNT_TEST, permissionGroup.getGroup().getGroupCount());
        assertEquals(TOTAL_PAX_COUNT_TEST, permissionGroup.getGroup().getTotalPaxCount());
        assertEquals(GROUP_PAX_ID_TEST, permissionGroup.getGroup().getGroupPaxId());
        assertNotNull(permissionGroup.getPermissions());
        for (PermissionDTO permission: permissionGroup.getPermissions()) {
            if (PERMISSION_CODES_LIST_TEST.contains(permission.getPermissionCode())) {
                assertEquals(Boolean.TRUE, permission.getEnabled());
            }
            else {
                assertEquals(Boolean.FALSE, permission.getEnabled());
            }
        }

    }

    private Map<String, Object> mockGroupsByPermissionsResponse() {
        Map<String, Object> node = new HashMap<>();
        node.put(GroupConstants.GROUP_ID, TestConstants.ID_1);
        node.put(GroupConstants.GROUP_CONFIG_ID, TestConstants.ID_2);
        node.put(GroupConstants.FIELD, FIELD_TEST);
        node.put(GroupConstants.FIELD_DISPLAY_NAME, FIELD_DISPLAY_NAME_TEST);
        node.put(GroupConstants.GROUP_NAME, GROUP_NAME_TEST);
        node.put(GroupConstants.GROUP_TYPE, GROUP_TYPE_TEST);
        node.put(GroupConstants.STATUS_TYPE_CODE, STATUS_TYPE_TEST);
        node.put(GroupConstants.CREATE_DATE, CREATE_DATE_TEST);
        node.put(GroupConstants.PAX_COUNT, PAX_COUNT_TEST);
        node.put(GroupConstants.GROUP_COUNT, GROUP_COUNT_TEST);
        node.put(GroupConstants.TOTAL_PAX_COUNT, TOTAL_PAX_COUNT_TEST);
        node.put(GroupConstants.GROUP_PAX_ID, GROUP_PAX_ID_TEST);
        node.put(PermissionConstants.PERMISSION_CODES_LIST_FIELD
                , StringUtils.formatDelimitedData(PERMISSION_CODES_LIST_TEST));
        node.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);
        return node;
    }

}
