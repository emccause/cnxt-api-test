package com.maritz.culturenext.project.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.test.AbstractRestTest;

public class ProjectProfileRestServiceTest extends AbstractRestTest {

    private static final String PROJECT_PROFILE_ENDPOINT = "/rest/project-profile";
    private static final String PROJECT_MESSAGES_ENDPOINT = "/rest/messages";
    private static final String LANGUAGE_CODE_PARAM = "?languageCode=";
    
    @Test
    public void get_profile_messages() throws Exception {
        mockMvc.perform(
                get(PROJECT_MESSAGES_ENDPOINT)
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_profile_messages_default_language_code() throws Exception {
        mockMvc.perform(
                get(PROJECT_MESSAGES_ENDPOINT + LANGUAGE_CODE_PARAM + LocaleCodeEnum.en_US.name())
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_profile_messages_valid_language_code() throws Exception {
        mockMvc.perform(
                get(PROJECT_MESSAGES_ENDPOINT + LANGUAGE_CODE_PARAM + LocaleCodeEnum.fr_CA.name())
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_profile_messages_invalid_language_code() throws Exception {
        mockMvc.perform(
                get(PROJECT_MESSAGES_ENDPOINT + LANGUAGE_CODE_PARAM + LocaleCodeEnum.nl_NL.name())
        )
                .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void get_project_profile() throws Exception {
        mockMvc.perform(
                get(PROJECT_PROFILE_ENDPOINT)
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_project_profile_as_admin() throws Exception {
        mockMvc.perform(
                get(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
        )
                .andExpect(status().isOk())
        ;
    }
    
    
    @Test
    public void put_project_profile_as_admin() throws Exception {
        mockMvc.perform(
                put(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                        "{\"clientName\":\"Novartis\", "
                        + "\"reportAbuseAdmin\":\"12345\", "
                        + "\"supportEmail\":\"support@project.culturenext.com\", "
                        + "\"ssoEnabled\":true, "
                        + "\"partnerSsoUrl\":\"https://partner.com/sso-login\", "
                        + "\"likingEnabled\":true, "
                        + "\"commentingEnabled\":true, "
                        + "\"engagementScoreEnabled\":true, "
                        + "\"networkConnectionsEnabled\":true, "
                        + "\"pointBank\":\"ABS\", "
                        + "\"colors\": {\"primaryColor\":\"#aabbcc\", "
                        +     "\"secondaryColor\":\"#aabbcc\","
                        +     "\"textOnPrimaryColor\":\"#aabbcc\""
                        + "},"
                        + "\"siteLanguages\":[\"en_US\",\"fr_CA\"], "
                        + "\"unassignedApproverPaxId\":\"12345\", "
                        + "\"calendarEnabled\":true, "
                        + "\"emailPreferences\": { "
                        +     "\"whenRecognized\": true, "
                        +    "\"directRptRecognized\": true, "
                        +    "\"approvalNeeded\": true, "
                        +    "\"weeklyDigest\": true, "
                        +    "\"notifyOthers\": true,"
                        +     "\"milestoneReminder\": true "
                        + "}, "
                        + "\"serviceAnniversaryEnabled\": true,"
                        + "\"serviceAnniversary\": {"
                        +    "\"provider\": \"Wayne's Wallet\","
                        +    "\"baseUrl\": \"https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif\""
                        +    "},"
                        + "\"awards\": {"
                        +     "\"pointsName\": \"POINTS\","                        
                        +     "\"cashName\": \"PINTS\","
                        +     "\"cashDescription\": \"Points from this recognition will be converted to a pint award\""
                        +    "},"
                        + "\"activityFeedFilter\": { "
                        +        "\"defaultFilter\": \"AREA\","
                        +         "\"filterOptions\": ["
                        +            "{"
                        +                "\"enabled\": true,"
                        +                "\"name\": \"CITY\""
                        +            "}"
                        +        "],"
                        +         "\"searchEnabled\": true"
                        +    "},"
                        + "\"seo\": { "
                        +         "\"clientTitle\":\"CultureNext\","
                        +         "\"clientDesc\":\"CultureNext\""
                        +    "},"
                        + "\"fromEmailAddress\":\"CultureNext <supportM365@maritz.com>\""
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void put_project_profile_as_admin_wo_MilestoneReminder() throws Exception {
        mockMvc.perform(
                put(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                        "{\"clientName\":\"Novartis\", "
                        + "\"reportAbuseAdmin\":\"12345\", "
                        + "\"supportEmail\":\"support@project.culturenext.com\", "
                        + "\"ssoEnabled\":true, "
                        + "\"partnerSsoUrl\":\"https://partner.com/sso-login\", "
                        + "\"likingEnabled\":true, "
                        + "\"commentingEnabled\":true, "
                        + "\"engagementScoreEnabled\":true, "
                        + "\"networkConnectionsEnabled\":true, "
                        + "\"pointBank\":\"ABS\", "
                        + "\"colors\": {\"primaryColor\":\"#aabbcc\", "
                        +     "\"secondaryColor\":\"#aabbcc\","
                        +     "\"textOnPrimaryColor\":\"#aabbcc\""
                        + "},"
                        + "\"siteLanguages\":[\"en_US\",\"fr_CA\"], "
                        + "\"unassignedApproverPaxId\":\"12345\", "
                        + "\"calendarEnabled\":true, "
                        + "\"emailPreferences\": { "
                        +     "\"whenRecognized\": true, "
                        +    "\"directRptRecognized\": true, "
                        +    "\"approvalNeeded\": true, "
                        +    "\"weeklyDigest\": true, "
                        +    "\"notifyOthers\": true"
                        + "}, "
                        + "\"serviceAnniversaryEnabled\": true,"
                        + "\"serviceAnniversary\": {"
                        +    "\"provider\": \"Wayne's Wallet\","
                        +    "\"baseUrl\": \"https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif\""
                        +    "},"
                        + "\"awards\": {"
                        +     "\"pointsName\": \"POINTS\","                        
                        +     "\"cashName\": \"PINTS\","
                        +     "\"cashDescription\": \"Points from this recognition will be converted to a pint award\""
                        +    "},"
                        + "\"activityFeedFilter\": { "
                        +        "\"defaultFilter\": \"AREA\","
                        +         "\"filterOptions\": ["
                        +            "{"
                        +                "\"enabled\": true,"
                        +                "\"name\": \"CITY\""
                        +            "}"
                        +        "],"
                        +         "\"searchEnabled\": true"
                        +    "},"
                        + "\"seo\": { "
                        +         "\"clientTitle\":\"CultureNext\","
                        +         "\"clientDesc\":\"CultureNext\""
                        +    "},"
                        + "\"fromEmailAddress\":\"CultureNext <supportM365@maritz.com>\""
                        + "}"
                )
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_projectProfile_missingActivityFeedFilter() throws Exception {
        mockMvc.perform(
            put(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                    "{\"clientName\":\"Novartis\", "
                        + "\"reportAbuseAdmin\":\"12345\", "
                        + "\"supportEmail\":\"support@project.culturenext.com\", "
                        + "\"ssoEnabled\":true, "
                        + "\"partnerSsoUrl\":\"https://partner.com/sso-login\", "
                        + "\"likingEnabled\":true, "
                        + "\"commentingEnabled\":true, "
                        + "\"engagementScoreEnabled\":true, "
                        + "\"networkConnectionsEnabled\":true, "
                        + "\"pointBank\":\"ABS\", "
                        + "\"colors\": {\"primaryColor\":\"#aabbcc\", "
                        +     "\"secondaryColor\":\"#aabbcc\","
                        +     "\"textOnPrimaryColor\":\"#aabbcc\""
                        + "},"
                        + "\"siteLanguages\":[\"en_US\",\"fr_CA\"], "
                        + "\"unassignedApproverPaxId\":\"12345\", "
                        + "\"calendarEnabled\":true, "
                        + "\"emailPreferences\": { "
                        +     "\"whenRecognized\": true, "
                        +    "\"directRptRecognized\": true, "
                        +    "\"approvalNeeded\": true, "
                        +    "\"weeklyDigest\": true, "
                        +    "\"notifyOthers\": true,"
                        +     "\"milestoneRemiders\": true }, "
                        + "\"serviceAnniversaryEnabled\": true,"
                        + "\"serviceAnniversary\": {"
                        +    "\"provider\": \"Wayne's Wallet\","
                        +    "\"baseUrl\": \"https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif\""
                        +    "},"
                        + "\"awards\": {"
                        +     "\"pointsName\": \"POINTS\","
                        +     "\"cashName\": \"PINTS\","
                        +     "\"cashDescription\": \"Points from this recognition will be converted to a pint award\""
                        +    "}"
                        + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void put_project_profile_as_admin_invalid_support_email_format() throws Exception {
        mockMvc.perform(
                put(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                        "{\"clientName\":\"Novartis\", "
                        + "\"reportAbuseAdmin\":\"12345\", "
                        + "\"supportEmail\":\"support @project.culturenext.com\", "
                        + "\"ssoEnabled\":true, "
                        + "\"partnerSsoUrl\":\"https://partner.com/sso-login\", "
                        + "\"likingEnabled\":true, "
                        + "\"commentingEnabled\":true, "
                        + "\"engagementScoreEnabled\":true, "
                        + "\"networkConnectionsEnabled\":true, "
                        + "\"pointBank\":\"ABS\", "
                        + "\"colors\": {\"primaryColor\":\"#aabbcc\", "
                        +     "\"secondaryColor\":\"#aabbcc\","
                        +     "\"textOnPrimaryColor\":\"#aabbcc\""
                        + "},"
                        + "\"siteLanguages\":[\"en_US\",\"fr_CA\"], "
                        + "\"unassignedApproverPaxId\":\"12345\", "
                        + "\"calendarEnabled\":true, "
                        + "\"emailPreferences\": { "
                        +     "\"whenRecognized\": true, "
                        +    "\"directRptRecognized\": true, "
                        +    "\"approvalNeeded\": true, "
                        +    "\"weeklyDigest\": true, "
                        +    "\"notifyOthers\": true,"
                        +     "\"milestoneRemiders\": true }, "
                        + "\"serviceAnniversaryEnabled\": true,"
                        + "\"serviceAnniversary\": {"
                        +    "\"provider\": \"Wayne's Wallet\","
                        +    "\"baseUrl\": \"https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif\""
                        +    "},"
                        + "\"awards\": {"
                        +     "\"pointsName\": \"POINTS\","                        
                        +     "\"cashName\": \"PINTS\","
                        +     "\"cashDescription\": \"Points from this recognition will be converted to a pint award\""
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isBadRequest())
        ;
    }
    
    @Test 
    public void patch_project_profile_client_name() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"clientName\":\"Novartis\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_report_abuse_admin() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"reportAbuseAdmin\":\"12345\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_support_email() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"supportEmail\":\"support@project.culturenext.com\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_invaild_support_email_format() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"supportEmail\":\"support @project.culturenext.com\"}")
        )
                .andExpect(status().isBadRequest())
        ;
    }
    
    @Test 
    public void patch_project_profile_partner_sso_url() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"partnerSsoUrl\":\"https://partner.com/sso-login\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_sso_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"ssoEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_liking_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"likingEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_commenting_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"commentingEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_engagement_score_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"engagementScoreEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_network_connections_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"networkConnectionsEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_calendar_enabled() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"calendarEnabled\":true}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_point_bank() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"pointBank\":\"ABS\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_colors() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                        "{"
                        + "\"colors\": "
                        + "        {\"primaryColor\":\"#aabbcc\", "
                        + "        \"secondaryColor\":\"#aabbcc\","
                        + "        \"textOnPrimaryColor\":\"#aabbcc\""
                        + "        }"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_site_languages() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"siteLanguages\":[\"en_US\",\"fr_CA\"]}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_unassigned_approver_pax_id() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"unassignedApproverPaxId\":\"12345\"}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_null_fields() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{}")
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_email_preferences() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        +     "\"emailPreferences\": { "
                        +         "\"whenRecognized\": true, "
                        +        "\"directRptRecognized\": true, "
                        +        "\"approvalNeeded\": true, "
                        +        "\"weeklyDigest\": true,"
                        +        "\"milestonReminder\": true }"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_email_preferences_when_recognized() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        +     "\"emailPreferences\": { "
                        +         "\"whenRecognized\": true "
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_email_preferences_direct_rpt_recognized() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        +     "\"emailPreferences\": { "
                        +        "\"directRptRecognized\": true "
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_email_preferences_approval_needed() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        +     "\"emailPreferences\": { "
                        +        "\"approvalNeeded\": true "
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void patch_project_profile_email_preferences_weekly_digest() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        +     "\"emailPreferences\": { "
                        +         "\"weeklyDigest\": true "
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_project_profile_email_preferences_milestone_reminders() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                        
                        +     "\"emailPreferences\": { "
                        +         "\"milestoneRemiders\": true "
                        +    "}"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_happyPath() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": true,"
                             +                "\"name\": \"AREA\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_missingFields() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_defaultFilter_happyPath() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"defaultFilter\": \"AREA\""
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_defaultFilterAndEnabledInSameCall() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +        "\"defaultFilter\": \"DEPARTMENT\","
                             +        "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": true,"
                             +                "\"name\": \"DEPARTMENT\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_defaultFilter_staticOptions() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +        "\"defaultFilter\": \"PUBLIC\""
                             +     "}"
                             + "}"
                )
        ).andExpect(status().isOk());

        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +        "\"defaultFilter\": \"NETWORK\""
                             +     "}"
                             + "}"
                )
        ).andExpect(status().isOk());
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidDefaultFilter() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"defaultFilter\": \"INVALID_FILTER\""
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidDefaultFilter_inactiveFilter() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +        "\"defaultFilter\": \"FUNCTION\""
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidDefaultFilter_notEnabled() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +        "\"defaultFilter\": \"STATE\""
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_filterOptions_happyPath() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": true,"
                             +                "\"name\": \"AREA\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidFilterName() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": true,"
                             +                "\"name\": \"TEST\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_disablingStaticFilterOption() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": false,"
                             +                "\"name\": \"PUBLIC\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest());

        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": false,"
                             +                "\"name\": \"NETWORK\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidFilterOption_inactiveFilter() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"filterOptions\": ["
                             +            "{"
                             +                "\"enabled\": true,"
                             +                "\"name\": \"FUNCTION\""
                             +            "}"
                             +        "]"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test
    public void patch_projectProfile_activityFeedFilterSearchEnabled_happyPath() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilters\": { "
                             +         "\"searchEnabled\": true"
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_projectProfile_activityFeedFilter_invalidSearchEnabled() throws Exception {
        mockMvc.perform(
            patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{"
                             +     "\"activityFeedFilter\": { "
                             +         "\"searchEnabled\": \"TEST\""
                             +     "}"
                             + "}"
                )
        )
            .andExpect(status().isBadRequest())
        ;
    }
    
    @Test 
    public void patch_project_profile_seo() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(
                        "{"
                        + "\"seo\": "
                        + "        {\"clientTitle\":\"CultureNext\", "
                        + "        \"clientDesc\":\"CultureNext\""
                        + "        }"
                        + "}"
                )
        )
                .andExpect(status().isOk())
        ;
    }
    
    @Test 
    public void patch_project_profile_from_email_address() throws Exception {
        mockMvc.perform(
                patch(PROJECT_PROFILE_ENDPOINT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("{\"fromEmailAddress\":\"CultureNext <supportM365@maritz.com>\"}")
        )
                .andExpect(status().isOk())
        ;
    }
}
