package com.maritz.culturenext.project.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.maritz.culturenext.dto.ActivityFeedFilterDTO;
import com.maritz.culturenext.dto.FilterDetailsDTO;
import com.maritz.culturenext.enums.ActivityFeedFilterEnum;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMethod;

import com.maritz.TestUtil;
import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.Country;
import com.maritz.core.jpa.entity.Language;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.TranslationConstants;
import com.maritz.culturenext.enums.CountryCodeEnum;
import com.maritz.culturenext.enums.LanguageCodeEnum;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.PointBankEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.groups.config.dao.GroupConfigServiceDao;
import com.maritz.culturenext.project.constants.ProjectProfileConstants;
import com.maritz.culturenext.project.dto.ColorsProfileDTO;
import com.maritz.culturenext.project.dto.EmailPreferencesDTO;
import com.maritz.culturenext.project.dto.ProjectMessagesDTO;
import com.maritz.culturenext.project.dto.ProjectProfilePublicDTO;
import com.maritz.culturenext.project.dto.SeoDTO;
import com.maritz.culturenext.project.dto.ServiceAnniversaryDTO;
import com.maritz.culturenext.project.dto.TermsAndConditionsDTO;
import com.maritz.culturenext.project.services.impl.ProjectProfileServiceImpl;
import com.maritz.culturenext.util.TranslationUtil;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;

public class ProjectProfileServiceUnitTest {

    @InjectMocks private ProjectProfileServiceImpl projectProfileService;
    @Mock private ApplicationDataService applicationDataService;
    @Mock private ConcentrixDao<ApplicationData> applicationDataDao;
    @Mock private ConcentrixDao<Country> countryDao;
    @Mock private ConcentrixDao<Language> languageDao;
    @Mock private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private GroupConfigServiceDao groupConfigServiceDao;
    @Mock private Environment environment;
    @Mock private Security security;    
    @Mock private TranslationUtil translationUtil;

    private String TEST_CLIENT_URL = "http://www.clienturl.com";
    private String TEST_CLIENT_NAME = "Novartis";
    private String TEST_DEFAULT_FILTER = ActivityFeedFilterEnum.AREA.name();
    private String TEST_AREA_FILTER_KEY = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
        ProjectConstants.DOT_DELIM +
        ActivityFeedFilterEnum.AREA;
    private String TEST_CITY_FILTER_KEY = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
        ProjectConstants.DOT_DELIM +
        ActivityFeedFilterEnum.CITY;
    private String TEST_COMPANY_FILTER_KEY = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
        ProjectConstants.DOT_DELIM +
        ActivityFeedFilterEnum.COMPANY_NAME;
    private String TEST_GRADE_FILTER_KEY = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
        ProjectConstants.DOT_DELIM +
        ActivityFeedFilterEnum.GRADE;
    private String TEST_DISABLED_FILTER_KEY = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
        ProjectConstants.DOT_DELIM +
        ActivityFeedFilterEnum.COUNTRY_CODE.name();
    private Long TEST_REPORT_ABUSE_ADMIN = 123456L;
    private Long TEST_UNASSIGNED_APPROVER_PAX_ID = 123456L;
    private String TEST_SUPPORT_EMAIL = "support@project.culturenext.com";
    private String TEST_PARTNER_SSO_URL = "https://partner.com/sso-login";
    private String TEST_PRIMARY_COLOR = "#aabbcc";
    private String TEST_SECONDARY_COLOR = "#112233";
    private String TEST_PRIMARY_TEXT_COLOR = "#FFEEDD";
    private String TEST_ADMIN_OVERVIEW_HELP = "Help text";
    private String TEST_LOGIN_WELCOME = "Welcome!";
    private String TEST_LOGIN_USERNAME_PLACEHOLDER = "Username";
    private String TEST_LOGIN_PASSWORD_PLACEHOLDER = "Password";
    private String TEST_SSO_WELCOME = "SSO Welcome!";
    private String TEST_LOGOUT_SSO = "Logged out!";
    private String TEST_SSO_ACCESS_ERROR = "ERROR";
    private String TEST_PARTNER_SSO_LINK = "Click here for SSO Partner";
    private String TEST_EMPLOYEE_ID = "Test ID!";
    private final int APPLICATION_DATA_SAVE_COUNT = 34; // increment this value whenever a field is added to ApplicationDataDTO
    private List<String> TEST_SITE_LANGUAGES = 
            Arrays.asList(LocaleCodeEnum.en_US.name(), LocaleCodeEnum.fr_CA.name(), LocaleCodeEnum.de_DE.name());
    private String TEST_INVALID_LANGUAGE_CODE = LocaleCodeEnum.nl_NL.name();
    private String AUTH_TOKEN = "auth-token";
    private String TEST_TC_EDIT_DATE_CURRENT = "2016-08-31 13:45:29.317";
    private String TEST_TC_EDIT_DATE_BEFORE_CURRENT = "2016-01-31 13:45:29.317";
    private String TEST_TC_MAJOR_CHANGE_DATE_CURRENT = "2016-09-20 13:45:29.317";
    private String TEST_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT = "2016-01-20 13:45:29.317";
    private String TEST_SEO_CLIENT_TITLE = "CultureNext";
    private String TEST_SEO_CLIENT_DESC = "CultureNext";
    private String TEST_FROM_EMAIL_ADDRESS = "CultureNext <supportM365@maritz.com>";
    private static final String SERVICE_ANNIVERSARY_PROVIDER = "Wayne's Wallet";
    private static final String SERVICE_ANNIVERSARY_BASE_URL = "https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif";
    
    private FindBy<LocaleInfo> localeInfoFindBy;
    private FindBy<Language> languageFindBy;
    private FindBy<Country> countryFindBy;
    private List<Language> testLanguages;
    private List<Country> testCountries;
    private List<LocaleInfo> testLocaleInfoEntities;

    private static Validator validator;

    ProjectProfilePublicDTO defaultProjectProfileDto;
    
    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.FALSE);
        PowerMockito.when(security.getAuthToken()).thenReturn(null);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        // ConcentrixDao mocking for getProjectPublicProfileInfo
        FindBy<ApplicationData> applicationDataFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> supportEmailFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> viewElementsFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> partnerSsoUrlFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> primaryColorFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> secondaryColorFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> textOnPrimaryColorFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> clientNameFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> reportAbuseAdminFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> unassignedApproverPaxIdFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> ssoEnabledFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> likingEnabledFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> commentingEnabledFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> engagementScoreEnabledFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> networkConnectionsEnabledFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> pointBankFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> whenRecognizedFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> directRptRecognizedFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> approvalNeededFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> weeklyDigestFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> notifyOthersFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> milestoneReminderFindBy= PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> employeeIdFindBy = PowerMockito.mock(FindBy.class);
        FindBy<ApplicationData> activityFeedFilterFindBy = PowerMockito.mock(FindBy.class);
        countryFindBy = PowerMockito.mock(FindBy.class);
        languageFindBy = PowerMockito.mock(FindBy.class);
        localeInfoFindBy = PowerMockito.mock(FindBy.class);

        PowerMockito.when(applicationDataDao.findBy()).thenReturn(applicationDataFindBy);
        PowerMockito.when(applicationDataFindBy.where(ProjectConstants.KEY_NAME)).thenReturn(applicationDataFindBy);

        PowerMockito.when(applicationDataFindBy.startsWith(ApplicationDataConstants.PROJECT_PROFILE_VIEW))
                .thenReturn(viewElementsFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_SUPPORT_EMAIL))
                .thenReturn(supportEmailFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_SSO_URL))
                .thenReturn(partnerSsoUrlFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR))
                .thenReturn(primaryColorFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR))
                .thenReturn(secondaryColorFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR))
                .thenReturn(textOnPrimaryColorFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME))
                .thenReturn(clientNameFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN))
                .thenReturn(reportAbuseAdminFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER))
                .thenReturn(unassignedApproverPaxIdFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(ssoEnabledFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED))
                .thenReturn(likingEnabledFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(commentingEnabledFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED))
                .thenReturn(engagementScoreEnabledFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED))
                .thenReturn(networkConnectionsEnabledFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_POINT_BANK))
                .thenReturn(pointBankFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_EMAIL_WHEN_RECOGNIZED))
            .thenReturn(whenRecognizedFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_EMAIL_DIRECT_RPT_RECOGNIZED))
            .thenReturn(directRptRecognizedFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_EMAIL_APPROVAL_NEEDED))
            .thenReturn(approvalNeededFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_EMAIL_WEEKLY_DIGEST))
            .thenReturn(weeklyDigestFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_NOTIFY_OTHERS))
                .thenReturn(notifyOthersFindBy);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_MILESTONE_REMINDER))
            .thenReturn(milestoneReminderFindBy);
        
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID))
            .thenReturn(employeeIdFindBy);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SEARCH_ENABLED))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT))
            .thenReturn(TEST_DEFAULT_FILTER);
        PowerMockito.when(environment.getProperty(TEST_AREA_FILTER_KEY))
            .thenReturn(Boolean.TRUE.toString());
        PowerMockito.when(environment.getProperty(TEST_CITY_FILTER_KEY))
            .thenReturn(Boolean.FALSE.toString());
        PowerMockito.when(environment.getProperty(TEST_COMPANY_FILTER_KEY))
            .thenReturn(Boolean.FALSE.toString());
        PowerMockito.when(environment.getProperty(TEST_DISABLED_FILTER_KEY))
            .thenReturn(Boolean.FALSE.toString());
        
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SUPPORT_EMAIL))
                .thenReturn(TEST_SUPPORT_EMAIL);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_URL))
                .thenReturn(TEST_PARTNER_SSO_URL);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR))
                .thenReturn(TEST_PRIMARY_COLOR);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR))
                .thenReturn(TEST_SECONDARY_COLOR);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR))
                .thenReturn(TEST_PRIMARY_TEXT_COLOR);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME))
                .thenReturn(TEST_CLIENT_NAME);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_REPORT_ABUSE_ADMIN))
                .thenReturn(String.valueOf(TEST_REPORT_ABUSE_ADMIN));
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER))
                .thenReturn(String.valueOf(TEST_UNASSIGNED_APPROVER_PAX_ID));
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_LIKING_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_ENGAGEMENT_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_POINT_BANK))
                .thenReturn(PointBankEnum.EY.getBank());
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_URL))
                .thenReturn(TEST_CLIENT_URL);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_ADMIN_HELP_TEXT))
                .thenReturn(TEST_ADMIN_OVERVIEW_HELP);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_LOGIN_WELCOME_MESSAGE))
                .thenReturn(TEST_LOGIN_WELCOME);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_USERNAME_PLACEHOLDER))
                .thenReturn(TEST_LOGIN_USERNAME_PLACEHOLDER);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_PASSWORD_PLACEHOLDER))
                .thenReturn(TEST_LOGIN_PASSWORD_PLACEHOLDER);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_WELCOME_MESSAGE))
                .thenReturn(TEST_SSO_WELCOME);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_LOGOUT_MESSAGE))
                .thenReturn(TEST_LOGOUT_SSO);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ERROR_MESSAGE))
                .thenReturn(TEST_SSO_ACCESS_ERROR);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_LINK_TEXT))
                .thenReturn(TEST_PARTNER_SSO_LINK);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID))
                .thenReturn(TEST_EMPLOYEE_ID);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_CALENDAR_ENABLED))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_WHEN_RECOGNIZED))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_DIRECT_RPT_RECOGNIZED))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_APPROVAL_NEEDED))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_EMAIL_WEEKLY_DIGEST))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_NOTIFY_OTHERS))
                .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_MILESTONE_REMINDER))
                .thenReturn(ProjectConstants.TRUE);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_ENABLED))
            .thenReturn(ProjectConstants.TRUE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL))
            .thenReturn(SERVICE_ANNIVERSARY_BASE_URL);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_PROVIDER))
            .thenReturn(SERVICE_ANNIVERSARY_PROVIDER);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_TITLE))
            .thenReturn(TEST_SEO_CLIENT_TITLE);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SEO_CLIENT_DESC))
            .thenReturn(TEST_SEO_CLIENT_DESC);
        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_NOTIFICATION_FROM_EMAIL_ADDRESS))
            .thenReturn(TEST_FROM_EMAIL_ADDRESS);
        
        LocaleInfo americanEnglish = new LocaleInfo();
        americanEnglish.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        americanEnglish.setLanguageCode(LanguageCodeEnum.en.name());
        americanEnglish.setCountryCode(CountryCodeEnum.US.name());
        americanEnglish.setLocaleCode(LocaleCodeEnum.en_US.name());

        LocaleInfo canadianFrench = new LocaleInfo();
        canadianFrench.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        canadianFrench.setLanguageCode(LanguageCodeEnum.fr.name());
        canadianFrench.setCountryCode(CountryCodeEnum.CA.name());
        canadianFrench.setLocaleCode(LocaleCodeEnum.fr_CA.name());

        LocaleInfo germanGerman = new LocaleInfo();
        germanGerman.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        germanGerman.setLanguageCode(LanguageCodeEnum.de.name());
        germanGerman.setCountryCode(CountryCodeEnum.DE.name());
        germanGerman.setLocaleCode(LocaleCodeEnum.de_DE.name());

        testLocaleInfoEntities = Arrays.asList(americanEnglish, canadianFrench, germanGerman);

        PowerMockito.when(localeInfoDao.findBy()).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE)).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.where(ProjectConstants.LOCALE_CODE)).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.in(anyListOf(String.class))).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.eq(anyString())).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.find()).thenReturn(testLocaleInfoEntities);
        PowerMockito.when(localeInfoFindBy.find(ProjectConstants.LOCALE_CODE, String.class)).thenReturn(TEST_SITE_LANGUAGES);
        PowerMockito.when(localeInfoFindBy.exists()).thenReturn(Boolean.TRUE);

        Language english = new Language();
        english.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        english.setLanguageCode(LanguageCodeEnum.en.name());

        Language french = new Language();
        french.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        french.setLanguageCode(LanguageCodeEnum.fr.name());

        Language german = new Language();
        german.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        german.setLanguageCode(LanguageCodeEnum.de.name());

        testLanguages = Arrays.asList(english, french, german);

        PowerMockito.when(languageDao.findBy()).thenReturn(languageFindBy);
        PowerMockito.when(languageFindBy.where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE)).thenReturn(languageFindBy);
        PowerMockito.when(languageFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(languageFindBy);
        PowerMockito.when(languageFindBy.where(ProjectConstants.LANGUAGE_CODE)).thenReturn(languageFindBy);
        PowerMockito.when(languageFindBy.in(anyListOf(String.class))).thenReturn(languageFindBy);
        PowerMockito.when(languageFindBy.find()).thenReturn(testLanguages);

        Country unitedStates = new Country();
        unitedStates.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        unitedStates.setCountryCode(CountryCodeEnum.US.name());

        Country canada = new Country();
        canada.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        canada.setCountryCode(CountryCodeEnum.CA.name());

        Country germany = new Country();
        germany.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());
        germany.setCountryCode(CountryCodeEnum.DE.name());

        testCountries = Arrays.asList(unitedStates, canada, germany);

        PowerMockito.when(countryDao.findBy()).thenReturn(countryFindBy);
        PowerMockito.when(countryFindBy.where(ProjectConstants.DISPLAY_STATUS_TYPE_CODE)).thenReturn(countryFindBy);
        PowerMockito.when(countryFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(countryFindBy);
        PowerMockito.when(countryFindBy.where(ProjectConstants.COUNTRY_CODE)).thenReturn(countryFindBy);
        PowerMockito.when(countryFindBy.in(anyListOf(String.class))).thenReturn(countryFindBy);
        PowerMockito.when(countryFindBy.find()).thenReturn(testCountries);

        //ConcentrixDao and object mocking for getProjectMessages
        List<ApplicationData> applicationData = new ArrayList<>();
        applicationData.add(new ApplicationData());
        applicationData.get(0).setKeyName(ApplicationDataConstants.KEY_NAME_ADMIN_HELP_TEXT);
        applicationData.get(0).setValue("Help text");
        applicationData.get(0).setApplicationDataId(0L);
        applicationData.add(new ApplicationData());
        applicationData.get(1).setKeyName(ApplicationDataConstants.KEY_NAME_LOGIN_WELCOME_MESSAGE);
        applicationData.get(1).setValue("Welcome!");
        applicationData.get(1).setApplicationDataId(TestConstants.ID_1);
        applicationData.add(new ApplicationData());
        applicationData.get(2).setKeyName(ApplicationDataConstants.KEY_NAME_USERNAME_PLACEHOLDER);
        applicationData.get(2).setValue("Username");
        applicationData.get(2).setApplicationDataId(TestConstants.ID_2);
        applicationData.add(new ApplicationData());
        applicationData.get(3).setKeyName(ApplicationDataConstants.KEY_NAME_PASSWORD_PLACEHOLDER);
        applicationData.get(3).setValue("Password");
        applicationData.get(3).setApplicationDataId(3L);
        applicationData.add(new ApplicationData());
        applicationData.get(4).setKeyName(ApplicationDataConstants.KEY_NAME_SSO_WELCOME_MESSAGE);
        applicationData.get(4).setValue("SSO Welcome!");
        applicationData.get(4).setApplicationDataId(4L);
        applicationData.add(new ApplicationData());
        applicationData.get(5).setKeyName(ApplicationDataConstants.KEY_NAME_SSO_LOGOUT_MESSAGE);
        applicationData.get(5).setValue("Logged out!");
        applicationData.get(5).setApplicationDataId(5L);
        applicationData.add(new ApplicationData());
        applicationData.get(6).setKeyName(ApplicationDataConstants.KEY_NAME_SSO_ERROR_MESSAGE);
        applicationData.get(6).setValue("ERROR");
        applicationData.get(6).setApplicationDataId(6L);
        applicationData.add(new ApplicationData());
        applicationData.get(7).setKeyName(ApplicationDataConstants.KEY_NAME_SSO_LINK_TEXT);
        applicationData.get(7).setValue("Click here for SSO Partner");
        applicationData.get(7).setApplicationDataId(7L);
        applicationData.add(new ApplicationData());
        applicationData.get(8).setKeyName(ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID);
        applicationData.get(8).setValue("Participant ID");
        applicationData.get(8).setApplicationDataId(8L);

        List<String> applicationDataKeyNames = Arrays.asList(
                ApplicationDataConstants.KEY_NAME_ADMIN_HELP_TEXT,
                ApplicationDataConstants.KEY_NAME_LOGIN_WELCOME_MESSAGE,
                ApplicationDataConstants.KEY_NAME_USERNAME_PLACEHOLDER,
                ApplicationDataConstants.KEY_NAME_PASSWORD_PLACEHOLDER,
                ApplicationDataConstants.KEY_NAME_SSO_WELCOME_MESSAGE,
                ApplicationDataConstants.KEY_NAME_SSO_LOGOUT_MESSAGE,
                ApplicationDataConstants.KEY_NAME_SSO_ERROR_MESSAGE,
                ApplicationDataConstants.KEY_NAME_SSO_LINK_TEXT,
                ApplicationDataConstants.KEY_NAME_EMPLOYEE_ID
        );

        FindBy<ApplicationData> projectUrlFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(applicationDataFindBy.eq(ApplicationDataConstants.KEY_NAME_CLIENT_URL))
                .thenReturn(projectUrlFindBy);
        PowerMockito.when(projectUrlFindBy.findOne(ProjectConstants.VALUE, String.class))
                .thenReturn("http://www.clienturl.com");
        PowerMockito.when(applicationDataFindBy.in(applicationDataKeyNames)).thenReturn(applicationDataFindBy);
        PowerMockito.when(applicationDataFindBy.find()).thenReturn(applicationData);

        Map<Long, String> translations = new HashMap<>();
        translations.put(0L, "Translated Help Text");
        translations.put(TestConstants.ID_1, "Translated Welcome!");
        translations.put(TestConstants.ID_2, "Translated Username");
        translations.put(3L, "Translated Password");
        translations.put(4L, "Translated SSO Welcome!");
        translations.put(5L, "Translated Logged Out!");
        translations.put(6L, "Translated ERROR");
        translations.put(7L, "Translated link text");
        translations.put(8L, "Translated Employee ID");

        PowerMockito.when(translationUtil.getTranslationsForListOfIds(eq(LocaleCodeEnum.en_US.name()),
                eq(TableName.APPLICATION_DATA.name()),
                eq(TranslationConstants.COLUMN_NAME_VALUE), anyListOf(Long.class), 
                anyMapOf(Long.class, String.class))).thenReturn(null);
        PowerMockito.when(translationUtil.getTranslationsForListOfIds(eq(LocaleCodeEnum.fr_CA.name()),
                eq(TableName.APPLICATION_DATA.name()),
                eq(TranslationConstants.COLUMN_NAME_VALUE), anyListOf(Long.class),
                anyMapOf(Long.class, String.class))).thenReturn(translations);

        // Mock Pax object
        Pax testPax = new Pax();
        testPax.setPaxId(123456L);
        PowerMockito.when(paxDao.findById(123456L)).thenReturn(testPax);

        defaultProjectProfileDto = createProjectProfileDTO();
        
        ApplicationDataDTO dummyData = new ApplicationDataDTO();
        dummyData.setId(TestConstants.ID_1);
        
        PowerMockito.when(applicationDataService.getApplicationData(anyString())).thenReturn(dummyData);
        
        //Email Preferences
        
        // Terms and Conditions preferences
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_ENFORCE_TC_AT_LOGIN))
            .thenReturn(new ApplicationDataDTO().setId(TestConstants.ID_1).setValue(Boolean.FALSE.toString()));
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_SHOW_IN_FOOTER))
            .thenReturn(new ApplicationDataDTO().setId(TestConstants.ID_1).setValue(Boolean.FALSE.toString()));
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_EDIT_DATE))
            .thenReturn(new ApplicationDataDTO().setId(TestConstants.ID_1).setValue(TEST_TC_EDIT_DATE_CURRENT));
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_TC_MAJOR_CHANGE_DATE))
            .thenReturn(new ApplicationDataDTO().setId(TestConstants.ID_1).setValue(TEST_TC_MAJOR_CHANGE_DATE_CURRENT));


        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testGetProjectPublicInfoDTOAsNonAdmin() {
        ProjectProfilePublicDTO projectProfileDto = projectProfileService.getProjectPublicInfoDTO();

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNull(projectProfileDto.getReportAbuseAdmin());
        assertNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEditDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEnforceTCAtLogin());
        assertNotNull(projectProfileDto.getTermsAndConditions().getMajorChangeDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getShowInFooter());
        assertNotNull(projectProfileDto.getActivityFeedFilter());
    }

    @Test
    public void testGetProjectPublicInfoDTOAsAdmin() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = projectProfileService.getProjectPublicInfoDTO();

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEditDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEnforceTCAtLogin());
        assertNotNull(projectProfileDto.getTermsAndConditions().getMajorChangeDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getShowInFooter());
        assertNotNull(projectProfileDto.getActivityFeedFilter());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
        assertNotNull(projectProfileDto.getFromEmailAddress());
    }

    @Test
    public void test_putProjectPublicInfoDTOAsAdmin() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);

        Mockito.verify(applicationDataService,times(APPLICATION_DATA_SAVE_COUNT))
            .setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        
        assertNotNull(projectProfileDto.getTermsAndConditions().getEditDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEnforceTCAtLogin());
        assertNotNull(projectProfileDto.getTermsAndConditions().getMajorChangeDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getShowInFooter());
        assertNotNull(projectProfileDto.getServiceAnniversaryEnabled());
        assertNotNull(projectProfileDto.getServiceAnniversary());
        assertNotNull(projectProfileDto.getServiceAnniversary().getBaseUrl());
        assertNotNull(projectProfileDto.getServiceAnniversary().getProvider());
        assertNotNull(projectProfileDto.getActivityFeedFilter());
        assertNotNull(projectProfileDto.getActivityFeedFilter().getFilterOptions());
        assertNotNull(projectProfileDto.getActivityFeedFilter().getDefaultFilter());
        assertNotNull(projectProfileDto.getActivityFeedFilter().getSearchEnabled());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
        assertNotNull(projectProfileDto.getFromEmailAddress());
    }

    @Test
    public void test_putProjectPublicInfoDTOAsAdminNewSiteLanguages() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSiteLanguages(Arrays.asList(
                LocaleCodeEnum.en_US.name(),
                LocaleCodeEnum.fr_CA.name(),
                LocaleCodeEnum.de_DE.name(),
                LocaleCodeEnum.zh_CN.name()));

        Language chinese = new Language();
        chinese.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        chinese.setLanguageCode(LanguageCodeEnum.zh.name());

        List<Language> specificTestLanguages = new ArrayList<>(testLanguages);
        specificTestLanguages.add(chinese);

        Country china = new Country();
        china.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        china.setCountryCode(CountryCodeEnum.CN.name());

        List<Country> specificTestCountries = new ArrayList<>(testCountries);
        specificTestCountries.add(china);

        LocaleInfo traditionalChinese = new LocaleInfo();
        traditionalChinese.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());
        traditionalChinese.setCountryCode(CountryCodeEnum.CN.name());
        traditionalChinese.setLanguageCode(LanguageCodeEnum.zh.name());
        traditionalChinese.setLocaleCode(LocaleCodeEnum.zh_CN.name());

        List<LocaleInfo> specificTestLocaleInfoEntities = new ArrayList<>(testLocaleInfoEntities);
        specificTestLocaleInfoEntities.add(traditionalChinese);

        List<String> specificTestSiteLanguages = new ArrayList<>(TEST_SITE_LANGUAGES);
        specificTestSiteLanguages.add(LocaleCodeEnum.zh_CN.name());

        FindBy testLanguageFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(languageFindBy.where(ProjectConstants.LANGUAGE_CODE)).thenReturn(testLanguageFindBy);
        PowerMockito.when(testLanguageFindBy.in(anyListOf(String.class))).thenReturn(testLanguageFindBy);
        PowerMockito.when(testLanguageFindBy.find()).thenReturn(specificTestLanguages);

        FindBy testCountryFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(countryFindBy.where(ProjectConstants.COUNTRY_CODE)).thenReturn(testCountryFindBy);
        PowerMockito.when(testCountryFindBy.in(anyListOf(String.class))).thenReturn(testCountryFindBy);
        PowerMockito.when(testCountryFindBy.find()).thenReturn(specificTestCountries);

        FindBy testLocaleInfoFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(localeInfoFindBy.where(ProjectConstants.LOCALE_CODE)).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.in(anyListOf(String.class))).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.eq(anyString())).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.find()).thenReturn(specificTestLocaleInfoEntities);
        PowerMockito.when(testLocaleInfoFindBy.exists()).thenReturn(Boolean.TRUE);
                
        PowerMockito.when(localeInfoFindBy.find(ProjectConstants.LOCALE_CODE, String.class))
                .thenReturn(specificTestSiteLanguages);
        
        ProjectProfilePublicDTO projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);

        Mockito.verify(applicationDataService,times(APPLICATION_DATA_SAVE_COUNT))
                .setApplicationData(any(ApplicationDataDTO.class));
        Mockito.verify(languageDao, times(7)).save(any(Language.class));
        Mockito.verify(countryDao, times(7)).save(any(Country.class));
        Mockito.verify(localeInfoDao, times(7)).save(any(LocaleInfo.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEditDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEnforceTCAtLogin());
        assertNotNull(projectProfileDto.getTermsAndConditions().getMajorChangeDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getShowInFooter());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
        assertNotNull(projectProfileDto.getFromEmailAddress());
    }

    @Test
    public void test_putProjectPublicInfoDTOAsAdminRemoveSiteLanguages() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSiteLanguages(Arrays.asList(LocaleCodeEnum.en_US.name(), LocaleCodeEnum.fr_CA.name()));

        List<Language> specificTestLanguages = new ArrayList<>(testLanguages);
        specificTestLanguages.remove(2);
        List<Country> specificTestCountries = new ArrayList<>(testCountries);
        specificTestCountries.remove(2);
        List<LocaleInfo> specificTestLocaleInfoEntities = new ArrayList<>(testLocaleInfoEntities);
        specificTestLocaleInfoEntities.remove(2);

        FindBy testLanguageFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(languageFindBy.where(ProjectConstants.LANGUAGE_CODE)).thenReturn(testLanguageFindBy);
        PowerMockito.when(testLanguageFindBy.in(anyListOf(String.class))).thenReturn(testLanguageFindBy);
        PowerMockito.when(testLanguageFindBy.find()).thenReturn(specificTestLanguages);

        FindBy testCountryFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(countryFindBy.where(ProjectConstants.COUNTRY_CODE)).thenReturn(testCountryFindBy);
        PowerMockito.when(testCountryFindBy.in(anyListOf(String.class))).thenReturn(testCountryFindBy);
        PowerMockito.when(testCountryFindBy.find()).thenReturn(specificTestCountries);

        FindBy testLocaleInfoFindBy = PowerMockito.mock(FindBy.class);
        PowerMockito.when(localeInfoFindBy.where(ProjectConstants.LOCALE_CODE)).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.in(anyListOf(String.class))).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.eq(anyString())).thenReturn(testLocaleInfoFindBy);
        PowerMockito.when(testLocaleInfoFindBy.find()).thenReturn(specificTestLocaleInfoEntities);
        PowerMockito.when(testLocaleInfoFindBy.exists()).thenReturn(Boolean.TRUE);
        ProjectProfilePublicDTO projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);

        Mockito.verify(applicationDataService,times(APPLICATION_DATA_SAVE_COUNT))
                .setApplicationData(any(ApplicationDataDTO.class));
        Mockito.verify(languageDao, times(5)).save(any(Language.class));
        Mockito.verify(countryDao, times(5)).save(any(Country.class));
        Mockito.verify(localeInfoDao, times(5)).save(any(LocaleInfo.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEditDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getEnforceTCAtLogin());
        assertNotNull(projectProfileDto.getTermsAndConditions().getMajorChangeDate());
        assertNotNull(projectProfileDto.getTermsAndConditions().getShowInFooter());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
        assertNotNull(projectProfileDto.getFromEmailAddress());
    }

    @Test
    public void test_updateProjectProfile_fail_missing_clientName() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setClientName(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_CLIENT_NAME, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_termsAndConditions_enforceLogin() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setEnforceTCAtLogin(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_ENFORCE_LOGIN_CODE, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_termsAndConditions_showInFooter() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setShowInFooter(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_SHOW_IN_FOOTER_CODE, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_termsAndConditions_editDate() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setEditDate(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_EDIT_DATE_CODE, errorMsgException);
        }
        
        defaultProjectProfileDto.getTermsAndConditions().setEditDate("");

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_EDIT_DATE_CODE, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_termsAndConditions_majorChangeDate() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setMajorChangeDate(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_MAJOR_CHANGE_DATE_CODE, errorMsgException);
        }
        
        defaultProjectProfileDto.getTermsAndConditions().setMajorChangeDate("");

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TC_MAJOR_CHANGE_DATE_CODE, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_termsAndConditions_majorChangeDate_before_current_value() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setMajorChangeDate(TEST_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_TC_MAJOR_CHANGE_DATE_BEFORE_CURRENT_CODE, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_termsAndConditions_editDate_before_current_value() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getTermsAndConditions().setEditDate(TEST_TC_EDIT_DATE_BEFORE_CURRENT);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_TC_EDIT_DATE_BEFORE_CURRENT_CODE, errorMsgException);
        }
    }
    
    
    @Test
    public void test_updateProjectProfile_fail_missing_supportEmail() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSupportEmail(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_SUPPORT_EMAIL, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_invalid_supportEmail() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSupportEmail("invalidEmailformat");
        Set<ConstraintViolation<ProjectProfilePublicDTO>> constraintViolations = 
                validator.validate(defaultProjectProfileDto);
        
        if(constraintViolations.isEmpty()){
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } 

    }

    @Test
    public void test_updateProjectProfile_fail_missing_partnerSsoUrl() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setPartnerSsoUrl(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_PARTNER_SSO_URL, 
                    errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_pointBank() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setPointBank(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_POINT_BANK, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_invalid_pointBank() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setPointBank("NOT_VALID");

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_INVALID_POINT_BANK, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_primaryColor() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getColors().setPrimaryColor(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_PRIMARY_COLOR, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_secondaryColor() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getColors().setSecondaryColor(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_SECONDARY_COLOR, 
                    errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_textPrimaryColor() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getColors().setTextOnPrimaryColor(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_TEXT_PRIMARY_COLOR,
                    errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_likingEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setLikingEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_LIKING_ENABLED, errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_commentingEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setCommentingEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_COMMENTING_ENABLED, 
                    errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_engagementScoreEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setEngagementScoreEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_ENGAGEMENT_SCORE_ENABLED, 
                    errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_ssoEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSsoEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_SSO_ENABLED, 
                    errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_siteLanguages() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setSiteLanguages(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_SITE_LANGUAGES, 
                    errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_activityFeedFilter() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setActivityFeedFilter(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_MISSING_ACTIVITY_FEED_FILTER,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_invalidFilterOption_invalidName() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName("INVALID_NAME");
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_INVALID_FILTER_OPTIONS,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_invalidFilterOption_inactiveFilterOption() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.GRADE.name());
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        // The GRADE filter option will be missing from APPLICATION_DATA
        PowerMockito.when(applicationDataService.getApplicationData(TEST_GRADE_FILTER_KEY))
            .thenReturn(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_INACTIVE_FILTER_OPTIONS,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missingFilterOptions() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        activityFeedFilterDto.setFilterOptions(null);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_MISSING_FILTER_OPTIONS,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_disablingStaticFilterOption_everyone() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.PUBLIC.name());
        filterOption.setEnabled(false);
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_disablingStaticFilterOption_meAndMyNetwork() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.NETWORK.name());
        filterOption.setEnabled(false);
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_CANNOT_DISABLE_STATIC_FILTER_OPTIONS,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_invalidDefaultFilter() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.AREA.name());
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter("INVALID_FILTER");
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_INVALID_DEFAULT_FILTER,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missingDefaultFilter() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
            .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.AREA.name());
        List<FilterDetailsDTO> filterOptions = Collections.singletonList(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(null);
        activityFeedFilterDto.setFilterOptions(filterOptions);
        activityFeedFilterDto.setSearchEnabled(Boolean.TRUE);

        defaultProjectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1,
                                            ProjectProfileConstants.ERROR_MISSING_DEFAULT_FILTER,
                                            errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_malformed_siteLanguages() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);
        
        PowerMockito.when(localeInfoDao.findBy()).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.where(ProjectConstants.LOCALE_CODE)).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.eq("xx_XX")).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.exists()).thenReturn(Boolean.FALSE);

        defaultProjectProfileDto.setSiteLanguages(Arrays.asList("INVALID", "STILL BAD", "xx_XX"));

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            
            // Can't use TestUtil here since xx_XX is technically formatted properly so a different error message needs to be thrown
            assertEquals(3, errorMsgException.getErrorMessages().size());
            
            int expectedInvalid = 3;
            int actualInvalid = 0;
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                if(ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE.equalsIgnoreCase(errorMessage.getCode())) {
                    actualInvalid++;
                }
                else {
                    fail("Unexpected error!");
                }
            }
            assertEquals(expectedInvalid, actualInvalid);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_inactive_unassignedApproverPaxId() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setUnassignedApproverPaxId(123L);
        PowerMockito.when(paxDao.findById(123L)).thenReturn(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_NONEXISTENT_UNASSIGNED_APPROVER_PAX_ID_MSG,
                    errorMsgException);
        }
    }


    @Test
    public void test_updateProjectProfile_fail_missing_calendarEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.setCalendarEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_CALENDAR_ENABLED, 
                    errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_email_whenRecognized() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getEmailPreferences().setWhenRecognized(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_EMAIL_WHEN_RECOGNIZED, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_email_directRptRecognized() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getEmailPreferences().setDirectRptRecognized(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_EMAIL_DIRECT_RPT_RECOGNIZED, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_email_approvalNeeded() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getEmailPreferences().setApprovalNeeded(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_EMAIL_APPROVAL_NEEDED, errorMsgException);
        }
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_email_weeklyDigest() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn("auth-token");

        defaultProjectProfileDto.getEmailPreferences().setWeeklyDigest(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_EMAIL_WEEKLY_DIGEST, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_email_notifyOthers() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn("auth-token");

        defaultProjectProfileDto.getEmailPreferences().setNotifyOthers(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_NOTIFY_OTHERS, errorMsgException);
        }
    }

    @Test
    public void test_updateProjectProfile_fail_missing_serviceAnniversary_provider() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn("auth-token");

        defaultProjectProfileDto.getServiceAnniversary().setProvider(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_MISSING_SERVICE_ANNIVERSARY_PROVIDER, errorMsgException);
        }
    }
    
    @Test 
    public void test_patch_project_profile_client_name() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setClientName("Test Client");
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_report_abuse_admin() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setReportAbuseAdmin(123456L);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_support_email() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setSupportEmail("testEmail@maritz.com");
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_partner_sso_url() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setPartnerSsoUrl("http://thisisatest.com/");
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_sso_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setSsoEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_liking_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setLikingEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_commenting_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setCommentingEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_engagement_score_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setEngagementScoreEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_network_connections_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setNetworkConnectionsEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_calendar_enabled() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setCalendarEnabled(true);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_point_bank() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setPointBank(PointBankEnum.ABS.getBank());
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_colors() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        ColorsProfileDTO colorsDto = new ColorsProfileDTO();
        colorsDto.setPrimaryColor("#AA0000");
        colorsDto.setSecondaryColor("#000000");
        colorsDto.setTextOnPrimaryColor("#FFFFFF");
        projectProfileDto.setColors(colorsDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(3)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_site_languages() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setSiteLanguages(Arrays.asList(
                LocaleCodeEnum.en_US.name(), 
                LocaleCodeEnum.fr_CA.name(), 
                LocaleCodeEnum.de_DE.name()));
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        //Site languages are not saved in application data
        Mockito.verify(languageDao, times(6)).save(any(Language.class));
        Mockito.verify(countryDao, times(6)).save(any(Country.class));
        Mockito.verify(localeInfoDao, times(6)).save(any(LocaleInfo.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_unassigned_approver_pax_id() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setUnassignedApproverPaxId(123456L);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_email_preferences() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setWhenRecognized(true);
        emailPreferencesDto.setDirectRptRecognized(true);
        emailPreferencesDto.setApprovalNeeded(true);
        emailPreferencesDto.setWeeklyDigest(true);
        emailPreferencesDto.setNotifyOthers(true);
        projectProfileDto.setEmailPreferences(emailPreferencesDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(5)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_email_preferences_when_recognized() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setWhenRecognized(true);
        projectProfileDto.setEmailPreferences(emailPreferencesDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_email_preferences_direct_rpt_recognized() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setDirectRptRecognized(true);
        projectProfileDto.setEmailPreferences(emailPreferencesDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_email_preferences_approval_needed() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setApprovalNeeded(true);
        projectProfileDto.setEmailPreferences(emailPreferencesDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }

    @Test
    public void test_patch_projectProfile_activityFeedFilter_filterOptions() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.COMPANY_NAME.name());
        filterOption.setEnabled(Boolean.TRUE);
        List<FilterDetailsDTO> filterOptions = new ArrayList<>();
        filterOptions.add(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setFilterOptions(filterOptions);
        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getActivityFeedFilter());
        assertNotNull(projectProfileDto.getActivityFeedFilter().getFilterOptions());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }

    @Test
    public void test_patch_projectProfile_activityFeedFilter_defaultFilter() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ActivityFeedFilterDTO activityFeedFilterDto = new ActivityFeedFilterDTO();
        activityFeedFilterDto.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setActivityFeedFilter(activityFeedFilterDto);

        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getActivityFeedFilter());
        assertNotNull(projectProfileDto.getActivityFeedFilter().getDefaultFilter());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test 
    public void test_patch_project_profile_seo() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        SeoDTO seo = new SeoDTO();
        seo.setClientTitle(TEST_SEO_CLIENT_TITLE);
        seo.setClientDesc(TEST_SEO_CLIENT_DESC);
        projectProfileDto.setSeo(seo);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(2)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
    }
    
    @Test 
    public void test_patch_project_profile_from_email_address() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setFromEmailAddress(TEST_FROM_EMAIL_ADDRESS);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
        assertNotNull(projectProfileDto.getSeo());
        assertNotNull(projectProfileDto.getSeo().getClientTitle());
        assertNotNull(projectProfileDto.getSeo().getClientDesc());
        assertNotNull(projectProfileDto.getFromEmailAddress());
    }
    
    @Test
    public void test_updateProjectProfile_fail_missing_searchEnabled() {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        defaultProjectProfileDto.getActivityFeedFilter().setSearchEnabled(null);

        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PUT.toString(), defaultProjectProfileDto);
            fail("Updating project profiles should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.MISSING_SEARCH_ENABLED_ERROR_MESSAGE, 
                    errorMsgException);
        }
    }

    @Test 
    public void test_patch_project_profile_email_preferences_weekly_digest() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setWeeklyDigest(true);
        projectProfileDto.setEmailPreferences(emailPreferencesDto);
        
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        Mockito.verify(applicationDataService,times(1)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
    
    @Test
    public void test_patch_project_profile_null_fields() throws Exception {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE)).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getAuthToken()).thenReturn(AUTH_TOKEN);

        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.toString(), projectProfileDto);

        //Nothing should be saved if nothing is passed in
        Mockito.verify(applicationDataService,times(0)).setApplicationData(any(ApplicationDataDTO.class));

        assertNotNull(projectProfileDto);
        assertNotNull(projectProfileDto.getClientName());
        assertNotNull(projectProfileDto.getPartnerSsoUrl());
        assertNotNull(projectProfileDto.getColors());
        assertNotNull(projectProfileDto.getColors().getPrimaryColor());
        assertNotNull(projectProfileDto.getColors().getSecondaryColor());
        assertNotNull(projectProfileDto.getColors().getTextOnPrimaryColor());
        assertNotNull(projectProfileDto.getSsoEnabled());
        assertNotNull(projectProfileDto.getSupportEmail());
        assertNotNull(projectProfileDto.getReportAbuseAdmin());
        assertNotNull(projectProfileDto.getUnassignedApproverPaxId());
        assertNotNull(projectProfileDto.getLikingEnabled());
        assertNotNull(projectProfileDto.getCommentingEnabled());
        assertNotNull(projectProfileDto.getEngagementScoreEnabled());
        assertNotNull(projectProfileDto.getPointBank());
        assertNotNull(projectProfileDto.getSiteLanguages());
        assertNotNull(projectProfileDto.getCalendarEnabled());
        assertNotNull(projectProfileDto.getEmailPreferences());
        assertNotNull(projectProfileDto.getEmailPreferences().getWhenRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getDirectRptRecognized());
        assertNotNull(projectProfileDto.getEmailPreferences().getApprovalNeeded());
        assertNotNull(projectProfileDto.getEmailPreferences().getWeeklyDigest());
        assertNotNull(projectProfileDto.getEmailPreferences().getNotifyOthers());
        assertNotNull(projectProfileDto.getEmailPreferences().getMilestoneReminder());
    }
        
    @Test
    public void test_getProjectProfileMessages_null_languageCode() {
        ProjectMessagesDTO projectMessagesDTO = projectProfileService.getProjectMessages(null);

        assertNotNull(projectMessagesDTO);
        assertNotNull(projectMessagesDTO.getLoginWelcomeMessage());
        assertNotNull(projectMessagesDTO.getLogoutSSOMessage());
        assertNotNull(projectMessagesDTO.getLoginSSOWelcomeMessage());
        assertNotNull(projectMessagesDTO.getSsoAccessErrorMessage());
        assertNotNull(projectMessagesDTO.getAdminOverviewHelpText());
        assertNotNull(projectMessagesDTO.getPartnerSSOLinkText());
        assertNotNull(projectMessagesDTO.getLoginPasswordPlaceholder());
        assertNotNull(projectMessagesDTO.getLoginUsernamePlaceholder());
    }

    @Test
    public void test_getProjectProfileMessages_default_languageCode() {
        ProjectMessagesDTO projectMessagesDTO = projectProfileService.getProjectMessages(LocaleCodeEnum.en_US.name());

        assertNotNull(projectMessagesDTO);
        assertNotNull(projectMessagesDTO.getLoginWelcomeMessage());
        assertNotNull(projectMessagesDTO.getLogoutSSOMessage());
        assertNotNull(projectMessagesDTO.getLoginSSOWelcomeMessage());
        assertNotNull(projectMessagesDTO.getSsoAccessErrorMessage());
        assertNotNull(projectMessagesDTO.getAdminOverviewHelpText());
        assertNotNull(projectMessagesDTO.getPartnerSSOLinkText());
        assertNotNull(projectMessagesDTO.getLoginPasswordPlaceholder());
        assertNotNull(projectMessagesDTO.getLoginUsernamePlaceholder());    
    }

    @Test
    public void test_getProjectProfileMessages_valid_languageCode() {
        ProjectMessagesDTO projectMessagesDTO = projectProfileService.getProjectMessages(LocaleCodeEnum.fr_CA.name());

        assertNotNull(projectMessagesDTO);
        assertNotNull(projectMessagesDTO.getLoginWelcomeMessage());
        assertNotNull(projectMessagesDTO.getLogoutSSOMessage());
        assertNotNull(projectMessagesDTO.getLoginSSOWelcomeMessage());
        assertNotNull(projectMessagesDTO.getSsoAccessErrorMessage());
        assertNotNull(projectMessagesDTO.getAdminOverviewHelpText());
        assertNotNull(projectMessagesDTO.getPartnerSSOLinkText());
        assertNotNull(projectMessagesDTO.getLoginPasswordPlaceholder());
        assertNotNull(projectMessagesDTO.getLoginUsernamePlaceholder());

        assertEquals(projectMessagesDTO.getLoginWelcomeMessage(), "Translated Welcome!");
        assertEquals(projectMessagesDTO.getLogoutSSOMessage(), "Translated Logged Out!");
        assertEquals(projectMessagesDTO.getLoginSSOWelcomeMessage(), "Translated SSO Welcome!");
        assertEquals(projectMessagesDTO.getSsoAccessErrorMessage(), "Translated ERROR");
        assertEquals(projectMessagesDTO.getAdminOverviewHelpText(), "Translated Help Text");
        assertEquals(projectMessagesDTO.getLoginPasswordPlaceholder(), "Translated Password");
        assertEquals(projectMessagesDTO.getLoginUsernamePlaceholder(), "Translated Username");
        assertEquals(projectMessagesDTO.getPartnerSSOLinkText(), "Translated link text");
    }

    @Test
    public void test_getProjectProfileMessages_invalid_languageCode() {

        try {
            projectProfileService.getProjectMessages(TEST_INVALID_LANGUAGE_CODE);
            fail("Invalid languageCode messages should have thrown an error!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ProjectProfileConstants.ERROR_INVALID_SITE_LANGUAGE, errorMsgException);
        }
    }
    
    private ProjectProfilePublicDTO createProjectProfileDTO(){
        ProjectProfilePublicDTO projectProfile = new ProjectProfilePublicDTO();
        
        ColorsProfileDTO colorsDto = new ColorsProfileDTO();
        colorsDto.setPrimaryColor(TEST_PRIMARY_COLOR);
        colorsDto.setSecondaryColor(TEST_SECONDARY_COLOR);
        colorsDto.setTextOnPrimaryColor(TEST_PRIMARY_TEXT_COLOR);
        
        EmailPreferencesDTO emailPreferencesDto = new EmailPreferencesDTO();
        emailPreferencesDto.setWhenRecognized(true);
        emailPreferencesDto.setDirectRptRecognized(true);
        emailPreferencesDto.setApprovalNeeded(true);
        emailPreferencesDto.setWeeklyDigest(true);
        emailPreferencesDto.setNotifyOthers(true);
        emailPreferencesDto.setMilestoneReminder(true);

        TermsAndConditionsDTO termsAndConditionsDto = new TermsAndConditionsDTO();
        termsAndConditionsDto.setEditDate(TEST_TC_EDIT_DATE_CURRENT);
        termsAndConditionsDto.setEnforceTCAtLogin(Boolean.TRUE);
        termsAndConditionsDto.setMajorChangeDate(TEST_TC_MAJOR_CHANGE_DATE_CURRENT);
        termsAndConditionsDto.setShowInFooter(Boolean.TRUE);
        
        projectProfile.setClientName(TEST_CLIENT_NAME);
        projectProfile.setReportAbuseAdmin(TEST_REPORT_ABUSE_ADMIN);
        projectProfile.setUnassignedApproverPaxId(TEST_UNASSIGNED_APPROVER_PAX_ID);
        projectProfile.setSupportEmail(TEST_SUPPORT_EMAIL);
        projectProfile.setSsoEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setPartnerSsoUrl(TEST_PARTNER_SSO_URL);
        projectProfile.setLikingEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setCommentingEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setEngagementScoreEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setNetworkConnectionsEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setPointBank(PointBankEnum.EY.getBank());
        projectProfile.setColors(colorsDto);
        projectProfile.setSiteLanguages(TEST_SITE_LANGUAGES);
        projectProfile.setCalendarEnabled(Boolean.valueOf(ProjectConstants.TRUE));
        projectProfile.setEmailPreferences(emailPreferencesDto);
        projectProfile.setTermsAndConditions(termsAndConditionsDto);
        projectProfile.setServiceAnniversaryEnabled(Boolean.TRUE);
        
        ServiceAnniversaryDTO serviceAnniversaryDto = new ServiceAnniversaryDTO();
        serviceAnniversaryDto.setProvider(SERVICE_ANNIVERSARY_PROVIDER);
        serviceAnniversaryDto.setBaseUrl(SERVICE_ANNIVERSARY_BASE_URL);
        
        projectProfile.setServiceAnniversary(serviceAnniversaryDto);

        List<FilterDetailsDTO> filterOptions = new ArrayList<>();
        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setEnabled(Boolean.FALSE);
        filterOption.setName(ActivityFeedFilterEnum.CITY.name());
        filterOptions.add(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDTO = new ActivityFeedFilterDTO();
        activityFeedFilterDTO.setFilterOptions(filterOptions);
        activityFeedFilterDTO.setSearchEnabled(Boolean.TRUE);
        activityFeedFilterDTO.setDefaultFilter(ActivityFeedFilterEnum.AREA.name());
        projectProfile.setActivityFeedFilter(activityFeedFilterDTO);
        
        SeoDTO seo = new SeoDTO();
        seo.setClientTitle(TEST_SEO_CLIENT_TITLE);
        seo.setClientDesc(TEST_SEO_CLIENT_DESC);
        projectProfile.setSeo(seo);
        
        projectProfile.setFromEmailAddress(TEST_FROM_EMAIL_ADDRESS);
        
        return projectProfile;
    }

}