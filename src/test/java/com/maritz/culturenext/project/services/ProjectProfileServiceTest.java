package com.maritz.culturenext.project.services;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.ActivityFeedFilterDTO;
import com.maritz.culturenext.dto.FilterDetailsDTO;
import com.maritz.culturenext.enums.ActivityFeedFilterEnum;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;
import com.maritz.culturenext.profile.services.EnrollmentGroupsService;
import com.maritz.culturenext.project.constants.ProjectProfileConstants;
import com.maritz.culturenext.project.dto.Element;
import com.maritz.culturenext.project.dto.ProjectProfilePublicDTO;
import com.maritz.culturenext.project.services.ProjectProfileService;
import com.maritz.test.AbstractDatabaseTest;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class ProjectProfileServiceTest extends AbstractDatabaseTest {
    @Inject private ConcentrixDao<Batch> batchDao;
    @Inject private EnrollmentGroupsService enrollmentGroupsService;
    @Inject private ProjectProfileService projectProfileService;

    @Test
    public void test_updateProjectPublicInfo_patch_activityFeedFilter_inactiveGroup() {
        // Attempt to enable an inactive group filter
        List<FilterDetailsDTO> filterOptions = new ArrayList<>();
        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.COUNTRY_CODE.name());
        filterOption.setEnabled(true);
        filterOptions.add(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDTO = new ActivityFeedFilterDTO();
        activityFeedFilterDTO.setFilterOptions(filterOptions);
        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setActivityFeedFilter(activityFeedFilterDTO);

        // This should fail since the group isn't ACTIVE
        boolean success = false;
        try {
            projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.name(),
                                                          projectProfileDto);
        } catch (ErrorMessageException ex) {
            for (ErrorMessage errorMessage : ex.getErrorMessages()) {
                if (ProjectProfileConstants.ERROR_INACTIVE_FILTER_OPTIONS.equals(errorMessage.getCode())) {
                    success = true;
                }
            }
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_updateProjectPublicInfo_patch_activityFeedFilter_newlyActiveGroup() {
        // Prepare to set the group to ACTIVE
        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();
        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_11);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.COUNTRY_CODE.name());
        enrollmentGroupDTO.setFieldDisplayName("Country Code");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.ACTIVE.name());
        enrollmentGroupDTO.setVisibility(PermissionConstants.ADMIN_ROLE_NAME);
        List<EnrollmentGroupDTO> groupList = new ArrayList<>();
        groupList.add(enrollmentGroupDTO);

        // Prepare to set the newly created filter to be enabled
        List<FilterDetailsDTO> filterOptions = new ArrayList<>();
        FilterDetailsDTO filterOption = new FilterDetailsDTO();
        filterOption.setName(ActivityFeedFilterEnum.COUNTRY_CODE.name());
        filterOption.setEnabled(true);
        filterOptions.add(filterOption);
        ActivityFeedFilterDTO activityFeedFilterDTO = new ActivityFeedFilterDTO();
        activityFeedFilterDTO.setFilterOptions(filterOptions);
        ProjectProfilePublicDTO projectProfileDto = new ProjectProfilePublicDTO();
        projectProfileDto.setActivityFeedFilter(activityFeedFilterDTO);

        // First need to set the group to ACTIVE
        enrollmentGroupsService.updateEnrollmentGroups(groupList);

        // Now update the filter to be enabled
        ProjectProfilePublicDTO result = projectProfileService.updateProjectPublicInfo(RequestMethod.PATCH.name(),
                                                                                       projectProfileDto);

        Assert.assertNotNull(result);
        activityFeedFilterDTO = result.getActivityFeedFilter();
        Assert.assertNotNull(activityFeedFilterDTO);
        filterOptions = activityFeedFilterDTO.getFilterOptions();
        Assert.assertNotNull(filterOptions);

        // Make sure the filter option is available and set to true
        boolean filterFound = false;
        for (FilterDetailsDTO filterOptionResult : filterOptions) {
            if (ActivityFeedFilterEnum.COUNTRY_CODE.name().equals(filterOptionResult.getName())) {
                filterFound = true;
                Assert.assertTrue(filterOptionResult.getEnabled());
            }
        }
        Assert.assertTrue(filterFound);
    }

    @After
    public void tearDown() throws Exception {
        // Manually set the BATCH that was created to COMPLETE
        Batch batchFromTest = batchDao.findBy()
            .where(ProjectConstants.BATCH_TYPE_CODE).eq(BatchType.GROUP_PROCESSING.getCode())
            .desc(ProjectConstants.CREATE_DATE)
            .findOne();

        // Only do this if the batch was created
        if (batchFromTest != null && StatusTypeCode.QUEUED.name().equals(batchFromTest.getStatusTypeCode())) {
            batchFromTest.setStatusTypeCode(StatusTypeCode.COMPLETE.name());
            batchDao.save(batchFromTest);

            // Set the group back to INACTIVE
            EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();
            enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_11);
            enrollmentGroupDTO.setField(ActivityFeedFilterEnum.COUNTRY_CODE.name());
            enrollmentGroupDTO.setFieldDisplayName("Country Code");
            enrollmentGroupDTO.setGroupCreation(StatusTypeCode.INACTIVE.name());
            enrollmentGroupDTO.setVisibility(PermissionConstants.ADMIN_ROLE_NAME);
            List<EnrollmentGroupDTO> groupList = new ArrayList<>();
            groupList.add(enrollmentGroupDTO);
            enrollmentGroupsService.updateEnrollmentGroups(groupList);
        }
    }

    @Test
    public void testProjectProfileViewElementAvailable (){
        ProjectProfilePublicDTO projectProfilePublicDTO = new ProjectProfilePublicDTO();
        Assert.assertNotNull(projectProfileService);
        ProjectProfilePublicDTO result = projectProfileService.getViewElements(projectProfilePublicDTO);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.getView().size() == 19);
        for (Element element: projectProfilePublicDTO.getView()) {
            Assert.assertFalse(element.getKey().contains(ApplicationDataConstants.PROJECT_PROFILE_VIEW + ProjectConstants.DOT_DELIM));
        }
    }
}