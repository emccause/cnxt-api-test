package com.maritz.culturenext.sitecontent.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.SiteContentTypeEnum;
import com.maritz.culturenext.sitecontent.constants.SiteContentConstants;
import com.maritz.culturenext.sitecontent.dto.SiteContentDTO;
import com.maritz.test.AbstractRestTest;

public class SiteContentRestServiceTest extends AbstractRestTest {

    private static final String TEST_SITE_CONTENT="/rest/site-content";
    private static final String TEST_TERMS_AND_CONDITIONS="/rest/site-content/TERMS_AND_CONDITIONS";
    private static final String TEST_TERMS_AND_CONDITIONS_FR_CA="/rest/site-content/TERMS_AND_CONDITIONS?languageCode=fr_CA";
    private static final String TEST_TERMS_AND_CONDITIONS_ZH_TW="/rest/site-content/TERMS_AND_CONDITIONS?languageCode=zh_TW";
    private static final String TEST_TERMS_AND_CONDITIONS_JA_JP="/rest/site-content/TERMS_AND_CONDITIONS?languageCode=ja_JP";
    private static final String TEST_TERMS_AND_CONDITIONS_LANGUAGE_CODE_ERROR="/rest/site-content/TERMS_AND_CONDITIONS?languageCode=returnDefault";

    private static final String TEST_DEFAULT_CONTENT = "Insert your Program's Terms & Conditions here.";
    private static final String TEST_CONTENT_FR_CA = "Insérez les conditions générales de votre programme ici.";
    private static final String TEST_CONTENT_ZH_TW = "在此處插入計劃的條款與細則。";
    private static final String TEST_CONTENT_JA_JP = "こちらにプログラムの利用規約を挿入します。";
    private static final String TEST_COMMENT_PUT = "This is the content that was submitted in the content editor using PUT method.";
    private static final String TEST_COMMENT_PATCH = "This is the content that was submitted in the content editor using PATCH method.";


    @Test
    public void get_site_content_terms_and_conditions() throws Exception {
        mockMvc.perform(get(TEST_TERMS_AND_CONDITIONS)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_DEFAULT_CONTENT)));
    }

    @Test
    public void get_site_content_terms_and_conditions_fr_ca() throws Exception {
        mockMvc.perform(get(TEST_TERMS_AND_CONDITIONS_FR_CA)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_CONTENT_FR_CA)));
    }

    @Test
    public void get_site_content_terms_and_conditions_zh_tw() throws Exception {
        mockMvc.perform(get(TEST_TERMS_AND_CONDITIONS_ZH_TW)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_CONTENT_ZH_TW)));
    }

    @Test
    public void get_site_content_terms_and_conditions_ja_jp() throws Exception {
        mockMvc.perform(get(TEST_TERMS_AND_CONDITIONS_JA_JP)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_CONTENT_JA_JP)));
    }
    @Test
    public void get_site_content_terms_and_conditions_with_error_and_return_default() throws Exception {
        mockMvc.perform(get(TEST_TERMS_AND_CONDITIONS_LANGUAGE_CODE_ERROR)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_DEFAULT_CONTENT)));
    }

    @Test
    public void patch_site_content_terms_and_conditions() throws Exception {

        SiteContentDTO siteContentDto = new SiteContentDTO();
        siteContentDto.setContent(TEST_COMMENT_PATCH);
        siteContentDto.setStatus(StatusTypeCode.ACTIVE.name());
        siteContentDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());

        mockMvc.perform(patch(TEST_SITE_CONTENT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(siteContentDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_COMMENT_PATCH)));
    }

    @Test
    public void patch_site_content_terms_and_conditions_missing_content() throws Exception {

        SiteContentDTO siteContentDto = new SiteContentDTO();
        siteContentDto.setStatus(StatusTypeCode.ACTIVE.name());
        siteContentDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());

        mockMvc.perform(patch(TEST_SITE_CONTENT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(siteContentDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_CONTENT)));
    }

    @Test
    public void patch_site_content_terms_and_conditions_missing_status() throws Exception {

        SiteContentDTO siteContentDto = new SiteContentDTO();
        siteContentDto.setContent(TEST_COMMENT_PATCH);
        siteContentDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());

        mockMvc.perform(patch(TEST_SITE_CONTENT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(siteContentDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_STATUS)));
    }

    @Test
    public void patch_site_content_terms_and_conditions_missing_type() throws Exception {

        SiteContentDTO siteContentDto = new SiteContentDTO();
        siteContentDto.setContent(TEST_COMMENT_PATCH);
        siteContentDto.setStatus(StatusTypeCode.ACTIVE.name());

        mockMvc.perform(patch(TEST_SITE_CONTENT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(siteContentDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$[0].code", is(SiteContentConstants.ERROR_SITE_CONTENT_INVALID_TYPE)));
    }

    @Test
    public void put_site_content_terms_and_conditions() throws Exception {

        SiteContentDTO siteContentDto = new SiteContentDTO();
        siteContentDto.setContent(TEST_COMMENT_PUT);
        siteContentDto.setStatus(StatusTypeCode.ACTIVE.name());
        siteContentDto.setType(SiteContentTypeEnum.TERMS_AND_CONDITIONS.name());

        mockMvc.perform(put(TEST_SITE_CONTENT)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE,MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectUtils.objectToJson(siteContentDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", is(TEST_COMMENT_PUT)));
    }

}
