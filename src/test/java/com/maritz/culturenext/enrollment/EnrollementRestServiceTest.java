package com.maritz.culturenext.enrollment;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EnrollementRestServiceTest extends AbstractRestTest {

    private static final String URL_GET_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS = "/rest/enrollments/errors/current";

    @Test
    public void test_getCurrentParticipantEnrollmentErrors() throws Exception {
        mockMvc.perform(
                get(URL_GET_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS
                        + "?page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER
                        + "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE
                )
                        .with(authToken(TestConstants.USER_ADMIN))
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void test_getCurrentParticipantEnrollmentErrorsWithUserRights() throws Exception {
        mockMvc.perform(
                get(URL_GET_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS
                        + "?page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER
                        + "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE
                )
                        .with(authToken(TestConstants.USER_BELLWE))
        )
                .andExpect(status().isForbidden())
        ;
    }

    @Test
    public void test_getCurrentParticipantEnrollmentErrorsWithWrongPaginationParameters() throws Exception {
        mockMvc.perform(
                get(URL_GET_CURRENT_PARTICIPANT_ENROLLMENT_ERRORS
                        + "?page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER
                        + "&page[size]= 0"
                )
                        .with(authToken(TestConstants.USER_ADMIN))
        )
                .andExpect(status().isUnprocessableEntity())
        ;
    }

}
