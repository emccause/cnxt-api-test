package com.maritz.culturenext.enrollment.dao;

import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;
import org.mockito.Mock;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class ParticipantEnrollmentErrorDaoTest extends AbstractDatabaseTest {

    private static final String CONTROL_NUMBER = "3646";
    @Mock
    private ParticipantEnrollmentErrorDao participantEnrollmentErrorDao;

    @Test
    public void testGetParticipantEnrollmentErrors(){
        assertNotNull(participantEnrollmentErrorDao);
        List<ParticipantEnrollmentErrorDTO> data = new ArrayList<>();
        ParticipantEnrollmentErrorDTO dto = new ParticipantEnrollmentErrorDTO();
        dto.setControlNumber(CONTROL_NUMBER);
        dto.setError("Error Message");
        dto.setErrorDatetime(ZonedDateTime.now());

        data.add(dto);
        when(participantEnrollmentErrorDao.getParticipantEnrollmentErrors(anyInt(), anyInt())).thenReturn(data);
        List<ParticipantEnrollmentErrorDTO> result = participantEnrollmentErrorDao.getParticipantEnrollmentErrors(1, 20);
        assertNotNull(result);

        assertTrue(result.size() ==1);

        assertEquals(CONTROL_NUMBER, result.get(0).getControlNumber());
    }

}
