package com.maritz.culturenext.enrollment.dao;

import com.maritz.culturenext.enrollment.dto.ParticipantEnrollmentErrorDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.*;

public class ParticipantEnrollmentErrorExceptionDaoTest extends AbstractDatabaseTest {

    private static final String CONTROL_NUMBER = "3646";
    @Inject
    private ParticipantEnrollmentErrorDao participantEnrollmentErrorDao;

    @Test(expected= IndexOutOfBoundsException.class)
    public void testGetParticipantEnrollmentErrorsWithNullPositioning(){
        assertNotNull(participantEnrollmentErrorDao);
        List<ParticipantEnrollmentErrorDTO> result = participantEnrollmentErrorDao.getParticipantEnrollmentErrors(0, 0);
        assertNotNull(result);

        assertTrue(result.size() ==1);

        assertEquals(CONTROL_NUMBER, result.get(0).getControlNumber());
    }
    @Test
    public void testGetParticipantEnrollmentFromDatabase(){
        assertNotNull(participantEnrollmentErrorDao);
        List<ParticipantEnrollmentErrorDTO> result = participantEnrollmentErrorDao.getParticipantEnrollmentErrors(1, 10);
        assertNotNull(result);
    }
}
