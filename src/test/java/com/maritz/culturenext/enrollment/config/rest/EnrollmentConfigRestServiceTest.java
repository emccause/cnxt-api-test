package com.maritz.culturenext.enrollment.config.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class EnrollmentConfigRestServiceTest extends AbstractRestTest{
    
    private String prependedPath = "/rest/enrollment-config/";
    
    @Test
    public void test_get_enrollment_config_list() throws Exception {
        mockMvc.perform(
                get(prependedPath + "/fields/"
                        )
                        .with(authToken(TestConstants.USER_WRIGHTMA))
        )
                .andExpect(status().isOk())
        ;
    }

}
