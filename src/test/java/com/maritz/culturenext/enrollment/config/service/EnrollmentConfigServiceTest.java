package com.maritz.culturenext.enrollment.config.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.culturenext.enrollment.config.services.impl.EnrollmentConfigServiceImpl;
import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;
import com.maritz.culturenext.jpa.entity.EnrollmentField;
import com.maritz.test.AbstractMockTest;


public class EnrollmentConfigServiceTest extends AbstractMockTest {

    @InjectMocks private EnrollmentConfigServiceImpl enrollmentConfigService;
    @Mock private ConcentrixDao<EnrollmentField> enrollmentFieldDao;

    private FindBy<EnrollmentField> mockedFindBy = mock(FindBy.class);

    @Before
    public void setup() {
        List<EnrollmentField> enrollmentFields = new ArrayList<>();
        EnrollmentField enrollmentField = new EnrollmentField();
        enrollmentField.setFieldName("AREA");
        enrollmentField.setFieldDisplayName("Area");

        enrollmentFields.add(enrollmentField);

        PowerMockito.when(enrollmentFieldDao.findBy()).thenReturn(mockedFindBy);
        PowerMockito.when(mockedFindBy.find()).thenReturn(enrollmentFields);
    }

    @Test
    public void test_get_enrollment_field_happy_path() {


        List<EnrollmentFieldDTO> enrollmentConfigResponse = enrollmentConfigService.getEnrollmentFields();

        assertTrue( !enrollmentConfigResponse.isEmpty() );


    }


}
