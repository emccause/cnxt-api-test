package com.maritz.culturenext.enrollment.batch;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.batch.core.Job;

import com.maritz.batch.launcher.BatchJobLauncher;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.BatchTypeCode;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.services.BatchService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.culturenext.enrollment.services.CardEnableDisableService;
import com.maritz.test.AbstractDatabaseTest;


public class WorkdayEnrollmentJobLauncherTest extends AbstractDatabaseTest {

    @Inject BatchJobLauncher batchJobLauncher;
    @Inject Job workdayEnrollmentJob;
    @Inject BatchService batchService;
    @Inject ApplicationDataService applicationDataService;
    @Inject CardEnableDisableService cardEnableDisableService;
    @Inject TriggeredTaskService triggeredTaskService;

    @Inject PaxRepository paxRepository;

    Batch batch;
    WorkdayEnrollmentJobLauncher workdayEnrollmentJobLauncher;

    @Before
    public void setUp() throws Exception {
        //only load 1 employee
        applicationDataService.setApplicationData("workday.filterByEmployeeId", "100403");

        workdayEnrollmentJobLauncher = new WorkdayEnrollmentJobLauncher()
            .setBatchJobLauncher(batchJobLauncher)
            .setWorkdayEnrollmentJob(workdayEnrollmentJob)
            .setCardEnableDisableService(cardEnableDisableService)
            .setTriggeredTaskService(triggeredTaskService)
        ;
        batch = batchService.startBatch(BatchTypeCode.TRIGGERED_TASK.name());
    }

    @Test
    public void workdayEnrollmentJobLauncher() throws Exception {
        workdayEnrollmentJobLauncher.workdayEnrollmentJob(batch);

        Pax pax = paxRepository.findByControlNum("100403");
        assertThat(pax, notNullValue());
        assertThat(pax.getFirstName(), is("Betty"));
        assertThat(pax.getLastName(), is("Mc Cormick"));
    }

}
