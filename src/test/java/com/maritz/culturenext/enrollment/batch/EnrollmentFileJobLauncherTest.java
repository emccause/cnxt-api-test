package com.maritz.culturenext.enrollment.batch;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.rest.NotFoundException;
import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.springframework.batch.core.Job;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.batch.launcher.BatchJobLauncher;
import com.maritz.core.dto.FileDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.BatchTypeCode;
import com.maritz.core.services.BatchService;
import com.maritz.core.services.FileService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.core.util.ResourceUtils;
import com.maritz.culturenext.batch.dto.EnrollmentBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.EnrollmentBatchService;
import com.maritz.culturenext.enrollment.services.CardEnableDisableService;
import com.maritz.culturenext.enrollment.services.EnrollmentFileService;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.jpa.entity.StageEnrollment;
import com.maritz.test.AbstractDatabaseTest;

@Ignore
public class EnrollmentFileJobLauncherTest extends AbstractDatabaseTest {

    @Inject BatchJobLauncher batchJobLauncher;
    @Inject BatchService batchService;
    @Inject CardEnableDisableService cardEnableDisableService;
    @Inject EnrollmentFileService enrollmentFileService;
    @Inject FileService inboxFileService;
    @Inject Job enrollmentFileJob;
    @Inject PaxRepository paxRepository;
    @Inject ConcentrixDao<StageEnrollment> stageEnrollmentDao;
    @Inject TriggeredTaskService triggeredTaskService;
    @Inject AddressRepository addressRepository;

    Batch batch;
    EnrollmentFileJobLauncher enrollmentFileJobLauncher;

    boolean filesAdded = false;

    @Before
    public void setUp() throws Exception {
        enrollmentFileJobLauncher = new EnrollmentFileJobLauncher()
            .setBatchJobLauncher(batchJobLauncher)
            .setEnrollmentFileJob(enrollmentFileJob)
            .setCardEnableDisableService(cardEnableDisableService)
            .setFileService(inboxFileService)
            .setTriggeredTaskService(triggeredTaskService);

        batch = batchService.startBatch(BatchTypeCode.ENRL.name());

        try {
            MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentEmptyData.csv", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentEmptyData.csv"));
            if(!enrollmentFolderExists()) inboxFileService.add(EnrollmentConstants.ENROLLMENT_SUBFOLDER,file);
        }catch (Exception e){}
    }

    @After
    public void cleanUp() throws Exception {
        cleanUpFiles();
    }

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    public void moveFile(String fileName, String filePath) {
        try {
            FileDTO file = inboxFileService.getFile(fileName);
            inboxFileService.update(file.getFullName(), file.setPath(filePath));
        }
        catch (Exception e) {
            // file not found, whatever
        }
    }

    @Test
    public void enrollmentFileJobLauncher() throws Exception {
        cleanUpFiles();
        addTestFiles(false);
        moveFile("stage/enrollmentLoadForeign.csv", "");
        moveFile("stage/enrollmentLoadSmall2.csv", EnrollmentConstants.ENROLLMENT_SUBFOLDER);
        enrollmentFileJobLauncher.enrollmentFileJob(batch);

        //make sure the stage enrollment table is loaded
        List<StageEnrollment> stageEnrollments = stageEnrollmentDao.findBy().find();
        assertThat(stageEnrollments.size(), is(8));

        Pax pax1 = paxRepository.findByControlNum("157290");
        assertThat(pax1, notNullValue());
        assertThat(pax1.getFirstName(), is("Adam"));
        assertThat(pax1.getLastName(), is("Rider"));

        Pax pax2 = paxRepository.findByControlNum("168629");
        assertThat(pax2, notNullValue());
        assertThat(pax2.getFirstName(), is("Joe"));
        assertThat(pax2.getLastName(), is("Epplin"));

        Pax pax3 = paxRepository.findByControlNum("5046");
        assertThat(pax3, notNullValue());
        assertThat(pax3.getFirstName(), is("Edilberto"));
        assertThat(pax3.getLastName(), is("Garza"));

        Pax pax4 = paxRepository.findByControlNum("5881");
        assertThat(pax4, notNullValue());
        assertThat(pax4.getFirstName(), is("Gerardo Arturo"));
        assertThat(pax4.getLastName(), is("Ramirez Lerma"));
    }

    @Ignore // Due to seemingly random failures in Jenkins
    @Test
    public void enrollmentFileJobLauncher_foreign() throws Exception {
        cleanUpFiles();
        addTestFiles(false);
        moveFile("stage/enrollmentLoadForeign.csv", "");
        moveFile("stage/enrollmentLoadSmall2.csv", EnrollmentConstants.ENROLLMENT_SUBFOLDER);
        enrollmentFileJobLauncher.enrollmentFileJob(batch);

        //make sure the stage enrollment table is loaded
        List<StageEnrollment> stageEnrollments = stageEnrollmentDao.findBy().find();
        assertThat(stageEnrollments.size(), is(8));

        Pax pax1 = paxRepository.findByControlNum("daniel1");
        assertThat(pax1, notNullValue());
        assertThat(pax1.getFirstName(), is("Daniel"));
        assertThat(pax1.getLastName(), is("Chavez"));

        Address address1 = addressRepository.findPreferredByPaxId(pax1.getPaxId());
        assertThat(address1, notNullValue());
        assertThat(address1.getState(), is(address1.getCountryCode()));
        assertThat(address1.getZip(), is("00000"));

        Pax pax2 = paxRepository.findByControlNum("laura1");
        assertThat(pax2, notNullValue());
        assertThat(pax2.getFirstName(), is("Laura"));
        assertThat(pax2.getLastName(), is("Farfan"));

        Address address2 = addressRepository.findPreferredByPaxId(pax2.getPaxId());
        assertThat(address2, notNullValue());
        assertThat(address2.getState(), is("AG"));
        assertThat(address2.getZip(), is("00000"));

        Pax pax3 = paxRepository.findByControlNum("juan1");
        assertThat(pax3, notNullValue());
        assertThat(pax3.getFirstName(), is("Juan"));
        assertThat(pax3.getLastName(), is("Sedano"));

        Address address3 = addressRepository.findPreferredByPaxId(pax3.getPaxId());
        assertThat(address3, notNullValue());
        assertThat(address3.getState(), is(address3.getCountryCode()));
        assertThat(address3.getZip(), is("92100"));

        Pax pax4 = paxRepository.findByControlNum("samantha1");
        assertThat(pax4, notNullValue());
        assertThat(pax4.getFirstName(), is("Samantha"));
        assertThat(pax4.getLastName(), is("Polina"));

        Address address4 = addressRepository.findPreferredByPaxId(pax4.getPaxId());
        assertThat(address4, notNullValue());
        assertThat(address4.getState(), is("TK"));
        assertThat(address4.getZip(), is("11000"));
    }

    @Test
    public void testUploadBreakingFileProcess() throws Exception {
        batch = batchService.startBatch(BatchTypeCode.ENRL.name());

        // An empty List should return an ErrorMessageException  and "At least one CSV file is required for this request" message.
        exceptionRule.expect(ErrorMessageException.class);
        exceptionRule.expectMessage("At least one CSV file is required for this request");

        List<MultipartFile> fileList = new ArrayList<>();
        enrollmentFileService.uploadEnrollmentFile(fileList);
    }

    @Test
    public void testUploadUpdateEnrollmentProcess() throws Exception {
        batch = batchService.startBatch(BatchTypeCode.ENRL.name());
        cleanUpFiles();
        addTestFiles(false);
        MultipartFile file = new MockMultipartFile(
                "enrollmentLoad_testers.csv", "enrollmentLoad_testers.csv", "text/csv",
                inboxFileService.getInputStream(inboxFileService.getFile("stage/enrollmentLoad_testers.csv"))
        );

        List<MultipartFile> fileList = new ArrayList<>();
        fileList.add(file);
        enrollmentFileService.validateFileToUpload(file);
        enrollmentFileService.uploadEnrollmentFile(fileList); //create the file
        enrollmentFileService.uploadEnrollmentFile(fileList); //update the file
        enrollmentFileJobLauncher.enrollmentFileJob(batch);
        List<StageEnrollment> stageEnrollments = stageEnrollmentDao.findBy().find();
        assertThat(stageEnrollments.size(), is(27));
        assertThat(enrollmentFileService.filesOnServer(EnrollmentConstants.ENROLLMENT_SUBFOLDER), is(false));
    }

    @Ignore // Due to seemingly random failures in Jenkins
    @Test
    public void enrollmentFileJobLauncherEmptyRootFolder() throws Exception {
        cleanUpFiles();
        addTestFiles(false);
        moveFile("stage/enrollmentLoadForeign.csv", EnrollmentConstants.ENROLLMENT_SUBFOLDER);
        moveFile("stage/enrollmentLoadSmall2.csv", EnrollmentConstants.ENROLLMENT_SUBFOLDER);
        moveFile("stage/enrollmentLoad_testers.csv", EnrollmentConstants.ENROLLMENT_SUBFOLDER);
        enrollmentFileJobLauncher.enrollmentFileJob(batch);

        //make sure the stage enrollment table is loaded
        List<StageEnrollment> stageEnrollments = stageEnrollmentDao.findBy().find();
        assertThat(stageEnrollments.size(), is(35));
    }

    @Test
    public void enrollmentFileJobLauncherEmptyEnrollmentSubfolder() throws Exception {
        cleanUpFiles();
        addTestFiles(false);
        moveFile("stage/enrollmentLoadForeign.csv", "");
        moveFile("stage/enrollmentLoadSmall2.csv", "");
        moveFile("stage/enrollmentLoad_testers.csv", "");
        enrollmentFileJobLauncher.enrollmentFileJob(batch);

        //make sure the stage enrollment table is loaded
        List<StageEnrollment> stageEnrollments = stageEnrollmentDao.findBy().find();
        assertThat(stageEnrollments.size(), is(35));
    }

    @Test
    public void enrollmentFileValidateExtension() throws Exception {
        batch = batchService.startBatch(BatchTypeCode.ENRL.name());
        cleanUpFiles();
        addTestFiles(false);
        MultipartFile file = new MockMultipartFile(
                "enrollmentFileWithoutCsvExtension", "enrollmentFileWithoutCsvExtension", "text/csv",
                inboxFileService.getInputStream(inboxFileService.getFile("stage/enrollmentFileWithoutCsvExtension"))
        );

        // An empty List should return an ErrorMessageException  and "At least one CSV file is required for this request" message.
        exceptionRule.expect(ErrorMessageException.class);
        exceptionRule.expectMessage(EnrollmentConstants.INVALID_FILE_FORMAT_MSG);

        List<MultipartFile> fileList = new ArrayList<>();
        fileList.add(file);
        enrollmentFileService.validateFileToUpload(file);
    }

    protected void addTestFiles(boolean force) throws IOException {
        //add the files if they haven't been added yet
        if (force || !filesAdded) {
            if (!inboxFileService.exists("stage/enrollmentLoad_testers.csv")) {
                MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentLoad_testers.csv", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentLoad_testers.csv"));
                inboxFileService.add("/", file);
            }

            if (!inboxFileService.exists("stage/enrollmentLoadForeign.csv")) {
                MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentLoadForeign.csv", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentLoadForeign.csv"));
                inboxFileService.add("/", file);
            }

            if (!inboxFileService.exists("stage/enrollmentLoadSmall.csv")) {
                MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentLoadSmall.csv", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentLoadSmall.csv"));
                inboxFileService.add("/", file);
            }
            if (!inboxFileService.exists("stage/enrollmentLoadSmall2.csv")) {
                MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentLoadSmall2.csv", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentLoadSmall2.csv"));
                inboxFileService.add("/", file);
            }
            if (!inboxFileService.exists("stage/enrollmentFileWithoutCsvExtension")) {
                MockMultipartFile file = new MockMultipartFile("file", "stage/enrollmentFileWithoutCsvExtension", MediaType.TEXT_PLAIN_VALUE, ResourceUtils.loadResourceAsBytes("files/stage/enrollmentFileWithoutCsvExtension"));
                inboxFileService.add("/", file);
            }

            filesAdded = true;
        }
    }

    private void cleanUpFiles() {
        try {
            inboxFileService.delete(inboxFileService.getFile("enrollmentLoadSmall.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollmentLoadSmall2.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollmentLoadForeign.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollmentLoad_testers.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollment/enrollmentLoadSmall.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollment/enrollmentLoadSmall2.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollment/enrollmentLoadForeign.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }

        try {
            inboxFileService.delete(inboxFileService.getFile("enrollment/enrollmentLoad_testers.csv"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }
        try {
            inboxFileService.delete(inboxFileService.getFile("enrollment/enrollmentFileWithoutCsvExtension"));
        } catch (NotFoundException e){
            //file not found, nothing to do.
        }
    }

    private boolean enrollmentFolderExists(){
        return inboxFileService.getFiles().stream().filter(f -> f.getIsDirectory()).findFirst().get().getName().compareTo(EnrollmentConstants.ENROLLMENT_SUBFOLDER_NAME)==0;
    }
}
