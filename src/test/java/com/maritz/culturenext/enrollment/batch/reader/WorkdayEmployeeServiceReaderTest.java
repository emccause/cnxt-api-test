package com.maritz.culturenext.enrollment.batch.reader;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.env.Environment;

import com.maritz.test.AbstractMockTest;
import com.maritz.workday.dto.EmployeeDTO;
import com.maritz.workday.services.WorkdayEmployeeService;


public class WorkdayEmployeeServiceReaderTest extends AbstractMockTest {

    @Mock Environment environment;
    @Mock WorkdayEmployeeService workdayEmployeeService;

    WorkdayEmployeeServiceReader workdayEmployeeServiceReader;

    @Before
    public void setUp() throws Exception {
        when(workdayEmployeeService.getEmployees()).thenReturn(Arrays.asList(
            new EmployeeDTO().setParticipantId("id1")
            ,new EmployeeDTO().setParticipantId("id2")
            ,new EmployeeDTO().setParticipantId("id3")
        ));

        when(workdayEmployeeService.getEmployee("id1")).thenReturn(
            new EmployeeDTO().setParticipantId("id1")
        );

        workdayEmployeeServiceReader = new WorkdayEmployeeServiceReader()
            .setEnvironment(environment)
            .setWorkdayEmployeeService(workdayEmployeeService)
        ;
    }

    @Test
    public void read_all_employees() throws Exception {
        when(environment.getProperty("workday.filterByEmployeeId")).thenReturn(null);

        workdayEmployeeServiceReader.read();

        verify(workdayEmployeeService, never()).getEmployee(any(String.class));
        verify(workdayEmployeeService).getEmployees();
    }

    @Test
    public void read_one_employees() throws Exception {
        when(environment.getProperty("workday.filterByEmployeeId")).thenReturn("id1");

        workdayEmployeeServiceReader.read();

        verify(workdayEmployeeService).getEmployee("id1");
        verify(workdayEmployeeService, never()).getEmployees();
    }

}
