package com.maritz.culturenext.enrollment.batch.writer;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enrollment.batch.writer.StageEnrollmentWriter;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.culturenext.jpa.entity.StageEnrollment;

public class StageEnrollmentWriterTests {

    private StageEnrollmentWriter stageEnrollmentWriter;
    @Mock private ConcentrixDao<StageEnrollment> stageEnrollmentDao; 
    private FindBy<StageEnrollment> stageEnrollmentsFindBy = PowerMockito.mock(FindBy.class);

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.stageEnrollmentWriter = new StageEnrollmentWriter();
        this.stageEnrollmentWriter.setStageEnrollmentDao(stageEnrollmentDao);
        
        PowerMockito.when(stageEnrollmentDao.findBy()).thenReturn(stageEnrollmentsFindBy);
        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                return null;
            }
        }).when(stageEnrollmentsFindBy).insert(Matchers.anyCollectionOf(StageEnrollment.class));

    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testTermDateLength() throws Exception {

        List<StageEnrollmentDTO> items = new ArrayList<StageEnrollmentDTO>();
        StageEnrollmentDTO item = new StageEnrollmentDTO();

        String expectedDateTimeString = "2015-07-16 23:59:59";
        String expectedDateString = "07-22";

        item.setStatus(StatusTypeCode.ACTIVE.name());
        //testing for truncation to length of 10 if its got a timestamp
        item.setTerminationDate(expectedDateTimeString);
        items.add(item);
        stageEnrollmentWriter.write(items);
        assertEquals(item.getTerminationDate(),"2015-07-16");

        //testing for value unchanged if its null
        item.setTerminationDate(null);
        stageEnrollmentWriter.write(items);
        assertEquals(item.getTerminationDate(),null);

        //testing for value unchanged if its less than 10 characters in length
        item.setTerminationDate(expectedDateString);
        stageEnrollmentWriter.write(items);
        assertEquals(item.getTerminationDate(),expectedDateString);

    }

    @Test
    public void testWrite() throws Exception {
        
        List<StageEnrollmentDTO> items = new ArrayList<StageEnrollmentDTO>();
        StageEnrollmentDTO item = new StageEnrollmentDTO();
        item.setStatus(StatusTypeCode.ACTIVE.name());
        items.add(item);
        assertNotNull("stageEnrollmentWriter was null and should not!", stageEnrollmentWriter);
        assertNull("hire date is not null but should have.",item.getHireDate());
        stageEnrollmentWriter.write(items);
        assertNull("hire date is not null but should have.",item.getHireDate());
    }

}
