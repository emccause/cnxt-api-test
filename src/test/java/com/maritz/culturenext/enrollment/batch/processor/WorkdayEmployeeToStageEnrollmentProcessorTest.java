package com.maritz.culturenext.enrollment.batch.processor;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.env.Environment;

import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.test.AbstractMockTest;
import com.maritz.workday.dto.EmployeeDTO;


public class WorkdayEmployeeToStageEnrollmentProcessorTest extends AbstractMockTest {

    @Mock Environment environment;

    WorkdayEmployeeToStageEnrollmentProcessor workdayEmployeeToStageEnrollmentProcessor;

    @Before
    public void setUp() throws Exception {
        workdayEmployeeToStageEnrollmentProcessor = new WorkdayEmployeeToStageEnrollmentProcessor()
            .setEnvironment(environment)
        ;
    }

    @Test
    public void process_convert_only() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);

        EmployeeDTO employee = new EmployeeDTO().setParticipantId("id1");

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
    }

    @Test
    public void process_when_ssoEnabled_is_false_then_default_password_is_set() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);
        when(environment.getProperty("projectProfile.defaultPassword")).thenReturn("password");

        EmployeeDTO employee = new EmployeeDTO().setParticipantId("id1");

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
        assertThat(stageEnrollment.getPassword(), is("password"));
    }

    @Test
    public void process_when_ssoEnabled_is_false_and_password_is_provided_then_default_password_is_not_used() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);
        when(environment.getProperty("projectProfile.defaultPassword")).thenReturn("password");

        EmployeeDTO employee = new EmployeeDTO()
            .setParticipantId("id1")
            .setPassword("abc123")
        ;

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
        assertThat(stageEnrollment.getPassword(), is(employee.getPassword()));
    }

    @Test
    public void process_when_ssoEnabled_is_true_then_default_password_is_not_set() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(true);
        when(environment.getProperty("projectProfile.defaultPassword")).thenReturn("password");

        EmployeeDTO employee = new EmployeeDTO().setParticipantId("id1");

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
        assertThat(stageEnrollment.getPassword(), nullValue());
    }

    @Test
    public void process_when_replaceWithTop_is_set_and_id_matches_then_primary_report_id_is_top() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);
        when(environment.getProperty("enrollment.replaceWithTop")).thenReturn("1234");

        EmployeeDTO employee = new EmployeeDTO()
            .setParticipantId("id1")
            .setPrimaryReportId("1234")
        ;

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
        assertThat(stageEnrollment.getPassword(), nullValue());
        assertThat(stageEnrollment.getPrimaryReportId(), is("TOP"));
    }

    @Test
    public void process_when_replaceWithTop_is_set_and_id_does_not_match_then_primary_report_id_not_changed() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);
        when(environment.getProperty("enrollment.replaceWithTop")).thenReturn("1234");

        EmployeeDTO employee = new EmployeeDTO()
            .setParticipantId("id1")
            .setPrimaryReportId("id2")
        ;

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getParticipantId(), is(employee.getParticipantId()));
        assertThat(stageEnrollment.getPassword(), nullValue());
        assertThat(stageEnrollment.getPrimaryReportId(), is("id2"));
    }

    @Test
    public void process_phone_number_truncation_and_strip_nondigits() throws Exception {
        when(environment.getProperty("projectProfile.ssoEnabled", Boolean.class, false)).thenReturn(false);

        //testing for truncation to length of 15 if its got other chars        
        String phoneNumString = "+1 (636) 827.2769";
        String expectedTrimmedPhoneString = "16368272769";
        String phoneExtString = "(1111-2345-6789)";
        String expectedTrimmedPhoneExtString = "111123456789";

        EmployeeDTO employee = new EmployeeDTO()
            .setPhoneNumber(phoneNumString)
            .setPhoneExtension(phoneExtString)
        ;

        StageEnrollmentDTO stageEnrollment = workdayEmployeeToStageEnrollmentProcessor.process(employee);

        assertThat(stageEnrollment, notNullValue());
        assertThat(stageEnrollment.getPhoneNumber(), is(expectedTrimmedPhoneString));
        
        assertThat(stageEnrollment.getPhoneExtension(), is(expectedTrimmedPhoneExtString));
    }    
}
