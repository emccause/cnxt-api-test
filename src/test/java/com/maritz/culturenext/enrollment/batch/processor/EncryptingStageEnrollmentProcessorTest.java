package com.maritz.culturenext.enrollment.batch.processor;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.maritz.core.util.MapBuilder;
import com.maritz.culturenext.enrollment.dto.StageEnrollmentDTO;
import com.maritz.test.AbstractMockTest;
import com.maritz.test.environment.TestEnvironment;


public class EncryptingStageEnrollmentProcessorTest extends AbstractMockTest {

    @Mock PasswordEncoder passwordEncoder;
    TestEnvironment environment;

    EncryptingStageEnrollmentProcessor encryptingStageEnrollmentProcessor;

    @Before
    public void setUp() throws Exception {
        environment = new TestEnvironment(MapBuilder.<String,Object>newHashMap()
            .add("projectProfile.ssoEnabled", "false")
            .add("projectProfile.defaultPassword", "ENC(defaultPassword)")
            .build()
        );

        when(passwordEncoder.encode(any())).thenAnswer(
            new Answer<String>() {
                @Override
                public String answer(InvocationOnMock invocation) throws Throwable {
                    String str = invocation.getArgumentAt(0, String.class);
                    return "ENC(" + str + ")";
                }
            }
        );

        encryptingStageEnrollmentProcessor = new EncryptingStageEnrollmentProcessor()
            .setEnvironment(environment)
            .setPasswordEncoder(passwordEncoder)
        ;
    }

    @Test
    public void process_password_provided() throws Exception {
        StageEnrollmentDTO input = new StageEnrollmentDTO()
            .setPassword("password")
        ;

        StageEnrollmentDTO output = encryptingStageEnrollmentProcessor.process(input);

        assertThat(output.getPassword(), is("ENC(password)"));
    }

    @Test
    public void process_password_not_provided() throws Exception {
        StageEnrollmentDTO input = new StageEnrollmentDTO();

        StageEnrollmentDTO output = encryptingStageEnrollmentProcessor.process(input);

        assertThat(output.getPassword(), is("ENC(defaultPassword)"));
    }

    @Test
    public void process_password_sso_enabled() throws Exception {
        environment.setProperty("projectProfile.ssoEnabled", "true");

        StageEnrollmentDTO input = new StageEnrollmentDTO();

        StageEnrollmentDTO output = encryptingStageEnrollmentProcessor.process(input);

        assertThat(output.getPassword(), nullValue());
    }

    @Test
    public void process_full_birthdate() throws Exception {
        StageEnrollmentDTO input = new StageEnrollmentDTO()
            .setBirthDate("1/31/2000");
        ;

        StageEnrollmentDTO output = encryptingStageEnrollmentProcessor.process(input);

        assertThat(output.getFullBirthDate(), is("ENC(2000-01-31)"));
        assertThat(output.getBirthDate(), is("01-31"));
    }

    @Test
    public void process_partial_birthdate() throws Exception {
        StageEnrollmentDTO input = new StageEnrollmentDTO()
            .setBirthDate("1/31");
        ;

        StageEnrollmentDTO output = encryptingStageEnrollmentProcessor.process(input);

        assertThat(output.getFullBirthDate(), nullValue());
        assertThat(output.getBirthDate(), is("01-31"));
    }


}
