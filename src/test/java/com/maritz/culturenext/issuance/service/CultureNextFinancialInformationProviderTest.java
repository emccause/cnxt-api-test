package com.maritz.culturenext.issuance.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Discretionary;
import com.maritz.core.jpa.entity.Earnings;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.entity.TransactionHeaderMisc;
import com.maritz.core.services.impl.TransactionHeaderDetail;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.VfName;
import com.maritz.issuance.rules.TransactionEarningsResult;
import com.maritz.issuance.services.FinancialInformationProvider;
import com.maritz.test.AbstractDatabaseTest;

public class CultureNextFinancialInformationProviderTest extends AbstractDatabaseTest {

    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private ConcentrixDao<TransactionHeaderMisc> transactionHeaderMiscDao;
    
    @Inject private FinancialInformationProvider cultureNextFinancialInformationProvider;
    
    private static final String US_OVERRIDE_TRANSACTION_METRIC = "US_OVERRIDE_NOMINATION";
    private static final String NON_US_OVERRIDE_TRANSACTION_METRIC = "NON_US_OVERRIDE_NOMINATION";
    
    @Test
    public void runTest() {
                
        List<TransactionHeader> transactionHeaderList = new ArrayList<>();
        transactionHeaderList.addAll(
                transactionHeaderDao.findBy()
                .where("performanceMetric").eq(US_OVERRIDE_TRANSACTION_METRIC)
                .find()
            );
        transactionHeaderList.addAll(
                transactionHeaderDao.findBy()
                .where("performanceMetric").eq(NON_US_OVERRIDE_TRANSACTION_METRIC)
                .find()
            );
        Assert.assertEquals("Test data wasn't created!", 4, transactionHeaderList.size());
        
        List<TransactionEarningsResult> transactionEarningsResultList = new ArrayList<>();
        Map<Long, String> transactionHeaderMiscEarningTypeCodeMap = new HashMap<>();
        Map<Long, String> transactionHeaderMiscProjectNumberMap = new HashMap<>();
        Map<Long, String> transactionHeaderMiscSubProjectNumberMap = new HashMap<>();
        
        transactionHeaderList.forEach(th -> {
            TransactionEarningsResult transactionEarningsResult = new TransactionEarningsResult();
            transactionEarningsResult.setTransactions(Arrays.asList(new TransactionHeaderDetail<>(th, new Discretionary())));
            transactionEarningsResult.setEarnings(new Earnings());
            Assert.assertTrue("Should be able to process", cultureNextFinancialInformationProvider.canProcess(transactionEarningsResult));
            transactionEarningsResultList.add(transactionEarningsResult);

             TransactionHeaderMisc transactionHeaderMiscEarningTypeCode = transactionHeaderMiscDao.findBy()
                        .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                        .and(ProjectConstants.VF_NAME).eq(VfName.PAYOUT_TYPE.name())
                        .findOne();
                String thmEarningTypeCode = transactionHeaderMiscEarningTypeCode.getMiscData();
                transactionHeaderMiscEarningTypeCodeMap.put(th.getId(), thmEarningTypeCode);
                
                TransactionHeaderMisc transactionHeaderMiscProject = transactionHeaderMiscDao.findBy()
                        .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                        .and(ProjectConstants.VF_NAME).eq(VfName.BUDGET_OVERRIDE_PROJECT_NUMBER.name())
                        .findOne();
                String thmProjectNumber = transactionHeaderMiscProject.getMiscData();
                transactionHeaderMiscProjectNumberMap.put(th.getId(), thmProjectNumber);
                
                TransactionHeaderMisc transactionHeaderMiscSubProject = transactionHeaderMiscDao.findBy()
                        .where(ProjectConstants.TRANSACTION_HEADER_ID).eq(th.getId())
                        .and(ProjectConstants.VF_NAME).eq(VfName.BUDGET_OVERRIDE_SUB_PROJECT_NUMBER.name())
                        .findOne();
                String thmSubProjectNumber = transactionHeaderMiscSubProject.getMiscData();
                transactionHeaderMiscSubProjectNumberMap.put(th.getId(), thmSubProjectNumber);
        });
        
        Collection<TransactionEarningsResult> results = cultureNextFinancialInformationProvider.process(transactionEarningsResultList);
    
        results.forEach(ter -> {
            TransactionHeader transactionHeader = ter.getTransactions().stream().findFirst().get().getHeader();
            Earnings earnings = ter.getEarnings();
            
            Assert.assertEquals("Wrong earnings type code", transactionHeaderMiscEarningTypeCodeMap.get(transactionHeader.getId()), earnings.getEarningsTypeCode());
            Assert.assertEquals("Wrong Project Number", transactionHeaderMiscProjectNumberMap.get(transactionHeader.getId()), earnings.getProjectNumber());
            Assert.assertEquals("Wrong Sub Project Number", transactionHeaderMiscSubProjectNumberMap.get(transactionHeader.getId()), earnings.getSubProjectNumber());
        });
        
    }
}
