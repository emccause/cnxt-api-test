package com.maritz.culturenext.awardtype.service;

import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;

import org.junit.Test;

import com.maritz.TestUtil;
import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.CardType;
import com.maritz.core.jpa.entity.PayoutItem;
import com.maritz.core.jpa.entity.PayoutVendor;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.awardtype.constants.AwardTypeConstants;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeDetailsDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeEligibilityToReceiveDTO;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.PayoutVendorEnum;
import com.maritz.test.AbstractDatabaseTest;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;

public class AwardTypeServiceTest extends AbstractDatabaseTest {

    private static final String LONG_AWARD_TYPE_DESCRIPTION_UPDATE = "long award type description update";
    private static final String TESTING_SUBPROJECT_NUMBER = "S0123";
    private static final String TESTING_SUBCLIENT = "123";
    private static final String TESTING_PROJECT_NUMBER = "P0123";
    private static final String DPH = "DPH";
    private static final String EYD = "EYD";
    private static final String TESTING_PAYOUT_TYPE = "DPP";
    private static final String PARKING_SPOTS = "parking spots";

    @Inject private ApplicationDataService applicationDataService;
    @Inject private AwardTypeService awardTypeService;
    @Inject private ConcentrixDao<CardType> cardTypeDao;
    @Inject private ConcentrixDao<PayoutItem> payoutItemDao;
    @Inject private ConcentrixDao<PayoutVendor> payoutVendorDao;
    
    private final Long ELEVEN = new Long(11);
    private final Long TWELVE = new Long(12);
    private final Long THIRTEEN = new Long(13);
    private final Long FOURTEEN = new Long(14);
    
    private AwardTypeDTO createAwardTypeForTests() {
        AwardTypeDTO awardType = new AwardTypeDTO();
        awardType.setDisplayName("serviceanniversary");
        awardType.setLongDesc("long description");
        awardType.setAwardValue(1D);
        awardType.setAbs(true);
        AwardTypeDetailsDTO awardTypeDetails = new AwardTypeDetailsDTO();
        awardTypeDetails.setPayoutType("TST");
        awardTypeDetails.setSubClient("1234");
        awardTypeDetails.setProjectNumber(TestConstants.PROJECT_NUMBER);
        awardTypeDetails.setSubProjectNumber("Maritz");
        awardType.setDetails(awardTypeDetails);
        return awardType;
    }
    
    // *** Validation Testing ***
    @Test
    public void test_validateAwardType_null_type() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - type is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_TYPE_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_display_name() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(false);
        awardType.setDisplayName(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - displayName is null");
        } catch (ErrorMessageException errorMsgException) {
            //Note: This will also throw a missing payoutType error, so use a different method to validate the array of errors
            TestUtil.assertValidationErrors(Arrays.asList(
                    AwardTypeConstants.ERROR_MISSING_DISPLAY_NAME, AwardTypeConstants.ERROR_MISSING_PAYOUT_TYPE), errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_details_abs() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(true);
        awardType.setDetails(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - details object is null");
        } catch (ErrorMessageException errorMsgException) {
            //Note: This will also throw a missing payoutType error, so use a different method to validate the array of errors
            TestUtil.assertValidationErrors(Arrays.asList(
                    AwardTypeConstants.ERROR_MISSING_DETAILS, AwardTypeConstants.ERROR_MISSING_PAYOUT_TYPE), errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_details_abs_update() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(true);
        awardType.setDetails(null);
        try {
            awardTypeService.updateAwardType(awardType);
            fail("Should have thrown error - details object is null");
        } catch (ErrorMessageException errorMsgException) {
            //Note: This will also throw a missing payoutType error, so use a different method to validate the array of errors
            TestUtil.assertValidationErrors(Arrays.asList(
                    AwardTypeConstants.ERROR_MISSING_DETAILS, AwardTypeConstants.ERROR_MISSING_PAYOUT_TYPE), errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_details_non_abs_update() {        
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(false);
        awardType.setDisplayName("Cookies!");
        awardType.setId(TestConstants.ID_1);
        awardType.setDetails(null);

        try {
            awardTypeService.updateAwardType(awardType);
            fail("Should have thrown error - details object is null");
        } catch (ErrorMessageException errorMsgException) {
            //Note: This will also throw a missing payoutType error, so use a different method to validate the array of errors
            TestUtil.assertValidationErrors(Arrays.asList(
                    AwardTypeConstants.ERROR_MISSING_DETAILS, AwardTypeConstants.ERROR_MISSING_PAYOUT_TYPE), errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_payout_type() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setPayoutType(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - payoutType is null");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_PAYOUT_TYPE_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_duplicate_award_type() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setPayoutType("DPP");
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - can't insert a duplicate award type");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.AWARD_TYPE_EXISTS_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_missing_award_type_for_edit() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setId(TestConstants.ID_1);
        awardType.getDetails().setPayoutType("123");
        try {
            awardTypeService.updateAwardType(awardType);
            fail("Should have thrown error - can't edit an award type that doesn't exist");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.AWARD_TYPE_DOES_NOT_EXIST, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_value() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAwardValue(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - missing awardValue");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_AWARD_VALUE_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_subclient() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setSubClient(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - missing subClient");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_SUBCLIENT_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_project_number() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setProjectNumber(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - missing projectNumber");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_PROJECT_NUMBER_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_null_sub_project_number() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setSubProjectNumber(null);
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - null subProjectNumber");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.MISSING_SUB_PROJECT_NUMBER_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_payout_type_max_length() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setPayoutType("FOUR");
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - payoutType max length is 3");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.PAYOUT_TYPE_MAX_LENGTH_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_subclient_max_length() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setSubClient("SubClientWayTooLongThisShouldThrowAnError");
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - subClient max length is 30");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.SUBCLIENT_MAX_LENGTH_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_project_number_max_length() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setProjectNumber("ProjectNumberTooLongExceedsCharacterCount");
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - projectNumber max lengthis 25");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.PROJET_NUMBER_MAX_LENGTH_MESSAGE, errorMsgException);
        }
    }
    
    @Test
    public void test_validateAwardType_sub_project_number_max_length() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setSubProjectNumber("SubProjectNumberLengthTest");
        try {
            awardTypeService.insertAwardType(awardType);
            fail("Should have thrown error - subProjectNumber max length is 8");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, AwardTypeConstants.SUB_PROJECT_NUMBER_MAX_LENGTH_MESSAGE, errorMsgException);
        }
    }
    
    // *** Happy Path Testing ***
    
    @Test
    public void test_getAwardTypesList() {
        List<AwardTypeDTO> awardTypesList = awardTypeService.getAwardTypesList();
        assertNotNull("Award Types List should not be null but was.",awardTypesList);
        
        for (AwardTypeDTO awardTypeDTO : awardTypesList) {
            
            if (ELEVEN.equals(awardTypeDTO.getId()) ) {
                assertTrue("cash".equals(awardTypeDTO.getDisplayName()));
                assertTrue("Points will be converted to cash".equals(awardTypeDTO.getLongDesc()));
                assertNull(awardTypeDTO.getAwardValue());
                assertTrue("FALSE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
                assertNotNull(awardTypeDTO.getDetails());
                assertTrue("CASH".equals(awardTypeDTO.getDetails().getPayoutType()));
                assertNull(awardTypeDTO.getDetails().getCatalogUrl());
                assertNull(awardTypeDTO.getDetails().getCatalogDeepLink());
                assertNull(awardTypeDTO.getDetails().getProjectNumber());
                assertNull(awardTypeDTO.getDetails().getSubProjectNumber());
                assertNull(awardTypeDTO.getDetails().getSubClient());
                assertTrue("FALSE".equalsIgnoreCase(awardTypeDTO.getIsUsed().toString()));
            }

            if (TWELVE.equals(awardTypeDTO.getId()) ) {
                assertTrue("points".equals(awardTypeDTO.getDisplayName()));
                assertTrue("long desc for points".equals(awardTypeDTO.getLongDesc()));
                assertNotNull(awardTypeDTO.getAwardValue());
                assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
                assertNotNull(awardTypeDTO.getDetails());
                assertTrue(TESTING_PAYOUT_TYPE.equals(awardTypeDTO.getDetails().getPayoutType()));
                assertTrue("google.com".equals(awardTypeDTO.getDetails().getCatalogUrl()));
                assertTrue("/CultureNext".equals(awardTypeDTO.getDetails().getCatalogDeepLink()));
                assertTrue("S01234".equals(awardTypeDTO.getDetails().getProjectNumber()));
                assertTrue("SubProj".equals(awardTypeDTO.getDetails().getSubProjectNumber()));
                assertTrue("123".equals(awardTypeDTO.getDetails().getSubClient()));
                assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getIsUsed().toString()));
            }
        }
    }

    @Test
    public void test_get_award_type_by_payout_type() {
        AwardTypeDTO awardTypeDTO = awardTypeService.getAwardTypeByPayoutType(TESTING_PAYOUT_TYPE);
        assertNotNull("The award type dto should not be null", awardTypeDTO);
        assertTrue("points".equals(awardTypeDTO.getDisplayName()));
        assertTrue("long desc for points".equals(awardTypeDTO.getLongDesc()));
        assertNotNull(awardTypeDTO.getAwardValue());
        assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
        assertNotNull(awardTypeDTO.getDetails());
        assertTrue(TESTING_PAYOUT_TYPE.equals(awardTypeDTO.getDetails().getPayoutType()));
        assertTrue("google.com".equals(awardTypeDTO.getDetails().getCatalogUrl()));
        assertTrue("/CultureNext".equals(awardTypeDTO.getDetails().getCatalogDeepLink()));
        assertTrue("S01234".equals(awardTypeDTO.getDetails().getProjectNumber()));
        assertTrue("SubProj".equals(awardTypeDTO.getDetails().getSubProjectNumber()));
        assertTrue("123".equals(awardTypeDTO.getDetails().getSubClient()));
        assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getIsUsed().toString()));
    };

    @Test
    public void test_deleteAwardType() {
        Long payoutItemId = 14L;
        awardTypeService.deleteAwardType(payoutItemId);
        List<AwardTypeDTO> awardTypesList = awardTypeService.getAwardTypesList();
        assertNotNull("Award Types List should not be null but was.", awardTypesList);
        
        for (AwardTypeDTO awardTypeDTO : awardTypesList) {
            
            if (ELEVEN.equals(awardTypeDTO.getId()) ) {
                assertNotNull("Award Type with payout Item Id = 11 should exist.",awardTypeDTO.getId());
            }

            if (FOURTEEN.equals(awardTypeDTO.getId()) ) {
                assertNull("Award Type with payout Item Id = 14 should not exist.",awardTypeDTO.getId());
            }
            
            if (THIRTEEN.equals(awardTypeDTO.getId()) ) {
                assertNotNull("Award Type with payout Item Id = 13 should exist.",awardTypeDTO.getId());
            }
        }
    }
    
    @Test
    public void testInsertAwardType() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc("long award type description");
        awardTypesDTO.setAwardValue(1.0);
        awardTypesDTO.setAbs(true);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink("/CultureNext");
        details.setCatalogUrl("google.com");
        details.setPayoutType(DPH);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertTrue(results.get(1).getLongDesc().equals("long award type description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertTrue(results.get(1).getAbs());
        assertTrue(results.get(1).getDetails().getCatalogDeepLink().equals("/CultureNext"));
        assertTrue(results.get(1).getDetails().getCatalogUrl().equals("google.com"));
        assertTrue(results.get(1).getDetails().getPayoutType().equals(DPH));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(1).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(1).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));
        
        //Should get tied to to the MARS payout vendor
        PayoutItem absPayoutItem = payoutItemDao.findBy()
                .where(ProjectConstants.PAYOUT_ITEM_NAME).eq(DPH).findOne();
        PayoutVendor marsPayoutVendor = payoutVendorDao.findBy()
                .where(ProjectConstants.PAYOUT_VENDOR_NAME).eq(PayoutVendorEnum.MARS.name()).findOne();
        assertEquals(absPayoutItem.getPayoutVendorId(), marsPayoutVendor.getPayoutVendorId());
        
        CardType cardType = cardTypeDao.findBy().where(ProjectConstants.CARD_TYPE_CODE).eq(DPH).findOne();
        assertNotNull("Card type should have been created", cardType);
        assertEquals("Card type desc should be populated", cardType.getCardTypeDesc(), AwardTypeConstants.DEFAULT_CARD_TYPE_DESC);
    }
    
    @Test
    public void testInsertAwardType_EYD() {
        String productValue = "EYD,CCR";

        ApplicationDataDTO absProducts = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_ABS_VENDOR_PRODUCTS);
        if (absProducts == null) {
        	absProducts = new ApplicationDataDTO();
        	absProducts.setKey(ApplicationDataConstants.KEY_NAME_ABS_VENDOR_PRODUCTS);
        	absProducts.setValue(productValue);
        } else {
        	absProducts.setValue(productValue);
        }
        applicationDataService.setApplicationData(absProducts);

    	
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName("EY Points");
        awardTypesDTO.setLongDesc("These points are EXCLUSIVELY YOURS");
        awardTypesDTO.setAwardValue(1.0);
        awardTypesDTO.setAbs(true);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setPayoutType(EYD);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals("EY Points"));
        assertTrue(results.get(1).getLongDesc().equals("These points are EXCLUSIVELY YOURS"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertTrue(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals(EYD));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(1).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(1).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));
        
        //The important thing here is it gets tied to the ABS payout vendor
        PayoutItem eyPayoutItem = payoutItemDao.findBy()
                .where(ProjectConstants.PAYOUT_ITEM_NAME).eq(EYD).findOne();
        PayoutVendor absPayoutVendor = payoutVendorDao.findBy()
                .where(ProjectConstants.PAYOUT_VENDOR_NAME).eq(PayoutVendorEnum.ABS.name()).findOne();
        assertEquals(eyPayoutItem.getPayoutVendorId(), absPayoutVendor.getPayoutVendorId());
    }

    @Test
    public void testInsertAwardType_ABC() {
        String productValue = "EYD,CCR";

        ApplicationDataDTO absProducts = applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_ABS_VENDOR_PRODUCTS);
        if (absProducts == null) {
        	absProducts = new ApplicationDataDTO();
        	absProducts.setKey(ApplicationDataConstants.KEY_NAME_ABS_VENDOR_PRODUCTS);
        	absProducts.setValue(productValue);
        } else {
        	absProducts.setValue(productValue);
        }
        applicationDataService.setApplicationData(absProducts);

    	
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName("ABC Points");
        awardTypesDTO.setLongDesc("These points are Visa Card");
        awardTypesDTO.setAwardValue(1.0);
        awardTypesDTO.setAbs(true);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setPayoutType("ABC");
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(0).getDisplayName().equals("ABC Points"));
        assertTrue(results.get(0).getLongDesc().equals("These points are Visa Card"));
        assertTrue(results.get(0).getAwardValue().equals(1.0));
        assertTrue(results.get(0).getAbs());
        assertNull(results.get(0).getDetails().getCatalogDeepLink());
        assertNull(results.get(0).getDetails().getCatalogUrl());
        assertTrue(results.get(0).getDetails().getPayoutType().equals("ABC"));
        assertTrue(results.get(0).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(0).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(0).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));
        
        //The important thing here is it gets tied to the MARS payout vendor
        PayoutItem eyPayoutItem = payoutItemDao.findBy()
                .where(ProjectConstants.PAYOUT_ITEM_NAME).eq("ABC").findOne();
        PayoutVendor marsPayoutVendor = payoutVendorDao.findBy()
                .where(ProjectConstants.PAYOUT_VENDOR_NAME).eq(PayoutVendorEnum.MARS.name()).findOne();
        assertEquals(eyPayoutItem.getPayoutVendorId(), marsPayoutVendor.getPayoutVendorId());
    }

    @Test
    public void testInsertAwardType_null_value_non_abs() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc("long award type description");
        awardTypesDTO.setAwardValue(null);
        awardTypesDTO.setAbs(false);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(DPH);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);
        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertTrue(results.get(1).getLongDesc().equals("long award type description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertFalse(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals("PARKING_SPOTS"));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(1).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(1).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));
    }

    @Test
    public void testInsertAwardType_non_abs() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc("long award type description");
        awardTypesDTO.setAwardValue(1.0);
        awardTypesDTO.setAbs(false);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(DPH);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);
        
        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertTrue(results.get(1).getLongDesc().equals("long award type description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertFalse(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals("PARKING_SPOTS"));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(1).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(1).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));
    }
    
    @Test
    public void test_insertAwardType_null_details_non_abs() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.setAbs(false);
        awardType.setDisplayName("Cookies!");
        awardType.setDetails(null);
        
        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardType);
        
        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals("Cookies!"));
        assertTrue(results.get(1).getLongDesc().equals("long description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertFalse(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals("COOKIES!"));
        assertNull(results.get(1).getDetails().getProjectNumber());
        assertNull(results.get(1).getDetails().getSubClient());
        assertNull(results.get(1).getDetails().getSubProjectNumber());
    }
    
    @Test
    public void testInsertAwardType_null_project_numbers() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc("long award type description");
        awardTypesDTO.setAwardValue(null);
        awardTypesDTO.setAbs(false);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(DPH);
        details.setProjectNumber(null);
        details.setSubClient(null);
        details.setSubProjectNumber(null);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertTrue(results.get(1).getLongDesc().equals("long award type description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertFalse(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals("PARKING_SPOTS"));
        assertNull(results.get(1).getDetails().getProjectNumber());
        assertNull(results.get(1).getDetails().getSubClient());
        assertNull(results.get(1).getDetails().getSubProjectNumber());
    }

    @Test
    public void testInsertAwardType_null_sub_project_number() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc("long award type description");
        awardTypesDTO.setAwardValue(null);
        awardTypesDTO.setAbs(false);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(DPH);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(null);
        details.setSubProjectNumber(null);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertTrue(results.get(1).getLongDesc().equals("long award type description"));
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertFalse(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals("PARKING_SPOTS"));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertNull(results.get(1).getDetails().getSubClient());
        assertNull(results.get(1).getDetails().getSubProjectNumber());
    }

    @Test
    public void testInsertAwardType_null_descriptions() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc(null);
        awardTypesDTO.setAwardValue(1.0);
        awardTypesDTO.setAbs(true);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(DPH);
        details.setProjectNumber(TESTING_PROJECT_NUMBER);
        details.setSubClient(TESTING_SUBCLIENT);
        details.setSubProjectNumber(TESTING_SUBPROJECT_NUMBER);
        awardTypesDTO.setDetails(details);

        List<AwardTypeDTO> results = awardTypeService.insertAwardType(awardTypesDTO);

        assertEquals(4, results.size());
        assertTrue(results.get(1).getDisplayName().equals(PARKING_SPOTS));
        assertNull(results.get(1).getLongDesc());
        assertTrue(results.get(1).getAwardValue().equals(1.0));
        assertTrue(results.get(1).getAbs());
        assertNull(results.get(1).getDetails().getCatalogDeepLink());
        assertNull(results.get(1).getDetails().getCatalogUrl());
        assertTrue(results.get(1).getDetails().getPayoutType().equals(DPH));
        assertTrue(results.get(1).getDetails().getProjectNumber().equals(TESTING_PROJECT_NUMBER));
        assertTrue(results.get(1).getDetails().getSubClient().equals(TESTING_SUBCLIENT));
        assertTrue(results.get(1).getDetails().getSubProjectNumber().equals(TESTING_SUBPROJECT_NUMBER));

    }

    @Test
    public void testUpdateAwardType_saveProductCodeInUppercase () {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardType.getDetails().setPayoutType("nat");

        awardTypeService.updateAwardType(awardType);

        PayoutItem testPayoutItem = payoutItemDao.findBy().where(ProjectConstants.PRODUCT_CODE).eq("NAT").findOne();
        assertEquals(testPayoutItem.getProductCode(), "NAT");
    }

    @Test
    public void testUpdateAwardType_non_abs() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setId(11l);
        awardTypesDTO.setDisplayName(PARKING_SPOTS);
        awardTypesDTO.setLongDesc(LONG_AWARD_TYPE_DESCRIPTION_UPDATE);
        awardTypesDTO.setAwardValue(null);
        awardTypesDTO.setAbs(false);

        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType(TESTING_PAYOUT_TYPE);
        details.setProjectNumber(null);
        details.setSubClient(null);
        details.setSubProjectNumber(null);
        awardTypesDTO.setDetails(details);
        List<AwardTypeDTO> results = awardTypeService.updateAwardType(awardTypesDTO);
        assertNotNull("results should not be null but was.",results);
        boolean checked = false;
        for (AwardTypeDTO item : results) {
            if (item.getDetails().getPayoutType().equals(TESTING_PAYOUT_TYPE)) {
                checked = true;
                assertEquals("values should be equal but was not!", item.getDisplayName(), PARKING_SPOTS);
                assertEquals("Long Description should be equal but was not!", item.getLongDesc(), LONG_AWARD_TYPE_DESCRIPTION_UPDATE);
            }
        }
        if (!checked){
            fail("Testing payout type not found!");
        }
    }
    
    @Test
    public void testUpdateAwardType_abs() {
        AwardTypeDTO awardTypesDTO = new AwardTypeDTO();
        awardTypesDTO.setId(11l);
        awardTypesDTO.setDisplayName("service anniversary points");
        awardTypesDTO.setAwardValue(1D);
        awardTypesDTO.setAbs(true);
        AwardTypeDetailsDTO details = new AwardTypeDetailsDTO();
        details.setCatalogDeepLink(null);
        details.setCatalogUrl(null);
        details.setPayoutType("SAA");
        details.setProjectNumber(TestConstants.PROJECT_NUMBER);
        details.setSubClient("456");
        details.setSubProjectNumber("UnitTest");
        awardTypesDTO.setDetails(details);
        List<AwardTypeDTO> results = awardTypeService.updateAwardType(awardTypesDTO);
        assertNotNull("results should not be null but was.", results);
        boolean checked = false;
        for (AwardTypeDTO item : results) {
            if (item.getDetails().getPayoutType().equals("SAA")) {
                checked = true;
                assertEquals("values should be equal but was not!", item.getDisplayName(), "service anniversary points");
            }
        }
        if (!checked){
            fail("Testing payout type not found!");
        }
    }    
    
    @Test
    public void test_getAwardTypeEligibilityToReceiveList() {
        
        List<AwardTypeEligibilityToReceiveDTO> awardTypeEligibilityToReceiveList =  awardTypeService.getAwardTypeEligibilityToReceiveList("en_US",TestConstants.PAX_VANLOOWA);
        
        assertNotNull("getAwardTypeEligibilityToReceiveLis should not be null but was.", awardTypeEligibilityToReceiveList);
        
        for (AwardTypeEligibilityToReceiveDTO awardTypeDTO : awardTypeEligibilityToReceiveList) {
            
            if (ELEVEN.equals(awardTypeDTO.getAwardTypeId()) ) {
                assertTrue("cash".equals(awardTypeDTO.getDisplayName()));
                assertTrue("CASH".equals(awardTypeDTO.getPayoutType()));
                assertTrue("FALSE".equalsIgnoreCase(awardTypeDTO.getEligibleToReceive().toString()));
                assertTrue("FALSE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
                assertNull(awardTypeDTO.getCatalogUrl());
                assertNull(awardTypeDTO.getCatalogDeepLink());
            }

            if (TWELVE.equals(awardTypeDTO.getAwardTypeId()) ) {
                assertTrue("points".equals(awardTypeDTO.getDisplayName()));
                assertTrue("DPP".equals(awardTypeDTO.getPayoutType()));
                assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getEligibleToReceive().toString()));
                assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
                assertTrue("google.com".equals(awardTypeDTO.getCatalogUrl()));
                assertTrue("/CultureNext".equals(awardTypeDTO.getCatalogDeepLink()));
            }

            if (THIRTEEN.equals(awardTypeDTO.getAwardTypeId()) ) {
                assertTrue("service anniversary points".equals(awardTypeDTO.getDisplayName()));
                assertTrue("SAA".equals(awardTypeDTO.getPayoutType()));
                assertTrue("FALSE".equalsIgnoreCase(awardTypeDTO.getEligibleToReceive().toString()));
                assertTrue("TRUE".equalsIgnoreCase(awardTypeDTO.getAbs().toString()));
                assertNull(awardTypeDTO.getCatalogUrl());
                assertNull(awardTypeDTO.getCatalogDeepLink());
            }
        }
    }
    
    @Test
    public void test_insertAwardType_create_mars_properties() {
        AwardTypeDTO awardType = createAwardTypeForTests();
        awardTypeService.insertAwardType(awardType);

        ApplicationDataDTO products =
                applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_MARS_PRODUCTS);
        ApplicationDataDTO subclients = 
                applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_MARS_SUBCLIENT);
        
        assertNotNull(products);
        assertTrue(products.getValue().contains("TST=TST"));
        assertNotNull(subclients);
        assertTrue(subclients.getValue().contains("TST=1234"));
    }
}
