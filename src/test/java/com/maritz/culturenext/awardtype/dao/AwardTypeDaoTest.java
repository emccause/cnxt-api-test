package com.maritz.culturenext.awardtype.dao;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;

public class AwardTypeDaoTest extends AbstractDatabaseTest {

    private static final String PAYOUT_TYPE = "DPP";

    @Inject private AwardTypeDao awardTypeDao;

    @Test
    public void get_all_award_types() {
        List<Map<String, Object>> awardTypes = awardTypeDao.getAllAwardTypes();
        assertEquals(3, awardTypes.size());
    }

    @Test
    public void get_award_type_by_payout_type() {
        List<Map<String, Object>> awardTypes = awardTypeDao.getAwardTypeByPayoutType(PAYOUT_TYPE);
        assertEquals(1, awardTypes.size());
    }

    @Test
    public void delete_award_type() {
        awardTypeDao.deleteAwardType(TestConstants.ID_11);
        List<Map<String, Object>> awardTypes = awardTypeDao.getAllAwardTypes();
        assertEquals(2, awardTypes.size());
    }
    
    @Test
    public void get_award_type_eligibility_to_receive_list() {
        List<Map<String, Object>> awardTypes = awardTypeDao.getAwardTypeEligibilityToReceiveList(TestConstants.PAX_VANLOOWA);
        assertEquals(3, awardTypes.size());
    }
}
