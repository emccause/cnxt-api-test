package com.maritz.culturenext.awardtype.rest;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.CardType;
import com.maritz.core.jpa.entity.PayoutItem;
import com.maritz.core.jpa.entity.PayoutVendor;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.awardtype.dto.AwardTypeDTO;
import com.maritz.culturenext.awardtype.dto.AwardTypeDetailsDTO;
import com.maritz.culturenext.awardtype.service.AwardTypeService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.PayoutVendorEnum;
import com.maritz.test.AbstractRestTest;

public class AwardTypeRestServiceTest  extends AbstractRestTest{

    private static final String TESTING_SUB_PROJECT_NUMBER = "S01234";
    private static final String PROJECT_NUMBER = "P01234";
    private static final String TESTING_SUB_CLIENT = "123";
    private static final String TESTING_PAYOUT_TYPE = "DPP";
    private static final String LONG_DESCRIPTION = "long description";
    private static final String DISPLAY_NAME = "Parking Spots";
    private static final String URL_CREATE_AWARD_TYPES = "/rest/award-types";
    private static final String URL_GET_ALL_AWARD_TYPES = "/rest/award-types";
    private static final String URL_DELETE_AWARD_TYPE_NO_ID = "/rest/award-types/";
    private static final String URL_DELETE_AWARD_TYPE_DPP = "/rest/award-types/12";
    private static final String URL_DELETE_AWARD_TYPE_SSA = "/rest/award-types/13";
    private static final String URL_UPDATE_AWARD_TYPES = "/rest/award-types/11";
    private static final String URL_USER_ELEGIBILITY_GET_AWARDS = "/rest/participants/~/award-types";
    private static final String URL_USER_ELEGIBILITY_GET_AWARDS_WRONG_PATH = "/rest/participants/award-types";

    @Inject private ApplicationDataService applicationDataService;
    @Inject private AwardTypeService awardTypeService;
    @Inject private ConcentrixDao<CardType> cardTypeDao;
    @Inject private ConcentrixDao<PayoutItem> payoutItemDao;
    @Inject private ConcentrixDao<PayoutVendor> payoutVendorDao;
    
    @Test
    public void create_award_type() throws Exception {
        AwardTypeDetailsDTO awardTypeDetailsDTO = new AwardTypeDetailsDTO();
        awardTypeDetailsDTO.setPayoutType("FCP");
        awardTypeDetailsDTO.setSubClient(TESTING_SUB_CLIENT);
        awardTypeDetailsDTO.setProjectNumber(PROJECT_NUMBER);
        awardTypeDetailsDTO.setSubProjectNumber(TESTING_SUB_PROJECT_NUMBER);
        awardTypeDetailsDTO.setCatalogUrl(null);
        awardTypeDetailsDTO.setCatalogDeepLink(null);

        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName(DISPLAY_NAME);
        awardTypeDTO.setLongDesc(LONG_DESCRIPTION);
        awardTypeDTO.setAwardValue(5.0);
        awardTypeDTO.setAbs(false);
        awardTypeDTO.setDetails(awardTypeDetailsDTO);

        String json = ObjectUtils.objectToJson(awardTypeDTO);

        MvcResult result = mockMvc.perform(
                post(URL_CREATE_AWARD_TYPES)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(DISPLAY_NAME));
        assert(result.getResponse().getContentAsString().contains(LONG_DESCRIPTION));
        assert(result.getResponse().getContentAsString().contains("5.0"));
        assert(result.getResponse().getContentAsString().contains("false"));
        assert(result.getResponse().getContentAsString().contains("PARKING_SPOTS"));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_CLIENT));
        assert(result.getResponse().getContentAsString().contains(PROJECT_NUMBER));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_PROJECT_NUMBER));
    }

    @Test
    public void create_award_type_non_maritz_admin() throws Exception {
        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName(DISPLAY_NAME);
        awardTypeDTO.setLongDesc(LONG_DESCRIPTION);
        awardTypeDTO.setAwardValue(5.0);
        awardTypeDTO.setAbs(false);

        String json = ObjectUtils.objectToJson(awardTypeDTO);

        mockMvc.perform(
                post(URL_CREATE_AWARD_TYPES)
                        .with(authToken(TestConstants.USER_GARCIAF2))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(json))
                .andExpect(status().isForbidden());
    }

    @Test
    public void create_award_type_no_abs() throws Exception {
        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName(DISPLAY_NAME);
        awardTypeDTO.setAwardValue(5.0);

        String json = ObjectUtils.objectToJson(awardTypeDTO);

        mockMvc.perform(
                post(URL_CREATE_AWARD_TYPES)
                        .with(authToken(TestConstants.USER_ADMIN))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(json))
                .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void update_award_type() throws Exception {
        AwardTypeDetailsDTO awardTypeDetailsDTO = new AwardTypeDetailsDTO();
        awardTypeDetailsDTO.setPayoutType(TESTING_PAYOUT_TYPE);
        awardTypeDetailsDTO.setSubClient(TESTING_SUB_CLIENT);
        awardTypeDetailsDTO.setProjectNumber(PROJECT_NUMBER);
        awardTypeDetailsDTO.setSubProjectNumber(TESTING_SUB_PROJECT_NUMBER);
        awardTypeDetailsDTO.setCatalogUrl(null);
        awardTypeDetailsDTO.setCatalogDeepLink(null);

        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName(DISPLAY_NAME);
        awardTypeDTO.setLongDesc(LONG_DESCRIPTION);
        awardTypeDTO.setAwardValue(5.0);
        awardTypeDTO.setAbs(false);
        awardTypeDTO.setDetails(awardTypeDetailsDTO);

        String json = ObjectUtils.objectToJson(awardTypeDTO);

        MvcResult result = mockMvc.perform(
                put(URL_UPDATE_AWARD_TYPES)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(DISPLAY_NAME));
        assert(result.getResponse().getContentAsString().contains(LONG_DESCRIPTION));
        assert(result.getResponse().getContentAsString().contains("5.0"));
        assert(result.getResponse().getContentAsString().contains("false"));
        assert(result.getResponse().getContentAsString().contains(DISPLAY_NAME));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_CLIENT));
        assert(result.getResponse().getContentAsString().contains(PROJECT_NUMBER));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_PROJECT_NUMBER));
    }
    
    @Test
    public void test_award_type_abs() throws Exception {
        AwardTypeDetailsDTO awardTypeDetailsDTO = new AwardTypeDetailsDTO();
        awardTypeDetailsDTO.setPayoutType("VSR");
        awardTypeDetailsDTO.setSubClient(TESTING_SUB_CLIENT);
        awardTypeDetailsDTO.setProjectNumber(PROJECT_NUMBER);
        awardTypeDetailsDTO.setSubProjectNumber(TESTING_SUB_PROJECT_NUMBER);
        awardTypeDetailsDTO.setCatalogUrl(null);
        awardTypeDetailsDTO.setCatalogDeepLink(null);

        AwardTypeDTO awardTypeDTO = new AwardTypeDTO();
        awardTypeDTO.setDisplayName("Visa Cash Card");
        awardTypeDTO.setLongDesc(LONG_DESCRIPTION);
        awardTypeDTO.setAwardValue(5.0);
        awardTypeDTO.setAbs(true);
        awardTypeDTO.setDetails(awardTypeDetailsDTO);

        String json = ObjectUtils.objectToJson(awardTypeDTO);

        MvcResult result = mockMvc.perform(
                post(URL_CREATE_AWARD_TYPES)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("Visa Cash Card"));
        assert(result.getResponse().getContentAsString().contains(LONG_DESCRIPTION));
        assert(result.getResponse().getContentAsString().contains("5.0"));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_CLIENT));
        assert(result.getResponse().getContentAsString().contains(PROJECT_NUMBER));
        assert(result.getResponse().getContentAsString().contains(TESTING_SUB_PROJECT_NUMBER));

        //The important thing here is it gets tied to the MARS payout vendor
        PayoutItem vsrPayoutItem = payoutItemDao.findBy()
                .where(ProjectConstants.PAYOUT_ITEM_NAME).eq("VSR").findOne();
        PayoutVendor marsPayoutVendor = payoutVendorDao.findBy()
                .where(ProjectConstants.PAYOUT_VENDOR_NAME).eq(PayoutVendorEnum.MARS.name()).findOne();
        assertEquals(vsrPayoutItem.getPayoutVendorId(), marsPayoutVendor.getPayoutVendorId());
    }
    
    @Test
    public void get_all_award_types_happy_path() throws  Exception {
        MvcResult result = mockMvc.perform(
                get(URL_GET_ALL_AWARD_TYPES)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(TestConstants.CASH));
        assert(result.getResponse().getContentAsString().contains(TestConstants.DPP));
        assert(result.getResponse().getContentAsString().contains(TestConstants.SAA));
    }
    
    @Test
    public void get_all_award_types_not_admin() throws  Exception {
        mockMvc.perform(
                get(URL_GET_ALL_AWARD_TYPES)
                        .with(authToken(TestConstants.USER_GARCIAF2))
        ).andExpect(status().isForbidden());
    }
    
    @Test
    public void delete_award_type_ssa_happy_path() throws  Exception {
        mockMvc.perform(
                delete(URL_DELETE_AWARD_TYPE_SSA)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
        
        MvcResult result = mockMvc.perform(
                get(URL_GET_ALL_AWARD_TYPES)
                .with(authToken(TestConstants.USER_ADMIN)))
                .andExpect(status().isOk())
                .andReturn();
        assert(result.getResponse().getContentAsString().contains(TestConstants.CASH));
        assert(result.getResponse().getContentAsString().contains(TestConstants.DPP));
        assert(!result.getResponse().getContentAsString().contains(TestConstants.SAA));
    }
    
    @Test
    public void delete_award_type_dpp_not_admin() throws  Exception {
        mockMvc.perform(
                delete(URL_DELETE_AWARD_TYPE_DPP)
                .with(authToken(TestConstants.USER_GARCIAF2))
        ).andExpect(status().isForbidden());
    }
    
    @Test
    public void delete_award_type_without_id() throws  Exception {
        mockMvc.perform(
                delete(URL_DELETE_AWARD_TYPE_NO_ID)
                .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isMethodNotAllowed());
    }
    
    @Test
    public void get_user_eligibility_get_awards_happy_path() throws  Exception {
        MvcResult result = mockMvc.perform(
                get(URL_USER_ELEGIBILITY_GET_AWARDS)
                .with(authToken(TestConstants.USER_VANLOOWA)))
                .andExpect(status().isOk())
                .andReturn();        
        assert(result.getResponse().getContentAsString().contains("{\"awardTypeId\":11,\"payoutType\":\"CASH\",\"displayName\":\"cash\",\"eligibleToReceive\":false,\"abs\":false,\"catalogUrl\":null,\"catalogDeepLink\":null,\"lookupDisplayNameId\":20}"));
        assert(result.getResponse().getContentAsString().contains("{\"awardTypeId\":12,\"payoutType\":\"DPP\",\"displayName\":\"points\",\"eligibleToReceive\":true,\"abs\":true,\"catalogUrl\":\"google.com\",\"catalogDeepLink\":\"/CultureNext\",\"lookupDisplayNameId\":2}"));
        assert(result.getResponse().getContentAsString().contains("{\"awardTypeId\":13,\"payoutType\":\"SAA\",\"displayName\":\"service anniversary points\",\"eligibleToReceive\":false,\"abs\":true,\"catalogUrl\":null,\"catalogDeepLink\":null,\"lookupDisplayNameId\":13}"));
    }
    

    @Test
    public void get_user_eligibility_get_awards_wrong_path() throws Exception {
        mockMvc.perform(
                get(URL_USER_ELEGIBILITY_GET_AWARDS_WRONG_PATH)
                .with(authToken(TestConstants.USER_VANLOOWA)))
                .andExpect(status().is4xxClientError());
    }
}