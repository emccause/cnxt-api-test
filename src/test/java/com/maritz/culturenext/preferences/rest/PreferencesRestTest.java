package com.maritz.culturenext.preferences.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.preferences.constants.PreferenceConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PreferencesRestTest extends AbstractRestTest {

    private static final String PATH = "/rest/participants/";
    private static final String PREFERENCE_ENDPOINT = "/preferences/";
    private static final String ACTIVITY_FEED_FILTER = "ACTIVITY_FEED_FILTER";

    @Test
    public void test_create_user_preference_no_preference_given() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_INVALID_PREFERENCES_MISSING));
    }

    @Test
    public void test_create_user_preference_key_missing() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"value\":\"testValue\"}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_KEY_MISSING));
    }

    @Test
    public void test_create_user_preference_value_missing() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\"}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_VALUE_MISSING));
    }

    @Test
    public void test_create_user_preference_already_exists() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_KEY_EXIST));
    }

    @Test
    public void test_create_user_preference_good() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("testKey"));
        assert(result.getResponse().getContentAsString().contains("testValue"));
    }

    @Test
    public void test_update_user_preference_value_missing() throws Exception {
        MvcResult result = mockMvc.perform(
                put(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "testKey")
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{}")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_VALUE_MISSING));
    }

    @Test
    public void test_update_user_preference_key_not_exist() throws Exception {
        MvcResult result = mockMvc.perform(
                put(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "invalidKey")
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\"value\":\"testValue\"}")
        )
                .andExpect(status().isNotFound())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST));
    }

    @Test
    public void test_update_user_preference_good() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                put(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "testKey")
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("{\"value\":\"updatedValue\"}")
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("updatedValue"));
    }

    @Test
    public void test_delete_user_preference_good() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk());

        mockMvc.perform(
                delete(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "testKey")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_delete_user_preference_not_found() throws Exception {
        MvcResult result = mockMvc.perform(
                delete(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "testKey")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isNotFound())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST));
    }

    @Test
    public void test_get_all_user_preferences_good() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void test_get_user_preferences_by_key_good() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"testKey\",\"value\":\"testValue\"}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "testKey")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().equals("{\"key\":\"testKey\",\"value\":\"testValue\"}"));
    }

    @Test
    public void test_get_user_preferences_by_key_not_found() throws Exception {
        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + "invalidKey")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isNotFound())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(PreferenceConstants.ERROR_PREFERENCE_KEY_NOT_EXIST));
    }

    @Test
    public void test_get_activity_feed_wo_public_default_override() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(org.springframework.http.HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"key\":\"ACTIVITY_FEED_FILTER\",\"value\":\"NETWORK\"}]")
        )
                .andExpect(status().isOk());
        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + PREFERENCE_ENDPOINT + ACTIVITY_FEED_FILTER)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();
        System.out.println(result.getResponse().getContentAsString());
        assert(!result.getResponse().getContentAsString().contains("PUBLIC"));
    }
}
