package com.maritz.culturenext.preferences.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.PaxData;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.preferences.dto.PreferenceDTO;
import com.maritz.culturenext.preferences.services.impl.PreferencesServiceImpl;
import com.maritz.test.AbstractMockTest;

public class PreferencesServiceTest extends AbstractMockTest {

    @InjectMocks private PreferencesServiceImpl preferencesService;
    @Mock private ConcentrixDao<PaxData> paxDataDao;

    private FindBy<PaxData> mockedFindByPaxData = mock(FindBy.class);

    private static final String testKey = "key";
    private static final String testValue = "value";

    // 4001 characters.
    private final String TEST_VALUE_TOO_LONG = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk"
            + "lmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd"
            + "efghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvw"
            + "xyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnop"
            + "qrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghij"
            + "klmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcde"
            + "fghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
            + "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrst"
            + "uvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmno"
            + "pqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghij"
            + "klmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd"
            + "efghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx"
            + "yzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqr"
            + "stuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklm"
            + "nopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefgh"
            + "ijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd"
            + "efghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy"
            + "zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrst"
            + "uvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmno"
            + "pqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghij"
            + "klmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdef"
            + "ghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab"
            + "cdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwx"
            + "yzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrst"
            + "uvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnop"
            + "qrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijkl"
            + "mnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefgh"
            + "ijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd"
            + "efghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
            + "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuv"
            + "wxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqr"
            + "stuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmn"
            + "opqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghij"
            + "klmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdef"
            + "ghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzab"
            + "cdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxy"
            + "zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstu"
            + "vwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopq"
            + "rstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklm"
            + "nopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghi"
            + "jklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvW";

    // 245 characters
    private final String TEST_KEY_TOO_LONG = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnop"
            + "qrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijkl"
            + "mnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijk";

    private static Comparator<PreferenceDTO> preferenceComparator
            = new Comparator<PreferenceDTO>() {
        @Override
        public int compare(PreferenceDTO o1, PreferenceDTO o2) {
            return o1.getKey().compareToIgnoreCase(o2.getKey());
        }
    };

    @Before
    public void setup() {
        PowerMockito.when(paxDataDao.findBy()).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.where(anyString())).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.eq(anyLong())).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.eq(anyString())).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.and(anyString())).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.contains(anyString())).thenReturn(mockedFindByPaxData);
        PowerMockito.when(mockedFindByPaxData.isNull()).thenReturn(mockedFindByPaxData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_getPreferenceForUserByKey_key_not_exist() {
        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        preferencesService.getPreferenceForUserByKey(TestConstants.ID_1, "Invalid_Key");
    }

    @Test
    public void test_getPreferenceForUserByKey_happy_path() {
        PaxData paxData = new PaxData();
        paxData.setKeyName(testKey);
        paxData.setValue(testValue);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxData);

        PreferenceDTO preferenceDTO = preferencesService.getPreferenceForUserByKey(TestConstants.ID_1, testKey);

        assertEquals(preferenceDTO.getKey(), testKey);
        assertEquals(preferenceDTO.getValue(), testValue);
    }

    @Test
    public void test_getAllPreferencesForUser_empty_result() {
        PowerMockito.when(mockedFindByPaxData.find()).thenReturn(new ArrayList<PaxData>());

        List<PreferenceDTO> preferenceDTOList = preferencesService.getAllPreferencesForUser(TestConstants.ID_1);

        assertTrue(preferenceDTOList.isEmpty());
    }

    @Test
    public void test_getAllPreferencesForUser_happy_path() {
        int resultSize = 10;

        List<PaxData> paxDataList = new ArrayList<>();
        for (int i = 0; i < resultSize; i++) {
            PaxData paxData = new PaxData();
            paxData.setKeyName(testKey+i);
            paxData.setValue(testValue+i);
            paxDataList.add(paxData);
        }

        PowerMockito.when(mockedFindByPaxData.find()).thenReturn(paxDataList);

        List<PreferenceDTO> preferenceDTOList = preferencesService.getAllPreferencesForUser(TestConstants.ID_1);

        // Guarantee order before running assertion loop
        Collections.sort(preferenceDTOList, preferenceComparator);

        for (int i = 0; i < resultSize; i++) {
            assertEquals(preferenceDTOList.get(i).getKey(), testKey+i);
            assertEquals(preferenceDTOList.get(i).getValue(), testValue+i);
        }
    }

    @Test(expected = ErrorMessageException.class)
    public void test_deletePreferenceForUser_key_not_exist() {
        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        preferencesService.deletePreferenceForUser(TestConstants.ID_1, testKey);
    }

    @Test
    public void test_deletePreferenceForUser_happy_path() {
        PaxData paxData = new PaxData();
        paxData.setKeyName(testKey);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxData);

        preferencesService.deletePreferenceForUser(TestConstants.ID_1, testKey);

        Mockito.verify(paxDataDao, times(1)).delete(paxData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preferences_missing() {
        preferencesService.createPreferencesForUser(TestConstants.ID_1, null);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preference_key_missing() {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setValue(testValue);
        preferenceDTOList.add(preferenceDTO);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preference_key_empty() {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey("");
        preferenceDTO.setValue(testValue);
        preferenceDTOList.add(preferenceDTO);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preference_value_empty() {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue("");
        preferenceDTOList.add(preferenceDTO);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preference_value_missing() {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTOList.add(preferenceDTO);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_preference_already_exists() {
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue(testValue);
        preferenceDTOList.add(preferenceDTO);

        PaxData paxData = new PaxData();
        paxData.setKeyName(testKey);
        paxData.setValue(testValue);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxData);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);
    }

    @Test
    public void test_createPreferencesForUser_happy_path() {
        int resultSize = 5;
        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();
        for (int i = 0; i < resultSize; i++) {
            PreferenceDTO preferenceDTO = new PreferenceDTO();
            preferenceDTO.setKey(testKey+i);
            preferenceDTO.setValue(testValue+i);
            preferenceDTOList.add(preferenceDTO);
        }

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        List<PreferenceDTO> preferenceResultSet =
                preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);

        // Guarantee order before running assertion loop
        Collections.sort(preferenceResultSet, preferenceComparator);

        for (int i = 0; i < resultSize; i++) {
            assertEquals(preferenceDTOList.get(i).getKey(), preferenceResultSet.get(i).getKey());
            assertEquals(preferenceDTOList.get(i).getValue(), preferenceResultSet.get(i).getValue());
        }
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updatePreferenceForUser_preference_key_missing() {
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setValue(testValue);

        preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updatePreferenceForUser_preference_value_missing() {
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);

        preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_updatePreferenceForUser_preference_not_exist() {
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue(testValue);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);
    }

    @Test
    public void test_updatePreferenceForUser_happy_path() {
        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue(testValue);

        PaxData paxDataToUpdate = new PaxData();
        paxDataToUpdate.setKeyName(testKey);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxDataToUpdate);

        PreferenceDTO updatedPreference = preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);

        Mockito.verify(paxDataDao, times(1)).update(paxDataToUpdate);
        assertSame(preferenceDTO, updatedPreference);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_fail_value_too_long() {

        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();

        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue(TEST_VALUE_TOO_LONG);
        preferenceDTOList.add(preferenceDTO);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);

    }

    @Test(expected = ErrorMessageException.class)
    public void test_updatePreferenceForUser_fail_value_too_long() {

        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(testKey);
        preferenceDTO.setValue(TEST_VALUE_TOO_LONG);

        PaxData paxDataToUpdate = new PaxData();
        paxDataToUpdate.setKeyName(testKey);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxDataToUpdate);

        preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);

    }

    @Test(expected = ErrorMessageException.class)
    public void test_createPreferencesForUser_fail_key_too_long() {

        List<PreferenceDTO> preferenceDTOList = new ArrayList<>();

        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(TEST_KEY_TOO_LONG);
        preferenceDTO.setValue(testValue);
        preferenceDTOList.add(preferenceDTO);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(null);

        preferencesService.createPreferencesForUser(TestConstants.ID_1, preferenceDTOList);

    }

    @Test(expected = ErrorMessageException.class)
    public void test_updatePreferenceForUser_fail_key_too_long() {

        PreferenceDTO preferenceDTO = new PreferenceDTO();
        preferenceDTO.setKey(TEST_KEY_TOO_LONG);
        preferenceDTO.setValue(testValue);

        PaxData paxDataToUpdate = new PaxData();
        paxDataToUpdate.setKeyName(testKey);

        PowerMockito.when(mockedFindByPaxData.findOne()).thenReturn(paxDataToUpdate);

        preferencesService.updatePreferenceForUser(TestConstants.ID_1, preferenceDTO);

    }
}
