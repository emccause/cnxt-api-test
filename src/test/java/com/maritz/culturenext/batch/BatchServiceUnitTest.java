package com.maritz.culturenext.batch;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.culturenext.batch.service.impl.BatchServiceImpl;
import com.maritz.culturenext.batch.util.BatchUtil;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.jpa.entity.StageAdvancedRec;
import com.maritz.test.AbstractMockTest;

public class BatchServiceUnitTest extends AbstractMockTest {
    @InjectMocks BatchServiceImpl batchService;

    @Mock ConcentrixDao<Batch> batchDao;
    @Mock BatchUtil batchUtil;
    @Mock ConcentrixDao<StageAdvancedRec> stageAdvancedRecDao;
    @Mock FindBy<StageAdvancedRec> stageAdvancedRecFindBy;

    @Test
    public void test_updateBatchStatus_happyPath() {
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(11111L);
        batchStatusDTO.setStatus(StatusTypeCode.QUEUED.name());

        boolean success = true;
        try {
            PowerMockito.when(batchUtil.validateBatchStatusDto(batchStatusDTO))
                .thenReturn(new Batch().setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name()));
            batchService.updateBatchStatus(batchStatusDTO);
        } catch (ErrorMessageException ex) {
            success = false;
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_updateBatchStatus_cancelled_happyPath() {
        Long batchId = 11111L;
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(batchId);
        batchStatusDTO.setStatus(StatusTypeCode.CANCELLED.name());

        boolean success = true;
        try {
            PowerMockito.when(batchUtil.validateBatchStatusDto(batchStatusDTO))
                .thenReturn(new Batch()
                                .setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name())
                                .setBatchTypeCode(BatchType.ADV_REC_BULK_UPLOAD.name()));

            // stageAdvancedRecDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchStatusDto.getBatchId()).find()
            PowerMockito.when(stageAdvancedRecDao.findBy()).thenReturn(stageAdvancedRecFindBy);
            PowerMockito.when(stageAdvancedRecFindBy.where(anyString())).thenReturn(stageAdvancedRecFindBy);
            PowerMockito.when(stageAdvancedRecFindBy.eq(anyLong())).thenReturn(stageAdvancedRecFindBy);

            StageAdvancedRec stageAdvancedRec = new StageAdvancedRec();
            stageAdvancedRec.setSubmitterId(TestConstants.USER_MUDDAM);
            stageAdvancedRec.setBudgetId(1L);
            List<StageAdvancedRec> stageAdvancedRecList = new ArrayList<>();
            stageAdvancedRecList.add(stageAdvancedRec);

            PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(stageAdvancedRecList);

            batchService.updateBatchStatus(batchStatusDTO);

            //Make sure the StageAdvanceRec records got deleted
            Mockito.verify(stageAdvancedRecDao, Mockito.times(1)).delete(stageAdvancedRec);
        } catch (ErrorMessageException ex) {
            success = false;
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_deleteStageAdvanceRecForBatch_happyPath() {
        // stageAdvancedRecDao.findBy().where(ProjectConstants.BATCH_ID).eq(batchStatusDto.getBatchId()).find()
        PowerMockito.when(stageAdvancedRecDao.findBy()).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.where(anyString())).thenReturn(stageAdvancedRecFindBy);
        PowerMockito.when(stageAdvancedRecFindBy.eq(anyLong())).thenReturn(stageAdvancedRecFindBy);

        StageAdvancedRec stageAdvancedRec = new StageAdvancedRec();
        stageAdvancedRec.setSubmitterId(TestConstants.USER_MUDDAM);
        stageAdvancedRec.setBudgetId(1L);
        List<StageAdvancedRec> stageAdvancedRecList = new ArrayList<>();
        stageAdvancedRecList.add(stageAdvancedRec);

        PowerMockito.when(stageAdvancedRecFindBy.find()).thenReturn(stageAdvancedRecList);

        batchService.deleteStageAdvanceRecForBatch(11111L);

        //Make sure the StageAdvanceRec records got deleted
        Mockito.verify(stageAdvancedRecDao, Mockito.times(1)).delete(stageAdvancedRec);
    }
}
