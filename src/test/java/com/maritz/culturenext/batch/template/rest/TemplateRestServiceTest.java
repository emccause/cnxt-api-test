package com.maritz.culturenext.batch.template.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class TemplateRestServiceTest extends AbstractRestTest{

    @Test
    public void getTemplatePointUpload() throws Exception {
        mockMvc.perform(
                get("/rest/templates/point-upload")
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk());
    }

    @Test
    public void getTemplateRecognition() throws Exception {
        mockMvc.perform(
                get("/rest/templates/recognitions")
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void getTemplateRecognitionFailedPermission() throws Exception {
        mockMvc.perform(
                get("/rest/templates/recognitions")
                        .with(authToken(TestConstants.USER_MUDDAM))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void getTemplateRecognitionFailedUnAuthorized() throws Exception {
        mockMvc.perform(
                get("/rest/templates/recognitions")
                        .with(authToken(TestConstants.USER_INVALID))
        ).andExpect(status().isUnauthorized());
    }

    @Test
    public void getTemplateWithAdmin() throws Exception {
        mockMvc.perform(
                get("/rest/templates/ADV_REC_BULK_UPLOAD")
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void getTemplateWithNonAdmin() throws Exception {
        mockMvc.perform(
                get("/rest/templates/ADV_REC_BULK_UPLOAD")
                        .with(authToken(TestConstants.USER_PORTERGA))
        ).andExpect(status().isForbidden());
    }

    @Test
    public void getTemplateFailedUnAuthorized() throws Exception {
        mockMvc.perform(
                get("/rest/templates/ADV_REC_BULK_UPLOAD")
                        .with(authToken(TestConstants.USER_INVALID))
        ).andExpect(status().isUnauthorized());
    }
}
