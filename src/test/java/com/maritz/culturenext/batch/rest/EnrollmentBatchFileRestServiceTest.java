package com.maritz.culturenext.batch.rest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import com.maritz.culturenext.enrollment.constants.EnrollmentConstants;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.core.jpa.repository.BatchEventRepository;
import com.maritz.core.services.FileService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.test.AbstractRestTest;

import java.util.ArrayList;
import java.util.List;

public class EnrollmentBatchFileRestServiceTest extends AbstractRestTest {

    @Inject private BatchEventRepository batchEventRepository;
    @Inject FileService inboxFileService;

    private static final String ENROLMENT_FILE_UPLOAD_URL =  "/rest/enrollments";

    private static final String FILE_PARAMETER = "file";
    private static final String FILE_NAME = "ENROLLMENT.csv";
    private static final String FILE_WRONG_FORMAT =
    "Award Amount;Recipient First Name;Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Award Type (Payout Type),Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n" +
    "246,,,1706.publicHeadline,Message private 1,programName,40,budgetName,76,CASH,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n" +
    "246,Francisco,Perez,1346,publicHeadline,,programName,40,budgetName,78,POINTS,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n" +
    "246;Pedro,Ontiveros,8326,publicHeadline,Message private 2,programName,40,budgetName,78,POINTS,submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";

    private static final MockMultipartFile WRONG_FORMAT_MULTIPART_FILE = new MockMultipartFile(
            FILE_PARAMETER,
            FILE_NAME,
            MediaTypesEnum.APPLICATION_OPEN_FORMAT_SPREADSHEET.value(),
            FILE_WRONG_FORMAT.getBytes());

    private static final MockMultipartFile RIGHT_FORMAT_MULTIPART_FILE = new MockMultipartFile(
            FILE_PARAMETER,
            FILE_NAME,
            MediaTypesEnum.TEXT_CSV.value(),
            FILE_WRONG_FORMAT.getBytes());

    @After
    public void cleanUp() throws Exception {
        try {
            // integration test doesn't actually move the test file out of stage, but the process writes a new file
            inboxFileService.delete(inboxFileService.getFile("enrollment/ENROLLMENT.csv"));
        }
        catch (Exception e) {
            // file not found, whatever
        }
    }

    @Test
    public void getLatestInfo() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments/latest")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void getLatestInfoIncludeInProgress() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments/latest?includeInProgress=TRUE")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void getInfoForIdGood() throws Exception {
        Long parentId = batchEventRepository.findBy()
                .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.CHILD_BATCH_ID.getCode())
                .and(ProjectConstants.MESSAGE).eq("1")
                .findOne(ProjectConstants.BATCH_ID, Long.class);

        mockMvc.perform(
                get("/rest/enrollments/" + parentId)
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void getInfoForIdNotEnrollment() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments/2")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void getInfoForIdBadNumber() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments/99999999")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void getInfoForBatchesSinceNoParam() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void postFileForProcessingWithFileWithWrongFormat() throws Exception {
        try {
            List<MockMultipartFile> fileList = new ArrayList<MockMultipartFile>();
            fileList.add(WRONG_FORMAT_MULTIPART_FILE);

            MvcResult result = mockMvc.perform(

                fileUpload(String.format(ENROLMENT_FILE_UPLOAD_URL))
                    .file(fileList.get(0))
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isBadRequest())
                .andReturn();

            assert(result.getResponse().getContentAsString().contains("The uploaded file is not a CSV."));
        } catch (Exception ex){
            assertThat(ex.getMessage(), is("Required MultipartFile parameter 'file' is not present"));
        }
    }

    @Test
    public void postFileForProcessingWithoutFileElement() throws Exception {
        mockMvc.perform(
            fileUpload(String.format(ENROLMENT_FILE_UPLOAD_URL))
                    .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void postFileForProcessingWithNullFile() throws Exception {
        mockMvc.perform(
            fileUpload(String.format(ENROLMENT_FILE_UPLOAD_URL))
                .file(new MockMultipartFile("EMPTY_FILE", new byte[0]))
                .with(authToken(TestConstants.USER_ADMIN))
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void insertEnrollmentFileTest() throws Throwable{
        mockMvc.perform(
                fileUpload(String.format(ENROLMENT_FILE_UPLOAD_URL))
                    .file(RIGHT_FORMAT_MULTIPART_FILE)
                    .with(authToken(TestConstants.USER_ADMIN))
                ).andExpect(status().isOk());
        }

    @Test
    public void getInfoForBatchesSinceGoodParam() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments?fromDate=2015-01-01")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    //Service should fall back to no date default functionality if fromDate isn't properly formed
    @Test
    public void getInfoForBatchesSinceBadParam() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments?fromDate=BREAKING")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void getEnrollmentErrorsByBatchGood() throws Exception {
        Long batchId = batchEventRepository.findBy()
                .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.CHILD_BATCH_ID.getCode())
                .and(ProjectConstants.MESSAGE).eq("102")
                .findOne(ProjectConstants.BATCH_ID, Long.class);
        mockMvc.perform(
                get("/rest/enrollments/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
            ;
    }

    @Test
    public void getEnrollmentErrorsByBatchBadId() throws Exception {
        Long batchId = -1L;
        mockMvc.perform(
                get("/rest/enrollments/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void getEnrollmentErrorsByBatchBadEnrollmentId() throws Exception {
        mockMvc.perform(
                get("/rest/enrollments/" + TestConstants.ID_2 + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void getEnrollmentErrorsByBatchIdMissingBatchFile() throws Exception {
        Long batchId = batchEventRepository.findBy()
                .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.CHILD_BATCH_ID.getCode())
                .and(ProjectConstants.MESSAGE).eq("3599")
                .findOne(ProjectConstants.BATCH_ID, Long.class);

        mockMvc.perform(
                get("/rest/enrollments/" + batchId + "/errorlog")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void getEnrollmentConfigInfo() throws Exception {
        mockMvc.perform(
                get("/rest/enrollment-config/groups-processing-status")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    @Test
    public void getEnrollmentConfigInfoIncludeInProgress() throws Exception {
        mockMvc.perform(
                get("/rest/enrollment-config/groups-processing-status?includeInProgress=TRUE")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk());
    }

    private boolean enrollmentFolderExists(){
        return inboxFileService.getFiles().stream().filter(f -> f.getIsDirectory()).findFirst().get().getName().compareTo(EnrollmentConstants.ENROLLMENT_SUBFOLDER_NAME)==0;
    }
}