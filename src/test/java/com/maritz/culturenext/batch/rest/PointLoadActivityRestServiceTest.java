package com.maritz.culturenext.batch.rest;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.BatchFile;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.test.AbstractRestTest;

public class PointLoadActivityRestServiceTest extends AbstractRestTest {
    
    @Inject ConcentrixDao<BatchFile> batchFileDao;
    
    @Test
    public void testGetPointLoadActivityNoFromDateNoStatuses() throws Exception {
        mockMvc.perform(
            get("/rest/point-uploads")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk());
    }

    @Test
    public void testGetPointLoadActivityWithFromDateNoStatuses() throws Exception {
        mockMvc.perform(
            get("/rest/point-uploads?fromDate=2015-01-01")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk());
    }

    @Test
    public void testGetPointLoadActivityWithFromDateAndStatuses() throws Exception {
        mockMvc.perform(
            get("/rest/point-uploads?fromDate=2015-01-01&status=SUCCESS,RESTRICTED")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk());
    }

    @Test
    public void testGetPointLoadActivityPppIndexApplied() throws Exception {
        mockMvc.perform(
            get("/rest/point-uploads")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[0].pppIndexApplied", notNullValue()));
    }
   
    @Test
    public void getPointLoadErrorsByBatchGood() throws Exception {
        Long batchId = 16397L;
        mockMvc.perform(
                get("/rest/point-uploads/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
            ;
    }
    
    @Test
    public void getPointLoadErrorsByBatchBadId() throws Exception {
        Long batchId = -1L;
        mockMvc.perform(
                get("/rest/point-uploads/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isBadRequest())
            ;
    }
    
    @Test
    public void getPointLoadErrorsByBatchBadEnrollmentId() throws Exception {
        mockMvc.perform(
                get("/rest/point-uploads/" + TestConstants.ID_2 + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void getPointLoadErrorsByBatchIdMissingBatchFile() throws Exception {
        Long batchId = 4722L;
        mockMvc.perform(
                get("/rest/point-uploads/" + batchId + "/errorlog")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(status().isNotFound());
    }
    
    @Test
    public void getPointLoadErrors_batchId_as_string() throws Exception {
        mockMvc.perform(
                get("/rest/point-uploads/batchIdString/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isNotFound())
            ;
    }
    
    @Test
    public void test_upload_file() throws Exception {
        
        String path = getClass().getClassLoader().getResource("test-data").getPath();
        FileInputStream fis = new FileInputStream(path + File.separator + "point_load.csv");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "point_load.csv", MediaTypesEnum.TEXT_CSV.value(), fis);

        mockMvc.perform(
                fileUpload("/rest/point-uploads")
                .file(multipartFile)
                .with(authToken(TestConstants.USER_HARWELLM)))
                .andExpect(status().isOk());
        
    }
}
