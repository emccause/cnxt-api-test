package com.maritz.culturenext.batch.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class BatchFileRestServiceTest extends AbstractRestTest{

    @Test
    public void getInfoForGoodId() throws Exception {
        mockMvc.perform(
                get("/rest/files/102")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void getInfoForNonEnrollmentId() throws Exception {
        mockMvc.perform(
                get("/rest/files/2")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void getInfoForBadId() throws Exception {
        mockMvc.perform(
                get("/rest/files/99999999")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void getBatchErrorsByBatchGood() throws Exception {
        Long batchId = 16397L;
        mockMvc.perform(get("/rest/files/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))).andExpect(status().isOk());
    }

    @Test
    public void getBatchErrorsByBatchBadId() throws Exception {
        Long batchId = -1L;
        mockMvc.perform(
                get("/rest/files/" + batchId + "/errorlog")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void getErrorsByBatchIdMissingBatchFile() throws Exception {
        Long batchId = 4722L;
        mockMvc.perform(
                get("/rest/files/" + batchId + "/errorlog")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void getPointLoadErrors_batchId_as_string() throws Exception {
        mockMvc.perform(
                get("/rest/files/batchIdString/errorlog")
                        .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
        ).andExpect(status().isNotFound());
    }
    
    @Test
    public void test_non_admin() throws Exception {
        mockMvc.perform(
                get("/rest/files/102")
                        .with(authToken(TestConstants.USER_PORTERGA))
        ).andExpect(status().isForbidden());

        Long batchId = 16397L;
        mockMvc.perform(get("/rest/files/" + batchId + "/errorlog")
                .with(authToken(TestConstants.USER_PORTERGA))).andExpect(status().isForbidden());
        
    }
}
