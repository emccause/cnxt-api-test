package com.maritz.culturenext.batch.util;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.BatchConstants;
import com.maritz.culturenext.batch.dto.BatchStatusDTO;
import com.maritz.test.AbstractMockTest;

public class BatchUtilUnitTest extends AbstractMockTest {
    @InjectMocks BatchUtil batchUtil;

    @Mock ConcentrixDao<Batch> batchDao;

    @Test
    public void test_validateBatchStatusDto_happyPath() {
        Long batchId = 11111L;
        String oldStatus = StatusTypeCode.PENDING_RELEASE.name();
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(batchId);
        batchStatusDTO.setStatus(StatusTypeCode.QUEUED.name());

        boolean success = true;
        Batch batch = null;
        try {
            PowerMockito.when(batchDao.findById(11111L))
                .thenReturn(new Batch().setBatchId(batchId).setStatusTypeCode(oldStatus));
            batch = batchUtil.validateBatchStatusDto(batchStatusDTO);
        } catch (Exception ex) {
            success = false;
        }

        Assert.assertTrue(success);
        Assert.assertNotNull(batch);
        Assert.assertEquals(batchId, batch.getBatchId());
        Assert.assertEquals(oldStatus, batch.getStatusTypeCode());
    }

    @Test
    public void test_validateBatchStatusDto_status_missing() {
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(11111L);
        batchStatusDTO.setStatus(null);

        boolean success = false;
        try {
            PowerMockito.when(batchDao.findById(11111L))
                .thenReturn(new Batch().setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name()));
            batchUtil.validateBatchStatusDto(batchStatusDTO);
        } catch (ErrorMessageException ex) {
            if (ex.getErrorMessages().contains(BatchConstants.STATUS_MISSING_MESSAGE)) {
                success = true;
            }
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_validateBatchStatusDto_status_notUpdateable() {
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(11111L);
        batchStatusDTO.setStatus(StatusTypeCode.QUEUED.name());

        boolean success = false;
        try {
            PowerMockito.when(batchDao.findById(11111L))
                .thenReturn(new Batch().setStatusTypeCode(StatusTypeCode.APPROVED.name()));

            batchUtil.validateBatchStatusDto(batchStatusDTO);
        } catch (ErrorMessageException ex) {
            if (ex.getErrorMessages().contains(BatchConstants.STATUS_NOT_UPDATEABLE_MESSAGE)) {
                success = true;
            }
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_validateBatchStatusDto_status_invalidOption() {
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(11111L);
        batchStatusDTO.setStatus(StatusTypeCode.ACCEPTED.name());

        boolean success = false;
        try {
            PowerMockito.when(batchDao.findById(11111L))
                .thenReturn(new Batch().setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name()));
            batchUtil.validateBatchStatusDto(batchStatusDTO);
        } catch (ErrorMessageException ex) {
            if (ex.getErrorMessages().contains(BatchConstants.STATUS_NOT_ALLOWED_MESSAGE)) {
                success = true;
            }
        }

        Assert.assertTrue(success);
    }

    @Test
    public void test_validateBatchStatusDto_batch_missing() {
        BatchStatusDTO batchStatusDTO = new BatchStatusDTO();
        batchStatusDTO.setBatchId(11111L);
        batchStatusDTO.setStatus(StatusTypeCode.QUEUED.name());

        boolean success = false;
        try {
            PowerMockito.when(batchDao.findById(11111L)).thenReturn(null);
            batchUtil.validateBatchStatusDto(batchStatusDTO);
        } catch (ErrorMessageException ex) {
            if (ex.getErrorMessages().contains(BatchConstants.BATCH_NOT_FOUND_MESSAGE)) {
                success = true;
            }
        }

        Assert.assertTrue(success);
    }
}
