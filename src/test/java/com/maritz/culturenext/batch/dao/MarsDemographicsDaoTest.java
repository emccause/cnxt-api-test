package com.maritz.culturenext.batch.dao;

import java.util.List;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;

import com.maritz.culturenext.batch.dto.MarsDemographicsDto;
import com.maritz.test.AbstractDatabaseTest;

public class MarsDemographicsDaoTest extends AbstractDatabaseTest {

    @Inject private MarsDemographicsDao marsDemographicsDao;
    
    @Test
    public void testGetDemographicsUpdate() {
        List<MarsDemographicsDto> demographicsList = marsDemographicsDao.getDemographicsUpdate(1, 10000);
        Assert.assertNotNull("demographicsList was null and should not!", demographicsList);
        Assert.assertEquals("Different number of expected elements", 10000, demographicsList.size());
    }

}
