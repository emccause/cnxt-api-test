package com.maritz.culturenext.batch.service;

import static com.maritz.DbRecordUtil.verifyTransactionStatusChanges;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.BatchEvent;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.dto.PointLoadActivityUpdateRequestDTO;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.PointLoadActivityService;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.BatchEventType;
import com.maritz.test.AbstractDatabaseTest;

import static org.junit.Assert.*;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class PointLoadActivityServiceTest extends AbstractDatabaseTest {
    

    @Inject private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Inject private PointLoadActivityService pointLoadActivityService;
    @Inject private ConcentrixDao<BatchEvent> batchEventDao;
    private List<BatchEvent> batchEvents;

    private static final Long INVALID_POINT_LOAD_BATCH_ID = 999992342383L;
    private static final Long VALID_NON_POINT_LOAD_BATCH_ID = 4436L;
    private static final Long VALID_POINT_LOAD_BATCH_ID = 4722L;
    private static final Long VALID_POINT_LOAD_ARCHIVED_BATCH_ID = 16401L;
    private static final Long BATCH_WITH_REPEATED_BATCH_EVENT = 23538L;

    private static Long VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID;
    private static final String VALID_POINT_LOAD_PENDING_RELEASE_BATCH_FILE_NAME = "Point Load - Pending Release.csv";

    private static Long VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_ID;
    private static final String VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_FILE_NAME = "Point Load - Budget Exceeded.csv";

    // error codes
    private static final String ERROR_FROM_DATE_IN_FUTURE = "FROM_DATE_IN_FUTURE";
    private static final String ERROR_INVALID_STATUS = "INVALID_STATUS";
    private static final String ERROR_MISSING_UPDATE_REQUEST = "MISSING_UPDATE_REQUEST";
    private static final String ERROR_MISSING_BATCH_ID = "MISSING_BATCH_ID";
    private static final String ERROR_INVALID_BATCH_ID = "INVALID_BATCH_ID";
    private static final String ERROR_MISSING_ACTION = "MISSING_ACTION";
    private static final String ERROR_INVALID_ACTION = "INVALID_ACTION";
    private static final String ERROR_INVALID_ACTION_TRANSITION = "INVALID_ACTION_TRANSITION";
    private static final String ERROR_NON_POINT_LOAD_BATCH = "NON_POINT_LOAD_BATCH";

    // valid actions
    private static final String CANCEL_ACTION = "CANCEL";
    private static final String RELEASE_ACTION = "RELEASE";
    private static final String REPROCESS_ACTION = "REPROCESS";

    // validation tests
    
    @Before
    public void setup() {
        VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID = batchEventDao.findBy()
            .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.FILE_NAME.getCode())
            .and(ProjectConstants.MESSAGE).eq(VALID_POINT_LOAD_PENDING_RELEASE_BATCH_FILE_NAME)
            .findOne(ProjectConstants.BATCH_ID, Long.class);
        
        VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_ID = batchEventDao.findBy()
                .where(ProjectConstants.BATCH_EVENT_TYPE_CODE).eq(BatchEventType.FILE_NAME.getCode())
                .and(ProjectConstants.MESSAGE).eq(VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_FILE_NAME)
                .findOne(ProjectConstants.BATCH_ID, Long.class);
        
        
        batchEvents = 
                batchEventDao.findBy().where(ProjectConstants.BATCH_ID).eq(BATCH_WITH_REPEATED_BATCH_EVENT).find();
        int countExistingEndTimEvents = 0;
        for (BatchEvent batchEvent: batchEvents){
            if (BatchEventType.END_TIME.getCode().equals(batchEvent.getBatchEventTypeCode().trim())) {
                ++countExistingEndTimEvents;
            }
        }
        assertEquals("Number of End Time Batch Events were different from expectd", 2, countExistingEndTimEvents);
    }

    @Test
    public void testGetPointLoadActivityFromDateInFuture() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_FROM_DATE_IN_FUTURE);

        Date futureFromDate = new Date(System.currentTimeMillis() + 1000000000000L);

        // default status but future fromDate
        verifyGetPointLoadActivityErrors(futureFromDate, null, expectedErrors);
    }

    @Test
    public void testGetPointLoadActivityInvalidStatus() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_INVALID_STATUS);

        // default fromDate but invalid status
        verifyGetPointLoadActivityErrors(null, "INVALID", expectedErrors);
    }

    // basic runthrough tests

    // basic sanity check, make sure that there are records though (only goes back 6 months)
    @Test
    public void testGetPointLoadActivityDefaultFromDateAndStatuses() {
        List<PointLoadBatchFileInfoDTO> pointLoadActivities =
                pointLoadActivityService.getPointLoadActivity(null, null);

        assertFalse(pointLoadActivities.isEmpty());
    }

    @Test
    public void testGetPointLoadActivityOldFromDateAndDefaultStatuses() {
        // determine fromDate
        Date oldDate = TestUtil.generateOldDate();
        List<PointLoadBatchFileInfoDTO> pointLoadActivities = 
                pointLoadActivityService.getPointLoadActivity(oldDate, null);

        assertFalse(pointLoadActivities.isEmpty());
    }

    @Test
    public void testGetPointLoadActivityOldFromDateAndNonDefaultStatuses() {
        // determine fromDate
        Date oldDate = TestUtil.generateOldDate();

        // determine statuses
        String statusString = "SUCCESS,COMPLETE,ARCHIVED,LOCKED";

        List<PointLoadBatchFileInfoDTO> pointLoadActivities =
                pointLoadActivityService.getPointLoadActivity(oldDate, statusString);

        assertFalse(pointLoadActivities.isEmpty());
    }

    @Test
    public void testGetPointLoadActivityOldFromDateInProgressStatusAdjust() {
        // determine fromDate
        Date oldDate = TestUtil.generateOldDate();

        // determine statuses that will be adjusted later
        String statusString = "NEW,QUEUED,PENDING_RELEASE";

        List<PointLoadBatchFileInfoDTO> pointLoadActivities =
                pointLoadActivityService.getPointLoadActivity(oldDate, statusString);

        assertFalse(pointLoadActivities.isEmpty());

        for(PointLoadBatchFileInfoDTO pointLoadActivity : pointLoadActivities) {
            assertTrue(StatusTypeCode.PENDING_RELEASE.name().equals(pointLoadActivity.getStatus()) ||
                    StatusTypeCode.IN_PROGRESS.name().equals(pointLoadActivity.getStatus()));
        }
    }

    @Test
    public void testUpdatePointLoadActivityMissingRequest() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_MISSING_UPDATE_REQUEST);

        // update with missing request
        verifyUpdatePointLoadActivityErrors(null, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityMissingBatchId() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_MISSING_BATCH_ID);

        // set up request with missing batch ID
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(null, CANCEL_ACTION);

        // update with request
        verifyUpdatePointLoadActivityErrors(request, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityInvalidBatchId() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_INVALID_BATCH_ID);

        // set up request with invalid batch ID
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(INVALID_POINT_LOAD_BATCH_ID, CANCEL_ACTION);

        // update with request
        verifyUpdatePointLoadActivityErrors(request, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityNonPointLoadBatch() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_NON_POINT_LOAD_BATCH);

        // set up request with missing batch ID
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_NON_POINT_LOAD_BATCH_ID, CANCEL_ACTION);

        // update with request
        verifyUpdatePointLoadActivityErrors(request, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityMissingAction() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_MISSING_ACTION);

        // set up request with missing action
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_POINT_LOAD_BATCH_ID, null);

        // update with request
        verifyUpdatePointLoadActivityErrors(request, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityInvalidAction() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_INVALID_ACTION);

        // set up request with invalid action
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_POINT_LOAD_BATCH_ID, "STOP");

        // update with request
        verifyUpdatePointLoadActivityErrors(request, expectedErrors);
    }

    @Test
    public void testUpdatePointLoadActivityInvalidActionTransitions() {
        // set up expected errors
        List<String> expectedErrors = Arrays.asList(ERROR_INVALID_ACTION_TRANSITION);

        String[] actions = new String[] {CANCEL_ACTION,RELEASE_ACTION,REPROCESS_ACTION};

        // verify action transition for each of the different actions
        for(String action : actions) {
            // set up request with valid action/batch but invalid status transition
            PointLoadActivityUpdateRequestDTO request =
                    generatePointLoadUpdateRequest(VALID_POINT_LOAD_ARCHIVED_BATCH_ID, action);

            // update with request
            verifyUpdatePointLoadActivityErrors(request, expectedErrors);
        }
    }

    // need to specify valid Point-Load batch and correct action
    @Test
    public void testUpdatePointLoadActivityCancel() {
        // set up request with valid action/batch
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID, CANCEL_ACTION);

        assertEquals("Batch Id different from expected", BATCH_WITH_REPEATED_BATCH_EVENT,request.getBatchId());

        PointLoadBatchFileInfoDTO response =
                pointLoadActivityService.updatePointLoadActivity(request);

        verifyPointLoadActivityUpdateResponse(response,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_FILE_NAME,
                1,0,1,1,StatusTypeCode.CANCELLED.name());

        verifyTransactionStatusChanges(transactionHeaderDao,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID,
                StatusTypeCode.REJECTED.name());
    }

    // need to specify valid Point-Load batch and correct action
    @Test
    public void testUpdatePointLoadActivityRelease() {
        // set up request with valid action/batch
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID, RELEASE_ACTION);

        assertEquals("Batch Id different from expected", BATCH_WITH_REPEATED_BATCH_EVENT,request.getBatchId());
        PointLoadBatchFileInfoDTO response =
                pointLoadActivityService.updatePointLoadActivity(request);

        verifyPointLoadActivityUpdateResponse(response,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_FILE_NAME,
                1,0,1,1,StatusTypeCode.COMPLETE.name());

        verifyTransactionStatusChanges(transactionHeaderDao,
                VALID_POINT_LOAD_PENDING_RELEASE_BATCH_ID,
                StatusTypeCode.APPROVED.name());
    }

    @Test
    public void testUpdatePointLoadActivityReprocess() {
        // set up request with valid action/batch
        PointLoadActivityUpdateRequestDTO request =
                generatePointLoadUpdateRequest(VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_ID, REPROCESS_ACTION);

        PointLoadBatchFileInfoDTO response =
                pointLoadActivityService.updatePointLoadActivity(request);

        verifyPointLoadActivityUpdateResponse(response,
                VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_ID,
                VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_FILE_NAME,
                1,0,1,1,StatusTypeCode.IN_PROGRESS.name());

        verifyTransactionStatusChanges(transactionHeaderDao,
                VALID_POINT_LOAD_BUDGET_EXCEEDED_BATCH_ID,
                StatusTypeCode.PENDING.name());
    }

    // helper methods
    /**
     * Verifies errors during retrieval of Point Load activities.
     *
     * @param fromDate start date of retrieval date range
     * @param statuses statuses to filter activities by
     * @param expectedErrorCodes expected validation error codes from failed retrieval
     */
    private void verifyGetPointLoadActivityErrors(Date fromDate,
                                                  String statuses,
                                                  @Nonnull List<String> expectedErrorCodes) {
        try {
            pointLoadActivityService.getPointLoadActivity(fromDate, statuses);
            fail();
        } catch(ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    /**
     * Verifies errors during update of the Point Load activity.
     *
     * @param request request
     * @param expectedErrorCodes expected validation error codes from failed update
     */
    private void verifyUpdatePointLoadActivityErrors(PointLoadActivityUpdateRequestDTO request,
                                                     @Nonnull List<String> expectedErrorCodes) {
        try {
            pointLoadActivityService.updatePointLoadActivity(request);
            fail();
        } catch(ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        }
    }

    /**
     * Generates a Point Load update request with the specified action on the specified batch.
     *
     * @param batchId batch ID to update
     * @param action action to perform
     * @return request DTO with the specified action and batch
     */
    private PointLoadActivityUpdateRequestDTO generatePointLoadUpdateRequest(Long batchId,
                                                String action) {
        PointLoadActivityUpdateRequestDTO pointLoadActivityUpdateRequestDTO =
                new PointLoadActivityUpdateRequestDTO();

        pointLoadActivityUpdateRequestDTO.setBatchId(batchId);
        pointLoadActivityUpdateRequestDTO.setAction(action);

        return pointLoadActivityUpdateRequestDTO;
    }

    /**
     * Verifies the Point Load activity response with the expected values.
     *
     * @param response response
     * @param expectedBatchId expected batch ID
     * @param expectedFileName expected file name
     * @param expectedTotalRecords expected total record count
     * @param expectedErrorRecords expected error record count
     * @param expectedGoodRecords expected good record count
     * @param expectedTotalPoints expected total point allocation
     * @param expectedStatus expected status
     */
    private void verifyPointLoadActivityUpdateResponse(PointLoadBatchFileInfoDTO response,
                                                       Long expectedBatchId,
                                                       String expectedFileName,
                                                       Integer expectedTotalRecords,
                                                       Integer expectedErrorRecords,
                                                       Integer expectedGoodRecords,
                                                       Integer expectedTotalPoints,
                                                       String expectedStatus) {
        assertEquals(expectedBatchId, response.getBatchId());
        assertEquals(expectedFileName, response.getFileName());
        assertEquals(expectedTotalRecords, response.getTotalRecords());
        assertEquals(expectedErrorRecords, response.getErrorRecords());
        assertEquals(expectedGoodRecords, response.getGoodRecords());
        assertEquals(expectedTotalPoints, response.getTotalPoints());
        assertEquals(expectedStatus, response.getStatus());
    }
}
