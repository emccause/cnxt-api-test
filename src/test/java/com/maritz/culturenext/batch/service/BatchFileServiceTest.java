package com.maritz.culturenext.batch.service;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.service.BatchFileService;
import com.maritz.test.AbstractDatabaseTest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;

public class BatchFileServiceTest extends AbstractDatabaseTest {

    @Inject private BatchFileService batchFileService;

    private MockHttpServletResponse response = new MockHttpServletResponse();

    @Test
    public void test_validateBatchIdTypeAndStatus_happy_path() {
        Long batchId = 102L;
        try {
            batchFileService.generateBatchFile(response, batchId);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_null_batchId() {
        Long batchId = null;
        try {
            batchFileService.generateBatchFile(response, batchId);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_validateBatchIdTypeAndStatus_wrong_batchId() {
        Long batchId = -1L;
        try {
            batchFileService.generateBatchFile(response, batchId);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }
}
