package com.maritz.culturenext.batch.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.BatchFileTemplate;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.batch.service.impl.RecognitionUploadServiceImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.AdvancedRecognitionFileContentDTO;
import com.maritz.test.AbstractMockTest;

public class RecognitionUploadServiceTest extends AbstractMockTest {

    @InjectMocks private RecognitionUploadServiceImpl recognitionUploadService;
    @Mock private ConcentrixDao<BatchFileTemplate> batchFileTemplateDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ParticipantInfoDao participantDao;

    @SuppressWarnings("unchecked")
    private FindBy<Pax> paxFindBy = PowerMockito.mock(FindBy.class);

    private static final String CONTROL_NUM_1 = "100";

    private static final String FILE_NAME_TEST = "Recognition_template";
    private static final String FILE_CONTENT_TEST =
            "Participant First Name,Participant Last Name,Participant ID - Mandatory,Award Amount\n"
            + "Andy,Kilinskis (Admin),141,5\n"
            + "Alex,Mudd,8987,10\n"
            + "Omar,MadridOntiveros,12289,5";

    private MultipartFile fileTest;
    private EmployeeDTO employee1;

    @Before
    public void setup() {
        PowerMockito.when(paxDao.findBy()).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.where(anyString())).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.eq(anyString())).thenReturn(paxFindBy);
        PowerMockito.when(paxFindBy.findOne(ProjectConstants.PAX_ID, Long.class)).thenReturn(TestConstants.ID_1);

        // Pax object test data
        employee1 = new EmployeeDTO();
        employee1.setPaxId(TestConstants.ID_1);
        employee1.setControlNum(CONTROL_NUM_1);
        employee1.setEligibleReceiverInProgram(Boolean.TRUE);

        PowerMockito.when(participantDao.getParticipantInfo(anyLong(),anyLong())).thenReturn(employee1);

        // Populate file content data

        fileTest = new MockMultipartFile(FILE_NAME_TEST, FILE_NAME_TEST,
                ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.get(0), FILE_CONTENT_TEST.getBytes());
    }

    @Test
    public void test_uploadAdvancedRecognitionFile_success(){

        List<AdvancedRecognitionFileContentDTO> contentObject =
                recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        assertThat(contentObject.size(), equalTo(3));
        assertNotNull(contentObject.get(0).getPax());
        assertNotNull(contentObject.get(1).getPax());
        assertNotNull(contentObject.get(2).getPax());
    }

    @Test
    public void test_uploadAdvancedRecognitionFile_success_invalid_pax_record(){

        PowerMockito.when(participantDao.getParticipantInfo(anyLong(),anyLong())).thenReturn(employee1,null,employee1);

        List<AdvancedRecognitionFileContentDTO> contentObject =
                recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        assertThat(contentObject.size(), equalTo(3));
        assertNotNull(contentObject.get(0).getPax());
        assertNull(contentObject.get(1).getPax());
        assertNotNull(contentObject.get(2).getPax());
    }

    @Test
    public void test_uploadAdvancedRecognitionFile_success_all_invalid_pax_record(){

        PowerMockito.when(participantDao.getParticipantInfo(anyLong(),anyLong())).thenReturn(null);

        List<AdvancedRecognitionFileContentDTO> contentObject =
                recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        assertThat(contentObject.size(), equalTo(3));
        assertNull(contentObject.get(0).getPax());
        assertNull(contentObject.get(1).getPax());
        assertNull(contentObject.get(2).getPax());
    }

    @Test
    public void test_uploadAdvancedRecognitionFile_success_empty_file(){

        // Empty file content data
        String emptyFileContent =
                "ParticipantName,ParticipantLastName,InvalidColumn,ParticipantID";

        fileTest = new MockMultipartFile(FILE_NAME_TEST, FILE_NAME_TEST,
                ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.get(0), emptyFileContent.getBytes());

        List<AdvancedRecognitionFileContentDTO> contentObject =
                recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        assertThat(contentObject.size(), equalTo(0));

    }

    @Test(expected = ErrorMessageException.class)
    public void test_uploadAdvancedRecognitionFile_fail_file_data_invalid(){

        // Invalid file content type
        fileTest = new MockMultipartFile(FILE_NAME_TEST, FILE_NAME_TEST,"text/plain", FILE_CONTENT_TEST.getBytes());

        recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        fail("Error is expected, this test case must fail");
    }

    @Test(expected = ErrorMessageException.class)
    public void test_uploadAdvancedRecognitionFile_fail_file_type_invalid(){

        // Invalid file content data
        String invalidFileContent =
                "ParticipantName,ParticipantLastName,InvalidColumn,ParticipantID\n"
                + "Andy,Kilinskis (Admin),141,5,1\n"
                + "Alex,Mudd,8987,10,1\n"
                + "Omar,MadridOntiveros,12289,5,1";

        fileTest = new MockMultipartFile(FILE_NAME_TEST, FILE_NAME_TEST,
                ProjectConstants.SUPPORTED_UPLOAD_FILES_TYPES.get(0), invalidFileContent.getBytes());

        recognitionUploadService.uploadAdvancedRecognitionFile(TestConstants.ID_1, fileTest);

        fail("Error is expected, this test case must fail");
    }


}
