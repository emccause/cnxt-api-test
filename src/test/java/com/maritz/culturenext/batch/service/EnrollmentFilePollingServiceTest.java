package com.maritz.culturenext.batch.service;

import com.maritz.core.dto.FileDTO;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.services.FileService;
import com.maritz.core.services.TriggeredTaskService;
import com.maritz.culturenext.enrollment.batch.EnrollmentFilePollingJob;
import com.maritz.test.AbstractMockTest;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

public class EnrollmentFilePollingServiceTest extends AbstractMockTest {
	@Mock private FileService fileService;
	@Mock private TriggeredTaskService triggeredTaskService;

	@InjectMocks private EnrollmentFilePollingJob enrollmentFilePollingJob;

	@Test
	public void testEnrollmentFilePollingJobBatch() throws Exception {
		List<FileDTO> files = new ArrayList<>();
		FileDTO file = new FileDTO();
		file.setIsDirectory(false);
		file.setLastModified(new Date());
		file.setName("enrollmentLoad_file.csv");
		files.add(file);

		Mockito.when(fileService.getFiles(anyString())).thenReturn(files);

		Batch batch = new Batch();
		batch.setBatchId(1L);
		enrollmentFilePollingJob.enrollmentFilePollingJob(batch);
		Mockito.verify(triggeredTaskService, times(1)).runTriggeredTaskAsync("enrollmentFileJob");
	}
}
