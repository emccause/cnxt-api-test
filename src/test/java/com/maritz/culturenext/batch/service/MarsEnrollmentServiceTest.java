package com.maritz.culturenext.batch.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.core.env.Environment;

import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.repository.PaxAccountRepository;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.core.services.BatchService;
import com.maritz.core.util.PropertiesUtils;
import com.maritz.culturenext.batch.dao.MarsDemographicsDao;
import com.maritz.culturenext.batch.dto.MarsDemographicsDto;
import com.maritz.culturenext.batch.service.impl.MarsEnrollmentServiceImpl;
import com.maritz.mars.services.MarsException;
import com.maritz.mars.services.MarsParticipantService;
import com.maritz.test.AbstractMockTest;
import com.maritzrewardservices.mars.Header;
import com.maritzrewardservices.mars.ServiceResponse;
import com.maritzrewardservices.mars.ServiceStatus;
import com.microsoft.schemas.serialization.arrays.ArrayOfstring;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest(PropertiesUtils.class)
public class MarsEnrollmentServiceTest extends AbstractMockTest {

    private static final String PROPERTY_PREFIX = "triggeredTask.marsEnrollmentUpdateJob";
    static {
        System.setProperty(PROPERTY_PREFIX + ".recordsPerIteration", "10");
    }

    @Mock private ApplicationDataService applicationDataService;
    @Mock private BatchService batchService;
    @Mock private Environment environment;
    @Mock private MarsDemographicsDao marsEnrollmentUpdateDao;
    @Mock private MarsParticipantService marsParticipantService;
    @Mock private PaxAccountRepository paxAccountRepository;
    
    @InjectMocks private MarsEnrollmentServiceImpl marsEnrollmentService;
    
    @Before
    public void setup() {
        initMocks();
        List<MarsDemographicsDto> resultList = new ArrayList<MarsDemographicsDto>();
        resultList.add(new MarsDemographicsDto()
                .setCardTypeCode("SAA")
                .setPaxId(14080L)
                .setControlNum("14080")
                .setProjectNumber("S20134")
                .setSubProjectNumber("Maritz1")
                .setLastName("Keller")
                .setFirstName("Paul")
                .setEmailAddress("a@b.c"));
        Mockito.when(marsEnrollmentUpdateDao.getDemographicsUpdate(Matchers.anyInt(), Matchers.anyInt())).thenReturn(resultList);

        marsEnrollmentService = new MarsEnrollmentServiceImpl()
                .setApplicationDataService(applicationDataService)
                .setBatchService(batchService)
                .setEnvironment(environment)
                .setMarsEnrollmentUpdateDao(marsEnrollmentUpdateDao)
                .setMarsParticipantService(marsParticipantService)
                .setPaxAccountRepository(paxAccountRepository);
        
        Mockito.when(environment.getProperty("triggeredTask.marsEnrollmentUpdateJob.recordsPerIteration", Integer.class)).thenReturn(new Integer(10));
        List<String> errorCodes = new ArrayList<String>();
        errorCodes.add("506");
        PowerMockito.mockStatic(PropertiesUtils.class);
        Mockito.when(PropertiesUtils.getCsvList(Mockito.anyObject(),
                Mockito.anyString())).thenReturn(errorCodes);
    }
    
    @Test
    public void testMarsEnrollmentUpdateJobBatch() {
        assertNotNull("mars enrollment service was null and should not!",marsEnrollmentService);
        assertNotNull("environment was null and should not!", environment);
        
        assertNotNull("marsEnrollmentUpdateDao was null and should not!",marsEnrollmentUpdateDao);
        
        logger.info(PROPERTY_PREFIX + ".recordsPerIteration is present:" + 
                environment.getProperty(PROPERTY_PREFIX + ".recordsPerIteration"));
        
        logger.info(PROPERTY_PREFIX + ".recordsPerIteration whole name is present:" + 
                environment.getProperty("triggeredTask.marsEnrollmentUpdateJob.recordsPerIteration"));

        Batch batch = new Batch();
        batch.setBatchId(1L);
        marsEnrollmentService.marsEnrollmentUpdateJob(batch);
    }
    
    @Test(expected=MarsException.class)
    public void testMarsEnrollmentUpdateJobBatchWithExceptionMarsExceptionWResponseCode() {        
        ServiceResponse serviceResponse = new ServiceResponse(); 
        ServiceStatus serviceValueStatus = new ServiceStatus();
        serviceValueStatus.setStatusCode(new Long(506L));
        ArrayOfstring message = new ArrayOfstring();
        serviceValueStatus.setDetailedMessage(message);
        serviceResponse.setStatus(serviceValueStatus);
        Header header = new Header();
        header.setTransactionId("Transaction Id");
        serviceResponse.setHeader(header );
        Mockito.when(marsParticipantService
            .createParticipantAccount(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString(), 
                Mockito.anyString(), Mockito.anyObject())).thenThrow(new MarsException(serviceResponse));
        
        Batch batch = new Batch();
        batch.setBatchId(1L);
        marsEnrollmentService.marsEnrollmentUpdateJob(batch);
    }
    
    @Test
    public void testMarsEnrollmentUpdateJobBatchWithExceptionMarsExceptionWOResponseCode() {
        
        ServiceResponse serviceResponse = new ServiceResponse(); 
        ServiceStatus serviceValueStatus = new ServiceStatus();
        serviceValueStatus.setStatusCode(new Long(666L));
        ArrayOfstring message = new ArrayOfstring();
        serviceValueStatus.setDetailedMessage(message);
        serviceResponse.setStatus(serviceValueStatus);
        Header header = new Header();
        header.setTransactionId("Transaction Id");
        serviceResponse.setHeader(header );
        Mockito.when(marsParticipantService
            .createParticipantAccount(Mockito.anyObject(), Mockito.anyString(), Mockito.anyString(), 
                Mockito.anyString(), Mockito.anyObject())).thenThrow(new MarsException(serviceResponse));
        
        Batch batch = new Batch();
        batch.setBatchId(1L);
        marsEnrollmentService.marsEnrollmentUpdateJob(batch);
    }

}
