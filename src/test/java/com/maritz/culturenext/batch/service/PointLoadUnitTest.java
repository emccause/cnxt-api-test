package com.maritz.culturenext.batch.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.context.ApplicationEventPublisher;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Batch;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.batch.PointLoadUpdateAction;
import com.maritz.culturenext.batch.dao.PointLoadActivityDao;
import com.maritz.culturenext.batch.dto.PointLoadActivityUpdateRequestDTO;
import com.maritz.culturenext.batch.dto.PointLoadBatchFileInfoDTO;
import com.maritz.culturenext.batch.service.impl.PointLoadActivityServiceImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.email.event.PointLoadEmailEvent;
import com.maritz.culturenext.enums.BatchType;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.test.AbstractMockTest;

public class PointLoadUnitTest extends AbstractMockTest {

    @InjectMocks private PointLoadActivityServiceImpl pointLoadActivityService;
    @Mock private ApplicationEventPublisher applicationEventPublisher;
    @Mock private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    @Mock private ConcentrixDao<Batch> batchDao;
    @Mock private PointLoadActivityDao pointLoadActivityDao;

    private static Long batchId = 23538L; //PENDING_RELEASE

    @Before
    public void setup() throws Exception {
        Batch batch = new Batch();
        batch.setBatchId(batchId);
        batch.setBatchTypeCode(BatchType.POINT_LOAD.getCode());
        batch.setStatusTypeCode(StatusTypeCode.PENDING_RELEASE.name());
        PowerMockito.when(batchDao.findById(anyLong())).thenReturn(batch);

        PointLoadBatchFileInfoDTO batchFile = new PointLoadBatchFileInfoDTO();
        batchFile.setBatchId(batchId);
        batchFile.setStatus(StatusTypeCode.COMPLETE.name());
        PowerMockito.when(pointLoadActivityDao.updatePointLoadActivity(anyLong(), any(PointLoadUpdateAction.class))).thenReturn(batchFile);

        PowerMockito.when(cnxtProgramMiscRepository.findNotificationRecipient(anyLong())).thenReturn(ProjectConstants.TRUE);
    }

    @Test
    public void test_publish_email_event() throws Exception {

        //TODO MP-7593 - rewrite this test to test the whole process of releasing points - not just the email sending

        PointLoadActivityUpdateRequestDTO pointLoadDto = new PointLoadActivityUpdateRequestDTO();
        pointLoadDto.setAction(PointLoadUpdateAction.RELEASE.name());
        pointLoadDto.setBatchId(batchId);

        pointLoadActivityService.updatePointLoadActivity(pointLoadDto);

        Mockito.verify(applicationEventPublisher, times(1)).publishEvent(any(PointLoadEmailEvent.class));
    }
}
