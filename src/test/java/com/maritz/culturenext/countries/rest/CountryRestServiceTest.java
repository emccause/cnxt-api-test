package com.maritz.culturenext.countries.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class CountryRestServiceTest extends AbstractRestTest {

    private static final String URL_COUNTRIES = "/rest/countries";
    
    @Test
    public void test_getCountriesData_happy_path() throws Exception {
        mockMvc.perform(
                get(URL_COUNTRIES)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk());
        
    }
    
    @Test
    public void test_getCountriesData_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(URL_COUNTRIES)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_PORTERGA))
            )
            .andExpect(status().isForbidden())
        ;
    }
}
