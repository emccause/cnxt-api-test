package com.maritz.culturenext.countries.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.maritz.culturenext.countries.service.impl.CountryServiceImpl;
import com.maritz.test.AbstractDatabaseTest;

public class CountryServiceTest extends AbstractDatabaseTest {

    @Autowired private CountryServiceImpl countryServiceImpl;
    
    @Test
    public void testGetCountries() throws Exception {
        assertNotNull(countryServiceImpl);
        assertNotNull(countryServiceImpl.getCountriesData());
    }
}
