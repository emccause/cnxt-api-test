package com.maritz.culturenext.countries.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.maritz.culturenext.countries.dao.impl.CountryDaoImpl;
import com.maritz.test.AbstractMockTest;

public class CountryDaoTest extends AbstractMockTest {

    @Mock CountryDaoImpl countryDaoImpl;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCountriesTest(){
        assertNotNull(this.countryDaoImpl.getCountriesData());
    }
}
