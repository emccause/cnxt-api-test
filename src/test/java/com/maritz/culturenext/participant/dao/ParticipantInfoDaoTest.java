package com.maritz.culturenext.participant.dao;

import com.maritz.TestUtil;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class ParticipantInfoDaoTest extends AbstractDatabaseTest {

    @Inject private ParticipantInfoDao participantInfoDao;

    private static final String STATUS_LIST = "ACTIVE";
    private static final String TYPE_LIST = "ENROLLMENT";
    private static final String ENROLLMENT_STATUS_ACTIVE = "ACTIVE";
    private static final String ENROLLMENT_STATUS_INACTIVE = "INACTIVE";


    private final Integer PAGE_NUMBER = 1;
    private final Integer PAGE_SIZE = 20;
    private final String CONTROL_NUMBER = "8987";

    @Test
    public void test_groups_by_participant() {

        List<Map<String, Object>> groupsByParticipantEntries =
                participantInfoDao.getGroupsByParticipant(TestConstants.PAX_KUSEY_ADMIN,
                        STATUS_LIST, TYPE_LIST, ProjectConstants.DEFAULT_PAGE_NUMBER,
                        ProjectConstants.DEFAULT_PAGE_SIZE);

        assertNotNull("Should get data back back from groups by participant query", groupsByParticipantEntries);
        assertFalse("Data should not be empty from groups by participant query", groupsByParticipantEntries.isEmpty());


    }

    @Test
    public void test_groups_by_participant_negative_page_number() {

        try {
            participantInfoDao.getGroupsByParticipant(TestConstants.PAX_KUSEY_ADMIN,
                    STATUS_LIST, TYPE_LIST, -1, ProjectConstants.DEFAULT_PAGE_SIZE);
            fail("Should have thrown an error");
        } catch (ErrorMessageException eme) {
            TestUtil.assertValidationErrors(1, "PAGE_NUMBER_INVALID", eme);
        }

    }

    @Test
    public void test_participant_overview() {
        ParticipantDTO getPaxOverview =
                participantInfoDao.getParticipantOverview("847");

        assertNotNull("Should get data back back from participant query", getPaxOverview);
        assertEquals(getPaxOverview.getFirstName(), "Greg");
        assertEquals(getPaxOverview.getStatus(), "ACTIVE");
        assertEquals(getPaxOverview.getManagerControlNumber(), "12671");
        assertEquals(getPaxOverview.getAddress().getPostalCode(), "DUBLIN 7");
        assertEquals(getPaxOverview.getAddress().getCity(), "Dublin");
        assertFalse(getPaxOverview.getAddress().getAddressLines().isEmpty());
        assertEquals(getPaxOverview.getCostCenter(), "CC_GREG");
        assertEquals(getPaxOverview.getArea(), "AREA_1");
    }

    @Test
    public void test_participant_overview_2() {
        ParticipantDTO getPaxOverview =
                participantInfoDao.getParticipantOverview("4");

        assertNotNull("Should get data back back from participant query", getPaxOverview);
        assertEquals(getPaxOverview.getFirstName(), "John");
        assertEquals(getPaxOverview.getStatus(), "ACTIVE");
        assertEquals(getPaxOverview.getManagerControlNumber(), "5");
        assertEquals(getPaxOverview.getEmailAddress(), "a@b.c");
        assertEquals(getPaxOverview.getCostCenter(), null);
        assertEquals(getPaxOverview.getArea(), null);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void test_participant_overview_not_found() {
        ParticipantDTO getPaxOverview =
                participantInfoDao.getParticipantOverview("156641");
        fail("Should have thrown an error");
    }

    @Test
    public void test_email_history_search_controlNumber_param(){
        assertNotNull(participantInfoDao);
        List<Map<String, Object>> emailMessageSearch = participantInfoDao.emailMessageSearch(PAGE_NUMBER, PAGE_SIZE, CONTROL_NUMBER);
        assertEquals(1, emailMessageSearch.size());
    }

    @Test
    public void test_participant_enrollment_history() {
        List<EnrollmentHistoryDTO> participantEnrollmentHistory =
                participantInfoDao.getParticipantEnrollmentHistory("847");

        ZonedDateTime fromDate = participantEnrollmentHistory.get(0).getFromDate().withZoneSameLocal(ZoneId.systemDefault());
        assertNotNull("Should get data back back from participant query", participantEnrollmentHistory);
        assertEquals(ENROLLMENT_STATUS_ACTIVE, participantEnrollmentHistory.get(0).getStatus());
        assertEquals(2012, fromDate.getYear());
        assertEquals(4, fromDate.getMonthValue());
        assertEquals(12, fromDate.getDayOfMonth());
        assertNotNull(participantEnrollmentHistory.get(0).getThruDate());
    }

    @Test
    public void test_participant_enrollment_history_2() {
        List<EnrollmentHistoryDTO> participantEnrollmentHistory =
                participantInfoDao.getParticipantEnrollmentHistory("4");

        ZonedDateTime thruDate = participantEnrollmentHistory.get(0).getThruDate().withZoneSameLocal(ZoneId.systemDefault());
        assertNotNull("Should get data back back from participant query", participantEnrollmentHistory);
        assertEquals(ENROLLMENT_STATUS_INACTIVE, participantEnrollmentHistory.get(0).getStatus());
        assertEquals(2012, thruDate.getYear());
        assertEquals(4, thruDate.getMonthValue());
        assertEquals(18, thruDate.getDayOfMonth());
        assertNotNull(participantEnrollmentHistory.get(0).getFromDate());
    }

    @Test
    public void test_hierarchy_history() {

        List<Map<String, Object>> hierarchyHistory =
                participantInfoDao.getHierarchyHistory("847", ProjectConstants.DEFAULT_PAGE_NUMBER,
                        ProjectConstants.DEFAULT_PAGE_SIZE);

        assertNotNull("Should get data back back from getHierarchyHistory query", hierarchyHistory);
        assertEquals(1, hierarchyHistory.size());
    }

    @Test
    public void test_hierarchy_history_negative_page_number() {

        try {
            participantInfoDao.getHierarchyHistory("847", -1, ProjectConstants.DEFAULT_PAGE_SIZE);
            fail("Should have thrown an error");
        } catch (ErrorMessageException eme) {
            TestUtil.assertValidationErrors(1, "PAGE_NUMBER_INVALID", eme);
        }
    }
}
