package com.maritz.culturenext.participant.rest;


import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ParticipantInfoRestServiceTest extends AbstractRestTest {

    private static final String PARTICIPANTS_BASE_PATH = "/rest/participants/";
    private static final String ACTIVITY_POSTPEND = "/groups";
    private static final String OVERVIEW = "/overview";
    private static final String EMAILS = "/emails";

    private static final String ENROLLMENT_HISTORY = "/enrollment";
    private static final String HIERARCHY_HISTORY = "/hierarchy";
    private static final String UPDATE_ROLES = "/roles";

    private static final String BAD_TARGET_ID = "BAD13";
    private static final String NO_CONTROL_NUMBER = "";


    @Test
    public void test_get_groups_by_participant_id_happy_path() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[number]=1&page[size]=1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_groups_by_participant_id_null_page_number() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[size]=1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_groups_by_participant_id_negative_page_number() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[number]=-1&page[size]=1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_groups_by_participant_id_null_page_size() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE?page[number]=1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_groups_by_participant_id_negative_page_size() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[number]=1&page[size]=-1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_groups_by_participant_id_null_page_size_and_multiple_pages() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[number]=1000000")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void test_get_groups_by_participant_id_bad_target_id() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + BAD_TARGET_ID + ACTIVITY_POSTPEND +
                        "?type=ENROLLMENT&status=ACTIVE&page[number]=1&page[size]=1")
                        .with(authToken(TestConstants.USER_KUSEY_ADMIN))
        ).andExpect(status().isNotFound());
    }


    @Test
    public void test_get_participant_overview_happy_path() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM + OVERVIEW)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_get_participant_overview_bad_control_num() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + BAD_TARGET_ID + OVERVIEW)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void test_get_participant_enrollment_history_happy_path() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH + TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM + ENROLLMENT_HISTORY)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_emails_with_control_number() throws Exception {
        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH +
                        TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                        + EMAILS + "?page[number]=1&page[size]=10")
                        .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void get_emails_without_control_number() throws Exception {
        mockMvc.perform(
                        get(PARTICIPANTS_BASE_PATH +
                                NO_CONTROL_NUMBER
                                + EMAILS + "?page[number]=1&page[size]=10")
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void test_get_hierachy_history_happy_path() throws Exception {

        mockMvc.perform(
                get(PARTICIPANTS_BASE_PATH +
                        TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                        + HIERARCHY_HISTORY +
                        "?page[number]=1&page[size]=1")
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_update_participant_roles () throws Exception{
        List<Integer> roleIds = new ArrayList<>();
        roleIds.add(4);
        roleIds.add(5);
        roleIds.add(22);
        roleIds.add(54);
        roleIds.add(55);
        String json = ObjectUtils.objectToJson(roleIds);
        mockMvc.perform(
                put(PARTICIPANTS_BASE_PATH +
                        TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                        + UPDATE_ROLES )
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(json)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }

    @Test
    public void test_update_participant_repeated_roles () throws Exception{
        List<Integer> roleIds = new ArrayList<>();
        roleIds.add(4);
        roleIds.add(5);
        roleIds.add(22);
        roleIds.add(54);
        roleIds.add(54);
        String json = ObjectUtils.objectToJson(roleIds);
        mockMvc.perform(
                put(PARTICIPANTS_BASE_PATH +
                        TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                        + UPDATE_ROLES )
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(json)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isOk());
    }
    @Test
    public void test_update_participant_roles_with_no_administrable_role () throws Exception{
        List<Integer> roleIds = new ArrayList<>();
        roleIds.add(4);
        roleIds.add(5);
        roleIds.add(22);
        roleIds.add(54);
        roleIds.add(30);
        String json = ObjectUtils.objectToJson(roleIds);
        mockMvc.perform(
                put(PARTICIPANTS_BASE_PATH +
                        TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                        + UPDATE_ROLES )
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content(json)
                        .with(authToken(TestConstants.USER_ADMIN))
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void put_update_address() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"ADDY1\", \"ADDY2\", \"ADDY3\", \"ADDY4\", \"ADDY5\", \"ADDY6\"], "
                                                + "\"city\": \"Miami\", "
                                                + "\"countryCode\": \"US\", "
                                                + "\"postalCode\": \"33129\", "
                                                + "\"state\": \"FL\""
                                                + "} "
                                )
                )
                .andExpect(status().isOk())
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_CONTROL_NUMBER);
    }

    @Test
    public void put_update_address_bad_formatting() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"    \", \"\t\t  \", \"  ADDY3\", \"ADDY4\", \"ADDY5\", \"ADDY6\"], "
                                                + "\"city\": \"Miami\", "
                                                + "\"countryCode\": \"  US\", "
                                                + "\"postalCode\": \" 33129\", "
                                                + "\"state\": \"FL\""
                                                + "} "
                                )
                )
                .andExpect(status().is4xxClientError())
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_CONTROL_NUMBER);
    }

    @Test
    public void put_update_address_fail_no_control_number() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_FAIL_NO_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_FAIL_NO_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"ADDY1\", \"ADDY2\", \"ADDY3\", \"ADDY4\", \"ADDY5\", \"ADDY6\"], "
                                                + "\"city\": \"Miami\", "
                                                + "\"countryCode\": \"US\", "
                                                + "\"postalCode\": \"33129\", "
                                                + "\"state\": \"FL\""
                                                + "} "
                                )
                )
                .andExpect(status().is4xxClientError());
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_FAIL_NO_CONTROL_NUMBER);
    }

    @Test
    public void put_update_address_fail_bad_control_number() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_BAD_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_BAD_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"ADDY1\", \"ADDY2\", \"ADDY3\", \"ADDY4\", \"ADDY5\", \"ADDY6\"], "
                                                + "\"city\": \"Miami\", "
                                                + "\"countryCode\": \"US\", "
                                                + "\"postalCode\": \"33129\", "
                                                + "\"state\": \"FL\""
                                                + "} "
                                )
                )
                .andExpect(status().is4xxClientError());
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_BAD_CONTROL_NUMBER);
    }

    @Test
    public void put_update_blank_address() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"\", \"\", \"\", \"\", \"\", \"\"], "
                                                + "\"city\": \"\", "
                                                + "\"countryCode\": \"\", "
                                                + "\"postalCode\": \"\", "
                                                + "\"state\": \"\""
                                                + "} "
                                )
                )
                .andExpect(status().isOk())
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_CONTROL_NUMBER);
    }

    @Test
    public void put_update_address_fail_no_address_id() throws Exception {
        mockMvc.perform(
                        put("/rest/participants/" + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + "/update-address/" + TestConstants.ADDRESS_TEST_FAIL_NO_ADDRESS_ID)
                                .with(authToken(TestConstants.USER_MCVEYCC_ADMIN_ADMIN))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(
                                        "{ "
                                                + "\"controlNumber\": " + TestConstants.ADDRESS_TEST_CONTROL_NUMBER + ", "
                                                + "\"addressId\": " + TestConstants.ADDRESS_TEST_FAIL_NO_ADDRESS_ID + ", "
                                                + "\"addressLines\": [\"ADDY1\", \"ADDY2\", \"ADDY3\", \"ADDY4\", \"ADDY5\", \"ADDY6\"], "
                                                + "\"city\": \"Miami\", "
                                                + "\"countryCode\": \"US\", "
                                                + "\"postalCode\": \"33129\", "
                                                + "\"state\": \"FL\""
                                                + "} "
                                )
                )
                .andExpect(status().is4xxClientError());
        ;

        assertNotNull(TestConstants.ADDRESS_TEST_CONTROL_NUMBER);
    }

}
