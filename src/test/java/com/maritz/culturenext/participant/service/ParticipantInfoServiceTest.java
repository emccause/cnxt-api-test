package com.maritz.culturenext.participant.service;


import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.jpa.support.util.AddressTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.rest.NotFoundException;
import com.maritz.core.security.Security;
import com.maritz.core.util.MapBuilder;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.AddressDTO;
import com.maritz.culturenext.dto.EmailDTO;
import com.maritz.culturenext.dto.EnrollmentHistoryDTO;
import com.maritz.culturenext.dto.HierarchyHistoryDTO;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.dto.PaginatedResponseObject.PaginationRequestDetails;
import com.maritz.culturenext.dto.ParticipantDTO;
import com.maritz.culturenext.dto.RoleDTO;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.participant.service.impl.ParticipantInfoServiceImpl;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.role.dao.RoleDao;
import com.maritz.culturenext.role.services.RoleService;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.maritz.culturenext.constants.ProjectConstants.CONTROL_NUMBER_PARAM;
import static com.maritz.culturenext.constants.RestParameterConstants.PAX_ID_REST_PARAM;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class ParticipantInfoServiceTest extends AbstractDatabaseTest {

    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private Environment environment;
    @Mock private Security security;
    @Mock private RoleService roleService;

    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<Acl> aclDao;

    @InjectMocks private ParticipantInfoServiceImpl participantsInfoService;

    @Inject private ParticipantInfoServiceImpl participantsInfoServiceImpl;

    @Inject PaxRepository paxRepository;
    @Inject AddressRepository addressRepository;

    private static final String GROUPS_BY_PARTICIPANT_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + PAX_ID_REST_PARAM + "}/" + ProjectConstants.GROUPS_URI_BASE;
    private static final String HIERARCHY_HISTORY_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/" + ProjectConstants.HIERARCHY_URI_BASE;
    private static final String EMAIL_HISTORY_PATH = ProjectConstants.PARTICIPANTS_URI_BASE + "/{" + CONTROL_NUMBER_PARAM + "}/" + ProjectConstants.EMAIL_HISTORY_URI_BASE;
    private static final ZonedDateTime FROM_DATE = ZonedDateTime.of(2020,1,1,0,0,0,0, ZoneId.systemDefault());
    private static final ZonedDateTime THRU_DATE = FROM_DATE.plusYears(1);

    private Long testingPaxId;
    private Long addressId;

    AddressDTO addressDTO;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        when(environment.getProperty(anyString())).thenReturn(Boolean.TRUE.toString());
        when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        List<Map<String, Object>> groupsByParticipantList = new ArrayList<>();
        Map<String, Object> groupsByParticipantEntry = new HashMap<>();

        groupsByParticipantEntry.put(ProjectConstants.GROUP_ID, 3124L);
        groupsByParticipantEntry.put(ProjectConstants.GROUP_CONFIG_ID, 12L);
        groupsByParticipantEntry.put(ProjectConstants.FIELD, "STATE");
        groupsByParticipantEntry.put(ProjectConstants.FIELD_DISPLAY_NAME, "State/Province");
        groupsByParticipantEntry.put(ProjectConstants.GROUP_NAME, "MO");
        groupsByParticipantEntry.put(ProjectConstants.TYPE, "ENROLLMENT");
        groupsByParticipantEntry.put(ProjectConstants.VISIBILITY, "ADMIN");
        groupsByParticipantEntry.put(ProjectConstants.STATUS, "ACTIVE");
        groupsByParticipantEntry.put(ProjectConstants.CREATE_DATE, DateUtil.getCurrentDateAtMidnight());
        groupsByParticipantEntry.put(ProjectConstants.PAX_COUNT, 61);
        groupsByParticipantEntry.put(ProjectConstants.GROUP_COUNT, 14);
        groupsByParticipantEntry.put(ProjectConstants.TOTAL_PAX_COUNT, 75);
        groupsByParticipantEntry.put(ProjectConstants.GROUP_PAX_ID, 42L);
        groupsByParticipantEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);

        groupsByParticipantList.add(groupsByParticipantEntry);

        when(participantInfoDao.getGroupsByParticipant(
                anyLong(), anyString(), anyString(), anyInt(), anyInt()))
                .thenReturn(groupsByParticipantList);

        Pax pax = new Pax();
        assertNotNull(paxDao);

        FindBy<Pax> paxDaoFindBy = PowerMockito.mock(FindBy.class);

        PowerMockito.when(paxDao.findBy()).thenReturn(paxDaoFindBy);

        PowerMockito.when(paxDaoFindBy.where(anyString())).thenReturn(paxDaoFindBy);
        PowerMockito.when(paxDaoFindBy.eq(anyString())).thenReturn(paxDaoFindBy);
        PowerMockito.when(paxDaoFindBy.findOne()).thenReturn(pax);

        pax = paxRepository.findByControlNum(TestConstants.ADDRESS_TEST_CONTROL_NUMBER);
        testingPaxId = pax.getPaxId();

        Address address = addressRepository.findByPaxIdAndAddressTypeCode(testingPaxId, AddressTypeCode.BUSINESS.name());
        addressId = address.getAddressId();

        ArrayList<String> addressLines = new ArrayList<>();
        addressLines.add("123 Street");
        addressLines.add("Apt 4");

        addressDTO = new AddressDTO()
                .setControlNumber(TestConstants.ADDRESS_TEST_CONTROL_NUMBER)
                .setAddressId(addressId)
                .setAddressLines(addressLines)
                .setCity("Fenton")
                .setState("MO")
                .setPostalCode("55555")
                .setCountryCode("US")
        ;



    }

    @Test
    public void test_put_address_update() {
        addressDTO.setCity("Springfield");
        AddressDTO newAddress = participantsInfoServiceImpl.updateParticipantAddress(TestConstants.ADDRESS_TEST_CONTROL_NUMBER
                , addressId, null, addressDTO);
        assertNotNull("Data should not be null", newAddress);
    }

    @Test
    public void test_get_groups_by_participant() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(GROUPS_BY_PARTICIPANT_PATH)
                .setPageNumber(1)
                .setPageSize(1);

        PaginatedResponseObject<GroupDTO> response =
                participantsInfoService.getGroupsByParticipant(
                        requestDetails, TestConstants.PAX_KUSEY_ADMIN, "ACTIVE", "ENROLLMENT", 1, 1);

        verify(participantInfoDao, times(1)).getGroupsByParticipant(
                anyLong(), anyString(), anyString(), anyInt(), anyInt());

        assertNotNull("Should have a response", response);

        List<GroupDTO> data = response.getData();
        assertNotNull("Data should not be null", data);
        assertFalse("Data should not be empty", data.isEmpty());
    }

    @Test

    public void test_get_email_history() {

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(EMAIL_HISTORY_PATH)
                .setPageNumber(1)
                .setPageSize(1);

        PaginatedResponseObject<EmailDTO> response =
                participantsInfoService.emailMessageSearch(
                        requestDetails, 1, 1, TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM
                );

        assertNotNull("Should have a response", response);
        assertFalse(response.getLinks().isEmpty());

    }

    @Test
    public void test_get_participant() {
        when(participantInfoDao.getParticipantOverview(anyString())).thenReturn(
                new ParticipantDTO()
                        .setFirstName("Adam")
                        .setLastName("Kusey (Admin)")
                        .setControlNumber(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM)
                        .setCompanyName("Company Test")
        );

        ParticipantDTO response =
                participantsInfoService.getParticipant(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM);

        verify(participantInfoDao, times(1)).getParticipantOverview(anyString());

        assertNotNull("Should have a response", response);
        assertEquals("Adam", response.getFirstName());
        assertEquals(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM, response.getControlNumber());
    }

    @Test(expected = NotFoundException.class)
    public void test_get_participant_not_found() {
        when(participantInfoDao.getParticipantOverview(anyString())).thenThrow(new
                EmptyResultDataAccessException(1));

        participantsInfoService.getParticipant(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM);

        verify(participantInfoDao, times(1)).getParticipantOverview(anyString());
    }

    @Test
    public void test_get_participant_enrollment_history() {
        final ZonedDateTime CHANGE_DATE = LocalDateTime.of(
                2012, 4, 13, 15, 25, 3).atZone(ZoneId.systemDefault());
        final ZonedDateTime FROM_DATE = LocalDateTime.of(2010, 1, 24, 15, 25, 3).atZone(ZoneId.systemDefault());
        final String STATUS = "ACTIVE";

        when(participantInfoDao.getParticipantEnrollmentHistory(anyString())).thenReturn(
                Arrays.asList(new EnrollmentHistoryDTO()
                        .setChangeDate(CHANGE_DATE)
                        .setFromDate(FROM_DATE)
                        .setThruDate(null)
                        .setStatus(STATUS)
                )
        );

        List<EnrollmentHistoryDTO> response =
                participantsInfoService.getParticipantEnrollmentHistory(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM);

        verify(participantInfoDao, times(1)).getParticipantEnrollmentHistory(anyString());

        assertNotNull("Should have a response", response);
        assertEquals(response.get(0).getChangeDate(), CHANGE_DATE);
        assertEquals(response.get(0).getFromDate(), FROM_DATE);
        assertNull(response.get(0).getThruDate());
        assertEquals(response.get(0).getStatus(), STATUS);
    }

    @Test
    public void test_get_hierarchy_history_happy_path() {
        when(participantInfoDao.getHierarchyHistory(
                anyString(), anyInt(), anyInt()))
                .thenReturn(getHierarchyHistoryResult(1));

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(HIERARCHY_HISTORY_PATH)
                .setPageNumber(1)
                .setPageSize(1);

        PaginatedResponseObject<HierarchyHistoryDTO> response =
                participantsInfoService.getHierarchyHistory(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM,
                        requestDetails,  1, 1);

        verify(participantInfoDao, times(1)).getHierarchyHistory(
                anyString(), anyInt(), anyInt());

        assertNotNull("Should have a response", response);

        List<HierarchyHistoryDTO> data = response.getData();
        assertNotNull("Data should not be null", data);
        assertFalse("Data should not be empty", data.isEmpty());
        assertEquals(FROM_DATE, data.get(0).getFromDate());
        assertEquals(THRU_DATE, data.get(0).getThruDate());
        assertEquals(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM, data.get(0).getControlNumber());
    }


    @Test
    public void test_get_hierarchy_history_multiple_pages() {
        when(participantInfoDao.getHierarchyHistory(
                anyString(), anyInt(), anyInt()))
                .thenReturn(getHierarchyHistoryResult(3));

        PaginationRequestDetails requestDetails = new PaginationRequestDetails()
                .setRequestPath(HIERARCHY_HISTORY_PATH)
                .setPageNumber(2)
                .setPageSize(2);

        PaginatedResponseObject<HierarchyHistoryDTO> response =
                participantsInfoService.getHierarchyHistory(TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM,
                        requestDetails,  2, 2);

        verify(participantInfoDao, times(1)).getHierarchyHistory(anyString(), anyInt(), anyInt());

        assertNotNull("Should have a response", response);
        List<HierarchyHistoryDTO> data = response.getData();
        assertNotNull("Data should not be null", data);
        assertFalse("Data should not be empty", data.isEmpty());
        assertTrue(response.getLinks().get("last").contains("?page[number]=2&page[size]=2"));
        assertTrue(response.getLinks().get("prev").contains("?page[number]=1&page[size]=2"));
        assertTrue(response.getLinks().get("self").contains("?page[number]=2&page[size]=2"));
        assertTrue(response.getLinks().get("first").contains("?page[number]=1&page[size]=2"));
    }

    private List<Map<String, Object>> getHierarchyHistoryResult(int totalRestult) {
        List<Map<String, Object>> hierarchyHistory = new ArrayList<>();


        hierarchyHistory.add(
                MapBuilder.<String,Object>newHashMap()
                        .add(ProjectConstants.FROM_DATE, Date.from(FROM_DATE.toInstant()))
                        .add(ProjectConstants.THRU_DATE, Date.from(THRU_DATE.toInstant()))
                        .add(CONTROL_NUMBER_PARAM, TestConstants.PAX_KUSEY_ADMIN_CONTROL_NUM)
                        .add(ProjectConstants.TOTAL_RESULTS_KEY, totalRestult)
                        .build()
        );
        return  hierarchyHistory;
    }

    @Test(expected = ErrorMessageException.class)
    public void test_ChangeParticipantRolesAddingWrongRoles(){
        RoleDTO tmp = new RoleDTO().setId(54L).setCode("CLIENT_SUPPORT").setName("Client Support");
        Set<RoleDTO> tmpAdministrableRoles = new HashSet<>();
        tmpAdministrableRoles.add(tmp);
        tmp = new RoleDTO().setId(55L).setCode("CLIENT_ADMINISTRATOR").setName("Client Administrator");
        tmpAdministrableRoles.add(tmp);
        Set<Long> roleIds = new HashSet<>();
        roleIds.add(4L);
        roleIds.add(5L);
        roleIds.add(22L);
        roleIds.add(54L);
        roleIds.add(55L);

        when(roleService.getRoles(anyBoolean())).thenReturn(tmpAdministrableRoles);
        participantsInfoService.changeParticipantRoles("10560",roleIds);
    }

    @Test
    public void test_ChangeParticipantRoles(){

        List<RoleDTO> lstControlNumberAssociatedRoles = new ArrayList<>();
        Set<RoleDTO> tmpAdministrableRoles = new HashSet<>();

        RoleDTO tmp = new RoleDTO().setId(54L).setCode("CLIENT_SUPPORT").setName("Client Support");

        tmpAdministrableRoles.add(tmp);
        lstControlNumberAssociatedRoles.add(tmp);
        tmp = new RoleDTO().setId(55L).setCode("CLIENT_ADMINISTRATOR").setName("Client Administrator");
        tmpAdministrableRoles.add(tmp);
        lstControlNumberAssociatedRoles.add(tmp);

        tmp = new RoleDTO().setId(4L).setCode("RECG").setName("Participant");
        lstControlNumberAssociatedRoles.add(tmp);

        tmp = new RoleDTO().setId(5L).setCode("CADM").setName("Client Admin");
        lstControlNumberAssociatedRoles.add(tmp);

        tmp = new RoleDTO().setId(22L).setCode("Admin").setName("Admin");
        lstControlNumberAssociatedRoles.add(tmp);

        Set<Long> roleIds = new HashSet<>();
        roleIds.add(4L);
        roleIds.add(5L);
        roleIds.add(22L);
        roleIds.add(54L);
        roleIds.add(55L);

        assertNotNull(roleIds);
        when(roleService.getRoles(anyBoolean())).thenReturn(tmpAdministrableRoles);
        when(roleService.getRolesForControlNumber(anyString())).thenReturn(lstControlNumberAssociatedRoles);


        participantsInfoService.changeParticipantRoles("10560",roleIds);
    }
}
