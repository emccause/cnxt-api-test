package com.maritz.culturenext.theme.rest;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.maritz.core.jpa.entity.Files;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.theme.dto.BrandingDto;
import com.maritz.culturenext.theme.services.impl.ThemeServiceImpl;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.junit.Before;
import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.powermock.api.mockito.PowerMockito.when;

import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_PRIMARY_COLOR;
import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_SECONDARY_COLOR;
import static com.maritz.culturenext.constants.ApplicationDataConstants.KEY_NAME_TEXT_ON_COLOR;

public class ThemeRestServiceTest extends AbstractRestTest {
    @Inject
    private ThemeServiceImpl themeServiceImpl;
    @Inject
    private FileImageService fileImageService;
    @Inject
    private EnvironmentUtil environmentUtil;
    @Inject
    private GcpUtil gcpUtil;

    @Mock
    private FileImageService mockFileImageService;
    @Mock
    private EnvironmentUtil mockEnvironmentUtil;
    @Mock
    private GcpUtil mockGcpUtil;

    @Before
    public void before() {
        // By default wire up actual implementations
        themeServiceImpl
                .setFileImageService(fileImageService)
                .setEnvironmentUtil(environmentUtil)
                .setGcpUtil(gcpUtil);

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void get_logo() throws Exception {
        mockMvc.perform(
                get("/rest/images/logo")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_background() throws Exception {
        mockMvc.perform(
                get("/rest/images/background")
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void getBranding_noCdn() throws Exception {
        themeServiceImpl
                .setFileImageService(mockFileImageService)
                .setEnvironmentUtil(mockEnvironmentUtil)
                .setGcpUtil(mockGcpUtil);

        when(mockEnvironmentUtil.readFromCdn()).thenReturn(false);
        when(mockEnvironmentUtil.getProperty(KEY_NAME_PRIMARY_COLOR, false)).thenReturn("primary");
        when(mockEnvironmentUtil.getProperty(KEY_NAME_SECONDARY_COLOR, false)).thenReturn("secondary");
        when(mockEnvironmentUtil.getProperty(KEY_NAME_TEXT_ON_COLOR, false)).thenReturn("text");

        BrandingDto branding = new ObjectMapper().readValue(
                                                        mockMvc.perform(
                                                                get("/rest/branding")
                                                        ).andExpect(status().isOk())
                                                        .andReturn().getResponse().getContentAsString(),
                                                        BrandingDto.class);

        assertThat(branding.getImageUrls().getLogo(), is("/rest/images/logo"));
        assertThat(branding.getImageUrls().getBackground(), is("/rest/images/background"));
        assertThat(branding.getColors().getPrimary(), is("primary"));
        assertThat(branding.getColors().getSecondary(), is("secondary"));
        assertThat(branding.getColors().getText(), is("text"));

        verify(mockEnvironmentUtil, times(1)).readFromCdn();
        verify(mockFileImageService, times(0)).searchForImage(any(FileImageTypes.class));
        verify(mockGcpUtil, times(0)).signUrl(any(Files.class));
    }

    @Test
    public void getBranding_cdn() throws Exception {
        themeServiceImpl
                .setFileImageService(mockFileImageService)
                .setEnvironmentUtil(mockEnvironmentUtil)
                .setGcpUtil(mockGcpUtil);

        when(mockEnvironmentUtil.readFromCdn()).thenReturn(true);
        Files logoFile = new Files();
        when(mockFileImageService.searchForImage(FileImageTypes.PROJECT_LOGO)).thenReturn(logoFile);
        Files backgroundImageFile = new Files();
        when(mockFileImageService.searchForImage(FileImageTypes.PROJECT_BACKGROUND)).thenReturn(backgroundImageFile);
        when(mockGcpUtil.signUrl(logoFile)).thenReturn("signedLogo");
        when(mockGcpUtil.signUrl(backgroundImageFile)).thenReturn("signedBackground");
        when(mockEnvironmentUtil.getProperty(KEY_NAME_PRIMARY_COLOR, false)).thenReturn("primary");
        when(mockEnvironmentUtil.getProperty(KEY_NAME_SECONDARY_COLOR, false)).thenReturn("secondary");
        when(mockEnvironmentUtil.getProperty(KEY_NAME_TEXT_ON_COLOR, false)).thenReturn("text");

        BrandingDto branding = new ObjectMapper().readValue(
                mockMvc.perform(
                        get("/rest/branding")
                ).andExpect(status().isOk())
                        .andReturn().getResponse().getContentAsString(),
                BrandingDto.class);

        assertThat(branding.getImageUrls().getLogo(), is("signedLogo"));
        assertThat(branding.getImageUrls().getBackground(), is("signedBackground"));
        assertThat(branding.getColors().getPrimary(), is("primary"));
        assertThat(branding.getColors().getSecondary(), is("secondary"));
        assertThat(branding.getColors().getText(), is("text"));

        verify(mockEnvironmentUtil, times(1)).readFromCdn();
        verify(mockFileImageService, times(2)).searchForImage(any(FileImageTypes.class));
        verify(mockFileImageService, times(1)).searchForImage(FileImageTypes.PROJECT_LOGO);
        verify(mockFileImageService, times(1)).searchForImage(FileImageTypes.PROJECT_BACKGROUND);
    }
}
