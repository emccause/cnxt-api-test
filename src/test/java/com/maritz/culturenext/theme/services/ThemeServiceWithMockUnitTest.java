package com.maritz.culturenext.theme.services;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.images.dao.impl.FilesDaoImpl;
import com.maritz.culturenext.images.service.FileImageService;
import com.maritz.culturenext.theme.services.impl.ThemeServiceImpl;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.mockito.expectation.PrivatelyExpectedArguments;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class ThemeServiceWithMockUnitTest {

    @Mock
    private Security security;
    @Mock
    private FileImageService fileImageService;

    @InjectMocks
    private ThemeServiceImpl themeService;

    private final String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private final String VALID_IMAGE = "resizing-image.png";

    @Before
    public void setup() {
        when(security.hasRole(anyString(), anyString()))
                .thenReturn(true);
    }


    @Test
    public void test_get_logo_light_happy_path() {
        assertNotNull(themeService);
        assertNotNull(fileImageService);
        Long imageId = 123l;
        String type = "image/png";
        MediaType.parseMediaType(type);

        Files image = new Files();
        image.setId(imageId);
        image.setMediaType(type);
        PowerMockito.when(fileImageService.searchForImage((FileImageTypes) anyObject()))
                .thenReturn(image);

        Files imageFile = themeService.getLogoLight();

        assertTrue(imageFile != null);

    }

    @Test(expected= ErrorMessageException.class)
    public void test_delete_logo_happy_path() throws Exception {
        themeService.deleteLogo();
    }
}
