package com.maritz.culturenext.theme.services;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.security.Security;
import com.maritz.culturenext.theme.services.impl.ThemeServiceImpl;
import com.maritz.test.AbstractDatabaseTest;
import org.apache.catalina.connector.Response;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@ContextConfiguration
public class ThemeServiceUnitTest extends AbstractDatabaseTest{

    @Mock
    private Security security;

    @Inject
    private ThemeServiceImpl themeService;

    private final String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private final String VALID_IMAGE = "resizing-image.png";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(security.hasRole(anyString(), anyString()))
                .thenReturn(true);
    }

    @Test
    public void test_get_logo_happy_path() {
        assertNotNull(themeService);
        Files imageFile = themeService.getLogo();
        assertTrue(imageFile != null);
    }

    @Test
    public void test_get_background_happy_path() {
        assertNotNull(themeService);
        Files imageFile = themeService.getBackground();
        assertNotNull("Mediatype should not be null but was.",imageFile.getMediaType());
        assertTrue(imageFile != null);
    }

    @Test
    public void test_get_background_response(){
        ResponseEntity<Object> response = themeService.getBackgroundResponse();
        assertNotNull(response);
    }

    @Test
    public void test_get_logo_response(){
        ResponseEntity<Object> response = themeService.getResponseLogo();
        assertNotNull(response);
    }

    @Test
    public void test_get_logolight_response(){
        ResponseEntity<Object> response = themeService.getResponseLogoLight();
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
    }
}
