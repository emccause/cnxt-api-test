package com.maritz.culturenext.catalog.blindpass.rest;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.culturenext.catalog.blindpass.constants.CatalogBlindPassConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ParticipantsBlindPassRestServiceTest extends AbstractRestTest{
    
    public static final String GET_CATALOG_LOGIN_INFO_URI = "/rest/participants/~/card-types/";
    
    @Test
    public void test_get_getCatalogBlindPass_success_rideau() throws Exception {
        mockMvc.perform(
                get(GET_CATALOG_LOGIN_INFO_URI + ProjectConstants.RIDEAU_CARD_TYPE)
                        .with(authToken(TestConstants.USER_GARCIAF2))
                        
                )
            .andExpect(status().isOk())
            .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.cardTypeCode",is(ProjectConstants.RIDEAU_CARD_TYPE)))
            .andExpect(jsonPath("$.params[0].name",is(CatalogBlindPassConstants.PARAM_NAME_JWT)))
            .andExpect(jsonPath("$.params[0].value",not(isEmptyOrNullString())))
            ;
        ;
    }

}
