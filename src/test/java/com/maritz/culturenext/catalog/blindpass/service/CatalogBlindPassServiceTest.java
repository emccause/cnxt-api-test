package com.maritz.culturenext.catalog.blindpass.service;


import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import org.mockito.Spy;

import java.util.Iterator;

import javax.inject.Named;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.CardType;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxAccount;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.CardTypeRepository;
import com.maritz.core.jpa.repository.PaxAccountRepository;
import com.maritz.core.jpa.repository.PaxRepository;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.catalog.blindpass.constants.CatalogBlindPassConstants;
import com.maritz.culturenext.catalog.blindpass.dto.CatalogBlindPassCardTypeDTO;
import com.maritz.culturenext.catalog.blindpass.service.impl.CatalogBlindPassServiceImpl;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractMockTest;
import com.maritz.wallet.dto.CatalogBlindPassDTO;
import com.maritz.wallet.dto.ParamDTO;
import com.maritz.wallet.services.WalletService;
import com.maritz.wallet.services.WalletServiceImpl;

import io.jsonwebtoken.Claims;


public class CatalogBlindPassServiceTest extends AbstractMockTest {

    private final String TEST_PAX_USERNAME = "tester";
    private final String TEST_PAX_PASSWORD = "s3cr3t";
    private final String TEST_PAX_ACCOUNT_CARD_TYPE_CODE = "TST";
    private final String TEST_PAX_ACCOUNT_CARD_NAME = "TEST";
    private final String TEST_PAX_ACCOUNT_CARD_DESCRIPTION = "Testing card description";
    private final String TEST_ADDRESS_COUNTRY_CODE = "US";
    private final String TEST_SA_BASE_URL = "https://www.rideau-test.com/sso-login.aspx";
    private final String TEST_SA_REDIRECT_LINK = "/redirect-url";

    //Rideau test values
    private final String TEST_RIDEAU_ISSUER = "maritz";
    private final String TEST_RIDEAU_JWT_TOKEN= "this.is.a.jwt.token.only.for.testing.purposes";


    @InjectMocks private CatalogBlindPassServiceImpl catalogBlindPassService;
    
    @Mock private AddressRepository addressRepository;
    @Mock private ApplicationDataService applicationDataService;
    @Mock private CardTypeRepository cardTypeRespository;
    @Mock private PaxAccountRepository paxAccountRepository;
    @Mock private Security security;
    @Mock private WalletServiceImpl walletService;
    @Mock @Named("rideauTokenService") private TokenService rideauTokenService;
    @Mock private PaxRepository paxRepository;

    @Mock Claims claims;
    
    @Mock CatalogBlindPassDTO blindPass;

    @Rule public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup () {
        // Mock PaxAccountRepository functionality
        PowerMockito.when(paxAccountRepository.findIssuedByPaxIdAndCardTypeCode(
                anyLong(), anyString())).thenReturn(mockedPaxAccountObj());

        // Mock Security functionality
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.ID_1);
        PowerMockito.when(security.getUserName()).thenReturn(TEST_PAX_USERNAME);

        // Mock CardTypeRepository functionality
        PowerMockito.when(cardTypeRespository.findByCardTypeCode(anyString())).thenReturn(mockedCardTypeObj());

        // Mock AddressRepository functionality
        PowerMockito.when(addressRepository.findPreferredByPaxId(anyLong())).thenReturn(mockedAddressObj());

        // Mock applicationDataService functionality
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_RIDEAU_TOKEN_ISSUER))
            .thenReturn(new ApplicationDataDTO().setValue(TEST_RIDEAU_ISSUER));
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL))
            .thenReturn(new ApplicationDataDTO().setValue(TEST_SA_BASE_URL));
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_REDIRECT_LINK))
            .thenReturn(new ApplicationDataDTO().setValue(TEST_SA_REDIRECT_LINK));

        // Mock walletService functionality
        ///(replaced) PowerMockito.when(walletService.getCatalogBlindPass(anyString()))
////        PowerMockito.when(walletService.getCatalogBlindPass(paxRepository.findOne(anyLong()), anyString(), anyString(), anyString()))
////            .thenReturn(mockedWalletCatalogBlindPassResponse());
            PowerMockito.when(walletService.getCatalogBlindPass(paxRepository.findOne(anyLong()),  "ABS", null, null))
            .thenReturn(mockedWalletCatalogBlindPassResponse());


        // Mock TokenService functionality
        PowerMockito.when(rideauTokenService.createClaims()).thenReturn(claims);
        PowerMockito.when(rideauTokenService.createToken(claims)).thenReturn(TEST_RIDEAU_JWT_TOKEN);
        
        //Pax pax3 = new Pax();
    	//pax3 = paxRepository.findOne(anyLong());
        
//        PowerMockito.when(walletService.getCatalogBlindPass(paxRepository.findOne(anyLong()),  "ABS", null, null))    ///
//        .thenReturn(mockedCatalogBlindPassResponse());                                                                ///
        
    	
    	PowerMockito.when(paxRepository.findOne(anyLong())).thenReturn(mockedPaxObj());                               ///
    	
    }

    @Test
    public void test_getCatalogBlindPass_fail_rideau_invalid_cardtype_not_belog_to_pax() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CatalogBlindPassConstants.ERROR_CATALOG_DATA_MISSED.getCode()));

        // Mock PaxAccountRepository functionality - not found
        PowerMockito.when(paxAccountRepository.findIssuedByPaxIdAndCardTypeCode(
                anyLong(), anyString())).thenReturn(null);

        // Card type code empty
        catalogBlindPassService.getCatalogBlindPass(ProjectConstants.RIDEAU_CARD_TYPE);

        fail("Servce must be failed.");
    }

    @Test
    public void test_getCatalogBlindPass_fail_rideau_baseUrl_not_set() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CatalogBlindPassConstants.ERROR_CATALOG_DATA_MISSED.getCode()));

        // Mock applicationDataService - service anniversary base Url not set
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SERVICE_ANNIVERSARY_BASE_URL))
            .thenReturn(new ApplicationDataDTO().setValue(null));

        // Card type code empty
        catalogBlindPassService.getCatalogBlindPass(ProjectConstants.RIDEAU_CARD_TYPE);

        fail("Servce must be failed.");
    }

    @Test
    public void test_getCatalogBlindPass_fail_rideau_jwtToken_not_generated() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CatalogBlindPassConstants.ERROR_CATALOG_DATA_MISSED.getCode()));

        // Mock rideauTokenService - not able to generate token
        PowerMockito.when(rideauTokenService.createToken(claims)).thenReturn(null);

        // Card type code empty
        catalogBlindPassService.getCatalogBlindPass(ProjectConstants.RIDEAU_CARD_TYPE);

        fail("Servce must be failed.");
    }

    @Test
    public void test_getCatalogBlindPass_fail_rideau_issuer_not_set() {
        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(containsString(CatalogBlindPassConstants.ERROR_CATALOG_DATA_MISSED.getCode()));

        // Mock applicationDataService - rideau isser not set
        PowerMockito.when(applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_RIDEAU_TOKEN_ISSUER))
            .thenReturn(new ApplicationDataDTO().setValue(null));

        // Card type code empty
        catalogBlindPassService.getCatalogBlindPass(ProjectConstants.RIDEAU_CARD_TYPE);

        fail("Servce must be failed.");
    }

    @Test
    public void test_getCatalogBlindPass_success_cardType_RIDEAU() {
        CatalogBlindPassCardTypeDTO blindPassResponse =
                catalogBlindPassService.getCatalogBlindPass(ProjectConstants.RIDEAU_CARD_TYPE);

        assertNotNull(blindPassResponse);
        assertThat(blindPassResponse.getCardTypeCode(), is(ProjectConstants.RIDEAU_CARD_TYPE));
        assertThat(blindPassResponse.getCardName(), is(TEST_PAX_ACCOUNT_CARD_NAME));
        assertThat(blindPassResponse.getCardDescription(), is(TEST_PAX_ACCOUNT_CARD_DESCRIPTION));
        assertThat(blindPassResponse.getCurrentCountryCode(), is(TEST_ADDRESS_COUNTRY_CODE));
        assertThat(blindPassResponse.getCatalogBaseURL(), is(TEST_SA_BASE_URL));
        assertThat(blindPassResponse.getRedirectMethod(), is(CatalogBlindPassConstants.REDIRECT_POST));
        assertNotNull(blindPassResponse.getParams());
        assertThat(blindPassResponse.getParams().size(), is(2));

        Iterator<ParamDTO> iterator = blindPassResponse.getParams().iterator();
        ParamDTO param = iterator.next();
        assertThat(param.getName(), is(CatalogBlindPassConstants.PARAM_NAME_JWT));
        assertThat(param.getValue(), is(TEST_RIDEAU_JWT_TOKEN));

        param = iterator.next();
        assertThat(param.getName(), is(CatalogBlindPassConstants.PARAM_NAME_REDIRECT_LINK));
        assertThat(param.getValue(), is(TEST_SA_REDIRECT_LINK));
    }

    @Ignore
    @Test
    public void test_getCatalogBlindPass_success_cardType_DPP() {
        CatalogBlindPassCardTypeDTO blindPassResponse =
                catalogBlindPassService.getCatalogBlindPass("DPP");

        assertNotNull(blindPassResponse);
        assertThat(blindPassResponse.getCardTypeCode(), is(TEST_PAX_ACCOUNT_CARD_TYPE_CODE));
        assertThat(blindPassResponse.getCardName(), is(TEST_PAX_ACCOUNT_CARD_NAME));
        assertThat(blindPassResponse.getCardDescription(), is(TEST_PAX_ACCOUNT_CARD_DESCRIPTION));
        assertThat(blindPassResponse.getCurrentCountryCode(), is(TEST_ADDRESS_COUNTRY_CODE));
        assertThat(blindPassResponse.getCatalogBaseURL(), is(TEST_SA_BASE_URL));
        assertThat(blindPassResponse.getRedirectMethod(), is(CatalogBlindPassConstants.REDIRECT_SIMPLE));
        assertNotNull(blindPassResponse.getParams());
        assertThat(blindPassResponse.getParams().size(), is(2));

        Iterator<ParamDTO> iterator = blindPassResponse.getParams().iterator();
        ParamDTO param = iterator.next();
        assertThat(param.getName(), is(CatalogBlindPassConstants.PARAM_NAME_USERNAME));
        assertThat(param.getValue(), is(TEST_PAX_USERNAME));

        param = iterator.next();
        assertThat(param.getName(), is(CatalogBlindPassConstants.PARAM_NAME_PASSWORD));
        assertThat(param.getValue(), is(TEST_PAX_PASSWORD));
    }


    private PaxAccount mockedPaxAccountObj() {
        PaxAccount paxAccount = new PaxAccount();
        paxAccount.setCardName(TEST_PAX_ACCOUNT_CARD_NAME);
        return paxAccount;
    }

    private Pax mockedPaxObj() {
        Pax pax = new Pax();
        Long paxId = 4L;
        pax.setPaxId(paxId);
        pax.setControlNum("PARTICIPANT1");
        return pax;
    }
    
    
    private CardType mockedCardTypeObj() {
        CardType cardType = new CardType();
        cardType.setCardTypeDesc(TEST_PAX_ACCOUNT_CARD_DESCRIPTION);
        return cardType;
    }

    private Address mockedAddressObj() {
        Address address = new Address();
        address.setCountryCode(TEST_ADDRESS_COUNTRY_CODE);
        return address;
    }

    private CatalogBlindPassDTO mockedWalletCatalogBlindPassResponse() {
        CatalogBlindPassDTO walletblindPass = new CatalogBlindPassDTO();
        walletblindPass.setCardName(TEST_PAX_ACCOUNT_CARD_NAME);
        walletblindPass.setCardType(TEST_PAX_ACCOUNT_CARD_TYPE_CODE);
        walletblindPass.setCatalogBaseURL(TEST_SA_BASE_URL);
        walletblindPass.setCurrentCountryCode(TEST_ADDRESS_COUNTRY_CODE);
        walletblindPass.setRedirectMethod(CatalogBlindPassConstants.REDIRECT_SIMPLE);
        walletblindPass
            .addParam(new ParamDTO()
                .setName(CatalogBlindPassConstants.PARAM_NAME_USERNAME)
                .setValue(TEST_PAX_USERNAME))
            .addParam(new ParamDTO()
                    .setName(CatalogBlindPassConstants.PARAM_NAME_PASSWORD)
                    .setValue(TEST_PAX_PASSWORD));
        return walletblindPass;
    }
    
    
//    private CatalogBlindPassDTO mockedCatalogBlindPassResponse() {
//        CatalogBlindPassDTO blindPass = new CatalogBlindPassDTO();
//        blindPass.setCardName(TEST_PAX_ACCOUNT_CARD_NAME);
//        blindPass.setCardType(TEST_PAX_ACCOUNT_CARD_TYPE_CODE);
//        blindPass.setCatalogBaseURL(TEST_SA_BASE_URL);
//        blindPass.setCurrentCountryCode(TEST_ADDRESS_COUNTRY_CODE);
//        blindPass.setRedirectMethod(CatalogBlindPassConstants.REDIRECT_SIMPLE);
//        blindPass
//            .addParam(new ParamDTO()
//                .setName(CatalogBlindPassConstants.PARAM_NAME_USERNAME)
//                .setValue(TEST_PAX_USERNAME))
//            .addParam(new ParamDTO()
//                    .setName(CatalogBlindPassConstants.PARAM_NAME_PASSWORD)
//                    .setValue(TEST_PAX_PASSWORD));
//        return blindPass;
//    }
    
}

