package com.maritz.culturenext.transaction.dao;

import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.*;

public class TransactionHeaderMiscDaoTest extends AbstractDatabaseTest {

    @Inject
    private TransactionHeaderMiscDao testingClass;

    @Test
    public void createNonUsOverrideProjectSubproject() {
        assertNotNull(testingClass);
        testingClass.createNonUsOverrideProjectSubproject();
    }
}