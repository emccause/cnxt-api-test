package com.maritz.culturenext.networkconnections.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.networkconnections.services.impl.NetworkConnectionServiceImpl;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.util.EmployeeSortUtil;
import com.maritz.test.AbstractMockTest;

public class NetworkConnectionServiceTest extends AbstractMockTest {

    @InjectMocks private NetworkConnectionServiceImpl networkConnectionService;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private Environment environment;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private Security security;

    private FindBy<Relationship> mockedFindbyRelationship = PowerMockito.mock(FindBy.class);

    private static final Long submitterPaxId = 10L;
    private static final Long submitterManagerPaxId = 100L;
    private static final Long targetPaxId = 11L;
    private static final Long targetManagerPaxId = 200L;
    private static final int magicAsciiNumber = 65;

    @Before
    public void setup() {
        PowerMockito.when(relationshipDao.findBy()).thenReturn(mockedFindbyRelationship);
        PowerMockito.when(mockedFindbyRelationship.where(anyString())).thenReturn(mockedFindbyRelationship);
        PowerMockito.when(mockedFindbyRelationship.and(anyString())).thenReturn(mockedFindbyRelationship);
        PowerMockito.when(mockedFindbyRelationship.eq(anyLong())).thenReturn(mockedFindbyRelationship);
        PowerMockito.when(mockedFindbyRelationship.eq(anyString())).thenReturn(mockedFindbyRelationship);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED))
                .thenReturn(Boolean.TRUE.toString());
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNetworkConnections_missing_pax() {
        List<EmployeeDTO> connections = new ArrayList<>();
        connections.add(new EmployeeDTO());
        networkConnectionService.createNetworkConnections(submitterPaxId, connections);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNetworkConnections_error_same_pax() {
        List<EmployeeDTO> connections = new ArrayList<>();
        EmployeeDTO pax = new EmployeeDTO();
        pax.setPaxId(submitterPaxId);
        pax.setManagerPaxId(submitterManagerPaxId);
        connections.add(pax);
        networkConnectionService.createNetworkConnections(submitterPaxId, connections);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_createNetworkConnections_invalid_pax() {
        List<EmployeeDTO> connections = new ArrayList<>();
        EmployeeDTO pax = new EmployeeDTO();
        pax.setPaxId(TestConstants.ID_2);
        pax.setManagerPaxId(3L);
        connections.add(pax);

        PowerMockito.when(participantInfoDao.getEmployeeDTO(TestConstants.ID_2)).thenReturn(null);

        networkConnectionService.createNetworkConnections(submitterPaxId, connections);
    }

    @Test
    public void test_createNetworkConnections_happy_path() {
        int size = 10;
        List<EmployeeDTO> connections = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            EmployeeDTO pax = new EmployeeDTO();
            Long connectionId = new Long(i);
            pax.setPaxId(connectionId);
            pax.setManagerPaxId(999L);
            if(i==2 ) {
                pax.setFirstName(Character.toString((char) (magicAsciiNumber + 1)));
                pax.setLastName(Character.toString((char) (magicAsciiNumber + (i - 2))));
            } else if(i==3){
                pax.setFirstName(Character.toString((char) (magicAsciiNumber + 1)));
                pax.setLastName(Character.toString((char) (magicAsciiNumber + (i + 2))));
            } else {
                pax.setFirstName(Character.toString((char) (magicAsciiNumber + i)));
                pax.setLastName(Character.toString((char)(magicAsciiNumber+i)));
            }

            connections.add(pax);

            PowerMockito.when(participantInfoDao.getEmployeeDTO(connectionId)).thenReturn(pax);
        }

        PowerMockito.when(mockedFindbyRelationship.findOne()).thenReturn(new Relationship());

        List<EmployeeDTO> result = networkConnectionService.createNetworkConnections(submitterPaxId, connections);

        // Guarantee order before running assertion loop
        Collections.sort(result, EmployeeSortUtil.firstAndLastNameComparator);

        for (int i = 0; i < size; i++) {
            if(i==1 ) {
                assertEquals(connections.get(i+1).getPaxId(), result.get(i).getPaxId());
                assertEquals(connections.get(i+1).getManagerPaxId(), result.get(i).getManagerPaxId());
            } else if(i==2){
                assertEquals(connections.get(i-1).getPaxId(), result.get(i).getPaxId());
                assertEquals(connections.get(i-1).getManagerPaxId(), result.get(i).getManagerPaxId());
            } else {
                assertEquals(connections.get(i).getPaxId(), result.get(i).getPaxId());
                assertEquals(connections.get(i).getManagerPaxId(), result.get(i).getManagerPaxId());
            }


        }
    }

    @Test

    public void test_getNetworkConnectionCount_valid_count() {
        PowerMockito.when(mockedFindbyRelationship.count()).thenReturn(0);

        Integer count = networkConnectionService.getNetworkConnectionCount(submitterPaxId);

        Mockito.verify(mockedFindbyRelationship, times(1)).and(ProjectConstants.RELATIONSHIP_TYPE_CODE);
        Mockito.verify(mockedFindbyRelationship, times(1)).eq(RelationshipType.FRIENDS.toString());
        assertEquals(count, new Integer(0));
    }

    @Test
    public void test_getAllNetworkConnections_empty_result() {
        PowerMockito.when(mockedFindbyRelationship.find()).thenReturn(null);

        List<EmployeeDTO> networkConnections =
                networkConnectionService.getAllNetworkConnections(submitterPaxId, null, null);

        assertTrue(networkConnections.isEmpty());
    }

    @Test
    public void test_getAllNetworkConnections_no_duplicates() {
        int size = 10;
        List<Relationship> relationships = new ArrayList<>();
        List<EmployeeDTO> connections = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Relationship relationship = new Relationship();
            relationship.setPaxId2(new Long(i));
            relationships.add(relationship);
            relationships.add(relationship);

            EmployeeDTO connection = new EmployeeDTO();
            connection.setPaxId(new Long(i));
            connection.setManagerPaxId(888L);
            connection.setFirstName(Character.toString((char)(magicAsciiNumber+i)));
            connection.setLastName(Character.toString((char)(magicAsciiNumber+i)));
            //PowerMockito.when(participantInfoDao.getEmployeeDTO(new Long(i))).thenReturn(connection);
            connections.add(connection);
        }

        PowerMockito.when(mockedFindbyRelationship.find()).thenReturn(relationships);
        PowerMockito.when(participantInfoDao.getInfo(anyList(),anyList()))
                .thenReturn(new ArrayList<EntityDTO>(connections));

        List<EmployeeDTO> networkConnections =
                networkConnectionService.getAllNetworkConnections(submitterPaxId, null, null);

        assertEquals(networkConnections.size(), size);

        // Resulting network connections should be in matching order
        Collections.sort(connections, EmployeeSortUtil.firstAndLastNameComparator);
        for (int i = 0; i < size; i++) {
            assertEquals(networkConnections.get(i).getFirstName(), connections.get(i).getFirstName());
            assertEquals(networkConnections.get(i).getManagerPaxId(), connections.get(i).getManagerPaxId());
        }
    }

    public void test_check_for_network_connection() {

        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setPaxId(targetPaxId);

        PowerMockito.when(security.getPaxId()).thenReturn(submitterPaxId);
        PowerMockito.when(mockedFindbyRelationship.findOne()).thenReturn(new Relationship());
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(employeeDTO);

        EmployeeDTO networkCheck = networkConnectionService.checkForNetworkConnection(submitterPaxId, targetPaxId);

        assertThat(networkCheck.getPaxId(), is(targetPaxId));

    }

    @Test
    public void test_check_for_network_connection_no_relationship() {

        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setPaxId(targetPaxId);
        employeeDTO.setManagerPaxId(targetManagerPaxId);

        PowerMockito.when(security.getPaxId()).thenReturn(submitterPaxId);
        PowerMockito.when(mockedFindbyRelationship.findOne()).thenReturn(null);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(employeeDTO);

        EmployeeDTO networkCheck = networkConnectionService.checkForNetworkConnection(submitterPaxId, targetPaxId);

        assertNull(networkCheck);
    }

    @Test
    public void test_delete_network_connection_happy_path() {
        PowerMockito.doNothing().when(relationshipDao).delete(any(Relationship.class), anyBoolean());

        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setPaxId(targetPaxId);
        employeeDTO.setManagerPaxId(targetManagerPaxId);

        PowerMockito.when(mockedFindbyRelationship.findOne()).thenReturn(new Relationship());
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(employeeDTO);

        networkConnectionService.deleteNetworkConnection(submitterPaxId, targetPaxId);

        Mockito.verify(relationshipDao, times(1)).delete(any(Relationship.class), anyBoolean());
    }

    @Test
    public void test_delete_network_connection_no_connection() {
        PowerMockito.doNothing().when(relationshipDao).delete(any(Relationship.class), anyBoolean());

        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setPaxId(targetPaxId);
        employeeDTO.setManagerPaxId(targetManagerPaxId);

        PowerMockito.when(mockedFindbyRelationship.findOne()).thenReturn(null);
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(employeeDTO);

        networkConnectionService.deleteNetworkConnection(submitterPaxId, targetPaxId);

        Mockito.verify(relationshipDao, times(0)).delete(any(Relationship.class), anyBoolean());
    }

    @Test
    public void basic_get_network_connections_pagination_test() {
        int size = 10;
        int pageSize = 6;
        int nextPageSize = 4;

        List<Relationship> relationships = new ArrayList<>();
        List<EmployeeDTO> connections = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            Relationship relationship = new Relationship();
            relationship.setPaxId2(new Long(i));
            relationships.add(relationship);
            relationships.add(relationship);

            EmployeeDTO connection = new EmployeeDTO();
            connection.setPaxId(new Long(i));
            connection.setFirstName(Character.toString((char)(magicAsciiNumber+i)));
            connection.setLastName(Character.toString((char)(magicAsciiNumber+i)));
            //PowerMockito.when(participantInfoDao.getEmployeeDTO(new Long(i))).thenReturn(connection);
            connections.add(connection);
        }

        PowerMockito.when(mockedFindbyRelationship.find()).thenReturn(relationships);
        PowerMockito.when(participantInfoDao.getInfo(anyList(),anyList()))
                .thenReturn(new ArrayList<EntityDTO>(connections));

        List<EmployeeDTO> networkConnections =
                networkConnectionService.getAllNetworkConnections(submitterPaxId, 1, pageSize);

        // First page size 6
        assertEquals(networkConnections.size(), pageSize);

        networkConnections = networkConnectionService.getAllNetworkConnections(submitterPaxId, 2, pageSize);

        // Second page size 4
        assertEquals(networkConnections.size(), nextPageSize);
    }
}
