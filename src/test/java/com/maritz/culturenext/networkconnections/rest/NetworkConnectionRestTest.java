package com.maritz.culturenext.networkconnections.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import com.google.common.net.HttpHeaders;
import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.networkconnections.constants.NetworkConnectionConstants;
import com.maritz.test.AbstractRestTest;

public class NetworkConnectionRestTest extends AbstractRestTest {
    private static final String PATH = "/rest/participants/";
    private static final String NETWORK_ENDPOINT = "/network-connections/";
    
    @Inject private ApplicationDataService applicationDataService;

    @Before
    public void setup() throws Exception{
        enableNetworkConnectionEndpoints(true);
    }
    
    // Create Network Connections Tests
    @Test
    public void test_create_network_connection_no_ids_given() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                .with(authToken(TestConstants.USER_KUSEY))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[]")
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_create_network_connection_missing_pax_id() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(NetworkConnectionConstants.ERROR_MISSING_PAX_ID));
    }

    @Test
    public void test_create_network_connection_same_pax_id() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":7213}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result
                .getResponse()
                .getContentAsString()
                .contains(NetworkConnectionConstants.ERROR_SAME_PAX_RELATIONSHIP));
    }

    @Test
    public void test_create_network_connection_not_exist() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":-1}]")
        )
                .andExpect(status().isBadRequest())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains(NetworkConnectionConstants.ERROR_INVALID_PAX_ID));
    }

    @Test
    public void test_create_network_connections() throws Exception {
        MvcResult result = mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("12698"));
        assert(result.getResponse().getContentAsString().contains("170"));
        assert(result.getResponse().getContentAsString().contains("165"));
    }

    @Test
    public void test_create_network_connection_with_permission() throws Exception {
        mockMvc.perform(
                post(PATH + "~" + NETWORK_ENDPOINT)
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[]")
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_create_network_connection_with_no_permission() throws Exception {
        mockMvc.perform(
                post(PATH + "~" + NETWORK_ENDPOINT)
                .with(authToken(TestConstants.USER_HAGOPIWL))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[]")
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_get_all_network_connections() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("12698"));
        assert(result.getResponse().getContentAsString().contains("170"));
        assert(result.getResponse().getContentAsString().contains("165"));
    }

    @Test
    public void test_get_all_network_connections_not_authorized() throws Exception {
        MvcResult result = mockMvc.perform(
                get(PATH + 165 + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("ACCESS_DENIED"));
    }

    @Test
    public void test_get_all_network_connections_with_permissions() throws Exception {
        mockMvc.perform(
                get(PATH + "~" + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_get_all_network_connections_with_no_permissions() throws Exception {
        mockMvc.perform(
                get(PATH + "~" + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_get_valid_network_connections_count() throws Exception {
        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + "count")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        Integer count = Integer.parseInt(result.getResponse().getContentAsString().replaceAll("[^0-9]", ""));
        assert(count >= 3);
    }

    @Test
    public void test_get_other_pax_network_connections_count() throws Exception {
        MvcResult result = mockMvc.perform(
                get(PATH + 165 + NETWORK_ENDPOINT + "count")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        Integer count = Integer.parseInt(result.getResponse().getContentAsString().replaceAll("[^0-9]",""));
        assert(count >= 0);
    }

    // Get Network Connections Tests
    @Test
    public void test_check_for_network_connection_invalid_pax_id() throws Exception {
        mockMvc.perform(
                get(PATH + TestConstants.PAX_DAGARFIN + NETWORK_ENDPOINT + TestConstants.PAX_KUSEY)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_check_for_network_connection_not_exist() throws Exception {
        mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + -1)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void test_check_for_network_connection_with_permission() throws Exception {
        mockMvc.perform(
                get(PATH + "~" + NETWORK_ENDPOINT + "count")
                        .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_check_for_network_connection_with_no_permission() throws Exception {
        mockMvc.perform(
                get(PATH + "~" + NETWORK_ENDPOINT + "count")
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_check_for_network_connections() throws Exception {

        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isOk());

        MvcResult result = mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + TestConstants.PAX_DAGARFIN)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

        assert(result.getResponse().getContentAsString().contains("12698"));

    }

    @Test
    public void test_delete_network_connection() throws Exception {

        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isOk());

        mockMvc.perform(
                delete(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + TestConstants.PAX_DAGARFIN)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void test_disabled_create_network_connections() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                post(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .content("[{\"paxId\":12698},{\"paxId\":170},{\"paxId\":165}]")
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_disabled_get_count_network_connections() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + "count")
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_disabled_delete_network_connections() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                delete(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + TestConstants.PAX_DAGARFIN)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_enabled_delete_network_connections_with_permission() throws Exception {

        mockMvc.perform(
                delete(PATH + "~" + NETWORK_ENDPOINT + 2090)
                        .with(authToken(TestConstants.USER_HARWELLM))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void test_disabled_delete_network_connections_with_no_permission() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                delete(PATH + "~" + NETWORK_ENDPOINT + 2090)
                        .with(authToken(TestConstants.USER_HAGOPIWL))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_disabled_get_all_network_connections() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden());
    }

    @Test
    public void test_disabled_check_network_connections() throws Exception {
        enableNetworkConnectionEndpoints(false);

        mockMvc.perform(
                get(PATH + TestConstants.PAX_KUSEY + NETWORK_ENDPOINT + TestConstants.PAX_DAGARFIN)
                        .with(authToken(TestConstants.USER_KUSEY))
        )
                .andExpect(status().isForbidden());
    }

    private void enableNetworkConnectionEndpoints(boolean value) throws Exception {
        ApplicationDataDTO appData = 
                applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_NETWORKING_ENABLED);
        appData.setValue(String.valueOf(value));
        
        applicationDataService.setApplicationData(appData);
    }

}
