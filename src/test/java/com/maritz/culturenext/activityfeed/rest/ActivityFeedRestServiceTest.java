package com.maritz.culturenext.activityfeed.rest;

import org.junit.Test;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.test.web.servlet.MvcResult;

public class ActivityFeedRestServiceTest extends AbstractRestTest {

    private static final String ACTIVITY_FEED_BY_PARTICIPANT_PREPEND = "/rest/participants/";
    private static final String ACTIVITY_FEED_BY_GROUP_PREPEND = "/rest/groups/";
    private static final String ACTIVITY_POSTPEND = "/activity";
    
    private static final String TEST_ACTIVITY_ID = "68123";
    private static final String BAD_TARGET_ID = "BAD13";
    @Autowired
    private Environment environment;
    
    @Test
    public void test_get_activity_feed_happy_path() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_null_page_number() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_negative_page_number() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=-1&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_null_page_size() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_negative_page_size() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=-1")
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_null_page_size_and_multiple_pages() throws Exception {
        if  (Boolean.valueOf(environment.getProperty("activityFeed.enabled"))) {
            MvcResult response  = mockMvc.perform(
                    get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                            "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                            "&page[number]=1000000")
                            .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isBadRequest()).andReturn();

            String responseBody = response.getResponse().getContentAsString();
            assert (responseBody.equalsIgnoreCase("[{\"field\":\"request\",\"code\":\"NO_DATA_ON_PAGE\",\"message\":\"No data found\",\"step\":null}]"));
        } else {
            MvcResult response = mockMvc.perform(
                    get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                            "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                            "&page[number]=1000000")
                            .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();

            String responseBody = response.getResponse().getContentAsString();
            assert (responseBody.equalsIgnoreCase("{\"meta\":null,\"data\":null,\"links\":null}"));
        }
    }
    
    @Test
    public void test_get_activity_feed_bad_target_id() throws Exception {

        if  (Boolean.valueOf(environment.getProperty("activityFeed.enabled"))) {
            MvcResult response = mockMvc.perform(
                    get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + BAD_TARGET_ID + ACTIVITY_POSTPEND +
                            "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                            "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER +
                            "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                            .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isNotFound()).andReturn();

            String responseBody = response.getResponse().getContentAsString();
            assert (responseBody.equalsIgnoreCase(""));
        }else {
            MvcResult response = mockMvc.perform(
                    get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + BAD_TARGET_ID + ACTIVITY_POSTPEND +
                            "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                            "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER +
                            "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                            .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk()).andReturn();

            String responseBody = response.getResponse().getContentAsString();
            assert (responseBody.equalsIgnoreCase("{\"meta\":null,\"data\":null,\"links\":null}"));
        }
    }    
    
    @Test
    public void test_get_activity_feed_by_activity_id_happy_path() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "/" + TEST_ACTIVITY_ID + "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE" + 
                        "&filter=GIVEN,RECEIVED")
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_id_bad_target_id() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_PARTICIPANT_PREPEND + BAD_TARGET_ID + ACTIVITY_POSTPEND +
                        "/" + TEST_ACTIVITY_ID +
                        "?audience=PUBLIC&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isNotFound());
    }    
    
    @Test
    public void test_get_activity_feed_by_group_happy_path() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_null_page_number() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_negative_page_number() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=-1&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_null_page_size() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_negative_page_size() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=-1")
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_null_page_size_and_multiple_pages() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=1000000")
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_get_activity_feed_by_group_bad_target_id() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + "" + ACTIVITY_POSTPEND +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isNotFound());
    }
    
    @Test
    public void test_get_activity_feed_by_group_by_activity_id_happy_path() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + TestConstants.PAX_HARWELLM + ACTIVITY_POSTPEND +
                        "/" + TEST_ACTIVITY_ID + "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE" + 
                        "&filter=GIVEN,RECEIVED")
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isOk());
    }
    
    @Test
    public void test_get_activity_feed_by_group_by_id_bad_target_id() throws Exception {
        
        mockMvc.perform(
                get(ACTIVITY_FEED_BY_GROUP_PREPEND + "" + ACTIVITY_POSTPEND +
                        "/" + TEST_ACTIVITY_ID +
                        "?audience=GROUP&activityType=RECOGNITION,AWARD_CODE&filter=GIVEN,RECEIVED" +
                        "&page[number]=" + ProjectConstants.DEFAULT_PAGE_NUMBER + 
                        "&page[size]=" + ProjectConstants.ITEM_BY_ID_PAGE_SIZE)
                    .with(authToken(TestConstants.USER_HARWELLM))
                ).andExpect(status().isNotFound());
    }    
}
