package com.maritz.culturenext.activityfeed.dao;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ActivityFeedAudienceEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class ActivityFeedGroupInfoTest extends AbstractDatabaseTest {

    private static final Integer DEFAULT_PAGE_NUMBER_AND_SIZE = 1;

    private static final Long SUBMITTER_PAX_ID = 11791L;
    private static final Long UNRELATED_PAX_ID = 3465L;
    private static final Long PRIVATE_RECIPIENT_PAX_ID = 1495L;
    private static final Long PUBLIC_RECIPIENT_PAX_ID = 10926L;
    private static final Long PENDING_RECIPIENT_PAX_ID = 4834L;
    private static final Long REJECTED_RECIPIENT_PAX_ID = 4776L;

    private static final Long SUBMITTER_MANAGER_PAX_ID = 560L;
    private static final Long PRIVATE_MANAGER_PAX_ID = 1080L;
    private static final Long PUBLIC_MANAGER_PAX_ID = 532L;
    private static final Long PENDING_MANAGER_PAX_ID = 32L;
    private static final Long REJECTED_MANAGER_PAX_ID = 2855L;

    private static final Long SUBMITTER_NETWORK_PAX_ID = 14013L;
    private static final Long PRIVATE_NETWORK_PAX_ID = 7571L;
    private static final Long PUBLIC_NETWORK_PAX_ID = 6644L;
    private static final Long PENDING_NETWORK_PAX_ID = 11177L;
    private static final Long REJECTED_NETWORK_PAX_ID = 3373L;

    private static final String GIVEN = "GIVEN";
    private static final String RECEIVED = "RECEIVED";
    private static final String DEFAULT_ACTIVITY_FILTER_LIST = "GIVEN,RECEIVED";
    private static final String DEFAULT_ACTIVITY_TYPE_LIST = "RECOGNITION,AWARD_CODE";
    private static final String TESTING_NOMINATION_COMMENT = "ACTIVITY_FEED_VISIBILITY_TEST";
    private static final String PUBLIC_AWARD_CODE_HEADLINE = "ACTIVITY_FEED_AWARD_CODE_VISIBLE_TEST";
    private static final String ANONYMOUS_AWARD_CODE_HEADLINE = "ACTIVITY_FEED_AWARD_CODE_ANONYMOUS_TEST";
    private static final String PUBLIC_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_PUBLIC_TEST";
    private static final String PRIVATE_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_PRIVATE_TEST";
    private static final String NONE_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_NONE_TEST";

    private static final Long GROUP_TARGET_ID = 1534L;

    private static final String TESTING_PROGRAM_NAME = "ACTIVITY_FEED_VISIBILITY_TEST_PROGRAM";
    private static final String FIELD_DATA_HEADLINE = "ACTIVITY_FEED_FIELD_TEST";

    @Inject
    private ActivityFeedInfoDao activityFeedInfoDao;
    @Inject private ConcentrixDao<GroupsPax> groupsPaxDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<Program> programDao;

    private Long recipientPrefActivityFeedItemId;
    private Long publicActivityFeedItemId;
    private Long privateActivityFeedItemId;
    private Long noneActivityFeedItemId;
    private Long publicAwardCodeActivityFeedItemId;
    private Long anonymousAwardCodeActivityFeedItemId;

    @Before
    public void setup() {
        Nomination recipeintPrefNomination = nominationDao.findBy().where(ProjectConstants.COMMENT).eq(TESTING_NOMINATION_COMMENT).findOne();
        assertNotNull("test data should have been created", recipeintPrefNomination);

        recipientPrefActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(recipeintPrefNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination publicAwardCode = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(PUBLIC_AWARD_CODE_HEADLINE)
                .findOne();
        assertNotNull("test data should have been created", publicAwardCode);

        publicAwardCodeActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(publicAwardCode.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination anonymousAwardCode = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(ANONYMOUS_AWARD_CODE_HEADLINE)
                .findOne();
        assertNotNull("test data should have been created", anonymousAwardCode);

        anonymousAwardCodeActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(anonymousAwardCode.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination publicNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(PUBLIC_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", publicNomination);

        publicActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(publicNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination privateNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(PRIVATE_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", privateNomination);

        privateActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(privateNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination noneNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(NONE_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", noneNomination);

        noneActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_TABLE).eq(noneNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);
    }

    @Test
    public void test_group_rejected_recipient_pax_view() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId,
                null, null
        );
    }

    @Test
    public void test_group_pending_recipient_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_recipient_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_private_recipient_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_unrelated_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_pax_views(){
        // group received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_manager_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_private_manager_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_manager_pax_views(){
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_pending_manager_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

    }

    @Test
    public void test_group_rejected_manager_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_network_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_private_network_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_network_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_pending_network_pax_views() {

        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_rejected_network_pax_views() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_unrelated_pax_views_public_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_recipient_pax_views_public_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_manager_pax_views_public_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_network_pax_views_public_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_network_pax_views_public_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_unrelated_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_recipient_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_manager_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_manager_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_submitter_network_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_group_public_network_pax_views_anonymous_award_code() {
        // group given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.GROUP.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_get_activity_feed_by_group_validate_data() {

        List<Map<String, Object>> activityFeedGroupEntries =
                activityFeedInfoDao.getActivityFeedGroup(TestConstants.PAX_HARWELLM, GROUP_TARGET_ID,
                        DEFAULT_ACTIVITY_TYPE_LIST, DEFAULT_ACTIVITY_FILTER_LIST,
                        DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, null);

        assertNotNull("Should be something returned for this group", activityFeedGroupEntries);
        assertFalse("Should have data for this group", activityFeedGroupEntries.isEmpty());

        for (Map<String, Object> activityFeedGroupEntry : activityFeedGroupEntries) {

            Long fromPaxId = (Long) activityFeedGroupEntry.get(ProjectConstants.SUBMITTER_PAX_ID);
            Long toPaxId = (Long) activityFeedGroupEntry.get(ProjectConstants.RECIPIENT_PAX_ID);

            GroupsPax groupsPaxFromPax = groupsPaxDao.findBy()
                    .where(ProjectConstants.GROUP_ID)
                    .eq(GROUP_TARGET_ID)
                    .and(ProjectConstants.PAX_ID)
                    .eq(fromPaxId)
                    .findOne();

            GroupsPax groupsPaxToPax = groupsPaxDao.findBy()
                    .where(ProjectConstants.GROUP_ID)
                    .eq(GROUP_TARGET_ID)
                    .and(ProjectConstants.PAX_ID)
                    .eq(toPaxId)
                    .findOne();

            if (groupsPaxFromPax == null && groupsPaxToPax == null) {
                fail("recipient or submitter should be in the group");
            }
        }
    }

    @Test
    public void validateGroupsCardinality (){
        List<Map<String, Object>> activityFeedGroupEntries =
                activityFeedInfoDao.getActivityFeedGroup(8571L, 1534L,
                        "RECOGNITION,AWARD_CODE", "GIVEN",
                        1, 200, null);
        assertNotNull(activityFeedGroupEntries);
        System.out.println("elements found:"+ activityFeedGroupEntries.size());
        assertEquals(107,activityFeedGroupEntries.size());
    }

    private void validateSubmitterReceiverData(

            Long loggedInPaxId, Long pathPaxId, String audience, String activityTypeListString,
            String statusTypeListString, String filterListString, Long activityFeedItemId,
            Long expectedSubmitterPaxId, Long expectedRecipientPaxId
    ) {

        List<Map<String, Object>> activityFeedDataList = null;
        if (ActivityFeedAudienceEnum.GROUP.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedGroup(
                    loggedInPaxId, pathPaxId, activityTypeListString, filterListString,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId);
        }
        assertNotNull("should have an activity feed list, even if it's empty", activityFeedDataList);

        if (expectedSubmitterPaxId == null && expectedRecipientPaxId == null) {
            assertTrue("should have no data", activityFeedDataList.isEmpty());
            return; // nothing more to test
        }


        assertEquals("should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), activityFeedDataList.size());

        Map<String, Object> activityFeedEntry = activityFeedDataList.get(0);
        assertNotNull("should have data", activityFeedEntry);

        Integer totalResults = (Integer) activityFeedEntry.get(ProjectConstants.TOTAL_RESULTS_KEY);
        assertNotNull("totalResults should have a value", totalResults);
        assertEquals("totalResults should be 1", DEFAULT_PAGE_NUMBER_AND_SIZE, totalResults);

        Long activityFeedIdResult = (Long) activityFeedEntry.get(ProjectConstants.NEWSFEED_ITEM_ID);
        assertNotNull("activityFeedIdResult should have a value", activityFeedIdResult);
        assertEquals("activityFeedIdResult should be the requested id", activityFeedItemId, activityFeedIdResult);

        Date createDate = (Date) activityFeedEntry.get(ProjectConstants.CREATE_DATE);
        assertNotNull("createDate should have a value", createDate);

        Long nominationId = (Long) activityFeedEntry.get(ProjectConstants.NOMINATION_ID);
        assertNotNull("nominationId should have a value", nominationId);

        Long submitterPaxId = (Long) activityFeedEntry.get(ProjectConstants.SUBMITTER_PAX_ID);
        assertEquals("submitterPaxId should be expected value", expectedSubmitterPaxId, submitterPaxId); // Could be null

        Long recipientPaxId = (Long) activityFeedEntry.get(ProjectConstants.RECIPIENT_PAX_ID);
        assertEquals("recipientPaxId should be expected value", expectedRecipientPaxId, recipientPaxId); // Could be null
    }
}
