package com.maritz.culturenext.activityfeed.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.GroupsPax;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.ActivityFeedAudienceEnum;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.activityfeed.constants.ActivityFeedConstants;
import com.maritz.culturenext.activityfeed.dao.ActivityFeedInfoDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class ActivityFeedInfoDaoTest extends AbstractDatabaseTest {

    private static final Integer DEFAULT_PAGE_NUMBER_AND_SIZE = 1;

    private static final Long SUBMITTER_PAX_ID = 11791L;
    private static final Long UNRELATED_PAX_ID = 3465L;
    private static final Long PRIVATE_RECIPIENT_PAX_ID = 1495L;
    private static final Long PUBLIC_RECIPIENT_PAX_ID = 10926L;
    private static final Long PENDING_RECIPIENT_PAX_ID = 4834L;
    private static final Long REJECTED_RECIPIENT_PAX_ID = 4776L;

    private static final Long SUBMITTER_MANAGER_PAX_ID = 560L;
    private static final Long PRIVATE_MANAGER_PAX_ID = 1080L;
    private static final Long PUBLIC_MANAGER_PAX_ID = 532L;
    private static final Long PENDING_MANAGER_PAX_ID = 32L;
    private static final Long REJECTED_MANAGER_PAX_ID = 2855L;

    private static final Long SUBMITTER_NETWORK_PAX_ID = 14013L;
    private static final Long PRIVATE_NETWORK_PAX_ID = 7571L;
    private static final Long PUBLIC_NETWORK_PAX_ID = 6644L;
    private static final Long PENDING_NETWORK_PAX_ID = 11177L;
    private static final Long REJECTED_NETWORK_PAX_ID = 3373L;

    private static final String GIVEN = "GIVEN";
    private static final String RECEIVED = "RECEIVED";
    private static final String DEFAULT_ACTIVITY_FILTER_LIST = "GIVEN,RECEIVED";
    private static final String DEFAULT_ACTIVITY_TYPE_LIST = "RECOGNITION,AWARD_CODE";
    private static final String TESTING_NOMINATION_COMMENT = "ACTIVITY_FEED_VISIBILITY_TEST";
    private static final String PUBLIC_AWARD_CODE_HEADLINE = "ACTIVITY_FEED_AWARD_CODE_VISIBLE_TEST";
    private static final String ANONYMOUS_AWARD_CODE_HEADLINE = "ACTIVITY_FEED_AWARD_CODE_ANONYMOUS_TEST";
    private static final String PUBLIC_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_PUBLIC_TEST";
    private static final String PRIVATE_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_PRIVATE_TEST";
    private static final String NONE_PROGRAM_VISIBILITY_NOMINATION_COMMENT = "ACTIVITY_FEED_PROGRAM_VISIBILITY_NONE_TEST";

    private static final Long GROUP_TARGET_ID = 1534L;

    private static final String TESTING_PROGRAM_NAME = "ACTIVITY_FEED_VISIBILITY_TEST_PROGRAM";
    private static final String FIELD_DATA_HEADLINE = "ACTIVITY_FEED_FIELD_TEST";

    @Inject private ActivityFeedInfoDao activityFeedInfoDao;
    @Inject private ConcentrixDao<GroupsPax> groupsPaxDao;
    @Inject private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private ConcentrixDao<Program> programDao;

    private Long recipientPrefActivityFeedItemId;
    private Long publicActivityFeedItemId;
    private Long privateActivityFeedItemId;
    private Long noneActivityFeedItemId;
    private Long publicAwardCodeActivityFeedItemId;
    private Long anonymousAwardCodeActivityFeedItemId;

    @Before
    public void setup() {
        Nomination recipeintPrefNomination = nominationDao.findBy().where(ProjectConstants.COMMENT).eq(TESTING_NOMINATION_COMMENT).findOne();
        assertNotNull("test data should have been created", recipeintPrefNomination);

        recipientPrefActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(recipeintPrefNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination publicAwardCode = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(PUBLIC_AWARD_CODE_HEADLINE)
                .findOne();
        assertNotNull("test data should have been created", publicAwardCode);

        publicAwardCodeActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(publicAwardCode.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination anonymousAwardCode = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(ANONYMOUS_AWARD_CODE_HEADLINE)
                .findOne();
        assertNotNull("test data should have been created", anonymousAwardCode);

        anonymousAwardCodeActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(anonymousAwardCode.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination publicNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(PUBLIC_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", publicNomination);

        publicActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(publicNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination privateNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(PRIVATE_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", privateNomination);

        privateActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_ID).eq(privateNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);

        Nomination noneNomination = nominationDao.findBy()
                .where(ProjectConstants.COMMENT).eq(NONE_PROGRAM_VISIBILITY_NOMINATION_COMMENT)
                .findOne();
        assertNotNull("test data should have been created", noneNomination);

        noneActivityFeedItemId = newsfeedItemDao.findBy()
                .where(ProjectConstants.TARGET_TABLE).eq(noneNomination.getId())
                .and(ProjectConstants.TARGET_TABLE).eq(TableName.NOMINATION.name())
                .findOne(ProjectConstants.ID, Long.class);
    }

    @Test
    public void test_submitter_pax_views() {

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPEINT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID because it's lowest approved pax id
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_unrelated_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

    }

    @Test
    public void test_private_recipient_pax_views() {

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );
    }


    @Test
    public void test_public_recipient_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_pending_recipient_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

    }


    @Test
    public void test_rejected_recipient_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_submitter_manager_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

    }

    @Test
    public void test_private_manager_pax_views() {

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_public_manager_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_pending_manager_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_rejected_manager_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_submitter_network_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_private_network_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID (network doesn't override privacy)
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PRIVATE_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_network_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_pending_network_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PENDING_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_rejected_network_pax_views() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, recipientPrefActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                recipientPrefActivityFeedItemId, null, null
        );

        // public feed
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (private) (given,received)
        // should see PRIVATE_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PRIVATE_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, noneActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, noneActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                noneActivityFeedItemId, null, null
        );

        // public feed
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (private) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PRIVATE_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (public) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (pending) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, PENDING_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // participant feed (rejected) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, REJECTED_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, privateActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, privateActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                REJECTED_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                privateActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_submitter_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_unrelated_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_recipient_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_submitter_manager_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_public_manager_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_submitter_network_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_network_pax_views_public_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, publicAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                publicAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_submitter_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_unrelated_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                UNRELATED_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_recipient_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_RECIPIENT_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_submitter_manager_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_manager_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given,received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_MANAGER_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );
    }

    @Test
    public void test_submitter_network_pax_views_anonymous_award_code() {

        // public feed
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                SUBMITTER_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_public_network_pax_views_anonymous_award_code() {

        // public feed
        // should see PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.PUBLIC.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (submitter) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (submitter) (given,received)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, SUBMITTER_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (given)
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // participant feed (public) (received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // participant feed (public) (given,received)
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, PUBLIC_RECIPIENT_PAX_ID, ActivityFeedAudienceEnum.PARTICIPANT.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // network received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // network given,received
        // should see no submitter, PUBLIC_RECIPIENT_PAX_ID
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.NETWORK.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, ActivityFeedConstants.ANONYMOUS_SUBMITTER_ID, PUBLIC_RECIPIENT_PAX_ID
        );

        // direct reports given
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, GIVEN, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, RECEIVED, anonymousAwardCodeActivityFeedItemId, null, null
        );

        // direct reports given,received
        // should see nothing
        validateSubmitterReceiverData(
                PUBLIC_NETWORK_PAX_ID, null, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(),
                DEFAULT_ACTIVITY_TYPE_LIST, null, ActivityFeedConstants.ACTIVITY_FEED_DEFAULT_FILTER_VALUE,
                anonymousAwardCodeActivityFeedItemId, null, null
        );
    }

    @Test
    public void test_activity_feed_data_pending_network_pax() {
        List<Map<String, Object>> activityFeedDataList = activityFeedInfoDao.getActivityFeedDetails(
                PENDING_NETWORK_PAX_ID, recipientPrefActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", activityFeedDataList);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), activityFeedDataList.size());

        Map<String, Object> activityFeedEntry = activityFeedDataList.get(0);
        assertNotNull("should have data", activityFeedEntry);

        Long newsfeedItemId = (Long) activityFeedEntry.get(ProjectConstants.NEWSFEED_ITEM_ID);
        assertEquals("should be the right activity id", recipientPrefActivityFeedItemId, newsfeedItemId);

        String activityType = (String) activityFeedEntry.get(ProjectConstants.ACTIVITY_TYPE);
        assertEquals("Should be RECOGNITION", NewsfeedItemTypeCode.RECOGNITION.name(), activityType);

        Integer recipientCount = (Integer) activityFeedEntry.get(ProjectConstants.RECIPIENT_COUNT);
        assertEquals("Should be 4 recipients", 4, recipientCount.intValue());

        Integer approvedRecipientCount = (Integer) activityFeedEntry.get(ProjectConstants.APPROVED_RECIPIENT_COUNT);
        assertEquals("Should be 2 approved recipients", 2, approvedRecipientCount.intValue());

        Integer directReportCount = (Integer) activityFeedEntry.get(ProjectConstants.DIRECT_REPORT_COUNT);
        assertEquals("Should be no direct reports", 0, directReportCount.intValue());

        Double pointsIssued = (Double) activityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("no points issued", 0, pointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Nomination nomination = nominationDao.findBy().where(ProjectConstants.COMMENT).eq(TESTING_NOMINATION_COMMENT).findOne();
        Program program = programDao.findBy().where(ProjectConstants.PROGRAM_NAME).eq(TESTING_PROGRAM_NAME).findOne();

        Long nominationId = (Long) activityFeedEntry.get(ProjectConstants.NOMINATION_ID);
        assertEquals("nomination id should match", nomination.getId(), nominationId);

        Long programId = (Long) activityFeedEntry.get(ProjectConstants.PROGRAM_ID);
        assertEquals("nomination id should match", program.getProgramId(), programId);

        Long imageId = (Long) activityFeedEntry.get(ProjectConstants.IMAGE_ID);
        assertEquals("should be image id 40", 40, imageId.intValue());

        String recognitionCriteriaJson = (String) activityFeedEntry.get(ProjectConstants.RECOGNITION_CRITERIA_JSON);
        assertEquals("recognition criteria json should be populated", "[{\"id\":1, \"displayName\":\"Achievement\"}]", recognitionCriteriaJson);

        String headline = (String) activityFeedEntry.get(ProjectConstants.HEADLINE);
        assertEquals("headline should be populated", TESTING_NOMINATION_COMMENT, headline);

        Integer likeCount = (Integer) activityFeedEntry.get(ProjectConstants.LIKE_COUNT);
        assertEquals("two likes", 2, likeCount.intValue());

        // logged in pax has liked, so it should be displayed instead of latest
        Long likePaxId = (Long) activityFeedEntry.get(ProjectConstants.LIKE_PAX_ID);
        assertEquals("logged in pax should be like pax", PENDING_NETWORK_PAX_ID, likePaxId);

        String privateMessage = (String) activityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNull("logged in pax should not be able to see private message", privateMessage);

        Integer commenterCount = (Integer) activityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("commenterCount should be 0", 0, commenterCount.intValue());

        Integer commentCount = (Integer) activityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("commentCount should be 0", 0, commentCount.intValue());

        Integer myCommentCount = (Integer) activityFeedEntry.get(ProjectConstants.MY_COMMENT_COUNT);
        assertEquals("myCommentCount should be 0", 0, myCommentCount.intValue());

        Long commentId = (Long) activityFeedEntry.get(ProjectConstants.COMMENT_ID);
        assertNull("no comments", commentId);

        Date commentDate = (Date) activityFeedEntry.get(ProjectConstants.COMMENT_DATE);
        assertNull("no comments", commentDate);

        String commentText = (String) activityFeedEntry.get(ProjectConstants.COMMENT_TEXT);
        assertNull("no comments", commentText);

        Long commentPaxId = (Long) activityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertNull("no comments", commentPaxId);

        Integer myRaisedCount = (Integer) activityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user has given no raises for this nomination", 0, myRaisedCount.intValue());

        Integer raisedCount = (Integer) activityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("no raises have been given", 0, raisedCount.intValue());

        Double raisedPointTotal = (Double) activityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("no raises have been given", 0, raisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long raisedPaxId = (Long) activityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertNull("no raises have been given", raisedPaxId);

        String awardCode = (String) activityFeedEntry.get(ProjectConstants.AWARD_CODE);
        assertNull("recognition isn't an award code", awardCode);

        String loggedInUserStatus = (String) activityFeedEntry.get(ProjectConstants.LOGGED_IN_USER_STATUS);
        assertEquals("logged in user is active", StatusTypeCode.ACTIVE.name(), loggedInUserStatus);

        Boolean programIsActive = (Boolean) activityFeedEntry.get(ProjectConstants.PROGRAM_IS_ACTIVE);
        assertTrue("program is active", programIsActive);

        Boolean canRaiseAnyTier = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_ANY_TIER);
        assertTrue("at least one tier tied to the program can be raised", canRaiseAnyTier);

        Boolean canRaiseTier = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_TIER);
        assertFalse("can raise the recognition that was given without points", canRaiseTier);
        // NOTE: rec isn't tied to tier but an active tier exists for the program

        Boolean canRaiseProgramBudget = (Boolean) activityFeedEntry.get(ProjectConstants.CAN_RAISE_PROGRAM_BUDGET);
        assertFalse("cannot give from a budget tied to this program", canRaiseProgramBudget);
        // NOTE: no budgets configured for this program.  bad data but gets the job done

        Boolean givenRaise = (Boolean) activityFeedEntry.get(ProjectConstants.GIVEN_RAISE);
        assertFalse("logged in user has given no raises for this nomination", givenRaise);
    }

    @Test
    public void test_data_submitter_pax() {
        List<Map<String, Object>> activityFeedDataList = activityFeedInfoDao.getActivityFeedDetails(
                SUBMITTER_PAX_ID, recipientPrefActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", activityFeedDataList);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), activityFeedDataList.size());

        Map<String, Object> activityFeedEntry = activityFeedDataList.get(0);
        assertNotNull("should have data", activityFeedEntry);

        // other things have been tested in test_activity_feed_data_pending_network_pax, so just check a couple things here

        Long likePaxId = (Long) activityFeedEntry.get(ProjectConstants.LIKE_PAX_ID);
        assertEquals("logged in pax should be like pax", PRIVATE_MANAGER_PAX_ID, likePaxId);
        // PRIVATE_MANAGER_PAX_ID is LATEST pax who liked the entry

        String privateMessage = (String) activityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNotNull("logged in pax is submitter, so they should be able to see private message", privateMessage);

    }

    @Test
    public void test_private_message_points_raises_comment() {
        Nomination nomination = nominationDao.findBy().where(ProjectConstants.HEADLINE_COMMENT).eq(FIELD_DATA_HEADLINE).findOne();
        assertNotNull("test data should have been created", nomination);

        Long dataActivityFeedItemId = newsfeedItemDao.findBy().where(ProjectConstants.TARGET_ID).eq(nomination.getId()).findOne(ProjectConstants.ID, Long.class);

        List<Map<String, Object>> submitterActivityFeed = activityFeedInfoDao.getActivityFeedDetails(
                SUBMITTER_PAX_ID, dataActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", submitterActivityFeed);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), submitterActivityFeed.size());

        Map<String, Object> submitterActivityFeedEntry = submitterActivityFeed.get(0);
        assertNotNull("should have data", submitterActivityFeedEntry);

        String submitterPrivateMessage = (String) submitterActivityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNotNull("logged in pax is submitter, so they should be able to see private message", submitterPrivateMessage);

        Integer submitterCommenterCount = (Integer) submitterActivityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("submitterCommenterCount should be 1", 1, submitterCommenterCount.intValue());

        Integer submitterCommentCount = (Integer) submitterActivityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("submitterCommentCount should be 1", 1, submitterCommentCount.intValue());

        Long submitterCommentPaxId = (Long) submitterActivityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertEquals("submitterCommentPaxId should be PUBLIC_RECIPIENT_PAX_ID", PUBLIC_RECIPIENT_PAX_ID, submitterCommentPaxId);

        Double submitterPointsIssued = (Double) submitterActivityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("submitter should see 30 points issued", 30, submitterPointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Integer submitterMyRaisedCount = (Integer) submitterActivityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user has given no raises for this nomination", 0, submitterMyRaisedCount.intValue());

        Integer submitterRaisedCount = (Integer) submitterActivityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("2 raises have been given", 2, submitterRaisedCount.intValue());

        Double submitterRaisedPointTotal = (Double) submitterActivityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("submitter can't see raises given", 0, submitterRaisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long submitterRaisedPaxId = (Long) submitterActivityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertNull("submitter can't see raises given", submitterRaisedPaxId);

        // received, received raise (not from manager receiver)
        List<Map<String, Object>> receiverActivityFeed = activityFeedInfoDao.getActivityFeedDetails(
                PENDING_RECIPIENT_PAX_ID, dataActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", receiverActivityFeed);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), receiverActivityFeed.size());

        Map<String, Object> receiverActivityFeedEntry = receiverActivityFeed.get(0);
        assertNotNull("should have data", receiverActivityFeedEntry);

        String receiverPrivateMessage = (String) receiverActivityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNotNull("logged in pax is receiver, so they should be able to see private message", receiverPrivateMessage);

        Integer receiverCommenterCount = (Integer) receiverActivityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("receiverCommenterCount should be 1", 1, receiverCommenterCount.intValue());

        Integer receiverCommentCount = (Integer) receiverActivityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("receiverCommentCount should be 1", 1, receiverCommentCount.intValue());

        Long receiverCommentPaxId = (Long) receiverActivityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertEquals("receiverCommentPaxId should be PUBLIC_RECIPIENT_PAX_ID", PUBLIC_RECIPIENT_PAX_ID, receiverCommentPaxId);

        Double receiverPointsIssued = (Double) receiverActivityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("logged in user should see 10 points issued", 10, receiverPointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Integer receiverMyRaisedCount = (Integer) receiverActivityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user has received 1 raise for this nomination", 1, receiverMyRaisedCount.intValue());

        Integer receiverRaisedCount = (Integer) receiverActivityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("2 raises have been given", 2, receiverRaisedCount.intValue());

        Double receiverRaisedPointTotal = (Double) receiverActivityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("logged in user has received 1 raise for this nomination", 10, receiverRaisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long receiverRaisedPaxId = (Long) receiverActivityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertEquals("logged in user has received 1 raise for this nomination from PENDING_NETWORK_PAX_ID", PENDING_NETWORK_PAX_ID, receiverRaisedPaxId);

        // self and direct report received, raised direct report
        List<Map<String, Object>> receiverManagerActivityFeed = activityFeedInfoDao.getActivityFeedDetails(
                PUBLIC_MANAGER_PAX_ID, dataActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", receiverManagerActivityFeed);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), receiverManagerActivityFeed.size());

        Map<String, Object> receiverManagerActivityFeedEntry = receiverManagerActivityFeed.get(0);
        assertNotNull("should have data", receiverManagerActivityFeedEntry);

        String receiverManagerPrivateMessage = (String) receiverManagerActivityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNotNull("logged in pax is receiverManager, so they should be able to see private message", receiverManagerPrivateMessage);

        Integer receiverManagerCommenterCount = (Integer) receiverManagerActivityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("receiverManagerCommenterCount should be 1", 1, receiverManagerCommenterCount.intValue());

        Integer receiverManagerCommentCount = (Integer) receiverManagerActivityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("receiverManagerCommentCount should be 1", 1, receiverManagerCommentCount.intValue());

        Long receiverManagerCommentPaxId = (Long) receiverManagerActivityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertEquals("receiverManagerCommentPaxId should be PUBLIC_RECIPIENT_PAX_ID", PUBLIC_RECIPIENT_PAX_ID, receiverManagerCommentPaxId);

        Double receiverManagerPointsIssued = (Double) receiverManagerActivityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("logged in user should see 20 points issued", 20, receiverManagerPointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Integer receiverManagerMyRaisedCount = (Integer) receiverManagerActivityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user has given 1 raise for this nomination (self and direct report)", 1, receiverManagerMyRaisedCount.intValue());

        Integer receiverManagerRaisedCount = (Integer) receiverManagerActivityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("2 raises have been given", 2, receiverManagerRaisedCount.intValue());

        Double receiverManagerRaisedPointTotal = (Double) receiverManagerActivityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("logged in user has given 1 raise for this nomination", 10, receiverManagerRaisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long receiverManagerRaisedPaxId = (Long) receiverManagerActivityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertEquals("logged in user has given 1 raise for this nomination, should see self", PUBLIC_MANAGER_PAX_ID, receiverManagerRaisedPaxId);

        // logged in user submitted raise, did not receive recg
        List<Map<String, Object>> raiseSubmitterActivityFeed = activityFeedInfoDao.getActivityFeedDetails(
                PENDING_NETWORK_PAX_ID, dataActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", raiseSubmitterActivityFeed);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), raiseSubmitterActivityFeed.size());

        Map<String, Object> raiseSubmitterActivityFeedEntry = raiseSubmitterActivityFeed.get(0);
        assertNotNull("should have data", raiseSubmitterActivityFeedEntry);

        String raiseSubmitterPrivateMessage = (String) raiseSubmitterActivityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNull("logged in pax should not be able to see private message", raiseSubmitterPrivateMessage);

        Integer raiseSubmitterCommenterCount = (Integer) raiseSubmitterActivityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("raiseSubmitterCommenterCount should be 1", 1, raiseSubmitterCommenterCount.intValue());

        Integer raiseSubmitterCommentCount = (Integer) raiseSubmitterActivityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("raiseSubmitterCommentCount should be 1", 1, raiseSubmitterCommentCount.intValue());

        Long raiseSubmitterCommentPaxId = (Long) raiseSubmitterActivityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertEquals("raiseSubmitterCommentPaxId should be PUBLIC_RECIPIENT_PAX_ID", PUBLIC_RECIPIENT_PAX_ID, raiseSubmitterCommentPaxId);

        Double raiseSubmitterPointsIssued = (Double) raiseSubmitterActivityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("logged in user can't see points issued", 0, raiseSubmitterPointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Integer raiseSubmitterMyRaisedCount = (Integer) raiseSubmitterActivityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user has given 1 raise for this nomination", 1, raiseSubmitterMyRaisedCount.intValue());

        Integer raiseSubmitterRaisedCount = (Integer) raiseSubmitterActivityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("2 raises have been given", 2, raiseSubmitterRaisedCount.intValue());

        Double raiseSubmitterRaisedPointTotal = (Double) raiseSubmitterActivityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("logged in user has given 1 raise for this nomination", 10, raiseSubmitterRaisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long raiseSubmitterRaisedPaxId = (Long) raiseSubmitterActivityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertEquals("logged in user has given 1 raise for this nomination, should see self", PENDING_NETWORK_PAX_ID, raiseSubmitterRaisedPaxId);

        // unrelated pax viewing from public feed
        List<Map<String, Object>> unrelatedActivityFeed = activityFeedInfoDao.getActivityFeedDetails(
                UNRELATED_PAX_ID, dataActivityFeedItemId.toString(), Boolean.FALSE
        );

        assertNotNull("Should have data", unrelatedActivityFeed);
        assertEquals("Should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), unrelatedActivityFeed.size());

        Map<String, Object> unrelatedActivityFeedEntry = unrelatedActivityFeed.get(0);
        assertNotNull("should have data", unrelatedActivityFeedEntry);

        String unrelatedPrivateMessage = (String) unrelatedActivityFeedEntry.get(ProjectConstants.PRIVATE_MESSAGE);
        assertNull("logged in pax should not be able to see private message", unrelatedPrivateMessage);

        Integer unrelatedCommenterCount = (Integer) unrelatedActivityFeedEntry.get(ProjectConstants.COMMENTER_COUNT);
        assertEquals("unrelatedCommenterCount should be 1", 1, unrelatedCommenterCount.intValue());

        Integer unrelatedCommentCount = (Integer) unrelatedActivityFeedEntry.get(ProjectConstants.COMMENT_COUNT);
        assertEquals("unrelatedCommentCount should be 1", 1, unrelatedCommentCount.intValue());

        Long unrelatedCommentPaxId = (Long) unrelatedActivityFeedEntry.get(ProjectConstants.COMMENT_PAX_ID);
        assertEquals("unrelatedCommentPaxId should be PUBLIC_RECIPIENT_PAX_ID", PUBLIC_RECIPIENT_PAX_ID, unrelatedCommentPaxId);

        Double unrelatedPointsIssued = (Double) unrelatedActivityFeedEntry.get(ProjectConstants.POINTS_ISSUED);
        assertEquals("logged in user can't see points issued", 0, unrelatedPointsIssued.intValue()); // for some reason, the version comparing doubles is deprecated

        Integer unrelatedMyRaisedCount = (Integer) unrelatedActivityFeedEntry.get(ProjectConstants.MY_RAISED_COUNT);
        assertEquals("logged in user can't see raises", 0, unrelatedMyRaisedCount.intValue());

        Integer unrelatedRaisedCount = (Integer) unrelatedActivityFeedEntry.get(ProjectConstants.RAISED_COUNT);
        assertEquals("2 raises have been given", 2, unrelatedRaisedCount.intValue());

        Double unrelatedRaisedPointTotal = (Double) unrelatedActivityFeedEntry.get(ProjectConstants.RAISED_POINT_TOTAL);
        assertEquals("logged in user can't see raises", 0, unrelatedRaisedPointTotal.intValue()); // for some reason, the version comparing doubles is deprecated

        Long unrelatedRaisedPaxId = (Long) unrelatedActivityFeedEntry.get(ProjectConstants.RAISED_PAX_ID);
        assertNull("logged in user can't see raises", unrelatedRaisedPaxId);
    }

    private void validateSubmitterReceiverData(
            Long loggedInPaxId, Long pathPaxId, String audience, String activityTypeListString,
            String statusTypeListString, String filterListString, Long activityFeedItemId,
            Long expectedSubmitterPaxId, Long expectedRecipientPaxId
    ) {

        List<Map<String, Object>> activityFeedDataList = null;
        if (ActivityFeedAudienceEnum.DIRECT_REPORTS.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedDirectReports(
                    loggedInPaxId, activityTypeListString, filterListString,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId
            );
        }
        else if (ActivityFeedAudienceEnum.NETWORK.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedNetwork(
                    loggedInPaxId, activityTypeListString, filterListString,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId
            );
        }
        else if (ActivityFeedAudienceEnum.PARTICIPANT.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedParticipant(
                    loggedInPaxId, pathPaxId, activityTypeListString, filterListString,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId
            );
        }
        else if (ActivityFeedAudienceEnum.PUBLIC.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedPublic(
                    loggedInPaxId, activityTypeListString, DEFAULT_PAGE_NUMBER_AND_SIZE,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId, Boolean.FALSE, Boolean.FALSE
            );
            List<Map<String, Object>> resultSet = activityFeedInfoDao.getActivityFeedPublic(loggedInPaxId, activityTypeListString, DEFAULT_PAGE_NUMBER_AND_SIZE,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId, Boolean.FALSE, Boolean.FALSE);

        }
        else if (ActivityFeedAudienceEnum.GROUP.name().equalsIgnoreCase(audience)) {
            activityFeedDataList = activityFeedInfoDao.getActivityFeedGroup(
                    loggedInPaxId, pathPaxId, activityTypeListString, filterListString,
                    DEFAULT_PAGE_NUMBER_AND_SIZE, DEFAULT_PAGE_NUMBER_AND_SIZE, activityFeedItemId);
        }

        assertNotNull("should have an activity feed list, even if it's empty", activityFeedDataList);

        if (expectedSubmitterPaxId == null && expectedRecipientPaxId == null) {
            assertTrue("should have no data", activityFeedDataList.isEmpty());
            return; // nothing more to test
        }

        assertEquals("should be one result", DEFAULT_PAGE_NUMBER_AND_SIZE.intValue(), activityFeedDataList.size());

        Map<String, Object> activityFeedEntry = activityFeedDataList.get(0);
        assertNotNull("should have data", activityFeedEntry);

        Integer totalResults = (Integer) activityFeedEntry.get(ProjectConstants.TOTAL_RESULTS_KEY);
        assertNotNull("totalResults should have a value", totalResults);
        assertEquals("totalResults should be 1", DEFAULT_PAGE_NUMBER_AND_SIZE, totalResults);

        Long activityFeedIdResult = (Long) activityFeedEntry.get(ProjectConstants.NEWSFEED_ITEM_ID);
        assertNotNull("activityFeedIdResult should have a value", activityFeedIdResult);
        assertEquals("activityFeedIdResult should be the requested id", activityFeedItemId, activityFeedIdResult);

        Date createDate = (Date) activityFeedEntry.get(ProjectConstants.CREATE_DATE);
        assertNotNull("createDate should have a value", createDate);

        Long nominationId = (Long) activityFeedEntry.get(ProjectConstants.NOMINATION_ID);
        assertNotNull("nominationId should have a value", nominationId);

        Long submitterPaxId = (Long) activityFeedEntry.get(ProjectConstants.SUBMITTER_PAX_ID);
        assertEquals("submitterPaxId should be expected value", expectedSubmitterPaxId, submitterPaxId); // Could be null

        Long recipientPaxId = (Long) activityFeedEntry.get(ProjectConstants.RECIPIENT_PAX_ID);
        assertEquals("recipientPaxId should be expected value", expectedRecipientPaxId, recipientPaxId); // Could be null
    }
}
