package com.maritz.culturenext.activityfeed.service;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.activityfeed.services.impl.ActivityFeedServiceImpl;
import com.maritz.culturenext.newsfeed.dto.NewsfeedNominationDTO;
import com.maritz.test.AbstractDatabaseTest;

public class ActivityFeedServiceRetrievalTest  extends AbstractDatabaseTest {
    private static final String POINTS = "points";
    private static final String LONG_DESC_FOR_POINTS = "long desc for points";
    @Inject private ActivityFeedServiceImpl activityFeedService;


    @Test
    public void testPopulatePayoutTypeInformation() {
        NewsfeedNominationDTO nominationDTO = new NewsfeedNominationDTO();
        nominationDTO.setPayoutType("DPP");
        activityFeedService.populatePayoutTypeInformation("en_US",nominationDTO);
        assertNotNull("nominationDTO was null and should not!",nominationDTO);
        assertNotNull("nominationDTO display name was null and should not", nominationDTO.getPayoutDisplayName());
        assertEquals("display name was not equals",POINTS, nominationDTO.getPayoutDisplayName());
        assertEquals("Description was not equal",LONG_DESC_FOR_POINTS, nominationDTO.getPayoutDescription());
    }

}
