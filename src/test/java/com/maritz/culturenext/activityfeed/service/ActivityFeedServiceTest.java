package com.maritz.culturenext.activityfeed.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Lookup;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.activityfeed.dao.ActivityFeedInfoDao;
import com.maritz.culturenext.activityfeed.services.impl.ActivityFeedServiceImpl;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.ActivityFeedAudienceEnum;
import com.maritz.culturenext.enums.ActivityFeedRaiseStatusEnum;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.newsfeed.constants.NewsfeedItemTypeCode;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.newsfeed.dto.NewsfeedNominationDTO;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.util.TranslationUtil;
import com.maritz.test.AbstractMockTest;

public class ActivityFeedServiceTest extends AbstractMockTest {

    @Mock private ActivityFeedInfoDao activityFeedInfoDao;
    @Mock private Environment environment;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private Security security;
    @Mock private ConcentrixDao<Lookup> lookupDao;
    @Mock private TranslationUtil translationUtil;

    @InjectMocks private ActivityFeedServiceImpl activityFeedService;

    private ActivityFeedServiceImpl spyActivityFeedService;

    private static final String TEST_PAYOUT_TYPE = "DPP";

    @Before
    public void setup() {
        spyActivityFeedService = spy(activityFeedService);
        when(environment.getProperty(anyString())).thenReturn(Boolean.TRUE.toString());
        when(security.getPaxId()).thenReturn(TestConstants.ID_1);

        List<Map<String, Object>> activityFeedCoreList = new ArrayList<>();
        Map<String, Object> activityFeedCoreEntry = new HashMap<>();

        activityFeedCoreEntry.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.CREATE_DATE, new Date());
        activityFeedCoreEntry.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.RECIPIENT_PAX_ID, TestConstants.ID_2);
        activityFeedCoreEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);

        activityFeedCoreList.add(activityFeedCoreEntry);

        when(activityFeedInfoDao.getActivityFeedDirectReports(anyLong(), anyString(), anyString(),
                anyInt(), anyInt(), anyLong())).thenReturn(activityFeedCoreList);
        when(activityFeedInfoDao.getActivityFeedNetwork(anyLong(), anyString(), anyString(), anyInt(),
      anyInt(), anyLong())).thenReturn(activityFeedCoreList);
        when(activityFeedInfoDao.getActivityFeedParticipant(anyLong(), anyLong(), anyString(), anyString(),
                anyInt(), anyInt(), anyLong())).thenReturn(activityFeedCoreList);
        when(activityFeedInfoDao.getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean())).thenReturn(activityFeedCoreList);
        when(activityFeedInfoDao.getActivityFeedGroup(anyLong(), anyLong(), anyString(), anyString(),
                anyInt(), anyInt(), anyLong())).thenReturn(activityFeedCoreList);

        when(activityFeedInfoDao.getActivityFeedDetails(anyLong(), anyString(), anyBoolean()))
                .thenReturn(mockActivityFeedDetailsData());

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                Object[] arguments = invocation.getArguments();
                if (arguments != null && arguments.length == 2 && arguments[1] != null) {
                    NewsfeedNominationDTO newsFeedItemDTOParameter = (NewsfeedNominationDTO) arguments[1];
                    if (newsFeedItemDTOParameter.getPayoutType() == null){
                        newsFeedItemDTOParameter.setPayoutType("payoutType");
                    }
                    newsFeedItemDTOParameter.setPayoutDescription("payoutDescription");
                    newsFeedItemDTOParameter.setPayoutDisplayName("payoutDisplayName");
                }
                return null;
            }

        }).when(spyActivityFeedService).populatePayoutTypeInformation(anyString(),any(NewsfeedNominationDTO.class));

        List<EntityDTO> paxList = new ArrayList<>();
        for (long i = 1; i < 6; i++) {
            EmployeeDTO employeeDto = new EmployeeDTO();
            employeeDto.setPaxId(i);
            paxList.add(employeeDto);
        }
        when(participantInfoDao.getInfo(anyListOf(Long.class), anyListOf(Long.class))).thenReturn(paxList);
    }

    @Test
    public void test_default_values_activity_feed() {

    PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
      TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, "");

    verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
    verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_direct_reports_activity_feed() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.DIRECT_REPORTS.name(), null, null, 1, 1, null,
                Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedDirectReports(anyLong(), anyString(), anyString(),
                anyInt(), anyInt(), anyLong());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_network_activity_feed() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.NETWORK.name(), null, null, 1, 1, null, Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedNetwork(anyLong(), anyString(), anyString(),
                anyInt(), anyInt(), anyLong());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_network_activity_feed_disabled() {
        when(environment.getProperty(anyString())).thenReturn(Boolean.FALSE.toString());

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.NETWORK.name(), null, null, 1, 1, null, Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedParticipant(anyLong(), anyLong(), anyString(),
                anyString(), anyInt(), anyInt(), anyLong());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_participant_activity_feed() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.PARTICIPANT.name(), null, null, 1, 1, null, Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedParticipant(anyLong(), anyLong(), anyString(),
                anyString(), anyInt(), anyInt(), anyLong());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_public_activity_feed() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.PUBLIC.name(), null, null, 1, 1, null, Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_group_activity_feed() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.GROUP.name(), null, null, 1, 1, null, Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedGroup(anyLong(), anyLong(), anyString(),
                anyString(), anyInt(), anyInt(), anyLong());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);
        assertFalse("Should have data", data.isEmpty());
    }

  @Test
  public void test_getActivityFeed_translates_program_values() {
    PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
      TestConstants.ID_1, ActivityFeedAudienceEnum.PUBLIC.name(), null, null, 1, 1, null, Boolean.FALSE,
      Boolean.FALSE, LocaleCodeEnum.fr_CA.name());

    verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
      anyLong(), anyBoolean(), anyBoolean());
    verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
    verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
    verify(translationUtil, times(2)).getTranslationsForListOfIds(anyString(), anyString(), anyString(), anyListOf(Long.class), anyMapOf(Long.class, String.class));

    assertNotNull("should have a response", response);

    List<NewsfeedItemDTO> data = response.getData();
    assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

    assertFalse("Should have data", data.isEmpty());
  }

  @Test
  public void test_getActivityFeed_does_not_translate_program_values() {
    PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
      TestConstants.ID_1, ActivityFeedAudienceEnum.PUBLIC.name(), null, null, 1, 1, null, Boolean.FALSE,
      Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

    verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
      anyLong(), anyBoolean(), anyBoolean());
    verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
    verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
    verify(translationUtil, times(0)).getTranslationsForListOfIds(anyString(), anyString(), anyString(), anyListOf(Long.class), anyMapOf(Long.class, String.class));

    assertNotNull("should have a response", response);

    List<NewsfeedItemDTO> data = response.getData();
    assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

    assertFalse("Should have data", data.isEmpty());
  }

  @Test
    public void test_bad_activity_feed_id() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, ActivityFeedAudienceEnum.PUBLIC.name(), null, null, 1, 1, "NOT A NUMBER",
                Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(0)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(0)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(0)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertTrue("Should not have data", data.isEmpty());

    }

    @Test
    public void test_activity_feed_by_id() {

        NewsfeedItemDTO activityFeedItem = spyActivityFeedService.getActivityById(TestConstants.ID_1, TestConstants.ID_1,
                null, null, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
        assertNotNull("should have a response", activityFeedItem);
        assertNotNull("should have nomination", activityFeedItem.getNomination());
        assertNotNull("payout type should not be null but was.", activityFeedItem.getNomination().getPayoutType());
        assertNotNull("payout type Display Name should not be null but was",
                activityFeedItem.getNomination().getPayoutDisplayName());
        assertNotNull("payout type Description should not be null but was",
                activityFeedItem.getNomination().getPayoutDescription());
    }

    @Test
    public void test_activity_feed_data_not_found() {

        List<Map<String, Object>> activityFeedDataList = new ArrayList<>();
        Map<String, Object> activityFeedDataEntry = new HashMap<>();
        activityFeedDataEntry.put(ProjectConstants.NEWSFEED_ITEM_ID, 0L);

        activityFeedDataList.add(activityFeedDataEntry);
        when(activityFeedInfoDao.getActivityFeedDetails(anyLong(), anyString(), anyBoolean()))
                .thenReturn(activityFeedDataList);

        try {
            activityFeedService.getActivityFeed(null, TestConstants.ID_1, ActivityFeedAudienceEnum.PUBLIC.name(), null,
                    null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);
            fail();
        } catch (ErrorMessageException e) {
            assertNotNull("should have an error", e.getErrorMessages());
        }

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(0)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
    }

    @Test
    public void test_activity_feed_invalid_audience() {

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, "ALEX", null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(0)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(0)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(0)).getInfo(anyListOf(Long.class), anyListOf(Long.class));
        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

        assertTrue("Should not have data", data.isEmpty());

    }

    @Test
    public void test_activity_feed_double_data_returned() {
        // shouldn't happen, code coverage!!

        List<Map<String, Object>> activityFeedCoreList = new ArrayList<>();
        Map<String, Object> activityFeedCoreEntry = new HashMap<>();

        activityFeedCoreEntry.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.CREATE_DATE, new Date());
        activityFeedCoreEntry.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.ID_1);
        activityFeedCoreEntry.put(ProjectConstants.RECIPIENT_PAX_ID, TestConstants.ID_2);
        activityFeedCoreEntry.put(ProjectConstants.TOTAL_RESULTS_KEY,1);

        activityFeedCoreList.add(activityFeedCoreEntry);
        Map<String, Object> activityFeedCoreEntrySecond = new HashMap<>();

        activityFeedCoreEntrySecond.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedCoreEntrySecond.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedCoreEntrySecond.put(ProjectConstants.CREATE_DATE, new Date());
        activityFeedCoreEntrySecond.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.ID_1);
        activityFeedCoreEntrySecond.put(ProjectConstants.RECIPIENT_PAX_ID, TestConstants.ID_2);
        activityFeedCoreEntrySecond.put(ProjectConstants.TOTAL_RESULTS_KEY,1);

        activityFeedCoreList.add(activityFeedCoreEntrySecond);

        when(activityFeedInfoDao.getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean())).thenReturn(activityFeedCoreList);

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", data);

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_no_pax_id_null_counts() {

        List<Map<String, Object>> activityFeedDataList = new ArrayList<>();
        Map<String, Object> activityFeedDataEntry = new HashMap<>();

        activityFeedDataEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);
        activityFeedDataEntry.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.ACTIVITY_TYPE, NewsfeedItemTypeCode.RECOGNITION.name());
        activityFeedDataEntry.put(ProjectConstants.RECIPIENT_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.APPROVED_RECIPIENT_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.LIKE_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENTER_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.RAISED_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_ID, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_DATE, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_TEXT, null);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_PAX_ID, null);
        activityFeedDataEntry.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_NAME, "PROGRAM_NAME");
        activityFeedDataEntry.put(ProjectConstants.IMAGE_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.HEADLINE, "HEADLINE");
        activityFeedDataEntry.put(ProjectConstants.AWARD_CODE, null);
        activityFeedDataEntry.put(ProjectConstants.PRIVATE_MESSAGE, "PRIVATE_MESSAGE");
        activityFeedDataEntry.put(ProjectConstants.RECOGNITION_CRITERIA_JSON, null);
        activityFeedDataEntry.put(ProjectConstants.DIRECT_REPORT_COUNT, 0);
        activityFeedDataEntry.put(ProjectConstants.MY_COMMENT_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.MY_RAISED_COUNT, null);
        activityFeedDataEntry.put(ProjectConstants.RAISED_POINT_TOTAL, 0D);
        activityFeedDataEntry.put(ProjectConstants.POINTS_ISSUED, 0D);
        activityFeedDataEntry.put(ProjectConstants.RAISED_PAX_ID, null);
        activityFeedDataEntry.put(ProjectConstants.LIKE_PAX_ID, null);
        activityFeedDataEntry.put(ProjectConstants.LOGGED_IN_USER_STATUS, StatusTypeCode.ACTIVE.name());
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_IS_ACTIVE, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_ANY_TIER, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_TIER, Boolean.FALSE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_PROGRAM_BUDGET, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.GIVEN_RAISE, Boolean.FALSE);

        activityFeedDataList.add(activityFeedDataEntry);
        when(activityFeedInfoDao.getActivityFeedDetails(anyLong(), anyString(), anyBoolean()))
                .thenReturn(activityFeedDataList);

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_invalid_entity() {
        // shouldn't happen, code coverage!!

        List<EntityDTO> paxList = new ArrayList<>();
        GroupDTO groupDto = new GroupDTO();
        paxList.add(groupDto);

        when(participantInfoDao.getInfo(anyListOf(Long.class), anyListOf(Long.class))).thenReturn(paxList);

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

        assertFalse("Should have data", data.isEmpty());
    }

    @Test
    public void test_multiple_entries() {
        // this is all junk situations that won't happen, just for code coverage

        List<Map<String, Object>> activityFeedDataList = new ArrayList<>();
        Map<String, Object> activityFeedDataEntryOne = new HashMap<>();

        activityFeedDataEntryOne.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);
        activityFeedDataEntryOne.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedDataEntryOne.put(ProjectConstants.ACTIVITY_TYPE, NewsfeedItemTypeCode.RECOGNITION.name());
        activityFeedDataEntryOne.put(ProjectConstants.RECIPIENT_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.APPROVED_RECIPIENT_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.LIKE_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.COMMENTER_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.RAISED_COUNT, 1);
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_ID, TestConstants.ID_1);
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_DATE, new Date());
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_TEXT, "COMMENT_TEXT");
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_PAX_ID, null);
        activityFeedDataEntryOne.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedDataEntryOne.put(ProjectConstants.PROGRAM_ID, TestConstants.ID_1);
        activityFeedDataEntryOne.put(ProjectConstants.PROGRAM_NAME, "PROGRAM_NAME");
        activityFeedDataEntryOne.put(ProjectConstants.IMAGE_ID, TestConstants.ID_1);
        activityFeedDataEntryOne.put(ProjectConstants.HEADLINE, "HEADLINE");
        activityFeedDataEntryOne.put(ProjectConstants.AWARD_CODE, null);
        activityFeedDataEntryOne.put(ProjectConstants.PRIVATE_MESSAGE, "PRIVATE_MESSAGE");
        activityFeedDataEntryOne.put(ProjectConstants.RECOGNITION_CRITERIA_JSON, "");
        activityFeedDataEntryOne.put(ProjectConstants.DIRECT_REPORT_COUNT, 0);
        activityFeedDataEntryOne.put(ProjectConstants.MY_COMMENT_COUNT, 0);
        activityFeedDataEntryOne.put(ProjectConstants.MY_RAISED_COUNT, 0);
        activityFeedDataEntryOne.put(ProjectConstants.RAISED_POINT_TOTAL, 0D);
        activityFeedDataEntryOne.put(ProjectConstants.POINTS_ISSUED, 10D);
        activityFeedDataEntryOne.put(ProjectConstants.RAISED_PAX_ID, 4L);
        activityFeedDataEntryOne.put(ProjectConstants.LIKE_PAX_ID, 5L);
        activityFeedDataEntryOne.put(ProjectConstants.LOGGED_IN_USER_STATUS, StatusTypeCode.ACTIVE.name());
        activityFeedDataEntryOne.put(ProjectConstants.PROGRAM_IS_ACTIVE, Boolean.TRUE);
        activityFeedDataEntryOne.put(ProjectConstants.CAN_RAISE_ANY_TIER, Boolean.TRUE);
        activityFeedDataEntryOne.put(ProjectConstants.CAN_RAISE_TIER, Boolean.TRUE);
        activityFeedDataEntryOne.put(ProjectConstants.CAN_RAISE_PROGRAM_BUDGET, Boolean.TRUE);
        activityFeedDataEntryOne.put(ProjectConstants.GIVEN_RAISE, Boolean.FALSE);

        activityFeedDataList.add(activityFeedDataEntryOne);

        Map<String, Object> activityFeedDataEntryTwo = new HashMap<>(activityFeedDataEntryOne);
        activityFeedDataEntryTwo.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_2);
        activityFeedDataEntryTwo.put(ProjectConstants.LOGGED_IN_USER_STATUS, StatusTypeCode.RESTRICTED.name());
        activityFeedDataList.add(activityFeedDataEntryTwo);

        Map<String, Object> activityFeedDataEntryThree = new HashMap<>(activityFeedDataEntryOne);
        activityFeedDataEntryThree.put(ProjectConstants.NEWSFEED_ITEM_ID, 3L);
        activityFeedDataEntryThree.put(ProjectConstants.RECIPIENT_COUNT, 0);
        activityFeedDataEntryOne.put(ProjectConstants.COMMENT_PAX_ID, TestConstants.ID_2);
        activityFeedDataList.add(activityFeedDataEntryThree);

        Map<String, Object> activityFeedDataEntryFour = new HashMap<>(activityFeedDataEntryOne);
        activityFeedDataEntryFour.put(ProjectConstants.NEWSFEED_ITEM_ID, 4L);
        activityFeedDataEntryFour.put(ProjectConstants.CAN_RAISE_ANY_TIER, Boolean.FALSE);
        activityFeedDataList.add(activityFeedDataEntryFour);

        Map<String, Object> activityFeedDataEntryFive = new HashMap<>(activityFeedDataEntryOne);
        activityFeedDataEntryFive.put(ProjectConstants.NEWSFEED_ITEM_ID, 5L);
        activityFeedDataEntryFive.put(ProjectConstants.PROGRAM_IS_ACTIVE, Boolean.FALSE);
        activityFeedDataList.add(activityFeedDataEntryFive);

        Map<String, Object> activityFeedDataEntrySix = new HashMap<>(activityFeedDataEntryOne);
        activityFeedDataEntrySix.put(ProjectConstants.NEWSFEED_ITEM_ID, 6L);
        activityFeedDataList.add(activityFeedDataEntrySix);

        when(activityFeedInfoDao.getActivityFeedDetails(anyLong(), anyString(), anyBoolean()))
                .thenReturn(activityFeedDataList);

        List<Map<String, Object>> activityFeedCoreList = new ArrayList<>();
        Map<String, Object> activityFeedCoreEntryOne = new HashMap<>();

        activityFeedCoreEntryOne.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedCoreEntryOne.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedCoreEntryOne.put(ProjectConstants.CREATE_DATE, new Date());
        activityFeedCoreEntryOne.put(ProjectConstants.SUBMITTER_PAX_ID, TestConstants.ID_1);
        activityFeedCoreEntryOne.put(ProjectConstants.RECIPIENT_PAX_ID, TestConstants.ID_2);
        activityFeedCoreEntryOne.put(ProjectConstants.TOTAL_RESULTS_KEY,1);
        activityFeedCoreList.add(activityFeedCoreEntryOne);

        Map<String, Object> activityFeedCoreEntryTwo = new HashMap<>(activityFeedCoreEntryOne);
        activityFeedCoreEntryTwo.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_2);
        activityFeedCoreEntryTwo.put(ProjectConstants.SUBMITTER_PAX_ID, 0L);
        activityFeedCoreEntryTwo.put(ProjectConstants.RECIPIENT_PAX_ID, null);
        activityFeedCoreList.add(activityFeedCoreEntryTwo);

        Map<String, Object> activityFeedCoreEntryThree = new HashMap<>(activityFeedCoreEntryOne);
        activityFeedCoreEntryThree.put(ProjectConstants.NEWSFEED_ITEM_ID, 3L);
        activityFeedCoreList.add(activityFeedCoreEntryThree);

        Map<String, Object> activityFeedCoreEntryFour = new HashMap<>(activityFeedCoreEntryOne);
        activityFeedCoreEntryFour.put(ProjectConstants.NEWSFEED_ITEM_ID, 4L);
        activityFeedCoreList.add(activityFeedCoreEntryFour);

        Map<String, Object> activityFeedCoreEntryFive = new HashMap<>(activityFeedCoreEntryOne);
        activityFeedCoreEntryFive.put(ProjectConstants.NEWSFEED_ITEM_ID, 5L);
        activityFeedCoreEntryFive.put(ProjectConstants.SUBMITTER_PAX_ID, null);
        activityFeedCoreList.add(activityFeedCoreEntryFive);

        Map<String, Object> activityFeedCoreEntrySix = new HashMap<>(activityFeedCoreEntryOne);
        activityFeedCoreEntrySix.put(ProjectConstants.NEWSFEED_ITEM_ID, 6L);
        activityFeedCoreEntrySix.put(ProjectConstants.SUBMITTER_PAX_ID, 3L);
        activityFeedCoreList.add(activityFeedCoreEntrySix);

        when(activityFeedInfoDao.getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean())).thenReturn(activityFeedCoreList);

        PaginatedResponseObject<NewsfeedItemDTO> response = activityFeedService.getActivityFeed(null,
                TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1)).getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1)).getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertNotNull("should have a response", response);

        List<NewsfeedItemDTO> data = response.getData();
        assertNotNull("should have data", ToStringBuilder.reflectionToString(data));

        assertFalse("Should have data", data.isEmpty());

    }

    @Test
    public void test_no_data() {
        // junk tests for code coverage
        when(activityFeedInfoDao.getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean())).thenReturn(new ArrayList<Map<String, Object>>());

        activityFeedService.getActivityFeed(null, TestConstants.ID_1, "test", "test", "test", 1, 1, "", Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE); // this shouldn't happen
        activityFeedService.getActivityFeed(null, TestConstants.ID_1, "", "", "", 1, 1, "", Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE); // this shouldn't happen
        activityFeedService.getActivityById(TestConstants.ID_1, TestConstants.ID_1, null, null, "", Boolean.FALSE,
                Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(2)).getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(),
                anyLong(), anyBoolean(), anyBoolean());
    }

    @Test
    public void test_raise_status() {

        // can raise tier, can raise budget, hasn't given
        String eligibleStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be eligible", ActivityFeedRaiseStatusEnum.ELIGIBLE.name(), eligibleStatus);

        // can raise tier, can raise budget, has given
        String givenStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2, StatusTypeCode.ACTIVE.name(),
                3L, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
        assertEquals("should be given", ActivityFeedRaiseStatusEnum.GIVEN.name(), givenStatus);

        // can raise tier, can't raise budget
        String cantRaiseBudgetStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.FALSE,
                Boolean.FALSE);
        assertEquals("should be ineligible", ActivityFeedRaiseStatusEnum.INELIGIBLE.name(), cantRaiseBudgetStatus);

        // can't raise tier, can raise budget
        String cantRaiseTierStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be ineligible", ActivityFeedRaiseStatusEnum.INELIGIBLE.name(), cantRaiseTierStatus);

        // logged in pax is recipient, more than one receiver
        String recipientPaxMultipleReceiversStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), TestConstants.ID_1, 2, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        assertEquals("should be eligible", ActivityFeedRaiseStatusEnum.ELIGIBLE.name(),
                recipientPaxMultipleReceiversStatus);

        // logged in pax is recipient, only one receiver
        String recipientPaxOnlyReceiverStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), TestConstants.ID_1, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        assertEquals("should be ineligible", ActivityFeedRaiseStatusEnum.INELIGIBLE.name(),
                recipientPaxOnlyReceiverStatus);

        // logged in pax is submitter, did not give points (null)
        String submitterNullPointsStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_1,
                StatusTypeCode.ACTIVE.name(), TestConstants.ID_2, 1, null, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        assertEquals("should be eligible", ActivityFeedRaiseStatusEnum.ELIGIBLE.name(), submitterNullPointsStatus);

        // logged in pax is submitter, did not give points (zero)
        String submitterZeroPointsStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_1,
                StatusTypeCode.ACTIVE.name(), TestConstants.ID_2, 1, 0D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        assertEquals("should be eligible", ActivityFeedRaiseStatusEnum.ELIGIBLE.name(), submitterZeroPointsStatus);

        // logged in pax is submitter, gave give points
        String submitterGavePointsStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_1,
                StatusTypeCode.ACTIVE.name(), TestConstants.ID_2, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        assertEquals("should be ineligible", ActivityFeedRaiseStatusEnum.INELIGIBLE.name(), submitterGavePointsStatus);

        // can't see nomination (recipient count is null) - shouldn't happen
        String cantSeeNullStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, null, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be not allowed", ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name(), cantSeeNullStatus);

        // can't see nomination (recipient count is 0) - shouldn't happen
        String cantSeeZeroStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 0, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be not allowed", ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name(), cantSeeZeroStatus);

        // can't raise any tier tied to the program
        String cantRaiseAnyTierStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 1, 1D, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be not allowed", ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name(), cantRaiseAnyTierStatus);

        // program is not active
        String programIsNotActiveStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.ACTIVE.name(), 3L, 1, 1D, Boolean.FALSE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be not allowed", ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name(), programIsNotActiveStatus);

        // user is restricted
        String userIsRestrictedStatus = activityFeedService.calculateRaiseStatus(TestConstants.ID_2,
                StatusTypeCode.RESTRICTED.name(), 3L, 1, 1D, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.FALSE);
        assertEquals("should be not allowed", ActivityFeedRaiseStatusEnum.NOT_ALLOWED.name(), userIsRestrictedStatus);

    }

    @Test
    public void test_getActivityFeed_payout_type_populated() {

        // Payout type is returned by query.
        List<Map<String, Object>> mockData = mockActivityFeedDetailsData();
        mockData.get(0).put(ProjectConstants.PAYOUT_TYPE, TEST_PAYOUT_TYPE);

        when(activityFeedInfoDao.getActivityFeedDetails(anyLong(), anyString(), anyBoolean()))
            .thenReturn(mockData);


        PaginatedResponseObject<NewsfeedItemDTO> response =
                spyActivityFeedService.getActivityFeed(null, TestConstants.ID_1, null, null, null, 1, 1, null, Boolean.FALSE, Boolean.FALSE, ProjectConstants.DEFAULT_LOCALE_CODE);

        verify(activityFeedInfoDao, times(1))
            .getActivityFeedPublic(anyLong(), anyString(), anyInt(), anyInt(), anyLong(), anyBoolean(), anyBoolean());
        verify(activityFeedInfoDao, times(1))
            .getActivityFeedDetails(anyLong(), anyString(), anyBoolean());
        verify(participantInfoDao, times(1))
            .getInfo(anyListOf(Long.class), anyListOf(Long.class));

        assertThat(response.getData().get(0).getNomination().getPayoutType(), equalTo(TEST_PAYOUT_TYPE));
    }

    /**
     * Mock getActivityFeedDetails method return data.
     *
     * @return
     */
    private List<Map<String, Object>> mockActivityFeedDetailsData() {
        List<Map<String, Object>> activityFeedDataList = new ArrayList<>();
        Map<String, Object> activityFeedDataEntry = new HashMap<>();

        activityFeedDataEntry.put(ProjectConstants.TOTAL_RESULTS_KEY, 1);
        activityFeedDataEntry.put(ProjectConstants.NEWSFEED_ITEM_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.ACTIVITY_TYPE, NewsfeedItemTypeCode.RECOGNITION.name());
        activityFeedDataEntry.put(ProjectConstants.RECIPIENT_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.APPROVED_RECIPIENT_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.LIKE_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.COMMENTER_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.RAISED_COUNT, 1);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.COMMENT_DATE, new Date());
        activityFeedDataEntry.put(ProjectConstants.COMMENT_TEXT, "COMMENT_TEXT");
        activityFeedDataEntry.put(ProjectConstants.COMMENT_PAX_ID, 3L);
        activityFeedDataEntry.put(ProjectConstants.NOMINATION_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_NAME, "PROGRAM_NAME");
        activityFeedDataEntry.put(ProjectConstants.IMAGE_ID, TestConstants.ID_1);
        activityFeedDataEntry.put(ProjectConstants.HEADLINE, "HEADLINE");
        activityFeedDataEntry.put(ProjectConstants.AWARD_CODE, null);
        activityFeedDataEntry.put(ProjectConstants.PRIVATE_MESSAGE, "PRIVATE_MESSAGE");
        activityFeedDataEntry.put(ProjectConstants.RECOGNITION_CRITERIA_JSON,
                "[{\"id\":1, \"displayName\":\"Achievement\"}]");
        activityFeedDataEntry.put(ProjectConstants.DIRECT_REPORT_COUNT, 0);
        activityFeedDataEntry.put(ProjectConstants.MY_COMMENT_COUNT, 0);
        activityFeedDataEntry.put(ProjectConstants.MY_RAISED_COUNT, 0);
        activityFeedDataEntry.put(ProjectConstants.RAISED_POINT_TOTAL, 0D);
        activityFeedDataEntry.put(ProjectConstants.POINTS_ISSUED, 10D);
        activityFeedDataEntry.put(ProjectConstants.RAISED_PAX_ID, 4L);
        activityFeedDataEntry.put(ProjectConstants.LIKE_PAX_ID, 5L);
        activityFeedDataEntry.put(ProjectConstants.LOGGED_IN_USER_STATUS, StatusTypeCode.ACTIVE.name());
        activityFeedDataEntry.put(ProjectConstants.PROGRAM_IS_ACTIVE, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_ANY_TIER, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_TIER, Boolean.FALSE);
        activityFeedDataEntry.put(ProjectConstants.CAN_RAISE_PROGRAM_BUDGET, Boolean.TRUE);
        activityFeedDataEntry.put(ProjectConstants.GIVEN_RAISE, Boolean.FALSE);
        activityFeedDataEntry.put(ProjectConstants.PAYOUT_TYPE, null);

        activityFeedDataList.add(activityFeedDataEntry);
        return activityFeedDataList;
    }
}
