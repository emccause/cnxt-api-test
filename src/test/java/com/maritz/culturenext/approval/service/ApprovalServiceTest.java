package com.maritz.culturenext.approval.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.ApplicationData;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.ApprovalPending;
import com.maritz.core.jpa.entity.ApprovalProcess;
import com.maritz.core.jpa.entity.ApprovalProcessConfig;
import com.maritz.core.jpa.entity.AuxiliaryNotification;
import com.maritz.core.jpa.entity.NewsfeedItem;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.NominationCriteria;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxFiles;
import com.maritz.core.jpa.entity.PaxGroup;
import com.maritz.core.jpa.entity.PaxGroupMisc;
import com.maritz.core.jpa.entity.Program;
import com.maritz.core.jpa.entity.ProgramAwardTier;
import com.maritz.core.jpa.entity.ProgramMisc;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.RecognitionCriteria;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.entity.TransactionHeader;
import com.maritz.core.jpa.repository.ApprovalPendingRepository;
import com.maritz.core.jpa.repository.ApprovalProcessRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.alert.service.AlertService;
import com.maritz.culturenext.alert.util.AlertUtil;
import com.maritz.culturenext.approval.dao.ApprovalDao;
import com.maritz.culturenext.approval.service.impl.ApprovalServiceImpl;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ApprovalType;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.jpa.repository.CnxtProgramMiscRepository;
import com.maritz.culturenext.jpa.repository.CnxtRecognitionRepository;
import com.maritz.culturenext.jpa.repository.CnxtRelationshipRepository;
import com.maritz.culturenext.jpa.repository.CnxtTransactionHeaderRepository;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberAwardDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.NominationRequestDTO;
import com.maritz.culturenext.recognition.dto.RaiseDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionCriteriaService;
import com.maritz.test.AbstractMockTest;

public class ApprovalServiceTest extends AbstractMockTest {
    @InjectMocks
    private final ApprovalServiceImpl approvalService = new ApprovalServiceImpl();

    @Mock private AlertService alertService;
    @Mock private AlertUtil alertUtil;
    @Mock private ApprovalDao approvalDao;
    @Mock private ApprovalPendingRepository approvalPendingRepository;
    @Mock private ApprovalProcessRepository approvalProcessRepository;
    @Mock private CnxtProgramMiscRepository cnxtProgramMiscRepository;
    @Mock private CnxtRecognitionRepository cnxtRecognitionRepository;
    @Mock private CnxtRelationshipRepository cnxtRelationshipRepository;
    @Mock private CnxtTransactionHeaderRepository cnxtTransactionHeaderRepository;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private ConcentrixDao<Nomination> nominationDao;
    @Mock private ConcentrixDao<ApprovalPending> approvalPendingDao;
    @Mock private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Mock private ConcentrixDao<ApprovalProcess> approvalProcessDao;
    @Mock private ConcentrixDao<ApprovalProcessConfig> approvalProcessConfigDao;
    @Mock private ConcentrixDao<AuxiliaryNotification> auxiliaryNotificationDao;
    @Mock private ConcentrixDao<Program> programDao;
    @Mock private ConcentrixDao<ProgramAwardTier> programAwardTierDao;
    @Mock private ConcentrixDao<ProgramMisc> programMiscDao;    
    @Mock private ConcentrixDao<SysUser> sysUserDao;
    @Mock private ConcentrixDao<PaxGroup> paxGroupDao;
    @Mock private ConcentrixDao<PaxFiles> paxFilesDao;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private ConcentrixDao<RecognitionCriteria> recognitionCriteriaDao;
    @Mock private ConcentrixDao<NominationCriteria> nominationCriteriaDao;
    @Mock private ConcentrixDao<PaxGroupMisc> paxGroupMiscDao;
    @Mock private ConcentrixDao<Alert> alertDao;
    @Mock private ConcentrixDao<NewsfeedItem> newsfeedItemDao;
    @Mock private ConcentrixDao<TransactionHeader> transactionHeaderDao;
    @Mock private Environment environment;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private PaxCountDao paxCountDao;
    @Mock private ProxyService proxyService;
    @Mock private RecognitionCriteriaService recognitionCriteriaService;
    @Mock private Security security;

    FindBy<Alert> alertFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ApplicationData> applicationDataFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ApprovalPending> approvalPendingFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ApprovalHistory> approvalHistoryFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ApprovalProcess> approvalProcessFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ApprovalProcessConfig> approvalProcessConfigFindBy = PowerMockito.mock(FindBy.class);
    FindBy<AuxiliaryNotification> auxiliaryNotificationFindBy = PowerMockito.mock(FindBy.class);
    FindBy<NewsfeedItem> newsfeedItemFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Nomination> nominationFindBy = PowerMockito.mock(FindBy.class);
    FindBy<NominationCriteria> nominationCriteriaFindBy = PowerMockito.mock(FindBy.class);
    FindBy<PaxFiles> paxFilesFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Pax> paxFindBy = PowerMockito.mock(FindBy.class);
    FindBy<PaxGroup> paxGroupFindBy = PowerMockito.mock(FindBy.class);
    FindBy<PaxGroupMisc> paxGroupMiscFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Program> programFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ProgramAwardTier> ProgramAwardTierFindBy = PowerMockito.mock(FindBy.class);
    FindBy<ProgramMisc> programMiscFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Recognition> recognitionFindBy = PowerMockito.mock(FindBy.class);
    FindBy<RecognitionCriteria> recognitionCriteriaFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Recognition> recognitionPendingFindBy = PowerMockito.mock(FindBy.class);
    FindBy<Recognition> recognitionApprovedFindBy = PowerMockito.mock(FindBy.class);
    FindBy<SysUser>  sysUserFindBy = PowerMockito.mock(FindBy.class);

    List<Recognition> recognitionList = new ArrayList<>();
    List<NominationCriteria> nominationCriteriaList = new ArrayList<>();
    Pax pax = new Pax();
    EmployeeDTO employeeDTO = new EmployeeDTO();
    Nomination nomination = new Nomination();

    List<RecognitionDetailsDTO> RECOGNITION_DETAIL_LIST = null;
    Long APPROVER_PAX_ID = 3L;
    Long MANAGER_PAX_ID = 13L;

    String MIN_AMOUNT = "minAmount";
    String MAX_AMOUNT = "maxAmount";
    String SUBMITTER_FIRST = "SUBMITTER_FIRST";
    String SUBMITTER_SECOND = "SUBMITTER_SECOND";

    NominationRequestDTO testNominationDTO;
    List<Map<String, Object>> testDistinctPax = new ArrayList<>();
    List<Long> testPaxIds = new ArrayList<>();
    List<Long> testGroupIds = new ArrayList<>();
    List<Long> testMyGroups = new ArrayList<>();
    List<Long> testSimpleAndParentBudgetIds = new ArrayList<>();
    List<Map<String, Object>> testBudgetConfigList = new ArrayList<>();
    ApprovalProcess testApprovalProcess = new ApprovalProcess();
    String testValidationType;
    Long testBudgetId = 12345L;

    private static final String TEST_UNASSIGNED_APPROVER_PAX = "350";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        testNominationDTO = new NominationRequestDTO();
        testNominationDTO.setHeadline("Not null");
        testNominationDTO.setBudgetId(testBudgetId);
        testNominationDTO.setProgramId(TestConstants.ID_1);
        testNominationDTO.setReceivers(new ArrayList<GroupMemberAwardDTO>());
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(165L));
        testNominationDTO.getReceivers().add(new GroupMemberAwardDTO().setPaxId(141L));

        PowerMockito.when(approvalDao.getRecognitionIdsPendingApproval(TestConstants.ID_1)).thenReturn(Arrays.asList(TestConstants.ID_1, TestConstants.ID_2));

        RECOGNITION_DETAIL_LIST = new ArrayList<>();
        RecognitionDetailsDTO recognitionDetailOne = new RecognitionDetailsDTO();
        recognitionDetailOne.setFromPax(new EmployeeDTO().setPaxId(TestConstants.ID_1).setManagerPaxId(TestConstants.ID_1));
        recognitionDetailOne.setToPax(new EmployeeDTO().setPaxId(TestConstants.ID_2).setManagerPaxId(12L));
        recognitionDetailOne.setApprovalPax(new EmployeeDTO()
                    .setPaxId(APPROVER_PAX_ID)
                    .setManagerPaxId(MANAGER_PAX_ID));
        recognitionDetailOne.setId(TestConstants.ID_1);
        recognitionDetailOne.setNominationId(TestConstants.ID_1);
        recognitionDetailOne.setStatus(StatusTypeCode.APPROVED.name());
        RECOGNITION_DETAIL_LIST.add(recognitionDetailOne);

        RecognitionDetailsDTO recognitionDetailTwo = new RecognitionDetailsDTO();
        recognitionDetailTwo.setFromPax(new EmployeeDTO().setPaxId(TestConstants.ID_1).setManagerPaxId(11L));
        recognitionDetailTwo.setToPax(new EmployeeDTO().setPaxId(4L).setManagerPaxId(14L));
        recognitionDetailTwo.setApprovalPax(new EmployeeDTO()
                    .setPaxId(APPROVER_PAX_ID)
                    .setManagerPaxId(MANAGER_PAX_ID));
        recognitionDetailTwo.setId(TestConstants.ID_2);
        recognitionDetailTwo.setNominationId(TestConstants.ID_1);
        recognitionDetailTwo.setStatus(StatusTypeCode.REJECTED.name());
        recognitionDetailTwo.setComment("Nope.");
        RECOGNITION_DETAIL_LIST.add(recognitionDetailTwo);

        // recognitionDao
        recognitionList.add(new Recognition().setId(TestConstants.ID_1).setNominationId(TestConstants.ID_1).setReceiverPaxId(TestConstants.ID_2));
        recognitionList.add(new Recognition().setId(TestConstants.ID_2).setNominationId(TestConstants.ID_1).setReceiverPaxId(4L));

        Recognition recognition = new Recognition();
        recognition.setReceiverPaxId(TestConstants.ID_1);
        recognition.setId(3L);
        recognition.setAmount(5d);
        Recognition recognition1 = new Recognition();
        recognition1.setReceiverPaxId(TestConstants.ID_2);
        recognition.setId(4L);
        Recognition recognition2 = new Recognition();
        recognition2.setReceiverPaxId(3L);
        recognition.setId(5L);

        List<Relationship> relationshipList = new ArrayList<>();
        Relationship testRelationship1 = new Relationship();
        testRelationship1.setPaxId1(141L);
        testRelationship1.setPaxId2(170L);
        testRelationship1.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship1);
        Relationship testRelationship2 = new Relationship();
        testRelationship2.setPaxId1(165L);
        testRelationship2.setPaxId2(170L);
        testRelationship2.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship2);
        Relationship testRelationship3 = new Relationship();
        testRelationship3.setPaxId1(TestConstants.PAX_DAGARFIN);
        testRelationship3.setPaxId2(170L);
        testRelationship3.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship3);
        Relationship testRelationship4 = new Relationship();
        testRelationship4.setPaxId1(170L);
        testRelationship4.setPaxId2(250L);
        testRelationship4.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(testRelationship4);

        recognitionList.add(recognition);
        recognitionList.add(recognition1);
        recognitionList.add(recognition2);

        testApprovalProcess.setId(100L);

        for (Recognition rec : recognitionList) {
            rec.setAmount(10.0);
            rec.setNominationId(4L);
            rec.setStatusTypeCode(StatusTypeCode.PENDING.name());
        }

        PowerMockito.when(recognitionDao.findById(anyListOf(Long.class))).thenReturn(recognitionList);
        PowerMockito.when(recognitionDao.findById(anyLong())).thenReturn(recognition);
        PowerMockito.when(recognitionDao.findBy()).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.where(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(anyLong())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.and(anyString())).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.eq(matches(StatusTypeCode.PENDING.toString())))
            .thenReturn(recognitionPendingFindBy);
        PowerMockito.when(recognitionPendingFindBy.count()).thenReturn(0);
        PowerMockito.when(recognitionFindBy.eq(matches(StatusTypeCode.APPROVED.toString())))
            .thenReturn(recognitionApprovedFindBy);
        PowerMockito.when(recognitionApprovedFindBy.count()).thenReturn(1);
        PowerMockito.when(recognitionFindBy.in(any(Collection.class))).thenReturn(recognitionFindBy);
        PowerMockito.when(recognitionFindBy.find(ProjectConstants.NOMINATION_ID, Long.class))
            .thenReturn(Arrays.asList(TestConstants.ID_1));
        PowerMockito.when(newsfeedItemDao.findBy()).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.where(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(anyLong())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.and(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.eq(anyString())).thenReturn(newsfeedItemFindBy);
        PowerMockito.when(newsfeedItemFindBy.findOne()).thenReturn(new NewsfeedItem());

        PowerMockito.when(cnxtRecognitionRepository.getApprovedReceiverPaxIdsWithPublicSharePermission(anyLong()))
            .thenReturn(Arrays.asList(TestConstants.ID_2));

        //transaction header
        TransactionHeader transactionHeader = new TransactionHeader();
        transactionHeader.setStatusTypeCode(StatusTypeCode.PENDING.toString());

        PowerMockito.when(cnxtTransactionHeaderRepository.getTransactionHeaderByNominationIdAndPaxId(anyLong(), anyLong()))
            .thenReturn(transactionHeader);

        // nominationDao
        Nomination nomination = new Nomination().setId(TestConstants.ID_1).setSubmitterPaxId(TestConstants.ID_1);

        PowerMockito.when(nominationDao.findById(anyLong())).thenReturn(nomination);

        // approvalPendingDao
        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        approvalPendingList.add(new ApprovalPending().setTargetId(TestConstants.ID_1).setTargetTable(TableName.RECOGNITION.name())
                .setApprovalProcessId(TestConstants.ID_1).setId(TestConstants.ID_1).setNextApproverPaxId(APPROVER_PAX_ID));
        approvalPendingList.add(new ApprovalPending().setTargetId(TestConstants.ID_2).setTargetTable(TableName.RECOGNITION.name())
                .setApprovalProcessId(TestConstants.ID_2).setId(TestConstants.ID_2).setNextApproverPaxId(APPROVER_PAX_ID));

        PowerMockito.when(approvalPendingDao.findBy()).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.where(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.eq(anyLong())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.and(anyString())).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.in(anyListOf(Long.class))).thenReturn(approvalPendingFindBy);
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(approvalPendingList);

        // approvalHistoryDao
        PowerMockito.when(approvalHistoryDao.findBy()).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.where(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.eq(anyLong())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.and(anyString())).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.find()).thenReturn(null);

        // approvalProcessDao;
        ApprovalProcess approvalProcessOne = new ApprovalProcess().setPendingLevel(1);
        ApprovalProcess approvalProcessTwo = new ApprovalProcess().setPendingLevel(1);

        PowerMockito.when(approvalProcessDao.findById(eq(TestConstants.ID_1))).thenReturn(approvalProcessOne);
        PowerMockito.when(approvalProcessDao.findById(eq(TestConstants.ID_2))).thenReturn(approvalProcessTwo);

        // alertDao
        List<Alert> alertList = new ArrayList<>();
        alertList.add(new Alert().setId(TestConstants.ID_1).setPaxId(APPROVER_PAX_ID));

        PowerMockito.when(alertDao.findBy()).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.where(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.and(anyString())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.eq(anyLong())).thenReturn(alertFindBy);
        PowerMockito.when(alertFindBy.find()).thenReturn(alertList);

        // participantInfoDao
        List<EntityDTO> resolvedPaxObjects = new ArrayList<>();
        resolvedPaxObjects.add(new EmployeeDTO().setPaxId(TestConstants.ID_1).setFirstName("test1"));
        resolvedPaxObjects.add(new EmployeeDTO().setPaxId(TestConstants.ID_2).setFirstName("test2"));
        resolvedPaxObjects.add(new EmployeeDTO().setPaxId(APPROVER_PAX_ID).setFirstName("test3"));
        resolvedPaxObjects.add(new EmployeeDTO().setPaxId(4L).setFirstName("test4"));

        List<ProgramAwardTier> testProgramAwardTierList = new ArrayList<>();
        ProgramAwardTier testProgramAwardTier = new ProgramAwardTier();
        testProgramAwardTier.setId(2002L);
        testProgramAwardTier.setProgramId(TestConstants.ID_1);
        testProgramAwardTier.setMinAmount(5.0);
        testProgramAwardTier.setMaxAmount(5.0);
        testProgramAwardTier.setIncrement(0.0);
        testProgramAwardTier.setRaisingStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testProgramAwardTier.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testProgramAwardTierList.add(testProgramAwardTier);
        
        // programMiscDao
        List<ProgramMisc> testProgramMiscList = new ArrayList<>();
        ProgramMisc testProgramMisc = new ProgramMisc();
        testProgramMisc.setProgramMiscId(123L);
        testProgramMisc.setProgramId(TestConstants.ID_1);
        testProgramMisc.setVfName(VfName.ROUTE_NEXT_LEVEL_MANAGER.name());
        testProgramMisc.setMiscData(TestConstants.MISC_DATA);        
        testProgramMiscList.add(testProgramMisc);

        List<ApprovalProcessConfig> testApprovalProcessConfigList = new ArrayList<>();
        ApprovalProcessConfig testConfig1 = new ApprovalProcessConfig();
        testConfig1.setTargetId(2002L);
        testConfig1.setApprovalLevel(TestConstants.ID_1);
        testConfig1.setApprovalTypeCode(SUBMITTER_FIRST);
        testApprovalProcessConfigList.add(testConfig1);
        ApprovalProcessConfig testConfig2 = new ApprovalProcessConfig();
        testConfig2.setTargetId(2002L);
        testConfig2.setApprovalLevel(TestConstants.ID_2);
        testConfig2.setApprovalTypeCode(SUBMITTER_SECOND);
        testApprovalProcessConfigList.add(testConfig2);

        PowerMockito.when(participantInfoDao.getInfo(anyListOf(Long.class), anyListOf(Long.class)))
            .thenReturn(resolvedPaxObjects);

        //paxCountDao
        //generate mock response
        List<Map<String, Object>> results = new ArrayList<>();
        Map<String, Object> user1 = new HashMap<>();
        user1.put("PAX_ID", TestConstants.ID_1);
        user1.put("MANAGER_PAX_ID", 4L);
        Map<String, Object> user2 = new HashMap<>();
        user2.put("PAX_ID", TestConstants.ID_2);
        user2.put("MANAGER_PAX_ID", 4L);
        Map<String, Object> user3 = new HashMap<>();
        user3.put("PAX_ID", 3L);
        user3.put("MANAGER_PAX_ID", 4L);
        results.add(user1);
        results.add(user2);
        results.add(user3);

        PowerMockito.when(paxCountDao.getDistinctPax(anyLong(),anyListOf(Long.class),anyListOf(Long.class)))
            .thenReturn(results);

        // CRUD operations - do nothing
        PowerMockito.doNothing().when(recognitionDao).update(any(Recognition.class));
        PowerMockito.doNothing().when(nominationDao).update(any(Nomination.class));
        PowerMockito.doNothing().when(approvalPendingDao).delete(any(ApprovalPending.class));
        PowerMockito.doNothing().when(approvalHistoryDao).save(any(ApprovalHistory.class));
        PowerMockito.doNothing().when(approvalProcessDao).update(any(ApprovalProcess.class));
        PowerMockito.doNothing().when(alertDao).update(any(Alert.class));
        PowerMockito.doNothing().when(newsfeedItemDao).update(any(NewsfeedItem.class));
        PowerMockito.doNothing().when(transactionHeaderDao).update(any(TransactionHeader.class));

        PowerMockito.when(approvalPendingRepository.save(anyListOf(ApprovalPending.class))).thenReturn(null);
        PowerMockito.when(approvalProcessRepository.save(anyListOf(ApprovalProcess.class))).thenReturn(null);

        PowerMockito.when(nominationCriteriaDao.findBy()).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.where(anyString())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.eq(anyLong())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.find()).thenReturn(nominationCriteriaList);

        PowerMockito.when(sysUserDao.findBy()).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.where(anyString())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.eq(anyLong())).thenReturn(sysUserFindBy);
        PowerMockito.when(sysUserFindBy.findOne()).thenReturn(new SysUser());

        PowerMockito.when(paxGroupDao.findBy()).thenReturn(paxGroupFindBy);
        PowerMockito.when(paxGroupFindBy.where(anyString())).thenReturn(paxGroupFindBy);
        PowerMockito.when(paxGroupFindBy.eq(anyLong())).thenReturn(paxGroupFindBy);
        PowerMockito.when(paxGroupFindBy.findOne()).thenReturn(new PaxGroup());
        PowerMockito.doNothing().when(alertUtil).generateRecognitionAlerts(any(Nomination.class),
                anyListOf(Recognition.class), anyListOf(Long.class), anyLong(), anyListOf(Long.class));

        PowerMockito.when(paxGroupMiscDao.findBy()).thenReturn(paxGroupMiscFindBy);
        PowerMockito.when(paxGroupMiscFindBy.where(anyString())).thenReturn(paxGroupMiscFindBy);
        PowerMockito.when(paxGroupMiscFindBy.eq(anyLong())).thenReturn(paxGroupMiscFindBy);
        PowerMockito.when(paxGroupMiscFindBy.and(anyString())).thenReturn(paxGroupMiscFindBy);
        PowerMockito.when(paxGroupMiscFindBy.eq(anyString())).thenReturn(paxGroupMiscFindBy);
        PowerMockito.when(paxGroupMiscFindBy.findOne()).thenReturn(new PaxGroupMisc());

        PowerMockito.when(paxFilesDao.findBy()).thenReturn(paxFilesFindBy);
        PowerMockito.when(paxFilesFindBy.where(anyString())).thenReturn(paxFilesFindBy);
        PowerMockito.when(paxFilesFindBy.eq(anyLong())).thenReturn(paxFilesFindBy);
        PowerMockito.when(paxFilesFindBy.findOne()).thenReturn(new PaxFiles());

        PowerMockito.when(programDao.findById(anyLong())).thenReturn(new Program());

        PowerMockito.when(nominationCriteriaDao.findBy()).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.where(anyString())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.eq(anyLong())).thenReturn(nominationCriteriaFindBy);
        PowerMockito.when(nominationCriteriaFindBy.find()).thenReturn(nominationCriteriaList);

        PowerMockito.when(programAwardTierDao.findBy()).thenReturn(ProgramAwardTierFindBy);        
        PowerMockito.when(ProgramAwardTierFindBy.where(ProjectConstants.PROGRAM_ID))
            .thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.eq(testNominationDTO.getProgramId()))
            .thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.find()).thenReturn(testProgramAwardTierList);
        PowerMockito.when(ProgramAwardTierFindBy.and()).thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.and(MIN_AMOUNT)).thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.le(anyInt())).thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.and(MAX_AMOUNT)).thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.ge(anyInt())).thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.and(ProjectConstants.STATUS_TYPE_CODE))
            .thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.eq(StatusTypeCode.ACTIVE.name()))
            .thenReturn(ProgramAwardTierFindBy);
        PowerMockito.when(ProgramAwardTierFindBy.findOne()).thenReturn(testProgramAwardTierList.get(0));
        
        PowerMockito.when(programMiscDao.findBy()).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq(testNominationDTO.getProgramId())).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.and(ProjectConstants.VF_NAME)).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.eq("ROUTE_NEXT_LEVEL_MANAGER")).thenReturn(programMiscFindBy);
        PowerMockito.when(programMiscFindBy.find()).thenReturn(testProgramMiscList);
        
        PowerMockito.when(approvalProcessConfigDao.findBy()).thenReturn(approvalProcessConfigFindBy);
        PowerMockito.when(approvalProcessConfigFindBy.where(ProjectConstants.TARGET_ID))
            .thenReturn(approvalProcessConfigFindBy);
        PowerMockito.when(approvalProcessConfigFindBy.eq(testProgramAwardTierList.get(0).getId()))
            .thenReturn(approvalProcessConfigFindBy);
        PowerMockito.when(approvalProcessConfigFindBy.find()).thenReturn(testApprovalProcessConfigList);

        PowerMockito.when(cnxtRelationshipRepository.getActiveManagerRelationships(anyListOf(Long.class)))
            .thenReturn(relationshipList);

        PowerMockito.when(environment.getProperty(ApplicationDataConstants.KEY_NAME_UNASSIGNED_APPROVER))
            .thenReturn(TEST_UNASSIGNED_APPROVER_PAX);

        PowerMockito.when(approvalProcessDao.findBy()).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.where(ProjectConstants.PROGRAM_ID)).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.where(ProjectConstants.TARGET_ID)).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.eq(any(Long.class))).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.in(anyListOf(Long.class))).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.not()).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.and(ProjectConstants.TARGET_ID)).thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.and(ProjectConstants.STATUS_TYPE_CODE))
            .thenReturn(approvalProcessFindBy);
        PowerMockito.when(approvalProcessFindBy.findOne()).thenReturn(testApprovalProcess);

        PowerMockito.doNothing().when(approvalProcessDao).save(any(ApprovalProcess.class));
        PowerMockito.doNothing().when(approvalProcessDao).update(testApprovalProcess);

        pax.setPaxId(TestConstants.ID_1);

        PowerMockito.doNothing().when(alertService).addApprovalActionTakenNotification(anyListOf(RecognitionDetailsDTO.class), anyLong());

        PowerMockito.when(cnxtProgramMiscRepository.findByNominationIdAndVfNameList(anyLong(), anyListOf(String.class)))
            .thenReturn(new ArrayList<ProgramMisc>());

        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.FALSE);

        //Notify Others
        PowerMockito.when(auxiliaryNotificationDao.findBy()).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.where(anyString())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.eq(anyLong())).thenReturn(auxiliaryNotificationFindBy);
        PowerMockito.when(auxiliaryNotificationFindBy.find(anyString(), Matchers.<Class<Long>>any())).thenReturn(null);
    }

    @Test
    public void test_approve_recognitions_good(){

        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, RECOGNITION_DETAIL_LIST);

        Mockito.verify(recognitionDao, times(RECOGNITION_DETAIL_LIST.size())).update(any(Recognition.class));
        Mockito.verify(approvalHistoryDao, times(RECOGNITION_DETAIL_LIST.size())).save(any(ApprovalHistory.class));
        Mockito.verify(approvalProcessDao, times(RECOGNITION_DETAIL_LIST.size())).update(any(ApprovalProcess.class));
        Mockito.verify(approvalPendingDao, times(RECOGNITION_DETAIL_LIST.size())).delete(any(ApprovalPending.class));
        Mockito.verify(nominationDao, times(1)).update(any(Nomination.class));
        Mockito.verify(alertDao, times(2)).update(any(Alert.class));
    }

    @Test
    public void test_approve_recognitions_good_other_approvers(){

        PowerMockito.when(recognitionPendingFindBy.count()).thenReturn(1);

        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, RECOGNITION_DETAIL_LIST);

        Mockito.verify(recognitionDao, times(RECOGNITION_DETAIL_LIST.size())).update(any(Recognition.class));
        Mockito.verify(approvalHistoryDao, times(RECOGNITION_DETAIL_LIST.size())).save(any(ApprovalHistory.class));
        Mockito.verify(approvalProcessDao, times(RECOGNITION_DETAIL_LIST.size())).update(any(ApprovalProcess.class));
        Mockito.verify(approvalPendingDao, times(RECOGNITION_DETAIL_LIST.size())).delete(any(ApprovalPending.class));
        // The below should only be called if there are no outstanding pending approvals left
        //(recognitionPendingFindBy.count() returns 0)
        Mockito.verify(nominationDao, times(0)).update(any(Nomination.class));
        Mockito.verify(alertDao, times(0)).update(any(Alert.class));
    }

    @Test(expected = ErrorMessageException.class)
    public void test_approve_recognitions_bad_nomination_id(){

        ArrayList<RecognitionDetailsDTO> badRecognitionsList = new ArrayList<>(RECOGNITION_DETAIL_LIST);
        for (RecognitionDetailsDTO recognitionDetail : badRecognitionsList) {
            recognitionDetail.setNominationId(TestConstants.ID_2);
        }
        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, badRecognitionsList);

    }

    @Test(expected = ErrorMessageException.class)
    public void test_approve_recognitions_bad_status(){

        ArrayList<RecognitionDetailsDTO> badRecognitionsList = new ArrayList<>(RECOGNITION_DETAIL_LIST);
        for (RecognitionDetailsDTO recognitionDetail : badRecognitionsList) {
            recognitionDetail.setStatus("INCORRECT");
        }
        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, badRecognitionsList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_approve_recognitions_missing_comment(){

        ArrayList<RecognitionDetailsDTO> badRecognitionsList = new ArrayList<>(RECOGNITION_DETAIL_LIST);
        for (RecognitionDetailsDTO recognitionDetail : badRecognitionsList) {
            recognitionDetail.setStatus(StatusTypeCode.REJECTED.name());
            recognitionDetail.setComment(null);
        }
        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, badRecognitionsList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_approve_recognitions_recognition_does_not_exist(){

        ArrayList<RecognitionDetailsDTO> badRecognitionsList = new ArrayList<>(RECOGNITION_DETAIL_LIST);
        for (RecognitionDetailsDTO recognitionDetail : badRecognitionsList) {
            recognitionDetail.setId(5L);
        }
        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, badRecognitionsList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_approve_recognitions_recognition_not_approveable(){

        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        approvalPendingList.add(new ApprovalPending().setTargetId(3L).setTargetTable(TableName.RECOGNITION.name())
                .setApprovalProcessId(TestConstants.ID_1).setId(TestConstants.ID_1).setNextApproverPaxId(APPROVER_PAX_ID));
        approvalPendingList.add(new ApprovalPending().setTargetId(4L).setTargetTable(TableName.RECOGNITION.name())
                .setApprovalProcessId(TestConstants.ID_2).setId(TestConstants.ID_2).setNextApproverPaxId(APPROVER_PAX_ID));
        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(approvalPendingList);

        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, RECOGNITION_DETAIL_LIST);
    }

    @Test
    public void test_approveOrRejectRaises_happy_path() {
        final int raiseQuantity = 10;
        List<RaiseDTO> raiseDTOList = new ArrayList<>();
        List<ApprovalPending> approvalPendingList = new ArrayList<>();
        for (int i = 0; i < raiseQuantity; i++) {
            RaiseDTO raiseDTO = new RaiseDTO();
            raiseDTO.setId(new Long(i));
            raiseDTO.setStatus(StatusTypeCode.PENDING.toString());
            raiseDTO.setComment("Blah");
            raiseDTOList.add(raiseDTO);

            ApprovalPending approvalPending = new ApprovalPending();
            approvalPending.setId(new Long(i));
            approvalPending.setApprovalProcessId(TestConstants.ID_1);
            approvalPending.setTargetId(new Long(i));
            approvalPending.setTargetTable(TableName.RECOGNITION.name());
            approvalPendingList.add(approvalPending);
        }

        PowerMockito.when(approvalPendingFindBy.find()).thenReturn(approvalPendingList);

        approvalService.approveOrRejectRaises(raiseDTOList, TestConstants.ID_1);

        Mockito.verify(approvalHistoryDao, times(raiseQuantity)).save(any(ApprovalHistory.class));
        Mockito.verify(approvalProcessDao, times(raiseQuantity)).update(any(ApprovalProcess.class));
        Mockito.verify(approvalPendingDao, times(raiseQuantity)).delete(any(ApprovalPending.class));
        Mockito.verify(nominationDao, times(1)).update(any(Nomination.class));
        Mockito.verify(newsfeedItemDao, times(1)).update(any(NewsfeedItem.class));
        Mockito.verify(alertDao, times(2)).update(any(Alert.class));
    }

    @Test
    public void test_create_pending_approvals_happy_path() {
        boolean pendingApproval = approvalService.createPendingApprovals(
                recognitionList, TestConstants.PAX_DAGARFIN, TestConstants.ID_1, 50, TestConstants.ID_1, 1L);

        assertTrue(pendingApproval);

    }

    @Test
    public void test_bulk_fetch_approvers_happy_path() {
        Map<Long, Long> approverList = approvalService.bulkFetchApprovers(Arrays.asList(165L, 141L), TestConstants.PAX_DAGARFIN,
                ApprovalType.SUBMITTER_FIRST.toString(), TestConstants.PAX_PORTERGA, 1L,false);

        assertThat(approverList.get(141L), is(170L));
        assertThat(approverList.get(165L), is(170L));
    }

    @Test
    public void test_bulk_fetch_approvers_budget_owner() {
        PowerMockito.when(approvalDao.getBudgetOwnerPaxId(Matchers.anyLong())).thenReturn(50L);
        Map<Long, Long> approverList = approvalService.bulkFetchApprovers(Arrays.asList(165L, 141L), TestConstants.PAX_DAGARFIN,
                ApprovalType.BUDGET_OWNER.toString(), null, 1L,false);

        assertThat(approverList.get(165L), is(50L));
        assertThat(approverList.get(141L), is(50L));
    }

    @Test public void test_bulk_fetch_approvers_budget_owner_unassigned_pax() {
        PowerMockito.when(approvalDao.getBudgetOwnerPaxId(Matchers.anyLong())).thenReturn(null);
        Map<Long, Long> approverList = approvalService.bulkFetchApprovers(Arrays.asList(165L, 141L), TestConstants.PAX_DAGARFIN,
                ApprovalType.BUDGET_OWNER.toString(), null, 1L,false);

        assertThat(approverList.get(165L), is(350L));
        assertThat(approverList.get(141L), is(350L));
    }

    @Test
    public void test_fetch_approval_process_configs_for_award_tier_happy_path() {
        List<ApprovalProcessConfig> approvalProcessConfigs =
                approvalService.fetchApprovalProcessConfigsForAwardTier(TestConstants.ID_1, 50);

        assertTrue(approvalProcessConfigs.size() > 0);
        assertThat(approvalProcessConfigs.get(0).getApprovalTypeCode(), is(ApprovalType.SUBMITTER_FIRST.toString()));
        assertThat(approvalProcessConfigs.get(0).getApprovalLevel(), is(TestConstants.ID_1));
        assertThat(approvalProcessConfigs.get(0).getTargetId(), is (2002L));

        assertThat(approvalProcessConfigs.get(1).getApprovalTypeCode(), is(ApprovalType.SUBMITTER_SECOND.toString()));
        assertThat(approvalProcessConfigs.get(1).getApprovalLevel(), is(TestConstants.ID_2));
        assertThat(approvalProcessConfigs.get(1).getTargetId(), is(2002L));
    }

    @Test
    public void test_determineApprovalTimestampByRecognitionId_for_recognition() {
        Date approvalDate = new Date();
        ApprovalProcess approvalProcess = new ApprovalProcess();
        approvalProcess.setTargetId(TestConstants.ID_1);
        approvalProcess.setTargetTable(TableName.RECOGNITION.name());
        approvalProcess.setLastApprovalDate(approvalDate);
        List<ApprovalProcess> approvalProcessList = Arrays.asList(approvalProcess);

        PowerMockito.when(approvalProcessFindBy.find()).thenReturn(approvalProcessList);

        Date resultDate = approvalService.determineApprovalTimestampByRecognitionId(TestConstants.ID_1);

        assertEquals(approvalDate, resultDate);
        Mockito.verifyZeroInteractions(approvalDao);
    }

    @Test
    public void test_determineApprovalTimestampByRecognitionId_for_auto_approved_recognition() {
        Date approvalDate = new Date();

        // Recognition entry not exist in approval process
        PowerMockito.when(approvalProcessFindBy.find()).thenReturn(new ArrayList<ApprovalProcess>());

        Map<Long, Date> approvalDateDaoMap = new HashMap<>();
        approvalDateDaoMap.put(TestConstants.ID_1, approvalDate);
        PowerMockito.when(approvalDao.getApprovedRecognitionCreateDates(Arrays.asList(TestConstants.ID_1)))
                .thenReturn(approvalDateDaoMap);

        Date resultDate = approvalService.determineApprovalTimestampByRecognitionId(TestConstants.ID_1);

        assertEquals(approvalDate, resultDate);
    }

    @Test
    public void test_determineApprovalTimestampByRecognitionId_for_recognitions() {
        int recognitionQuantity = 10;
        Date approvalDate = new Date();
        List<Long> recognitionIdList = new ArrayList<>();
        List<ApprovalProcess> approvalProcessList = new ArrayList<>();
        for (long i = 1; i < recognitionQuantity; i++) {
            recognitionIdList.add(i);
            ApprovalProcess approvalProcess = new ApprovalProcess();
            approvalProcess.setTargetId(i);
            approvalProcess.setTargetTable(TableName.RECOGNITION.name());
            approvalProcess.setLastApprovalDate(approvalDate);
            approvalProcessList.add(approvalProcess);
        }

        PowerMockito.when(approvalProcessFindBy.find()).thenReturn(approvalProcessList);

        Map<Long, Date> approvalDateMap = approvalService.determineApprovalTimestampByRecognitionId(recognitionIdList);

        for (long i = 1; i < recognitionQuantity; i++) {
            assertEquals(approvalDate, approvalDateMap.get(i));
        }
        Mockito.verifyZeroInteractions(approvalDao);
    }

    @Test
    public void test_determineApprovalTimestampByRecognitionId_for_auto_approved_recognitions() {
        int recognitionQuantity = 10;
        Date approvalDate = new Date();
        List<Long> recognitionIdList = new ArrayList<>();
        Map<Long, Date> approvalDateDaoMap = new HashMap<>();
        for (long i = 1; i < recognitionQuantity; i++) {
            recognitionIdList.add(i);
            approvalDateDaoMap.put(i, approvalDate);
        }
        PowerMockito.when(approvalDao.getApprovedRecognitionCreateDates(recognitionIdList))
                .thenReturn(approvalDateDaoMap);

        // Recognition entries not exist in approval process
        PowerMockito.when(approvalProcessFindBy.find()).thenReturn(new ArrayList<ApprovalProcess>());

        Map<Long, Date> approvalDateMap = approvalService.determineApprovalTimestampByRecognitionId(recognitionIdList);

        for (long i = 1; i < recognitionQuantity; i++) {
            assertEquals(approvalDate, approvalDateMap.get(i));
        }
    }

    @Test(expected = ErrorMessageException.class)
    public void test_proxyApprovalNoPermission() {
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_1);
        PowerMockito.when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.FALSE);

        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, RECOGNITION_DETAIL_LIST);
    }

    @Test
    public void test_proxyApprovalWithPermission() {
        PowerMockito.when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        PowerMockito.when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_1);
        PowerMockito.when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.TRUE);

        approvalService.approveOrRejectRecognitions(APPROVER_PAX_ID, TestConstants.ID_1, RECOGNITION_DETAIL_LIST);
    }
}
