package com.maritz.culturenext.approval.dao;

import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ApprovalDaoTest extends AbstractDatabaseTest{

    public static final Long NOMINATION_ID = 70788L;
    public static final Long NOMINATION_ID_INVALID = 123456L;
    public static final Long BUDGET_OWNER_PAX = 5564L;

    @Inject
    private ApprovalDao approvalDao;

    @Test
    public void test_getBudgetOwnerPaxId_happy_path() {
        Long budgetOwnerPaxId = approvalDao.getBudgetOwnerPaxId(NOMINATION_ID);

        assertTrue(budgetOwnerPaxId.equals(BUDGET_OWNER_PAX));
    }

    @Test
    public void test_getBudgetOwnerPaxId_invalid_nomination_id() {
        Long budgetOwnerPaxId = approvalDao.getBudgetOwnerPaxId(NOMINATION_ID_INVALID);

        assertNull(budgetOwnerPaxId);
    }

    @Test
    public void test_getBudgetOwnerPaxId_null_nomination_id() {
        Long budgetOwnerPaxId = approvalDao.getBudgetOwnerPaxId(null);

        assertNull(budgetOwnerPaxId);
    }
}