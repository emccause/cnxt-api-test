package com.maritz.culturenext.content.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.content.services.TranslatableContentService;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.test.AbstractRestTest;

@PrepareForTest(TranslatableContentService.class)
public class TranslatableContentRestTest extends AbstractRestTest {

    @Test
    public void getTranslatableContentCSV() throws Exception {
        mockMvc.perform(
                get("/rest/translatable-content")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header("content-type", "application/csv")
            ).andExpect(status().isOk());
    }

    @Test
    public void getTranslatableContentCSV_by_targetType() throws Exception {
        mockMvc.perform(
                get("/rest/translatable-content?targetType=CALENDAR_ENTRY")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header("content-type", "application/csv")
            ).andExpect(status().isOk());
    }
    
    @Test
    public void getTranslatableContentCSV_platformOnly() throws Exception {
        mockMvc.perform(
                get("/rest/translatable-content?platformOnly=true")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header("content-type", "application/csv")
            ).andExpect(status().isOk());
    }
    
    @Test
    public void uploadTranslatableContentCSV() throws Exception {
        
        String path = getClass().getClassLoader().getResource("test-data").getPath();
        FileInputStream fis = new FileInputStream(path + File.separator + "translatable_content.csv");
        MockMultipartFile multipartFile = new MockMultipartFile(
                "translatable-content", "translatable_content.csv", MediaTypesEnum.TEXT_CSV.value(), fis);

        mockMvc.perform(
                fileUpload("/rest/translatable-content")
                .file(multipartFile)
                .with(authToken(TestConstants.USER_HARWELLM)))
                .andExpect(status().isOk());
        
        fis.close();
        
    }
}
