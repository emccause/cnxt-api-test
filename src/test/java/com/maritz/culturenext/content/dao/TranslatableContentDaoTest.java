package com.maritz.culturenext.content.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.content.dao.TranslatableContentDao;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TranslatableContentDaoTest extends AbstractDatabaseTest {

    @Inject private TranslatableContentDao translatableContentDao;

    private static final List<String> validTables = Arrays.asList("APPLICATION_DATA", "CALENDAR_ENTRY", "FILES");
    
    @Test
    public void get_platform_translatable_content() throws Exception {

        List<Map<String,Object>> nodes = translatableContentDao.getPlatformTranslatableContent();

        assertThat(nodes.size(), is(41));

        //Only APPLICATION_DATA, CALENDAR_ENTRY, and FILES records should be returned
        for (Map<String, Object> node : nodes) {
            assertNotNull(node);
            assertTrue(validTables.contains(node.get(ProjectConstants.TABLE_NAME)));
            assertNotNull(node.get(ProjectConstants.KEY));
            assertNotNull(node.get("enUs"));
        }
    }
}
