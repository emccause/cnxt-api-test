package com.maritz.culturenext.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jpa.entity.Acl;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class AclUtilTest extends AbstractDatabaseTest {

    private static final long TEST_PROGRAM_ID = 3534L;
    private static final long TEST_PAX_ID_1 = 4417L;
    private static final long TEST_PAX_ID_2 = 5961L;
    private static final long TEST_GROUP_1_ID = 1447L;
    private static final long TEST_GROUP_2_ID = 1446L;
    private static final String RECEIVE_ROLE_NAME = "Program Receiver";
    private static final String GIVER_ROLE_NAME = "Program Giver";
    private static final String PAX = "PAX";
    private static final String GROUPS = "GROUPS";
    private static final String PROGRAM = "PROGRAM";

    @Inject private AclUtil aclUtil;

    @Before
    public void setUp() throws Exception {

        setupAclDatabase();
    }

    /**
     * Sets up the ACL database with some initial values.
     */
    private void setupAclDatabase() {
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS, TEST_GROUP_1_ID);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID, PROGRAM,GROUPS, TEST_GROUP_1_ID);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID, PROGRAM,GROUPS, TEST_GROUP_2_ID);

        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TEST_PAX_ID_1);
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TEST_PAX_ID_2);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TEST_PAX_ID_2);
    }

    // Getter tests

    @Test
    public void testGetGiverAclEligibilityReceivers() {
        int expectedGivers = 3;
        List<Acl> givers = aclUtil.getAclEligibilityGivers(TEST_PROGRAM_ID);
        assertNotNull(givers);
        assertEquals(expectedGivers, givers.size());
    }

    @Test
    public void testGetReceiverAclEligibilityReceivers() {
        int expectedReceivers = 3;
        List<Acl> receivers = aclUtil.getAclEligibilityReceivers(TEST_PROGRAM_ID);
        assertNotNull(receivers);
        assertEquals(expectedReceivers, receivers.size());
    }

    // Add/Remove ACL tests

    @Test
    public void testIndividualAddAndDelete() {
        // verify pre-existing number of givers/receivers
        List<Acl> members = aclUtil.getAllAclEligibility(TEST_PROGRAM_ID);
        int existingMembersSize = members.size();

        // add dummy PAX and Group receivers/givers
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS,TestConstants.ID_1);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS, TestConstants.ID_1);

        // verify add members
        members = aclUtil.getAllAclEligibility(TEST_PROGRAM_ID);
        int addedMembersSize = members.size();
        assertEquals(existingMembersSize + 4, addedMembersSize);

        // delete added members
        aclUtil.deleteAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.deleteAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.deleteAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM, GROUPS, TestConstants.ID_1);
        aclUtil.deleteAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS, TestConstants.ID_1);

        // verify delete
        members = aclUtil.getAllAclEligibility(TEST_PROGRAM_ID);
        int deletedMembersSize = members.size();
        assertEquals(existingMembersSize, deletedMembersSize);
    }

    @Test
    public void testProgramAllDelete() {
        // add dummy PAX and Group receivers/givers
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,PAX, TestConstants.ID_1);
        aclUtil.addAcl(GIVER_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS, TestConstants.ID_1);
        aclUtil.addAcl(RECEIVE_ROLE_NAME,TEST_PROGRAM_ID,PROGRAM,GROUPS, TestConstants.ID_1);

        // verify non-zero members
        List<Acl> members = aclUtil.getAllAclEligibility(TEST_PROGRAM_ID);
        int existingSize = members.size();
        assertTrue(existingSize > 0);

        // delete all members associated with program
        aclUtil.deleteAcl(null,TEST_PROGRAM_ID,PROGRAM,null,null);

        // verify delete
        members = aclUtil.getAllAclEligibility(TEST_PROGRAM_ID);
        int expectedDeletedSize = 0;
        int deletedMembersSize = members.size();
        assertEquals(expectedDeletedSize, deletedMembersSize);
    }
}