package com.maritz.culturenext.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;

import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.PermissionManagementTypes;
import com.maritz.culturenext.permission.constants.PermissionConstants;
import com.maritz.test.AbstractMockTest;

public class PermissionUtilTest extends AbstractMockTest {

    @InjectMocks private PermissionUtil permissionUtil;
    @Mock private Environment environment;
    @Mock private Security security;

    private static final String INVALID_PERMISSION_TYPE = "TESTING";


    @Test
    public void test_no_permissions_default_false() {
        PowerMockito.when(security.hasPermission(PermissionManagementTypes.ENABLE_COMMENTING.toString())).thenReturn(false);
        PowerMockito.when(environment.getProperty(
                ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED)).thenReturn(ProjectConstants.FALSE);
        assertFalse(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE));
    }

    @Test
    public void test_no_permissions_default_true() {
        PowerMockito.when(security.hasPermission(PermissionManagementTypes.DISABLE_LIKING.toString())).thenReturn(false);
        PowerMockito.when(environment.getProperty(
                ApplicationDataConstants.KEY_NAME_LIKING_ENABLED)).thenReturn(ProjectConstants.TRUE);
        assertTrue(permissionUtil.hasPermission(PermissionConstants.LIKING_TYPE));
    }

    @Test
    public void test_enable_permission() {
        PowerMockito.when(security.hasPermission(PermissionManagementTypes.ENABLE_COMMENTING.toString())).thenReturn(true);
        PowerMockito.when(environment.getProperty(
                ApplicationDataConstants.KEY_NAME_COMMENTING_ENABLED)).thenReturn(ProjectConstants.FALSE);
        assertTrue(permissionUtil.hasPermission(PermissionConstants.COMMENTING_TYPE));
    }

    @Test
    public void test_disable_permission() {
        PowerMockito.when(security.hasPermission(PermissionManagementTypes.DISABLE_LIKING.toString())).thenReturn(true);
        PowerMockito.when(environment.getProperty(
                ApplicationDataConstants.KEY_NAME_LIKING_ENABLED)).thenReturn(ProjectConstants.TRUE);
        assertFalse(permissionUtil.hasPermission(PermissionConstants.LIKING_TYPE));
    }

    @Test
    public void test_invalid_permission_type() {
        assertFalse(permissionUtil.hasPermission(INVALID_PERMISSION_TYPE));
    }
}
