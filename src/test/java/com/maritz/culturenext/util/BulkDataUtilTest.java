package com.maritz.culturenext.util;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BulkDataUtilTest {
    
    @Test(expected = NullPointerException.class)
    public void test_buildCommonTablesQuery_nullParams() {
        String mockQuery = "test query";
        List<Long> mockPaxIds = Collections.singletonList(1L);

        BulkDataUtil.buildCommonTablesQuery(mockQuery,
                                            null,
                                            mockPaxIds,
                                            new ArrayList<Long>(),
                                            new ArrayList<String>());
    }

    @Test
    public void test_buildCommonTablesQuery_nullLists() {
        String mockQuery = "test query";
        MapSqlParameterSource mockParams = new MapSqlParameterSource("testParamName", "testParamValue");

        String expectedQuery = mockQuery;
        String actualQuery = BulkDataUtil.buildCommonTablesQuery(mockQuery,
                                                                 mockParams,
                                                                 null,
                                                                 null,
                                                                 null);
        Assert.assertEquals(expectedQuery, actualQuery);
    }

    @Test
    public void test_buildCommonTablesQuery_missingLists() {
        String mockQuery = "test query";
        MapSqlParameterSource mockParams = new MapSqlParameterSource("testParamName", "testParamValue");

        String expectedQuery = mockQuery;
        String actualQuery = BulkDataUtil.buildCommonTablesQuery(mockQuery,
                                                                 mockParams,
                                                                 new ArrayList<Long>(),
                                                                 new ArrayList<Long>(),
                                                                 new ArrayList<String>());
        Assert.assertEquals(expectedQuery, actualQuery);
    }
}
