package com.maritz.culturenext.util;

import com.google.common.collect.ImmutableMap;
import com.maritz.core.jpa.entity.Acl;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.util.EntityUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.program.constants.ProgramConstants;
import com.maritz.culturenext.program.dto.EligibilityEntityStubDTO;

import org.apache.commons.collections4.ListUtils;
import org.junit.Test;

import javax.annotation.Nonnull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;


public class EntityUtilTest {

    //Subject Types
    private static final String GROUPS = "GROUPS";
    private static final String PAX = "PAX";
    
    private Long activeId = 8571L;
    private Long inactiveId = 6861L;
    private Long newId = 12211L;
    private Long restrictedId = 989L;
    private Long suspendedId = 8851L;
    private Long lockedId = 11187L;
    
    // budget entity error mapping
    private Map<String, String> errorInformation = ImmutableMap.<String, String>builder()
            .put(EntityUtil.getErrorEntityNotFound(), ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND)
            .put(EntityUtil.getErrorEntityNotFoundMsg(), ProgramConstants.ERROR_GIVING_MEMBER_NOT_FOUND_MSG)
            .put(EntityUtil.getErrorPaxEntityNotActive(), ProgramConstants.ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE)
            .put(EntityUtil.getErrorPaxEntityNotActiveMsg(), ProgramConstants.ERROR_PAX_GIVING_MEMBER_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityNotActive(), ProgramConstants.ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE)
            .put(EntityUtil.getErrorGroupEntityNotActiveMsg(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_NOT_ACTIVE_MSG)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupType(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE)
            .put(EntityUtil.getErrorGroupEntityInvalidGroupTypeMsg(), 
                    ProgramConstants.ERROR_GROUP_GIVING_MEMBER_INVALID_GROUP_TYPE_MSG)
            .build();

    // Filter Duplicate Eligibility Entities tests

    @Test
    public void testFilterDuplicateEligibilityEntitiesEmptyList() {
        // filter empty list
        List<EligibilityEntityStubDTO> entities =
                EntityUtil.filterDuplicateEligibilityEntities(new ArrayList<EligibilityEntityStubDTO>());

        // assert filtered entities are also empty
        assertTrue(entities.isEmpty());
    }

    @Test
    public void testFilterDuplicateEligibilityEntitiesNoDuplicates() {
        List<EligibilityEntityStubDTO> entities =
                Arrays.asList(generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(null, TestConstants.ID_1));

        // filter entity list
        List<EligibilityEntityStubDTO> filteredEntities =
                EntityUtil.filterDuplicateEligibilityEntities(entities);

        // assert filtered entities are the same (no duplicates were provided)
        List<Long> expectedGroupIds = Arrays.asList(TestConstants.ID_1);
        List<Long> expectedPaxIds = Arrays.asList(TestConstants.ID_1);
        assertEquals(expectedGroupIds.size() + expectedPaxIds.size(), filteredEntities.size());
        verifyEligibilityEntities(filteredEntities, expectedGroupIds, expectedPaxIds);
    }

    @Test
    public void testFilterDuplicateEligibilityEntitiesAllDuplicates() {
        List<EligibilityEntityStubDTO> entities =
                Arrays.asList(
                        generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(TestConstants.ID_1, null),
                        generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(TestConstants.ID_1, null));

        // filter entity list
        List<EligibilityEntityStubDTO> filteredEntities =
                EntityUtil.filterDuplicateEligibilityEntities(entities);

        // assert filtered contains no duplicates
        List<Long> expectedGroupIds = Arrays.asList(TestConstants.ID_1);
        List<Long> expectedPaxIds = new ArrayList<Long>();
        assertEquals(expectedGroupIds.size() + expectedPaxIds.size(), filteredEntities.size());
        verifyEligibilityEntities(filteredEntities, expectedGroupIds, expectedPaxIds);
    }

    @Test
    public void testFilterDuplicateEligibilityEntities() {
        List<EligibilityEntityStubDTO> entities =
                Arrays.asList(
                        generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(3L, null),
                        generateEligibilityEntity(4L, null), generateEligibilityEntity(TestConstants.ID_1, null),
                        generateEligibilityEntity(3L, null), generateEligibilityEntity(TestConstants.ID_1, null),
                        generateEligibilityEntity(null, TestConstants.ID_2), generateEligibilityEntity(null, 9L),
                        generateEligibilityEntity(null, 5L), generateEligibilityEntity(null, 7L),
                        generateEligibilityEntity(null, TestConstants.ID_2), generateEligibilityEntity(null, 7L));

        // filter entity list
        List<EligibilityEntityStubDTO> filteredEntities =
                EntityUtil.filterDuplicateEligibilityEntities(entities);

        // assert filtered contains no duplicates
        List<Long> expectedGroupIds = Arrays.asList(TestConstants.ID_1, 3L, 4L);
        List<Long> expectedPaxIds = Arrays.asList(TestConstants.ID_2, 5L, 7L, 9L);
        assertEquals(expectedGroupIds.size() + expectedPaxIds.size(), filteredEntities.size());
        verifyEligibilityEntities(filteredEntities, expectedGroupIds, expectedPaxIds);
    }

    // Filter Duplicate Ids tests

    @Test
    public void testFilterDuplicateIdsEmptyList() {
        // filter empty list
        List<Long> ids = EntityUtil.filterDuplicateIds(new ArrayList<Long>());

        // assert filtered ids are also empty
        assertTrue(ids.isEmpty());
    }

    @Test
    public void testFilterDuplicateIdsNoDuplicates() {
        List<Long> expectedIds = Arrays.asList(TestConstants.ID_1, TestConstants.ID_2, 3L);

        // filter list of values
        List<Long> ids = EntityUtil.filterDuplicateIds(expectedIds);

        // assert filtered ids are the same values (no duplicates were provided)
        assertEquals(expectedIds.size(), ids.size());
        assertTrue(ListUtils.isEqualList(ids, expectedIds));
    }

    @Test
    public void testFilterDuplicateIdsAllDuplicates() {
        List<Long> duplicateIds = Arrays.asList(TestConstants.ID_1, TestConstants.ID_1, TestConstants.ID_1, TestConstants.ID_1, TestConstants.ID_1);

        // filter list of duplicate values
        List<Long> ids = EntityUtil.filterDuplicateIds(duplicateIds);

        // assert filtered ids remove all other duplicates (except one instance of value)
        List<Long> expectedIds = Arrays.asList(TestConstants.ID_1);
        assertEquals(1, ids.size());
        assertTrue(ListUtils.isEqualList(ids, expectedIds));
    }

    @Test
    public void testFilterDuplicateIds() {
        List<Long> ids = Arrays.asList(TestConstants.ID_1, TestConstants.ID_1, 5L, TestConstants.ID_2, 3L, 3L, 3L, 6L, 4L, 4L);

        // filter list of values (with duplicates and non-duplicate values)
        List<Long> filteredIds = EntityUtil.filterDuplicateIds(ids);

        // assert filtered ids removes duplicates (except one instance of value)
        List<Long> expectedIds = Arrays.asList(TestConstants.ID_1, TestConstants.ID_2, 3L, 4L, 5L, 6L);
        Collections.sort(filteredIds);
        Collections.sort(expectedIds);
        assertEquals(expectedIds.size(), filteredIds.size());
        assertTrue(ListUtils.isEqualList(filteredIds, expectedIds));
    }

    // isEligibilityEntitiesSame tests

    @Test
    public void testIsEligibilityEntitiesSameBothEmpty() {
        // both empty so should be same
        assertTrue(EntityUtil.isEligibilityEntitiesSame(
                new ArrayList<Acl>(), new ArrayList<EligibilityEntityStubDTO>()));
    }

    @Test
    public void testIsEligibilityEntitiesSameEmptyExistingAndMissingActual() {
        // missing is treated as empty so both should be same
        assertTrue(EntityUtil.isEligibilityEntitiesSame(
                new ArrayList<Acl>(), null));
    }

    @Test
    public void testIsEligibilityEntitiesSameEmptyExistingNonEmptyActual() {
        List<EligibilityEntityStubDTO> eligibilityEntities =
                Arrays.asList(generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(null, TestConstants.ID_1));

        // empty existing and non empty actual should not be same
        assertFalse(EntityUtil.isEligibilityEntitiesSame(
                new ArrayList<Acl>(), eligibilityEntities));
    }

    @Test
    public void testIsEligibilityEntitiesSameNonEmptyExistingEmptyActual() {
        List<Acl> aclEntities =
                Arrays.asList(generateAclEntity(TestConstants.ID_1, null), generateAclEntity(null, TestConstants.ID_1));

        // non empty existing and empty actual should not be same
        assertFalse(EntityUtil.isEligibilityEntitiesSame(
                aclEntities, new ArrayList<EligibilityEntityStubDTO>()));
    }

    @Test
    public void testIsEligibilityEntitiesSameDifferentExistingAndActual() {
        List<Acl> aclEntities =
                Arrays.asList(generateAclEntity(TestConstants.ID_1, null));
        List<EligibilityEntityStubDTO> eligibilityEntities =
                Arrays.asList(generateEligibilityEntity(null, TestConstants.ID_1));

        // different acl and eligibility entities should not be same
        assertFalse(EntityUtil.isEligibilityEntitiesSame(
                aclEntities, eligibilityEntities));
    }

    @Test
    public void testIsEligibilityEntitiesSameDifferentExistingAndActualWithDuplicates() {
        List<Acl> aclEntities =
                Arrays.asList(generateAclEntity(TestConstants.ID_1, null));
        List<EligibilityEntityStubDTO> eligibilityEntities =
                Arrays.asList(generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(TestConstants.ID_1, null),
                              generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(TestConstants.ID_1, null));

        // acl and eligibility entities have same entity representation but
        //eligibility has duplicates so should not be same
        assertFalse(EntityUtil.isEligibilityEntitiesSame(
                aclEntities, eligibilityEntities));
    }

    @Test
    public void testIsEligibilityEntitiesSameExistingAndActualSame() {
        List<Acl> aclEntities =
                Arrays.asList(generateAclEntity(TestConstants.ID_1, null), generateAclEntity(null, TestConstants.ID_1));
        List<EligibilityEntityStubDTO> eligibilityEntities =
                Arrays.asList(generateEligibilityEntity(TestConstants.ID_1, null), generateEligibilityEntity(null, TestConstants.ID_1));

        // existing and actual represent same entities so should be same
        assertTrue(EntityUtil.isEligibilityEntitiesSame(
                aclEntities, eligibilityEntities));
    }
    
    @Test
    public void testValidateEntityStateActivePax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(activeId, StatusTypeCode.ACTIVE.name()), errorInformation, 
                ProjectConstants.GIVING_MEMBERS);
        
        assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testValidateEntityStateInactivePax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(inactiveId, StatusTypeCode.INACTIVE.name()), errorInformation,
                ProjectConstants.GIVING_MEMBERS);
        
        //INACTIVE status should work fine
        assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testValidateEntityStateNewPax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(newId, StatusTypeCode.NEW.name()), errorInformation,
                ProjectConstants.GIVING_MEMBERS);
        
        //NEW status should work fine
        assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testValidateEntityStateLockedPax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(lockedId, StatusTypeCode.LOCKED.name()), errorInformation,
                ProjectConstants.GIVING_MEMBERS);
        
        //LOCKED status should work fine
        assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testValidateEntityStateRestrictedPax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(restrictedId, StatusTypeCode.RESTRICTED.name()), errorInformation,
                ProjectConstants.GIVING_MEMBERS);
        
        //RESTRICTED status should work fine
        assertTrue(errors.isEmpty());
    }
    
    @Test
    public void testValidateEntityStateSuspendedPax() {
        List<ErrorMessage> errors = new ArrayList<ErrorMessage>();        
        
        errors = EntityUtil.validateEntityState(
                setUpEntityMap(suspendedId, StatusTypeCode.SUSPENDED.name()), errorInformation, 
                ProjectConstants.GIVING_MEMBERS);
        
        //SUSPENDED status should work fine
        assertTrue(errors.isEmpty());
    }
  
    // helper methods
    
    private List<Map<String, Object>> setUpEntityMap(Long paxId, String statusTypeCode) {
        List<Map<String, Object>> entityMap = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(ProjectConstants.PAX_ID, paxId);
        map.put(ProjectConstants.STATUS_TYPE_CODE, statusTypeCode);
        map.put("inputType", "PAX");
        map.put("status", null);
        map.put("inputId", paxId);
        entityMap.add(map); 
        return entityMap;
    }
    
    private EligibilityEntityStubDTO generateEligibilityEntity(Long groupId, Long paxId) {
        EligibilityEntityStubDTO eligibilityEntityStubDTO = new EligibilityEntityStubDTO();
        eligibilityEntityStubDTO.setGroupId(groupId);
        eligibilityEntityStubDTO.setPaxId(paxId);
        return eligibilityEntityStubDTO;
    }

    private Acl generateAclEntity(Long groupId, Long paxId) {
        Acl entity = new Acl();

        String subjectType = null;
        Long subjectId = null;

        // determine subject type and id for ACL
        if(groupId != null) {
            subjectType = GROUPS;
            subjectId = groupId;
        } else if(paxId != null) {
            subjectType = PAX;
            subjectId = paxId;
        }

        entity.setSubjectTable(subjectType);
        entity.setSubjectId(subjectId);

        return entity;
    }

    private void verifyEligibilityEntities(@Nonnull List<EligibilityEntityStubDTO> entities,
                                           @Nonnull List<Long> expectedGroupIds,
                                           @Nonnull List<Long> expectedPaxIds) {
        List<Long> groupIds = new ArrayList<Long>(expectedGroupIds);
        List<Long> paxIds = new ArrayList<Long>(expectedPaxIds);

        for(EligibilityEntityStubDTO entity : entities) {
            if((entity.getGroupId() != null && entity.getPaxId() != null) ||
                    ((entity.getGroupId() == null && entity.getPaxId() == null))) {
                fail("Entity is invalid!!!");
            }

            if(entity.getGroupId() != null) {
                Long groupId = entity.getGroupId();
                if(groupIds.contains(groupId)) {
                    groupIds.remove(groupId);
                } else {
                    fail("Group ID is not expected!!");
                }
            } else if(entity.getPaxId() != null) {
                Long paxId = entity.getPaxId();
                if(paxIds.contains(paxId)) {
                    paxIds.remove(paxId);
                } else {
                    fail("Pax ID is not expected!!");
                }
            }
        }

        if(!groupIds.isEmpty() || !paxIds.isEmpty()) {
            fail("Expected IDs were not accounted for!!");
        }
    }
}