package com.maritz.culturenext.util;


import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.social.comment.dto.CommentDTO;

public class PaginationUtilTest {

    int page;
    int size;

    List<Long> longList = new ArrayList<>();
    List<Integer> integerList = new ArrayList<>();
    List<CommentDTO> commentDTOList = new ArrayList<>();
    List<NewsfeedItemDTO> newsfeedItemDTOList = new ArrayList<>();

    int smallSize = 25;
    int midSize = 50;
    int largeSize = 75;
    int oddNumberSize = 51;

    @Before
    public void setup() {
        longList = new ArrayList<>();
        integerList = new ArrayList<>();
        commentDTOList = new ArrayList<>();
        newsfeedItemDTOList = new ArrayList<>();

        page = 0;
        size = 0;

    }

    @Test
    public void test_pagination_page_null_size_null() {

        for (int i = 0; i <= largeSize; i++) {
            longList.add(Long.valueOf(i));
        }

        longList = PaginationUtil.getPaginatedList(longList, null, null);

        // Testing default list size
        assertThat(longList.size(), equalTo(50));
        // Testing default page number
        assertThat(longList.get(0), equalTo(0L));
    }
    @Test
    public void test_pagination_page_1_size_20() {

        for (int i = 0; i <= smallSize; i++) {
            CommentDTO commentDTO = new CommentDTO();
            commentDTO.setId(Long.valueOf(i));
            commentDTOList.add(commentDTO);
        }

        commentDTOList = PaginationUtil.getPaginatedList(commentDTOList, 1, 20);

        // Testing list size
        assertThat(commentDTOList.size(), equalTo(20));
        // Testing page number
        assertThat(commentDTOList.get(0).getId(), equalTo(0L));

    }
    @Test
    public void test_pagination_page_2_size_20() {

        for (int i = 0; i <= midSize; i++) {
            integerList.add(i);
        }

        integerList = PaginationUtil.getPaginatedList(integerList, 2, 20);

        // Testing list size
        assertThat(integerList.size(), equalTo(20));
        // Testing page number
        assertThat(integerList.get(0), equalTo(20));

    }
    @Test
    public void test_pagination_page_3_size_25() {

        for (int i = 0; i < oddNumberSize; i++) {
            NewsfeedItemDTO newsfeedItemDTO = new NewsfeedItemDTO();
            newsfeedItemDTO.setId(Long.valueOf(i));
            newsfeedItemDTOList.add(newsfeedItemDTO);
        }

        newsfeedItemDTOList = PaginationUtil.getPaginatedList(newsfeedItemDTOList, 3, 25);

        // Testing list size
        assertThat(newsfeedItemDTOList.size(), equalTo(1));
        // Testing page number
        assertThat(newsfeedItemDTOList.get(0).getId(), equalTo(50L));

    }
    @Test
    public void test_pagination_page_3_size_20() {

        for (int i = 0; i < oddNumberSize; i++) {
            integerList.add(i);
        }

        integerList = PaginationUtil.getPaginatedList(integerList, 3, 20);

        // Testing list size
        assertThat(integerList.size(), equalTo(11));
        // Testing page number
        assertThat(integerList.get(0), equalTo(40));
    }
    @Test
    public void test_pagination_page_4_size_20() {

        for (int i = 0; i < midSize; i++) {
            integerList.add(i);
        }

        integerList = PaginationUtil.getPaginatedList(integerList, 4, 20);

        // Testing list size
        assertThat(integerList.size(), equalTo(0));

    }

    @Test
    public void test_pagination_page_4_size_20_large_list() {

        for (int i = 0; i < largeSize; i++) {
            integerList.add(i);
        }

        integerList = PaginationUtil.getPaginatedList(integerList, 4, 20);

        // Testing list size
        assertThat(integerList.size(), equalTo(15));
        // Testing page number
        assertThat(integerList.get(0), equalTo(60));

    }
}
