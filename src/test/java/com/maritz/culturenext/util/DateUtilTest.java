package com.maritz.culturenext.util;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class DateUtilTest {

    // convertToStartOfDayString tests

    @Test
    public void testConvertToStartOfDayStringNullValue() {
        assertNull(DateUtil.convertToStartOfDayString(null));
    }

    @Test
    public void testConvertFromDateStringValid() {
        String fromDate = "2015-07-16";
        Date expectedDate = new Date(1437004800000L); // 2015-07-16 00:00:00 GMT
        Date actualDate = DateUtil.convertToStartOfDayString(fromDate);

        assertEquals(expectedDate, actualDate);
    }

    @Test
    public void testConvertToStartOfDayStringInvalidFormat() {
        assertNull(DateUtil.convertToStartOfDayString("2015/07/15"));
    }

    @Test
    public void testConvertToStartOfDayStringDateTimeFormat() {
        String fromDate = "2015-07-16 17:00:00";
        Date expectedDate = new Date(1437004800000L); // 2015-07-16 00:00:00 GMT
        Date actualDate = DateUtil.convertToStartOfDayString(fromDate);

        assertEquals(expectedDate, actualDate);
    }

    // convertToEndOfDayString tests

    @Test
    public void testConvertToEndOfDayStringNullValue() {
        assertNull(DateUtil.convertToEndOfDayString(null));
    }

    @Test
    public void testConvertToEndOfDayStringValid() {
        String fromDate = "2015-07-16";
        Date expectedDate = new Date(1437091199999L); // 2015-07-16 23:59:59 GMT
        Date actualDate = DateUtil.convertToEndOfDayString(fromDate);

        assertEquals(expectedDate, actualDate);
    }

    @Test
    public void testConvertToEndOfDayStringInvalidFormat() {
        assertNull(DateUtil.convertToEndOfDayString("2015/07/15"));
    }

    @Test
    public void testConvertToEndOfDayStringDateTimeFormat() {
        String fromDate = "2015-07-16 17:00:00";
        Date expectedDate = new Date(1437091199999L); // 2015-07-16 23:59:59 GMT
        Date actualDate = DateUtil.convertToEndOfDayString(fromDate);

        assertEquals(expectedDate, actualDate);
    }
    
 // convertToDateComponentsFormat tests

    @Test
    public void testConvertToDateComponentsFormatMissingDate() {
        assertNull(DateUtil.convertToDateComponentsFormat(null, TimeZone.getDefault()));
    }

    @Test
    public void testConvertToDateComponentsFormatMissingTimeZone() {
        assertNull(DateUtil.convertToDateComponentsFormat(new Date(), null));
    }

    @Test
    public void testConvertToDateComponentsFormatInvalidTimeZone() {
        String expectedDateString = "2015-07-16 23:59:59";

        // should default to UTC if invalid timezone
        String convertedDate = 
                DateUtil.convertToDateComponentsFormat(new Date(1437091199999L), TimeZone.getTimeZone("INVALID"));
        assertEquals(expectedDateString, convertedDate);
    }

    @Test
    public void testConvertToDateComponentsFormat() {
        String expectedDateString = "2015-07-16 23:59:59";
        String convertedDate = 
                DateUtil.convertToDateComponentsFormat(new Date(1437091199999L), TimeZone.getTimeZone("UTC"));
        assertEquals(expectedDateString, convertedDate);
    }

    @Test
    public void test() {
        assertNotNull(DateUtil.getCurrentTimeInUTCDateTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.getTime();

        String currentThruDateString = DateUtil.convertToDateComponentsFormat(new Date(), TimeZone.getTimeZone("UTC"));
        DateUtil.convertToStartOfDayString(currentThruDateString); // default end date is current day


        String startDateString = DateUtil.convertToDateComponentsFormat(new Date(), TimeZone.getDefault());
        DateUtil.convertToEndOfDayString(startDateString, TimeZone.getDefault()); 
        // default end date is current day
    }
}