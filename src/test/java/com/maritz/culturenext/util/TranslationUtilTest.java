package com.maritz.culturenext.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Translatable;
import com.maritz.core.jpa.entity.TranslatablePhrase;
import com.maritz.core.jpa.repository.TranslatablePhraseRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.content.dao.TranslatableContentDao;
import com.maritz.culturenext.content.dto.TranslatableContentDTO;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.test.AbstractMockTest;

public class TranslationUtilTest extends AbstractMockTest {

    @InjectMocks private TranslationUtil translationUtil;
    @Mock private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Mock private ConcentrixDao<Translatable> translatableDao;
    @Mock private ConcentrixDao<TranslatablePhrase> translatablePhraseDao;
    @Mock private TranslatableContentDao translatableContentDao;
    @Mock private TranslatablePhraseRepository translatablePhraseRepository;

    @Mock private FindBy<LocaleInfo> localeInfoFindBy;
    @Mock private FindBy<Translatable> translatableFindBy;
    @Mock private FindBy<TranslatablePhrase> translatablePhraseFindBy;

    private static final Long TEST_ID_1 = 1L;
    private static final Long TEST_ID_2 = 2L;
    private static final Long TEST_ID_3 = 3L;
    private static final Long TEST_ID_4 = 4L;

    private static final String TRANSLATED_PHRASE_1 = "Test Phrase 1";
    private static final String TRANSLATED_PHRASE_2 = "Test Phrase 2";
    private static final String TRANSLATED_PHRASE_3 = "Test Phrase 3";
    private static final String TRANSLATED_PHRASE_4 = "Test Phrase 4";

    private static final String UNTRANSLATED_PHRASE_1 = "Untranslated Phrase 1";
    private static final String UNTRANSLATED_PHRASE_2 = "Untranslated Phrase 2";
    private static final String UNTRANSLATED_PHRASE_3 = "Untranslated Phrase 3";
    private static final String UNTRANSLATED_PHRASE_4 = "Untranslated Phrase 4";

    private static final String TEST_INVALID_LANGUAGE_CODE = "bad";
    private static final String TEST_INVALID_TABLE_NAME = "bad";
    private static final String TEST_VALID_TABLE_NAME = "APPLICATION_DATA";
    private static final String TEST_INVALID_COLUMN_NAME = "bad";
    private static final String TEST_VALID_COLUMN_NAME = "VALUE_1";
    private static final String TEST_COLUMN_NAME_2 = "VALUE_2";
    private static final String TEST_COLUMN_NAME_3  = "VALUE_3";
    private static final String TEST_COLUMN_NAME_4  = "VALUE_4";
    private static final Long TEST_VALID_ID = 1001L;
    private static final Long TEST_INVALID_ID = 1000000L;

    @Before
    public void setup() {
        // Mocks for localeInfoDao
        PowerMockito.when(localeInfoDao.findBy()).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.where(Matchers.anyString())).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.eq(Matchers.anyString())).thenReturn(localeInfoFindBy);
        PowerMockito.when(localeInfoFindBy.find(Matchers.anyString(), Matchers.<Class<String>>any()))
            .thenReturn(Arrays.asList(
                LocaleCodeEnum.en_US.name(), LocaleCodeEnum.de_DE.name(),
                LocaleCodeEnum.es_ES.name(), LocaleCodeEnum.fr_CA.name(),
                LocaleCodeEnum.fr_FR.name(), LocaleCodeEnum.it_IT.name(),
                LocaleCodeEnum.ja_JP.name(), LocaleCodeEnum.pt_BR.name(),
                LocaleCodeEnum.ru_RU.name(), LocaleCodeEnum.zh_CN.name(),
                LocaleCodeEnum.zh_TW.name()));

        // Mocks for platform translatable content
        List<Map<String, Object>> nodes = new ArrayList<>();
        Map<String, Object> node1 = new HashMap<>();
        node1.put(ProjectConstants.TABLE_NAME, "APPLICATION_DATA");
        node1.put(ProjectConstants.KEY, ApplicationDataConstants.KEY_NAME_RECGPX_HEADER);
        node1.put("enUs", "You have been recognized!");
        Map<String, Object> node2 = new HashMap<>();
        node2.put(ProjectConstants.TABLE_NAME, "CALENDAR_ENTRY");
        node2.put(ProjectConstants.KEY, "Customer Service Week");
        node2.put("enUs", "Customer Service Week");
        nodes.add(node1);
        nodes.add(node2);

        PowerMockito.when(translatableContentDao.getPlatformTranslatableContent()).thenReturn(nodes);

        // translatableDao
        PowerMockito.when(translatableDao.findBy()).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.where(Matchers.anyString())).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.eq(Matchers.anyString())).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.and(Matchers.anyString())).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.or(Matchers.anyString())).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.in(Matchers.anyListOf(String.class))).thenReturn(translatableFindBy);
        PowerMockito.when(translatableFindBy.findOne()).thenReturn(getTranslatableList().get(0));
        PowerMockito.when(translatableFindBy.find()).thenReturn(getTranslatableList());

        // translatablePhraseDao
        PowerMockito.when(translatablePhraseDao.findBy()).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.where(Matchers.anyString())).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.eq(Matchers.anyLong())).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.and(Matchers.anyString())).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.in(Matchers.anyListOf(Long.class))).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.eq(Matchers.anyString())).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.isNull()).thenReturn(translatablePhraseFindBy);
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);
        // Tests need to mock the final find() call
        // getTranslationsForListOfIds should return getIdTranslatablePhraseList
        // getTranslationsForListOfColumns should return getColumnTranslatablePhraseList
    }

    @Test
    public void testGetTranslationsForListOfIdsNullLanguageCode() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(null,
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsInvalidLanguageCode() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(TEST_INVALID_LANGUAGE_CODE,
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNullTableName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                null, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsInvalidTableName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());
        PowerMockito.when(translatableFindBy.findOne()).thenReturn(null);

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_INVALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNullColumnName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, null, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsInvalidColumnName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());
        PowerMockito.when(translatableFindBy.findOne()).thenReturn(null);

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_INVALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNullIds() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, null, getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsEmptyIds() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNullDefaultMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), null);

        assertTranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsEmptyDefaultMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), new HashMap<>());

        assertTranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNoTranslatablePhrases() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertUntranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfIdsNoTranslatablePhrasesNullDefaultMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), null);

        assertNotNull(response);
        assertThat(response.size(), is(0));
    }

    @Test
    public void testGetTranslationsForListOfIdsHappyPath() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getIdTranslatablePhraseList());

        Map<Long, String> response = translationUtil.getTranslationsForListOfIds(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, TEST_VALID_COLUMN_NAME, new ArrayList<>(getDefaultMapIds().keySet()), getDefaultMapIds());

        assertTranslatedIds(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNullLanguageCode() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(null,
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsInvalidLanguageCode() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(TEST_INVALID_LANGUAGE_CODE,
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNullTableName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                null, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsInvalidTableName() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());
        PowerMockito.when(translatableFindBy.find()).thenReturn(null);

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_INVALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNullColumnNameList() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, null, TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsEmptyColumnNameList() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNullId() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), null, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsInvalidId() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_INVALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNullColumnMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, null);

        assertTranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsEmptyColumnMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, new HashMap<String, String>());

        assertTranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNoTranslatablePhrases() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);
        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertUntranslatedColumns(response);
    }

    @Test
    public void testGetTranslationsForListOfColumnsNoTranslatablePhrasesNullDefaultMap() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, null);

        assertNotNull(response);
        assertThat(response.size(), is(0));
    }

    @Test
    public void testGetTranslationsForListOfColumnsHappyPath() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getColumnTranslatablePhraseList());

        Map<String, String> response = translationUtil.getTranslationsForListOfColumns(LocaleCodeEnum.fr_CA.name(),
                TEST_VALID_TABLE_NAME, new ArrayList<>(getDefaultMapColumns().keySet()), TEST_VALID_ID, getDefaultMapColumns());

        assertTranslatedColumns(response);
    }

    @Test
    public void testGetAllTranslatableContentHappyPath() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getAllTranslatablePhrases());

        List<TranslatableContentDTO> results = translationUtil.getAllTranslatableContent(null);

        assertNotNull(results);
        assertThat(results.size(), is(4));
        for (TranslatableContentDTO result : results) {
            assertThat(result.getTranslations().size(), is(2));
        }
    }

    @Test
    public void testGetAllTranslatableContentNullTableNameNoTranslatable() {
        PowerMockito.when(translatableFindBy.find()).thenReturn(null);

        List<TranslatableContentDTO> results = translationUtil.getAllTranslatableContent(null);
        assertNotNull(results);
        assertTrue(CollectionUtils.isEmpty(results));
    }

    @Test
    public void testGetAllTranslatableContentNullTableNameNoTranslatablePhrase() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(null);

        List<TranslatableContentDTO> results = translationUtil.getAllTranslatableContent(null);
        assertNotNull(results);
        assertTrue(CollectionUtils.isEmpty(results));
    }

    @Test
    public void testGetAllTranslatableContentTableNameHappyPath() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getAllTranslatablePhrases());
        List<TranslatableContentDTO> results = translationUtil.getAllTranslatableContent(TEST_VALID_TABLE_NAME);

        assertNotNull(results);
        assertThat(results.size(), is(4));
        for (TranslatableContentDTO result : results) {
            assertThat(result.getTranslations().size(), is(2));
        }
    }

    @Test
    public void testGetPlatformTranslatableContent() {
        List<Map<String, Object>> results = translationUtil.getPlatformTranslatableContent();
        assertNotNull(results);
        assertThat(results.size(), is(2));
    }

    @Test
    public void testGetAllTranslatableContentInvalidTableName() {
        PowerMockito.when(translatableFindBy.find()).thenReturn(null);
        List<TranslatableContentDTO> results = translationUtil.getAllTranslatableContent(TEST_INVALID_TABLE_NAME);

        assertNotNull(results);
        assertTrue(CollectionUtils.isEmpty(results));
    }

    @Test
    public void testUpdateTranslatableContentHappyNoBlanks() {
        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(getAllTranslatablePhrases());
        final String deDETranslation = "Berlin";
        final String frCATranslation = "Montréal";

        TranslatableContentDTO content = new TranslatableContentDTO();
        content.setTargetType(TEST_VALID_TABLE_NAME);
        content.setField(TEST_VALID_COLUMN_NAME);
        content.setTargetId(TEST_ID_1);
        content.getTranslations().put(LocaleCodeEnum.de_DE.name(), deDETranslation);
        content.getTranslations().put(LocaleCodeEnum.fr_CA.name(), frCATranslation);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ArrayList<TranslatablePhrase> translatablePhraseList = (ArrayList<TranslatablePhrase>)args[0];

                for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
                    assertNotNull(translatablePhrase);
                    assertThat(translatablePhrase.getTargetId(), is(TEST_ID_1));
                    if (translatablePhrase.getLocaleCode().equals(LocaleCodeEnum.de_DE.name())) {
                        assertNull(translatablePhrase.getId());
                        assertThat(translatablePhrase.getTranslation(), is(deDETranslation));
                    } else if (translatablePhrase.getLocaleCode().equals(LocaleCodeEnum.fr_CA.name())) {
                        assertNotNull(translatablePhrase.getId());
                        assertThat(translatablePhrase.getTranslation(), is(frCATranslation));
                    } else {
                        Assert.fail("Invalid locale code");
                    }
                }

                return null;
            }
        }).when(translatablePhraseRepository).save(Matchers.anyListOf(TranslatablePhrase.class));

        translationUtil.updateTranslatableContent(Arrays.asList(content));

        Mockito.verify(translatablePhraseRepository, Mockito.times(1)).save(Matchers.anyListOf(TranslatablePhrase.class));
    }

    @Test
    public void testUpdateTranslatableContentHappyBlanks() {

        List<TranslatablePhrase> translatablePhraseList = new ArrayList<>(getAllTranslatablePhrases());

        for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
            translatablePhrase.setTranslation(null);
            translatablePhrase.setLocaleCode(null);
        }

        PowerMockito.when(translatablePhraseFindBy.find()).thenReturn(translatablePhraseList);
        final String deDETranslation = "Berlin";
        final String frCATranslation = "Montréal";

        TranslatableContentDTO content = new TranslatableContentDTO();
        content.setTargetType(TEST_VALID_TABLE_NAME);
        content.setField(TEST_VALID_COLUMN_NAME);
        content.setTargetId(TEST_ID_1);
        content.getTranslations().put(LocaleCodeEnum.de_DE.name(), deDETranslation);
        content.getTranslations().put(LocaleCodeEnum.fr_CA.name(), frCATranslation);

        PowerMockito.doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                ArrayList<TranslatablePhrase> translatablePhraseList = (ArrayList<TranslatablePhrase>)args[0];

                for (TranslatablePhrase translatablePhrase : translatablePhraseList) {
                    assertNotNull(translatablePhrase);
                    assertThat(translatablePhrase.getTargetId(), is(TEST_ID_1));
                    if (translatablePhrase.getLocaleCode().equals(LocaleCodeEnum.de_DE.name())) {
                        assertNull(translatablePhrase.getId());
                        assertThat(translatablePhrase.getTranslation(), is(deDETranslation));
                    } else if (translatablePhrase.getLocaleCode().equals(LocaleCodeEnum.fr_CA.name())) {
                        assertNull(translatablePhrase.getId());
                        assertThat(translatablePhrase.getTranslation(), is(frCATranslation));
                    } else {
                        Assert.fail("Invalid locale code");
                    }
                }

                return null;
            }
        }).when(translatablePhraseRepository).save(Matchers.anyListOf(TranslatablePhrase.class));

        translationUtil.updateTranslatableContent(Arrays.asList(content));

        Mockito.verify(translatablePhraseRepository, Mockito.times(1)).save(Matchers.anyListOf(TranslatablePhrase.class));
    }

    public List<Translatable> getTranslatableList() {
        Translatable testTranslatable1 = new Translatable();
        testTranslatable1.setTableName(TEST_VALID_TABLE_NAME);
        testTranslatable1.setColumnName(TEST_VALID_COLUMN_NAME);
        testTranslatable1.setId(TEST_ID_1);
        testTranslatable1.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        Translatable testTranslatable2 = new Translatable();
        testTranslatable2.setTableName(TEST_VALID_TABLE_NAME);
        testTranslatable2.setColumnName(TEST_COLUMN_NAME_2);
        testTranslatable2.setId(TEST_ID_2);
        testTranslatable2.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        Translatable testTranslatable3 = new Translatable();
        testTranslatable3.setTableName(TEST_VALID_TABLE_NAME);
        testTranslatable3.setColumnName(TEST_COLUMN_NAME_3);
        testTranslatable3.setId(TEST_ID_3);
        testTranslatable3.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        Translatable testTranslatable4 = new Translatable();
        testTranslatable4.setTableName(TEST_VALID_TABLE_NAME);
        testTranslatable4.setColumnName(TEST_COLUMN_NAME_4);
        testTranslatable4.setId(TEST_ID_4);
        testTranslatable4.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        return Arrays.asList(testTranslatable1, testTranslatable2, testTranslatable3, testTranslatable4);
    }

    public List<TranslatablePhrase> getIdTranslatablePhraseList() {
        TranslatablePhrase testTranslatablePhrase1 = new TranslatablePhrase();
        testTranslatablePhrase1.setId(TEST_ID_1);
        testTranslatablePhrase1.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase1.setTargetId(TEST_ID_1);
        testTranslatablePhrase1.setTranslatableId(TEST_ID_1);
        testTranslatablePhrase1.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase1.setTranslation(TRANSLATED_PHRASE_1);

        TranslatablePhrase testTranslatablePhrase2 = new TranslatablePhrase();
        testTranslatablePhrase2.setId(TEST_ID_2);
        testTranslatablePhrase2.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase2.setTargetId(TEST_ID_2);
        testTranslatablePhrase2.setTranslatableId(TEST_ID_1);
        testTranslatablePhrase2.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase2.setTranslation(TRANSLATED_PHRASE_2);

        TranslatablePhrase testTranslatablePhrase3 = new TranslatablePhrase();
        testTranslatablePhrase3.setId(TEST_ID_3);
        testTranslatablePhrase3.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase3.setTargetId(TEST_ID_3);
        testTranslatablePhrase3.setTranslatableId(TEST_ID_1);
        testTranslatablePhrase3.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase3.setTranslation(TRANSLATED_PHRASE_3);

        TranslatablePhrase testTranslatablePhrase4 = new TranslatablePhrase();
        testTranslatablePhrase4.setId(TEST_ID_4);
        testTranslatablePhrase4.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase4.setTargetId(TEST_ID_4);
        testTranslatablePhrase4.setTranslatableId(TEST_ID_1);
        testTranslatablePhrase4.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase4.setTranslation(TRANSLATED_PHRASE_4);

        return Arrays.asList(testTranslatablePhrase1,
                testTranslatablePhrase2, testTranslatablePhrase3, testTranslatablePhrase4);
    }
    public List<TranslatablePhrase> getColumnTranslatablePhraseList() {
        TranslatablePhrase testTranslatablePhrase1 = new TranslatablePhrase();
        testTranslatablePhrase1.setId(TEST_ID_1);
        testTranslatablePhrase1.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase1.setTargetId(TEST_ID_1);
        testTranslatablePhrase1.setTranslatableId(TEST_ID_1);
        testTranslatablePhrase1.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase1.setTranslation(TRANSLATED_PHRASE_1);

        TranslatablePhrase testTranslatablePhrase2 = new TranslatablePhrase();
        testTranslatablePhrase2.setId(TEST_ID_2);
        testTranslatablePhrase2.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase2.setTargetId(TEST_ID_1);
        testTranslatablePhrase2.setTranslatableId(TEST_ID_2);
        testTranslatablePhrase2.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase2.setTranslation(TRANSLATED_PHRASE_2);

        TranslatablePhrase testTranslatablePhrase3 = new TranslatablePhrase();
        testTranslatablePhrase3.setId(TEST_ID_3);
        testTranslatablePhrase3.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase3.setTargetId(TEST_ID_1);
        testTranslatablePhrase3.setTranslatableId(TEST_ID_3);
        testTranslatablePhrase3.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase3.setTranslation(TRANSLATED_PHRASE_3);

        TranslatablePhrase testTranslatablePhrase4 = new TranslatablePhrase();
        testTranslatablePhrase4.setId(TEST_ID_4);
        testTranslatablePhrase4.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        testTranslatablePhrase4.setTargetId(TEST_ID_1);
        testTranslatablePhrase4.setTranslatableId(TEST_ID_4);
        testTranslatablePhrase4.setLocaleCode(LocaleCodeEnum.fr_CA.name());
        testTranslatablePhrase4.setTranslation(TRANSLATED_PHRASE_4);

        return Arrays.asList(testTranslatablePhrase1,
                testTranslatablePhrase2, testTranslatablePhrase3, testTranslatablePhrase4);
    }

    public List<TranslatablePhrase> getAllTranslatablePhrases() {
        // build up list of "all" translatable content
        // in this scenario, we should have 4 sets of translations
        // each with a translation for en_US and fr_CA
        List<TranslatablePhrase> list = new ArrayList<>(getColumnTranslatablePhraseList());

        for (TranslatablePhrase phrase : getColumnTranslatablePhraseList()) {
            phrase.setLocaleCode(LocaleCodeEnum.en_US.name());
            list.add(phrase);
        }
        return list;
    }

    public Map<Long, String> getDefaultMapIds() {

        Map<Long, String> map = new HashMap<>();

        map.put(TEST_ID_1, UNTRANSLATED_PHRASE_1);
        map.put(TEST_ID_2, UNTRANSLATED_PHRASE_2);
        map.put(TEST_ID_3, UNTRANSLATED_PHRASE_3);
        map.put(TEST_ID_4, UNTRANSLATED_PHRASE_4);

        return map;
    }

    public Map<String, String> getDefaultMapColumns() {

        Map<String, String> map = new HashMap<>();

        map.put(TEST_VALID_COLUMN_NAME, UNTRANSLATED_PHRASE_1);
        map.put(TEST_COLUMN_NAME_2, UNTRANSLATED_PHRASE_2);
        map.put(TEST_COLUMN_NAME_3, UNTRANSLATED_PHRASE_3);
        map.put(TEST_COLUMN_NAME_4, UNTRANSLATED_PHRASE_4);

        return map;
    }

    public void assertTranslatedColumns(Map<String, String> response) {
        assertThat(response.size(), is(4));
        assertThat(response.get(TEST_VALID_COLUMN_NAME), is(TRANSLATED_PHRASE_1));
        assertThat(response.get(TEST_COLUMN_NAME_2), is(TRANSLATED_PHRASE_2));
        assertThat(response.get(TEST_COLUMN_NAME_3), is(TRANSLATED_PHRASE_3));
        assertThat(response.get(TEST_COLUMN_NAME_4), is(TRANSLATED_PHRASE_4));
    }

    public void assertUntranslatedColumns(Map<String, String> response) {
        assertThat(response.size(), is(4));
        assertThat(response.get(TEST_VALID_COLUMN_NAME), is(UNTRANSLATED_PHRASE_1));
        assertThat(response.get(TEST_COLUMN_NAME_2), is(UNTRANSLATED_PHRASE_2));
        assertThat(response.get(TEST_COLUMN_NAME_3), is(UNTRANSLATED_PHRASE_3));
        assertThat(response.get(TEST_COLUMN_NAME_4), is(UNTRANSLATED_PHRASE_4));
    }

    public void assertTranslatedIds(Map<Long, String> response) {
        assertThat(response.size(), is(4));
        assertThat(response.get(TEST_ID_1), is(TRANSLATED_PHRASE_1));
        assertThat(response.get(TEST_ID_2), is(TRANSLATED_PHRASE_2));
        assertThat(response.get(TEST_ID_3), is(TRANSLATED_PHRASE_3));
        assertThat(response.get(TEST_ID_4), is(TRANSLATED_PHRASE_4));
    }

    public void assertUntranslatedIds(Map<Long, String> response) {
        assertThat(response.size(), is(4));
        assertThat(response.get(TEST_ID_1), is(UNTRANSLATED_PHRASE_1));
        assertThat(response.get(TEST_ID_2), is(UNTRANSLATED_PHRASE_2));
        assertThat(response.get(TEST_ID_3), is(UNTRANSLATED_PHRASE_3));
        assertThat(response.get(TEST_ID_4), is(UNTRANSLATED_PHRASE_4));
    }
}
