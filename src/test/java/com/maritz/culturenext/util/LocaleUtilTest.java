package com.maritz.culturenext.util;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class LocaleUtilTest {

    @Test
    public void testFormatLocaleCodeNull() {
        assertNull(LocaleUtil.formatLocaleCode(null));
    }

    @Test
    public void testFormatLocaleCodeEmptyString() {
        assertNull(LocaleUtil.formatLocaleCode(""));
    }

    @Test
    public void testFormatLocaleCodeNonLocale() {
        assertNull(LocaleUtil.formatLocaleCode("hello"));
    }

    @Test
    public void testFormatLocaleCodeAllCaps() {
        assertThat(LocaleUtil.formatLocaleCode("EN_US"), is("en_US"));
    }

    @Test
    public void testFormatLocaleCodeAllLowercase() {
        assertThat(LocaleUtil.formatLocaleCode("fr_ca"), is("fr_CA"));
    }

    @Test
    public void testFormatLocaleCodeMixedCapitalization() {
        assertThat(LocaleUtil.formatLocaleCode("zH_Cn"), is("zh_CN"));
    }

    @Test
    public void testFormatLocaleCodeDashToUnderscore() {
        assertThat(LocaleUtil.formatLocaleCode("en-GB"), is("en_GB"));
    }
}
