package com.maritz.culturenext.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

public class EnvironmentUtilTest {
    private EnvironmentUtil environmentUtil;

    @Mock
    private Environment mockEnvironment;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        environmentUtil = new EnvironmentUtil()
                                .setEnvironment(mockEnvironment);
    }

    @Test
    public void getProperty_required_null() {
        when(mockEnvironment.getProperty("foo")).thenReturn(null);

        IllegalStateException illegalStateException = null;
        try {
            environmentUtil.getProperty("foo", true);
        } catch (IllegalStateException e) {
            illegalStateException = e;
        }
        assertThat(illegalStateException, notNullValue());
        assertThat(illegalStateException.getMessage(), is("Property \"foo\" has no value."));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getIntProperty_validValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn("123");

        int property = environmentUtil.getIntProperty("foo", 456);
        assertThat(property, is(123));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getIntProperty_nullValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn(null);

        int property = environmentUtil.getIntProperty("foo", 456);
        assertThat(property, is(456));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getIntProperty_unparseableValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn("abc123");

        int property = environmentUtil.getIntProperty("foo", 456);
        assertThat(property, is(456));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getLongProperty_validValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn("2147483648");

        long property = environmentUtil.getLongProperty("foo", 456);
        assertThat(property, is(2147483648L));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getLongProperty_nullValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn(null);

        long property = environmentUtil.getLongProperty("foo", 456L);
        assertThat(property, is(456L));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getLongProperty_unparseableValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn("abc123");

        long property = environmentUtil.getLongProperty("foo", 2147483648L);
        assertThat(property, is(2147483648L));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getProperties() {
        when(mockEnvironment.getProperty("foo")).thenReturn("bar,baz");

        List<String> properties = environmentUtil.getProperties("foo");
        assertThat(properties.size(), is(2));
        assertThat(properties.get(0), is("bar"));
        assertThat(properties.get(1), is("baz"));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void getProperties_nullValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn(null);

        IllegalStateException illegalStateException = null;
        try {
            environmentUtil.getProperties("foo");
        } catch (IllegalStateException e) {
            illegalStateException = e;
        }
        assertThat(illegalStateException, notNullValue());
        assertThat(illegalStateException.getMessage(), is("Property \"foo\" has no value."));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }

    @Test
    public void contains() {
        when(mockEnvironment.getProperty("foo")).thenReturn("bar,bAz");

        assertThat(environmentUtil.contains("foo", "BaZ"), is(true));
        assertThat(environmentUtil.contains("foo", "qux"), is(false));

        verify(mockEnvironment, times(2)).getProperty("foo");
    }

    @Test
    public void contains_nullValue() {
        when(mockEnvironment.getProperty("foo")).thenReturn(null);

        IllegalStateException illegalStateException = null;
        try {
            environmentUtil.contains("foo", "bar");
        } catch (IllegalStateException e) {
            illegalStateException = e;
        }
        assertThat(illegalStateException, notNullValue());
        assertThat(illegalStateException.getMessage(), is("Property \"foo\" has no value."));

        verify(mockEnvironment, times(1)).getProperty("foo");
    }
}