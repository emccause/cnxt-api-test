package com.maritz.culturenext.util;

import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.jpa.repository.FilesRepository;
import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.enums.MediaTypesEnum;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

public class GcpUtilTest {
    private static final String FILE_NAME = "foo.jpg";
    private static final byte[] FILE_DATA = new byte[100];
    private static final MediaTypesEnum MEDIA_TYPE = MediaTypesEnum.IMAGE_JPEG;

    private static final StreamingOutput STREAMING_OUTPUT = new StreamingOutput() {
        @Override
        public void write(OutputStream outputStream) throws IOException {
            outputStream.write(FILE_DATA);
        }
    };

    private static final MultipartFile MULTIPART_FILE = new MockMultipartFile("oscar", FILE_DATA);


    private GcpUtil gcpUtil;
    private Files file;

    @Mock
    private EnvironmentUtil mockEnvironmentUtil;
    @Mock
    private Storage mockStorage;
    @Mock
    private FilesRepository mockFilesRepository;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        gcpUtil = new GcpUtil()
                        .setEnvironmentUtil(mockEnvironmentUtil);
        file = new Files();
    }

    @Test
    public void getSignedUrl_nullFile() throws InvalidKeyException, NoSuchAlgorithmException {
        assertThat(gcpUtil.signUrl(null), nullValue());
    }

    @Test
    public void getSignedUrl_noUrlInFile() throws InvalidKeyException, NoSuchAlgorithmException {
        IllegalStateException illegalStateException = null;
        try {
            gcpUtil.signUrl(file);
        } catch (IllegalStateException e) {
            illegalStateException = e;
        }
        assertThat(illegalStateException, notNullValue());
        assertThat(illegalStateException.getMessage(), startsWith("Encountered a file with no URL: com.maritz.core.jpa.entity.Files@"));
    }

    @Test
    public void getSignedUrl_happyPath() throws InvalidKeyException, NoSuchAlgorithmException {
        file.setUrl(FILE_NAME);

        when(mockEnvironmentUtil.getProperty("cdn.secure.url")).thenReturn("https://some.server.name");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");
        when(mockEnvironmentUtil.getProperty("url.signing.key-name")).thenReturn("signer");
        when(mockEnvironmentUtil.getProperty("url.signing.key")).thenReturn("KmCPD18sPjgwauRu6Ft6lw==");

        String signedUrl = gcpUtil.signUrl(file);
        assertThat(signedUrl, startsWith("https://some.server.name/client/foo.jpg?Expires="));
        assertThat(signedUrl, containsString("&KeyName=signer&Signature="));

        verify(mockEnvironmentUtil, times(1)).getIntProperty("url.signing.duration.web", 60);
        verify(mockEnvironmentUtil, times(1)).getProperty("url.signing.key");
    }

    @Test
    public void uploadFile_noStorageExists() {
        when(mockEnvironmentUtil.getProperty("cdn.secure.bucket")).thenReturn("bucket");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");

        IllegalStateException illegalStateException = null;
        try {
            gcpUtil.uploadFile(FILE_DATA, MEDIA_TYPE);
        } catch (IllegalStateException e) {
            illegalStateException = e;
        }
        assertThat(illegalStateException, notNullValue());
        assertThat(illegalStateException.getMessage(), startsWith("Google Storage does not exist in this environment"));

        verify(mockEnvironmentUtil, times(1)).getProperty("cdn.secure.bucket");
        verify(mockEnvironmentUtil, times(1)).getProperty("client.name");
    }

    @Test
    public void uploadFile_byteArray_happyPath() {
        gcpUtil.setStorage(mockStorage);

        when(mockEnvironmentUtil.getProperty("cdn.secure.bucket")).thenReturn("bucket");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");

        gcpUtil.uploadFile(FILE_DATA, MEDIA_TYPE);

        verify(mockEnvironmentUtil, times(1)).getProperty("cdn.secure.bucket");
        verify(mockEnvironmentUtil, times(1)).getProperty("client.name");

        ArgumentCaptor<BlobInfo> blobInfoArgumentCaptor = ArgumentCaptor.forClass(BlobInfo.class);
        ArgumentCaptor<byte[]> byteArrayArgumentCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(mockStorage, times(1)).create(blobInfoArgumentCaptor.capture(), byteArrayArgumentCaptor.capture());

        BlobInfo blobInfo = blobInfoArgumentCaptor.getValue();
        assertThat(blobInfo.getBucket(), is("bucket"));
        assertThat(blobInfo.getName(), startsWith("client/"));
        assertThat(blobInfo.getName(), endsWith(".jpeg"));
        assertThat(blobInfo.getContentType(), is(MEDIA_TYPE.value()));

        assertThat(byteArrayArgumentCaptor.getValue(), is(FILE_DATA));
    }

    @Test
    public void uploadFile_streamingOutput_happyPath() throws Exception {
        gcpUtil.setStorage(mockStorage);

        when(mockEnvironmentUtil.getProperty("cdn.secure.bucket")).thenReturn("bucket");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");

        gcpUtil.uploadFile(STREAMING_OUTPUT, MEDIA_TYPE);

        verify(mockEnvironmentUtil, times(1)).getProperty("cdn.secure.bucket");
        verify(mockEnvironmentUtil, times(1)).getProperty("client.name");

        ArgumentCaptor<BlobInfo> blobInfoArgumentCaptor = ArgumentCaptor.forClass(BlobInfo.class);
        ArgumentCaptor<byte[]> byteArrayArgumentCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(mockStorage, times(1)).create(blobInfoArgumentCaptor.capture(), byteArrayArgumentCaptor.capture());

        BlobInfo blobInfo = blobInfoArgumentCaptor.getValue();
        assertThat(blobInfo.getBucket(), is("bucket"));
        assertThat(blobInfo.getName(), startsWith("client/"));
        assertThat(blobInfo.getName(), endsWith(".jpeg"));
        assertThat(blobInfo.getContentType(), is(MEDIA_TYPE.value()));

        assertThat(byteArrayArgumentCaptor.getValue(), is(FILE_DATA));
    }

    @Test
    public void upload_fileId_byteArray_happyPath() {
        gcpUtil.setStorage(mockStorage);
        gcpUtil.setFilesRepository(mockFilesRepository);

        when(mockEnvironmentUtil.getProperty("cdn.secure.bucket")).thenReturn("bucket");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");
        when(mockFilesRepository.getOne(1L)).thenReturn(file.setMediaType(MEDIA_TYPE.value()));

        gcpUtil.uploadFile(1L, FILE_DATA);

        verify(mockEnvironmentUtil, times(1)).getProperty("cdn.secure.bucket");
        verify(mockEnvironmentUtil, times(1)).getProperty("client.name");

        verify(mockFilesRepository, times(1)).save(file);
        assertThat(file.getUrl(), notNullValue());

        ArgumentCaptor<BlobInfo> blobInfoArgumentCaptor = ArgumentCaptor.forClass(BlobInfo.class);
        ArgumentCaptor<byte[]> byteArrayArgumentCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(mockStorage, times(1)).create(blobInfoArgumentCaptor.capture(), byteArrayArgumentCaptor.capture());

        BlobInfo blobInfo = blobInfoArgumentCaptor.getValue();
        assertThat(blobInfo.getBucket(), is("bucket"));
        assertThat(blobInfo.getName(), startsWith("client/"));
        assertThat(blobInfo.getName(), endsWith("." + MEDIA_TYPE.getFileExtension()));
        assertThat(blobInfo.getContentType(), is(MEDIA_TYPE.value()));

        assertThat(byteArrayArgumentCaptor.getValue(), is(FILE_DATA));
    }

    @Test
    public void upload_fileId_multiPartFile_happyPath() {
        gcpUtil.setStorage(mockStorage);
        gcpUtil.setFilesRepository(mockFilesRepository);

        when(mockEnvironmentUtil.getProperty("cdn.secure.bucket")).thenReturn("bucket");
        when(mockEnvironmentUtil.getProperty("client.name")).thenReturn("client");
        when(mockFilesRepository.getOne(1L)).thenReturn(file.setMediaType(MEDIA_TYPE.value()));

        gcpUtil.uploadFile(1L, MULTIPART_FILE);

        verify(mockEnvironmentUtil, times(1)).getProperty("cdn.secure.bucket");
        verify(mockEnvironmentUtil, times(1)).getProperty("client.name");

        verify(mockFilesRepository, times(1)).save(file);
        assertThat(file.getUrl(), notNullValue());

        ArgumentCaptor<BlobInfo> blobInfoArgumentCaptor = ArgumentCaptor.forClass(BlobInfo.class);
        ArgumentCaptor<byte[]> byteArrayArgumentCaptor = ArgumentCaptor.forClass(byte[].class);
        verify(mockStorage, times(1)).create(blobInfoArgumentCaptor.capture(), byteArrayArgumentCaptor.capture());

        BlobInfo blobInfo = blobInfoArgumentCaptor.getValue();
        assertThat(blobInfo.getBucket(), is("bucket"));
        assertThat(blobInfo.getName(), startsWith("client/"));
        assertThat(blobInfo.getName(), endsWith("." + MEDIA_TYPE.getFileExtension()));
        assertThat(blobInfo.getContentType(), is(MEDIA_TYPE.value()));

        assertThat(byteArrayArgumentCaptor.getValue(), is(FILE_DATA));
    }
}