package com.maritz.culturenext.util;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.image.BufferedImage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
public class ImageManipulationUtilTest {

    @InjectMocks private ImageManipulationUtil imageManipulationUtil;

    private final int maxWidth = 150;
    private final int maxHeight = 150;


    @Test
    public void test_squareCrop() {

        BufferedImage image = new BufferedImage(500, 400, BufferedImage.TYPE_INT_RGB);
        BufferedImage newImage = null;

        try {
            newImage = imageManipulationUtil.squareCrop(image, maxWidth, maxHeight);
        } catch (Exception e) {
            fail();
        }

        assertTrue(newImage != null);
        assertTrue(newImage.getWidth() == maxWidth);
        assertTrue(newImage.getHeight() == maxHeight);

    }

    @Test
    public void test_crop () {
        BufferedImage image = new BufferedImage(2500, 2500, BufferedImage.TYPE_INT_RGB);
        BufferedImage newImage = null;

        try {
            newImage = imageManipulationUtil.crop(image, 0, 20, 2448, 2448);
        } catch (Exception e) {
            fail();
        }

        assertTrue(newImage != null);
        assertTrue(newImage.getHeight() == 2448);
        assertTrue(newImage.getWidth() == 2448);
    }


    @Test
    public void test_resize() {
        BufferedImage image = new BufferedImage(500, 400, BufferedImage.TYPE_INT_RGB);
        BufferedImage newImage = null;

        try {
            newImage = imageManipulationUtil.resize(image, maxWidth, maxHeight);
        } catch (Exception e) {
            fail();
        }

        assertTrue(newImage != null);
        assertTrue(newImage.getWidth() == maxWidth);
        assertTrue(newImage.getHeight() == maxHeight);
    }

    @Test
    public void test_circleCrop() {
        BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
        BufferedImage newImage = null;

        try {
            newImage = imageManipulationUtil.circleCrop(image);
        } catch (Exception e) {
            fail();
        }

        assertTrue(newImage != null);

    }

}
