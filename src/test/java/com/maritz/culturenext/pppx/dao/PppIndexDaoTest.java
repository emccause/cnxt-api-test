package com.maritz.culturenext.pppx.dao;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.pppx.constants.PppIndexConstants;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;
import com.maritz.test.AbstractDatabaseTest;


public class PppIndexDaoTest extends AbstractDatabaseTest {

    @Inject private PppIndexDao pppIndexDao;
    
    private final Long COUNTRY_ID_236 = 236L;
    private final Long COUNTRY_ID_108 = 108L;
    private final Long COUNTRY_ID_500 = 500L;
    private final Double VALID_PPP_VALUE = 9.9D;
    private final String NON_VALID_COUNTRY_CODE = "XX";
    
    @Test
    public void test_getPppIndex(){
        assertNotNull(pppIndexDao);
        assertNotNull(pppIndexDao.getPppIndexData());
    }

    @Test
    public void test_getPppIndexById_happy_path(){
        PppIndexDTO result = pppIndexDao.getPppIndexById(TestConstants.ID_1);
        assertNotNull("PPPX Default Record should exist", result.getCountryId());
    }

    @Test
    public void test_getPppIndexById_not_valid_id(){
        PppIndexDTO pppIndex = pppIndexDao.getPppIndexById(TestConstants.ID_MAX_VALUE);
        assertNull("ID " + TestConstants.ID_MAX_VALUE + " should not exist", pppIndex.getCountryId());
    }
    
    @Test
    public void test_getPppIndexById_null_id(){
        PppIndexDTO pppIndex = pppIndexDao.getPppIndexById(null);
        assertNull("ID should not be null", pppIndex.getCountryId());
    }
    
    @Test
    public void test_getCountryIdByCountryCode_happy_path(){
        assertNotNull("Default PPPX Country should exist", pppIndexDao.getCountryIdByCountryCode(PppIndexConstants.DEFAULT_PPP_INDEX_COUNTRY_CODE));
    }
    
    @Test
    public void test_getCountryIdByCountryCode_no_valid_country_code(){
        assertNull("Country Code " + NON_VALID_COUNTRY_CODE + " is not valid", pppIndexDao.getCountryIdByCountryCode(NON_VALID_COUNTRY_CODE));
    }
    
    @Test
    public void test_getCountryIdByCountryCode_null_country_code(){
        assertNull("Country Code should not be null", pppIndexDao.getCountryIdByCountryCode(null));
    }
    
    @Test
    public void test_isPppxCountryId_happy_path(){
        assertTrue(pppIndexDao.isPppxCountryId(COUNTRY_ID_236));
    }
    
    @Test
    public void test_isPppxCountryId_false(){
        assertFalse(pppIndexDao.isPppxCountryId(COUNTRY_ID_500));
    }
    
    @Test
    public void test_isPppxCountryId_null_country_id(){
        assertFalse(pppIndexDao.isPppxCountryId(null));
    }

    @Test
    public void test_isValidCountryId_happy_path(){
        assertTrue(pppIndexDao.isValidCountryId(COUNTRY_ID_236));
    }
    
    @Test
    public void test_isValidCountryId_false(){
        assertFalse(pppIndexDao.isValidCountryId(COUNTRY_ID_500));
    }
    
    @Test
    public void test_isValidCountryId_null_country_id(){
        assertFalse(pppIndexDao.isValidCountryId(null));
    }
    
    @Test
    public void test_createPppIndexRecord(){
        pppIndexDao.createPppIndexRecord(COUNTRY_ID_108, VALID_PPP_VALUE);
    }

    @Test
    public void test_updatePppIndexRecord(){
        pppIndexDao.updatePppIndexRecord(TestConstants.ID_MAX_VALUE, COUNTRY_ID_108, VALID_PPP_VALUE);
    }

    @Test
    public void test_deletePppIndexById(){
        pppIndexDao.deletePppIndexById(TestConstants.ID_MAX_VALUE);
    }

}
