package com.maritz.culturenext.pppx.dao;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.test.AbstractDatabaseTest;

public class ParticipantsPppIndexDaoTest extends AbstractDatabaseTest {
    
    @Inject private ParticipantsPppIndexDao participantsPppIndexDao;
    
    @Test
    public void test_getParticipantsPppxByPaxIds_happy_path(){
        List<Long> paxIds = Arrays.asList(1L,2L);
        assertNotNull(participantsPppIndexDao);
        List<ParticipantsPppIndexDTO> participantsPppIndexList = participantsPppIndexDao.getParticipantsPppxByPaxIds(paxIds);
        assertNotNull("Pax Ids should exist", participantsPppIndexList);
    }

    @Test
    public void test_getParticipantsPppxByPaxIds_no_pax_ids(){
        assertNotNull(participantsPppIndexDao);
        List<ParticipantsPppIndexDTO> participantsPppIndexList = participantsPppIndexDao.getParticipantsPppxByPaxIds(null);
        assertTrue("Pax Ids is empty", participantsPppIndexList.isEmpty());
    }

}
