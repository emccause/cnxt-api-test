package com.maritz.culturenext.pppx.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class ParticipantsPppIndexRestServiceTest extends AbstractRestTest {
    
    private static final String URL_PARTICIPANTS_PPP_INDEX = "/rest/participants/ppp-index";
    private static final String URL_PARTICIPANTS_PPP_INDEX_GOOD = URL_PARTICIPANTS_PPP_INDEX + "?paxIds=1,2&groupIds=5,6";
    private static final String URL_PARTICIPANTS_PPP_INDEX_ONLY_PAXIDS = URL_PARTICIPANTS_PPP_INDEX + "?paxIds=1,2";
    private static final String URL_PARTICIPANTS_PPP_INDEX_ONLY_GROUPIDS = URL_PARTICIPANTS_PPP_INDEX + "?groupIds=5,6";
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_happy_path() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX_GOOD)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
    }
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_only_paxIds() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX_ONLY_PAXIDS)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
    }
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_only_groupIds() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX_ONLY_GROUPIDS)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
    }
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_no_ids() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_invalid_paxIds() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX + "?paxIds=A,B")
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_getParticipantsPppIndexDataByPaxIds_invalid_groupIds() throws Exception {
        mockMvc.perform(
                get(URL_PARTICIPANTS_PPP_INDEX + "?groupIds=A,B")
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isBadRequest());
    }

}
