package com.maritz.culturenext.pppx.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;
import com.maritz.test.AbstractRestTest;

public class PppIndexRestServiceTest extends AbstractRestTest {
    
    private static final String URL_PPP_INDEX = "/rest/ppp-index/";
    private static final String URL_PPP_INDEX_ID = "/rest/ppp-index/%d";
    
    private final Long COUNTRY_ID_236 = 236L;
    private static final Long COUNTRY_ID_GERMANY = 86L;
    private static final Double PPP_NEW_VALUE = 3.2D;
    private static final Double PPP_UPDATE_VALUE = 2.6D;
    
    private ObjectWriter ow;
    
    @Before
    public void setup() {
        ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
    }

    @Test
    public void test_getPppIndexData_happy_path() throws Exception {
        mockMvc.perform(
                get(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk());
    }
    
    @Test
    public void test_getPppIndexData_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isForbidden());
    }
    
    @Test
    public void test_create_PppIndexData_happy_path() throws Exception {
        
        PppIndexDTO postPppIndex = new PppIndexDTO();
        postPppIndex.setCountryId(COUNTRY_ID_GERMANY);
        postPppIndex.setPppValue(PPP_NEW_VALUE);
        
        String json = ObjectUtils.objectToJson(postPppIndex);

        mockMvc.perform(
                post(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
                .contentType(MediaType.APPLICATION_JSON).content(json)
            ).andExpect(status().isOk());
    }

    @Test
    public void test_createPppIndexData_fail_no_authorized() throws Exception {
        String json = ow.writeValueAsString(createPppIndex());
        
        mockMvc.perform(
                post(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_PORTERGA))
                .content(json)
            ).andExpect(status().isForbidden());
    }
    
    @Test
    public void test_createPppIndexData_null_requestbody() throws Exception {
        mockMvc.perform(
                post(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void test_update_PppIndexData_happy_path() throws Exception {
        PppIndexDTO putPppIndex = new PppIndexDTO();
        putPppIndex.setId(TestConstants.ID_2);
        putPppIndex.setCountryId(COUNTRY_ID_GERMANY);
        putPppIndex.setPppValue(PPP_UPDATE_VALUE);
        
        List<PppIndexDTO> pppIndexList = new ArrayList<PppIndexDTO>();
        pppIndexList.add(createPppIndex());
        pppIndexList.add(putPppIndex);
        
        String json = ObjectUtils.objectToJson(pppIndexList);
        
        mockMvc.perform(
                put(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
                .contentType(MediaType.APPLICATION_JSON).content(json)
            ).andExpect(status().isOk());    
    }    
        
    @Test
    public void test_updatePppIndexes_fail_no_authorized() throws Exception {
        List<PppIndexDTO> pppIndexList = new ArrayList<PppIndexDTO>();
        pppIndexList.add(createPppIndex());
        String json = ow.writeValueAsString(pppIndexList);
        
        mockMvc.perform(
                put(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_PORTERGA))
                .content(json)
            ).andExpect(status().isForbidden());
    }
    
    @Test
    public void test_updatePppIndexes_null_requestbody() throws Exception {
        mockMvc.perform(
                put(URL_PPP_INDEX)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isBadRequest());
    }
    
    private PppIndexDTO createPppIndex() {
        PppIndexDTO pppIndex = new PppIndexDTO();
        pppIndex.setId(TestConstants.ID_1);
        pppIndex.setCountryId(COUNTRY_ID_236);
        pppIndex.setPppValue(1D);
        return pppIndex;
    }
    
    @Test
    public void test_delete_PppIndexData_happy_path() throws Exception {
        mockMvc.perform(
                delete(URL_PPP_INDEX + TestConstants.ID_2)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_HARWELLM))
            ).andExpect(status().isOk());
    }
    
    @Test
    public void test_deletePppIndexById_fail_no_authorized() throws Exception {
        mockMvc.perform(
                delete(String.format(URL_PPP_INDEX_ID, TestConstants.ID_1))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isForbidden());
    }
}
