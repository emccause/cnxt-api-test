package com.maritz.culturenext.pppx.services;

import static org.junit.Assert.*;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.pppx.dto.ParticipantsPppIndexDTO;
import com.maritz.culturenext.pppx.services.impl.ParticipantsPppIndexServiceImpl;
import com.maritz.test.AbstractDatabaseTest;

public class ParticipantsPppIndexServiceTest extends AbstractDatabaseTest {
    
    @Inject private ParticipantsPppIndexServiceImpl participantsPppIndexService;
    
    @Test
    public void test_getParticipantsPppxByPaxIds_happy_path(){
        assertNotNull("Service should not be null", participantsPppIndexService);
        List<ParticipantsPppIndexDTO> participantsPppxList = participantsPppIndexService.getParticipantsPppxByPaxIds("1,2", "5,6");
        assertNotNull("Participants list should not be null", participantsPppxList);
        assertEquals("Participants List should have 34 members", participantsPppxList.size(), 34);
    }

    @Test
    public void test_getParticipantsPppxByPaxIds_only_paxIds(){
        assertNotNull("Service should not be null", participantsPppIndexService);
        List<ParticipantsPppIndexDTO> participantsPppxList = participantsPppIndexService.getParticipantsPppxByPaxIds("1,2", null);
        assertNotNull("Participants list should not be null", participantsPppxList);
        assertEquals("Participants List should have 2 members", participantsPppxList.size(), 2);
    }

    @Test
    public void test_getParticipantsPppxByPaxIds_only_groupIds(){
        assertNotNull("Service should not be null", participantsPppIndexService);
        List<ParticipantsPppIndexDTO> participantsPppxList = participantsPppIndexService.getParticipantsPppxByPaxIds(null, "5,6");
        assertNotNull("Participants list should not be null", participantsPppxList);
        assertEquals("Participants List should have 32 members", participantsPppxList.size(), 32);
    }

    @Test(expected=ErrorMessageException.class)
    public void test_getParticipantsPppxByPaxIds_no_ids(){
        participantsPppIndexService.getParticipantsPppxByPaxIds(null, null);
    }

    @Test(expected=ErrorMessageException.class)
    public void test_getParticipantsPppxByPaxIds_invalid_pax_ids(){
        participantsPppIndexService.getParticipantsPppxByPaxIds("A,B", null);
    }

    @Test(expected=ErrorMessageException.class)
    public void test_getParticipantsPppxByPaxIds_invalid_group_ids(){
        participantsPppIndexService.getParticipantsPppxByPaxIds(null, "A,B");
    }

}
