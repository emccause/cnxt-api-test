package com.maritz.culturenext.pppx.services;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.pppx.dto.PppIndexDTO;
import com.maritz.culturenext.pppx.services.impl.PppIndexServiceImpl;
import com.maritz.test.AbstractDatabaseTest;

public class PppIndexServiceTest extends AbstractDatabaseTest {

    @Inject private PppIndexServiceImpl pppIndexServiceImpl;
    
    private final Long COUNTRY_ID_236 = 236L;
    private final Long COUNTRY_ID_108 = 108L;
    private final Long COUNTRY_ID_500 = 500L;
    private final Double VALID_PPP_VALUE = 8.8D;
    private final Double INVALID_PPP_VALUE = -5.5D;
    
    private List<PppIndexDTO> pppxList = new ArrayList<PppIndexDTO>();
    
    @Test
    public void test_getPppIndex() throws Exception {
        assertNotNull(pppIndexServiceImpl);
        assertNotNull(pppIndexServiceImpl.getPppIndexData());
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_createPppIndexData_invalid_country(){
        pppIndexServiceImpl.createPppIndexData(COUNTRY_ID_500, VALID_PPP_VALUE);
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_createPppIndexData_default_country() {
        pppIndexServiceImpl.createPppIndexData(COUNTRY_ID_236, VALID_PPP_VALUE);
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_createPppIndexData_null_country() {
        pppIndexServiceImpl.createPppIndexData(null, VALID_PPP_VALUE);
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_createPppIndexData_invalid_ppp_value() {
        pppIndexServiceImpl.createPppIndexData(COUNTRY_ID_108, INVALID_PPP_VALUE);
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_createPppIndexData_null_ppp_value() {
        pppIndexServiceImpl.createPppIndexData(COUNTRY_ID_108, null);
    }
    
    private List<PppIndexDTO> valid_pppIndexList() {
        PppIndexDTO pppIndex = new PppIndexDTO();
        pppIndex.setId(TestConstants.ID_1);
        pppIndex.setCountryId(COUNTRY_ID_236);
        pppIndex.setPppValue(1D);
        pppxList.add(pppIndex);
        
        pppIndex = new PppIndexDTO();
        pppIndex.setId(TestConstants.ID_2);
        pppIndex.setCountryId(COUNTRY_ID_108);
        pppIndex.setPppValue(VALID_PPP_VALUE);
        pppxList.add(pppIndex);
        
        return pppxList;
    }
    
    @Test
    public void test_updatePppIndexData_happy_path() {
        pppIndexServiceImpl.updatePppIndexData(valid_pppIndexList());
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_deletePppIndexById_no_valid_id() {
        pppIndexServiceImpl.deletePppIndexById(TestConstants.ID_MAX_VALUE);
    }
    
    @Test(expected=ErrorMessageException.class)
    public void test_deletePppIndexById_default_id() {
        pppIndexServiceImpl.deletePppIndexById(TestConstants.ID_1);
    }
}
