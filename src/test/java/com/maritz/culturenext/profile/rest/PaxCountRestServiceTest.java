package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class PaxCountRestServiceTest extends AbstractRestTest {

    @Test
    public void test_get_counts() throws Exception {
        mockMvc.perform(
                post("/rest/participants/count")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_get_counts_more_groups() throws Exception {
        mockMvc.perform(
                post("/rest/participants/count")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245},{\"groupId\":539},{\"groupId\":465},"
                            + "{\"groupId\":124},{\"groupId\":975}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_get_counts_no_pax() throws Exception {
        mockMvc.perform(
                post("/rest/participants/count")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"groupId\":1245},{\"groupId\":539},{\"groupId\":465},{\"groupId\":124},"
                            + "{\"groupId\":975}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_get_counts_no_groups() throws Exception {
        mockMvc.perform(
                post("/rest/participants/count")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":1245},{\"paxId\":539},{\"paxId\":465},{\"paxId\":124},{\"paxId\":975}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }
    
}
