package com.maritz.culturenext.profile.rest;

import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class PaxSearchRestServiceTest extends AbstractRestTest {

    @Test
    public void getMatchingSearch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=harwell")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParams() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=harwell&page=1&size=20")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParamsTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&page=100&size=20")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNonMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=BREAKING_EVERYTHING")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNoSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/participants?page=1&size=10")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().is4xxClientError());
    }

    @Test
    public void getMatchingSearchWithSingleStatusParamGood() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=porter&status=ACTIVE")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void getMatchingSearchWithListStatusParamGood() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=porter&status=ACTIVE,LOCKED")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void getNonMatchingSearchWithStatusParamBad() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&status=ALEX")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }
    
    @Test
    public void getMatchingSearchWithSysUserId() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=mudd")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(jsonPath("$[0]['sysUserId']", notNullValue()));
    }
    
    //programId test cases
    
    @Test
    public void getMatchingSearchWithParamsAndProg() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=baker&page=1&size=30&programid=3200")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getNonMatchingSearchWithParamsAndProgTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=garcia&page=100&size=20&programid=1")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getMatchingSearchWithSingleStatusParamAndProgGood() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=baker&status=ACTIVE&programid=3200")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void getNonMatchingSearchWithStatusParamBadAndProg() throws Exception {
        mockMvc.perform(
                get("/rest/participants?searchStr=harwell&status=ALEX&programid=1")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }
    
    @Test
    public void getMatchingSearchWithExcludeSelfFalse() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=harwell&excludeSelf=false")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithExcludeSelfTrue() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=harwell&excludeSelf=true")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getControlNumNoMatch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=T1023")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getControlNumMatch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/participants?searchStr=T10234")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
}
