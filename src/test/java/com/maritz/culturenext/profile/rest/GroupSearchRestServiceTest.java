package com.maritz.culturenext.profile.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class GroupSearchRestServiceTest extends AbstractRestTest {

    @Test
    public void getMatchingSearch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/groups?searchStr=healthcare")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParams() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/groups?searchStr=healthcare&page=1&size=20")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParamsTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/groups?searchStr=healthcare&page=100&size=20")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNonMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/groups?searchStr=BREAKING_EVERYTHING")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNoSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/groups?page=1&size=10")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
            .andExpect(status().isBadRequest());
    }

    @Test
    public void getMatchingSearchWithConfigIdParam() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/groups?searchStr=mo&groupConfigId=12")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }

    @Test
    public void getNonMatchingSearchWithConfigIdParam() throws Exception {
        mockMvc.perform(
                get("/rest/groups?searchStr=healthcare&groupConfigId=12")
                .with(authToken(TestConstants.USER_KUMARSJ))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }
    
    @Test
    public void getGroupInfoGood() throws Exception{
        mockMvc.perform(
                get("/rest/groups/551")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(jsonPath("$.groupId", is(551)));
    }
    

    
    @Test
    public void getGroupInfoBad() throws Exception{
        mockMvc.perform(
            get("/rest/groups/0")
            .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(status().isBadRequest());
    }
    
    @Test
    public void getGroupMembersGood() throws Exception{
            MvcResult result = mockMvc.perform(
                    get("/rest/groups/539/members")
                    .with(authToken(TestConstants.USER_KUMARSJ))
                ).andExpect(status().isOk()).andReturn();
            
            assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    

    @Test
    public void getGroupMembersBad() throws Exception{
        mockMvc.perform(
            get("/rest/groups/0/members")
            .with(authToken(TestConstants.USER_KUMARSJ))
        ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }


    @Test
    public void get_all_active_groups() throws Exception{
        mockMvc.perform(
                get("/rest/groups/active")
                        .with(authToken(TestConstants.USER_PULSEM))
        )
                .andExpect(status().isOk());
    }

    @Test
    public void fail_with_403_on_bad_credentials_all_active_groups() throws Exception{
        mockMvc.perform(
                get("/rest/groups/active")
                        .with(authToken(TestConstants.USER_KUMARSJ))
        )
        .andExpect(status().isForbidden());
    }
}
