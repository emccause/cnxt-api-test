package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;


public class LightProfileRestTest extends AbstractRestTest {

    @Test
    public void get_light_profile_info() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/basic")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

}
