package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class GroupRestServiceTest extends AbstractRestTest {

    @Test
    public void test_good_personal_group() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My greater group\",\"type\":\"PERSONAL\",\"status\":\"ACTIVE\","
                            + "\"members\":[{\"paxId\": 2043},{\"paxId\": 3341},"
                            + " {\"paxId\": 4545}]}")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_good_custom_group() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My greatest group\",\"type\":\"CUSTOM\",\"visibility\":\"ADMIN\","
                            + "\"status\":\"INACTIVE\",\"members\":[{\"groupId\":21},{\"paxId\": 2046},"
                            + " {\"groupId\":23},{\"paxId\": 3342}, {\"paxId\": 4545},{\"groupId\":231}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_non_existant_pax_id() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My great group\",\"type\":\"CUSTOM\",\"visibility\":\"PUBLIC\","
                            + "\"status\":\"ACTIVE\",\"members\":[{\"groupId\":21},{\"paxId\": 2046},"
                            + " {\"groupId\":23},{\"paxId\": 3342}, {\"paxId\": 4545},{\"groupId\":231}, "
                            + "{\"paxId\": 454551}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_non_existant_group_id() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My great group\",\"type\":\"CUSTOM\",\"visibility\":\"PUBLIC\","
                            + "\"status\":\"ACTIVE\",\"members\":[{\"groupId\":21},{\"paxId\": 2046}, "
                            + "{\"groupId\":23},{\"paxId\": 3342}, {\"paxId\": 4545},{\"groupId\":231}, "
                            + "{\"groupId\": 454551}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_no_status() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My great group\",\"type\":\"CUSTOM\",\"visibility\":\"PUBLIC\","
                            + "\"members\":[{\"groupId\":21},{\"paxId\": 2046}, {\"groupId\":23},{\"paxId\": 3342},"
                            + " {\"paxId\": 4545},{\"groupId\":231}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_no_visibility() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My great group\",\"type\":\"CUSTOM\",\"status\":\"ACTIVE\","
                            + "\"members\":[{\"groupId\":21},{\"paxId\": 2046}, {\"groupId\":23},{\"paxId\": 3342}, "
                            + "{\"paxId\": 4545},{\"groupId\":231}")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_name_too_long() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My great group My great group My great group My great group "
                            + "My great group My great group My great group My great group My great group \","
                            + "\"type\":\"CUSTOM\",\"visibility\":\"PUBLIC\",\"status\":\"ACTIVE\","
                            + "\"members\":[{\"groupId\":21},{\"paxId\": 2046}, {\"groupId\":23},{\"paxId\": 3342},"
                            + " {\"paxId\": 4545},{\"groupId\":231}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_duplicate_name() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My Better group\",\"type\":\"CUSTOM\",\"visibility\":\"PUBLIC\","
                            + "\"status\":\"ACTIVE\",\"members\":[{\"groupId\":21},{\"paxId\": 2046}, {\"groupId\":23},"
                            + "{\"paxId\": 3342}, {\"paxId\": 4545},{\"groupId\":231}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_duplicate_name_personal_group() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"Personal Group Test 1\",\"type\":\"PERSONAL\",\"status\":\"ACTIVE\","
                            + "\"members\":[{\"groupId\":21},{\"paxId\": 2043}, {\"groupId\":23},{\"paxId\": 3341},"
                            + " {\"paxId\": 4545},{\"groupId\":231}] }")
                    .with(authToken(TestConstants.USER_HARWELLM))
                )
                .andExpect(status().isBadRequest())
            ;
    }

    @Test
    public void test_duplicate_name_personal_group_diff_owner() throws Exception {
        mockMvc.perform(
                post("/rest/groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"Personal Group Test 1\",\"type\":\"PERSONAL\",\"status\":\"ACTIVE\","
                            + "\"members\":[{\"paxId\": 2043},{\"paxId\": 3341},"
                            + " {\"paxId\": 4545}] }")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_update_group() throws Exception {
        mockMvc.perform(
                put("/rest/groups/1261")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"groupName\":\"My Okay group\",\"visibility\":\"PUBLIC\",\"status\":\"INACTIVE\"}")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
    }

    @Test
    public void test_add_members_good() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
        
    }

    @Test
    public void test_delete_members_good() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
        
    }

    @Test
    public void test_add_members_bad_id() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245},{\"groupId\":454455}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
        
    }

    @Test
    public void test_add_members_duplicates() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245},{\"groupId\":1245}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
        
    }

    @Test
    public void test_delete_members_bad_id() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245},{\"groupId\":454455}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isBadRequest())
            ;
        
    }

    @Test
    public void test_delete_members_duplicates() throws Exception {
        mockMvc.perform(
                post("/rest/groups/1261/members")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("[{\"paxId\":969},{\"groupId\":1245},{\"groupId\":1245}]")
                    .with(authToken(TestConstants.USER_MUDDAM))
                )
                .andExpect(status().isOk())
            ;
        
    }

    @Test
    public void get_group_members() throws Exception {
        mockMvc.perform(
                get("/rest/groups/4/members")
                .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk());
    }
    
    @Test
    public void getGroupMembersExpanded() throws Exception {
        mockMvc.perform(
                get("/rest/groups/1272/members?expandGroups")
                .with(authToken(TestConstants.USER_MUDDAM))
        )
                .andExpect(status().isOk());
        
    }
}
