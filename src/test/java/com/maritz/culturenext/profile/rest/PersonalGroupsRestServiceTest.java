package com.maritz.culturenext.profile.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.constants.GroupConstants;
import com.maritz.test.AbstractRestTest;

public class PersonalGroupsRestServiceTest extends AbstractRestTest {
    
    private static final String PERSONAL_GROUPS_ENDPOINT_8987 = "/rest/participants/8987/personal-groups";
    private static final String BAD_PERSONAL_GROUPS_ENDPOINT = "/rest/participants/abcd/personal-groups";
    private static final String PERSONAL_GROUPS_DELETE_ENDPOINT = "/rest/participants/personal-groups/1264/delete";
    private static final String BAD_PERSONAL_GROUPS_DELETE_ENDPOINT = "/rest/participants/personal-groups/abcd/delete";
    
    @Test
    public void get_personal_groups_happy_path() throws Exception {
        mockMvc.perform(
                get(PERSONAL_GROUPS_ENDPOINT_8987)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
             .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_personal_groups_bad_id() throws Exception {
        mockMvc.perform(
                get(PERSONAL_GROUPS_ENDPOINT_8987)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(GroupConstants.ERROR_CANNOT_GET_GROUP_CODE)))
       ;
    }
    
    @Test
    public void get_personal_groups_bad_endpoint() throws Exception {
        mockMvc.perform(
                get(BAD_PERSONAL_GROUPS_ENDPOINT)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isNotFound())
       ;
    }

    @Test
    public void delete_personal_groups_happy_path() throws Exception {
        mockMvc.perform(
                post(PERSONAL_GROUPS_DELETE_ENDPOINT)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
             .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void delete_personal_groups_bad_id() throws Exception {
        mockMvc.perform(
                post(PERSONAL_GROUPS_DELETE_ENDPOINT)
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(GroupConstants.ERROR_CANNOT_EDIT_GROUP_CODE)))
       ;
    }
    
    @Test
    public void delete_personal_groups_bad_endpoint() throws Exception {
        mockMvc.perform(
                post(BAD_PERSONAL_GROUPS_DELETE_ENDPOINT)
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isNotFound())
       ;
    }
}
