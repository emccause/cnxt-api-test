package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class CombinedSearchRestServiceTest extends AbstractRestTest {

    @Test
    public void getMatchingSearch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=adm&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParams() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=adm&page=1&size=20&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getMatchingSearchWithParamsTooFar() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=adm&page=100&size=20&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            ).andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNonMatchingSearch() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=BREAKING_EVERYTHING&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk()).andExpect(content().string("[]"));
    }

    @Test
    public void getNoSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/search?page=1&size=10&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void getNotExcludeSelf() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=Mudd&excludeSelf=false")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getExcludeSelf() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=Mudd&excludeSelf=true")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk()).andReturn();
        
        assert(result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getShortSearchString() throws Exception {
        mockMvc.perform(
                get("/rest/search?searchStr=M&page=1&size=10&usePersonalGroups=TRUE")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isBadRequest());
    }
    
    @Test
    public void getControlNumNoMatch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=T1023")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk()).andReturn();
        
        assert(result.getResponse().getContentAsString().equals("[]"));
    }
    
    @Test
    public void getControlNumMatch() throws Exception {
        MvcResult result = mockMvc.perform(
                get("/rest/search?searchStr=T10234")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
            .andExpect(status().isOk()).andReturn();
        
        assert(!result.getResponse().getContentAsString().equals("[]"));
    }
}
