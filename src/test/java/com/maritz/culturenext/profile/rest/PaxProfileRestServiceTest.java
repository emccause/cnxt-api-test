package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.test.AbstractRestTest;

public class PaxProfileRestServiceTest extends AbstractRestTest {

    @Test
    public void get_pictureLoggedInDefaultPlaceholder() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/picture?size=default")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_pictureLoggedInDefaultPaxId() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/picture?size=default")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_pictureLoggedInTiny() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/picture?size=tiny")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_pictureOtherDefault() throws Exception {
        mockMvc.perform(
                get("/rest/participants/8987/profile/picture?size=default")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_pictureOtherTiny() throws Exception {
        mockMvc.perform(
                get("/rest/participants/8987/profile/picture?size=tiny")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void post_pictureError() throws Exception {
            
        mockMvc.perform(
                fileUpload("/rest/participants/~/profile/picture")
                .file(new MockMultipartFile("image" , "", MediaTypesEnum.IMAGE_JPEG.value(), "".getBytes()))
                .with(authToken(TestConstants.USER_KUMARSJ))
                )
            .andExpect(status().isBadRequest())
        ;
    }

    @Test
    public void delete_picture() throws Exception {
        mockMvc.perform(
                delete("/rest/participants/~/profile/picture")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void delete_pictureOther() throws Exception {
        mockMvc.perform(
                delete("/rest/participants/8987/profile/picture")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isForbidden())
        ;
    }
}
