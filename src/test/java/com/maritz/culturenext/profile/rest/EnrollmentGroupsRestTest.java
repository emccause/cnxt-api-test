package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;


public class EnrollmentGroupsRestTest extends AbstractRestTest {

    private static final String ENROLLMENT_GROUPS_ENDPOINT = "/rest/enrollment-config/groups";

    @Test
    public void get_enrollment_groups() throws Exception {
        mockMvc.perform(
                get("/rest/enrollment-config/groups")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
             .andExpect(status().isOk())
        ;
    }

    @Test
    public void update_enrollment_groups_bad_id() throws Exception {
        mockMvc.perform(
                put(ENROLLMENT_GROUPS_ENDPOINT)
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[{\"groupConfigId\":100, \"field\":\"AREA\", \"fieldDisplayName\":\"Area\", "
                        + "\"groupCreation\":\"ACTIVE\", \"visibility\":\"ADMIN ONLY\"}]")
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void update_enrollment_groups_bad_status() throws Exception {
        mockMvc.perform(
                put(ENROLLMENT_GROUPS_ENDPOINT)
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[{\"groupConfigId\":2, \"field\":\"AREA\", \"fieldDisplayName\":\"Area\", "
                        + "\"groupCreation\":\"BADSTATUS\", \"visibility\":\"ADMIN ONLY\"}]")
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void update_enrollment_groups_bad_visibility() throws Exception {
        mockMvc.perform(
                put(ENROLLMENT_GROUPS_ENDPOINT)
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[{\"groupConfigId\":2, \"field\":\"AREA\", \"fieldDisplayName\":\"Area\", "
                        + "\"groupCreation\":\"ACTIVE\", \"visibility\":\"BAD VISIBILITY\"}]")
            )
               .andExpect(status().isBadRequest())
       ;
    }

    @Test
    public void update_enrollment_groups_good() throws Exception {
        mockMvc.perform(
                put(ENROLLMENT_GROUPS_ENDPOINT)
                .with(authToken(TestConstants.USER_KUMARSJ))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .content("[{\"groupConfigId\":2, \"field\":\"AREA\", \"fieldDisplayName\":\"Area\", "
                        + "\"groupCreation\":\"ACTIVE\", \"visibility\":\"ADMIN\"}]")
            )
               .andExpect(status().isOk())
       ;
    }
}
