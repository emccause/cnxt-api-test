package com.maritz.culturenext.profile.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.EmailType;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EmailPasswordWrapperDTO;
import com.maritz.culturenext.profile.dto.LocaleDTO;
import com.maritz.culturenext.profile.dto.PaxMiscDTO;
import com.maritz.test.AbstractRestTest;


public class DetailProfileRestTest extends AbstractRestTest {

    public static final Long USER_MUDDAM_EMAIL_ID = 6392L;

    @Test
    public void get_detail_profile_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_demographics_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/demographics")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_emails_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/demographics/email")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_privacy_settings_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/privacy")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_recg_preferences_by_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/recognition_preferences")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_detail_profile_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_demographics_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/demographics")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_emails_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/demographics/email")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_privacy_settings_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/privacy")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_recg_preferences_for_logged_in_user() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/profile/recognition_preferences")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_detail_profile_invalid_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/x/profile")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
            .andExpect(status().isNotFound())
        ;
    }

    @Test
    public void put_update_email() throws Exception {
        mockMvc.perform(
                put("/rest/participants/~/profile/demographics/email")
                .with(authToken(TestConstants.USER_MUDDAM))
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        "{ "
                            + "\"emails\":[ "
                                + "{ "
                                + "\"emailId\": " + USER_MUDDAM_EMAIL_ID + ", "
                                + "\"paxId\": " + TestConstants.PAX_MUDDAM + ", "
                                + "\"emailAddress\": \"supportM365@maritz.com\", "
                                + "\"preferred\": \"Y\", "
                                + "\"emailTypeCode\": \"BUSINESS\", "
                                + "\"editable\": true "
                                + "} "
                            + "] "
                        + "} "
                    )
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void put_update_email_invalid_format() throws Exception {
        mockMvc.perform(
                put("/rest/participants/~/profile/demographics/email")
                .with(authToken(TestConstants.USER_ADMIN))
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        "{ "
                            + "\"emails\":[ "
                                + "{ "
                                + "\"emailId\": " + USER_MUDDAM_EMAIL_ID + ", "
                                + "\"paxId\": " + TestConstants.PAX_ADMIN + ", "
                                + "\"emailAddress\": \"supportM365 @maritz.com\", "
                                + "\"preferred\": \"Y\", "
                                + "\"emailTypeCode\": \"BUSINESS\", "
                                + "\"editable\": true "
                                + "} "
                            + "] "
                        + "} "
                    )
            )
            .andExpect(status().isBadRequest())
        ;
    }


    @Test
    public void post_insert_email() throws Exception {

        EmailDTO email = new EmailDTO();
        email.setPaxId(7119L);
        email.setEmailAddress("leah.harwell@maritz.com");
        email.setEmailTypeCode(EmailType.HOME.name());
        email.setPreferred("N");
        email.setEditable(true);
        List<EmailDTO> emailList = new ArrayList<>();
        emailList.add(email);
        EmailPasswordWrapperDTO requestObject = new EmailPasswordWrapperDTO();
        requestObject.setEmails(emailList);

        mockMvc.perform(
            post("/rest/participants/~/profile/demographics/email")
                .with(authToken("BESSEL"))
                .with(json(requestObject))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void patch_profile_privacy() throws Exception {

        PaxMiscDTO shareRec = new PaxMiscDTO();
        shareRec.setPaxId(TestConstants.PAX_HARWELLM);
        shareRec.setVfName(VfName.SHARE_REC.name());
        shareRec.setMiscData(ProjectConstants.TRUE);

        mockMvc.perform(
            patch("/rest/participants/~/profile/privacy")
                .with(authToken(TestConstants.USER_HARWELLM))
                .with(json(Arrays.asList(shareRec)))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void put_profile_privacy() throws Exception {

        List<PaxMiscDTO> privacyList = new ArrayList<>();
        PaxMiscDTO shareRec = new PaxMiscDTO();
        shareRec.setPaxId(TestConstants.PAX_HARWELLM);
        shareRec.setVfName(VfName.SHARE_REC.name());
        shareRec.setMiscData(ProjectConstants.TRUE);
        privacyList.add(shareRec);
        PaxMiscDTO shareBday = new PaxMiscDTO();
        shareBday.setPaxId(TestConstants.PAX_HARWELLM);
        shareBday.setVfName(VfName.SHARE_BDAY.name());
        shareBday.setMiscData(ProjectConstants.TRUE);
        privacyList.add(shareBday);
        PaxMiscDTO shareSA = new PaxMiscDTO();
        shareSA.setPaxId(TestConstants.PAX_HARWELLM);
        shareSA.setVfName(VfName.SHARE_SA.name());
        shareSA.setMiscData(ProjectConstants.TRUE);
        privacyList.add(shareSA);
        PaxMiscDTO emailRec = new PaxMiscDTO();
        emailRec.setPaxId(TestConstants.PAX_HARWELLM);
        emailRec.setVfName(VfName.RECEIVE_EMAIL_REC.name());
        emailRec.setMiscData(ProjectConstants.TRUE);
        privacyList.add(emailRec);
        PaxMiscDTO emailMgr = new PaxMiscDTO();
        emailMgr.setPaxId(TestConstants.PAX_HARWELLM);
        emailMgr.setVfName(VfName.RECEIVE_EMAIL_MGR_REC.name());
        emailMgr.setMiscData(ProjectConstants.TRUE);
        privacyList.add(emailMgr);
        PaxMiscDTO emailAppr = new PaxMiscDTO();
        emailAppr.setPaxId(TestConstants.PAX_HARWELLM);
        emailAppr.setVfName(VfName.RECEIVE_EMAIL_APPR.name());
        emailAppr.setMiscData(ProjectConstants.TRUE);
        privacyList.add(emailAppr);
        PaxMiscDTO emailNotify = new PaxMiscDTO();
        emailNotify.setPaxId(TestConstants.PAX_HARWELLM);
        emailNotify.setVfName(VfName.RECEIVE_EMAIL_NOTIFY_OTHERS.name());
        emailNotify.setMiscData(ProjectConstants.TRUE);
        privacyList.add(emailNotify);
        PaxMiscDTO emailDigest = new PaxMiscDTO();
        emailDigest.setPaxId(TestConstants.PAX_HARWELLM);
        emailDigest.setVfName(VfName.RECEIVE_EMAIL_DIGEST.name());
        emailDigest.setMiscData(ProjectConstants.TRUE);
        privacyList.add(emailDigest);

        mockMvc.perform(
                put("/rest/participants/~/profile/privacy")
                .with(authToken(TestConstants.USER_HARWELLM))
                .with(json(privacyList))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void post_profile_privacy() throws Exception {

        PaxMiscDTO shareRec = new PaxMiscDTO();
        shareRec.setPaxId(TestConstants.PAX_ABBOTTM);
        shareRec.setVfName(VfName.RECEIVE_EMAIL_NOTIFY_OTHERS.name());
        shareRec.setMiscData(ProjectConstants.TRUE);

        mockMvc.perform(
                post("/rest/participants/~/profile/privacy")
                .with(authToken(TestConstants.USER_ABBOTTM))
                .with(json(Arrays.asList(shareRec)))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void put_recognition_preferences() throws Exception {

        List<PaxMiscDTO> preferencesList = new ArrayList<>();
        PaxMiscDTO verbal1 = new PaxMiscDTO();
        verbal1.setPaxId(TestConstants.PAX_HARWELLM);
        verbal1.setVfName(VfName.VERBAL_REC_1.name());
        verbal1.setMiscData(ProjectConstants.TRUE);
        preferencesList.add(verbal1);
        PaxMiscDTO verbal2 = new PaxMiscDTO();
        verbal2.setPaxId(TestConstants.PAX_HARWELLM);
        verbal2.setVfName(VfName.VERBAL_REC_2.name());
        verbal2.setMiscData(ProjectConstants.TRUE);
        preferencesList.add(verbal2);
        PaxMiscDTO verbal3 = new PaxMiscDTO();
        verbal3.setPaxId(TestConstants.PAX_HARWELLM);
        verbal3.setVfName(VfName.VERBAL_REC_3.name());
        verbal3.setMiscData(ProjectConstants.TRUE);
        preferencesList.add(verbal3);
        PaxMiscDTO written1 = new PaxMiscDTO();
        written1.setPaxId(TestConstants.PAX_HARWELLM);
        written1.setVfName(VfName.WRITTEN_REC_1.name());
        written1.setMiscData(ProjectConstants.TRUE);
        preferencesList.add(written1);
        PaxMiscDTO written2 = new PaxMiscDTO();
        written2.setPaxId(TestConstants.PAX_HARWELLM);
        written2.setVfName(VfName.WRITTEN_REC_2.name());
        written2.setMiscData(ProjectConstants.TRUE);
        preferencesList.add(written2);

        mockMvc.perform(
            put("/rest/participants/~/profile/recognition_preferences")
                .with(authToken(TestConstants.USER_HARWELLM))
                .with(json(preferencesList))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void post_recognition_preferences() throws Exception {

        PaxMiscDTO verbal1 = new PaxMiscDTO();
        verbal1.setPaxId(TestConstants.PAX_ABBOTTM);
        verbal1.setVfName(VfName.VERBAL_REC_1.name());
        verbal1.setMiscData(ProjectConstants.TRUE);

        mockMvc.perform(
            post("/rest/participants/~/profile/recognition_preferences")
                .with(authToken(TestConstants.USER_ABBOTTM))
                .with(json(Arrays.asList(verbal1)))
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_direct_reports() throws Exception {
        mockMvc.perform(
            get("/rest/participants/~/direct-reports")
                .with(authToken(TestConstants.USER_HARWELLM))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void get_direct_reports_without_inactive() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/direct-reports?excludeInactive=true")
                        .with(authToken(TestConstants.USER_HARWELLM))
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
        )
                .andExpect(status().isOk())
        ;
    }

    @Test
    public void update_pax_language_and_locale() throws Exception {
        LocaleDTO locale = new LocaleDTO();
        locale.setLanguageCode(LocaleCodeEnum.en_US.name());

        mockMvc.perform(
            put("/rest/participants/~/profile/pax")
                .with(authToken(TestConstants.USER_HARWELLM))
                .with(json(locale))
            )
            .andExpect(status().isOk())
        ;
    }
}
