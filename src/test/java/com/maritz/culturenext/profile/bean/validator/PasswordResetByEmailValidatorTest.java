package com.maritz.culturenext.profile.bean.validator;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.culturenext.profile.bean.validator.PasswordResetByEmailValidator;
import com.maritz.profile.dto.PasswordResetDTO;
import com.maritz.test.AbstractDatabaseTest;

public class PasswordResetByEmailValidatorTest extends AbstractDatabaseTest {
    public static final String VALID_EMAIL = "a@b.c";
    public static final String INVALID_EMAIL = "invalid.user@b.c";
    
    @Inject EmailRepository emailRepository;

    PasswordResetByEmailValidator passwordResetByEmailValidator;

    @Before
    public void setup() {
        passwordResetByEmailValidator = new PasswordResetByEmailValidator();
        passwordResetByEmailValidator.setEmailRepository(emailRepository);
    }

    @Test
    public void when_valid_email_then_no_errors() {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        PasswordResetDTO passwordReset = new PasswordResetDTO();
        passwordReset.setEmail(VALID_EMAIL);

        passwordResetByEmailValidator.validate(passwordReset, errorMessages);

        assertThat(errorMessages.isEmpty(), is(true));
    }

    @Test
    public void when_username_set_then_no_errors() {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        PasswordResetDTO passwordReset = new PasswordResetDTO();
        passwordReset.setUserName("a_user_name");

        passwordResetByEmailValidator.validate(passwordReset, errorMessages);

        assertThat(errorMessages.isEmpty(), is(true));
    }

    @Test
    public void when_invalid_email_then_errors() {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        PasswordResetDTO passwordReset = new PasswordResetDTO();
        passwordReset.setEmail(INVALID_EMAIL);

        passwordResetByEmailValidator.validate(passwordReset, errorMessages);

        assertThat(errorMessages.isEmpty(), is(true));
    }
    
}