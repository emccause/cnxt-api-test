package com.maritz.culturenext.profile.dao;

import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dao.PaxCountDao;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class PaxCountDaoTest extends AbstractDatabaseTest {
    @Inject private PaxCountDao paxCountDao;
    @Inject private AuthenticationService authenticationService;
    @Inject private Security security;

    private static Long submitterId = 0L;

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        submitterId = security.getPaxId();
    }

    @Test
    public void test_getCountDistinctPax_happyPath_exclude_true() {
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(5L);
        List<Long> paxIds = new ArrayList<>();

        Integer countDistinctPax = paxCountDao.getCountDistinctPax(groupIds,
                                                                   paxIds,
                                                                   true);

        Assert.assertTrue(countDistinctPax == 3);
    }

    @Test
    public void test_getCountDistinctPax_happyPath_exclude_false() {
        List<Long> groupIds = new ArrayList<>();
        groupIds.add(5L);
        List<Long> paxIds = new ArrayList<>();

        Integer countDistinctPax = paxCountDao.getCountDistinctPax(groupIds,
                                                                   paxIds,
                                                                   false);

        Assert.assertEquals(Integer.valueOf(4), countDistinctPax);
    }

    @Test
    public void test_getCountDistinctPax_empty_lists() {
        List<Long> groupIds = new ArrayList<>();
        List<Long> paxIds = new ArrayList<>();

        Integer countDistinctPax = paxCountDao.getCountDistinctPax(groupIds,
                                                                   paxIds,
                                                                   true);

        Assert.assertEquals(Integer.valueOf(0), countDistinctPax);
    }
}
