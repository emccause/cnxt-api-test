package com.maritz.culturenext.profile.dao;

import com.maritz.culturenext.profile.dao.PaginatedGroupMembershipDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class PaginatedGroupMembershipDaoTest extends AbstractDatabaseTest {
    
    @Inject private PaginatedGroupMembershipDao paginatedGroupMembershipDao;
    
    Long TEST_GROUP_ID = 1272L; // 3 groups, 3 pax - 11 total pax
    
    @Test
    public void test_get_members(){
        List<EntityDTO> members = paginatedGroupMembershipDao.getMembership(TEST_GROUP_ID);
        assertThat(members.isEmpty(), is(false));
    }
    
    @Test
    public void test_get_members_expanded(){
        List<EntityDTO> members = paginatedGroupMembershipDao.getMembership(TEST_GROUP_ID);
        assertThat(members.isEmpty(), is(false));
        List<EntityDTO> membersExpanded = paginatedGroupMembershipDao.getExpandedMembership(TEST_GROUP_ID);
        assertThat(membersExpanded.isEmpty(), is(false));
        
        for(EntityDTO memberExpandedBase : membersExpanded){
            boolean found = false;
            EmployeeDTO memberPax = null;
            if(memberExpandedBase instanceof EmployeeDTO){
                memberPax = (EmployeeDTO) memberExpandedBase;
            }
            else{
                fail("Should only be EmployeeDTOs in expanded member list");
            }
            for(EntityDTO memberBase : members){
                if(memberBase instanceof EmployeeDTO){
                    EmployeeDTO memberBasePax = (EmployeeDTO) memberBase;
                    if(memberBasePax.getPaxId().equals(memberPax.getPaxId())){
                        found = true;
                        continue; // no need to keep checking
                    }
                }
            }
            if(found == false){
                // didn't find pax in non-expanded members list
                break; // no need to keep testing, we've already found a pax in 
                        //the expanded list that wasn't in the non-expanded list. 
            }
        }
    }

}
