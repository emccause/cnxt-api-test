package com.maritz.culturenext.profile.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.test.AbstractDatabaseTest;

public class ManagerDirectReportDaoTest extends AbstractDatabaseTest {
    
    @Inject private ManagerDirectReportDao managerDirectReportDao;
    
    private static final Long DIRECT_REPORTS_MANAGER_PAX_ID = 13L;
    private static final Long DIRECT_REPORTS_PAX_ID = 1644L;
    private static final String STATUS = "INACTIVE";

    @Test
    public void test_get_manager_direct_reports_good() { 
        
        List<Map<String, Object>> directReportList = managerDirectReportDao.getManagerDirectReports(
                DIRECT_REPORTS_MANAGER_PAX_ID, false, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE
            );
        boolean found = false;
        
        for (Map<String, Object> directReport : directReportList) {
            if(DIRECT_REPORTS_PAX_ID.compareTo((Long) directReport.get(ProjectConstants.PAX_ID)) == 0) {
                found = true;
                break;
            }
        }

        assertTrue(found);
    }

    @Test
    public void test_get_manager_direct_reports_bad() { 
        
        List<Map<String, Object>> directReportList = managerDirectReportDao.getManagerDirectReports(
                DIRECT_REPORTS_MANAGER_PAX_ID, false, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE
            );
        boolean found = false;
        
        for (Map<String, Object> directReport : directReportList) {
            if(DIRECT_REPORTS_MANAGER_PAX_ID.compareTo((Long) directReport.get(ProjectConstants.PAX_ID)) == 0) {
                found = true;
                break;
            }
        }

        assertFalse(found);
    }

    @Test
    public void test_get_manager_direct_reports_with_inactive() {
        List<Map<String, Object>> directReportList = managerDirectReportDao.getManagerDirectReports(
                DIRECT_REPORTS_MANAGER_PAX_ID, false, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE
        );
        boolean found = false;

        for (Map<String, Object> directReport : directReportList) {
            if(STATUS.compareTo((String) directReport.get(ProjectConstants.STATUS_TYPE_CODE)) == 0) {
                found = true;
                break;
            }
        }

        assertTrue(found);
    }

    @Test
    public void test_get_manager_direct_reports_without_inactive() {
        List<Map<String, Object>> directReportList = managerDirectReportDao.getManagerDirectReports(
                DIRECT_REPORTS_MANAGER_PAX_ID, true, ProjectConstants.DEFAULT_PAGE_NUMBER, ProjectConstants.MAX_PAGE_SIZE
        );
        boolean found = false;

        for (Map<String, Object> directReport : directReportList) {
            if(STATUS.compareTo((String) directReport.get(ProjectConstants.STATUS_TYPE_CODE)) == 0) {
                found = true;
                break;
            }
        }

        assertFalse(found);
    }
}
