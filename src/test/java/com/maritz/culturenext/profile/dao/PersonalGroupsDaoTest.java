package com.maritz.culturenext.profile.dao;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class PersonalGroupsDaoTest extends AbstractDatabaseTest {
    
    @Inject private PersonalGroupsDao personalGroupsDao;
    
    @Test
    public void test_get_personal_groups(){
        List<Map<String, Object>> personalGroups = personalGroupsDao.getPersonalGroups(TestConstants.PAX_HARWELLM);
        assertThat(personalGroups.isEmpty(), is(false));
    }
    
    @Test
    public void test_get_personal_groups_size(){
        List<Map<String, Object>> personalGroups = personalGroupsDao.getPersonalGroups(TestConstants.PAX_HARWELLM);
        assertThat(personalGroups.size(), is(2));
    }
    
    @Test
    public void test_get_personal_groups_name(){
        List<Map<String, Object>> personalGroups = personalGroupsDao.getPersonalGroups(TestConstants.PAX_HARWELLM);
        Map<String, Object> personalGroup = personalGroups.get(0);
        assertThat(personalGroup.get("groupName"), is("Personal Group Test 1"));
    }
    
    @Test
    public void test_get_personal_groups_pax_count(){
        List<Map<String, Object>> personalGroups = personalGroupsDao.getPersonalGroups(TestConstants.PAX_HARWELLM);
        Map<String, Object> personalGroup = personalGroups.get(0);
        assertThat(personalGroup.get("paxCount"), is(3));
    }

}
