package com.maritz.culturenext.profile.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.enums.ActivityFeedFilterEnum;
import com.maritz.culturenext.jpa.repository.CnxtGroupConfigRepository;
import com.maritz.culturenext.profile.dto.aggregate.EnrollmentGroupDTO;
import com.maritz.culturenext.profile.services.EnrollmentGroupsService;

import org.junit.Assert;
import org.junit.Test;

import com.maritz.core.jpa.entity.GroupConfig;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class EnrollmentGroupsServiceTest extends AbstractDatabaseTest {

    private static final String ENROLLMENT_GROUP = "ENRL_FIELD";

    @Inject private ApplicationDataService applicationDataService;
    @Inject private CnxtGroupConfigRepository cnxtGroupConfigRepository;
    @Inject private EnrollmentGroupsService egService;
    
    @Test
    public void test_get_enrollment_groups() throws Exception {
        List<EnrollmentGroupDTO> groups = egService.getEnrollmentGroups();
        List<GroupConfig> gcList = cnxtGroupConfigRepository.findByGroupConfigTypeCode(ENROLLMENT_GROUP);

        assertThat(gcList.size(), is(groups.size()));
    }

    @Test
    public void test_update_enrollment_groups() throws Exception {

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_2);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.AREA.name());
        enrollmentGroupDTO.setFieldDisplayName("Area");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.ACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_2);

        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));

    }

    @Test
    public void test_updateEnrollmentGroups_addActivityFeedFilter() throws Exception {
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            ActivityFeedFilterEnum.AREA.name();

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_2);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.AREA.name());
        enrollmentGroupDTO.setFieldDisplayName("Area");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.ACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_2);
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);

        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));
        Assert.assertEquals(applicationDataDto.getKey(), activityFeedFilterKey);
    }

    @Test
    public void test_updateEnrollmentGroups_activityFeedFilter_activeAdd() throws Exception {
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            ActivityFeedFilterEnum.FUNCTION.name();

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_9);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.FUNCTION.name());
        enrollmentGroupDTO.setFieldDisplayName("Function");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.ACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_9);
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);

        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));
        Assert.assertEquals(applicationDataDto.getKey(), activityFeedFilterKey);
    }

    @Test
    public void test_updateEnrollmentGroups_activityFeedFilter_inactiveDelete() throws Exception {
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            ActivityFeedFilterEnum.CITY.name();

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_3);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.CITY.name());
        enrollmentGroupDTO.setFieldDisplayName("City");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.INACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_3);
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);

        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));
        Assert.assertEquals(null, applicationDataDto);
    }

    @Test
    public void test_updateEnrollmentGroups_activityFeedFilter_activeNoDelete() throws Exception {
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            ActivityFeedFilterEnum.AREA.name();

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_2);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.AREA.name());
        enrollmentGroupDTO.setFieldDisplayName("Area");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.ACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_2);
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);

        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));
        Assert.assertEquals(activityFeedFilterKey, applicationDataDto.getKey());
    }

    @Test
    public void test_updateEnrollmentGroups_activityFeedFilter_inactive_defaultFilterChange() throws Exception {
        String groupName = ActivityFeedFilterEnum.AREA.name();
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            groupName;

        // First set the defaultFilter in APPLICATION_DATA to be the same group being deactivated
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(
            ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT);
        applicationDataDto.setValue(groupName);
        applicationDataService.setApplicationData(applicationDataDto);

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_2);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.AREA.name());
        enrollmentGroupDTO.setFieldDisplayName("Area");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.INACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_2);
        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));

        applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);
        Assert.assertEquals(null, applicationDataDto);

        // Make sure the default value gets reset to PUBLIC
        applicationDataDto = applicationDataService.getApplicationData(
            ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT);
        Assert.assertEquals(ActivityFeedFilterEnum.PUBLIC.name(), applicationDataDto.getValue());
    }

    @Test
    public void test_updateEnrollmentGroups_activityFeedFilter_inactive_defaultFilterUnchanged() throws Exception {
        String activityFeedFilterKey = ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER +
            ProjectConstants.DOT_DELIM +
            ActivityFeedFilterEnum.AREA.name();
        String defaultFilter = ActivityFeedFilterEnum.CITY.name();

        // First set the defaultFilter in APPLICATION_DATA to be something other than group being deactivated
        ApplicationDataDTO applicationDataDto = applicationDataService.getApplicationData(
            ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT);
        applicationDataDto.setValue(defaultFilter);
        applicationDataService.setApplicationData(applicationDataDto);

        EnrollmentGroupDTO enrollmentGroupDTO = new EnrollmentGroupDTO();

        enrollmentGroupDTO.setGroupConfigId(TestConstants.ID_2);
        enrollmentGroupDTO.setField(ActivityFeedFilterEnum.AREA.name());
        enrollmentGroupDTO.setFieldDisplayName("Area");
        enrollmentGroupDTO.setGroupCreation(StatusTypeCode.INACTIVE.name());
        enrollmentGroupDTO.setVisibility("ADMIN");

        List<EnrollmentGroupDTO> groupList = new ArrayList<>();

        groupList.add(enrollmentGroupDTO);

        egService.updateEnrollmentGroups(groupList);

        GroupConfig groupConfig = cnxtGroupConfigRepository.findOne(TestConstants.ID_2);
        assertThat(groupConfig.getId(), is(enrollmentGroupDTO.getGroupConfigId()));
        assertThat(groupConfig.getGroupConfigName(), is(enrollmentGroupDTO.getField()));
        assertThat(groupConfig.getGroupConfigDesc(), is(enrollmentGroupDTO.getFieldDisplayName()));
        assertThat(groupConfig.getStatusTypeCode(), is(enrollmentGroupDTO.getGroupCreation()));

        applicationDataDto = applicationDataService.getApplicationData(activityFeedFilterKey);
        Assert.assertEquals(null, applicationDataDto);

        // Make sure the defaultFilter wasn't changed
        applicationDataDto = applicationDataService.getApplicationData(
            ApplicationDataConstants.KEY_NAME_ACTIVITY_FEED_FILTER_DEFAULT);
        Assert.assertEquals(defaultFilter, applicationDataDto.getValue());
    }
}
