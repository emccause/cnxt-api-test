package com.maritz.culturenext.profile.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupMemberStubDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentFullDTO;
import com.maritz.culturenext.profile.dto.aggregate.GroupEnrollmentWithStubDTO;
import com.maritz.test.AbstractDatabaseTest;

public class GroupsServiceTest extends AbstractDatabaseTest {
    
    @Inject private AuthenticationService authenticationService;
    @Inject private GroupsService groupsService;
    
    private static final Long TEST_ENROLLMENT_GROUP_ID = 1261L;
    private static final Long TEST_MUDDAM_PERSONAL_GROUP_ID_1 = 553L;
    private static final Long TEST_MUDDAM_PERSONAL_GROUP_ID_2 = 1264L;
    private static final Long TEST_PAX_ID = 5961L;
    private static final String TEST_GROUP_NAME = "Test Personal Group muddam";
    
    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_MUDDAM);
    }
    
    @Test
    public void test_create_personal_group() throws Exception {
        assertNotNull("Groups Service should not be null", groupsService);
        
        GroupEnrollmentWithStubDTO groupEnrollmentStub = new GroupEnrollmentWithStubDTO();
        groupEnrollmentStub.setType(GroupType.PERSONAL.name());
        groupEnrollmentStub.setGroupName(TEST_GROUP_NAME);
        //Setting members
        GroupMemberStubDTO groupMemberStubPax = new GroupMemberStubDTO();
        groupMemberStubPax.setPaxId(TestConstants.PAX_HARWELLM);
        groupEnrollmentStub.setMembers(Arrays.asList(groupMemberStubPax));
        
        GroupEnrollmentFullDTO finalGroupInfo = groupsService.createGroup(groupEnrollmentStub);
        assertEquals(finalGroupInfo.getGroupName(), TEST_GROUP_NAME);
        assertNotNull("Group ID should not be null", finalGroupInfo.getGroupId());
    }
    
    @Test
    public void test_create_bad_personal_group() throws Exception {
        assertNotNull("Groups Service should not be null", groupsService);
        
        GroupEnrollmentWithStubDTO groupEnrollmentStub = new GroupEnrollmentWithStubDTO();
        groupEnrollmentStub.setType(GroupType.PERSONAL.name());
        groupEnrollmentStub.setGroupName(TEST_GROUP_NAME);
        //Setting members
        GroupMemberStubDTO groupMemberStubPax = new GroupMemberStubDTO();
        groupMemberStubPax.setPaxId(TestConstants.PAX_HARWELLM);
        GroupMemberStubDTO groupMemberStubGroup = new GroupMemberStubDTO();
        groupMemberStubGroup.setGroupId(TEST_MUDDAM_PERSONAL_GROUP_ID_1);
        groupEnrollmentStub.setMembers(Arrays.asList(groupMemberStubPax, groupMemberStubGroup));
        try {
        	groupsService.createGroup(groupEnrollmentStub);
        } catch (ErrorMessageException e){
        	assertEquals(e.getErrorMessages().size(), 1);
            assertTrue(e.getMessage().contains("GROUP_NOT_ALLOWED"));
        }
    }
    
    @Test
    public void test_add_members_personal_groups() throws Exception {
        //Setting members
        GroupMemberStubDTO groupMemberStubPax = new GroupMemberStubDTO();
        groupMemberStubPax.setPaxId(TestConstants.PAX_HARWELLM);
        List<GroupMemberStubDTO> groupMembers = Arrays.asList(groupMemberStubPax);

        List<EntityDTO> members = groupsService.addMembers(TEST_MUDDAM_PERSONAL_GROUP_ID_1, groupMembers);
        assertEquals("Members List should have 1 member", members.size(), 1);
        for (EntityDTO member : members) {
        	assertEquals("Member Pax ID should not be null", ((EmployeeDTO) member).getPaxId(), TestConstants.PAX_HARWELLM);
        }
    }

    @Test
    public void test_add_bad_members_personal_groups() throws Exception {
        //Setting members
        GroupMemberStubDTO groupMemberStubPax = new GroupMemberStubDTO();
        groupMemberStubPax.setPaxId(TestConstants.PAX_HARWELLM);
        GroupMemberStubDTO groupMemberStubGroup = new GroupMemberStubDTO();
        groupMemberStubGroup.setGroupId(TEST_ENROLLMENT_GROUP_ID);
        List<GroupMemberStubDTO> groupMembers = Arrays.asList(groupMemberStubPax, groupMemberStubGroup);
        try {
        	groupsService.addMembers(TEST_MUDDAM_PERSONAL_GROUP_ID_1, groupMembers);
        } catch (ErrorMessageException e) {
        	assertEquals(e.getErrorMessages().size(), 1);
            assertTrue(e.getMessage().contains("GROUP_NOT_ALLOWED"));
        }
    }
    
    @Test
    public void test_delete_members_personal_groups() throws Exception {
        assertEquals("Members List should have 5 members before delete", groupsService.getGroupMembers(TEST_MUDDAM_PERSONAL_GROUP_ID_2, false, 0, 50).size(), 5);
        
        //Setting members
        GroupMemberStubDTO groupMemberStubPax = new GroupMemberStubDTO();
        groupMemberStubPax.setPaxId(TEST_PAX_ID);
        List<GroupMemberStubDTO> groupMembers = Arrays.asList(groupMemberStubPax);

        groupsService.deleteMembers(TEST_MUDDAM_PERSONAL_GROUP_ID_2, groupMembers);
        
        assertEquals("Members List should have 4 members after delete", groupsService.getGroupMembers(TEST_MUDDAM_PERSONAL_GROUP_ID_2, false, 0, 50).size(), 4);
    }

}
