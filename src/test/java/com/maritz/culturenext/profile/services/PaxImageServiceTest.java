package com.maritz.culturenext.profile.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import javax.inject.Inject;

import com.maritz.core.util.stream.StreamingOutput;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.dao.FilesDao;
import com.maritz.culturenext.profile.services.impl.PaxImageServiceImpl;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PaxImageServiceTest extends AbstractDatabaseTest {
    private static final String FILE_3K_X_3K = "test-images/test-image-3000x3000.jpg";
    
    @Inject private PaxImageServiceImpl paxImageService;
    @Inject private SysUserRepository sysUserRepository;
    @Inject private FilesDao filesDao;

    @Mock
    private EnvironmentUtil mockEnvironmentUtil;
    @Mock
    private GcpUtil mockGcpUtil;


    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

        
    @Test
    public void test_getLoggedInImageDefaultGood() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_KUMARSJ);
        ResponseEntity<Object> response = paxImageService.getImage(sysUser.getPaxId(), "default");
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }
    
    @Test
    public void test_getLoggedInImageTinyGood() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_KUMARSJ);
        ResponseEntity<Object> response = paxImageService.getImage(sysUser.getPaxId(), "tiny");
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }
    
    @Test
    public void test_getLoggedInImageTinyBad() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_RIDERAC1);
        ResponseEntity<Object> response = paxImageService.getImage(sysUser.getPaxId(), "tiny");
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
    
    @Test
    public void test_getOtherImageDefaultGood() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_MUDDAM);
        ResponseEntity<Object> response = paxImageService.getImage(sysUser.getPaxId(), "default");
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }
    
    @Test
    public void test_getOtherImageTinyGood() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_MUDDAM);
        ResponseEntity<Object> response = paxImageService.getImage(sysUser.getPaxId(), "tiny");
        assertThat(response.getStatusCode(), is(HttpStatus.OK));
    }

    @Test
    public void uploadImage_cdnEnabled() throws Exception {
        paxImageService
                .setEnvironmentUtil(mockEnvironmentUtil)
                .setGcpUtil(mockGcpUtil);

        when(mockEnvironmentUtil.writeToCdn()).thenReturn(true);

        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_MUDDAM);
        paxImageService.uploadImage(sysUser.getPaxId(), null, new MultipartFile[] {
                new MockMultipartFile("alex.jpg", "alex.jpg",
                        MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_3K_X_3K).getInputStream())
                }, false);

        verify(mockGcpUtil, times(1)).uploadFile(anyLong(), any(StreamingOutput.class));
    }

    private static byte[] toByteArray(StreamingOutput streamingOutput) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        streamingOutput.write(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }
}
