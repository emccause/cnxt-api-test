package com.maritz.culturenext.profile.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.core.env.Environment;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.Email;
import com.maritz.core.jpa.entity.Language;
import com.maritz.core.jpa.entity.LocaleInfo;
import com.maritz.core.jpa.entity.Pax;
import com.maritz.core.jpa.entity.PaxEventCommunication;
import com.maritz.core.jpa.entity.PaxMisc;
import com.maritz.core.jpa.entity.Phone;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.EmailRepository;
import com.maritz.core.jpa.repository.PaxEventCommunicationRepository;
import com.maritz.core.jpa.repository.PaxMiscRepository;
import com.maritz.core.jpa.repository.PhoneRepository;
import com.maritz.core.jpa.repository.RelationshipRepository;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.jpa.support.util.Tuple4;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.EmailType;
import com.maritz.culturenext.enums.LocaleCodeEnum;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.VfName;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.permission.services.ProxyService;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EmailPasswordWrapperDTO;
import com.maritz.culturenext.profile.dto.LocaleDTO;
import com.maritz.culturenext.profile.dto.PaxMiscDTO;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.profile.dto.aggregate.PaxMiscListDTO;
import com.maritz.culturenext.profile.dto.aggregate.ProfileDemographics;
import com.maritz.culturenext.profile.services.impl.DetailProfileServiceImpl;
import com.maritz.test.AbstractMockTest;

public class DetailProfileServiceUnitTest extends AbstractMockTest {

    @InjectMocks private DetailProfileServiceImpl dpService;
    @Mock private Security security;
    @Mock private ProxyService proxyService;
    @Mock private AuthenticationService authenticationService;
    @Mock private ConcentrixDao<Pax> paxDao;
    @Mock private PaxMiscRepository paxMiscRepository;
    @Mock private ConcentrixDao<PaxMisc> paxMiscDao;
    @Mock private AddressRepository addressRepository;
    @Mock private EmailRepository emailRepository;
    @Mock private SysUserRepository sysUserRepository;
    @Mock private PaxEventCommunicationRepository paxEventCommunicationRepository;
    @Mock private PhoneRepository phoneRepository;
    @Mock private RelationshipRepository relationshipRepository;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private ConcentrixDao<Language> languageDao;
    @Mock private ConcentrixDao<LocaleInfo> localeInfoDao;
    @Mock private Environment environment;
    @Mock private ProfileIncompleteService profileIncompleteService;

    private static final Long PAX_ID = 123L;
    private static final Long MANAGER_PAX_ID = 321L;
    private static final String BAD_VFNAME_TEST = "BAD_VFNAME_TEST";
    private static final String BAD_MISC_DATA_TEST = "BAD_MISC_DATA_TEST";

    // user information
    private static final Long TEST_INSERT_EMAIL_PAX_ID = 5564L;

    // emails
    private static final String VALID_EMAIL = "junitTest@junit.com";

    private com.maritz.core.jpa.support.findby.FindBy<Relationship> relationshipFindBy =
            mock(com.maritz.core.jpa.support.findby.FindBy.class);

    // Imported 'Relationship' findby differs from 'PaxMisc' findby
    private FindBy<Pax> paxFindBy = mock(FindBy.class);
    private FindBy<LocaleInfo> localeInfoFindBy = mock(FindBy.class);
    private FindBy<LocaleInfo> invalidLocaleInfoFindBy = mock(FindBy.class);
    private FindBy<LocaleInfo> inactiveLocaleInfoFindBy = mock(FindBy.class);

    private static Validator validator;

    @Before
    public void setup() {
        //Mock out user default
        EmployeeDTO userPax = new EmployeeDTO();
        String controlNum = "s345";
        String firstName = "Kramer";
        String middleName = "M";
        String lastName = "Martin";
        Integer profilePictureVersion = 1;
        String status = StatusTypeCode.ACTIVE.name();
        String jobTitle = "Developer II";

        userPax.setPaxId(PAX_ID);
        userPax.setLanguageCode(LocaleCodeEnum.en_US.name());
        userPax.setPreferredLocale(LocaleCodeEnum.en_US.name());
        userPax.setControlNum(controlNum);
        userPax.setFirstName(firstName);
        userPax.setMiddleName(middleName);
        userPax.setLastName(lastName);
        userPax.setProfilePictureVersion(profilePictureVersion);
        userPax.setStatus(status);
        userPax.setJobTitle(jobTitle);
        userPax.setManagerPaxId(MANAGER_PAX_ID);

        when(participantInfoDao.getEmployeeDTO(PAX_ID)).thenReturn(userPax);

        //Mock manager default
        EmployeeDTO managerPax = new EmployeeDTO();
        jobTitle = "Manager";

        managerPax.setPaxId(MANAGER_PAX_ID);
        userPax.setLanguageCode(LocaleCodeEnum.en_US.name());
        userPax.setPreferredLocale(LocaleCodeEnum.en_US.name());
        userPax.setControlNum(controlNum);
        userPax.setFirstName(firstName);
        userPax.setMiddleName(middleName);
        userPax.setLastName(lastName);
        userPax.setProfilePictureVersion(profilePictureVersion);
        userPax.setStatus(status);
        userPax.setJobTitle(jobTitle);
        userPax.setManagerPaxId(MANAGER_PAX_ID);

        when(participantInfoDao.getEmployeeDTO(MANAGER_PAX_ID)).thenReturn(userPax);


        //Mock Address default
        Address address1 = new Address();
        address1.setAddressId(TestConstants.ID_1);
        address1.setPaxId(PAX_ID);
        address1.setAddress1("404 SOUTH 2ND AVENUE");
        address1.setCity("SAN DIEGO");
        address1.setState("CA");
        address1.setZip("678015611");
        address1.setCountryCode("US");
        address1.setCode1Bypass("Y");
        address1.setAddressTypeCode(EmailType.BUSINESS.name());

        List<Address> addressList = Arrays.asList(address1);
        when(addressRepository.findByPaxId(PAX_ID)).thenReturn(addressList);

        //Mock SysUser default data.
        SysUser sysUser1 = new SysUser();
        sysUser1.setSysUserId(TestConstants.ID_1);
        sysUser1.setPaxId(PAX_ID);
        sysUser1.setSysUserName("tester");
        sysUser1.setStatusDate(new Date());
        sysUser1.setStatusTypeCode(StatusTypeCode.ACTIVE.name());
        sysUser1.setSysUserPasswordDate(new Date());
        List<SysUser> suList = Arrays.asList(sysUser1);

        when( sysUserRepository.findByPaxId(PAX_ID)).thenReturn(suList);
        when( sysUserRepository.findOne(PAX_ID)).thenReturn(sysUser1);
        when( sysUserRepository.save(any(SysUser.class))).thenReturn(sysUser1);

        // Mock Phone default data
        Phone phone1 = new Phone();
        phone1.setPhoneId(TestConstants.ID_1);
        phone1.setPaxId(PAX_ID);
        phone1.setPhoneTypeCode(EmailType.HOME.getCode());
        List<Phone> phList = Arrays.asList(phone1);
        when( phoneRepository.findByPaxId(PAX_ID)).thenReturn(phList);

        //Mock Email default data
        Email email1 = new Email();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress("test@culturenext.com");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setPreferred("Y");
        List<Email> emailsList = Arrays.asList(email1);

        when(emailRepository.findByPaxId(PAX_ID)).thenReturn(emailsList);
        // home email not setup.
        when(emailRepository.findByPaxIdAndEmailTypeCode(PAX_ID, EmailType.HOME.getCode())).thenReturn(null);
        when(emailRepository.exists(anyLong())).thenReturn(true); //email exist
        when(emailRepository.findOne(anyLong())).thenReturn(email1);
        when(emailRepository.save(any(Email.class))).thenReturn(email1);

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.HIRE_DATE.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_REC.name());
        pmisc2.setMiscData(ProjectConstants.TRUE);

        PaxMisc pmisc3 = new PaxMisc();
        pmisc3.setPaxId(PAX_ID);
        pmisc3.setPaxMiscId(3L);
        pmisc3.setVfName(VfName.RECEIVE_EMAIL_REC.name());
        pmisc3.setMiscData(ProjectConstants.TRUE);

        PaxMisc pmisc4 = new PaxMisc();
        pmisc4.setPaxId(PAX_ID);
        pmisc4.setPaxMiscId(4L);
        pmisc4.setVfName(VfName.BIRTH_DAY.toString());
        pmisc4.setMiscData("01/01/1970");

        // Mock pax data
        Pax pax = new Pax();
        pax.setPaxId(PAX_ID);

        // Mock localeInfo data
        LocaleInfo localeInfoEnUs = new LocaleInfo();
        localeInfoEnUs.setLanguageCode("en");
        localeInfoEnUs.setCountryCode("US");
        localeInfoEnUs.setLocaleCode("en_US");
        localeInfoEnUs.setDisplayStatusTypeCode(StatusTypeCode.ACTIVE.name());

        LocaleInfo localeInfoNlNl = new LocaleInfo();
        localeInfoNlNl.setLanguageCode("nl");
        localeInfoNlNl.setCountryCode("NL");
        localeInfoNlNl.setLocaleCode("nl_NL");
        localeInfoNlNl.setDisplayStatusTypeCode(StatusTypeCode.INACTIVE.name());

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2, pmisc3, pmisc4);
        when(paxMiscRepository.findAll(anyLong())).thenReturn(paxMiscsList);
        when(paxMiscRepository.findAll(anyLong(), anyListOf(String.class))).thenReturn(paxMiscsList);
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(pmisc1);
        when(paxMiscRepository.save(any(PaxMisc.class))).thenReturn(pmisc1);

        when(paxDao.findBy()).thenReturn(paxFindBy);
        when(paxFindBy.where(anyString())).thenReturn(paxFindBy);
        when(paxFindBy.eq(PAX_ID)).thenReturn(paxFindBy);
        when(paxFindBy.findOne()).thenReturn(pax);

        when(localeInfoDao.findBy()).thenReturn(localeInfoFindBy);
        when(localeInfoFindBy.where("localeCode")).thenReturn(localeInfoFindBy);
        when(localeInfoFindBy.eq("en_US")).thenReturn(localeInfoFindBy);
        when(localeInfoFindBy.eq("nl_NL")).thenReturn(inactiveLocaleInfoFindBy);
        when(localeInfoFindBy.findOne()).thenReturn(localeInfoEnUs);
        when(localeInfoFindBy.and("displayStatusTypeCode")).thenReturn(localeInfoFindBy);
        when(localeInfoFindBy.eq(StatusTypeCode.ACTIVE.name())).thenReturn(localeInfoFindBy);
        when(localeInfoFindBy.findOne("localeCode", String.class)).thenReturn("en_US");
        when(localeInfoFindBy.eq("xx_US")).thenReturn(invalidLocaleInfoFindBy);
        when(localeInfoFindBy.eq("xx_XX")).thenReturn(invalidLocaleInfoFindBy);
        when(invalidLocaleInfoFindBy.and("displayTypeStatusCode")).thenReturn(invalidLocaleInfoFindBy);
        when(invalidLocaleInfoFindBy.eq(StatusTypeCode.ACTIVE.name()))
                .thenReturn(invalidLocaleInfoFindBy);
        when(invalidLocaleInfoFindBy.findOne("localeCode", String.class)).thenReturn(null);
        when(inactiveLocaleInfoFindBy.findOne()).thenReturn(null);

        //Mock Relationship default data
        Relationship rel1 = new Relationship();
        rel1.setId(TestConstants.ID_1);
        rel1.setPaxId1(PAX_ID);
        rel1.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        rel1.setPaxId2(MANAGER_PAX_ID);

        when(relationshipRepository.findBy()).thenReturn(relationshipFindBy);
        when(relationshipFindBy.where(ProjectConstants.RELATIONSHIP_TYPE_CODE))
                .thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(RelationshipType.REPORT_TO.toString()))
                .thenReturn(relationshipFindBy);
        when(relationshipFindBy.and(ProjectConstants.PAX_ID_1)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(PAX_ID)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.findOne()).thenReturn(rel1);


        //Mock security service default
        when(security.getPaxId()).thenReturn(PAX_ID);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);
        when(security.isMyPax(PAX_ID)).thenReturn(true); //logged user
        when(security.isNewUser()).thenReturn(false); //is not newUser

        //Mock SsoUtil default
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(Boolean.FALSE.toString()); //sso not enable

        //Mock ProfileIncompleteUtil service
        when(profileIncompleteService.isProfileIncomplete(PAX_ID)).thenReturn(false); //profile is completed

        //Mock AuthenticationService
        doNothing().when(authenticationService).reAuthenticate(anyString());

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();

        // Mock paxEventCommunicationRepository
        /*
          * Tuple4 result structure:
          * A = event communication id (primary key)
          * B = event type code (NOTIFY_OTHER etc)
          * C = communication type code (EMAIL or SMS)
          * D = status type code (as expected)
          */
        List<Tuple4<Long, String, String, String>> tupleList = new ArrayList<>();
        tupleList.add(new Tuple4<>(1L, "NOTIFY_OTHER", "EMAIL", "ACTIVE"));
        tupleList.add(new Tuple4<>(2L, "RECGPX", "EMAIL", "ACTIVE"));
        tupleList.add(new Tuple4<>(3L, "MILESTONE_REMINDER", "EMAIL", "ACTIVE"));

        when(paxEventCommunicationRepository.findParticipantEventCommunicationPreferences(Matchers.anyLong())).thenReturn(tupleList);
        when(paxEventCommunicationRepository.findParticipantEventCommunicationPreferences(Matchers.anyLong(), Matchers.anyListOf(String.class))).thenReturn(tupleList);
        when(paxEventCommunicationRepository.findByPaxIdAndEventCommunicationId(Matchers.anyLong(), Matchers.anyLong())).thenReturn(new PaxEventCommunication());

    }

    @Test
    public void test_get_detail_profile_info_logged_in_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(true); //logged user

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertThat(detailProfileInfo.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNotNull(detailProfileInfo.getAddresses());
        assertNotNull(detailProfileInfo.getSysUsers());
        assertNotNull(detailProfileInfo.getPhones());
        assertNotNull(detailProfileInfo.getEmails());
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }
    @Test
    public void test_get_detail_profile_info_admin_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString(),anyString())).thenReturn(true);  //admin user

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertThat(detailProfileInfo.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNotNull(detailProfileInfo.getAddresses());
        assertNotNull(detailProfileInfo.getSysUsers());
        assertNotNull(detailProfileInfo.getPhones());
        assertNotNull(detailProfileInfo.getEmails());
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_detail_profile_info_other_user_shareSA_false() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);  //admin user

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.HIRE_DATE.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_SA.name());
        pmisc2.setMiscData("FALSE");    //NOT SHARE HIRE DATE

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyListOf(String.class))).thenReturn(paxMiscsList);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertThat(detailProfileInfo.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNull(detailProfileInfo.getAddresses());
        assertNull(detailProfileInfo.getSysUsers());
        assertNull(detailProfileInfo.getPhones());
        assertNull(detailProfileInfo.getEmails());

        //validate Hire date is not shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }

        assertThat(hireDateFound, is(false));
    }

    @Test
    public void test_get_detail_profile_info_other_user_shareSA_true() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);  //admin user

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.HIRE_DATE.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_SA.name());
        pmisc2.setMiscData(ProjectConstants.TRUE);    //SHARE HIRE DATE

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyListOf(String.class))).thenReturn(paxMiscsList);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertThat(detailProfileInfo.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNull(detailProfileInfo.getAddresses());
        assertNull(detailProfileInfo.getSysUsers());
        assertNull(detailProfileInfo.getPhones());
        assertNull(detailProfileInfo.getEmails());
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_detail_profile_info_other_user_shareBday_false() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);  //admin user

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.BIRTH_DAY.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_BDAY.name());
        pmisc2.setMiscData("FALSE");    //NOT SHARE BIRTHDAY

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyListOf(String.class))).thenReturn(paxMiscsList);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertNull(detailProfileInfo.getAddresses());
        assertNull(detailProfileInfo.getSysUsers());
        assertNull(detailProfileInfo.getPhones());
        assertNull(detailProfileInfo.getEmails());

        //validate Birthday is not shown.
        boolean birthdayFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.BIRTH_DAY.toString())){
                birthdayFound = true;
                break;
            }
        }

        assertThat(birthdayFound, is(false));
    }

    @Test
    public void test_get_detail_profile_info_other_user_shareBday_true() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);  //admin user

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.BIRTH_DAY.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_BDAY.name());
        pmisc2.setMiscData(ProjectConstants.TRUE);    //SHARE HIRE DATE

        //Manager's PAX_ID_2 is null for some silly reason
        Relationship rel1 = new Relationship();
        rel1.setId(TestConstants.ID_1);
        rel1.setPaxId1(PAX_ID);
        rel1.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        rel1.setPaxId2(null);

        when(relationshipRepository.findBy()).thenReturn(relationshipFindBy);
        when(relationshipFindBy.where(ProjectConstants.RELATIONSHIP_TYPE_CODE))
        .thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(RelationshipType.REPORT_TO.toString()))
        .thenReturn(relationshipFindBy);
        when(relationshipFindBy.and(ProjectConstants.PAX_ID_1)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(PAX_ID)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.findOne()).thenReturn(rel1);

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyListOf(String.class))).thenReturn(paxMiscsList);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(PAX_ID);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertNull(detailProfileInfo.getAddresses());
        assertNull(detailProfileInfo.getSysUsers());
        assertNull(detailProfileInfo.getPhones());
        assertNull(detailProfileInfo.getEmails());
        //validate Birthday is shown.
        boolean birthdayFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.BIRTH_DAY.toString())){
                birthdayFound = true;
                break;
            }
        }
        assertTrue(birthdayFound);
    }

    @Test
    public void test_get_detail_profile_info_null_pax() throws Exception {
        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(null);

        assertNull(detailProfileInfo);
    }

    @Test
    public void test_get_detail_profile_info_no_pax_specified() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(true); //logged user

        //No manager
        when(relationshipRepository.findBy()).thenReturn(relationshipFindBy);
        when(relationshipFindBy.where(ProjectConstants.RELATIONSHIP_TYPE_CODE))
                .thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(RelationshipType.REPORT_TO.toString()))
                .thenReturn(relationshipFindBy);
        when(relationshipFindBy.and(ProjectConstants.PAX_ID_1)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.eq(PAX_ID)).thenReturn(relationshipFindBy);
        when(relationshipFindBy.findOne()).thenReturn(null);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfo();

        assertThat(detailProfileInfo.getPax().getPaxId(), is(PAX_ID));
        assertThat(detailProfileInfo.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNotNull(detailProfileInfo.getAddresses());
        assertNotNull(detailProfileInfo.getSysUsers());
        assertNotNull(detailProfileInfo.getPhones());
        assertNotNull(detailProfileInfo.getEmails());
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: detailProfileInfo.getPaxMiscs()){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_demographics_logged_in_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(true);  //logged user
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(PAX_ID);

        assertThat(profileDemographics.getPax().getPaxId(), is(PAX_ID));
        assertThat(profileDemographics.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNotNull(profileDemographics.getAddresses());
        assertNotNull(profileDemographics.getSysUsers());
        assertNotNull(profileDemographics.getPhones());
        assertNotNull(profileDemographics.getEmails());

    }

    @Test
    public void test_get_demographics_admin_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString(),anyString())).thenReturn(true);//admin

        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(PAX_ID);

        assertThat(profileDemographics.getPax().getPaxId(), is(PAX_ID));
        assertThat(profileDemographics.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNotNull(profileDemographics.getAddresses());
        assertNotNull(profileDemographics.getSysUsers());
        assertNotNull(profileDemographics.getPhones());
        assertNotNull(profileDemographics.getEmails());

    }

    @Test
    public void test_get_demographics_other_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(PAX_ID);

        assertThat(profileDemographics.getPax().getPaxId(), is(PAX_ID));
        assertThat(profileDemographics.getPax().getManagerPaxId(), is(MANAGER_PAX_ID));
        assertNull(profileDemographics.getAddresses());
        assertNull(profileDemographics.getSysUsers());
        assertNull(profileDemographics.getPhones());
        assertNull(profileDemographics.getEmails());
    }

    @Test
    public void test_get_demographics_null_pax() throws Exception {
        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(null);
        assertNull(profileDemographics);
    }

    @Test
    public void test_get_demographics_missing_data() throws Exception {

        when(security.isMyPax(TestConstants.ID_1)).thenReturn(true);
        when(addressRepository.findByPaxId(TestConstants.ID_1)).thenReturn(null);
        when(sysUserRepository.findByPaxId(TestConstants.ID_1)).thenReturn(null);
        when(phoneRepository.findByPaxId(TestConstants.ID_1)).thenReturn(null);
        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(TestConstants.ID_1);
        assertNull(profileDemographics.getAddresses());
        assertNull(profileDemographics.getSysUsers());
        assertNull(profileDemographics.getPhones());
    }

    @Test
    public void test_get_demographics_no_pax_specified() throws Exception {
        ProfileDemographics profileDemographics = dpService.getProfileDemographics();
        assertNotNull(profileDemographics);
    }


    @Test
    public void test_get_emails_logged_in_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(true);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        List<EmailDTO> emails =  dpService.getEmailsByPaxId(PAX_ID);

        if (emails.size() > 0) {
            assertThat(emails.get(0).getPaxId(), is(PAX_ID));
        }
    }

    @Test
    public void test_get_emails_admin_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(true);

        List<EmailDTO> emails =  dpService.getEmailsByPaxId(PAX_ID);

        if (emails.size() > 0) {
            assertThat(emails.get(0).getPaxId(), is(PAX_ID));
        }
    }

    @Test
    public void test_get_emails_other_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        List<EmailDTO> emails =  dpService.getEmailsByPaxId(PAX_ID);

        assertThat(emails.size(), is(0));
    }

    @Test
    public void test_get_emails_null_pax() throws Exception {
        List<EmailDTO> emails = dpService.getEmailsByPaxId(null);
        assertNull(emails);
    }

    @Test
    public void test_get_emails_empty() throws Exception {
        when(security.isMyPax(TestConstants.ID_1)).thenReturn(true);
        when(emailRepository.findByPaxId(TestConstants.ID_1)).thenReturn(null);

        List<EmailDTO> emails = dpService.getEmailsByPaxId(TestConstants.ID_1);
        assertTrue(emails.isEmpty());
    }

    @Test
    public void test_get_email_is_editable() throws Exception {
        when(security.getPaxId()).thenReturn(TestConstants.ID_1);
        when(security.isMyPax(TestConstants.ID_1)).thenReturn(true);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        //Mock Email data
        Email email1 = new Email();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress("test@culturenext.com");
        email1.setEmailTypeCode(EmailType.HOME.getCode());
        email1.setPreferred("Y");
        List<Email> emailsList = Arrays.asList(email1);

        when(emailRepository.findByPaxId(TestConstants.ID_1)).thenReturn(emailsList);

        List<EmailDTO> emails =  dpService.getEmails();
        assertTrue(emails.get(0).getEditable());
    }

    @Test
    public void test_get_privacy_settings_logged_in_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(true);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List<PaxMiscDTO> pm = paxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: pm){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_privacy_settings_admin_user() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(true);

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List<PaxMiscDTO> pm = paxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: pm){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_privacy_settings_other_user_shareSA_true() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.HIRE_DATE.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_SA.name());
        pmisc2.setMiscData(ProjectConstants.TRUE);    //SHARE HIRE DATE

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyList())).thenReturn(paxMiscsList);

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List<PaxMiscDTO> pm = paxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: pm){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertTrue(hireDateFound);
    }

    @Test
    public void test_get_privacy_settings_other_user_shareSA_false() throws Exception {
        //Mock security service default
        when(security.isMyPax(PAX_ID)).thenReturn(false);
        when(security.hasRole(anyString(),anyString())).thenReturn(false);

        //Mock Misc default data
        PaxMisc pmisc1 = new PaxMisc();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.HIRE_DATE.toString());
        pmisc1.setMiscData("12/12/2001");

        PaxMisc pmisc2 = new PaxMisc();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_SA.name());
        pmisc2.setMiscData("FALSE");    //SHARE HIRE DATE

        List<PaxMisc> paxMiscsList = Arrays.asList(pmisc1, pmisc2);
        when(paxMiscRepository.findAll(anyLong(), anyList())).thenReturn(paxMiscsList);

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List<PaxMiscDTO> pm = paxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
        //validate Hire date is shown.
        boolean hireDateFound = false;
        for(PaxMiscDTO paxMiscDTO: pm){
            if(paxMiscDTO.getVfName().equals(VfName.HIRE_DATE.toString())){
                hireDateFound = true;
                break;
            }
        }
        assertFalse(hireDateFound);
    }

    @Test
    public void test_get_recognition_preferences_logged_in_user() throws Exception {
        PaxMiscListDTO profileRecognitionPreferences = dpService.getProfilePreferences();
        List<PaxMiscDTO> pm = profileRecognitionPreferences.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
    }

    @Test
    public void test_get_recognition_preferences_by_pax_id() throws Exception {
        PaxMiscListDTO profileRecognitionPreferences = dpService.getProfilePreferencesByPaxId(PAX_ID);
        List<PaxMiscDTO> pm = profileRecognitionPreferences.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(PAX_ID));
        }
    }

    @Test
    public void test_insert_emails()  throws Exception {

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(VALID_EMAIL);
        emailDTOInsert.setPaxId(PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        dpService.insertEmail(PAX_ID, emailPasswordWrapper);

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if (emailDTO.getEmailAddress().equals(VALID_EMAIL)) {
                assertThat(emailDTO.getEmailAddress(), is(VALID_EMAIL));
                assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
                assertThat(emailDTO.getEditable(), is(Boolean.TRUE));
            }
        }
    }

    @Test
    public void test_insert_emails_sso_enabled()  throws Exception {

        //Enable SSO flag
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(VALID_EMAIL);
        emailDTOInsert.setPaxId(PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        //password not set for sso users
        emailPasswordWrapper.setPassword(null);

        dpService.insertEmail(PAX_ID, emailPasswordWrapper);

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if (emailDTO.getEmailAddress().equals(VALID_EMAIL)) {
                assertThat(emailDTO.getEmailAddress(), is(VALID_EMAIL));
                assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
                assertThat(emailDTO.getEditable(), is(Boolean.TRUE));
            }
        }
    }

    @Test
    public void test_insert_emails_invalid_email_no_at_sign()  throws Exception {
        String testEmailString = "invalidEmailAccount.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        Set<ConstraintViolation<EmailDTO>> constraintViolations = validator.validate(emailDTOInsert);

        if (constraintViolations.isEmpty()) {
            dpService.insertEmail(PAX_ID, emailPasswordWrapper);
            fail();
        }

    }

    @Test
    public void test_insert_emails_valid_email_no_period()  throws Exception {
        String testEmailString = "noPeriod@Emailcom";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.insertEmail(PAX_ID, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_insert_emails_invalid_pax_id()  throws Exception {
        String testEmailString = "test@Email.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(TestConstants.ID_1);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.insertEmail(PAX_ID, emailPasswordWrapper);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void test_insert_emails_null_password_after_first_time_login()  throws Exception {
        //first time user
        when(security.isNewUser()).thenReturn(false);

        String testEmailString = "test@Email.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword("");

        try {
            dpService.insertEmail(PAX_ID, emailPasswordWrapper);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_NOT_FIRST_TIME));
            }
        }
    }

    @Test
    public void testInsertEmailsMissingPaxId() {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.MISSING_PAX_ID);

        // set the paxId of the EmailDTOs to be missing
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = generateValidInsertEmailPasswordWrapperDTO();
        if(emailPasswordWrapperDTO.getEmails() != null) {
            for(EmailDTO emailDTO : emailPasswordWrapperDTO.getEmails()) {
                emailDTO.setPaxId(null);
            }
        }

        verifyInsertEmailsErrors(TEST_INSERT_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void test_update_emails() throws Exception {

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        dpService.updateEmails(PAX_ID, emailPasswordWrapper);

        emailDTOList = dpService.getEmailsByPaxId(PAX_ID);
        verify(emailRepository,times(1)).save(any(Email.class));
    }

    @Test
    public void test_update_emails_sso_enabled() throws Exception {
        //Enable SSO flag
        when(environment.getProperty(ApplicationDataConstants.KEY_NAME_SSO_ENABLED))
                .thenReturn(Boolean.TRUE.toString());

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        //password not set for sso users
        emailPasswordWrapper.setPassword(null);

        dpService.updateEmails(PAX_ID, emailPasswordWrapper);

        verify(emailRepository,times(1)).save(any(Email.class));
    }

    @Test
    public void test_update_emails_profile_incomplete() throws Exception {
        //Mock ProfileIncompleteUtil service
        when(profileIncompleteService.isProfileIncomplete(PAX_ID)).thenReturn(true); //profile incomplete

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(null);

        dpService.updateEmails(PAX_ID, emailPasswordWrapper);

        Mockito.verify(emailRepository,times(1)).save(any(Email.class));
    }

    @Test
    public void test_insert_emails_profile_incomplete() throws Exception {

        //Mock ProfileIncompleteUtil service
        when(profileIncompleteService.isProfileIncomplete(PAX_ID)).thenReturn(true); //profile incomplete

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(null);

        dpService.insertEmail(PAX_ID, emailPasswordWrapper);

        Mockito.verify(emailRepository,times(1)).save(any(Email.class));
    }

    @Test
    public void test_update_emails_null_password_after_first_time_login() throws Exception {
        //first time user
        when(security.isNewUser()).thenReturn(false);

        String validPassword = "";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(validPassword);

        try {
            dpService.updateEmails(PAX_ID, emailPasswordWrapper);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_NOT_FIRST_TIME));
            }
        }

    }

    @Test
    public void test_update_emails_invalid_email_no_andpersand() throws Exception {

        String testEmailString = "noAndpersandTest.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(testEmailString);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

         Set<ConstraintViolation<EmailDTO>> constraintViolations = validator.validate(email1);

         if (constraintViolations.isEmpty()) {
             dpService.insertEmail(PAX_ID, emailPasswordWrapper);
             fail();
        }
    }

    @Test
    public void test_update_emails_valid_email_no_period() throws Exception {

        String testEmailString = "noPeriod@Testcom";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(testEmailString);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.updateEmails(PAX_ID, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_update_emails_invalid_pax_id() throws Exception {

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(TestConstants.ID_1); //invalid value
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.updateEmails(PAX_ID, emailPasswordWrapper);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void testUpdateEmailsMissingPaxId() {

        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.MISSING_PAX_ID);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(null); //invalid value
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsMissingEmailId() {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.MISSING_EMAIL_ID);

        // setup DTO using existing emails (but set emailId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(null); //invalid value
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsIncorrectEmailId() {
        //Mock email search to update.
        Email emailFind = new Email();
        emailFind.setPaxId(TestConstants.ID_1); //another pax id.
        emailFind.setEmailTypeCode(EmailType.BUSINESS.getCode());
        when(emailRepository.findOne(anyLong())).thenReturn(emailFind);

        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INCORRECT_EMAIL_ID);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);


        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsMissingEmailTypeCode() {

        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.MISSING_EMAIL_TYPE_CODE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode(null); //invalid value
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsInvalidEmailTypeCode() {

        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_EMAIL_TYPE_CODE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("Y");
        email1.setEmailTypeCode("INV"); //invalid value
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsInvalidPreferredValue() {

        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_PREFERRED_VALUE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        EmailDTO email1 = new EmailDTO();
        email1.setEmailId(TestConstants.ID_1);
        email1.setPaxId(PAX_ID);
        email1.setEmailAddress(VALID_EMAIL);
        email1.setPreferred("F"); //invalid value
        email1.setEmailTypeCode(EmailType.BUSINESS.getCode());
        email1.setEditable(true);

        List <EmailDTO> emailDTOList = Arrays.asList(email1);

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void test_update_profile_privacy() throws Exception {
        PaxMiscDTO pmisc1 = new PaxMiscDTO();
        pmisc1.setPaxId(PAX_ID);
        pmisc1.setPaxMiscId(TestConstants.ID_1);
        pmisc1.setVfName(VfName.SHARE_REC.name());
        pmisc1.setMiscData(ProjectConstants.TRUE);

        PaxMiscDTO pmisc2 = new PaxMiscDTO();
        pmisc2.setPaxId(PAX_ID);
        pmisc2.setPaxMiscId(TestConstants.ID_2);
        pmisc2.setVfName(VfName.SHARE_REC.name());
        pmisc2.setMiscData(ProjectConstants.TRUE);

        PaxMiscDTO pmisc3 = new PaxMiscDTO();
        pmisc3.setPaxId(PAX_ID);
        pmisc3.setPaxMiscId(3L);
        pmisc3.setVfName(VfName.RECEIVE_EMAIL_REC.name());
        pmisc3.setMiscData(ProjectConstants.TRUE);

        PaxMiscDTO pmisc4 = new PaxMiscDTO();
        pmisc4.setPaxId(PAX_ID);
        pmisc4.setPaxMiscId(4L);
        pmisc4.setVfName(VfName.RECEIVE_EMAIL_NOTIFY_OTHERS.name());
        pmisc4.setMiscData(ProjectConstants.TRUE);

        PaxMiscDTO pmisc5 = new PaxMiscDTO();
        pmisc5.setPaxId(PAX_ID);
        pmisc5.setPaxMiscId(5L);
        pmisc5.setVfName(VfName.RECEIVE_EMAIL_MILESTONE_REMINDER.name());
        pmisc5.setMiscData(ProjectConstants.TRUE);

        List<PaxMiscDTO> paxMiscDTOList = Arrays.asList(pmisc1, pmisc2, pmisc3, pmisc4, pmisc5);

        dpService.updateProfilePrivacy(PAX_ID, paxMiscDTOList);

        Mockito.verify(paxMiscRepository, times(1)).save(anyListOf(PaxMisc.class));
    }

    @Test
    public void test_update_profile_privacy_bad_pax_id() throws Exception {

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(VfName.SHARE_REC.name());
            paxMiscDTO.setPaxId(TestConstants.ID_1);
        }

        try {
            dpService.updateProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void test_update_profile_privacy_bad_vfname() throws Exception {

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(BAD_VFNAME_TEST);
        }

        try {
            dpService.updateProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    @Test
    public void test_update_profile_privacy_bad_misc_data() throws Exception {

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(PAX_ID);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData("BAD");
            paxMiscDTO.setVfName(VfName.SHARE_REC.name());
        }

        try {
            dpService.updateProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_privacy() throws Exception {
        //new records.
        when(paxMiscRepository.findOne(anyLong(),anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.SHARE_REC.name(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.SHARE_SA.name(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.RECEIVE_EMAIL_REC.name(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.RECEIVE_EMAIL_NOTIFY_OTHERS.name(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.RECEIVE_EMAIL_MILESTONE_REMINDER.name(), ProjectConstants.TRUE));

        dpService.insertProfilePrivacy(PAX_ID, paxMiscDTOList);

        Mockito.verify(paxMiscRepository, times(1)).save(anyListOf(PaxMisc.class));
        Mockito.verify(paxEventCommunicationRepository, times(1)).save(anyListOf(PaxEventCommunication.class));
    }

    @Test
    public void test_insert_profile_privacy_bad_vfname() throws Exception {
        //new misc data record
        when(paxMiscRepository.findOne(anyLong(),anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, BAD_VFNAME_TEST, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    public PaxMiscDTO setPaxMiscDTO(Long paxID, String vfName, String miscData) {
        PaxMiscDTO paxMiscDTO = new PaxMiscDTO();
        paxMiscDTO.setPaxId(paxID);
        paxMiscDTO.setVfName(vfName);
        paxMiscDTO.setMiscData(miscData);
        return paxMiscDTO;
    }

    @Test
    public void test_insert_profile_privacy_bad_misc_data() throws Exception {
        //new misc data record
        when(paxMiscRepository.findOne(anyLong(),anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.SHARE_REC.name(), BAD_MISC_DATA_TEST));

        try {
            dpService.insertProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_privacy_bad_pax_id() throws Exception {
        //new misc data record
        when(paxMiscRepository.findOne(anyLong(),anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(new Long(1), VfName.SHARE_REC.name(), ProjectConstants.TRUE));

        try {
            dpService.insertProfilePrivacy(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test(expected = ErrorMessageException.class)
    public void test_insert_profile_privacy_proxy_no_permission() {
        Long myPaxId = security.getPaxId();
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();

        when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_1);
        when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.FALSE);

        dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
    }

    @Test
    public void test_insert_profile_privacy_proxy_with_permission() {
        Long myPaxId = security.getPaxId();
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(null);
        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.SHARE_SA.toString(), ProjectConstants.TRUE));

        when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_9);
        when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.TRUE);

        dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
    }

    @Test
    public void test_update_profile_preferences() throws Exception {

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_3.toString(), ProjectConstants.TRUE));

        dpService.updateProfilePreferences(PAX_ID, paxMiscDTOList);
         Mockito.verify(paxMiscRepository, times(5)).save(any(PaxMisc.class));
    }

    @Test
    public void test_update_profile_preferences_bad_vfname() throws Exception {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_PARAMETER);
        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, BAD_VFNAME_TEST, ProjectConstants.TRUE));

        verifyUpdateProfilePreferencesErrors(PAX_ID, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void test_update_profile_preferences_bad_misc_data() throws Exception {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_DATA);
        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), BAD_MISC_DATA_TEST));

        verifyUpdateProfilePreferencesErrors(PAX_ID, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void test_update_profile_preferences_bad_pax_id() throws Exception {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_PAX_ID);
        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.WRITTEN_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.WRITTEN_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.VERBAL_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.VERBAL_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.VERBAL_REC_3.toString(), ProjectConstants.TRUE));

        verifyUpdateProfilePreferencesErrors(PAX_ID, paxMiscDTOList, expectedErrorCodes);

    }

    @Test
    public void testUpdateProfilePreferencesMissingVfName() {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_PARAMETER);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID,null,ProjectConstants.TRUE));

        verifyUpdateProfilePreferencesErrors(PAX_ID, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void testUpdateProfilePreferencesMissingMiscData() {
        List<String> expectedErrorCodes = Arrays.asList(ProfileConstants.INVALID_DATA);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), null));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_2.toString(), null));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_1.toString(), null));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_2.toString(), null));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_3.toString(), null));


        verifyUpdateProfilePreferencesErrors(PAX_ID, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void test_insert_profile_preferences() throws Exception {
        //new pax misc data, it doesn't exist
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_1.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_2.toString(), ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.VERBAL_REC_3.toString(), ProjectConstants.TRUE));

        dpService.insertProfilePreferences(PAX_ID, paxMiscDTOList);

        Mockito.verify(paxMiscRepository,times(5)).save(any(PaxMisc.class));
    }

    @Test
    public void test_insert_profile_preferences_bad_vfname() throws Exception {
        //new pax misc data, it doesn't exist
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, BAD_VFNAME_TEST, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePreferences(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    @Test
    public void test_insert_profile_preferences_bad_misc_data() throws Exception {
        //new pax misc data, it doesn't exist
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), BAD_MISC_DATA_TEST));

        try {
            dpService.insertProfilePreferences(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_preferences_bad_pax_id() throws Exception {
        //new pax misc data, it doesn't exist
        when(paxMiscRepository.findOne(anyLong(), anyString())).thenReturn(null);

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(TestConstants.ID_1, VfName.WRITTEN_REC_1.toString(), ProjectConstants.TRUE));

        try {
            dpService.insertProfilePreferences(PAX_ID, paxMiscDTOList);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_null_object() {
        dpService.updatePaxLanguageAndLocale(PAX_ID, null);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_null_pax_id() {
        LocaleDTO testData = new LocaleDTO();
        dpService.updatePaxLanguageAndLocale(null, testData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_null_dto_contents() {
        LocaleDTO testData = new LocaleDTO();
        dpService.updatePaxLanguageAndLocale(PAX_ID, testData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_invalid_preferred_locale() {

        LocaleDTO testData = new LocaleDTO();
        testData.setLanguageCode("not a locale");

        dpService.updatePaxLanguageAndLocale(PAX_ID, testData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_fake_language() {
        LocaleDTO testData = new LocaleDTO();
        testData.setLanguageCode("xx_US");

        dpService.updatePaxLanguageAndLocale(PAX_ID, testData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_fake_locale() {
        LocaleDTO testData = new LocaleDTO();
        testData.setLanguageCode("xx_XX");

        dpService.updatePaxLanguageAndLocale(PAX_ID, testData);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_profile_privacy_proxy_no_permission() {
        Long myPaxId = security.getPaxId();
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();

        when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_1);
        when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.FALSE);

        dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
    }

    @Test
    public void test_update_profile_privacy_proxy_with_permission() {
        Long myPaxId = security.getPaxId();
        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();
        paxMiscDTOList.add(setPaxMiscDTO(PAX_ID, VfName.WRITTEN_REC_1.toString(), ProjectConstants.TRUE));

        when(security.isImpersonated()).thenReturn(Boolean.TRUE);
        when(security.getImpersonatorPaxId()).thenReturn(TestConstants.ID_3);
        when(proxyService.doesPaxHaveProxyPermission(anyString(), anyLong(), anyLong()))
                .thenReturn(Boolean.TRUE);

        dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
    }

    @Test(expected = ErrorMessageException.class)
    public void test_update_pax_language_and_locale_inactive_locale() {
        LocaleDTO testData = new LocaleDTO();
        testData.setLanguageCode("nl_NL");

        dpService.updatePaxLanguageAndLocale(PAX_ID, testData);
    }

    @Test
    public void test_update_pax_language_and_locale_happy_path() {
        LocaleDTO testData = new LocaleDTO();
        testData.setLanguageCode("en_US");

        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Pax pax = (Pax)args[0];
                assertThat(pax.getLanguageCode(), is("en"));
                assertThat(pax.getPreferredLocale(), is("en_US"));
                return null;
            }
        }).when(paxDao).save(any(Pax.class));

        LocaleDTO response = dpService.updatePaxLanguageAndLocale(PAX_ID, testData);

        Mockito.verify(paxDao, times(1)).save(any(Pax.class));
        assertThat(response.getLanguageCode(), is("en_US"));
    }

    private void verifyUpdateProfilePreferencesErrors(Long paxId,
            List<PaxMiscDTO> paxMiscDTOs,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.updateProfilePreferences(paxId, paxMiscDTOs);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedErrorCodes, errorMsgException);
        }
    }

    private void verifyInsertEmailsErrors(Long paxId,
            EmailPasswordWrapperDTO emailPasswordWrapperDTO,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.insertEmail(paxId, emailPasswordWrapperDTO);
            fail();
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        } catch (Exception e) {
            fail();
        }
    }

    private void verifyUpdateEmailsErrors(Long paxId,
            EmailPasswordWrapperDTO emailPasswordWrapperDTO,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.updateEmails(paxId, emailPasswordWrapperDTO);
            fail();
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        } catch (Exception e) {
            fail();
        }
    }

    @Nonnull
    private EmailPasswordWrapperDTO generateValidInsertEmailPasswordWrapperDTO() {
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(VALID_EMAIL);
        emailDTOInsert.setPaxId(TEST_INSERT_EMAIL_PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapperDTO.setEmails(insertEmailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        return emailPasswordWrapperDTO;
    }

}
