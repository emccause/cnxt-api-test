package com.maritz.culturenext.profile.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.TestUtil;
import com.maritz.core.dto.ApplicationDataDTO;
import com.maritz.core.rest.ErrorMessage;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.core.services.ApplicationDataService;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.EmailType;
import com.maritz.culturenext.profile.constants.ProfileConstants;
import com.maritz.culturenext.profile.dto.EmailDTO;
import com.maritz.culturenext.profile.dto.EmailPasswordWrapperDTO;
import com.maritz.culturenext.profile.dto.PaxMiscDTO;
import com.maritz.culturenext.profile.dto.aggregate.DetailProfileInfo;
import com.maritz.culturenext.profile.dto.aggregate.PaxMiscListDTO;
import com.maritz.culturenext.profile.dto.aggregate.ProfileDemographics;
import com.maritz.test.AbstractDatabaseTest;

public class DetailProfileServiceTest extends AbstractDatabaseTest {

    @Inject private ApplicationDataService applicationDataService;
    @Inject private AuthenticationService authenticationService;
    @Inject private DetailProfileService dpService;
    @Inject private Security security;

    private Long paxId = new Long(0);
    private static final String SHARE_REC = "SHARE_REC";
    private static final String WRITTEN_REC_1 = "WRITTEN_REC_1";
    private static final String WRITTEN_REC_2 = "WRITTEN_REC_2";
    private static final String VERBAL_REC_1 = "VERBAL_REC_1";
    private static final String VERBAL_REC_2 = "VERBAL_REC_2";
    private static final String VERBAL_REC_3 = "VERBAL_REC_3";
    private static final String SHARE_SA = "SHARE_SA";
    private static final String RECEIVE_EMAIL_REC = "RECEIVE_EMAIL_REC";
    private static final String BAD_VFNAME_TEST = "BAD_VFNAME_TEST";
    private static final String BAD_MISC_DATA_TEST = "BAD_MISC_DATA_TEST";

    // user information
    private static final Long TEST_INSERT_EMAIL_PAX_ID = 5564L;
    private static final Long TEST_UPDATE_EMAIL_PAX_ID = 13021L;

    // emails
    private static final String VALID_EMAIL = "junitTest@junit.com";

    // error codes
    private static final String MISSING_PAX_ID = "MISSING_PAX_ID";
    private static final String MISSING_EMAIL_ID = "MISSING_EMAIL_ID";
    private static final String MISSING_EMAIL_TYPE_CODE = "MISSING_EMAIL_TYPE_CODE";
    private static final String INVALID_EMAIL_TYPE_CODE = "INVALID_EMAIL_TYPE_CODE";
    private static final String INVALID_PREFERRED_VALUE = "INVALID_PREFERRED_VALUE";
    private static final String INCORRECT_EMAIL_ID = "INCORRECT_EMAIL_ID";

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        paxId = security.getPaxId();

        enableSSOFlag(false);
    }

    @Test
    public void test_get_detail_profile_info_logged_in_user() throws Exception {
        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfo();

        assertThat(detailProfileInfo.getPax().getPaxId(), is(paxId));
    }

    @Test
    public void test_get_demographics_logged_in_user() throws Exception {
        ProfileDemographics profileDemographics = dpService.getProfileDemographics();

        assertThat(profileDemographics.getPax().getPaxId(), is(paxId));
    }

    @Test
    public void test_get_emails_logged_in_user() throws Exception {
        List<EmailDTO> emails = dpService.getEmails();

        if (emails.size() > 0) {
            assertThat(emails.get(0).getPaxId(), is(paxId));
        }
    }

    @Test
    public void test_get_other_pax_demographics_pulsem_user() throws Exception {
        authenticationService.authenticate(TestConstants.USER_PULSEM);
        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(TestConstants.PAX_KUMARSJ);

        assertNotNull(profileDemographics.getAddresses());
        assertNotNull(profileDemographics.getSysUsers());
        assertNotNull(profileDemographics.getPhones());
        assertNotNull(profileDemographics.getEmails());

    }

    @Test
    public void test_get_other_pax_emails_pulsem_user() throws Exception {
        authenticationService.authenticate(TestConstants.USER_PULSEM);
        List<EmailDTO> emails = dpService.getEmailsByPaxId(TestConstants.PAX_KUMARSJ);
        assertThat(emails.size(), is(new Integer(1)));

    }

    @Test
    public void test_get_privacy_settings_logged_in_user() throws Exception {
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacy();
        List<PaxMiscDTO> pm = PaxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(paxId));
        }
    }

    @Test
    public void test_get_recognition_preferences_logged_in_user() throws Exception {
        PaxMiscListDTO profileRecognitionPreferences =  dpService.getProfilePreferences();
        List<PaxMiscDTO> pm = profileRecognitionPreferences.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(paxId));
        }
    }

    @Test
    public void test_get_detail_profile_info_by_pax_id() throws Exception {
        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfoByPaxId(TestConstants.PAX_KUMARSJ);

        assertThat(detailProfileInfo.getPax().getPaxId(), is(TestConstants.PAX_KUMARSJ));
    }

    @Test
    public void test_get_demographics_by_pax_id() throws Exception {
        ProfileDemographics profileDemographics = dpService.getProfileDemographicsByPaxId(TestConstants.PAX_KUMARSJ);

        assertThat(profileDemographics.getPax().getPaxId(), is(TestConstants.PAX_KUMARSJ));
    }

    @Test
    public void test_get_emails_by_pax_id() throws Exception {
        List<EmailDTO> emails = dpService.getEmailsByPaxId(TestConstants.PAX_KUMARSJ);

        if (emails.size() > 0) {
            assertThat(emails.get(0).getPaxId(), is(TestConstants.PAX_KUMARSJ));
        }
    }

    @Test
    public void test_get_privacy_settings_by_pax_id() throws Exception {
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(TestConstants.PAX_KUMARSJ);
        List<PaxMiscDTO> pm = PaxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(TestConstants.PAX_KUMARSJ));
        }
    }


    @Test
    public void update_privacy_settings_by_pax_id() throws Exception {
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(TestConstants.PAX_KUMARSJ);
        List<PaxMiscDTO> pm = PaxMiscListDTO.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(TestConstants.PAX_KUMARSJ));
        }
    }




    @Test
    public void test_get_recognition_preferences_by_pax_id() throws Exception {
        PaxMiscListDTO profileRecognitionPreferences
        = dpService.getProfilePreferencesByPaxId(TestConstants.PAX_KUMARSJ);
        List<PaxMiscDTO> pm = profileRecognitionPreferences.getPaxMiscs();

        if (pm.size() > 0) {
            assertThat(pm.get(0).getPaxId(), is(TestConstants.PAX_KUMARSJ));
        }
    }

    @Test
    public void test_insert_emails()  throws Exception {
        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        //If this is failing, make sure user abbottm has no emails causing a constraint violation
        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(myPaxId);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        dpService.insertEmail(myPaxId, emailPasswordWrapper);

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            if (emailDTO.getEmailAddress().equals(testEmailString)) {
                assertThat(emailDTO.getEmailAddress(), is(testEmailString));
                assertThat(emailDTO.getPreferred(), is(preferred));
                assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
                assertThat(emailDTO.getEditable(), is(isEditable));
            }
        }
    }
    @Test
    public void test_insert_emails_sso_enabled()  throws Exception {
        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        // Enable SSO flag
        enableSSOFlag(true);

        //If this is failing, make sure user abbottm has no emails causing a constraint violation
        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(myPaxId);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        //password not set for sso users
        emailPasswordWrapper.setPassword(null);

        dpService.insertEmail(myPaxId, emailPasswordWrapper);

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            if (emailDTO.getEmailAddress().equals(testEmailString)) {
                assertThat(emailDTO.getEmailAddress(), is(testEmailString));
                assertThat(emailDTO.getPreferred(), is(preferred));
                assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
                assertThat(emailDTO.getEditable(), is(isEditable));
            }
        }
    }

    //      @Test
    //      public void test_insert_emails_invalid_password()  throws Exception {
    //          authenticationService.authenticate("abbottm");
    //          Long myPaxId = authenticationService.getPaxId();
    //          String testEmailString = "junitTest@junit.com";
    //
    //          EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
    //          EmailDTO emailDTOInsert = new EmailDTO();
    //          emailDTOInsert.setEmailAddress(testEmailString);
    //          emailDTOInsert.setPaxId(myPaxId);
    //
    //          List <EmailDTO> insertEmailDTOList = new ArrayList();
    //          insertEmailDTOList.add(emailDTOInsert);
    //
    //          emailPasswordWrapper.setEmails(insertEmailDTOList);
    //          emailPasswordWrapper.setPassword("invalid_password");
    //
    //          ResponseEntity<Object> responseEntity = dpService.insertEmail(myPaxId, emailPasswordWrapper);
    //          assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    //      }

    @Test
    public void test_insert_emails_invalid_email_no_andpersand()  throws Exception {
        String testEmailString = "noAndpersandEmail.com";

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();
        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(myPaxId);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.insertEmail(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_insert_emails_invalid_email_no_period()  throws Exception {
        String testEmailString = "noPeriod@Emailcom";

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();
        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(myPaxId);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.insertEmail(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_insert_emails_invalid_pax_id()  throws Exception {
        String testEmailString = "test@Email.com";

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();
        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(TestConstants.ID_1);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.insertEmail(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }


    @Test
    public void test_insert_emails_null_password_after_first_time_login()  throws Exception {
        String testEmailString = "test@Email.com";


        authenticationService.authenticate(TestConstants.USER_ABBOTTM);

        Long myPaxId = security.getPaxId();

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(testEmailString);
        emailDTOInsert.setPaxId(myPaxId);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapper.setEmails(insertEmailDTOList);
        emailPasswordWrapper.setPassword("");

        try {
            dpService.insertEmail(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_NOT_FIRST_TIME));
            }
        }
    }


    @Test
    public void testInsertEmailsMissingPaxId() {
        authenticateUser(TestConstants.USER_ABBOTTM);

        List<String> expectedErrorCodes = Arrays.asList(MISSING_PAX_ID);

        // set the paxId of the EmailDTOs to be missing
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = generateValidInsertEmailPasswordWrapperDTO();
        if(emailPasswordWrapperDTO.getEmails() != null) {
            for(EmailDTO emailDTO : emailPasswordWrapperDTO.getEmails()) {
                emailDTO.setPaxId(null);
            }
        }

        verifyInsertEmailsErrors(TEST_INSERT_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void test_update_emails() throws Exception {

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();

        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
            emailDTO.setPreferred(preferred);
            emailDTO.setEmailTypeCode(EmailType.HOME.getCode());
            emailDTO.setEditable(isEditable);
        }

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        dpService.updateEmails(myPaxId, emailPasswordWrapper);

        emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {

            assertThat(emailDTO.getEmailAddress(), is(testEmailString));
            assertThat(emailDTO.getPreferred(), is(preferred));
            assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
            assertThat(emailDTO.getEditable(), is(isEditable));

        }
    }

    @Test
    public void test_update_emails_sso_enabled() throws Exception {

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();

        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        //Enable SSO flag
        enableSSOFlag(true);

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
            emailDTO.setPreferred(preferred);
            emailDTO.setEmailTypeCode(EmailType.HOME.getCode());
            emailDTO.setEditable(isEditable);
        }

        emailPasswordWrapper.setEmails(emailDTOList);
        //password not set for sso users
        emailPasswordWrapper.setPassword(null);

        dpService.updateEmails(myPaxId, emailPasswordWrapper);

        emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {

            assertThat(emailDTO.getEmailAddress(), is(testEmailString));
            assertThat(emailDTO.getPreferred(), is(preferred));
            assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
            assertThat(emailDTO.getEditable(), is(isEditable));

        }
    }

    @Test
    public void test_update_emails_profile_incomplete() throws Exception {

        authenticationService.authenticate(TestConstants.USER_CARRAM);
        Long myPaxId = security.getPaxId();

        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
            emailDTO.setPreferred(preferred);
            emailDTO.setEmailTypeCode(EmailType.HOME.getCode());
            emailDTO.setEditable(isEditable);
        }

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(null);

        dpService.updateEmails(myPaxId, emailPasswordWrapper);

        emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {

            assertThat(emailDTO.getEmailAddress(), is(testEmailString));
            assertThat(emailDTO.getPreferred(), is(preferred));
            assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
            assertThat(emailDTO.getEditable(), is(isEditable));

        }
    }
    @Test
    public void test_insert_emails_profile_incomplete() throws Exception {

        authenticationService.authenticate(TestConstants.USER_CARRAM);
        Long myPaxId = security.getPaxId();

        String testEmailString = "junitTest@junit.com";
        String preferred = "N";
        boolean isEditable = true;

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
            emailDTO.setPreferred(preferred);
            emailDTO.setEmailTypeCode(EmailType.HOME.getCode());
            emailDTO.setEditable(isEditable);
        }

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(null);

        dpService.insertEmail(myPaxId, emailPasswordWrapper);

        emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {

            assertThat(emailDTO.getEmailAddress(), is(testEmailString));
            assertThat(emailDTO.getPreferred(), is(preferred));
            assertThat(emailDTO.getEmailTypeCode(), is(EmailType.HOME.getCode()));
            assertThat(emailDTO.getEditable(), is(isEditable));

        }
    }
    @Test
    public void test_update_emails_null_password_after_first_time_login() throws Exception {

        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();
        String validPassword = "";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);

        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(validPassword);

        try {
            dpService.updateEmails(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_NOT_FIRST_TIME));
            }
        }

    }
    //
    //      @Test
    //      public void test_update_emails_invalid_password_after_first_time_login() throws Exception {
    //
    //          authenticationService.authenticate("abbottm");
    //          Long myPaxId = authenticationService.getPaxId();
    //          String validPassword = INVALID_PASSWORD_TEST;
    //
    //          EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();
    //          List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
    //
    //          emailPasswordWrapper.setEmails(emailDTOList);
    //          emailPasswordWrapper.setPassword(validPassword);
    //
    //          ResponseEntity<Object> responseEntity = dpService.updateEmails(myPaxId, emailPasswordWrapper);
    //          assertThat(responseEntity.getStatusCode(), is(HttpStatus.UNAUTHORIZED));
    //      }

    @Test
    public void test_update_emails_invalid_email_no_andpersand() throws Exception {

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();

        String testEmailString = "noAndpersandTest.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
        }
        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.updateEmails(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_update_emails_invalid_email_no_period() throws Exception {

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();

        String testEmailString = "noPeriod@Testcom";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
        }
        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.updateEmails(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_EMAIL));
            }
        }
    }

    @Test
    public void test_update_emails_invalid_pax_id() throws Exception {

        authenticationService.authenticate(TestConstants.USER_MENDELLL);
        Long myPaxId = security.getPaxId();

        String testEmailString = "test@Test.com";

        EmailPasswordWrapperDTO emailPasswordWrapper = new EmailPasswordWrapperDTO();

        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(myPaxId);
        for (EmailDTO emailDTO : emailDTOList) {
            emailDTO.setEmailAddress(testEmailString);
            emailDTO.setPaxId(TestConstants.ID_1);
        }
        emailPasswordWrapper.setEmails(emailDTOList);
        emailPasswordWrapper.setPassword(TestConstants.DEFAULT_PASSWORD);

        try {
            dpService.updateEmails(myPaxId, emailPasswordWrapper);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void testUpdateEmailsMissingPaxId() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(MISSING_PAX_ID);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setPaxId(null);
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsMissingEmailId() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(MISSING_EMAIL_ID);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setEmailId(null);
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsIncorrectEmailId() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(INCORRECT_EMAIL_ID);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setEmailId(343L);
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsMissingEmailTypeCode() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(MISSING_EMAIL_TYPE_CODE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setEmailTypeCode(null);
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsInvalidEmailTypeCode() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(INVALID_EMAIL_TYPE_CODE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setEmailTypeCode("INV");
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void testUpdateEmailsInvalidPreferredValue() {
        authenticateUser(TestConstants.USER_HANDERJ);

        List<String> expectedErrorCodes = Arrays.asList(INVALID_PREFERRED_VALUE);

        // setup DTO using existing emails (but set paxId to be missing)
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();
        List <EmailDTO> emailDTOList = dpService.getEmailsByPaxId(TEST_UPDATE_EMAIL_PAX_ID);
        for (EmailDTO emailDTO : emailDTOList) {
            if(EmailType.HOME.getCode().equals(emailDTO.getEmailTypeCode())) {
                emailDTO.setPreferred("F");
            }
        }

        emailPasswordWrapperDTO.setEmails(emailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        verifyUpdateEmailsErrors(TEST_UPDATE_EMAIL_PAX_ID, emailPasswordWrapperDTO, expectedErrorCodes);
    }

    @Test
    public void updateProfilePrivacy_getProfilePrivacyByPaxId() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = paxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.FALSE);
        }

        dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
        PaxMiscListDTO paxMiscListUpdated = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscValidationDTOList = paxMiscListUpdated.getPaxMiscs();

        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            boolean found = false;
            for (PaxMiscDTO paxMiscValidationDTO : paxMiscValidationDTOList) {
                if (paxMiscDTO.getVfName().equals(paxMiscValidationDTO.getVfName())) {
                    assertEquals(paxMiscDTO.getMiscData().toUpperCase(), paxMiscValidationDTO.getMiscData().toUpperCase());
                    found = true;
                }
            }
            assertTrue(found);
        }
    }

    @Test
    public void updateProfilePrivacy_getDetailProfileInfo() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO paxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = paxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.FALSE);
        }

        dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);

        DetailProfileInfo detailProfileInfo = dpService.getDetailProfileInfo();
        List<PaxMiscDTO> paxMiscValidationDTOList = detailProfileInfo.getPaxMiscs();

        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            boolean found = false;
            for (PaxMiscDTO paxMiscValidationDTO : paxMiscValidationDTOList) {
                if (paxMiscDTO.getVfName().equals(paxMiscValidationDTO.getVfName())) {
                    assertThat(paxMiscDTO.getMiscData().toUpperCase(), is(paxMiscValidationDTO.getMiscData().toUpperCase()));
                    found = true;
                }
            }
            assertThat(found, is(true));
        }
    }

    @Test
    public void test_update_profile_privacy_bad_pax_id() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(SHARE_REC);
            paxMiscDTO.setPaxId(TestConstants.ID_1);
        }

        try {
            dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void test_update_profile_privacy_bad_vfname() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(BAD_VFNAME_TEST);
        }

        try {
            dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    @Test
    public void test_update_profile_privacy_bad_misc_data() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData("BAD");
            paxMiscDTO.setVfName(SHARE_REC);
        }

        try {
            dpService.updateProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_privacy() throws Exception {

        authenticationService.authenticate(TestConstants.USER_PULSEM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, SHARE_REC, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, SHARE_SA, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, RECEIVE_EMAIL_REC, ProjectConstants.TRUE));

        dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
        PaxMiscListDTO paxMiscListUpdated = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscValidationDTOList = paxMiscListUpdated.getPaxMiscs();

        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            boolean found = false;
            for (PaxMiscDTO paxMiscValidationDTO : paxMiscValidationDTOList) {
                if (paxMiscDTO.getVfName().equals(paxMiscValidationDTO.getVfName())) {
                    assertEquals(paxMiscDTO.getMiscData().toUpperCase(), paxMiscValidationDTO.getMiscData().toUpperCase());
                    found = true;
                }
            }
            assertTrue(found);
        }
    }

    @Test
    public void test_insert_profile_privacy_bad_vfname() throws Exception {

        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, BAD_VFNAME_TEST, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    public PaxMiscDTO setPaxMiscDTO(Long paxID, String vfName, String miscData) {
        PaxMiscDTO paxMiscDTO = new PaxMiscDTO();
        paxMiscDTO.setPaxId(paxID);
        paxMiscDTO.setVfName(vfName);
        paxMiscDTO.setMiscData(miscData);
        return paxMiscDTO;
    }

    @Test
    public void test_insert_profile_privacy_bad_misc_data() throws Exception {
        authenticationService.authenticate(TestConstants.USER_PULSEM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, SHARE_REC, BAD_MISC_DATA_TEST));

        try {
            dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_privacy_bad_pax_id() throws Exception {

        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(new Long(1), SHARE_REC, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePrivacy(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    @Test
    public void test_update_profile_preferences() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePreferencesByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
        }

        dpService.updateProfilePreferences(myPaxId, paxMiscDTOList);
        PaxMiscListDTO = dpService.getProfilePreferencesByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOValidationList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOValidationList) {
            assertThat(paxMiscDTO.getMiscData(), is(ProjectConstants.TRUE));
        }
    }

    @Test
    public void test_update_profile_preferences_bad_vfname() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(BAD_VFNAME_TEST);
        }

        try {
            dpService.updateProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    @Test
    public void test_update_profile_preferences_bad_misc_data() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(BAD_MISC_DATA_TEST);
            paxMiscDTO.setVfName(WRITTEN_REC_1);
        }

        try {
            dpService.updateProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_update_profile_preferences_bad_pax_id() throws Exception {

        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(ProjectConstants.TRUE);
            paxMiscDTO.setVfName(WRITTEN_REC_1);
            paxMiscDTO.setPaxId(TestConstants.ID_1);
        }

        try {
            dpService.updateProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }

    }

    @Test
    public void testUpdateProfilePreferencesMissingVfName() {
        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        List<String> expectedErrorCodes = new ArrayList<>();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setVfName(null);
            expectedErrorCodes.add(ProfileConstants.INVALID_PARAMETER); // happens 7 times
        }

        verifyUpdateProfilePreferencesErrors(myPaxId, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void testUpdateProfilePreferencesMissingMiscData() {
        authenticationService.authenticate(TestConstants.USER_HAGOPIWL);
        Long myPaxId = security.getPaxId();

        List<String> expectedErrorCodes = new ArrayList<>();

        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscDTOList = PaxMiscListDTO.getPaxMiscs();
        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            paxMiscDTO.setMiscData(null);
            expectedErrorCodes.add(ProfileConstants.INVALID_DATA);
        }

        verifyUpdateProfilePreferencesErrors(myPaxId, paxMiscDTOList, expectedErrorCodes);
    }

    @Test
    public void test_insert_profile_preferences() throws Exception {

        authenticationService.authenticate(TestConstants.USER_PULSEM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, WRITTEN_REC_1, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, WRITTEN_REC_2, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, VERBAL_REC_1, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, VERBAL_REC_2, ProjectConstants.TRUE));
        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, VERBAL_REC_3, ProjectConstants.TRUE));

        dpService.insertProfilePreferences(myPaxId, paxMiscDTOList);
        PaxMiscListDTO PaxMiscListDTO = dpService.getProfilePrivacyByPaxId(myPaxId);
        List <PaxMiscDTO> paxMiscValidationDTOList = PaxMiscListDTO.getPaxMiscs();

        for (PaxMiscDTO paxMiscDTO : paxMiscDTOList) {
            boolean found = false;
            for (PaxMiscDTO paxMiscValidationDTO : paxMiscValidationDTOList) {
                if (paxMiscDTO.getVfName().equals(paxMiscValidationDTO.getVfName())) {
                    assertEquals(paxMiscDTO.getMiscData().toUpperCase(), paxMiscValidationDTO.getMiscData().toUpperCase());
                    found = true;
                }
            }
            assertTrue(found);
        }
    }

    @Test
    public void test_insert_profile_preferences_bad_vfname() throws Exception {

        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, BAD_VFNAME_TEST, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PARAMETER));
            }
        }
    }

    @Test
    public void test_insert_profile_preferences_bad_misc_data() throws Exception {

        authenticationService.authenticate(TestConstants.USER_PULSEM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(myPaxId, WRITTEN_REC_1, BAD_MISC_DATA_TEST));

        try {
            dpService.insertProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_DATA));
            }
        }
    }

    @Test
    public void test_insert_profile_preferences_bad_pax_id() throws Exception {

        authenticationService.authenticate(TestConstants.USER_ABBOTTM);
        Long myPaxId = security.getPaxId();

        List <PaxMiscDTO> paxMiscDTOList = new ArrayList<>();

        paxMiscDTOList.add(setPaxMiscDTO(new Long(1), WRITTEN_REC_1, ProjectConstants.TRUE));

        try {
            dpService.insertProfilePreferences(myPaxId, paxMiscDTOList);
        } catch (ErrorMessageException errorMsgException) {
            for (ErrorMessage errorMessage : errorMsgException.getErrorMessages()) {
                assertThat(errorMessage.getCode(), is(ProfileConstants.INVALID_PAX_ID));
            }
        }
    }

    private void verifyUpdateProfilePreferencesErrors(Long paxId,
            List<PaxMiscDTO> paxMiscDTOs,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.updateProfilePrivacy(paxId, paxMiscDTOs);
            fail();
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(expectedErrorCodes, errorMsgException);
        }
    }

    private void verifyInsertEmailsErrors(Long paxId,
            EmailPasswordWrapperDTO emailPasswordWrapperDTO,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.insertEmail(paxId, emailPasswordWrapperDTO);
            fail();
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        } catch (Exception e) {
            fail();
        }
    }

    private void verifyUpdateEmailsErrors(Long paxId,
            EmailPasswordWrapperDTO emailPasswordWrapperDTO,
            @Nonnull List<String> expectedErrorCodes){
        try {
            dpService.updateEmails(paxId, emailPasswordWrapperDTO);
            fail();
        } catch (ErrorMessageException e) {
            TestUtil.assertValidationErrors(expectedErrorCodes, e);
        } catch (Exception e) {
            fail();
        }
    }

    @Nonnull
    private EmailPasswordWrapperDTO generateValidInsertEmailPasswordWrapperDTO() {
        EmailPasswordWrapperDTO emailPasswordWrapperDTO = new EmailPasswordWrapperDTO();

        EmailDTO emailDTOInsert = new EmailDTO();
        emailDTOInsert.setEmailAddress(VALID_EMAIL);
        emailDTOInsert.setPaxId(TEST_INSERT_EMAIL_PAX_ID);

        List <EmailDTO> insertEmailDTOList = new ArrayList<>();
        insertEmailDTOList.add(emailDTOInsert);

        emailPasswordWrapperDTO.setEmails(insertEmailDTOList);
        emailPasswordWrapperDTO.setPassword(TestConstants.DEFAULT_PASSWORD);

        return emailPasswordWrapperDTO;
    }

    private void authenticateUser(String userName) {
        authenticationService.authenticate(userName);
        paxId = security.getPaxId();
    }

    private void enableSSOFlag(boolean value){
        ApplicationDataDTO appData =
                applicationDataService.getApplicationData(ApplicationDataConstants.KEY_NAME_SSO_ENABLED);
        appData.setValue(String.valueOf(value));

        applicationDataService.setApplicationData(appData);
    }
}
