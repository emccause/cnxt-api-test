package com.maritz.culturenext.profile.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.GroupType;
import com.maritz.culturenext.profile.dao.PersonalGroupsDao;
import com.maritz.culturenext.profile.services.impl.PersonalGroupsServiceImpl;

public class PersonalGroupsServiceTest {
    
    @InjectMocks private PersonalGroupsServiceImpl personalGroupsService;
    @Mock private GroupsService groupsService;
    @Mock private PersonalGroupsDao personalGroupsDao;
    @Mock private Security security;
    @Mock private ConcentrixDao<Groups> groupsDao;
    
    private static final Long TEST_MUDDAM_PERSONAL_GROUP_ID = 553L;
    
    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.when(security.getPaxId()).thenReturn(TestConstants.PAX_MUDDAM);
        
        List<Map<String, Object>> personalGroupsList = new ArrayList<Map<String, Object>>();
        
        Map<String, Object> groupInfoMap = new HashMap<String, Object>();
        groupInfoMap.put(ProjectConstants.TYPE, GroupType.PERSONAL.name());
        groupInfoMap.put(ProjectConstants.GROUP_PAX_ID, TestConstants.PAX_MUDDAM);
        
        PowerMockito.when(personalGroupsDao.getPersonalGroups(anyLong())).thenReturn(personalGroupsList);
        PowerMockito.when(groupsService.getGroupInfo(anyLong())).thenReturn(groupInfoMap);
        
        PowerMockito.doNothing().when(groupsService).deleteMembers(anyLong(), anyList());
        PowerMockito.doNothing().when(groupsDao).delete(any(Groups.class));
    }
    
    @Test
    public void test_get_personal_groups() throws Exception {
        assertNotNull("Personal Groups Service should not be null", personalGroupsService);
        List<Map<String, Object>> personalGroupsList = personalGroupsService.getPersonalGroups(TestConstants.PAX_MUDDAM);
        assertNotNull("Personal Groups should not be null for PAX_MUDDAM", personalGroupsList);
    }
    
    @Test
    public void test_delete_personal_groups() throws Exception {
        personalGroupsService.deletePersonalGroup(TEST_MUDDAM_PERSONAL_GROUP_ID);
        Mockito.verify(groupsDao, times(1)).delete(any(Groups.class));
    }

}
