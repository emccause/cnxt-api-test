package com.maritz.culturenext.profile.services;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.profile.dto.aggregate.LightweightProfileInfoDTO;
import com.maritz.culturenext.profile.services.LightProfileService;
import com.maritz.test.AbstractDatabaseTest;

public class LightProfileServiceTest extends AbstractDatabaseTest {

    @Inject private AuthenticationService authenticationService;
    @Inject private LightProfileService lpService;
    @Inject private SysUserRepository sysUserRepository;

    @Before
    public void setup() {
        authenticationService.authenticate(TestConstants.USER_ADMIN);
    }
 
    @Test
    public void test_get_light_profile_info() throws Exception {
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_HARWELLM);

        LightweightProfileInfoDTO lightweightProfileInfoDTO = lpService.getLightProfileInfo();
        
        assertThat(lightweightProfileInfoDTO.getPax().getPaxId(), is(sysUser.getPaxId()));
    }

    @Test
    public void test_profile_incomplete_flag_has_email() throws Exception {
        //This user should have profileIncomplete = FALSE because they do have an email address
        //Password is not currently checked since the ssoEnabled flag is set to TRUE (but this user has valid PW anyway)
        
        authenticationService.authenticate(TestConstants.USER_HARWELLM);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_HARWELLM);
        
        LightweightProfileInfoDTO lightweightProfileInfoDTO = lpService.getLightProfileInfo();

        assertThat(lightweightProfileInfoDTO.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(lightweightProfileInfoDTO.isProfileIncomplete(), is(false));
    }
    
    @Test
    public void test_profile_incomplete_flag_no_email() throws Exception {
        //This user should have profileIncomplete = TRUE because they don't have an email address
        //Password is not currently checked since the ssoEnabled flag is set to TRUE
        
        authenticationService.authenticate(TestConstants.USER_ADKINSJL);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_ADKINSJL);
        
        LightweightProfileInfoDTO lightweightProfileInfoDTO = lpService.getLightProfileInfo();

        assertThat(lightweightProfileInfoDTO.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(lightweightProfileInfoDTO.isProfileIncomplete(), is(true));
    }
    
    //Password is not currently checked since the ssoEnabled flag is set to TRUE
    @Test 
    public void test_profile_incomplete_flag_no_password_date() throws Exception {
        //This user has an email, but does not have a SYS_USER_PASSWORD_DATE
        //Therefore they should have profileIncomplete = TRUE
        
        authenticationService.authenticate(TestConstants.USER_ISMAILJ);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_ISMAILJ);
        
        LightweightProfileInfoDTO lightweightProfileInfoDTO = lpService.getLightProfileInfo();

        assertThat(lightweightProfileInfoDTO.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(lightweightProfileInfoDTO.isProfileIncomplete(), is(true));
    }
    
    //Password is not currently checked since the ssoEnabled flag is set to TRUE
    @Test
    public void test_profile_incomplete_flag_new_user() throws Exception {
        //This user has an email, but is in the NEW user status
        //Therefore they should have profileIncomplete = TRUE
    
        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_KUMARSJ);
        
        LightweightProfileInfoDTO lightweightProfileInfoDTO = lpService.getLightProfileInfo();

        assertThat(lightweightProfileInfoDTO.getPax().getPaxId(), is(sysUser.getPaxId()));
        assertThat(lightweightProfileInfoDTO.isProfileIncomplete(), is(true));
    }
}
