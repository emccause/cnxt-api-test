package com.maritz.culturenext.survey.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.inject.Inject;

import org.junit.Test;
import org.springframework.core.env.Environment;

import com.maritz.core.dto.AuthTokenDTO;
import com.maritz.core.jpa.entity.SysUser;
import com.maritz.core.jpa.repository.SysUserRepository;
import com.maritz.core.rest.NotFoundException;
import com.maritz.core.security.authentication.token.TokenService;
import com.maritz.core.security.authentication.token.TokenUtils;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.constants.ApplicationDataConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

import io.jsonwebtoken.Claims;

public class SurveyApiRestServiceTest extends AbstractRestTest {

    @Inject Environment environment;
    @Inject TokenService surveyApiTokenService;
    @Inject SysUserRepository sysUserRepository;

    @Test
    public void getSurveyApiToken() throws Exception {
        SysUser sysUser = sysUserRepository.findBySysUserName(TestConstants.USER_KUMARSJ);
        if (sysUser == null) throw new NotFoundException("sysUserName", TestConstants.USER_KUMARSJ);

        AuthTokenDTO authToken = ObjectUtils.jsonToObject(
            mockMvc.perform(
                get("/rest/participants/~/survey-api-token")
                    .with(authToken(sysUser.getSysUserName()))
                )
               .andExpect(status().isOk())
               .andReturn().getResponse().getContentAsString()
           ,AuthTokenDTO.class
        );

        assertThat(authToken, notNullValue());
        assertThat(authToken.getAuthToken(), notNullValue());

        Claims claims = surveyApiTokenService.getClaims(authToken.getAuthToken());
        assertThat(TokenUtils.getClaimAsLong(claims, "paxId"), is(sysUser.getPaxId()));
        assertThat(claims.get("clientName"), is(environment.getProperty(ApplicationDataConstants.KEY_NAME_CLIENT_DISPLAY_NAME)));
    }

}
