package com.maritz.culturenext.reports.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.reports.constants.ParticipantReportsConstants;
import com.maritz.test.AbstractRestTest;

public class ParticipantReportsRestServiceTest extends AbstractRestTest {
    
    public static final String BAD_PAX_ID = "BAD13";

    @Test
    public void get_participant_transaction_report_good() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }

    @Test
    public void get_pax_transaction_report_null_from_date() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate="
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_MISSING_FROM_DATE)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_from_date_format() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=05-01-2016"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_FROM_DATE_FORMAT)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_thru_date_format() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=05-31-2016"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_THRU_DATE_FORMAT)));
    }
    
    @Test
    public void get_pax_transaction_report_thru_date_before_from_date() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-06-31"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_THRU_DATE_BEFORE_FROM_DATE)));
    }
    
    @Test
    public void get_pax_transaction_report_group_id_NaN() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=Participants"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID_FORMAT)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_group_id() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=0"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_GROUP_ID)));
    }
    
    @Test
    public void get_pax_transaction_report_pax_team_NaN() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&paxTeam=Avengers"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM_FORMAT)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_pax_team() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&paxTeam=0"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PAX_TEAM)));
    }
                    
    @Test
    public void get_pax_transaction_report_pax_org_NaN() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&paxOrg=Hydra"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG_FORMAT)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_pax_org() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&paxOrg=0"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PAX_ORG)));
    }
        
    @Test
    public void get_pax_transaction_report_null_type() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type="
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_MISSING_TYPE)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_type() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=RECOGNITION"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_TYPE)));
    }

    @Test
    public void get_pax_transaction_report_null_status() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status="
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_MISSING_STATUS)));
    }
    
    @Test
    public void get_pax_transaction_report_missing_program_filter() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_pax_transaction_report_invalid_program_type() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER,RECOGNITION"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_TYPE)));
    }
            
    @Test
    public void get_pax_transaction_report_program_id_NaN() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programId=TEST_PROGRAM"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID_FORMAT)));
    }    
    
    @Test
    public void get_pax_transaction_report_invalid_program_id() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programId=0"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_PROGRAM_ID)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_recipient_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE,TEST"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_RECIPIENT_FIELD)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_submitter_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME,TEST"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_SUBMITTER_FIELD)));
    }
    
    @Test
    public void get_pax_transaction_report_invalid_approver_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1,TEST")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(ParticipantReportsConstants.ERROR_INVALID_APPROVER_FIELD)));
    }
    
    @Test
    public void get_my_activity_report_happy_path() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk());
    }

    @Test
    public void get_my_activity_report_without_program_type() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk());
    }

    @Test
    public void get_my_activity_report_with_programId_and_without_program_type() throws Exception {
        mockMvc.perform(
                get("/rest/participants/~/reports/pax-transactions-detail"
                        + "?fromDate=2010-05-01"
                        + "&thruDate=2018-05-31"
                        + "&programId=1"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_KUMARSJ))
            )
               .andExpect(status().isOk());
    }
    
    @Test
    public void get_my_activity_report_invalid_pax_id_format() throws Exception {
        mockMvc.perform(
                get("/rest/participants/" + BAD_PAX_ID + "/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isForbidden());
    }
    
    @Test
    public void get_my_activity_report_invalid_pax_id() throws Exception {
        mockMvc.perform(
                get("/rest/participants/0/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isForbidden());
    }
    
    @Test
    public void get_my_activity_report_not_logged_in_pax() throws Exception {
        mockMvc.perform(
                get("/rest/participants/" + TestConstants.PAX_HARWELLM + "/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isForbidden());
    }
    
    @Test
    public void get_participant_transaction_report_good_pax_team() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&teamId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_participant_transaction_report_good_pax_org() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&orgId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_participant_transaction_report_good_program_id() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&programId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_participant_transaction_report_not_admin() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&programId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_MUDDAM))
            )
               .andExpect(status().isForbidden())
       ;
    }
    
    @Test
    public void get_participant_transaction_report_type_milestone_good() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=MILESTONE"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
    
    @Test
    public void get_participant_transaction_report_several_types_good() throws Exception {
        mockMvc.perform(
                get("/rest/reports/pax-transactions-detail"
                        + "?fromDate=2016-05-01"
                        + "&thruDate=2016-05-31"
                        + "&groupId=4"
                        + "&type=GIVEN,RECEIVED"
                        + "&status=APPROVED,PENDING,REJECTED,ISSUED"
                        + "&programType=PEER_TO_PEER,MILESTONE"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_HARWELLM))
            )
               .andExpect(status().isOk())
       ;
    }
}
