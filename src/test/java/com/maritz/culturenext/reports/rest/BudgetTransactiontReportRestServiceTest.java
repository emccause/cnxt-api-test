package com.maritz.culturenext.reports.rest;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.reports.constants.BudgetTransactionReportConstants;
import com.maritz.test.AbstractRestTest;

public class BudgetTransactiontReportRestServiceTest extends AbstractRestTest {

    @Test
    public void get_budget_transaction_report_good() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=ACTIVE,ARCHIVED,SCHEDULED,ENDED"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isOk())
       ;
    }

    @Test
    public void get_budget_transaction_report_null_status() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_MISSING_STATUS)));
    }
    
    @Test
    public void get_budget_transaction_report_invalid_status() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=ACT")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_STATUS)));
    }
    
    @Test
    public void get_budget_transaction_report_budget_id_NaN() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=TEST_BUDGET"
                        + "&status=ACTIVE")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID_FORMAT)));
    }    
    
    @Test
    public void get_budget_transaction_report_invalid_BUDGET_id() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=0"
                        + "&status=ACTIVE")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_BUDGET_ID)));
    }
    
    @Test
    public void get_budget_transaction_report_invalid_recipient_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=ACTIVE"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE,TEST")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_RECIPIENT_FIELD)));
    }
    
    @Test
    public void get_budget_transaction_report_invalid_submitter_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=ACTIVE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME,TEST"
                        + "&approverFields=CUSTOM_1")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_SUBMITTER_FIELD)));
    }
    
    @Test
    public void get_budget_transaction_report_invalid_approver_field() throws Exception {
        mockMvc.perform(
                get("/rest/reports/budget-transaction-detail"
                        + "?budgetId=1"
                        + "&status=ACTIVE"
                        + "&recipientFields=CITY,STATE,POSTAL_CODE"
                        + "&submitterFields=EMAIL,TITLE,COMPANY_NAME"
                        + "&approverFields=CUSTOM_1,TEST")
                .with(authToken(TestConstants.USER_KUMARSJ_ADMIN))
            )
               .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$[0].code", is(BudgetTransactionReportConstants.ERROR_INVALID_APPROVER_FIELD)));
    }
}
