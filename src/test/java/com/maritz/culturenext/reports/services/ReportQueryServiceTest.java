package com.maritz.culturenext.reports.services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.test.AbstractDatabaseTest;
import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.budget.dto.BudgetDTO;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dto.QueryIdDto;
import com.maritz.culturenext.reports.dto.ReportsQueryRequestDto;
import com.maritz.culturenext.reports.service.ReportsQueryService;

public class ReportQueryServiceTest extends AbstractDatabaseTest {
    
    @Inject private AuthenticationService authenticationService;
    @Inject private ReportsQueryService reportsQueryService;
    
    @Test
    public void test_good_query_id() {
        ReportsQueryRequestDto requestDto = ObjectUtils.jsonToObject("{\"fromDate\": \"2015-05-01\","
                + "\"thruDate\":\"2015-06-17\",\"members\":[{\"paxId\":969}],\"directReports\":[847],"
                + "\"orgReports\":[12671, 553, 12851]}", ReportsQueryRequestDto.class);
        
        try {
            authenticationService.authenticate(TestConstants.USER_PORTERGA);
            QueryIdDto query = reportsQueryService.getQueryId(requestDto);
            
            List<Map<String,Object>> programs = reportsQueryService.getProgramsForQuery(query.getQueryId(), null);
            if (programs == null || programs.isEmpty()) {
                fail();
            }
            
            List<BudgetDTO> budgets = reportsQueryService.getBudgetsForQuery(query.getQueryId());
            if (budgets == null || budgets.isEmpty()) {
                fail();
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void test_good_query_id_with_language_code() {
        ReportsQueryRequestDto requestDto = ObjectUtils.jsonToObject("{\"fromDate\": \"2015-05-01\","
                + "\"thruDate\":\"2015-06-17\",\"members\":[{\"paxId\":969}],\"directReports\":[847],"
                + "\"orgReports\":[12671, 553, 12851]}", ReportsQueryRequestDto.class);

        try {
            authenticationService.authenticate(TestConstants.USER_PORTERGA);
            QueryIdDto query = reportsQueryService.getQueryId(requestDto);

            List<Map<String,Object>> programs = reportsQueryService.getProgramsForQuery(query.getQueryId(), "fr_CA");
            if (programs == null || programs.isEmpty()) {
                fail();
            }

            List<BudgetDTO> budgets = reportsQueryService.getBudgetsForQuery(query.getQueryId());
            if (budgets == null || budgets.isEmpty()) {
                fail();
            }
        } catch (Exception e) {
            fail();
        }
    }
    
    @Test
    public void test_bad_query_id() {
        try {
            reportsQueryService.getProgramsForQuery("THIS SHOULD BREAK EVERYTHING", null);
            fail("That query id should have failed.");
        }
        catch(Exception e) {
            if (e instanceof ErrorMessageException) {
                assertTrue(((ErrorMessageException) e)
                        .getErrorMessages()
                        .contains(ReportConstants.INVALID_QUERY_ID_MESSAGE));
            } else {
                fail("Wrong error thrown. " + e.getMessage());
            }
        }
        try {
            reportsQueryService.getBudgetsForQuery("THIS SHOULD BREAK EVERYTHING");
            fail("That query id should have failed.");
        } catch(Exception e) {
            if (e instanceof ErrorMessageException) {
                assertTrue(((ErrorMessageException) e)
                        .getErrorMessages()
                        .contains(ReportConstants.INVALID_QUERY_ID_MESSAGE));
            } else {
                fail("Wrong error thrown. " + e.getMessage());
            }
        }
    }
    
    @Test
    public void test_not_my_query_id() {
        ReportsQueryRequestDto requestDto = ObjectUtils.jsonToObject("{\"fromDate\": \"2015-05-01\","
                + "\"thruDate\":\"2015-06-17\",\"members\":[{\"paxId\":969}],\"directReports\":[847],"
                + "\"orgReports\":[12671, 553, 12851]}", ReportsQueryRequestDto.class);
        
        QueryIdDto query = null;
        try {
            authenticationService.authenticate(TestConstants.USER_PORTERGA);
            query = reportsQueryService.getQueryId(requestDto);
        } catch (Exception e) {
            fail();
        }

        authenticationService.authenticate(TestConstants.USER_KUMARSJ);
        try {
            reportsQueryService.getProgramsForQuery(query.getQueryId(), null);
            fail();
        }
        catch (Exception e) {
            if (e instanceof ErrorMessageException) {
                assertTrue(((ErrorMessageException) e)
                        .getErrorMessages()
                        .contains(ReportConstants.QUERY_NOT_YOURS_ERROR));
            } else {
                fail("Wrong error thrown. " + e.getMessage());
            }
        }
        try {
            reportsQueryService.getBudgetsForQuery(query.getQueryId());
            fail();
        }
        catch (Exception e) {
            if (e instanceof ErrorMessageException) {
                assertTrue(((ErrorMessageException) e)
                        .getErrorMessages()
                        .contains(ReportConstants.QUERY_NOT_YOURS_ERROR));
            } else {
                fail("Wrong error thrown. " + e.getMessage());
            }
        }
        
    }

}