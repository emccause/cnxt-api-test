package com.maritz.culturenext.reports.services;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockHttpServletResponse;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.maritz.TestUtil;
import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Groups;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.enrollment.config.services.EnrollmentConfigService;
import com.maritz.culturenext.enrollment.field.dto.EnrollmentFieldDTO;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.PointsReportDao;
import com.maritz.culturenext.reports.dto.PointsReportContentDTO;
import com.maritz.culturenext.reports.dto.PointsReportRequestDTO;
import com.maritz.culturenext.reports.service.impl.PointsReportServiceImpl;
import com.maritz.culturenext.reports.util.DeliveryMethodTypes;
import com.maritz.culturenext.reports.util.EnrollmentFieldReportUtil;
import com.maritz.culturenext.reports.util.PointsReportUtil;
import com.maritz.culturenext.reports.util.ReportTypes;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;

public class PointsReportServiceTest extends AbstractMockTest {

    @InjectMocks private PointsReportServiceImpl pointsReportService;
    @Mock private ConcentrixDao<Groups> groupsDao;
    @Mock private EnrollmentConfigService enrollmentConfigService;
    @Mock private PointsReportDao pointsReportDao;
    @Mock private Security security;
    @Mock private Environment environment;

    private PointsReportRequestDTO requestReport;
    private EnrollmentFieldReportUtil enrollmentFieldReportUtil;
    private MockHttpServletResponse response = new MockHttpServletResponse();

    private static final String DEFAULT_FROM_DATE = "2015-01-01";
    private static final String DEFAULT_TO_DATE = "2015-12-31";
    private static final Long DEFAULT_GROUP_ID = 5L;

    // File content test fields
    private static Date TEST_CONTENT_ISSUANCE_DATE = DateUtil.convertFromUTCStringDate("2015-10-21");
    private static final long TEST_CONTENT_RECIPIENT_ID = 23L;
    private static final String TEST_CONTENT_RECIPIENT_FIRST_NAME = "Michael";
    private static final String TEST_CONTENT_RECIPIENT_LAST_NAME = "Jordan";
    private static final String TEST_CONTENT_RECIPIENT_COMPANY = "Chicago Bulls";
    private static final String TEST_CONTENT_RECIPIENT_TITLE = "Shooting Guard";
    private static final String TEST_CONTENT_RECIPIENT_EMAIL = "mj23@maritz.com";
    private static final String TEST_CONTENT_RECIPIENT_CITY = "Brooklyn";
    private static final String TEST_CONTENT_RECIPIENT_STATE = "New York";
    private static final String TEST_CONTENT_RECIPIENT_POSTAL_CODE = "99182";
    private static final String TEST_CONTENT_RECIPIENT_COUNTRY = "US";
    private static final String TEST_CONTENT_RECIPIENT_DEPTO = "Basketball Team";
    private static final String TEST_CONTENT_RECIPIENT_COST_CENTER = "40162";
    private static final String TEST_CONTENT_RECIPIENT_AREA = "Area Opt1";
    private static final String TEST_CONTENT_RECIPIENT_GRADE = "Grade Opt1";
    private static final String TEST_CONTENT_RECIPIENT_FUNCTION = "Function Opt1";
    private static final String TEST_CONTENT_RECIPIENT_CUSTOM1 = "Custom1 Opt1";
    private static final String TEST_CONTENT_RECIPIENT_CUSTOM2 = "Custom2 Opt1";
    private static final String TEST_CONTENT_RECIPIENT_CUSTOM3 = "Custom3 Opt1";
    private static final String TEST_CONTENT_RECIPIENT_CUSTOM4 = "Custom4 Opt1";
    private static final String TEST_CONTENT_RECIPIENT_CUSTOM5 = "Custom5 Opt1";
    private static final long TEST_CONTENT_SUBMITTER_ID = 33L;
    private static final String TEST_CONTENT_SUBMITTER_FIRST_NAME = "Scottie";
    private static final String TEST_CONTENT_SUBMITTER_LAST_NAME = "Pippen";
    private static final String TEST_CONTENT_SUBMITTER_COMPANY = "Chicago Bulls";
    private static final String TEST_CONTENT_SUBMITTER_TITLE = "Point Guard";
    private static final String TEST_CONTENT_SUBMITTER_EMAIL = "sp25@maritz.com";
    private static final String TEST_CONTENT_SUBMITTER_CITY = "Hamburg";
    private static final String TEST_CONTENT_SUBMITTER_STATE = "Arkansas";
    private static final String TEST_CONTENT_SUBMITTER_POSTAL_CODE = "99183";
    private static final String TEST_CONTENT_SUBMITTER_COUNTRY = "US";
    private static final String TEST_CONTENT_SUBMITTER_DEPTO = "Basketball Team";
    private static final String TEST_CONTENT_SUBMITTER_COST_CENTER = "40163";
    private static final String TEST_CONTENT_SUBMITTER_AREA = "Area Opt2";
    private static final String TEST_CONTENT_SUBMITTER_GRADE = "Grade Opt2";
    private static final String TEST_CONTENT_SUBMITTER_FUNCTION = "Function Opt2";
    private static final String TEST_CONTENT_SUBMITTER_CUSTOM1 = "Custom1 Opt2";
    private static final String TEST_CONTENT_SUBMITTER_CUSTOM2 = "Custom2 Opt2";
    private static final String TEST_CONTENT_SUBMITTER_CUSTOM3 = "Custom3 Opt2";
    private static final String TEST_CONTENT_SUBMITTER_CUSTOM4 = "Custom4 Opt2";
    private static final String TEST_CONTENT_SUBMITTER_CUSTOM5 = "Custom5 Opt2";
    private static final String TEST_CONTENT_BUDGET_NAME = "teammate budget";
    private static final double TEST_CONTENT_POINTS_AWARDED = 100d;
    private static final String TEST_CONTENT_PROGRAM_NAME = "Chicago Bull awards";

    // Mock enrollment fields names
    public static final String COMPANY_FIELD = "COMPANY_NAME";
    public static final String TITLE_FIELD = "TITLE";
    public static final String EMAIL_FIELD = "EMAIL";
    public static final String CITY_FIELD = "CITY";
    public static final String STATE_FIELD = "STATE";
    public static final String COUNTRY_FIELD = "COUNTRY_CODE";
    public static final String POSTAL_CODE_FIELD = "POSTAL_CODE";
    public static final String DEPARTMENT_FIELD = "DEPARTMENT";
    public static final String COST_CENTER_FIELD = "COST_CENTER";
    public static final String AREA_FIELD = "AREA";
    public static final String GRADE_FIELD = "GRADE";
    public static final String FUNCTION_FIELD = "FUNCTION";
    public static final String CUSTOM1_FIELD = "CUSTOM1";
    public static final String CUSTOM2_FIELD = "CUSTOM2";
    public static final String CUSTOM3_FIELD = "CUSTOM3";
    public static final String CUSTOM4_FIELD = "CUSTOM4";
    public static final String CUSTOM5_FIELD = "CUSTOM5";
    public static final List<String> ENROLLMENT_FIELDS_LIST = Arrays.asList(COMPANY_FIELD, TITLE_FIELD, EMAIL_FIELD,
            CITY_FIELD, STATE_FIELD, COUNTRY_FIELD, POSTAL_CODE_FIELD, DEPARTMENT_FIELD, COST_CENTER_FIELD, AREA_FIELD,
            GRADE_FIELD, FUNCTION_FIELD, CUSTOM1_FIELD, CUSTOM2_FIELD, CUSTOM3_FIELD, CUSTOM4_FIELD, CUSTOM5_FIELD);

    // Mock enrollment fields display names
    public static final String COMPANY_DISPLAY_NAME = "Company Name";
    public static final String TITLE_DISPLAY_NAME = "Title";
    public static final String EMAIL_DISPLAY_NAME = "Email";
    public static final String CITY_DISPLAY_NAME = "City";
    public static final String STATE_DISPLAY_NAME = "State";
    public static final String COUNTRY_DISPLAY_NAME = "Country Code";
    public static final String POSTAL_CODE_DISPLAY_NAME = "Postal Code";
    public static final String DEPARTMENT_DISPLAY_NAME = "Department";
    public static final String COST_CENTER_DISPLAY_NAME = "Cost Center";
    public static final String AREA_DISPLAY_NAME = "Area";
    public static final String GRADE_DISPLAY_NAME = "Grade";
    public static final String FUNCTION_DISPLAY_NAME = "Function";
    public static final String CUSTOM1_DISPLAY_NAME = "Custom1";
    public static final String CUSTOM2_DISPLAY_NAME = "Custom2";
    public static final String CUSTOM3_DISPLAY_NAME = "Custom3";
    public static final String CUSTOM4_DISPLAY_NAME = "Custom4";
    public static final String CUSTOM5_DISPLAY_NAME = "Custom5";

    public static final List<String> ENROLLMENT_DISPLAY_NAME_LIST = Arrays.asList(COMPANY_DISPLAY_NAME,
            TITLE_DISPLAY_NAME, EMAIL_DISPLAY_NAME, CITY_DISPLAY_NAME, STATE_DISPLAY_NAME, COUNTRY_DISPLAY_NAME,
            POSTAL_CODE_DISPLAY_NAME, DEPARTMENT_DISPLAY_NAME, COST_CENTER_DISPLAY_NAME, AREA_DISPLAY_NAME,
            GRADE_DISPLAY_NAME, FUNCTION_DISPLAY_NAME, CUSTOM1_DISPLAY_NAME, CUSTOM2_DISPLAY_NAME, CUSTOM3_DISPLAY_NAME,
            CUSTOM4_DISPLAY_NAME, CUSTOM5_DISPLAY_NAME);

    @Before
    public void setup() {
        // Default mocks
        PowerMockito.when(groupsDao.findById(anyLong())).thenReturn(PowerMockito.mock(Groups.class));
        PowerMockito.when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class),
                anyLong())).thenReturn(null);

        List<EnrollmentFieldDTO> enrollmentFieldData = generateEnrollmentFieldData();
        PowerMockito.when(enrollmentConfigService.getEnrollmentFields()).thenReturn(enrollmentFieldData);

        enrollmentFieldReportUtil = new EnrollmentFieldReportUtil(enrollmentFieldData);

        // Create a default request
        requestReport = defaultReportRequest();

        Mockito.when(environment.getProperty("reports.logging.enabled", String.class)).thenReturn("true");
    }

    @Test
    public void test_generatePointsReport_fail_missing_report_type() {
        requestReport.setType(null);
        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_REPORT_TYPE_MISSING, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_missing_from_date() {

        requestReport.setFromDate(null);

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_MISSING_FROM_DATE_TYPE_CODE, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_missing_to_date() {

        requestReport.setToDate(null);

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_MISSING_TO_DATE_TYPE_CODE, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_report_type() {
        requestReport.setType("NOT_VALID");
        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_REPORT_TYPE_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_delivery_method() {

        requestReport.setDeliveryMethod("NOT_VALID");

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_DELIVERY_METHOD_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_recipient_fields() {

        requestReport.setRecipientFields(Arrays.asList("NOT_VALID"));

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_RECIPIENT_FIELDS_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_submitter_fields() {

        requestReport.setSubmitterFields(Arrays.asList("NOT_VALID"));

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_SUBMITTER_FIELDS_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_from_date() {

        requestReport.setFromDate("null");

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_FROM_DATE_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_to_date() {

        requestReport.setToDate("null");

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_INVALID_TO_DATE_TYPE_CODE, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_fromDate_after_toDate() {
        requestReport.setFromDate("2015-12-30");
        requestReport.setToDate("2015-10-30");

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_FROM_DATE_IS_AFTER_TO_DATE_CODE,
                    errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_fail_invalid_group_id() {
        // group id not exists.
        PowerMockito.when(groupsDao.findById(anyLong())).thenReturn(null);

        try {
            pointsReportService.generatePointsReport(response, requestReport);
            fail("Getting report should have thrown errors!");
        } catch (ErrorMessageException errorMsgException) {
            TestUtil.assertValidationErrors(1, ReportConstants.ERROR_GROUP_ID_INVALID, errorMsgException);
        }
    }

    @Test
    public void test_generatePointsReport_validate_csvFile_content_issuance_report() {
        // Request Issuance report type
        requestReport.setType(ReportTypes.GIVEN.toString());

        PowerMockito
                .when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class), anyLong()))
                .thenReturn(generatePointsGivenReportResult());

        pointsReportService.generatePointsReport(response, requestReport);

        validateCSVFileContent();
    }

    @Test
    public void test_generatePointsReport_validate_csvFile_content_income_report() {
        // Request Income report type
        requestReport.setType(ReportTypes.RECEIVED.toString());

        PowerMockito
                .when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class), anyLong()))
                .thenReturn(generatePointsGivenReportResult());

        pointsReportService.generatePointsReport(response, requestReport);

        validateCSVFileContent();
    }

    @Test
    public void test_generatePointsReport_validate_csvFile_content_recipient_all_fields() {
        // add all recipient fields
        requestReport.setRecipientFields(ENROLLMENT_FIELDS_LIST);

        PowerMockito
                .when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class), anyLong()))
                .thenReturn(generatePointsGivenReportResult());

        pointsReportService.generatePointsReport(response, requestReport);

        validateCSVFileContent();
    }

    @Test
    public void test_generatePointsReport_validate_csvFile_content_submitter_all_fields() {
        // add all submitter fields
        requestReport.setSubmitterFields(ENROLLMENT_FIELDS_LIST);

        PowerMockito
                .when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class), anyLong()))
                .thenReturn(generatePointsGivenReportResult());

        pointsReportService.generatePointsReport(response, requestReport);

        validateCSVFileContent();
    }

    @Test
    public void test_generatePointsReport_default_delivery_method() {

        // use default delivery method
        requestReport.setDeliveryMethod(null);

        PowerMockito
        .when(pointsReportDao.getPointsReportContent(anyString(), any(Date.class), any(Date.class), anyLong()))
        .thenReturn(generatePointsGivenReportResult());

        pointsReportService.generatePointsReport(response, requestReport);

        validateCSVFileContent();
    }

    private void validateCSVFileContent() {
        try {
            String content = response.getContentAsString();
            Reader reader = new StringReader(content);

            // Create CSV report properties : headers, field mappings, processors
            Map<String, Object[]> csvReportProps = PointsReportUtil.createCSVReportProperties(requestReport,
                    enrollmentFieldReportUtil);

            String[] fieldMapping = (String[]) csvReportProps.get(ProjectConstants.FIELD_MAPPING);
            CellProcessor[] processor = (CellProcessor[]) csvReportProps.get(ProjectConstants.PROCESSORS);

            // Issuance date processor
            processor[0] = new ParseDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT);


            List<PointsReportContentDTO> reportResult = CsvFileUtil.readCSVFILE(reader, PointsReportContentDTO.class,
                    processor, fieldMapping);

            assertTrue(reportResult.size() == 1);
            validateReportColumns(requestReport.getRecipientFields(), requestReport.getSubmitterFields(),
                    reportResult.get(0));
        } catch (Exception e) {
            fail("Getting report is throwing an error: " + e.getMessage());
        }
    }

    /**
     * Create a default service's request using mocks
     *
     * @return
     */
    private PointsReportRequestDTO defaultReportRequest() {
        PointsReportRequestDTO requestReport = new PointsReportRequestDTO();
        requestReport.setType(ReportTypes.GIVEN.toString());
        requestReport.setDeliveryMethod(DeliveryMethodTypes.DOWNLOAD.toString());
        requestReport.setFromDate(DEFAULT_FROM_DATE);
        requestReport.setToDate(DEFAULT_TO_DATE);
        requestReport.setGroupId(DEFAULT_GROUP_ID);
        requestReport.setRecipientFields(Arrays.asList(AREA_FIELD));
        requestReport.setSubmitterFields(Arrays.asList(AREA_FIELD));

        return requestReport;
    }

    /**
     * Mock report's query result
     * @return
     */
    private List<PointsReportContentDTO> generatePointsGivenReportResult() {
        List<PointsReportContentDTO> pointsGivenResult = new ArrayList<>();
        PointsReportContentDTO node = new PointsReportContentDTO();

        node.setIssuanceDate(TEST_CONTENT_ISSUANCE_DATE);
        node.setRecipientPaxId(TEST_CONTENT_RECIPIENT_ID);
        node.setRecipientFirstName(TEST_CONTENT_RECIPIENT_FIRST_NAME);
        node.setRecipientLastName(TEST_CONTENT_RECIPIENT_LAST_NAME);
        node.setRecipientCompanyName(TEST_CONTENT_RECIPIENT_COMPANY);
        node.setRecipientTitle(TEST_CONTENT_RECIPIENT_TITLE);
        node.setRecipientEmail(TEST_CONTENT_RECIPIENT_EMAIL);
        node.setRecipientCity(TEST_CONTENT_RECIPIENT_CITY);
        node.setRecipientState(TEST_CONTENT_RECIPIENT_STATE);
        node.setRecipientCountryCode(TEST_CONTENT_RECIPIENT_COUNTRY);
        node.setRecipientPostalCode(TEST_CONTENT_RECIPIENT_POSTAL_CODE);
        node.setRecipientDepartment(TEST_CONTENT_RECIPIENT_DEPTO);
        node.setRecipientCostCenter(TEST_CONTENT_RECIPIENT_COST_CENTER);
        node.setRecipientArea(TEST_CONTENT_RECIPIENT_AREA);
        node.setRecipientGrade(TEST_CONTENT_RECIPIENT_GRADE);
        node.setRecipientFunction(TEST_CONTENT_RECIPIENT_FUNCTION);
        node.setRecipientCustom1(TEST_CONTENT_RECIPIENT_CUSTOM1);
        node.setRecipientCustom2(TEST_CONTENT_RECIPIENT_CUSTOM2);
        node.setRecipientCustom3(TEST_CONTENT_RECIPIENT_CUSTOM3);
        node.setRecipientCustom4(TEST_CONTENT_RECIPIENT_CUSTOM4);
        node.setRecipientCustom5(TEST_CONTENT_RECIPIENT_CUSTOM5);
        node.setSubmitterPaxId(TEST_CONTENT_SUBMITTER_ID);
        node.setSubmitterFirstName(TEST_CONTENT_SUBMITTER_FIRST_NAME);
        node.setSubmitterLastName(TEST_CONTENT_SUBMITTER_LAST_NAME);
        node.setSubmitterCompanyName(TEST_CONTENT_SUBMITTER_COMPANY);
        node.setSubmitterTitle(TEST_CONTENT_SUBMITTER_TITLE);
        node.setSubmitterEmail(TEST_CONTENT_SUBMITTER_EMAIL);
        node.setSubmitterCity(TEST_CONTENT_SUBMITTER_CITY);
        node.setSubmitterState(TEST_CONTENT_SUBMITTER_STATE);
        node.setSubmitterCountryCode(TEST_CONTENT_SUBMITTER_COUNTRY);
        node.setSubmitterPostalCode(TEST_CONTENT_SUBMITTER_POSTAL_CODE);
        node.setSubmitterDepartment(TEST_CONTENT_SUBMITTER_DEPTO);
        node.setSubmitterCostCenter(TEST_CONTENT_SUBMITTER_COST_CENTER);
        node.setSubmitterArea(TEST_CONTENT_SUBMITTER_AREA);
        node.setSubmitterGrade(TEST_CONTENT_SUBMITTER_GRADE);
        node.setSubmitterFunction(TEST_CONTENT_SUBMITTER_FUNCTION);
        node.setSubmitterCustom1(TEST_CONTENT_SUBMITTER_CUSTOM1);
        node.setSubmitterCustom2(TEST_CONTENT_SUBMITTER_CUSTOM2);
        node.setSubmitterCustom3(TEST_CONTENT_SUBMITTER_CUSTOM3);
        node.setSubmitterCustom4(TEST_CONTENT_SUBMITTER_CUSTOM4);
        node.setSubmitterCustom5(TEST_CONTENT_SUBMITTER_CUSTOM5);
        node.setBudgetName(TEST_CONTENT_BUDGET_NAME);
        node.setBudgetId(TestConstants.ID_2);
        node.setPointsAwarded(TEST_CONTENT_POINTS_AWARDED);
        node.setProgramName(TEST_CONTENT_PROGRAM_NAME);

        pointsGivenResult.add(node);
        return pointsGivenResult;
    }

    /**
     * Verify report columns according to recipients and submitter fields
     * request.
     *
     * @param recipientColumns
     * @param submitterColumns
     * @param reportContent
     */
    private void validateReportColumns(List<String> recipientColumns, List<String> submitterColumns,
            PointsReportContentDTO reportContent) {
        // verify standard columns
        assertThat(reportContent.getBudgetId(), equalTo(TestConstants.ID_2));
        assertThat(reportContent.getBudgetName(), equalTo(TEST_CONTENT_BUDGET_NAME));
        assertThat(reportContent.getPointsAwarded(), equalTo(TEST_CONTENT_POINTS_AWARDED));
        assertThat(reportContent.getProgramName(), equalTo(TEST_CONTENT_PROGRAM_NAME));
        assertThat(reportContent.getRecipientPaxId(), equalTo(TEST_CONTENT_RECIPIENT_ID));
        assertThat(reportContent.getRecipientFirstName(), equalTo(TEST_CONTENT_RECIPIENT_FIRST_NAME));
        assertThat(reportContent.getRecipientLastName(), equalTo(TEST_CONTENT_RECIPIENT_LAST_NAME));
        assertThat(reportContent.getSubmitterPaxId(), equalTo(TEST_CONTENT_SUBMITTER_ID));
        assertThat(reportContent.getSubmitterFirstName(), equalTo(TEST_CONTENT_SUBMITTER_FIRST_NAME));
        assertThat(reportContent.getSubmitterLastName(), equalTo(TEST_CONTENT_SUBMITTER_LAST_NAME));

        // verify recipient optional columns
        if (recipientColumns.contains(COMPANY_FIELD)
                && !reportContent.getRecipientCompanyName().equals(TEST_CONTENT_RECIPIENT_COMPANY)
                || recipientColumns.contains(TITLE_FIELD)
                        && !reportContent.getRecipientTitle().equals(TEST_CONTENT_RECIPIENT_TITLE)
                || recipientColumns.contains(EMAIL_FIELD)
                        && !reportContent.getRecipientEmail().equals(TEST_CONTENT_RECIPIENT_EMAIL)
                || recipientColumns.contains(CITY_FIELD)
                        && !reportContent.getRecipientCity().equals(TEST_CONTENT_RECIPIENT_CITY)
                || recipientColumns.contains(STATE_FIELD)
                        && !reportContent.getRecipientState().equals(TEST_CONTENT_RECIPIENT_STATE)
                || recipientColumns.contains(COUNTRY_FIELD)
                        && !reportContent.getRecipientCountryCode().equals(TEST_CONTENT_RECIPIENT_COUNTRY)
                || recipientColumns.contains(POSTAL_CODE_FIELD)
                        && !reportContent.getRecipientPostalCode().equals(TEST_CONTENT_RECIPIENT_POSTAL_CODE)
                || recipientColumns.contains(DEPARTMENT_FIELD)
                        && !reportContent.getRecipientDepartment().equals(TEST_CONTENT_RECIPIENT_DEPTO)
                || recipientColumns.contains(COST_CENTER_FIELD)
                        && !reportContent.getRecipientCostCenter().equals(TEST_CONTENT_RECIPIENT_COST_CENTER)
                || recipientColumns.contains(AREA_FIELD)
                        && !reportContent.getRecipientArea().equals(TEST_CONTENT_RECIPIENT_AREA)
                || recipientColumns.contains(GRADE_FIELD)
                        && !reportContent.getRecipientGrade().equals(TEST_CONTENT_RECIPIENT_GRADE)
                || recipientColumns.contains(FUNCTION_FIELD)
                        && !reportContent.getRecipientFunction().equals(TEST_CONTENT_RECIPIENT_FUNCTION)
                || recipientColumns.contains(CUSTOM1_FIELD)
                        && !reportContent.getRecipientCustom1().equals(TEST_CONTENT_RECIPIENT_CUSTOM1)
                || recipientColumns.contains(CUSTOM2_FIELD)
                        && !reportContent.getRecipientCustom2().equals(TEST_CONTENT_RECIPIENT_CUSTOM2)
                || recipientColumns.contains(CUSTOM3_FIELD)
                        && !reportContent.getRecipientCustom3().equals(TEST_CONTENT_RECIPIENT_CUSTOM3)
                || recipientColumns.contains(CUSTOM4_FIELD)
                        && !reportContent.getRecipientCustom4().equals(TEST_CONTENT_RECIPIENT_CUSTOM4)
                || recipientColumns.contains(CUSTOM5_FIELD)
                        && !reportContent.getRecipientCustom5().equals(TEST_CONTENT_RECIPIENT_CUSTOM5)) {
            fail("Report recipient columns don´t match with the expected columns");
        }

        // verify submitter optional fields
        if (submitterColumns.contains(COMPANY_FIELD)
                && !reportContent.getSubmitterCompanyName().equals(TEST_CONTENT_SUBMITTER_COMPANY)
                || submitterColumns.contains(TITLE_FIELD)
                        && !reportContent.getSubmitterTitle().equals(TEST_CONTENT_SUBMITTER_TITLE)
                || submitterColumns.contains(EMAIL_FIELD)
                        && !reportContent.getSubmitterEmail().equals(TEST_CONTENT_SUBMITTER_EMAIL)
                || submitterColumns.contains(CITY_FIELD)
                        && !reportContent.getSubmitterCity().equals(TEST_CONTENT_SUBMITTER_CITY)
                || submitterColumns.contains(STATE_FIELD)
                        && !reportContent.getSubmitterState().equals(TEST_CONTENT_SUBMITTER_STATE)
                || submitterColumns.contains(COUNTRY_FIELD)
                        && !reportContent.getSubmitterCountryCode().equals(TEST_CONTENT_SUBMITTER_COUNTRY)
                || submitterColumns.contains(POSTAL_CODE_FIELD)
                        && !reportContent.getSubmitterPostalCode().equals(TEST_CONTENT_SUBMITTER_POSTAL_CODE)
                || submitterColumns.contains(DEPARTMENT_FIELD)
                        && !reportContent.getSubmitterDepartment().equals(TEST_CONTENT_SUBMITTER_DEPTO)
                || submitterColumns.contains(COST_CENTER_FIELD)
                        && !reportContent.getSubmitterCostCenter().equals(TEST_CONTENT_SUBMITTER_COST_CENTER)
                || submitterColumns.contains(AREA_FIELD)
                        && !reportContent.getSubmitterArea().equals(TEST_CONTENT_SUBMITTER_AREA)
                || submitterColumns.contains(GRADE_FIELD)
                        && !reportContent.getSubmitterGrade().equals(TEST_CONTENT_SUBMITTER_GRADE)
                || submitterColumns.contains(FUNCTION_FIELD)
                        && !reportContent.getSubmitterFunction().equals(TEST_CONTENT_SUBMITTER_FUNCTION)
                || submitterColumns.contains(CUSTOM1_FIELD)
                        && !reportContent.getSubmitterCustom1().equals(TEST_CONTENT_SUBMITTER_CUSTOM1)
                || submitterColumns.contains(CUSTOM2_FIELD)
                        && !reportContent.getSubmitterCustom2().equals(TEST_CONTENT_SUBMITTER_CUSTOM2)
                || submitterColumns.contains(CUSTOM3_FIELD)
                        && !reportContent.getSubmitterCustom3().equals(TEST_CONTENT_SUBMITTER_CUSTOM3)
                || submitterColumns.contains(CUSTOM4_FIELD)
                        && !reportContent.getSubmitterCustom4().equals(TEST_CONTENT_SUBMITTER_CUSTOM4)
                || submitterColumns.contains(CUSTOM5_FIELD)
                        && !reportContent.getSubmitterCustom5().equals(TEST_CONTENT_SUBMITTER_CUSTOM5)) {
            fail("Report submitter columns don´t match with the expected columns");
        }
    }

    /**
     * Mock Enrollment Fields table data
     * @return
     */
    private List<EnrollmentFieldDTO> generateEnrollmentFieldData(){
        List<EnrollmentFieldDTO> enrollmentFieldList = new ArrayList<>();
        EnrollmentFieldDTO enrollFieldDto;

        for(int i = 0 ; i < ENROLLMENT_FIELDS_LIST.size() ; i ++){
            enrollFieldDto = new EnrollmentFieldDTO();
            enrollFieldDto.setName(ENROLLMENT_FIELDS_LIST.get(i));
            enrollFieldDto.setDisplayName(ENROLLMENT_DISPLAY_NAME_LIST.get(i));
            enrollmentFieldList.add(enrollFieldDto );
        }

        return enrollmentFieldList;
    }

}
