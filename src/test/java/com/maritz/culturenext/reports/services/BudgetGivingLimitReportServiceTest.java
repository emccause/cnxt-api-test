package com.maritz.culturenext.reports.services;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.springframework.core.env.Environment;
import org.springframework.mock.web.MockHttpServletResponse;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseDouble;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Budget;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.budget.dao.BudgetReportDao;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.date.constants.DateConstants;
import com.maritz.culturenext.reports.constants.BudgetCsvReportConstants;
import com.maritz.culturenext.reports.dto.CsvColumnPropertiesDTO;
import com.maritz.culturenext.reports.service.impl.BudgetCsvReportServiceImpl;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.test.AbstractMockTest;

public class BudgetGivingLimitReportServiceTest extends AbstractMockTest {

    private static final Object TEST_BUDGET_NAME_1 = "TEST_BUDGET_NAME_1";
    private static final Object TEST_BUDGET_DISPLAY_NAME_1 = "TEST_BUDGET_DISPLAY_NAME_1" ;
    private static final Object TEST_BUDGET_GIVING_LIMIT_1 = 10d;
    private static final Object TEST_START_DATE_1 =  DateUtil.convertToStartOfDayString("2016-01-01", TimeZone.getDefault());
    private static final Object TEST_END_DATE_1 = DateUtil.convertToStartOfDayString("2017-01-01", TimeZone.getDefault());
    private static final Object TEST_BUDGET_USED_1 = 6d;
    private static final Object TEST_BUDGET_REMAINING_1 = 4d;
    private static final Object TEST_CONTROL_NUM_1 = "1";
    private static final Object TEST_FIRST_NAME_1 = "Name1";
    private static final Object TEST_LAST_NAME_1 = "LastName1";
    private static final Object TEST_PAX_AMOUNT_USED_1 = 2d;
    private static final Object TEST_BUDGET_NAME_2 = "TEST_BUDGET_NAME_2";
    private static final Object TEST_BUDGET_DISPLAY_NAME_2 = "TEST_BUDGET_DISPLAY_NAME_2";
    private static final Object TEST_BUDGET_GIVING_LIMIT_2 = 20d;
    private static final Object TEST_START_DATE_2 = DateUtil.convertToStartOfDayString("2020-01-01", TimeZone.getDefault());
    private static final Object TEST_END_DATE_2 = null;
    private static final Object TEST_BUDGET_USED_2 = 15d;
    private static final Object TEST_BUDGET_REMAINING_2 = 5d;
    private static final Object TEST_CONTROL_NUM_2 = "2";
    private static final Object TEST_FIRST_NAME_2 = "Name2";
    private static final Object TEST_LAST_NAME_2 = "LastName2";
    private static final Object TEST_PAX_AMOUNT_USED_2 = 8d;


    @InjectMocks private BudgetCsvReportServiceImpl budgetReportService;

    @Mock private BudgetReportDao budgetReportDao;
    @Mock private ConcentrixDao<Budget> budgetDao;
    @Mock private Security security;
    @Mock private Environment environment;

    @SuppressWarnings("unchecked")
    private FindBy<Budget> budgetFindby = PowerMockito.mock(FindBy.class);

    private MockHttpServletResponse response = new MockHttpServletResponse();

    @Rule public ExpectedException thrown= ExpectedException.none();

    @Before
    public void setup() {
        // Mock objects.
        PowerMockito.when(budgetDao.findBy()).thenReturn(budgetFindby);
        PowerMockito.when(budgetFindby.where(anyString())).thenReturn(budgetFindby);
        PowerMockito.when(budgetFindby.in(anyListOf(String.class))).thenReturn(budgetFindby);
        PowerMockito.when(security.hasRole("ADMIN")).thenReturn(true);
        Mockito.when(environment.getProperty("reports.logging.enabled", String.class)).thenReturn("true");
    }

    @Test
    public void test_createBudgetGivingLimitReport_fail_budgetIds_invalid() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID));

        // Method arguments
        String budgetIdsDelim = "INVALID_IDS";
        String statusDelim = StatusTypeCode.ACTIVE.name();

        // Mocking inner services result.
        // Budget id not found.
        PowerMockito.when(budgetFindby.findDistinct(ProjectConstants.ID, Long.class)).thenReturn(null);

        // Testing method
        budgetReportService.createBudgetGivingLimitReport(response, budgetIdsDelim, statusDelim);

    }

    @Test
    public void test_createBudgetGivingLimitReport_fail_one_budgetId_invalid() {

        // Error expected
        thrown.expect(ErrorMessageException.class);
        thrown.expectMessage(Matchers.containsString(BudgetCsvReportConstants.ERROR_INVALID_BUDGET_ID));

        // Method arguments
        String budgetIdsDelim = "1, XX";
        String statusDelim = StatusTypeCode.ACTIVE.name();

        // Mocking inner services result.
        // Two budget ids were expected but it only one is returned.
        PowerMockito.when(budgetFindby.findDistinct(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1));

        // Testing method
        budgetReportService.createBudgetGivingLimitReport(response, budgetIdsDelim, statusDelim);

    }

    @Test
    public void test_createBudgetGivingLimitReport_empty_report() {

        // Method arguments
        String budgetIdsDelim = "1,2,10,200";
        String statusDelim = StatusTypeCode.ACTIVE.name();

        // Mocking inner services result.
        PowerMockito.when(budgetFindby.findDistinct(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1));
        PowerMockito.when(budgetReportDao.getBudgetGivingLimit(anyString(), anyString()))
            .thenReturn( new ArrayList<Map<String, Object>>());

        // Testing method
        budgetReportService.createBudgetGivingLimitReport(response, budgetIdsDelim, statusDelim);

        // Verify that inner service's method were correctly called.
        Mockito.verify(budgetReportDao, times(1)).getBudgetGivingLimit(budgetIdsDelim, statusDelim);

        // Checking file content
        List<Map<String, Object>> responseContent = getCSVFileContent(response);
        assertThat(responseContent.size(), equalTo(0));
    }

    @Test
    public void test_createBudgetGivingLimitReport_success_default_inputs_admin() {

        // Method arguments
        String budgetIdsDelim = null;
        String statusDelim = null;

        // Mocking inner services result.
        PowerMockito.when(budgetFindby.findDistinct(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1));

        List<Map<String, Object>> mockedReportContent = createReportContent ();
        PowerMockito.when(budgetReportDao.getBudgetGivingLimit(anyString(), anyString())).thenReturn(mockedReportContent);

        // Testing method
        budgetReportService.createBudgetGivingLimitReport(response, budgetIdsDelim, statusDelim);

        // Verify that inner service's method were correctly called.
        Mockito.verify(budgetReportDao, times(1)).getBudgetGivingLimit(budgetIdsDelim, statusDelim);

        // Checking file content
        List<Map<String, Object>> responseContent = getCSVFileContent(response);
        assertThat(responseContent, equalTo(mockedReportContent));
    }

    @Test
    public void test_createBudgetGivingLimitReport_success_default_inputs_non_admin() {

        // Method arguments
        String budgetIdsDelim = null;
        String statusDelim = null;

        // Mocking inner services result.
        PowerMockito.when(security.hasRole("ADMIN")).thenReturn(false);
        PowerMockito.when(budgetFindby.findDistinct(ProjectConstants.ID, Long.class)).thenReturn(Arrays.asList(TestConstants.ID_1));

        List<Map<String, Object>> mockedReportContent = createReportContent ();
        PowerMockito.when(budgetReportDao.getBudgetGivingLimit(anyString(), anyString())).thenReturn(mockedReportContent);

        // Testing method
        budgetReportService.createBudgetGivingLimitReport(response, budgetIdsDelim, statusDelim);

        // Verify that inner service's method were correctly called.
        Mockito.verify(budgetReportDao, times(1)).getBudgetGivingLimit("", statusDelim);

        // Checking file content
        List<Map<String, Object>> responseContent = getCSVFileContent(response);
        assertThat(responseContent, equalTo(mockedReportContent));
    }


    /**
     * Read CSV file and returning the content in a map
     * @param response mocked HttpServletResponse
     * @return file content in a map
     */
    private List<Map<String, Object>> getCSVFileContent(MockHttpServletResponse response) {

        List<Map<String, Object>> reportMap = new ArrayList<>();

        List<CsvColumnPropertiesDTO> cvsColumnProperties = new ArrayList<>(
                BudgetCsvReportConstants.BUDGET_GIVING_LIMIT_CSV_COLUMNS_PROPS);

        // Modifying cvsColumnProperties processors on date types, for reading purpose (ParseDate instead of FmtDate).
        cvsColumnProperties.set(4, new CsvColumnPropertiesDTO(BudgetCsvReportConstants.BUDGET_START_DATE_HEADER
                , ProjectConstants.START_DATE
                , new Optional(new ParseDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT))));

        cvsColumnProperties.set(5, new CsvColumnPropertiesDTO(BudgetCsvReportConstants.BUDGET_END_DATE_HEADER
                , ProjectConstants.END_DATE
                , new Optional(new ParseDate(DateConstants.ISO_8601_DATE_ONLY_FORMAT))));

        cvsColumnProperties.set(7, new CsvColumnPropertiesDTO(BudgetCsvReportConstants.BUDGET_REMAINING_HEADER
                , BudgetCsvReportConstants.BUDGET_REMAINING,(new ParseDouble())));

        try {

            String content = response.getContentAsString();
            Reader reader = new StringReader(content);


            reportMap = CsvFileUtil.readCSVFILEIntoMap(reader, cvsColumnProperties);

        } catch (Exception e) {
            fail("Getting report is throwing an error: " + e.getMessage());
        }

        return reportMap;
    }

    /**
     * Mocked budget giving limit query result for testing.
     * @return
     */
    private List<Map<String, Object>> createReportContent() {
        List<Map<String, Object>> reportContent = new ArrayList<>();

        Map<String, Object> row1 = new HashMap<>();
        row1.put(ProjectConstants.BUDGET_ID, TestConstants.ID_1);
        row1.put(ProjectConstants.BUDGET_NAME, TEST_BUDGET_NAME_1);
        row1.put(ProjectConstants.BUDGET_DISPLAY_NAME, TEST_BUDGET_DISPLAY_NAME_1);
        row1.put(BudgetCsvReportConstants.BUDGET_GIVING_LIMIT, TEST_BUDGET_GIVING_LIMIT_1);
        row1.put(ProjectConstants.START_DATE, TEST_START_DATE_1);
        row1.put(ProjectConstants.END_DATE, TEST_END_DATE_1);
        row1.put(BudgetCsvReportConstants.BUDGET_USED, TEST_BUDGET_USED_1);
        row1.put(BudgetCsvReportConstants.BUDGET_REMAINING, TEST_BUDGET_REMAINING_1);
        row1.put(ProjectConstants.CONTROL_NUM, TEST_CONTROL_NUM_1);
        row1.put(ProjectConstants.FIRST_NAME, TEST_FIRST_NAME_1);
        row1.put(ProjectConstants.LAST_NAME, TEST_LAST_NAME_1);
        row1.put(BudgetCsvReportConstants.PAX_AMOUNT_USED, TEST_PAX_AMOUNT_USED_1);
        row1.put(ProjectConstants.STATUS, StatusTypeCode.ACTIVE.name());
        reportContent.add(row1);

        Map<String, Object> row2 = new HashMap<>();
        row2.put(ProjectConstants.BUDGET_ID, TestConstants.ID_2);
        row2.put(ProjectConstants.BUDGET_NAME, TEST_BUDGET_NAME_2);
        row2.put(ProjectConstants.BUDGET_DISPLAY_NAME, TEST_BUDGET_DISPLAY_NAME_2);
        row2.put(BudgetCsvReportConstants.BUDGET_GIVING_LIMIT, TEST_BUDGET_GIVING_LIMIT_2);
        row2.put(ProjectConstants.START_DATE, TEST_START_DATE_2);
        row2.put(ProjectConstants.END_DATE, TEST_END_DATE_2);
        row2.put(BudgetCsvReportConstants.BUDGET_USED, TEST_BUDGET_USED_2);
        row2.put(BudgetCsvReportConstants.BUDGET_REMAINING, TEST_BUDGET_REMAINING_2);
        row2.put(ProjectConstants.CONTROL_NUM, TEST_CONTROL_NUM_2);
        row2.put(ProjectConstants.FIRST_NAME, TEST_FIRST_NAME_2);
        row2.put(ProjectConstants.LAST_NAME, TEST_LAST_NAME_2);
        row2.put(BudgetCsvReportConstants.PAX_AMOUNT_USED, TEST_PAX_AMOUNT_USED_2);
        row2.put(ProjectConstants.STATUS, StatusTypeCode.NEW.name());
        reportContent.add(row2);

        return reportContent;
    }
}
