package com.maritz.culturenext.reports.services;


import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.ApprovalHistory;
import com.maritz.core.jpa.entity.Recognition;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.core.security.Security;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.enums.TableName;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.impl.RecognitionServiceImpl;
import com.maritz.culturenext.reports.constants.ReportConstants;
import com.maritz.culturenext.reports.dao.ApprovalReportDao;
import com.maritz.culturenext.reports.dto.ApprovalReportDTO;
import com.maritz.culturenext.reports.service.impl.RecognitionReportServiceImpl;
import com.maritz.culturenext.util.CsvFileUtil;
import com.maritz.culturenext.util.PaxUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import org.springframework.core.env.Environment;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.management.*")
@PrepareForTest({CsvFileUtil.class})
public class RecognitionReportServiceTest {

    @InjectMocks private RecognitionReportServiceImpl recognitionReportService;
    @Mock private ApprovalReportDao approvalReportDao;
    @Mock private ConcentrixDao<ApprovalHistory> approvalHistoryDao;
    @Mock private ConcentrixDao<Recognition> recognitionDao;
    @Mock private ConcentrixDao<Relationship> relationshipDao;
    @Mock private CsvFileUtil csvFileUtil;
    @Mock private HttpServletResponse response;
    @Mock private PaxUtil paxUtil;
    @Mock private RecognitionServiceImpl recognitionService;
    @Mock private Security security;
    @Mock private Environment environment;

    private FindBy<ApprovalHistory> approvalHistoryFindBy = PowerMockito.mock(FindBy.class);
    private FindBy<Relationship> relationshipFindBy = PowerMockito.mock(FindBy.class);

    private final String FROM_DATE = "2014-01-01";
    private final String TO_DATE = "2015-01-01";

    private final Long USER_PAX = 1234L;
    private final Long MANAGER_PAX = 789L;
    private final Long BUDGET_ID = 32L;

    @Before
    public void setup () {
        MockitoAnnotations.initMocks(this);
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE,
                ProjectConstants.MANAGER_ROLE)).thenReturn(true);

        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setFirstName("Joe");
        employeeDTO.setLastName("Smith");
        employeeDTO.setPaxId(USER_PAX);
        employeeDTO.setManagerPaxId(MANAGER_PAX);

        Map<Long, EmployeeDTO> employeePax = new HashMap<>();
        employeePax.put(USER_PAX, employeeDTO);

        List<RecognitionDetailsDTO> testRecognitionDetailsList = new ArrayList<>();
        RecognitionDetailsDTO recognitionDetails = new RecognitionDetailsDTO();
        recognitionDetails.setProgram("Program Name");
        recognitionDetails.setApprovalComment("Some Comment");
        recognitionDetails.setBudgetId(BUDGET_ID);
        recognitionDetails.setId(TestConstants.ID_1);
        recognitionDetails.setFromPax(employeeDTO);
        recognitionDetails.setToPax(employeeDTO);
        recognitionDetails.setApprovalPax(employeeDTO);
        testRecognitionDetailsList.add(recognitionDetails);


        List<ApprovalHistory> testApprovalHistoryList = new ArrayList<>();
        ApprovalHistory approvalHistory = new ApprovalHistory();
        approvalHistory.setId(TestConstants.ID_1);
        approvalHistory.setPaxId(USER_PAX);
        approvalHistory.setStatusTypeCode(StatusTypeCode.APPROVED.toString());
        approvalHistory.setApprovalNotes("Some Text");
        approvalHistory.setTargetId(TestConstants.ID_1);
        approvalHistory.setTargetTable(TableName.RECOGNITION.name());
        approvalHistory.setApprovalDate(new Date());
        testApprovalHistoryList.add(approvalHistory);

        List<Map<String, Object>> approvalHistoryDaoList = new ArrayList<>();
        Map<String, Object> approvalHistoryNode = new HashMap<>();
        approvalHistoryNode.put(ProjectConstants.ID, TestConstants.ID_1);
        approvalHistoryNode.put(ProjectConstants.PAX_ID, USER_PAX);
        approvalHistoryNode.put(ProjectConstants.APPROVAL_PROCESS_ID, null);
        approvalHistoryNode.put(ProjectConstants.TARGET_ID, TestConstants.ID_1);
        approvalHistoryNode.put(ProjectConstants.TARGET_TABLE, TableName.RECOGNITION.name());
        approvalHistoryNode.put(ProjectConstants.PROXY_PAX_ID, null);
        approvalHistoryNode.put(ProjectConstants.APPROVAL_DATE, new Date());
        approvalHistoryNode.put(ProjectConstants.APPROVAL_LEVEL, null);
        approvalHistoryNode.put(ProjectConstants.STATUS_TYPE_CODE, StatusTypeCode.APPROVED.toString());
        approvalHistoryNode.put(ProjectConstants.APPROVAL_NOTES, "Some Text");
        approvalHistoryDaoList.add(approvalHistoryNode);

        List<Map<String, Object>> approvalPendingDaoList = new ArrayList<>();
        Map<String, Object> approvalPendingNode = new HashMap<>();
        approvalPendingNode.put(ProjectConstants.ID, TestConstants.ID_1);
        approvalPendingNode.put(ProjectConstants.PAX_ID, USER_PAX);
        approvalPendingNode.put(ProjectConstants.APPROVAL_PROCESS_ID, null);
        approvalPendingNode.put(ProjectConstants.TARGET_ID, TestConstants.ID_1);
        approvalPendingNode.put(ProjectConstants.TARGET_TABLE, TableName.RECOGNITION.name());
        approvalPendingNode.put(ProjectConstants.NEXT_APPROVER_PAX_ID, null);

        List<Relationship> relationshipList = new ArrayList<>();
        Relationship relationship = new Relationship();
        relationship.setPaxId1(USER_PAX);
        relationship.setPaxId1(MANAGER_PAX);
        relationship.setRelationshipTypeCode(RelationshipType.REPORT_TO.toString());
        relationshipList.add(relationship);


        //Mock ApprovalHistory ConcentrixDao
        PowerMockito.when(approvalHistoryDao.findBy()).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.where(ProjectConstants.STATUS_TYPE_CODE))
                .thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.in(any(List.class))).thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.and(ProjectConstants.APPROVAL_DATE))
                .thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.between(any(Date.class), any(Date.class)))
                .thenReturn(approvalHistoryFindBy);
        PowerMockito.when(approvalHistoryFindBy.find()).thenReturn(testApprovalHistoryList);

        //Mock RelationShip ConcentrixDao
        PowerMockito.when(relationshipDao.findBy()).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.where(ProjectConstants.PAX_ID_2)).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.eq(any(Long.class))).thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.and(ProjectConstants.RELATIONSHIP_TYPE_CODE))
                .thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.eq(RelationshipType.REPORT_TO.toString()))
                .thenReturn(relationshipFindBy);
        PowerMockito.when(relationshipFindBy.find()).thenReturn(relationshipList);

        //Mock RecognitionService
        PowerMockito.when(recognitionService.getRecognitionDetailsByIds(any(List.class)))
                .thenReturn(testRecognitionDetailsList);
        PowerMockito.when(paxUtil.getPaxIdEmployeeDTOMap(any(List.class))).thenReturn(employeePax);

        //Mock CsvFileUtil
        PowerMockito.mockStatic(CsvFileUtil.class);
        PowerMockito.doNothing().when(CsvFileUtil.class);
        List<ApprovalReportDTO> approvalReportDTOs = new ArrayList<>();
        CsvFileUtil.writeDownloadCSVFile(response, "ApprovalReport", approvalReportDTOs,
                ReportConstants.PROCESSOR, ReportConstants.HEADERS,null);

        //Mock ApprovalReportDao calls
        PowerMockito.when(approvalReportDao.getApprovalHistoryByApproversForAdmin(any(List.class), 
                any(Date.class), any(Date.class), any(Long.class))).thenReturn(approvalHistoryDaoList);
        PowerMockito.when(approvalReportDao.getApprovalPendingByApproversForAdmin(any(Date.class), 
                any(Date.class), any(Long.class))).thenReturn(approvalPendingDaoList);
        PowerMockito.when(approvalReportDao.getApprovalHistoryByApproversForManager(any(List.class), 
                any(Date.class), any(Date.class), any(List.class))).thenReturn(approvalHistoryDaoList);
        PowerMockito.when(approvalReportDao.getApprovalPendingByApproversForManager(any(Date.class),
                any(Date.class), any(List.class))).thenReturn(approvalPendingDaoList);
        PowerMockito.when(approvalReportDao.getApprovalHistoryByGiversForAdmin(any(List.class), 
                any(Date.class), any(Date.class), any(Long.class))).thenReturn(approvalHistoryDaoList);
        PowerMockito.when(approvalReportDao.getApprovalPendingByGiversForManager(any(Date.class), 
                any(Date.class), any(List.class))).thenReturn(approvalPendingDaoList);
        PowerMockito.when(approvalReportDao.getApprovalPendingByGiversForAdmin(any(Date.class), 
                any(Date.class), any(Long.class))).thenReturn(approvalHistoryDaoList);
        PowerMockito.when(approvalReportDao.getApprovalHistoryByGiversForManager(any(List.class), 
                any(Date.class), any(Date.class), any(List.class))).thenReturn(approvalPendingDaoList);

        Mockito.when(environment.getProperty("reports.logging.enabled", String.class)).thenReturn("true");
    }

    @Test
    public void test_throw_exception_when_not_admin_nor_manager () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE, 
                ProjectConstants.MANAGER_ROLE)).thenReturn(false);

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(),
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

    }

    @Test
    public void test_throw_exception_when_no_recognitionType () {

        try {
            recognitionReportService.getApprovalsReport(response, null, ReportConstants.VALID_STATUS,
                    FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

    }

    @Test
    public void test_throw_exception_when_invalid_recognitionType () {

        try {
            recognitionReportService.getApprovalsReport(response, "RECEIVED", ReportConstants.VALID_STATUS
                    , FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_throw_exception_when_no_status () {

        try {
            recognitionReportService.getApprovalsReport(response, ReportConstants.GIVEN, null,
                    FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_throw_exception_when_invalid_status () {

        List<String> statues = new ArrayList<>(Arrays.asList(StatusTypeCode.PENDING.toString(), "RECEIVED"));

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(),
                    statues, FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_throw_exception_when_no_fromDate () {

        try {
            recognitionReportService.getApprovalsReport(response, ReportConstants.GIVEN, 
                    ReportConstants.VALID_STATUS, null, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_throw_exception_when_invalid_fromDate () {
        String invalidDate = "12-22-2015";

        try {
            recognitionReportService.getApprovalsReport(response, ReportConstants.GIVEN, 
                    ReportConstants.VALID_STATUS, invalidDate, TO_DATE, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }

    }

    @Test
    public void test_throw_exception_when_no_toDate () {

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(), 
                    ReportConstants.VALID_STATUS, FROM_DATE, null, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_throw_exception_when_invalid_toDate () {
        String invalidDate = "2015/08/15";

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(), 
                    ReportConstants.VALID_STATUS, FROM_DATE, invalidDate, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }


    @Test
    public void test_throw_exception_when_from_date_after_to_date () {
        String fromDate = "2015-01-01";
        String toDate = "2014-01-01";

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(), 
                    ReportConstants.VALID_STATUS, fromDate, toDate, null);
        } catch (ErrorMessageException e) {
            assertThat(e.getErrorMessages().size(), is(1));
        }
    }

    @Test
    public void test_happy_path_for_admin_with_no_groupId () {
        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(), 
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            fail();
        }

    }

    @Test
    public void test_happy_path_for_admin_query_approved_with_groupId () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(),
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, 4L);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_happy_path_for_manager_query_approved () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.hasRole(ProjectConstants.MANAGER_ROLE)).thenReturn(true);
        PowerMockito.when(security.getPaxId()).thenReturn(MANAGER_PAX);

        try {
            recognitionReportService.getApprovalsReport(response, StatusTypeCode.APPROVED.toString(), 
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            fail();
        }

    }

    @Test
    public void test_happy_path_for_admin_query_given_with_groupId () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(true);

        try {
            recognitionReportService.getApprovalsReport(response, ReportConstants.GIVEN,
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, 4L);
        } catch (ErrorMessageException e) {
            fail();
        }
    }

    @Test
    public void test_happy_path_for_manager_given_approved () {

        PowerMockito.when(security.hasRole(ProjectConstants.ADMIN_ROLE, ProjectConstants.CLIENT_ADMIN_ROLE))
                .thenReturn(false);
        PowerMockito.when(security.hasRole(ProjectConstants.MANAGER_ROLE)).thenReturn(true);
        PowerMockito.when(security.getPaxId()).thenReturn(MANAGER_PAX);

        try {
            recognitionReportService.getApprovalsReport(response, ReportConstants.GIVEN,
                    ReportConstants.VALID_STATUS, FROM_DATE, TO_DATE, null);
        } catch (ErrorMessageException e) {
            fail();
        }

    }


}
