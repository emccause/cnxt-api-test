package com.maritz.culturenext.reports.dao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.reports.dao.BudgetTransactionReportDao;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class BudgetTransactionReportDaoTest extends AbstractDatabaseTest {

    @Inject private BudgetTransactionReportDao budgetTransactionReportDao;

    //Report Parameters - Defaults
    public static final Date FROM_DATE_PARAM = 
            DateUtil.convertToStartOfDayString("2016-05-01", TimeZone.getDefault());
    public static final Date THRU_DATE_PARAM = 
            DateUtil.convertToEndOfDayString("2016-05-31", TimeZone.getDefault());
    public static final Long GROUP_ID_PARAM = 4L;
    public static final Long PAX_TEAM_PARAM = null;
    public static final    Long PAX_ORG_PARAM = null;
    public static final String TYPE_LIST_PARAM = "GIVEN,RECEIVED";
    public static final List<String> ALL_STATUS_LIST_PARAM =  Arrays.asList(
            StatusTypeCode.ACTIVE.name(),StatusTypeCode.ARCHIVED.name(),StatusTypeCode.SCHEDULED.name(),StatusTypeCode.ENDED.name());
    public static final String ALL_STATUS_LIST_STRING = 
            StatusTypeCode.ACTIVE.name() + ", " + StatusTypeCode.ARCHIVED.name() + ", " + 
                    StatusTypeCode.SCHEDULED.name() + ", " + StatusTypeCode.ENDED.name();
    public static final List<String> ACTIVE_STATUS_LIST_PARAM =  Arrays.asList(StatusTypeCode.ACTIVE.name());
    public static final    List<String> BUDGET_ID_LIST_PARAM = Arrays.asList("1");
    public static final String BUDGET_ID_LIST_STRING = "1";

    //First row in the result set - pulled from test DB
    public static final Long TRANSACTION_ID = 48349L;
    public static final Long NOMINATION_ID = 70757L;
    public static final String SUBMITTED_DATE = "Mon Jun 13 20:12:19 UTC 2016";
    public static final String RECIPIENT_ID = "969";
    public static final String RECIPIENT_FIRST_NAME = "Sunitha";
    public static final String RECIPIENT_LAST_NAME = "Kumar";
    public static final String SUBMITTER_ID = "8987";
    public static final String SUBMITTER_FIRST_NAME = "Alex";
    public static final String SUBMITTER_LAST_NAME = "Mudd";
    public static final String APPROVER_ID = "847";
    public static final String APPROVER_FIRST_NAME = "Greg";
    public static final String APPROVER_LAST_NAME = "Porter";
    public static final Long PROGRAM_ID = 13567L;
    public static final String PROGRAM_NAME = "NEWSFEED_STATUS_TEST_PROGRAM";
    public static final String VALUE = "Achievement";
    public static final String HEADLINE = "NEWSFEED_STATUS_TEST";
    public static final Double AWARD_AMOUNT = 5.0;
    public static final String PAYOUT_TYPE = "DPP";
    public static final String BUDGET_NAME = "Leah's Super Awesome Budget";
    
    @Test
    public void get_budget_transactions_report_good() throws Exception {

        List<Map<String,Object>> nodes = budgetTransactionReportDao.getBudgetTransactionReportData(
                ALL_STATUS_LIST_STRING, BUDGET_ID_LIST_STRING);

        assertTrue(nodes.size() > 0);

        Map<String,Object> result1 = nodes.get(0);
        
        //iterate to the required Transaction
        for (Map<String,Object> result2 : nodes) {
            if (TRANSACTION_ID.equals(result2.get(ProjectConstants.TRANSACTION_ID))) { 
                result1 = result2; 
                break; 
            }
        }
        
        //Only verify one record
        //All pax-misc info will be null at this point because we never called UP_GET_PAX_INFO
        assertNotNull(result1);
        assertEquals(result1.get(ProjectConstants.TRANSACTION_ID), TRANSACTION_ID);
        assertEquals(result1.get(ProjectConstants.NOMINATION_ID), NOMINATION_ID);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_PAX_ID), TestConstants.PAX_KUMARSJ);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_ID), RECIPIENT_ID);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_FIRST_NAME), RECIPIENT_FIRST_NAME);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_LAST_NAME), RECIPIENT_LAST_NAME);
        assertNull(result1.get(ProjectConstants.RECIPIENT_EMAIL));
        assertNull(result1.get(ProjectConstants.RECIPIENT_CITY));
        assertNull(result1.get(ProjectConstants.RECIPIENT_STATE));
        assertNull(result1.get(ProjectConstants.RECIPIENT_POSTAL_CODE));
        assertNull(result1.get(ProjectConstants.RECIPIENT_COUNTRY_CODE));
        assertNull(result1.get(ProjectConstants.RECIPIENT_TITLE));
        assertEquals(result1.get(ProjectConstants.SUBMITTER_PAX_ID), TestConstants.PAX_MUDDAM);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_ID), SUBMITTER_ID);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_FIRST_NAME), SUBMITTER_FIRST_NAME);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_LAST_NAME), SUBMITTER_LAST_NAME);
        assertNull(result1.get(ProjectConstants.SUBMITTER_EMAIL));
        assertNull(result1.get(ProjectConstants.SUBMITTER_CITY));
        assertNull(result1.get(ProjectConstants.SUBMITTER_STATE));
        assertNull(result1.get(ProjectConstants.SUBMITTER_POSTAL_CODE));
        assertNull(result1.get(ProjectConstants.SUBMITTER_COUNTRY_CODE));
        assertNull(result1.get(ProjectConstants.SUBMITTER_TITLE));
        assertEquals(result1.get(ProjectConstants.APPROVER_PAX_ID), TestConstants.PAX_PORTERGA);
        assertEquals(result1.get(ProjectConstants.APPROVER_ID), APPROVER_ID);
        assertEquals(result1.get(ProjectConstants.APPROVER_FIRST_NAME), APPROVER_FIRST_NAME);
        assertEquals(result1.get(ProjectConstants.APPROVER_LAST_NAME), APPROVER_LAST_NAME);
        assertNull(result1.get(ProjectConstants.APPROVER_CITY));
        assertNull(result1.get(ProjectConstants.APPROVER_STATE));
        assertEquals(result1.get(ProjectConstants.PROGRAM_ID), PROGRAM_ID);
        assertEquals(result1.get(ProjectConstants.PROGRAM_NAME), PROGRAM_NAME);
        assertEquals(result1.get(ProjectConstants.PROGRAM_TYPE), ProgramTypeEnum.PEER_TO_PEER.name());
        assertEquals(result1.get(ProjectConstants.RECOGNITION_STATUS), StatusTypeCode.PENDING.name());
        assertEquals(result1.get(ProjectConstants.AWARD_AMOUNT), AWARD_AMOUNT);
        assertEquals(result1.get(ProjectConstants.PAYOUT_TYPE), PAYOUT_TYPE);
        assertEquals(result1.get(ProjectConstants.BUDGET_NAME), BUDGET_NAME);
        assertEquals(result1.get(ProjectConstants.BUDGET_STATUS), StatusTypeCode.ACTIVE.name());
        assertEquals(result1.get(ProjectConstants.BUDGET_ID), TestConstants.ID_1);
    }

    @Test
    public void get_budget_transactions_report_verify_budget_status() throws Exception {

        List<Map<String,Object>> nodes = budgetTransactionReportDao.getBudgetTransactionReportData(
                ALL_STATUS_LIST_STRING, null);

        assertTrue(nodes.size() > 0);

        //Verify that all returned records are ACTIVE
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.BUDGET_STATUS), StatusTypeCode.ACTIVE.name());
        }
    }

    
    @Test
    public void get_budget_transactions_report_verify_budget_id() throws Exception {

        List<Map<String,Object>> nodes = budgetTransactionReportDao.getBudgetTransactionReportData(
                ALL_STATUS_LIST_STRING, BUDGET_ID_LIST_STRING);

        assertTrue(nodes.size() > 0 );

        //Verify that all returned records are tied to BUDGET_ID 1
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.BUDGET_ID), TestConstants.ID_1);
        }
    }
}
