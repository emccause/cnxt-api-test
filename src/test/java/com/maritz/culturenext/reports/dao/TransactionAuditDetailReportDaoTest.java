package com.maritz.culturenext.reports.dao;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.reports.dao.TransactionAuditDetailReportDao;
import com.maritz.test.AbstractDatabaseTest;

public class TransactionAuditDetailReportDaoTest extends AbstractDatabaseTest {
    
    @Inject private TransactionAuditDetailReportDao transactionDetailReportDao;
    
    @Test
    public void test_getTransactionDetail_default_inputs() {
        List<Map<String, Object>> result = transactionDetailReportDao.getTransactionDetailReportData(null);
        
        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));
        
        verifyResultRowContent(result.get(16));
    }
    
    @Test
    public void test_getTransactionDetail_filteredBy_status() {

        List<String> statusList = new ArrayList<String>();
        statusList.add(StatusTypeCode.REQUESTED.name());
        statusList.add(StatusTypeCode.ERROR.name());
        List<Map<String, Object>> result = transactionDetailReportDao.getTransactionDetailReportData(statusList);

        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));

        for (Map<String, Object> row : result) {
            if (("".equals(String.valueOf(row.get(ProjectConstants.PAYOUT_ERROR_DESC))))) {
                fail("Query filter by status is not working as expected");
            }
        }

        verifyResultRowContent(result.get(0));
    }
    
    
    @Test
    public void test_getTransactionDetail_filteredBy_pending_status() {

        List<String> statusList = new ArrayList<String>();
        statusList.add(StatusTypeCode.PENDING.name());
        
        List<Map<String, Object>> result = transactionDetailReportDao.getTransactionDetailReportData(statusList);
        
        assertNotNull(result);
        assertThat(result.size(), greaterThan(0));
        
        for (Map<String,Object> row : result){
            if (!(StatusTypeCode.PENDING.name().equals(String.valueOf(row.get(ProjectConstants.STATUS))))) {
                fail("Query filter by pending status is not working as expected");
            }
        }
        
        verifyResultRowContent(result.get(0));

    }
    
    private void verifyResultRowContent(Map<String,Object> row) {
        assertNotNull(row);
        assertNotNull(row.get(ProjectConstants.PAYOUT_ID));
        assertThat(row.get(ProjectConstants.PAYOUT_ID), instanceOf(Long.class));
        
        assertNotNull(row.get(ProjectConstants.PAYOUT_STATUS_DATE));
        assertThat(row.get(ProjectConstants.PAYOUT_STATUS_DATE), instanceOf(Date.class));
        
        assertNotNull(row.get(ProjectConstants.PARTICIPANT_ID));
        assertThat(row.get(ProjectConstants.PARTICIPANT_ID), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.PARTICIPANT_FIRST_NAME));
        assertThat(row.get(ProjectConstants.PARTICIPANT_FIRST_NAME), instanceOf(String.class));

        assertNotNull(row.get(ProjectConstants.PARTICIPANT_LAST_NAME));
        assertThat(row.get(ProjectConstants.PARTICIPANT_LAST_NAME), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.AWARD_AMOUNT));
        assertThat(row.get(ProjectConstants.AWARD_AMOUNT), instanceOf(Double.class));
        
        assertNotNull(row.get(ProjectConstants.TRANSACTION_ID));
        assertThat(row.get(ProjectConstants.TRANSACTION_ID), instanceOf(Long.class));
        
        // can be null for things like point load
        if (row.get(ProjectConstants.NOMINATION_ID) != null) {
            assertThat(row.get(ProjectConstants.NOMINATION_ID), instanceOf(Long.class));
        }

        if (row.get(ProjectConstants.SUBMITTER_ID) != null) {
            assertThat(row.get(ProjectConstants.SUBMITTER_ID), instanceOf(String.class));
        }

        if (row.get(ProjectConstants.SUBMITTER_FIRST_NAME) != null) {
            assertThat(row.get(ProjectConstants.SUBMITTER_FIRST_NAME), instanceOf(String.class));
        }

        if (row.get(ProjectConstants.SUBMITTER_LAST_NAME) != null) {
            assertThat(row.get(ProjectConstants.SUBMITTER_LAST_NAME), instanceOf(String.class));
        }
        
        assertNotNull(row.get(ProjectConstants.BUDGET_NAME));
        assertThat(row.get(ProjectConstants.BUDGET_NAME), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.PROJECT_NUMBER));
        assertThat(row.get(ProjectConstants.PROJECT_NUMBER), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.SUB_PROJECT_NUMBER));
        assertThat(row.get(ProjectConstants.SUB_PROJECT_NUMBER), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.STATUS));
        assertThat(row.get(ProjectConstants.STATUS), instanceOf(String.class));
        
        assertNotNull(row.get(ProjectConstants.PAYOUT_ERROR_DESC));
        assertThat(row.get(ProjectConstants.PAYOUT_ERROR_DESC), instanceOf(String.class));
        
    }
}

