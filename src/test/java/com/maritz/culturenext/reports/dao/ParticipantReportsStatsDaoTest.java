package com.maritz.culturenext.reports.dao;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.culturenext.enums.ProgramTypeEnum;
import com.maritz.culturenext.util.DateUtil;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

import org.junit.Test;

import javax.inject.Inject;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ParticipantReportsStatsDaoTest extends AbstractDatabaseTest {

    @Inject private ParticipantReportsDao participantReportsDao;

    //Report Parameters - Defaults
    public static final Date FROM_DATE_PARAM = 
            DateUtil.convertToStartOfDayString("2015-01-01", TimeZone.getDefault());
    public static final Date THRU_DATE_PARAM = 
            DateUtil.convertToEndOfDayString("2015-12-31", TimeZone.getDefault());
    public static final Long GROUP_ID_PARAM = 4L;
    public static final Long PAX_TEAM_PARAM = null;
    public static final    Long PAX_ORG_PARAM = null;
    public static final String TYPE_LIST_PARAM = "GIVEN,RECEIVED";
    public static final String STATUS_LIST_PARAM = "APPROVED,PENDING,REJECTED";
    public static final    String PROGRAM_TYPE_LIST_PARAM = 
            "PEER_TO_PEER,MANAGER_DISCRETIONARY,ECARD_ONLY,POINT_LOAD,AWARD_CODE";
    public static final    String BAD_PROGRAM_TYPE_LIST_PARAM = 
            ";PEER_TO_PEER;MANAGER_DISCRETIONARY;ECARD_ONLY;POINT_LOAD;AWARD_CODE;";
    public static final    Long PROGRAM_ID_PARAM = null;
    public static final    Long BATCH_ID_PARAM = null;

    //First row in the result set - pulled from test DB
    public static final Long NOMINATION_ID = 57290L;
    public static final Long RECIPIENT_PAX_ID = 11840L;
    public static final String RECIPIENT_ID = "11840";
    public static final String RECIPIENT_FIRST_NAME = "Clint";
    public static final String RECIPIENT_LAST_NAME = "Sharp";
    public static final Long SUBMITTER_PAX_ID = 773L;
    public static final String SUBMITTER_ID = "773";
    public static final String SUBMITTER_FIRST_NAME = "Cara";
    public static final String SUBMITTER_LAST_NAME = "McVey";
    public static final String EMAIL_ADDRESS = "a@b.c";
    public static final Long PROGRAM_ID = 4L;
    public static final String PROGRAM_NAME = "Difference Makers (Points)";
    public static final String PROGRAM_TYPE = "PEER_TO_PEER";
    public static final String VALUE = "Client Focus";
    public static final String RECOGNITION_STATUS = "AUTO_APPROVED";

    @Test
    public void get_participant_transactions_report_good() throws Exception {

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, RECIPIENT_PAX_ID, GROUP_ID_PARAM, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, STATUS_LIST_PARAM, PROGRAM_TYPE_LIST_PARAM, 
                PROGRAM_ID_PARAM, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(67));

        Map<String,Object> result1 = nodes.get(0);
        
        //Only verify the first record of the result set
        assertNotNull(result1);
        assertNull(result1.get(ProjectConstants.TRANSACTION_ID));
        assertEquals(result1.get(ProjectConstants.NOMINATION_ID), NOMINATION_ID);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_PAX_ID), RECIPIENT_PAX_ID);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_ID), RECIPIENT_ID);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_FIRST_NAME), RECIPIENT_FIRST_NAME);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_LAST_NAME), RECIPIENT_LAST_NAME);
        assertEquals(result1.get(ProjectConstants.RECIPIENT_EMAIL), EMAIL_ADDRESS);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_PAX_ID), SUBMITTER_PAX_ID);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_ID), SUBMITTER_ID);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_FIRST_NAME), SUBMITTER_FIRST_NAME);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_LAST_NAME), SUBMITTER_LAST_NAME);
        assertEquals(result1.get(ProjectConstants.SUBMITTER_EMAIL), EMAIL_ADDRESS);
        assertNull(result1.get(ProjectConstants.APPROVER_PAX_ID));
        assertNull(result1.get(ProjectConstants.APPROVER_ID));
        assertNull(result1.get(ProjectConstants.APPROVER_FIRST_NAME));
        assertNull(result1.get(ProjectConstants.APPROVER_LAST_NAME));
        assertEquals(result1.get(ProjectConstants.PROGRAM_ID), PROGRAM_ID);
        assertEquals(result1.get(ProjectConstants.PROGRAM_NAME), PROGRAM_NAME);
        assertEquals(result1.get(ProjectConstants.PROGRAM_TYPE), PROGRAM_TYPE);
        assertEquals(result1.get(ProjectConstants.VALUE), VALUE);
        assertNull(result1.get(ProjectConstants.HEADLINE));
        assertEquals(result1.get(ProjectConstants.RECOGNITION_STATUS), RECOGNITION_STATUS);
        assertNull(result1.get(ProjectConstants.POINTS));
        assertNull(result1.get(ProjectConstants.BUDGET_NAME));
        assertNull(result1.get(ProjectConstants.BUDGET_STATUS));
        assertNull(result1.get(ProjectConstants.BUDGET_ID));
    }

    @Test
    public void get_participant_transactions_report_verify_status() throws Exception {

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, null, null, TestConstants.ID_2, 
                null, TYPE_LIST_PARAM, StatusTypeCode.PENDING.toString(), 
                PROGRAM_TYPE_LIST_PARAM, PROGRAM_ID_PARAM, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(8));

        //Verify that all returned records are PENDING
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.RECOGNITION_STATUS), StatusTypeCode.PENDING.toString());
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_issued_status() throws Exception {

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, RECIPIENT_PAX_ID, GROUP_ID_PARAM, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, StatusTypeCode.ISSUED.toString(), 
                PROGRAM_TYPE_LIST_PARAM, PROGRAM_ID_PARAM, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(4));

        String status = null;
        //Verify that all returned records are ISSUED
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            status = (String) node.get(ProjectConstants.RECOGNITION_STATUS);
            assertTrue(status.equalsIgnoreCase(StatusTypeCode.APPROVED.toString()) || 
                    status.equalsIgnoreCase(StatusTypeCode.AUTO_APPROVED.toString()));
            assertNotNull(node.get(ProjectConstants.ISSUANCE_DATE));
        }
    }

    @Test
    public void get_participant_transactions_report_verify_program_type() throws Exception {

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, SUBMITTER_PAX_ID, GROUP_ID_PARAM, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, STATUS_LIST_PARAM, 
                ProgramTypeEnum.MANAGER_DISCRETIONARY.toString(), PROGRAM_ID_PARAM, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(1));

        //Verify that all returned records have MANAGER_DISCRETIONARY program type
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.PROGRAM_TYPE), ProgramTypeEnum.MANAGER_DISCRETIONARY.toString());
        }
    }

    @Test
    public void get_participant_transactions_report_verify_program_id() throws Exception {
        
        Long programIdTest = 3L;

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, RECIPIENT_PAX_ID, GROUP_ID_PARAM, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, STATUS_LIST_PARAM, null, programIdTest, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(20));

        //Verify that all returned records are tied to PROGRAM_ID 3
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            assertEquals(node.get(ProjectConstants.PROGRAM_ID), 3L);
            assertEquals(node.get(ProjectConstants.PROGRAM_NAME), "Compass Kudos");
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_group_id() throws Exception {

        Long groupIdTest = 896L;
        Long clintSharpPaxId = 11840L;
        
        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, SUBMITTER_PAX_ID, groupIdTest, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, STATUS_LIST_PARAM, PROGRAM_TYPE_LIST_PARAM, 
                null, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(5));

        //Verify that all returned records are tied to GROUP_ID 896
        //Clint Sharp (11840) is the only person in that group, so he should be giver/receiver for every recognition
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            
            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);
            
            assertTrue(recipientPaxId.equals(clintSharpPaxId) || submitterPaxId.equals(clintSharpPaxId));
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_pax_team() throws Exception {
        
        Long paxTeamTest = 8571L; //Leah Harwell
        List<Long> leahsTeam = Arrays.asList(8571L, 14095L);
        
        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, null, null, paxTeamTest, 
                PAX_ORG_PARAM, TYPE_LIST_PARAM, STATUS_LIST_PARAM, PROGRAM_TYPE_LIST_PARAM, 
                null, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(145));

        //Verify that all returned records are tied to Leah's team
        //Leah's team consists of pax IDs 8571 and 14095
        //NOTE: Leah is not in group 4 which is why there are more records here than when we used the groupId = 4 filter
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            
            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);
            
            assertTrue(leahsTeam.contains(recipientPaxId) || leahsTeam.contains(submitterPaxId));
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_pax_org() throws Exception {
        
        Long paxOrgTest = 1074L; //Lisa Weaner
        List<Long> lisasOrg = Arrays.asList(1074L, 141L, 1135L, 3900L);

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, null, null, PAX_TEAM_PARAM, 
                paxOrgTest, TYPE_LIST_PARAM, STATUS_LIST_PARAM, PROGRAM_TYPE_LIST_PARAM, 
                null, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(2));

        //Verify that all returned records are tied to Lisa's team
        //Andy Kilinskis (Admin) (141) - Reports to Lisa
        //Dave Blatt (1135) - Reports to Lisa
        //Mark    Richter (Admin) (3900) - Reported to Dave Blatt from 2015-04-07 until 2015-06-04
        
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            
            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);
            
            assertTrue(lisasOrg.contains(recipientPaxId) || lisasOrg.contains(submitterPaxId));
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_type_given() throws Exception {
        
        Long groupIdTest = 896L;
        Long clintSharpPaxId = 11840L;
        
        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, RECIPIENT_PAX_ID, groupIdTest, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, "GIVEN", STATUS_LIST_PARAM, PROGRAM_TYPE_LIST_PARAM, 
                null, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(1));

        //Verify that all returned records are GIVEN by Clint Sharp
        //Clint Sharp (11840) is the only person in this group
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);
            
            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);
            
            assertTrue(!recipientPaxId.equals(clintSharpPaxId) && submitterPaxId.equals(clintSharpPaxId));
        }
    }
    
    @Test
    public void get_participant_transactions_report_verify_type_received() throws Exception {
        
        Long groupIdTest = 896L;
        Long clintSharpPaxId = 11840L;

        List<Map<String,Object>> nodes = participantReportsDao.getParticipantTransactionReportData(
                FROM_DATE_PARAM, THRU_DATE_PARAM, SUBMITTER_PAX_ID, groupIdTest, PAX_TEAM_PARAM, 
                PAX_ORG_PARAM, "RECEIVED", STATUS_LIST_PARAM, null, 
                PROGRAM_ID_PARAM, BATCH_ID_PARAM);

        assertThat(nodes.size(), is(4));

        //Verify that all returned records are RECEIVED by Clint Sharp
        //Clint Sharp (11840) is the only person in this group
        for (Map<String,Object> node : nodes) {
            assertNotNull(node);

            Long recipientPaxId = (Long) node.get(ProjectConstants.RECIPIENT_PAX_ID);
            Long submitterPaxId = (Long) node.get(ProjectConstants.SUBMITTER_PAX_ID);

            assertTrue(recipientPaxId.equals(clintSharpPaxId) && !submitterPaxId.equals(clintSharpPaxId));
        }
    }
}
