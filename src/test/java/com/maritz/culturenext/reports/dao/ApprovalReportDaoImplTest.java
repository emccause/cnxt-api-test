package com.maritz.culturenext.reports.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.test.util.ReflectionTestUtils;

import com.maritz.core.jdbc.util.CamelCaseMapRowMapper;
import com.maritz.culturenext.reports.dao.impl.ApprovalReportDaoImpl;
import com.maritz.test.AbstractMockTest;

public class ApprovalReportDaoImplTest extends AbstractMockTest {

    private static final String GIVEN_APPROVAL_PENDING_BY_GIVEN_MESSAGE_EXCEPTION = "The query given Approval Pending by given has failed";
    private static final String GIVEN_APPROVAL_PENDING_BY_APPROVALS_EXCEPTION_MESSAGE = "The query given Approval Pending by approvals has failed";
    private static final String GET_APPROVAL_PENDING_BY_GIVERS_FOR_ADMIN_EXCEPTION_MESSAGE = "The query getApprovalPendingByGiversForAdmin has failed";
    private static final String GET_APPROVAL_PENDING_BY_DATES_EXCEPTION_METHOD = "The query getApprovalPendingByDates has failed";
    private static final String APPROVAL_HISTORY_BY_APPROVERS_FOR_MANAGER_EXCEPTION_MESSAGE = "The query getApprovalHistoryByApproversForManager has failed.";
    private static final String GET_APPROVAL_HISTORY_BY_GIVERS_FOR_ADMIN_EXCEPTION_MESSAGE = "The query getApprovalHistoryByGiversForAdmin has failed.";
    private static final String GET_APPROVAL_HISTORY_BY_APPROVERS_FOR_ADMIN_EXCEPTION = "The query getApprovalHistoryByApproversForAdmin has failed.";
    @InjectMocks private ApprovalReportDaoImpl approvalReportDaoImpl;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        //this.namedParameterJdbcTemplate
        namedParameterJdbcTemplate = Mockito.mock(NamedParameterJdbcTemplate.class);
        ReflectionTestUtils.setField(approvalReportDaoImpl, "namedParameterJdbcTemplate", namedParameterJdbcTemplate);
    }

    @Test
    public void testGetApprovalHistoryByApproversForAdmin() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        Long groupId = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByApproversForAdmin(statusList, fromTime, toTime, groupId);
        assertNotNull(result);
    }

    @SuppressWarnings("unchecked")
    @Test(expected=Exception.class)
    public void testGetApprovalHistoryByApproversForAdminThrowingException() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        Long groupId = new Long(1L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(RowMapper.class))).thenThrow(new RecoverableDataAccessException(GET_APPROVAL_HISTORY_BY_APPROVERS_FOR_ADMIN_EXCEPTION));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByApproversForAdmin(statusList, fromTime, toTime, groupId);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(RowMapper.class)));
            assertTrue(e.getMessage().contains(GET_APPROVAL_HISTORY_BY_APPROVERS_FOR_ADMIN_EXCEPTION));
        }
    }

    @SuppressWarnings("unchecked")
    @Test(expected=Exception.class)
    public void testGetApprovalHistoryByGiversForAdminThrowingException() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        Long groupId = null;
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(RowMapper.class))).thenThrow(new RecoverableDataAccessException(GET_APPROVAL_HISTORY_BY_GIVERS_FOR_ADMIN_EXCEPTION_MESSAGE));
        try {
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByGiversForAdmin(statusList, fromTime, toTime, groupId);
            Assert.assertNotNull(result);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(RowMapper.class)));
            assertTrue(e.getMessage().contains(GET_APPROVAL_HISTORY_BY_GIVERS_FOR_ADMIN_EXCEPTION_MESSAGE));
        }
    }

    @Test
    public void testGetApprovalHistoryByGiversForAdmin() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        Long groupId = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByGiversForAdmin(statusList, fromTime, toTime, groupId);
        assertNotNull(result);
    }


    @Test
    public void testGetApprovalHistoryByApproversForManager() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;

        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByApproversForManager(statusList, fromTime, toTime, null);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalHistoryByApproversForManagerWithPaxIdListThrowingException() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);

        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class))).thenThrow(new RecoverableDataAccessException(APPROVAL_HISTORY_BY_APPROVERS_FOR_MANAGER_EXCEPTION_MESSAGE));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByApproversForManager(statusList, fromTime, toTime, paxIdList);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
            assertTrue(e.getMessage().contains(APPROVAL_HISTORY_BY_APPROVERS_FOR_MANAGER_EXCEPTION_MESSAGE));
        }
    }

    @Test
    public void testGetApprovalHistoryByApproversForManagerWithPaxIdList() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByApproversForManager(statusList, fromTime, toTime, paxIdList);
        assertNotNull(result);
    }

    @Test
    public void testGetApprovalHistoryByGiversForManager() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByGiversForManager(statusList, fromTime, toTime, null);
        assertNotNull(result);
    }


    @Test
    public void testGetApprovalHistoryByGiversForManagerWithPaxIdList() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByGiversForManager(statusList, fromTime, toTime, paxIdList);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalHistoryByGiversForManagerWithPaxIdListThrowingException() {
        assertNotNull(this.approvalReportDaoImpl);
        List<String> statusList = null;
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(APPROVAL_HISTORY_BY_APPROVERS_FOR_MANAGER_EXCEPTION_MESSAGE));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalHistoryByGiversForManager(statusList, fromTime, toTime, paxIdList);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

    @Test
    public void testGetApprovalPendingByDates() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByDates(fromTime, toTime);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalPendingByDatesWithException() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(GET_APPROVAL_PENDING_BY_DATES_EXCEPTION_METHOD));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByDates(fromTime, toTime);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

    @Test
    public void testGetApprovalPendingByApproversForAdmin() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);

        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByApproversForAdmin(fromTime, toTime, null);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalPendingByApproversForAdminWithException() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(GET_APPROVAL_PENDING_BY_DATES_EXCEPTION_METHOD));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByApproversForAdmin(fromTime, toTime, null);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

    @Test
    public void testGetApprovalPendingByGiversForAdmin() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByGiversForAdmin(fromTime, toTime, null);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalPendingByGiversForAdminWithException() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(GET_APPROVAL_PENDING_BY_GIVERS_FOR_ADMIN_EXCEPTION_MESSAGE));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByGiversForAdmin(fromTime, toTime, null);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

    @Test
    public void testGetApprovalPendingByApproversForManager() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByApproversForManager(fromTime, toTime, null);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalPendingByApproversForManagerWithException() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(GIVEN_APPROVAL_PENDING_BY_APPROVALS_EXCEPTION_MESSAGE));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByApproversForManager(fromTime, toTime, paxIdList);
        } catch (Exception e){
            Mockito.verify(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

    @Test
    public void testGetApprovalPendingByApproversForManagerWithPaxIdList() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByApproversForManager(fromTime, toTime, paxIdList);
        assertNotNull(result);
    }

    @Test
    public void testGetApprovalPendingByGiversForManager() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByGiversForManager(fromTime, toTime, null);
        assertNotNull(result);
    }

    @Test
    public void testGetApprovalPendingByGiversForManagerWithPaxIdList() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByGiversForManager(fromTime, toTime, paxIdList);
        assertNotNull(result);
    }

    @Test(expected=Exception.class)
    public void testGetApprovalPendingByGiversForManagerWithPaxIdListWithException() {
        assertNotNull(this.approvalReportDaoImpl);
        Date fromTime = null;
        Date toTime = null;
        List<Long> paxIdList = new ArrayList<>();
        paxIdList.add(3L);
        Mockito.when(namedParameterJdbcTemplate.query(Mockito.any(String.class),
                Mockito.any(SqlParameterSource.class),
                Mockito.any(CamelCaseMapRowMapper.class)))
                .thenThrow(new RecoverableDataAccessException(GIVEN_APPROVAL_PENDING_BY_GIVEN_MESSAGE_EXCEPTION));
        try {
            @SuppressWarnings("unused")
            List<Map<String, Object>> result = this.approvalReportDaoImpl.getApprovalPendingByGiversForManager(fromTime, toTime, paxIdList);
        } catch (Exception e){
            Mockito.verify(this.namedParameterJdbcTemplate.query(Mockito.any(String.class),
                    Mockito.any(SqlParameterSource.class),
                    Mockito.any(CamelCaseMapRowMapper.class)));
        }
    }

}
