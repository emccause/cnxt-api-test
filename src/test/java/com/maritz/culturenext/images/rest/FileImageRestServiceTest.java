package com.maritz.culturenext.images.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileInputStream;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;

import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.test.AbstractRestTest;

public class FileImageRestServiceTest extends AbstractRestTest {
    
    public static final String USER_NAME = "admin";
    
    public static final String URI_AWARD_CODE_ICON = "/rest/images?type="+FileImageTypes.AWARD_CODE_ICON.name()+"&targetId=13589";
    public static final String URI_AWARD_CODE_CERT ="/rest/images?type="+FileImageTypes.AWARD_CODE_CERT.name()+"&targetId=13581";
    public static final String URI_ECARD_DEFAULT="/rest/images?type="+FileImageTypes.ECARD_DEFAULT.name()+"&targetId=10129";
    public static final String URI_ECARD_THUMBNAIL="/rest/images?type="+FileImageTypes.ECARD_DEFAULT.name()+"&targetId=10129";
    public static final String URI_IMAGE_BY_ID = "/rest/images/1879";
    public static final String URI_GROUP_LOGO="/rest/images?type="+FileImageTypes.GROUP_LOGO.name()+"&targetId=1494";
    public static final String URI_PRFD = "/rest/images?type="+FileImageTypes.PRFD.name()+"&targetId=530";
    public static final String URI_PRFT = "/rest/images?type="+FileImageTypes.PRFT.name()+"&targetId=530";
    public static final String URI_PROJECT_LOGO = "/rest/images?type="+FileImageTypes.PROJECT_LOGO.name();
    
    private final static String URI_NEW_PRFD = "/rest/images?type="+FileImageTypes.PRFD.name()+"&targetId=24";
    private final static String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private final static String LOGO_FILE = "carousel-image.png";
    private final static String IMAGE = "image";
    private static final String FILE_PARAMETER = "image";

    private static final String HERO_IMAGE_FILE = "cycle.jpg";
    private static final String HERO_IMAGE_FILE_PNG = "resizing-image.png";

    private static final String URI_POST_HERO_IMAGE = "/rest/images?type="+FileImageTypes.HERO_IMAGE.name()+"&targetId=24";
    
    private static final String URI_DELETE_HERO_IMAGE = "/rest/images?type="+FileImageTypes.HERO_IMAGE.name();

    private static final String BIG_IMAGE_FILE = "arp147.jpg";
    
    FileInputStream image = null; 

    @Test
    public void post_new_prfd() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + LOGO_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(IMAGE , 
                LOGO_FILE ,
                MediaTypesEnum.IMAGE_PNG.value(), 
                image);
    
        mockMvc.perform(
                fileUpload(String.format(URI_NEW_PRFD))
                .file(multipartfile)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isOk());

    }
    
    @Test
    public void get_award_code_cert() throws Exception {
        mockMvc.perform(
                get(URI_AWARD_CODE_CERT)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_award_code_icon() throws Exception {
        mockMvc.perform(
                get(URI_AWARD_CODE_ICON )
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_ecard_default() throws Exception {
        mockMvc.perform(
                get(URI_ECARD_DEFAULT)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_ecard_thumbnail() throws Exception {
        mockMvc.perform(
                get(URI_ECARD_THUMBNAIL)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_group_logo() throws Exception {
        mockMvc.perform(
                get(URI_GROUP_LOGO)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_image_by_id() throws Exception {
        mockMvc.perform(
                get(URI_IMAGE_BY_ID)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_prfd() throws Exception {
        mockMvc.perform(
                get(URI_PRFD)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
    
    @Test
    public void get_prft() throws Exception {
        mockMvc.perform(
                get(URI_PRFT)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }
        
    @Test
    public void get_project_logo() throws Exception {
        mockMvc.perform(
                get(URI_PROJECT_LOGO)
                .with(authToken(USER_NAME))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
            )
            .andExpect(status().isOk())
        ;
    }

    @Test
    public void testPostHeroImage() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + HERO_IMAGE_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(FILE_PARAMETER , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_JPEG.value(),  // must be jpeg because jpg will not be recognized by the system.
                        image);
    
        mockMvc.perform(
                fileUpload(String.format(URI_POST_HERO_IMAGE))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isOk());
    }

    @Test
    public void testPostHeroImagePNG() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + HERO_IMAGE_FILE_PNG);
        MockMultipartFile multipartfile = new MockMultipartFile(FILE_PARAMETER , 
                        LOGO_FILE ,
                        MediaTypesEnum.IMAGE_PNG.value(),
                        image);
    
        mockMvc.perform(
                fileUpload(String.format(URI_POST_HERO_IMAGE))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isOk());
    }
    
    @Test
    public void testPostBigHeroImage() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + BIG_IMAGE_FILE);
        MockMultipartFile multipartfile = new MockMultipartFile(FILE_PARAMETER , 
                        BIG_IMAGE_FILE ,
                        MediaTypesEnum.IMAGE_JPEG.value(),  // must be jpeg because jpg will not be recognized by the system.
                        image);
    
        mockMvc.perform(
                fileUpload(String.format(URI_POST_HERO_IMAGE))
                .file(multipartfile)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testDeleteHeroImage() throws Exception {
        image = new FileInputStream(TEST_IMAGES_DIR + BIG_IMAGE_FILE);
    
        mockMvc.perform(delete(URI_DELETE_HERO_IMAGE)
                .with(authToken(TestConstants.USER_ADMIN))
                .header(ProjectConstants.VERSION, ProjectConstants.FUTURE)
                )
                .andExpect(status().isOk());
    }

}