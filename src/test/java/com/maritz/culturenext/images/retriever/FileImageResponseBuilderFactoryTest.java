package com.maritz.culturenext.images.retriever;

import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@ContextConfiguration
public class FileImageResponseBuilderFactoryTest {
    private FileImageResponseBuilderFactory fileImageResponseBuilderFactory;

    @Mock
    private Environment mockEnvironment;


    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        EnvironmentUtil environmentUtil = new EnvironmentUtil()
                                                    .setEnvironment(mockEnvironment);
        fileImageResponseBuilderFactory = new FileImageResponseBuilderFactory()
                                                    .setEnvironmentUtil(environmentUtil)
                                                    .setGcpUtil(new GcpUtil()
                                                                        .setEnvironmentUtil(environmentUtil));
    }

    @Test
    public void testPrepareImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareImageForResponse(null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());

        result = fileImageResponseBuilderFactory.prepareImageForResponse(4L, null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());

        Files files = new Files();
        result = fileImageResponseBuilderFactory.prepareImageForResponse(4L, files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareImageForResponseWrongMediaType(){
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareImageForResponse(4L, files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareProfileImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareProfileImageForResponse(4L, null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareProfileImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareProfileImageForResponse(4L, files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareEcardNameProfileImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareProfileImageForResponse("ecardName", null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareEcardNameProfileImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareProfileImageForResponse("ecardName", files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareLogoImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareLogoImageForResponse( null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareLogoImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareLogoImageForResponse( files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareLogoLightImageForResponse (){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareLogoImageForResponse( null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareLogoLightImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareLogoImageForResponse( files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareBackgroundImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareBackgroundImageForResponse( null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareBackgroundImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareBackgroundImageForResponse(files);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void testPrepareGenericImageForResponse(){
        assertNotNull(fileImageResponseBuilderFactory);
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareGenericImageForResponse(null, null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test(expected = ErrorMessageException.class)
    public void testPrepareGenericImageForResponseWrongMediaType(){
        assertNotNull(fileImageResponseBuilderFactory);
        Files files = new Files();
        files.setMediaType("csv");
        ResponseEntity<Object> result = fileImageResponseBuilderFactory.prepareGenericImageForResponse(files, null);
        assertEquals(HttpStatus.NOT_FOUND,result.getStatusCode());
    }

    @Test
    public void getImage_cdnUrl_defaultDuration() {
        when(mockEnvironment.getProperty("image.read.strategy")).thenReturn("cdn");
        when(mockEnvironment.getProperty("cdn.secure.url")).thenReturn("https://dk8s-app-cdn.culturenxt.com");
        when(mockEnvironment.getProperty("client.name")).thenReturn("novartis");
        when(mockEnvironment.getProperty("url.signing.key-name")).thenReturn("url-key-2021-09-09");
        when(mockEnvironment.getProperty("url.signing.key")).thenReturn("KmCPD18sPjgwauRu6Ft6lw==");

        ResponseEntity<Object> responseEntity = fileImageResponseBuilderFactory.prepareGenericImageForResponse(new Files().setUrl("Admin.png").setMediaType("image/gif"), null);
        String cdnUrl = ((FileImageResponseBuilderFactory.UrlWrapper) responseEntity.getBody()).getUrl();
        assertThat(cdnUrl, startsWith("https://dk8s-app-cdn.culturenxt.com/novartis/Admin.png?Expires="));
        assertThat(cdnUrl, StringContains.containsString("&KeyName=url-key-2021-09-09&Signature="));

        verify(mockEnvironment, times(1)).getProperty("image.read.strategy");
        verify(mockEnvironment, times(1)).getProperty("cdn.secure.url");
        verify(mockEnvironment, times(1)).getProperty("client.name");
        verify(mockEnvironment, times(1)).getProperty("url.signing.key-name");
        verify(mockEnvironment, times(1)).getProperty("url.signing.key");
    }

    @Test
    public void getImage_cdnUrl_durationNonNumeric() {
        when(mockEnvironment.getProperty("image.read.strategy")).thenReturn("cdn");
        when(mockEnvironment.getProperty("cdn.secure.url")).thenReturn("https://dk8s-app-cdn.culturenxt.com");
        when(mockEnvironment.getProperty("client.name")).thenReturn("novartis");
        when(mockEnvironment.getProperty("url.signing.key-name")).thenReturn("url-key-2021-09-09");
        when(mockEnvironment.getProperty("url.signing.key")).thenReturn("KmCPD18sPjgwauRu6Ft6lw==");
        when(mockEnvironment.getProperty("url.signing.duration")).thenReturn("abc123");

        ResponseEntity<Object> responseEntity = fileImageResponseBuilderFactory.prepareGenericImageForResponse(new Files().setUrl("Admin.png").setMediaType("image/gif"), null);
        String cdnUrl = ((FileImageResponseBuilderFactory.UrlWrapper) responseEntity.getBody()).getUrl();
        assertThat(cdnUrl, startsWith("https://dk8s-app-cdn.culturenxt.com/novartis/Admin.png?Expires="));
        assertThat(cdnUrl, StringContains.containsString("&KeyName=url-key-2021-09-09&Signature="));

        verify(mockEnvironment, times(1)).getProperty("image.read.strategy");
        verify(mockEnvironment, times(1)).getProperty("cdn.secure.url");
        verify(mockEnvironment, times(1)).getProperty("client.name");
        verify(mockEnvironment, times(1)).getProperty("url.signing.key-name");
        verify(mockEnvironment, times(1)).getProperty("url.signing.key");
        verify(mockEnvironment, times(1)).getProperty("url.signing.duration.web");
    }
}
