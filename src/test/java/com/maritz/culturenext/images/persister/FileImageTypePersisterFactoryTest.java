package com.maritz.culturenext.images.persister;

import com.maritz.core.rest.ErrorMessageException;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@ContextConfiguration
public class FileImageTypePersisterFactoryTest extends AbstractDatabaseTest  {
    @Inject
    private FileImageTypePersisterFactory fileImageTypePersisterFactory;

    @Test
    public void testPostContructor(){
        assertNotNull(fileImageTypePersisterFactory);
        FileImageTypePersister persister = fileImageTypePersisterFactory.getFileImagePersister("HERO_IMAGE");
        assertNotNull(persister);
        assertTrue(persister instanceof GenericFileImageTypePersisterImpl);
    }

    @Test(expected = ErrorMessageException.class)
    public void testNullFileTypeImage(){
        assertNotNull(fileImageTypePersisterFactory);
        FileImageTypePersister persister = fileImageTypePersisterFactory.getFileImagePersister(null);
    }

    @Test(expected = ErrorMessageException.class)
    public void testNotImplementedFileTypeImage(){
        assertNotNull(fileImageTypePersisterFactory);
        FileImageTypePersister persister = fileImageTypePersisterFactory.getFileImagePersister("csv");
    }
}
