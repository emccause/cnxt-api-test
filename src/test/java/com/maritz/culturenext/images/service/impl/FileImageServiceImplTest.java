package com.maritz.culturenext.images.service.impl;

import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.maritz.core.jpa.entity.Files;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.dto.IdDTO;
import com.maritz.culturenext.enums.FileTypes;
import com.maritz.culturenext.enums.MediaTypesEnum;
import com.maritz.culturenext.images.constants.FileImageTypes;
import com.maritz.culturenext.images.dao.impl.FilesDaoImpl;
import com.maritz.culturenext.util.EnvironmentUtil;
import com.maritz.culturenext.util.GcpUtil;
import com.maritz.test.AbstractDatabaseTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import javax.inject.Inject;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;


@ContextConfiguration
public class FileImageServiceImplTest extends AbstractDatabaseTest {

    @Inject FileImageServiceImpl fileImageServiceImpl;
    @Inject FilesDaoImpl filesDaoImpl;

    private static final String TEST_IMAGES_DIR = "src/test/resources/test-images/";
    private static final String VALID_IMAGE = "resizing-image.png";
    private static final String CAROUSEL_IMAGE = "carousel-image.png";
    private static final String CAROUSEL_IMAGE_SHORT = "carousel-image-short.png";

    private static final String FILE_PARAMETER = "file";
    private static final String FILE_NAME = "test-images/cycle.jpg";
    private static final String FILE_3K_X_3K = "test-images/test-image-3000x3000.jpg";
    private static final String FILE_NAME_PNG = "test-images/resizing-image.png";

    private static final String FILE_CONTENTS = "Award Amount;Recipient First Name;Recipient Last Name,Recipient ID (Participant ID),Public Headline,Private Message,Program Name,Program ID,Budget Name,Budget ID,Award Type (Payout Type),Submitter First Name,Submitter Last Name,Submitter ID,Ecard ID,Value,Value ID,Make recognition private\n"
            + "246,,,1706.publicHeadline,Message private 1,programName,40,budgetName,76,CASH,submitterFirstName,submitterLastName,3482,30,Client Focus,2,TRUE\n"
            + "246,Francisco,Perez,1346,publicHeadline,,programName,40,budgetName,78,POINTS,submitterFirstName,submitterLastName,3851,29,Fun,3,FALSE\n"
            + "246;Pedro,Ontiveros,8326,publicHeadline,Message private 2,programName,40,budgetName,78,POINTS,submitterFirstName,submitterLastName,1958,30,Fun,3,TRUE\n";

    private static final MockMultipartFile CSV_TEXT_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME,
            MediaTypesEnum.TEXT_CSV.value(), FILE_CONTENTS.getBytes());

    private static final MockMultipartFile NULL_CONTENT_TYPE_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER,
            FILE_NAME, null, FILE_CONTENTS.getBytes());

    private static final MockMultipartFile IMG_EMPTY_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME,
            MediaTypesEnum.IMAGE_PNG.value(), (byte[]) null);

    private static final String BIG_FILE_NAME = "test-images/arp147.jpg";

    @Inject
    private EnvironmentUtil environmentUtil;
    @Inject
    private Environment environment;
    @Inject
    private GcpUtil gcpUtil;
    @Autowired(required = false)
    private Storage storage;

    @Mock
    private Environment mockEnvironment;
    @Mock
    private Storage mockStorage;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);

        // By default, use the real environment
        environmentUtil.setEnvironment(environment);

        gcpUtil.setStorage(mockStorage);
    }

    @Test
    public void testUploadImage() throws Exception {
        assertNotNull(fileImageServiceImpl);
        MockMultipartFile IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_NAME).getInputStream());

        IdDTO result = fileImageServiceImpl.uploadImage(IMG_MULTIPART_FILE, FileTypes.HERO_IMAGE.toString(), 4L);
        assertNotNull(result);
        assertNotNull("Id was not expected to be null but it was.",result.getId());
        
        Files imageEntry = filesDaoImpl.findById(result.getId());
        assertNotNull("Image should be in Files table", imageEntry);
        assertEquals("Media Type should be JPEG", imageEntry.getMediaType(), MediaTypesEnum.IMAGE_JPEG.value());
    }

    @Test
    public void upload_profile_cdnEnabled() throws Exception {
        environmentUtil.setEnvironment(mockEnvironment);

//        // This will use real Google Storage (conditional on spring.cloud.gcp.storage.enabled=true)
//        gcpUtil.setStorage(storage);

        when(mockEnvironment.getProperty("image.write.strategies")).thenReturn("foo,cdn");
        when(mockEnvironment.getProperty("cdn.secure.bucket")).thenReturn("cnxt-securecdn-dev");
        when(mockEnvironment.getProperty("client.name")).thenReturn("novartis");

        MockMultipartFile IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_3K_X_3K,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_3K_X_3K).getInputStream());

        IdDTO idDto = fileImageServiceImpl.uploadImage(IMG_MULTIPART_FILE, FileTypes.PRFD.toString(), 4L);
        Files imageEntry = filesDaoImpl.findById(idDto.getId());
        assertNotNull("Image should be in Files table", imageEntry);
        assertNotNull("Image CDN URL should be in Files table", imageEntry.getUrl());

        verify(mockEnvironment, times(1)).getProperty("image.write.strategies");
        verify(mockEnvironment, times(3)).getProperty("cdn.secure.bucket");
        verify(mockEnvironment, times(3)).getProperty("client.name");
        verify(mockStorage, times(3)).create(any(BlobInfo.class), any(byte[].class));

        // Useful for testing locally (when not using the mocked Storage) -- this prints a signed URL for the CDN image which is good for 60 seconds.
//        when(mockEnvironment.getProperty("cdn.secure.url")).thenReturn("https://dk8s-app-cdn.culturenxt.com");
//        when(mockEnvironment.getProperty("url.signing.key-name")).thenReturn("url-key-2021-09-09");
//        when(mockEnvironment.getProperty("url.signing.key")).thenReturn("KmCPD18sPjgwauRu6Ft6lw==");
//        System.out.println(gcpUtil.signUrl(imageEntry.getUrl(), 60));
    }

    @Test
    public void testUploadImagePNG() throws Exception {
        assertNotNull(fileImageServiceImpl);
        MockMultipartFile IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME_PNG,
                MediaTypesEnum.IMAGE_PNG.value(), new ClassPathResource(FILE_NAME_PNG).getInputStream());

        IdDTO result = fileImageServiceImpl.uploadImage(IMG_MULTIPART_FILE, FileTypes.HERO_IMAGE.toString(), 4L);
        assertNotNull(result);
        assertNotNull("Id was not expected to be null but it was.",result.getId());
        
        Files imageEntry = filesDaoImpl.findById(result.getId());
        assertNotNull("Image should be in Files table", imageEntry);
        assertEquals("Media Type should be PNG", imageEntry.getMediaType(), MediaTypesEnum.IMAGE_PNG.value());
    }

    @Test(expected= ErrorMessageException.class)
    public void testGetImageFileAllNullValues() {
        assertNotNull(fileImageServiceImpl);
        @SuppressWarnings("unused")
        ResponseEntity<Object> result = fileImageServiceImpl.getImageFile(null, null, null);
    }

    @Test(expected=ErrorMessageException.class)
    public void testSaveImageByTypeNullValues() {
        assertNotNull(fileImageServiceImpl);
        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", null, null, null);
    }

    @Test(expected=ErrorMessageException.class)
    public void testSaveImageByType() {
        assertNotNull(fileImageServiceImpl);

        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", CSV_TEXT_MULTIPART_FILE,
                null, null);
    }

    @Test(expected = ErrorMessageException.class)
    public void testSaveImageByTypeWrongType() {
        assertNotNull(fileImageServiceImpl);

        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", CSV_TEXT_MULTIPART_FILE,
                MediaTypesEnum.TEXT_CSV.value(), null);
    }

    @Test
    public void testSaveImageByTypeNoTargetId() throws IOException {
        assertNotNull(fileImageServiceImpl);
        MockMultipartFile IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_NAME).getInputStream());

        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", IMG_MULTIPART_FILE,
                FileTypes.HERO_IMAGE.toString(), null);
        assertNotNull(result);
    }

    @Test(expected=ErrorMessageException.class)
    public void testSaveImageByTypeInvalidContentType() {
        assertNotNull(fileImageServiceImpl);

        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", CSV_TEXT_MULTIPART_FILE,
                FileTypes.HERO_IMAGE.toString(), 4L);
    }

    @Test(expected=ErrorMessageException.class)
    public void testSaveImageByTypeNullContentType() {
        assertNotNull(fileImageServiceImpl);

        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType",
                NULL_CONTENT_TYPE_MULTIPART_FILE, FileTypes.HERO_IMAGE.toString(), 4L);
    }

    @Test(expected=ErrorMessageException.class)
    public void testSaveImageByTypeEmptyFile() {
        assertNotNull(fileImageServiceImpl);

        @SuppressWarnings("unused")
        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType",
                IMG_EMPTY_MULTIPART_FILE, FileTypes.HERO_IMAGE.toString(), 4L);
    }

    @Test
    public void testSaveImageByTypeRightContentType() throws IOException {
        assertNotNull(fileImageServiceImpl);

        MockMultipartFile IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_NAME,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_NAME).getInputStream());

        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", IMG_MULTIPART_FILE,
                FileTypes.HERO_IMAGE.toString(), 4L);
        assertNotNull(result);
    }

    @Test
    public void testSaveImageByTypeRightContentTypeButSizeBiggerThan6MB() throws IOException {
        assertNotNull(fileImageServiceImpl);
        MockMultipartFile IMG_3K_X_3K_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, FILE_3K_X_3K,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(FILE_3K_X_3K).getInputStream());

        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", IMG_3K_X_3K_MULTIPART_FILE,
                FileTypes.HERO_IMAGE.toString(), 4L);
        assertNotNull(result);
    }

    @Test(expected = ErrorMessageException.class)
    public void testSaveImageByTypeRightContentTypeButSizeBiggerThan6MBOtherThanHero() throws IOException {
        assertNotNull(fileImageServiceImpl);
        MockMultipartFile BIG_IMG_MULTIPART_FILE = new MockMultipartFile(FILE_PARAMETER, BIG_FILE_NAME,
                MediaTypesEnum.IMAGE_JPEG.value(), new ClassPathResource(BIG_FILE_NAME).getInputStream());

        Long result = ReflectionTestUtils.invokeMethod(fileImageServiceImpl, "saveImageByType", BIG_IMG_MULTIPART_FILE,
                FileTypes.PROGRAM_IMAGE.toString(), 4L);
        assertNotNull(result);
    }

    @Test
    public void testDeleteHeroImage(){
        assertNotNull(fileImageServiceImpl);
        fileImageServiceImpl.deleteImageByType(FileImageTypes.HERO_IMAGE.name());
    }

    @Test
    public void test_upload_program_image_happy_path() {

        assertNotNull(fileImageServiceImpl);
        IdDTO programImageDTO = null;
        FileInputStream fileImage = null;
        try {
            fileImage = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile mockImage = new MockMultipartFile("test", "newTest", "image/png", fileImage);
            programImageDTO = fileImageServiceImpl.uploadImage(mockImage, FileTypes.PROGRAM_IMAGE.name(), null);
        } catch (Exception e) {
            fail();
        }
        assertTrue(programImageDTO != null);
        assertNotNull(programImageDTO.getId());

        try {
            fileImage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_upload_program_image_small_happy_path() {
        assertNotNull(fileImageServiceImpl);
        IdDTO programImageDTO = null;
        FileInputStream fileImage = null;
        try {
            fileImage = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile mockImage = new MockMultipartFile("test", "newTest", "image/png", fileImage);
            programImageDTO = fileImageServiceImpl.uploadImage(mockImage, FileTypes.PROGRAM_IMAGE_SMALL.name(), null);
        } catch (Exception e) {
            fail();
        }
        assertTrue(programImageDTO != null);
        assertNotNull(programImageDTO.getId());

        try {
            fileImage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_upload_program_image_detailed_happy_path() {
        assertNotNull(fileImageServiceImpl);
        IdDTO programImageDTO = null;
        FileInputStream fileImage = null;
        try {
            fileImage = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile mockImage = new MockMultipartFile("test", "newTest", "image/png", fileImage);
            programImageDTO = fileImageServiceImpl.uploadImage(mockImage, FileTypes.PROGRAM_IMAGE_DETAILED.name(), null);
        } catch (Exception e) {
            fail();
        }
        assertTrue(programImageDTO != null);
        assertNotNull(programImageDTO.getId());

        try {
            fileImage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_upload_carousel_image_happy_path() {

        assertNotNull(fileImageServiceImpl);
        IdDTO imageDTO = null;
        FileInputStream fileImage = null;
        try {
            fileImage = new FileInputStream(TEST_IMAGES_DIR + CAROUSEL_IMAGE);
            MockMultipartFile mockImage = new MockMultipartFile("test", "newTest", "image/png", fileImage);
            imageDTO = fileImageServiceImpl.uploadImage(mockImage, FileTypes.CAROUSEL.name(), null);
        } catch (Exception e) {

            fail();
        }

        assertTrue(imageDTO != null);
        assertNotNull(imageDTO.getId());
        try {
            fileImage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test_project_logo_logo_happy_path() throws Exception {
        assertNotNull(fileImageServiceImpl);
        FileInputStream image = null;
        try {
            image = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile[] images = new MockMultipartFile[1];
            images[0] = new MockMultipartFile("test", "newTest", "image/png", image);
            fileImageServiceImpl.uploadImage(images[0], FileTypes.PROJECT_LOGO.name(),null);
        } catch (Exception e) {
            fail();
        }
        image.close();
    }

    @Test
    public void test_upload_logo_light_happy_path() throws Exception {
        assertNotNull(fileImageServiceImpl);
        FileInputStream image = null;
        try {
            image = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile[] images = new MockMultipartFile[1];
            images[0] = new MockMultipartFile("test", "newTest", "image/png", image);
            fileImageServiceImpl.uploadImage(images[0], FileTypes.PROJECT_LOGO_LIGHT.name(), null);
        } catch (Exception e) {
            fail();
        }
        image.close();
    }

    @Test
    public void test_upload_background_happy_path() throws Exception {
        assertNotNull(fileImageServiceImpl);
        FileInputStream image = null;
        try {
            image = new FileInputStream(TEST_IMAGES_DIR + VALID_IMAGE);
            MockMultipartFile[] images = new MockMultipartFile[1];
            images[0] = new MockMultipartFile("test", "newTest", "image/png", image);
            fileImageServiceImpl.uploadImage(images[0], FileTypes.PROJECT_BACKGROUND.name(), null);
        } catch (Exception e) {
            fail();
        }
        image.close();
    }

    @Test(expected = ErrorMessageException.class)
    public void test_upload_carousel_image_short() throws Exception {
        assertNotNull(fileImageServiceImpl);
        FileInputStream fileImage = null;
        IdDTO imageDTO = null;
            fileImage = new FileInputStream(TEST_IMAGES_DIR + CAROUSEL_IMAGE_SHORT);
            MockMultipartFile image =  new MockMultipartFile("test", "newTest", "image/png", fileImage);

            imageDTO = fileImageServiceImpl.uploadImage(image, FileTypes.CAROUSEL.name(), null);
            assertTrue(imageDTO != null);
            assertNotNull(imageDTO.getId());

        try {
            fileImage.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
