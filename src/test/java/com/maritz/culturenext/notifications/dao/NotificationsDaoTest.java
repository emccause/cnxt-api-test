package com.maritz.culturenext.notifications.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.junit.Test;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.Nomination;
import com.maritz.core.jpa.entity.Relationship;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.enums.RelationshipType;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class NotificationsDaoTest extends AbstractDatabaseTest {

    @Inject private ConcentrixDao<Alert> alertDao;
    @Inject private ConcentrixDao<Relationship> relationshipDao;
    @Inject private ConcentrixDao<Nomination> nominationDao;
    @Inject private NotificationsDao notificationsDao;
    
    private static final String TEST_NOMINATION_HEADLINE = "NOTIFICATION_MANAGER_RECOGNITION_TEST";
    private static final String TEST_ANONYMOUS_NOMINATION_HEADLINE = "Milestone Email Test";
    
    @Test
    public void test_notification_change_manager() {
        
        // Find test data
        Nomination nomination = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(TEST_NOMINATION_HEADLINE)
                .findOne();
        assertNotNull("test data should exist", nomination);
        
        Alert alert = alertDao.findBy()
                .where(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.MANAGER_RECOGNITION.name())
                .and(ProjectConstants.TARGET_ID).eq(nomination.getId())
                .findOne();
        assertNotNull("test data should exist", alert);
        
        // grab notification - should find since pax is direct report
        List<Map<String, Object>> originalEntry = notificationsDao.getNotificationList(TestConstants.PAX_PORTERGA,
                null,
                null,
                ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE,
                alert.getId());
        
        assertFalse("Should have found notification", originalEntry.isEmpty());
        
        assertNotNull("Nominator should not be null", originalEntry.get(0).get("FROM_PAX_ID"));
        
        Relationship relationship = relationshipDao.findBy()
                .where(ProjectConstants.PAX_ID_1).eq(TestConstants.PAX_MUDDAM)
                .and(ProjectConstants.PAX_ID_2).eq(TestConstants.PAX_PORTERGA)
                .and(ProjectConstants.RELATIONSHIP_TYPE_CODE).eq(RelationshipType.REPORT_TO.name())
                .findOne();
        assertNotNull("test data should exist", relationship);
        
        // change it so pax doesn't report to logged in pax
        relationship.setPaxId2(TestConstants.PAX_HARWELLM);
        relationshipDao.save(relationship, false);
        
        List<Map<String, Object>> notDirectReportEntry = notificationsDao.getNotificationList(TestConstants.PAX_PORTERGA,
                null,
                null,
                ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE,
                alert.getId());
        assertTrue("Should not have found notification", notDirectReportEntry.isEmpty());
        
        // change manager back to logged in pax
        relationship.setPaxId2(TestConstants.PAX_PORTERGA);
        relationshipDao.save(relationship, false);
        
        List<Map<String, Object>> directReportEntry = notificationsDao.getNotificationList(TestConstants.PAX_PORTERGA,
                null,
                null,
                ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE,
                alert.getId());
        assertFalse("Should have found notification", directReportEntry.isEmpty());
        
    }
    
    @Test
    public void testAnonymousNomination() {
        // Find test data
        Nomination nomination = nominationDao.findBy()
                .where(ProjectConstants.HEADLINE_COMMENT).eq(TEST_ANONYMOUS_NOMINATION_HEADLINE)
                .findOne();
        assertNotNull("test data should exist", nomination);
        
        Alert alert = alertDao.findBy()
                .where(ProjectConstants.ALERT_SUB_TYPE_CODE).eq(AlertSubType.MANAGER_RECOGNITION.name())
                .and(ProjectConstants.TARGET_ID).eq(nomination.getId())
                .findOne();
        assertNotNull("test data should exist", alert);
        assertEquals(new Long(434428),alert.getId());

        logger.info("ALERT"+ReflectionToStringBuilder.toString(alert));
        // grab notification - should find since pax is direct report
        
        List<Map<String, Object>> originalEntry = notificationsDao.getNotificationList(TestConstants.PAX_PORTERGA,
                null,
                null,
                ProjectConstants.DEFAULT_PAGE_NUMBER,
                ProjectConstants.ITEM_BY_ID_PAGE_SIZE,
                alert.getId());
        assertFalse("Should have found notification", originalEntry.isEmpty());

        assertNull("Nominator should be null", originalEntry.get(0).get("FROM_PAX_ID"));

        
    }

}
