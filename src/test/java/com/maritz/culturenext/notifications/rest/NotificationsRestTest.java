package com.maritz.culturenext.notifications.rest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.Test;

import com.maritz.core.util.ObjectUtils;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.constants.ProjectConstants;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;

public class NotificationsRestTest extends AbstractRestTest {
    
    private static final String PATH = "/rest/participants/~/notifications/";

    @Test
    public void test_notifications() throws Exception {
        PaginatedResponseObject result = ObjectUtils.jsonToObject(
            mockMvc.perform(
                get(PATH)
                    .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                    .with(authToken(TestConstants.USER_PORTERGA))
            )
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString()
            ,PaginatedResponseObject.class
        );
        
        assertFalse("Should have results", result.getData().isEmpty());
        
        Object notification = result.getData().get(0); // ObjectUtils can't handle nested objects, bleh
        assertNotNull("Should have data", notification);
        Map<String, Object> notificationMap;
        if (notification instanceof Map) {
            notificationMap = (Map<String, Object>) notification;
            Integer notificationId = (Integer) notificationMap.get(ProjectConstants.ID); // tableflip
            assertNotNull("Should have an id", notificationId);
            
            // make sure that get by id works too
            mockMvc.perform(
                    get(PATH + notificationId)
                        .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                        .with(authToken(TestConstants.USER_PORTERGA))
                ).andExpect(status().isOk());
        }
    }
    
    // Code coverage!
    
    @Test
    public void test_notifications_code_coverage() throws Exception {
        
        // no notification found
        mockMvc.perform(
                get(PATH + "0")
                    .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                    .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isNotFound());

        // page number and size
        mockMvc.perform(
                get(PATH + "?page[number]=0&page[size]=0")
                    .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                    .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
        
        mockMvc.perform(
                get(PATH + "?page[number]=2")
                    .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                    .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
        
        mockMvc.perform(
                get(PATH + "?page[number]=1&page[size]=50")
                    .header(ProjectConstants.VERSION_REQUEST_HEADER, TestConstants.FUTURE_VERSION)
                    .with(authToken(TestConstants.USER_PORTERGA))
            ).andExpect(status().isOk());
    }
}
