package com.maritz.culturenext.notifications.service;

import com.maritz.core.jdbc.dao.ConcentrixDao;
import com.maritz.core.jdbc.util.FindBy;
import com.maritz.core.jpa.entity.Alert;
import com.maritz.core.jpa.entity.AlertMisc;
import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.rest.ErrorMessageException;
import com.maritz.culturenext.activityfeed.services.ActivityFeedService;
import com.maritz.culturenext.alert.dao.AlertDao;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.dto.PaginatedResponseObject;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.newsfeed.dto.NewsfeedItemDTO;
import com.maritz.culturenext.notifications.constants.NotificationConstants;
import com.maritz.culturenext.notifications.dao.NotificationsDao;
import com.maritz.culturenext.notifications.dto.CommentNotificationDTO;
import com.maritz.culturenext.notifications.dto.LikeNotificationDTO;
import com.maritz.culturenext.notifications.dto.NotificationDTO;
import com.maritz.culturenext.notifications.services.impl.NotificationsServiceImpl;
import com.maritz.culturenext.participant.dao.ParticipantInfoDao;
import com.maritz.culturenext.profile.dto.EntityDTO;
import com.maritz.culturenext.profile.dto.GroupDTO;
import com.maritz.culturenext.profile.dto.aggregate.EmployeeDTO;
import com.maritz.culturenext.program.milestone.constants.MilestoneConstants;
import com.maritz.culturenext.recognition.dto.RecognitionDetailsDTO;
import com.maritz.culturenext.recognition.services.RecognitionService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;

import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

public class NotificationsServiceTest {
    
    @InjectMocks NotificationsServiceImpl notificationService;

    @Mock private ActivityFeedService activityFeedService;
    @Mock private AlertDao customAlertDao;
    @Mock private ConcentrixDao<Alert> alertDao;
    @Mock private ConcentrixDao<AlertMisc> alertMiscDao;
    @Mock private NotificationsDao notificationDao;
    @Mock private ParticipantInfoDao participantInfoDao;
    @Mock private RecognitionService recognitionService;

    @SuppressWarnings("unchecked")
    private FindBy<AlertMisc> alertMiscFindBy = PowerMockito.mock(FindBy.class);
    private String USER = "2";
    private EmployeeDTO pax;
    private List<Map<String, Object>> notificationList;
    private Map<String, Object> notificationEntry;
    private long paxId = 20L;
    private long alertId = 25L;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        
        List<Map<String, Object>> notificationDataList = new ArrayList<>();

        // Comments
        Map<String, Object> commentRecognitionReceiver = new HashMap<>();
        commentRecognitionReceiver.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.COMMENT_RECOGNITION_RECEIVER.name());
        commentRecognitionReceiver.put(NotificationConstants.COMMENT_ID_FIELD, TestConstants.ID_1);
        commentRecognitionReceiver.put(NotificationConstants.COMMENT_TEXT_FIELD, "This is a comment");
        commentRecognitionReceiver.put(NotificationConstants.COMMENT_CREATE_DATE_FIELD, new Date());
        commentRecognitionReceiver.put(NotificationConstants.COMMENT_STATUS_FIELD, StatusTypeCode.ACTIVE.name());
        commentRecognitionReceiver.put(NotificationConstants.COMMENT_PAX_ID_FIELD, TestConstants.ID_1);
        commentRecognitionReceiver.put(NotificationConstants.COMMENTER_COUNT_FIELD, 1);
        setCommonNotificationData(commentRecognitionReceiver);
        notificationDataList.add(commentRecognitionReceiver);
        
        Map<String, Object> commentRecognitionSubmitter = new HashMap<>(commentRecognitionReceiver);
        commentRecognitionSubmitter.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.COMMENT_RECOGNITION_SUBMITTER.name());
        commentRecognitionSubmitter.put(NotificationConstants.COMMENT_PAX_ID_FIELD, TestConstants.ID_2);
        notificationDataList.add(commentRecognitionSubmitter);
        
        Map<String, Object> commentBroken = new HashMap<>(commentRecognitionReceiver);
        commentBroken.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.COMMENT_RECOGNITION_SUBMITTER.name());
        commentBroken.put(NotificationConstants.COMMENT_ID_FIELD, null);
        notificationDataList.add(commentBroken);
        
        Map<String, Object> reportCommentAbuse = new HashMap<>(commentRecognitionReceiver);
        reportCommentAbuse.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.REPORT_COMMENT_ABUSE.name());
        notificationDataList.add(reportCommentAbuse);
        
        // Likes
        Map<String, Object> likeRecognitionReceiver = new HashMap<>();
        likeRecognitionReceiver.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.LIKE_RECOGNITION_RECEIVER.name());
        likeRecognitionReceiver.put(NotificationConstants.LIKE_COUNT_FIELD, 1);
        likeRecognitionReceiver.put(NotificationConstants.LIKE_PAX_ID_FIELD, TestConstants.ID_1);
        setCommonNotificationData(likeRecognitionReceiver);
        notificationDataList.add(likeRecognitionReceiver);
        
        Map<String, Object> likeRecognitionSubmitter = new HashMap<>(likeRecognitionReceiver);
        likeRecognitionSubmitter.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.LIKE_RECOGNITION_SUBMITTER.name());
        likeRecognitionSubmitter.put(NotificationConstants.LIKE_PAX_ID_FIELD, 3L);
        notificationDataList.add(likeRecognitionSubmitter);
        
        // Raises
        Map<String, Object> managerRaiseReceiver = new HashMap<>();
        managerRaiseReceiver.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.MANAGER_RAISE_RECEIVER.name());
        managerRaiseReceiver.put(NotificationConstants.RAISE_RECIPIENT_COUNT_FIELD, 1);
        setCommonNotificationData(managerRaiseReceiver);
        notificationDataList.add(managerRaiseReceiver);
        
        Map<String, Object> raiseReceiver = new HashMap<>(managerRaiseReceiver);
        raiseReceiver.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RAISE_RECEIVER.name());
        notificationDataList.add(raiseReceiver);
        
        // Recognition
        Map<String, Object> managerRecognition = new HashMap<>();
        managerRecognition.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.MANAGER_RECOGNITION.name());
        managerRecognition.put(NotificationConstants.NOMINATION_HEADLINE_FIELD, "This is a headline");
        managerRecognition.put(NotificationConstants.POINTS_ISSUED_FIELD, 1D);
        managerRecognition.put(NotificationConstants.PAYOUT_TYPE_FIELD, "POINTS");
        managerRecognition.put(NotificationConstants.PAYOUT_DISPLAY_NAME_FIELD, "Point display name");
        setCommonNotificationData(managerRecognition);
        notificationDataList.add(managerRecognition);
        
        Map<String, Object> recognition = new HashMap<>(managerRecognition);
        recognition.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RECOGNITION.name());
        notificationDataList.add(recognition);
        
        // Approvals
        Map<String, Object> recognitionApproval = new HashMap<>();
        recognitionApproval.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RECOGNITION_APPROVAL.name());
        recognitionApproval.put(NotificationConstants.POINTS_ISSUED_FIELD, 1D);
        recognitionApproval.put(NotificationConstants.PAYOUT_TYPE_FIELD, "POINTS");
        recognitionApproval.put(NotificationConstants.PAYOUT_DISPLAY_NAME_FIELD, "Point display name");
        setCommonNotificationData(recognitionApproval);
        notificationDataList.add(recognitionApproval);

        Map<String, Object> pendingRecognition = new HashMap<>(recognitionApproval);
        pendingRecognition.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RECOGNITION_PENDING_APPROVAL.name());
        notificationDataList.add(pendingRecognition);

        Map<String, Object> approvedRecognition = new HashMap<>(recognitionApproval);
        approvedRecognition.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RECOGNITION_APPROVED.name());
        notificationDataList.add(approvedRecognition);

        Map<String, Object> deniedRecognition = new HashMap<>(recognitionApproval);
        deniedRecognition.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.RECOGNITION_DENIED.name());
        notificationDataList.add(deniedRecognition);
        
        // Point Load
        Map<String, Object> pointLoad = new HashMap<>();
        pointLoad.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.POINT_LOAD.name());
        pointLoad.put(NotificationConstants.POINTS_ISSUED_FIELD, 1D);
        pointLoad.put(NotificationConstants.PAYOUT_TYPE_FIELD, "POINTS");
        pointLoad.put(NotificationConstants.PAYOUT_DISPLAY_NAME_FIELD, "Point display name");
        setCommonNotificationData(pointLoad);
        // None of these are populated
        pointLoad.put(NotificationConstants.NOMINATION_CREATE_DATE_FIELD, null);
        pointLoad.put(NotificationConstants.NOMINATION_ID_FIELD, null);
        pointLoad.put(NotificationConstants.TOTAL_RECIPIENT_COUNT_FIELD, null);
        pointLoad.put(NotificationConstants.APPROVED_RECIPIENT_COUNT_FIELD, null);
        pointLoad.put(NotificationConstants.DIRECT_REPORT_COUNT_FIELD, null);
        pointLoad.put(NotificationConstants.FROM_PAX_ID_FIELD, null);
        notificationDataList.add(pointLoad);

        // Milestone
        Map<String, Object> milestone = new HashMap<>();
        milestone.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.MILESTONE_REMINDER.name());
        milestone.put(NotificationConstants.MILESTONE_DATE_FIELD, "2015-01-01");
        milestone.put(NotificationConstants.MILESTONE_TYPE, MilestoneConstants.SERVICE_ANNIVERSARY_TYPE);
        milestone.put(NotificationConstants.MILESTONE_YEARS_OF_SERVICE_FIELD, 5);
        milestone.put(NotificationConstants.POINTS_ISSUED_FIELD, 1D);
        milestone.put(NotificationConstants.PAYOUT_DISPLAY_NAME_FIELD, "Free Lunch");
        setCommonNotificationData(milestone);
        notificationDataList.add(milestone);
        
        // Other (covers RECOGNITION_PENDING_APPROVAL, RECOGNITION_APPROVED, RECOGNITION_DENIED, MANAGER_AWARD_CODE)
        Map<String, Object> steve = new HashMap<>();
        steve.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, "Steve");
        setCommonNotificationData(steve);
        steve.put(NotificationConstants.FROM_PAX_ID_FIELD, 4L);
        steve.put(NotificationConstants.TO_PAX_ID_FIELD, 5L);
        notificationDataList.add(steve);

        Map<String, Object> dumbledore = new HashMap<>();
        dumbledore.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, "Dumbledore");
        setCommonNotificationData(dumbledore);
        dumbledore.remove(NotificationConstants.FROM_PAX_ID_FIELD);
        dumbledore.remove(NotificationConstants.TO_PAX_ID_FIELD);
        notificationDataList.add(dumbledore);
        
        PowerMockito.when(
                notificationDao.getNotificationList(anyLong(), anyString(), anyString(), anyInt(), anyInt(), anyLong())
            ).thenReturn(notificationDataList);
        
        List<EntityDTO> paxList = new ArrayList<>();
        paxList.add(new EmployeeDTO().setPaxId(TestConstants.ID_1));
        paxList.add(new EmployeeDTO().setPaxId(TestConstants.ID_2));
        paxList.add(new EmployeeDTO().setPaxId(3L));
        paxList.add(new EmployeeDTO().setPaxId(4L));
        paxList.add(new EmployeeDTO().setPaxId(5L));
        paxList.add(new GroupDTO());
        
        PowerMockito.when(participantInfoDao.getInfo(anyListOf(Long.class), anyListOf(Long.class))).thenReturn(paxList);
        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(new Alert().setId(TestConstants.ID_2));
        PowerMockito.when(participantInfoDao.getEmployeeDTO(anyLong())).thenReturn(new EmployeeDTO().setPaxId(TestConstants.ID_1));
                
        PowerMockito.when(alertMiscDao.findBy()).thenReturn(alertMiscFindBy);
        PowerMockito.when(alertMiscFindBy.where(anyString())).thenReturn(alertMiscFindBy);
        PowerMockito.when(alertMiscFindBy.eq(anyString())).thenReturn(alertMiscFindBy);
        PowerMockito.when(alertMiscFindBy.and(anyString())).thenReturn(alertMiscFindBy);
        PowerMockito.when(alertMiscFindBy.eq(anyString())).thenReturn(alertMiscFindBy);
        PowerMockito.when(alertMiscFindBy.findOne()).thenReturn(new AlertMisc().setMiscData(USER));
        
        PowerMockito.when(recognitionService.getRecognitionDetails(anyLong(), any(Boolean.class),
                anyLong(), anyString(), any(Boolean.class)))
            .thenReturn(Arrays.asList(new RecognitionDetailsDTO()));

        //Setup for testing getNotificationById
        pax = new EmployeeDTO();
        pax.setPaxId(paxId);
        pax.setFirstName("Natalie");
        pax.setLastName("Grace");
        paxList.add(pax);

        notificationList = new ArrayList<Map<String, Object>>();

        notificationEntry = new HashMap<String, Object>();
        notificationEntry.put(NotificationConstants.NOTIFICATION_CREATE_DATE_FIELD, new Date());
        notificationEntry.put(NotificationConstants.NOTIFICATION_STATUS_DATE_FIELD, new Date());

        when(notificationDao.getNotificationList(Mockito.eq(paxId), anyString(), anyString(), anyInt(), anyInt(), Mockito.eq(alertId))).thenReturn(notificationList);
        when(participantInfoDao.getInfo(anyList(), anyList())).thenReturn(paxList);
        when(activityFeedService.getActivityById(anyLong(), anyLong(), anyString(), anyString(), anyString(), anyBoolean(), anyBoolean(), anyString())).thenReturn(new NewsfeedItemDTO());
    }
    
    @Test
    public void testNotificationService() {
        PaginatedResponseObject<NotificationDTO> result =
                notificationService.getNotificationList(null, null, null, null, null, null); // yolo?
        
        assertNotNull("Should have data.", result);
        assertNotNull("Should have data.", result.getData());
        assertFalse("Should have data.", result.getData().isEmpty());
    }

    
    @Test(expected = ErrorMessageException.class)
    public void test_getRecognitionsByNotification_notification_not_found(){

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(null);
        
        notificationService.getRecognitionsByNotification(TestConstants.ID_1, TestConstants.ID_1, null, null, null, null, null);
    }
    
    @Test(expected = ErrorMessageException.class)
    public void test_getRecognitionsByNotification_notification_belongs_to_another(){
            
        Alert alert = new Alert();
        alert.setPaxId(TestConstants.ID_1);

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);
        
        notificationService.getRecognitionsByNotification(TestConstants.ID_2, TestConstants.ID_1, null, null, null, null, null);
    }
    
    @Test
    public void test_getRecognitionsByNotification_success() {
        Long  notificationId = 10L;
        Long  nominationId = 100L;
        
        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<RecognitionDetailsDTO>();
        RecognitionDetailsDTO recognition1 = new RecognitionDetailsDTO();
        recognition1.setId(TestConstants.ID_1);
        recognitionDetailsList.add(recognition1);
        
        RecognitionDetailsDTO recognition2 = new RecognitionDetailsDTO();
        recognition1.setId(TestConstants.ID_2);
        recognitionDetailsList.add(recognition2);
        
        Alert alert = new Alert();
        alert.setPaxId(TestConstants.ID_1);

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);
        PowerMockito.when(customAlertDao.getNominationByAlertId(notificationId)).thenReturn(nominationId);
        PowerMockito.when(recognitionService.getRecognitionDetails(nominationId, null, null, null, true))
            .thenReturn(recognitionDetailsList);
        
        List<RecognitionDetailsDTO> response = 
                notificationService.getRecognitionsByNotification(TestConstants.ID_1, notificationId, null, null, null, null, null);
        assertThat(response.size(), equalTo(recognitionDetailsList.size()));
    }
    
    @Test
    public void test_getRecognitionsByNotification_success_empty() {
        
        Long  notificationId = 10L;
        Long  nominationId = 100L;
        
        List<RecognitionDetailsDTO> recognitionDetailsList = new ArrayList<RecognitionDetailsDTO>();

        Alert alert = new Alert();
        alert.setPaxId(TestConstants.ID_1);

        PowerMockito.when(alertDao.findById(anyLong())).thenReturn(alert);
        PowerMockito.when(customAlertDao.getNominationByAlertId(notificationId)).thenReturn(nominationId);
        PowerMockito.when(recognitionService.getRecognitionDetails(nominationId, null, null, null, true))
            .thenReturn(recognitionDetailsList);
        
        List<RecognitionDetailsDTO> response = 
                notificationService.getRecognitionsByNotification(TestConstants.ID_1, notificationId, null, null, null, null, null);
        assertThat(response.size(), equalTo(0));
    }
    
    private void setCommonNotificationData(Map<String, Object> notification) {
        notification.put(NotificationConstants.NOTIFICATION_ID_FIELD, TestConstants.ID_1);
        notification.put(NotificationConstants.NOTIFICATION_STATUS_FIELD, StatusTypeCode.NEW.name());
        notification.put(NotificationConstants.NOTIFICATION_CREATE_DATE_FIELD, new Date());
        notification.put(NotificationConstants.NOTIFICATION_STATUS_DATE_FIELD, new Date());
        notification.put(NotificationConstants.NOMINATION_CREATE_DATE_FIELD, new Date());
        notification.put(NotificationConstants.ACTIVITY_FEED_ITEM_ID_FIELD, TestConstants.ID_1);
        notification.put(NotificationConstants.NOMINATION_ID_FIELD, TestConstants.ID_1);
        notification.put(NotificationConstants.PROGRAM_NAME_FIELD, "Test Program Please Ignore");
        notification.put(NotificationConstants.FROM_PAX_ID_FIELD, TestConstants.ID_1);
        notification.put(NotificationConstants.TO_PAX_ID_FIELD, TestConstants.ID_1);
        notification.put(NotificationConstants.TOTAL_RECIPIENT_COUNT_FIELD, 1);
        notification.put(NotificationConstants.APPROVED_RECIPIENT_COUNT_FIELD, 1);
        notification.put(NotificationConstants.DIRECT_REPORT_COUNT_FIELD, 1);
        notification.put(NotificationConstants.TOTAL_RESULTS_FIELD, 1);
    }

    @Test
    public void test_getNotificationById_setToPax() {
        //the pax is set as a toPax on the notification
        notificationEntry.put(NotificationConstants.TO_PAX_ID_FIELD, paxId);
        notificationList.add(notificationEntry);

        NotificationDTO notification = notificationService.getNotificationById(paxId, alertId);

        assertThat(notification.getToPax().getPaxId(), is(paxId));
        assertThat(notification.getToPax().getFirstName(), is("Natalie"));
        assertThat(notification.getToPax().getLastName(), is("Grace"));

        //the pax is not set as a toPax on the notification
        notificationEntry.put(NotificationConstants.TO_PAX_ID_FIELD, 6L);
        notificationList.add(notificationEntry);

        NotificationDTO notificationDTO = notificationService.getNotificationById(paxId, alertId);

        assertThat(notificationDTO.getToPax().getPaxId(), is(6L));
    }

    @Test
    public void test_getNotificationById_setFromPax() {
        //the pax is set as a fromPax on the notification
        notificationEntry.put(NotificationConstants.FROM_PAX_ID_FIELD, paxId);
        notificationList.add(notificationEntry);

        NotificationDTO notification = notificationService.getNotificationById(paxId, alertId);

        assertThat(notification.getFromPax().getPaxId(), is(paxId));
        assertThat(notification.getFromPax().getFirstName(), is("Natalie"));
        assertThat(notification.getFromPax().getLastName(), is("Grace"));

        //the pax is not set as a fromPax on the notification
        notificationEntry.remove(NotificationConstants.FROM_PAX_ID_FIELD, paxId);
        notificationEntry.put(NotificationConstants.FROM_PAX_ID_FIELD, 6L);
        notificationList.add(notificationEntry);

        NotificationDTO notificationDTO = notificationService.getNotificationById(paxId, alertId);

        assertThat(notificationDTO.getFromPax().getPaxId(), is(6L));
    }

    @Test
    public void test_getNotificationById_setLikePax() {
        //the pax is set as a likePax on the notification
        notificationEntry.put(NotificationConstants.LIKE_PAX_ID_FIELD, paxId);
        notificationEntry.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.LIKE_RECOGNITION_RECEIVER.name());
        notificationEntry.put(NotificationConstants.LIKE_COUNT_FIELD, 20);
        notificationList.add(notificationEntry);

        NotificationDTO notification = notificationService.getNotificationById(paxId, alertId);

        assertThat(((LikeNotificationDTO) notification).getLikePax().getPaxId(), is(paxId));

        //the pax is not set as a likePax on the notification
        notificationEntry.remove(NotificationConstants.LIKE_PAX_ID_FIELD, paxId);
        notificationEntry.put(NotificationConstants.LIKE_PAX_ID_FIELD, 6L);
        notificationList.add(notificationEntry);

        NotificationDTO notificationDTO = notificationService.getNotificationById(paxId, alertId);

        assertThat(((LikeNotificationDTO) notificationDTO).getLikePax().getPaxId(), is(6L));
    }


    @Test
    public void test_getNotificationById_setCommentPax() {
        //the pax is set as a commentPax on the notification
        notificationEntry.put(NotificationConstants.COMMENT_PAX_ID_FIELD, paxId);
        notificationEntry.put(NotificationConstants.NOTIFICATION_TYPE_FIELD, AlertSubType.COMMENT_RECOGNITION_RECEIVER.name());
        notificationEntry.put(NotificationConstants.COMMENT_ID_FIELD, 4L);
        notificationEntry.put(NotificationConstants.COMMENT_CREATE_DATE_FIELD, new Date());
        notificationList.add(notificationEntry);

        NotificationDTO notification = notificationService.getNotificationById(paxId, alertId);

        assertThat(((CommentNotificationDTO) notification).getComment().getPax().getPaxId(), is(paxId));

        //the pax is not set as a commentPax on the notification
        notificationEntry.remove(NotificationConstants.COMMENT_PAX_ID_FIELD, paxId);
        notificationEntry.put(NotificationConstants.COMMENT_PAX_ID_FIELD, 6L);
        notificationList.add(notificationEntry);

        NotificationDTO notificationDTO = notificationService.getNotificationById(paxId, alertId);

        assertThat(((CommentNotificationDTO) notificationDTO).getComment().getPax().getPaxId(), is(6L));
    }
}
