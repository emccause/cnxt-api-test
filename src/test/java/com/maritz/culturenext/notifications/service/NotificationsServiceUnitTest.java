package com.maritz.culturenext.notifications.service;

import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.support.util.StatusTypeCode;
import com.maritz.core.util.StringUtils;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.culturenext.enums.AlertSubType;
import com.maritz.culturenext.notifications.dto.NotificationCountDTO;
import com.maritz.culturenext.notifications.services.NotificationsService;
import com.maritz.test.AbstractDatabaseTest;

public class NotificationsServiceUnitTest extends AbstractDatabaseTest {
    
    @Inject private NotificationsService notificationsService;
    
    @Test
    public void test_notification_count_differences_by_type() {
        
        NotificationCountDTO countWithAll = notificationsService.getNotificationCount(
                TestConstants.PAX_MUDDAM,
                StatusTypeCode.NEW.name(),
                StringUtils.formatDelimitedData(
                        AlertSubType.RECOGNITION.name(),
                        AlertSubType.LIKE_RECOGNITION_RECEIVER.name(),
                        AlertSubType.RECOGNITION_APPROVAL.name()
                    )
            ); // null would also work, but I don't want this test to break if a new alert is added with a 4th type.
        
        NotificationCountDTO countNoLikes = notificationsService.getNotificationCount(
                TestConstants.PAX_MUDDAM,
                StatusTypeCode.NEW.name(),
                StringUtils.formatDelimitedData(
                        AlertSubType.RECOGNITION.name(),
                        AlertSubType.RECOGNITION_APPROVAL.name()
                    )
            );
                
        NotificationCountDTO countLikesOnly = notificationsService.getNotificationCount(
                TestConstants.PAX_MUDDAM,
                StatusTypeCode.NEW.name(),
                StringUtils.formatDelimitedData(
                        AlertSubType.LIKE_RECOGNITION_RECEIVER.name()
                    )
            );
        
        assertTrue(
                "count without likes + count of likes should add up to the count of all notifications",
                countWithAll.getAlertCount().equals(countNoLikes .getAlertCount()+ countLikesOnly.getAlertCount())
            );
    }
}
