package com.maritz.culturenext.cdn.rest;

import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractRestTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.net.URI;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CdnRestServiceTest extends AbstractRestTest {

    private static final String URL_CDN = "/rest/cdn";
    private static final String URL_CDN_NAME_PARAM = URL_CDN + "?name=serviceAccount.type";

    @Test
    public void test_getCdnUrl_happy_path() throws Exception {
        mockMvc.perform(
                get(URL_CDN)
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .with(authToken(TestConstants.USER_HARWELLM))
        ).andExpect(status().isOk());
    }
    @Test
    public void test_getCdnUrl_happy_path_issue_461() throws Exception {
        MvcResult result = mockMvc.perform(
                get(URL_CDN_NAME_PARAM)
                        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .with(authToken(TestConstants.USER_HARWELLM)))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
        Assert.assertTrue(result.getResponse().getContentAsString().isEmpty());
    }

    @Test
    public void test_getCdnUrl_fail_no_authorized() throws Exception {
        mockMvc.perform(
                get(URL_CDN).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isUnauthorized());
    }
}

