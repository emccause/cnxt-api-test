package com.maritz.culturenext.jdbc.tomcat.interceptor;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import javax.inject.Inject;

import org.junit.Test;

import com.maritz.core.jpa.entity.Address;
import com.maritz.core.jpa.entity.HistoryAddress;
import com.maritz.core.jpa.repository.AddressRepository;
import com.maritz.core.jpa.repository.HistoryAddressRepository;
import com.maritz.core.jpa.support.util.AddressTypeCode;
import com.maritz.core.security.Security;
import com.maritz.core.security.authentication.AuthenticationService;
import com.maritz.culturenext.constants.TestConstants;
import com.maritz.test.AbstractDatabaseTest;

public class ContextInfoUserNameInterceptorAuditTest extends AbstractDatabaseTest {

    @Inject Security security;
    @Inject AuthenticationService authenticationService;
    @Inject AddressRepository addressRepository;
    @Inject HistoryAddressRepository historyAddressRepository;

    @Test
    public void audit_name() {
        //authentication user1
        authenticationService.authenticate(TestConstants.USER_MUDDAM);

        //create an address record
        Address address1 = addressRepository.save(new Address()
            .setPaxId(security.getPaxId())
            .setAddressTypeCode(AddressTypeCode.HOME.name())
            .setAddress1("123 street")
            .setCity("city")
            .setState("ST")
            .setZip("12345")
            .setCountryCode("US")
            .setCode1Bypass("Y")
        );

        //make sure the createId/updateId columns are set to the logged in user (user1)
        assertThat(address1.getCreateId(), is(TestConstants.USER_MUDDAM));
        assertThat(address1.getUpdateId(), is(TestConstants.USER_MUDDAM));

        //authentication user2
        authenticationService.authenticate(TestConstants.USER_RIDERAC1);

        //create an address record
        Address address2 = addressRepository.save(new Address()
            .setPaxId(security.getPaxId())
            .setAddressTypeCode(AddressTypeCode.HOME.name())
            .setAddress1("123 street")
            .setCity("city")
            .setState("ST")
            .setZip("12345")
            .setCountryCode("US")
            .setCode1Bypass("Y")
        );

        //make sure the createId/updateId columns are set to the logged in user (user2)
        assertThat(address2.getCreateId(), is(TestConstants.USER_RIDERAC1));
        assertThat(address2.getUpdateId(), is(TestConstants.USER_RIDERAC1));


        //authentication user1
        authenticationService.authenticate(TestConstants.USER_MUDDAM);

        //delete user1's address record
        addressRepository.delete(address1);
        addressRepository.flush();

        HistoryAddress historyAddress1 = historyAddressRepository.findBy()
            .where("addressId").eq(address1.getAddressId())
            .orderDesc("historyId")
            .findOne()
        ;

        //make sure the history table reflects the logged in user that deleted the record (user1)
        assertThat(historyAddress1.getChangeId(), is(TestConstants.USER_MUDDAM));

        //authenticate user2
        authenticationService.authenticate(TestConstants.USER_RIDERAC1);

        //delete user2's address record
        addressRepository.delete(address2);
        addressRepository.flush();

        HistoryAddress historyAddress2 = historyAddressRepository.findBy()
            .where("addressId").eq(address2.getAddressId())
            .orderDesc("historyId")
            .findOne()
        ;

        //make sure the history table reflects the logged in user that deleted the record (user2)
        assertThat(historyAddress2.getChangeId(), is(TestConstants.USER_RIDERAC1));
    }

}
