#!/usr/bin/env bash

echo "
# ====================================================================
#
#                    M365-Services STARTUP SCRIPT
#
# ===================================================================="

export AWS_ACCESS_KEY_ID="ACCESS KEY ID"
export AWS_SECRET_ACCESS_KEY="SECRET KEY"

# get sqljdbc4.jar
aws s3 cp s3://m365-team/sqljdbc4.jar /opt/jdk-8/lib/sqljdbc4.jar
export CLASSPATH=/opt/jdk-8/lib/sqljdbc.jar:/opt/jdk-8/lib

# install underscore cli
export PATH=$PATH:/opt/node-0.12/bin
npm install -g underscore-cli

# create ssh certificate file to connect to application instances
mkdir -p /tmp/keys

cat << EOF > /tmp/keys/m365-app.pem
-----BEGIN RSA PRIVATE KEY-----
<INSERT SSH KEY HERE>
-----END RSA PRIVATE KEY-----
EOF

chown bamboo /tmp/keys
chown bamboo /tmp/keys/m365-app.pem
chmod 755 /tmp/keys
chmod 600 /tmp/keys/m365-app.pem

# everyone can read and write. This gets updated on each API build
chmod 666 /opt/maven-3.3/conf/settings.xml

# download API certs
aws s3 cp s3://m365-certs/MaritzMGTSInternetOps.cer       ./MaritzMGTSInternetOps.cer
aws s3 cp s3://m365-certs/MaritzMGTSProxyCA1cert.cer      ./MaritzMGTSProxyCA1cert.cer
aws s3 cp s3://m365-certs/MaritzMGTSProxyCA2cert.cer      ./MaritzMGTSProxyCA2cert.cer
aws s3 cp s3://m365-certs/components-build.cer            ./components-build.cer
aws s3 cp s3://m365-certs/components-build.maritz.com.crt ./components-build.maritz.com.crt
aws s3 cp s3://m365-certs/proxy.maritz.com.cer            ./proxy.maritz.com.cer

# install API certs
mkdir -p /opt/jdk-8/lib/security
sudo keytool -import -noprompt -alias maritz-internet-ops -keystore "/opt/jdk-8/lib/security/cacerts" -file "./MaritzMGTSInternetOps.cer"       -storepass changeit -keypass changeit
sudo keytool -import -noprompt -alias components-build    -keystore "/opt/jdk-8/lib/security/cacerts" -file "./components-build.maritz.com.crt" -storepass changeit -keypass changeit
sudo keytool -import -noprompt -alias maritz-proxy-ca1    -keystore "/opt/jdk-8/lib/security/cacerts" -file "./MaritzMGTSProxyCA1cert.cer"      -storepass changeit -keypass changeit
sudo keytool -import -noprompt -alias maritz-proxy-ca2    -keystore "/opt/jdk-8/lib/security/cacerts" -file "./MaritzMGTSProxyCA2cert.cer"      -storepass changeit -keypass changeit
sudo keytool -import -noprompt -alias proxy-maritz-com    -keystore "/opt/jdk-8/lib/security/cacerts" -file "./proxy.maritz.com.cer"            -storepass changeit -keypass changeit
sudo keytool -import -noprompt -alias artifactory_newer   -keystore "/opt/jdk-8/lib/security/cacerts" -file "./components-build.cer"            -storepass changeit -keypass changeit

# setup git authentication file for bamboo user
runuser -l bamboo -c 'mkdir -p ~/.ssh'

runuser -l bamboo -c 'cat << EOF > ~/.ssh/config
Host bitbucket.org
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
EOF'

runuser -l bamboo -c 'cat << EOF > ~/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY-----
<INSERT BITBUCKET KEY HERE>
-----END RSA PRIVATE KEY-----
EOF'

runuser -l bamboo -c 'chmod 600 ~/.ssh/id_rsa'
runuser -l bamboo -c 'chmod 600 ~/.ssh/config'
