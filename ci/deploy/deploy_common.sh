#!/usr/bin/env bash

. ./ci/common/common.sh

function get_tenant() {
  path=$1
  i=$2
  TENANT=$(underscore --in "$path" select ".tenants :nth-child($i)")
}

function get_tenant_property() {
  tenant=$1
  selector=$2
  TENANT_PROPERTY=$(underscore --data "$tenant" select "$selector" | underscore reduce 'value' | sed 's/\"//g')
}
