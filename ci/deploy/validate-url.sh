#!/usr/bin/env bash

# expected ENV vars:
# env (e.g. d, d-future)
# tenant_name (e.g. google)

printf "\nVerifying deployment URL . . .\n"

url="http://$env-api.cnxtcloud.com/culture-next/rest/projects/$tenant_name/health-check"
status=''
try_counter=1
MAX_TRY_COUNT=10

until [[ $status == 200 ]]; do
    printf "Polling $url (Attempt $try_counter of $MAX_TRY_COUNT) ... "
    status=$( curl -w %{http_code} -s --output /dev/null $url )
    printf "HTTP $status\n"

    if [[ $status != 200 ]]; then
      sleep 10s
      (( try_counter ++ ))
    fi

    if (( $try_counter > $MAX_TRY_COUNT )); then
      echo "Polled URL $MAX_TRY_COUNT times without success."
      exit 1
    fi
done

printf "complete.\n"
