#!/usr/bin/env bash

# expected ENV vars:
# app_ip (ip of api instance)
# tenant_user (linux user, e.g. tenant_google)
# war_name (name of the war file without extention)
# bamboo_AWS_ACCESS_KEY_ID (Bamboo global var)
# bamboo_AWS_SECRET_ACCESS_KEY (Bamboo global var)

export AWS_ACCESS_KEY_ID=$bamboo_AWS_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$bamboo_AWS_SECRET_ACCESS_KEY

# remove versioning from war name
mv *.war $war_name.war

# get certificate file
mkdir -p keys
aws s3 cp s3://m365-keys/m365-app.pem ./keys
chmod 755 ./keys
chmod 600 ./keys/m365-app.pem

ssh -o StrictHostKeyChecking=no -i ./keys/m365-app.pem "$tenant_user@$app_ip" "
echo 'Stopping Tomcat'
./maritz/bin/shutdown.sh
# killing just incase shutdwon fails
sudo pkill -u tenant_google

echo 'Removing existing wars'
rm -f ./maritz/webapps/$war_name.war
rm -fr ./maritz/webapps/$war_name
"

echo "Copying ./$war_name.war => $tenant_user@$app_ip:maritz/webapps/$war_name.war"
scp -o StrictHostKeyChecking=no -i ./keys/m365-app.pem ./$war_name.war "$tenant_user@$app_ip":maritz/webapps/$war_name.war

ssh -o StrictHostKeyChecking=no "$tenant_user@$app_ip" -i ./keys/m365-app.pem "
echo 'Starting Tomcat'
./maritz/bin/startup.sh
"
