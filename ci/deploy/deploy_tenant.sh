#!/usr/bin/env bash

set -euo pipefail

# Expected Environment variables
# Auto provided by Bamboo
#
# Part of Build Plan
#   env='d-future'
#   app_ip='54.174.153.166'
#   db_ip='52.23.232.184'
export db_user='tenant_admin'
export db_pw='Password1'
export db_contexts='live'

MAX_TRY_COUNT=10

. ci/common/common.sh

# copy over ssh certificate file
mkdir -p keys
cp /tmp/keys/m365-app.pem ./keys
chmod 755 ./keys
chmod 600 ./keys/m365-app.pem

# bring in foreach tenant function
. ci/deploy/foreach-tenant.sh

# update execute permissions
chmod ugo+x SQL/liquibase-3.4.1-bin/liquibase

# remove versioning from war name
# mv maritz-m365-*-SNAPSHOT.war maritz-m365.war

cat > script.sh << 'EOF'

    index="$1"
    tenant="$2"
    name="$3"
    is_client="$4"

    if [[ $is_client == 'true' ]]; then
      echo "Performing predeployment checks . . ."
        ssh -o StrictHostKeyChecking=no -i ./keys/m365-app.pem "tenant_$name"@"$app_ip" '
          echo "Passed ssh connection test."
        '
      if [ $? == 0 ]; then
        printf "complete.\n"

        printf "\nRunning Liquibase for $name . . . "
            cd SQL/liquibase-3.4.1-bin

            db_name="tenant_$name"
            ./liquibase \
                  --url="jdbc:sqlserver://$db_ip:1433;databaseName=$db_name" \
                  --contexts=$db_contexts \
                  --username=$db_user \
                  --password=$db_pw \
                  update

            if [ $? != 0 ]; then
              exit 1
            fi

            cd "$OLDPWD"
        printf "complete.\n"


        printf "\nDeploying war for $name . . . "
            ssh -o StrictHostKeyChecking=no -i ./keys/m365-app.pem "tenant_$name"@"$app_ip" '
              # Stop the service
              ./maritz/bin/shutdown.sh

              # remove the existing war
              rm -f ./maritz/webapps/ROOT.war
              rm -fr ./maritz/webapps/ROOT
            '

            if [ $? != 0 ]; then
              exit 1
            fi

            # copy war to destination server
            scp -o StrictHostKeyChecking=no -i ./keys/m365-app.pem ./ROOT.war "tenant_$name"@"$app_ip":maritz/webapps/ROOT.war

            if [ $? != 0 ]; then
              exit 1
            fi

            # start the service back up
            ssh -o StrictHostKeyChecking=no "tenant_$name"@"$app_ip" -i ./keys/m365-app.pem '
              ./maritz/bin/startup.sh
            '

            if [ $? != 0 ]; then
              exit 1
            fi
        printf "complete.\n"


        printf "\nVerifying deployment for $name . . .\n"
            url="http://$env-$name-api.cnxtcloud.com/rest/health-check"
            status=''
            try_counter=1

            until [[ $status == 200 ]]; do
              printf "Polling $url (Attempt $try_counter of $MAX_TRY_COUNT) ... "
              status=$( curl -w %{http_code} -s --output /dev/null $url )
              printf "HTTP $status\n"

              if [[ $status != 200 ]]; then
                sleep 10s
                (( try_counter ++ ))
              fi

              if (( $try_counter > $MAX_TRY_COUNT )); then
                echo "Polled URL $MAX_TRY_COUNT times without success."
                exit 1
              fi
            done
        printf "\n\ncomplete.\n"
      else
        printf "\n$name predeployment check failed (has it been provisioned?). Skipping.\n\n"
      fi
    else
      printf "\n$name is not a client - skipping.\n\n"
    fi
EOF

foreach_tenant ${bamboo_build_working_directory}/ci/tenants.json ${bamboo_build_working_directory}/script.sh

if [ $? != 0 ]; then
  exit 1
fi
