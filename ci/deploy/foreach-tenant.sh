#!/usr/bin/env bash

. ${bamboo_build_working_directory}/ci/deploy/deploy_common.sh

function foreach_tenant() {
  local data_file=$1
  local script=$2
  local i=1
  local name=' '

  until [ -z "$name" ]; do
    echo "-----------------------"
    get_tenant $data_file $i

    get_tenant_property "$TENANT" '.name'
    name=$TENANT_PROPERTY

    get_tenant_property "$TENANT" '.is_client'
    is_client=$TENANT_PROPERTY

    if [ "$name" ]; then
      . ${script} $i "${TENANT}" "${name}" "${is_client}"
    fi

    (( i ++ ))
  done
}
