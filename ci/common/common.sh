#!/usr/bin/env bash

# Expected Environment variables

export CLASSPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/sqljdbc4.jar:.
export PATH=$PATH:/opt/node-0.10/bin
export KEY_PATH=/tmp/keys/m365-app.pem