#!/usr/bin/env bash

# Set flags to allow immediate failure for specific exceptions
set -euo pipefail

. ./ci/common/common.sh

cd ./ci/build

#Drop test DB

javac sqlrunner.java

sed -i -- "s/~db_name~/$db_name/g" drop-database.sql

echo "Dropping database $db_name on $db_server. . ."
java sqlrunner "$db_server" master "$db_login" "$db_pw" drop-database.sql
sql_status=$?

printf "Database $db_name drop "
if [ "$sql_status" == '0' ]; then
  echo "succeeded."
else
  echo "failed."
  exit 1
fi