USE [master]

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'~db_user~')
    BEGIN
        CREATE LOGIN ~db_user~ WITH PASSWORD=N'~db_user_pw~', DEFAULT_DATABASE=[~db_name~], DEFAULT_LANGUAGE = US_ENGLISH
        ALTER LOGIN ~db_user~ ENABLE
        CREATE USER [~db_user~] FOR LOGIN [~db_user~]
        EXEC sp_addrolemember N'db_owner', N'~db_user~'
    END;


