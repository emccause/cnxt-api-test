USE [master]

EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'[~db_name~]';
DROP DATABASE [~db_name~];
