#!/usr/bin/env bash

# Expected Environment variables
# Auto provided by Bamboo
#   bamboo_buildResultKey
# Part of Build Plan

get_database_name() {
  TEMP_DATABASE_NAME=$(echo "${bamboo_buildResultKey:-}${BUILDKITE_BUILD_NUMBER:-}" | tr - _)
  echo $TEMP_DATABASE_NAME
}
