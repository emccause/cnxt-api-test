#!/usr/bin/env bash

# Set flags to allow immediate failure for specific exceptions
set -euo pipefail

cd "$( dirname "${BASH_SOURCE[0]}" )"

### Create container dir for files
if [[ ! -e db-container ]]; then
    mkdir db-container

    ## Create integ test DB and integ test user
    cat create-container-database.sql | sed "s/~db_name~/${DB_NAME}/g" > db-container/create-container-database.sql
    cat create-container-database-user.sql | \
    sed "s/~db_user~/${DB_USER_LOGIN}/g; s/~db_user_pw~/${DB_USER_PW}/g; s/~db_name~/${DB_NAME}/g" >> db-container/create-container-database.sql
fi

## Include driver jar for SQL Server and compile sqlrunner tool
export CLASSPATH="../common/sqljdbc4.jar:."

javac sqlrunner.java

echo "Creating database $DB_NAME on $DB_SERVER_IP and database user $DB_USER_LOGIN for database $DB_NAME. . ."
java sqlrunner "$DB_SERVER_IP" master "$DB_SA" "$DB_SA_PW" db-container/create-container-database.sql

SQL_STATUS=$?
printf "Database $DB_NAME and user $DB_USER_LOGIN creation "
if [[ "$SQL_STATUS" == '0' ]]; then
  echo "succeeded."
else
  echo "failed."
  exit 1
fi
