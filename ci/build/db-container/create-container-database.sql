USE [master]
-- Only run if DB does not exist
IF EXISTS (SELECT name FROM sys.databases WHERE name = N'tenant_google') RETURN

CREATE DATABASE [tenant_google]
  CONTAINMENT = NONE
  ON  PRIMARY
  ( NAME = N'tenant_google',
    FILENAME = N'/var/opt/mssql/data/tenant_google.mdf',
    SIZE = 69632KB,
    MAXSIZE = UNLIMITED,
    FILEGROWTH = 1024KB )
  LOG ON
  ( NAME = N'tenant_google_log',
    FILENAME = N'/var/opt/mssql/data/tenant_google_log.ldf',
    SIZE = 76736KB,
    MAXSIZE = 2048GB,
    FILEGROWTH = 10% );

ALTER DATABASE [tenant_google] SET COMPATIBILITY_LEVEL = 120;

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled')) BEGIN
  EXEC [tenant_google].[dbo].[sp_fulltext_database] @action = 'enable';
END

ALTER DATABASE [tenant_google] SET ANSI_NULL_DEFAULT OFF;

ALTER DATABASE [tenant_google] SET ANSI_NULLS OFF;

ALTER DATABASE [tenant_google] SET ANSI_PADDING OFF;

ALTER DATABASE [tenant_google] SET ANSI_WARNINGS OFF;

ALTER DATABASE [tenant_google] SET ARITHABORT OFF;

ALTER DATABASE [tenant_google] SET AUTO_CLOSE OFF;

ALTER DATABASE [tenant_google] SET AUTO_SHRINK OFF;

ALTER DATABASE [tenant_google] SET AUTO_UPDATE_STATISTICS ON;

ALTER DATABASE [tenant_google] SET CURSOR_CLOSE_ON_COMMIT OFF;

ALTER DATABASE [tenant_google] SET CURSOR_DEFAULT GLOBAL;

ALTER DATABASE [tenant_google] SET CONCAT_NULL_YIELDS_NULL OFF;

ALTER DATABASE [tenant_google] SET NUMERIC_ROUNDABORT OFF;

ALTER DATABASE [tenant_google] SET QUOTED_IDENTIFIER OFF;

ALTER DATABASE [tenant_google] SET RECURSIVE_TRIGGERS OFF;

ALTER DATABASE [tenant_google] SET DISABLE_BROKER;

ALTER DATABASE [tenant_google] SET AUTO_UPDATE_STATISTICS_ASYNC OFF;

ALTER DATABASE [tenant_google] SET DATE_CORRELATION_OPTIMIZATION OFF;

ALTER DATABASE [tenant_google] SET TRUSTWORTHY OFF;

ALTER DATABASE [tenant_google] SET ALLOW_SNAPSHOT_ISOLATION OFF;

ALTER DATABASE [tenant_google] SET PARAMETERIZATION SIMPLE;

ALTER DATABASE [tenant_google] SET READ_COMMITTED_SNAPSHOT OFF;

ALTER DATABASE [tenant_google] SET HONOR_BROKER_PRIORITY OFF;

ALTER DATABASE [tenant_google] SET RECOVERY SIMPLE;

ALTER DATABASE [tenant_google] SET MULTI_USER;

ALTER DATABASE [tenant_google] SET PAGE_VERIFY CHECKSUM;

ALTER DATABASE [tenant_google] SET DB_CHAINING OFF;

ALTER DATABASE [tenant_google] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF );

ALTER DATABASE [tenant_google] SET TARGET_RECOVERY_TIME = 0 SECONDS;

ALTER DATABASE [tenant_google] SET DELAYED_DURABILITY = DISABLED;

ALTER DATABASE [tenant_google] SET READ_WRITE;
USE [master]

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'tenant_client')
    BEGIN
        CREATE LOGIN tenant_client WITH PASSWORD=N'n^2GGSlxO4', DEFAULT_DATABASE=[tenant_google], DEFAULT_LANGUAGE = US_ENGLISH
        ALTER LOGIN tenant_client ENABLE
        CREATE USER [tenant_client] FOR LOGIN [tenant_client]
        EXEC sp_addrolemember N'db_owner', N'tenant_client'
    END;


