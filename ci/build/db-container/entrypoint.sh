#!/usr/bin/env bash
set -e

DB_SA="SA"
#  SA_PASSWORD comes from docker-compose env
DB_SERVER_IP='localhost'
DB_NAME='tenant_google' # This is hard-coded in liquibase properties

# wait for MSSQL server to start
export STATUS=1
i=0
echo "Attempting to create database ${DB_NAME} on ${DB_SERVER_IP}. Waiting for SQL Server to start..."
sleep 5

while [[ ${STATUS} -ne 0 ]] && [[ $i -lt 30 ]]; do
    ((i=i+1))
    echo "Attempt to connect $i..."
    /opt/mssql-tools/bin/sqlcmd -S ${DB_SERVER_IP} -U ${DB_SA} -P ${SA_PASSWORD} -Q "select 1" >> /dev/null
    STATUS=$?
    sleep 1
done

if [[ ${STATUS} -ne 0 ]]; then
	echo "Error: MSSQL SERVER took more than thirty seconds to start up."
	exit 1
fi

# Run the setup script to create the DB
/opt/mssql-tools/bin/sqlcmd -S ${DB_SERVER_IP} -U ${DB_SA} -P ${SA_PASSWORD} -i ./create-container-database.sql

SQL_STATUS=$?
printf "Database and user creation "
if [[ "$SQL_STATUS" == '0' ]]; then
  echo "succeeded."
else
  echo "failed."
  exit 1
fi

### Run Liquibase to setup test data - FIXME: cannot get this to run
#export JAVA_OPTS="-Xms1024m -Xmx2048m -XX:+UseG1GC -XX:+UseStringDeduplication"
#echo "Running Liquibase against DB ${DB_NAME} with JAVA_OPTS: ${JAVA_OPTS}..."
#cd ./sql/liquibase-3.4.1-bin && ./liquibase \
#    --url="jdbc:sqlserver://$DB_SERVER_IP:1433;databaseName=$DB_NAME" \
#    --contexts='live,test' \
#    --username=${DB_SA} \
#    --password=${DB_SA_PW} \
#    update
#
#SQL_STATUS=$?
#printf "Liquibase result was "
#if [[ "$SQL_STATUS" == '0' ]]; then
#  echo "successful."
#else
#  echo "failed."
#  exit 1
#fi

