#!/usr/bin/env bash

# Expected Environment variables
# Auto provided by Bamboo
#   bamboo_buildResultKey  (build_common.sh)
# Part of Build Plan
#   db_server

. ./ci/build/build_common.sh

get_database_name
db_name=$TEMP_DATABASE_NAME

file='src/test/resources/config/application-development.properties'
cat "$file"     | perl -ne "s/(?<=\/\/).+(?=:1433)/$db_server/ig; print;"     > "$file"temp
cat "$file"temp | perl -ne "s/(?<=databaseName=).+(?=)/$db_name/ig; print;" > "$file"
rm -f "$file"temp