import java.sql.*;
import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;

public class sqlrunner {

   public static void main(String[] args) {
      int FAIL = 1;
      int SUCCESS = 0;
      String connectionUrl = "jdbc:sqlserver://" + args[0] + ":1433;databaseName=" + args[1];
      String username = args[2];
      String password = args[3];
      Path sqlFileName = FileSystems.getDefault().getPath("", args[4]);
      Connection connection = null;
      Statement statement = null;
      File sqlFile = null;
      String sql = "";

      System.out.println(connectionUrl);

      try (InputStream in = Files.newInputStream(sqlFileName);
           BufferedReader reader = new BufferedReader(new InputStreamReader(in)))
      {
        String line = null;
        while ((line = reader.readLine()) != null)
        {
            sql += (line + "\n");
        }

        System.out.println(sql);
      }
      catch (IOException x)
      {
        System.err.println(x);
      }

      if (sql != "") {
        try {
          Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
          connection = DriverManager.getConnection(connectionUrl, username, password);
          statement = connection.createStatement();
          statement.executeUpdate(sql);
        }
        catch (Exception e) {
          e.printStackTrace();
          System.exit(FAIL);
        }
        finally {
          if (statement != null) try { statement.close(); } catch(Exception e) { }
          if (connection != null) try { connection.close(); } catch(Exception e) { }
        }
      }

      System.exit(SUCCESS);
   }
}
