#!/usr/bin/env bash

# See common.sh for expected env vars

cd "$( dirname "${BASH_SOURCE[0]}" )"

## get expected env vars
. ./common.sh

## Run liquibase scripts
pushd ../../SQL/liquibase-3.4.1-bin

CONTEXTS="'live,test'"  # this must match with --contexts below. can't put this in line, trouble with the single quotes.
echo "Running Liquibase against DB ${DB_NAME} with Contexts ${CONTEXTS}..."
./liquibase \
    --url="jdbc:sqlserver://$DB_SERVER_IP:1433;databaseName=$DB_NAME" \
    --contexts='live,test' \
    --username=${DB_SA} \
    --password=${DB_SA_PW} \
    update

echo ""
popd
