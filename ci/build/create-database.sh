#!/usr/bin/env bash

# Expected Environment variables
# Auto provided by Bamboo
#   bamboo_buildResultKey  (build_common.sh)
#   bamboo_working_directory (common.sh)
# Part of Build Plan
#   db_server
#   db_login
#   db_pw

chmod 555 ci/**/*.sh

. ./ci/common/common.sh
. ./ci/build/build_common.sh

cd ./ci/build

get_database_name
db_name=$TEMP_DATABASE_NAME

javac sqlrunner.java

sed -i -- "s/~db_name~/$db_name/g" create-database.sql

echo "Creating database $db_name on $db_server. . ."
java sqlrunner "$db_server" master "$db_login" "$db_pw" create-database.sql
sql_status=$?

printf "Database $db_name creation "
if [ "$sql_status" == '0' ]; then
  echo "succeeded."
else
  echo "failed."
  exit 1
fi
