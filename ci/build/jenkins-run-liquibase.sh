#!/usr/bin/env bash

# Set flags to allow immediate failure for specific exceptions
set -euo pipefail

#Run Liquibase

cd ./SQL/liquibase-3.4.1-bin
chmod ugo+x liquibase

./liquibase \
    --url="jdbc:sqlserver://$DB_SERVER_IP:1433;databaseName=$DB_NAME" \
    --contexts='live,test' \
    --username=${DB_SA} \
    --password=${DB_SA_PW} \
    update
