#!/usr/bin/env bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

# start SQL Server container
pushd ../..
docker-compose up &
sleep 10
popd

# Create DB, users, schema and load
source ./run-liquibase.sh

## Run integ tests
pushd ../..
mvn clean verify -Dspring.profiles.active=local
popd

function cleanup {
    pushd ../..
    docker-compose down
    popd
}
trap cleanup EXIT
