#!/usr/bin/env bash

# Expected Environment variables

export PATH=$JAVA_HOME:$MAVEN_HOME/bin:$PATH
export M2_HOME=$MAVEN_HOME
export MAVEN_OPTS='-Xmx2048m -XX:MaxPermSize=1024m'
export AWS_ACCESS_KEY_ID=${bamboo_AWS_ACCESS_KEY_ID:-}${AWS_ACCESS_KEY_ID:-}
export AWS_SECRET_ACCESS_KEY=${bamboo_AWS_SECRET_ACCESS_KEY:-}${AWS_SECRET_ACCESS_KEY:-}

aws s3 cp s3://m365-team/settingsnew.xml ./doc/settings.xml

# cp is aliased with cp --interactive so use the direct executable
/bin/cp -f doc/settings.xml $MAVEN_HOME/conf/settings.xml

mvn clean install -Dsurefire.rerunFailingTestsCount=3 -U
# mvn -DskipTests