#==================================#
#====== Configure API Server ======#
#==================================#

# This script will prepare a Linux server to be configured for a multitenant Tomcat environment.

#TODOs:
# create the volume seperate from the instance. If the instance dies or is terminated, recovery may be possible

#===========================#
#====== Configurables ======#
#===========================#
# env="development"

# ===== install tools
yum install wget -y
yum install httpd -y  # Apache
yum install mod_ssl -y   # Apache ssl module
curl -sL https://rpm.nodesource.com/setup | bash -
yum install -y nodejs

#TODO test installing from here, and if successful, remove the wget's below
# yum install java-1.7.0-openjdk-devel -y
# yum install tomcat -y # TODO: lock down to version 8

export PATH=$PATH:/opt/node-0.12/bin
npm install -g underscore-cli

# ===== install java
cd /usr/share
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u79-b15/jdk-7u79-linux-x64.tar.gz"
tar -xzf jdk-7u79-linux-x64.tar.gz
ln -s /usr/share/jdk1.7.0_79 /usr/share/java7

# ===== install tomcat
cd /usr/share
wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.64/bin/apache-tomcat-7.0.64.tar.gz
tar -xzf apache-tomcat-7.0.64.tar.gz
ln -s /usr/share/apache-tomcat-7.0.64 /usr/share/tomcat7
chmod -R o+rw "/usr/share/tomcat7/logs"

# ====== install sql drivers
cd /usr/share/apache-tomcat-7.0.64/lib
# cd /usr/share/tomcat/lib
wget https://s3.amazonaws.com/m365-team/sqljdbc4.jar

# ====== Download SSL certificate ======
# mkdir /etc/httpd/ssl_certs
# cd /etc/httpd/ssl_certs
# wget https://s3.amazonaws.com/m365-team/cnxtcloud-self-signed-cert.pem
# wget https://s3.amazonaws.com/m365-team/cnxtcloud-self-signed-cert-key.pem

# ====== configure Apache
# mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.bak
cd /etc/httpd/conf.d
cat << EOF > "base_tenant.conf"
# Define ssl_cert /etc/httpd/ssl_certs/cnxtcloud-self-signed-cert.pem
# Define ssl_key /etc/httpd/ssl_certs/cnxtcloud-self-signed-cert-key.pem

# Listen 443
# Listen 80 is already in /conf/httpd.conf
# LoadModule  ssl_module           modules/mod_ssl.so
LoadModule  proxy_module         modules/mod_proxy.so
LoadModule  proxy_http_module    modules/mod_proxy_http.so
EOF
