#=========================================#
#====== Configure API Client Tenant ======#
##========================================#

#===== AWS EC2 instance =====
# AMI ID: ami-12663b7a
# type: t2.medium
# enable termination protection

#===================#
#====== NOTES ======#
#===================#
# - Change the configuration for each tenant

#===================#
#====== TODOs ======#
#===================#
# - right now each user password is 'password'
# - create a user account for apache
# - each tenant tomcat should run on start up
# - apache should run on startup
# - remove https port logic

#========================#
#====== Add Tenant ======#
#========================#
export tenant="google"
export env="d"
export version="current"
export db_pw="n^2GGSlxO4"
export db_ip="54.225.197.16"
export http_port="8080"
export https_port="8443"
export shutdown_port="8005"
export apj_port="8009"

# add config to temp .bashrc file
cat << EOF > /tmp/tenant.bashrc
  export JAVA_HOME=/usr/share/java7
  export PATH=$JAVA_HOME/bin:$PATH
  export CATALINA_HOME=/usr/share/tomcat7
  export CATALINA_BASE=~/maritz
  export JAVA_OPTS="-Dspring.profiles.active=development -Xmx1024m -XX:+UseConcMarkSweepGC -XX:PermSize=512m -XX:MaxPermSize=1024m"

  #===== START Configurables =====
  # (store in .bashrc just so we have a convenient record.)
  export tenant="$tenant"
  export env="$env"
  export version="$version"
  export db_pw="$db_pw"
  export db_ip="$db_ip"
  export http_port=$http_port
  export https_port=$https_port
  export shutdown_port=$shutdown_port
  export apj_port=$apj_port

  export tenant_name="tenant_$tenant"
  export db_name="tenant_$tenant"
  export db_login="tenant_client"
  export url_domain=$env"-"$version"-"$tenant"-api.cnxtcloud.com"
  # ===== END Configurables =====
EOF

# load configuration
chmod o+x /tmp/tenant.bashrc
. /tmp/tenant.bashrc

# create user for tenant
# pw=$(< /dev/urandom tr -dc [:alnum:] | head -c10)
# echo $pw
# encryptedPw=$(echo $pw | openssl passwd -1 -stdin)
# useradd $tenant_name --password $encryptedPw
useradd "$tenant_name" --password '$1$Iukd/6yR$AaAZ1f1Vu8f4aNlzIme/V.' #pw is password for now

# create tenant .bashrc and set permissions
cat "/tmp/tenant.bashrc" >> "/home/$tenant_name/.bashrc"
chmod uo+rwx "/home/$tenant_name/.bashrc"

# enable remote ssh
mkdir "/home/$tenant_name/.ssh"
chmod 755 "/home/$tenant_name/.ssh"
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCNtzPoDpb+Ev9pYl80dxi7Fe7eAP4XXW7ZzeL9NVVM9Ge/WPmOGRbv0StnE0//0uvukd/d13SgXrnKmdiOIGrkXFnj/1kLcTzxnPvH3VvXJaa9YbJZWEihpQuabev4cGkad6hP8DYzmxCc53lQiYGgNHXj0J00o+0b6YNGkIf2cvrcVyOtoxTSXMEdlMGA2uXqbDZkLNkamfTdHqu/FIfXWbA27qykbkRRLjsQirv+zYZGjHs98E8TBqWVvswy1B8Xj7+ANW+PdFo7VXv8d59EuLyiLbaPUZ3XqFstodUCHdcM6OkWnTxxto7YUPU6B/odyPXxY+a+cP1wflEabwuz m365-app' > "/home/$tenant_name/.ssh/authorized_keys"
chmod u+rw,go= "/home/$tenant_name/.ssh/authorized_keys"
chown "$tenant_name" "/home/$tenant_name/.ssh/authorized_keys"

# create symlinks back to tomcat install
mkdir "/home/$tenant_name/maritz"
cd "/home/$tenant_name/maritz"
cp -r /usr/share/tomcat7/{conf,webapps,logs,work,temp} .
ln -s /usr/share/tomcat7/bin "/home/$tenant_name/maritz/bin"
ln -s /usr/share/tomcat7/lib "/home/$tenant_name/maritz/lib"
ln -s /usr/share/tomcat7/endorsed "/home/$tenant_name/maritz/endorsed"
chmod -R uo+rw /usr/share/tomcat7/logs
chmod -R uo+rw "/home/$tenant_name/maritz/logs"
chown -R $tenant_name "/home/$tenant_name/maritz"

# ====== update ports in server.xml
sed -i "s/8080/$http_port/g" "/home/$tenant_name/maritz/conf/server.xml"
sed -i "s/8443/$https_port/g" "/home/$tenant_name/maritz/conf/server.xml"
sed -i "s/8005/$shutdown_port/g" "/home/$tenant_name/maritz/conf/server.xml"
sed -i "s/8009/$apj_port/g" "/home/$tenant_name/maritz/conf/server.xml"

# ====== update context.xml
cp "/home/$tenant_name/maritz/conf/context.xml" "/home/$tenant_name/maritz/conf/context.bak"
sed -i "/<\/Context>/i\<Resource name=\"jdbc/DataSourcePrimary\"\n  auth=\"Container\"\n  type=\"javax.sql.DataSource\"\n    factory=\"org.apache.tomcat.jdbc.pool.DataSourceFactory\"\n    driverClassName=\"com.microsoft.sqlserver.jdbc.SQLServerDriver\"\n    url=\"jdbc:sqlserver://$db_ip:1433;databaseName=$tenant_name\"\n    username=\"$db_login\"\n    password=\"$db_pw\"\n    initialSize=\"1\"\n    maxActive=\"6\"\n    maxIdle=\"3\"\n    minIdle=\"1\"\n    validationQuery=\"select 1\"\n    poolPreparedStatements=\"true\"\n\/>" "/home/$tenant_name/maritz/conf/context.xml"


# ====== configure Apache
cat > "/etc/httpd/conf.d/$tenant_name.conf" << EOF
<VirtualHost *:80>
  ProxyPreserveHost On
  ProxyRequests Off
  ServerName $url_domain
  ProxyPass / http://localhost:$http_port/ KeepAlive=On
  ProxyPassReverse / http://localhost:$http_port/ KeepAlive=On
</VirtualHost>
EOF

# ====== start Apache
httpd -k restart

# ====== start Tomcat as user
sudo su "$tenant_name"

# ====== bounce tomcat
cd ~/maritz/bin
./startup.sh

# ======= deploy war
# Option 1
#  - Get latest build from latest bamboo build plan
#  - copy to the m365-team S3 bucket and give everyone permissions
#  - wget <URL to war> -O ROOT.war
#  - copy to /home/tenant_<tenant>/maritz/webapps
#  - remove permissions from s3 file
# Option 2
#  - create bamboo deploy plan
#  - run bamboo deploy plan

# ===== Add Route 53 entries
TODO: script this out using AWS cli (or maybe just api calls)
Do in AWS.
Ex. i-v2-google-api.cnxtcloud.com

return 1
