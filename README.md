# cnxt-api
## Development Practices
This is how development and testing should be done in `cnxt-api`. These are part of [The Twelve-Factor App](https://12factor.net/) methodology:

1. Do your development on a feature branch, e.g., F1
1. Add tests to validate the feature works as intended
1. Run all tests locally to make sure your development did not cause a regression
1. Commit all changes as push your feature branch to `origin/F1` (remote)
1. [Jenkins](https://k8s-jenkins.salesnext.com/job/cnxt-services/) will pick it up and start a build/test run for F1
1. When the Jenkins build completes successfully, create a Pull Request for your feature
1. Again, [Jenkins will build your PR](https://k8s-jenkins.salesnext.com/job/cnxt-services/view/change-requests/)
1. When, and only when, the PR build completes successfully and there two approvals by peers, the PR can be merged

Following the steps below, we can now easily run all JUnit tests in this project.  We should minimize the differences
between our development environment and the CI (Continuous Integration) build as much as possible.  Since we 
(and Jenkins - the CI build) both now run the SQL Server database in a Docker container, cleanly bootstrapped for each
test run, we don't have the dependency for a locally installed database instance or the questions that come about local 
data cleanliness.

We can easily, locally run the whole suit of JUnit tests or start the database and run or debug one test at a time.

When we follow these steps, we will be doing the same as the `Jenkins` build, automatically and repeatably, which will 
allow us to move forward confidently with new features knowing that the feature works and we haven't caused a regression.

## New Parent Dependencies
This CultureNext version has had changes made to its parent dependencies to support running more than one instance in the same environment.  This is a requirement for running multiple replicas in Kubernetes (K8s).

Some parent libraries, part of maritz-core, have been forked and modified.  These new dependencies are listed in the `pom.xml` file.

## Spring Properties Files and Active Profiles
We now have more than one `application.properties` file.  This is in keeping with the way Spring manages different sets of properties for different environments.

| Properties File | Purpose | How Used |
|---|---|---|
| `src/main/resources/application.properties` | The Spring Active Profile "`local`". | * The base properties file that contains all `cnxt-api` properties. |
| | | * This is the `local` environment that developers run locally. |
| | | * Properties that are intended for higher environments are excluded from `local` by `*.*.profiles=1local`. |
| | | * Any changes made here must be merged into `k8s/<tenant>-<env>/application.properties` for HA deployments. |
| `src/main/resources/application-containerize.properties` | Jenkins integration testing. | * Additional properties for Jenkins to do container testing. |
| | | * Connection properties for testing with remote resources. |
| | | * Included by `-Dspring.profiles.active=local,containerize`. |
| `src/main/resources/application-docker.properties` | For local testing and debugging the docker container. | *Included by `-Dspring.profiles.active=local,docker`. |
| `k8s/<tenant>-<env>/application.properties` | The K8s deployment properties for each tenant. | * These are copies of merged `src/main/resources/application.properties` and `src/main/resources/application-containerize.properties` |
| | | * They are tied to a K8s namespace (environment) and a Spring Active Profile, e.g., `spring.profiles.active=useracceptance,actuator`. |
| | | * The K8s env and the Spring Active Profile _are not necessarily the same_. |
| | | * The are turned into `ConfigMap`s by `kustomize` for each tenant's K8s container environment. |
| | | * They _must_ be updated when changes are made to `src/main/resources/application.properties`. |
| | | * See [K8s README](k8s/README.md). |

We need to keep all these in sync with any configuration properties changes we make. 

## Docker Issues
If you have issues running `docker-compose` and you're running it from a Linux VM, you may be [hitting this issue](#docker-compose-issues) (see below).

---
## Build Only (no tests) locally
Just build all `src/main/java` and `src/test/java` and skip running the tests.

In project root dir, run:

```$ mvn clean package -DskipTests```

## Build and Run all Tests locally. Do this before pushing your code to a remote branch
**Requires docker (docker-compose) installed**.

* Starts SQL Server database running in a docker container
* creates database schema and users
* runs liquibase against database
    * **TODO**: convert `run-liquiabase.sh` to use [the new liquibase maven plugin](https://maritz.atlassian.net/l/c/CJirixDZ) 
* compiles all code and runs all tests
* terminates the docker container running SQL Server database
* Takes about 15 min to run end to end

In project root dir, run:

   ```$ ./ci/build/run-integ-tests.sh```

## Start SQL Server in a Docker Container and Run/Debug Individual Tests
**Requires docker (docker-compose) installed**.

* Starts SQL Server database running in a docker container
* creates database schema and users
* runs liquibase against database
    * **TODO**: convert `run-liquiabase.sh` to use [the new liquibase maven plugin](https://maritz.atlassian.net/l/c/CJirixDZ) 
* SQL Server is now running with liquibase contexts `live,test` applied
* This allows for the database to be queried, all JUnit tests to be run or individual tests run via your IDE
    * It's available at the URL and connection properties listed in the `src/main/resources/application.properties`, which is `localhost:1433`.
* The SQL Server database remains running in the container until stopped (see below)

In project root directory:
* Start the SQL Server container, create schema and user

    ```$ docker-compose up &```

* After you see the log msg "Database and user creation succeeded" (See NOTE below)...

    ```$ ./ci/build/run-liquibase.sh```
    
    * **TODO**: convert `run-liquiabase.sh` to use [the new liquibase maven plugin](https://maritz.atlassian.net/l/c/CJirixDZ) 

    * **NOTE**: Liquibase only needs to run the first time you run `docker-compose`. The database volume is persisted 
    within the SQL Server Docker image created by `docker-compose`. So, after running Liquibase once, the database is
    ready to go after each `docker-compose up &`.  If you ever need to recreate the database, running `docker-compose down -v`
    will remove the database volume.  But if you do re-run Liquibase on a database that has had it run before, it won't hurt
    anything and will finish quickly.

* Wait until you see the log msg `"Liquibase Update Successful"` and run or debug individual tests from your IDE, query
the database directly or connect SQL client tool, etc.  The connection properties are the same the JUnit tests use, the spring boot `local`
profile in `src/main/resources/application.properties`. 

* To run a JUnit test from the command line:

    ```$ mvn verify -Dtest=<SomeTest>```
    
    * Example:
    
    ```$ mvn test-Dtest=ApprovalDaoTest```
    
* Likewise, a single test can be run and debugged in your IDE.

To stop:
* Stops the SQL Server DB (docker-compose). 
    * Add `-v` to remove the database volume, if wanting to start the database clean the next time.
    * This may be needed to apply certain Liqubase changes.

    ```$ docker-compose down```
---

## Build a cnxt-services Docker Image and Run the Container
**Requires docker (docker-compose) installed**.

You don't really need to run the container as part of feature development.  Running/debugging tests, as described above, will, in most cases, assure your changes work in the legacy (war) as well as K8s (docker container) deployment.  And, in fact, it's not recommended to run the same image locally that's built by Jenkins for K8s.  There are layers needed by K8s that are not needed locally and may cause the container to be unable to start.

But, if you want or need to, these instructions: 
* Build and package the cnxt-api project
* Starts SQL Server database running in a docker container (as above)
* Compiles the executable war (`cnxt-services-executable.war`)
* Builds the `cnxt-services` docker image
* Starts the `cnxt-services` docker container
* Hits an endpoint

In project root directory:
* Start the SQL Server container, create schema and user

    ```$ docker-compose up &```

* **Liquibase only required first time.** See discussion above. After you see the log msg "Database and user creation succeeded"...
    * **TODO**: convert `run-liquiabase.sh` to use [the new liquibase maven plugin](https://maritz.atlassian.net/l/c/CJirixDZ)

    ```$ ./ci/build/run-liquibase.sh```

* Build and package the application:

    ```$ mvn clean package -DskipTests```

* Build the Docker image (pass in log level of your choosing):
	* If you want remote java debugging enabled:
    
        ```$ docker build -f Dockerfile-debug -t cnxt-services .```
    
    * If don't want remote java debugging not enabled build with the default `Dockerfile`:
    
        ```$ docker build -f Dockerfile-local -t cnxt-services .```

* After you you see "Liquibase Update Successful" in the console where you ran `docker-compose`, start the Docker container (add the `-d` flag to run in the background):
    
    ```$ docker run -e "SPRING_PROFILES_ACTIVE=local,docker" --rm -p 8080:8080 -p 8000:8000 --network cnxt-api_local-db-test-network --name cnxt-services cnxt-services```    
    
    * Or, if not running in debug mode:
    
    ```$ docker run -e "SPRING_PROFILES_ACTIVE=local,docker" --rm -p 8080:8080  --network cnxt-api_local-db-test-network --name cnxt-services cnxt-services```    

    * If starting in background mode (using the `-d` flag), tail the container log:
    
        ```docker logs -f cnxt-services```

* After the app is started, hit one of the actuator endpoints:

    * Note the `curl` response can be made more readable, especially when hitting a verbose endpoint like `metrics`, by piping it through [jq](https://stedolan.github.io/jq/).  
Example:

    ```$ curl -v http://localhost:8080/rest/health-check```

    ```$ curl -v http://localhost:8080/rest/actuator/metrics | jq```

To stop:
* Stops the `cnxt-services` container
* Stops the SQL Server DB (docker-compose) but preserves the `tenant_google` database.

```
    $ docker stop cnxt-services
    $ docker-compose down
```
---

## Example execution against a `cnxt-api` container running in K8s
* Replace `<my_user>` and `<my_pwd>` with a valid user name and password.
* This example hits `maritz.com` (`we.com`) running in a UAT environment (`https://uk8s-api.culturenxt.com/rest/projects/maritz/`).
    
```
    $ TOKEN=$(curl -s -d '{"userName":"<my_user>", "password":"<my_pwd>"}' -H "Content-Type: application/json" -X POST https://uk8s-api.culturenxt.com/rest/projects/maritz/authenticate/login | jq -r '.attributes.AuthToken')
    $ curl -s -H "Authorization: Bearer ${TOKEN}" GET https://uk8s-api.culturenxt.com/rest/projects/maritz/project-profile | jq
```  
    
    
* No authentication needed for the `health-check` endpoint:
        
    ```curl -ki -X GET https://uk8s-api.culturenxt.com/rest/projects/maritz/health-check```

---

## Docker Compose Issues
Known Issues with `docker-compose`

`docker-compose` works without issue on Mac OS X and MS Windows but there were issues seen with people running in a 
Linux VM and has to do with `selinux`, the Linux security manager.  When starting the MS SQL Server container, it
prohibits running the `entrypoint.sh` script that creates the schema.  If you see a msg like 
`./entrypoint.sh - PERMISISON DENIED`, you are likely running into this issue.

If you are running this on a local Linux host, you may need to disable SELinux as it can block access to files and give 
you permission denied errors with docker.  As a work around, before running `docker-compose`, you can disable SELinux by
running `setenforce 0` on the host VM as the root user.

This does NOT affect Windows or Mac hosts.
