FROM openjdk:8u242-jdk-slim

RUN yes | apt-get update && yes | apt-get install -y && yes | apt-get install curl bash zip unzip

RUN addgroup --system cnxtgroup
RUN adduser --system cnxtuser
RUN adduser cnxtuser cnxtgroup
RUN mkdir -p /cnxt/newrelic

RUN curl -O "http://download.newrelic.com/newrelic/java-agent/newrelic-agent/current/newrelic-java.zip"
RUN ["unzip", "newrelic-java.zip", "-d", "/cnxt"]

COPY target/cnxt-services-executable.war /cnxt

RUN chown -R cnxtuser /cnxt
RUN chgrp -R cnxtgroup /cnxt

USER cnxtuser
WORKDIR /cnxt

ARG log_level_arg
ENV log_level=${log_level_arg:-INFO}
ENV NR_LICENSE="e1e0d3f05697e941dfa9e2bcdb03301426fd822a"
ENV APP_NAME="cnxt-local"
# Added for New Relic to distinguish the same tenant in diff envs
ENV ENV="-env"

EXPOSE 8080

# WebLogic server settings
#ENV JAVA_OPTS="-server -XX:MaxPermSize=128m -XX:+UseParNewGC -XX:MaxNewSize=256m -XX:NewSize=256m -Xms768m -Xmx768m -XX:SurvivorRatio=128 -XX:MaxTenuringThreshold=0  -XX:+UseTLAB -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+CMSPermGenSweepingEnabled"
# Current Novartis settings
#ENV JAVA_OPTS="-Xms10240m -Xmx10240m -XX:+UseConcMarkSweepGC -Djava.awt.headless=true"

# Initial test:
# Memory limit set to 4GB in container spec
# https://medium.com/faun/java-application-optimization-on-kubernetes-on-the-example-of-a-spring-boot-microservice-cf3737a2219c
# https://developers.redhat.com/blog/2017/04/04/openjdk-and-containers/
#ENV JAVA_OPTS="-server -Xms3g -Xmx3g -XX:MaxRAM=4g -XX:GCTimeRatio=4 -XX:AdaptiveSizePolicyWeight=90 -XX:ParallelGCThreads=2 -javaagent:/cnxt/newrelic/newrelic.jar"

#ENTRYPOINT ["java",  "${JAVA_OPTS}", \
#"-Djava.security.egd=file:/dev/./urandom", \
#"-Dlogging.level.org.springframework=${log_level}", \
#"-jar","./cnxt-services-executable.war"]
ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -Djava.security.egd=file:/dev/./urandom -Dlogging.level.org.springframework=${log_level} -Dnewrelic.config.license_key=$NR_LICENSE -Dnewrelic.config.file=/cnxt/newrelic/newrelic.yml -Dnewrelic.config.app_name=$APP_NAME-$ENV -jar ./cnxt-services-executable.war"]
